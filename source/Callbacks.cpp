/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  DemoApplication.CPP          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLibDemos            RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  WidgetSeeder                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :	Handles the callback		                              */
/*					for the trigger in                                        */
/*					VflApplicationContext          Copyright (c)  1998-2002   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*
* $Id: TriggerCallback.cpp,v 1.2 2009-09-30 09:02:25 gg508531 Exp $
*/
#include "Callbacks.h"
#include <VistaFlowLibAux/VfaSeeder.h>
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>
#include <VflVisGpuParticles.h>
#include <VflGpuParticleRendererInterface.h>
#include <VflGpuParticleRenderer.h>
#include <VflGpuTracerRenderer.h>
#include <VistaOGLExt/Rendering/VistaParticleRenderingProperties.h>


/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/**
 * Set parameters as members.
 */
TriggerCallback::TriggerCallback( IVfaSeeder* pSeeder, IVfaWidgetController* pController )
: m_pSeeder ( pSeeder ), m_pController ( pController )
{}


bool TriggerCallback::Do()
{
	std::cout << "Seed" << std::endl;
	m_pSeeder->Seed();
	return true;
}


NextDarwModeCallback::NextDarwModeCallback(VflVisGpuParticles* particles)
: _particles(particles)
{}

bool NextDarwModeCallback::Do()
{
	auto* renderer = _particles->GetRenderer(_particles->GetCurrentRenderer());
	auto iNewDrawMode = (renderer->GetDrawMode() + 1) % renderer->GetDrawModeCount();
	if (iNewDrawMode == 1)
		iNewDrawMode++;
	renderer->SetDrawMode( iNewDrawMode);
	return true;
}

ParticleSizeCallback::ParticleSizeCallback(VflVisGpuParticles* particles, float scale)
: _particles(particles)
, _scale(scale)
{}

bool ParticleSizeCallback::Do()
{
	_particles->GetProperties()->SetRadius( 
		_scale*_particles->GetProperties()->GetRadius());
	return true;
}

NextRendererCallback::NextRendererCallback(VflVisGpuParticles* particles)
: _particles(particles)
{}

bool NextRendererCallback::Do()
{
	_particles->SetCurrentTracer(
		(_particles->GetCurrentTracer() + 1) % _particles->GetTracerCount());
	return true;
}

ToggleHalosCallback::ToggleHalosCallback(VflVisGpuParticles* particles)
: _particles(particles)
{}

bool ToggleHalosCallback::Do()
{
	auto* renderer = _particles->GetRenderer(_particles->GetCurrentRenderer());

	VistaParticleRenderingProperties* properties = nullptr;

	auto particle = dynamic_cast<VflGpuParticleRenderer*>(renderer);
	if(particle)
		properties = particle->GetParticleRenderingProperties();
	auto tracer = dynamic_cast<VflGpuTracerRenderer*>(renderer);
	if( tracer )
		properties = tracer->GetParticleRenderingProperties();
	if(properties)
		properties->SetRenderHalos(!properties->GetRenderHalos());

	return true;
}

ToggleHaloColorCallback::ToggleHaloColorCallback(VflVisGpuParticles* particles)
: _particles(particles)
{}

bool ToggleHaloColorCallback::Do()
{
	auto* renderer = _particles->GetRenderer(_particles->GetCurrentRenderer());

	VistaParticleRenderingProperties* properties = nullptr;

	auto particle = dynamic_cast<VflGpuParticleRenderer*>(renderer);
	if( particle )
		properties = particle->GetParticleRenderingProperties();
	auto tracer = dynamic_cast<VflGpuTracerRenderer*>(renderer);
	if( tracer )
		properties = tracer->GetParticleRenderingProperties();
	if( properties )
	{
		VistaColor col = properties->GetHaloColor();
		col[0] = 1- col[0];
		col[1] = 1- col[1];
		col[2] = 1- col[2];
		properties->SetHaloColor(col);
	}

	return true;
}
/*============================================================================*/
/*  END OF FILE "TriggerCallback.cpp"                                         */
/*============================================================================*/
