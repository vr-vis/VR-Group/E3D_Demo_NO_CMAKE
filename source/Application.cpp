/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// projct stuff
#include <GL/glew.h>

#include "Application.h"
#include "BoxWidgetPointGenerator.h"
#include "Callbacks.h"

#include <sstream>

// ViSTA base classes.
#include <VistaBase/VistaStreamUtils.h>
#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/GraphicsManager/VistaGraphicsManager.h>
#include <VistaKernel/GraphicsManager/VistaSceneGraph.h>
#include <VistaTools/VistaIniFileParser.h>

// ViSTA OGLExt.
#include <VistaOGLExt/VistaShaderRegistry.h>
#include <VistaOGLExt/Rendering/VistaParticleRenderingProperties.h>

// VistaFolwlib
#include <VistaFlowLib/Visualization/VflVtkLookupTable.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaFlowLib/Visualization/Geometry/VflVisGeometry.h>
#include <VistaFlowLib/Visualization/Geometry/VflDefaultGeometryRenderer.h>

// VistaFlowLibAux
#include <VistaFlowLibAux/VfaSeeder.h>
#include <VistaFlowLibAux/VfaBaseApplication.h>
#include <VistaFlowLibAux/Widgets/VfaWidget.h>
#include <VistaFlowLibAux/Widgets/VfaWidgetManager.h>
#include <VistaFlowLibAux/Widgets/VfaIntentionSelectFocusStrategy.h>
#include <VistaFlowLibAux/Widgets/Box/VfaBoxWidget.h>
#include <VistaFlowLibAux/Interaction/VfaCommandToActionHandler.h>

// VistaVisExt
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Data/VveVtkData.h>
#include <VistaVisExt/Data/VveTetGrid.h>
#include <VistaVisExt/Data/VveUnsteadyTetGrid.h>
#include <VistaVisExt/IO/VveUnsteadyTetGridLoader.h>
#include <VistaVisExt/IO/VveUnsteadyVtkDataLoader.h>

// GPU Particles
#include <VflGpuParticlesTetGrid.h>
#include <VflGpuTracersTetGrid.h>
#include <VflGpuParticleRenderer.h>
#include <VflGpuParticleSorter.h>
#include <VflVisGpuParticles.h>
#include <VflGpuParticleSeeder.h>
#include <VflGpuTracerRenderer.h>


#include <vtkPolyData.h>
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
Application::Application()
: _system(std::make_unique<VistaSystem>())
, _scene()
{
	// In this constructor, only some basic ViSTA CoreLib-related stuff is
	// going on. For details refer to the tutorials coming along the ViSTA
	// CoreLib package.

	// Display the standard ViSTA banner (just to keep corporate identity).
	_system->IntroMsg();

	// Mind to init the search path for configuration files particularly mind to
	// include the "global" ini-files.
	std::list<std::string> liSearchPaths;
	liSearchPaths.push_back("configfiles/");
	liSearchPaths.push_back("../configfiles/");
	liSearchPaths.push_back("../../configfiles/");
	_system->SetIniSearchPaths(liSearchPaths);
}

Application::~Application()
{
	//_seeder.reset();
	//_scene.reset();
	//_system.reset();
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
bool Application::InitBasics(int argc, char** argv)
{
	if (!_system->Init(argc, argv))
	{
		vstr::errp() << "VistaSystem initialization failed\n" << std::endl;
		vstr::erri() << "\tBAILING OUT!\n" << std::endl;

		_system.reset();

		return false;
	}

	_scene = std::make_unique<VfaBaseApplication>(_system.get(), true, false);

	VistaShaderRegistry::GetInstance().AddSearchDirectory("resources/shaders/");
	return true;
}

bool Application::InitDemo()
{
	VistaSceneGraph* pSG = _system->GetGraphicsManager()->GetSceneGraph();

	VistaIniFileParser oFile;
	std::list<std::string> liSearchPaths;
	liSearchPaths.push_back("configfiles/");
	if (oFile.ReadFile("flowvis.ini",liSearchPaths) == false)
	{
		vstr::err() << "Couldn't find configuration file \"flowvis.ini\"." << std::endl;
		return false;
	}
	auto PropertyList = oFile.GetPropertyList();

	auto GPUParticlesPattern = PropertyList.GetValue<std::string>("DATA_PATTERN");
	auto GPUParticlesDirectory = PropertyList.GetValue<std::string>("DATA_DIRECTORY");
	auto RoomModelFile = PropertyList.GetValue<std::string>("ROOM_FILE");

	stringstream RoomModelStream;
	stringstream GPUParticleStream;

	GPUParticleStream << "data/" << GPUParticlesDirectory << "/" << GPUParticlesPattern << ".vtk";
	InitGPUParticles(LoadeTetGrid(GPUParticleStream.str(), 1));
	//InitGPUParticles(LoadeTetGrid("data/computation_room_0117/natural_convection_**.vtk", 1));
	InitSeederWidgets();

	RoomModelStream << "data/models/" << RoomModelFile;
	//InitGeometryRenderer(LoadePolyData("data/computation_room_0117/room.vtk", 1));
	InitGeometryRenderer(LoadePolyData(RoomModelStream.str(), 1));

	_scene->RegisterAction('t', new TriggerCallback(_seeder.get(), _widget->GetController()));
	_scene->RegisterAction('z', new NextRendererCallback(_gpuParticles.get()));
	_scene->RegisterAction('u', new NextDarwModeCallback(_gpuParticles.get()));
	_scene->RegisterAction('h', new ToggleHalosCallback(_gpuParticles.get()));
	_scene->RegisterAction('c', new ToggleHaloColorCallback(_gpuParticles.get()));
	_scene->RegisterAction('i', new ParticleSizeCallback(_gpuParticles.get(),1.1f));
	_scene->RegisterAction('o', new ParticleSizeCallback(_gpuParticles.get(),0.9f));

	_scene->GetRenderNode()->SetRotation(VistaQuaternion(VistaAxisAndAngle(
		VistaVector3D(1,0,0), Vista::DegToRad(-90))));


	return true;
}

void Application::Run()
{
	// Enter the app loop of the underlying ViSTA system.
	_system->Run();
}

VveUnsteadyTetGrid* Application::LoadeTetGrid(
	const std::string& file, 
	unsigned numLevels)
{
	VveTimeMapper::Timings timings;
	timings.m_iFirstLevel = 0;
	timings.m_iNumberOfLevels = numLevels;
	timings.m_dSimStart = 0.0f;
	timings.m_dSimEnd = 1.0f;
	timings.m_iNumberOfIndices = numLevels;

	_particleTimeMapper = std::make_unique<VveTimeMapper>(timings);

	auto* grid = new VveUnsteadyTetGrid(_particleTimeMapper.get());
	// get amount of time steps
	int iLevelCount = _particleTimeMapper->GetNumberOfTimeLevels();
	for( int i = 0; i<iLevelCount; ++i )
	{
		grid->GetTypedLevelDataByLevelIndex(i)->SetData(new VveTetGrid);
	}

	VveUnsteadyTetGridLoader loader(grid);
	loader.GetProperties()->SetFilePattern(file);
	//loader.GetProperties()->SetScalarsName("pressure");
	loader.GetProperties()->SetVectorsName("veloc");
	loader.LoadDataThreadSafe();

	return grid;
}

VveUnsteadyVtkPolyData* Application::LoadePolyData(const std::string& file, unsigned numLevels)
{

	VveTimeMapper::Timings timings;
	timings.m_iFirstLevel = 0;
	timings.m_iNumberOfLevels = numLevels;
	timings.m_dSimStart = 0.0f;
	timings.m_dSimEnd = 1.0f;
	timings.m_iNumberOfIndices = numLevels;

	_geometryTimeMapper = std::make_unique<VveTimeMapper>(timings);

	auto* polyData = new VveUnsteadyVtkPolyData(_geometryTimeMapper.get());
	// get amount of time steps
	int iLevelCount = _particleTimeMapper->GetNumberOfTimeLevels();
	for( int i = 0; i<iLevelCount; ++i )
	{
		polyData->GetTypedLevelDataByLevelIndex(i)->SetData(vtkPolyData::New());
	}

	VveUnsteadyVtkDataLoader loader(polyData);
	loader.GetProperties()->SetFilePattern(file);
	loader.GetProperties()->SetFileFormat(VveUnsteadyVtkDataLoader::DF_VTK_POLYDATA);
	loader.LoadDataThreadSafe();

	return polyData;
}

void Application::InitGPUParticles(VveUnsteadyTetGrid* grid)
{
	_lookUpTable = std::make_unique<VflVtkLookupTable>();
	_lookUpTable->SetNameProp("Particle LUT");
	_lookUpTable->SetTableRange(0.0f, 1.0f);
	_lookUpTable->SetHueRange(0.67f, 0.0f);
	_lookUpTable->SetAlphaRange(1.0f, 1.0f);
	_lookUpTable->SetValueCount(256);


	_gpuParticles = std::make_unique<VflVisGpuParticles>(grid, _lookUpTable.get());
	_gpuParticles->SetNameForNameable("gpu_particles");
	_gpuParticles->GetProperties()->SetParticleTextureSize(512, 512);
	_gpuParticles->GetProperties()->SetLookAheadCount(0);
	_gpuParticles->GetProperties()->SetWrapAround(true);
	_gpuParticles->GetProperties()->SetStreamingUpdate(true);
	_gpuParticles->GetProperties()->SetActive(true);
	_gpuParticles->GetProperties()->SetIntegrator(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3);
	_gpuParticles->GetProperties()->SetDeltaTime(0.05f);
	_gpuParticles->GetProperties()->SetMaxDeltaTime(0.5f);
	_gpuParticles->GetProperties()->SetAdaptiveTimeSteps(true);
	_gpuParticles->GetProperties()->SetRadius(0.002f);
	_gpuParticles->GetProperties()->SetPointSize(0.5);
	_gpuParticles->GetProperties()->SetComputeScalars(true);
	_gpuParticles->GetProperties()->SetSingleBuffered(false);
	_gpuParticles->GetProperties()->SetUseLookupTable(true);
	_gpuParticles->GetProperties()->SetAllowSorting(true);
	_gpuParticles->GetProperties()->SetForceHardwareInterpolation(false);
	_gpuParticles->GetProperties()->SetForceDataPreparation(false);
	_gpuParticles->GetProperties()->SetRespectProcessingUnit(true);
	_gpuParticles->GetProperties()->SetRespectTracerType(true);

	_particlesTreacer = std::make_unique<VflGpuParticlesTetGrid>(_gpuParticles.get());
	_traceTreacer = std::make_unique<VflGpuTracersTetGrid>(_gpuParticles.get());

	_particlesRenderer = std::make_unique<VflGpuParticleRenderer>(_gpuParticles.get());
	_particlesRenderer->SetDrawMode(
		//VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS_DEPTH_REPLACE
		//VistaParticleRenderingProperties::DM_SMOKE
		VistaParticleRenderingProperties::DM_SIMPLE
		);

	_traceRenderer = std::make_unique<VflGpuTracerRenderer>(_gpuParticles.get());
	_traceRenderer->SetDrawMode(
		//VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS_DEPTH_REPLACE
		//VistaParticleRenderingProperties::DM_SMOKE
		VistaParticleRenderingProperties::DM_SIMPLE
		);

	if( _gpuParticles->Init() )
	{
		_scene->GetRenderNode()->AddRenderable(_gpuParticles.get()); // render the particles
	}
}

void Application::InitSeederWidgets()
{
	_scene->GetApplicationContext()->RegisterCommand("resize");
	_widget = std::make_unique<VfaBoxWidget>(_scene->GetRenderNode());

	//_widget->GetModel()->SetTranslation(VistaVector3D(.5, 0.5, 0.5));
	//_widget->GetModel()->SetExtents(1.0f, 1.0f, 1.0f);

	_widget->GetModel()->SetTranslation(VistaVector3D(.075, -0.07, -0.1));
	_widget->GetModel()->SetExtents(2.91f, 4.74f, 2.74f);

	_widget->GetView()->GetProperties()->SetLineColor(VistaColor(0, 0, 1));
	_widget->GetModel()->SetRotation(VistaQuaternion(0.0f, 0.0f, 0.0f, 1.0f));
	_widget->GetView()->GetProperties()->SetLineWidth(0.03f);
	_widget->GetController()->GetProperties()->SetHandleRadius(0.03f);

	_pointGen = std::make_unique<BoxWidgetPointGenerator>(
		_widget->GetModel(),
		_scene->GetRenderNode()->GetVisTiming(),
		_particleTimeMapper.get(),
		100,
		"MODE_CONST_NUMBER_RAND");

	_seeder = std::make_unique<VflGpuParticleSeeder>(_gpuParticles.get(), _pointGen.get());

	_widgetMgr = std::make_unique<VfaWidgetManager>(_scene->GetApplicationContext());
	auto* intentSelect = new VfaIntentionSelectFocusStrategy();
	_widgetMgr->SetFocusStrategy(intentSelect);
	_widgetMgr->RegisterWidget(_widget.get());
}

void Application::InitGeometryRenderer(VveUnsteadyVtkPolyData* data)
{
	_visGeometry = std::make_unique<VflVisGeometry>();

	_visGeometry->SetUnsteadyData(data);
	_visGeometry->SetNameForNameable("test_geometry");
	_visGeometry->Init();

	//Create new default renderer
	VflDefaultGeometryRenderer* renderer = new VflDefaultGeometryRenderer(_visGeometry.get());
	renderer->SetScalarName("TextureCoords");
	renderer->SetDrawMode(VflDefaultGeometryRenderer::DM_SOLID_COLOR);
	renderer->SetIsLightingEnabled(true);
	renderer->SetColor(VistaColor(0.5f, 0.5f, 0.5f));
	renderer->SetIsFaceCullingEnabled(true);
	_visGeometry->AddRenderer(renderer);

	_scene->GetRenderNode()->AddRenderable(_visGeometry.get());
}

/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

