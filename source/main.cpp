
#include "Application.h"

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
int main(int argc, char **argv)
{
	//setup the basic tutorial object
	Application app;

	//initialize basics (you'll not have to have a look at this one)
	if(!app.InitBasics(argc, argv))
	{
		system("Pause");
		return -1;
	}

	if(!app.InitDemo())
	{
		system("Pause");
		return -1;
	}
	//finally kick it...
	app.Run();



	return 0;
}


/*============================================================================*/
/* END OF FILE	                                                              */
/*============================================================================*/