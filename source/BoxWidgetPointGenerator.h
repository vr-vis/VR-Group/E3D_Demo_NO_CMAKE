/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _BOXWIDGETPOINTGENERATOR_H
#define _BOXWIDGETPOINTGENERATOR_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VfaPointGenerator.h>
#include <string>
#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaBoxModel;
class VflVisTiming;
class VveTimeMapper;

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class BoxWidgetPointGenerator : public IVfaPointGenerator
{

public:

	/**
	 * Use this enum to set PointGenerator properties.
	 */
	enum
	{
		MODE_CONST_NUMBER,
		MODE_CONST_DENSITY,
		MODE_CONST_NUMBER_RAND,
		MODE_CONST_DENSITY_RAND
	};

	/**
	 * iDenity can have different meanings. In case you use a "number" mode it is the amount
	 * of particles. Otherwise it is the particle density in its original meaning.
	 * @todo think about default strMode
	 */
	BoxWidgetPointGenerator(	const VfaBoxModel * const pModel,
								const VflVisTiming * const pVisTiming,
								VveTimeMapper * pTimeMapper,
								int iDensity, const std::string& strMode
							);
	virtual ~BoxWidgetPointGenerator();

	/**
	 * Inherited from IVfaPointGenerator interface.
	 */
	int GetPoints( std::vector<float>& vecPoints4D );
	
	/**
	 * Sets the mode by string given in the enum.
	 * Retruns true if the string is in the enum.
	 */
	bool SetGeneratorMode( const std::string& strModename );

	/**
	 * Sets the mode without checking if it is
	 * a correct mode or not.
	 * @todo check?!
	 */
	void SetGeneratorMode( const int iMode );

	/**
	 * returns the generator mode.
	 */
	int  GetGeneratorMode( );

private:

	int                        _mode;
	int                        _density;
	const VfaBoxModel * const  _model;
	const VflVisTiming * const _visTiming;
	VveTimeMapper *           _timeMapper;

};

/*============================================================================*/
/* END OF FILE "BoxWidgetPointGenerator.h                                     */
/*============================================================================*/
#endif /* _BOXWIDGETPOINTGENERATOR_H */
