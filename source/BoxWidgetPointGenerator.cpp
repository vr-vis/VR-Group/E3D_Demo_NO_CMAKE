/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  BoxWidgetPointGenerator.h    RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLibDemos            RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  WidgetSeeder				   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  Generates Points from                                     */
/*                  from a BoxWidget for                                      */
/*                  the VfaSeeder API.             Copyright (c)  1998-2001   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*
 * $Id: BoxWidgetPointGenerator.cpp,v 1.5 2009-10-06 14:45:48 gg508531 Exp $
 */

#include "BoxWidgetPointGenerator.h"

#include <VistaFlowLibAux/Widgets/Box/VfaBoxModel.h>

#include <VistaFlowLib/Visualization/VflVisTiming.h>

#include <VistaVisExt/Tools/VveTimeMapper.h>

#ifdef WIN32
#include <time.h>
#endif

#include <cstdlib>

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
BoxWidgetPointGenerator::BoxWidgetPointGenerator( 
										const VfaBoxModel * const pModel, 
										const VflVisTiming * const pVisTiming,
										VveTimeMapper * pTimeMapper,
										int iDensity, const std::string& strMode )
	:	_model( pModel ), 
		_density( iDensity ), 
		_visTiming( pVisTiming ),
		_timeMapper( pTimeMapper )
{
	// Set Generator mode by string
	SetGeneratorMode( strMode );
}

BoxWidgetPointGenerator::~BoxWidgetPointGenerator()
{
}

int BoxWidgetPointGenerator::GetPoints( std::vector<float>& vecPoints4D )
{
	vecPoints4D.clear();
	
	float vec1[3];
	float vec2[3];
	float vec3[3];

	float pnt1[3];
	float pnt2[3];

	int size;

	_model->GetCorner( VfaBoxModel::CORNER_LLB, pnt1 );
	_model->GetCorner( VfaBoxModel::CORNER_RLB, pnt2 );

	vec1[0] = pnt2[0] - pnt1[0];
	vec1[1] = pnt2[1] - pnt1[1];
	vec1[2] = pnt2[2] - pnt1[2];

	_model->GetCorner( VfaBoxModel::CORNER_LTB, pnt2 );

	vec2[0] = pnt2[0] - pnt1[0];
	vec2[1] = pnt2[1] - pnt1[1];
	vec2[2] = pnt2[2] - pnt1[2];

	_model->GetCorner( VfaBoxModel::CORNER_LLF, pnt2 );

	vec3[0] = pnt2[0] - pnt1[0];
	vec3[1] = pnt2[1] - pnt1[1];
	vec3[2] = pnt2[2] - pnt1[2];

	const double dVisTime = _visTiming->GetVisualizationTime();
	const double dSimTime = _timeMapper->GetSimulationTime( dVisTime );

	const float fDiv = 1.0f / ( _density - 1 );

	switch ( _mode )
	{
	case MODE_CONST_NUMBER:
	{
		vecPoints4D.reserve(4*_density*_density*_density);
		size = _density*_density*_density;
		for( int a = 0; a < _density; ++a )
		{
			for( int b = 0; b < _density; ++b )
			{
				for( int c = 0; c < _density; ++c )
				{
					//p = LLB + a*v1/(D-1) + b*v2/(D-1) + c*v3/(D-1)
					vecPoints4D.push_back(
							pnt1[0] +
							a*vec1[0]*fDiv +
							b*vec2[0]*fDiv +
							c*vec3[0]*fDiv );
					vecPoints4D.push_back(
							pnt1[1] +
							a*vec1[1]*fDiv +
							b*vec2[1]*fDiv +
							c*vec3[1]*fDiv );
					vecPoints4D.push_back(
							pnt1[2] +
							a*vec1[2]*fDiv +
							b*vec2[2]*fDiv +
							c*vec3[2]*fDiv );
					vecPoints4D.push_back( static_cast<float>(dSimTime) );

				}
			}
		}
	}
		break;
	case MODE_CONST_DENSITY:
	{
		float l1 = sqrt(vec1[0]*vec1[0] + vec1[1]*vec1[1] + vec1[2]*vec1[2]);
		float l2 = sqrt(vec2[0]*vec2[0] + vec2[1]*vec2[1] + vec2[2]*vec2[2]);
		float l3 = sqrt(vec3[0]*vec3[0] + vec3[1]*vec3[1] + vec3[2]*vec3[2]);

		float fDivl1 = 1.0f / (_density * l1);
		float fDivl2 = 1.0f / (_density * l2);
		float fDivl3 = 1.0f / (_density * l3);

		const int iNumI = static_cast<int>(floor(l1 * _density));
		const int iNumJ = static_cast<int>(floor(l2 * _density));
		const int iNumK = static_cast<int>(floor(l3 * _density));

		vecPoints4D.reserve(4*iNumI*iNumJ*iNumK);
		size = iNumI*iNumJ*iNumK;
		for( int a = 0; a <= iNumI; ++a )
		{
			for( int b = 0; b <= iNumJ; ++b )
			{
				for( int c = 0; c <= iNumK; ++c )
				{
					vecPoints4D.push_back(
							pnt1[0] +
							a*vec1[0]*fDivl1 +
							b*vec2[0]*fDivl2 +
							c*vec3[0]*fDivl3 );
					vecPoints4D.push_back(
							pnt1[1] +
							a*vec1[1]*fDivl1 +
							b*vec2[1]*fDivl2 +
							c*vec3[1]*fDivl3 );
					vecPoints4D.push_back(
							pnt1[2] +
							a*vec1[2]*fDivl1 +
							b*vec2[2]*fDivl2 +
							c*vec3[2]*fDivl3 );
					vecPoints4D.push_back( static_cast<float>(dSimTime) );
				}
			}
		}
	}
	break;
	case MODE_CONST_NUMBER_RAND:
	{
		srand( static_cast<unsigned int>(_visTiming->GetCurrentClock()) ) ;
		vecPoints4D.reserve(4*_density);
		size = _density;
		for( int i = 0; i < _density; ++i )
		{
			const float r1 = rand() / ( RAND_MAX + 1.0f );
			const float r2 = rand() / ( RAND_MAX + 1.0f );
			const float r3 = rand() / ( RAND_MAX + 1.0f );

			vecPoints4D.push_back(
					pnt1[0] +
					vec1[0]*r1 +
					vec2[0]*r2 +
					vec3[0]*r3 );
			vecPoints4D.push_back(
					pnt1[1] +
					vec1[1]*r1 +
					vec2[1]*r2 +
					vec3[1]*r3 );
			vecPoints4D.push_back(
					pnt1[2] +
					vec1[2]*r1 +
					vec2[2]*r2 +
					vec3[2]*r3 );
			vecPoints4D.push_back( static_cast<float>(dSimTime) );
		}
	}
	break;
	case MODE_CONST_DENSITY_RAND:
	{
		const float l1 = sqrt(vec1[0]*vec1[0] + vec1[1]*vec1[1] + vec1[2]*vec1[2]);
		const float l2 = sqrt(vec2[0]*vec2[0] + vec2[1]*vec2[1] + vec2[2]*vec2[2]);
		const float l3 = sqrt(vec3[0]*vec3[0] + vec3[1]*vec3[1] + vec3[2]*vec3[2]);
		const int max = static_cast<int>(
			floor( l1*l2*l3*_density*_density*_density ));

		srand( static_cast<unsigned int>(_visTiming->GetCurrentClock()) ) ;
		vecPoints4D.reserve(4*max);
		size = max;
		for( int i = 0; i < max; ++i )
		{
			const float r1 = rand() / ( RAND_MAX + 1.0f );
			const float r2 = rand() / ( RAND_MAX + 1.0f );
			const float r3 = rand() / ( RAND_MAX + 1.0f );

			vecPoints4D.push_back(
					pnt1[0] +
					vec1[0]*r1 +
					vec2[0]*r2 +
					vec3[0]*r3 );
			vecPoints4D.push_back(
					pnt1[1] +
					vec1[1]*r1 +
					vec2[1]*r2 +
					vec3[1]*r3 );
			vecPoints4D.push_back(
					pnt1[2] +
					vec1[2]*r1 +
					vec2[2]*r2 +
					vec3[2]*r3 );
			vecPoints4D.push_back( static_cast<float>(dSimTime) );
		}
	}
	break;
	}
	return size;
}

bool BoxWidgetPointGenerator::SetGeneratorMode(const std::string& strModename )
{
	/*
	  	MODE_CONST_NUMBER,
		MODE_CONST_DENSITY,
		MODE_CONST_NUMBER_RAND,
		MODE_CONST_DENSITY_RAND
	 */
	if( strModename == "MODE_CONST_NUMBER" )
	{
		SetGeneratorMode( MODE_CONST_NUMBER );
		return true;
	}
	else if( strModename == "MODE_CONST_DENSITY" )
	{
		SetGeneratorMode( MODE_CONST_DENSITY );
		return true;
	}
	else if( strModename == "MODE_CONST_NUMBER_RAND" )
	{
		SetGeneratorMode( MODE_CONST_NUMBER_RAND );
		return true;
	}
	else if( strModename == "MODE_CONST_DENSITY_RAND" )
	{
		SetGeneratorMode( MODE_CONST_DENSITY_RAND );
		return true;
	}
	else
	{
		return false;
	}
}

void BoxWidgetPointGenerator::SetGeneratorMode( const int iMode )
{
	_mode = iMode;
}

int BoxWidgetPointGenerator::GetGeneratorMode( )
{
	return _mode;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
