/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  TriggerCallback.h            RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLibDemos            RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  WidgetSeeder				   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  Handles the callback                                      */
/*                  for the trigger in                                        */
/*                  VflApplicationContext          Copyright (c)  1998-2001   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*
 * $Id: TriggerCallback.h,v 1.2 2009-09-30 09:02:25 gg508531 Exp $
 */

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _TRIGGERCALLBACK_H
#define _TRIGGERCALLBACK_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaExplicitCallbackInterface.h>
#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaSeeder;
class IVfaWidgetController;
class VflVisGpuParticles;

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This callback is used to handle the button click for seeding
 */
class TriggerCallback : public IVistaExplicitCallbackInterface
{
public:

	/**
	 * Constructor needs two vectors of seeders and controllers
	 */
	TriggerCallback( IVfaSeeder* pSeeder, IVfaWidgetController* pController );

	bool Do();
private:
	
	IVfaSeeder				*m_pSeeder;

	IVfaWidgetController	*m_pController;
};

class NextDarwModeCallback : public IVistaExplicitCallbackInterface
{
public:

	/**
	 * Constructor needs two vectors of seeders and controllers
	 */
	NextDarwModeCallback( VflVisGpuParticles* );

	bool Do();
private:

	VflVisGpuParticles* _particles;
};

class ParticleSizeCallback : public IVistaExplicitCallbackInterface
{
public:

	/**
	 * Constructor needs two vectors of seeders and controllers
	 */
	ParticleSizeCallback( VflVisGpuParticles* , float);

	bool Do();
private:

	VflVisGpuParticles* _particles;
	float               _scale;
};

class NextRendererCallback : public IVistaExplicitCallbackInterface
{
public:

	/**
	 * Constructor needs two vectors of seeders and controllers
	 */
	NextRendererCallback( VflVisGpuParticles*);

	bool Do();
private:

	VflVisGpuParticles* _particles;
};

class ToggleHalosCallback : public IVistaExplicitCallbackInterface
{
public:

	/**
	 * Constructor needs two vectors of seeders and controllers
	 */
	ToggleHalosCallback( VflVisGpuParticles*);

	bool Do();
private:

	VflVisGpuParticles* _particles;
};

class ToggleHaloColorCallback : public IVistaExplicitCallbackInterface
{
public:

	/**
	 * Constructor needs two vectors of seeders and controllers
	 */
	ToggleHaloColorCallback( VflVisGpuParticles*);

	bool Do();
private:

	VflVisGpuParticles* _particles;
};


/*============================================================================*/
/* END OF FILE "TriggerCallback.h                                             */
/*============================================================================*/
#endif /* _TRIGGERCALLBACK_H */
