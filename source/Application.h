#ifndef APPLICATION_H
#define APPLICATION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <memory>
#include <string>

#include <VistaVisExt/Data/VveVtkData.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaSystem;
class VfaBaseApplication;
class VveTimeMapper;
class VflGpuParticlesTetGrid;
class VflVtkLookupTable;
class VflVisGpuParticles;
class VflGpuParticleRenderer;
class VfaBoxWidget;
class BoxWidgetPointGenerator;
class VflGpuParticleSeeder;
class VfaWidgetManager;
class VflGpuTracerRenderer;
class VflGpuTracersTetGrid;
class VveUnsteadyTetGrid;
class VflVisGeometry;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class Application
{
public:
	Application();
	virtual ~Application();

	bool InitBasics(int argc, char** argv);

	bool InitDemo();

	void Run();

private:
	VveUnsteadyTetGrid*     LoadeTetGrid(const std::string& file, unsigned numLevels);
	VveUnsteadyVtkPolyData* LoadePolyData(const std::string& file, unsigned numLevels);
	
	void InitGPUParticles(VveUnsteadyTetGrid*);
	void InitSeederWidgets();


	void InitGeometryRenderer(VveUnsteadyVtkPolyData*);

private:
	std::unique_ptr<VistaSystem>        _system;
	std::unique_ptr<VfaBaseApplication> _scene;

	std::unique_ptr<VveTimeMapper> _particleTimeMapper;
	std::unique_ptr<VveTimeMapper> _geometryTimeMapper;

	std::unique_ptr<VflVtkLookupTable> _lookUpTable;

	std::unique_ptr<VflVisGpuParticles>	    _gpuParticles;
	std::unique_ptr<VflGpuParticleRenderer>	_particlesRenderer;
	std::unique_ptr<VflGpuTracerRenderer>   _traceRenderer;

	std::unique_ptr<VflGpuParticlesTetGrid> _particlesTreacer;
	std::unique_ptr<VflGpuTracersTetGrid>   _traceTreacer;

	std::unique_ptr<VfaWidgetManager>       _widgetMgr;

	std::unique_ptr<VfaBoxWidget>            _widget;
	std::unique_ptr<BoxWidgetPointGenerator> _pointGen;
	std::unique_ptr<VflGpuParticleSeeder>    _seeder;


	std::unique_ptr<VflVisGeometry>    _visGeometry;
};

#endif // include guard
/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
