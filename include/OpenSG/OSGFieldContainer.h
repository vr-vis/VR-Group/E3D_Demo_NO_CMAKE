/*---------------------------------------------------------------------------*\
 *                                OpenSG                                     *
 *                                                                           *
 *                                                                           *
 *           Copyright (C) 2000-2002,2002 by the OpenSG Forum                *
 *                                                                           *
 *                            www.opensg.org                                 *
 *                                                                           *
 *   contact: dirk@opensg.org, gerrit.voss@vossg.org, jbehr@zgdv.de          *
 *                                                                           *
\*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*\
 *                                License                                    *
 *                                                                           *
 * This library is free software; you can redistribute it and/or modify it   *
 * under the terms of the GNU Library General Public License as published    *
 * by the Free Software Foundation, version 2.                               *
 *                                                                           *
 * This library is distributed in the hope that it will be useful, but       *
 * WITHOUT ANY WARRANTY; without even the implied warranty of                *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
 * Library General Public License for more details.                          *
 *                                                                           *
 * You should have received a copy of the GNU Library General Public         *
 * License along with this library; if not, write to the Free Software       *
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
 *                                                                           *
\*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*\
 *                                Changes                                    *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                                                                           *
\*---------------------------------------------------------------------------*/

#ifndef _OSGFIELDCONTAINER_H_
#define _OSGFIELDCONTAINER_H_
#ifdef __sgi
#pragma once
#endif

#if 0
#include <OpenSG/OSGFieldContainerPtrForward.h>

#include <OpenSG/OSGFieldContainerPtrFuncsImpl.h>

#include <OpenSG/OSGFieldContainerPtrImpl.h>

#include <OpenSG/OSGFieldContainerTypeImpl.h>
#include <OpenSG/OSGFieldContainerTypeImpl.inl>

#include <OpenSG/OSGFieldContainerImpl.h>

#include <OpenSG/OSGFieldDescriptionImpl.h>

#include <OpenSG/OSGFieldContainerFactoryImpl.h>
#include <OpenSG/OSGFieldContainerFactoryImpl.inl>

#include <OpenSG/OSGChangeList.h>
#include <OpenSG/OSGFieldContainerPtrImpl.inl>

#include <OpenSG/OSGFieldContainerImpl.inl>
#include <OpenSG/OSGFieldDescriptionImpl.inl>

#include <OpenSG/OSGFieldContainerPtrFuncsImpl.inl>
#endif

#include <OpenSG/OSGPrimary.h>

#endif /* _OSGFIELDCONTAINER_H_ */
