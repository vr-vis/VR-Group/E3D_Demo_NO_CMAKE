/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuParticleData.cpp       RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$


/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflGpuParticleData.h"
#include "VflVisGpuParticles.h"

#include <VistaKernel/VistaSystem.h>
#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaFramebufferObj.h>
#include <VistaOGLExt/VistaShaderRegistry.h>
#include <VistaOGLExt/VistaGLSLShader.h>

#include <cassert>
#include <string.h>

using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static void DumpTexture(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						ostream &out);

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflGpuParticleData::VflGpuParticleData(VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties)
: m_pProperties(pProperties),
  m_pFBO(NULL),
  m_iCurrentData(0),
  m_dDataTimeStamp(0),
  m_dTextureTimeStamp(0),
  m_iWidth(1),
  m_iHeight(1),
  m_iActiveParticles(0),
  m_iNextParticle(0),
  m_iActiveTracers(0),
  m_iNextTracer(0),
  m_iCurrentLine(0),
  m_pPassThroughShader(NULL)
{
	// retrieve some parameters
	m_pProperties->GetParticleTextureSize(m_iWidth, m_iHeight);

	m_aData[0] = m_aData[1] = m_aData[2] = m_aData[3] = NULL;
	m_aTextures[0] = m_aTextures[1] = m_aTextures[2] = m_aTextures[3] = NULL;

	InitMemory();

	Observe(m_pProperties);

	// create pass-through shader
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();
	std::string strPassThrough =
		rShaderReg.RetrieveShader( "VflGpuParticleData_PassThrough_frag.glsl" );

	VistaGLSLShader* pProgram = new VistaGLSLShader();	
	if(	   strPassThrough.empty()
		|| !pProgram->InitShaderFromString( GL_FRAGMENT_SHADER, strPassThrough )
		|| !pProgram->Link() )
	{
		cout << " [VflGpuParticleData] - ERROR - unable to initialize pass-through shader..." << endl;
		delete pProgram;
		return;
	}
	m_pPassThroughShader = pProgram;
	
	// Set uniforms
	m_pPassThroughShader->Bind();
	m_pPassThroughShader->SetUniform( "samp_texture", 0 );
	m_pPassThroughShader->Release();
}

VflGpuParticleData::~VflGpuParticleData()
{
	ReleaseObserveable(m_pProperties);

	for (int i=0; i<4; ++i)
	{
		delete [] m_aData[i];
		m_aData[i] = NULL;

		delete m_aTextures[i];
		m_aTextures[i] = NULL;
	}

	delete m_pPassThroughShader;
	m_pPassThroughShader = NULL;

	delete m_pFBO;
	m_pFBO = NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	// well, obviously some parameters have changed...
	int w, h;
	m_pProperties->GetParticleTextureSize(w, h);
	if (w != m_iWidth || h != m_iHeight)
	{
		m_iWidth = w;
		m_iHeight = h;
		InitMemory();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Swap                                                        */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::Swap()
{
	m_iCurrentData = 1-m_iCurrentData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Reset                                                       */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::Reset()
{
	m_iActiveParticles = 0;
	m_iNextParticle = 0;
	m_iActiveTracers = 0;
	m_iNextTracer = 0;

	int iCount = m_iWidth*m_iHeight*4;
	for (int i=0; i<4; ++i)
		memset(m_aData[i], 0, iCount*sizeof(float));

	SignalCpuDataChange();
	SynchToGpu(true);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw                                                        */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::Draw()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable(GL_LIGHTING);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
	glColor4f(1, 1, 1, 1);

	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 1, 0, 1);

	float fSize = 0.1f;
	float fSpacing = 0.02f;
	float s, t;
	int iOffset=0;

	m_pPassThroughShader->Bind();

	for (int i=0; i<4; ++i)
	{
		s = m_aTextures[i]->GetMaxS();
		t = m_aTextures[i]->GetMaxT();

		m_aTextures[i]->Bind();

#if 1
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f((i+iOffset)*fSize + (i+iOffset)*fSpacing, 1.0f-fSize);
		glTexCoord2f(s, 0);
		glVertex2f(((i+iOffset)+1)*fSize + (i+iOffset)*fSpacing, 1.0f-fSize);
		glTexCoord2f(s, t);
		glVertex2f(((i+iOffset)+1)*fSize + (i+iOffset)*fSpacing, 1.0f);
		glTexCoord2f(0, t);
		glVertex2f((i+iOffset)*fSize + (i+iOffset)*fSpacing, 1.0f);
		glEnd();
#else
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(i*fSize + i*fSpacing, 1.0f-fSize);
		glTexCoord2f(s, 0);
		glVertex2f((i+1)*fSize + i*fSpacing, 1.0f-fSize);
		glTexCoord2f(s, t);
		glVertex2f((i+1)*fSize + i*fSpacing, 1.0f);
		glTexCoord2f(0, t);
		glVertex2f(i*fSize + i*fSpacing, 1.0f);
		glEnd();
#endif

	}

	m_pPassThroughShader->Release();

	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCurrentData                                              */
/*                                                                            */
/*============================================================================*/
float *VflGpuParticleData::GetCurrentData() const
{
	return m_aData[m_iCurrentData];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetStaleData                                                */
/*                                                                            */
/*============================================================================*/
float *VflGpuParticleData::GetStaleData() const
{
	return m_aData[1-m_iCurrentData];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCurrentAttributes                                        */
/*                                                                            */
/*============================================================================*/
float *VflGpuParticleData::GetCurrentAttributes() const
{
	return m_aData[2+m_iCurrentData];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetStaleAttributes                                          */
/*                                                                            */
/*============================================================================*/
float *VflGpuParticleData::GetStaleAttributes() const
{
	return m_aData[3-m_iCurrentData];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCurrentDataIndex                                         */
/*                                                                            */
/*============================================================================*/
int VflGpuParticleData::GetCurrentDataIndex() const
{
	return m_iCurrentData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCurrentTexture                                           */
/*                                                                            */
/*============================================================================*/
VistaTexture *VflGpuParticleData::GetCurrentTexture() const
{
	return m_aTextures[m_iCurrentData];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetStaleTexture                                             */
/*                                                                            */
/*============================================================================*/
VistaTexture *VflGpuParticleData::GetStaleTexture() const
{
	return m_aTextures[1-m_iCurrentData];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCurrentAttributeTexture                                  */
/*                                                                            */
/*============================================================================*/
VistaTexture *VflGpuParticleData::GetCurrentAttributeTexture() const
{
	return m_aTextures[2+m_iCurrentData];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetStaleAttributeTexture                                    */
/*                                                                            */
/*============================================================================*/
VistaTexture *VflGpuParticleData::GetStaleAttributeTexture() const
{
	return m_aTextures[3-m_iCurrentData];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFramebufferObj                                           */
/*                                                                            */
/*============================================================================*/
VistaFramebufferObj *VflGpuParticleData::GetFramebufferObj() const
{
	return m_pFBO;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Synch                                                       */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::Synch()
{
	if (m_dDataTimeStamp < m_dTextureTimeStamp)
		SynchToCpu();
	else if (m_dTextureTimeStamp < m_dDataTimeStamp)
		SynchToGpu();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SynchToGpu                                                  */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::SynchToGpu(bool bForce)
{
	if (!bForce && m_dTextureTimeStamp >= m_dDataTimeStamp)
		return;

	for (int i=0; i<4; ++i)
	{
		// determine data format
		GLenum eFormat = GL_RGBA;

		// send texture data to the graphics subsystem
		m_aTextures[i]->Bind();
		glTexSubImage2D(m_aTextures[i]->GetTarget(), 0, 0, 0, 
			m_iWidth, m_iHeight, eFormat, GL_FLOAT, m_aData[i]);
	}

	m_dTextureTimeStamp = m_dDataTimeStamp;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SynchToCpu                                                  */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::SynchToCpu(bool bForce)
{
	if (!bForce && m_dDataTimeStamp >= m_dTextureTimeStamp)
		return;

	for (int i=0; i<4; ++i)
	{
		// determine format
		GLenum eFormat = GL_RGBA;

		// retrieve data
		m_aTextures[i]->Bind();
		glGetTexImage(m_aTextures[i]->GetTarget(), 0, 
			eFormat, GL_FLOAT, m_aData[i]);
	}

	m_dDataTimeStamp = m_dTextureTimeStamp;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SignalCpuDataChange                                         */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::SignalCpuDataChange()
{
	m_dDataTimeStamp = GetVistaSystem()->GetFrameClock();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SignalGpuDataChange                                         */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::SignalGpuDataChange()
{
	m_dTextureTimeStamp = GetVistaSystem()->GetFrameClock();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get[Cpu|Gpu]DataChangeTimeStep                              */
/*                                                                            */
/*============================================================================*/
double VflGpuParticleData::GetCpuDataChangeTimeStamp() const
{
	return m_dDataTimeStamp;
}

double VflGpuParticleData::GetGpuDataChangeTimeStamp() const
{
	return m_dTextureTimeStamp;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetSize                                                 */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::SetSize(int iWidth, int iHeight)
{
	if (iWidth != m_iWidth || iHeight != m_iHeight)
	{
		m_iWidth = iWidth;
		m_iHeight = iHeight;
		InitMemory();
	}
}

void VflGpuParticleData::GetSize(int &iWidth, int &iHeight) const
{
	iWidth = m_iWidth;
	iHeight = m_iHeight;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetParticleCount                                            */
/*                                                                            */
/*============================================================================*/
int VflGpuParticleData::GetParticleCount() const
{
	return m_iWidth * m_iHeight;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetActiveParticles                                      */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::SetActiveParticles(int iCount)
{
	if (m_iActiveParticles <= m_iWidth*m_iHeight)
	{
		m_iActiveParticles = iCount;

		// try to maintain a relatively sensible value...
		m_iActiveTracers =  (iCount > m_iWidth ? m_iWidth : iCount);
	}
}

int VflGpuParticleData::GetActiveParticles() const
{
	return m_iActiveParticles;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNextParticle                                         */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::SetNextParticle(int iNext)
{
	if (0<=iNext && iNext<m_iWidth*m_iHeight)
		m_iNextParticle = iNext;

}

int VflGpuParticleData::GetNextParticle() const
{
	return m_iNextParticle;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetActiveTracers                                        */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::SetActiveTracers(int iCount)
{
	if (m_iActiveTracers <= m_iWidth)
	{
		m_iActiveTracers = iCount;

		// try to maintain a relatively sensible value...
		m_iActiveParticles = iCount * m_iHeight;
	}
}

int VflGpuParticleData::GetActiveTracers() const
{
	return m_iActiveTracers;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNextTracer                                           */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::SetNextTracer(int iNext)
{
	if (0<=iNext && iNext<m_iWidth)
		m_iNextTracer = iNext;
}

int VflGpuParticleData::GetNextTracer() const
{
	return m_iNextTracer;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetCurrentLine                                          */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::SetCurrentLine(int iCurrent)
{
	if (0<=iCurrent && iCurrent<m_iHeight)
		m_iCurrentLine = iCurrent;
}

int VflGpuParticleData::GetCurrentLine() const
{
	return m_iCurrentLine;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   InitMemory                                                  */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleData::InitMemory()
{
	// initialize memory for (double-buffered) particle positions
	// and (single-buffered) particle attributes
	for (int i=0; i<4; ++i)
	{
		// determine memory requirements and texture formats
		int iCount = m_iWidth*m_iHeight*4;
		GLint iInternalFormat = GL_RGBA32F_ARB;
		GLenum eFormat = GL_RGBA;

		// delete old memory
		delete m_aData[i];
		m_aData[i] = NULL;

		// allocate new one
		m_aData[i] = new float[iCount];
		memset(m_aData[i], 0, iCount*sizeof(float));

		// We don't have to destroy the old texture, as we can just 
		// replace its data. The driver will handle any necessary
		// reallocation (hopefully ;-)...
		if (!m_aTextures[i])
		{
			m_aTextures[i] = new VistaTexture(GL_TEXTURE_2D);
			m_aTextures[i]->Bind();
			glTexParameteri(m_aTextures[i]->GetTarget(), GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(m_aTextures[i]->GetTarget(), GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(m_aTextures[i]->GetTarget(), GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(m_aTextures[i]->GetTarget(), GL_TEXTURE_WRAP_T, GL_REPEAT);
//			glTexParameteri(m_aTextures[i]->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP);
//			glTexParameteri(m_aTextures[i]->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP);
		}
		else
		{
			m_aTextures[i]->Bind();
		}

		// send texture data to the graphics subsystem
		glTexImage2D(m_aTextures[i]->GetTarget(), 0, iInternalFormat, 
			m_iWidth, m_iHeight, 0, eFormat, GL_FLOAT, m_aData[i]);
	}

	if (!m_pFBO)
	{
		m_pFBO = new VistaFramebufferObj;
	}

	m_pFBO->Bind();
	for (int i=0; i<4; ++i)
		m_pFBO->Attach(m_aTextures[i], GL_COLOR_ATTACHMENT0_EXT+i);
	m_pFBO->Release();
	if (!m_pFBO->IsValid())
	{
		cout << " ***" << endl;
		cout << " [VflGpuParticleData] - ERROR - invalid FBO..." << endl;
		cout << "                                FBO status: " << m_pFBO->GetStatusAsString() << endl;
		cout << " *** prepare for emergency landing (i.e. a crash?)!" << endl;
		cout << " ***" << endl;
		return;
	}

	Reset();
}

/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/
static void DumpTexture(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						ostream &out)
{
	if (!pTexture)
		return;

	float *pData = new float[iWidth*iHeight*iTuples];
	pTexture->Bind();
	glGetTexImage(pTexture->GetTarget(), 0, GL_RGBA, GL_FLOAT, pData);
	float *pPos = pData;

	for (int i=0; i<iHeight; ++i)
	{
		for (int j=0; j<iWidth; ++j)
		{
			if (j>0)
				out << "| ";
			for (int k=0; k<iTuples; ++k)
			{
				out << (*(pPos++)) << " ";
			}
		}
		out << endl;
	}

	delete [] pData;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
