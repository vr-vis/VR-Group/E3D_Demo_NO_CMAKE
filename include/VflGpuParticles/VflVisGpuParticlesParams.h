/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflVisGpuParticlesParams.h    RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflVisGpuParticlesParams										  */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLVISGPUPARTICLESPARAMS_H
#define __VFLVISGPUPARTICLESPARAMS_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflGpuParticlesConfig.h"
#include <VistaAspects/VistaReflectionable.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Data/VflObserver.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflSharedVtkLookupTable;
class VflSharedLookupTexture;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI VflVisGpuParticlesParams : public IVistaReflectionable
{
public:
	VflVisGpuParticlesParams(VflSharedVtkLookupTable *pLut = NULL);
	virtual ~VflVisGpuParticlesParams();

	VflSharedVtkLookupTable	*GetLookupTable() const;
	VflSharedLookupTexture		*GetLookupTexture() const;

	// particle parameters ------------------------------------
	bool SetParticleTextureSize(int iWidth, int iHeight);
	bool SetParticleTextureSize(const std::list<int> &liSize);
	bool GetParticleTextureSize(int &iWidth, int &iHeight) const;
	int GetParticleTextureSize() const;
	std::list<int> GetParticleTextureSizeAsList() const;

	// texture upload behavior --------------------------------
	bool SetLookAheadCount(int iCount);
	int GetLookAheadCount() const;

	bool SetWrapAround(bool bWrap);
	bool GetWrapAround() const;

	bool SetStreamingUpdate(bool bStream);
	bool GetStreamingUpdate() const;

	// integration parameters ---------------------------------
	bool SetActive(bool bActive);
	bool GetActive() const;

	enum INTEGRATOR
	{
		IR_INVALID = -1,
		IR_EULER = 0,
		IR_RK3,
		IR_RK4,
		IR_LAST
	};

	bool SetIntegrator(int iIntegrator);
	bool SetIntegrator(const std::string &strIntegrator);
	int GetIntegrator() const;
	std::string GetIntegratorAsString() const;

	bool SetDeltaTime(float fTime);
	float GetDeltaTime() const;

	bool SetMaxDeltaTime(float fTime);
	float GetMaxDeltaTime() const;

	bool SetUseFixedDeltaTime(bool bFixed);
	bool GetUseFixedDeltaTime() const;

	bool SetFixedDeltaTime(float fTime);
	float GetFixedDeltaTime() const;

	bool SetAdaptiveTimeSteps(bool bAdaptive);
	bool GetAdaptiveTimeSteps() const;

	bool SetComputeScalars(bool bScalars);
	bool GetComputeScalars() const;

	bool SetSingleBuffered(bool bSingle);
	bool GetSingleBuffered() const;

	bool SetForceHardwareInterpolation(bool bForce);
	bool GetForceHardwareInterpolation() const;

	bool SetBenchmarkMode(bool bBench);
	bool GetBenchmarkMode() const;

	// rendering parameters -----------------------------------
	bool SetVisible(bool bVisible);
	bool GetVisible() const;

	bool SetColor(const VistaVector3D &v3Color);
	VistaVector3D GetColor() const;

	bool SetUseLookupTable(bool bUseLut);
	bool GetUseLookupTable() const;

	bool SetRadius(float fRadius);
	float GetRadius() const;

	bool SetPointSize(float fPointSize);
	float GetPointSize() const;

	bool SetLineWidth(float fLineWidth);
	float GetLineWidth() const;

	bool SetDecreaseLuminance(bool bDecrease);
	bool GetDecreaseLuminance() const;

	bool SetDrawOverlay(bool bOverlay);
	bool GetDrawOverlay() const;

	bool SetAllowSorting(bool bSort);
	bool GetAllowSorting() const;

	bool SetMaxSortingSteps(int iMax);
	int GetMaxSortingSteps() const;

	bool SetHighPrecisionSorting(bool bSort);
	bool GetHighPrecisionSorting() const;

	bool SetForceDataPreparation(bool bForce);
	bool GetForceDataPreparation() const;

	// allow for mix'n'match?? --------------------------------
	bool SetRespectProcessingUnit(bool bRPU);
	bool GetRespectProcessingUnit() const;

	bool SetRespectTracerType(bool bRTT);
	bool GetRespectTracerType() const;

	virtual void Debug(std::ostream &out) const;

	virtual std::string GetReflectionableType() const;

protected:
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	VflSharedVtkLookupTable	*m_pLookupTable;
	VflSharedLookupTexture		*m_pLookupTexture;
	bool						m_bManageLut;
	bool						m_bManageTexture;

	int							m_iWidth, m_iHeight;
	int							m_iLookAheadCount;
	bool						m_bWrapAround;
	bool						m_bStreamingUpdate;
	bool						m_bActive;
	int							m_iIntegrator;
	float						m_fDeltaTime;
	float						m_fMaxDeltaTime;
	bool						m_bUseFixedDeltaTime;
	float						m_fFixedDeltaTime;
	bool						m_bAdaptiveTimeSteps;
	bool						m_bComputeScalars;
	bool						m_bSingleBuffered;
	bool						m_bForceHardwareInterpolation;
	bool						m_bBenchmarkMode;
	bool						m_bVisible;
	VistaVector3D				m_v3Color;
	bool						m_bUseLookupTable;
	float						m_fRadius;
	float						m_fPointSize;
	float						m_fLineWidth;
	bool						m_bDecreaseLuminance;
	bool						m_bDrawOverlay;
	bool						m_bAllowSorting;
	int							m_iMaxSortingSteps;
	bool						m_bHighPrecisionSorting;
	bool						m_bForceDataPreparation;
	bool						m_bRespectProcessingUnit;
	bool						m_bRespectTracerType;
};


#endif // __VFLVISGPUPARTICLESPARAMS_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

