/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuTracersTetGrid.cpp     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflGpuTracersTetGrid.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include <VistaFlowLib/Data/VflUnsteadyTetGridPusher.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaVisExt/Data/VveTetGrid.h>
#include <VistaVisExt/Data/VveUnsteadyTetGrid.h>
#include <VistaVisExt/Algorithms/VveTetGridPointLocator.h>
#include <VistaAspects/VistaAspectsUtils.h>

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaFramebufferObj.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

#include <set>
#include <string>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflGpuTracersTetGridReflType("VflGpuTracersTetGrid");

static IVistaPropertyGetFunctor *s_aCVflGpuTracersTetGridGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflGpuTracersTetGridSetFunctors[] =
{
	NULL
};

#define SHADER_STEADY_EULER					"VflGpuParticlesTet_Steady_Euler.glsl"
#define SHADER_STEADY_EULER_SCALARS			"VflGpuParticlesTet_Steady_EulerScalars.glsl"
#define SHADER_STEADY_RK3					"VflGpuParticlesTet_Steady_RK3.glsl"
#define SHADER_STEADY_RK3_SCALARS			"VflGpuParticlesTet_Steady_RK3Scalars.glsl"
#define SHADER_STEADY_RK4					"VflGpuParticlesTet_Steady_RK4.glsl"
#define SHADER_STEADY_RK4_SCALARS			"VflGpuParticlesTet_Steady_RK4Scalars.glsl"
#define SHADER_STEADY_ADAPTIVE_EULER		"VflGpuParticlesTet_SteadyAdaptive_Euler.glsl"
#define SHADER_STEADY_ADAPTIVE_EULER_SCALARS	"VflGpuParticlesTet_SteadyAdaptive_EulerScalars.glsl"
#define SHADER_STEADY_ADAPTIVE_RK3			"VflGpuParticlesTet_SteadyAdaptive_RK3.glsl"
#define SHADER_STEADY_ADAPTIVE_RK3_SCALARS	"VflGpuParticlesTet_SteadyAdaptive_RK3Scalars.glsl"
#define SHADER_STEADY_ADAPTIVE_RK4			"VflGpuParticlesTet_SteadyAdaptive_RK4.glsl"
#define SHADER_STEADY_ADAPTIVE_RK4_SCALARS	"VflGpuParticlesTet_SteadyAdaptive_RK4Scalars.glsl"
//#define SHADER_UNSTEADY_EULER			"VflGpuParticlesCart_Unsteady_Euler.glsl"
//#define SHADER_UNSTEADY_EULER_SCALARS	"VflGpuParticlesCart_Unsteady_EulerScalars.glsl"
//#define SHADER_UNSTEADY_RK3				"VflGpuParticlesCart_Unsteady_RK3.glsl"
//#define SHADER_UNSTEADY_RK3_SCALARS		"VflGpuParticlesCart_Unsteady_RK3Scalars.glsl"
//#define SHADER_UNSTEADY_RK4				"VflGpuParticlesCart_Unsteady_RK4.glsl"
//#define SHADER_UNSTEADY_RK4_SCALARS		"VflGpuParticlesCart_Unsteady_RK4Scalars.glsl"

static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveTetGrid *pGrid);

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveTetGrid *pGrid);

static void DumpTexture(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						std::ostream &out);

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflGpuTracersTetGrid::VflGpuTracersTetGrid(VflVisGpuParticles *pParent)
: IVflGpuParticleTracer(pParent),
  m_pWriteMultipleTC(NULL),
  m_pPassThroughShader(NULL),
  m_bValid(false)
{
	m_bValid = CreateGpuResources();
}

VflGpuTracersTetGrid::~VflGpuTracersTetGrid()
{
	DestroyGpuResources();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersTetGrid::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();

	if (!pProperties->GetActive())
		return;

	double dVisTime = m_pParent->GetVisTime();
	VveTimeMapper *pTimeMapper = m_pParent->GetData()->GetTimeMapper();

	// retrieve integration parameters
	int iIntegrator = pProperties->GetIntegrator();
	bool bComputeScalars = pProperties->GetComputeScalars();

	// determine dataset boundaries
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pTimeMapper->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return;
	}
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	static bool bDump = false;
	if (bDump)
	{
		std::cout << "before ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		int w, h;
		m_pParent->GetParticleData()->GetSize(w, h);
		DumpTexture(m_pParent->GetParticleData()->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(m_pParent->GetParticleData()->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		std::cout << std::endl;
		bDump = true;
	}

	// get access to flow data
	VflUnsteadyTexturePusher *pPusher = m_pParent->GetPusher();
	VflUnsteadyTetGridPusher *pGridPusher = dynamic_cast<VflUnsteadyTetGridPusher *>(pPusher);
	if (!pGridPusher)
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to retrieve texture pusher..." << std::endl;
		return;
	}

	m_pParent->SaveOGLState();

	// retrieve previous particle population
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	VistaTexture *pOldPopulation = pParticles->GetCurrentTexture();
	pOldPopulation->Bind(GL_TEXTURE0);
	VistaTexture *pOldAttributes = pParticles->GetCurrentAttributeTexture();
	pOldAttributes->Bind(GL_TEXTURE1);

	// bind render target
	pParticles->GetFramebufferObj()->Bind();

	if (pProperties->GetSingleBuffered())
	{
		GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex(),
			GL_COLOR_ATTACHMENT2_EXT + pParticles->GetCurrentDataIndex()};
		glDrawBuffers(2, db);
	}
	else
	{
		GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + 1-pParticles->GetCurrentDataIndex(),
			GL_COLOR_ATTACHMENT2_EXT + 1-pParticles->GetCurrentDataIndex()};
		glDrawBuffers(2, db);
	}

	int w, h;
	pParticles->GetSize(w, h);

	VistaGLSLShader *pShader = NULL;
	float fDeltaT = 0;

	// determine and bind shader
	if (m_pParent->GetRenderNode()->GetVisTiming()->GetAnimationPlaying())
	{
		//// determine time step length
		//float fDeltaTVisTime = 0;
		//if (m_pParent->GetTimeWarning())
		//{
		//	fDeltaT = pProperties->GetMaxDeltaTime();

		//	fDeltaTVisTime = pTimeMapper->GetVisualizationTime(pTimeMapper->GetSimulationTime(0)+fDeltaT);
		//}
		//else
		//{
		//	float fLastVisTime = m_pParent->GetLastVisTime();
		//	fDeltaTVisTime = fVisTime - fLastVisTime;
		//	while (fDeltaTVisTime < 0)
		//		fDeltaTVisTime += 1.0f;

		//	fDeltaT = pTimeMapper->GetSimulationTime(fDeltaTVisTime)
		//		- pTimeMapper->GetSimulationTime(0);
		//}

		//if (bComputeScalars)
		//{
		//	pShader = m_vecShadersUnsteadyScalars[iIntegrator];
		//}
		//else
		//{
		//	pShader = m_vecShadersUnsteady[iIntegrator];
		//}

		//if (pShader)
		//{
		//	pShader->Bind();

		//	// set texture information
		//	vector<VistaTexture *> vecTextures;
		//	vector<float> vecOffsets, vecWeights;
		//	switch (iIntegrator)
		//	{
		//	case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER:
		//		vecOffsets.push_back(0);
		//		if (pGridPusher->GetTexturesAndWeights(vecOffsets, vecTextures, vecWeights) == 1)
		//		{
		//			vecTextures[0]->Bind(GL_TEXTURE1);
		//			vecTextures[1]->Bind(GL_TEXTURE2);

		//			float aWeights[2];
		//			aWeights[0] = 1.0f - vecWeights[0];
		//			aWeights[1] = vecWeights[0];

		//			pShader->SetUniform(pShader->GetUniformLocation("aWeights"), 1, 2, aWeights);
		//		}
		//		break;
		//	case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3:
		//	case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4:
		//		vecOffsets.push_back(0);
		//		vecOffsets.push_back(0.5f * fDeltaTVisTime);
		//		vecOffsets.push_back(fDeltaTVisTime);
		//		if (pGridPusher->GetTexturesAndWeights(vecOffsets, vecTextures, vecWeights) == 3)
		//		{
		//			GLenum iTextureUnit = GL_TEXTURE1;
		//			float aWeights[6];
		//			for (int i=0; i<3; ++i)
		//			{
		//				vecTextures[2*i]->Bind(iTextureUnit++);
		//				vecTextures[2*i+1]->Bind(iTextureUnit++);
		//				aWeights[2*i] = 1.0f - vecWeights[i];
		//				aWeights[2*i+1] = vecWeights[i];
		//			}
		//			pShader->SetUniform(pShader->GetUniformLocation("aWeights"), 1, 6, aWeights);
		//		}
		//		break;
		//	};
		//}
	}
	else
	{
		// determine time step length
		fDeltaT = pProperties->GetDeltaTime();

		if (pProperties->GetAdaptiveTimeSteps())
			iIntegrator += VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST;

		if (bComputeScalars)
		{
			pShader = m_vecShadersSteadyScalars[iIntegrator];
		}
		else
		{
			pShader = m_vecShadersSteady[iIntegrator];
		}

		if (pShader)
		{
			pShader->Bind();

			// bind flow data
			pGridPusher->GetVertexTexture()->Bind(GL_TEXTURE2);
			pGridPusher->GetPointDataTexture()->Bind(GL_TEXTURE3);
			pGridPusher->GetCellTexture()->Bind(GL_TEXTURE4);
			pGridPusher->GetCellNeighborTexture()->Bind(GL_TEXTURE5);

			// but stay on texture unit 0
			glActiveTexture(GL_TEXTURE0);

			// determine texture widths and send it to the shader
			pShader->SetUniform(pShader->GetUniformLocation("v2TexWidth"), 
				pGridPusher->GetVertexTexture()->GetMaxS(),
				pGridPusher->GetCellTexture()->GetMaxS());
		}
	}

	if (pShader)
	{
		// specify time step length
		pShader->SetUniform(pShader->GetUniformLocation("fDeltaT"), fDeltaT);

		// viewport and projection setup
		glViewport(0, 0, w, h);
		glLoadIdentity();
		glOrtho(0, 1, 0, 1, -1, 1);

		// find line to insert new particle data
		int iSourceLine = pParticles->GetCurrentLine();
		int iTargetLine = (iSourceLine + 1) % h;

		float fSource = (iSourceLine + 0.5f) / h;
		float fTarget = (iTargetLine + 0.5f) / h;

		// [picard] Energize! [\picard]
		glBegin(GL_LINES);
		glTexCoord2f(0, fSource);
		glVertex2f(0, fTarget);
		glTexCoord2f(1, fSource);
		glVertex2f(1, fTarget);
		glEnd();

		pShader->Release();

		// if we're double buffering, we still have to copy the old positions
		if (!pProperties->GetSingleBuffered())
		{
			m_pPassThroughShader->Bind();
			glBegin(GL_LINES);
			glTexCoord2f(0, fSource);
			glVertex2f(0, fSource);
			glTexCoord2f(1, fSource);
			glVertex2f(1, fSource);
			glEnd();
			m_pPassThroughShader->Release();
		}

		// return to fixed-function pipeline
		pShader->Release();

		// advance current line
		pParticles->SetCurrentLine(iTargetLine);
	}
	pParticles->GetFramebufferObj()->Release();

	m_pParent->RestoreOGLState();

	if (!pProperties->GetSingleBuffered())
		m_pParent->GetParticleData()->Swap();

	m_pParent->GetParticleData()->SignalGpuDataChange();

	if (bDump)
	{
		std::cout << "after ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(pParticles->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		std::cout << std::endl;
	}

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticle                                                */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersTetGrid::SeedParticle(const VistaVector3D &v3Pos)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	float fCell = float(pGrid->GetPointLocator()->GetCellId(&v3Pos[0]));
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount, iLength;
	pParticles->GetSize(iCount, iLength);
	int iActive = pParticles->GetActiveTracers();
	int iNext = pParticles->GetNextTracer();

	m_pParent->SaveOGLState();

	static bool bDump = false;
	if (bDump)
	{
		std::cout << "before ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), iCount, iLength, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(pParticles->GetCurrentAttributeTexture(), iCount, iLength, 4, std::cout);
		std::cout << std::endl;
	}

	// set render target
	pParticles->GetFramebufferObj()->Bind();

	// activate shader for writing particle positions (given as TCs)
	m_pWriteMultipleTC->Bind();

	// set viewport and transformation matrices
	glViewport(0, 0, iCount, iLength);
	glLoadIdentity();
	glOrtho(0, iCount, 0, iLength, -1, 1);

#if 1
	// ok - who's next?
	float fPosX = (iNext % iCount) + 0.5f;

	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex(),
			GL_COLOR_ATTACHMENT2_EXT + pParticles->GetCurrentDataIndex()};
		glDrawBuffers(2, db);
		glBegin(GL_LINES);
		glMultiTexCoord4fv(GL_TEXTURE0, &v3Pos[0]);
		glMultiTexCoord1f(GL_TEXTURE1, fCell);
		glVertex2f(fPosX, 0);
		glVertex2f(fPosX, float(iLength+1));
		glEnd();
	}
	else
	{
		for (int i=0; i<2; ++i)
		{
			GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + i, GL_COLOR_ATTACHMENT2_EXT + i};
			glDrawBuffers(2, db);
			glBegin(GL_LINES);
			glMultiTexCoord4fv(GL_TEXTURE0, &v3Pos[0]);
			glMultiTexCoord1f(GL_TEXTURE1, fCell);
			glVertex2f(fPosX, 0);
			glVertex2f(fPosX, float(iLength+1));
			glEnd();
		}
	}

	m_pWriteMultipleTC->Release();
	pParticles->GetFramebufferObj()->Release();

	if (bDump)
	{
		std::cout << "after ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), iCount, iLength, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(pParticles->GetCurrentAttributeTexture(), iCount, iLength, 4, std::cout);
		std::cout << std::endl;
		bDump = false;
	}

	m_pParent->RestoreOGLState();

	pParticles->SetNextTracer((iNext+1)%iCount);
	if (iActive < iCount)
	{
		++iActive;
		pParticles->SetActiveTracers(iActive);
		if (iActive == iCount)
		{
			std::cout << " [VflGpuTracersTetGrid] - all tracers are active now..." << std::endl;
		}
	}
#else
	// ok - who's next?
	float fPosX = (iNext % w) + 0.5f;
	float fPosY = (iNext / w) + 0.5f;

	glBegin(GL_POINTS);
	glMultiTexCoord4fv(GL_TEXTURE0, &v3Pos[0]);
	glMultiTexCoord1f(GL_TEXTURE1, fCell);
	glVertex2f(fPosX, fPosY);
	glEnd();

	m_pWriteMultipleTC->Release();
	m_pFBO->Release();
	m_pParent->RestoreOGLState();

	pParticles->SetNextParticle((iNext+1)%iCount);
	if (iActive < iCount)
	{
		++iActive;
		pParticles->SetActiveParticles(iActive);
		if (iActive == iCount)
		{
			std::cout << " [VflGpuTracersTetGrid] - all particles are active now..." << std::endl;
		}
	}
#endif

	pParticles->SignalGpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticles                                               */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersTetGrid::SeedParticles(const VistaVector3D &v3Pos1, 
										  const VistaVector3D &v3Pos2, 
										  int iCount)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iTracerCount, iLength;
	pParticles->GetSize(iTracerCount, iLength);
	int iActive = pParticles->GetActiveTracers();
	int iNext = pParticles->GetNextTracer();

	if (iCount > iTracerCount)
		iCount = iTracerCount;

	if (iActive < iTracerCount)
	{
		iActive += iCount;
		if (iActive >= iTracerCount)
		{
			iActive = iTracerCount;
			std::cout << " [VflGpuTracersTetGrid] - all tracers are active now..." << std::endl;
		}

		pParticles->SetActiveTracers(iActive);
	}

	float fDelta = 1.0f;
	if (iCount > 1)
		fDelta /= (iCount-1);
	float fOffset = 0;
	VistaVector3D v3Delta(v3Pos2-v3Pos1);
	float fScalarDelta(v3Pos2[3]-v3Pos1[3]);

	m_pParent->SaveOGLState();

	// activate shader for writing particle positions (given as TCs)
	m_pWriteMultipleTC->Bind();

	// set viewport and transformation matrices
	glViewport(0, 0, iTracerCount, iLength);
	glLoadIdentity();
	glOrtho(0, iTracerCount, 0, iLength, -1, 1);

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();

#if 1
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		// set render target
		pParticles->GetFramebufferObj()->Bind();
		GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex(),
			GL_COLOR_ATTACHMENT2_EXT + pParticles->GetCurrentDataIndex()};
		glDrawBuffers(2, db);

		glBegin(GL_LINES);
		for (int i=0; i<iCount; ++i, fOffset+=fDelta)
		{
			VistaVector3D v3NewPos(v3Pos1 + fOffset*v3Delta);
			v3NewPos[3] = v3Pos1[3] + fOffset*fScalarDelta;

			float fPosX = (iNext % iTracerCount) + 0.5f;
			glMultiTexCoord4fv(GL_TEXTURE0, &v3NewPos[0]);
			glMultiTexCoord1f(GL_TEXTURE1, float(pGrid->GetPointLocator()->GetCellId(&v3NewPos[0])));
			glVertex2f(fPosX, 0);
			glVertex2f(fPosX, float(iLength+1));
			iNext = (iNext+1) % iTracerCount;
		}
		glEnd();
	}
	else
	{
		int iLocalNext(0);

		// find cells
		float *pCells = new float[iCount];
		for (int i=0; i<iCount; ++i, fOffset+=fDelta)
		{
			VistaVector3D v3NewPos(v3Pos1 + fOffset*v3Delta);
			pCells[i] = float(pGrid->GetPointLocator()->GetCellId(&v3NewPos[0]));
		}

		for (int j=0; j<2; ++j)
		{
			// set render target
			pParticles->GetFramebufferObj()->Bind();
			GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + j, GL_COLOR_ATTACHMENT2_EXT + j};
			glDrawBuffers(2, db);

			iLocalNext = iNext;
			fOffset = 0;

			glBegin(GL_LINES);
			for (int i=0; i<iCount; ++i, fOffset+=fDelta)
			{
				VistaVector3D v3NewPos(v3Pos1 + fOffset*v3Delta);
				v3NewPos[3] = v3Pos1[3] + fOffset*fScalarDelta;

				float fPosX = (iLocalNext % iTracerCount) + 0.5f;
				glMultiTexCoord4fv(GL_TEXTURE0, &v3NewPos[0]);
				glMultiTexCoord1f(GL_TEXTURE1, pCells[i]);
				glVertex2f(fPosX, 0);
				glVertex2f(fPosX, float(iLength+1));
				iLocalNext = (iLocalNext+1) % iTracerCount;
			}
			glEnd();
		}
		delete [] pCells;
		iNext = iLocalNext;
	}
#else
	for (int i=0; i<iCount; ++i, fOffset+=fDelta)
	{
		VistaVector3D v3NewPos(v3Pos1 + fOffset*v3Delta);
		v3NewPos[3] = v3Pos1[3] + fOffset*fScalarDelta;

		// ok - who's next?
		float fPosX = (iNext % w) + 0.5f;
		float fPosY = (iNext / w) + 0.5f;

		glBegin(GL_POINTS);
		glMultiTexCoord4fv(GL_TEXTURE0, &v3NewPos[0]);
		glMultiTexCoord1f(GL_TEXTURE1, float(pGrid->GetPointLocator()->GetCellId(&v3NewPos[0])));
		glVertex2f(fPosX, fPosY);
		glEnd();

		iNext = (iNext+1) % iParticleCount;
	}
#endif
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	m_pWriteMultipleTC->Release();
	pParticles->GetFramebufferObj()->Release();
	m_pParent->RestoreOGLState();

	pParticles->SetNextTracer(iNext);
	pParticles->SignalGpuDataChange();
}

void VflGpuTracersTetGrid::SeedParticles(const std::vector<float> &vecPositions)
{
	VistaVector3D v3Pos;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iTracerCount, iLength;
	pParticles->GetSize(iTracerCount, iLength);
	int iCount = int(vecPositions.size()) / 4;

	if (iCount > iTracerCount)
		iCount = iTracerCount;

	const float *pPositions = &vecPositions[0];
	for (int i=0; i<iCount; ++i)
	{
		memcpy(&v3Pos[0], &pPositions[4*i], 4*sizeof(float));
		SeedParticle(v3Pos);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticleNormalized                                      */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersTetGrid::SeedParticleNormalized(const VistaVector3D &v3Pos)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3PosDN;
	DeNormalize(v3PosDN, v3Pos, pGrid);
	SeedParticle(v3PosDN);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticlesNormalized                                     */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersTetGrid::SeedParticlesNormalized(const VistaVector3D &v3Pos1, 
													   const VistaVector3D &v3Pos2, 
													   int iCount)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3Pos1DN, v3Pos2DN;
	DeNormalize(v3Pos1DN, v3Pos1, pGrid);
	DeNormalize(v3Pos2DN, v3Pos2, pGrid);
	SeedParticles(v3Pos1DN, v3Pos2DN, iCount);
}

void VflGpuTracersTetGrid::SeedParticlesNormalized(const std::vector<float> &vecPositions)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	std::vector<float> vecDNPositions;
	int iCount = int(vecPositions.size()) / 4;
	vecDNPositions.resize(vecPositions.size());
	for (int i=0; i<iCount; ++i)
	{
		DeNormalize(&vecDNPositions[4*i], &vecPositions[4*i], pGrid);
	}

	SeedParticles(vecDNPositions);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGridType                                                 */
/*                                                                            */
/*============================================================================*/
int VflGpuTracersTetGrid::GetGridType() const
{
	return VflVisGpuParticles::GT_TET_GRID;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProcessingUnit                                           */
/*                                                                            */
/*============================================================================*/
int VflGpuTracersTetGrid::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_GPU;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerType                                               */
/*                                                                            */
/*============================================================================*/
int VflGpuTracersTetGrid::GetTracerType() const
{
	return VflVisGpuParticles::TT_TRACERS;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflGpuTracersTetGrid::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflGpuTracersTetGrid::GetReflectionableType() const
{
	return s_strCVflGpuTracersTetGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateGpuResources                                          */
/*                                                                            */
/*============================================================================*/
bool VflGpuTracersTetGrid::CreateGpuResources()
{
	// TODO: check for required OpenGL extensions...
#ifdef DEBUG
	std::cout << " [VflGpuTracersTetGrid] - creating GPU resources..." << std::endl;
#endif

	// init shaders -----------------------------------------------------------
#ifdef DEBUG
	std::cout << " [VflGpuTracersTetGrid] - creating shader for writing tex coords..." << std::endl;
#endif
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();
	std::string strShader = rShaderReg.RetrieveShader( "VflGpuTetGrid_DualWriteTC_frag.glsl" );

	VistaGLSLShader* pProgram = new VistaGLSLShader();

	if(    strShader.empty()
		|| !pProgram->InitShaderFromString( GL_FRAGMENT_SHADER, strShader )
		|| !pProgram->Link() )
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to initialize shader for writing texcoords..." << std::endl;
		delete pProgram;
		return false;
	}
	m_pWriteMultipleTC = pProgram;

#ifdef DEBUG
	std::cout << " [VflGpuTracersTetGrid] - creating pass-through shader..." << std::endl;
#endif
	strShader = rShaderReg.RetrieveShader( "VflGpuTetGrid_DualPassThrough_frag.glsl" );
	pProgram = new VistaGLSLShader();

	if(    strShader.empty()
		|| !pProgram->InitShaderFromString( GL_FRAGMENT_SHADER, strShader )
		|| !pProgram->Link() )
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to initialize pass-through shader..." << std::endl;
		delete pProgram;
		return false;
	}
	m_pPassThroughShader = pProgram;
	m_pPassThroughShader->Bind();
	m_pPassThroughShader->SetUniform( "samp_texture1", 0 );
	m_pPassThroughShader->SetUniform( "samp_texture2", 1 );
	m_pPassThroughShader->Release();

	m_vecShadersSteady.resize(2*VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersSteadyScalars.resize(2*VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
#ifdef DEBUG
	std::cout << " [VflGpuTracersTetGrid] - creating Euler integration shaders..." << std::endl;
#endif
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_EULER);
	VistaGLSLShader *pShader = new VistaGLSLShader;	
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - Euler integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_EULER_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - Euler integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_EULER);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - adaptive Euler integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init adaptive Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link adaptive Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_EULER_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - adaptive Euler integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init adaptive Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link adaptive Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

#ifdef DEBUG
	std::cout << " [VflGpuTracersTetGrid] - creating RK3 integration shaders..." << std::endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK3);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - RK3 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK3_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - RK3 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_RK3);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - adaptive RK3 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init adaptive RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link adaptive RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_RK3_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - adaptive RK3 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init adaptive RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link adaptive RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

#ifdef DEBUG
	std::cout << " [VflGpuTracersTetGrid] - creating RK4 integration shaders..." << std::endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK4);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - RK4 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK4_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - RK4 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_RK4);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - adaptive RK4 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init adaptive RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link adaptive RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_RK4_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - adaptive RK4 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to init adaptive RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersTetGrid] - ERROR - unable to link adaptive RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DestroyGpuResources                                         */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersTetGrid::DestroyGpuResources()
{
	delete m_pWriteMultipleTC;
	m_pWriteMultipleTC = NULL;

	delete m_pPassThroughShader;
	m_pPassThroughShader = NULL;

	std::set<VistaGLSLShader *> setShaders;

	for (unsigned int i=0; i<m_vecShadersSteady.size(); ++i)
	{
		setShaders.insert(m_vecShadersSteady[i]);
	}
	m_vecShadersSteady.clear();

	for (unsigned int i=0; i<m_vecShadersSteadyScalars.size(); ++i)
	{
		setShaders.insert(m_vecShadersSteadyScalars[i]);
	}
	m_vecShadersSteadyScalars.clear();

	std::set<VistaGLSLShader *>::iterator it;
	for (it=setShaders.begin(); it!=setShaders.end(); ++it)
	{
		delete *it;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflGpuTracersTetGrid::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleTracer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflGpuTracersTetGridReflType);
	return i+1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRendererId                                               */
/*                                                                            */
/*============================================================================*/
const std::string VflGpuTracersTetGrid::GetRendererId()
{
	return s_strCVflGpuTracersTetGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetShaderKeys                                               */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersTetGrid::GetShaderKeys(
	std::vector<std::string>& vecShaderKeys)
{
	vecShaderKeys.clear();
	vecShaderKeys.push_back(SHADER_STEADY_EULER);
	vecShaderKeys.push_back(SHADER_STEADY_EULER_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_RK3);
	vecShaderKeys.push_back(SHADER_STEADY_RK3_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_RK4);
	vecShaderKeys.push_back(SHADER_STEADY_RK4_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_EULER);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_EULER_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_RK3);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_RK3_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_RK4);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_RK4_SCALARS);
}

/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeNormalize                                                 */
/*                                                                            */
/*============================================================================*/
static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveTetGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	v3Out[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * v3In[0];
	v3Out[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * v3In[1];
	v3Out[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * v3In[2];
	v3Out[3] = v3In[3];
}

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveTetGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	aOut[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * aIn[0];
	aOut[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * aIn[1];
	aOut[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * aIn[2];
	aOut[3] = aIn[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DumpTexture                                                 */
/*                                                                            */
/*============================================================================*/
static void DumpTexture(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						std::ostream &out)
{
	if (!pTexture)
		return;

	float *pData = new float[iWidth*iHeight*iTuples];
	pTexture->Bind();
	glGetTexImage(pTexture->GetTarget(), 0, GL_RGBA, GL_FLOAT, pData);
	float *pPos = pData;

	for (int i=0; i<iHeight; ++i)
	{
		for (int j=0; j<iWidth; ++j)
		{
			if (j>0)
				out << "| ";
			for (int k=0; k<iTuples; ++k)
			{
				out << (*(pPos++)) << " ";
			}
		}
		out << std::endl;
	}

	delete [] pData;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
