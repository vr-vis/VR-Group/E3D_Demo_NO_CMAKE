/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflGpuParticleData.h          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflGpuParticleData												  */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLGPUPARTICLEDATA_H
#define __VFLGPUPARTICLEDATA_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "VflGpuParticlesConfig.h"
#include <VistaFlowLib/Data/VflObserver.h>
#include "VflVisGpuParticles.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;
class VistaGLSLShader;
class VistaFramebufferObj;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI VflGpuParticleData : public VflObserver
{
public:
	VflGpuParticleData(VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties);
	virtual ~VflGpuParticleData();

	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	void Swap();
	void Reset();

	/**
	 * Display particle and attribute data as overlay.
	 * Use this for debugging only...
	 */
	void Draw();

	float *GetCurrentData() const;
	float *GetStaleData() const;
	float *GetCurrentAttributes() const;
	float *GetStaleAttributes() const;
	int GetCurrentDataIndex() const;

	VistaTexture	*GetCurrentTexture() const;
	VistaTexture	*GetStaleTexture() const;
	VistaTexture	*GetCurrentAttributeTexture() const;
	VistaTexture	*GetStaleAttributeTexture() const;

	VistaFramebufferObj	*GetFramebufferObj() const;

	/**
	 * Synchronize main memory and GPU memory.
	 * The older copy gets overwritten.
	 * NOTE: This functionality is typically quite slow.
	 */
	void Synch();

	/**
	 * Make sure, that GPU memory contains the current version
	 * of all particle data, i.e. synchronize iff main memory contains
	 * the most recently modified particle data.
	 * NOTE: This functionality is potentially quite slow.
	 */
	void SynchToGpu(bool bForce = false);

	/**
	 * Make sure, that main memory contains the current version
	 * of all particle data, i.e. synchronize iff GPU memory contains
	 * the most recently modified particle data.
	 * NOTE: This functionality is potentially quite slow.
	 */
	void SynchToCpu(bool bForce = false);

	/** 
	 * Modify time stamp for data in main memory.
	 */
	void SignalCpuDataChange();

	/**
	 * Modify time stamp for GPU data.
	 */
	void SignalGpuDataChange();

	/**
	 * Retrieve time stamps for data changes.
	 */
	double GetGpuDataChangeTimeStamp() const;
	double GetCpuDataChangeTimeStamp() const;

	void SetSize(int iWidth, int iHeight);
	void GetSize(int &iWidth, int &iHeight) const;
	int GetParticleCount() const;

	void SetActiveParticles(int iCount);
	int GetActiveParticles() const;

	void SetNextParticle(int iNext);
	int GetNextParticle() const;

	void SetActiveTracers(int iCount);
	int GetActiveTracers() const;

	void SetNextTracer(int iNext);
	int GetNextTracer() const;

	void SetCurrentLine(int iCurrent);
	int GetCurrentLine() const;

protected:
	void InitMemory();

	VflVisGpuParticles::VflVisGpuParticlesProperties *m_pProperties;

	float			*m_aData[4];
	VistaTexture	*m_aTextures[4];
	VistaFramebufferObj	*m_pFBO;

	int				m_iCurrentData;

	double			m_dDataTimeStamp;
	double			m_dTextureTimeStamp;

	int				m_iWidth, m_iHeight;

	int				m_iActiveParticles;
	int				m_iNextParticle;

	int				m_iActiveTracers;
	int				m_iNextTracer;
	int				m_iCurrentLine;

	// for texture drawing
	VistaGLSLShader	*m_pPassThroughShader;
};

#endif // __VFLGPUPARTICLEDATA_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

