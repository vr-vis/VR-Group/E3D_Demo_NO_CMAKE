/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflCpuParticlesTetGrid.cpp   RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflCpuParticlesTetGrid.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaVisExt/Data/VveTetGrid.h>
#include <VistaVisExt/Data/VveUnsteadyTetGrid.h>
#include <VistaVisExt/Algorithms/VveTetGridPointLocator.h>
#include <VistaAspects/VistaAspectsUtils.h>

#include <set>
#include <string.h>

using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflCpuParticlesTetGridReflType("VflCpuParticlesTetGrid");

static IVistaPropertyGetFunctor *s_aCVflCpuParticlesTetGridGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflCpuParticlesTetGridSetFunctors[] =
{
	NULL
};

#define SCALAR_MULT(vec, scalar) \
	vec[0] *= scalar; \
	vec[1] *= scalar; \
	vec[2] *= scalar;
#define VEC_SMAD(dest, scalar, add1, add2) \
	dest[0] = scalar*add1[0] + add2[0]; \
	dest[1] = scalar*add1[1] + add2[1]; \
	dest[2] = scalar*add1[2] + add2[2];
#define VEC_SMULT(dest, scalar, vec) \
	dest[0] = scalar*vec[0]; \
	dest[1] = scalar*vec[1]; \
	dest[2] = scalar*vec[2];
#define VEC_ADD(dest, add1, add2) \
	dest[0] = add1[0] + add2[0]; \
	dest[1] = add1[1] + add2[1]; \
	dest[2] = add1[2] + add2[2];
#define VEC_CROSS(dest, v1, v2) \
	(dest)[0] = (v1)[1]*(v2)[2]-(v1)[2]*(v2)[1]; \
	(dest)[1] = (v2)[0]*(v1)[2]-(v1)[0]*(v2)[2]; \
	(dest)[2] = (v1)[0]*(v2)[1]-(v1)[1]*(v2)[0];
#define VEC_DOT(v1, v2) \
	(v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])
#define VEC_LENGTH(v1) \
	sqrt(VEC_DOT(v1, v1))
#define SATURATE(vec) \
	for (int _i=0; _i<3; ++_i) \
	{ \
		if (vec[_i] < 0) \
			vec[_i] = 0; \
		else if (vec[_i] > 1) \
			vec[_i] = 1; \
	}
#define SATURATE_LIMIT(vec, min, max) \
	for (int _i=0; _i<3; ++_i) \
	{ \
		if (vec[_i] < min[_i]) \
			vec[_i] = min[_i]; \
		else if (vec[_i] > max[_i]) \
			vec[_i] = max[_i]; \
	}

#define INTERPOLATE(dest, vec1, vec2, alpha) \
	{ \
		float _fOneMinusAlpha = 1.0f - alpha; \
		dest[0] = _fOneMinusAlpha * vec1[0] + alpha * vec2[0]; \
		dest[1] = _fOneMinusAlpha * vec1[1] + alpha * vec2[1]; \
		dest[2] = _fOneMinusAlpha * vec1[2] + alpha * vec2[2]; \
		dest[3] = _fOneMinusAlpha * vec1[3] + alpha * vec2[3]; \
	}

static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveTetGrid *pGrid);

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveTetGrid *pGrid);

static inline int RetrieveVelocity(VveTetGrid *pGrid, VveTetGridPointLocator *pLocator,
								   float aPos[3], int iStartCell, float aData[3]);
static inline int RetrieveVelocity(VveTetGrid *pGrid1, VveTetGrid *pGrid2, 
								   VveTetGridPointLocator *pLocator,
								   float aWeights[2],
								   float aPos[3], int iStartCell, float aData[3]);
static inline int RetrieveScalar(VveTetGrid *pGrid, VveTetGridPointLocator *pLocator,
								 float aPos[3], int iStartCell, float &fData);
static inline int RetrieveScalar(VveTetGrid *pGrid1, VveTetGrid *pGrid2, 
								 VveTetGridPointLocator *pLocator,
								 float aWeights[2],
								 float aPos[3], int iStartCell, float &fData);

static inline void ComputeRadii(float &fInsphere, float &fCircumsphere,
								int *pCell, float *pVertices);


#ifdef DEBUG
//#define COLLECT_STATISTICS
#endif

#ifdef COLLECT_STATISTICS
static vector<int> s_vecTetSteps;
//static vector<int> s_vecKdSteps;
//static vector<int> s_vecKdTetSteps;
static vector<int> s_vecIterations;
unsigned int s_iInvalidRatio;
unsigned int s_iCellChecks;
void StartStatisticsCollection();
void FinishStatisticsCollection();
#endif

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflCpuParticlesTetGrid::VflCpuParticlesTetGrid(VflVisGpuParticles *pParent)
: IVflGpuParticleTracer(pParent)
{
}

VflCpuParticlesTetGrid::~VflCpuParticlesTetGrid()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflCpuParticlesTetGrid::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();

	if (!pProperties->GetActive())
		return;

	double dVisTime = m_pParent->GetVisTime();
	double dLastVisTime = m_pParent->GetLastVisTime();

	// retrieve integration parameters
	int iIntegrator = pProperties->GetIntegrator();
	bool bComputeScalars = pProperties->GetComputeScalars();
	bool bSuccess = false;
	if (m_pParent->GetRenderNode()->GetVisTiming()->GetAnimationPlaying())
	{
		VveTimeMapper *pTimeMapper = m_pParent->GetData()->GetTimeMapper();
		double dDelta = dVisTime - dLastVisTime;
		while (dDelta < 0)
			dDelta += 1.0f;
		double dDeltaT = pTimeMapper->GetSimulationTime(dDelta)
			- pTimeMapper->GetSimulationTime(0);

		if (m_pParent->GetTimeWarning())
		{
			dDeltaT = pProperties->GetMaxDeltaTime();
		}

		if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER)
			bSuccess = IntegrateEulerUnsteady(dVisTime, dLastVisTime, dDeltaT, bComputeScalars);
		else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3)
			bSuccess = IntegrateRK3Unsteady(dVisTime, dLastVisTime, dDeltaT, bComputeScalars);
		else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4)
			bSuccess = IntegrateRK4Unsteady(dVisTime, dLastVisTime, dDeltaT, bComputeScalars);
	}
	else
	{
		double dDeltaT = pProperties->GetDeltaTime();
		if (pProperties->GetAdaptiveTimeSteps())
		{
			if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER)
				bSuccess = IntegrateEulerAdaptive(dVisTime, dDeltaT, bComputeScalars);
			else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3)
				bSuccess = IntegrateRK3Adaptive(dVisTime, dDeltaT, bComputeScalars);
			else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4)
				bSuccess = IntegrateRK4Adaptive(dVisTime, dDeltaT, bComputeScalars);
		}
		else
		{
			if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER)
				bSuccess = IntegrateEuler(dVisTime, dDeltaT, bComputeScalars);
			else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3)
				bSuccess = IntegrateRK3(dVisTime, dDeltaT, bComputeScalars);
			else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4)
				bSuccess = IntegrateRK4(dVisTime, dDeltaT, bComputeScalars);
		}
	}

	if (bSuccess)
	{
		if (!pProperties->GetSingleBuffered())
			m_pParent->GetParticleData()->Swap();

		m_pParent->GetParticleData()->SignalCpuDataChange();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticle                                                */
/*                                                                            */
/*============================================================================*/
void VflCpuParticlesTetGrid::SeedParticle(const VistaVector3D &v3Pos)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	float fCell = float(pGrid->GetPointLocator()->GetCellId(&v3Pos[0]));
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();

	float *pPos = &pParticles->GetCurrentData()[4*iNext];
	float *pAttrib = &pParticles->GetCurrentAttributes()[4*iNext];
	pParticles->SetNextParticle((iNext+1)%iCount);

	if (iActive < iCount)
	{
		++iActive;
		pParticles->SetActiveParticles(iActive);
		if (iActive == iCount)
		{
			cout << " [VflCpuParticlesTetGrid] - all particles are active now..." << endl;
		}
	}

	memcpy(pPos, &v3Pos[0], 4*sizeof(float));
	pAttrib[0] = fCell;
	pParticles->SignalCpuDataChange();

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticles                                               */
/*                                                                            */
/*============================================================================*/
void VflCpuParticlesTetGrid::SeedParticles(const VistaVector3D &v3Pos1,
											const VistaVector3D &v3Pos2, 
											int iCount)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iParticleCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();

	if (iCount > iParticleCount)
		iCount = iParticleCount;

	float fDelta = 1.0f;
	if (iCount > 1)
		fDelta /= (iCount-1);
	float fOffset = 0;
	VistaVector3D v3Delta(v3Pos2-v3Pos1);
	float fScalarDelta(v3Pos2[3]-v3Pos1[3]);

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();

	for (int i=0; i<iCount; ++i, fOffset+=fDelta)
	{
		float *pPos = &pParticles->GetCurrentData()[4*(iNext)];
		float *pAttrib = &pParticles->GetCurrentAttributes()[4*(iNext++)];
		iNext %= iParticleCount;

		if (iActive < iParticleCount)
		{
			++iActive;
			pParticles->SetActiveParticles(iActive);
			if (iActive == iParticleCount)
			{
				cout << " [VflCpuParticlesTetGrid] - all particles are active now..." << endl;
			}
		}

		VistaVector3D v3NewPos(v3Pos1 + fOffset*v3Delta);
		v3NewPos[3] = v3Pos1[3] + fOffset*fScalarDelta;
		memcpy(pPos, &v3NewPos[0], 4*sizeof(float));
		pAttrib[0] = float(pGrid->GetPointLocator()->GetCellId(&v3NewPos[0]));
	}
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	pParticles->SetNextParticle(iNext);
	pParticles->SignalCpuDataChange();
}

void VflCpuParticlesTetGrid::SeedParticles(const std::vector<float> &vecPositions)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iParticleCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();
	int iCount = int(vecPositions.size()) / 4;

	if (iCount > iParticleCount)
		iCount = iParticleCount;

	float *pData = pParticles->GetCurrentData();
	float *pAttribs = pParticles->GetCurrentAttributes();
	const float *pPositions = &vecPositions[0];

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();

	for (int i=0; i<iCount; ++i)
	{
		float *pPos = &pData[4*(iNext)];
		float *pAttrib = &pAttribs[4*(iNext++)];
		iNext %= iParticleCount;

		if (iActive < iParticleCount)
		{
			++iActive;
			pParticles->SetActiveParticles(iActive);
			if (iActive == iParticleCount)
			{
				cout << " [VflCpuParticlesTetGrid] - all particles are active now..." << endl;
			}
		}

		memcpy(pPos, &pPositions[4*i], 4*sizeof(float));
		pAttrib[0] = float(pGrid->GetPointLocator()->GetCellId(&pPositions[4*i]));
	}
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	pParticles->SetNextParticle(iNext);
	pParticles->SignalCpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticleNormalized                                      */
/*                                                                            */
/*============================================================================*/
void VflCpuParticlesTetGrid::SeedParticleNormalized(const VistaVector3D &v3Pos)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3PosDN;
	DeNormalize(v3PosDN, v3Pos, pGrid);
	SeedParticle(v3PosDN);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticlesNormalized                                     */
/*                                                                            */
/*============================================================================*/
void VflCpuParticlesTetGrid::SeedParticlesNormalized(const VistaVector3D &v3Pos1, 
													  const VistaVector3D &v3Pos2, 
													  int iCount)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3Pos1DN, v3Pos2DN;
	DeNormalize(v3Pos1DN, v3Pos1, pGrid);
	DeNormalize(v3Pos2DN, v3Pos2, pGrid);
	SeedParticles(v3Pos1DN, v3Pos2DN, iCount);
}

void VflCpuParticlesTetGrid::SeedParticlesNormalized(const std::vector<float> &vecPositions)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	vector<float> vecDNPositions;
	int iCount = int(vecPositions.size()) / 4;
	vecDNPositions.resize(vecPositions.size());
	for (int i=0; i<iCount; ++i)
	{
		DeNormalize(&vecDNPositions[4*i], &vecPositions[4*i], pGrid);
	}

	SeedParticles(vecDNPositions);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGridType                                                 */
/*                                                                            */
/*============================================================================*/
int VflCpuParticlesTetGrid::GetGridType() const
{
	return VflVisGpuParticles::GT_TET_GRID;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProcessingUnit                                           */
/*                                                                            */
/*============================================================================*/
int VflCpuParticlesTetGrid::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_CPU;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerType                                               */
/*                                                                            */
/*============================================================================*/
int VflCpuParticlesTetGrid::GetTracerType() const
{
	return VflVisGpuParticles::TT_PARTICLES;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesTetGrid::IsValid() const
{
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflCpuParticlesTetGrid::GetReflectionableType() const
{
	return s_strCVflCpuParticlesTetGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateEuler                                              */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesTetGrid::IntegrateEuler(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
#ifdef COLLECT_STATISTICS
	bool bCollectStatistics = false;
	if (bCollectStatistics)
		StartStatisticsCollection();
#endif

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	int iCell;
	float aVel[4];

	for (int i=0; i<iCount; ++i)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), aVel);

		VEC_SMAD(pNew, static_cast<float>(dDeltaT), aVel, pOld);
//		pNew[3] = (bComputeScalars ? aVel[3] : pOld[3];

		pNewAttrib[0] = float(iCell);
		pNew += 4;
		pOld += 4;
		pNewAttrib += 4;
		pOldAttrib += 4;
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

#ifdef COLLECT_STATISTICS
	if (bCollectStatistics)
		FinishStatisticsCollection();
#endif

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK3                                                */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesTetGrid::IntegrateRK3(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
#ifdef COLLECT_STATISTICS
	bool bCollectStatistics = false;
	if (bCollectStatistics)
		StartStatisticsCollection();
#endif

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	int iCell;
	float dx1[3], dx2[3], dx3[3], dxt[3];

	for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), dx1);
		if (iCell < 0)
		{
			pNew[0] = pOld[0];
			pNew[1] = pOld[1];
			pNew[2] = pOld[2];
			pNewAttrib[0] = -1.0f;
			continue;
		}

		SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

		VEC_SMAD(dxt, 0.5f, dx1, pOld);
		RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx2);
		SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

		VEC_SMAD(dxt, -1.0f, dx1, pOld);
		VEC_SMAD(dxt, 2.0f, dx2, dxt);
		RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx3);
		SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

		pNew[0] = pOld[0] + (dx1[0] + 2*dx2[0] + dx3[0]) / 6.0f;
		pNew[1] = pOld[1] + (dx1[1] + 2*dx2[1] + dx3[1]) / 6.0f;
		pNew[2] = pOld[2] + (dx1[2] + 2*dx2[2] + dx3[2]) / 6.0f;

		pNewAttrib[0] = float(iCell);
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

#ifdef COLLECT_STATISTICS
	if (bCollectStatistics)
		FinishStatisticsCollection();
#endif

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK4                                                */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesTetGrid::IntegrateRK4(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
#ifdef COLLECT_STATISTICS
	bool bCollectStatistics = false;
	if (bCollectStatistics)
		StartStatisticsCollection();
#endif

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	int iCell;
	float dx1[3], dx2[3], dx3[3], dx4[3], dxt[3];

	for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), dx1);
		if (iCell < 0)
		{
			pNew[0] = pOld[0];
			pNew[1] = pOld[1];
			pNew[2] = pOld[2];
			pNewAttrib[0] = -1.0f;
			continue;
		}

		SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

		VEC_SMAD(dxt, 0.5f, dx1, pOld);
		RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx2);
		SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

		VEC_SMAD(dxt, 0.5f, dx2, pOld);
		RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx3);
		SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

		VEC_SMAD(dxt, 1.0f, dx3, pOld);
		RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx4);
		SCALAR_MULT(dx4, static_cast<float>(dDeltaT));

		pNew[0] = pOld[0] + (dx1[0] + 2*dx2[0] + 2*dx3[0] + dx4[0]) / 6.0f;
		pNew[1] = pOld[1] + (dx1[1] + 2*dx2[1] + 2*dx3[1] + dx4[1]) / 6.0f;
		pNew[2] = pOld[2] + (dx1[2] + 2*dx2[2] + 2*dx3[2] + dx4[2]) / 6.0f;

		pNewAttrib[0] = float(iCell);
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

#ifdef COLLECT_STATISTICS
	if (bCollectStatistics)
		FinishStatisticsCollection();
#endif

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateEulerAdaptive                                      */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesTetGrid::IntegrateEulerAdaptive(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
#ifdef COLLECT_STATISTICS
	bool bCollectStatistics = false;
	if (bCollectStatistics)
		StartStatisticsCollection();
#endif

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	int iCell;
	float aVel[4];
	int *pCells = pGrid->GetCells();
	float *pVertices = pGrid->GetVertices();
	int *pCell;
	int iIterationCount;
	double dDeltaTLocal;
	float fVelMag;
	float fInsphere, fCircumsphere;

	for (int i=0; i<iCount; ++i)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), aVel);
		if (pNew!=pOld)
			memcpy(pNew, pOld, 3*sizeof(float));
		if (iCell >= 0)
		{
			pCell = &pCells[4*iCell];
			ComputeRadii(fInsphere, fCircumsphere, pCell, pVertices);
			dDeltaTLocal = dDeltaT;

			iIterationCount = 1;
			if (fInsphere/fCircumsphere > 1e-5)
			{
				fVelMag = sqrt(VEC_LENGTH(aVel)) * static_cast<float>(dDeltaT);
				iIterationCount = int(ceil(fVelMag/fInsphere));
				dDeltaTLocal /= iIterationCount;
			}
#ifdef COLLECT_STATISTICS
			else
			{
				++s_iInvalidRatio;
			}
			++s_iCellChecks;


			if (iIterationCount >= int(s_vecIterations.size()))
				s_vecIterations.resize(iIterationCount+1);
			++s_vecIterations[iIterationCount];
#endif

			while(true)
			{
				VEC_SMAD(pNew, static_cast<float>(dDeltaTLocal), aVel, pNew);

				if (--iIterationCount <= 0)
					break;

				iCell = RetrieveVelocity(pGrid, pLocator, pNew, iCell, aVel);
			}
		}

		pNewAttrib[0] = float(iCell);
		pNew += 4;
		pOld += 4;
		pNewAttrib += 4;
		pOldAttrib += 4;
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

#ifdef COLLECT_STATISTICS
	if (bCollectStatistics)
		FinishStatisticsCollection();
#endif

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK3Adaptive                                        */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesTetGrid::IntegrateRK3Adaptive(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
#ifdef COLLECT_STATISTICS
	bool bCollectStatistics = false;
	if (bCollectStatistics)
		StartStatisticsCollection();
#endif

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	int iCell;
	float dx1[3], dx2[3], dx3[3], dxt[3];
	int *pCells = pGrid->GetCells();
	float *pVertices = pGrid->GetVertices();
	int *pCell;
	int iIterationCount;
	double dDeltaTLocal;
	float fVelMag;
	float fInsphere, fCircumsphere;

	for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), dx1);
		if (pNew!=pOld)
			memcpy(pNew, pOld, 3*sizeof(float));
		if (iCell < 0)
		{
			pNewAttrib[0] = -1.0f;
			continue;
		}

		pCell = &pCells[4*iCell];
		ComputeRadii(fInsphere, fCircumsphere, pCell, pVertices);
		dDeltaTLocal = dDeltaT;

		iIterationCount = 1;
		if (fInsphere/fCircumsphere > 1e-5)
		{
			fVelMag = sqrt(VEC_LENGTH(dx1))*static_cast<float>(dDeltaT);
			iIterationCount = int(ceil(fVelMag/fInsphere));
			dDeltaTLocal /= iIterationCount;
		}
#ifdef COLLECT_STATISTICS
		else
		{
			++s_iInvalidRatio;
		}
		++s_iCellChecks;


		if (iIterationCount >= (int) s_vecIterations.size())
			s_vecIterations.resize(iIterationCount+1);
		++s_vecIterations[iIterationCount];
#endif

		while (true)
		{
			SCALAR_MULT(dx1, static_cast<float>(dDeltaTLocal));
			VEC_SMAD(dxt, 0.5f, dx1, pNew);
			RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx2);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaTLocal));

			VEC_SMAD(dxt, -1.0f, dx1, pNew);
			VEC_SMAD(dxt, 2.0f, dx2, dxt);
			RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx3);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaTLocal));

			pNew[0] += (dx1[0] + 2*dx2[0] + dx3[0]) / 6.0f;
			pNew[1] += (dx1[1] + 2*dx2[1] + dx3[1]) / 6.0f;
			pNew[2] += (dx1[2] + 2*dx2[2] + dx3[2]) / 6.0f;

			if (--iIterationCount <= 0)
				break;

			iCell = RetrieveVelocity(pGrid, pLocator, pNew, iCell, dx1);
		}

		pNewAttrib[0] = float(iCell);
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

#ifdef COLLECT_STATISTICS
	if (bCollectStatistics)
		FinishStatisticsCollection();
#endif

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK4Adaptive                                        */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesTetGrid::IntegrateRK4Adaptive(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
#ifdef COLLECT_STATISTICS
	bool bCollectStatistics = false;
	if (bCollectStatistics)
		StartStatisticsCollection();
#endif

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	int iCell;
	float dx1[3], dx2[3], dx3[3], dx4[3], dxt[3];
	int *pCells = pGrid->GetCells();
	float *pVertices = pGrid->GetVertices();
	int *pCell;
	int iIterationCount;
	double dDeltaTLocal;
	float fVelMag;
	float fInsphere, fCircumsphere;

	for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), dx1);
		if (pNew!=pOld)
			memcpy(pNew, pOld, 3*sizeof(float));
		if (iCell < 0)
		{
			pNewAttrib[0] = -1.0f;
			continue;
		}

		pCell = &pCells[4*iCell];
		ComputeRadii(fInsphere, fCircumsphere, pCell, pVertices);
		dDeltaTLocal = dDeltaT;

		iIterationCount = 1;
		if (fInsphere/fCircumsphere > 1e-5)
		{
			fVelMag = sqrt(VEC_LENGTH(dx1))*static_cast<float>(dDeltaT);
			iIterationCount = int(ceil(fVelMag/fInsphere));
			dDeltaTLocal /= iIterationCount;
		}
#ifdef COLLECT_STATISTICS
		else
		{
			++s_iInvalidRatio;
		}
		++s_iCellChecks;


		if (iIterationCount >= (int) s_vecIterations.size())
			s_vecIterations.resize(iIterationCount+1);
		++s_vecIterations[iIterationCount];
#endif

		while (true)
		{
			SCALAR_MULT(dx1, static_cast<float>(dDeltaTLocal));

			VEC_SMAD(dxt, 0.5f, dx1, pNew);
			RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx2);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaTLocal));

			VEC_SMAD(dxt, 0.5f, dx2, pNew);
			RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx3);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaTLocal));

			VEC_SMAD(dxt, 1.0f, dx3, pNew);
			RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx4);
			SCALAR_MULT(dx4, static_cast<float>(dDeltaTLocal));

			pNew[0] += (dx1[0] + 2*dx2[0] + 2*dx3[0] + dx4[0]) / 6.0f;
			pNew[1] += (dx1[1] + 2*dx2[1] + 2*dx3[1] + dx4[1]) / 6.0f;
			pNew[2] += (dx1[2] + 2*dx2[2] + 2*dx3[2] + dx4[2]) / 6.0f;

			if (--iIterationCount <= 0)
				break;

			iCell = RetrieveVelocity(pGrid, pLocator, pNew, iCell, dx1);
		}

		pNewAttrib[0] = float(iCell);
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

#ifdef COLLECT_STATISTICS
	if (bCollectStatistics)
		FinishStatisticsCollection();
#endif

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateEulerUnsteady                                      */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesTetGrid::IntegrateEulerUnsteady(
	double dVisTime, double dLastVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	float aAlpha[2];
	int aIndices[2];

	// retrieve interpolation weights
	aAlpha[1] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(dLastVisTime, aIndices[0], aIndices[1]) );
	aAlpha[0] = 1.0f - aAlpha[1];

	if (aAlpha[0] < 0)
		return false;

	// lock indices
	set<int> setIndices;
	for (int i=0; i<2; ++i)
		setIndices.insert(aIndices[i]);

	set<int>::iterator it;
	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
		pUGrid->GetTypedLevelDataByLevelIndex(*it)->LockData();

	VveTetGrid *aGrids[2];
	bool bValid = true;
	for (int i=0; i<2; ++i)
	{
		aGrids[i] = pUGrid->GetTypedLevelDataByLevelIndex(aIndices[i])->GetData();
		bValid = bValid & aGrids[i]->GetValid();
	}

	VveTetGrid *pTopologyGrid = aGrids[0]->GetTopologyGrid();
	if (pTopologyGrid)
	{
		pUGrid->GetTopologyGrid()->LockData();
		bValid = bValid & pTopologyGrid->GetValid();
	}
	else
	{
		return false;
	}

	if (!bValid)
	{
		for (it=setIndices.begin(); it!=setIndices.end(); ++it)
			pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
		if (pTopologyGrid)
			pUGrid->GetTopologyGrid()->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pTopologyGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();

	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	// Do we have some shared topology?
	if (pTopologyGrid)
	{
		int iCell;
		float aVel[4];

		for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
		{
			iCell = RetrieveVelocity(aGrids[0], aGrids[1], pLocator, &aAlpha[0], pOld, int(pOldAttrib[0]), aVel);
			VEC_SMAD(pNew, static_cast<float>(dDeltaT), aVel, pOld);
			pNewAttrib[0] = float(iCell);
		}

		if (bComputeScalars)
		{
			if (m_pParent->GetProperties()->GetSingleBuffered())
			{
				pNew = pParticles->GetCurrentData();
				pNewAttrib = pParticles->GetCurrentAttributes();
			}
			else
			{
				pNew = pParticles->GetStaleData();
				pNewAttrib = pParticles->GetStaleAttributes();
			}
			for (int i=0; i<iCount; ++i)
			{
				iCell = RetrieveScalar(aGrids[0], aGrids[1], pLocator, &aAlpha[0], pNew, int(pNewAttrib[0]), pNew[3]);
				pNewAttrib[0] = float(iCell);
				pNew += 4;
				pNewAttrib += 4;
			}
		}
		else
		{
			if (!m_pParent->GetProperties()->GetSingleBuffered())
			{
				pOld = pParticles->GetCurrentData();
				pNew = pParticles->GetStaleData();
				for (int i=0; i<iCount; ++i)
				{
					pNew[3] = pOld[3];
					pNew += 4;
					pOld += 4;
				}
			}
		}
	}
	else
	{
		// now, what do we do with unsteady grids...?!
	}

	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
		pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
	if (pTopologyGrid)
		pUGrid->GetTopologyGrid()->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK3Unsteady                                        */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesTetGrid::IntegrateRK3Unsteady(
	double dVisTime, double dLastVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	float aAlpha[6];
	int aIndices[6];

	// determine t+h/2
	// we already know t==fLastVisTime and t+h==fVisTime
	double dIntermediateTime = dLastVisTime + 0.5 * (dVisTime - dLastVisTime);
	if (dLastVisTime > dVisTime)
	{
		dIntermediateTime = dLastVisTime + 0.5;
		if (dIntermediateTime > 1.0)
		{
			dIntermediateTime -= 1.0;
		}
	}

	// retrieve interpolation weights for all t, t+h/2 and t+h
	aAlpha[1] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(dLastVisTime, aIndices[0], aIndices[1]) );
	aAlpha[0] = 1.0f - aAlpha[1];
	aAlpha[3] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(dIntermediateTime, aIndices[2], aIndices[3]) );
	aAlpha[2] = 1.0f - aAlpha[3];
	aAlpha[5] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(dVisTime, aIndices[4], aIndices[5]) );
	aAlpha[4] = 1.0f - aAlpha[5];

	if (aAlpha[0] < 0 || aAlpha[1] < 0 || aAlpha[2] < 0)
		return false;

	// lock indices
	set<int> setIndices;
	for (int i=0; i<6; ++i)
		setIndices.insert(aIndices[i]);

	set<int>::iterator it;
	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
		pUGrid->GetTypedLevelDataByLevelIndex(*it)->LockData();

	VveTetGrid *aGrids[6];
	bool bValid = true;
	for (int i=0; i<6; ++i)
	{
		aGrids[i] = pUGrid->GetTypedLevelDataByLevelIndex(aIndices[i])->GetData();
		bValid = bValid & aGrids[i]->GetValid();
	}

	VveTetGrid *pTopologyGrid = aGrids[0]->GetTopologyGrid();
	if (pTopologyGrid)
	{
		pUGrid->GetTopologyGrid()->LockData();
		bValid = bValid & pTopologyGrid->GetValid();
	}
	else
	{
		return false;
	}

	if (!bValid)
	{
		for (it=setIndices.begin(); it!=setIndices.end(); ++it)
			pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
		if (pTopologyGrid)
			pUGrid->GetTopologyGrid()->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pTopologyGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();

	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	// Do we have some shared topology?
	if (pTopologyGrid)
	{
		int iCell;
		float dx1[3], dx2[3], dx3[3], dxt[3];

		for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
		{
			iCell = RetrieveVelocity(aGrids[0], aGrids[1], pLocator, &aAlpha[0], pOld, int(pOldAttrib[0]), dx1);
			if (iCell<0)
			{
				pNew[0] = pOld[0];
				pNew[1] = pOld[1];
				pNew[2] = pOld[2];
				pNewAttrib[0] = -1.0f;
				continue;
			}

			SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx1, pOld);
			RetrieveVelocity(aGrids[2], aGrids[3], pLocator, &aAlpha[2], dxt, iCell, dx2);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, -1.0f, dx1, pOld);
			VEC_SMAD(dxt, 2.0f, dx2, dxt);
			RetrieveVelocity(aGrids[4], aGrids[5], pLocator, &aAlpha[4], dxt, iCell, dx3);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

			pNew[0] = pOld[0] + (dx1[0] + 2*dx2[0] + dx3[0]) / 6.0f;
			pNew[1] = pOld[1] + (dx1[1] + 2*dx2[1] + dx3[1]) / 6.0f;
			pNew[2] = pOld[2] + (dx1[2] + 2*dx2[2] + dx3[2]) / 6.0f;

			pNewAttrib[0] = float(iCell);
		}

		if (bComputeScalars)
		{
			if (m_pParent->GetProperties()->GetSingleBuffered())
			{
				pNew = pParticles->GetCurrentData();
				pNewAttrib = pParticles->GetCurrentAttributes();
			}
			else
			{
				pNew = pParticles->GetStaleData();
				pNewAttrib = pParticles->GetStaleAttributes();
			}
			for (int i=0; i<iCount; ++i)
			{
				iCell = RetrieveScalar(aGrids[4], aGrids[5], pLocator, &aAlpha[4], pNew, int(pNewAttrib[0]), pNew[3]);
				pNewAttrib[0] = float(iCell);
				pNew += 4;
				pNewAttrib += 4;
			}
		}
		else
		{
			if (!m_pParent->GetProperties()->GetSingleBuffered())
			{
				pOld = pParticles->GetCurrentData();
				pNew = pParticles->GetStaleData();
				for (int i=0; i<iCount; ++i)
				{
					pNew[3] = pOld[3];
					pNew += 4;
					pOld += 4;
				}
			}
		}
	}
	else
	{
		// now, what do we do with unsteady grids...?!
	}

	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
		pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
	if (pTopologyGrid)
		pUGrid->GetTopologyGrid()->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK4Unsteady                                        */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesTetGrid::IntegrateRK4Unsteady(
	double dVisTime, double dLastVisTime, double dDeltaT,  bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	float aAlpha[6];
	int aIndices[6];

	// determine t+h/2
	// we already know t==fLastVisTime and t+h==fVisTime
	double dIntermediateTime = dLastVisTime + 0.5 * (dVisTime - dLastVisTime);
	if (dLastVisTime > dVisTime)
	{
		dIntermediateTime = dLastVisTime + 0.5;
		if (dIntermediateTime > 1.0)
		{
			dIntermediateTime -= 1.0;
		}
	}

	// retrieve interpolation weights for all t, t+h/2 and t+h
	aAlpha[1] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(dLastVisTime, aIndices[0], aIndices[1]) );
	aAlpha[0] = 1.0f - aAlpha[1];
	aAlpha[3] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(dIntermediateTime, aIndices[2], aIndices[3]) );
	aAlpha[2] = 1.0f - aAlpha[3];
	aAlpha[5] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(dVisTime, aIndices[4], aIndices[5]) );
	aAlpha[4] = 1.0f - aAlpha[5];

	if (aAlpha[0] < 0 || aAlpha[1] < 0 || aAlpha[2] < 0)
		return false;

	// lock indices
	set<int> setIndices;
	for (int i=0; i<6; ++i)
		setIndices.insert(aIndices[i]);

	set<int>::iterator it;
	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
		pUGrid->GetTypedLevelDataByLevelIndex(*it)->LockData();

	VveTetGrid *aGrids[6];
	bool bValid = true;
	for (int i=0; i<6; ++i)
	{
		aGrids[i] = pUGrid->GetTypedLevelDataByLevelIndex(aIndices[i])->GetData();
		bValid = bValid & aGrids[i]->GetValid();
	}

	VveTetGrid *pTopologyGrid = aGrids[0]->GetTopologyGrid();
	if (pTopologyGrid)
	{
		pUGrid->GetTopologyGrid()->LockData();
		bValid = bValid & pTopologyGrid->GetValid();
	}

	if (!bValid)
	{
		for (it=setIndices.begin(); it!=setIndices.end(); ++it)
			pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
		if (pTopologyGrid)
			pUGrid->GetTopologyGrid()->UnlockData();
		return false;
	}

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();

	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	// Do we have some shared topology?
	if (pTopologyGrid)
	{
		VveTetGridPointLocator *pLocator = pTopologyGrid->GetPointLocator();
		int iCell;
		float dx1[3], dx2[3], dx3[3], dx4[3], dxt[3];

		for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
		{
			iCell = RetrieveVelocity(aGrids[0], aGrids[1], pLocator, &aAlpha[0], pOld, int(pOldAttrib[0]), dx1);
			if (iCell<0)
			{
				pNew[0] = pOld[0];
				pNew[1] = pOld[1];
				pNew[2] = pOld[2];
				pNewAttrib[0] = -1.0f;
				continue;
			}

			SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx1, pOld);
			RetrieveVelocity(aGrids[2], aGrids[3], pLocator, &aAlpha[2], dxt, iCell, dx2);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx2, pOld);
			RetrieveVelocity(aGrids[2], aGrids[3], pLocator, &aAlpha[2], dxt, iCell, dx3);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 1.0f, dx3, pOld);
			RetrieveVelocity(aGrids[4], aGrids[5], pLocator, &aAlpha[4], dxt, iCell, dx4);
			SCALAR_MULT(dx4, static_cast<float>(dDeltaT));

			pNew[0] = pOld[0] + (dx1[0] + 2*dx2[0] + 2*dx3[0] + dx4[0]) / 6.0f;
			pNew[1] = pOld[1] + (dx1[1] + 2*dx2[1] + 2*dx3[1] + dx4[1]) / 6.0f;
			pNew[2] = pOld[2] + (dx1[2] + 2*dx2[2] + 2*dx3[2] + dx4[2]) / 6.0f;

			pNewAttrib[0] = float(iCell);
		}

		if (bComputeScalars)
		{
			if (m_pParent->GetProperties()->GetSingleBuffered())
			{
				pNew = pParticles->GetCurrentData();
				pNewAttrib = pParticles->GetCurrentAttributes();
			}
			else
			{
				pNew = pParticles->GetStaleData();
				pNewAttrib = pParticles->GetStaleAttributes();
			}
			for (int i=0; i<iCount; ++i)
			{
				iCell = RetrieveScalar(aGrids[4], aGrids[5], pLocator, &aAlpha[4], pNew, int(pNewAttrib[0]), pNew[3]);
				pNewAttrib[0] = float(iCell);
				pNew += 4;
				pNewAttrib += 4;
			}
		}
		else
		{
			if (!m_pParent->GetProperties()->GetSingleBuffered())
			{
				pOld = pParticles->GetCurrentData();
				pNew = pParticles->GetStaleData();
				for (int i=0; i<iCount; ++i)
				{
					pNew[3] = pOld[3];
					pNew += 4;
					pOld += 4;
				}
			}
		}
	}
	else
	{
		// now, what do we do with unsteady grids...?!
	}

	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
		pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
	if (pTopologyGrid)
		pUGrid->GetTopologyGrid()->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflCpuParticlesTetGrid::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleTracer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflCpuParticlesTetGridReflType);
	return i+1;
}

/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeNormalize                                                 */
/*                                                                            */
/*============================================================================*/
static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveTetGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	v3Out[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * v3In[0];
	v3Out[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * v3In[1];
	v3Out[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * v3In[2];
	v3Out[3] = v3In[3];
}

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveTetGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	aOut[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * aIn[0];
	aOut[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * aIn[1];
	aOut[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * aIn[2];
	aOut[3] = aIn[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RetrieveVelocity                                            */
/*                                                                            */
/*============================================================================*/
static inline int RetrieveVelocity(VveTetGrid *pGrid, VveTetGridPointLocator *pLocator,
								   float aPos[3], int iStartCell, float aData[3])
{
	float aBC[4];

#ifdef COLLECT_STATISTICS
	list<int> liSteps;
	int iCell = pLocator->TetWalk(iStartCell, aPos, aBC, liSteps);
	int iSteps = (int) liSteps.size();
	--iSteps;
	if (iSteps>=0 && iSteps>=(int)s_vecTetSteps.size())
		s_vecTetSteps.resize(iSteps+1);
	if (iSteps>=0)
		++s_vecTetSteps[iSteps];
#else
	int iCell = pLocator->TetWalk(iStartCell, aPos, aBC);
#endif

	if (iCell < 0)
	{
		memset(aData, 0, 3*sizeof(float));
		return -1;
	}

	int *pCell = &pGrid->GetCells()[4*iCell];
	float *v[4];
	for (int j=0; j<4; ++j)
		v[j] = &pGrid->GetPointData()[pGrid->GetComponents()*pCell[j]];

	for (int j=0; j<3; ++j)
	{
		aData[j] = 0;
		for (int k=0; k<4; ++k)
			aData[j] += aBC[k] * v[k][j];
	}
	return iCell;
}

static inline int RetrieveVelocity(VveTetGrid *pGrid1, VveTetGrid *pGrid2,
								   VveTetGridPointLocator *pLocator,
								   float aWeights[2],
								   float aPos[3], int iStartCell, float aData[3])
{
	float aBC[4];
#ifdef COLLECT_STATISTICS
	list<int> liSteps;
	int iCell = pLocator->TetWalk(iStartCell, aPos, aBC, liSteps);
	int iSteps = (int) liSteps.size();
	--iSteps;
	if (iSteps>=0 && iSteps>=(int)s_vecTetSteps.size())
		s_vecTetSteps.resize(iSteps+1);
	if (iSteps>=0)
		++s_vecTetSteps[iSteps];
#else
	int iCell = pLocator->TetWalk(iStartCell, aPos, aBC);
#endif

	if (iCell < 0)
	{
		memset(aData, 0, 3*sizeof(float));
		return -1;
	}

	int *pCell = &pGrid1->GetCells()[4*iCell];
	float *u[4], *v[4];
	for (int j=0; j<4; ++j)
	{
		u[j] = &pGrid1->GetPointData()[pGrid1->GetComponents()*pCell[j]];
		v[j] = &pGrid2->GetPointData()[pGrid2->GetComponents()*pCell[j]];
	}

	for (int j=0; j<3; ++j)
	{
		aData[j] = 0;
		for (int k=0; k<4; ++k)
			aData[j] += aBC[k] * (aWeights[0] * u[k][j] + aWeights[1] * v[k][j]);;
	}
	return iCell;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RetrieveScalar                                              */
/*                                                                            */
/*============================================================================*/
static inline int RetrieveScalar(VveTetGrid *pGrid, VveTetGridPointLocator *pLocator,
								 float aPos[3], int iStartCell, float &fData)
{
	float aBC[4];
#ifdef COLLECT_STATISTICS
	list<int> liSteps;
	int iCell = pLocator->TetWalk(iStartCell, aPos, aBC, liSteps);
	int iSteps = (int) liSteps.size();
	--iSteps;
	if (iSteps>=0 && iSteps>=(int)s_vecTetSteps.size())
		s_vecTetSteps.resize(iSteps+1);
	if (iSteps>=0)
		++s_vecTetSteps[iSteps];
#else
	int iCell = pLocator->TetWalk(iStartCell, aPos, aBC);
#endif

	if (iCell < 0)
	{
		fData = 0;
		return -1;
	}

	int *pCell = &pGrid->GetCells()[4*iCell];
	float *v[4];
	for (int j=0; j<4; ++j)
		v[j] = &pGrid->GetPointData()[pGrid->GetComponents()*pCell[j]];

	fData = 0;
	for (int j=0; j<4; ++j)
		fData += aBC[j] * v[j][3];

	return iCell;
}

static inline int RetrieveScalar(VveTetGrid *pGrid1, VveTetGrid *pGrid2,
								 VveTetGridPointLocator *pLocator,
								 float aWeights[2],
								 float aPos[3], int iStartCell, float &fData)
{
	float aBC[4];
#ifdef COLLECT_STATISTICS
	list<int> liSteps;
	int iCell = pLocator->TetWalk(iStartCell, aPos, aBC, liSteps);
	int iSteps = (int) liSteps.size();
	--iSteps;
	if (iSteps>=0 && iSteps>=(int)s_vecTetSteps.size())
		s_vecTetSteps.resize(iSteps+1);
	if (iSteps>=0)
		++s_vecTetSteps[iSteps];
#else
	int iCell = pLocator->TetWalk(iStartCell, aPos, aBC);
#endif

	if (iCell < 0)
	{
		fData = 0;
		return -1;
	}

	int *pCell = &pGrid1->GetCells()[4*iCell];
	float *u[4], *v[4];
	for (int j=0; j<4; ++j)
	{
		u[j] = &pGrid1->GetPointData()[pGrid1->GetComponents()*pCell[j]];
		v[j] = &pGrid2->GetPointData()[pGrid2->GetComponents()*pCell[j]];
	}

	fData = 0;
	for (int j=0; j<4; ++j)
		fData += aBC[j] * (aWeights[0] * u[j][3] + aWeights[1] * v[j][3]);

	return iCell;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeRadii                                                */
/*                                                                            */
/*============================================================================*/
static inline void ComputeRadii(float &fInsphere, float &fCircumsphere,
								int *pCell, float *pVertices)
{
	float *x1(&pVertices[3*pCell[0]]);
	float *x2(&pVertices[3*pCell[1]]);
	float *x3(&pVertices[3*pCell[2]]);
	float *x4(&pVertices[3*pCell[3]]);

	float v21[3], v31[3], v41[3], v32[3], v42[3];
	float v31Xv41[3], vTemp[3], vTemp2[3];
	float L0, L1, L2, L3, fGamma;

	v21[0] = x2[0]-x1[0];
	v21[1] = x2[1]-x1[1];
	v21[2] = x2[2]-x1[2];

	v31[0] = x3[0]-x1[0];
	v31[1] = x3[1]-x1[1];
	v31[2] = x3[2]-x1[2];

	v41[0] = x4[0]-x1[0];
	v41[1] = x4[1]-x1[1];
	v41[2] = x4[2]-x1[2];

	v32[0] = x3[0]-x2[0];
	v32[1] = x3[1]-x2[1];
	v32[2] = x3[2]-x2[2];

	v42[0] = x4[0]-x2[0];
	v42[1] = x4[1]-x2[1];
	v42[2] = x4[2]-x2[2];

	VEC_CROSS(v31Xv41, v31, v41);
	VEC_CROSS(vTemp2, v21, v31);
	L0 = VEC_LENGTH(vTemp2);

	VEC_SMULT(vTemp, VEC_DOT(v41, v41), vTemp2);
	VEC_CROSS(vTemp2, v41, v21);
	L1 = VEC_LENGTH(vTemp2);

	VEC_SMAD(vTemp, VEC_DOT(v31, v31), vTemp2, vTemp);
	VEC_SMAD(vTemp, VEC_DOT(v21, v21), v31Xv41, vTemp);
	fCircumsphere = fabs(0.5f * VEC_LENGTH(vTemp) / VEC_DOT(v21, v31Xv41));

	VEC_CROSS(vTemp, x3, x4);
	fGamma = VEC_DOT(x2, vTemp) - VEC_DOT(x1, vTemp);
	VEC_CROSS(vTemp, x2, x4);
	fGamma += VEC_DOT(x1, vTemp);
	VEC_CROSS(vTemp, x2, x3);
	fGamma -= VEC_DOT(x1, vTemp);

	L2 = VEC_LENGTH(v31Xv41);
	VEC_CROSS(vTemp, v42, v32);
	L3 = VEC_LENGTH(vTemp);
	fInsphere = fabs(fGamma / (L0+L1+L2+L3));
}

#ifdef COLLECT_STATISTICS
/*============================================================================*/
/*                                                                            */
/*  NAME      :   StartStatisticsCollection                                   */
/*                                                                            */
/*============================================================================*/
void StartStatisticsCollection()
{
	s_vecTetSteps.clear();
	s_vecIterations.clear();
	s_iInvalidRatio = 0;
	s_iCellChecks = 0;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   FinishStatisticsCollection                                  */
/*                                                                            */
/*============================================================================*/
void FinishStatisticsCollection()
{
	cout << "************************************" << endl;
	cout << "*** dumping statistics" << endl;
	cout << endl;
	cout << "cell checks: " << s_iCellChecks << endl;
	cout << "cells with invalid ratio: " << s_iInvalidRatio 
		<< " (" << (s_iInvalidRatio*100.0f/s_iCellChecks) << "%)" << endl;
	cout << endl;

	cout << "tet walk statistics:" << endl;
	unsigned int iTetWalks = 0;
	for (int i=0; i<(int)s_vecTetSteps.size(); ++i)
		iTetWalks += s_vecTetSteps[i];
	cout << "step counts:" << endl;
	unsigned int iTotalSteps = 0;
	for (int i=0; i<(int)s_vecTetSteps.size(); ++i)
	{
		cout << "steps: " << i << " - count: " << s_vecTetSteps[i] << "          ("
			<< (s_vecTetSteps[i]*100.0f/iTetWalks) << "%)" << endl;
		iTotalSteps += i*s_vecTetSteps[i];
	}
	cout << "tet walk count: " << iTetWalks << endl;
	cout << "tet steps count: " << iTotalSteps << endl;
	cout << "avg. steps per walk: " << (float(iTotalSteps)/iTetWalks) << endl;
	cout << endl;

	cout << "iteration statistics: " << endl;
	unsigned int iIntegrations = 0;
	for (int i=0; i<(int)s_vecIterations.size(); ++i)
		iIntegrations += s_vecIterations[i];
	cout << "iteration counts:" << endl;
	int iTotalIterations = 0;
	for (int i=0; i<(int)s_vecIterations.size(); ++i)
	{
		cout << "iterations: " << i << " - count: " << s_vecIterations[i] << "          ("
			<< (s_vecIterations[i]*100.0f/iIntegrations) << "%)" << endl;
		iTotalIterations += s_vecIterations[i];
	}
	cout << "integration count: " << iIntegrations << endl;
	cout << "iteration count: " << iTotalIterations << endl;
	cout << "avg. iterations per integration: " 
		<< (float(iTotalIterations)/iIntegrations) << endl;
	cout << endl;
}

#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
