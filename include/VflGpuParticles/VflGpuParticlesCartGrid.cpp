/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuParticlesCartGrid.cpp  RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflGpuParticlesCartGrid.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include <VistaFlowLib/Data/VflUnsteadyCartesianGridPusher.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaVisExt/Data/VveCartesianGrid.h>
#include <VistaAspects/VistaAspectsUtils.h>

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaFramebufferObj.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

#include <set>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflGpuParticlesCartGridReflType("VflGpuParticlesCartGrid");

static IVistaPropertyGetFunctor *s_aCVflGpuParticlesCartGridGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflGpuParticlesCartGridSetFunctors[] =
{
	NULL
};

#define SHADER_STEADY_EULER				"VflGpuParticlesCart_Steady_Euler.glsl"
#define SHADER_STEADY_EULER_SCALARS		"VflGpuParticlesCart_Steady_EulerScalars.glsl"
#define SHADER_STEADY_RK3				"VflGpuParticlesCart_Steady_RK3.glsl"
#define SHADER_STEADY_RK3_SCALARS		"VflGpuParticlesCart_Steady_RK3Scalars.glsl"
#define SHADER_STEADY_RK4				"VflGpuParticlesCart_Steady_RK4.glsl"
#define SHADER_STEADY_RK4_SCALARS		"VflGpuParticlesCart_Steady_RK4Scalars.glsl"
#define SHADER_UNSTEADY_EULER			"VflGpuParticlesCart_Unsteady_Euler.glsl"
#define SHADER_UNSTEADY_EULER_SCALARS	"VflGpuParticlesCart_Unsteady_EulerScalars.glsl"
#define SHADER_UNSTEADY_RK3				"VflGpuParticlesCart_Unsteady_RK3.glsl"
#define SHADER_UNSTEADY_RK3_SCALARS		"VflGpuParticlesCart_Unsteady_RK3Scalars.glsl"
#define SHADER_UNSTEADY_RK4				"VflGpuParticlesCart_Unsteady_RK4.glsl"
#define SHADER_UNSTEADY_RK4_SCALARS		"VflGpuParticlesCart_Unsteady_RK4Scalars.glsl"

static inline void DeNormalize(VistaVector3D &v3Out,
	const VistaVector3D &v3In,
	VveCartesianGrid *pGrid);

static inline void DeNormalize(float aOut[4],
	const float aIn[4],
	VveCartesianGrid *pGrid);


static void DumpTexture(VistaTexture *pTexture, 
	int iWidth, int iHeight, int iTuples,
	ostream &out);

static void DumpTextureToFile(VistaTexture *pTexture, 
	int iWidth, int iHeight, int iTuples,
	const std::string &strInfo,
	const std::string &strFilename);

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflGpuParticlesCartGrid::VflGpuParticlesCartGrid(VflVisGpuParticles *pParent)
	: IVflGpuParticleTracer(pParent),
	m_pWriteTC(NULL),
	m_bValid(false)
{
	m_bValid = CreateGpuResources();
}

VflGpuParticlesCartGrid::~VflGpuParticlesCartGrid()
{
	DestroyGpuResources();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesCartGrid::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();

	if (!pProperties->GetActive())
		return;

	double dVisTime = m_pParent->GetVisTime();
	VveTimeMapper *pTimeMapper = m_pParent->GetData()->GetTimeMapper();

	// retrieve integration parameters
	int iIntegrator = pProperties->GetIntegrator();
	bool bComputeScalars = pProperties->GetComputeScalars();

	// determine dataset boundaries
	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pTimeMapper->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return;
	}
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	// get access to flow data
	VflUnsteadyTexturePusher *pPusher = m_pParent->GetPusher();
	VflUnsteadyCartesianGridPusher *pGridPusher = dynamic_cast<VflUnsteadyCartesianGridPusher *>(pPusher);
	if (!pGridPusher)
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to retrieve texture pusher..." << endl;
		return;
	}

	// make sure, we use the correct interpolation scheme
	int iPrecision = pGridPusher->GetTexturePrecision();
	if (iPrecision == VflUnsteadyCartesianGridPusher::TP_FLOAT)
	{
		//MW changed translation from params (force interpolation true/false) to CartesianGridPusher (three interpolation modes)
		if ((pGridPusher->GetInterpolationMode()==VflUnsteadyCartesianGridPusher::IM_LINEAR) != pProperties->GetForceHardwareInterpolation())
		{
			if (pProperties->GetForceHardwareInterpolation())
				pGridPusher->SetInterpolationMode (VflUnsteadyCartesianGridPusher::IM_LINEAR);
			else
				pGridPusher->SetInterpolationMode (VflUnsteadyCartesianGridPusher::IM_NOT_SET);
		}
	}

	m_pParent->SaveOGLState();

	// retrieve previous particle population
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	VistaTexture *pOldPopulation = pParticles->GetCurrentTexture();
	pOldPopulation->Bind(GL_TEXTURE0);


	// bind render target
	pParticles->GetFramebufferObj()->Bind();

	if (pProperties->GetSingleBuffered())
	{
		glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex());
	}
	else
	{
		glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + 1 - pParticles->GetCurrentDataIndex());
	}

	int w, h;
	pParticles->GetSize(w, h);

	VistaGLSLShader *pShader = NULL;
	double dDeltaT = 0.0;

	// determine and bind shader


	if (m_pParent->GetRenderNode()->GetVisTiming()->GetAnimationPlaying())
	{
		// determine time step length
		double dDeltaTVisTime = 0;
		if (m_pParent->GetTimeWarning())
		{
			dDeltaT = pProperties->GetMaxDeltaTime();

			dDeltaTVisTime = pTimeMapper->GetVisualizationTime(pTimeMapper->GetSimulationTime(0)+dDeltaT);
		}
		else
		{
			double dLastVisTime = m_pParent->GetLastVisTime();
			dDeltaTVisTime = dVisTime - dLastVisTime;
			while (dDeltaTVisTime < 0)
				dDeltaTVisTime += 1.0f;

			dDeltaT = pTimeMapper->GetSimulationTime(dDeltaTVisTime)
				- pTimeMapper->GetSimulationTime(0);
		}

		if (pGridPusher->GetTexturePrecision() == VflUnsteadyCartesianGridPusher::TP_FLOAT
			&& !pProperties->GetForceHardwareInterpolation())
		{
			if (bComputeScalars)
			{
				pShader = m_vecShadersUnsteadyScalarsTI[iIntegrator];
			}
			else
			{
				pShader = m_vecShadersUnsteadyTI[iIntegrator];
			}
			if (pShader)
			{
				int aDims[3];
				pGridPusher->GetMaxDimensions(aDims);
				pShader->Bind();
				pShader->SetUniform(pShader->GetUniformLocation("v3Dims"),
					float(aDims[0]), float(aDims[1]), float(aDims[2]));
				pShader->SetUniform(pShader->GetUniformLocation("v3OneByDims"),
					1.0f/aDims[0], 1.0f/aDims[1], 1.0f/aDims[2]);

			}
		}
		else
		{
			if (bComputeScalars)
			{
				pShader = m_vecShadersUnsteadyScalars[iIntegrator];
			}
			else
			{
				pShader = m_vecShadersUnsteady[iIntegrator];
			}
		}


		if (pShader)
		{
			pShader->Bind();

			// set texture information
			vector<VistaTexture *> vecTextures;
			vector<double> vecOffsets;
			vector<float> vecWeights;
			switch (iIntegrator)
			{
			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER:
				vecOffsets.push_back(0);
				if (pGridPusher->GetTexturesAndWeights(vecOffsets, vecTextures, vecWeights) == 1)
				{
					vecTextures[0]->Bind(GL_TEXTURE1);
					vecTextures[1]->Bind(GL_TEXTURE2);

					float aWeights[2];
					aWeights[0] = 1.0f - vecWeights[0];
					aWeights[1] = vecWeights[0];

					pShader->SetUniform(pShader->GetUniformLocation("aWeights[0]"), 1, 2, aWeights);
				}
				break;
			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3:
			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4:
				vecOffsets.push_back(0);
				vecOffsets.push_back(0.5f * dDeltaTVisTime);
				vecOffsets.push_back(dDeltaTVisTime);
				if (pGridPusher->GetTexturesAndWeights(vecOffsets, vecTextures, vecWeights) == 3)
				{
					GLenum iTextureUnit = GL_TEXTURE1;
					float aWeights[6];
					for (int i=0; i<3; ++i)
					{
						vecTextures[2*i]->Bind(iTextureUnit++);
 						vecTextures[2*i+1]->Bind(iTextureUnit++);
						aWeights[2*i] = 1.0f - vecWeights[i];
						aWeights[2*i+1] = vecWeights[i];
					}
					pShader->SetUniform(pShader->GetUniformLocation("aWeights[0]"), 1, 6, aWeights);

					glFlush();
				}
				break;
			};
		}
	}
	else
	{
		// determine time step length
		dDeltaT = pProperties->GetDeltaTime();

		if (pGridPusher->GetTexturePrecision() == VflUnsteadyCartesianGridPusher::TP_FLOAT
			&& !pProperties->GetForceHardwareInterpolation())
		{
			if (bComputeScalars)
			{
				pShader = m_vecShadersSteadyScalarsTI[iIntegrator];
			}
			else
			{
				pShader = m_vecShadersSteadyTI[iIntegrator];
			}
			if (pShader)
			{
				int aDims[3];
				pGridPusher->GetMaxDimensions(aDims);
				pShader->Bind();
				pShader->SetUniform(pShader->GetUniformLocation("v3Dims"),
					float(aDims[0]), float(aDims[1]), float(aDims[2]));
				pShader->SetUniform(pShader->GetUniformLocation("v3OneByDims"),
					1.0f/aDims[0], 1.0f/aDims[1], 1.0f/aDims[2]);
			}
		}
		else
		{		
			if (bComputeScalars)
			{
				pShader = m_vecShadersSteadyScalars[iIntegrator];
			}
			else
			{
				pShader = m_vecShadersSteady[iIntegrator];
			}
		}

		if (pShader)
		{
			pShader->Bind();

			// bind flow data
			pGridPusher->GetTexture()->Bind(GL_TEXTURE1);

			// but stay on texture unit 0
			glActiveTexture(GL_TEXTURE0);
		}
	}

	if (pShader)
	{
		// specify time step length
		pShader->SetUniform(pShader->GetUniformLocation("fDeltaT"),
			static_cast<float>(dDeltaT));

		// inform shader about dataset boundaries
		pShader->SetUniform(pShader->GetUniformLocation("v3Min"),
			v3Min[0], v3Min[1], v3Min[2]);
		pShader->SetUniform(pShader->GetUniformLocation("v3Max"),
			v3Max[0], v3Max[1], v3Max[2]);

		// compute scale and bias for lookup from particle positions
		// into 3D texture
		int aDims[3];
		int aDataDims[3];
		VistaVector3D v3Scale, v3Bias;

		// better ask someone, who knows what to do...
		pGridPusher->GetMaxDimensions(aDims);
		// pGrid->GetDimensions(aDims);

		pGrid->GetDataDimensions(aDataDims);
		for (int i=0; i<3; ++i)
		{
			float fDataDimsByDeltaAndDim = (aDataDims[i]-1) / (v3Max[i] - v3Min[i]) / aDims[i];
			v3Scale[i] = fDataDimsByDeltaAndDim;
			v3Bias[i] = 0.5f/aDims[i] - v3Min[i] * fDataDimsByDeltaAndDim;
		}
		pShader->SetUniform(pShader->GetUniformLocation("v3Scale"),
			v3Scale[0], v3Scale[1], v3Scale[2]);
		pShader->SetUniform(pShader->GetUniformLocation("v3Bias"),
			v3Bias[0], v3Bias[1], v3Bias[2]);

		// draw to the complete texture
		glViewport(0, 0, w, h);
		glLoadIdentity();
		glOrtho(0, 1, 0, 1, -1, 1);


		// [picard] Energize! [\picard]
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(0.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex2f(1.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f);
		glVertex2f(1.0f, 1.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(0.0f, 1.0f);
		glEnd();

		// huh! that's it?!
		pShader->Release();
	}
	pParticles->GetFramebufferObj()->Release();

	m_pParent->RestoreOGLState();

	if (!pProperties->GetSingleBuffered())
		m_pParent->GetParticleData()->Swap();

	m_pParent->GetParticleData()->SignalGpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticle                                                */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesCartGrid::SeedParticle(const VistaVector3D &v3Pos)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();

	int w, h;
	pParticles->GetSize(w, h);

	m_pParent->SaveOGLState();

	// set render target
	pParticles->GetFramebufferObj()->Bind();
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex());

	// activate shader for writing particle positions (given as TCs)
	m_pWriteTC->Bind();

	// set viewport and transformation matrices
	glViewport(0, 0, w, h);
	glLoadIdentity();
	glOrtho(0, w, 0, h, -1, 1);

	// ok - who's next?
	float fPosX = (iNext % w) + 0.5f;
	float fPosY = (iNext / w) + 0.5f;

	glBegin(GL_POINTS);
	glTexCoord4fv(&v3Pos[0]);
	glVertex2f(fPosX, fPosY);
	glEnd();

	m_pWriteTC->Release();
	pParticles->GetFramebufferObj()->Release();
	m_pParent->RestoreOGLState();

	pParticles->SetNextParticle((iNext+1)%iCount);
	if (iActive < iCount)
	{
		++iActive;
		pParticles->SetActiveParticles(iActive);
		if (iActive == iCount)
		{
			cout << " [VflGpuParticlesCartGrid] - all particles are active now..." << endl;
		}
	}

	pParticles->SignalGpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticles                                               */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesCartGrid::SeedParticles(const VistaVector3D &v3Pos1, 
	const VistaVector3D &v3Pos2, 
	int iCount)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iParticleCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();

	if (iCount > iParticleCount)
		iCount = iParticleCount;

	float fDelta = 1.0f;
	if (iCount > 1)
		fDelta /= (iCount-1);
	float fOffset = 0;
	VistaVector3D v3Delta(v3Pos2-v3Pos1);
	float fScalarDelta(v3Pos2[3]-v3Pos1[3]);

	int w, h;
	pParticles->GetSize(w, h);

	m_pParent->SaveOGLState();

	// set render target
	pParticles->GetFramebufferObj()->Bind();
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex());

	// activate shader for writing particle positions (given as TCs)
	m_pWriteTC->Bind();

	// set viewport and transformation matrices
	glViewport(0, 0, w, h);
	glLoadIdentity();
	glOrtho(0, w, 0, h, -1, 1);

	glBegin(GL_POINTS);
	for (int i=0; i<iCount; ++i, fOffset+=fDelta)
	{
		VistaVector3D v3NewPos(v3Pos1 + fOffset*v3Delta);
		v3NewPos[3] = v3Pos1[3] + fOffset*fScalarDelta;

		// ok - who's next?
		float fPosX = (iNext % w) + 0.5f;
		float fPosY = (iNext / w) + 0.5f;

		glTexCoord4fv(&v3NewPos[0]);
		glVertex2f(fPosX, fPosY);

		iNext = (iNext+1) % iParticleCount;
	}
	glEnd();

	m_pWriteTC->Release();
	pParticles->GetFramebufferObj()->Release();
	m_pParent->RestoreOGLState();

	if (iActive < iParticleCount)
	{
		iActive += iCount;
		if (iActive >= iParticleCount)
		{
			iActive = iParticleCount;
			cout << " [VflGpuParticlesCartGrid] - all particles are active now..." << endl;
		}

		pParticles->SetActiveParticles(iActive);
	}

	pParticles->SetNextParticle(iNext);
	pParticles->SignalGpuDataChange();
}

void VflGpuParticlesCartGrid::SeedParticles(const std::vector<float> &vecPositions)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iParticleCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();
	int iCount = int(vecPositions.size()) / 4;

	if (iCount > iParticleCount)
		iCount = iParticleCount;

	if (m_vecSeedBuffer.size() < (unsigned int)(iCount*6))
		m_vecSeedBuffer.resize(iCount*6);

	int w, h;
	pParticles->GetSize(w, h);

	m_pParent->SaveOGLState();

#if 0 // def DEBUG
	cout << "dumping texture info BEFORE..." << endl;
	char buf[1024];
	sprintf(buf, "before %d %d %d %d", iNext, iActive, iCount, iParticleCount);
	cout << "blah." << endl;
	DumpTextureToFile(pParticles->GetCurrentTexture(), w, h, 4, std::string(buf), "particle_texture.log");
#endif

	// set render target
	pParticles->GetFramebufferObj()->Bind();
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex());

	// activate shader for writing particle positions (given as TCs)
	m_pWriteTC->Bind();

	// set viewport and transformation matrices
	glViewport(0, 0, w, h);
	glLoadIdentity();
	glOrtho(0, w, 0, h, -1, 1);

	float *pBuffer = &m_vecSeedBuffer[0];
	const float *pPositions = &vecPositions[0];
	for (int i=0; i<iCount; ++i)
	{
		memcpy(&pBuffer[6*i], &pPositions[4*i], 4*sizeof(float));

		// ok - who's next?
		pBuffer[6*i + 4] = (iNext % w) + 0.5f;
		pBuffer[6*i + 5] = (iNext / w) + 0.5f;

		iNext = (iNext+1) % iParticleCount;
	}

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, 6*sizeof(float), &pBuffer[4]);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(4, GL_FLOAT, 6*sizeof(float), pBuffer);

	glDrawArrays(GL_POINTS, 0, iCount);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	m_pWriteTC->Release();
	pParticles->GetFramebufferObj()->Release();
	m_pParent->RestoreOGLState();

	if (iActive < iParticleCount)
	{
		iActive += iCount;
		if (iActive >= iParticleCount)
		{
			iActive = iParticleCount;
			cout << " [VflGpuParticlesCartGrid] - all particles are active now..." << endl;
		}

		pParticles->SetActiveParticles(iActive);
	}

#if 0 // def DEBUG
	cout << "dumping texture info AFTER..." << endl;
	sprintf(buf, "after %d %d %d %d", iNext, iActive, iCount, iParticleCount);
	DumpTextureToFile(pParticles->GetCurrentTexture(), w, h, 4, std::string(buf), "particle_texture.log");
#endif

	pParticles->SetNextParticle(iNext);
	pParticles->SignalGpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticleNormalized                                      */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesCartGrid::SeedParticleNormalized(const VistaVector3D &v3Pos)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3PosDN;
	DeNormalize(v3PosDN, v3Pos, pGrid);
	SeedParticle(v3PosDN);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticlesNormalized                                     */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesCartGrid::SeedParticlesNormalized(const VistaVector3D &v3Pos1, 
	const VistaVector3D &v3Pos2, 
	int iCount)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3Pos1DN, v3Pos2DN;
	DeNormalize(v3Pos1DN, v3Pos1, pGrid);
	DeNormalize(v3Pos2DN, v3Pos2, pGrid);
	SeedParticles(v3Pos1DN, v3Pos2DN, iCount);
}

void VflGpuParticlesCartGrid::SeedParticlesNormalized(const std::vector<float> &vecPositions)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	vector<float> vecDNPositions;
	int iCount = int(vecPositions.size()) / 4;
	vecDNPositions.resize(vecPositions.size());
	for (int i=0; i<iCount; ++i)
	{
		DeNormalize(&vecDNPositions[4*i], &vecPositions[4*i], pGrid);
	}

	SeedParticles(vecDNPositions);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGridType                                                 */
/*                                                                            */
/*============================================================================*/
int VflGpuParticlesCartGrid::GetGridType() const
{
	return VflVisGpuParticles::GT_CARTESIAN_GRID;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProcessingUnit                                           */
/*                                                                            */
/*============================================================================*/
int VflGpuParticlesCartGrid::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_GPU;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerType                                               */
/*                                                                            */
/*============================================================================*/
int VflGpuParticlesCartGrid::GetTracerType() const
{
	return VflVisGpuParticles::TT_PARTICLES;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflGpuParticlesCartGrid::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflGpuParticlesCartGrid::GetReflectionableType() const
{
	return s_strCVflGpuParticlesCartGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateGpuResources                                          */
/*                                                                            */
/*============================================================================*/
bool VflGpuParticlesCartGrid::CreateGpuResources()
{
	// TODO: check for required OpenGL extensions...
#ifdef DEBUG
	cout << " [VflGpuParticlesCartGrid] - creating GPU resources..." << endl;
#endif

	// init shaders -----------------------------------------------------------
#ifdef DEBUG
	cout << " [VflGpuParticlesCartGrid] - creating shader for writing tex coords..." << endl;
#endif
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();
	std::string strShader =
		rShaderReg.RetrieveShader( "VflGpuCartGrid_WriteTC_frag.glsl" );

	VistaGLSLShader* pProgram = new VistaGLSLShader();
	if(    strShader.empty()
		|| !pProgram->InitShaderFromString( GL_FRAGMENT_SHADER, strShader )
		|| !pProgram->Link() )
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to initialize shader for writing texcoords..." << endl;
		delete pProgram;
		return false;
	}
	m_pWriteTC = pProgram;

	string strPrefix("#define TRILINEAR_INTERPOLATION\n");

	m_vecShadersSteady.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersSteadyScalars.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersSteadyTI.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersSteadyScalarsTI.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
#ifdef DEBUG
	cout << " [VflGpuParticlesCartGrid] - creating Euler integration shaders..." << endl;
#endif
	VistaGLSLShader *pShader = new VistaGLSLShader;

	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_EULER );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - Euler integration shader not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init Euler integration shader..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link Euler integration shader..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_EULER_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - Euler integration shader w/scalars not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init Euler integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link Euler integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_EULER );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - Euler integration shader w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init Euler integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link Euler integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_EULER_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - Euler integration shader w/scalars w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init Euler integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link Euler integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyScalarsTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

#ifdef DEBUG
	cout << " [VflGpuParticlesCartGrid] - creating RK3 integration shaders..." << endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_RK3 );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - RK3 integration shader not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init RK3 integration shader..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link RK3 integration shader..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_RK3_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - RK3 integration shader w/scalars not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init RK3 integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link RK3 integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_RK3 );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - RK3 integration shader w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init RK3 integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link RK3 integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_RK3_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - RK3 integration shader w/scalars w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init RK3 integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link RK3 integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyScalarsTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

#ifdef DEBUG
	cout << " [VflGpuParticlesCartGrid] - creating RK4 integration shaders..." << endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_RK4 );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - RK4 integration shader not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init RK4 integration shader..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link RK4 integration shader..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_RK4_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - RK4 integration shader w/scalars not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init RK4 integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link RK4 integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_RK4 );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - RK4 integration shader w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init RK4 integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link RK4 integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_STEADY_RK4_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - RK4 integration shader w/scalars w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init RK4 integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link RK4 integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyScalarsTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	m_vecShadersUnsteady.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersUnsteadyScalars.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersUnsteadyTI.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersUnsteadyScalarsTI.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
#ifdef DEBUG
	cout << " [VflGpuParticlesCartGrid] - creating unsteady Euler integration shaders..." << endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_EULER );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady Euler integration shader not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init unsteady Euler integration shader..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link unsteady Euler integration shader..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 2, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_EULER_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady Euler integration shader w/scalars not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init unsteady Euler integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link unsteady Euler integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 2, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_EULER );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady Euler integration shader w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init unsteady Euler integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link unsteady Euler integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 2, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_EULER_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady Euler integration shader w/scalars w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init Euler integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link Euler integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 2, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalarsTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

#ifdef DEBUG
	cout << " [VflGpuParticlesCartGrid] - creating unsteady RK3 integration shaders..." << endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_RK3 );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady RK3 integration shader not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init unsteady RK3 integration shader..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link unsteady RK3 integration shader..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_RK3_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady RK3 integration shader w/scalars not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init unsteady RK3 integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link unsteady RK3 integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_RK3 );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady RK3 integration shader w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init unsteady RK3 integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link unsteady RK3 integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_RK3_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady RK3 integration shader w/scalars w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init unsteady RK3 integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link unsteady RK3 integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalarsTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

#ifdef DEBUG
	cout << " [VflGpuParticlesCartGrid] - creating unsteady RK4 integration shaders..." << endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_RK4 );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady RK4 integration shader not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init unsteady RK4 integration shader..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link unsteady RK4 integration shader..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_RK4_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady RK4 integration shader w/scalars not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to unsteady init RK4 integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to unsteady link RK4 integration shader w/scalars..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_RK4 );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady RK4 integration shader w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to init unsteady RK4 integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to link unsteady RK4 integration shader w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader( SHADER_UNSTEADY_RK4_SCALARS );
	if(strShader.empty())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unsteady RK4 integration shader w/scalars w/trilinear interpolation not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strPrefix+strShader))
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to unsteady init RK4 integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticlesCartGrid] - ERROR - unable to unsteady link RK4 integration shader w/scalars w/trilinear interpolation..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalarsTI[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DestroyGpuResources                                         */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesCartGrid::DestroyGpuResources()
{
	delete m_pWriteTC;
	m_pWriteTC = NULL;

	set<VistaGLSLShader *> setShaders;

	for (unsigned int i=0; i<m_vecShadersSteady.size(); ++i)
	{
		setShaders.insert(m_vecShadersSteady[i]);
	}
	m_vecShadersSteady.clear();

	for (unsigned int i=0; i<m_vecShadersSteadyScalars.size(); ++i)
	{
		setShaders.insert(m_vecShadersSteadyScalars[i]);
	}
	m_vecShadersSteadyScalars.clear();

	for (unsigned int i=0; i<m_vecShadersSteadyTI.size(); ++i)
	{
		setShaders.insert(m_vecShadersSteadyTI[i]);
	}
	m_vecShadersSteadyTI.clear();

	for (unsigned int i=0; i<m_vecShadersSteadyScalarsTI.size(); ++i)
	{
		setShaders.insert(m_vecShadersSteadyScalarsTI[i]);
	}
	m_vecShadersSteadyScalarsTI.clear();

	for (unsigned int i=0; i<m_vecShadersUnsteady.size(); ++i)
	{
		setShaders.insert(m_vecShadersUnsteady[i]);
	}
	m_vecShadersUnsteady.clear();

	for (unsigned int i=0; i<m_vecShadersUnsteadyScalars.size(); ++i)
	{
		setShaders.insert(m_vecShadersUnsteadyScalars[i]);
	}
	m_vecShadersUnsteadyScalars.clear();

	for (unsigned int i=0; i<m_vecShadersUnsteadyTI.size(); ++i)
	{
		setShaders.insert(m_vecShadersUnsteadyTI[i]);
	}
	m_vecShadersUnsteadyTI.clear();

	for (unsigned int i=0; i<m_vecShadersUnsteadyScalarsTI.size(); ++i)
	{
		setShaders.insert(m_vecShadersUnsteadyScalarsTI[i]);
	}
	m_vecShadersUnsteadyScalarsTI.clear();

	set<VistaGLSLShader *>::iterator it;
	for (it=setShaders.begin(); it!=setShaders.end(); ++it)
	{
		delete *it;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflGpuParticlesCartGrid::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleTracer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflGpuParticlesCartGridReflType);
	return i+1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRendererId                                               */
/*                                                                            */
/*============================================================================*/
const string VflGpuParticlesCartGrid::GetRendererId()
{
	return s_strCVflGpuParticlesCartGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetShaderKeys                                               */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesCartGrid::GetShaderKeys(
	std::vector<std::string>& vecShaderKeys)
{
	vecShaderKeys.clear();
	vecShaderKeys.push_back(SHADER_STEADY_EULER);
	vecShaderKeys.push_back(SHADER_STEADY_EULER_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_RK3);
	vecShaderKeys.push_back(SHADER_STEADY_RK3_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_RK4);
	vecShaderKeys.push_back(SHADER_STEADY_RK4_SCALARS);
	vecShaderKeys.push_back(SHADER_UNSTEADY_EULER);
	vecShaderKeys.push_back(SHADER_UNSTEADY_EULER_SCALARS);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK3);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK3_SCALARS);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK4);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK4_SCALARS);
}

/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeNormalize                                                 */
/*                                                                            */
/*============================================================================*/
static inline void DeNormalize(VistaVector3D &v3Out,
	const VistaVector3D &v3In,
	VveCartesianGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	v3Out[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * v3In[0];
	v3Out[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * v3In[1];
	v3Out[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * v3In[2];
	v3Out[3] = v3In[3];
}

static inline void DeNormalize(float aOut[4],
	const float aIn[4],
	VveCartesianGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	aOut[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * aIn[0];
	aOut[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * aIn[1];
	aOut[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * aIn[2];
	aOut[3] = aIn[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DumpTexture                                                 */
/*                                                                            */
/*============================================================================*/
static void DumpTexture(VistaTexture *pTexture, 
	int iWidth, int iHeight, int iTuples,
	ostream &out)
{
	if (!pTexture)
		return;

	float *pData = new float[iWidth*iHeight*iTuples];
	pTexture->Bind();
	glGetTexImage(pTexture->GetTarget(), 0, GL_RGBA, GL_FLOAT, pData);
	float *pPos = pData;

	for (int i=0; i<iHeight; ++i)
	{
		for (int j=0; j<iWidth; ++j)
		{
			if (j>0)
				out << "| ";
			for (int k=0; k<iTuples; ++k)
			{
				out << (*(pPos++)) << " ";
			}
		}
		out << endl;
	}

	delete [] pData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DumpTextureToFile                                           */
/*                                                                            */
/*============================================================================*/
static void DumpTextureToFile(VistaTexture *pTexture,
	int iWidth, int iHeight, int iTuples,
	const std::string &strInfo,
	const std::string &strFilename)
{
	cout << "trying to dump texture" << endl;
	ofstream os(strFilename.c_str(), ios_base::app | ios_base::out);
	os << "--" << endl;
	os << strInfo << endl;
	DumpTexture(pTexture, iWidth, iHeight, iTuples, os);
	os << endl;
	cout << "done..." << endl;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
