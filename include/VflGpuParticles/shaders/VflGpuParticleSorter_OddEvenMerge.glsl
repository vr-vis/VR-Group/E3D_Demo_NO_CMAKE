// Algorithm and implementation are largely adopted from Kipfer's
// article "Improved GPU Sorting" in GPU Gems 2, chapter 46

uniform sampler2DRect texUnitMapping;
uniform vec3 Params1;
uniform vec3 Params2;

#define OwnPos			gl_TexCoord[0]
#define TwoStage		Params1.x
#define PassModStage	Params1.y
#define TwoStage_PmS_1	Params1.z
#define Width			Params2.x
#define Height			Params2.y
#define Pass			Params2.z

void main(void)
{
	vec3 self = texture2DRect(texUnitMapping, OwnPos.xy).xyz;
	float i = floor(OwnPos.x) + floor(OwnPos.y) * Width;
	float j = floor(mod(i, TwoStage));

	float compare;

	if ( (j<PassModStage) || (j>TwoStage_PmS_1) )
		compare = 0.0;	// just copy
	else
	{	// sort
#if 0
		if (mod((j+PassModStage)/Pass, 2.0) < 1.0)
			compare = 1.0;
		else
			compare = -1.0;
#else
		compare = mod((j+PassModStage)/Pass, 2.0) < 1.0 ? 1.0 : -1.0;
#endif
	}

	// get the partner
	float adr = i + compare*Pass;
	vec3 partner = texture2DRect(texUnitMapping, 
		vec2(floor(mod(adr,Width)), floor(adr/Width))).xyz;

	gl_FragColor = vec4(self.z*compare>=partner.z*compare ? self : partner, 1.0);
}
