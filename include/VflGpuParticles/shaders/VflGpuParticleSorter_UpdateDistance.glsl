uniform sampler2D texUnitParticles;
uniform sampler2DRect texUnitMapping;
uniform vec4 v4ViewPosition;	// viewer position (xyz), active particle count (w)
uniform vec2 v2WidthHeight;		// width, height

void main(void)
{

	// retrieve texture coordinate for particle position
	vec2 v2TC = texture2DRect(texUnitMapping, gl_TexCoord[0].xy).xy;

	// retrieve particle position
	vec3 v3Pos = texture2D(texUnitParticles, v2TC).xyz;

	// determine particle index
	vec2 v2Temp = floor(v2TC * v2WidthHeight);
	float fIndex = v2Temp.x + v2Temp.y * v2WidthHeight.x;

	// compute squared distance
	v3Pos -= v4ViewPosition.xyz;
#if 0
	float fDist = dot(v3Pos, v3Pos);
#else
	float fDist = fIndex < v4ViewPosition.w ? dot(v3Pos, v3Pos) : -1.0;
#endif

	gl_FragColor = vec4(v2TC, fDist, 1.0);
}
