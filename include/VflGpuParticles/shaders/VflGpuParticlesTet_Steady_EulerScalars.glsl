uniform sampler2D texUnitPosition;
uniform sampler2D texUnitAttrib;
uniform sampler2DRect texUnitVertices;
uniform sampler2DRect texUnitPointData;
uniform sampler2DRect texUnitCells;
uniform sampler2DRect texUnitCellNeighbors;

uniform vec2 v2TexWidth;	// texture widths for vertex and cell textures
uniform float fDeltaT;

#if 1
vec2 ComputePointTCs(float fIndex)
{
	vec2 result;
	int iTemp = int(fIndex) / int(v2TexWidth.x);
	result.y = float(iTemp) + 0.5;
	result.x = fIndex - v2TexWidth.x * float(iTemp) + 0.5;
	return result;
}

vec2 ComputeCellTCs(float fIndex)
{
	vec2 result;
	int iTemp = int(fIndex) / int(v2TexWidth.y);
	result.y = float(iTemp) + 0.5;
	result.x = fIndex - v2TexWidth.y * float(iTemp) + 0.5;
	return result;
}
#else
vec2 ComputePointTCs(float fIndex)
{
	vec2 result;
	int iTemp = int(fIndex) / int(v2TexWidth.x);
	result.y = float(iTemp) + 0.5;
	result.x = fIndex - v2TexWidth.x * float(iTemp) + 0.5;
	return result;
}

vec2 ComputeCellTCs(float fIndex)
{
	vec2 result;
	int iTemp = int(fIndex) / int(v2TexWidth.y);
	result.y = float(iTemp) + 0.5;
	result.x = fIndex - v2TexWidth.y * float(iTemp) + 0.5;
	return result;
}
#endif

vec4 ComputeBCs(vec3 pos, vec4 v4VertexIndices)
{
	vec3 x1 = texture2DRect(texUnitVertices, ComputePointTCs(v4VertexIndices.x)).xyz;
	vec3 x2 = texture2DRect(texUnitVertices, ComputePointTCs(v4VertexIndices.y)).xyz;
	vec3 x3 = texture2DRect(texUnitVertices, ComputePointTCs(v4VertexIndices.z)).xyz;
	vec3 x4 = texture2DRect(texUnitVertices, ComputePointTCs(v4VertexIndices.w)).xyz;

	vec3 v41 = x4-x1;
	vec3 v34 = x3-x4;
	vec3 v12 = x1-x2;
	vec3 v23 = x2-x3;
	vec3 v21 = x2-x1;
	vec3 v31 = x3-x1;
	
	float fDet = dot(v21, cross(v31, v41));
	vec3 vp1ByDet = (pos-x1) / fDet;
	
	vec4 weights;
	weights.y = dot(cross(v34, v41), vp1ByDet);
	weights.z = dot(cross(v12, v41), vp1ByDet);
	weights.w = dot(cross(v12, v23), vp1ByDet);
	weights.x = 1.0 - dot(vec3(1.0), weights.yzw);

	return weights;
}

float TetWalk(float fStartCell, vec3 v3Pos, out vec4 v4BCs, out vec4 v4VertexIndices)
{
	int iCount = 20;
	while (fStartCell > 0)
	{
		vec2 v2CellTCs = ComputeCellTCs(fStartCell);
		v4VertexIndices = texture2DRect(texUnitCells, v2CellTCs);
		v4BCs = ComputeBCs(v3Pos, v4VertexIndices);

		if (any(lessThan(v4BCs, vec4(-0.001))))
		{
			vec4 v4Neighbors = texture2DRect(texUnitCellNeighbors, v2CellTCs);
			float fMinBC = 0.0;
			for (int i=0; i<4; ++i)
			{
				if (v4BCs[i] < fMinBC)
				{
					fMinBC = v4BCs[i];
					fStartCell = v4Neighbors[i];
				}
			}
		}
		else
			break;

		if (iCount < 0)
		{
			fStartCell = -1;
			break;
		}
		--iCount;
	}
	return fStartCell;
}

void main(void)
{
	vec4 current = texture2D(texUnitPosition, gl_TexCoord[0].xy);
	float fCell = texture2D(texUnitAttrib, gl_TexCoord[0].xy).x;

	vec4 v4BCs;
	vec4 v4VertexIndices;

	fCell = TetWalk(fCell, current.xyz, v4BCs, v4VertexIndices);

	if (fCell < 0)
	{
		gl_FragData[0] = current;
		gl_FragData[1].x = fCell;
	}
	else
	{
		vec3 velocity = v4BCs.x * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.x)).xyz
			+ v4BCs.y * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.y)).xyz
			+ v4BCs.z * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.z)).xyz
			+ v4BCs.w * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.w)).xyz;

		current.xyz += fDeltaT * velocity;

		gl_FragData[0].xyz = current.xyz;
		gl_FragData[1].x = TetWalk(fCell, current.xyz, v4BCs, v4VertexIndices);

		gl_FragData[0].w = v4BCs.x * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.x)).w
			+ v4BCs.y * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.y)).w
			+ v4BCs.z * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.z)).w
			+ v4BCs.w * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.w)).w;
	}
}
