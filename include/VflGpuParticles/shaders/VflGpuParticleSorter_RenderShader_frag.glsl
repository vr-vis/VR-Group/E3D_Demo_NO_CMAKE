#version 120
#extension GL_ARB_texture_rectangle : enable

uniform sampler2DRect samp_tex;

void main()
{
	vec3 color = texture2DRect( samp_tex, gl_TexCoord[0].st ).rgb;
	color = vec3( color.b ) + vec3( 0.0f, 0.0f, 0.2f );
	gl_FragColor = vec4( color, 1.0f );
}