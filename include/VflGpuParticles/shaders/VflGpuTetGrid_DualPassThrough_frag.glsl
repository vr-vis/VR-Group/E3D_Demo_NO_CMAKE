#version 120

uniform sampler2D samp_texture1;
uniform sampler2D samp_texture2;

void main()
{
	// Using gl_TexCoord with index 0 for both is correct
	gl_FragData[0] = texture2D( samp_texture1, gl_TexCoord[0].st );
	gl_FragData[1] = texture2D( samp_texture2, gl_TexCoord[0].st );
}