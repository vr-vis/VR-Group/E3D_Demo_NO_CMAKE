/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflCpuParticleRenderer.h      RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflCpuParticleRenderer                                            */
/*                                                                            */
/*============================================================================*/
// $Id$


#ifndef __VFLCPUPARTICLERENDERER_H
#define __VFLCPUPARTICLERENDERER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "VflGpuParticlesConfig.h"

#include "VflGpuParticleRendererInterface.h"

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI VflCpuParticleRenderer
:	public IVflGpuParticleRenderer
{
public:
	VflCpuParticleRenderer(VflVisGpuParticles *pParent);
	virtual ~VflCpuParticleRenderer();

	virtual void Update();
	virtual void DrawOpaque();

	virtual std::string GetDrawModeString(int iIndex) const;
	virtual int GetDrawModeCount() const;

	virtual int GetProcessingUnit() const;
	virtual int GetTracerType() const;

	enum DRAW_MODE
	{
		DM_POINTS = IVflGpuParticleRenderer::DM_LAST,
		DM_LAST
	};

	virtual bool IsValid() const;
	virtual std::string GetReflectionableType() const;

protected:
	VflCpuParticleRenderer();
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	double	m_dPreparationTimeStamp;
	GLuint	m_iParticleVBO;
	int		m_iParticleCount;

	bool	m_bValid;
};


#endif // __VFLCPUPARTICLERENDERER_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

