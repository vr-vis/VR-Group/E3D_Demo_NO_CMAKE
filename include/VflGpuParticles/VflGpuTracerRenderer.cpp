/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuTracerRenderer.cpp     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflGpuTracerRenderer.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflLookupTexture.h>

#include <VistaBase/VistaStreamUtils.h>
#include <VistaAspects/VistaAspectsUtils.h>

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaFramebufferObj.h>
#include <VistaOGLExt/VistaRenderToVertexArray.h>
#include <VistaOGLExt/VistaOGLUtils.h>
#include <VistaOGLExt/VistaShaderRegistry.h>
#include <VistaOGLExt/VistaBufferObject.h>
#include <VistaOGLExt/VistaVertexArrayObject.h>
#include <VistaOGLExt/Rendering/VistaParticleTraceRenderingCore.h>
#include <VistaOGLExt/Rendering/VistaParticleRenderingProperties.h>

#include <vtkLookupTable.h>
#include <set>

#include <VistaKernel/GraphicsManager/VistaOpenGLDebug.h>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflGpuTracerRendererReflType("VflGpuTracerRenderer");

static IVistaPropertyGetFunctor *s_aCVflGpuTracerRendererGetFunctors[] =
{
	NULL
};

static IVistaPropertySetFunctor *s_aCVflGpuTracerRendererSetFunctors[] =
{
	NULL
};

#define BUFFER_OFFSET(i) ((char *)NULL + (i))
#define DEFAULT_TEXTURE_RESOLUTION 64

#define SHADER_TANGENTS_VS	"VflGpuTracerRenderer_Tangents_VS.glsl"
#define SHADER_TANGENTS_FS	"VflGpuTracerRenderer_Tangents_FS.glsl"

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflGpuTracerRenderer::VflGpuTracerRenderer(VflVisGpuParticles *pParent)
	:	IVflGpuParticleRenderer(pParent)
	,	m_pCore( new VistaParticleTraceRenderingCore() )
	,	m_dPreparationTimeStamp(0)
	,	m_iTracerCount(0)
	,	m_iTracerLength(0)
	,	m_pTargetTexture(NULL)
	,	m_pFBO(NULL)
	,	m_pBodyVAO(NULL)
	,	m_pBodyPositions(NULL)
	,	m_pBodyTangents(NULL)
	,	m_pBodyOffsetsVBO(NULL)
	,	m_pBodyIBO(NULL)
	,	m_pCapVAO(NULL)
	,	m_pCapPositions(NULL)
	,	m_pCapNormals(NULL)
	,	m_pCapIBO(NULL)
	,	m_pCapOffsetsVBO(NULL)
	,	m_pPassThrough(NULL)
	,	m_pComputeTangents(NULL)
	,	m_bValid(false)
{
	m_iDrawMode = m_pCore->GetProperties()->GetDrawMode();

	m_pCore->SetMaxLuminanceFactor( 0 );

	m_pParent->GetParticleData()->GetSize(m_iTracerCount, m_iTracerLength);

	m_bValid = CreateGpuResources();
}

VflGpuTracerRenderer::~VflGpuTracerRenderer()
{
	DestroyGpuResources();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
VistaParticleRenderingProperties* 
	VflGpuTracerRenderer::GetParticleRenderingProperties()
{
	return m_pCore->GetProperties();
}

void VflGpuTracerRenderer::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = 
		m_pParent->GetProperties();

	if (!pProperties->GetVisible() || !IsValid())
		return;

	// update particle radius
	m_pCore->SetParticleRadius( pProperties->GetRadius() );
	m_pCore->SetLineWidth( pProperties->GetLineWidth() );

	if( pProperties->GetUseLookupTable() )
	{
		float fMin, fMax;
		pProperties->GetLookupTable()->GetTableRange( fMin, fMax );
		m_pCore->SetLookupTexture( pProperties->GetLookupTexture()->GetLookupTexture() );
		m_pCore->SetLookupRange( fMin, fMax );	
	}
	else
	{
		VistaColor oColor = pProperties->GetColor();
		m_pCore->SetParticleColor( oColor );
		m_pCore->SetLookupTexture( NULL );
	}

	if( pProperties->GetDecreaseLuminance() )
		m_pCore->SetLuminanceReductionFactor( 1.0f );
	else
		m_pCore->SetLuminanceReductionFactor( 0.0f );

	// retrieve viewer position and light direction
	VflRenderNode *pRenderNode = m_pParent->GetRenderNode();
	m_pCore->SetViewerPosition(  pRenderNode->GetLocalViewPosition()   );
	m_pCore->SetLightDirection( -pRenderNode->GetLocalLightDirection() );
	m_pCore->GetProperties()->SetDrawMode( m_iDrawMode );

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	pParticles->SynchToGpu();

	if( pParticles->GetGpuDataChangeTimeStamp() > m_dPreparationTimeStamp )
	{
		int w, h;
		pParticles->GetSize(w, h);
		if( m_iTracerCount!=w || m_iTracerLength!=h )
		{
			m_iTracerCount  = w;
			m_iTracerLength = h;
			UpdateGpuResources();
			if (!IsValid())
				return;
		}
		PrepareRendering();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflGpuTracerRenderer::DrawOpaque()
{
	if( !m_pParent->GetProperties()->GetVisible() )
		return;

	if( VistaParticleRenderingProperties::DM_SMOKE == m_iDrawMode )
		return;

	m_pCore->Draw();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflGpuTracerRenderer::DrawTransparent()
{ 
	if( !m_pParent->GetProperties()->GetVisible() )
		return;

	if( VistaParticleRenderingProperties::DM_SMOKE != m_iDrawMode )
		return;

	m_pCore->Draw();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDrawMode*                                            */
/*                                                                            */
/*============================================================================*/
std::string VflGpuTracerRenderer::GetDrawModeString(int iIndex) const
{
	switch (iIndex)
	{
	case VistaParticleRenderingProperties::DM_SIMPLE:
		return std::string("DM_SIMPLE");
	case VistaParticleRenderingProperties::DM_SMOKE:
		return std::string("DM_SIMPLE");
	case VistaParticleRenderingProperties::DM_BILLBOARDS:
		return std::string("DM_BILLBOARDS");
	case VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS:
		return std::string("DM_BUMPED_BILLBOARDS");
	case VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS_DEPTH_REPLACE:
		return std::string("DM_BUMPED_BILLBOARDS_DEPTH_REPLACE");
	}
	return IVflGpuParticleRenderer::GetDrawModeString(iIndex);
}

int VflGpuTracerRenderer::GetDrawModeCount() const
{
	return VistaParticleRenderingProperties::DM_LAST;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProcessingUnit                                           */
/*                                                                            */
/*============================================================================*/
int VflGpuTracerRenderer::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_GPU;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerType                                               */
/*                                                                            */
/*============================================================================*/
int VflGpuTracerRenderer::GetTracerType() const
{
	return VflVisGpuParticles::TT_TRACERS;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflGpuTracerRenderer::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflGpuTracerRenderer::GetReflectionableType() const
{
	return s_strCVflGpuTracerRendererReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateGpuResources                                          */
/*                                                                            */
/*============================================================================*/
bool VflGpuTracerRenderer::CreateGpuResources()
{
	// TODO: check for required OpenGL extensions...
	vstr::outi() << "[VflGpuTracerRenderer] - creating GPU resources..." << endl;
	vstr::IndentObject oIndent;

	// create texture to render particle data into
	// TODO: check, whether a renderbuffer might be sufficient, here...
	vstr::debugi() << " - creating render target texture..." << endl;

	float fTemp(0);

	m_pTargetTexture = new VistaTexture(GL_TEXTURE_2D);
	m_pTargetTexture->Bind();
	glTexParameteri(m_pTargetTexture->GetTarget(), GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(m_pTargetTexture->GetTarget(), GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(m_pTargetTexture->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(m_pTargetTexture->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(m_pTargetTexture->GetTarget(), 0, GL_ALPHA, 
		1, 1, 0, GL_ALPHA, GL_FLOAT, &fTemp);

	// init shaders ----------------------------------------------------------
	vstr::debugi() << " - creating pass-through shader..." << endl;

	/*
	string strTemp = "!!ARBfp1.0\n";
	strTemp += "TEX result.color, fragment.texcoord[0], texture[0], 2D;\n";
	strTemp += "END\n";

	VistaARBProgram *pProgram = new VistaARBProgram(GL_FRAGMENT_PROGRAM_ARB);
	if (!pProgram->InitFromProgramString(strTemp))
	*/
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();
	std::string strShader =
		rShaderReg.RetrieveShader( "VflGpuParticleData_PassThrough_frag.glsl" );

	VistaGLSLShader* pProgram = new VistaGLSLShader();

	if(    strShader.empty()
		|| !pProgram->InitShaderFromString( GL_FRAGMENT_SHADER, strShader )
		|| !pProgram->Link() )
	{
		vstr::errp() << "[VflGpuTracerRenderer] unable to init pass-through shader..." << endl;
		delete pProgram;
		return false;
	}
	m_pPassThrough = pProgram;
	m_pPassThrough->Bind();
	m_pPassThrough->SetUniform( "samp_texture", 0 );
	m_pPassThrough->Release();

	vstr::debugi() << " - initializing tangent shaders..." << endl;

	VistaGLSLShader *pShader = new VistaGLSLShader;
	string strVertexShader, strFragmentShader;

	strVertexShader   = rShaderReg.RetrieveShader(SHADER_TANGENTS_VS);
	strFragmentShader = rShaderReg.RetrieveShader(SHADER_TANGENTS_FS);
	if(strVertexShader.empty() || strFragmentShader.empty())
	{
		vstr::errp() << "[VflGpuTracerRenderer] can't find required shader files:" << endl;
		vstr::IndentObject oIndent;
		if(strVertexShader.empty())
			vstr::erri() << "Can't fined " << SHADER_TANGENTS_VS << endl;
		if(strFragmentShader.empty())
			vstr::erri() << "Can't fined " << SHADER_TANGENTS_FS << endl;
		return false;
	}
	if (!pShader->InitFromStrings(strVertexShader, strFragmentShader))
	{
		vstr::errp() << "[VflGpuTracerRenderer] unable to init tangent shader." << endl;
		return false;
	}
	if (!pShader->Link())
	{
		vstr::errp() << "[VflGpuTracerRenderer] unable to link tangent shader." << endl;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->Release();
	m_pComputeTangents = pShader;

	// create framebuffer object ---------------------------------------------
	vstr::debugi() << " - creating framebuffer object..." << endl;

	m_pFBO = new VistaFramebufferObj();
	m_pFBO->Bind();
	m_pFBO->Attach(m_pTargetTexture, GL_COLOR_ATTACHMENT0_EXT);
	m_pFBO->Release();

	// creating body index buffer
	vstr::debugi() << " - creating body index buffer..." << endl;

	m_pBodyIBO = new VistaBufferObject();
	m_pBodyIBO->BindAsIndexBuffer();
	m_pBodyIBO->BufferData( 1, NULL, GL_STATIC_DRAW );
	m_pBodyIBO->Release();

	// creating body offset buffer
	vstr::debugi() << " - creating body offset buffer..." << endl;
	m_pBodyOffsetsVBO = new VistaBufferObject();
	m_pBodyOffsetsVBO->BindAsVertexDataBuffer();
	m_pBodyOffsetsVBO->BufferData( 1, NULL, GL_STATIC_DRAW );
	m_pBodyOffsetsVBO->Release();

	// creating cap index buffer
	vstr::debugi() <<  " - creating cap index buffer..." << endl;
	m_pCapIBO = new VistaBufferObject();
	m_pCapIBO->BindAsIndexBuffer();
	m_pCapIBO->BufferData( 1, NULL, GL_STATIC_DRAW );
	m_pCapIBO->Release();

	// creating cap offset buffer
	vstr::debugi() << " - creating cap offset buffer..." << endl;
	m_pCapOffsetsVBO = new VistaBufferObject();
	m_pCapOffsetsVBO->BindAsVertexDataBuffer();
	m_pCapOffsetsVBO->BufferData( 1, NULL, GL_STATIC_DRAW );
	m_pCapOffsetsVBO->Release();


	// creating body index buffer
	vstr::debugi() << " - creating body vertex array object..." << endl;
	m_pBodyVAO = new VistaVertexArrayObject();
	m_pBodyVAO->Bind();

	m_pBodyVAO->EnableAttributeArray( 0 ); // position
	m_pBodyVAO->EnableAttributeArray( 1 ); // tangent
	m_pBodyVAO->EnableAttributeArray( 2 ); // scalar value
	m_pBodyVAO->EnableAttributeArray( 3 ); // luminance
	m_pBodyVAO->EnableAttributeArray( 4 ); // vertex offsets

	m_pBodyVAO->SpecifyAttributeArrayFloat( 4 , 2, GL_FLOAT, false, 0, 0, m_pBodyOffsetsVBO );
	m_pBodyVAO->SpecifyIndexBufferObject( m_pBodyIBO );
	m_pBodyVAO->Release();

	// creating body index buffer
	vstr::debugi() << " - creating cap vertex array object..." << endl;
	m_pCapVAO  = new VistaVertexArrayObject();
	m_pCapVAO->Bind();

	m_pCapVAO->EnableAttributeArray( 0 ); // position
	m_pCapVAO->EnableAttributeArray( 1 ); // tangent
	m_pCapVAO->EnableAttributeArray( 2 ); // scalar value
	m_pCapVAO->EnableAttributeArray( 3 ); // luminance
	m_pCapVAO->EnableAttributeArray( 4 ); // vertex offsets

	m_pCapVAO->SpecifyAttributeArrayFloat( 4 , 2, GL_FLOAT, false, 0, 0, m_pCapOffsetsVBO );
	m_pCapVAO->SpecifyIndexBufferObject( m_pCapIBO );
	m_pCapVAO->Release();

	// initialize rendering core
	vstr::debugi() << " - initializing rendering core..." << endl;
	m_pCore->SetBodyVBO( m_pBodyVAO );
	m_pCore->SetCapVBO( m_pCapVAO );
	if( !m_pCore->Init() )
		return false;


	// use UpdateGpuResources for creation of semi-dynamic objects
	// and allocation of resources...
	m_bValid = true;
	UpdateGpuResources();
	return IsValid();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateGpuResources                                          */
/*                                                                            */
/*============================================================================*/
void VflGpuTracerRenderer::UpdateGpuResources()
{
#ifdef DEBUG
	cout << " [VflGpuTracerRenderer] - updating GPU resources..." << endl;
#endif
	delete m_pBodyPositions;
	m_pBodyPositions = NULL;
	delete m_pBodyTangents;
	m_pBodyTangents = NULL;
	delete m_pCapPositions;
	m_pCapPositions = NULL;
	delete m_pCapNormals;
	m_pCapNormals = NULL;

#ifdef DEBUG
	cout << " [VflGpuTracerRenderer] - creating render-to-vertex-array objects..." << endl;
#endif
	m_pBodyPositions = new VistaRenderToVertexArray(2*m_iTracerCount*(m_iTracerLength+2), 4, GL_FLOAT);
	m_pBodyTangents  = new VistaRenderToVertexArray(2*m_iTracerCount*(m_iTracerLength+2), 4, GL_FLOAT);
	m_pCapPositions  = new VistaRenderToVertexArray(2*m_iTracerCount*4, 4, GL_FLOAT);
	m_pCapNormals    = new VistaRenderToVertexArray(2*m_iTracerCount*4, 4, GL_FLOAT);

	m_pBodyVAO->Bind();
	m_pBodyVAO->SpecifyAttributeArrayFloat( 0, 3, false, 4*sizeof(float), 0*sizeof(float), m_pBodyPositions );
	m_pBodyVAO->SpecifyAttributeArrayFloat( 2, 1, false, 4*sizeof(float), 3*sizeof(float), m_pBodyPositions );
	m_pBodyVAO->SpecifyAttributeArrayFloat( 1, 3, false, 4*sizeof(float), 0*sizeof(float), m_pBodyTangents );
	m_pBodyVAO->SpecifyAttributeArrayFloat( 3, 1, false, 4*sizeof(float), 3*sizeof(float), m_pBodyTangents );
	m_pBodyVAO->Release();

	m_pCapVAO->Bind();
	m_pCapVAO->SpecifyAttributeArrayFloat( 0, 3, false, 4*sizeof(float), 0*sizeof(float), m_pCapPositions );
	m_pCapVAO->SpecifyAttributeArrayFloat( 2, 1, false, 4*sizeof(float), 3*sizeof(float), m_pCapPositions );
	m_pCapVAO->SpecifyAttributeArrayFloat( 1, 3, false, 4*sizeof(float), 0*sizeof(float), m_pCapNormals );
	m_pCapVAO->SpecifyAttributeArrayFloat( 3, 1, false, 4*sizeof(float), 3*sizeof(float), m_pCapNormals );
	m_pCapVAO->Release();

#ifdef DEBUG
	cout << " [VflGpuTracerRenderer] - initializing render target texture..." << endl;
#endif
	float *pData = new float[2*m_iTracerCount*(m_iTracerLength+2)*4];
	memset(pData, 0, 2*m_iTracerCount*(m_iTracerLength+2)* 4*sizeof(float));

	m_pTargetTexture->Bind();
	glTexImage2D(
		m_pTargetTexture->GetTarget(), 0, m_pBodyPositions->GetInternalFormat(),
		2*m_iTracerCount, m_iTracerLength+2, 0, m_pBodyPositions->GetFormat(), 
		m_pBodyPositions->GetType(), pData );
	delete [] pData;

#ifdef DEBUG
	cout << " [VflGpuTracerRenderer] - checking framebuffer object..." << endl;
#endif
	m_pFBO->Bind();
	m_pFBO->Attach(m_pTargetTexture, GL_COLOR_ATTACHMENT0_EXT);
	m_pFBO->Release();
	if (!m_pFBO->IsValid())
	{
		cout << " ***" << endl;
		cout << " [VflGpuTracerRenderer] - ERROR - invalid FBO..." << endl;
		cout << "                                    FBO status: " << m_pFBO->GetStatusAsString() << endl;
		cout << " *** prepare for emergency landing (i.e. a crash?)!" << endl;
		cout << " ***" << endl;
		m_bValid = false;
		return;
	}

#ifdef DEBUG
	cout << " [VflGpuTracerRenderer] - initializing body index and offset data..." << endl;
#endif
	int iIndexArraySize  = 2*m_iTracerCount*(m_iTracerLength+2) + m_iTracerCount;
	int iOffsetArraySize = 2*m_iTracerCount*(m_iTracerLength+2)*2;
	GLuint* pIndexArray  = new GLuint[ iIndexArraySize ];
	float*  pOffsetArray = new float[ iOffsetArraySize ];
	GLuint *pPos = pIndexArray;

	for (int i=0; i<m_iTracerCount; ++i)
	{
		for (int j=0; j<m_iTracerLength+2; ++j)
		{
			*(pPos++) = 2*i + (j*2*m_iTracerCount);
			*(pPos++) = 2*i + (j*2*m_iTracerCount) + 1;
			//cout << 2*i + (j*2*m_iVBOWidth) << " " << 2*i + (j*2*m_iVBOWidth) + 1;

			int iOffsetArrayIndex = 2 * ((j*2*m_iTracerCount) + 2*i);
			float fDepthOffset = (j>0&&j<=m_iTracerLength ? 0 : 1.0f);
			pOffsetArray[iOffsetArrayIndex+0] = fDepthOffset;
			pOffsetArray[iOffsetArrayIndex+1] = 1.0f;
			pOffsetArray[iOffsetArrayIndex+2] = fDepthOffset;
			pOffsetArray[iOffsetArrayIndex+3] = -1.0f;

			//cout << " - " << iOffsetArrayIndex << " " << fDepthOffset << endl;
		}
		*(pPos++) = m_pCore->GetPrimitiveRestartIndex();
	}

	m_pBodyIBO->BindAsIndexBuffer();
	m_pBodyIBO->BufferData( iIndexArraySize*sizeof(GLuint), pIndexArray, GL_STATIC_DRAW );
	m_pBodyIBO->Release();

	m_pBodyOffsetsVBO->BindAsVertexDataBuffer();
	m_pBodyOffsetsVBO->BufferData( iOffsetArraySize*sizeof(float), pOffsetArray, GL_STATIC_DRAW );
	m_pBodyOffsetsVBO->Release();

	delete [] pIndexArray;
	pIndexArray = NULL;
	delete [] pOffsetArray;
	pOffsetArray = NULL;

#ifdef DEBUG
	cout << " [VflGpuTracerRenderer] - initializing cap index and offset data..." << endl;
#endif
	iIndexArraySize  = 2*4*m_iTracerCount;
	iOffsetArraySize = 2*8*m_iTracerCount;
	pIndexArray  = new GLuint[iIndexArraySize];
	pOffsetArray = new float[iOffsetArraySize];
	pPos = pIndexArray;

	for( int i=0; i<m_iTracerCount; ++i )
	{
		for( int j=0; j<2; ++j )
		{
			int iIndex1 = (2*i + 0) + (2*j + 0)*2*m_iTracerCount;
			int iIndex2 = (2*i + 0) + (2*j + 1)*2*m_iTracerCount;
			int iIndex3 = (2*i + 1) + (2*j + 1)*2*m_iTracerCount;
			int iIndex4 = (2*i + 1) + (2*j + 0)*2*m_iTracerCount;

			*(pPos++) = iIndex1;
			*(pPos++) = iIndex2;
			*(pPos++) = iIndex3;
			*(pPos++) = iIndex4;

			pOffsetArray[ 2*iIndex1 + 0 ] =  1-2*j;
			pOffsetArray[ 2*iIndex1 + 1 ] =  1.0f;

			pOffsetArray[ 2*iIndex2 + 0 ] =  2*j-1;
			pOffsetArray[ 2*iIndex2 + 1 ] =  1.0f;

			pOffsetArray[ 2*iIndex3 + 0 ] =  2*j-1;
			pOffsetArray[ 2*iIndex3 + 1 ] = -1.0f;

			pOffsetArray[ 2*iIndex4 + 0 ] =  1-2*j;
			pOffsetArray[ 2*iIndex4 + 1 ] = -1.0f;
		}
	}


	m_pCapIBO->BindAsIndexBuffer();
	m_pCapIBO->BufferData( iIndexArraySize*sizeof(GLuint), pIndexArray, GL_STATIC_DRAW );
	m_pCapIBO->Release();

	m_pCapOffsetsVBO->BindAsVertexDataBuffer();
	m_pCapOffsetsVBO->BufferData( iOffsetArraySize*sizeof(float), pOffsetArray, GL_STATIC_DRAW );
	m_pCapOffsetsVBO->Release();

	delete [] pIndexArray;
	delete [] pOffsetArray;

#ifdef DEBUG
	cout << " [VflGpuTracerRenderer] - updating tangent shader parameterization..." << endl;
#endif
	float fOffset = 1.0f/m_iTracerLength;

	int uLoc = m_pComputeTangents->GetUniformLocation("v2Offsets");
	m_pComputeTangents->Bind();
	m_pComputeTangents->SetUniform(uLoc, -fOffset, fOffset);
	m_pComputeTangents->Release();


#ifdef DEBUG
	cout << " [VflGpuTracerRenderer] - GPU resource update finished..." << endl;
#endif
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DestroyGpuResources                                         */
/*                                                                            */
/*============================================================================*/
void VflGpuTracerRenderer::DestroyGpuResources()
{
	// destroy shaders...
	delete m_pPassThrough;
	m_pPassThrough = NULL;

	delete m_pComputeTangents;
	m_pComputeTangents = NULL;

	// destroy VAOs 
	delete m_pBodyVAO;
	delete m_pCapVAO;

	m_pBodyVAO = NULL;
	m_pCapVAO  = NULL;

	// destroy IBOs 
	delete m_pBodyIBO;
	delete m_pCapIBO;

	m_pBodyIBO = NULL;
	m_pCapIBO  = NULL;

	// destroy VBOs 
	delete m_pBodyOffsetsVBO;
	delete m_pCapOffsetsVBO;

	m_pBodyOffsetsVBO = NULL;
	m_pCapOffsetsVBO  = NULL;

	// destroy FBO and target texture
	delete m_pFBO;
	m_pFBO = NULL;

	delete m_pTargetTexture;
	m_pTargetTexture = NULL;

	// destroy render to VA objects
	delete m_pCapNormals;
	m_pCapNormals = NULL;
	delete m_pCapPositions;
	m_pCapPositions = NULL;

	delete m_pBodyPositions;
	m_pBodyPositions = NULL;

	delete m_pBodyTangents;
	m_pBodyTangents = NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   PrepareRendering                                            */
/*                                                                            */
/*============================================================================*/
void VflGpuTracerRenderer::PrepareRendering()
{
	if( !m_pParent->GetVisible() )
		return;

	m_pParent->SaveOGLState();

	glViewport(0, 0, 2*m_iTracerCount, m_iTracerLength+2);
	glLoadIdentity();
	glOrtho( 0, 2*m_iTracerCount, 0, m_iTracerLength+2, -1, 1 );

	// set render target
	m_pFBO->Bind();

	UpdatePositions();
	UpdateTangents();

	m_pFBO->Release();

	m_pParent->RestoreOGLState();

	int iActiveTracers = m_pParent->GetParticleData()->GetActiveTracers();

	m_pCore->SetNumBodyIndices( (2*(m_iTracerLength+2) + 1 )*iActiveTracers );
	m_pCore->SetCapCount( 2*iActiveTracers );


	m_dPreparationTimeStamp = m_pParent->GetParticleData()->GetGpuDataChangeTimeStamp();

	VistaOGLUtils::CheckForOGLError(__FILE__,__LINE__);
}

void VflGpuTracerRenderer::UpdatePositions()
{	
	// bind population texture
	m_pParent->GetParticleData()->GetCurrentTexture()->Bind();

	int iHeadLineCount = m_pParent->GetParticleData()->GetCurrentLine()+1;
	int iTexturewidth  = 2*m_iTracerCount;

	float fNormFactor = 1.0f/(m_iTracerLength+2);

	// particle positions -----------------------------------------
	m_pPassThrough->Bind();

	// "smear" particle data across future vertex array
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2f(0, m_iTracerLength-iHeadLineCount+1);
	glTexCoord2f(1, 0);
	glVertex2f(iTexturewidth, (m_iTracerLength-iHeadLineCount+1));
	glTexCoord2f(1, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(iTexturewidth, (m_iTracerLength+1));
	glTexCoord2f(0, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(0, (m_iTracerLength+1));

	if (iHeadLineCount < m_iTracerLength)
	{
		glTexCoord2f(0, iHeadLineCount/float(m_iTracerLength));
		glVertex2f(0, 1.0f);
		glTexCoord2f(1, iHeadLineCount/float(m_iTracerLength));
		glVertex2f(iTexturewidth, 1.0f);
		glTexCoord2f(1, 1);
		glVertex2f(iTexturewidth, (m_iTracerLength-iHeadLineCount+1));
		glTexCoord2f(0, 1);
		glVertex2f(0, (m_iTracerLength-iHeadLineCount+1));
	}

	// don't forget the extra data at the start and end of the tubelets
	glTexCoord2f(0, (iHeadLineCount-1)/float(m_iTracerLength));
	glVertex2f(0, (m_iTracerLength+1));
	glTexCoord2f(1, (iHeadLineCount-1)/float(m_iTracerLength));
	glVertex2f(iTexturewidth, (m_iTracerLength+1));
	glTexCoord2f(1, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(iTexturewidth, m_iTracerLength+2);
	glTexCoord2f(0, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(0, m_iTracerLength+2);

	iHeadLineCount = iHeadLineCount%m_iTracerLength;
	glTexCoord2f(0, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(0, 0);
	glTexCoord2f(1, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(iTexturewidth, 0);
	glTexCoord2f(1, (iHeadLineCount+1)/float(m_iTracerLength));
	glVertex2f(iTexturewidth, 1);
	glTexCoord2f(0, (iHeadLineCount+1)/float(m_iTracerLength));
	glVertex2f(0, 1);

	glEnd();

	m_pPassThrough->Release();

	// copy data into vertex arrays
	m_pBodyPositions->Read(GL_COLOR_ATTACHMENT0_EXT, 
		0, 0, iTexturewidth, m_iTracerLength+2, 0 );

	// read positional data for caps
	int iOffset = 2*iTexturewidth * 4*sizeof(float);
	m_pCapPositions->Read(GL_COLOR_ATTACHMENT0_EXT, 0,               0, iTexturewidth, 2, 0 );
	m_pCapPositions->Read(GL_COLOR_ATTACHMENT0_EXT, 0, m_iTracerLength, iTexturewidth, 2, iOffset );
}

void VflGpuTracerRenderer::UpdateTangents()
{
	m_pComputeTangents->Bind();

	int iHeadLineCount = m_pParent->GetParticleData()->GetCurrentLine()+1;
	int iTexturewidth  = 2*m_iTracerCount;
	// compute tangents (and normals)
	glBegin(GL_QUADS);
	glMultiTexCoord3f(GL_TEXTURE1, -1.0f, (iHeadLineCount-0.5f)/m_iTracerLength,
		1.0f/m_iTracerLength);
	glTexCoord2f(0, 0);
	glVertex2f(0, (m_iTracerLength-iHeadLineCount+1));
	glTexCoord2f(1, 0);
	glVertex2f(iTexturewidth, (m_iTracerLength-iHeadLineCount+1));
	glTexCoord2f(1, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(iTexturewidth, (m_iTracerLength+1));
	glTexCoord2f(0, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(0, (m_iTracerLength+1));

	if (iHeadLineCount < m_iTracerLength)
	{
		glMultiTexCoord3f(GL_TEXTURE1, (iHeadLineCount+0.5f)/m_iTracerLength, 2.0f,
			1.0f/m_iTracerLength);
		glTexCoord2f(0, iHeadLineCount/float(m_iTracerLength));
		glVertex2f(0, 1.0f);
		glTexCoord2f(1, iHeadLineCount/float(m_iTracerLength));
		glVertex2f(iTexturewidth, 1.0f);
		glTexCoord2f(1, 1);
		glVertex2f(iTexturewidth, (m_iTracerLength-iHeadLineCount+1));
		glTexCoord2f(0, 1);
		glVertex2f(0, (m_iTracerLength-iHeadLineCount+1));
	}

	// don't forget the extra data at the start...
	glMultiTexCoord3f(GL_TEXTURE1, -1.0f, (iHeadLineCount-0.5f)/m_iTracerLength, 
		1.0f/m_iTracerLength);
	glTexCoord2f(0, (iHeadLineCount-1)/float(m_iTracerLength));
	glVertex2f(0, (m_iTracerLength+1));
	glTexCoord2f(1, (iHeadLineCount-1)/float(m_iTracerLength));
	glVertex2f(iTexturewidth, (m_iTracerLength+1));
	glTexCoord2f(1, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(iTexturewidth, m_iTracerLength+2);
	glTexCoord2f(0, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(0, m_iTracerLength+2);

	// ...and end of the tubelets
	iHeadLineCount = iHeadLineCount%m_iTracerLength;
	glMultiTexCoord3f(GL_TEXTURE1, (iHeadLineCount+0.5f)/m_iTracerLength, 2.0f, 
		1.0f/m_iTracerLength);
	glTexCoord2f(0, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(0, 0);
	glTexCoord2f(1, iHeadLineCount/float(m_iTracerLength));
	glVertex2f(iTexturewidth, 0);
	glTexCoord2f(1, (iHeadLineCount+1)/float(m_iTracerLength));
	glVertex2f(iTexturewidth, 1.0f);
	glTexCoord2f(0, (iHeadLineCount+1)/float(m_iTracerLength));
	glVertex2f(0, 1.0f);

	glEnd();

	m_pComputeTangents->Release();

	// copy data into vertex arrays
	m_pBodyTangents->Read(GL_COLOR_ATTACHMENT0_EXT, 
		0, 0, iTexturewidth, m_iTracerLength+2, 0);

	// read positional data for caps
	int iOffset = 2*iTexturewidth * 4*sizeof(float);
	m_pCapNormals->Read(GL_COLOR_ATTACHMENT0_EXT, 0,               0, iTexturewidth, 2, 0 );
	m_pCapNormals->Read(GL_COLOR_ATTACHMENT0_EXT, 0, m_iTracerLength, iTexturewidth, 2, iOffset );
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflGpuTracerRenderer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleRenderer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflGpuTracerRendererReflType);
	return i+1;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
