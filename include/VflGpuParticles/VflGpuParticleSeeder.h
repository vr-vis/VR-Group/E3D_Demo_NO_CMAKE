/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflGpuParticleSeeder.h        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflGpuParticleSeeder                                              */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLGPUPARTICLESEEDER_H
#define __VFLGPUPARTICLESEEDER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflGpuParticlesConfig.h"
#include <VistaFlowLibAux/VfaSeeder.h>
#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisGpuParticles;
class IVfaPointGenerator;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
/**
 * VflGpuParticleSeeder is now adopted to a new concept. The seeder has no
 * "intelligence" any more. It has a methode called Seed which is triggered by
 * an external source. And a method to get and set the pointGenerator.
 * The pointGenerator gives the Seeder a an vector of points which ist given to
 * the GpuParticleTracer. Now you are independent from the pointsource because
 * the seeder does not carry about it. For example you can use it in combination
 * with an VflWidget. See VflPrototypes/WidgetSeeder for an example. It is on
 * your own to implement an pointGenerator.
 */
class VFLGPUPARTICLESAPI VflGpuParticleSeeder : public IVfaSeeder
{
public:
   	VflGpuParticleSeeder ( VflVisGpuParticles *pParent,
		IVfaPointGenerator *pGenerator );
	
	void Seed( void );

private:
	VflVisGpuParticles	*m_pParent;
};

#endif /* __VFLGPUPARTICLESEEDER_H */
