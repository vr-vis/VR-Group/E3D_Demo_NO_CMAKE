/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflGpuParticleSorter.h        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflGpuParticleSorter                                              */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLGPUPARTICLESORTER_H
#define __VFLGPUPARTICLESORTER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflGpuParticlesConfig.h"
#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaBase/VistaVectorMath.h>
#include "VflVisGpuParticles.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisGpuParticles;
class VflGpuParticleData;
class VistaTexture;
class VistaGLSLShader;
class VistaFramebufferObj;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI VflGpuParticleSorter : public VflObserver
{
public:
	VflGpuParticleSorter(VflVisGpuParticles *pParent);
	VflGpuParticleSorter(VflGpuParticleData *pParticleData,
						 VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties);
	virtual ~VflGpuParticleSorter();

	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	void Sort(const VistaVector3D &v3ViewPosition);

	bool GetValid() const;
	VistaTexture *GetMappingTexture() const;

	/**
	 * Display sorting data as overlay.
	 * Use this for debugging only...
	 */
	void Draw();

	static const std::string GetRendererId();
	static void GetShaderKeys(std::vector<std::string>& vecShaderKeys);

protected:
	bool CreateGpuResources();
	void DestroyGpuResources();
	bool ResetTextures();

	void InitDistances();
	void UpdateDistances();
	void SortStep();

	// TODO: These functions have been copied from VisGpuParticles. Where to
	// put them? (Code Duplication)
	void SaveOGLState();
	void RestoreOGLState();

	VflVisGpuParticles	*m_pParent;
	VflGpuParticleData *m_pParticles;
	VflVisGpuParticles::VflVisGpuParticlesProperties *m_pProperties;

	VistaTexture	*m_aTextures[2];
	VistaFramebufferObj	*m_pFBO;
	VistaGLSLShader	*m_pInitDistanceShader;
	VistaGLSLShader	*m_pUpdateDistanceShader;
	VistaGLSLShader	*m_pSortShader;

	int				m_aViewportState[4];
	
	int				m_iCurrentData;

	int				m_iWidth, m_iHeight;

	int				m_iActiveParticles;

	double			m_dDataTimeStamp;
	VistaVector3D	m_v3ViewPosition;
	
	bool m_bValid;
	bool m_bResetTextures;
	bool m_bNeedRestart;
	bool m_bInitDistance;
	bool m_bHighPrecision;

	// keep track of progress...
	int	m_iPass;
	int m_iStage;
	int m_iStepsLeft;
	int m_iTotalSteps;
	int m_iMaxSteps;

	// for overlay texture drawing
	VistaGLSLShader	*m_pRenderShader;
};

#endif // __VFLGPUPARTICLESORTER_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

