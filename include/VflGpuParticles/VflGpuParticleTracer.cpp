/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuParticleTracer.cpp     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflGpuParticleTracer.h"
#include "VflVisGpuParticles.h"
#include <VistaAspects/VistaAspectsUtils.h>

using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strVflGpuParticleTracerReflType("IVflGpuParticleTracer");

static IVistaPropertyGetFunctor *s_aIVflGpuParticleTracerGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aIVflGpuParticleTracerSetFunctors[] =
{
	NULL
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
IVflGpuParticleTracer::IVflGpuParticleTracer(VflVisGpuParticles *pParent)
: m_pParent(pParent)
{
	m_pParent->AddTracer(this);
}

IVflGpuParticleTracer::~IVflGpuParticleTracer()
{
	// remove self from parent's tracer set
	for (int i=0; i<m_pParent->GetTracerCount(); ++i)
	{
		IVflGpuParticleTracer *pTracer = m_pParent->GetTracer(i);
		if (pTracer == this)
		{
			m_pParent->RemoveTracer(i);
			break;
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string IVflGpuParticleTracer::GetReflectionableType() const
{
	return s_strVflGpuParticleTracerReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int IVflGpuParticleTracer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strVflGpuParticleTracerReflType);
	return i+1;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
