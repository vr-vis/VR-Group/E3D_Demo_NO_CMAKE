/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflVisGpuParticlesParams.cpp RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
// VistaFlowLib includes
#include "VflVisGpuParticlesParams.h"

#include <VistaFlowLib/Visualization/VflSharedVtkLookupTable.h>
#include <VistaFlowLib/Visualization/VflSharedLookupTexture.h>

#include <VistaVisExt/Tools/VveUtils.h>

#include <VistaAspects/VistaAspectsUtils.h>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflVisGpuParticlesParamsReflType("VflVisGpuParticlesParams");

static IVistaPropertyGetFunctor *s_aCVflVisGpuParticlesParamsGetFunctors[] =
{
	new TVistaPropertyGet<std::list<int>, VflVisGpuParticlesParams>
		("PARTICLE_TEXTURE_SIZE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetParticleTextureSizeAsList),
	new TVistaPropertyGet<int, VflVisGpuParticlesParams>
		("LOOK_AHEAD_COUNT", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetLookAheadCount),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("WRAP_AROUND", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetWrapAround),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("STREAMING_UPDATE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetStreamingUpdate),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("ACTIVE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetActive),
	new TVistaPropertyGet<std::string, VflVisGpuParticlesParams>
		("INTEGRATOR", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetIntegratorAsString),
	new TVistaPropertyGet<float, VflVisGpuParticlesParams>
		("DELTA_TIME", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetDeltaTime),
	new TVistaPropertyGet<float, VflVisGpuParticlesParams>
		("MAX_DELTA_TIME", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetMaxDeltaTime),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("USE_FIXED_DELTA_TIME", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetUseFixedDeltaTime),
	new TVistaPropertyGet<float, VflVisGpuParticlesParams>
		("FIXED_DELTA_TIME", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetFixedDeltaTime),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("ADAPTIVE_TIME_STEPS", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetAdaptiveTimeSteps),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("COMPUTE_SCALARS", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetComputeScalars),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("SINGLE_BUFFERED", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetSingleBuffered),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("FORCE_HARDWARE_INTERPOLATION", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetForceHardwareInterpolation),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("BENCHMARK_MODE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetBenchmarkMode),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("VISIBLE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetVisible),
	new TVistaPropertyConvertAndGet<VistaVector3D, const VistaVector3D &, VflVisGpuParticlesParams>
		("COLOR", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetColor,
		&VveConversionStuff::ConvertToString),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("USE_LOOKUP_TABLE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetUseLookupTable),
	new TVistaPropertyGet<float, VflVisGpuParticlesParams>
		("RADIUS", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetRadius),
	new TVistaPropertyGet<float, VflVisGpuParticlesParams>
		("POINT_SIZE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetPointSize),
	new TVistaPropertyGet<float, VflVisGpuParticlesParams>
		("LINE_WIDTH", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetLineWidth),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("DECREASE_LUMINANCE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetDecreaseLuminance),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("DRAW_OVERLAY", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetDrawOverlay),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("ALLOW_SORTING", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetAllowSorting),
	new TVistaPropertyGet<int, VflVisGpuParticlesParams>
		("MAX_SORTING_STEPS", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetMaxSortingSteps),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("HIGH_PRECISION_SORTING", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetHighPrecisionSorting),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("FORCE_DATA_PREPARATION", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetForceDataPreparation),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("RESPECT_PROCESSING_UNIT", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetRespectProcessingUnit),
	new TVistaPropertyGet<bool, VflVisGpuParticlesParams>
		("RESPECT_TRACER_TYPE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::GetRespectTracerType),
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflVisGpuParticlesParamsSetFunctors[] =
{
	new TVistaPropertySet<const list<int> &, list<int>, VflVisGpuParticlesParams>
		("PARTICLE_TEXTURE_SIZE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetParticleTextureSize),
	new TVistaPropertySet<int, int, VflVisGpuParticlesParams>
		("LOOK_AHEAD_COUNT", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetLookAheadCount),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("WRAP_AROUND", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetWrapAround),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("STREAMING_UPDATE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetStreamingUpdate),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("ACTIVE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetActive),
	new TVistaPropertySet<const std::string&, std::string, VflVisGpuParticlesParams>
		("INTEGRATOR", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetIntegrator),
	new TVistaPropertySet<float, float, VflVisGpuParticlesParams>
		("DELTA_TIME", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetDeltaTime),
	new TVistaPropertySet<float, float, VflVisGpuParticlesParams>
		("MAX_DELTA_TIME", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetMaxDeltaTime),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("USE_FIXED_DELTA_TIME", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetUseFixedDeltaTime),
	new TVistaPropertySet<float, float, VflVisGpuParticlesParams>
		("FIXED_DELTA_TIME", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetFixedDeltaTime),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("ADAPTIVE_TIME_STEPS", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetAdaptiveTimeSteps),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("COMPUTE_SCALARS", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetComputeScalars),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("SINGLE_BUFFERED", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetSingleBuffered),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("FORCE_HARDWARE_INTERPOLATION", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetForceHardwareInterpolation),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("BENCHMARK_MODE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetBenchmarkMode),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("VISIBLE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetVisible),
	new TVistaPropertySet<const VistaVector3D&, VistaVector3D, VflVisGpuParticlesParams>
		("COLOR", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetColor),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("USE_LOOKUP_TABLE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetUseLookupTable),
	new TVistaPropertySet<float, float, VflVisGpuParticlesParams>
		("RADIUS", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetRadius),
	new TVistaPropertySet<float, float, VflVisGpuParticlesParams>
		("POINT_SIZE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetPointSize),
	new TVistaPropertySet<float, float, VflVisGpuParticlesParams>
		("LINE_WIDTH", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetLineWidth),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("DECREASE_LUMINANCE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetDecreaseLuminance),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("DRAW_OVERLAY", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetDrawOverlay),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("ALLOW_SORTING", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetAllowSorting),
	new TVistaPropertySet<int, int, VflVisGpuParticlesParams>
		("MAX_SORTING_STEPS", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetMaxSortingSteps),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("HIGH_PRECISION_SORTING", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetHighPrecisionSorting),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("FORCE_DATA_PREPARATION", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetForceDataPreparation),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("RESPECT_PROCESSING_UNIT", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetRespectProcessingUnit),
	new TVistaPropertySet<bool, bool, VflVisGpuParticlesParams>
		("RESPECT_TRACER_TYPE", s_strCVflVisGpuParticlesParamsReflType,
		&VflVisGpuParticlesParams::SetRespectTracerType),
	NULL
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflVisGpuParticlesParams::VflVisGpuParticlesParams(VflSharedVtkLookupTable *pLut)
: m_pLookupTable(pLut),
  m_pLookupTexture(NULL),
  m_bManageLut(false),
  m_bManageTexture(false),
  m_iWidth(1),
  m_iHeight(1),
  m_iLookAheadCount(0),
  m_bWrapAround(false),
  m_bStreamingUpdate(false),
  m_bActive(true),
  m_iIntegrator(IR_INVALID),
  m_fDeltaTime(0),
  m_fMaxDeltaTime(numeric_limits<float>::max()),
  m_bUseFixedDeltaTime(false),
  m_fFixedDeltaTime(0),
  m_bAdaptiveTimeSteps(false),
  m_bComputeScalars(false),
  m_bSingleBuffered(false),
  m_bForceHardwareInterpolation(false),
  m_bBenchmarkMode(false),
  m_bVisible(true),
  m_bUseLookupTable(false),
  m_fRadius(1.0f),
  m_fPointSize(1.0f),
  m_fLineWidth(1.0f),
  m_bDecreaseLuminance(false),
  m_bDrawOverlay(false),
  m_bAllowSorting(false),
  m_iMaxSortingSteps(-1),
  m_bHighPrecisionSorting(false),
  m_bForceDataPreparation(false),
  m_bRespectProcessingUnit(false),
  m_bRespectTracerType(false)
{
	if (!m_pLookupTable)
	{
		m_pLookupTable = new VflSharedVtkLookupTable;
		m_bManageLut = true;
	}

	if (!m_pLookupTexture)
	{
		m_pLookupTexture = new VflSharedLookupTexture(m_pLookupTable);
		m_bManageTexture = true;
	}
}

VflVisGpuParticlesParams::~VflVisGpuParticlesParams()
{
	if (m_bManageTexture)
		delete m_pLookupTexture;
	m_pLookupTexture = NULL;

	if (m_bManageLut)
		delete m_pLookupTable;
	
	m_pLookupTable = NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTable                                              */
/*                                                                            */
/*============================================================================*/
VflSharedVtkLookupTable *VflVisGpuParticlesParams::GetLookupTable() const
{
	return m_pLookupTable;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTexture                                            */
/*                                                                            */
/*============================================================================*/
VflSharedLookupTexture *VflVisGpuParticlesParams::GetLookupTexture() const
{
	return m_pLookupTexture;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetParticleTextureSize                                  */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetParticleTextureSize(int iWidth, int iHeight)
{
	if (m_iWidth != iWidth || m_iHeight != iHeight)
	{
		m_iWidth = iWidth;
		m_iHeight = iHeight;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::SetParticleTextureSize(const std::list<int> &liSize)
{
	if (liSize.size() != 2)
		return false;

	list<int>::const_iterator it=liSize.begin();
	int iWidth = *(it++);
	int iHeight = *it;
	return SetParticleTextureSize(iWidth, iHeight);
}

bool VflVisGpuParticlesParams::GetParticleTextureSize(int &iWidth, int &iHeight) const
{
	iWidth = m_iWidth;
	iHeight = m_iHeight;
	return true;
}

std::list<int> VflVisGpuParticlesParams::GetParticleTextureSizeAsList() const
{
	list<int> liTemp;
	liTemp.push_back(m_iWidth);
	liTemp.push_back(m_iHeight);
	return liTemp;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLookAheadCount                                       */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetLookAheadCount(int iCount)
{
	if (iCount != m_iLookAheadCount)
	{
		m_iLookAheadCount = iCount;
		Notify();
		return true;
	}
	return false;
}

int VflVisGpuParticlesParams::GetLookAheadCount() const
{
	return m_iLookAheadCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetWrapAround                                           */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetWrapAround(bool bWrap)
{
	if (bWrap != m_bWrapAround)
	{
		m_bWrapAround = bWrap;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetWrapAround() const
{
	return m_bWrapAround;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetStreamingUpdate                                      */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetStreamingUpdate(bool bStream)
{
	if (bStream != m_bStreamingUpdate)
	{
		m_bStreamingUpdate = bStream;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetStreamingUpdate() const
{
	return m_bStreamingUpdate;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetActive                                               */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetActive(bool bActive)
{
	if (bActive != m_bActive)
	{
		m_bActive = bActive;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetActive() const
{
	return m_bActive;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIntegrator[AsString]                                 */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetIntegrator(int iIntegrator)
{
	if (iIntegrator != m_iIntegrator)
	{
		m_iIntegrator = iIntegrator;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::SetIntegrator(const std::string &strIntegrator)
{
	string strTemp = VistaAspectsConversionStuff::ConvertToLower(strIntegrator);
	int iSeedMode = IR_INVALID;
	if (strTemp == "euler")
		iSeedMode = IR_EULER;
	else if (strTemp == "rk3")
		iSeedMode = IR_RK3;
	else if (strTemp == "rk4")
		iSeedMode = IR_RK4;

	return SetIntegrator(iSeedMode);
}


int VflVisGpuParticlesParams::GetIntegrator() const
{
	return m_iIntegrator;
}

std::string VflVisGpuParticlesParams::GetIntegratorAsString() const
{
	switch (m_iIntegrator)
	{
	case IR_INVALID:
		return "INVALID";
	case IR_EULER:
		return "EULER";
	case IR_RK3:
		return "RK3";
	case IR_RK4:
		return "RK4";
	}

	return "UNKNOWN";
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDeltaTime                                            */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetDeltaTime(float fTime)
{
	if (fTime != m_fDeltaTime)
	{
		m_fDeltaTime = fTime;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticlesParams::GetDeltaTime() const
{
	return m_fDeltaTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMaxDeltaTime                                            */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetMaxDeltaTime(float fTime)
{
	if (fTime != m_fMaxDeltaTime)
	{
		m_fMaxDeltaTime = fTime;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticlesParams::GetMaxDeltaTime() const
{
	return m_fMaxDeltaTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetUseFixedDeltaTime                                    */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetUseFixedDeltaTime(bool bFixed)
{
	if (bFixed != m_bUseFixedDeltaTime)
	{
		m_bUseFixedDeltaTime = bFixed;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetUseFixedDeltaTime() const
{
	return m_bUseFixedDeltaTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFixedDeltaTime                                       */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetFixedDeltaTime(float fTime)
{
	if (fTime != m_fFixedDeltaTime)
	{
		m_fFixedDeltaTime = fTime;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticlesParams::GetFixedDeltaTime() const
{
	return m_fFixedDeltaTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetAdaptiveTimeSteps                                    */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetAdaptiveTimeSteps(bool bAdaptive)
{
	if (bAdaptive != m_bAdaptiveTimeSteps)
	{
		m_bAdaptiveTimeSteps = bAdaptive;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetAdaptiveTimeSteps() const
{
	return m_bAdaptiveTimeSteps;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetComputeScalars                                       */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetComputeScalars(bool bScalars)
{
	if (bScalars != m_bComputeScalars)
	{
		m_bComputeScalars = bScalars;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetComputeScalars() const
{
	return m_bComputeScalars;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetSingleBuffered                                       */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetSingleBuffered(bool bSingle)
{
	if (bSingle != m_bSingleBuffered)
	{
		m_bSingleBuffered = bSingle;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetSingleBuffered() const
{
	return m_bSingleBuffered;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetForceHardwareInterpolation                           */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetForceHardwareInterpolation(bool bForce)
{
	if (bForce != m_bForceHardwareInterpolation)
	{
		m_bForceHardwareInterpolation = bForce;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetForceHardwareInterpolation() const
{
	return m_bForceHardwareInterpolation;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetBenchmarkMode                                        */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetBenchmarkMode(bool bBench)
{
	if (bBench != m_bBenchmarkMode)
	{
		m_bBenchmarkMode = bBench;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetBenchmarkMode() const
{
	return m_bBenchmarkMode;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetVisible                                              */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetVisible(bool bVisible)
{
	if (bVisible != m_bVisible)
	{
		m_bVisible = bVisible;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetVisible() const
{
	return m_bVisible;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetColor                                                */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetColor(const VistaVector3D &v3Color)
{
	if (v3Color != m_v3Color)
	{
		m_v3Color = v3Color;
		Notify();
		return true;
	}
	return false;
}

VistaVector3D VflVisGpuParticlesParams::GetColor() const
{
	return m_v3Color;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetUseLookupTable                                       */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetUseLookupTable(bool bUseLut)
{
	if (bUseLut != m_bUseLookupTable)
	{
		m_bUseLookupTable = bUseLut;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetUseLookupTable() const
{
	return m_bUseLookupTable;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRadius                                               */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetRadius(float fRadius)
{
	if (fRadius != m_fRadius)
	{
		m_fRadius = fRadius;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticlesParams::GetRadius() const
{
	return m_fRadius;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetPointSize                                            */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetPointSize(float fPointSize)
{
	if (fPointSize != m_fPointSize)
	{
		m_fPointSize = fPointSize;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticlesParams::GetPointSize() const
{
	return m_fPointSize;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLineWidth                                            */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetLineWidth(float fLineWidth)
{
	if (fLineWidth != m_fLineWidth)
	{
		m_fLineWidth = fLineWidth;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticlesParams::GetLineWidth() const
{
	return m_fLineWidth;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDecreaseLuminance                                    */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetDecreaseLuminance(bool bDecrease)
{
	if (bDecrease != m_bDecreaseLuminance)
	{
		m_bDecreaseLuminance = bDecrease;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetDecreaseLuminance() const
{
	return m_bDecreaseLuminance;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDrawOverlay                                          */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetDrawOverlay(bool bOverlay)
{
	if (bOverlay != m_bDrawOverlay)
	{
		m_bDrawOverlay = bOverlay;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetDrawOverlay() const
{
	return m_bDrawOverlay;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetAllowSorting                                         */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetAllowSorting(bool bSort)
{
	if (bSort != m_bAllowSorting)
	{
		m_bAllowSorting = bSort;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetAllowSorting() const
{
	return m_bAllowSorting;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMaxSortingSteps                                      */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetMaxSortingSteps(int iMax)
{
	if (iMax != m_iMaxSortingSteps)
	{
		m_iMaxSortingSteps = iMax;
		Notify();
		return true;
	}
	return false;
}

int VflVisGpuParticlesParams::GetMaxSortingSteps() const
{
	return m_iMaxSortingSteps;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHighPrecisionSorting                                 */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetHighPrecisionSorting(bool bSort)
{
	if (bSort != m_bHighPrecisionSorting)
	{
		m_bHighPrecisionSorting = bSort;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetHighPrecisionSorting() const
{
	return m_bHighPrecisionSorting;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetForceDataPreparation                                 */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetForceDataPreparation(bool bForce)
{
	if (bForce != m_bForceDataPreparation)
	{
		m_bForceDataPreparation = bForce;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetForceDataPreparation() const
{
	return m_bForceDataPreparation;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRespectProcessingUnit                                */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetRespectProcessingUnit(bool bRPU)
{
	if (bRPU != m_bRespectProcessingUnit)
	{
		m_bRespectProcessingUnit = bRPU;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetRespectProcessingUnit() const
{
	return m_bRespectProcessingUnit;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRespectTracerType                                    */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticlesParams::SetRespectTracerType(bool bRTT)
{
	if (bRTT != m_bRespectTracerType)
	{
		m_bRespectTracerType = bRTT;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticlesParams::GetRespectTracerType() const
{
	return m_bRespectTracerType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticlesParams::Debug(std::ostream &out) const
{
	out << " [VflVisGpuPartsParams] - particle texture size: " << m_iWidth 
		<< "x" << m_iHeight << endl;
	out << " [VflVisGpuPartsParams] - look ahead count:     " << m_iLookAheadCount << endl;
	out << " [VflVisGpuPartsParams] - wrap around:          " << (m_bWrapAround ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - streaming update:     " << (m_bStreamingUpdate ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - active:               " << (m_bActive ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - integrator:           " << GetIntegratorAsString() << endl;
	out << " [VflVisGpuPartsParams] - delta time:           " << m_fDeltaTime << endl;
	out << " [VflVisGpuPartsParams] - max delta time:       " << m_fMaxDeltaTime << endl;
	out << " [VflVisGpuPartsParams] - use fixed delta time: " << (m_bUseFixedDeltaTime ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - fixed delta time:     " << m_fFixedDeltaTime << endl;
	out << " [VflVisGpuPartsParams] - adaptive time steps:  " << (m_bAdaptiveTimeSteps? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - compute scalars:      " << (m_bComputeScalars ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - single buffered:      " << (m_bSingleBuffered ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - force hw interp.:     " << (m_bForceHardwareInterpolation ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - visible:              " << (m_bVisible ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - color:                " << m_v3Color[0] << ", " << m_v3Color[1] 
		<< ", " << m_v3Color[2] << ", " << m_v3Color[3] << endl;
	out << " [VflVisGpuPartsParams] - use lookup table:     " << (m_bUseLookupTable ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - radius:               " << m_fRadius << endl;
	out << " [VflVisGpuPartsParams] - point size:           " << m_fPointSize << endl;
	out << " [VflVisGpuPartsParams] - line width:           " << m_fLineWidth << endl;
	out << " [VflVisGpuPartsParams] - decrease luminance:   " << (m_bDecreaseLuminance ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - draw overlay:         " << (m_bDrawOverlay ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - allow sorting:        " << (m_bAllowSorting ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - max sorting steps:    " << m_iMaxSortingSteps << endl;
	out << " [VflVisGpuPartsParams] - high precision sort:  " << (m_bHighPrecisionSorting ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - respect processor:    " << (m_bRespectProcessingUnit ? "true" : "false") << endl;
	out << " [VflVisGpuPartsParams] - respect tracer type:  " << (m_bRespectTracerType ? "true" : "false") << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflVisGpuParticlesParams::GetReflectionableType() const
{
	return s_strCVflVisGpuParticlesParamsReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflVisGpuParticlesParams::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflVisGpuParticlesParamsReflType);
	return i+1;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
