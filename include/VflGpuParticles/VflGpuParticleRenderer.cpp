/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuParticleRenderer.cpp   RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflGpuParticleRenderer.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include "VflGpuParticleSorter.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflLookupTexture.h>

#include <VistaOGLExt/Rendering/VistaParticleRenderingCore.h>
#include <VistaOGLExt/Rendering/VistaParticleRenderingProperties.h>

using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
VflGpuParticleRenderer::VflGpuParticleRenderer( VflVisGpuParticles *pParent )
	:	IVflGpuParticleRenderer( pParent )
	,	m_pCore( new VistaParticleRenderingCore )
	,	m_bValid(false)
{
	m_bValid    = m_pCore->Init();
	m_iDrawMode = m_pCore->GetProperties()->GetDrawMode();
}

VflGpuParticleRenderer::~VflGpuParticleRenderer()
{
	delete m_pCore;
}

/******************************************************************************/
VistaParticleRenderingProperties* 
	VflGpuParticleRenderer::GetParticleRenderingProperties()
{
	return m_pCore->GetProperties();
}

std::string VflGpuParticleRenderer::GetDrawModeString(int iIndex) const
{
	switch (iIndex)
	{
	case VistaParticleRenderingProperties::DM_SIMPLE:
		return std::string("DM_SIMPLE");
	case VistaParticleRenderingProperties::DM_SMOKE:
		return std::string("DM_SMOKE");
	case VistaParticleRenderingProperties::DM_BILLBOARDS:
		return std::string("DM_BILLBOARDS");
	case VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS:
		return std::string("DM_BUMPED_BILLBOARDS");
	case VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS_DEPTH_REPLACE:
		return std::string("DM_BUMPED_BILLBOARDS_DEPTH_REPLACE");
	}
	return IVflGpuParticleRenderer::GetDrawModeString(iIndex);
}

int VflGpuParticleRenderer::GetDrawModeCount() const
{
	return VistaParticleRenderingProperties::DM_LAST;
}

int VflGpuParticleRenderer::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_GPU;
}

int VflGpuParticleRenderer::GetTracerType() const
{
	return VflVisGpuParticles::TT_PARTICLES;
}

bool VflGpuParticleRenderer::IsValid() const
{
	return m_bValid;
}

/******************************************************************************/
void VflGpuParticleRenderer::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = 
		m_pParent->GetProperties();

	if (!pProperties->GetVisible() || !IsValid())
		return;

	// update particle radius
	m_pCore->SetParticleRadiusScale( pProperties->GetRadius() );
	m_pCore->SetPointSize( pProperties->GetPointSize() );

	bool bTransparent = false;

	if( VistaParticleRenderingProperties::DM_SMOKE == m_iDrawMode )
	{
		if( pProperties->GetAllowSorting() )
			m_pCore->GetProperties()->SetBlendingMode( 
			VistaParticleRenderingProperties::ALPHA_BLENDING );
		else
			m_pCore->GetProperties()->SetBlendingMode( 
			VistaParticleRenderingProperties::ADDITIVE_BLENDING );
	}

	if( pProperties->GetUseLookupTable() )
	{
		float fMin, fMax;
		pProperties->GetLookupTable()->GetTableRange( fMin, fMax );
		m_pCore->SetLookupTexture( pProperties->GetLookupTexture()->GetLookupTexture() );
		m_pCore->SetLookupRange( fMin, fMax );	
	}
	else
	{
		VistaColor oColor = pProperties->GetColor();
		m_pCore->SetParticleColor( oColor );
		m_pCore->SetLookupTexture( NULL );
	}

	// retrieve viewer position and light direction
	VflRenderNode *pRenderNode = m_pParent->GetRenderNode();
	m_pCore->SetViewerPosition(  pRenderNode->GetLocalViewPosition()   );
	m_pCore->SetLightDirection( -pRenderNode->GetLocalLightDirection() );

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	pParticles->SynchToGpu();

	// to sort or not to sort - that is the question...
	if( VistaParticleRenderingProperties::DM_SMOKE == m_iDrawMode &&
		pProperties->GetAllowSorting() )
	{
		m_pParent->GetParticleSorter()->Sort(
			pRenderNode->GetLocalViewPosition() );

		m_pCore->SetMappingTexture(
			m_pParent->GetParticleSorter()->GetMappingTexture() );
	}

	int iWidth, iHeight;
	pParticles->GetSize( iWidth, iHeight );
	m_pCore->SetDataTextureSize( iWidth, iHeight );
	m_pCore->SetDataTexture( pParticles->GetCurrentTexture() );
	m_pCore->SetParticleCount( pParticles->GetActiveParticles() );
	m_pCore->GetProperties()->SetDrawMode( m_iDrawMode );
}

void VflGpuParticleRenderer::DrawOpaque()
{
	if( !m_pParent->GetProperties()->GetVisible() )
		return;

	if( VistaParticleRenderingProperties::DM_SMOKE == m_iDrawMode )
		return;

	m_pCore->Draw();
}

void VflGpuParticleRenderer::DrawTransparent()
{
	if( !m_pParent->GetProperties()->GetVisible() )
		return;


	if( VistaParticleRenderingProperties::DM_SMOKE != m_iDrawMode )
		return;

	m_pCore->Draw();
}

/******************************************************************************/
#pragma region Reflectionable intervace

static std::string s_strCVflGpuParticleRendererReflType("GpuParticleRenderer");

std::string VflGpuParticleRenderer::GetReflectionableType() const
{
	return s_strCVflGpuParticleRendererReflType;
}

int VflGpuParticleRenderer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleRenderer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflGpuParticleRendererReflType);
	return i+1;
}

static IVistaPropertyGetFunctor *s_aCVflGpuParticleRendererGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflGpuParticleRendererSetFunctors[] =
{
	NULL //<* terminate array
};

#pragma endregion
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
