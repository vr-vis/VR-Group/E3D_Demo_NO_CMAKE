/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuParticlesTetGrid.cpp   RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflGpuParticlesTetGrid.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include <VistaFlowLib/Data/VflUnsteadyTetGridPusher.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaVisExt/Data/VveTetGrid.h>
#include <VistaVisExt/Data/VveUnsteadyTetGrid.h>
#include <VistaVisExt/Algorithms/VveTetGridPointLocator.h>
#include <VistaAspects/VistaAspectsUtils.h>

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaFramebufferObj.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

#include <set>
#include <string.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflGpuParticlesTetGridReflType("VflGpuParticlesTetGrid");

static IVistaPropertyGetFunctor *s_aCVflGpuParticlesTetGridGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflGpuParticlesTetGridSetFunctors[] =
{
	NULL
};

#define SHADER_STEADY_EULER					"VflGpuParticlesTet_Steady_Euler.glsl"
#define SHADER_STEADY_EULER_SCALARS			"VflGpuParticlesTet_Steady_EulerScalars.glsl"
#define SHADER_STEADY_RK3					"VflGpuParticlesTet_Steady_RK3.glsl"
#define SHADER_STEADY_RK3_SCALARS			"VflGpuParticlesTet_Steady_RK3Scalars.glsl"
#define SHADER_STEADY_RK4					"VflGpuParticlesTet_Steady_RK4.glsl"
#define SHADER_STEADY_RK4_SCALARS			"VflGpuParticlesTet_Steady_RK4Scalars.glsl"
#define SHADER_STEADY_ADAPTIVE_EULER		"VflGpuParticlesTet_SteadyAdaptive_Euler.glsl"
#define SHADER_STEADY_ADAPTIVE_EULER_SCALARS	"VflGpuParticlesTet_SteadyAdaptive_EulerScalars.glsl"
#define SHADER_STEADY_ADAPTIVE_RK3			"VflGpuParticlesTet_SteadyAdaptive_RK3.glsl"
#define SHADER_STEADY_ADAPTIVE_RK3_SCALARS	"VflGpuParticlesTet_SteadyAdaptive_RK3Scalars.glsl"
#define SHADER_STEADY_ADAPTIVE_RK4			"VflGpuParticlesTet_SteadyAdaptive_RK4.glsl"
#define SHADER_STEADY_ADAPTIVE_RK4_SCALARS	"VflGpuParticlesTet_SteadyAdaptive_RK4Scalars.glsl"
#define SHADER_UNSTEADY_EULER				"VflGpuParticlesTet_Unsteady_Euler.glsl"
#define SHADER_UNSTEADY_EULER_SCALARS		"VflGpuParticlesTet_Unsteady_EulerScalars.glsl"
#define SHADER_UNSTEADY_RK3					"VflGpuParticlesTet_Unsteady_RK3.glsl"
#define SHADER_UNSTEADY_RK3_SCALARS			"VflGpuParticlesTet_Unsteady_RK3Scalars.glsl"
#define SHADER_UNSTEADY_RK4					"VflGpuParticlesTet_Unsteady_RK4.glsl"
#define SHADER_UNSTEADY_RK4_SCALARS			"VflGpuParticlesTet_Unsteady_RK4Scalars.glsl"

static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveTetGrid *pGrid);

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveTetGrid *pGrid);

static void DumpTexture(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						std::ostream &out);
static void DumpTextureAsInt(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						std::ostream &out);

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflGpuParticlesTetGrid::VflGpuParticlesTetGrid(VflVisGpuParticles *pParent)
: IVflGpuParticleTracer(pParent),
  m_pWriteMultipleTC(NULL),
  m_bValid(false)
{
	m_bValid = CreateGpuResources();
}

VflGpuParticlesTetGrid::~VflGpuParticlesTetGrid()
{
	DestroyGpuResources();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesTetGrid::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();

	if (!pProperties->GetActive())
		return;

	double dVisTime = m_pParent->GetVisTime();
	VveTimeMapper *pTimeMapper = m_pParent->GetData()->GetTimeMapper();

	// retrieve integration parameters
	int iIntegrator = pProperties->GetIntegrator();
	bool bComputeScalars = pProperties->GetComputeScalars();

	// determine dataset boundaries
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pTimeMapper->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return;
	}
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	static bool bDump = false;
	if (bDump)
	{
		std::cout << "before ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		int w, h;
		m_pParent->GetParticleData()->GetSize(w, h);
		DumpTexture(m_pParent->GetParticleData()->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(m_pParent->GetParticleData()->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		DumpTextureAsInt(m_pParent->GetParticleData()->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		std::cout << std::endl;
	}

	// get access to flow data
	VflUnsteadyTexturePusher *pPusher = m_pParent->GetPusher();
	VflUnsteadyTetGridPusher *pGridPusher = dynamic_cast<VflUnsteadyTetGridPusher *>(pPusher);
	if (!pGridPusher)
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to retrieve texture pusher..." << std::endl;
		return;
	}

	m_pParent->SaveOGLState();

	// retrieve previous particle population
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	VistaTexture *pOldPopulation = pParticles->GetCurrentTexture();
	pOldPopulation->Bind(GL_TEXTURE0);
	VistaTexture *pOldAttributes = pParticles->GetCurrentAttributeTexture();
	pOldAttributes->Bind(GL_TEXTURE1);

	// bind render target
	pParticles->GetFramebufferObj()->Bind();

	if (pProperties->GetSingleBuffered())
	{
		GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex(),
			GL_COLOR_ATTACHMENT2_EXT + pParticles->GetCurrentDataIndex()};
		glDrawBuffers(2, db);
	}
	else
	{
		GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + 1-pParticles->GetCurrentDataIndex(),
			GL_COLOR_ATTACHMENT2_EXT + 1-pParticles->GetCurrentDataIndex()};
		glDrawBuffers(2, db);
	}

	int w, h;
	pParticles->GetSize(w, h);

	VistaGLSLShader *pShader = NULL;
	double dDeltaT = 0.0;

	// determine and bind shader
	if (m_pParent->GetRenderNode()->GetVisTiming()->GetAnimationPlaying())
	{
		// determine time step length
		double dDeltaTVisTime = 0.0;
		if (m_pParent->GetTimeWarning())
		{
			dDeltaT = pProperties->GetMaxDeltaTime();

			dDeltaTVisTime = pTimeMapper->GetVisualizationTime(pTimeMapper->GetSimulationTime(0)+dDeltaT);
		}
		else
		{
			double dLastVisTime = m_pParent->GetLastVisTime();
			dDeltaTVisTime = dVisTime - dLastVisTime;
			while (dDeltaTVisTime < 0.0)
			{
				dDeltaTVisTime += 1.0;
			}

			dDeltaT = pTimeMapper->GetSimulationTime(dDeltaTVisTime)
				- pTimeMapper->GetSimulationTime(0);
		}

		if (pGridPusher->GetUseTopologyGrid())
		{
			if (bComputeScalars)
			{
				pShader = m_vecShadersUnsteadyScalars[iIntegrator];
			}
			else
			{
				pShader = m_vecShadersUnsteady[iIntegrator];
			}
		}

		if (pShader)
		{
			pShader->Bind();

			// bind flow data
			pGridPusher->GetVertexTexture()->Bind(GL_TEXTURE2);
			pGridPusher->GetCellTexture()->Bind(GL_TEXTURE3);
			pGridPusher->GetCellNeighborTexture()->Bind(GL_TEXTURE4);

			// set texture information
			std::vector<VistaTexture *> vecTextures;
			std::vector<float> vecOffsets, vecWeights;
			switch (iIntegrator)
			{
				case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER:
				vecOffsets.push_back(0);
				if (pGridPusher->GetTexturesAndWeights(vecOffsets, vecTextures, vecWeights) == 1)
				{
					vecTextures[0]->Bind(GL_TEXTURE5);
					vecTextures[1]->Bind(GL_TEXTURE6);

					float aWeights[2];
					aWeights[0] = 1.0f - vecWeights[0];
					aWeights[1] = vecWeights[0];

					pShader->SetUniform(pShader->GetUniformLocation("aWeights[0]"), 1, 2, aWeights);
				}
				break;
			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3:
			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4:
				vecOffsets.push_back(0);
				vecOffsets.push_back(0.5f * static_cast<float>(dDeltaTVisTime));
				vecOffsets.push_back(static_cast<float>(dDeltaTVisTime));
				if (pGridPusher->GetTexturesAndWeights(vecOffsets, vecTextures, vecWeights) == 3)
				{
					GLenum iTextureUnit = GL_TEXTURE5;
					float aWeights[6];
					for (int i=0; i<3; ++i)
					{
						vecTextures[2*i]->Bind(iTextureUnit++);
						vecTextures[2*i+1]->Bind(iTextureUnit++);
						aWeights[2*i] = 1.0f - vecWeights[i];
						aWeights[2*i+1] = vecWeights[i];
					}
					pShader->SetUniform(pShader->GetUniformLocation("aWeights[0]"), 1, 6, aWeights);
				}
				break;
			}
			// but stay on texture unit 0
			glActiveTexture(GL_TEXTURE0);

			// determine texture widths and send it to the shader
			pShader->SetUniform(pShader->GetUniformLocation("v2TexWidth"), 
				pGridPusher->GetVertexTexture()->GetMaxS(),
				pGridPusher->GetCellTexture()->GetMaxS());
		}
	}
	else
	{
		// determine time step length
		dDeltaT = pProperties->GetDeltaTime();

		if (pProperties->GetAdaptiveTimeSteps())
			iIntegrator += VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST;

		if (bComputeScalars)
		{
			pShader = m_vecShadersSteadyScalars[iIntegrator];
		}
		else
		{
			pShader = m_vecShadersSteady[iIntegrator];
		}

		if (pShader)
		{
			pShader->Bind();

			// bind flow data
			pGridPusher->GetVertexTexture()->Bind(GL_TEXTURE2);
			pGridPusher->GetPointDataTexture()->Bind(GL_TEXTURE3);
			pGridPusher->GetCellTexture()->Bind(GL_TEXTURE4);
			pGridPusher->GetCellNeighborTexture()->Bind(GL_TEXTURE5);

			// but stay on texture unit 0
			glActiveTexture(GL_TEXTURE0);

			// determine texture widths and send it to the shader
			pShader->SetUniform(pShader->GetUniformLocation("v2TexWidth"), 
				pGridPusher->GetVertexTexture()->GetMaxS(),
				pGridPusher->GetCellTexture()->GetMaxS());
		}
	}

	if (pShader)
	{
		// specify time step length
		pShader->SetUniform(pShader->GetUniformLocation("fDeltaT"),
			static_cast<float>(dDeltaT));

		// draw to the complete texture
		glViewport(0, 0, w, h);
		glLoadIdentity();
		glOrtho(0, 1, 0, 1, -1, 1);

		// [picard] Energize! [\picard]
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(0, 0);
		glTexCoord2f(1, 0);
		glVertex2f(1, 0);
		glTexCoord2f(1, 1);
		glVertex2f(1, 1);
		glTexCoord2f(0, 1);
		glVertex2f(0, 1);
		glEnd();

		// huh! that's it?!
		pShader->Release();
	}
	pParticles->GetFramebufferObj()->Release();

	m_pParent->RestoreOGLState();

	if (pShader)
	{
		if (!pProperties->GetSingleBuffered())
			m_pParent->GetParticleData()->Swap();

		m_pParent->GetParticleData()->SignalGpuDataChange();
	}

	if (bDump)
	{
		std::cout << "after ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(pParticles->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		DumpTextureAsInt(m_pParent->GetParticleData()->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		std::cout << std::endl;
		bDump = false;
	}

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticle                                                */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesTetGrid::SeedParticle(const VistaVector3D &v3Pos)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	float fCell = float(pGrid->GetPointLocator()->GetCellId(&v3Pos[0]));
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();

	int w, h;
	pParticles->GetSize(w, h);

	m_pParent->SaveOGLState();

	static bool bDump = false;
	if (bDump)
	{
		std::cout << "before ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(pParticles->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		DumpTextureAsInt(m_pParent->GetParticleData()->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		std::cout << std::endl;
	}

	// set render target
	pParticles->GetFramebufferObj()->Bind();
//	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex());
	GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex(),
		GL_COLOR_ATTACHMENT2_EXT + pParticles->GetCurrentDataIndex()};
	glDrawBuffers(2, db);

	// activate shader for writing particle positions (given as TCs)
	m_pWriteMultipleTC->Bind();

	// set viewport and transformation matrices
	glViewport(0, 0, w, h);
	glLoadIdentity();
	glOrtho(0, w, 0, h, -1, 1);

	// ok - who's next?
	float fPosX = (iNext % w) + 0.5f;
	float fPosY = (iNext / w) + 0.5f;

	glBegin(GL_POINTS);
	glMultiTexCoord4fv(GL_TEXTURE0, &v3Pos[0]);
	glMultiTexCoord1f(GL_TEXTURE1, fCell);
	glVertex2f(fPosX, fPosY);
	glEnd();

	m_pWriteMultipleTC->Release();
	pParticles->GetFramebufferObj()->Release();
	m_pParent->RestoreOGLState();

	pParticles->SetNextParticle((iNext+1)%iCount);
	if (iActive < iCount)
	{
		++iActive;
		pParticles->SetActiveParticles(iActive);
		if (iActive == iCount)
		{
			std::cout << " [VflGpuParticlesTetGrid] - all particles are active now..." << std::endl;
		}
	}

	if (bDump)
	{
		std::cout << "after ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(pParticles->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		DumpTextureAsInt(m_pParent->GetParticleData()->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		std::cout << std::endl;
		bDump = false;
	}

	pParticles->SignalGpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticles                                               */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesTetGrid::SeedParticles(const VistaVector3D &v3Pos1, 
											const VistaVector3D &v3Pos2, 
											int iCount)
{
	double fVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(fVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iParticleCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();

	if (iCount > iParticleCount)
		iCount = iParticleCount;

	float fDelta = 1.0f;
	if (iCount > 1)
		fDelta /= (iCount-1);
	float fOffset = 0;
	VistaVector3D v3Delta(v3Pos2-v3Pos1);
	float fScalarDelta(v3Pos2[3]-v3Pos1[3]);

	int w, h;
	pParticles->GetSize(w, h);

	m_pParent->SaveOGLState();

	static bool bDump = false;
	if (bDump)
	{
		std::cout << "before ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(pParticles->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		DumpTextureAsInt(m_pParent->GetParticleData()->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		std::cout << std::endl;
	}

	// set render target
	pParticles->GetFramebufferObj()->Bind();
	GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex(),
		GL_COLOR_ATTACHMENT2_EXT + pParticles->GetCurrentDataIndex()};
	glDrawBuffers(2, db);

	// activate shader for writing particle positions (given as TCs)
	m_pWriteMultipleTC->Bind();

	// set viewport and transformation matrices
	glViewport(0, 0, w, h);
	glLoadIdentity();
	glOrtho(0, w, 0, h, -1, 1);

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	glBegin(GL_POINTS);
	for (int i=0; i<iCount; ++i, fOffset+=fDelta)
	{
		VistaVector3D v3NewPos(v3Pos1 + fOffset*v3Delta);
		v3NewPos[3] = v3Pos1[3] + fOffset*fScalarDelta;

		// ok - who's next?
		float fPosX = (iNext % w) + 0.5f;
		float fPosY = (iNext / w) + 0.5f;

		glMultiTexCoord4fv(GL_TEXTURE0, &v3NewPos[0]);
		glMultiTexCoord1f(GL_TEXTURE1, float(pGrid->GetPointLocator()->GetCellId(&v3NewPos[0])));
		glVertex2f(fPosX, fPosY);

		iNext = (iNext+1) % iParticleCount;
	}
	glEnd();
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	m_pWriteMultipleTC->Release();
	pParticles->GetFramebufferObj()->Release();
	m_pParent->RestoreOGLState();

	if (iActive < iParticleCount)
	{
		iActive += iCount;
		if (iActive >= iParticleCount)
		{
			iActive = iParticleCount;
			std::cout << " [VflGpuParticlesTetGrid] - all particles are active now..." << std::endl;
		}

		pParticles->SetActiveParticles(iActive);
	}

	if (bDump)
	{
		std::cout << "after ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(pParticles->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		DumpTextureAsInt(m_pParent->GetParticleData()->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		std::cout << std::endl;
		bDump = false;
	}

	pParticles->SetNextParticle(iNext);
	pParticles->SignalGpuDataChange();
}

void VflGpuParticlesTetGrid::SeedParticles(const std::vector<float> &vecPositions)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iParticleCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();
	int iCount = int(vecPositions.size()) / 4;

	if (iCount > iParticleCount)
		iCount = iParticleCount;

	if (m_vecSeedBuffer.size() < (unsigned int)(iCount*7))
		m_vecSeedBuffer.resize(iCount*7);

	int w, h;
	pParticles->GetSize(w, h);

	m_pParent->SaveOGLState();

	static bool bDump = false;
	if (bDump)
	{
		std::cout << "before ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(pParticles->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		DumpTextureAsInt(m_pParent->GetParticleData()->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		std::cout << std::endl;
	}

	// set render target
	pParticles->GetFramebufferObj()->Bind();
	GLenum db[2] = {GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex(),
		GL_COLOR_ATTACHMENT2_EXT + pParticles->GetCurrentDataIndex()};
	glDrawBuffers(2, db);

	// activate shader for writing particle positions (given as TCs)
	m_pWriteMultipleTC->Bind();

	// set viewport and transformation matrices
	glViewport(0, 0, w, h);
	glLoadIdentity();
	glOrtho(0, w, 0, h, -1, 1);

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	float *pBuffer = &m_vecSeedBuffer[0];
	const float *pPositions = &vecPositions[0];
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();
	for (int i=0; i<iCount; ++i)
	{
		memcpy(&pBuffer[7*i], &pPositions[4*i], 4*sizeof(float));
		pBuffer[7*i+4] = float(pLocator->GetCellId(&pPositions[4*i]));

		// ok - who's next?
		pBuffer[7*i+5] = (iNext % w) + 0.5f;
		pBuffer[7*i+6] = (iNext / w) + 0.5f;

		iNext = (iNext+1) % iParticleCount;
	}
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, 7*sizeof(float), &pBuffer[5]);

	glClientActiveTexture(GL_TEXTURE1);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(1, GL_FLOAT, 7*sizeof(float), &pBuffer[4]);

	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(4, GL_FLOAT, 7*sizeof(float), pBuffer);

	glDrawArrays(GL_POINTS, 0, iCount);

	glClientActiveTexture(GL_TEXTURE1);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	m_pWriteMultipleTC->Release();
	pParticles->GetFramebufferObj()->Release();
	m_pParent->RestoreOGLState();

	if (iActive < iParticleCount)
	{
		iActive += iCount;
		if (iActive >= iParticleCount)
		{
			iActive = iParticleCount;
			std::cout << " [VflGpuParticlesTetGrid] - all particles are active now..." << std::endl;
		}

		pParticles->SetActiveParticles(iActive);
	}

	if (bDump)
	{
		std::cout << "before ---------------------------------" << std::endl;
		std::cout << "particles: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "attributes:" << std::endl;
		DumpTexture(pParticles->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		DumpTextureAsInt(m_pParent->GetParticleData()->GetCurrentAttributeTexture(), w, h, 4, std::cout);
		std::cout << std::endl;
		bDump = false;
	}

	pParticles->SetNextParticle(iNext);
	pParticles->SignalGpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticleNormalized                                      */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesTetGrid::SeedParticleNormalized(const VistaVector3D &v3Pos)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3PosDN;
	DeNormalize(v3PosDN, v3Pos, pGrid);
	SeedParticle(v3PosDN);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticlesNormalized                                     */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesTetGrid::SeedParticlesNormalized(const VistaVector3D &v3Pos1, 
													   const VistaVector3D &v3Pos2, 
													   int iCount)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3Pos1DN, v3Pos2DN;
	DeNormalize(v3Pos1DN, v3Pos1, pGrid);
	DeNormalize(v3Pos2DN, v3Pos2, pGrid);
	SeedParticles(v3Pos1DN, v3Pos2DN, iCount);
}

void VflGpuParticlesTetGrid::SeedParticlesNormalized(const std::vector<float> &vecPositions)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	std::vector<float> vecDNPositions;
	int iCount = int(vecPositions.size()) / 4;
	vecDNPositions.resize(vecPositions.size());
	for (int i=0; i<iCount; ++i)
	{
		DeNormalize(&vecDNPositions[4*i], &vecPositions[4*i], pGrid);
	}

	SeedParticles(vecDNPositions);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGridType                                                 */
/*                                                                            */
/*============================================================================*/
int VflGpuParticlesTetGrid::GetGridType() const
{
	return VflVisGpuParticles::GT_TET_GRID;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProcessingUnit                                           */
/*                                                                            */
/*============================================================================*/
int VflGpuParticlesTetGrid::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_GPU;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerType                                               */
/*                                                                            */
/*============================================================================*/
int VflGpuParticlesTetGrid::GetTracerType() const
{
	return VflVisGpuParticles::TT_PARTICLES;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflGpuParticlesTetGrid::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflGpuParticlesTetGrid::GetReflectionableType() const
{
	return s_strCVflGpuParticlesTetGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateGpuResources                                          */
/*                                                                            */
/*============================================================================*/
bool VflGpuParticlesTetGrid::CreateGpuResources()
{
	// TODO: check for required OpenGL extensions...
#ifdef DEBUG
	std::cout << " [VflGpuParticlesTetGrid] - creating GPU resources..." << std::endl;
#endif

	// init shaders -----------------------------------------------------------
#ifdef DEBUG
	std::cout << " [VflGpuParticlesTetGrid] - creating shader for writing tex coords..." << std::endl;
#endif
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();
	std::string strShader =
		rShaderReg.RetrieveShader( "VflGpuTetGrid_DualWriteTC_frag.glsl" );

	VistaGLSLShader* pProgram = new VistaGLSLShader();

	if(    strShader.empty()
		|| !pProgram->InitShaderFromString( GL_FRAGMENT_SHADER, strShader )
		|| !pProgram->Link() )
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to initialize shader for writing texcoords..." << std::endl;
		delete pProgram;
		return false;
	}
	m_pWriteMultipleTC = pProgram;

	m_vecShadersSteady.resize(2*VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersSteadyScalars.resize(2*VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersUnsteady.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersUnsteadyScalars.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
#ifdef DEBUG
	std::cout << " [VflGpuParticlesTetGrid] - creating Euler integration shaders..." << std::endl;
#endif
	VistaGLSLShader *pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_EULER);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - Euler integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_EULER_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - Euler integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_EULER);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - adaptive Euler integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init adaptive Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link adaptive Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_EULER_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - adaptive Euler integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init adaptive Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link adaptive Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_EULER);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unsteady Euler integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init unsteady Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link unsteady Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 4);
	{
		int aTexUnits[] = { 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData[0]"), 1, 2, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_EULER_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unsteady Euler integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init unsteady Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link unsteady Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 4);
	{
		int aTexUnits[] = { 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData[0]"), 1, 2, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

#ifdef DEBUG
	std::cout << " [VflGpuParticlesTetGrid] - creating RK3 integration shaders..." << std::endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK3);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - RK3 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK3_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - RK3 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_RK3);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - adaptive RK3 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init adaptive RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link adaptive RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_RK3_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - adaptive RK3 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init adaptive RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link adaptive RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_RK3);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unsteady RK3 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init unsteady RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link unsteady RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 4);
	{
		int aTexUnits[] = { 5, 6, 7, 8, 9, 10 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_RK3_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unsteady RK3 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init unsteady RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link unsteady RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 4);
	{
		int aTexUnits[] = { 5, 6, 7, 8, 9, 10 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

#ifdef DEBUG
	std::cout << " [VflGpuParticlesTetGrid] - creating RK4 integration shaders..." << std::endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK4);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - RK4 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK4_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - RK4 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_RK4);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - adaptive RK4 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init adaptive RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link adaptive RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_ADAPTIVE_RK4_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - adaptive RK4 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 4);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 5);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4+VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_RK4);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unsteady RK4 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init unsteady RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link unsteady RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 4);
	{
		int aTexUnits[] = { 5, 6, 7, 8, 9, 10 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_RK4_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unsteady RK4 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to init unsteady RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuParticlesTetGrid] - ERROR - unable to link unsteady RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitAttrib"), 1);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVertices"), 2);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCells"), 3);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitCellNeighbors"), 4);
	{
		int aTexUnits[] = { 5, 6, 7, 8, 9, 10 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitPointData[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DestroyGpuResources                                         */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesTetGrid::DestroyGpuResources()
{
	delete m_pWriteMultipleTC;
	m_pWriteMultipleTC = NULL;

	std::set<VistaGLSLShader *> setShaders;

	for (unsigned int i=0; i<m_vecShadersSteady.size(); ++i)
	{
		setShaders.insert(m_vecShadersSteady[i]);
	}
	m_vecShadersSteady.clear();

	for (unsigned int i=0; i<m_vecShadersSteadyScalars.size(); ++i)
	{
		setShaders.insert(m_vecShadersSteadyScalars[i]);
	}
	m_vecShadersSteadyScalars.clear();

	for (unsigned int i=0; i<m_vecShadersUnsteady.size(); ++i)
	{
		setShaders.insert(m_vecShadersUnsteady[i]);
	}
	m_vecShadersUnsteady.clear();

	for (unsigned int i=0; i<m_vecShadersUnsteadyScalars.size(); ++i)
	{
		setShaders.insert(m_vecShadersUnsteadyScalars[i]);
	}
	m_vecShadersUnsteadyScalars.clear();

	std::set<VistaGLSLShader *>::iterator it;
	for (it=setShaders.begin(); it!=setShaders.end(); ++it)
	{
		delete *it;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflGpuParticlesTetGrid::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleTracer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflGpuParticlesTetGridReflType);
	return i+1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRendererId                                               */
/*                                                                            */
/*============================================================================*/
const std::string VflGpuParticlesTetGrid::GetRendererId()
{
	return s_strCVflGpuParticlesTetGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetShaderKeys                                               */
/*                                                                            */
/*============================================================================*/
void VflGpuParticlesTetGrid::GetShaderKeys(
	std::vector<std::string>& vecShaderKeys)
{
	vecShaderKeys.clear();
	vecShaderKeys.push_back(SHADER_STEADY_EULER);
	vecShaderKeys.push_back(SHADER_STEADY_EULER_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_RK3);
	vecShaderKeys.push_back(SHADER_STEADY_RK3_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_RK4);
	vecShaderKeys.push_back(SHADER_STEADY_RK4_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_EULER);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_EULER_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_RK3);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_RK3_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_RK4);
	vecShaderKeys.push_back(SHADER_STEADY_ADAPTIVE_RK4_SCALARS);
	vecShaderKeys.push_back(SHADER_UNSTEADY_EULER);
	vecShaderKeys.push_back(SHADER_UNSTEADY_EULER_SCALARS);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK3);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK3_SCALARS);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK4);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK4_SCALARS);
}

/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeNormalize                                                 */
/*                                                                            */
/*============================================================================*/
static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveTetGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	v3Out[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * v3In[0];
	v3Out[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * v3In[1];
	v3Out[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * v3In[2];
	v3Out[3] = v3In[3];
}

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveTetGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	aOut[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * aIn[0];
	aOut[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * aIn[1];
	aOut[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * aIn[2];
	aOut[3] = aIn[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DumpTexture                                                 */
/*                                                                            */
/*============================================================================*/
static void DumpTexture(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						std::ostream &out)
{
	if (!pTexture)
		return;

	float *pData = new float[iWidth*iHeight*iTuples];
	pTexture->Bind();
	glGetTexImage(pTexture->GetTarget(), 0, GL_RGBA, GL_FLOAT, pData);
	float *pPos = pData;

	for (int i=0; i<iHeight; ++i)
	{
		for (int j=0; j<iWidth; ++j)
		{
			if (j>0)
				out << "| ";
			for (int k=0; k<iTuples; ++k)
			{
				out << (*(pPos++)) << " ";
			}
		}
		out << std::endl;
	}

	delete [] pData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DumpTextureAsInt                                            */
/*                                                                            */
/*============================================================================*/
static void DumpTextureAsInt(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						std::ostream &out)
{
	if (!pTexture)
		return;

	float *pData = new float[iWidth*iHeight*iTuples];
	pTexture->Bind();
	glGetTexImage(pTexture->GetTarget(), 0, GL_RGBA, GL_FLOAT, pData);
	float *pPos = pData;

	for (int i=0; i<iHeight; ++i)
	{
		for (int j=0; j<iWidth; ++j)
		{
			if (j>0)
				out << "| ";
			for (int k=0; k<iTuples; ++k)
			{
				out << int((*(pPos++))) << " ";
			}
		}
		out << std::endl;
	}

	delete [] pData;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
