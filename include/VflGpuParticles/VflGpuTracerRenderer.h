/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflGpuTracerRenderer.h        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflGpuTracerRenderer                                             */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLGPUTRACERRENDERER_H
#define __VFLGPUTRACERRENDERER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "VflGpuParticlesConfig.h"
#include "VflGpuParticleRendererInterface.h"
#include <VistaBase/VistaVectorMath.h>

#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaFramebufferObj;
class VistaRenderToVertexArray;
class VistaTexture;
class VistaGLSLShader;
class VistaBufferObject;
class VistaVertexArrayObject;
class VistaParticleTraceRenderingCore;
class VistaParticleRenderingProperties;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI VflGpuTracerRenderer : public IVflGpuParticleRenderer
{
public:
	VflGpuTracerRenderer(VflVisGpuParticles *pParent);
	virtual ~VflGpuTracerRenderer();

	VistaParticleRenderingProperties* GetParticleRenderingProperties();

	virtual void Update();
	virtual void DrawOpaque();
	virtual void DrawTransparent();

	virtual std::string GetDrawModeString(int iIndex) const;
	virtual int GetDrawModeCount() const;

	virtual int GetProcessingUnit() const;
	virtual int GetTracerType() const;

	virtual bool IsValid() const;
	virtual std::string GetReflectionableType() const;

protected:
	VflGpuTracerRenderer();
	bool CreateGpuResources();	// create textures, shaders etc.
	void UpdateGpuResources();	// re-allocate memory etc.
	void DestroyGpuResources();	// destroy textures, shaders etc.
	void PrepareRendering();

	void UpdatePositions();
	void UpdateTangents();
	
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	VistaParticleTraceRenderingCore* m_pCore;

	double	m_dPreparationTimeStamp;

	int	m_iTracerCount;
	int m_iTracerLength;

	// GPGPU stuff
	VistaTexture		*m_pTargetTexture; 
	VistaFramebufferObj	*m_pFBO;

	// graphical representation - tubelets
	VistaVertexArrayObject*   m_pBodyVAO;
	VistaRenderToVertexArray* m_pBodyPositions;
	VistaRenderToVertexArray* m_pBodyTangents;
	VistaBufferObject*        m_pBodyOffsetsVBO;
	VistaBufferObject*        m_pBodyIBO;

	VistaVertexArrayObject*   m_pCapVAO;
	VistaRenderToVertexArray* m_pCapPositions;
	VistaRenderToVertexArray* m_pCapNormals;
	VistaBufferObject*        m_pCapOffsetsVBO;
	VistaBufferObject*        m_pCapIBO;

	VistaGLSLShader* m_pPassThrough;
	VistaGLSLShader* m_pComputeTangents;

	bool m_bValid;
};


#endif // __VFLGPUTRACERRENDERER_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

