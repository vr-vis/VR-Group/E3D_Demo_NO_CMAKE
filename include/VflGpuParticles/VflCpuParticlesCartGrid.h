/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflCpuParticlesCartGrid.h     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflCpuParticlesCartGrid                                           */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLCPUPARTICLESCARTGRID_H
#define __VFLCPUPARTICLESCARTGRID_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflGpuParticlesConfig.h"
#include "VflGpuParticleTracer.h"

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI VflCpuParticlesCartGrid : public IVflGpuParticleTracer
{
public:
	VflCpuParticlesCartGrid(VflVisGpuParticles *pParent);
	virtual ~VflCpuParticlesCartGrid();

	virtual void Update();

	virtual void SeedParticle(const VistaVector3D &v3Pos);
	virtual void SeedParticles(const VistaVector3D &v3Pos1,
		const VistaVector3D &v3Pos2, int iCount);
	virtual void SeedParticles(const std::vector<float> &vecPositions);
	virtual void SeedParticleNormalized(const VistaVector3D &v3Pos);
	virtual void SeedParticlesNormalized(const VistaVector3D &v3Pos1,
		const VistaVector3D &v3Pos2, int iCount);
	virtual void SeedParticlesNormalized(
		const std::vector<float> &vecPositions);

	/**
	 * Retrieve the grid type supported by this tracer as defined in 
	 * VflVisGpuParticles::GRID_TYPE
	 */
	virtual int GetGridType() const;

	/**
	 * Retrieve the processing unit for this tracer.
	 */
	virtual int GetProcessingUnit() const;

	/**
	 * Determine the tracer type for this tracer.
	 */
	virtual int GetTracerType() const;

	virtual bool IsValid() const;
	virtual std::string GetReflectionableType() const;

protected:
	// @todo The below functions use a lot of static_cast's (and macros for that
	//		 matter). See if you can get rif of them!!
	bool IntegrateEuler(double dVisTime, double dDeltaT, bool bComputeScalars);
	bool IntegrateRK3(double dVisTime, double dDeltaT, bool bComputeScalars);
	bool IntegrateRK4(double dVisTime, double dDeltaT, bool bComputeScalars);
	bool IntegrateEulerUnsteady(double dVisTime, double dLastVisTime,
		double dDeltaT, bool bComputeScalars);
	bool IntegrateRK3Unsteady(double dVisTime, double dLastVisTime,
		double dDeltaT, bool bComputeScalars);
	bool IntegrateRK4Unsteady(double dVisTime, double dLastVisTime,
		double dDeltaT, bool bComputeScalars);

	VflCpuParticlesCartGrid();
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;
};


#endif // __VFLCPUPARTICLESCARTGRID_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

