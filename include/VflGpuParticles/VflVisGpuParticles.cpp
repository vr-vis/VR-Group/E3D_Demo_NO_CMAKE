/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflVisGpuParticles.cpp       RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflVisGpuParticles.h"

#include "VflGpuParticleData.h"
#include "VflGpuParticleSorter.h"
#include "VflGpuParticleTracer.h"
#include "VflGpuParticleRendererInterface.h"
#include <VistaVisExt/Data/VveTetGrid.h>
#include <VistaVisExt/Data/VveUnsteadyTetGrid.h>
#include <VistaVisExt/Data/VveCartesianGrid.h>

// VistaFlowLib includes
#include <VistaFlowLib/Data/VflUnsteadyCartesianGridPusher.h>
#include <VistaFlowLib/Data/VflUnsteadyTetGridPusher.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

#include <VistaFlowLib/Visualization/VflLookupTexture.h>

// VistaOGLExt includes
#include <VistaOGLExt/VistaTexture.h>

#include <string.h>

using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflVisGpuParticles::VflVisGpuParticles(VveUnsteadyCartesianGrid *pData, VflVtkLookupTable* pLut)
: m_pParticles(NULL),
  m_pPusher(NULL),
  m_pSorter(NULL),
  m_iCurrentTracer(-1),
  m_iCurrentRenderer(-1),
  m_eGridType(GT_CARTESIAN_GRID),
  m_dVisTime(0),
  m_dLastVisTime(0),
  m_bTimeWarning(false)
{
	m_uData.pcg = pData;

	if(pLut)
	{
		m_pProperties = CreateProperties(pLut);
		m_pProperties->SetParentRenderable(this);
		Observe(m_pProperties, E_PROP_TICKET);	
	}
	else
	{
		m_pProperties = CreateProperties();
		m_pProperties->SetParentRenderable(this);
		Observe(m_pProperties, E_PROP_TICKET);
	}

	m_pParticles = new VflGpuParticleData(GetProperties());

	m_pPusher = new VflUnsteadyCartesianGridPusher(m_uData.pcg);

	m_pSorter = new VflGpuParticleSorter(this);

	memset(m_aViewportState, 0, 4*sizeof(float));
}

VflVisGpuParticles::VflVisGpuParticles(VveUnsteadyTetGrid *pData, VflVtkLookupTable* pLut)
: m_pParticles(NULL),
  m_pPusher(NULL),
  m_iCurrentTracer(-1),
  m_iCurrentRenderer(-1),
  m_eGridType(GT_TET_GRID),
  m_dVisTime(0),
  m_dLastVisTime(0),
  m_bTimeWarning(false)
{
	m_uData.ptg = pData;

	if(pLut)
	{
		m_pProperties = CreateProperties(pLut);
		m_pProperties->SetParentRenderable(this);
		Observe(m_pProperties, E_PROP_TICKET);	
	}
	else
	{
		m_pProperties = CreateProperties();
		m_pProperties->SetParentRenderable(this);
		Observe(m_pProperties, E_PROP_TICKET);
	}

	m_pParticles = new VflGpuParticleData(GetProperties());

	m_pPusher = new VflUnsteadyTetGridPusher(m_uData.ptg);

	m_pSorter = new VflGpuParticleSorter(this);
}

VflVisGpuParticles::~VflVisGpuParticles()
{
	if(m_pSorter)
	{
		delete m_pSorter;
		m_pSorter = NULL;
	}

	if(m_pPusher)
	{
		delete m_pPusher;
		m_pPusher = NULL;
	}

	if(m_pParticles)
	{
		delete m_pParticles;
		m_pParticles = NULL;
	}

	ReleaseObserveable(m_pProperties);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::Init()
{
	// TODO: check for OpenGL extensions!!!

	if (!m_uData.pv)
	{
		cout << " [VflVisGpuParticles] - ERROR - no data given..." << endl;
		return false;
	}

	if (!m_pProperties)
	{
		cout << " [VflVisGpuParticles] - ERROR - no parameters given..." << endl;
		return false;
	}

	if (!m_pParticles)
	{
		cout << " [VflVisGpuParticles] - ERROR - unable to create particle data..." << endl;
		return false;
	}

	if (!m_pPusher)
	{
		cout << " [VflVisGpuParticles] - ERROR - unable to retrieve texture pusher..." << endl;
		return false;
	}

	if (!m_pSorter)
	{
		cout << " [VflVisGpuParticles] - ERROR - unable to create particle sorter..." << endl;
		return false;
	}

	if (!m_pSorter->GetValid())
	{
		cout << " [VflVisGpuParticles] - ERROR - unable to create valid particle sorter..." << endl;
		return false;
	}

	// configure texture pusher
	m_pPusher->SetLookAheadCount(GetProperties()->GetLookAheadCount());
	m_pPusher->SetWrapAround(GetProperties()->GetWrapAround());
	m_pPusher->SetStreamingUpdate(GetProperties()->GetStreamingUpdate());

	// check tracers
	if (m_vecTracers.empty())
		cout << " [VflVisGpuParticles] - WARNING - no tracers given..." << endl;
	else
		m_iCurrentTracer = 0;
	
	// check renderers
	if (m_vecRenderers.empty())
		cout << " [VflVisGpuParticles] - WARNING - no renderers given..." << endl;
	else 
	{
		m_iCurrentRenderer = 0;
		if (GetProperties()->GetRespectProcessingUnit()
			|| GetProperties()->GetRespectTracerType())
		{
			SynchTracerAndRenderer();
		}
	}

//	VflVisObject::SetVisController(pController);
	return true;
}




/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflVisObject::VflVisObjProperties *VflVisGpuParticles::CreateProperties() const
{
	return new VflVisGpuParticlesProperties();
}

IVflVisObject::VflVisObjProperties *VflVisGpuParticles::CreateProperties(VflVtkLookupTable *pLut) const
{
	return new VflVisGpuParticlesProperties(pLut);
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticles::Update()
{
	// an update consists of the following steps:
	// - let the texture pusher send data to the GPU
	// - update the seeders, i.e. make 'em seed gg: deprecated - do NOT let the particle make the seeders seed!!
	// - update the currently active tracer, i.e. integrate active particles
	// - swap the particle population (EDIT: leave that to the integrator!!)
	// - let the currently active renderer prepare data

	if (m_iCurrentTracer < 0)		// no arms, no cookies...
	{
		m_dLastVisTime = m_dVisTime;
		m_dVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
		return;
	}

	bool bAnimationPlaying = GetRenderNode()->GetVisTiming()->GetAnimationPlaying();
	if (bAnimationPlaying
		&& (GetProperties()->GetUseFixedDeltaTime()
		|| m_vecTracers[m_iCurrentTracer]->GetTracerType() == TT_TRACERS))
	{
		VveTimeMapper *pMapper = NULL;
		if (m_eGridType == GT_CARTESIAN_GRID)
			pMapper = m_uData.pcg->GetTimeMapper();
		else 
			pMapper = m_uData.ptg->GetTimeMapper();

		float fSimDeltaT = GetProperties()->GetFixedDeltaTime();
		double dVisDeltaT = pMapper->GetVisualizationTime(pMapper->GetSimulationTime((float)0)+fSimDeltaT);
		if (dVisDeltaT<0)
		{
			// I have a bad feeling about this...
			cout << " [VflVisGpuParticles] - WARNING - negative delta t, stopping animation..." << endl;
			// bail out...
			return;
		}

		double dCurrentVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
		while (m_dVisTime + dVisDeltaT < dCurrentVisTime
			|| (dCurrentVisTime < m_dVisTime && m_dVisTime+dVisDeltaT-1.0f < dCurrentVisTime))
		{
			m_dLastVisTime = m_dVisTime;
			m_dVisTime += dVisDeltaT;
			if (m_dVisTime > 1.0f)
				m_dVisTime -= 1.0f;

			if (m_dVisTime < m_dLastVisTime
				&& !GetProperties()->GetWrapAround())
			{
				m_pParticles->Reset();
			}

			// if we're dealing with a GPU-based tracer, keep texture data up-to-date
			if (m_vecTracers[m_iCurrentTracer]->GetProcessingUnit() == PU_GPU)
			{
				m_pParticles->SynchToGpu();
				m_pPusher->Update(GetRenderNode());
			}
			else
				m_pParticles->SynchToCpu();

			m_vecTracers[m_iCurrentTracer]->Update();

			// If we're benchmarking (read: we don't want to have thousands of
			// particles buzzing around like there's no tomorrow), we revoke 
			// the buffer swap and (for tracer computation) advancing the head line.
			if (GetProperties()->GetBenchmarkMode() && !GetProperties()->GetSingleBuffered())
			{
				m_pParticles->Swap();
				if (m_vecTracers[m_iCurrentTracer]->GetTracerType() == TT_TRACERS)
				{
					int iLine = m_pParticles->GetCurrentLine()-1;
					if (iLine<0)
					{
						int iWidth, iHeight;
						m_pParticles->GetSize(iWidth, iHeight);
						iLine = iHeight-1;
					}
					m_pParticles->SetCurrentLine(iLine);
				}
			}

			if (dVisDeltaT==0)
			{
				cout << " [VflVisGpuParticles] - WARNING - fixed delta t is zero..." << endl;
				break;
			}
		}
	}
	else
	{
		if (GetProperties()->GetBenchmarkMode())
		{
			// if we're benchmarking, stop the animation...
			GetRenderNode()->GetVisTiming()->SetVisualizationTime(m_dVisTime);
		}
		else
		{
			m_dLastVisTime = m_dVisTime;
			m_dVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
		}

		if (bAnimationPlaying && GetProperties()->GetActive())
		{
			double dDeltaVisTime = m_dVisTime - m_dLastVisTime;

			if (m_dVisTime < m_dLastVisTime)
			{
				m_bTimeWarning = false;

				if (!GetProperties()->GetWrapAround())
					m_pParticles->Reset();

				while (dDeltaVisTime < 0.0)
				{
					++dDeltaVisTime;
				}
			}

			VveTimeMapper *pMapper = NULL;
			if (m_eGridType == GT_CARTESIAN_GRID)
				pMapper = m_uData.pcg->GetTimeMapper();
			else 
				pMapper = m_uData.ptg->GetTimeMapper();

			// if the animation is playing, test whether we are fast enough to 
			// follow with the particle integration
			double dDeltaSimTime = pMapper->GetSimulationTime(dDeltaVisTime)
				- pMapper->GetSimulationTime(0);
			if (dDeltaSimTime > GetProperties()->GetMaxDeltaTime())
			{
				if (!m_bTimeWarning)
				{
					m_bTimeWarning = true;
					cout << " [VflVisGpuParticles] - WARNING - integration too slow for animation..." << endl;
				}
			}
			else
				m_bTimeWarning = false;
		}

		// if we're dealing with a GPU-based tracer, keep texture data up-to-date
		if (m_vecTracers[m_iCurrentTracer]->GetProcessingUnit() == PU_GPU)
		{
			m_pParticles->SynchToGpu();
			m_pPusher->Update(GetRenderNode());

			if (GetProperties()->GetForceDataPreparation())
				m_pParticles->SignalGpuDataChange();
		}
		else
			m_pParticles->SynchToCpu();

		m_vecTracers[m_iCurrentTracer]->Update();

		// If we're benchmarking (read: we don't want to have thousands of
		// particles buzzing around like there's no tomorrow), we revoke 
		// the buffer swap and (for tracer computation) advancing the head line.
		if (GetProperties()->GetBenchmarkMode() && !GetProperties()->GetSingleBuffered())
		{
			m_pParticles->Swap();
			if (m_vecTracers[m_iCurrentTracer]->GetTracerType() == TT_TRACERS)
			{
				int iLine = m_pParticles->GetCurrentLine()-1;
				if (iLine<0)
				{
					int iWidth, iHeight;
					m_pParticles->GetSize(iWidth, iHeight);
					iLine = iHeight-1;
				}
				m_pParticles->SetCurrentLine(iLine);
			}
		}
	}

	if (m_iCurrentRenderer >= 0)
		m_vecRenderers[m_iCurrentRenderer]->Update();

	if (GetProperties()->GetDrawOverlay())
		m_pParticles->SynchToGpu();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticles::DrawOpaque()
{
	if (!GetVisible())
		return;

	if (m_iCurrentRenderer >= 0)
		m_vecRenderers[m_iCurrentRenderer]->DrawOpaque();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticles::DrawTransparent()
{
	if (!GetVisible())
		return;

	if (m_iCurrentRenderer >= 0)
		m_vecRenderers[m_iCurrentRenderer]->DrawTransparent();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticles::Draw2D()
{
	if (!GetVisible())
		return;

	if (m_iCurrentRenderer >= 0)
		m_vecRenderers[m_iCurrentRenderer]->Draw2D();

	if (GetProperties()->GetDrawOverlay())
	{
		m_pParticles->Draw();

		if (GetProperties()->GetAllowSorting())
			m_pSorter->Draw();

		// draw texture
		VflLookupTexture *pSharedLut = GetProperties()->GetLookupTexture();
		if (pSharedLut)
		{

			glPushAttrib(GL_ALL_ATTRIB_BITS);
			glDisable(GL_LIGHTING);
			glDisable(GL_ALPHA_TEST);
			glDisable(GL_DEPTH_TEST);
			glDisable(GL_BLEND);
			glDisable(GL_CULL_FACE);
			glColor4f(1, 1, 1, 1);

			glPushMatrix();
			glLoadIdentity();
			glMatrixMode(GL_PROJECTION);
			glPushMatrix();
			glLoadIdentity();
			gluOrtho2D(0, 1, 0, 1);

			pSharedLut->GetLookupTexture()->Enable();
			pSharedLut->GetLookupTexture()->Bind();

			glBegin(GL_QUADS);
			glTexCoord1f(0);
			glVertex2f(0.95f, 0.8f);
			glTexCoord1f(0);
			glVertex2f(1.0f, 0.8f);
			glTexCoord1f(1.0f);
			glVertex2f(1.0f, 1.0f);
			glTexCoord1f(1.0f);
			glVertex2f(0.95f, 1.0f);
			glEnd();

			glPopMatrix();
			glMatrixMode(GL_MODELVIEW);
			glPopMatrix();
			glPopAttrib();		
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflVisGpuParticles::GetRegistrationMode() const
{
	return OLI_ALL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	// TODO: think about adding bounding box information to the texture pusher...
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVisGpuParticles::GetType() const
{
	return IVflVisObject::GetType() + std::string("::VflVisGpuParticles");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticles::Debug(std::ostream &out) const
{
	IVflVisObject::Debug(out);
	cout << " [VflVisGpuParticles] - grid type: " 
		<< (m_eGridType==GT_CARTESIAN_GRID ? "cartesian grid" : "tetrahedral grid") << endl;
	cout << " [VflVisGpuParticles] - grid data:        " << m_uData.pv << endl;
	cout << " [VflVisGpuParticles] - properties object: " << GetProperties() << endl;
	cout << " [VflVisGpuParticles] - particle data:    " << m_pParticles << endl;
	cout << " [VflVisGpuParticles] - texture pusher:   " << m_pPusher << endl;
	cout << " [VflVisGpuParticles] - particle sorter:  " << m_pSorter << endl;
	cout << " [VflVisGpuParticles] - tracer count:     " << m_vecTracers.size() << endl;
	cout << " [VflVisGpuParticles] - current tracer:   " << m_iCurrentTracer << endl;
	cout << " [VflVisGpuParticles] - renderer count:   " << m_vecRenderers.size() << endl;
	cout << " [VflVisGpuParticles] - current renderer: " << m_iCurrentRenderer << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticles::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	// we expect notifications from the particle object, only...
	if (pObserveable == static_cast<IVistaObserveable *>(GetProperties()))
	{
		m_pPusher->SetLookAheadCount(GetProperties()->GetLookAheadCount());
		m_pPusher->SetWrapAround(GetProperties()->GetWrapAround());
		m_pPusher->SetStreamingUpdate(GetProperties()->GetStreamingUpdate());
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDataType                                                 */
/*                                                                            */
/*============================================================================*/
int VflVisGpuParticles::GetDataType() const
{
	return m_eGridType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData[As*Grid]                                            */
/*                                                                            */
/*============================================================================*/
VveUnsteadyData *VflVisGpuParticles::GetData() const
{
	if (m_eGridType == GT_CARTESIAN_GRID)
		return m_uData.pcg;
	else if (m_eGridType == GT_TET_GRID)
		return m_uData.ptg;
	else
		return NULL;
}

VveUnsteadyCartesianGrid *VflVisGpuParticles::GetDataAsCartesianGrid() const
{
	if (m_eGridType == GT_CARTESIAN_GRID)
		return m_uData.pcg;
	else
		return NULL;
}

VveUnsteadyTetGrid *VflVisGpuParticles::GetDataAsTetGrid() const
{
	if (m_eGridType == GT_TET_GRID)
		return m_uData.ptg;
	else
		return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPusher                                                   */
/*                                                                            */
/*============================================================================*/
VflUnsteadyTexturePusher *VflVisGpuParticles::GetPusher() const
{
	return m_pPusher;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VflVisGpuParticles::VflVisGpuParticlesProperties *VflVisGpuParticles::GetProperties() const
{
	return static_cast<VflVisGpuParticlesProperties*>(
		IVflRenderable::GetProperties());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetParticleData                                             */
/*                                                                            */
/*============================================================================*/
VflGpuParticleData *VflVisGpuParticles::GetParticleData() const
{
	return m_pParticles;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetParticleSorter                                           */
/*                                                                            */
/*============================================================================*/
VflGpuParticleSorter *VflVisGpuParticles::GetParticleSorter() const
{
	return m_pSorter;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get[Last]VisTime                                            */
/*                                                                            */
/*============================================================================*/
double VflVisGpuParticles::GetVisTime() const
{
	return m_dVisTime;
}

double VflVisGpuParticles::GetLastVisTime() const
{
	return m_dLastVisTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Add/Remove/GetTracer                                        */
/*                                                                            */
/*============================================================================*/
int VflVisGpuParticles::AddTracer(IVflGpuParticleTracer *pTracer)
{
	if (!pTracer)
		return -1;

	int iIndex = (int) m_vecTracers.size();
	m_vecTracers.resize(iIndex+1);
	m_vecTracers[iIndex] = pTracer;

	return iIndex;
}

bool VflVisGpuParticles::RemoveTracer(int iIndex)
{
	if (iIndex<0 || iIndex>=int(m_vecTracers.size()))
		return false;

	for (int i=iIndex; i<int(m_vecTracers.size())-1; ++i)
		m_vecTracers[i] = m_vecTracers[i+1];

	m_vecTracers.resize(m_vecTracers.size()-1);

	if (m_vecTracers.empty())
		m_iCurrentTracer = -1;
	else
		SetCurrentTracer(0);

	return true;
}

IVflGpuParticleTracer *VflVisGpuParticles::GetTracer(int iIndex)
{
	if (iIndex<0 || iIndex>=int(m_vecTracers.size()))
		return NULL;

	return m_vecTracers[iIndex];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetCurrentTracer                                        */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::SetCurrentTracer(int iIndex)
{
	if (iIndex<0 || iIndex>=int(m_vecTracers.size()))
		return false;

	// reset particles if tracer type changes
	IVflGpuParticleTracer *pOldTracer = GetTracer(m_iCurrentTracer);
	IVflGpuParticleTracer *pNewTracer = GetTracer(iIndex);

	int iOldType = pOldTracer ? pOldTracer->GetTracerType() : TT_INVALID;
	int iNewType = pNewTracer ? pNewTracer->GetTracerType() : TT_INVALID;
	if (iOldType != iNewType)
	{
		m_pParticles->Reset();
	}

	m_iCurrentTracer = iIndex;

	if (GetProperties()->GetRespectProcessingUnit()
		|| GetProperties()->GetRespectTracerType())
		SynchTracerAndRenderer();

	return true;
}

int VflVisGpuParticles::GetCurrentTracer() const
{
	return m_iCurrentTracer;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerCount                                              */
/*                                                                            */
/*============================================================================*/
int VflVisGpuParticles::GetTracerCount() const
{
	return (int) m_vecTracers.size();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Add/Remove/GetRenderer                                      */
/*                                                                            */
/*============================================================================*/
int VflVisGpuParticles::AddRenderer(IVflGpuParticleRenderer *pRenderer)
{
	if (!pRenderer)
		return -1;

	int iIndex = (int) m_vecRenderers.size();
	m_vecRenderers.resize(iIndex+1);
	m_vecRenderers[iIndex] = pRenderer;

	return iIndex;
}

bool VflVisGpuParticles::RemoveRenderer(int iIndex)
{
	if (iIndex<0 || iIndex>=int(m_vecRenderers.size()))
		return false;

	for (int i=iIndex; i<int(m_vecRenderers.size())-1; ++i)
		m_vecRenderers[i] = m_vecRenderers[i+1];

	m_vecRenderers.resize(m_vecRenderers.size()-1);

	if (m_vecRenderers.empty())
		m_iCurrentRenderer = -1;
	else
		SetCurrentRenderer(0);

	return true;
}

IVflGpuParticleRenderer *VflVisGpuParticles::GetRenderer(int iIndex)
{
	if (iIndex<0 || iIndex>=int(m_vecRenderers.size()))
		return NULL;

	return m_vecRenderers[iIndex];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetCurrentRenderer                                      */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::SetCurrentRenderer(int iIndex)
{
	if (iIndex<0 || iIndex>=int(m_vecRenderers.size()))
		return false;

	m_iCurrentRenderer = iIndex;

	if (GetProperties()->GetRespectProcessingUnit()
		|| GetProperties()->GetRespectTracerType())
		SynchTracerAndRenderer();

	return true;
}

int VflVisGpuParticles::GetCurrentRenderer() const
{
	return m_iCurrentRenderer;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRendererCount                                            */
/*                                                                            */
/*============================================================================*/
int VflVisGpuParticles::GetRendererCount() const
{
	return (int) m_vecRenderers.size();
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SaveOGLState                                                */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticles::SaveOGLState()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);
	glLineWidth(1.0f);
	glPointSize(1.0f);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	// save old viewport state
	glGetIntegerv(GL_VIEWPORT, m_aViewportState);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RestoreOGLState                                             */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticles::RestoreOGLState()
{
	// restore old projection and modelview matrices
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	// restore old viewport
	glViewport(m_aViewportState[0], m_aViewportState[1], m_aViewportState[2], m_aViewportState[3]);

	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeWarning                                              */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::GetTimeWarning() const
{
	return m_bTimeWarning;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SynchTracerAndRenderer                                      */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticles::SynchTracerAndRenderer()
{
	if (m_iCurrentTracer<0 || m_vecRenderers.empty())
		return;

	if (m_iCurrentRenderer<0)
		m_iCurrentRenderer=0;

	bool bRespectPU = GetProperties()->GetRespectProcessingUnit();
	bool bRespectTT = GetProperties()->GetRespectTracerType();

	int iTracerPU = m_vecTracers[m_iCurrentTracer]->GetProcessingUnit();
	int iRendererPU = m_vecRenderers[m_iCurrentRenderer]->GetProcessingUnit();

	int iTracerTT = m_vecTracers[m_iCurrentTracer]->GetTracerType();
	int iRendererTT = m_vecRenderers[m_iCurrentRenderer]->GetTracerType();

	if ((iTracerPU == iRendererPU || !bRespectPU)
		&& (iTracerTT == iRendererTT || !bRespectTT))
	{
#ifdef DEBUG
		cout << "    processing units: tracer="
			<< (iTracerPU == PU_GPU ? "GPU" : "CPU") << ", renderer="
			<< (iRendererPU == PU_GPU ? "GPU" : "CPU")
			<< (bRespectPU ? " - OK":" - IGNORED") << endl;
		cout << "    tracer types: tracer="
			<< (iTracerTT == TT_PARTICLES ? "PARTICLES" : "TRACERS") << ", renderer="
			<< (iRendererTT == TT_PARTICLES ? "PARTICLES" : "TRACERS")
			<< (bRespectTT ? " - OK":" - IGNORED") << endl;
#endif
		return;
	}

	cout << " [VflVisGpuParticles] - synchronizing tracer and renderer..." << endl;
	if (iTracerPU != iRendererPU && bRespectPU)
	{

		cout << "    processing unit mismatch..." << endl;
		cout << "    tracer running on " 
			<< (iTracerPU == PU_GPU ? "GPU" : "CPU") << endl;
		cout << "    renderer running on "
			<< (iRendererPU == PU_GPU ? "GPU" : "CPU") << endl;
	}

	if (iTracerTT != iRendererTT && bRespectTT)
	{
		cout << "    tracer type mismatch..." << endl;
		cout << "    tracer computing " 
			<< (iTracerTT == TT_PARTICLES ? "PARTICLES" : "TRACERS") << endl;
		cout << "    renderer displaying "
			<< (iRendererTT == TT_PARTICLES ? "PARTICLES" : "TRACERS") << endl;
	}

	int iRenderer = (m_iCurrentRenderer+1) % (int) m_vecRenderers.size();
	while (iRenderer != m_iCurrentRenderer)
	{
		if ((m_vecRenderers[iRenderer]->GetProcessingUnit() == iTracerPU || !bRespectPU)
			&& (m_vecRenderers[iRenderer]->GetTracerType() == iTracerTT || !bRespectTT))
			break;
		iRenderer = (iRenderer+1)%(int)m_vecRenderers.size();
	}

	if (iRenderer != m_iCurrentRenderer)
	{
		m_iCurrentRenderer = iRenderer;
		cout << " [VflVisGpuParticles] - switching to renderer " << m_iCurrentRenderer << endl;
		cout << "                        name: " 
			<< m_vecRenderers[m_iCurrentRenderer]->GetNameForNameable() << endl;
		cout << "                        type: " 
			<< m_vecRenderers[m_iCurrentRenderer]->GetReflectionableType() << endl;
	}
	else
	{
		cout << " [VflVisGpuParticles] - WARNING - unable to find renderer for ";
		if (bRespectPU)
			cout << (iTracerPU == PU_GPU ? "GPU " : "CPU ");
		if (bRespectPU && bRespectTT)
			cout << "and ";
		if (bRespectTT)
			cout << (iTracerTT == TT_PARTICLES ? "PARTICLES" : "TRACERS");
		cout << endl;
	}
}

// #############################################################################
// ################### VflVisGpuParticlesProperties ############################
// #############################################################################

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflVisGpuParticlesPropertiesReflType("VflVisGpuParticlesProperties");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<std::list<int>, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("PARTICLE_TEXTURE_SIZE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetParticleTextureSizeAsList),
	new TVistaPropertyGet<int,  VflVisGpuParticles::VflVisGpuParticlesProperties>
		("LOOK_AHEAD_COUNT", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetLookAheadCount),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("WRAP_AROUND", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetWrapAround),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("STREAMING_UPDATE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetStreamingUpdate),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("ACTIVE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetActive),
	new TVistaPropertyGet<std::string, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("INTEGRATOR", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetIntegratorAsString),
	new TVistaPropertyGet<float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("DELTA_TIME", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetDeltaTime),
	new TVistaPropertyGet<float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("MAX_DELTA_TIME", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetMaxDeltaTime),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("USE_FIXED_DELTA_TIME", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetUseFixedDeltaTime),
	new TVistaPropertyGet<float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("FIXED_DELTA_TIME", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetFixedDeltaTime),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("ADAPTIVE_TIME_STEPS", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetAdaptiveTimeSteps),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("COMPUTE_SCALARS", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetComputeScalars),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("SINGLE_BUFFERED", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetSingleBuffered),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("FORCE_HARDWARE_INTERPOLATION", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetForceHardwareInterpolation),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("BENCHMARK_MODE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetBenchmarkMode),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("VISIBLE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetVisible),
	new TVistaPropertyGet<VistaColor, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("COLOR", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetColor),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("USE_LOOKUP_TABLE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetUseLookupTable),
	new TVistaPropertyGet<float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("RADIUS", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetRadius),
	new TVistaPropertyGet<float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("POINT_SIZE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetPointSize),
	new TVistaPropertyGet<float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("LINE_WIDTH", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetLineWidth),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("DECREASE_LUMINANCE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetDecreaseLuminance),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("DRAW_OVERLAY", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetDrawOverlay),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("ALLOW_SORTING", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetAllowSorting),
	new TVistaPropertyGet<int, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("MAX_SORTING_STEPS", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetMaxSortingSteps),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("HIGH_PRECISION_SORTING", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetHighPrecisionSorting),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("FORCE_DATA_PREPARATION", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetForceDataPreparation),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("RESPECT_PROCESSING_UNIT", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetRespectProcessingUnit),
	new TVistaPropertyGet<bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("RESPECT_TRACER_TYPE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::GetRespectTracerType),
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<const list<int> &, list<int>, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("PARTICLE_TEXTURE_SIZE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetParticleTextureSize),
	new TVistaPropertySet<int, int, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("LOOK_AHEAD_COUNT", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetLookAheadCount),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("WRAP_AROUND", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetWrapAround),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("STREAMING_UPDATE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetStreamingUpdate),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("ACTIVE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetActive),
	new TVistaPropertySet<const std::string&, std::string, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("INTEGRATOR", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetIntegrator),
	new TVistaPropertySet<float, float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("DELTA_TIME", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetDeltaTime),
	new TVistaPropertySet<float, float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("MAX_DELTA_TIME", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetMaxDeltaTime),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("USE_FIXED_DELTA_TIME", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetUseFixedDeltaTime),
	new TVistaPropertySet<float, float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("FIXED_DELTA_TIME", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetFixedDeltaTime),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("ADAPTIVE_TIME_STEPS", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetAdaptiveTimeSteps),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("COMPUTE_SCALARS", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetComputeScalars),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("SINGLE_BUFFERED", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetSingleBuffered),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("FORCE_HARDWARE_INTERPOLATION", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetForceHardwareInterpolation),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("BENCHMARK_MODE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetBenchmarkMode),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("VISIBLE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetVisible),
	new TVistaPropertySet<const VistaColor&, VistaColor, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("COLOR", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetColor),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("USE_LOOKUP_TABLE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetUseLookupTable),
	new TVistaPropertySet<float, float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("RADIUS", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetRadius),
	new TVistaPropertySet<float, float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("POINT_SIZE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetPointSize),
	new TVistaPropertySet<float, float, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("LINE_WIDTH", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetLineWidth),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("DECREASE_LUMINANCE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetDecreaseLuminance),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("DRAW_OVERLAY", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetDrawOverlay),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("ALLOW_SORTING", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetAllowSorting),
	new TVistaPropertySet<int, int, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("MAX_SORTING_STEPS", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetMaxSortingSteps),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("HIGH_PRECISION_SORTING", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetHighPrecisionSorting),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("FORCE_DATA_PREPARATION", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetForceDataPreparation),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("RESPECT_PROCESSING_UNIT", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetRespectProcessingUnit),
	new TVistaPropertySet<bool, bool, VflVisGpuParticles::VflVisGpuParticlesProperties>
		("RESPECT_TRACER_TYPE", s_strCVflVisGpuParticlesPropertiesReflType,
		&VflVisGpuParticles::VflVisGpuParticlesProperties::SetRespectTracerType),
	NULL
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflVisGpuParticles::VflVisGpuParticlesProperties::VflVisGpuParticlesProperties()
: m_pLookupTable(NULL),
  m_pLookupTexture(NULL),
  m_bManageLut(false),
  m_bManageTexture(false),
  m_iWidth(1),
  m_iHeight(1),
  m_iLookAheadCount(0),
  m_bWrapAround(false),
  m_bStreamingUpdate(false),
  m_bActive(true),
  m_iIntegrator(IR_INVALID),
  m_fDeltaTime(0),
  m_fMaxDeltaTime(numeric_limits<float>::max()),
  m_bUseFixedDeltaTime(false),
  m_fFixedDeltaTime(0),
  m_bAdaptiveTimeSteps(false),
  m_bComputeScalars(false),
  m_bSingleBuffered(false),
  m_bForceHardwareInterpolation(false),
  m_bBenchmarkMode(false),
  m_bVisible(true),
  m_bUseLookupTable(false),
  m_fRadius(1.0f),
  m_fPointSize(1.0f),
  m_fLineWidth(1.0f),
  m_bDecreaseLuminance(false),
  m_bDrawOverlay(false),
  m_bAllowSorting(false),
  m_iMaxSortingSteps(-1),
  m_bHighPrecisionSorting(false),
  m_bForceDataPreparation(false),
  m_bRespectProcessingUnit(false),
  m_bRespectTracerType(false)
{
	if (!m_pLookupTable)
	{
		m_pLookupTable = new VflVtkLookupTable;
		m_bManageLut = true;
	}

	if (!m_pLookupTexture)
	{
		m_pLookupTexture = new VflLookupTexture(m_pLookupTable);
		m_bManageTexture = true;
	}
}

VflVisGpuParticles::VflVisGpuParticlesProperties::VflVisGpuParticlesProperties(VflVtkLookupTable *pLut)
: m_pLookupTable(pLut),
  m_pLookupTexture(NULL),
  m_bManageLut(false),
  m_bManageTexture(false),
  m_iWidth(1),
  m_iHeight(1),
  m_iLookAheadCount(0),
  m_bWrapAround(false),
  m_bStreamingUpdate(false),
  m_bActive(true),
  m_iIntegrator(IR_INVALID),
  m_fDeltaTime(0),
  m_fMaxDeltaTime(numeric_limits<float>::max()),
  m_bUseFixedDeltaTime(false),
  m_fFixedDeltaTime(0),
  m_bAdaptiveTimeSteps(false),
  m_bComputeScalars(false),
  m_bSingleBuffered(false),
  m_bForceHardwareInterpolation(false),
  m_bBenchmarkMode(false),
  m_bVisible(true),
  m_bUseLookupTable(false),
  m_fRadius(1.0f),
  m_fPointSize(1.0f),
  m_fLineWidth(1.0f),
  m_bDecreaseLuminance(false),
  m_bDrawOverlay(false),
  m_bAllowSorting(false),
  m_iMaxSortingSteps(-1),
  m_bHighPrecisionSorting(false),
  m_bForceDataPreparation(false),
  m_bRespectProcessingUnit(false),
  m_bRespectTracerType(false)
{

	if (!m_pLookupTexture)
	{
		m_pLookupTexture = new VflLookupTexture(m_pLookupTable);
		m_bManageTexture = true;
	}
}

VflVisGpuParticles::VflVisGpuParticlesProperties::~VflVisGpuParticlesProperties()
{
	if (m_bManageTexture)
		delete m_pLookupTexture;
	m_pLookupTexture = NULL;

	if (m_bManageLut)
		delete m_pLookupTable;
	
	m_pLookupTable = NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTable                                              */
/*                                                                            */
/*============================================================================*/
VflVtkLookupTable	*VflVisGpuParticles::VflVisGpuParticlesProperties::GetLookupTable() const
{
	return m_pLookupTable;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTexture                                            */
/*                                                                            */
/*============================================================================*/
VflLookupTexture *VflVisGpuParticles::VflVisGpuParticlesProperties::GetLookupTexture() const
{
	return m_pLookupTexture;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetParticleTextureSize                                  */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetParticleTextureSize(int iWidth, int iHeight)
{
	if (m_iWidth != iWidth || m_iHeight != iHeight)
	{
		m_iWidth = iWidth;
		m_iHeight = iHeight;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetParticleTextureSize(const std::list<int> &liSize)
{
	if (liSize.size() != 2)
		return false;

	list<int>::const_iterator it=liSize.begin();
	int iWidth = *(it++);
	int iHeight = *it;
	return SetParticleTextureSize(iWidth, iHeight);
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetParticleTextureSize(int &iWidth, int &iHeight) const
{
	iWidth = m_iWidth;
	iHeight = m_iHeight;
	return true;
}

std::list<int> VflVisGpuParticles::VflVisGpuParticlesProperties::GetParticleTextureSizeAsList() const
{
	list<int> liTemp;
	liTemp.push_back(m_iWidth);
	liTemp.push_back(m_iHeight);
	return liTemp;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLookAheadCount                                       */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetLookAheadCount(int iCount)
{
	if (iCount != m_iLookAheadCount)
	{
		m_iLookAheadCount = iCount;
		Notify();
		return true;
	}
	return false;
}

int VflVisGpuParticles::VflVisGpuParticlesProperties::GetLookAheadCount() const
{
	return m_iLookAheadCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetWrapAround                                           */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetWrapAround(bool bWrap)
{
	if (bWrap != m_bWrapAround)
	{
		m_bWrapAround = bWrap;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetWrapAround() const
{
	return m_bWrapAround;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetStreamingUpdate                                      */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetStreamingUpdate(bool bStream)
{
	if (bStream != m_bStreamingUpdate)
	{
		m_bStreamingUpdate = bStream;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetStreamingUpdate() const
{
	return m_bStreamingUpdate;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetActive                                               */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetActive(bool bActive)
{
	if (bActive != m_bActive)
	{
		m_bActive = bActive;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetActive() const
{
	return m_bActive;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIntegrator[AsString]                                 */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetIntegrator(int iIntegrator)
{
	if (iIntegrator != m_iIntegrator)
	{
		m_iIntegrator = iIntegrator;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetIntegrator(const std::string &strIntegrator)
{
	string strTemp = VistaAspectsConversionStuff::ConvertToLower(strIntegrator);
	int iSeedMode = IR_INVALID;
	if (strTemp == "euler")
		iSeedMode = IR_EULER;
	else if (strTemp == "rk3")
		iSeedMode = IR_RK3;
	else if (strTemp == "rk4")
		iSeedMode = IR_RK4;

	return SetIntegrator(iSeedMode);
}


int VflVisGpuParticles::VflVisGpuParticlesProperties::GetIntegrator() const
{
	return m_iIntegrator;
}

std::string VflVisGpuParticles::VflVisGpuParticlesProperties::GetIntegratorAsString() const
{
	switch (m_iIntegrator)
	{
	case IR_INVALID:
		return "INVALID";
	case IR_EULER:
		return "EULER";
	case IR_RK3:
		return "RK3";
	case IR_RK4:
		return "RK4";
	}

	return "UNKNOWN";
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDeltaTime                                            */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetDeltaTime(float fTime)
{
	if (fTime != m_fDeltaTime)
	{
		m_fDeltaTime = fTime;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticles::VflVisGpuParticlesProperties::GetDeltaTime() const
{
	return m_fDeltaTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMaxDeltaTime                                            */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetMaxDeltaTime(float fTime)
{
	if (fTime != m_fMaxDeltaTime)
	{
		m_fMaxDeltaTime = fTime;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticles::VflVisGpuParticlesProperties::GetMaxDeltaTime() const
{
	return m_fMaxDeltaTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetUseFixedDeltaTime                                    */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetUseFixedDeltaTime(bool bFixed)
{
	if (bFixed != m_bUseFixedDeltaTime)
	{
		m_bUseFixedDeltaTime = bFixed;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetUseFixedDeltaTime() const
{
	return m_bUseFixedDeltaTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFixedDeltaTime                                       */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetFixedDeltaTime(float fTime)
{
	if (fTime != m_fFixedDeltaTime)
	{
		m_fFixedDeltaTime = fTime;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticles::VflVisGpuParticlesProperties::GetFixedDeltaTime() const
{
	return m_fFixedDeltaTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetAdaptiveTimeSteps                                    */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetAdaptiveTimeSteps(bool bAdaptive)
{
	if (bAdaptive != m_bAdaptiveTimeSteps)
	{
		m_bAdaptiveTimeSteps = bAdaptive;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetAdaptiveTimeSteps() const
{
	return m_bAdaptiveTimeSteps;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetComputeScalars                                       */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetComputeScalars(bool bScalars)
{
	if (bScalars != m_bComputeScalars)
	{
		m_bComputeScalars = bScalars;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetComputeScalars() const
{
	return m_bComputeScalars;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetSingleBuffered                                       */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetSingleBuffered(bool bSingle)
{
	if (bSingle != m_bSingleBuffered)
	{
		m_bSingleBuffered = bSingle;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetSingleBuffered() const
{
	return m_bSingleBuffered;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetForceHardwareInterpolation                           */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetForceHardwareInterpolation(bool bForce)
{
	if (bForce != m_bForceHardwareInterpolation)
	{
		m_bForceHardwareInterpolation = bForce;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetForceHardwareInterpolation() const
{
	return m_bForceHardwareInterpolation;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetBenchmarkMode                                        */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetBenchmarkMode(bool bBench)
{
	if (bBench != m_bBenchmarkMode)
	{
		m_bBenchmarkMode = bBench;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetBenchmarkMode() const
{
	return m_bBenchmarkMode;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetVisible                                              */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetVisible(bool bVisible)
{
	if (bVisible != m_bVisible)
	{
		m_bVisible = bVisible;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetVisible() const
{
	return m_bVisible;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetColor                                                */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetColor(const VistaColor &v3Color)
{
	if (v3Color != m_oColor)
	{
		m_oColor = v3Color;
		Notify();
		return true;
	}
	return false;
}

VistaColor VflVisGpuParticles::VflVisGpuParticlesProperties::GetColor() const
{
	return m_oColor;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetUseLookupTable                                       */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetUseLookupTable(bool bUseLut)
{
	if (bUseLut != m_bUseLookupTable)
	{
		m_bUseLookupTable = bUseLut;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetUseLookupTable() const
{
	return m_bUseLookupTable;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRadius                                               */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetRadius(float fRadius)
{
	if (fRadius != m_fRadius)
	{
		m_fRadius = fRadius;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticles::VflVisGpuParticlesProperties::GetRadius() const
{
	return m_fRadius;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetPointSize                                            */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetPointSize(float fPointSize)
{
	if (fPointSize != m_fPointSize)
	{
		m_fPointSize = fPointSize;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticles::VflVisGpuParticlesProperties::GetPointSize() const
{
	return m_fPointSize;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLineWidth                                            */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetLineWidth(float fLineWidth)
{
	if (fLineWidth != m_fLineWidth)
	{
		m_fLineWidth = fLineWidth;
		Notify();
		return true;
	}
	return false;
}

float VflVisGpuParticles::VflVisGpuParticlesProperties::GetLineWidth() const
{
	return m_fLineWidth;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDecreaseLuminance                                    */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetDecreaseLuminance(bool bDecrease)
{
	if (bDecrease != m_bDecreaseLuminance)
	{
		m_bDecreaseLuminance = bDecrease;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetDecreaseLuminance() const
{
	return m_bDecreaseLuminance;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDrawOverlay                                          */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetDrawOverlay(bool bOverlay)
{
	if (bOverlay != m_bDrawOverlay)
	{
		m_bDrawOverlay = bOverlay;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetDrawOverlay() const
{
	return m_bDrawOverlay;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetAllowSorting                                         */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetAllowSorting(bool bSort)
{
	if (bSort != m_bAllowSorting)
	{
		m_bAllowSorting = bSort;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetAllowSorting() const
{
	return m_bAllowSorting;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMaxSortingSteps                                      */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetMaxSortingSteps(int iMax)
{
	if (iMax != m_iMaxSortingSteps)
	{
		m_iMaxSortingSteps = iMax;
		Notify();
		return true;
	}
	return false;
}

int VflVisGpuParticles::VflVisGpuParticlesProperties::GetMaxSortingSteps() const
{
	return m_iMaxSortingSteps;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHighPrecisionSorting                                 */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetHighPrecisionSorting(bool bSort)
{
	if (bSort != m_bHighPrecisionSorting)
	{
		m_bHighPrecisionSorting = bSort;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetHighPrecisionSorting() const
{
	return m_bHighPrecisionSorting;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetForceDataPreparation                                 */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetForceDataPreparation(bool bForce)
{
	if (bForce != m_bForceDataPreparation)
	{
		m_bForceDataPreparation = bForce;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetForceDataPreparation() const
{
	return m_bForceDataPreparation;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRespectProcessingUnit                                */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetRespectProcessingUnit(bool bRPU)
{
	if (bRPU != m_bRespectProcessingUnit)
	{
		m_bRespectProcessingUnit = bRPU;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetRespectProcessingUnit() const
{
	return m_bRespectProcessingUnit;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRespectTracerType                                    */
/*                                                                            */
/*============================================================================*/
bool VflVisGpuParticles::VflVisGpuParticlesProperties::SetRespectTracerType(bool bRTT)
{
	if (bRTT != m_bRespectTracerType)
	{
		m_bRespectTracerType = bRTT;
		Notify();
		return true;
	}
	return false;
}

bool VflVisGpuParticles::VflVisGpuParticlesProperties::GetRespectTracerType() const
{
	return m_bRespectTracerType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflVisGpuParticles::VflVisGpuParticlesProperties::Debug(std::ostream &out) const
{
	out << " [VflVisGpuParticlesProperties] - particle texture size: " << m_iWidth 
		<< "x" << m_iHeight << endl;
	out << " [VflVisGpuParticlesProperties] - look ahead count:     " << m_iLookAheadCount << endl;
	out << " [VflVisGpuParticlesProperties] - wrap around:          " << (m_bWrapAround ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - streaming update:     " << (m_bStreamingUpdate ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - active:               " << (m_bActive ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - integrator:           " << GetIntegratorAsString() << endl;
	out << " [VflVisGpuParticlesProperties] - delta time:           " << m_fDeltaTime << endl;
	out << " [VflVisGpuParticlesProperties] - max delta time:       " << m_fMaxDeltaTime << endl;
	out << " [VflVisGpuParticlesProperties] - use fixed delta time: " << (m_bUseFixedDeltaTime ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - fixed delta time:     " << m_fFixedDeltaTime << endl;
	out << " [VflVisGpuParticlesProperties] - adaptive time steps:  " << (m_bAdaptiveTimeSteps? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - compute scalars:      " << (m_bComputeScalars ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - single buffered:      " << (m_bSingleBuffered ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - force hw interp.:     " << (m_bForceHardwareInterpolation ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - visible:              " << (m_bVisible ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - color:                " << m_oColor[0] << ", " << m_oColor[1] 
		<< ", " << m_oColor[2] << ", " << m_oColor[3] << endl;
	out << " [VflVisGpuParticlesProperties] - use lookup table:     " << (m_bUseLookupTable ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - radius:               " << m_fRadius << endl;
	out << " [VflVisGpuParticlesProperties] - point size:           " << m_fPointSize << endl;
	out << " [VflVisGpuParticlesProperties] - line width:           " << m_fLineWidth << endl;
	out << " [VflVisGpuParticlesProperties] - decrease luminance:   " << (m_bDecreaseLuminance ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - draw overlay:         " << (m_bDrawOverlay ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - allow sorting:        " << (m_bAllowSorting ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - max sorting steps:    " << m_iMaxSortingSteps << endl;
	out << " [VflVisGpuParticlesProperties] - high precision sort:  " << (m_bHighPrecisionSorting ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - respect processor:    " << (m_bRespectProcessingUnit ? "true" : "false") << endl;
	out << " [VflVisGpuParticlesProperties] - respect tracer type:  " << (m_bRespectTracerType ? "true" : "false") << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflVisGpuParticles::VflVisGpuParticlesProperties::GetReflectionableType() const
{
	return s_strCVflVisGpuParticlesPropertiesReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflVisGpuParticles::VflVisGpuParticlesProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflVisGpuParticlesPropertiesReflType);
	return i+1;
}



// #############################################################################
// ################## End of VlfVisGpuParticlesProperties ######################
// #############################################################################

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
