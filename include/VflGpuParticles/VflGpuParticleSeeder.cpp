/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuParticleSeeder.cpp     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflGpuParticleSeeder.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleTracer.h"
#include <VistaFlowLibAux/VfaPointGenerator.h>

/*============================================================================*/
/* IMPLEMENTATION	                                                          */
/*============================================================================*/
VflGpuParticleSeeder::VflGpuParticleSeeder( VflVisGpuParticles *pParent,
	IVfaPointGenerator *pGenerator )
:	m_pParent( pParent )
{
	SetPointGenerator(pGenerator); // cause of inheritance not in initlist
}

void VflGpuParticleSeeder::Seed()
{  
	IVflGpuParticleTracer *pTracer = m_pParent->GetTracer(m_pParent->GetCurrentTracer());	// get Tracer
	std::vector<float> pnts;
	GetPointGenerator()->GetPoints( pnts );	// get Points
	pTracer->SeedParticles( pnts );	// put together....

  //std::cout << "[VflGpuParticleSeeder] Seed" << std::endl; // debug
}


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

