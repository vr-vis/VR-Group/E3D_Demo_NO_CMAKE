/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuParticleRendererI.cpp  RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*
* $Id$
*/

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflGpuParticleRendererInterface.h"
#include "VflVisGpuParticles.h"
#include <VistaAspects/VistaAspectsUtils.h>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strIVflGpuParticleRendererReflType("IVflGpuParticleRenderer");

static IVistaPropertyGetFunctor *s_aIVflGpuParticleRendererGetFunctors[] =
{
	new TVistaPropertyGet<std::string, IVflGpuParticleRenderer>
		("DRAW_MODE", s_strIVflGpuParticleRendererReflType,
		&IVflGpuParticleRenderer::GetDrawModeAsString),
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aIVflGpuParticleRendererSetFunctors[] =
{
	new TVistaPropertySet<const std::string&, std::string, IVflGpuParticleRenderer>
		("DRAW_MODE", s_strIVflGpuParticleRendererReflType,
		&IVflGpuParticleRenderer::SetDrawMode),
	NULL
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
IVflGpuParticleRenderer::IVflGpuParticleRenderer(VflVisGpuParticles *pParent)
: m_pParent(pParent),
  m_iDrawMode(DM_INVALID)
{
	m_pParent->AddRenderer(this);
}

IVflGpuParticleRenderer::~IVflGpuParticleRenderer()
{
	// remove self from parent's renderer set
	for (int i=0; i<m_pParent->GetRendererCount(); ++i)
	{
		IVflGpuParticleRenderer *pRenderer = m_pParent->GetRenderer(i);
		if (pRenderer == this)
		{
			m_pParent->RemoveRenderer(i);
			break;
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void IVflGpuParticleRenderer::Update()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void IVflGpuParticleRenderer::DrawOpaque()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void IVflGpuParticleRenderer::DrawTransparent()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void IVflGpuParticleRenderer::Draw2D()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDrawMode*                                            */
/*                                                                            */
/*============================================================================*/
bool IVflGpuParticleRenderer::SetDrawMode(int iMode)
{
	int iNewMode = 0;
	if (iMode < 0)
		iNewMode = 0;
	else if (iMode >= GetDrawModeCount())
		iNewMode = GetDrawModeCount() - 1;
	else
		iNewMode = iMode;

	if (iNewMode == m_iDrawMode)
		return false;

	m_iDrawMode = iNewMode;
	Notify();

	return true;
}

bool IVflGpuParticleRenderer::SetDrawMode(const std::string &strMode)
{
	for (int i=0; i<GetDrawModeCount(); ++i)
	{
		if (strMode == GetDrawModeString(i))
		{
			return SetDrawMode(i);
		}
	}
	return false;
}

int IVflGpuParticleRenderer::GetDrawMode() const
{
	return m_iDrawMode;
}

std::string IVflGpuParticleRenderer::GetDrawModeAsString() const
{
	return GetDrawModeString(m_iDrawMode);
}

std::string IVflGpuParticleRenderer::GetDrawModeString(int iIndex) const
{
	return std::string("INVALID");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string IVflGpuParticleRenderer::GetReflectionableType() const
{
	return s_strIVflGpuParticleRendererReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int IVflGpuParticleRenderer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strIVflGpuParticleRendererReflType);
	return i+1;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
