/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuTracersCartGrid.cpp    RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflGpuTracersCartGrid.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include <VistaFlowLib/Data/VflUnsteadyCartesianGridPusher.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaVisExt/Data/VveCartesianGrid.h>
#include <VistaAspects/VistaAspectsUtils.h>

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaFramebufferObj.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

#include <set>
#include <string>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflGpuTracersCartGridReflType("VflGpuTracersCartGrid");

static IVistaPropertyGetFunctor *s_aCVflGpuTracersCartGridGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflGpuTracersCartGridSetFunctors[] =
{
	NULL
};

#define SHADER_STEADY_EULER				"VflGpuParticlesCart_Steady_Euler.glsl"
#define SHADER_STEADY_EULER_SCALARS		"VflGpuParticlesCart_Steady_EulerScalars.glsl"
#define SHADER_STEADY_RK3				"VflGpuParticlesCart_Steady_RK3.glsl"
#define SHADER_STEADY_RK3_SCALARS		"VflGpuParticlesCart_Steady_RK3Scalars.glsl"
#define SHADER_STEADY_RK4				"VflGpuParticlesCart_Steady_RK4.glsl"
#define SHADER_STEADY_RK4_SCALARS		"VflGpuParticlesCart_Steady_RK4Scalars.glsl"
#define SHADER_UNSTEADY_EULER			"VflGpuParticlesCart_Unsteady_Euler.glsl"
#define SHADER_UNSTEADY_EULER_SCALARS	"VflGpuParticlesCart_Unsteady_EulerScalars.glsl"
#define SHADER_UNSTEADY_RK3				"VflGpuParticlesCart_Unsteady_RK3.glsl"
#define SHADER_UNSTEADY_RK3_SCALARS		"VflGpuParticlesCart_Unsteady_RK3Scalars.glsl"
#define SHADER_UNSTEADY_RK4				"VflGpuParticlesCart_Unsteady_RK4.glsl"
#define SHADER_UNSTEADY_RK4_SCALARS		"VflGpuParticlesCart_Unsteady_RK4Scalars.glsl"

static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveCartesianGrid *pGrid);

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveCartesianGrid *pGrid);

static void DumpTexture(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						std::ostream &out);

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflGpuTracersCartGrid::VflGpuTracersCartGrid(VflVisGpuParticles *pParent)
: IVflGpuParticleTracer(pParent),
  m_pWriteTC(NULL),
  m_pPassThroughShader(NULL),
  m_bValid(false)
{
	m_bValid = CreateGpuResources();
}

VflGpuTracersCartGrid::~VflGpuTracersCartGrid()
{
	DestroyGpuResources();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersCartGrid::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();

	if (!pProperties->GetActive())
		return;

	double dVisTime = m_pParent->GetVisTime();
	VveTimeMapper *pTimeMapper = m_pParent->GetData()->GetTimeMapper();

	// retrieve integration parameters
	int iIntegrator = pProperties->GetIntegrator();
	bool bComputeScalars = pProperties->GetComputeScalars();

	// determine dataset boundaries
	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pTimeMapper->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return;
	}
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	// get access to flow data
	VflUnsteadyTexturePusher *pPusher = m_pParent->GetPusher();
	VflUnsteadyCartesianGridPusher *pGridPusher = dynamic_cast<VflUnsteadyCartesianGridPusher *>(pPusher);
	if (!pGridPusher)
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to retrieve texture pusher..." << std::endl;
		return;
	}
	int iPrecision = pGridPusher->GetTexturePrecision();

	m_pParent->SaveOGLState();

	// retrieve previous particle population
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	VistaTexture *pOldPopulation = pParticles->GetCurrentTexture();
	pOldPopulation->Bind(GL_TEXTURE0);

	// bind render target
	pParticles->GetFramebufferObj()->Bind();

#if 0
	VistaFramebufferObj *pFBO = pParticles->GetFramebufferObj();
	bool bValid = pFBO->IsValid();
	std::string strStatus = pFBO->GetStatusAsString();
#endif

	if (pProperties->GetSingleBuffered())
	{
		glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex());
	}
	else
	{
		glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + 1 - pParticles->GetCurrentDataIndex());
	}

	VistaGLSLShader *pShader = NULL;
	float fDeltaT = 0;

	// determine and bind shader
	if (m_pParent->GetRenderNode()->GetVisTiming()->GetAnimationPlaying())
	{
		// determine time step length
		double dDeltaTVisTime = 0;
#if 1
		fDeltaT = pProperties->GetFixedDeltaTime();
		dDeltaTVisTime = pTimeMapper->GetVisualizationTime(pTimeMapper->GetSimulationTime(0)+fDeltaT);
#else
		if (m_pParent->GetTimeWarning())
		{
			fDeltaT = pTimeMapper->GetVisualizationTime(pTimeMapper->GetSimulationTime(0.0f)
				+ pProperties->GetMaxDeltaTime());

			dDeltaTVisTime = pTimeMapper->GetVisualizationTime(pTimeMapper->GetSimulationTime(0)+fDeltaT);
		}
		else
		{
			float fLastVisTime = m_pParent->GetLastVisTime();
			dDeltaTVisTime = dVisTime - fLastVisTime;
			while (dDeltaTVisTime < 0)
				dDeltaTVisTime += 1.0f;

			fDeltaT = pTimeMapper->GetSimulationTime(dDeltaTVisTime)
				- pTimeMapper->GetSimulationTime(0);
		}
#endif

		if (bComputeScalars)
		{
			pShader = m_vecShadersUnsteadyScalars[iIntegrator];
		}
		else
		{
			pShader = m_vecShadersUnsteady[iIntegrator];
		}

		if (pShader)
		{
			pShader->Bind();

			// set texture information
			std::vector<VistaTexture *> vecTextures;
			std::vector<double> vecOffsets;
			std::vector<float> vecWeights;
			switch (iIntegrator)
			{
			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER:
				vecOffsets.push_back(0);
				if (pGridPusher->GetTexturesAndWeights(vecOffsets, vecTextures, vecWeights) == 1)
				{
					vecTextures[0]->Bind(GL_TEXTURE1);
					vecTextures[1]->Bind(GL_TEXTURE2);

					float aWeights[2];
					aWeights[0] = 1.0f - vecWeights[0];
					aWeights[1] = vecWeights[0];

					pShader->SetUniform(pShader->GetUniformLocation("aWeights[0]"), 1, 2, aWeights);
				}
				break;
			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3:
			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4:
				vecOffsets.push_back(0);
				vecOffsets.push_back(0.5f * dDeltaTVisTime);
				vecOffsets.push_back(dDeltaTVisTime);
				if (pGridPusher->GetTexturesAndWeights(vecOffsets, vecTextures, vecWeights) == 3)
				{
					GLenum iTextureUnit = GL_TEXTURE1;
					float aWeights[6];
					for (int i=0; i<3; ++i)
					{
						vecTextures[2*i]->Bind(iTextureUnit++);
						vecTextures[2*i+1]->Bind(iTextureUnit++);
						aWeights[2*i] = 1.0f - vecWeights[i];
						aWeights[2*i+1] = vecWeights[i];
					}
					pShader->SetUniform(pShader->GetUniformLocation("aWeights[0]"), 1, 6, aWeights);
				}
				break;
			};
		}
	}
	else
	{
		// determine time step length
		fDeltaT = pProperties->GetDeltaTime();

		if (bComputeScalars)
		{
			pShader = m_vecShadersSteadyScalars[iIntegrator];
		}
		else
		{
			pShader = m_vecShadersSteady[iIntegrator];
		}

		if (pShader)
		{
			pShader->Bind();

			// bind flow data
			pGridPusher->GetTexture()->Bind(GL_TEXTURE1);

			// but stay on texture unit 0
			glActiveTexture(GL_TEXTURE0);
		}
	}

	if (pShader)
	{
		// specify time step length
		pShader->SetUniform(pShader->GetUniformLocation("fDeltaT"), fDeltaT);

		// inform shader about dataset boundaries
		pShader->SetUniform(pShader->GetUniformLocation("v3Min"),
			v3Min[0], v3Min[1], v3Min[2]);
		pShader->SetUniform(pShader->GetUniformLocation("v3Max"),
			v3Max[0], v3Max[1], v3Max[2]);

		// compute scale and bias for lookup from particle positions
		// into 3D texture
		int aDims[3];
		int aDataDims[3];
		VistaVector3D v3Scale, v3Bias;

		// better ask someone, who knows what to do...
		pGridPusher->GetMaxDimensions(aDims);
		// pGrid->GetDimensions(aDims);

		pGrid->GetDataDimensions(aDataDims);
		for (int i=0; i<3; ++i)
		{
			float fDataDimsByDeltaAndDim = (aDataDims[i]-1) / (v3Max[i] - v3Min[i]) / aDims[i];
			v3Scale[i] = fDataDimsByDeltaAndDim;
			v3Bias[i] = 0.5f/aDims[i] - v3Min[i] * fDataDimsByDeltaAndDim;
		}
		pShader->SetUniform(pShader->GetUniformLocation("v3Scale"),
			v3Scale[0], v3Scale[1], v3Scale[2]);
		pShader->SetUniform(pShader->GetUniformLocation("v3Bias"),
			v3Bias[0], v3Bias[1], v3Bias[2]);

		int iWidth, iHeight;
		pParticles->GetSize(iWidth, iHeight);

		// viewport and projection setup
		glViewport(0, 0, iWidth, iHeight);
		glLoadIdentity();
		glOrtho(0, 1, 0, 1, -1, 1);

		// find line to insert new particle data
		int iSourceLine = pParticles->GetCurrentLine();
		int iTargetLine = (iSourceLine + 1) % iHeight;

		float fSource = (iSourceLine + 0.5f) / iHeight;
		float fTarget = (iTargetLine + 0.5f) / iHeight;

		// [picard] Energize! [\picard]
		glBegin(GL_LINES);
		glTexCoord2f(0, fSource);
		glVertex2f(0, fTarget);
		glTexCoord2f(1, fSource);
		glVertex2f(1, fTarget);
		glEnd();

		pShader->Release();

		// if we're double buffering, we still have to copy the old positions
		if (!pProperties->GetSingleBuffered())
		{
			m_pPassThroughShader->Bind();
			glBegin(GL_LINES);
			glTexCoord2f(0, fSource);
			glVertex2f(0, fSource);
			glTexCoord2f(1, fSource);
			glVertex2f(1, fSource);
			glEnd();
			m_pPassThroughShader->Release();
		}

		// return to fixed-function pipeline
		pShader->Release();

		// advance current line
		pParticles->SetCurrentLine(iTargetLine);
	}
	pParticles->GetFramebufferObj()->Release();

	static bool bDump=false;
	if (bDump)
	{
		std::cout << "current line: " << pParticles->GetCurrentLine() << std::endl;
		int w, h;
		pParticles->GetSize(w, h);
		std::cout << "current texture: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), w, h, 4, std::cout);
		std::cout << "stale texture: " << std::endl;
		DumpTexture(pParticles->GetStaleTexture(), w, h, 4, std::cout);
	}

	m_pParent->RestoreOGLState();

	if (!pProperties->GetSingleBuffered())
		m_pParent->GetParticleData()->Swap();

	m_pParent->GetParticleData()->SignalGpuDataChange();
}

///*============================================================================*/
///*                                                                            */
///*  NAME      :   Update                                                      */
///*                                                                            */
///*============================================================================*/
//void VflGpuTracersCartGrid::Update()
//{
//	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();
//
//	if (!pProperties->GetActive())
//		return;
//
//	float fVisTime = m_pParent->GetVisTime();
//	VflTimeMapper *pTimeMapper = m_pParent->GetData()->GetTimeMapper();
//
//	// retrieve integration parameters
//	int iIntegrator = pProperties->GetIntegrator();
//	bool bComputeScalars = pProperties->GetComputeScalars();
//
//	// determine dataset boundaries
//	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
//	int iIndex = pTimeMapper->GetLevelIndex(fVisTime);
//	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
//	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
//	if (!pGrid->GetValid())
//	{
//		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
//		return;
//	}
//	VistaVector3D v3Min, v3Max;
//	pGrid->GetBounds(v3Min, v3Max);
//	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
//
//	// get access to flow data
//	VflUnsteadyTexturePusher *pPusher = m_pParent->GetPusher();
//	VflUnsteadyCartesianGridPusher *pGridPusher = dynamic_cast<VflUnsteadyCartesianGridPusher *>(pPusher);
//	if (!pGridPusher)
//	{
//		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to retrieve texture pusher..." << std::endl;
//		return;
//	}
//	int iPrecision = pGridPusher->GetTexturePrecision();
//
//	m_pParent->SaveOGLState();
//
//	// retrieve previous particle population
//	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
//
//	IVistaShader *pShader = NULL;
//	float fDeltaT = 0;
//
//	// determine and bind shader
//	if (m_pParent->GetRenderNode()->IsAnimationPlaying())
//	{
//		// determine time step length
//		float fDeltaTVisTime = 0;
//#if 1
//		fDeltaT = pProperties->GetFixedDeltaTime();
//		fDeltaTVisTime = pTimeMapper->GetVisualizationTime(pTimeMapper->GetSimulationTime(0)+fDeltaT);
//#else
//		if (m_pParent->GetTimeWarning())
//		{
//			fDeltaT = pTimeMapper->GetVisualizationTime(pTimeMapper->GetSimulationTime(0.0f)
//				+ pProperties->GetMaxDeltaTime());
//
//			fDeltaTVisTime = pTimeMapper->GetVisualizationTime(pTimeMapper->GetSimulationTime(0)+fDeltaT);
//		}
//		else
//		{
//			float fLastVisTime = m_pParent->GetLastVisTime();
//			fDeltaTVisTime = fVisTime - fLastVisTime;
//			while (fDeltaTVisTime < 0)
//				fDeltaTVisTime += 1.0f;
//
//			fDeltaT = pTimeMapper->GetSimulationTime(fDeltaTVisTime)
//				- pTimeMapper->GetSimulationTime(0);
//		}
//#endif
//
//		if (bComputeScalars)
//		{
//			pShader = m_vecShadersUnsteadyScalars[iIntegrator];
//		}
//		else
//		{
//			pShader = m_vecShadersUnsteady[iIntegrator];
//		}
//
//		if (pShader)
//		{
//			pShader->Bind();
//
//			// set texture information
//			std::vector<VistaTexture *> vecTextures;
//			std::vector<float> vecOffsets, vecWeights;
//			switch (iIntegrator)
//			{
//			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER:
//				vecOffsets.push_back(0);
//				if (pGridPusher->GetTexturesAndWeights(vecOffsets, vecTextures, vecWeights) == 1)
//				{
//					vecTextures[0]->Bind(GL_TEXTURE1);
//					vecTextures[1]->Bind(GL_TEXTURE2);
//
//					float aWeights[2];
//					aWeights[0] = 1.0f - vecWeights[0];
//					aWeights[1] = vecWeights[0];
//
//					pShader->SetUniform(pShader->GetUniformLocation("aWeights"), 1, 2, aWeights);
//				}
//				break;
//			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3:
//			case VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4:
//				vecOffsets.push_back(0);
//				vecOffsets.push_back(0.5f * fDeltaTVisTime);
//				vecOffsets.push_back(fDeltaTVisTime);
//				if (pGridPusher->GetTexturesAndWeights(vecOffsets, vecTextures, vecWeights) == 3)
//				{
//					GLenum iTextureUnit = GL_TEXTURE1;
//					float aWeights[6];
//					for (int i=0; i<3; ++i)
//					{
//						vecTextures[2*i]->Bind(iTextureUnit++);
//						vecTextures[2*i+1]->Bind(iTextureUnit++);
//						aWeights[2*i] = 1.0f - vecWeights[i];
//						aWeights[2*i+1] = vecWeights[i];
//					}
//					pShader->SetUniform(pShader->GetUniformLocation("aWeights"), 1, 6, aWeights);
//				}
//				break;
//			};
//		}
//	}
//	else
//	{
//		// determine time step length
//		fDeltaT = pProperties->GetDeltaTime();
//
//		if (bComputeScalars)
//		{
//			pShader = m_vecShadersSteadyScalars[iIntegrator];
//		}
//		else
//		{
//			pShader = m_vecShadersSteady[iIntegrator];
//		}
//
//		if (pShader)
//		{
//			pShader->Bind();
//
//			// bind flow data
//			pGridPusher->GetTexture()->Bind(GL_TEXTURE1);
//
//			// but stay on texture unit 0
//			glActiveTexture(GL_TEXTURE0);
//		}
//	}
//
//	if (pShader)
//	{
//		// specify time step length
//		pShader->SetUniform(pShader->GetUniformLocation("fDeltaT"), fDeltaT);
//
//		// inform shader about dataset boundaries
//		pShader->SetUniform(pShader->GetUniformLocation("v3Min"),
//			v3Min[0], v3Min[1], v3Min[2]);
//		pShader->SetUniform(pShader->GetUniformLocation("v3Max"),
//			v3Max[0], v3Max[1], v3Max[2]);
//
//		// compute scale and bias for lookup from particle positions
//		// into 3D texture
//		int aDims[3];
//		int aDataDims[3];
//		VistaVector3D v3Scale, v3Bias;
//		pGrid->GetDimensions(aDims);
//		pGrid->GetDataDimensions(aDataDims);
//		for (int i=0; i<3; ++i)
//		{
//			float fDataDimsByDeltaAndDim = (aDataDims[i]-1) / (v3Max[i] - v3Min[i]) / aDims[i];
//			v3Scale[i] = fDataDimsByDeltaAndDim;
//			v3Bias[i] = 0.5f/aDims[i] - v3Min[i] * fDataDimsByDeltaAndDim;
//		}
//		pShader->SetUniform(pShader->GetUniformLocation("v3Scale"),
//			v3Scale[0], v3Scale[1], v3Scale[2]);
//		pShader->SetUniform(pShader->GetUniformLocation("v3Bias"),
//			v3Bias[0], v3Bias[1], v3Bias[2]);
//
//		int iWidth, iHeight;
//		pParticles->GetSize(iWidth, iHeight);
//
//		// viewport and projection setup
//		glViewport(0, 0, iWidth, iHeight);
//		glLoadIdentity();
//		glOrtho(0, 1, 0, 1, -1, 1);
//
//		// bind render target
//		pParticles->GetFramebufferObj()->Bind();
//
//#if 0
//		VistaFramebufferObj *pFBO = pParticles->GetFramebufferObj();
//		bool bValid = pFBO->IsValid();
//		std::string strStatus = pFBO->GetStatusAsString();
//#endif
//
//		for (int i=0; i<512; ++i)
//		{
//			VistaTexture *pOldPopulation = pParticles->GetCurrentTexture();
//			pOldPopulation->Bind(GL_TEXTURE0);
//
//			if (pProperties->GetSingleBuffered())
//			{
//				glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex());
//			}
//			else
//			{
//				glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + 1 - pParticles->GetCurrentDataIndex());
//			}
//
//
//			// find line to insert new particle data
//			int iSourceLine = pParticles->GetCurrentLine();
//			int iTargetLine = (iSourceLine + 1) % iHeight;
//
//			float fSource = (iSourceLine + 0.5f) / iHeight;
//			float fTarget = (iTargetLine + 0.5f) / iHeight;
//
//			// [picard] Energize! [\picard]
//			glBegin(GL_LINES);
//			glTexCoord2f(0, fSource);
//			glVertex2f(0, fTarget);
//			glTexCoord2f(1, fSource);
//			glVertex2f(1, fTarget);
//			glEnd();
//
//			// if we're double buffering, we still have to copy the old positions
//			if (!pProperties->GetSingleBuffered())
//			{
//				pShader->Release();
//				m_pPassThroughShader->Bind();
//				glBegin(GL_LINES);
//				glTexCoord2f(0, fSource);
//				glVertex2f(0, fSource);
//				glTexCoord2f(1, fSource);
//				glVertex2f(1, fSource);
//				glEnd();
//				m_pPassThroughShader->Release();
//				pShader->Bind();
//			}
//
//			// advance current line
//			pParticles->SetCurrentLine(iTargetLine);
//			if (!pProperties->GetSingleBuffered())
//				m_pParent->GetParticleData()->Swap();
//		}
//
//		// return to fixed-function pipeline
//		pShader->Release();
//	}
//	m_pFBO->Release();
//
//	static bool bDump=false;
//	if (bDump)
//	{
//		std::cout << "current line: " << pParticles->GetCurrentLine() << std::endl;
//		int w, h;
//		pParticles->GetSize(w, h);
//		std::cout << "current texture: " << std::endl;
//		DumpTexture(pParticles->GetCurrentTexture(), w, h, 4, std::cout);
//		std::cout << "stale texture: " << std::endl;
//		DumpTexture(pParticles->GetStaleTexture(), w, h, 4, std::cout);
//	}
//
//	m_pParent->RestoreOGLState();
//
//	m_pParent->GetParticleData()->SignalGpuDataChange();
//}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticle                                                */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersCartGrid::SeedParticle(const VistaVector3D &v3Pos)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount, iLength;
	pParticles->GetSize(iCount, iLength);
	int iActive = pParticles->GetActiveTracers();
	int iNext = pParticles->GetNextTracer();

	m_pParent->SaveOGLState();

	static bool bDump=false;
	if (bDump)
	{
		std::cout << "===" << std::endl;
		std::cout << "before: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), iCount, iLength, 4, std::cout);
		std::cout << "-" << std::endl;
		DumpTexture(pParticles->GetStaleTexture(), iCount, iLength, 4, std::cout);
	}

	// activate render target
	pParticles->GetFramebufferObj()->Bind();

	// activate shader for writing particle positions (given as TCs)
	m_pWriteTC->Bind();

	// set viewport and transformation matrices
	glViewport(0, 0, iCount, iLength);
	glLoadIdentity();
	glOrtho(0, iCount, 0, iLength, -1, 1);

	// ok - who's next?
	float fPosX = (iNext % iCount) + 0.5f;

	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex());
		glBegin(GL_LINES);
		glTexCoord4fv(&v3Pos[0]);
		glVertex2f(fPosX, 0);
		glVertex2f(fPosX, float(iLength+1));
		glEnd();
	}
	else
	{
		for (int i=0; i<2; ++i)
		{
			glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + i);
			glBegin(GL_LINES);
			glTexCoord4fv(&v3Pos[0]);
			glVertex2f(fPosX, 0);
			glVertex2f(fPosX, float(iLength+1));
			glEnd();
		}
	}

	m_pWriteTC->Release();
	pParticles->GetFramebufferObj()->Release();

	if (bDump)
	{
		std::cout << "after: " << std::endl;
		DumpTexture(pParticles->GetCurrentTexture(), iCount, iLength, 4, std::cout);
		std::cout << "-" << std::endl;
		DumpTexture(pParticles->GetStaleTexture(), iCount, iLength, 4, std::cout);
		std::cout << "===" << std::endl;
		bDump = false;
	}

	m_pParent->RestoreOGLState();

	pParticles->SetNextTracer((iNext+1)%iCount);
	if (iActive < iCount)
	{
		++iActive;
		pParticles->SetActiveTracers(iActive);
		if (iActive == iCount)
		{
			std::cout << " [VflGpuTracersCartGrid] - all tracers are active now..." << std::endl;
		}
	}

	pParticles->SignalGpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticles                                               */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersCartGrid::SeedParticles(const VistaVector3D &v3Pos1, 
										   const VistaVector3D &v3Pos2, 
										   int iCount)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iTracerCount, iLength;
	pParticles->GetSize(iTracerCount, iLength);
	int iActive = pParticles->GetActiveTracers();
	int iNext = pParticles->GetNextTracer();

	if (iCount > iTracerCount)
		iCount = iTracerCount;

	if (iActive < iTracerCount)
	{
		iActive += iCount;
		if (iActive >= iTracerCount)
		{
			iActive = iTracerCount;
			std::cout << " [VflGpuTracersCartGrid] - all tracers are active now..." << std::endl;
		}

		pParticles->SetActiveTracers(iActive);
	}

	float fDelta = 1.0f;
	if (iCount > 1)
		fDelta /= (iCount-1);
	float fOffset = 0;
	VistaVector3D v3Delta(v3Pos2-v3Pos1);
	float fScalarDelta(v3Pos2[3]-v3Pos1[3]);

	m_pParent->SaveOGLState();

	// set render target
	pParticles->GetFramebufferObj()->Bind();
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex());

	// activate shader for writing particle positions (given as TCs)
	m_pWriteTC->Bind();

	// set viewport and transformation matrices
	glViewport(0, 0, iTracerCount, iLength);
	glLoadIdentity();
	glOrtho(0, iTracerCount, 0, iLength, -1, 1);

	while (iCount > 0)
	{
		int iFrom = iNext;
		int iTo = iNext + iCount;
		if (iTo > iTracerCount)
			iTo = iTracerCount;

		int iNextCount = iTo - iFrom;

		VistaVector3D v3From(v3Pos1 + fOffset * v3Delta);
		v3From[3] = v3Pos1[3] + fOffset * fScalarDelta;
		VistaVector3D v3To(v3From + (iNextCount-1)*fDelta * v3Delta);
		v3To[3] = v3From[3] + (iNextCount-1)*fDelta*fScalarDelta;

		// seed iNextCount particles
		glBegin(GL_QUADS);
		glTexCoord4fv(&v3From[0]);
		glVertex2f(float(iFrom), 0);
		glTexCoord4fv(&v3To[0]);
		glVertex2f(float(iTo), 0);
		glTexCoord4fv(&v3To[0]);
		glVertex2f(float(iTo), float(iLength));
		glTexCoord4fv(&v3From[0]);
		glVertex2f(float(iFrom), float(iLength));
		glEnd();

		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + 1 - pParticles->GetCurrentDataIndex());

			// seed iNextCount particles
			glBegin(GL_QUADS);
			glTexCoord4fv(&v3From[0]);
			glVertex2f(float(iFrom), 0);
			glTexCoord4fv(&v3To[0]);
			glVertex2f(float(iTo), 0);
			glTexCoord4fv(&v3To[0]);
			glVertex2f(float(iTo), float(iLength));
			glTexCoord4fv(&v3From[0]);
			glVertex2f(float(iFrom), float(iLength));
			glEnd();

			glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + pParticles->GetCurrentDataIndex());
		}

		iCount -= iNextCount;
		fOffset += iNextCount * fDelta;
		iNext = (iNext + iNextCount) % iTracerCount;
	}

	m_pWriteTC->Release();
	pParticles->GetFramebufferObj()->Release();
	m_pParent->RestoreOGLState();

	pParticles->SetNextTracer(iNext);
	pParticles->SignalGpuDataChange();
}

void VflGpuTracersCartGrid::SeedParticles(const std::vector<float> &vecPositions)
{
	VistaVector3D v3Pos;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iTracerCount, iLength;
	pParticles->GetSize(iTracerCount, iLength);
	int iCount = int(vecPositions.size()) / 4;

	if (iCount > iTracerCount)
		iCount = iTracerCount;

	const float *pPositions = &vecPositions[0];
	for (int i=0; i<iCount; ++i)
	{
		memcpy(&v3Pos[0], &pPositions[4*i], 4*sizeof(float));
		SeedParticle(v3Pos);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticleNormalized                                      */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersCartGrid::SeedParticleNormalized(const VistaVector3D &v3Pos)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3PosDN;
	DeNormalize(v3PosDN, v3Pos, pGrid);
	SeedParticle(v3PosDN);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticlesNormalized                                     */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersCartGrid::SeedParticlesNormalized(const VistaVector3D &v3Pos1, 
													   const VistaVector3D &v3Pos2, 
													   int iCount)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3Pos1DN, v3Pos2DN;
	DeNormalize(v3Pos1DN, v3Pos1, pGrid);
	DeNormalize(v3Pos2DN, v3Pos2, pGrid);
	SeedParticles(v3Pos1DN, v3Pos2DN, iCount);
}

void VflGpuTracersCartGrid::SeedParticlesNormalized(const std::vector<float> &vecPositions)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	std::vector<float> vecDNPositions;
	int iCount = int(vecPositions.size()) / 4;
	vecDNPositions.resize(vecPositions.size());
	for (int i=0; i<iCount; ++i)
	{
		DeNormalize(&vecDNPositions[4*i], &vecPositions[4*i], pGrid);
	}

	SeedParticles(vecDNPositions);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGridType                                                 */
/*                                                                            */
/*============================================================================*/
int VflGpuTracersCartGrid::GetGridType() const
{
	return VflVisGpuParticles::GT_CARTESIAN_GRID;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProcessingUnit                                           */
/*                                                                            */
/*============================================================================*/
int VflGpuTracersCartGrid::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_GPU;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerType                                               */
/*                                                                            */
/*============================================================================*/
int VflGpuTracersCartGrid::GetTracerType() const
{
	return VflVisGpuParticles::TT_TRACERS;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflGpuTracersCartGrid::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflGpuTracersCartGrid::GetReflectionableType() const
{
	return s_strCVflGpuTracersCartGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateGpuResources                                          */
/*                                                                            */
/*============================================================================*/
bool VflGpuTracersCartGrid::CreateGpuResources()
{
	// TODO: check for required OpenGL extensions...
#ifdef DEBUG
	std::cout << " [VflGpuTracersCartGrid] - creating GPU resources..." << std::endl;
#endif

	// init shaders -----------------------------------------------------------
#ifdef DEBUG
	std::cout << " [VflGpuTracersCartGrid] - creating shader for writing tex coords..." << std::endl;
#endif
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();
	std::string strShader =
		rShaderReg.RetrieveShader( "VflGpuCartGrid_WriteTC_frag.glsl" );

	VistaGLSLShader* pProgram = new VistaGLSLShader();

	if(    strShader.empty()
		|| !pProgram->InitShaderFromString( GL_FRAGMENT_SHADER, strShader )
		|| !pProgram->Link() )
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to initialize shader for writing texcoords..." << std::endl;
		delete pProgram;
		return false;
	}
	m_pWriteTC = pProgram;

#ifdef DEBUG
	std::cout << " [VflGpuTracersCartGrid] - creating pass-through shader..." << std::endl;
#endif
	strShader =
		rShaderReg.RetrieveShader( "VflGpuParticleData_PassThrough_frag.glsl" );

	pProgram = new VistaGLSLShader();

	if(    strShader.empty()
		|| !pProgram->InitShaderFromString( GL_FRAGMENT_SHADER, strShader )
		|| !pProgram->Link() )
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to initialize pass-through shader..." << std::endl;
		delete pProgram;
		return false;
	}
	m_pPassThroughShader = pProgram;
	m_pPassThroughShader->Bind();
	m_pPassThroughShader->SetUniform( "samp_texture", 0 );
	m_pPassThroughShader->Release();

	m_vecShadersSteady.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersSteadyScalars.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
#ifdef DEBUG
	std::cout << " [VflGpuTracersCartGrid] - creating Euler integration shaders..." << std::endl;
#endif
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_EULER);

	VistaGLSLShader *pShader = new VistaGLSLShader;	
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - Euler integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_EULER_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - Euler integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

#ifdef DEBUG
	std::cout << " [VflGpuTracersCartGrid] - creating RK3 integration shaders..." << std::endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK3);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - RK3 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK3_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - RK3 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

#ifdef DEBUG
	std::cout << " [VflGpuTracersCartGrid] - creating RK4 integration shaders..." << std::endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK4);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - RK4 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_STEADY_RK4_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - RK4 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume"), 1);
	pShader->Release();
	m_vecShadersSteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	m_vecShadersUnsteady.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
	m_vecShadersUnsteadyScalars.resize(VflVisGpuParticles::VflVisGpuParticlesProperties::IR_LAST, NULL);
#ifdef DEBUG
	std::cout << " [VflGpuTracersCartGrid] - creating unsteady Euler integration shaders..." << std::endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_EULER);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unsteady Euler integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init unsteady Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link unsteady Euler integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 2, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_EULER_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unsteady Euler integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init unsteady Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link unsteady Euler integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 2, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER] = pShader;

#ifdef DEBUG
	std::cout << " [VflGpuTracersCartGrid] - creating unsteady RK3 integration shaders..." << std::endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_RK3);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unsteady RK3 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init unsteady RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link unsteady RK3 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_RK3_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unsteady RK3 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init unsteady RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link unsteady RK3 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3] = pShader;

#ifdef DEBUG
	std::cout << " [VflGpuTracersCartGrid] - creating unsteady RK4 integration shaders..." << std::endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_RK4);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unsteady RK4 integration shader not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to init unsteady RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to link unsteady RK4 integration shader..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteady[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_UNSTEADY_RK4_SCALARS);
	if(strShader.empty())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unsteady init RK4 integration shader w/scalars not available" << std::endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to unsteady init RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		std::cout << " [VflGpuTracersCartGrid] - ERROR - unable to unsteady link RK4 integration shader w/scalars..." << std::endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitPosition"), 0);
	{
		int aTexUnits[] = { 1, 2, 3, 4, 5, 6 };
		pShader->SetUniform(pShader->GetUniformLocation("texUnitVolume[0]"), 1, 6, aTexUnits);
	}
	pShader->Release();
	m_vecShadersUnsteadyScalars[VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4] = pShader;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DestroyGpuResources                                         */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersCartGrid::DestroyGpuResources()
{
	delete m_pWriteTC;
	m_pWriteTC = NULL;

	delete m_pPassThroughShader;
	m_pPassThroughShader = NULL;

	std::set<VistaGLSLShader *> setShaders;

	for (unsigned int i=0; i<m_vecShadersSteady.size(); ++i)
	{
		setShaders.insert(m_vecShadersSteady[i]);
	}
	m_vecShadersSteady.clear();

	for (unsigned int i=0; i<m_vecShadersSteadyScalars.size(); ++i)
	{
		setShaders.insert(m_vecShadersSteadyScalars[i]);
	}
	m_vecShadersSteadyScalars.clear();

	std::set<VistaGLSLShader *>::iterator it;
	for (it=setShaders.begin(); it!=setShaders.end(); ++it)
	{
		delete *it;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflGpuTracersCartGrid::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleTracer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflGpuTracersCartGridReflType);
	return i+1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRendererId                                               */
/*                                                                            */
/*============================================================================*/
const std::string VflGpuTracersCartGrid::GetRendererId()
{
	return s_strCVflGpuTracersCartGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetShaderKeys                                               */
/*                                                                            */
/*============================================================================*/
void VflGpuTracersCartGrid::GetShaderKeys(
	std::vector<std::string>& vecShaderKeys)
{
	vecShaderKeys.clear();
	vecShaderKeys.push_back(SHADER_STEADY_EULER);
	vecShaderKeys.push_back(SHADER_STEADY_EULER_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_RK3);
	vecShaderKeys.push_back(SHADER_STEADY_RK3_SCALARS);
	vecShaderKeys.push_back(SHADER_STEADY_RK4);
	vecShaderKeys.push_back(SHADER_STEADY_RK4_SCALARS);
	vecShaderKeys.push_back(SHADER_UNSTEADY_EULER);
	vecShaderKeys.push_back(SHADER_UNSTEADY_EULER_SCALARS);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK3);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK3_SCALARS);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK4);
	vecShaderKeys.push_back(SHADER_UNSTEADY_RK4_SCALARS);
}

/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeNormalize                                                 */
/*                                                                            */
/*============================================================================*/
static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveCartesianGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	v3Out[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * v3In[0];
	v3Out[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * v3In[1];
	v3Out[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * v3In[2];
	v3Out[3] = v3In[3];
}

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveCartesianGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	aOut[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * aIn[0];
	aOut[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * aIn[1];
	aOut[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * aIn[2];
	aOut[3] = aIn[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DumpTexture                                                 */
/*                                                                            */
/*============================================================================*/
static void DumpTexture(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						std::ostream &out)
{
	if (!pTexture)
		return;

	float *pData = new float[iWidth*iHeight*iTuples];
	pTexture->Bind();
	glGetTexImage(pTexture->GetTarget(), 0, GL_RGBA, GL_FLOAT, pData);
	float *pPos = pData;

	for (int i=0; i<iHeight; ++i)
	{
		for (int j=0; j<iWidth; ++j)
		{
			if (j>0)
				out << "| ";
			for (int k=0; k<iTuples; ++k)
			{
				out << (*(pPos++)) << " ";
			}
		}
		out << std::endl;
	}

	delete [] pData;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
