/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflCpuTracerRenderer.cpp     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflCpuTracerRenderer.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include <VistaFlowLib/Visualization/VflLookupTexture.h>

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaOGLExt/VistaTexture.h>

#include <vtkLookupTable.h>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflCpuTracerRendererReflType("VflCpuTracerRenderer");

static IVistaPropertyGetFunctor *s_aCVflCpuTracerRendererGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflCpuTracerRendererSetFunctors[] =
{
	NULL
};

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflCpuTracerRenderer::VflCpuTracerRenderer(VflVisGpuParticles *pParent)
: IVflGpuParticleRenderer(pParent),
  m_dPreparationTimeStamp(0),
  m_iTracerVBO(0),
  m_iTracerCount(0),
  m_iTracerLength(0),
  m_iIndexVBO(0),
  m_aIndexOffsets(NULL),
  m_aIndexCounts(NULL),
  m_bValid(false)
{
	if (!GLEW_VERSION_1_5)
	{
		cout << " [VflCpuTracerRenderer] - ERROR - OpenGL 1.5 not supported..." << endl;
		return;
	}

	m_iDrawMode = DM_LINES;

	m_pParent->GetParticleData()->GetSize(m_iTracerCount, m_iTracerLength);

	glGenBuffers(1, &m_iTracerVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_iTracerVBO);
	glBufferData(GL_ARRAY_BUFFER, 1, NULL, GL_STREAM_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &m_iIndexVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 1, NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	UpdateGpuResources();

	m_bValid = true;
}

VflCpuTracerRenderer::~VflCpuTracerRenderer()
{
	glDeleteBuffers(1, &m_iTracerVBO);
	m_iTracerVBO = 0;

	glDeleteBuffers(1, &m_iIndexVBO);
	m_iIndexVBO = 0;

	delete [] m_aIndexOffsets;
	m_aIndexOffsets = NULL;

	delete [] m_aIndexCounts;
	m_aIndexCounts = NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflCpuTracerRenderer::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	pParticles->SynchToCpu();

	if (pParticles->GetCpuDataChangeTimeStamp() > m_dPreparationTimeStamp)
	{
		int w, h;
		pParticles->GetSize(w, h);
		if (w != m_iTracerCount || h != m_iTracerLength)
		{
			m_iTracerCount = w;
			m_iTracerLength = h;
			UpdateGpuResources();
		}

		glBindBuffer(GL_ARRAY_BUFFER, m_iTracerVBO);

		// does this work asynchronously as well??
		float *pData = (float *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		float *pParticleData = pParticles->GetCurrentData();
		int iHeadLineCount = pParticles->GetCurrentLine()+1;

		// copy the head of the particle tracers (i.e. from
		// the current line to the bottom)
		memcpy(&pData[(m_iTracerLength-iHeadLineCount)*4*m_iTracerCount],
			pParticleData, 
			iHeadLineCount*4*m_iTracerCount*sizeof(float));

		// copy the rest, i.e. from the top to the current line
		if (iHeadLineCount < m_iTracerLength)
		{
			memcpy(pData,
				&pParticleData[iHeadLineCount*4*m_iTracerCount],
				(m_iTracerLength-iHeadLineCount)*4*m_iTracerCount*sizeof(float));
		}

		if (!glUnmapBuffer(GL_ARRAY_BUFFER))
		{
			cout << " [VflCpuTracerRenderer] - ERROR - unable to unmap vertex buffer object..." << endl;
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		m_dPreparationTimeStamp = pParticles->GetCpuDataChangeTimeStamp();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflCpuTracerRenderer::DrawOpaque()
{
	if (m_iDrawMode < DM_POINTS || m_iDrawMode > DM_LINES)
		return;

	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;

	glPushAttrib(GL_ENABLE_BIT | GL_POINT_BIT | GL_LINE_BIT);
	glDisable(GL_LIGHTING);

	glPointSize(pProperties->GetPointSize());
	glLineWidth(pProperties->GetLineWidth());

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iIndexVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_iTracerVBO);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 4*sizeof(float), 0);

	if (pProperties->GetUseLookupTable())
	{
		glColor4ub(255, 255, 255, 255);
		VistaTexture *pLut = pProperties->GetLookupTexture()->GetLookupTexture();
		pLut->Enable();
		pLut->Bind();

		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(1, GL_FLOAT, 4*sizeof(float), BUFFER_OFFSET(3*sizeof(float)));

		glMatrixMode(GL_TEXTURE);
		glPushMatrix();
		glLoadIdentity();
#if (VTK_MAJOR_VERSION >= 5)
		double aRange[2];
#else
		float aRange[2];
#endif
		pProperties->GetLookupTable()->GetLookupTable()->GetTableRange(aRange);
		glScalef(1.0f/(aRange[1]-aRange[0]), 1, 1);
		glTranslatef(-aRange[0], 0, 0);
	}
	else
	{
		glColor4fv(&(pProperties->GetColor()[0]));
	}

	if (m_iDrawMode == DM_POINTS)
	{
		glMultiDrawElements(GL_POINTS, m_aIndexCounts, GL_UNSIGNED_INT,
			(const GLvoid **)m_aIndexOffsets, 
			m_pParent->GetParticleData()->GetActiveTracers());
	}
	else
	{
		glMultiDrawElements(GL_LINE_STRIP, m_aIndexCounts, GL_UNSIGNED_INT,
			(const GLvoid **)m_aIndexOffsets, 
			m_pParent->GetParticleData()->GetActiveTracers());
	}

	glDisableClientState(GL_VERTEX_ARRAY);

	if (pProperties->GetUseLookupTable())
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDrawMode*                                            */
/*                                                                            */
/*============================================================================*/
std::string VflCpuTracerRenderer::GetDrawModeString(int iIndex) const
{
	switch (iIndex)
	{
	case DM_POINTS:
		return std::string("DM_POINTS");
	case DM_LINES:
		return std::string("DM_LINES");
	}
	return IVflGpuParticleRenderer::GetDrawModeString(iIndex);
}

int VflCpuTracerRenderer::GetDrawModeCount() const
{
	return DM_LAST;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProcessingUnit                                           */
/*                                                                            */
/*============================================================================*/
int VflCpuTracerRenderer::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_CPU;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerType                                               */
/*                                                                            */
/*============================================================================*/
int VflCpuTracerRenderer::GetTracerType() const
{
	return VflVisGpuParticles::TT_TRACERS;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflCpuTracerRenderer::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflCpuTracerRenderer::GetReflectionableType() const
{
	return s_strCVflCpuTracerRendererReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflCpuTracerRenderer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleRenderer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflCpuTracerRendererReflType);
	return i+1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateGpuResources                                          */
/*                                                                            */
/*============================================================================*/
void VflCpuTracerRenderer::UpdateGpuResources()
{
	// resize particle data buffer
	glBindBuffer(GL_ARRAY_BUFFER, m_iTracerVBO);
	glBufferData(GL_ARRAY_BUFFER, 4*m_iTracerCount*m_iTracerLength*sizeof(float),
		NULL, GL_STREAM_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// create topology info for lines...
	int iSizeIndices = m_iTracerCount * m_iTracerLength * sizeof(GLuint);
	GLuint *pIndexArray = new GLuint[m_iTracerCount * m_iTracerLength];
	GLuint *pPos = pIndexArray;
	delete [] m_aIndexOffsets;
	delete [] m_aIndexCounts;
	m_aIndexOffsets = new GLvoid*[m_iTracerCount];
	m_aIndexCounts = new GLsizei[m_iTracerCount];

	for (int j=0; j<m_iTracerCount; ++j)
	{
//		m_aIndexOffsets[j] = j*m_iTracerLength*sizeof(GLuint);
		m_aIndexOffsets[j] = BUFFER_OFFSET(j*m_iTracerLength*sizeof(GLuint)); 
		m_aIndexCounts[j] = m_iTracerLength;
		for (int i=0; i<m_iTracerLength; ++i)
		{
			*(pPos++) = i * m_iTracerCount + j;
		}
	}

	// ... and send it to graphics memory
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, iSizeIndices, pIndexArray,
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete [] pIndexArray;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
