/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflCpuParticleRenderer.cpp   RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include <GL/glew.h>

#include "VflCpuParticleRenderer.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include <VistaFlowLib/Visualization/VflLookupTexture.h>

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaOGLExt/VistaTexture.h>

#include <vtkLookupTable.h>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflCpuParticleRendererReflType("VflCpuParticleRenderer");

static IVistaPropertyGetFunctor *s_aCVflCpuParticleRendererGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflCpuParticleRendererSetFunctors[] =
{
	NULL
};

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflCpuParticleRenderer::VflCpuParticleRenderer(VflVisGpuParticles *pParent)
: IVflGpuParticleRenderer(pParent),
  m_dPreparationTimeStamp(0),
  m_iParticleVBO(0),
  m_iParticleCount(0),
  m_bValid(false)
{
	if (!GLEW_VERSION_1_5)
	{
		cout << " [VflCpuParticleRenderer] - ERROR - OpenGL 1.5 not supported..." << endl;
		return;
	}

	m_iDrawMode = DM_POINTS;

	m_iParticleCount = m_pParent->GetParticleData()->GetParticleCount();

	glGenBuffers(1, &m_iParticleVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_iParticleVBO);
	glBufferData(GL_ARRAY_BUFFER, 4*m_iParticleCount*sizeof(float), NULL, GL_STREAM_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	m_bValid = true;
}

VflCpuParticleRenderer::~VflCpuParticleRenderer()
{
	glDeleteBuffers(1, &m_iParticleVBO);
	m_iParticleVBO = 0;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflCpuParticleRenderer::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	pParticles->SynchToCpu();

	if (pParticles->GetCpuDataChangeTimeStamp() > m_dPreparationTimeStamp)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_iParticleVBO);
		if (m_iParticleCount != pParticles->GetParticleCount())
		{
			// resize VBO if necessary...
			m_iParticleCount = pParticles->GetParticleCount();
			glBufferData(GL_ARRAY_BUFFER, 4*m_iParticleCount*sizeof(float), NULL, GL_STREAM_DRAW);
		}

		// does this work asynchronously as well??
		void *pData = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		memcpy(pData, pParticles->GetCurrentData(), 
			4*pParticles->GetActiveParticles()*sizeof(float));

		if (!glUnmapBuffer(GL_ARRAY_BUFFER))
		{
			cout << " [VflCpuParticleRenderer] - ERROR - unable to unmap vertex buffer object..." << endl;
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		m_dPreparationTimeStamp = pParticles->GetCpuDataChangeTimeStamp();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflCpuParticleRenderer::DrawOpaque()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;

	glPushAttrib(GL_ENABLE_BIT | GL_POINT_BIT);
	glDisable(GL_LIGHTING);

	glPointSize(pProperties->GetPointSize());

	glBindBuffer(GL_ARRAY_BUFFER, m_iParticleVBO);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 4*sizeof(float), 0);

	if (pProperties->GetUseLookupTable())
	{
		glColor4ub(255, 255, 255, 255);
		VistaTexture *pLut = pProperties->GetLookupTexture()->GetLookupTexture();
		pLut->Enable();
		pLut->Bind();

		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(1, GL_FLOAT, 4*sizeof(float), BUFFER_OFFSET(3*sizeof(float)));

		glMatrixMode(GL_TEXTURE);
		glPushMatrix();
		glLoadIdentity();
#if (VTK_MAJOR_VERSION >= 5)
		double aRange[2];
#else
		float aRange[2];
#endif
		pProperties->GetLookupTable()->GetLookupTable()->GetTableRange(aRange);
		glScalef(1.0f/(aRange[1]-aRange[0]), 1, 1);
		glTranslatef(-aRange[0], 0, 0);
	}
	else
	{
		glColor4fv(&(pProperties->GetColor()[0]));
	}

	
	glDrawArrays(GL_POINTS, 0, m_pParent->GetParticleData()->GetActiveParticles());
	glDisableClientState(GL_VERTEX_ARRAY);

	if (pProperties->GetUseLookupTable())
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDrawMode*                                            */
/*                                                                            */
/*============================================================================*/
std::string VflCpuParticleRenderer::GetDrawModeString(int iIndex) const
{
	switch (iIndex)
	{
	case DM_POINTS:
		return std::string("POINTS");
	}
	return IVflGpuParticleRenderer::GetDrawModeString(iIndex);
}

int VflCpuParticleRenderer::GetDrawModeCount() const
{
	return DM_LAST;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProcessingUnit                                           */
/*                                                                            */
/*============================================================================*/
int VflCpuParticleRenderer::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_CPU;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerType                                               */
/*                                                                            */
/*============================================================================*/
int VflCpuParticleRenderer::GetTracerType() const
{
	return VflVisGpuParticles::TT_PARTICLES;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticleRenderer::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflCpuParticleRenderer::GetReflectionableType() const
{
	return s_strCVflCpuParticleRendererReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflCpuParticleRenderer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleRenderer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflCpuParticleRendererReflType);
	return i+1;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
