/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflCpuTracersTetGrid.cpp     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflCpuTracersTetGrid.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaVisExt/Data/VveTetGrid.h>
#include <VistaVisExt/Algorithms/VveTetGridPointLocator.h>
#include <VistaVisExt/Data/VveUnsteadyTetGrid.h>
#include <VistaAspects/VistaAspectsUtils.h>

#include <set>
#include <string.h>

using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflCpuTracersTetGridReflType("VflCpuTracersTetGrid");

static IVistaPropertyGetFunctor *s_aCVflCpuTracersTetGridGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflCpuTracersTetGridSetFunctors[] =
{
	NULL
};

#define SCALAR_MULT(vec, scalar) \
	vec[0] *= scalar; \
	vec[1] *= scalar; \
	vec[2] *= scalar;
#define VEC_SMAD(dest, scalar, add1, add2) \
	dest[0] = scalar*add1[0] + add2[0]; \
	dest[1] = scalar*add1[1] + add2[1]; \
	dest[2] = scalar*add1[2] + add2[2];
#define VEC_SMULT(dest, scalar, vec) \
	dest[0] = scalar*vec[0]; \
	dest[1] = scalar*vec[1]; \
	dest[2] = scalar*vec[2];
#define VEC_ADD(dest, add1, add2) \
	dest[0] = add1[0] + add2[0]; \
	dest[1] = add1[1] + add2[1]; \
	dest[2] = add1[2] + add2[2];
#define VEC_CROSS(dest, v1, v2) \
	(dest)[0] = (v1)[1]*(v2)[2]-(v1)[2]*(v2)[1]; \
	(dest)[1] = (v2)[0]*(v1)[2]-(v1)[0]*(v2)[2]; \
	(dest)[2] = (v1)[0]*(v2)[1]-(v1)[1]*(v2)[0];
#define VEC_DOT(v1, v2) \
	(v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])
#define VEC_LENGTH(v1) \
	sqrt(VEC_DOT(v1, v1))
#define SATURATE(vec) \
	for (int _i=0; _i<3; ++_i) \
	{ \
		if (vec[_i] < 0) \
			vec[_i] = 0; \
		else if (vec[_i] > 1) \
			vec[_i] = 1; \
	}
#define SATURATE_LIMIT(vec, min, max) \
	for (int _i=0; _i<3; ++_i) \
	{ \
		if (vec[_i] < min[_i]) \
			vec[_i] = min[_i]; \
		else if (vec[_i] > max[_i]) \
			vec[_i] = max[_i]; \
	}

#define INTERPOLATE(dest, vec1, vec2, alpha) \
	{ \
		float _fOneMinusAlpha = 1.0f - alpha; \
		dest[0] = _fOneMinusAlpha * vec1[0] + alpha * vec2[0]; \
		dest[1] = _fOneMinusAlpha * vec1[1] + alpha * vec2[1]; \
		dest[2] = _fOneMinusAlpha * vec1[2] + alpha * vec2[2]; \
		dest[3] = _fOneMinusAlpha * vec1[3] + alpha * vec2[3]; \
	}

static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveTetGrid *pGrid);

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveTetGrid *pGrid);

static inline int RetrieveVelocity(VveTetGrid *pGrid, VveTetGridPointLocator *pLocator,
								   float aPos[3], int iStartCell, float aData[3]);
static inline int RetrieveScalar(VveTetGrid *pGrid, VveTetGridPointLocator *pLocator,
								 float aPos[3], int iStartCell, float &fData);

static inline void ComputeRadii(float &fInsphere, float &fCircumsphere,
								int *pCell, float *pVertices);


/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflCpuTracersTetGrid::VflCpuTracersTetGrid(VflVisGpuParticles *pParent)
: IVflGpuParticleTracer(pParent)
{
}

VflCpuTracersTetGrid::~VflCpuTracersTetGrid()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflCpuTracersTetGrid::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();

	if (!pProperties->GetActive())
		return;

	// don't move, please...
	if (m_pParent->GetRenderNode()->GetVisTiming()->GetAnimationPlaying())
		return;

	double dVisTime = m_pParent->GetVisTime();
	double dLastVisTime = m_pParent->GetLastVisTime();

	// retrieve integration parameters
	int iIntegrator = pProperties->GetIntegrator();
	bool bComputeScalars = pProperties->GetComputeScalars();
	bool bSuccess = false;
	//if (m_pParent->GetRenderNode()->IsAnimationPlaying())
	//{
	//	VflTimeMapper *pTimeMapper = m_pParent->GetData()->GetTimeMapper();
	//	float fDelta = fVisTime - fLastVisTime;
	//	while (fDelta < 0)
	//		fDelta += 1.0f;
	//	float fDeltaT = pTimeMapper->GetSimulationTime(fDelta)
	//		- pTimeMapper->GetSimulationTime(0);

	//	if (m_pParent->GetTimeWarning())
	//	{
	//		fDeltaT = pProperties->GetMaxDeltaTime();
	//	}

	//	if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER)
	//		bSuccess = IntegrateEulerUnsteady(fVisTime, fLastVisTime, fDeltaT, bComputeScalars);
	//	else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3)
	//		bSuccess = IntegrateRK3Unsteady(fVisTime, fLastVisTime, fDeltaT, bComputeScalars);
	//	else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4)
	//		bSuccess = IntegrateRK4Unsteady(fVisTime, fLastVisTime, fDeltaT, bComputeScalars);
	//}
	//else
	{
		double dDeltaT = static_cast<double>(pProperties->GetDeltaTime());
		if (pProperties->GetAdaptiveTimeSteps())
		{
			if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER)
				bSuccess = IntegrateEulerAdaptive(dVisTime, dDeltaT, bComputeScalars);
			else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3)
				bSuccess = IntegrateRK3Adaptive(dVisTime, dDeltaT, bComputeScalars);
			else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4)
				bSuccess = IntegrateRK4Adaptive(dVisTime, dDeltaT, bComputeScalars);
		}
		else
		{
			if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER)
				bSuccess = IntegrateEuler(dVisTime, dDeltaT, bComputeScalars);
			else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3)
				bSuccess = IntegrateRK3(dVisTime, dDeltaT, bComputeScalars);
			else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4)
				bSuccess = IntegrateRK4(dVisTime, dDeltaT, bComputeScalars);
		}
	}

	if (bSuccess)
	{
		VflGpuParticleData *pParticles = m_pParent->GetParticleData();

		int iWidth, iHeight;
		pParticles->GetSize(iWidth, iHeight);
		int iLine = pParticles->GetCurrentLine();

		if (!pProperties->GetSingleBuffered())
		{
			float *pOld = pParticles->GetCurrentData();
			float *pNew = pParticles->GetStaleData();

			// copy current particle positions
			pNew = &(pNew[iLine*4*iWidth]);
			pOld = &(pOld[iLine*4*iWidth]);
			memcpy(pNew, pOld, iWidth*4*sizeof(float));

			// do the same for particle attributes
			pOld = pParticles->GetCurrentAttributes();
			pNew = pParticles->GetStaleAttributes();
			pNew = &(pNew[iLine*4*iWidth]);
			pOld = &(pOld[iLine*4*iWidth]);
			memcpy(pNew, pOld, iWidth*4*sizeof(float));

			pParticles->Swap();
		}

		// advance current line
		pParticles->SetCurrentLine((iLine+1)%iHeight);

		m_pParent->GetParticleData()->SignalCpuDataChange();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticle                                                */
/*                                                                            */
/*============================================================================*/
void VflCpuTracersTetGrid::SeedParticle(const VistaVector3D &v3Pos)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount, iLength;
	pParticles->GetSize(iCount, iLength);
	int iActive = pParticles->GetActiveTracers();
	int iNext = pParticles->GetNextTracer();

	if (iActive < iCount)
	{
		++iActive;
		pParticles->SetActiveTracers(iActive);
		if (iActive == iCount)
		{
			cout << " [VflCpuTracersTetGrid] - all tracers are active now..." << endl;
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	float fCell = float(pGrid->GetPointLocator()->GetCellId(&v3Pos[0]));
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		float *pPos1 = &pParticles->GetCurrentData()[4*iNext];
		float *pAttrib1 = &pParticles->GetCurrentAttributes()[4*iNext];
		pParticles->SetNextTracer((iNext+1)%iCount);

		for (int i=0; i<iLength; ++i, pPos1+=4*iCount, pAttrib1+=4*iCount)
		{
			memcpy(pPos1, &v3Pos[0], 4*sizeof(float));
			pAttrib1[0] = fCell;
		}
	}
	else
	{
		float *pPos1 = &pParticles->GetCurrentData()[4*iNext];
		float *pPos2 = &pParticles->GetStaleData()[4*iNext];
		float *pAttrib1 = &pParticles->GetCurrentAttributes()[4*iNext];
		float *pAttrib2 = &pParticles->GetStaleAttributes()[4*iNext];
		pParticles->SetNextTracer((iNext+1)%iCount);

		for (int i=0; i<iLength; ++i, pPos1+=4*iCount, pPos2+=4*iCount, pAttrib1+=4*iCount, pAttrib2+=4*iCount)
		{
			memcpy(pPos1, &v3Pos[0], 4*sizeof(float));
			memcpy(pPos2, &v3Pos[0], 4*sizeof(float));
			pAttrib1[0] = pAttrib2[0] = fCell;
		}
	}

	pParticles->SignalCpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticles                                               */
/*                                                                            */
/*============================================================================*/
void VflCpuTracersTetGrid::SeedParticles(const VistaVector3D &v3Pos1,
										  const VistaVector3D &v3Pos2, 
										  int iCount)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iParticleCount, iLength;
	pParticles->GetSize(iParticleCount, iLength);
	int iActive = pParticles->GetActiveTracers();
	int iNext = pParticles->GetNextTracer();

	if (iCount > iParticleCount)
		iCount = iParticleCount;

	float fDelta = 1.0f;
	if (iCount > 1)
		fDelta /= (iCount-1);
	float fOffset = 0;
	VistaVector3D v3Delta(v3Pos2-v3Pos1);
	float fScalarDelta(v3Pos2[3]-v3Pos1[3]);
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		for (int i=0; i<iCount; ++i, fOffset+=fDelta)
		{
			float *pPos1 = &pParticles->GetCurrentData()[4*(iNext)];
			float *pAttrib1 = &pParticles->GetCurrentAttributes()[4*(iNext++)];
			iNext %= iParticleCount;

			if (iActive < iParticleCount)
			{
				++iActive;
				pParticles->SetActiveTracers(iActive);
				if (iActive == iParticleCount)
				{
					cout << " [VflCpuTracersTetGrid] - all tracers are active now..." << endl;
				}
			}

			VistaVector3D v3NewPos(v3Pos1 + fOffset*v3Delta);
			v3NewPos[3] = v3Pos1[3] + fOffset*fScalarDelta;
			float fCell = float(pLocator->GetCellId(&v3NewPos[0]));
			for (int j=0; j<iLength; ++j, pPos1+=4*iParticleCount, pAttrib1+=4*iParticleCount)
			{
				memcpy(pPos1, &v3NewPos[0], 4*sizeof(float));
				pAttrib1[0] = fCell;
			}
		}
	}
	else
	{
		for (int i=0; i<iCount; ++i, fOffset+=fDelta)
		{
			float *pPos1 = &pParticles->GetCurrentData()[4*(iNext)];
			float *pPos2 = &pParticles->GetStaleData()[4*(iNext)];
			float *pAttrib1 = &pParticles->GetCurrentAttributes()[4*iNext];
			float *pAttrib2 = &pParticles->GetStaleAttributes()[4*(iNext++)];
			iNext %= iParticleCount;

			if (iActive < iParticleCount)
			{
				++iActive;
				pParticles->SetActiveTracers(iActive);
				if (iActive == iParticleCount)
				{
					cout << " [VflCpuTracersTetGrid] - all tracers are active now..." << endl;
				}
			}

			VistaVector3D v3NewPos(v3Pos1 + fOffset*v3Delta);
			v3NewPos[3] = v3Pos1[3] + fOffset*fScalarDelta;
			float fCell = float(pLocator->GetCellId(&v3NewPos[0]));
			for (int j=0; j<iLength; ++j, pPos1+=4*iParticleCount, pPos2+=4*iParticleCount, pAttrib1+=4*iParticleCount, pAttrib2+=4*iParticleCount)
			{
				memcpy(pPos1, &v3NewPos[0], 4*sizeof(float));
				memcpy(pPos2, &v3NewPos[0], 4*sizeof(float));
				pAttrib1[0] = pAttrib2[0] = fCell;
			}
		}
	}
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	pParticles->SetNextTracer(iNext);
	pParticles->SignalCpuDataChange();
}

void VflCpuTracersTetGrid::SeedParticles(const std::vector<float> &vecPositions)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid || !pGrid->GetValid())
		return;

	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iParticleCount, iLength;
	pParticles->GetSize(iParticleCount, iLength);
	int iActive = pParticles->GetActiveTracers();
	int iNext = pParticles->GetNextTracer();
	int iCount = int(vecPositions.size()) / 4;

	if (iCount > iParticleCount)
		iCount = iParticleCount;

	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();
	const float *pPositions = &vecPositions[0];

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		float *pCurrentData = pParticles->GetCurrentData();
		float *pCurrentAttribs = pParticles->GetCurrentAttributes();

		for (int i=0; i<iCount; ++i)
		{
			float *pPos1 = &pCurrentData[4*(iNext)];
			float *pAttrib1 = &pCurrentAttribs[4*(iNext++)];
			iNext %= iParticleCount;

			if (iActive < iParticleCount)
			{
				++iActive;
				pParticles->SetActiveTracers(iActive);
				if (iActive == iParticleCount)
				{
					cout << " [VflCpuTracersTetGrid] - all tracers are active now..." << endl;
				}
			}

			float fCell = float(pLocator->GetCellId(&pPositions[4*i]));
			for (int j=0; j<iLength; ++j, pPos1+=4*iParticleCount, pAttrib1+=4*iParticleCount)
			{
				memcpy(pPos1, &pPositions[4*i], 4*sizeof(float));
				pAttrib1[0] = fCell;
			}
		}
	}
	else
	{
		float *pCurrentData = pParticles->GetCurrentData();
		float *pStaleData = pParticles->GetStaleData();
		float *pCurrentAttribs = pParticles->GetCurrentAttributes();
		float *pStaleAttribs = pParticles->GetStaleAttributes();

		for (int i=0; i<iCount; ++i)
		{
			float *pPos1 = &pCurrentData[4*(iNext)];
			float *pPos2 = &pStaleData[4*(iNext)];
			float *pAttrib1 = &pCurrentAttribs[4*iNext];
			float *pAttrib2 = &pStaleAttribs[4*(iNext++)];
			iNext %= iParticleCount;

			if (iActive < iParticleCount)
			{
				++iActive;
				pParticles->SetActiveTracers(iActive);
				if (iActive == iParticleCount)
				{
					cout << " [VflCpuTracersTetGrid] - all tracers are active now..." << endl;
				}
			}

			float fCell = float(pLocator->GetCellId(&pPositions[4*i]));
			for (int j=0; j<iLength; ++j, pPos1+=4*iParticleCount, pPos2+=4*iParticleCount, pAttrib1+=4*iParticleCount, pAttrib2+=4*iParticleCount)
			{
				memcpy(pPos1, &pPositions[4*i], 4*sizeof(float));
				memcpy(pPos2, &pPositions[4*i], 4*sizeof(float));
				pAttrib1[0] = pAttrib2[0] = fCell;
			}
		}
	}
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	pParticles->SetNextTracer(iNext);
	pParticles->SignalCpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticleNormalized                                      */
/*                                                                            */
/*============================================================================*/
void VflCpuTracersTetGrid::SeedParticleNormalized(const VistaVector3D &v3Pos)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3PosDN;
	DeNormalize(v3PosDN, v3Pos, pGrid);
	SeedParticle(v3PosDN);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticlesNormalized                                     */
/*                                                                            */
/*============================================================================*/
void VflCpuTracersTetGrid::SeedParticlesNormalized(const VistaVector3D &v3Pos1, 
													  const VistaVector3D &v3Pos2, 
													  int iCount)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	VistaVector3D v3Pos1DN, v3Pos2DN;
	DeNormalize(v3Pos1DN, v3Pos1, pGrid);
	DeNormalize(v3Pos2DN, v3Pos2, pGrid);
	SeedParticles(v3Pos1DN, v3Pos2DN, iCount);
}

void VflCpuTracersTetGrid::SeedParticlesNormalized(const std::vector<float> &vecPositions)
{
	double dVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

	vector<float> vecDNPositions;
	int iCount = int(vecPositions.size()) / 4;
	vecDNPositions.resize(vecPositions.size());
	for (int i=0; i<iCount; ++i)
	{
		DeNormalize(&vecDNPositions[4*i], &vecPositions[4*i], pGrid);
	}

	SeedParticles(vecDNPositions);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGridType                                                 */
/*                                                                            */
/*============================================================================*/
int VflCpuTracersTetGrid::GetGridType() const
{
	return VflVisGpuParticles::GT_TET_GRID;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProcessingUnit                                           */
/*                                                                            */
/*============================================================================*/
int VflCpuTracersTetGrid::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_CPU;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerType                                               */
/*                                                                            */
/*============================================================================*/
int VflCpuTracersTetGrid::GetTracerType() const
{
	return VflVisGpuParticles::TT_TRACERS;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflCpuTracersTetGrid::IsValid() const
{
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflCpuTracersTetGrid::GetReflectionableType() const
{
	return s_strCVflCpuTracersTetGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateEuler                                              */
/*                                                                            */
/*============================================================================*/
bool VflCpuTracersTetGrid::IntegrateEuler(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveTracers();
	int iWidth, iHeight;
	pParticles->GetSize(iWidth, iHeight);

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	// find line to insert new particle data
	int iCurrentLine = pParticles->GetCurrentLine();
	int iTargetLine = (iCurrentLine+1) % iHeight;
	pNew = &(pNew[iTargetLine*4*iWidth]);
	pOld = &(pOld[iCurrentLine*4*iWidth]);
	pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
	pOldAttrib = &(pOldAttrib[iCurrentLine*4*iWidth]);

	int iCell;
	float aVel[4];

	for (int i=0; i<iCount; ++i)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), aVel);

		VEC_SMAD(pNew, static_cast<float>(dDeltaT), aVel, pOld);
//		pNew[3] = (bComputeScalars ? aVel[3] : pOld[3];

		pNewAttrib[0] = float(iCell);
		pNew += 4;
		pOld += 4;
		pNewAttrib += 4;
		pOldAttrib += 4;
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		pNew = &(pNew[iTargetLine*4*iWidth]);
		pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			pNew = &(pNew[iTargetLine*4*iWidth]);
			pOld = &(pOld[iCurrentLine*4*iWidth]);
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK3                                                */
/*                                                                            */
/*============================================================================*/
bool VflCpuTracersTetGrid::IntegrateRK3(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveTracers();
	int iWidth, iHeight;
	pParticles->GetSize(iWidth, iHeight);

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	// find line to insert new particle data
	int iCurrentLine = pParticles->GetCurrentLine();
	int iTargetLine = (iCurrentLine+1) % iHeight;
	pNew = &(pNew[iTargetLine*4*iWidth]);
	pOld = &(pOld[iCurrentLine*4*iWidth]);
	pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
	pOldAttrib = &(pOldAttrib[iCurrentLine*4*iWidth]);

	int iCell;
	float dx1[3], dx2[3], dx3[3], dxt[3];

	for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), dx1);
		if (iCell < 0)
		{
			pNew[0] = pOld[0];
			pNew[1] = pOld[1];
			pNew[2] = pOld[2];
			pNewAttrib[0] = -1.0f;
			continue;
		}

		SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

		VEC_SMAD(dxt, 0.5f, dx1, pOld);
		RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx2);
		SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

		VEC_SMAD(dxt, -1.0f, dx1, pOld);
		VEC_SMAD(dxt, 2.0f, dx2, dxt);
		RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx3);
		SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

		pNew[0] = pOld[0] + (dx1[0] + 2*dx2[0] + dx3[0]) / 6.0f;
		pNew[1] = pOld[1] + (dx1[1] + 2*dx2[1] + dx3[1]) / 6.0f;
		pNew[2] = pOld[2] + (dx1[2] + 2*dx2[2] + dx3[2]) / 6.0f;

		pNewAttrib[0] = float(iCell);
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		pNew = &(pNew[iTargetLine*4*iWidth]);
		pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			pNew = &(pNew[iTargetLine*4*iWidth]);
			pOld = &(pOld[iCurrentLine*4*iWidth]);
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK4                                                */
/*                                                                            */
/*============================================================================*/
bool VflCpuTracersTetGrid::IntegrateRK4(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveTracers();
	int iWidth, iHeight;
	pParticles->GetSize(iWidth, iHeight);

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	// find line to insert new particle data
	int iCurrentLine = pParticles->GetCurrentLine();
	int iTargetLine = (iCurrentLine+1) % iHeight;
	pNew = &(pNew[iTargetLine*4*iWidth]);
	pOld = &(pOld[iCurrentLine*4*iWidth]);
	pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
	pOldAttrib = &(pOldAttrib[iCurrentLine*4*iWidth]);

	int iCell;
	float dx1[3], dx2[3], dx3[3], dx4[3], dxt[3];

	for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), dx1);
		if (iCell < 0)
		{
			pNew[0] = pOld[0];
			pNew[1] = pOld[1];
			pNew[2] = pOld[2];
			pNewAttrib[0] = -1.0f;
			continue;
		}

		SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

		VEC_SMAD(dxt, 0.5f, dx1, pOld);
		RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx2);
		SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

		VEC_SMAD(dxt, 0.5f, dx2, pOld);
		RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx3);
		SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

		VEC_SMAD(dxt, 1.0f, dx3, pOld);
		RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx4);
		SCALAR_MULT(dx4, static_cast<float>(dDeltaT));

		pNew[0] = pOld[0] + (dx1[0] + 2*dx2[0] + 2*dx3[0] + dx4[0]) / 6.0f;
		pNew[1] = pOld[1] + (dx1[1] + 2*dx2[1] + 2*dx3[1] + dx4[1]) / 6.0f;
		pNew[2] = pOld[2] + (dx1[2] + 2*dx2[2] + 2*dx3[2] + dx4[2]) / 6.0f;

		pNewAttrib[0] = float(iCell);
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		pNew = &(pNew[iTargetLine*4*iWidth]);
		pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			pNew = &(pNew[iTargetLine*4*iWidth]);
			pOld = &(pOld[iCurrentLine*4*iWidth]);
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateEulerAdaptive                                      */
/*                                                                            */
/*============================================================================*/
bool VflCpuTracersTetGrid::IntegrateEulerAdaptive(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveTracers();
	int iWidth, iHeight;
	pParticles->GetSize(iWidth, iHeight);

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	// find line to insert new particle data
	int iCurrentLine = pParticles->GetCurrentLine();
	int iTargetLine = (iCurrentLine+1) % iHeight;
	pNew = &(pNew[iTargetLine*4*iWidth]);
	pOld = &(pOld[iCurrentLine*4*iWidth]);
	pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
	pOldAttrib = &(pOldAttrib[iCurrentLine*4*iWidth]);

	int iCell;
	float aVel[4];
	int *pCells = pGrid->GetCells();
	float *pVertices = pGrid->GetVertices();
	int *pCell;
	int iIterationCount;
	double dDeltaTLocal;
	float fVelMag;
	float fInsphere, fCircumsphere;

	for (int i=0; i<iCount; ++i)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), aVel);
		if (pNew!=pOld)
			memcpy(pNew, pOld, 3*sizeof(float));
		if (iCell >= 0)
		{
			pCell = &pCells[4*iCell];
			ComputeRadii(fInsphere, fCircumsphere, pCell, pVertices);
			dDeltaTLocal = dDeltaT;

			iIterationCount = 1;
			if (fInsphere/fCircumsphere > 1e-5)
			{
				fVelMag = sqrt(VEC_LENGTH(aVel)) * static_cast<float>(dDeltaT);
				iIterationCount = int(ceil(fVelMag/fInsphere));
				dDeltaTLocal /= iIterationCount;
			}

			while(true)
			{
				VEC_SMAD(pNew, static_cast<float>(dDeltaTLocal), aVel, pNew);

				if (--iIterationCount <= 0)
					break;

				iCell = RetrieveVelocity(pGrid, pLocator, pNew, iCell, aVel);
			}
		}

		pNewAttrib[0] = float(iCell);
		pNew += 4;
		pOld += 4;
		pNewAttrib += 4;
		pOldAttrib += 4;
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		pNew = &(pNew[iTargetLine*4*iWidth]);
		pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			pNew = &(pNew[iTargetLine*4*iWidth]);
			pOld = &(pOld[iCurrentLine*4*iWidth]);
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK3Adaptive                                        */
/*                                                                            */
/*============================================================================*/
bool VflCpuTracersTetGrid::IntegrateRK3Adaptive(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveTracers();
	int iWidth, iHeight;
	pParticles->GetSize(iWidth, iHeight);

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	// find line to insert new particle data
	int iCurrentLine = pParticles->GetCurrentLine();
	int iTargetLine = (iCurrentLine+1) % iHeight;
	pNew = &(pNew[iTargetLine*4*iWidth]);
	pOld = &(pOld[iCurrentLine*4*iWidth]);
	pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
	pOldAttrib = &(pOldAttrib[iCurrentLine*4*iWidth]);

	int iCell;
	float dx1[3], dx2[3], dx3[3], dxt[3];
	int *pCells = pGrid->GetCells();
	float *pVertices = pGrid->GetVertices();
	int *pCell;
	int iIterationCount;
	double dDeltaTLocal;
	float fVelMag;
	float fInsphere, fCircumsphere;

	for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), dx1);
		if (pNew!=pOld)
			memcpy(pNew, pOld, 3*sizeof(float));
		if (iCell < 0)
		{
			pNewAttrib[0] = -1.0f;
			continue;
		}

		pCell = &pCells[4*iCell];
		ComputeRadii(fInsphere, fCircumsphere, pCell, pVertices);
		dDeltaTLocal = dDeltaT;

		iIterationCount = 1;
		if (fInsphere/fCircumsphere > 1e-5)
		{
			fVelMag = sqrt(VEC_LENGTH(dx1)) * static_cast<float>(dDeltaT);
			iIterationCount = int(ceil(fVelMag/fInsphere));
			dDeltaTLocal /= iIterationCount;
		}

		while (true)
		{
			SCALAR_MULT(dx1, static_cast<float>(dDeltaTLocal));
			VEC_SMAD(dxt, 0.5f, dx1, pNew);
			RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx2);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, -1.0f, dx1, pNew);
			VEC_SMAD(dxt, 2.0f, dx2, dxt);
			RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx3);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaTLocal));

			pNew[0] += (dx1[0] + 2*dx2[0] + dx3[0]) / 6.0f;
			pNew[1] += (dx1[1] + 2*dx2[1] + dx3[1]) / 6.0f;
			pNew[2] += (dx1[2] + 2*dx2[2] + dx3[2]) / 6.0f;

			if (--iIterationCount <= 0)
				break;

			iCell = RetrieveVelocity(pGrid, pLocator, pNew, iCell, dx1);
		}

		pNewAttrib[0] = float(iCell);
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		pNew = &(pNew[iTargetLine*4*iWidth]);
		pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			pNew = &(pNew[iTargetLine*4*iWidth]);
			pOld = &(pOld[iCurrentLine*4*iWidth]);
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK4Adaptive                                        */
/*                                                                            */
/*============================================================================*/
bool VflCpuTracersTetGrid::IntegrateRK4Adaptive(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveTracers();
	int iWidth, iHeight;
	pParticles->GetSize(iWidth, iHeight);

	VveUnsteadyTetGrid *pUGrid = m_pParent->GetDataAsTetGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveTetGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}
	VveTetGridPointLocator *pLocator = pGrid->GetPointLocator();

	register float *pOld = pParticles->GetCurrentData();
	register float *pNew = pParticles->GetStaleData();
	register float *pOldAttrib = pParticles->GetCurrentAttributes();
	register float *pNewAttrib = pParticles->GetStaleAttributes();
	if (m_pParent->GetProperties()->GetSingleBuffered())
	{
		pNew = pOld;
		pNewAttrib = pOldAttrib;
	}

	// find line to insert new particle data
	int iCurrentLine = pParticles->GetCurrentLine();
	int iTargetLine = (iCurrentLine+1) % iHeight;
	pNew = &(pNew[iTargetLine*4*iWidth]);
	pOld = &(pOld[iCurrentLine*4*iWidth]);
	pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
	pOldAttrib = &(pOldAttrib[iCurrentLine*4*iWidth]);

	int iCell;
	float dx1[3], dx2[3], dx3[3], dx4[3], dxt[3];
	int *pCells = pGrid->GetCells();
	float *pVertices = pGrid->GetVertices();
	int *pCell;
	int iIterationCount;
	double dDeltaTLocal;
	float fVelMag;
	float fInsphere, fCircumsphere;

	for (int i=0; i<iCount; ++i, pNew+=4, pOld+=4, pNewAttrib+=4, pOldAttrib+=4)
	{
		iCell = RetrieveVelocity(pGrid, pLocator, pOld, int(pOldAttrib[0]), dx1);
		if (pNew!=pOld)
			memcpy(pNew, pOld, 3*sizeof(float));
		if (iCell < 0)
		{
			pNewAttrib[0] = -1.0f;
			continue;
		}

		pCell = &pCells[4*iCell];
		ComputeRadii(fInsphere, fCircumsphere, pCell, pVertices);
		dDeltaTLocal = dDeltaT;

		iIterationCount = 1;
		if (fInsphere/fCircumsphere > 1e-5)
		{
			fVelMag = sqrt(VEC_LENGTH(dx1)) * static_cast<float>(dDeltaT);
			iIterationCount = int(ceil(fVelMag/fInsphere));
			dDeltaTLocal /= iIterationCount;
		}

		while (true)
		{
			SCALAR_MULT(dx1, static_cast<float>(dDeltaTLocal));

			VEC_SMAD(dxt, 0.5f, dx1, pNew);
			RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx2);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaTLocal));

			VEC_SMAD(dxt, 0.5f, dx2, pNew);
			RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx3);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaTLocal));

			VEC_SMAD(dxt, 1.0f, dx3, pNew);
			RetrieveVelocity(pGrid, pLocator, dxt, iCell, dx4);
			SCALAR_MULT(dx4, static_cast<float>(dDeltaTLocal));

			pNew[0] += (dx1[0] + 2*dx2[0] + 2*dx3[0] + dx4[0]) / 6.0f;
			pNew[1] += (dx1[1] + 2*dx2[1] + 2*dx3[1] + dx4[1]) / 6.0f;
			pNew[2] += (dx1[2] + 2*dx2[2] + 2*dx3[2] + dx4[2]) / 6.0f;

			if (--iIterationCount <= 0)
				break;

			iCell = RetrieveVelocity(pGrid, pLocator, pNew, iCell, dx1);
		}

		pNewAttrib[0] = float(iCell);
	}

	if (bComputeScalars)
	{
		if (m_pParent->GetProperties()->GetSingleBuffered())
		{
			pNew = pParticles->GetCurrentData();
			pNewAttrib = pParticles->GetCurrentAttributes();
		}
		else
		{
			pNew = pParticles->GetStaleData();
			pNewAttrib = pParticles->GetStaleAttributes();
		}
		pNew = &(pNew[iTargetLine*4*iWidth]);
		pNewAttrib = &(pNewAttrib[iTargetLine*4*iWidth]);
		for (int i=0; i<iCount; ++i)
		{
			iCell = RetrieveScalar(pGrid, pLocator, pNew, int(pNewAttrib[0]), pNew[3]);
			pNewAttrib[0] = float(iCell);
			pNew += 4;
			pNewAttrib += 4;
		}
	}
	else
	{
		if (!m_pParent->GetProperties()->GetSingleBuffered())
		{
			pOld = pParticles->GetCurrentData();
			pNew = pParticles->GetStaleData();
			pNew = &(pNew[iTargetLine*4*iWidth]);
			pOld = &(pOld[iCurrentLine*4*iWidth]);
			for (int i=0; i<iCount; ++i)
			{
				pNew[3] = pOld[3];
				pNew += 4;
				pOld += 4;
			}
		}
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
	return true;
}

///*============================================================================*/
///*                                                                            */
///*  NAME      :   IntegrateEulerUnsteady                                      */
///*                                                                            */
///*============================================================================*/
//bool VflCpuTracersTetGrid::IntegrateEulerUnsteady(float fVisTime, 
//													  float fLastVisTime,
//													  float fDeltaT, 
//													  bool bComputeScalars)
//{
//	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
//	int iCount = pParticles->GetActiveParticles();
//
//	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
//	int iIndex1, iIndex2;
//	float fAlpha = pUGrid->GetTimeMapper()->GetWeightedLevelIndices(fLastVisTime, iIndex1, iIndex2);
////	float fAlpha = pUGrid->GetTimeMapper()->GetWeightedLevelIndices(fVisTime, iIndex1, iIndex2);
//
//	if (fAlpha < 0)
//		return false;
//
//	pUGrid->GetTypedLevelDataByLevelIndex(iIndex1)->LockData();
//	pUGrid->GetTypedLevelDataByLevelIndex(iIndex2)->LockData();
//	VflCartesianGrid *pGrid1 = pUGrid->GetTypedLevelDataByLevelIndex(iIndex1)->GetData();
//	VflCartesianGrid *pGrid2 = pUGrid->GetTypedLevelDataByLevelIndex(iIndex2)->GetData();
//	if (!pGrid1->GetValid() || !pGrid2->GetValid())
//	{
//		pUGrid->GetTypedLevelDataByLevelIndex(iIndex1)->UnlockData();
//		pUGrid->GetTypedLevelDataByLevelIndex(iIndex2)->UnlockData();
//		return false;
//	}
//
//	float aVel1[4], aVel2[4];
//	float *pOld = pParticles->GetCurrentData();
//	float *pNew = pParticles->GetStaleData();
//	if (m_pParent->GetProperties()->GetSingleBuffered())
//		pNew = pOld;
//
//	VistaVector3D v3Min, v3Max;
//	pGrid1->GetBounds(v3Min, v3Max);
//	float fOneMinusAlpha = 1.0f - fAlpha;
//
//	for (int i=0; i<iCount; ++i)
//	{
//		pGrid1->GetData(pOld, aVel1);
//		pGrid2->GetData(pOld, aVel2);
//		for (int j=0; j<3; ++j)
//		{
//			pNew[j] = pOld[j] + fDeltaT * (fOneMinusAlpha*aVel1[j] + fAlpha*aVel2[j]);
//			if (pNew[j] < v3Min[j])
//				pNew[j] = v3Min[j];
//			else if (pNew[j] > v3Max[j])
//				pNew[j] = v3Max[j];
//		}
//		pNew[3] = (bComputeScalars ? fOneMinusAlpha*aVel1[3] + fAlpha*aVel2[3] : pOld[3]);
//
//		pNew += 4;
//		pOld += 4;
//	}
//
//	pUGrid->GetTypedLevelDataByLevelIndex(iIndex1)->UnlockData();
//	pUGrid->GetTypedLevelDataByLevelIndex(iIndex2)->UnlockData();
//	return true;
//}
//
///*============================================================================*/
///*                                                                            */
///*  NAME      :   IntegrateRK3Unsteady                                        */
///*                                                                            */
///*============================================================================*/
//bool VflCpuTracersTetGrid::IntegrateRK3Unsteady(float fVisTime, 
//													float fLastVisTime,
//													float fDeltaT, 
//													bool bComputeScalars)
//{
//	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
//	int iCount = pParticles->GetActiveParticles();
//
//	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
//	float aAlpha[3];
//	int aIndices[6];
//
//	// determine t+h/2
//	// we already know t==fLastVisTime and t+h==fVisTime
//	float fIntermediateTime = fLastVisTime + 0.5f*(fVisTime-fLastVisTime);
//	if (fLastVisTime > fVisTime)
//	{
//		fIntermediateTime = fLastVisTime + 0.5f;
//		if (fIntermediateTime > 1.0f)
//			fIntermediateTime -= 1.0f;
//	}
//
//	// retrieve interpolation weights for all t, t+h/2 and t+h
//	aAlpha[0] = pUGrid->GetTimeMapper()->GetWeightedLevelIndices(fLastVisTime, aIndices[0], aIndices[1]);
//	aAlpha[1] = pUGrid->GetTimeMapper()->GetWeightedLevelIndices(fIntermediateTime, aIndices[2], aIndices[3]);
//	aAlpha[2] = pUGrid->GetTimeMapper()->GetWeightedLevelIndices(fVisTime, aIndices[4], aIndices[5]);
//
//	if (aAlpha[0] < 0 || aAlpha[1] < 0 || aAlpha[2] < 0)
//		return false;
//
//	// lock indices
//	set<int> setIndices;
//	for (int i=0; i<6; ++i)
//		setIndices.insert(aIndices[i]);
//
//	set<int>::iterator it;
//	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
//		pUGrid->GetTypedLevelDataByLevelIndex(*it)->LockData();
//
//	VflCartesianGrid *aGrids[6];
//	bool bValid = true;
//	for (int i=0; i<6; ++i)
//	{
//		aGrids[i] = pUGrid->GetTypedLevelDataByLevelIndex(aIndices[i])->GetData();
//		bValid = bValid & aGrids[i]->GetValid();
//	}
//
//	if (!bValid)
//	{
//		for (it=setIndices.begin(); it!=setIndices.end(); ++it)
//			pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
//		return false;
//	}
//
//	float dx1[4], dx2[4], dx3[4], dxt[4], dxt1[4], dxt2[4];
//	float *pOld = pParticles->GetCurrentData();
//	float *pNew = pParticles->GetStaleData();
//	if (m_pParent->GetProperties()->GetSingleBuffered())
//		pNew = pOld;
//
//	VistaVector3D v3Min, v3Max;
//	aGrids[0]->GetBounds(v3Min, v3Max);
//
//	for (int i=0; i<iCount; ++i)
//	{
//		aGrids[0]->GetData(pOld, dxt1);
//		aGrids[1]->GetData(pOld, dxt2);
//		INTERPOLATE(dx1, dxt1, dxt2, aAlpha[0]);
//		SCALAR_MULT(dx1, fDeltaT);
//
//		VEC_SMAD(dxt, 0.5f, dx1, pOld);
////		SATURATE_LIMIT(dxt, v3Min, v3Max);
//		aGrids[2]->GetData(dxt, dxt1);
//		aGrids[3]->GetData(dxt, dxt2);
//		INTERPOLATE(dx2, dxt1, dxt2, aAlpha[1]);
//		SCALAR_MULT(dx2, fDeltaT);
//
//		VEC_SMAD(dxt, -1.0f, dx1, pOld);
//		VEC_SMAD(dxt, 2.0f, dx2, dxt);
////		SATURATE_LIMIT(dxt, v3Min, v3Max);
//		aGrids[4]->GetData(dxt, dxt1);
//		aGrids[5]->GetData(dxt, dxt2);
//		INTERPOLATE(dx3, dxt1, dxt2, aAlpha[2]);
//		SCALAR_MULT(dx3, fDeltaT);
//
//		for (int j=0; j<3; ++j)
//		{
//			pNew[j] = pOld[j] + (dx1[j] + 2*dx2[j] + dx3[j]) / 6.0f;
//			if (pNew[j] < v3Min[j])
//				pNew[j] = v3Min[j];
//			else if (pNew[j] > v3Max[j])
//				pNew[j] = v3Max[j];
//		}
////		pNew[3] = (bComputeScalars ? (dx1[3] + 2*dx2[3] + 2*dx3[3] + dx4[3]) / 6.0f : pOld[3]);
//		pNew[3] = (bComputeScalars ? dx1[3] : pOld[3]);
//
//		pNew += 4;
//		pOld += 4;
//	}
//
//	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
//		pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
//	return true;
//}
//
///*============================================================================*/
///*                                                                            */
///*  NAME      :   IntegrateRK4Unsteady                                        */
///*                                                                            */
///*============================================================================*/
//bool VflCpuTracersTetGrid::IntegrateRK4Unsteady(float fVisTime,
//													float fLastVisTime,
//													float fDeltaT, 
//													bool bComputeScalars)
//{
//	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
//	int iCount = pParticles->GetActiveParticles();
//
//	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
//	float aAlpha[3];
//	int aIndices[6];
//
//	// determine t+h/2
//	// we already know t==fLastVisTime and t+h==fVisTime
//	float fIntermediateTime = fLastVisTime + 0.5f*(fVisTime-fLastVisTime);
//	if (fLastVisTime > fVisTime)
//	{
//		fIntermediateTime = fLastVisTime + 0.5f;
//		if (fIntermediateTime > 1.0f)
//			fIntermediateTime -= 1.0f;
//	}
//
//	// retrieve interpolation weights for all t, t+h/2 and t+h
//	aAlpha[0] = pUGrid->GetTimeMapper()->GetWeightedLevelIndices(fLastVisTime, aIndices[0], aIndices[1]);
//	aAlpha[1] = pUGrid->GetTimeMapper()->GetWeightedLevelIndices(fIntermediateTime, aIndices[2], aIndices[3]);
//	aAlpha[2] = pUGrid->GetTimeMapper()->GetWeightedLevelIndices(fVisTime, aIndices[4], aIndices[5]);
//
//	if (aAlpha[0] < 0 || aAlpha[1] < 0 || aAlpha[2] < 0)
//		return false;
//
//	// lock indices
//	set<int> setIndices;
//	for (int i=0; i<6; ++i)
//		setIndices.insert(aIndices[i]);
//
//	set<int>::iterator it;
//	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
//		pUGrid->GetTypedLevelDataByLevelIndex(*it)->LockData();
//
//	VflCartesianGrid *aGrids[6];
//	bool bValid = true;
//	for (int i=0; i<6; ++i)
//	{
//		aGrids[i] = pUGrid->GetTypedLevelDataByLevelIndex(aIndices[i])->GetData();
//		bValid = bValid & aGrids[i]->GetValid();
//	}
//
//	if (!bValid)
//	{
//		for (it=setIndices.begin(); it!=setIndices.end(); ++it)
//			pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
//		return false;
//	}
//
//	float dx1[4], dx2[4], dx3[4], dx4[4], dxt[4], dxt1[4], dxt2[4];
//	float *pOld = pParticles->GetCurrentData();
//	float *pNew = pParticles->GetStaleData();
//	if (m_pParent->GetProperties()->GetSingleBuffered())
//		pNew = pOld;
//
//	VistaVector3D v3Min, v3Max;
//	aGrids[0]->GetBounds(v3Min, v3Max);
//
//	for (int i=0; i<iCount; ++i)
//	{
//		aGrids[0]->GetData(pOld, dxt1);
//		aGrids[1]->GetData(pOld, dxt2);
//		INTERPOLATE(dx1, dxt1, dxt2, aAlpha[0]);
//		SCALAR_MULT(dx1, fDeltaT);
//
//		VEC_SMAD(dxt, 0.5f, dx1, pOld);
//		aGrids[2]->GetData(dxt, dxt1);
//		aGrids[3]->GetData(dxt, dxt2);
//		INTERPOLATE(dx2, dxt1, dxt2, aAlpha[1]);
//		SCALAR_MULT(dx2, fDeltaT);
//
//		VEC_SMAD(dxt, 0.5f, dx2, pOld);
//		aGrids[2]->GetData(dxt, dxt1);
//		aGrids[3]->GetData(dxt, dxt2);
//		INTERPOLATE(dx3, dxt1, dxt2, aAlpha[1]);
//		SCALAR_MULT(dx3, fDeltaT);
//
//		VEC_ADD(dxt, dx3, pOld);
//		aGrids[4]->GetData(dxt, dxt1);
//		aGrids[5]->GetData(dxt, dxt2);
//		INTERPOLATE(dx4, dxt1, dxt2, aAlpha[2]);
//		SCALAR_MULT(dx4, fDeltaT);
//
//		for (int j=0; j<3; ++j)
//		{
//			pNew[j] = pOld[j] + (dx1[j] + 2*dx2[j] + 2*dx3[j] + dx4[j]) / 6.0f;
//			if (pNew[j] < v3Min[j])
//				pNew[j] = v3Min[j];
//			else if (pNew[j] > v3Max[j])
//				pNew[j] = v3Max[j];
//		}
////		pNew[3] = (bComputeScalars ? (dx1[3] + 2*dx2[3] + 2*dx3[3] + dx4[3]) / 6.0f : pOld[3]);
//		pNew[3] = (bComputeScalars ? dx1[3] : pOld[3]);
//
//		pNew += 4;
//		pOld += 4;
//	}
//
//	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
//		pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
//
//	return true;
//}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflCpuTracersTetGrid::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleTracer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflCpuTracersTetGridReflType);
	return i+1;
}

/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeNormalize                                                 */
/*                                                                            */
/*============================================================================*/
static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveTetGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	v3Out[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * v3In[0];
	v3Out[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * v3In[1];
	v3Out[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * v3In[2];
	v3Out[3] = v3In[3];
}

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveTetGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	aOut[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * aIn[0];
	aOut[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * aIn[1];
	aOut[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * aIn[2];
	aOut[3] = aIn[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RetrieveVelocity                                            */
/*                                                                            */
/*============================================================================*/
static inline int RetrieveVelocity(VveTetGrid *pGrid, VveTetGridPointLocator *pLocator,
								   float aPos[3], int iStartCell, float aData[3])
{
	float aBC[4];
	int iCell = pLocator->TetWalk(iStartCell, aPos, aBC);

	if (iCell < 0)
	{
		memset(aData, 0, 3*sizeof(float));
		return -1;
	}

	int *pCell = &pGrid->GetCells()[4*iCell];
	float *v[4];
	for (int j=0; j<4; ++j)
		v[j] = &pGrid->GetPointData()[pGrid->GetComponents()*pCell[j]];

	for (int j=0; j<3; ++j)
	{
		aData[j] = 0;
		for (int k=0; k<4; ++k)
			aData[j] += aBC[k] * v[k][j];
	}
	return iCell;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RetrieveScalar                                              */
/*                                                                            */
/*============================================================================*/
static inline int RetrieveScalar(VveTetGrid *pGrid, VveTetGridPointLocator *pLocator,
								 float aPos[3], int iStartCell, float &fData)
{
	float aBC[4];
	int iCell = pLocator->TetWalk(iStartCell, aPos, aBC);

	if (iCell < 0)
	{
		fData = 0;
		return -1;
	}

	int *pCell = &pGrid->GetCells()[4*iCell];
	float *v[4];
	for (int j=0; j<4; ++j)
		v[j] = &pGrid->GetPointData()[pGrid->GetComponents()*pCell[j]];

	fData = 0;
	for (int j=0; j<4; ++j)
		fData += aBC[j] * v[j][3];

	return iCell;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeRadii                                                */
/*                                                                            */
/*============================================================================*/
static inline void ComputeRadii(float &fInsphere, float &fCircumsphere,
								int *pCell, float *pVertices)
{
	float *x1(&pVertices[3*pCell[0]]);
	float *x2(&pVertices[3*pCell[1]]);
	float *x3(&pVertices[3*pCell[2]]);
	float *x4(&pVertices[3*pCell[3]]);

	float v21[3], v31[3], v41[3], v32[3], v42[3];
	float v31Xv41[3], vTemp[3], vTemp2[3];
	float L0, L1, L2, L3, fGamma;

	v21[0] = x2[0]-x1[0];
	v21[1] = x2[1]-x1[1];
	v21[2] = x2[2]-x1[2];

	v31[0] = x3[0]-x1[0];
	v31[1] = x3[1]-x1[1];
	v31[2] = x3[2]-x1[2];

	v41[0] = x4[0]-x1[0];
	v41[1] = x4[1]-x1[1];
	v41[2] = x4[2]-x1[2];

	v32[0] = x3[0]-x2[0];
	v32[1] = x3[1]-x2[1];
	v32[2] = x3[2]-x2[2];

	v42[0] = x4[0]-x2[0];
	v42[1] = x4[1]-x2[1];
	v42[2] = x4[2]-x2[2];

	VEC_CROSS(v31Xv41, v31, v41);
	VEC_CROSS(vTemp2, v21, v31);
	L0 = VEC_LENGTH(vTemp2);

	VEC_SMULT(vTemp, VEC_DOT(v41, v41), vTemp2);
	VEC_CROSS(vTemp2, v41, v21);
	L1 = VEC_LENGTH(vTemp2);

	VEC_SMAD(vTemp, VEC_DOT(v31, v31), vTemp2, vTemp);
	VEC_SMAD(vTemp, VEC_DOT(v21, v21), v31Xv41, vTemp);
	fCircumsphere = fabs(0.5f * VEC_LENGTH(vTemp) / VEC_DOT(v21, v31Xv41));

	VEC_CROSS(vTemp, x3, x4);
	fGamma = VEC_DOT(x2, vTemp) - VEC_DOT(x1, vTemp);
	VEC_CROSS(vTemp, x2, x4);
	fGamma += VEC_DOT(x1, vTemp);
	VEC_CROSS(vTemp, x2, x3);
	fGamma -= VEC_DOT(x1, vTemp);

	L2 = VEC_LENGTH(v31Xv41);
	VEC_CROSS(vTemp, v42, v32);
	L3 = VEC_LENGTH(vTemp);
	fInsphere = fabs(fGamma / (L0+L1+L2+L3));
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
