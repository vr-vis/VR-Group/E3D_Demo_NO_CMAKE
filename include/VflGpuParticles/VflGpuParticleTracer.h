/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflGpuParticleTracer.h        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - IVflGpuParticleTracer                                             */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLGPUPARTICLETRACER_H
#define __VFLGPUPARTICLETRACER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflGpuParticlesConfig.h"
#include <VistaAspects/VistaReflectionable.h>
#include <vector>
#include <string>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisGpuParticles;
class VistaVector3D;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI IVflGpuParticleTracer : public IVistaReflectionable
{
public:
	/**
	 * Destructor. Removes self from parent's tracer list, if applicable.
	 */
	virtual ~IVflGpuParticleTracer();

	virtual void Update() = 0;

	virtual void SeedParticle(const VistaVector3D &v3Pos) = 0;
	virtual void SeedParticles(const VistaVector3D &v3Pos1,
		const VistaVector3D &v3Pos2, int iCount) = 0;
	virtual void SeedParticles(const std::vector<float> &vecPositions) = 0;
	virtual void SeedParticleNormalized(const VistaVector3D &v3Pos) = 0;
	virtual void SeedParticlesNormalized(const VistaVector3D &v3Pos1,
		const VistaVector3D &v3Pos2, int iCount) = 0;
	virtual void SeedParticlesNormalized(
		const std::vector<float> &vecPositions) = 0;

	/**
	 * Retrieve the grid type supported by this tracer as defined in 
	 * VflVisGpuParticle::GRID_TYPE
	 */
	virtual int GetGridType() const = 0;

	/**
	 * Retrieve the processing unit for this tracer.
	 */
	virtual int GetProcessingUnit() const = 0;

	/**
	 * Determine the tracer type for this tracer.
	 */
	virtual int GetTracerType() const = 0;

	virtual bool IsValid() const = 0;
	virtual std::string GetReflectionableType() const;

protected:
	/**
	 * Constructor for a new particle tracers. Adds self
	 * as tracer to parent object.
	 */
	IVflGpuParticleTracer(VflVisGpuParticles *pParent);

	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	VflVisGpuParticles			*m_pParent;
};


#endif // __VFLGPUPARTICLETRACER_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

