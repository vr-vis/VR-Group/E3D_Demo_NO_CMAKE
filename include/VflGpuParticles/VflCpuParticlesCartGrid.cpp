/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflCpuParticlesCartGrid.cpp  RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflCpuParticlesCartGrid.h"
#include "VflVisGpuParticles.h"
#include "VflGpuParticleData.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaVisExt/Data/VveCartesianGrid.h>
#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaBase/Half/VistaHalf.h>

#include <set>
#include <string.h>

using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflCpuParticlesCartGridReflType("VflCpuParticlesCartGrid");

static IVistaPropertyGetFunctor *s_aCVflCpuParticlesCartGridGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflCpuParticlesCartGridSetFunctors[] =
{
	NULL
};

#define ASSIGN(dst, src) \
	dst[0] = src[0]; \
	dst[1] = src[1]; \
	dst[2] = src[2]; \
	dst[3] = src[3];
#define SCALAR_MULT(vec, scalar) \
	vec[0] *= scalar; \
	vec[1] *= scalar; \
	vec[2] *= scalar;
#define VEC_SMAD(dest, scalar, add1, add2) \
	dest[0] = scalar*add1[0] + add2[0]; \
	dest[1] = scalar*add1[1] + add2[1]; \
	dest[2] = scalar*add1[2] + add2[2];
#define VEC_ADD(dest, add1, add2) \
	dest[0] = add1[0] + add2[0]; \
	dest[1] = add1[1] + add2[1]; \
	dest[2] = add1[2] + add2[2];
#define SATURATE(vec) \
	for (int _i=0; _i<3; ++_i) \
	{ \
		if (vec[_i] < 0) \
			vec[_i] = 0; \
		else if (vec[_i] > 1) \
			vec[_i] = 1; \
	}
#define SATURATE_LIMIT(vec, min, max) \
	for (int _i=0; _i<3; ++_i) \
	{ \
		if (vec[_i] < min[_i]) \
			vec[_i] = min[_i]; \
		else if (vec[_i] > max[_i]) \
			vec[_i] = max[_i]; \
	}

#define INTERPOLATE(dest, vec1, vec2, alpha) \
	{ \
		float _fOneMinusAlpha = 1.0f - alpha; \
		dest[0] = _fOneMinusAlpha * vec1[0] + alpha * vec2[0]; \
		dest[1] = _fOneMinusAlpha * vec1[1] + alpha * vec2[1]; \
		dest[2] = _fOneMinusAlpha * vec1[2] + alpha * vec2[2]; \
		dest[3] = _fOneMinusAlpha * vec1[3] + alpha * vec2[3]; \
	}

static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveCartesianGrid *pGrid);

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveCartesianGrid *pGrid);

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflCpuParticlesCartGrid::VflCpuParticlesCartGrid(VflVisGpuParticles *pParent)
: IVflGpuParticleTracer(pParent)
{
}

VflCpuParticlesCartGrid::~VflCpuParticlesCartGrid()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflCpuParticlesCartGrid::Update()
{
	VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties = m_pParent->GetProperties();

	if (!pProperties->GetActive())
		return;

	// don't move, please...
//	if (m_pParent->GetRenderNode()->IsAnimationPlaying())
//		return;

	double dVisTime = m_pParent->GetVisTime();
	double dLastVisTime = m_pParent->GetLastVisTime();

	// retrieve integration parameters
	int iIntegrator = pProperties->GetIntegrator();
	bool bComputeScalars = pProperties->GetComputeScalars();
	bool bSuccess = false;
	if (m_pParent->GetRenderNode()->GetVisTiming()->GetAnimationPlaying())
	{
		VveTimeMapper *pTimeMapper = m_pParent->GetData()->GetTimeMapper();
		double dDelta = dVisTime - dLastVisTime;
		while (dDelta < 0.0)
		{
			dDelta += 1.0f;
		}

		double dDeltaT = pTimeMapper->GetSimulationTime(dDelta)
			- pTimeMapper->GetSimulationTime(0);

		if (m_pParent->GetTimeWarning())
		{
			dDeltaT = pProperties->GetMaxDeltaTime();
		}

		if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER)
		{
			bSuccess = IntegrateEulerUnsteady(dVisTime, dLastVisTime, dDeltaT,
			bComputeScalars);
		}
		else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3)
		{
			bSuccess = IntegrateRK3Unsteady(dVisTime, dLastVisTime, dDeltaT,
			bComputeScalars);
		}
		else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4)
		{
			bSuccess = IntegrateRK4Unsteady(dVisTime, dLastVisTime, dDeltaT,
			bComputeScalars);
		}
	}
	else
	{
		float fDeltaT = pProperties->GetDeltaTime();
		if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_EULER)
			bSuccess = IntegrateEuler(dVisTime, fDeltaT, bComputeScalars);
		else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK3)
			bSuccess = IntegrateRK3(dVisTime, fDeltaT, bComputeScalars);
		else if (iIntegrator == VflVisGpuParticles::VflVisGpuParticlesProperties::IR_RK4)
			bSuccess = IntegrateRK4(dVisTime, fDeltaT, bComputeScalars);
	}

	if (bSuccess)
	{
		if (!pProperties->GetSingleBuffered())
			m_pParent->GetParticleData()->Swap();

		m_pParent->GetParticleData()->SignalCpuDataChange();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticle                                                */
/*                                                                            */
/*============================================================================*/
void VflCpuParticlesCartGrid::SeedParticle(const VistaVector3D &v3Pos)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();

	float *pPos = &pParticles->GetCurrentData()[4*iNext];
	pParticles->SetNextParticle((iNext+1)%iCount);

	if (iActive < iCount)
	{
		++iActive;
		pParticles->SetActiveParticles(iActive);
		if (iActive == iCount)
		{
			cout << " [VflCpuParticlesCartGrid] - all particles are active now..." << endl;
		}
	}

	memcpy(pPos, &v3Pos[0], 4*sizeof(float));
	pParticles->SignalCpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticles                                               */
/*                                                                            */
/*============================================================================*/
void VflCpuParticlesCartGrid::SeedParticles(const VistaVector3D &v3Pos1, 
											 const VistaVector3D &v3Pos2, 
											 int iCount)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iParticleCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();

	if (iCount > iParticleCount)
		iCount = iParticleCount;

	float fDelta = 1.0f;
	if (iCount > 1)
		fDelta /= (iCount-1);
	float fOffset = 0;
	VistaVector3D v3Delta(v3Pos2-v3Pos1);
	float fScalarDelta(v3Pos2[3]-v3Pos1[3]);

	for (int i=0; i<iCount; ++i, fOffset+=fDelta)
	{
		float *pPos = &pParticles->GetCurrentData()[4*(iNext++)];
		iNext %= iParticleCount;

		if (iActive < iParticleCount)
		{
			++iActive;
			pParticles->SetActiveParticles(iActive);
			if (iActive == iParticleCount)
			{
				cout << " [VflCpuParticlesCartGrid] - all particles are active now..." << endl;
			}
		}

		VistaVector3D v3NewPos(v3Pos1 + fOffset*v3Delta);
		v3NewPos[3] = v3Pos1[3] + fOffset*fScalarDelta;
		memcpy(pPos, &v3NewPos[0], 4*sizeof(float));
	}
	pParticles->SetNextParticle(iNext);
	pParticles->SignalCpuDataChange();
}

void VflCpuParticlesCartGrid::SeedParticles(const std::vector<float> &vecPositions)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iParticleCount = pParticles->GetParticleCount();
	int iActive = pParticles->GetActiveParticles();
	int iNext = pParticles->GetNextParticle();
	int iCount = int(vecPositions.size()) / 4;

	if (iCount > iParticleCount)
		iCount = iParticleCount;

	float *pData = pParticles->GetCurrentData();
	const float *pPositions = &vecPositions[0];
	for (int i=0; i<iCount; ++i)
	{
		float *pPos = &pData[4*(iNext++)];
		iNext %= iParticleCount;

		if (iActive < iParticleCount)
		{
			++iActive;
			pParticles->SetActiveParticles(iActive);
			if (iActive == iParticleCount)
			{
				cout << " [VflCpuParticlesCartGrid] - all particles are active now..." << endl;
			}
		}

		memcpy(pPos, &pPositions[4*i], 4*sizeof(float));
	}
	pParticles->SetNextParticle(iNext);
	pParticles->SignalCpuDataChange();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticleNormalized                                      */
/*                                                                            */
/*============================================================================*/
void VflCpuParticlesCartGrid::SeedParticleNormalized(const VistaVector3D &v3Pos)
{
	double dVisTime =
		m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(
		iIndex)->GetData();

	VistaVector3D v3PosDN;
	DeNormalize(v3PosDN, v3Pos, pGrid);
	SeedParticle(v3PosDN);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SeedParticlesNormalized                                     */
/*                                                                            */
/*============================================================================*/
void VflCpuParticlesCartGrid::SeedParticlesNormalized(const VistaVector3D &v3Pos1, 
													   const VistaVector3D &v3Pos2, 
													   int iCount)
{
	double dVisTime =
		m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(
		iIndex)->GetData();

	VistaVector3D v3Pos1DN, v3Pos2DN;
	DeNormalize(v3Pos1DN, v3Pos1, pGrid);
	DeNormalize(v3Pos2DN, v3Pos2, pGrid);
	SeedParticles(v3Pos1DN, v3Pos2DN, iCount);
}

void VflCpuParticlesCartGrid::SeedParticlesNormalized(const std::vector<float> &vecPositions)
{
	double dVisTime =
		m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(
		iIndex)->GetData();

	vector<float> vecDNPositions;
	int iCount = int(vecPositions.size()) / 4;
	vecDNPositions.resize(vecPositions.size());
	for (int i=0; i<iCount; ++i)
	{
		DeNormalize(&vecDNPositions[4*i], &vecPositions[4*i], pGrid);
	}

	SeedParticles(vecDNPositions);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGridType                                                 */
/*                                                                            */
/*============================================================================*/
int VflCpuParticlesCartGrid::GetGridType() const
{
	return VflVisGpuParticles::GT_CARTESIAN_GRID;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProcessingUnit                                           */
/*                                                                            */
/*============================================================================*/
int VflCpuParticlesCartGrid::GetProcessingUnit() const
{
	return VflVisGpuParticles::PU_CPU;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTracerType                                               */
/*                                                                            */
/*============================================================================*/
int VflCpuParticlesCartGrid::GetTracerType() const
{
	return VflVisGpuParticles::TT_PARTICLES;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesCartGrid::IsValid() const
{
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflCpuParticlesCartGrid::GetReflectionableType() const
{
	return s_strCVflCpuParticlesCartGridReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateEuler                                              */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesCartGrid::IntegrateEuler(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid() || pGrid->GetComponents()<3 )
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}

	float *pOld = pParticles->GetCurrentData();
	float *pNew = pParticles->GetStaleData();
	if (m_pParent->GetProperties()->GetSingleBuffered())
		pNew = pOld;

	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	if (pGrid->GetDataType() == VveCartesianGrid::DT_FLOAT)
	{
		float aVel[4];
		for (int i=0; i<iCount; ++i)
		{
			pGrid->GetData(pOld, aVel);
			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + static_cast<float>(dDeltaT) * aVel[j];
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			pNew[3] = (bComputeScalars ? aVel[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else if (pGrid->GetDataType() == VveCartesianGrid::DT_HALF)
	{
		VistaHalf aVel[4];
		for (int i=0; i<iCount; ++i)
		{
			pGrid->GetData(pOld, aVel);
			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + static_cast<float>(dDeltaT) * aVel[j];
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			pNew[3] = (bComputeScalars ? float(aVel[3]) : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else
	{
		cout << " [VflCpuParticlesCartGrid] - WARNING - unsupported grid type..." << endl;
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK3                                                */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesCartGrid::IntegrateRK3(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid() || pGrid->GetComponents()<3)
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}

	if (pGrid->GetDataType() != VveCartesianGrid::DT_FLOAT
		&& pGrid->GetDataType() != VveCartesianGrid::DT_HALF)
	{
		cout << " [VflCpuParticlesCartGrid] - WARNING - unsupported grid type for this integrator..." << endl;
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}

	float dx1[4], dx2[4], dx3[4], dxt[4];
	float *pOld = pParticles->GetCurrentData();
	float *pNew = pParticles->GetStaleData();
	VistaHalf aHalf[4];

	if (m_pParent->GetProperties()->GetSingleBuffered())
		pNew = pOld;

	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	if (pGrid->GetDataType() == VveCartesianGrid::DT_HALF)
	{
		for (int i=0; i<iCount; ++i)
		{
			pGrid->GetData(pOld, aHalf);
			ASSIGN(dx1, aHalf);
			SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx1, pOld);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			pGrid->GetData(dxt, aHalf);
			ASSIGN(dx2, aHalf);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, -1.0f, dx1, pOld);
			VEC_SMAD(dxt, 2.0f, dx2, dxt);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			pGrid->GetData(dxt, aHalf);
			ASSIGN(dx3, aHalf);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + (dx1[j] + 2*dx2[j] + dx3[j]) / 6.0f;
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			//		pNew[3] = (bComputeScalars ? (dx1[3] + 2*dx2[3] + 2*dx3[3] + dx4[3]) / 6.0f : pOld[3]);
			pNew[3] = (bComputeScalars ? dx1[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else if (pGrid->GetDataType() == VveCartesianGrid::DT_FLOAT)
	{
		for (int i=0; i<iCount; ++i)
		{
			pGrid->GetData(pOld, dx1);
			SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx1, pOld);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			pGrid->GetData(dxt, dx2);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, -1.0f, dx1, pOld);
			VEC_SMAD(dxt, 2.0f, dx2, dxt);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			pGrid->GetData(dxt, dx3);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + (dx1[j] + 2*dx2[j] + dx3[j]) / 6.0f;
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			//		pNew[3] = (bComputeScalars ? (dx1[3] + 2*dx2[3] + 2*dx3[3] + dx4[3]) / 6.0f : pOld[3]);
			pNew[3] = (bComputeScalars ? dx1[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else
	{
		cout << " [VflCpuParticlesCartGrid] - WARNING - unsupported grid type..." << endl;
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK4                                                */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesCartGrid::IntegrateRK4(
	double dVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex = pUGrid->GetTimeMapper()->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid() || pGrid->GetComponents()<3)
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}

	if (pGrid->GetDataType() != VveCartesianGrid::DT_FLOAT
		&& pGrid->GetDataType() != VveCartesianGrid::DT_HALF)
	{
		cout << " [VflCpuParticlesCartGrid] - WARNING - unsupported grid type for this integrator..." << endl;
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return false;
	}

	float dx1[4], dx2[4], dx3[4], dx4[4], dxt[4];
	float *pOld = pParticles->GetCurrentData();
	float *pNew = pParticles->GetStaleData();
	VistaHalf aHalf[4];

	if (m_pParent->GetProperties()->GetSingleBuffered())
		pNew = pOld;

	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	if (pGrid->GetDataType() == VveCartesianGrid::DT_HALF)
	{
		for (int i=0; i<iCount; ++i)
		{
			pGrid->GetData(pOld, aHalf);
			ASSIGN(dx1, aHalf);
			SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx1, pOld);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			pGrid->GetData(dxt, aHalf);
			ASSIGN(dx2, aHalf);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx2, pOld);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			pGrid->GetData(dxt, aHalf);
			ASSIGN(dx3, aHalf);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

			VEC_ADD(dxt, dx3, pOld);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			pGrid->GetData(dxt, aHalf);
			ASSIGN(dx4, aHalf);
			SCALAR_MULT(dx4, static_cast<float>(dDeltaT));

			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + (dx1[j] + 2*dx2[j] + 2*dx3[j] + dx4[j]) / 6.0f;
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			//		pNew[3] = (bComputeScalars ? (dx1[3] + 2*dx2[3] + 2*dx3[3] + dx4[3]) / 6.0f : pOld[3]);
			pNew[3] = (bComputeScalars ? dx1[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else if (pGrid->GetDataType() == VveCartesianGrid::DT_FLOAT)
	{
		for (int i=0; i<iCount; ++i)
		{
			pGrid->GetData(pOld, dx1);
			SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx1, pOld);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			pGrid->GetData(dxt, dx2);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx2, pOld);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			pGrid->GetData(dxt, dx3);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

			VEC_ADD(dxt, dx3, pOld);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			pGrid->GetData(dxt, dx4);
			SCALAR_MULT(dx4, static_cast<float>(dDeltaT));

			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + (dx1[j] + 2*dx2[j] + 2*dx3[j] + dx4[j]) / 6.0f;
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			//		pNew[3] = (bComputeScalars ? (dx1[3] + 2*dx2[3] + 2*dx3[3] + dx4[3]) / 6.0f : pOld[3]);
			pNew[3] = (bComputeScalars ? dx1[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else
	{
		cout << " [VflCpuParticlesCartGrid] - WARNING - unsupported grid type..." << endl;
	}

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateEulerUnsteady                                      */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesCartGrid::IntegrateEulerUnsteady(
	double dVisTime, double dLastVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	int iIndex1, iIndex2;
	float fAlpha = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(
		dLastVisTime, iIndex1, iIndex2) );
//	float fAlpha = pUGrid->GetTimeMapper()->GetWeightedLevelIndices(
//		fVisTime, iIndex1, iIndex2);

	if (fAlpha < 0)
		return false;

	pUGrid->GetTypedLevelDataByLevelIndex(iIndex1)->LockData();
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex2)->LockData();
	VveCartesianGrid *pGrid1 = pUGrid->GetTypedLevelDataByLevelIndex(
		iIndex1)->GetData();
	VveCartesianGrid *pGrid2 = pUGrid->GetTypedLevelDataByLevelIndex(
		iIndex2)->GetData();

	if (!pGrid1->GetValid() || !pGrid2->GetValid())
	{
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex1)->UnlockData();
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex2)->UnlockData();
		return false;
	}

	float *pOld = pParticles->GetCurrentData();
	float *pNew = pParticles->GetStaleData();
	if (m_pParent->GetProperties()->GetSingleBuffered())
		pNew = pOld;

	VistaVector3D v3Min, v3Max;
	pGrid1->GetBounds(v3Min, v3Max);
	float fOneMinusAlpha = 1.0f - fAlpha;

	if (pGrid1->GetDataType() == VveCartesianGrid::DT_FLOAT)
	{
		float aVel1[4], aVel2[4];
		for (int i=0; i<iCount; ++i)
		{
			pGrid1->GetData(pOld, aVel1);
			pGrid2->GetData(pOld, aVel2);
			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + static_cast<float>(dDeltaT) * (fOneMinusAlpha*aVel1[j] + fAlpha*aVel2[j]);
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			pNew[3] = (bComputeScalars ? fOneMinusAlpha*aVel1[3] + fAlpha*aVel2[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else if (pGrid1->GetDataType() == VveCartesianGrid::DT_HALF)
	{
		VistaHalf aVel1[4], aVel2[4];
		for (int i=0; i<iCount; ++i)
		{
			pGrid1->GetData(pOld, aVel1);
			pGrid2->GetData(pOld, aVel2);
			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + static_cast<float>(dDeltaT) * (fOneMinusAlpha*aVel1[j] + fAlpha*aVel2[j]);
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			pNew[3] = (bComputeScalars ? fOneMinusAlpha*aVel1[3] + fAlpha*aVel2[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}	
	}
	else
	{
		cout << " [VflCpuParticlesCartGrid] - WARNING - unsupported grid type..." << endl;
	}


	pUGrid->GetTypedLevelDataByLevelIndex(iIndex1)->UnlockData();
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex2)->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK3Unsteady                                        */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesCartGrid::IntegrateRK3Unsteady(
	double dVisTime, double dLastVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	float aAlpha[3];
	int aIndices[6];

	// determine t+h/2
	// we already know t==fLastVisTime and t+h==fVisTime
	double dIntermediateTime = dLastVisTime + 0.5f*(dVisTime-dLastVisTime);
	if (dLastVisTime > dVisTime)
	{
		dIntermediateTime = dLastVisTime + 0.5;
		if (dIntermediateTime > 1.0)
		{
			dIntermediateTime -= 1.0;
		}
	}

	// retrieve interpolation weights for all t, t+h/2 and t+h
	aAlpha[0] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(
		dLastVisTime, aIndices[0], aIndices[1]) );
	aAlpha[1] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(
		dIntermediateTime, aIndices[2], aIndices[3]) );
	aAlpha[2] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(
		dVisTime, aIndices[4], aIndices[5]) );

	if (aAlpha[0] < 0 || aAlpha[1] < 0 || aAlpha[2] < 0)
		return false;

	// lock indices
	set<int> setIndices;
	for (int i=0; i<6; ++i)
		setIndices.insert(aIndices[i]);

	set<int>::iterator it;
	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
		pUGrid->GetTypedLevelDataByLevelIndex(*it)->LockData();

	VveCartesianGrid *aGrids[6];
	bool bValid = true;
	for (int i=0; i<6; ++i)
	{
		aGrids[i] = pUGrid->GetTypedLevelDataByLevelIndex(
			aIndices[i])->GetData();
		bValid = bValid & aGrids[i]->GetValid();
	}

	if (!bValid)
	{
		for (it=setIndices.begin(); it!=setIndices.end(); ++it)
			pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
		return false;
	}

	float dx1[4], dx2[4], dx3[4], dxt[4], dxt1[4], dxt2[4];
	float *pOld = pParticles->GetCurrentData();
	float *pNew = pParticles->GetStaleData();
	if (m_pParent->GetProperties()->GetSingleBuffered())
		pNew = pOld;

	VistaVector3D v3Min, v3Max;
	aGrids[0]->GetBounds(v3Min, v3Max);

	if (aGrids[0]->GetDataType() == VveCartesianGrid::DT_FLOAT)
	{
		for (int i=0; i<iCount; ++i)
		{
			aGrids[0]->GetData(pOld, dxt1);
			aGrids[1]->GetData(pOld, dxt2);
			INTERPOLATE(dx1, dxt1, dxt2, aAlpha[0]);
			SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx1, pOld);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			aGrids[2]->GetData(dxt, dxt1);
			aGrids[3]->GetData(dxt, dxt2);
			INTERPOLATE(dx2, dxt1, dxt2, aAlpha[1]);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, -1.0f, dx1, pOld);
			VEC_SMAD(dxt, 2.0f, dx2, dxt);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			aGrids[4]->GetData(dxt, dxt1);
			aGrids[5]->GetData(dxt, dxt2);
			INTERPOLATE(dx3, dxt1, dxt2, aAlpha[2]);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + (dx1[j] + 2*dx2[j] + dx3[j]) / 6.0f;
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			//		pNew[3] = (bComputeScalars ? (dx1[3] + 2*dx2[3] + 2*dx3[3] + dx4[3]) / 6.0f : pOld[3]);
			pNew[3] = (bComputeScalars ? dx1[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else if (aGrids[0]->GetDataType() == VveCartesianGrid::DT_HALF)
	{
		VistaHalf aHalf[4];
		for (int i=0; i<iCount; ++i)
		{
			aGrids[0]->GetData(pOld, aHalf);
			ASSIGN(dxt1, aHalf);
			aGrids[1]->GetData(pOld, aHalf);
			ASSIGN(dxt2, aHalf);
			INTERPOLATE(dx1, dxt1, dxt2, aAlpha[0]);
			SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx1, pOld);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			aGrids[2]->GetData(dxt, aHalf);
			ASSIGN(dxt1, aHalf);
			aGrids[3]->GetData(dxt, aHalf);
			ASSIGN(dxt2, aHalf);
			INTERPOLATE(dx2, dxt1, dxt2, aAlpha[1]);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, -1.0f, dx1, pOld);
			VEC_SMAD(dxt, 2.0f, dx2, dxt);
			//		SATURATE_LIMIT(dxt, v3Min, v3Max);
			aGrids[4]->GetData(dxt, aHalf);
			ASSIGN(dxt1, aHalf);
			aGrids[5]->GetData(dxt, aHalf);
			ASSIGN(dxt2, aHalf)
			INTERPOLATE(dx3, dxt1, dxt2, aAlpha[2]);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + (dx1[j] + 2*dx2[j] + dx3[j]) / 6.0f;
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			//		pNew[3] = (bComputeScalars ? (dx1[3] + 2*dx2[3] + 2*dx3[3] + dx4[3]) / 6.0f : pOld[3]);
			pNew[3] = (bComputeScalars ? dx1[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else
	{
		cout << " [VflCpuParticlesCartGrid] - WARNING - unsupported grid type..." << endl;
	}

	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
		pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntegrateRK4Unsteady                                        */
/*                                                                            */
/*============================================================================*/
bool VflCpuParticlesCartGrid::IntegrateRK4Unsteady(
	double dVisTime, double dLastVisTime, double dDeltaT, bool bComputeScalars)
{
	VflGpuParticleData *pParticles = m_pParent->GetParticleData();
	int iCount = pParticles->GetActiveParticles();

	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetDataAsCartesianGrid();
	float aAlpha[3];
	int aIndices[6];

	// determine t+h/2
	// we already know t==fLastVisTime and t+h==fVisTime
	double dIntermediateTime = dLastVisTime + 0.5 * (dVisTime-dLastVisTime);
	if (dLastVisTime > dVisTime)
	{
		dIntermediateTime = dLastVisTime + 0.5;
		if (dIntermediateTime > 1.0)
			dIntermediateTime -= 1.0;
	}

	// retrieve interpolation weights for all t, t+h/2 and t+h
	aAlpha[0] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(
		dLastVisTime, aIndices[0], aIndices[1]) );
	aAlpha[1] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(
		dIntermediateTime, aIndices[2], aIndices[3]) );
	aAlpha[2] = static_cast<float>( pUGrid->GetTimeMapper()->GetWeightedLevelIndices(
		dVisTime, aIndices[4], aIndices[5]) );

	if (aAlpha[0] < 0 || aAlpha[1] < 0 || aAlpha[2] < 0)
		return false;

	// lock indices
	set<int> setIndices;
	for (int i=0; i<6; ++i)
		setIndices.insert(aIndices[i]);

	set<int>::iterator it;
	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
		pUGrid->GetTypedLevelDataByLevelIndex(*it)->LockData();

	VveCartesianGrid *aGrids[6];
	bool bValid = true;
	for (int i=0; i<6; ++i)
	{
		aGrids[i] = pUGrid->GetTypedLevelDataByLevelIndex(aIndices[i])->GetData();
		bValid = bValid & aGrids[i]->GetValid();
	}

	if (!bValid)
	{
		for (it=setIndices.begin(); it!=setIndices.end(); ++it)
			pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();
		return false;
	}

	float dx1[4], dx2[4], dx3[4], dx4[4], dxt[4], dxt1[4], dxt2[4];
	float *pOld = pParticles->GetCurrentData();
	float *pNew = pParticles->GetStaleData();
	if (m_pParent->GetProperties()->GetSingleBuffered())
		pNew = pOld;

	VistaVector3D v3Min, v3Max;
	aGrids[0]->GetBounds(v3Min, v3Max);

	if (aGrids[0]->GetDataType() == VveCartesianGrid::DT_FLOAT)
	{
		for (int i=0; i<iCount; ++i)
		{
			aGrids[0]->GetData(pOld, dxt1);
			aGrids[1]->GetData(pOld, dxt2);
			INTERPOLATE(dx1, dxt1, dxt2, aAlpha[0]);
			SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx1, pOld);
			aGrids[2]->GetData(dxt, dxt1);
			aGrids[3]->GetData(dxt, dxt2);
			INTERPOLATE(dx2, dxt1, dxt2, aAlpha[1]);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx2, pOld);
			aGrids[2]->GetData(dxt, dxt1);
			aGrids[3]->GetData(dxt, dxt2);
			INTERPOLATE(dx3, dxt1, dxt2, aAlpha[1]);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

			VEC_ADD(dxt, dx3, pOld);
			aGrids[4]->GetData(dxt, dxt1);
			aGrids[5]->GetData(dxt, dxt2);
			INTERPOLATE(dx4, dxt1, dxt2, aAlpha[2]);
			SCALAR_MULT(dx4, static_cast<float>(dDeltaT));

			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + (dx1[j] + 2*dx2[j] + 2*dx3[j] + dx4[j]) / 6.0f;
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			//		pNew[3] = (bComputeScalars ? (dx1[3] + 2*dx2[3] + 2*dx3[3] + dx4[3]) / 6.0f : pOld[3]);
			pNew[3] = (bComputeScalars ? dx1[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else if (aGrids[0]->GetDataType() == VveCartesianGrid::DT_HALF)
	{
		VistaHalf aHalf[4];
		for (int i=0; i<iCount; ++i)
		{
			aGrids[0]->GetData(pOld, aHalf);
			ASSIGN(dxt1, aHalf);
			aGrids[1]->GetData(pOld, aHalf);
			ASSIGN(dxt2, aHalf);
			INTERPOLATE(dx1, dxt1, dxt2, aAlpha[0]);
			SCALAR_MULT(dx1, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx1, pOld);
			aGrids[2]->GetData(dxt, aHalf);
			ASSIGN(dxt1, aHalf);
			aGrids[3]->GetData(dxt, aHalf);
			ASSIGN(dxt2, aHalf);
			INTERPOLATE(dx2, dxt1, dxt2, aAlpha[1]);
			SCALAR_MULT(dx2, static_cast<float>(dDeltaT));

			VEC_SMAD(dxt, 0.5f, dx2, pOld);
			aGrids[2]->GetData(dxt, aHalf);
			ASSIGN(dxt1, aHalf);
			aGrids[3]->GetData(dxt, aHalf);
			ASSIGN(dxt2, aHalf);
			INTERPOLATE(dx3, dxt1, dxt2, aAlpha[1]);
			SCALAR_MULT(dx3, static_cast<float>(dDeltaT));

			VEC_ADD(dxt, dx3, pOld);
			aGrids[4]->GetData(dxt, aHalf);
			ASSIGN(dxt1, aHalf);
			aGrids[5]->GetData(dxt, aHalf);
			ASSIGN(dxt2, aHalf);
			INTERPOLATE(dx4, dxt1, dxt2, aAlpha[2]);
			SCALAR_MULT(dx4, static_cast<float>(dDeltaT));

			for (int j=0; j<3; ++j)
			{
				pNew[j] = pOld[j] + (dx1[j] + 2*dx2[j] + 2*dx3[j] + dx4[j]) / 6.0f;
				if (pNew[j] < v3Min[j])
					pNew[j] = v3Min[j];
				else if (pNew[j] > v3Max[j])
					pNew[j] = v3Max[j];
			}
			//		pNew[3] = (bComputeScalars ? (dx1[3] + 2*dx2[3] + 2*dx3[3] + dx4[3]) / 6.0f : pOld[3]);
			pNew[3] = (bComputeScalars ? dx1[3] : pOld[3]);

			pNew += 4;
			pOld += 4;
		}
	}
	else
	{
		cout << " [VflCpuParticlesCartGrid] - WARNING - unsupported grid type..." << endl;
	}

	for (it=setIndices.begin(); it!=setIndices.end(); ++it)
		pUGrid->GetTypedLevelDataByLevelIndex(*it)->UnlockData();

	return true;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflCpuParticlesCartGrid::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVflGpuParticleTracer::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflCpuParticlesCartGridReflType);
	return i+1;
}

/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeNormalize                                                 */
/*                                                                            */
/*============================================================================*/
static inline void DeNormalize(VistaVector3D &v3Out,
							   const VistaVector3D &v3In,
							   VveCartesianGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	v3Out[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * v3In[0];
	v3Out[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * v3In[1];
	v3Out[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * v3In[2];
	v3Out[3] = v3In[3];
}

static inline void DeNormalize(float aOut[4],
							   const float aIn[4],
							   VveCartesianGrid *pGrid)
{
	VistaVector3D v3Min, v3Max;
	pGrid->GetBounds(v3Min, v3Max);

	aOut[0] = v3Min[0] + (v3Max[0]-v3Min[0]) * aIn[0];
	aOut[1] = v3Min[1] + (v3Max[1]-v3Min[1]) * aIn[1];
	aOut[2] = v3Min[2] + (v3Max[2]-v3Min[2]) * aIn[2];
	aOut[3] = aIn[3];
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
