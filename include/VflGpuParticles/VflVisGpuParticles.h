/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflVisGpuParticles.h          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflVisGpuParticles                                                */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLVISGPUPARTICLES_H
#define __VFLVISGPUPARTICLES_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VflGpuParticlesConfig.h"
#include <VistaFlowLib/Visualization/VflVtkLookupTable.h>
#include <VistaFlowLib/Visualization/VflLookupTexture.h>
#include <VistaFlowLib/Visualization/VflVisObject.h>
#include <VistaVisExt/Tools/VveUtils.h>
#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaVisExt/Data/VveCartesianGrid.h>
#include <VistaVisExt/Data/VveTetGrid.h>
#include <VistaBase/VistaColor.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflGpuParticleData;
class VflGpuParticleSorter;
class IVflGpuParticleTracer;
class IVflGpuParticleRenderer;
class VflGpuParticleSeeder;
class VflUnsteadyTexturePusher;
class VveUnsteadyTetGrid;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI VflVisGpuParticles : public IVflVisObject
{
	friend class VflVisGpuParticlesProperties;

public:
	/**
	 * Construct a new VflVisGpuParticles object.
	 */
	VflVisGpuParticles(VveUnsteadyCartesianGrid *pData, VflVtkLookupTable* pLut = NULL);
	VflVisGpuParticles(VveUnsteadyTetGrid *pData, VflVtkLookupTable* pLut = NULL);
	virtual ~VflVisGpuParticles();

    /**
     * Initialize object.
     */    
	virtual bool Init();

	/** 
	 */
	virtual void Update();

    /**
     */    
    virtual void DrawOpaque();

    /**
     */    
    virtual void DrawTransparent();

	/**
	 */
	virtual void Draw2D();

	virtual unsigned int GetRegistrationMode() const;

    /**
     * Returns the boundaries of the visualization object.
	 * Note: This method is not declared const, as it might change the
	 *       cached boundaries inside the object.
     * 
     * @OUTPUT  VistaVector3D minBounds    minimum of the object's AABB
	 * @OUTPUT  VistaVector3D maxBounds    maximum of the object's AABB
     * @RETURN  bool	is the bounding box valid?
     */    
	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);

    /**
     * Process a given command event.
     */    
	//virtual void ProcessCommand(VflVOCEvent *pEvent);

	/**
     * Returns the name of the visualization object.
     * 
     * @INPUT   --
     * @RETURN  std::string
     */    
	virtual std::string GetType() const;

    /**
     * Prints out some debug information to the given output stream.
     * 
     * @INPUT   std::ostream & out
     * @RETURN  --
     */    
    virtual void Debug(std::ostream & out) const;


	class VFLGPUPARTICLESAPI VflVisGpuParticlesProperties : public IVflVisObject::VflVisObjProperties
	{
		friend class VflVisGpuParticles;
	public:
		VflVisGpuParticlesProperties();
		VflVisGpuParticlesProperties(VflVtkLookupTable *pLut);
		virtual ~VflVisGpuParticlesProperties();
		VflVtkLookupTable	*GetLookupTable() const;
		VflLookupTexture	*GetLookupTexture() const;

		// particle parameters ------------------------------------
		bool SetParticleTextureSize(int iWidth, int iHeight);
		bool SetParticleTextureSize(const std::list<int> &liSize);
		bool GetParticleTextureSize(int &iWidth, int &iHeight) const;
		int GetParticleTextureSize() const;
		std::list<int> GetParticleTextureSizeAsList() const;

		// texture upload behavior --------------------------------
		bool SetLookAheadCount(int iCount);
		int GetLookAheadCount() const;

		bool SetWrapAround(bool bWrap);
		bool GetWrapAround() const;

		bool SetStreamingUpdate(bool bStream);
		bool GetStreamingUpdate() const;

		// integration parameters ---------------------------------
		bool SetActive(bool bActive);
		bool GetActive() const;

		enum INTEGRATOR
		{
			IR_INVALID = -1,
			IR_EULER = 0,
			IR_RK3,
			IR_RK4,
			IR_LAST
		};

		bool SetIntegrator(int iIntegrator);
		bool SetIntegrator(const std::string &strIntegrator);
		int GetIntegrator() const;
		std::string GetIntegratorAsString() const;

		bool SetDeltaTime(float fTime);
		float GetDeltaTime() const;

		bool SetMaxDeltaTime(float fTime);
		float GetMaxDeltaTime() const;

		bool SetUseFixedDeltaTime(bool bFixed);
		bool GetUseFixedDeltaTime() const;

		bool SetFixedDeltaTime(float fTime);
		float GetFixedDeltaTime() const;

		bool SetAdaptiveTimeSteps(bool bAdaptive);
		bool GetAdaptiveTimeSteps() const;

		bool SetComputeScalars(bool bScalars);
		bool GetComputeScalars() const;

		bool SetSingleBuffered(bool bSingle);
		bool GetSingleBuffered() const;

		bool SetForceHardwareInterpolation(bool bForce);
		bool GetForceHardwareInterpolation() const;

		bool SetBenchmarkMode(bool bBench);
		bool GetBenchmarkMode() const;

		// rendering parameters -----------------------------------
		bool SetVisible(bool bVisible);
		bool GetVisible() const;

		bool SetColor(const VistaColor &v3Color);
		VistaColor GetColor() const;

		bool SetUseLookupTable(bool bUseLut);
		bool GetUseLookupTable() const;

		bool SetRadius(float fRadius);
		float GetRadius() const;

		bool SetPointSize(float fPointSize);
		float GetPointSize() const;

		bool SetLineWidth(float fLineWidth);
		float GetLineWidth() const;

		bool SetDecreaseLuminance(bool bDecrease);
		bool GetDecreaseLuminance() const;

		bool SetDrawOverlay(bool bOverlay);
		bool GetDrawOverlay() const;

		bool SetAllowSorting(bool bSort);
		bool GetAllowSorting() const;

		bool SetMaxSortingSteps(int iMax);
		int GetMaxSortingSteps() const;

		bool SetHighPrecisionSorting(bool bSort);
		bool GetHighPrecisionSorting() const;

		bool SetForceDataPreparation(bool bForce);
		bool GetForceDataPreparation() const;

		// allow for mix'n'match?? --------------------------------
		bool SetRespectProcessingUnit(bool bRPU);
		bool GetRespectProcessingUnit() const;

		bool SetRespectTracerType(bool bRTT);
		bool GetRespectTracerType() const;

		virtual void Debug(std::ostream &out) const;

		virtual std::string GetReflectionableType() const;

	protected:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

		VflVtkLookupTable	*m_pLookupTable;
		VflLookupTexture		*m_pLookupTexture;
		bool						m_bManageLut;
		bool						m_bManageTexture;

		int							m_iWidth, m_iHeight;
		int							m_iLookAheadCount;
		bool						m_bWrapAround;
		bool						m_bStreamingUpdate;
		bool						m_bActive;
		int							m_iIntegrator;
		float						m_fDeltaTime;
		float						m_fMaxDeltaTime;
		bool						m_bUseFixedDeltaTime;
		float						m_fFixedDeltaTime;
		bool						m_bAdaptiveTimeSteps;
		bool						m_bComputeScalars;
		bool						m_bSingleBuffered;
		bool						m_bForceHardwareInterpolation;
		bool						m_bBenchmarkMode;
		bool						m_bVisible;
		VistaColor				    m_oColor;
		bool						m_bUseLookupTable;
		float						m_fRadius;
		float						m_fPointSize;
		float						m_fLineWidth;
		bool						m_bDecreaseLuminance;
		bool						m_bDrawOverlay;
		bool						m_bAllowSorting;
		int							m_iMaxSortingSteps;
		bool						m_bHighPrecisionSorting;
		bool						m_bForceDataPreparation;
		bool						m_bRespectProcessingUnit;
		bool						m_bRespectTracerType;
	};

    /**
     * Notify this object of any changes in the tracing parameters.
     */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg,
		int ticket);

	/** 
	 * Retrieve some information.
	 */
	int GetDataType() const;
	VveUnsteadyData *GetData() const;
	VveUnsteadyCartesianGrid *GetDataAsCartesianGrid() const;
	VveUnsteadyTetGrid *GetDataAsTetGrid() const;
	VflUnsteadyTexturePusher *GetPusher() const;
	VflVisGpuParticlesProperties *GetProperties() const;
	VflGpuParticleData *GetParticleData() const;
	VflGpuParticleSorter *GetParticleSorter() const;

	/**
	 * Retrieve timing information.
	 */
	double GetVisTime() const;
	double GetLastVisTime() const;

	/**
	 * Manage particle tracers.
	 */
	int AddTracer(IVflGpuParticleTracer *pTracer);
	bool RemoveTracer(int iIndex);
	IVflGpuParticleTracer *GetTracer(int iIndex);
	bool SetCurrentTracer(int iIndex);
	int GetCurrentTracer() const;
	int GetTracerCount() const;

	/**
	 * Manage particle renderers.
	 */
	int AddRenderer(IVflGpuParticleRenderer *pRenderer);
	bool RemoveRenderer(int iIndex);
	IVflGpuParticleRenderer *GetRenderer(int iIndex);
	bool SetCurrentRenderer(int iIndex);
	int GetCurrentRenderer() const;
	int GetRendererCount() const;

	/**
	 * Helpers for managing OpenGL state for GPGPU.
	 */
	void SaveOGLState();
	void RestoreOGLState();

	/**
	 * Is the integration too slow for the animation?
	 */
	bool GetTimeWarning() const;

	enum GRID_TYPE
	{
		GT_NONE = -1,
		GT_CARTESIAN_GRID = 0,
		GT_TET_GRID,
		GT_LAST
	};

	enum PROCESSING_UNIT
	{
		PU_INVALID = -1,
		PU_CPU = 0,
		PU_GPU,
		PU_LAST
	};

	enum TRACER_TYPE
	{
		TT_INVALID = -1,
		TT_PARTICLES = 0,
		TT_TRACERS,
		TT_LAST
	};

	enum NOTIFICATION_ID
	{
		NI_PARAMETERS_CHANGE
	};

protected:
	virtual VflVisObjProperties *CreateProperties() const;
	virtual VflVisObjProperties *CreateProperties(VflVtkLookupTable *pLut) const;

	VflVisGpuParticles();
	void SynchTracerAndRenderer();

	union
	{
		VveUnsteadyCartesianGrid	*pcg;
		VveUnsteadyTetGrid			*ptg;
		void						*pv;
	}	m_uData;

	VflGpuParticleData					*m_pParticles;
	VflUnsteadyTexturePusher			*m_pPusher;
	VflGpuParticleSorter				*m_pSorter;

	std::vector<IVflGpuParticleTracer *>		m_vecTracers;
	std::vector<IVflGpuParticleRenderer *>	m_vecRenderers;

	int		m_iCurrentTracer;
	int		m_iCurrentRenderer;

	double	m_dVisTime;
	double	m_dLastVisTime;

	GRID_TYPE	m_eGridType;

	int	m_aViewportState[4];

	bool	m_bTimeWarning; // is the integration too slow for the animation?
};

#endif // __VFLVISGPUPARTICLES_H


/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

