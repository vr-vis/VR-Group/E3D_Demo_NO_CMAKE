/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflCpuTracerRenderer.h        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflCpuTracerRenderer                                              */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLCPUTRACERRENDERER_H
#define __VFLCPUTRACERRENDERER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "VflGpuParticlesConfig.h"
#include "VflGpuParticleRendererInterface.h"

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI VflCpuTracerRenderer : public IVflGpuParticleRenderer
{
public:
	VflCpuTracerRenderer(VflVisGpuParticles *pParent);
	virtual ~VflCpuTracerRenderer();

	virtual void Update();
	virtual void DrawOpaque();

	virtual std::string GetDrawModeString(int iIndex) const;
	virtual int GetDrawModeCount() const;

	virtual int GetProcessingUnit() const;
	virtual int GetTracerType() const;

	enum DRAW_MODE
	{
		DM_POINTS = IVflGpuParticleRenderer::DM_LAST,
		DM_LINES,
		DM_LAST
	};

	virtual bool IsValid() const;
	virtual std::string GetReflectionableType() const;

protected:
	VflCpuTracerRenderer();
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	void UpdateGpuResources();

	double	m_dPreparationTimeStamp;
	GLuint	m_iTracerVBO;
	int		m_iTracerCount;
	int		m_iTracerLength;
	GLuint	m_iIndexVBO;
	GLvoid	**m_aIndexOffsets;
	GLsizei	*m_aIndexCounts;

	bool m_bValid;
};


#endif // __VFLCPUTRACERRENDERER_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

