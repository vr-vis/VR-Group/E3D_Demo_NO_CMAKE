/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflGpuParticleRenderer.h      RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflGpuParticleRenderer                                            */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLGPUPARTICLERENDERER_H
#define __VFLGPUPARTICLERENDERER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflGpuParticlesConfig.h"
#include "VflGpuParticleRendererInterface.h"
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaParticleRenderingCore;
class VistaParticleRenderingProperties;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI VflGpuParticleRenderer : public IVflGpuParticleRenderer
{
protected:
	VflGpuParticleRenderer();
public:
	VflGpuParticleRenderer( VflVisGpuParticles *pParent );
	virtual ~VflGpuParticleRenderer();
	
	VistaParticleRenderingProperties* GetParticleRenderingProperties();

	virtual std::string GetDrawModeString( int iIndex ) const;
	virtual int GetDrawModeCount() const;

	virtual int GetProcessingUnit() const;
	virtual int GetTracerType() const;

	virtual bool IsValid() const;

	virtual void Update();
	virtual void DrawOpaque();
	virtual void DrawTransparent();

	virtual std::string GetReflectionableType() const;

	virtual int AddToBaseTypeList( std::list<std::string> &rBtList ) const;

protected:
	VistaParticleRenderingCore* m_pCore;

	bool m_bValid;
};


#endif // __VFLGPUPARTICLERENDERER_H
/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

