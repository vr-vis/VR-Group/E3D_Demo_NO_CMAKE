/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflGpuParticleRendererI.h     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - IVflGpuParticleRenderer                                           */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLGPUPARTICLERENDERERINTERFACE_H
#define __VFLGPUPARTICLERENDERERINTERFACE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflGpuParticlesConfig.h"
#include <VistaAspects/VistaReflectionable.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisGpuParticles;
class VistaVector3D;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI IVflGpuParticleRenderer : public IVistaReflectionable
{
public:
	/**
	 * Destructor. Removes self from parent's renderer list, if applicable.
	 */
	virtual ~IVflGpuParticleRenderer();

	virtual void Update();
	virtual void DrawOpaque();
	virtual void DrawTransparent();
	virtual void Draw2D();

	bool SetDrawMode(int iMode);
	bool SetDrawMode(const std::string &strMode);
	int GetDrawMode() const;
	std::string GetDrawModeAsString() const;
	virtual std::string GetDrawModeString(int iIndex) const;
	virtual int GetDrawModeCount() const = 0;

	virtual int GetProcessingUnit() const = 0;
	virtual int GetTracerType() const = 0;

	enum DRAW_MODE
	{
		DM_INVALID = -1,
		DM_LAST = 0
	};

	virtual bool IsValid() const = 0;
	virtual std::string GetReflectionableType() const;

protected:
	/**
	 * Constructor. Adds self to parent's renderer list.
	 */
	IVflGpuParticleRenderer(VflVisGpuParticles *pParent);

	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	VflVisGpuParticles			*m_pParent;
	int							m_iDrawMode;
};


#endif // __VFLGPUPARTICLERENDERERINTERFACE_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

