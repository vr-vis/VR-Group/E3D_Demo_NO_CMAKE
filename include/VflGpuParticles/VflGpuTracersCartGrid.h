/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VflGpuTracersCartGrid.h       RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VflGpuTracersCartGrid                                             */
/*                                                                            */
/*============================================================================*/
// $Id$

#ifndef __VFLGPUTRACERSCARTGRID_H
#define __VFLGPUTRACERSCARTGRID_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflGpuParticlesConfig.h"
#include "VflGpuParticleTracer.h"
#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaGLSLShader;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VFLGPUPARTICLESAPI VflGpuTracersCartGrid : public IVflGpuParticleTracer
{
public:
	VflGpuTracersCartGrid(VflVisGpuParticles *pParent);
	virtual ~VflGpuTracersCartGrid();

	virtual void Update();

	virtual void SeedParticle(const VistaVector3D &v3Pos);
	virtual void SeedParticles(const VistaVector3D &v3Pos1,
		const VistaVector3D &v3Pos2, int iCount);
	virtual void SeedParticles(const std::vector<float> &vecPositions);
	virtual void SeedParticleNormalized(const VistaVector3D &v3Pos);
	virtual void SeedParticlesNormalized(const VistaVector3D &v3Pos1,
		const VistaVector3D &v3Pos2, int iCount);
	virtual void SeedParticlesNormalized(
		const std::vector<float> &vecPositions);

	/**
	 * Retrieve the grid type supported by this tracer as defined in 
	 * VflVisGpuParticles::GRID_TYPE
	 */
	virtual int GetGridType() const;

	/**
	 * Retrieve the processing unit for this tracer.
	 */
	virtual int GetProcessingUnit() const;

	/**
	 * Determine the tracer type for this tracer.
	 */
	virtual int GetTracerType() const;

	virtual bool IsValid() const;
	virtual std::string GetReflectionableType() const;

	static const std::string GetRendererId();
	static void GetShaderKeys(std::vector<std::string>& vecShaderKeys);

protected:
	bool CreateGpuResources();
	void DestroyGpuResources();

	VflGpuTracersCartGrid();
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	// integration stuff
	std::vector<VistaGLSLShader *> m_vecShadersSteady;
	std::vector<VistaGLSLShader *> m_vecShadersSteadyScalars;
//	std::vector<IVistaShader *> m_vecShadersSteadyFP32;
	std::vector<VistaGLSLShader *> m_vecShadersUnsteady;
	std::vector<VistaGLSLShader *> m_vecShadersUnsteadyScalars;
//	std::vector<IVistaShader *> m_vecShadersUnsteadyFP32;

	VistaGLSLShader	*m_pWriteTC;
	VistaGLSLShader	*m_pPassThroughShader;

	bool	m_bValid;
};


#endif // __VFLGPUTRACERSCARTGRID_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

