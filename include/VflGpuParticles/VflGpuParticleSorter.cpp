/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflGpuParticleSorter.cpp     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2000   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// $Id$

/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflVisGpuParticles.h"
#include "VflGpuParticleSorter.h"
#include "VflGpuParticleData.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaFramebufferObj.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

#include <VistaBase/VistaTimer.h>

#include <cassert>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strCVflGpuParticleSorterReflType("VflGpuParticleSorter");

#define SHADER_INIT_DISTANCE	"VflGpuParticleSorter_InitDistance.glsl"
#define SHADER_UPDATE_DISTANCE	"VflGpuParticleSorter_UpdateDistance.glsl"
#define SHADER_SORT				"VflGpuParticleSorter_OddEvenMerge.glsl"

static void DumpTexture(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						ostream &out);

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflGpuParticleSorter::VflGpuParticleSorter(VflVisGpuParticles *pParent)
: m_pParent(pParent),
  m_pParticles(NULL),
  m_pProperties(NULL),
  m_pFBO(NULL),
  m_pInitDistanceShader(NULL),
  m_pUpdateDistanceShader(NULL),
  m_pSortShader(NULL),
  m_iCurrentData(0),
  m_dDataTimeStamp(0),
  m_iWidth(1),
  m_iHeight(1),
  m_iActiveParticles(0),
  m_bValid(false),
  m_bResetTextures(true),
  m_bNeedRestart(true),
  m_bInitDistance(true),
  m_bHighPrecision(false),
  m_iPass(-1),
  m_iStage(-1),
  m_iStepsLeft(0),
  m_iTotalSteps(0),
  m_iMaxSteps(-1),
  m_pRenderShader(NULL)
{
	if (!m_pParent)
		return;

	m_pParticles = m_pParent->GetParticleData();
	m_pProperties = m_pParent->GetProperties();

	// retrieve some parameters
	m_pProperties->GetParticleTextureSize(m_iWidth, m_iHeight);
	m_bHighPrecision = m_pProperties->GetHighPrecisionSorting();

	m_aTextures[0] = m_aTextures[1] = NULL;

	m_bValid = CreateGpuResources();

	Observe(m_pProperties);
}

VflGpuParticleSorter::VflGpuParticleSorter(VflGpuParticleData *pParticleData,
					VflVisGpuParticles::VflVisGpuParticlesProperties *pProperties)
: m_pParticles(pParticleData),
  m_pProperties(pProperties),
  m_pFBO(NULL),
  m_pInitDistanceShader(NULL),
  m_pUpdateDistanceShader(NULL),
  m_pSortShader(NULL),
  m_iCurrentData(0),
  m_dDataTimeStamp(0),
  m_iWidth(1),
  m_iHeight(1),
  m_iActiveParticles(0),
  m_bValid(false),
  m_bResetTextures(true),
  m_bNeedRestart(true),
  m_bInitDistance(true),
  m_bHighPrecision(false),
  m_iPass(-1),
  m_iStage(-1),
  m_iStepsLeft(0),
  m_iTotalSteps(0),
  m_iMaxSteps(-1),
  m_pRenderShader(NULL)
{
	if(!(m_pParticles && m_pProperties))
		return;

	// retrieve some parameters
	m_pProperties->GetParticleTextureSize(m_iWidth, m_iHeight);
	m_bHighPrecision = m_pProperties->GetHighPrecisionSorting();

	m_aTextures[0] = m_aTextures[1] = NULL;

	m_bValid = CreateGpuResources();

	Observe(m_pProperties);
}

VflGpuParticleSorter::~VflGpuParticleSorter()
{
	ReleaseObserveable(m_pProperties);

	DestroyGpuResources();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleSorter::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	// well, obviously some parameters have changed...
	int w, h;
	m_pProperties->GetParticleTextureSize(w, h);
	if (w != m_iWidth || h != m_iHeight)
	{
		m_iWidth = w;
		m_iHeight = h;
		m_bResetTextures = true;
	}

	if (m_pProperties->GetHighPrecisionSorting() != m_bHighPrecision)
	{
		m_bHighPrecision = m_pProperties->GetHighPrecisionSorting();
		m_bResetTextures = true;
	}

	if (m_pProperties->GetMaxSortingSteps() != m_iMaxSteps)
	{
		m_iMaxSteps = m_pProperties->GetMaxSortingSteps();
		m_bNeedRestart = true;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Sort                                                        */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleSorter::Sort(const VistaVector3D &v3ViewPosition)
{
	if (m_bResetTextures)
		m_bValid = ResetTextures();

	if (!m_bValid)
		return;

	this->SaveOGLState();

	// we always draw into our target texture
	m_pFBO->Bind();
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + 1 - m_iCurrentData);

	glViewport(0, 0, m_iWidth, m_iHeight);
	glLoadIdentity();
	glOrtho(0, 1, 0, 1, -1, 1);

	// we might have to re-init particle distances...
	int iActiveParticles = m_pParticles->GetActiveParticles();
	if (iActiveParticles < m_iActiveParticles)
		m_bInitDistance = true;
	m_iActiveParticles = iActiveParticles;

	if (m_bInitDistance)
	{
		m_dDataTimeStamp = m_pParticles->GetGpuDataChangeTimeStamp();
		m_iActiveParticles = m_pParticles->GetActiveParticles();
		m_v3ViewPosition = v3ViewPosition;
		m_v3ViewPosition[3] = float(m_iActiveParticles);

		InitDistances();

		m_bNeedRestart = true;
	}
	else
	{
		bool bNeedUpdate = false;
		if (m_dDataTimeStamp != m_pParticles->GetGpuDataChangeTimeStamp())
		{
			bNeedUpdate = true;
			m_dDataTimeStamp = m_pParticles->GetGpuDataChangeTimeStamp();
		}
		if (v3ViewPosition != m_v3ViewPosition)
		{
			bNeedUpdate = true;
			m_v3ViewPosition = v3ViewPosition;
		}
		if (bNeedUpdate)
		{
			m_v3ViewPosition[3] = float(m_iActiveParticles);
			UpdateDistances();
		}
	}

	if (m_bNeedRestart || m_iStepsLeft==0)
	{
		m_iPass = m_iStage = -1;
		m_iStepsLeft = m_iTotalSteps;
		m_bNeedRestart = false;
	}

#if 1
	VistaTimer oTimer;

	int iSteps(m_iMaxSteps<0 
		? m_iStepsLeft 
		: (m_iStepsLeft>m_iMaxSteps ? m_iMaxSteps : m_iStepsLeft));

	for (int i=0; i<iSteps; ++i)
	{
		--m_iPass;
		if (m_iPass<0)
		{
			++m_iStage;
			m_iPass = m_iStage;
		}
		SortStep();
	}
	m_iStepsLeft -= iSteps;
#endif

	m_pFBO->Release();

	this->RestoreOGLState();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetValid                                                    */
/*                                                                            */
/*============================================================================*/
bool VflGpuParticleSorter::GetValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetMappingTexture                                           */
/*                                                                            */
/*============================================================================*/
VistaTexture *VflGpuParticleSorter::GetMappingTexture() const
{
	return m_aTextures[m_iCurrentData];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw                                                        */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleSorter::Draw()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable(GL_LIGHTING);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
	glColor4f(1, 1, 1, 1);

	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 1, 0, 1);

	float fSize = 0.1f;
	float fSpacing = 0.02f;
	float s, t;
	int iOffset = 4;

	m_pRenderShader->Bind();

	for (int i=0; i<2; ++i)
	{
		s = m_aTextures[i]->GetMaxS();
		t = m_aTextures[i]->GetMaxT();

		m_aTextures[i]->Bind();

		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f((i+iOffset)*fSize + (i+iOffset)*fSpacing, 1.0f-fSize);
		glTexCoord2f(s, 0);
		glVertex2f(((i+iOffset)+1)*fSize + (i+iOffset)*fSpacing, 1.0f-fSize);
		glTexCoord2f(s, t);
		glVertex2f(((i+iOffset)+1)*fSize + (i+iOffset)*fSpacing, 1.0f);
		glTexCoord2f(0, t);
		glVertex2f((i+iOffset)*fSize + (i+iOffset)*fSpacing, 1.0f);
		glEnd();

	}

	m_pRenderShader->Release();

	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateGpuResources                                          */
/*                                                                            */
/*============================================================================*/
bool VflGpuParticleSorter::CreateGpuResources()
{
	// TODO: check for required OpenGL extensions...
#ifdef DEBUG
	cout << " [VflGpuParticleSorter] - creating GPU resources..." << endl;
#endif

	if (!ResetTextures())
		return false;

	// create distance initialization shader
#ifdef DEBUG
	cout << " [VflGpuParticleSorter] - creating distance init shader..." << endl;
#endif
	VistaGLSLShader *pShader = new VistaGLSLShader;
	string strShader;

	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();
	strShader = rShaderReg.RetrieveShader(SHADER_INIT_DISTANCE);

	if(strShader.empty())
	{
		cout << " [VflGpuParticleSorter] - ERROR - distance init shader not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticleSorter] - ERROR - unable to init distance init shader..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticleSorter] - ERROR - unable to link distance init shader..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitParticles"), 0);
	pShader->Release();
	m_pInitDistanceShader = pShader;

	// create distance update shader
#ifdef DEBUG
	cout << " [VflGpuParticleSorter] - creating distance update shader..." << endl;
#endif
	pShader = new VistaGLSLShader;

	strShader = rShaderReg.RetrieveShader(SHADER_UPDATE_DISTANCE);
	if(strShader.empty())
	{
		cout << " [VflGpuParticleSorter] - ERROR - distance update shader not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticleSorter] - ERROR - unable to init distance update shader..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticleSorter] - ERROR - unable to link distance update shader..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitParticles"), 0);
	pShader->SetUniform(pShader->GetUniformLocation("texUnitMapping"), 1);
	pShader->Release();
	m_pUpdateDistanceShader = pShader;

	// create sorting shader 
#ifdef DEBUG
	cout << " [VflGpuParticleSorter] - creating sorting shader..." << endl;
#endif
	pShader = new VistaGLSLShader;
	strShader = rShaderReg.RetrieveShader(SHADER_SORT);
	if(strShader.empty())
	{
		cout << " [VflGpuParticleSorter] - ERROR - sorting shader not available" << endl;
		return false;
	}
	if (!pShader->InitFragmentShaderFromString(strShader))
	{
		cout << " [VflGpuParticleSorter] - ERROR - unable to init sorting shader..." << endl;
		delete pShader;
		return false;
	}
	if (!pShader->Link())
	{
		cout << " [VflGpuParticleSorter] - ERROR - unable to link sorting shader..." << endl;
		delete pShader;
		return false;
	}
	pShader->Bind();
	pShader->SetUniform(pShader->GetUniformLocation("texUnitMapping"), 0);
	pShader->Release();
	m_pSortShader = pShader;

	// create render shader
#ifdef DEBUG
	cout << " [VflGpuParticleSorter] - creating overlay rendering shader..." << endl;
#endif
	std::string strOverlayShader =
		rShaderReg.RetrieveShader( "VflGpuParticleSorter_RenderShader_frag.glsl" );
	
	VistaGLSLShader* pProgram = new VistaGLSLShader();

	if(    strShader.empty()
		|| !pProgram->InitShaderFromString( GL_FRAGMENT_SHADER, strOverlayShader )
		|| !pProgram->Link() )
	{
		cout << " [VflGpuParticleSorter] - ERROR - unable to initialize overlay render shader..." << endl;
		delete pProgram;
		return false;
	}
	m_pRenderShader = pProgram;
	m_pRenderShader->Bind();
	m_pRenderShader->SetUniform( "samp_tex", 0 );
	m_pRenderShader->Release();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DestroyGpuResources                                         */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleSorter::DestroyGpuResources()
{
	delete m_pFBO;
	m_pFBO = NULL;

	for (int i=0; i<2; ++i)
	{
		delete m_aTextures[i];
		m_aTextures[i] = NULL;
	}

	delete m_pInitDistanceShader;
	m_pInitDistanceShader = NULL;

	delete m_pUpdateDistanceShader;
	m_pUpdateDistanceShader = NULL;

	delete m_pSortShader;
	m_pSortShader = NULL;

	delete m_pRenderShader;
	m_pRenderShader = NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ResetTextures                                               */
/*                                                                            */
/*============================================================================*/
bool VflGpuParticleSorter::ResetTextures()
{
	for (int i=0; i<2; ++i)
	{
		// determine texture formats
		GLint iInternalFormat(m_bHighPrecision?GL_RGB32F_ARB:GL_RGB16F_ARB);

		GLenum eFormat = GL_RGB;

		// We don't have to destroy the old texture, as we can just 
		// resize it. The driver will handle any necessary
		// reallocation (hopefully ;-)...
		if (!m_aTextures[i])
		{
			m_aTextures[i] = new VistaTexture(GL_TEXTURE_RECTANGLE_ARB);
			m_aTextures[i]->Bind();
			glTexParameteri(m_aTextures[i]->GetTarget(), GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(m_aTextures[i]->GetTarget(), GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(m_aTextures[i]->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(m_aTextures[i]->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP);
		}
		else
		{
			m_aTextures[i]->Bind();
		}

		// set texture size
		glTexImage2D(m_aTextures[i]->GetTarget(), 0, iInternalFormat, 
			m_iWidth, m_iHeight, 0, eFormat, GL_FLOAT, NULL);

		m_aTextures[i]->SetMaxS(float(m_iWidth));
		m_aTextures[i]->SetMaxT(float(m_iHeight));
	}

	if (!m_pFBO)
	{
		m_pFBO = new VistaFramebufferObj;
	}

	m_pFBO->Bind();
	for (int i=0; i<2; ++i)
		m_pFBO->Attach(m_aTextures[i], GL_COLOR_ATTACHMENT0_EXT+i);
	m_pFBO->Release();
	if (!m_pFBO->IsValid())
	{
		cout << " [VflGpuParticleSorter] - ERROR - invalid FBO..." << endl;
		cout << "                                  FBO status: " << m_pFBO->GetStatusAsString() << endl;
		return false;
	}

	m_bInitDistance = true;

	int iLogElements = int(ceil(log(float(m_iWidth * m_iHeight)) / log(2.0f)));
	m_iTotalSteps = iLogElements * (iLogElements+1) / 2;
	
	cout << " [VflGpuParticleSorter] - particle count: " << (m_iWidth*m_iHeight) << endl;
	cout << "                          sorting steps:  " << m_iTotalSteps << endl;

	m_bNeedRestart = true;
	m_bResetTextures = false;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   InitDistances                                               */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleSorter::InitDistances()
{
	// bind shader
	m_pInitDistanceShader->Bind();

	// set program parameters
	m_pInitDistanceShader->SetUniform(m_pInitDistanceShader->GetUniformLocation("v4ViewPosition"),
		&m_v3ViewPosition[0]);
	m_pInitDistanceShader->SetUniform(m_pInitDistanceShader->GetUniformLocation("v2WidthHeight"),
		float(m_iWidth), float(m_iHeight));

	// bind particle texture
	m_pParticles->GetCurrentTexture()->Bind();

	// draw quad
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2f(0, 0);
	glTexCoord2f(1, 0);
	glVertex2f(1, 0);
	glTexCoord2f(1, 1);
	glVertex2f(1, 1);
	glTexCoord2f(0, 1);
	glVertex2f(0, 1);
	glEnd();

	// that's all, folks...
	m_pInitDistanceShader->Release();

	// swap buffers
	m_iCurrentData = 1 - m_iCurrentData;

	m_bInitDistance = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateDistances                                             */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleSorter::UpdateDistances()
{
	// bind shader
	m_pUpdateDistanceShader->Bind();

	// set program parameters
	m_pUpdateDistanceShader->SetUniform(m_pUpdateDistanceShader->GetUniformLocation("v4ViewPosition"),
		&m_v3ViewPosition[0]);
	m_pUpdateDistanceShader->SetUniform(m_pUpdateDistanceShader->GetUniformLocation("v2WidthHeight"),
		float(m_iWidth), float(m_iHeight));

	// bind particle texture
	m_aTextures[m_iCurrentData]->Bind(GL_TEXTURE1);
	m_pParticles->GetCurrentTexture()->Bind(GL_TEXTURE0);

	float s = m_aTextures[m_iCurrentData]->GetMaxS();
	float t = m_aTextures[m_iCurrentData]->GetMaxT();

	// draw quad
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2f(0, 0);
	glTexCoord2f(s, 0);
	glVertex2f(1, 0);
	glTexCoord2f(s, t);
	glVertex2f(1, 1);
	glTexCoord2f(0, t);
	glVertex2f(0, 1);
	glEnd();

	// that's all, folks...
	m_pUpdateDistanceShader->Release();

	// swap buffers
	m_iCurrentData = 1 - m_iCurrentData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SortStep                                                    */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleSorter::SortStep()
{
	// set draw buffer
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + 1 - m_iCurrentData);

	// bind shader
	m_pSortShader->Bind();

	// set program parameters
	int iPStage = (1<<m_iStage);
	int iPPass = (1<<m_iPass);
	m_pSortShader->SetUniform(m_pSortShader->GetUniformLocation("Params1"),
		2.0f*iPStage, float(iPPass%iPStage), 2.0f*iPStage-(iPPass%iPStage)-1);
	m_pSortShader->SetUniform(m_pSortShader->GetUniformLocation("Params2"),
		float(m_iWidth), float(m_iHeight), float(iPPass));

	// bind mapping texture
	m_aTextures[m_iCurrentData]->Bind();

	float s = m_aTextures[m_iCurrentData]->GetMaxS();
	float t = m_aTextures[m_iCurrentData]->GetMaxT();

	// draw quad
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2f(0, 0);
	glTexCoord2f(s, 0);
	glVertex2f(1, 0);
	glTexCoord2f(s, t);
	glVertex2f(1, 1);
	glTexCoord2f(0, t);
	glVertex2f(0, 1);
	glEnd();

	// that's all, folks...
	m_pSortShader->Release();

	// swap buffers
	m_iCurrentData = 1 - m_iCurrentData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRendererId                                               */
/*                                                                            */
/*============================================================================*/
const string VflGpuParticleSorter::GetRendererId()
{
	return s_strCVflGpuParticleSorterReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetShaderKeys                                               */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleSorter::GetShaderKeys(
	std::vector<std::string>& vecShaderKeys)
{
	vecShaderKeys.clear();
	vecShaderKeys.push_back(SHADER_INIT_DISTANCE);
	vecShaderKeys.push_back(SHADER_UPDATE_DISTANCE);
	vecShaderKeys.push_back(SHADER_SORT);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SaveOGLState                                                */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleSorter::SaveOGLState()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);
	glLineWidth(1.0f);
	glPointSize(1.0f);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	// save old viewport state
	glGetIntegerv(GL_VIEWPORT, m_aViewportState);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RestoreOGLState                                             */
/*                                                                            */
/*============================================================================*/
void VflGpuParticleSorter::RestoreOGLState()
{
	// restore old projection and modelview matrices
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	// restore old viewport
	glViewport(m_aViewportState[0], m_aViewportState[1], m_aViewportState[2], m_aViewportState[3]);

	glPopAttrib();
}

/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/
static void DumpTexture(VistaTexture *pTexture, 
						int iWidth, int iHeight, int iTuples,
						ostream &out)
{
	if (!pTexture)
		return;

	float *pData = new float[iWidth*iHeight*iTuples];
	pTexture->Bind();
	GLenum eFormat = GL_RGBA;
	switch (iTuples)
	{
	case 1:
		eFormat = GL_LUMINANCE;
		break;
	case 2:
		eFormat = GL_LUMINANCE_ALPHA;
		break;
	case 3:
		eFormat = GL_RGB;
		break;
	}

	glGetTexImage(pTexture->GetTarget(), 0, eFormat, GL_FLOAT, pData);
	float *pPos = pData;

	for (int i=0; i<iHeight; ++i)
	{
		for (int j=0; j<iWidth; ++j)
		{
			if (j>0)
				out << "| ";
			for (int k=0; k<iTuples; ++k)
			{
				out << (*(pPos++)) << " ";
			}
		}
		out << endl;
	}

	delete [] pData;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
