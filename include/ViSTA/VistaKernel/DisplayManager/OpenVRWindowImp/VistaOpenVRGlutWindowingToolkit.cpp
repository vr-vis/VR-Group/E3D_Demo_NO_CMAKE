/*============================================================================*/
/*                              ViSTA VR toolkit                              */
/*               Copyright (c) 1997-2016 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: VistaGlutWindowingToolkit.cpp 42600 2014-06-18 19:23:49Z dr165799 $

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VistaOpenVRGlutWindowingToolkit.h"

#include "VistaBase/VistaStreamUtils.h"
#include "VistaBase/VistaUtilityMacros.h"
#include "VistaKernel/DisplayManager/VistaWindow.h"
#include <VistaBase/VistaTimeUtils.h>
#include <VistaKernel/DisplayManager/VistaViewport.h>
#include <VistaKernel/EventManager/VistaEventManager.h>
#include <VistaKernel/EventManager/VistaEventObserver.h>
#include <VistaKernel/EventManager/VistaSystemEvent.h>
#include <VistaKernel/VistaSystem.h>

#include <openvr.h>

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#endif
#include "GL/glut.h"

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

class VistaOpenVRGlutWindowingToolkit::Internal : public VistaEventObserver
{
public:
	vr::IVRSystem* m_pHMD = NULL;
	vr::IVRRenderModels* m_pRenderModels;	

	vr::TrackedDevicePose_t m_aTrackedDevicePose[ vr::k_unMaxTrackedDeviceCount ];

	virtual void Notify( const VistaEvent *pEvent )
	{
		if( pEvent->GetId() == VistaSystemEvent::VSE_UPDATE_DELAYED_INTERACTION )
			vr::VRCompositor()->WaitGetPoses( m_aTrackedDevicePose, vr::k_unMaxTrackedDeviceCount, NULL, 0 );
	}

};

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VistaOpenVRGlutWindowingToolkit::VistaOpenVRGlutWindowingToolkit()
: VistaGlutWindowingToolkit()
, m_pData( new Internal() )
{	
}

VistaOpenVRGlutWindowingToolkit::~VistaOpenVRGlutWindowingToolkit()
{
	if( m_pData->m_pHMD != NULL )
	{
		::GetVistaSystem()->GetEventManager()->UnregisterObserver( m_pData, VistaSystemEvent::GetTypeId() );
		vr::VR_Shutdown();
	}
}

bool VistaOpenVRGlutWindowingToolkit::RegisterWindow( VistaWindow* pWindow )
{
	if( m_pData->m_pHMD == NULL )
	{
		vr::EVRInitError eError = vr::VRInitError_None;
		m_pData->m_pHMD = vr::VR_Init( &eError, vr::VRApplication_Scene );
		if ( eError != vr::VRInitError_None )
		{
			vstr::errp() << "OpenVR HMD Init failed: " << vr::VR_GetVRInitErrorAsEnglishDescription( eError ) << std::endl;
			VISTA_THROW( "Initializing OpenVR HMD failed", -1 );
		}

		m_pData->m_pRenderModels = (vr::IVRRenderModels *)vr::VR_GetGenericInterface( vr::IVRRenderModels_Version, &eError );
		if( m_pData->m_pRenderModels == NULL)
		{
			vstr::errp() << "OpenVR RenderModels invalid: " << vr::VR_GetVRInitErrorAsEnglishDescription( eError ) << std::endl;
			vr::VR_Shutdown();
			VISTA_THROW( "OpenVR RenderModels invalid", -1 );
		}

		if( vr::VRCompositor() == nullptr )
		{
			vstr::errp() << "OpenVR Compositor invalid" << std::endl;
			vr::VR_Shutdown();
			VISTA_THROW( "OpenVR Compositor invalid", -1 );
		}
	}

	::GetVistaSystem()->GetEventManager()->RegisterObserver( m_pData, VistaSystemEvent::GetTypeId() );
	
	bool bSuccess = VistaGlutWindowingToolkit::RegisterWindow( pWindow );
	return bSuccess;
}

bool VistaOpenVRGlutWindowingToolkit::UnregisterWindow( VistaWindow* pWindow )
{
	return VistaGlutWindowingToolkit::UnregisterWindow( pWindow );
}

bool VistaOpenVRGlutWindowingToolkit::InitWindow( VistaWindow* pWindow )
{
	int nMajor, nMinor;
	GetContextVersion( nMajor, nMinor, pWindow );
	if( nMajor < 4 )
	{
		vstr::warnp() << "OpenVR requires GL context 4.0 or greater, enforcing 4.1 - please adjust config" << std::endl;
		SetContextVersion( 4, 1, pWindow );
	}
	SetMultiSamples( pWindow, 0 ); // no multisamples, since we'll just copy a texture

	if( VistaGlutWindowingToolkit::InitWindow( pWindow ) == false )
		return false;
	VistaGlutWindowingToolkit::SetVSyncMode( pWindow, false );
	return true;
}

void VistaOpenVRGlutWindowingToolkit::DisplayWindow( const VistaWindow* pWindow )
{
	VistaGlutWindowingToolkit::DisplayWindow( pWindow );
	vr::VRCompositor()->PostPresentHandoff();
}

vr::IVRSystem* VistaOpenVRGlutWindowingToolkit::GetHmd()
{
	return m_pData->m_pHMD;
}

vr::IVRRenderModels* VistaOpenVRGlutWindowingToolkit::GetRenderModels()
{
	return m_pData->m_pRenderModels;
}

bool VistaOpenVRGlutWindowingToolkit::SetVSyncMode( VistaWindow* pWindow, const bool bEnabled )
{
	// we always set vsync to false
	if( bEnabled )
		return false;
	else
		return VistaGlutWindowingToolkit::SetVSyncMode( pWindow, false );
}





/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/


