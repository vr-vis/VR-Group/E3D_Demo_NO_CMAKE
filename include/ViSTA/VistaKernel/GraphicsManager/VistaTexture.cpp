/*============================================================================*/
/*                              ViSTA VR toolkit                              */
/*               Copyright (c) 1997-2016 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: VistaAutoBuffer.cpp 31862 2012-08-31 22:54:08Z ingoassenmacher $

#include "VistaTexture.h"

#include "VistaImageAndTextureFactory.h"


/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/


VistaTexture::VistaTexture( DataHandling eDataHandlingMode /*= DH_COPY_ON_WRITE */ )
: IVistaSharedCoreOwner( IVistaImageAndTextureCoreFactory::GetSingleton()->CreateTextureCore(), eDataHandlingMode )
{
}

VistaTexture::VistaTexture( const VistaTexture& oOther )
: IVistaSharedCoreOwner( oOther )
{
}

VistaTexture::~VistaTexture()
{
}

VistaTexture& VistaTexture::operator=( const VistaTexture& oOther )
{
	IVistaSharedCoreOwner::operator=( oOther );
	return (*this);
}

const IVistaTextureCore* VistaTexture::GetCore() const
{
	return static_cast<const IVistaTextureCore*>( IVistaSharedCoreOwner::GetCore() );
}

IVistaTextureCore* VistaTexture::GetCoreForWriting()
{
	return static_cast<IVistaTextureCore*>( IVistaSharedCoreOwner::GetCoreForWriting() );
}

bool VistaTexture::GetIsValid() const
{
	return GetCore()->GetIsValid();
}

GLenum VistaTexture::GetTextureTarget() const
{
	return GetCore()->GetTextureTarget();
}

GLenum VistaTexture::GetInternalFormat() const
{
	return GetCore()->GetInternalFormat();
}

VistaImage VistaTexture::LoadTextureDataToImage()
{
	// @todo: writing or not?
	return GetCoreForWriting()->LoadTextureDataToImage();
}

bool VistaTexture::Set1DData( const int nWidth, const VistaType::byte* pData, const bool bGenMipmaps /*= true*/, const GLenum eInternalFormat /*= GL_RGBA*/, const GLenum ePixelFormat /*= GL_RGBA*/, const GLenum eDataFormat /*= GL_UNSIGNED_BYTE */ )
{
	return GetCoreForWriting()->Set1DData( nWidth, pData, bGenMipmaps, eInternalFormat, ePixelFormat, eDataFormat );
}

bool VistaTexture::Set2DData( const int nWidth, const int nHeight, const VistaType::byte* pData, const bool bGenMipmaps /*= true*/, const GLenum eInternalFormat /*= GL_RGBA*/, const GLenum ePixelFormat /*= GL_RGBA*/, const GLenum eDataFormat /*= GL_UNSIGNED_BYTE */, const GLuint nMultiSampleNum )
{
	return GetCoreForWriting()->Set2DData( nWidth, nHeight, pData, bGenMipmaps, eInternalFormat, ePixelFormat, eDataFormat );
}

bool VistaTexture::Set3DData( const int nWidth, const int nHeight, const int nDepth, const VistaType::byte* pData, const bool bGenMipmaps /*= true*/, const GLenum eInternalFormat /*= GL_RGBA*/, const GLenum ePixelFormat /*= GL_RGBA*/, const GLenum eDataFormat /*= GL_UNSIGNED_BYTE */ )
{
	return GetCoreForWriting()->Set3DData( nWidth, nHeight, nWidth, pData, bGenMipmaps, eInternalFormat, ePixelFormat, eDataFormat );
}

bool VistaTexture::SetData( const VistaImage& oImage, const bool bGenMipmaps, const GLenum eInternalFormat )
{
	return GetCoreForWriting()->SetData( oImage, bGenMipmaps, eInternalFormat );
}

unsigned int VistaTexture::GetWidth() const
{
	return GetCore()->GetWidth();
}

unsigned int VistaTexture::GetHeight() const
{
	return GetCore()->GetHeight();
}

unsigned int VistaTexture::GetDepth() const
{
	return GetCore()->GetDepth();
}

bool VistaTexture::GetHasMipmaps() const
{
	return GetCore()->GetHasMipmaps();
}

bool VistaTexture::CreateMipmaps()
{
	return GetCoreForWriting()->CreateMipmaps();
}

GLenum VistaTexture::GetMinFilter() const
{
	return GetCore()->GetMinFilter();
}

void VistaTexture::SetMinFilter( const GLenum eMinFilterMode )
{
	return GetCoreForWriting()->SetMinFilter( eMinFilterMode );
}

GLenum VistaTexture::GetMagFilter() const
{
	return GetCore()->GetMagFilter();
}

void VistaTexture::SetMagFilter( const GLenum eMagFilterMode )
{
	return GetCoreForWriting()->SetMagFilter( eMagFilterMode );
}

GLenum VistaTexture::GetWrapS() const
{
	return GetCore()->GetWrapR();	
}

void VistaTexture::SetWrapS( const GLenum eWrappingMode )
{
	return GetCoreForWriting()->SetWrapS( eWrappingMode );
}

GLenum VistaTexture::GetWrapT() const
{
	return GetCore()->GetWrapT();
}

void VistaTexture::SetWrapT( const GLenum eWrappingMode )
{
	return GetCoreForWriting()->SetWrapT( eWrappingMode );
}

GLenum VistaTexture::GetWrapR() const
{
	return GetCore()->GetWrapR();
}

void VistaTexture::SetWrapR( const GLenum eWrappingMode )
{
	return GetCoreForWriting()->SetWrapR( eWrappingMode );
}

float VistaTexture::GetAnisotropy() const
{
	return GetCore()->GetAnisotropy();
}

void VistaTexture::SetAnisotropy( const float nAnisotropy )
{
	return GetCoreForWriting()->SetAnisotropy( nAnisotropy );
}

float VistaTexture::GetLodBias() const
{
	return GetCore()->GetLodBias();
}

void VistaTexture::SetLodBias( const float nBias )
{
	return GetCoreForWriting()->SetLodBias( nBias );
}

bool VistaTexture::Bind() const
{
	// @todo: writing
	return GetCore()->Bind();
}

bool VistaTexture::Bind( const GLenum eTextureUnit ) const
{
	// @todo: writing
	return GetCore()->Bind( eTextureUnit );
}

bool VistaTexture::Release() const
{
	// @todo: writing
	return GetCore()->Release();
}

bool VistaTexture::Release( const GLenum eTextureUnit ) const
{
	// @todo: writing
	return GetCore()->Release( eTextureUnit );
}

GLuint VistaTexture::GetGLId() const
{
	return GetCore()->GetGLId();
}

GLenum VistaTexture::GetTextureEnvMode() const
{
	return GetCore()->GetTextureEnvMode();
}

void VistaTexture::SetTextureEnvMode( GLenum eMode )
{
	GetCoreForWriting()->SetTextureEnvMode( eMode );
}

bool VistaTexture::SwapTextures( VistaTexture& oOther )
{
	std::swap( m_pCore, oOther.m_pCore );
	return true;
}

bool VistaTexture::GetIsMultisample() const
{
	return ( GetCore()->GetNumMultisamples() > 0 );
}

int VistaTexture::GetNumMultisamples() const
{
	return GetCore()->GetNumMultisamples();
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
