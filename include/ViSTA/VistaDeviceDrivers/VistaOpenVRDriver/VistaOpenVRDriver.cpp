/*============================================================================*/
/*                              ViSTA VR toolkit                              */
/*               Copyright (c) 1997-2016 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VistaOpenVRDriver.h"

#include <VistaDeviceDriversBase/VistaDeviceSensor.h>

#include <VistaInterProcComm/Connections/VistaConnection.h>

#include <VistaBase/VistaExceptionBase.h>
#include <VistaBase/VistaTimeUtils.h>
#include <VistaBase/VistaStreamUtils.h>

#include <cstring>
#include <cstdio>

#include <string>
#include <stdlib.h>
#include "VistaDeviceDriversBase/DriverAspects/VistaDriverSensorMappingAspect.h"
#include "VistaDeviceDriversBase/DriverAspects/VistaDriverGenericParameterAspect.h"
#include "VistaKernel/VistaSystem.h"
#include "VistaKernel/OpenSG/VistaOpenSGDisplayBridge.h"
#include "VistaKernel/GraphicsManager/VistaSceneGraph.h"
#include "VistaBase/VistaUtilityMacros.h"

#include <openvr.h>
#include "VistaDeviceDriversBase/VistaDeviceDriver.h"
#include "VistaDeviceDriversBase/DriverAspects/VistaDriverMeasureHistoryAspect.h"
#include "VistaKernel/InteractionManager/VistaUserPlatform.h"




VistaOpenVRDriver::BodyData::BodyData()
: m_nSensorInfoType( VistaDriverSensorMappingAspect::INVALID_TYPE )
, m_nSensorType( VistaDriverSensorMappingAspect::INVALID_TYPE )
, m_nSensorIndex( VistaDriverSensorMappingAspect::INVALID_ID )
, m_eType( TYPE_NONE )
, m_nOVRIndex( ~0U )
, m_bConnected( false )
, m_nNumButtons( 0 )
, m_nNumAxes( 0 )
, m_nButtonPressedMask( 0 )
, m_nButtonTouchedMask( 0 )
, m_pTransformNode( nullptr )
, m_pGeomNode( nullptr )
, m_pRenderData( nullptr )
{
}

VistaOpenVRDriver::BodyData::~BodyData()
{
	// will be deleted by scene graph
	//delete m_pGeomNode;
	//delete m_pTransformNode;
}


////////////////////////////////////////////////////////////////////////////////

REFL_IMPLEMENT_FULL( VistaOpenVRDriver::Parameters, VistaDriverGenericParameterAspect::IParameterContainer );

VistaOpenVRDriver::Parameters::Parameters( VistaOpenVRDriver* pDriver )
: m_bDrawControllers( false )
, m_nControllerModelEmissive( 0 )
, m_bProcessButtonPressesBetweenFrames( false )
, m_bProcessButtonTouchesBetweenFrames( false )
{
}

bool VistaOpenVRDriver::Parameters::GetDrawControllers() const
{
	return m_bDrawControllers;
}

bool VistaOpenVRDriver::Parameters::SetDrawControllers( const bool bSet )
{
	m_bDrawControllers = bSet;
	Notify( MSG_DRAW_CONTROLLER_CHG );
	return true;
}

float VistaOpenVRDriver::Parameters::GetControllerModelEmissive() const
{
	return m_nControllerModelEmissive;
}

bool VistaOpenVRDriver::Parameters::SetControllerModelEmissive( const float nEmissive )
{
	m_nControllerModelEmissive = nEmissive;
	Notify( MSG_CONTROLLER_MODEL_EMISSIVE_CHANGED );
	return true;
}

bool VistaOpenVRDriver::Parameters::GetProcessButtonPressesBetweenFrames() const
{
	return m_bProcessButtonPressesBetweenFrames;
}

bool VistaOpenVRDriver::Parameters::SetProcessButtonPressesBetweenFrames( const bool bSet )
{
	m_bProcessButtonPressesBetweenFrames = bSet;
	Notify( MSG_PROCESS_BUTTON_PRESSES_BETWEEN_FRAMES );
	return true;
}

bool VistaOpenVRDriver::Parameters::GetProcessButtonTouchesBetweenFrames() const
{
	return m_bProcessButtonTouchesBetweenFrames;
}

bool VistaOpenVRDriver::Parameters::SetProcessButtonTouchesBetweenFrames( const bool bSet )
{
	m_bProcessButtonTouchesBetweenFrames = bSet;
	Notify( MSG_PROCESS_BUTTON_TOUCHES_BETWEEN_FRAMES );
	return true;
}

namespace
{
	IVistaPropertyGetFunctor* s_aParameterGetter[] =
	{
		
		new TVistaPropertyGet< bool, VistaOpenVRDriver::Parameters, VistaProperty::PROPT_BOOL > (
						"DRAW_CONTROLLERS", SsReflectionName, 
						&VistaOpenVRDriver::Parameters::GetDrawControllers,
						"Draws models of the controllers using OpenVR-provided models. Requires an active VistaSystem" ),
		new TVistaPropertyGet< float, VistaOpenVRDriver::Parameters, VistaProperty::PROPT_DOUBLE > (
						"CONTROLLER_MODEL_EMISSIVE", SsReflectionName, 
						&VistaOpenVRDriver::Parameters::GetControllerModelEmissive,
						"Emissive light for controller models" ),
		new TVistaPropertyGet< bool, VistaOpenVRDriver::Parameters, VistaProperty::PROPT_BOOL > (
						"PROCESS_BUTTON_PRESSES_BETWEEN_FRAMES", SsReflectionName, 
						&VistaOpenVRDriver::Parameters::GetProcessButtonPressesBetweenFrames,
						"Write button presses between two frames as separate sensor measures" ),
		new TVistaPropertyGet< bool, VistaOpenVRDriver::Parameters, VistaProperty::PROPT_BOOL > (
						"PROCESS_BUTTON_TOUCHES_BETWEEN_FRAMES", SsReflectionName, 
						&VistaOpenVRDriver::Parameters::GetProcessButtonTouchesBetweenFrames,
						"Write button touches between two frames as separate sensor measures" ),
		NULL
	};

	IVistaPropertySetFunctor* s_aParameterSetter[] =
	{
		new TVistaPropertySet< bool, bool, VistaOpenVRDriver::Parameters > (
						"DRAW_CONTROLLERS",	SsReflectionName, 
						&VistaOpenVRDriver::Parameters::SetDrawControllers,
						"Draws models of the controllers using OpenVR-provided models. Requires an active VistaSystem" ),
		new TVistaPropertySet< float, float, VistaOpenVRDriver::Parameters > (
						"CONTROLLER_MODEL_EMISSIVE", SsReflectionName, 
						&VistaOpenVRDriver::Parameters::SetControllerModelEmissive,
						"Emissive light for controller models" ),
		new TVistaPropertySet< bool, bool, VistaOpenVRDriver::Parameters > (
						"PROCESS_BUTTON_PRESSES_BETWEEN_FRAMES", SsReflectionName, 
						&VistaOpenVRDriver::Parameters::SetProcessButtonPressesBetweenFrames,
						"Write button presses between two frames as separate sensor measures" ),
		new TVistaPropertySet< bool, bool, VistaOpenVRDriver::Parameters > (
						"PROCESS_BUTTON_TOUCHES_BETWEEN_FRAMES", SsReflectionName, 
						&VistaOpenVRDriver::Parameters::SetProcessButtonTouchesBetweenFrames,
						"Write button touches between two frames as separate sensor measures" ),
		
		NULL
	};
}


////////////////////////////////////////////////////////////////////////////////

VistaOpenVRDriver::VistaOpenVRDriver( IVistaDriverCreationMethod* pCreationMethod )
: IVistaDeviceDriver( pCreationMethod )
, m_pSensorMapping( new VistaDriverSensorMappingAspect( pCreationMethod ) )
, m_pParameters( nullptr )
, m_pHMD( nullptr )
, m_nHMDIndex( ~0U )
, m_pRenderTransform( nullptr )
, m_vecData( vr::k_unMaxTrackedDeviceCount )
{
	SetUpdateType( IVistaDeviceDriver::UPDATE_EXPLICIT_POLL );	

	m_nHMDType = m_pSensorMapping->GetTypeId( "HMD" );
	assert( m_nHMDType != VistaDriverSensorMappingAspect::INVALID_TYPE );
	m_nControllerType = m_pSensorMapping->GetTypeId( "CONTROLLER" );
	assert( m_nControllerType != VistaDriverSensorMappingAspect::INVALID_TYPE );
	m_nBodyType = m_pSensorMapping->GetTypeId( "BODY" );
	assert( m_nBodyType != VistaDriverSensorMappingAspect::INVALID_TYPE );
	m_nHMDInfoType = m_pSensorMapping->GetTypeId( "HMD_INFO" );
	assert( m_nHMDInfoType != VistaDriverSensorMappingAspect::INVALID_TYPE );
	m_nControllerInfoType = m_pSensorMapping->GetTypeId( "CONTROLLER_INFO" );
	assert( m_nControllerInfoType != VistaDriverSensorMappingAspect::INVALID_TYPE );
	m_nBodyInfoType = m_pSensorMapping->GetTypeId( "BODY_INFO" );
	assert( m_nBodyInfoType != VistaDriverSensorMappingAspect::INVALID_TYPE );
	RegisterAspect( m_pSensorMapping );

	m_pParameterAspect = new VistaDriverGenericParameterAspect( new TParameterCreate< VistaOpenVRDriver, VistaOpenVRDriver::Parameters >( this ) );
	RegisterAspect( m_pParameterAspect );
	m_pParameters = m_pParameterAspect->GetParameter< Parameters >();
}

VistaOpenVRDriver::~VistaOpenVRDriver()
{
	UnregisterAspect( m_pSensorMapping, IVistaDeviceDriver::DO_NOT_DELETE_ASPECT );
	delete m_pSensorMapping;
	UnregisterAspect( m_pParameterAspect, IVistaDeviceDriver::DO_NOT_DELETE_ASPECT );
	delete m_pParameterAspect;

	// nodes already cleared by scene graph
	//m_mapRenderModelProtos.clear();
	//delete m_pRenderTransform;
}

bool VistaOpenVRDriver::DoSensorUpdate( VistaType::microtime dTs )
{
	if( m_pHMD == nullptr )
		return false;

 	if( m_nHMDIndex == ~0U )
	{
		// no connection checked yet, parse all devices
		for( unsigned int nDevice = 0; nDevice < vr::k_unMaxTrackedDeviceCount; ++nDevice )
		{
			if( m_pHMD->IsTrackedDeviceConnected( nDevice ) )
			{
				EnableTrackedDevice( nDevice );
				UpdateDeviceProperties( nDevice, dTs );
			}
		}
		assert( m_nHMDIndex != ~0U );
	}
	
	vr::EVRCompositorError eError = vr::VRCompositorError_None;
	eError = vr::VRCompositor()->GetLastPoses( m_aTrackedDevicePoses, vr::k_unMaxTrackedDeviceCount, NULL, 0 );

	if( eError )
	{
		vstr::warnp() << "[OpenVRDriver]: Error retrieving poses: " << eError << std::endl;
		return false;
	}

	ProcessEvents( dTs );

	ProcessPoses( dTs );
	
	return true;
}


bool VistaOpenVRDriver::DoConnect()
{
	m_pHMD = vr::VRSystem();
	if( m_pHMD == nullptr )
	{
		vstr::warnp() << "[VistaOpenVRDriver]: Could not retrieve OpenVR HMD - cannot connect" << std::endl;
	}
	return ( m_pHMD != nullptr );
}

bool VistaOpenVRDriver::DoDisconnect()
{
	m_pHMD = NULL;
	return false;
}

void VistaOpenVRDriver::ProcessEvents( VistaType::microtime dTs )
{
	vr::VREvent_t oEvent;
	while( m_pHMD->PollNextEvent( &oEvent, sizeof( oEvent ) ) )
	{
		switch( oEvent.eventType )
		{
			case vr::VREvent_TrackedDeviceActivated:
			{
				EnableTrackedDevice( (unsigned int)oEvent.trackedDeviceIndex );
				UpdateDeviceProperties( (unsigned int)oEvent.trackedDeviceIndex , dTs - oEvent.eventAgeSeconds );
				break;
			}
			case vr::VREvent_TrackedDeviceDeactivated:
			{
				DisableTrackedDevice( (unsigned int)oEvent.trackedDeviceIndex );
				break;
			}
			case vr::VREvent_TrackedDeviceUpdated:
			{
				UpdateDeviceProperties( (unsigned int)oEvent.trackedDeviceIndex, dTs - oEvent.eventAgeSeconds );
				break;
			}
			//case vr::VREvent_TrackedDeviceUserInteractionStarted:
			//case vr::VREvent_TrackedDeviceUserInteractionEnded:
			//case vr::VREvent_EnterStandbyMode:
			//case vr::VREvent_LeaveStandbyMode:
			//case vr::VREvent_TouchPadMove:
			
			case vr::VREvent_ButtonPress:
			{
				UpdateControllerButtons( (unsigned int)oEvent.trackedDeviceIndex, oEvent.data.controller.button,
											false, true, dTs - oEvent.eventAgeSeconds );
				break;
			}
			case vr::VREvent_ButtonUnpress:
			{
				UpdateControllerButtons( (unsigned int)oEvent.trackedDeviceIndex, oEvent.data.controller.button,
											false, false, dTs - oEvent.eventAgeSeconds );
				break;
			}
			case vr::VREvent_ButtonTouch:
			{
				UpdateControllerButtons( (unsigned int)oEvent.trackedDeviceIndex, oEvent.data.controller.button,
											true, true, dTs - oEvent.eventAgeSeconds );
				break;
			}
			case vr::VREvent_ButtonUntouch:
			{
				UpdateControllerButtons( (unsigned int)oEvent.trackedDeviceIndex, oEvent.data.controller.button,
											true, false, dTs - oEvent.eventAgeSeconds );
				break;
			}

			case vr::VREvent_HideRenderModels:
			{
				m_bRenderModelsForbidden = true;
				if( m_pRenderTransform )
					m_pRenderTransform->SetIsEnabled( !m_bRenderModelsForbidden && m_pParameters->GetDrawControllers() );
				break;
			}
			case vr::VREvent_ShowRenderModels:
			{
				m_bRenderModelsForbidden = false;
				if( m_pRenderTransform )
					m_pRenderTransform->SetIsEnabled( !m_bRenderModelsForbidden && m_pParameters->GetDrawControllers() );
				break;
			}
			default:
				break;
		}
	}
}

void VistaOpenVRDriver::UpdateDeviceProperties( const unsigned int nDeviceIndex, VistaType::microtime dTs )
{
	BodyData& oData = m_vecData[nDeviceIndex];
	if( oData.m_bConnected == false )
		return;

	unsigned int nSensorIndex = m_pSensorMapping->GetSensorId( oData.m_nSensorInfoType, oData.m_nSensorIndex );
	VistaDeviceSensor* pSensor = GetSensorByIndex( nSensorIndex );
	if( pSensor == nullptr )
	{
		if( oData.m_eType == TYPE_CONTROLLER )
		{
			oData.m_nNumButtons = m_pHMD->GetUint64TrackedDeviceProperty( nDeviceIndex, vr::Prop_SupportedButtons_Uint64 );
			int aAxes[5] = { 0, 0, 0, 0 };
			aAxes[0] = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_Axis0Type_Int32 );
			aAxes[1] = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_Axis1Type_Int32 );
			aAxes[2] = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_Axis2Type_Int32 );
			aAxes[3] = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_Axis3Type_Int32 );
			aAxes[4] = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_Axis4Type_Int32 );
			oData.m_nNumAxes = std::count_if( aAxes, aAxes + 5, []( const int& eType ) { return ( eType != vr::k_eControllerAxis_None ); } );
		}
		return;
	}

	m_pHistoryAspect->MeasureStart( pSensor, dTs );
	VistaSensorMeasure* pMeasure = m_pHistoryAspect->GetCurrentSlot( pSensor );

	VistaOpenVRMeasures::BodyInfo* pBodyInfo = pMeasure->getWrite< VistaOpenVRMeasures::BodyInfo >();

	char sPropertyGetBuffer[ vr::k_unMaxPropertyStringSize ];

	// general properties that apply to all device classes
	m_pHMD->GetStringTrackedDeviceProperty( nDeviceIndex, vr::Prop_TrackingSystemName_String, sPropertyGetBuffer, vr::k_unControllerStateAxisCount );
	pBodyInfo->m_sTrackingSystemName = sPropertyGetBuffer;
	m_pHMD->GetStringTrackedDeviceProperty( nDeviceIndex, vr::Prop_ModelNumber_String, sPropertyGetBuffer, vr::k_unControllerStateAxisCount );
	pBodyInfo->m_sModelNumber = sPropertyGetBuffer;
	m_pHMD->GetStringTrackedDeviceProperty( nDeviceIndex, vr::Prop_RenderModelName_String, sPropertyGetBuffer, vr::k_unControllerStateAxisCount );
	pBodyInfo->m_sRenderModelName = sPropertyGetBuffer;
	pBodyInfo->m_bWillDrifftInYaw = m_pHMD->GetBoolTrackedDeviceProperty( nDeviceIndex, vr::Prop_WillDriftInYaw_Bool );
	m_pHMD->GetStringTrackedDeviceProperty( nDeviceIndex, vr::Prop_ManufacturerName_String, sPropertyGetBuffer, vr::k_unControllerStateAxisCount );
	pBodyInfo->m_sManufacturerName = sPropertyGetBuffer;
	pBodyInfo->m_bIsWirelessDevice = m_pHMD->GetBoolTrackedDeviceProperty( nDeviceIndex, vr::Prop_DeviceIsWireless_Bool );
	pBodyInfo->m_bDeviceIsCharging = m_pHMD->GetBoolTrackedDeviceProperty( nDeviceIndex, vr::Prop_DeviceIsCharging_Bool );
	pBodyInfo->m_fDeviceBatteryCharge = m_pHMD->GetFloatTrackedDeviceProperty( nDeviceIndex, vr::Prop_DeviceBatteryPercentage_Float );
	pBodyInfo->m_bDeviceProvidesBatteryCharge = m_pHMD->GetBoolTrackedDeviceProperty( nDeviceIndex, vr::Prop_DeviceProvidesBatteryStatus_Bool );
	pBodyInfo->m_bDeviceCanPowerOff = m_pHMD->GetBoolTrackedDeviceProperty( nDeviceIndex, vr::Prop_DeviceCanPowerOff_Bool );
	pBodyInfo->m_bHasCamera = m_pHMD->GetBoolTrackedDeviceProperty( nDeviceIndex, vr::Prop_HasCamera_Bool );

	if( oData.m_eType == TYPE_HMD )
	{
		VistaOpenVRMeasures::HMDInfo* pHMDInfo = pMeasure->getWrite< VistaOpenVRMeasures::HMDInfo >();

		// Properties that are unique to TrackedDeviceClass_HMD
		pHMDInfo->m_bReportsTimeSinceVSync = m_pHMD->GetBoolTrackedDeviceProperty( nDeviceIndex, vr::Prop_ReportsTimeSinceVSync_Bool );
		pHMDInfo->m_fSecondsFromVSyncToPhotons = m_pHMD->GetFloatTrackedDeviceProperty( nDeviceIndex, vr::Prop_SecondsFromVsyncToPhotons_Float );
		pHMDInfo->m_fDisplayFrequency = m_pHMD->GetFloatTrackedDeviceProperty( nDeviceIndex, vr::Prop_DisplayFrequency_Float );
		pHMDInfo->m_fUserIPD = m_pHMD->GetFloatTrackedDeviceProperty( nDeviceIndex, vr::Prop_UserIpdMeters_Float );
		pHMDInfo->m_fUserHeadToEyeDepth = m_pHMD->GetFloatTrackedDeviceProperty( nDeviceIndex, vr::Prop_UserHeadToEyeDepthMeters_Float );
		vr::HmdMatrix34_t matTransform = m_pHMD->GetMatrix34TrackedDeviceProperty( nDeviceIndex, vr::Prop_CameraToHeadTransform_Matrix34 );
		for( int i = 0; i < 3; ++i )
		{
			for( int j = 0; j < 4; ++j )
			{
				pHMDInfo->m_matCameraToHeadTransform[i][j] = matTransform.m[i][j];
			}
		}
	}
	else if( oData.m_eType == TYPE_CONTROLLER )
	{
		VistaOpenVRMeasures::ControllerInfo* pHMDInfo = pMeasure->getWrite< VistaOpenVRMeasures::ControllerInfo >();

		// Properties that are unique to TrackedDeviceClass_Controller
		pHMDInfo->m_nNumButtons = m_pHMD->GetUint64TrackedDeviceProperty( nDeviceIndex, vr::Prop_SupportedButtons_Uint64 );
		pHMDInfo->m_eAxisTypes[0] = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_Axis0Type_Int32 );
		pHMDInfo->m_eAxisTypes[1] = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_Axis1Type_Int32 );
		pHMDInfo->m_eAxisTypes[2] = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_Axis2Type_Int32 );
		pHMDInfo->m_eAxisTypes[3] = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_Axis3Type_Int32 );
		pHMDInfo->m_eAxisTypes[4] = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_Axis4Type_Int32 );
		pHMDInfo->m_nNumAxes = std::count_if( pHMDInfo->m_eAxisTypes, pHMDInfo->m_eAxisTypes + 5,
					[]( const int& eType ) { return ( eType != VistaOpenVRMeasures::ControllerInfo::AT_NONE ); } );
		pHMDInfo->m_eControllerHand = m_pHMD->GetInt32TrackedDeviceProperty( nDeviceIndex, vr::Prop_ControllerRoleHint_Int32 );

		oData.m_nNumButtons = pHMDInfo->m_nNumButtons;
		oData.m_nNumAxes = pHMDInfo->m_nNumAxes;		
	}
	m_pHistoryAspect->MeasureStop( pSensor );

}

void VistaOpenVRDriver::ProcessPoses( VistaType::microtime dTs )
{
	for( int nDevice = 0; nDevice < vr::k_unMaxTrackedDeviceCount; ++nDevice )
	{
		const vr::TrackedDevicePose_t& oPose = m_aTrackedDevicePoses[ nDevice ];
		if( oPose.bDeviceIsConnected == false || oPose.bPoseIsValid == false )
			continue;

		BodyData& oData = m_vecData[ nDevice ];
		if( oData.m_bConnected == false )
			continue;

		unsigned int nSensorIndex = m_pSensorMapping->GetSensorId( oData.m_nSensorType, oData.m_nSensorIndex );
		VistaDeviceSensor* pSensor = GetSensorByIndex( nSensorIndex );

		if( pSensor == nullptr )
			continue;

		m_pHistoryAspect->MeasureStart( pSensor, dTs );
		VistaSensorMeasure* pMeasure = m_pHistoryAspect->GetCurrentSlot( pSensor );
		VistaOpenVRMeasures::BodyMeasure* pBodyData = pMeasure->getWrite< VistaOpenVRMeasures::BodyMeasure >();

		for( int i = 0; i < 3; ++i )
		{
			pBodyData->m_v3LinearVelocity[i] = oPose.vVelocity.v[i];
			pBodyData->m_v3AngularVelocity[i] = oPose.vAngularVelocity.v[i];
			for( int j = 0; j < 4; ++j )
				pBodyData->m_matTransform[i][j] = oPose.mDeviceToAbsoluteTracking.m[i][j];
			pBodyData->m_matTransform[3][0] = 0;
			pBodyData->m_matTransform[3][1] = 0;
			pBodyData->m_matTransform[3][2] = 0;
			pBodyData->m_matTransform[3][3] = 1;
		}

		if( oData.m_eType == TYPE_CONTROLLER )
		{
			VistaOpenVRMeasures::ControllerMeasure* pControllerData = pMeasure->getWrite< VistaOpenVRMeasures::ControllerMeasure >();

			vr::VRControllerState_t oControllerState = { };
			if( m_pHMD->GetControllerState( oData.m_nOVRIndex, &oControllerState, sizeof(oControllerState) ) )
			{
				oData.m_nButtonPressedMask = oControllerState.ulButtonPressed;
				oData.m_nButtonTouchedMask = oControllerState.ulButtonTouched;

				pControllerData->m_nNumButtons = oData.m_nNumButtons;
				pControllerData->m_nButtonPressedMask = oControllerState.ulButtonPressed;
				pControllerData->m_nButtonTouchedMask = oControllerState.ulButtonTouched;
				pControllerData->m_nNumAxes = oData.m_nNumAxes;
				for( int i = 0; i < VistaOpenVRMeasures::s_nMaxAxesPerController; ++i )
				{
					pControllerData->m_aAxes[i][0] = oControllerState.rAxis[i].x;
					pControllerData->m_aAxes[i][1] = oControllerState.rAxis[i].y;
				}
			}
			else
			{
				vstr::warnp() << "[VistaOpenVRDriver]: Could not retrieve state of controller" << std::endl;
			}

			if( m_pParameters->GetDrawControllers() )
				UpdateControllerRenderModels( oData, pBodyData->m_matTransform );
		}
		m_pHistoryAspect->MeasureStop( pSensor );
	}
}

void VistaOpenVRDriver::EnableTrackedDevice( const unsigned int nIndex )
{
	BodyData& oData = m_vecData[ nIndex ];

	vr::ETrackedDeviceClass eClass = m_pHMD->GetTrackedDeviceClass( nIndex );
	
	if( oData.m_bConnected )
	{
		return;
	}
	
	switch( eClass )
	{
		case vr::TrackedDeviceClass_HMD:
		{
			if( oData.m_eType == TYPE_NONE )
			{
				oData.m_eType = TYPE_HMD;
				oData.m_nSensorType = m_nHMDType;
				oData.m_nSensorInfoType = m_nHMDInfoType;
				assert( oData.m_nSensorIndex == VistaDriverSensorMappingAspect::INVALID_ID );
				oData.m_nOVRIndex = (unsigned int)nIndex;
				oData.m_nSensorIndex = 0; // there is only one HMD
				m_nHMDIndex = nIndex;
			}
			assert( oData.m_eType == TYPE_HMD ); // same index should not be reused for different device types
			oData.m_bConnected = true;
			break;
		}
		case vr::TrackedDeviceClass_Controller:
		{			
			if( oData.m_eType == TYPE_NONE )
			{
				oData.m_eType = TYPE_CONTROLLER;
				oData.m_nSensorType = m_nControllerType;
				oData.m_nSensorInfoType = m_nControllerInfoType;
				assert( oData.m_nSensorIndex == VistaDriverSensorMappingAspect::INVALID_ID );
				oData.m_nSensorIndex = (unsigned int)m_vecControllerIndices.size();
				oData.m_nOVRIndex = (unsigned int)nIndex;
				m_vecControllerIndices.push_back( oData.m_nOVRIndex );
			}
			assert( oData.m_eType == TYPE_CONTROLLER ); // same index should not be reused for different device types
			oData.m_bConnected = true;
			if( m_pParameters->GetDrawControllers() )
				UpdateControllerRenderModels( oData, VistaTransformMatrix() );
			break;
		}
		case vr::TrackedDeviceClass_GenericTracker:
		{
			if( oData.m_eType == TYPE_NONE )
			{
				oData.m_eType = TYPE_BODY;
				oData.m_nSensorType = m_nBodyType;
				oData.m_nSensorInfoType = m_nBodyInfoType;
				assert( oData.m_nSensorIndex == VistaDriverSensorMappingAspect::INVALID_ID );
				oData.m_nOVRIndex = (unsigned int)nIndex;
				oData.m_nSensorIndex = (unsigned int)m_vecBodyIndices.size();
				m_vecBodyIndices.push_back( oData.m_nOVRIndex );
			}
			assert( oData.m_eType == TYPE_BODY ); // same index should not be reused for different device types
			oData.m_bConnected = true;
			break;
		}
		case vr::TrackedDeviceClass_Invalid:
		case vr::TrackedDeviceClass_TrackingReference:
		{
			oData.m_bConnected = false;
			oData.m_eType = TYPE_NONE;
			oData.m_nSensorIndex = VistaDriverSensorMappingAspect::INVALID_ID;
			oData.m_nSensorType = VistaDriverSensorMappingAspect::INVALID_TYPE;
			oData.m_nSensorInfoType = VistaDriverSensorMappingAspect::INVALID_TYPE;
			break;
		}
		
	}	
}
void VistaOpenVRDriver::DisableTrackedDevice( unsigned int nIndex )
{
	m_vecData[ nIndex ].m_bConnected = false;
}


void VistaOpenVRDriver::UpdateControllerButtons( unsigned int nIndex, const int nButton, const bool bTouch, const bool bPressed, VistaType::microtime dTs )
{
	BodyData& oData = m_vecData[nIndex];

	if( bTouch )
	{
		if( m_pParameters->GetProcessButtonTouchesBetweenFrames() == false )
			return;
		if( bPressed )
			oData.m_nButtonTouchedMask |= ( 1ULL << nButton );
		else
			oData.m_nButtonTouchedMask &= ~( 1ULL << nButton );
	}
	else
	{
		if( m_pParameters->GetProcessButtonPressesBetweenFrames() == false )
			return;
		if( bPressed )
			oData.m_nButtonPressedMask |= ( 1ULL << nButton );
		else
			oData.m_nButtonPressedMask &= ~( 1ULL << nButton );
	}		

	unsigned int nSensorIndex = m_pSensorMapping->GetSensorId( oData.m_nSensorType, oData.m_nSensorIndex );
	VistaDeviceSensor* pSensor = GetSensorByIndex( nSensorIndex );

	if( pSensor == nullptr )
		return;

	VistaSensorMeasure* pPreviousMeasure = m_pHistoryAspect->GetCurrentSlot( pSensor );
	if( pPreviousMeasure == nullptr )
		return;

	VistaOpenVRMeasures::ControllerMeasure* pPreviousBodyData = ( pPreviousMeasure ? pPreviousMeasure->getWrite< VistaOpenVRMeasures::ControllerMeasure >() : nullptr );
	
	m_pHistoryAspect->MeasureStart( pSensor, dTs );
	VistaSensorMeasure* pMeasure = m_pHistoryAspect->GetCurrentSlot( pSensor );
	VistaOpenVRMeasures::ControllerMeasure* pBodyData = pMeasure->getWrite< VistaOpenVRMeasures::ControllerMeasure >();

	*pBodyData = *pPreviousBodyData;

	pBodyData->m_nButtonPressedMask = oData.m_nButtonPressedMask;
	pBodyData->m_nButtonTouchedMask = oData.m_nButtonTouchedMask;

	m_pHistoryAspect->MeasureStop( pSensor );
}

void VistaOpenVRDriver::UpdateControllerRenderModels( BodyData& oData, const VistaTransformMatrix& matTransform )
{
	if( oData.m_pTransformNode )
	{
		oData.m_pTransformNode->SetTransform( matTransform );
	}
	else if( oData.m_pRenderData == nullptr )
	{
		char cBuffer[ vr::k_unMaxPropertyStringSize ];
		m_pHMD->GetStringTrackedDeviceProperty( oData.m_nOVRIndex, vr::Prop_RenderModelName_String, cBuffer, vr::k_unMaxPropertyStringSize );
		std::string sRenderModelName = cBuffer;
		RenderData& oRenderData = m_mapRenderModelProtos[ sRenderModelName ];
		if( sRenderModelName.empty() )
			oRenderData.m_bLoadingHasFailed = true;
		oData.m_pRenderData = &oRenderData;
		oRenderData.m_sRenderModelName = sRenderModelName;
	}
	else if( oData.m_pRenderData->m_bLoadingHasFailed == false )
	{
		CheckRenderModelUpdate( oData.m_pRenderData );
		if( oData.m_pRenderData->m_pNode != nullptr )
		{
			VistaSceneGraph* pSceneGraph = GetSceneGraph();
			if( m_pRenderTransform == nullptr )
			{
				VistaUserPlatform* pPlatform = ::GetVistaSystem()->GetPlatformFor( ::GetVistaSystem()->GetDisplayManager()->GetDisplaySystem() );
				m_pRenderTransform  = pSceneGraph->NewTransformNode( pPlatform->GetPlatformNode() );
				m_pRenderTransform->SetName( "OpenVR_Controller_Models" );
			}
			oData.m_pTransformNode = pSceneGraph->NewTransformNode( m_pRenderTransform );
			oData.m_pTransformNode->SetName( "OpenVR_Controller_" + VistaConversion::ToString( oData.m_nOVRIndex ) + "_transform" );
			oData.m_pTransformNode->SetTransform( matTransform );
			oData.m_pGeomNode = pSceneGraph->NewGeomNode( oData.m_pTransformNode, oData.m_pRenderData->m_pNode->GetGeometry() );
			oData.m_pGeomNode->SetName( "OpenVR_Controller_" + VistaConversion::ToString( oData.m_nOVRIndex ) + "_geom" );
		}
	}
}

void VistaOpenVRDriver::CheckRenderModelUpdate( RenderData* pRenderData )
{
	VistaSceneGraph* pSceneGraph = GetSceneGraph();
	if( pSceneGraph == nullptr )
	{
		vstr::warnp() << "[OpenVRDriver] Cannot create render models - no scenegraph available" << std::endl;
		pRenderData->m_bLoadingHasFailed = true;
		return;
	}

	vr::EVRRenderModelError nError = vr::VRRenderModels()->LoadRenderModel_Async( pRenderData->m_sRenderModelName.c_str(), &pRenderData->m_pOVRModel );
	if ( nError == vr::VRRenderModelError_Loading )
		return;
	if( nError != vr::VRRenderModelError_None || pRenderData->m_pOVRModel == nullptr )
	{
		vstr::warnp() << "[OpenVRDriver] Loading Render Model " << pRenderData->m_sRenderModelName << " failed with error code " << nError << std::endl;
		pRenderData->m_bIsLoading = false;
		pRenderData->m_bLoadingHasFailed = true;
		return;
	}
	
	nError = vr::VRRenderModels()->LoadTexture_Async( pRenderData->m_pOVRModel->diffuseTextureId, &pRenderData->m_pOVRTexture );
	if ( nError == vr::VRRenderModelError_Loading )
		return;
	if( nError != vr::VRRenderModelError_None )
	{
		vstr::warnp() << "[OpenVRDriver] Loading Texture for Render Model " << pRenderData->m_sRenderModelName << " failed with error code " << nError << std::endl;
		pRenderData->m_bIsLoading = false;
		pRenderData->m_bLoadingHasFailed = true;
		return;
	}
	
	int nNumVertices = pRenderData->m_pOVRModel->unVertexCount;
	std::vector< float > vecCoords;
	vecCoords.reserve( 3 * nNumVertices );
	std::vector< float > vecNormals;
	vecNormals.reserve( 3 * nNumVertices );
	std::vector< float > vecTexCoords;
	vecTexCoords.reserve( 2 * nNumVertices );
	std::vector< VistaColor > vecColors;
	const vr::RenderModel_Vertex_t* pVertex = pRenderData->m_pOVRModel->rVertexData;
	for( int nVert = 0; nVert < nNumVertices; ++nVert, ++pVertex )
	{
		for( int i = 0; i < 3; ++i )
		{
			vecCoords.push_back( pVertex->vPosition.v[i] );
			vecNormals.push_back( pVertex->vNormal.v[i] );
		}
		for( int i = 0; i < 2; ++i )
		{
			vecTexCoords.push_back( pVertex->rfTextureCoord[i] );
		}
	}
	int nNumTris = pRenderData->m_pOVRModel->unTriangleCount;
	std::vector< VistaIndexedVertex > vecTriIndices( 3 * nNumTris );
	std::transform( pRenderData->m_pOVRModel->rIndexData, pRenderData->m_pOVRModel->rIndexData + 3 * nNumTris,
					vecTriIndices.begin(), []( const uint16_t& nIndex )
					{
						VistaIndexedVertex nVert( nIndex );
						nVert.SetTextureCoordinateIndex( nIndex );
						nVert.SetNormalIndex( nIndex );
						return nVert;
					} );
	VistaVertexFormat oFormat;
	oFormat.normal = VistaVertexFormat::NORMAL;
	oFormat.textureCoord = VistaVertexFormat::TEXTURE_COORD_2D;
	oFormat.color = VistaVertexFormat::COLOR_NONE;
	VistaGeometry* pGeom = pSceneGraph->NewIndexedGeometry( vecTriIndices, vecCoords, vecTexCoords, vecNormals, vecColors, oFormat );
	// @TODO: should make SetTexture API const correct
	pGeom->SetTexture( pRenderData->m_pOVRTexture->unWidth, pRenderData->m_pOVRTexture->unHeight, 8, true,
				const_cast< VistaType::byte* >( reinterpret_cast< const VistaType::byte* >( pRenderData->m_pOVRTexture->rubTextureMapData ) ) );

	VistaMaterial oMaterial;
	oMaterial.SetAmbientColor( VistaColor::WHITE );
	oMaterial.SetDiffuseColor( VistaColor::WHITE );
	oMaterial.SetSpecularColor( VistaColor::WHITE );
	float fEmissive = m_pParameters->GetControllerModelEmissive();
	oMaterial.SetEmissionColor( VistaColor( fEmissive, fEmissive, fEmissive ) );
	pGeom->SetMaterial( oMaterial );

	pRenderData->m_pNode = pSceneGraph->NewGeomNode( NULL, pGeom );
	pRenderData->m_bIsLoading = false;

	vr::VRRenderModels()->FreeRenderModel( pRenderData->m_pOVRModel );
	vr::VRRenderModels()->FreeTexture( pRenderData->m_pOVRTexture );
	pRenderData->m_pOVRModel = nullptr;
	pRenderData->m_pOVRTexture = nullptr;
}

VistaSceneGraph* VistaOpenVRDriver::GetSceneGraph()
{
	VistaSystem* pSystem = ::GetVistaSystem();
	if( pSystem == nullptr )
		return nullptr;
	return pSystem->GetGraphicsManager()->GetSceneGraph();
}

