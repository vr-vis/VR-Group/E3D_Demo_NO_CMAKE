#include "VistaBase\VistaTransformMatrix.h"
/*============================================================================*/
/*                              ViSTA VR toolkit                              */
/*               Copyright (c) 1997-2016 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VISTAOPENVRCOMMONSHARE_H
#define _VISTAOPENVRCOMMONSHARE_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

namespace VistaOpenVRMeasures
{
	struct BodyMeasure
	{
		VistaTransformMatrix m_matTransform;
		VistaVector3D m_v3LinearVelocity;
		VistaVector3D m_v3AngularVelocity;
	};
	struct BodyInfo
	{
		std::string m_sTrackingSystemName;
		std::string m_sModelNumber;
		std::string m_sRenderModelName;
		std::string m_sManufacturerName;
		bool m_bWillDrifftInYaw;
		bool m_bIsWirelessDevice;
		bool m_bDeviceIsCharging;
		float m_fDeviceBatteryCharge;
		bool m_bDeviceProvidesBatteryCharge;
		bool m_bDeviceCanPowerOff;
		bool m_bHasCamera;
	};

	struct HMDMeasure : public BodyMeasure
	{
	
	};
	struct HMDInfo : BodyInfo
	{
		bool m_bReportsTimeSinceVSync;
		float m_fSecondsFromVSyncToPhotons;
		float m_fDisplayFrequency;
		float m_fUserIPD;
		float m_fUserHeadToEyeDepth;
		VistaTransformMatrix m_matCameraToHeadTransform;
	};

	const int s_nMaxAxesPerController = 5;
	struct ControllerMeasure : public BodyMeasure
	{
		unsigned int m_nNumButtons;
		VistaType::uint64 m_nButtonPressedMask;
		VistaType::uint64 m_nButtonTouchedMask;
		unsigned int m_nNumAxes;
		float m_aAxes[ s_nMaxAxesPerController ][2];
	};
	struct ControllerInfo : BodyInfo
	{
		enum
		{
			AT_NONE = 0,
			AT_TRACKPAD = 1,
			AT_JOYSTICK = 2,
			AT_ANALOG_TRIGGER = 3,
		};
		enum
		{
			CH_UNKNOWN = -1,
			CH_LEFT = 0,
			CH_RIGHT = 1,
		};
		int m_nNumButtons;
		int m_eAxisTypes[5];
		int m_nNumAxes;
		int m_eControllerHand;
	};

	
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif //_VISTAOPENVRCOMMONSHARE_H

