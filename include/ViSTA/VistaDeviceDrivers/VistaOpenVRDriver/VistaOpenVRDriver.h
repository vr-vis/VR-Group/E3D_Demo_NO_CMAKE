/*============================================================================*/
/*                              ViSTA VR toolkit                              */
/*               Copyright (c) 1997-2016 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VISTAOPENVRDRIVER_H
#define _VISTAOPENVRDRIVER_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaDeviceDriversBase/VistaDeviceDriver.h>
#include "VistaOpenVRCommonShare.h"

#include <VistaDeviceDriversBase/VistaDeviceSensor.h>
#include "VistaDeviceDriversBase/DriverAspects/VistaDriverGenericParameterAspect.h"

#include <openvr.h>
#include "VistaKernel/GraphicsManager/VistaTransformNode.h"
#include "VistaKernel/GraphicsManager/VistaGeomNode.h"
#include "VistaKernel/GraphicsManager/VistaOpenGLNode.h"

class VistaSceneGraph;
class VistaDriverSensorMappingAspect;

// Windows DLL build
#if defined(WIN32) && !defined(VISTAOPENVRDRIVER_STATIC) 
#ifdef VISTAOPENVRDRIVER_EXPORTS
#define VISTAOPENVRDRIVERAPI __declspec(dllexport)
#else
#define VISTAOPENVRDRIVERAPI __declspec(dllimport)
#endif
#else // no Windows or static build
#define VISTAOPENVRDRIVERAPI
#endif

/**
 * a driver for OpenVR HMDs (e.g. HTC Vive)
 */
class VISTAOPENVRDRIVERAPI VistaOpenVRDriver : public IVistaDeviceDriver
{
public:
	class VISTAOPENVRDRIVERAPI Parameters : public VistaDriverGenericParameterAspect::IParameterContainer
	{
		REFL_DECLARE
	public:
		Parameters( VistaOpenVRDriver* pDriver );

		enum
		{
			MSG_CAPTURE_CURSOR_CHG = VistaDriverGenericParameterAspect::IParameterContainer::MSG_LAST,
			MSG_DRAW_CONTROLLER_CHG,
			MSG_CONTROLLER_MODEL_EMISSIVE_CHANGED,
			MSG_PROCESS_BUTTON_PRESSES_BETWEEN_FRAMES,
			MSG_PROCESS_BUTTON_TOUCHES_BETWEEN_FRAMES,
			MSG_LAST
		};

		bool GetDrawControllers() const;
		bool SetDrawControllers( const bool bSet );
		
		float GetControllerModelEmissive() const;
		bool SetControllerModelEmissive( const float nEmissive );
		
		bool GetProcessButtonPressesBetweenFrames() const;
		bool SetProcessButtonPressesBetweenFrames( const bool bSet );
		bool GetProcessButtonTouchesBetweenFrames() const;
		bool SetProcessButtonTouchesBetweenFrames( const bool bSet );

	private:
		bool m_bDrawControllers;
		float m_nControllerModelEmissive;
		bool m_bProcessButtonPressesBetweenFrames;
		bool m_bProcessButtonTouchesBetweenFrames;
	};
public:
	VistaOpenVRDriver( IVistaDriverCreationMethod* pCreationMethod );
	virtual ~VistaOpenVRDriver();

protected:
	
	virtual bool DoConnect();
	virtual bool DoDisconnect();

	virtual bool DoSensorUpdate( VistaType::microtime dTs );

private:
	enum Type
	{
		TYPE_NONE,
		TYPE_HMD,
		TYPE_CONTROLLER,
		TYPE_BODY,
	};
	struct RenderData
	{
		std::string m_sRenderModelName;
		bool m_bIsLoading = false;
		bool m_bLoadingHasFailed = false;
		VistaGeomNode* m_pNode = nullptr;
		vr::RenderModel_t* m_pOVRModel = nullptr;
		vr::RenderModel_TextureMap_t* m_pOVRTexture = nullptr;
	};
	struct BodyData 
	{
		BodyData();
		~BodyData();

		Type m_eType;
		
		uint64_t m_nButtonPressedMask;
		uint64_t m_nButtonTouchedMask;

		bool m_bConnected;
		unsigned int m_nOVRIndex;
		unsigned int m_nSensorIndex;
		unsigned int m_nSensorType;
		unsigned int m_nSensorInfoType;
		int m_nNumButtons;
		int m_nNumAxes;

		RenderData* m_pRenderData;
		VistaTransformNode* m_pTransformNode;
		VistaGeomNode* m_pGeomNode;
	};

	void ProcessEvents( VistaType::microtime dTs );
	void UpdateDeviceProperties( const unsigned int nDeviceIndex, VistaType::microtime dTs );
	void ProcessPoses( VistaType::microtime dTs );
	void EnableTrackedDevice( const unsigned int nIndex );
	void DisableTrackedDevice( unsigned int nIndex );
	void UpdateControllerButtons( unsigned int nIndex, const int nButton, const bool bTouch, const bool bPressed, VistaType::microtime dTs );
	void UpdateControllerRenderModels( BodyData& oData, const VistaTransformMatrix& matTransform );
	void CheckRenderModelUpdate( RenderData* pRenderData );
	VistaSceneGraph* GetSceneGraph();
private:

	unsigned int m_nHMDType;
	unsigned int m_nControllerType;
	unsigned int m_nBodyType;
	unsigned int m_nHMDInfoType;
	unsigned int m_nControllerInfoType;
	unsigned int m_nBodyInfoType;
	VistaDriverSensorMappingAspect* m_pSensorMapping;
	VistaDriverGenericParameterAspect* m_pParameterAspect;

	std::vector< BodyData > m_vecData;
	int m_nHMDIndex;
	std::vector< int > m_vecBodyIndices;
	std::vector< int > m_vecControllerIndices;

	Parameters* m_pParameters;
	vr::IVRSystem* m_pHMD;
	vr::TrackedDevicePose_t m_aTrackedDevicePoses[ vr::k_unMaxTrackedDeviceCount ];

	bool m_bRenderModelsForbidden;
	VistaTransformNode* m_pRenderTransform;
	std::map< std::string, RenderData > m_mapRenderModelProtos;
};

class VISTAOPENVRDRIVERAPI OpenVRCreationMethod : public IVistaDriverCreationMethod
{
public:
	OpenVRCreationMethod( IVistaTranscoderFactoryFactory* pMetaFac )
	: IVistaDriverCreationMethod( pMetaFac )
	{
		RegisterSensorType( "HMD", sizeof(VistaOpenVRMeasures::HMDMeasure),
			120, pMetaFac->CreateFactoryForType( "BODY" ) );
		RegisterSensorType( "CONTROLLER", sizeof(VistaOpenVRMeasures::ControllerMeasure),
			120, pMetaFac->CreateFactoryForType( "CONTROLLER" ) );
		RegisterSensorType( "BODY", sizeof(VistaOpenVRMeasures::BodyMeasure),
			120, pMetaFac->CreateFactoryForType( "BODY" ) );
		RegisterSensorType( "HMD_INFO", sizeof(VistaOpenVRMeasures::HMDInfo),
			5, pMetaFac->CreateFactoryForType( "BODY_INFO" ) );
		RegisterSensorType( "CONTROLLER_INFO", sizeof(VistaOpenVRMeasures::ControllerInfo),
			5, pMetaFac->CreateFactoryForType( "CONTROLLER_INFO" ) );
		RegisterSensorType( "BODY_INFO", sizeof(VistaOpenVRMeasures::BodyInfo),
			5, pMetaFac->CreateFactoryForType( "BODY_INFO" ) );
	}

	virtual IVistaDeviceDriver *CreateDriver()
	{
		return new VistaOpenVRDriver( this );
	}
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif //_VISTAOPENVRDRIVER_H

