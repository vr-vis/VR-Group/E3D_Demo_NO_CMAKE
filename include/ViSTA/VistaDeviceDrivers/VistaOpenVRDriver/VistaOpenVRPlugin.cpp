/*============================================================================*/
/*                              ViSTA VR toolkit                              */
/*               Copyright (c) 1997-2016 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VistaOpenVRDriver.h"
#include "VistaOpenVRCommonShare.h"

#include <VistaBase/VistaExceptionBase.h>
#include <VistaBase/VistaTimeUtils.h>


#include <cstring>
#include <cstdio>

#include <string>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

#if defined(WIN32) && !defined(VISTAOPENVRDRIVERPLUGIN_STATIC)
#ifdef VISTAOPENVRPLUGIN_EXPORTS
#define VISTAOPENVRDRIVERPLUGINAPI __declspec(dllexport)
#else
#define VISTAOPENVRDRIVERPLUGINAPI __declspec(dllimport)
#endif
#else // no Windows or static build
#define VISTAOPENVRDRIVERPLUGINAPI
#endif

// ############################################################################
// OPENVR COMMANDSET VARIANTS INTERFACE
// ############################################################################

namespace
{
	OpenVRCreationMethod *s_pFactory = nullptr;
}

extern "C" VISTAOPENVRDRIVERPLUGINAPI IVistaDeviceDriver* CreateDevice( IVistaDriverCreationMethod* pCreamtionMethod )
{
	return new VistaOpenVRDriver( pCreamtionMethod );
}

extern "C" VISTAOPENVRDRIVERPLUGINAPI IVistaDriverCreationMethod* GetCreationMethod (IVistaTranscoderFactoryFactory* pMetaFactory )
{
	if( s_pFactory == nullptr )
		s_pFactory = new OpenVRCreationMethod( pMetaFactory );

	IVistaReferenceCountable::refup( s_pFactory );
	return s_pFactory;
}

extern "C" VISTAOPENVRDRIVERPLUGINAPI void DisposeCreationMethod( IVistaDriverCreationMethod* pCreamtionMethod )
{
	if( s_pFactory == pCreamtionMethod )
	{
		delete s_pFactory;
		s_pFactory = nullptr;
	}
	else
		delete pCreamtionMethod;
}

extern "C" VISTAOPENVRDRIVERPLUGINAPI void UnloadCreationMethod( IVistaDriverCreationMethod* pCreamtionMethod )
{
	if( s_pFactory != nullptr )
	{
		if( IVistaReferenceCountable::refdown( s_pFactory ) )
			s_pFactory = nullptr;
	}
}


extern "C" VISTAOPENVRDRIVERPLUGINAPI const char *GetDeviceClassName()
{
	return "OPENVR";
}

