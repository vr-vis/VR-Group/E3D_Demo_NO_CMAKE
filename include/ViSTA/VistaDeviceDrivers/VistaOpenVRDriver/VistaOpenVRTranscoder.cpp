/*============================================================================*/
/*                              ViSTA VR toolkit                              */
/*               Copyright (c) 1997-2016 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <VistaDeviceDriversBase/VistaDriverPlugDev.h>
#include <VistaDeviceDriversBase/VistaDeviceSensor.h>
#include <VistaBase/VistaExceptionBase.h>
#include <VistaBase/VistaTimeUtils.h>

#include "VistaOpenVRCommonShare.h"

#include <cstring>
#include <cstdio>

#include <string>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

namespace
{
	class VistaOpenVRBodyTranscoder : public IVistaMeasureTranscode
	{
	public:
		VistaOpenVRBodyTranscoder() : IVistaMeasureTranscode() {}
		static std::string GetTypeString() { return "VistaOpenVRBodyTranscoder"; }
		REFL_INLINEIMP( VistaOpenVRBodyTranscoder, IVistaMeasureTranscode );
	};
	class VistaOpenVRBodyPositionGet : public IVistaMeasureTranscode::TTranscodeValueGet< VistaVector3D >
	{
	public:
		VistaOpenVRBodyPositionGet() : IVistaMeasureTranscode::TTranscodeValueGet< VistaVector3D >( "POSITION", "VistaOpenVRBodyTranscoder", "position" ) {}
		virtual VistaVector3D GetValue( const VistaSensorMeasure* pMeasure ) const
		{
			VistaVector3D v3Res;
			GetValue( pMeasure, v3Res );
			return v3Res;
		}
		virtual bool GetValue( const VistaSensorMeasure* pMeasure, VistaVector3D& v3Value ) const
		{
			const VistaOpenVRMeasures::BodyMeasure* pRealMeasure = pMeasure->getRead< VistaOpenVRMeasures::BodyMeasure >();
			pRealMeasure->m_matTransform.GetTranslation( v3Value );
			return true;
		}
	};
	class VistaOpenVRBodyOrientationGet : public IVistaMeasureTranscode::TTranscodeValueGet< VistaQuaternion >
	{
	public:
		VistaOpenVRBodyOrientationGet() : IVistaMeasureTranscode::TTranscodeValueGet< VistaQuaternion >( "ORIENTATION", "VistaOpenVRBodyTranscoder", "orientation" ) {}
		virtual VistaQuaternion GetValue( const VistaSensorMeasure* pMeasure ) const
		{
			VistaQuaternion qRes;
			GetValue( pMeasure, qRes );
			return qRes;
		}
		virtual bool GetValue( const VistaSensorMeasure* pMeasure, VistaQuaternion& qValue ) const
		{
			const VistaOpenVRMeasures::BodyMeasure* pRealMeasure = pMeasure->getRead< VistaOpenVRMeasures::BodyMeasure >();
			qValue = pRealMeasure->m_matTransform.GetRotationAsQuaternion();
			return true;
		}
	};
	IVistaPropertyGetFunctor* s_aOpenVRBodyGetter[] =
	{
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyMeasure, VistaTransformMatrix >(
											"TRANSFORM",
											"VistaOpenVRBodyTranscoder",
											"transform matrix of the body",
											&VistaOpenVRMeasures::BodyMeasure::m_matTransform ),
		new VistaOpenVRBodyPositionGet(),
		new VistaOpenVRBodyOrientationGet(),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyMeasure, VistaVector3D >(
											"LINEAR_VELOCITY",
											"VistaOpenVRBodyTranscoder",
											"linear velocity of the body",
											&VistaOpenVRMeasures::BodyMeasure::m_v3AngularVelocity ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyMeasure, VistaVector3D >(
											"ANGULAR_VELOCITY",
											"VistaOpenVRBodyTranscoder",
											"angular velocity of the body",
											&VistaOpenVRMeasures::BodyMeasure::m_v3LinearVelocity ),
	};

	class VistaOpenVRHMDTranscoder : public VistaOpenVRBodyTranscoder
	{
	public:
		VistaOpenVRHMDTranscoder() : VistaOpenVRBodyTranscoder() {}
		static std::string GetTypeString() { return "VistaOpenVRHMDTranscoder"; }
		REFL_INLINEIMP( VistaOpenVRHMDTranscoder, IVistaMeasureTranscode );
	};

	class VistaOpenVRControllerTranscoder : public VistaOpenVRBodyTranscoder
	{
	public:
		VistaOpenVRControllerTranscoder() : VistaOpenVRBodyTranscoder() {}
		static std::string GetTypeString() { return "VistaOpenVRControllerTranscoder"; }
		REFL_INLINEIMP( VistaOpenVRControllerTranscoder, VistaOpenVRBodyTranscoder );
	};
	class VistaOpenVRControllerButtonPressedTranscoder : public IVistaMeasureTranscode::TTranscodeIndexedGet< bool >
	{
	public:
		VistaOpenVRControllerButtonPressedTranscoder()
		: IVistaMeasureTranscode::TTranscodeIndexedGet< bool >( "BUTTON", "VistaOpenVRControllerTranscoder", "button state" )
		{
		}
		bool GetValueIndexed( const VistaSensorMeasure* pMeasure, bool& bResult, unsigned int nIndex ) const
		{
			const VistaOpenVRMeasures::ControllerMeasure* pRealMeasure = pMeasure->getRead< VistaOpenVRMeasures::ControllerMeasure >();
			unsigned int nMaskIndex = 0;
			if( (int)nIndex < pRealMeasure->m_nNumButtons )
				nMaskIndex = nIndex;
			else if( nIndex < pRealMeasure->m_nNumButtons + pRealMeasure->m_nNumAxes )
				nMaskIndex = 32 + ( nIndex - pRealMeasure->m_nNumButtons );
			else
				return false;

			bResult = ( pRealMeasure->m_nButtonPressedMask & ( VistaType::uint64(1ULL) << nMaskIndex ) )!= 0;
			return true;
		}
		virtual unsigned int GetNumberOfIndices() const
		{
			return 32;
		}

	};
	class VistaOpenVRControllerButtonTouchedTranscoder : public IVistaMeasureTranscode::TTranscodeIndexedGet< bool >
	{
	public:
		VistaOpenVRControllerButtonTouchedTranscoder()
		: IVistaMeasureTranscode::TTranscodeIndexedGet< bool >( "BUTTON_TOUCHED", "VistaOpenVRControllerTranscoder", "button touched state" )
		{
		}
		bool GetValueIndexed( const VistaSensorMeasure* pMeasure, bool& bResult, unsigned int nIndex ) const
		{
			const VistaOpenVRMeasures::ControllerMeasure* pRealMeasure = pMeasure->getRead< VistaOpenVRMeasures::ControllerMeasure >();
			unsigned int nMaskIndex = 0;
			if( (int)nIndex < pRealMeasure->m_nNumButtons )
				nMaskIndex = nIndex;
			else if( nIndex < pRealMeasure->m_nNumButtons + pRealMeasure->m_nNumAxes )
				nMaskIndex = 32 + ( nIndex - pRealMeasure->m_nNumButtons );
			else
				return false;

			bResult = ( pRealMeasure->m_nButtonTouchedMask & ( VistaType::uint64(1ULL) << nMaskIndex ) ) != 0;
			return true;
		}
		virtual unsigned int GetNumberOfIndices() const
		{
			return 32;
		}
	};
	class VistaOpenVRControllerAxesTranscoder : public IVistaMeasureTranscode::TTranscodeIndexedGet< VistaVector3D >
	{
	public:
		VistaOpenVRControllerAxesTranscoder()
		: IVistaMeasureTranscode::TTranscodeIndexedGet< VistaVector3D >( "AXIS", "VistaOpenVRControllerTranscoder", "axis state" )
		{
		}
		bool GetValueIndexed( const VistaSensorMeasure* pMeasure, VistaVector3D& v3Result, unsigned int nIndex ) const
		{
			const VistaOpenVRMeasures::ControllerMeasure* pRealMeasure = pMeasure->getRead< VistaOpenVRMeasures::ControllerMeasure >();
			if( (int)nIndex >= pRealMeasure->m_nNumAxes )
				return false;
			v3Result[0] = pRealMeasure->m_aAxes[ nIndex ][0];
			v3Result[1] = pRealMeasure->m_aAxes[ nIndex ][1];
			return true;
		}
		virtual unsigned int GetNumberOfIndices() const
		{
			return 5;
		}
	};
	IVistaPropertyGetFunctor* s_aOpenVRControllerGetter[] =
	{
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::ControllerMeasure, unsigned int >(
											"NUM_BUTTONS",
											"VistaOpenVRControllerTranscoder",
											"number of buttons provided by controller",
											&VistaOpenVRMeasures::ControllerMeasure::m_nNumButtons ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::ControllerMeasure, VistaType::uint64 >(
											"PRESSED_BUTTON_MASK",
											"VistaOpenVRControllerTranscoder",
											"mask of pressed buttons",
											&VistaOpenVRMeasures::ControllerMeasure::m_nButtonPressedMask ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::ControllerMeasure, VistaType::uint64 >(
											"TOUCHED_BUTTON_MASK",
											"VistaOpenVRControllerTranscoder",
											"mask of touched buttons",
											&VistaOpenVRMeasures::ControllerMeasure::m_nButtonTouchedMask ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::ControllerMeasure, unsigned int >(
											"NUM_AXES",
											"VistaOpenVRControllerTranscoder",
											"number of axes provided by controller",
											&VistaOpenVRMeasures::ControllerMeasure::m_nNumAxes ),
		new VistaOpenVRControllerButtonPressedTranscoder(),
		new VistaOpenVRControllerButtonTouchedTranscoder(),
		new VistaOpenVRControllerAxesTranscoder,
	};

	class VistaOpenVRBodyInfoTranscoder : public IVistaMeasureTranscode
	{
	public:
		VistaOpenVRBodyInfoTranscoder() : IVistaMeasureTranscode() {}
		static std::string GetTypeString() { return "VistaOpenVRBodyInfoTranscoder"; }
		REFL_INLINEIMP( VistaOpenVRBodyInfoTranscoder, IVistaMeasureTranscode );
	};
	IVistaPropertyGetFunctor* s_aOpenVRBodyInfoGetter[] =
	{
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, std::string >(
											"TRACKING_SYSTEM",
											"VistaOpenVRBodyInfoTranscoder",
											"tracking system name",
											&VistaOpenVRMeasures::BodyInfo::m_sTrackingSystemName ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, std::string >(
											"MODEL_NUMBER",
											"VistaOpenVRBodyInfoTranscoder",
											"device model number",
											&VistaOpenVRMeasures::BodyInfo::m_sModelNumber ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, std::string >(
											"RENDER_MODEL",
											"VistaOpenVRBodyInfoTranscoder",
											"descriptor of render model",
											&VistaOpenVRMeasures::BodyInfo::m_sRenderModelName ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, std::string >(
											"MANUFACTURER",
											"VistaOpenVRBodyInfoTranscoder",
											"name of device manufacturer",
											&VistaOpenVRMeasures::BodyInfo::m_sManufacturerName ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, bool >(
											"YAW_WILL_DRIFT",
											"VistaOpenVRBodyInfoTranscoder",
											"true if device yaw will drift",
											&VistaOpenVRMeasures::BodyInfo::m_bWillDrifftInYaw ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, bool >(
											"IS_WIRELESS",
											"VistaOpenVRBodyInfoTranscoder",
											"true if device is wireless",
											&VistaOpenVRMeasures::BodyInfo::m_bIsWirelessDevice ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, bool >(
											"IS_CHARGING",
											"VistaOpenVRBodyInfoTranscoder",
											"true if device is charging",
											&VistaOpenVRMeasures::BodyInfo::m_bDeviceIsCharging ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, float >(
											"BATTERY_CHARGE",
											"VistaOpenVRBodyInfoTranscoder",
											"battery charge (from 0 to 1)",
											&VistaOpenVRMeasures::BodyInfo::m_fDeviceBatteryCharge ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, bool >(
											"PROVIDES_BATTERY_CHARGE",
											"VistaOpenVRBodyInfoTranscoder",
											"true if device provides battery charge",
											&VistaOpenVRMeasures::BodyInfo::m_bDeviceProvidesBatteryCharge ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, bool >(
											"CAN_POWER_OFF",
											"VistaOpenVRBodyInfoTranscoder",
											"true if device can be pwoered off",
											&VistaOpenVRMeasures::BodyInfo::m_bDeviceCanPowerOff ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::BodyInfo, bool >(
											"HAS_CAMERA",
											"VistaOpenVRBodyInfoTranscoder",
											"true if device has a camera",
											&VistaOpenVRMeasures::BodyInfo::m_bHasCamera ),	
	};

	class VistaOpenVRHMDInfoTranscoder : public VistaOpenVRBodyInfoTranscoder
	{
	public:
		VistaOpenVRHMDInfoTranscoder() : VistaOpenVRBodyInfoTranscoder() {}
		static std::string GetTypeString() { return "VistaOpenVRHMDInfoTranscoder"; }
		REFL_INLINEIMP( VistaOpenVRBodyInfoTranscoder, VistaOpenVRBodyInfoTranscoder );
	};
	IVistaPropertyGetFunctor* s_aOpenVRHMDInfoGetter[] =
	{
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::HMDInfo, bool >(
											"REPORTS_TIME_SINCE_VSYNC",
											"VistaOpenVRHMDInfoTranscoder",
											"true if HMD reports time since last vsync",
											&VistaOpenVRMeasures::HMDInfo::m_bReportsTimeSinceVSync ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::HMDInfo, float >(
											"VSYNC_TO_PHOTON_LATENCY",
											"VistaOpenVRHMDInfoTranscoder",
											"latency in seconds from vsync to photons",
											&VistaOpenVRMeasures::HMDInfo::m_fSecondsFromVSyncToPhotons ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::HMDInfo, float >(
											"DISPLAY_FREQUENCY",
											"VistaOpenVRHMDInfoTranscoder",
											"refresh rate of display",
											&VistaOpenVRMeasures::HMDInfo::m_fDisplayFrequency ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::HMDInfo, float >(
											"IPD",
											"VistaOpenVRHMDInfoTranscoder",
											"inter-pupilary distance",
											&VistaOpenVRMeasures::HMDInfo::m_fUserIPD ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::HMDInfo, float >(
											"HEAD_TO_EYE",
											"VistaOpenVRHMDInfoTranscoder",
											"distance from head to eyes",
											&VistaOpenVRMeasures::HMDInfo::m_fUserHeadToEyeDepth ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::HMDInfo, VistaTransformMatrix >(
											"CAMERA_TO_HEAD",
											"VistaOpenVRHMDInfoTranscoder",
											"transform from camera to head",
											&VistaOpenVRMeasures::HMDInfo::m_matCameraToHeadTransform ),
	};

	class VistaOpenVRControllerInfoTranscoder : public VistaOpenVRBodyInfoTranscoder
	{
	public:
		VistaOpenVRControllerInfoTranscoder() : VistaOpenVRBodyInfoTranscoder() {}
		static std::string GetTypeString() { return "VistaOpenVRControllerInfoTranscoder"; }
		REFL_INLINEIMP( VistaOpenVRControllerInfoTranscoder, VistaOpenVRBodyInfoTranscoder );
	};
	template< int nValue >
	class VistaOpenVRControllerHandGet : public IVistaMeasureTranscode::TTranscodeValueGet< bool >
	{
	public:
		VistaOpenVRControllerHandGet( const std::string& sName, const std::string& sDesc )
		: IVistaMeasureTranscode::TTranscodeValueGet< bool >( sName, "VistaOpenVRControllerInfoTranscoder", sDesc)
		{
		}
		virtual bool GetValue( const VistaSensorMeasure* pMeasure ) const
		{
			bool bRes;
			GetValue( pMeasure, bRes );
			return bRes;
		}
		virtual bool GetValue( const VistaSensorMeasure* pMeasure, bool& bRes ) const
		{
			const VistaOpenVRMeasures::ControllerInfo* pRealMeasure = pMeasure->getRead< VistaOpenVRMeasures::ControllerInfo >();
			bRes = ( pRealMeasure->m_eControllerHand == nValue );
			return true;
		}
	};
	template< int nValue >
	class VistaOpenVRControllerAxisTypeGet : public IVistaMeasureTranscode::TTranscodeIndexedGet< bool >
	{
	public:
		VistaOpenVRControllerAxisTypeGet( const std::string& sName, const std::string& sDesc )
		: IVistaMeasureTranscode::TTranscodeIndexedGet< bool >( sName, "VistaOpenVRControllerInfoTranscoder", sDesc)
		{
		}
		bool GetValueIndexed( const VistaSensorMeasure* pMeasure, bool& bResult, unsigned int nIndex ) const
		{
			const VistaOpenVRMeasures::ControllerInfo* pRealMeasure = pMeasure->getRead< VistaOpenVRMeasures::ControllerInfo >();
			if( (int)nIndex >= pRealMeasure->m_nNumAxes )
				return false;
			bResult = ( pRealMeasure->m_eAxisTypes[ nIndex ] == nValue );
			return true;
		}
	};
	IVistaPropertyGetFunctor* s_aOpenVRControllerInfoGetter[] =
	{
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::ControllerInfo, int >(
											"NUM_BUTTONS",
											"VistaOpenVRHMDInfoTranscoder",
											"number of buttons on controller",
											&VistaOpenVRMeasures::ControllerInfo::m_nNumButtons ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::ControllerInfo, int >(
											"NUM_AXES",
											"VistaOpenVRHMDInfoTranscoder",
											"number of axes on controller",
											&VistaOpenVRMeasures::ControllerInfo::m_nNumAxes ),
		new VistaOpenVRControllerAxisTypeGet< VistaOpenVRMeasures::ControllerInfo::AT_ANALOG_TRIGGER >(
											"AXIS_IS_ANALOG_TRIGGER", "true if controller's axis is an analog trigger" ),
		new VistaOpenVRControllerAxisTypeGet< VistaOpenVRMeasures::ControllerInfo::AT_JOYSTICK >(
											"AXIS_IS_JOYSTICK", "true if controller's axis is a joystick" ),
		new VistaOpenVRControllerAxisTypeGet< VistaOpenVRMeasures::ControllerInfo::AT_TRACKPAD >(
											"AXIS_IS_TRACKPAD", "true if controller's axis is a trackpad" ),
		new IVistaMeasureTranscode::TTranscodeMemberGet< VistaOpenVRMeasures::ControllerInfo, int >(
											"CONTROLLER_HAND",
											"VistaOpenVRHMDInfoTranscoder",
											"hand assigned to controller (0=left, 1=right, -1=unknown)",
											&VistaOpenVRMeasures::ControllerInfo::m_nNumButtons ),
		new VistaOpenVRControllerHandGet< VistaOpenVRMeasures::ControllerInfo::CH_LEFT >(
											"HAND_IS_LEFT", "true if controller's assigned hand is left" ),
		new VistaOpenVRControllerHandGet< VistaOpenVRMeasures::ControllerInfo::CH_RIGHT >(
											"HAND_IS_RIGHT", "true if controller's assigned hand is right" ),
		new VistaOpenVRControllerHandGet< VistaOpenVRMeasures::ControllerInfo::CH_UNKNOWN >(
											"HAND_IS_UNASSIGNED", "true if controller's assigned hand is unassigned" ),
	};

	class VistaOpenVRDriverTranscodeFactoryFactory : public IVistaTranscoderFactoryFactory
	{
	public:
		typedef std::map<std::string,ICreateTranscoder*> CreatorsMap;

		VistaOpenVRDriverTranscodeFactoryFactory()
		{
			CreateCreators( m_mapCreators );
		}
		~VistaOpenVRDriverTranscodeFactoryFactory()
		{
			CleanupCreators( m_mapCreators );
		}		

		static void CreateCreators( CreatorsMap& m_mapCreators )
		{
			m_mapCreators[ "BODY" ]  = new TCreateTranscoder< VistaOpenVRBodyTranscoder >();
			m_mapCreators[ "HMD" ]  = new TCreateTranscoder< VistaOpenVRHMDTranscoder >();
			m_mapCreators[ "CONTROLLER" ]  = new TCreateTranscoder< VistaOpenVRControllerTranscoder >();

			m_mapCreators[ "BODY_INFO" ]  = new TCreateTranscoder< VistaOpenVRBodyInfoTranscoder >();
			m_mapCreators[ "HMD_INFO" ]  = new TCreateTranscoder< VistaOpenVRHMDInfoTranscoder >();
			m_mapCreators[ "CONTROLLER_INFO" ]  = new TCreateTranscoder< VistaOpenVRControllerInfoTranscoder >();
		}
		static void CleanupCreators( CreatorsMap& m_mapCreators )
		{
			for( CreatorsMap::iterator it = m_mapCreators.begin(); it != m_mapCreators.end(); ++it )
				delete (*it).second;

			m_mapCreators.clear();
		}

		virtual IVistaMeasureTranscoderFactory* CreateFactoryForType( const std::string& strTypeName )
		{
			CreatorsMap::const_iterator it = m_mapCreators.find( strTypeName );
			if( it == m_mapCreators.end() )
				return NULL;
			return (*it).second->Create();
		}


		virtual void DestroyTranscoderFactory( IVistaMeasureTranscoderFactory* pFactory )
		{
			delete pFactory;
		}

		static void OnUnload()
		{
			CreatorsMap mapCreators;
			CreateCreators( mapCreators );
			for( CreatorsMap::iterator itCreator = mapCreators.begin(); itCreator != mapCreators.end(); ++itCreator )
				(*itCreator).second->OnUnload();
			CleanupCreators( mapCreators );
		}
	private:
		CreatorsMap m_mapCreators;
	};
}

#ifdef VISTAOPENVRTRANSCODER_EXPORTS
DEFTRANSCODERPLUG_FUNC_EXPORTS( VistaOpenVRDriverTranscodeFactoryFactory )
#else
DEFTRANSCODERPLUG_FUNC_IMPORTS( VistaOpenVRDriverTranscodeFactoryFactory )
#endif

DEFTRANSCODERPLUG_CLEANUP;
IMPTRANSCODERPLUG_CLEANUP(VistaOpenVRDriverTranscodeFactoryFactory)
