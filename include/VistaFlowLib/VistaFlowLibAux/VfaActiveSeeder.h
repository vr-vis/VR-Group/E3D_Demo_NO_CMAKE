/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _IVFAACTIVESEEDER_H
#define _IVFAACTIVESEEDER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaSeeder;
class VfaApplicationContextObject;
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI IVfaActiveSeeder
{
public:
	/**
	* The ActiveSeeder owns a IVfaSeeder and should trigger it continuously while active.
	* This baseclass contains only the ownership and activation mechanisms,
	* but not the control flow for the continuous seeding itself. 
	* This is handled by inherited classes, which also specify which time frame
	* is used.
	*
	* @see VistaFlowLibAux/IVfaSeeder.h
	*/
	IVfaActiveSeeder ();

	virtual ~IVfaActiveSeeder();

	void SetSeeder( IVfaSeeder * pSeeder );
	IVfaSeeder * GetSeeder();

	/*
	* Only an active seeder should seed.
	*
	*/
	void SetIsActive (bool bActive);
	bool GetIsActive () const;

	void Activate ();
	void Deactivate();

private:
	IVfaSeeder*		m_pSeeder;

	bool			m_bIsActive;
};

/*============================================================================*/
/*					                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaActiveRealTimeSeeder : public IVfaActiveSeeder, public VflObserver
{
public:
	/**
	* The ActiveRealTimeSeeder triggers the IVfaSeeder every m_dSeedingInterval time units.
	* The time information of the application context object is used, therefore a time unit is
	* typically a second in real time (or user time)
	*
	*/
	VfaActiveRealTimeSeeder (VfaApplicationContextObject *pAppContext);
	virtual ~VfaActiveRealTimeSeeder();

	/*
	* Seeding interval typically in seconds (i.e., the time unit of the application context object).
	* A value <=0 results in a really continuous seeding (with the temporal resolution of the application context object).
	*/
	void SetSeedingInterval (double dInterval);
	double GetSeedingInterval () const;

	// Observer interface
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

private:
	VfaApplicationContextObject *m_pAppContext;
	double		m_dLastTimeSeeding;
	double		m_dSeedingInterval;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _IVFAACTIVESEEDER_H */
