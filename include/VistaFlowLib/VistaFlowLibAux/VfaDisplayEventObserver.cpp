/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VfaDisplayEventObserver.h" 

#include <VistaKernel/DisplayManager/VistaViewport.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <cassert>

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaDisplayEventObserver::VfaDisplayEventObserver( VistaViewport* pViewport,
	VflRenderNode* pRenderNode )
:	m_pViewport( pViewport )
,	m_pRenderNode( pRenderNode )
{
	assert( m_pViewport && m_pRenderNode && "Both are required to be valid!" );
	m_pViewport->GetViewportProperties()->AttachObserver( this );
}

VfaDisplayEventObserver::~VfaDisplayEventObserver()
{
	m_pViewport->GetViewportProperties()->DetachObserver( this );
}


VfaDisplayEventObserver::VfaDisplayEventObserver(const VfaDisplayEventObserver& )
{ /* Intentionally left empty. */ }

VfaDisplayEventObserver& VfaDisplayEventObserver::operator=( const VfaDisplayEventObserver& )
{
	/* Intentionally left empty. */
	return *this;
}



void VfaDisplayEventObserver::ObserverUpdate( IVistaObserveable* pObs, int nMsg, int nTicket )
{
	// Only want to handle size changes.
	if( nMsg != VistaViewport::VistaViewportProperties::MSG_SIZE_CHANGE )
		return;

	int nVPWidth = -1;
	int nVPHeight = -1;
	m_pViewport->GetViewportProperties()->GetSize( nVPWidth, nVPHeight );
	
	m_pRenderNode->UpdateVtkWindowSize( nVPWidth, nVPHeight );
}


/*============================================================================*/
/* END OF FILE	                                                              */
/*============================================================================*/

