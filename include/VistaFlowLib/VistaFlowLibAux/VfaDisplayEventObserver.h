/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFADISPLAYEVENTOBSERVER_H
#define _VFADISPLAYEVENTOBSERVER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaFlowLib/Data/VflObserver.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VistaViewport;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This class will attach to the properties of a VistaViewport and observe them
 * for viewport size changes. In case a size change has been observed, the new
 * viewport size will be propagated to a RenderNode which can subsequently
 * forward the change to its vtkRenderer.
 */
class VISTAFLOWLIBAUXAPI VfaDisplayEventObserver : public VflObserver
{
public:
	/**
	 * @param pViewport The viewport whose properties to observe for size changes.
	 * @param pRenderNode The target to which size changes are propagated.
	 */
	VfaDisplayEventObserver( VistaViewport* pViewport, VflRenderNode* pRenderNode );
	virtual ~VfaDisplayEventObserver();

	virtual void ObserverUpdate( IVistaObserveable* pObs, int nMsg, int nTicket );

private:
	VfaDisplayEventObserver( const VfaDisplayEventObserver& );
	VfaDisplayEventObserver& operator=( const VfaDisplayEventObserver& );
	
	VistaViewport*	m_pViewport;
	VflRenderNode*	m_pRenderNode;	
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

