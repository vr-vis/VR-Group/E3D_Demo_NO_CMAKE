/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACYLINDERHANDLE_H
#define _VFACYLINDERHANDLE_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VfaControlHandle.h"
#include "VfaCylinderVis.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VistaVector3D;
class VflRenderNode;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Sphere visualization which can be highlighted and tell in this way
 * that it can be grabbed and further moved.
 */
class VISTAFLOWLIBAUXAPI VfaCylinderHandle : public IVfaCenterControlHandle
{
public: 
	VfaCylinderHandle(VflRenderNode *pRN);
	virtual ~VfaCylinderHandle();

	//redefinition in order to get the center to the sphere vis
	void SetCenter(const VistaVector3D &v3Center);
	
	//! sets normal AND highlight radius
	void SetRadius(float f);
	float GetRadius() const;

	//! sets normal radius
	void SetNormalRadius(float f);
	float GetNormalRadius() const;
	
	//! sets highlighted radius
	void SetHighlightRadius(float f);
	float GetHighlightRadius() const;


	/**
	 *  Reimplementation of Set/Get Visible/Enable from IVfaControlHandle/IVfaCenterControlHandle
	 */
	virtual void SetVisible(bool b);
	virtual bool GetVisible() const;

	virtual void SetEnable(bool b);
	//virtual bool GetEnable();       // using IVfaCenterControlHandle::GetEnable()

	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);

	void SetNormalColor(const VistaColor& color);
	VistaColor GetNormalColor() const;
	void SetNormalColor(float fColor[4]);
	void GetNormalColor(float fColor[4]) const;

	void SetHighlightColor(const VistaColor& color);
	VistaColor GetHighlightColor() const;
	void SetHighlightColor(float fColor[4]);
	void GetHighlightColor(float fColor[4]) const;

	void SetIsHighlighted(bool b);
	bool GetIsHighlighted() const;

	VfaCylinderVis* GetCylinderVis() const;

private:
	float m_fNormalColor[4];
	float m_fHighlightColor[4];

	float m_fNormalRadius;
	float m_fHighlightRadius;

	VfaCylinderVis *m_pCylinderVis;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASPHEREHANDLE_H
