/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFALUTDISPLAYVIEW_H
#define _VFALUTDISPLAYVIEW_H

/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include <VistaFlowLib/Visualization/VflRenderable.h>


/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/
class VfaLUTDisplayModel;
class VflRenderNode;
class Vfl3DTextLabel;


/*============================================================================*/
/* CLASS DEFINITION	                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaLUTDisplayView : public IVflRenderable
{
public:
	VfaLUTDisplayView(VfaLUTDisplayModel* pModel);
	virtual ~VfaLUTDisplayView();

	bool GetOrientedBounds(VistaVector3D& v3Origin, VistaVector3D& v3Right,
		VistaVector3D& v3Up) const;

	class VISTAFLOWLIBAUXAPI VfaLUTDisplayProps : public VflRenderableProperties
	{
	public:
		enum EMessages
		{
			MSG_DRAW_ORI_CHANGED = VflRenderableProperties::MSG_LAST,
			MSG_FOLLOW_MODE_CHANGED,
			MSG_SHOW_CURRENT_VALUE_MARKER_FLAG_CHANGED,
			MSG_LENGTH_CHANGED,
			MSG_WIDTH_CHANGED,
			MSG_NUMBER_OF_DECIMAL_DIGITS_CHANGED,
			MSG_FRAME_COLOR_CHANGED,
			MSG_SHOW_FRAME_FLAG_CHANGED,
			MSG_LAST
		};

		enum EDrawOrientation
		{
			DO_VERTICAL = 0,
			DO_HORIZONTAL,
			DO_LAST
		};

		enum EFollowMode
		{
			FM_FOLLOW_NONE = 0,
			FM_FOLLOW_VIEW_POS,
			FM_FOLLOW_VIEW_ORI,
			FM_LAST
		};

		VfaLUTDisplayProps();
		virtual ~VfaLUTDisplayProps();

		bool SetDrawOrientation(const EDrawOrientation eDrawOri);
		EDrawOrientation GetDrawOrientation() const;

		bool SetFollowMode(const EFollowMode eFollowMode);
		EFollowMode GetFollowMode() const;

		void SetShowCurrentValueMarker(const bool bShow);
		bool GetShowCurrentValueMarker() const;

		bool SetLength(const float fLength);
		float GetLength() const;

		bool SetWidth(const float fWidth);
		float GetWidth() const;

		void SetNumberOfDecimalDigits(const unsigned int uiNumDigits);
		unsigned int GetNumberOfDecimalDigits() const;

		void SetFrameColor(const VistaColor& colFrame);
		VistaColor GetFrameColor() const;

		void SetShowFrame(const bool bShow);
		bool GetShowFrame() const;

	private:
		EDrawOrientation m_eDrawOri;
		EFollowMode m_eFollowMode;
		bool m_bShowCurrentValueMarker;
		float m_fLength;
		float m_fWidth;
		unsigned int m_uiNumDecimalDigits;
		VistaColor m_colFrame;
		bool m_bShowFrame;
	};

	// *** IVflRenderable interface. ***
	virtual void Update();
	virtual void DrawOpaque();
	virtual unsigned int GetRegistrationMode() const;
	virtual VfaLUTDisplayProps* GetProperties() const;

	Vfl3DTextLabel* GetMinTextLabel() const;
	Vfl3DTextLabel* GetMaxTextLabel() const;
	Vfl3DTextLabel* GetActiveValueTextLabel() const;

	// *** IVistaObserver interface. ***
	virtual void ObserverUpdate(IVistaObserveable *pObs, int iMsg, int iTicket);

	
protected:
	// *** IVflRenderable interface. ***
	virtual VfaLUTDisplayProps* CreateProperties() const;

private:
	VflRenderNode* m_pCurrentRenderNode;
	VfaLUTDisplayModel* m_pModel;
	Vfl3DTextLabel* m_pMinLabel;
	Vfl3DTextLabel* m_pMaxLabel;
	Vfl3DTextLabel* m_pActiveValueLabel;
	float m_fActiveDataValueParam;
};


#endif // Include guard.

/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
