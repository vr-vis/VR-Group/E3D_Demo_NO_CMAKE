/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaLUTDisplayController.h"
#include "VfaLUTDisplayModel.h"
#include "VfaLUTDisplayView.h"
#include "../VfaRaycastPlaneHandle.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <cassert>


/*============================================================================*/
/* IMPLEMENTATION			                                                  */
/*============================================================================*/
VfaLUTDisplayController::VfaLUTDisplayController(VflRenderNode* pRenderNode,
	VfaLUTDisplayModel* pModel, VfaLUTDisplayView* pView)
:	IVfaWidgetController(pRenderNode)
,	m_pModel(pModel)
,	m_pView(pView)
,	m_pMainHandle(NULL)
,	m_bIsEnabled(true)
,	m_iInteractionSlot(0) // FlowLib's standard "select" slot.
,	m_bIsDragging(false)
{
	assert(m_pModel && m_pView);
	m_pModel->AttachObserver(this, TICKET_MODEL);
	m_pView->GetProperties()->AttachObserver(this, TICKET_VIEW_PROPS);

	m_pMainHandle = new VfaRaycastPlaneHandle(pRenderNode->GetTransformable());
	AddControlHandle(m_pMainHandle);
	UpdateHandle();
}

VfaLUTDisplayController::~VfaLUTDisplayController()
{
	m_pView->GetProperties()->DetachObserver(this);
	m_pModel->DetachObserver(this);
	delete m_pMainHandle;
}


void VfaLUTDisplayController::SetIsEnabled(bool bIsEnabled)
{
	m_bIsEnabled = bIsEnabled;
	m_pMainHandle->SetEnable(m_bIsEnabled);
}

bool VfaLUTDisplayController::GetIsEnabled() const
{
	return m_bIsEnabled;
}


void VfaLUTDisplayController::OnSensorSlotUpdate(int iSlot,
	const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	m_matCurrentGrabFrame.SetTransform(
		oFrameData.qOrientation,
		oFrameData.v3Position
		);
	UpdateDragging();

	// @todo This might not be the right place for this as OnSensorSlotUpdates
	//		 are not guranteed to happen in a regular fashion.
	m_pView->GetProperties()->SetShowFrame(
		m_bIsDragging || GetControllerState() == CS_FOCUS);
}

void VfaLUTDisplayController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	if(bSet && GetControllerState() == CS_FOCUS)
	{
		m_bIsDragging = true;
		InitDragging();
	}
	else
	{
		m_bIsDragging = false;
	}
}


int VfaLUTDisplayController::GetSensorMask() const
{
	return (1 << VfaApplicationContextObject::SLOT_POINTER_VIS);
}

int VfaLUTDisplayController::GetCommandMask() const
{
	return (1 << m_iInteractionSlot);
}


void VfaLUTDisplayController::ObserverUpdate(IVistaObserveable* pObs, int iMsg,
	int iTicket)
{
	if( iTicket == TICKET_MODEL )
	{
		if(iMsg == VfaLUTDisplayModel::MSG_POSITION_CHANGE ||
		   iMsg == VfaLUTDisplayModel::MSG_ORIENTATION_CHANGE)
		{
			UpdateHandle();
		}
	}
	else if(iTicket == TICKET_VIEW_PROPS)
	{
		if(iMsg == VfaLUTDisplayView::VfaLUTDisplayProps::MSG_LENGTH_CHANGED ||
		   iMsg == VfaLUTDisplayView::VfaLUTDisplayProps::MSG_WIDTH_CHANGED)
		{
			UpdateHandle();
		}
	}
}

void VfaLUTDisplayController::UpdateHandle()
{
	VistaVector3D v3Origin, v3Right, v3Up;
	m_pView->GetOrientedBounds(v3Origin, v3Right, v3Up);
	
	m_pMainHandle->UpdateExtents(v3Right, v3Up, v3Origin);
}

void VfaLUTDisplayController::InitDragging()
{
	// Store the position of the widget relative to the dragging input device.
	VistaTransformMatrix matInvGrabFrame = m_matCurrentGrabFrame.GetInverted();
	m_v3GrabPos = matInvGrabFrame.TransformPoint(
		m_pModel->GetPosition()
		);
	m_qGrabOri = matInvGrabFrame.Transform(
		m_pModel->GetOrientation()
		);
}

void VfaLUTDisplayController::UpdateDragging()
{
	if(!m_bIsDragging)
		return;
	// Re-store the position and orientation of the widget relative to the
	// dragging widget to the coordinate frame of the owning render node.
	m_pModel->SetPosition(
		m_matCurrentGrabFrame.TransformPoint(m_v3GrabPos)
		);
	m_pModel->SetOrientation(
		m_matCurrentGrabFrame.Transform(m_qGrabOri)
		);
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
