/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include <GL/glew.h>

#include "VfaLUTDisplayView.h"
#include "VfaLUTDisplayModel.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflLookupTexture.h>

#include <cmath>
#include <cassert>

using namespace std;


/*============================================================================*/
/* ANONYMOUS NAMESPACE														  */
/*============================================================================*/
namespace
{
	const float g_fBorderOffsetFactor = 1.25f;
	const float g_fRelativeFrameWidth = 0.15f;

	// The supplied string must be a valid decimal number (with or without '.').
	const string EnforceNumberOfDigits(
		const string& strTarget,
		unsigned int uiNumDigits)
	{
		const size_t uiDotPos = strTarget.find('.');
		// '.' was found.
		if(uiDotPos != string::npos)
		{
			return strTarget.substr(0,
				uiDotPos + static_cast<size_t>(uiNumDigits) + 1);
		}
		// '.' was not found.
		else
		{
			string strResult = strTarget + '.';
			for(unsigned int i=0; i<uiNumDigits; ++i)
				strResult += '0';
			return strResult;
		}
	}

	const float CalculateActiveDataValueParam(
		const float fActiveDataValue,
		const float fMinDataValue,
		const float fMaxDataValue)
	{
		float fRelativePosition = 0.5f;
		if(fMaxDataValue - fMinDataValue != 0)
		{
			fRelativePosition =
				(fActiveDataValue - fMinDataValue) /
				(fMaxDataValue - fMinDataValue);
			fRelativePosition = max(0.0f, min(fRelativePosition, 1.0f));
		}
		return fRelativePosition;
	}
}




/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
VfaLUTDisplayView::VfaLUTDisplayView(VfaLUTDisplayModel* pModel)
:	m_pCurrentRenderNode(NULL)
,	m_pModel(pModel)
,	m_pMinLabel(new Vfl3DTextLabel())
,	m_pMaxLabel(new Vfl3DTextLabel())
,	m_pActiveValueLabel(new Vfl3DTextLabel())
,	m_fActiveDataValueParam(0.5f)
{
	assert(m_pModel);
	m_pModel->AttachObserver(this);

	m_pMinLabel->Init();
	m_pMinLabel->SetTextSize(0.03f);
	m_pMinLabel->SetTextFollowViewDir(false);
	m_pMinLabel->GetProperties()->SetUseLighting(false);

	m_pMaxLabel->Init();
	m_pMaxLabel->SetTextSize(0.03f);
	m_pMaxLabel->SetTextFollowViewDir(false);
	m_pMaxLabel->GetProperties()->SetUseLighting(false);

	m_pActiveValueLabel->Init();
	m_pActiveValueLabel->SetTextSize(0.03f);
	m_pActiveValueLabel->SetTextFollowViewDir(false);
	m_pActiveValueLabel->GetProperties()->SetUseLighting(false);
}

VfaLUTDisplayView::~VfaLUTDisplayView()
{
	m_pModel->DetachObserver(this);

	if(m_pCurrentRenderNode)
	{
		m_pCurrentRenderNode->RemoveRenderable(m_pActiveValueLabel);
		m_pCurrentRenderNode->RemoveRenderable(m_pMaxLabel);
		m_pCurrentRenderNode->RemoveRenderable(m_pMinLabel);
	}

	delete m_pActiveValueLabel;
	delete m_pMaxLabel;
	delete m_pMinLabel;
}


bool VfaLUTDisplayView::GetOrientedBounds( VistaVector3D& v3Origin,
	VistaVector3D& v3Right, VistaVector3D& v3Up ) const
{
	const VistaVector3D v3Pos = m_pModel->GetPosition();
	const VistaQuaternion qOri = m_pModel->GetOrientation();
	const VistaTransformMatrix matTrans(qOri, v3Pos);

	VfaLUTDisplayProps* const pProps = GetProperties();

	// Didn't init the display view?
	if(!pProps)
		return false;

	const float fLength = pProps->GetLength();
	const float fWidth = pProps->GetWidth();
	
	VistaVector3D v3RightVec;
	VistaVector3D v3UpVec;

	switch(pProps->GetDrawOrientation())
	{
	case VfaLUTDisplayProps::DO_HORIZONTAL:
		v3RightVec = VistaVector3D(fLength, 0.0f, 0.0f, 0.0f);
		v3UpVec = VistaVector3D(0.0f, fWidth, 0.0f, 0.0f);
		break;
	case VfaLUTDisplayProps::DO_VERTICAL:
		v3RightVec = VistaVector3D(fWidth, 0.0f, 0.0f, 0.0f);
		v3UpVec = VistaVector3D(0.0f, fLength, 0.0f, 0.0f);
		break;
	default:
		assert(false && "There should be no 'unknown' draw orientation.");
		break;
	}

	v3Right = matTrans.TransformVector(v3RightVec);
	v3Up = matTrans.TransformVector(v3UpVec);
	v3Origin = matTrans.TransformPoint(VistaVector3D(0.0f, 0.0f, 0.0f))
		- 0.5f * (v3Right + v3Up);
	return true;
}


void VfaLUTDisplayView::Update()
{
	// This is necessary as there is no virtual interface available to
	// propagate changes in the render node.
	if(GetRenderNode() != m_pCurrentRenderNode)
	{
		if(m_pCurrentRenderNode)
		{
			m_pCurrentRenderNode->RemoveRenderable(m_pActiveValueLabel);
			m_pCurrentRenderNode->RemoveRenderable(m_pMaxLabel);
			m_pCurrentRenderNode->RemoveRenderable(m_pMinLabel);
		}
		m_pCurrentRenderNode = GetRenderNode();
		if(m_pCurrentRenderNode)
		{
			m_pCurrentRenderNode->AddRenderable(m_pActiveValueLabel);
			m_pCurrentRenderNode->AddRenderable(m_pMaxLabel);
			m_pCurrentRenderNode->AddRenderable(m_pMinLabel);
		}
	}

	m_pMinLabel->SetVisible( GetVisible() );
	m_pMaxLabel->SetVisible( GetVisible() );	
	// Deactivate active value label if the value marker is not to be shown.
	if(!GetProperties()->GetShowCurrentValueMarker())
		m_pActiveValueLabel->SetVisible(false);
	else
		m_pActiveValueLabel->SetVisible( GetVisible() );

	// Position and orientation updates.
	const float fHalfLength = 0.5f * GetProperties()->GetLength();
	const float fHalfWidth = 0.5f * GetProperties()->GetWidth();
	const VistaVector3D v3Pos = m_pModel->GetPosition();
	const VistaQuaternion qOri = m_pModel->GetOrientation();
	const VistaTransformMatrix matWidgetFrame(qOri, v3Pos);
	
	switch(GetProperties()->GetDrawOrientation())
	{
	case VfaLUTDisplayView::VfaLUTDisplayProps::DO_HORIZONTAL:
		{
			VistaVector3D v3Origin, v3Right, v3Up, v3TextPos;			
			
			m_pMinLabel->SetOrientation(qOri);
			m_pMinLabel->GetOrientedBounds(v3Origin, v3Right, v3Up);
			v3TextPos = matWidgetFrame.TransformPoint(
				VistaVector3D(-fHalfLength, ::g_fBorderOffsetFactor * fHalfWidth, 0.0f))
				- 0.5f * v3Right;
			m_pMinLabel->SetPositionExact(v3TextPos);			
	
			m_pMaxLabel->SetOrientation(qOri);
			m_pMaxLabel->GetOrientedBounds(v3Origin, v3Right, v3Up);
			v3TextPos = matWidgetFrame.TransformPoint(
				VistaVector3D(fHalfLength, ::g_fBorderOffsetFactor * fHalfWidth, 0.0f))
				- 0.5f * v3Right;
			m_pMaxLabel->SetPositionExact(v3TextPos);			

			// *** Draw active value marker. ***
			if(GetProperties()->GetShowCurrentValueMarker())
			{
				m_pActiveValueLabel->SetOrientation(qOri);
				m_pActiveValueLabel->GetOrientedBounds(v3Origin, v3Right, v3Up);
				v3TextPos = matWidgetFrame.TransformPoint(
					VistaVector3D(-fHalfLength, -::g_fBorderOffsetFactor * fHalfWidth, 0.0f) +
					VistaVector3D(2.0f * fHalfLength * m_fActiveDataValueParam, 0.0f, 0.0f))
					- 0.5f * v3Right - v3Up;
				m_pActiveValueLabel->SetPositionExact(v3TextPos);				
			}
		}
		break;
	case VfaLUTDisplayView::VfaLUTDisplayProps::DO_VERTICAL:
		{
			VistaVector3D v3Origin, v3Right, v3Up, v3TextPos;

			m_pMaxLabel->SetOrientation(qOri);
			m_pMinLabel->GetOrientedBounds(v3Origin, v3Right, v3Up);
			v3TextPos = matWidgetFrame.TransformPoint(
				VistaVector3D(-::g_fBorderOffsetFactor * fHalfWidth, -fHalfLength, 0.0f))
				- v3Right - 0.5f * v3Up;
			m_pMinLabel->SetPositionExact(v3TextPos);
			m_pMinLabel->SetOrientation(qOri);

			m_pMaxLabel->SetOrientation(qOri);
			m_pMaxLabel->GetOrientedBounds(v3Origin, v3Right, v3Up);
			v3TextPos = matWidgetFrame.TransformPoint(
				VistaVector3D(-::g_fBorderOffsetFactor * fHalfWidth, fHalfLength, 0.0f))
				- v3Right - 0.5f * v3Up;
			m_pMaxLabel->SetPositionExact(v3TextPos);
			
			// *** Draw active value marker. ***
			if(GetProperties()->GetShowCurrentValueMarker())
			{
				m_pActiveValueLabel->SetOrientation(qOri);
				m_pActiveValueLabel->GetOrientedBounds(v3Origin, v3Right, v3Up);
				v3TextPos = matWidgetFrame.TransformPoint(
					VistaVector3D(::g_fBorderOffsetFactor * fHalfWidth, -fHalfLength, 0.0f) +
					VistaVector3D(0.0f, 2.0f * fHalfLength * m_fActiveDataValueParam, 0.0f))
					- 0.5f * v3Up;
				m_pActiveValueLabel->SetPositionExact(v3TextPos);			
			}
		}
		break;
	default:
		break;
	}	
}

void VfaLUTDisplayView::DrawOpaque()
{
	if(!m_pModel->GetLookupTexture())
		return;

	glPushAttrib(GL_TEXTURE_BIT // Texture binding.
		| GL_ENABLE_BIT // Lighting, texturing.
		| GL_CURRENT_BIT // Color.
		| GL_LINE_BIT // Line width.
		);

	glEnable(GL_TEXTURE_1D);
	glDisable(GL_LIGHTING);

	glPushMatrix();

	const VistaVector3D v3Pos = m_pModel->GetPosition();
	const VistaQuaternion qOri = m_pModel->GetOrientation();
	glTranslatef(v3Pos[0], v3Pos[1], v3Pos[2]);
	glRotatef(Vista::RadToDeg(acos(qOri[3])*2), qOri[0], qOri[1], qOri[2]);

	const float fHalfLength = 0.5f * GetProperties()->GetLength();
	const float fHalfWidth = 0.5f * GetProperties()->GetWidth();

	m_pModel->GetLookupTexture()->GetLookupTexture()->Bind();
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	switch(GetProperties()->GetDrawOrientation())
	{
	case VfaLUTDisplayView::VfaLUTDisplayProps::DO_HORIZONTAL:
		glBegin(GL_QUADS);
			// Left.
			glTexCoord1f(0.0f); glVertex2f(-fHalfLength,  fHalfWidth);
			glTexCoord1f(0.0f); glVertex2f(-fHalfLength, -fHalfWidth);
			// Right.
			glTexCoord1f(1.0f); glVertex2f( fHalfLength, -fHalfWidth);
			glTexCoord1f(1.0f); glVertex2f( fHalfLength,  fHalfWidth);
		glEnd();
		break;
	case VfaLUTDisplayView::VfaLUTDisplayProps::DO_VERTICAL:
		glBegin(GL_QUADS);
			// Bottom.
			glTexCoord1f(0.0f); glVertex2f(-fHalfWidth, -fHalfLength);
			glTexCoord1f(0.0f); glVertex2f( fHalfWidth, -fHalfLength);
			// Top.
			glTexCoord1f(1.0f); glVertex2f( fHalfWidth,  fHalfLength);
			glTexCoord1f(1.0f); glVertex2f(-fHalfWidth,  fHalfLength);
		glEnd();
		break;
	default:
		break;
	}
	m_pModel->GetLookupTexture()->GetLookupTexture()->Unbind();

	if(GetProperties()->GetShowFrame())
	{
		glColor3fv(&GetProperties()->GetFrameColor()[0]);
		const float fExtension = ::g_fRelativeFrameWidth *
			min(fHalfLength, fHalfWidth);
		switch(GetProperties()->GetDrawOrientation())
		{
		case VfaLUTDisplayView::VfaLUTDisplayProps::DO_HORIZONTAL:
			glBegin(GL_QUADS);
				// Left.
				glVertex3f(-fHalfLength - fExtension,  fHalfWidth + fExtension, -0.001f);
				glVertex3f(-fHalfLength - fExtension, -fHalfWidth - fExtension, -0.001f);
				// Right.
				glVertex3f( fHalfLength + fExtension, -fHalfWidth - fExtension, -0.001f);
				glVertex3f( fHalfLength + fExtension,  fHalfWidth + fExtension, -0.001f);
			glEnd();
			break;
		case VfaLUTDisplayView::VfaLUTDisplayProps::DO_VERTICAL:
			glBegin(GL_QUADS);
				// Bottom.
				glVertex3f(-fHalfWidth - fExtension, -fHalfLength - fExtension, -0.001f);
				glVertex3f( fHalfWidth + fExtension, -fHalfLength - fExtension, -0.001f);
				// Top.
				glVertex3f( fHalfWidth + fExtension,  fHalfLength + fExtension, -0.001f);
				glVertex3f(-fHalfWidth - fExtension,  fHalfLength + fExtension, -0.001f);
			glEnd();
			break;
		default:
			break;
		}
	}

	// *** Draw active value marker. ***
	if(GetProperties()->GetShowCurrentValueMarker())
	{
		glLineWidth(2.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		switch(GetProperties()->GetDrawOrientation())
		{
		case VfaLUTDisplayView::VfaLUTDisplayProps::DO_HORIZONTAL:
			glBegin(GL_LINES);
				glVertex3f((2.0f * m_fActiveDataValueParam - 1.0f) * fHalfLength,  fHalfWidth, 0.001f);
				glVertex3f((2.0f * m_fActiveDataValueParam - 1.0f) * fHalfLength, -fHalfWidth, 0.001f);
			glEnd();
			break;
		case VfaLUTDisplayView::VfaLUTDisplayProps::DO_VERTICAL:
			glBegin(GL_LINES);
				glVertex3f(-fHalfWidth, (2.0f * m_fActiveDataValueParam - 1.0f) * fHalfLength, 0.001f);
				glVertex3f( fHalfWidth, (2.0f * m_fActiveDataValueParam - 1.0f) * fHalfLength, 0.001f);
			glEnd();
			break;
		default:
			break;
		}
	}

	glPopMatrix();
	glPopAttrib();
}

unsigned int VfaLUTDisplayView::GetRegistrationMode() const
{
	return (OLI_UPDATE | OLI_DRAW_OPAQUE);
}

VfaLUTDisplayView::VfaLUTDisplayProps* VfaLUTDisplayView::GetProperties() const
{
	// Static cast is safe, as we can be sure that only this type of properties
	// can be created for a VfaLUTDisplayView object.
	return static_cast<VfaLUTDisplayProps*>(IVflRenderable::GetProperties());
}


void VfaLUTDisplayView::ObserverUpdate(IVistaObserveable *pObs, int iMsg,
	int iTicket)
{
	if(pObs == m_pModel)
	{
		switch(iMsg)
		{
		case VfaLUTDisplayModel::MSG_DATA_SCALAR_RANGE_CHANGED:
			{
				float fMinDataValue, fMaxDataValue;
				m_pModel->GetDataScalarRange(fMinDataValue, fMaxDataValue);

				const unsigned int uiNumDigits =
					GetProperties()->GetNumberOfDecimalDigits();

				m_pMinLabel->SetText(::EnforceNumberOfDigits(
					VistaConversion::ToString(fMinDataValue),
					uiNumDigits) + " " + m_pModel->GetUnitPostfix()
					);
				m_pMaxLabel->SetText(::EnforceNumberOfDigits(
					VistaConversion::ToString(fMaxDataValue),
					uiNumDigits) + " " + m_pModel->GetUnitPostfix()
					);

				const float fActiveValue = m_pModel->GetActiveDataValue();
				m_fActiveDataValueParam = ::CalculateActiveDataValueParam(
					fActiveValue, fMinDataValue, fMaxDataValue);
			}
			break;
		case VfaLUTDisplayModel::MSG_ACTIVE_DATA_VALUE_CHANGED:
			{
				const float fActiveValue = m_pModel->GetActiveDataValue();
				const unsigned int uiNumDigits =
					GetProperties()->GetNumberOfDecimalDigits();

				m_pActiveValueLabel->SetText(::EnforceNumberOfDigits(
					VistaConversion::ToString(fActiveValue),
					uiNumDigits) + " " + m_pModel->GetUnitPostfix()
					);

				float fMinDataValue, fMaxDataValue;
				m_pModel->GetDataScalarRange(fMinDataValue, fMaxDataValue);
				m_fActiveDataValueParam = ::CalculateActiveDataValueParam(
					fActiveValue, fMinDataValue, fMaxDataValue);					
			}
			break;
		default:
			break;
		} // Switch(iMsg).
	} // Is observeable model?
}


VfaLUTDisplayView::VfaLUTDisplayProps* 
	VfaLUTDisplayView::CreateProperties() const
{
	return new VfaLUTDisplayProps();
}

Vfl3DTextLabel* VfaLUTDisplayView::GetMinTextLabel() const
{
	return m_pMinLabel;
}

Vfl3DTextLabel* VfaLUTDisplayView::GetMaxTextLabel() const
{
	return m_pMaxLabel;
}

Vfl3DTextLabel* VfaLUTDisplayView::GetActiveValueTextLabel() const
{
	return m_pActiveValueLabel;
}

//============================================================================//

VfaLUTDisplayView::VfaLUTDisplayProps::VfaLUTDisplayProps()
:	m_eDrawOri(DO_HORIZONTAL)
,	m_eFollowMode(FM_FOLLOW_NONE)
,	m_bShowCurrentValueMarker(true)
,	m_fLength(0.4f)
,	m_fWidth(0.06f)
,	m_uiNumDecimalDigits(1)
,	m_colFrame(VistaColor(VistaColor::VISTA_BLUE))
,	m_bShowFrame(false)
{ }

VfaLUTDisplayView::VfaLUTDisplayProps::~VfaLUTDisplayProps()
{ }


bool VfaLUTDisplayView::VfaLUTDisplayProps::
	SetDrawOrientation(const EDrawOrientation eDrawOri)
{
	if(eDrawOri < DO_LAST && eDrawOri != m_eDrawOri )
	{
		m_eDrawOri = eDrawOri;
		Notify(MSG_DRAW_ORI_CHANGED);
		return true;
	}
	return false;
}

VfaLUTDisplayView::VfaLUTDisplayProps::EDrawOrientation
	VfaLUTDisplayView::VfaLUTDisplayProps::GetDrawOrientation() const
{
	return m_eDrawOri;
}


bool VfaLUTDisplayView::VfaLUTDisplayProps::
	SetFollowMode(EFollowMode eFollowMode)
{
	if(eFollowMode < FM_LAST && eFollowMode != m_eFollowMode )
	{
		m_eFollowMode = eFollowMode;
		Notify(MSG_FOLLOW_MODE_CHANGED);
		return true;
	}
	return false;
}

VfaLUTDisplayView::VfaLUTDisplayProps::EFollowMode
	VfaLUTDisplayView::VfaLUTDisplayProps::GetFollowMode() const
{
	return m_eFollowMode;
}


void VfaLUTDisplayView::VfaLUTDisplayProps::
	SetShowCurrentValueMarker(const bool bShow)
{
	if(bShow != m_bShowCurrentValueMarker)
	{
		m_bShowCurrentValueMarker = bShow;
		Notify(MSG_SHOW_CURRENT_VALUE_MARKER_FLAG_CHANGED);
	}
}

bool VfaLUTDisplayView::VfaLUTDisplayProps::
	GetShowCurrentValueMarker() const
{
	return m_bShowCurrentValueMarker;
}


bool VfaLUTDisplayView::VfaLUTDisplayProps::
	SetLength(const float fLength)
{
	if(fLength >= 0.0f && fLength != m_fLength)
	{
		m_fLength = fLength;
		Notify(MSG_LENGTH_CHANGED);
		return true;
	}
	return false;
}

float VfaLUTDisplayView::VfaLUTDisplayProps::
	GetLength() const
{
	return m_fLength;
}


bool VfaLUTDisplayView::VfaLUTDisplayProps::
	SetWidth(const float fWidth)
{
	if(fWidth >= 0.0f && fWidth != m_fWidth)
	{
		m_fWidth = fWidth;
		Notify(MSG_WIDTH_CHANGED);
		return true;
	}
	return false;
}

float VfaLUTDisplayView::VfaLUTDisplayProps::
	GetWidth() const
{
	return m_fWidth;
}


void VfaLUTDisplayView::VfaLUTDisplayProps::
	SetNumberOfDecimalDigits(const unsigned int uiNumDigits)
{
	if(uiNumDigits != m_uiNumDecimalDigits)
	{
		m_uiNumDecimalDigits = uiNumDigits;
		Notify(MSG_NUMBER_OF_DECIMAL_DIGITS_CHANGED);
	}
}

unsigned int VfaLUTDisplayView::VfaLUTDisplayProps::
	GetNumberOfDecimalDigits() const
{
	return m_uiNumDecimalDigits;
}


void VfaLUTDisplayView::VfaLUTDisplayProps::
	SetFrameColor(const VistaColor& colFrame)
{
	if(colFrame != m_colFrame)
	{
		m_colFrame = colFrame;
		Notify(MSG_FRAME_COLOR_CHANGED);
	}
}

VistaColor VfaLUTDisplayView::VfaLUTDisplayProps::
	GetFrameColor() const
{
	return m_colFrame;
}


void VfaLUTDisplayView::VfaLUTDisplayProps::
	SetShowFrame(const bool bShow)
{
	if(bShow != m_bShowFrame)
	{
		m_bShowFrame = bShow;
		Notify(MSG_SHOW_FRAME_FLAG_CHANGED);
	}
}

bool VfaLUTDisplayView::VfaLUTDisplayProps::
	GetShowFrame() const
{
	return m_bShowFrame;
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
