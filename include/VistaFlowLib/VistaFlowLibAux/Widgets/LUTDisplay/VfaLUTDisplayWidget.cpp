/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VfaLUTDisplayWidget.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>


/*============================================================================*/
/* IMPLEMENTATION			                                                  */
/*============================================================================*/
VfaLUTDisplayWidget::VfaLUTDisplayWidget(VflRenderNode *pRenderNode)
:	IVfaWidget(pRenderNode)
,	m_pModel(NULL)
,	m_pView(NULL)
,	m_pController(NULL)
,	m_bIsEnabled(true)
,	m_bIsVisible(true)
{
	m_pModel = new VfaLUTDisplayModel();

	m_pView = new VfaLUTDisplayView(m_pModel);
	m_pView->Init();
	GetRenderNode()->AddRenderable(m_pView);

	m_pController = new VfaLUTDisplayController(
		GetRenderNode(), m_pModel, m_pView);
}

VfaLUTDisplayWidget::~VfaLUTDisplayWidget()
{
	if(m_pView)
		GetRenderNode()->RemoveRenderable(m_pView);
	
	delete m_pController;
	delete m_pView;
	delete m_pModel;	
}


void VfaLUTDisplayWidget::SetIsActive(bool bIsActive)
{
	SetIsEnabled(bIsActive);
	SetIsVisible(bIsActive);
}

bool VfaLUTDisplayWidget::GetIsActive() const
{
	return GetIsEnabled() && GetIsVisible();
}


void VfaLUTDisplayWidget::SetIsEnabled(bool bIsEnabled)
{
	m_bIsEnabled = bIsEnabled;
	m_pController->SetIsEnabled(m_bIsEnabled);

	// It makes no sense to have an active but invisible widget here.
	if(m_bIsEnabled)
		m_pView->SetVisible(true);
}

bool VfaLUTDisplayWidget::GetIsEnabled() const
{
	return m_bIsEnabled;
}


void VfaLUTDisplayWidget::SetIsVisible(bool bIsVisible)
{
	m_bIsVisible = bIsVisible;
	m_pView->SetVisible(m_bIsVisible);

	// It makes no sense to have an invisible but active widget here.
	if(!m_bIsVisible)
		m_pController->SetIsEnabled(false);
}
bool VfaLUTDisplayWidget::GetIsVisible() const
{
	return m_bIsVisible;
}


VfaLUTDisplayModel* VfaLUTDisplayWidget::GetModel() const
{
	return m_pModel;
}

VfaLUTDisplayView* VfaLUTDisplayWidget::GetView() const
{
	return m_pView;
}

VfaLUTDisplayController* VfaLUTDisplayWidget::GetController() const
{
	return m_pController;
}

/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
