

set( RelativeDir "./Widgets/LUTDisplay" )
set( RelativeSourceGroup "Source Files\\Widgets\\LUTDisplay" )

set( DirFiles
	VfaLUTDisplayController.cpp
	VfaLUTDisplayController.h
	VfaLUTDisplayModel.cpp
	VfaLUTDisplayModel.h
	VfaLUTDisplayView.cpp
	VfaLUTDisplayView.h
	VfaLUTDisplayWidget.cpp
	VfaLUTDisplayWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

