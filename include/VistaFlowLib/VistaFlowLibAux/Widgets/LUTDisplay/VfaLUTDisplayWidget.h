/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFALUTDISPLAYWIDGET_H
#define VFALUTDISPLAYWIDGET_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaWidget.h"

#include "VfaLUTDisplayModel.h"
#include "VfaLUTDisplayView.h"
#include "VfaLUTDisplayController.h"


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 *
 */
class VISTAFLOWLIBAUXAPI VfaLUTDisplayWidget : public IVfaWidget
{
public:
	VfaLUTDisplayWidget(VflRenderNode *pRenderNode);
	virtual ~VfaLUTDisplayWidget();

	// Simultaneously sets enabled & visible to the specified value.
	void SetIsActive(bool bIsActive);
	bool GetIsActive() const;

	void SetIsEnabled(bool bIsEnabled);
	bool GetIsEnabled() const;

	void SetIsVisible(bool bIsVisible);
	bool GetIsVisible() const;

	// *** IVfaWidget interface. ***
	virtual VfaLUTDisplayModel* GetModel() const;
	virtual VfaLUTDisplayView* GetView() const;
	virtual VfaLUTDisplayController* GetController() const;

private:
	VfaLUTDisplayModel*			m_pModel;
	VfaLUTDisplayView*			m_pView;
	VfaLUTDisplayController*	m_pController;

	bool						m_bIsEnabled;
	bool						m_bIsVisible;
};


#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
