/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaLUTDisplayModel.h"


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
VfaLUTDisplayModel::VfaLUTDisplayModel()
:	m_pLUT(NULL)
,	m_fMinDataValue(0)
,	m_fMaxDataValue(0)
,	m_fActiveDataValue(0)
{ }

VfaLUTDisplayModel::~VfaLUTDisplayModel()
{ }


void VfaLUTDisplayModel::SetLookupTexture(VflLookupTexture* pLUT)
{
	if(m_pLUT != pLUT)
	{
		m_pLUT = pLUT;
		Notify(MSG_LUT_CHANGED);
	}
}

VflLookupTexture* VfaLUTDisplayModel::GetLookupTexture() const
{
	return m_pLUT;
}


bool VfaLUTDisplayModel::SetDataScalarRange(const float fMin, const float fMax)
{
	if((fMin != m_fMinDataValue || fMax != m_fMaxDataValue) &&
		fMin <= fMax)
	{
		m_fMinDataValue = fMin;
		m_fMaxDataValue = fMax;
		Notify(MSG_DATA_SCALAR_RANGE_CHANGED);
		return true;
	}
	return false;
}

void VfaLUTDisplayModel::GetDataScalarRange(float& fMin, float& fMax) const
{
	fMin = m_fMinDataValue;
	fMax = m_fMaxDataValue;
}


void VfaLUTDisplayModel::SetActiveDataValue(const float fActiveDataValue)
{
	if(fActiveDataValue != m_fActiveDataValue)
	{
		m_fActiveDataValue = fActiveDataValue;
		Notify(MSG_ACTIVE_DATA_VALUE_CHANGED);
	}
}

float VfaLUTDisplayModel::GetActiveDataValue() const
{
	return m_fActiveDataValue;
}


void VfaLUTDisplayModel::SetUnitPostfix(const std::string& strUnitPostfix)
{
	if(strUnitPostfix != m_strUnitPostfix)
	{
		m_strUnitPostfix = strUnitPostfix;
		Notify(MSG_UNIT_POSTFIX_CHANGED);
	}
}

const std::string& VfaLUTDisplayModel::GetUnitPostfix() const
{
	return m_strUnitPostfix;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
