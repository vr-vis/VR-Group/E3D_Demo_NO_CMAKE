/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFALUTDISPLAYMODEL_H
#define _VFALUTDISPLAYMODEL_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaTransformableWidgetModel.h"

#include <string>


/*============================================================================*/
/* FORWARD DECLARATION                                                        */
/*============================================================================*/
class VflLookupTexture;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaLUTDisplayModel : public VfaTransformableWidgetModel
{
public:
	enum EMessages
	{
		MSG_LUT_CHANGED = VfaWidgetModelBase::MSG_LAST,
		MSG_DATA_SCALAR_RANGE_CHANGED,
		MSG_ACTIVE_DATA_VALUE_CHANGED,
		MSG_UNIT_POSTFIX_CHANGED,
		MSG_LAST		
	};

	VfaLUTDisplayModel();
	virtual ~VfaLUTDisplayModel();

	void SetLookupTexture(VflLookupTexture* pLUT);
	VflLookupTexture* GetLookupTexture() const;

	bool SetDataScalarRange(const float fMin, const float fMax);
	void GetDataScalarRange(float& fMin, float& fMax) const;

	void SetActiveDataValue(const float fActiveDataValue);
	float GetActiveDataValue() const;

	void SetUnitPostfix(const std::string& strUnitPostfix);
	const std::string& GetUnitPostfix() const;

private:
	VflLookupTexture* m_pLUT;
	float m_fMinDataValue;
	float m_fMaxDataValue;
	float m_fActiveDataValue;
	std::string m_strUnitPostfix;
};


#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
