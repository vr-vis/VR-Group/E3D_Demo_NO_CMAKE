/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFALUTDISPLAYCONTROLLER_H
#define _VFALUTDISPLAYCONTROLLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaWidgetController.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Data/VflObserver.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaLUTDisplayModel;
class VfaLUTDisplayView;
class VfaRaycastPlaneHandle;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaLUTDisplayController : public IVfaWidgetController,
												   public VflObserver
{
public:
	VfaLUTDisplayController(VflRenderNode* pRenderNode,
		VfaLUTDisplayModel* pModel, VfaLUTDisplayView* pView);
	virtual ~VfaLUTDisplayController();

	// *** IVfaWidgetController interface. ***
	virtual void SetIsEnabled(bool bIsEnabled);
	virtual bool GetIsEnabled() const;

	// *** IVfaSlotObserver interface. ***
	void OnSensorSlotUpdate(int iSlot,
		const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate(int iSlot, const bool bSet);
	
	int GetSensorMask() const;
	int GetCommandMask() const;

	// *** IVistaObserver interface. ***
	virtual void ObserverUpdate(IVistaObserveable* pObs, int iMsg, int iTicket);
		
protected:
	enum LocalObserverTickets
	{
		TICKET_MODEL = 0,
		TICKET_VIEW_PROPS,
		TICKET_LAST
	};

	void UpdateHandle();
	void InitDragging();
	void UpdateDragging();

private:
	VfaLUTDisplayModel* m_pModel;
	VfaLUTDisplayView* m_pView;

	VfaRaycastPlaneHandle* m_pMainHandle;

	bool m_bIsEnabled;
	int m_iInteractionSlot;
	bool m_bIsDragging;

	VistaVector3D m_v3GrabPos;
	VistaQuaternion m_qGrabOri;
	VistaTransformMatrix m_matCurrentGrabFrame;
};


#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
