/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAARROWVIS_H
#define _VFAARROWVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Arrow is composed of cone and cylinder and can be used for showing direction,  
 * for example the normal of a plane.
 */
class VISTAFLOWLIBAUXAPI VfaArrowVis : public IVflRenderable
{
public: 
	VfaArrowVis();
	virtual ~VfaArrowVis();

	virtual void DrawOpaque();

	virtual unsigned int GetRegistrationMode() const;


	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);


	//order: cylinder radius, cylinder height, cone radius, cone height
	void SetArrowAttributes(float f[4]);
	void GetArrowAttributes(float f[4]) const;

	void  SetCylinderRadius(float fRad);
	float GetCylinderRadius();

	void  SetCylinderHeight(float fHeight);
	float GetCylinderHeight();

	void  SetConeRadius(float fRad);
	float GetConeRadius();

	void  SetConeHeight(float fConeHeight);
	float GetConeHeight();

	void SetCenter(const VistaVector3D &v3Center);
	void GetCenter(VistaVector3D &v3Center) const;
	void SetCenter(float fC[4]);
	void GetCenter(float fC[4]) const;

	void SetRotate(const VistaVector3D &v3Normal);
	void GetRotate(float fRotate[4]) const;

	void SetColor(const VistaColor& color);
	VistaColor GetColor() const;
	
	void SetColor(float fColor[4]);
	void GetColor(float fColor[4]) const;

	bool SetUseLighting(bool b);
	bool GetUseLighting() const;

	//! convenience method to specify the arrow transformation by start and endpoint.
	//! @param autoSize 'true' keeps original radius/width/tip size aspects
	void SetPointFromTo(const VistaVector3D &from, const VistaVector3D &to, bool autoSize = false);

protected:
	virtual VflRenderableProperties* CreateProperties() const;

	GLUquadricObj *quadratic;

private:
	VistaVector3D m_v3Center;
	float m_fRotate[4];
	float m_fColor[4];
	//cylinder and cone properties
	float m_fCylinderRadius;
	float m_fCylinderHeight;
	float m_fConeRadius;
	float m_fConeHeight;

	bool m_bUseLighting;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAARROWVIS_H
