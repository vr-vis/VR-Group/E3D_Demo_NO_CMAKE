/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAPROXIMITYQUADHANDLE_H
#define _VFAPROXIMITYQUADHANDLE_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaProximityHandle.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaQuadVis;
class VistaTexture;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A proximity handle which is touched, whenever the cursor position is inside
 * a given(axis aligned) box.
 */
class VISTAFLOWLIBAUXAPI VfaProximityQuadHandle : public IVfaProximityHandle
{
public:
	VfaProximityQuadHandle(VflRenderNode *pRN);
	virtual ~VfaProximityQuadHandle();

	// redefine highlighting -> we need to highlight the handle's vis in here!
	virtual void SetIsHighlighted(bool b);

	virtual void SetVisEnabled(bool b);

	void SetPoint(float fPt[3]);
	void SetPoint(double dPt[3]);
	void SetPoint(const VistaVector3D& v3Pt);
	
	void GetPoint(float fPt[3]) const;
	void GetPoint(double dPt[3]) const;
	void GetPoint(VistaVector3D& v3Pt) const;

	void SetNormal(float fPt[3]);
	void SetNormal(double dPt[3]);
	void SetNormal(const VistaVector3D& v3Pt);

	void GetNormal(float fPt[3]) const;
	void GetNormal(double fPt[3]) const;
	void GetNormal(VistaVector3D& v3Pt) const;

	/** to avoid set the rotation accordingly standard normal is in z-direction */
	bool SetRotation(const VistaQuaternion &qRotation);
	void GetRotation(VistaQuaternion &qRot) const;

	void SetExtent(float fWidth, float fHeight);
	void GetExtent(float& fWidth, float& fHeight) const;

	void SetHighlightLineWidth(float f);
	float GetHighlightLineWidth() const;

	void SetNormalLineWidth(float f);
	float GetNormalLineWidth() const;

	void SetHighlightColor(const VistaColor& color);
	VistaColor GetHighlightColor() const;
	void SetHighlightColor(float fC[4]);
	void GetHighlightColor(float fC[4]) const;

	void SetNormalColor(const VistaColor& color);
	VistaColor GetNormalColor() const;
	void SetNormalColor(float fC[4]);
	void GetNormalColor(float fC[4]) const;

	void SetLineColor(const VistaColor& color);
	VistaColor GetLineColor() const;
	void SetLineColor(float fC[4]);
	void GetLineColor(float fC[4]) const;

	void SetDrawTransparentHighlight(bool b);
	bool GetDrawTransparentHighlight() const;

	void SetTexture(VistaTexture *pTex);
	VistaTexture *GetTexture() const;

	/**
	* quad will be always drawn filled
	*/
	bool SetFillPlane(bool bFilled);
	bool GetFillPlane();


	/** 
	 * retrieve the corner coordinates in the defining space
	 * using the aforementioned indexing
	 */
	void GetCorner(unsigned char i, float fCorner[3]) const;
	void GetCorner(unsigned char i, VistaVector3D &v3Corner) const;

	virtual bool IsTouched(const VistaVector3D & v3Pos) const;

protected:
	
private:
	VfaQuadVis *m_pVis;

	float m_fNormalLW;
	float m_fHighlightLW;
	float m_fNormalColor[4];
	float m_fHighlightColor[4];
	float m_fLineColor[4];
	bool m_bDrawTransparentHighlight;
	bool m_bFillPlane;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaProximityQuadHandle_H
