/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaSphereHandle.h"
#include "VfaSphereVis.h"
#include "Sphere/VfaSphereModel.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLibAux/Widgets/VfaSphereVis.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaSphereHandle                                      */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaSphereHandle::VfaSphereHandle(VflRenderNode *pRN)
:	IVfaCenterControlHandle(),
	m_pSphereVis(new VfaSphereVis(new VfaSphereModel))
{
	//normal color
	m_fNormalColor[0] = 1.0f;
	m_fNormalColor[1] = 0.0f;
	m_fNormalColor[2] = 0.0f;
	m_fNormalColor[3] = 1.0f;
	
	//color if sphere highlighted
	m_fHighlightColor[0] = 1.0f;
	m_fHighlightColor[1] = 0.75f;
	m_fHighlightColor[2] = 0.75f;
	m_fHighlightColor[3] = 1.0f;

	if(m_pSphereVis->Init())
		pRN->AddRenderable(m_pSphereVis);

	m_pSphereVis->GetProperties()->SetToSolid(true);
	m_pSphereVis->GetProperties()->SetColor(m_fNormalColor);
	m_pSphereVis->GetProperties()->SetUseLighting(true);
}

VfaSphereHandle::~VfaSphereHandle()
{
	m_pSphereVis->GetRenderNode()->RemoveRenderable(m_pSphereVis);
	delete m_pSphereVis->GetModel();
	delete m_pSphereVis;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void VfaSphereHandle::SetCenter(const VistaVector3D &v3Center)
{
	IVfaCenterControlHandle::SetCenter(v3Center);
	m_pSphereVis->GetModel()->SetCenter(v3Center);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaSphereHandle::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaSphereHandle::GetBounds(VistaBoundingBox &)
{
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetVisible                                              */
/*                                                                            */
/*============================================================================*/
void VfaSphereHandle::SetVisible(bool b)
{
	m_bIsVisible = b;
	this->m_pSphereVis->SetVisible(b);

	if(!b)                            // if invisible, then disable this handle
	{
		m_bIsEnable = false;
		SetIsSelectionEnabled(false);
	}
}

bool VfaSphereHandle::GetVisible() const
{
	if( m_bIsVisible != this->m_pSphereVis->GetVisible())
		vstr::warnp() << "[VfaSphereHandle]: unsynchronized state of visibility" << endl;
	
	return this->m_pSphereVis->GetVisible();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetEnable                                                   */
/*                                                                            */
/*============================================================================*/
void VfaSphereHandle::SetEnable(bool b)
{
	IVfaCenterControlHandle::SetEnable(b);
	if(b)							// if enabled, then force visibility
	{
		m_bIsVisible = true;
		this->m_pSphereVis->SetVisible(true);
	}
	else
	{
		m_bIsVisible = false;
		this->m_pSphereVis->SetVisible(false);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRadius                                               */
/*                                                                            */
/*============================================================================*/
void VfaSphereHandle::SetRadius(float f)
{
	m_pSphereVis->GetModel()->SetRadius(f);
	m_fNormalRadius = f;
	m_fHighlightRadius = f;
}

float VfaSphereHandle::GetRadius() const
{
	return m_pSphereVis->GetModel()->GetRadius();
}


void VfaSphereHandle::SetNormalRadius(float f)
{
	m_fNormalRadius = f;	
}
float VfaSphereHandle::GetNormalRadius() const
{
	return m_fNormalRadius;
}

void VfaSphereHandle::SetHighlightRadius(float f)
{
	m_fHighlightRadius = f;
}
float VfaSphereHandle::GetHighlightRadius() const
{
	return m_fHighlightRadius;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNormalColor                                          */
/*                                                                            */
/*============================================================================*/
void VfaSphereHandle::SetNormalColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC, VistaColor::RGBA);
	this->SetNormalColor(fC);
}

VistaColor VfaSphereHandle::GetNormalColor() const
{
	VistaColor color(m_fNormalColor);
	return color;
}
void VfaSphereHandle::SetNormalColor(float fNormalColor[4])
{
	m_fNormalColor[0] = fNormalColor[0];
	m_fNormalColor[1] = fNormalColor[1];
	m_fNormalColor[2] = fNormalColor[2];
	m_fNormalColor[3] = fNormalColor[3];
	m_pSphereVis->GetProperties()->SetColor(m_fNormalColor);
}

void VfaSphereHandle::GetNormalColor(float fNormalColor[4]) const
{
	fNormalColor[0] = m_fNormalColor[0];
	fNormalColor[1] = m_fNormalColor[1];
	fNormalColor[2] = m_fNormalColor[2];
	fNormalColor[3] = m_fNormalColor[3];
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHighlightColor                                       */
/*                                                                            */
/*============================================================================*/
void VfaSphereHandle::SetHighlightColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC, VistaColor::RGBA);
	this->SetHighlightColor(fC);
}

VistaColor VfaSphereHandle::GetHighlightColor() const
{
	VistaColor color(m_fHighlightColor);
	return color;
}

void VfaSphereHandle::SetHighlightColor(float fHighlightColor[4])
{
	m_fHighlightColor[0] = fHighlightColor[0];
	m_fHighlightColor[1] = fHighlightColor[1];
	m_fHighlightColor[2] = fHighlightColor[2];
	m_fHighlightColor[3] = fHighlightColor[3];
}

void VfaSphereHandle::GetHighlightColor(float fHighlightColor[4]) const
{
	fHighlightColor[0] = m_fHighlightColor[0];
	fHighlightColor[1] = m_fHighlightColor[1];
	fHighlightColor[2] = m_fHighlightColor[2];
	fHighlightColor[3] = m_fHighlightColor[3];
}

/*============================================================================*/
/*  NAME      :   Get/SetIsHighlighted		                                  */
/*============================================================================*/
void VfaSphereHandle::SetIsHighlighted(bool b)
{
	IVfaCenterControlHandle::SetIsHighlighted(b);
	
	if(b)
	{
		m_pSphereVis->GetProperties()->SetColor(m_fHighlightColor);
		m_pSphereVis->GetModel()->SetRadius(m_fHighlightRadius);
	}
	else
	{
		m_pSphereVis->GetProperties()->SetColor(m_fNormalColor);
		m_pSphereVis->GetModel()->SetRadius(m_fNormalRadius);
	}
}

bool VfaSphereHandle::GetIsHighlighted() const
{
	return IVfaCenterControlHandle::GetIsHighlighted();
}



/*============================================================================*/
/*  NAME      :   GetSphereVis		                                          */
/*============================================================================*/
VfaSphereVis* VfaSphereHandle::GetSphereVis() const
{
	return m_pSphereVis;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaSphereHandle.cpp"									      */
/*============================================================================*/



