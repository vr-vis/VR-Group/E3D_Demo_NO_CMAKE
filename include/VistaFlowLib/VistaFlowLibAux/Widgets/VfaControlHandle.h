/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFACONTROLHANDLE_H
#define _VFACONTROLHANDLE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VistaFlowLibAuxConfig.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/InteractionManager/VistaIntentionSelect.h>

#include <VistaFlowLib/Visualization/VflRenderable.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VistaReferenceFrame;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Interface class for control handles, i.e. handles which can be touched and
 * focused by all IVfaFocusStrategy strategies. Handles can be highlighted.
 */
class VISTAFLOWLIBAUXAPI IVfaControlHandle
{
public:
	enum eTypes { 
		HANDLE_CENTER = 0,
		HANDLE_PROXIMITY_BOX,
		HANDLE_PROXIMITY_QUAD,
		HANDLE_PROXIMITY_SPHERE,
		HANDLE_FIRST_EXTERNAL
	};
	virtual ~IVfaControlHandle();

	virtual void SetIsHighlighted(bool bIsHighlighted);
	virtual bool GetIsHighlighted() const;

	/**
	 * Attributes "visible" and "enable" of a control handle
	 * logical dependencies: enable -> visible
	 * enable | visible | cond.
	 *   0    |    0    |   1
	 *   0    |    1    |   1
	 *   1    |    0    |   0  (i.e. this condition is invalid!)
	 *   1    |    1    |   1
	 *
	 *  The getters and setters of these attributes automatically adjust
	 *  m_bIsVisible or m_bIsEnable, such that only the valid conditions
	 *  will appears.
	 *  Initial state: enabled and visible.
	 *
	 *  SetEnabled MUST be reimplemented in the subclasses to make sure that
	 *  a special handle is automatically set to visible if the function call
	 *  enables the handle.
	 *
	 *  SetVisible MUST be reimplemented in the subclasses to make sure that
	 *  a special handles is automatically set to inactive if the function call
	 *  hides the handle.
	 */
	virtual void SetVisible(bool bIsVisible);
	virtual bool GetVisible();

	virtual void SetEnable(bool bIsEnabled);
	virtual bool GetEnable();

	int GetType();

protected:
	IVfaControlHandle(int iType);

	bool			m_bIsVisible;
	bool			m_bIsEnable;

private:
	int				m_iType;
	bool			m_bIsHighlighted;
};

////////////////////////////////////////////////////////////////////////////////

/**
 * Interface class for control handles with a center.
 * 
 * They are also a VistaNodeAdaptor for the IntentionSelect mechanism. However,
 * they report as global position a position in vis space, therefore,
 * IntentionSelect must work in vis space, too.
 */
class VISTAFLOWLIBAUXAPI IVfaCenterControlHandle : public IVfaControlHandle,
	public IVistaIntentionSelectAdapter
{
public:
	IVfaCenterControlHandle();
	virtual ~IVfaCenterControlHandle();

	virtual void SetCenter(const VistaVector3D &v3DCenter);
	void GetCenter(VistaVector3D &v3Center) const;
	const VistaVector3D& GetCenter() const;

	// *** IVfaControlHandle interface. ***
	virtual void SetVisible(bool bIsVisible);
	// Getter used from base class, i.e. IVfaControlHandle::GetVisible().

	virtual void SetEnable(bool bIsEnabled);
	virtual bool GetEnable();

	// *** IVistaIntentionSelectAdapter interface. ***
	bool GetPosition(VistaVector3D &pTrans, const VistaReferenceFrame &oReferenceFrame) const;

protected:
	VistaVector3D	m_v3Center;
};


#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
