/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef SLIDER_WIDGET_H
#define SLIDER_WIDGET_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include "VfaSliderModel.h"
#include "VfaSliderVis.h"
#include "VfaSliderController.h"

#include <VistaFlowLibAux/Widgets/VfaWidget.h>
#include <VistaFlowLibAux/Widgets/VfaRulerVis.h>
#include <VistaAspects/VistaReflectionable.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VflRenderNode;

class VfaSliderModel;
class VfaSliderVis;
class VfaSliderController;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaSliderWidget : public IVfaWidget
{
public:

	VfaSliderWidget(VflRenderNode *pRenderNode);
	virtual ~VfaSliderWidget();

	/** 
	 * NOTE : Handle visibility and reaction to events separately.
	 *        Thus the widget may be visible but not being interacted with!
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    

	virtual VfaSliderModel*			GetModel() const;
	virtual VfaSliderVis*			GetView() const;
	virtual VfaSliderController*	GetController() const;

private:
	VfaSliderModel*			m_pModel;
	VfaSliderVis*			m_pVis;
	VfaSliderController*	m_pCtr;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
