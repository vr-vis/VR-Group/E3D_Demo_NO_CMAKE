/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

#ifndef SLIDER_EMODEL_H
#define SLIDER_EMODEL_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <VistaBase/VistaVector3D.h>
#include <VistaBase/VistaQuaternion.h>

#include <VistaFlowLibAux/Widgets/VfaWidgetModelBase.h>
#include <VistaBase/VistaVectorMath.h>

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaSliderModel : public VfaWidgetModelBase
{
public:
	enum{
		MSG_POSITION_CHANGED = VfaWidgetModelBase::MSG_LAST,
		MSG_ORIANTATION_CHANGED,
		MSG_SLIDER_POSITION_CHANGED,
		MSG_MIN_VALUE_CHANGED,
		MSG_MAX_VALUE_CHANGED,
		MSG_VALUE_ACCEPTED,
		MSG_VALUE_REFUSED,
		MSG_LAST
	};

	/**************************************************************************/
	/*  CONSTRUCTORS / DESTRUCTOR                                             */
	/**************************************************************************/
	VfaSliderModel();
	virtual ~VfaSliderModel();

	/**************************************************************************/
	/*  GETTER                                                                */
	/**************************************************************************/
	VistaVector3D	GetPosition()	 const;
	VistaQuaternion	GetOrientation() const;

	float GetOldSliderPos() const;

	float GetSliderPos() const;
	float GetValue()	 const;
	float GetMinValue()	 const;
	float GetMaxValue()	 const;

	bool GetUseLogarithmicScale() const;

	/**************************************************************************/
	/*  SETTER                                                                */
	/**************************************************************************/
	void SetPosition   ( VistaVector3D	 v3Position	  );
	void SetOrientation( VistaQuaternion qOrientation );

	void SetSliderPos( float fValue );
	void SetValue    ( float fValue );
	void SetMinValue ( float fValue );
	void SetMaxValue ( float fValue );

	void SetNumSteps( int   iNumSteps );
	void SetStepSize( float fStepSize );

	void SetUseLogarithmicScale(bool b);

	void AcceptValue();
	void RefuseValue();

	/**************************************************************************/
	/*  REFLECTIONABLE INTERVACE                                              */
	/**************************************************************************/
	virtual std::string GetReflectionableType() const;

protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

private:
	VistaVector3D	m_v3Position;
	VistaQuaternion m_qOrientation;

	float	m_fSliderPos;
	float	m_fOldSliderPos;

	float	m_fMinValue;
	float	m_fMaxValue;	

	int		m_iNumSteps;

	bool	m_bUseLogarithmicScale;

};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
