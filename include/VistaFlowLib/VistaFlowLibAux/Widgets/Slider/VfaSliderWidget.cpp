/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaSliderWidget.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaSliderWidget::VfaSliderWidget(VflRenderNode *pRenderNode)
	:	IVfaWidget(pRenderNode)
	,	m_pModel(NULL)
	,	m_pVis(NULL)
	,	m_pCtr(NULL)
{
	m_pModel	= new VfaSliderModel();
	m_pVis		= new VfaSliderVis(m_pModel, pRenderNode);
	m_pCtr		= new VfaSliderController(m_pModel, pRenderNode);


	m_pVis->Init();
	pRenderNode->AddRenderable(m_pVis);

}

VfaSliderWidget::~VfaSliderWidget()
{
	delete m_pCtr;
	delete m_pModel;
}

/*============================================================================*/
/*  NAME: Set/GetIsEndabled                                                   */
/*============================================================================*/
void VfaSliderWidget::SetIsEnabled(bool b)
{
	m_pCtr->SetIsEnabled(b);
	if (b)
		m_pVis->SetVisible(true);
}
bool VfaSliderWidget::GetIsEnabled() const
{
	return m_pCtr->GetIsEnabled();
}

/*============================================================================*/
/*  NAME: Set/GetIsVisible                                                    */
/*============================================================================*/
void VfaSliderWidget::SetIsVisible(bool b)
{
	m_pVis->SetVisible(b);
	if (!b)
		m_pCtr->SetIsEnabled(false);
}
bool VfaSliderWidget::GetIsVisible() const
{
	return m_pVis->GetVisible();
}


/*============================================================================*/
/*  NAME: GetModel/View/Controller                                            */
/*============================================================================*/
VfaSliderModel *VfaSliderWidget::GetModel() const
{
	return m_pModel;
}
VfaSliderVis *VfaSliderWidget::GetView() const
{
	return m_pVis;
}
VfaSliderController *VfaSliderWidget::GetController() const
{
	return m_pCtr;
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
