/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaSliderModel.h"
#include "VfaSliderVis.h"
#include "VfaSliderController.h"

//VistaFlowLib
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>

//VistaFlowLibAux
#include <VistaFlowLibAux/Widgets/VfaWidgetTools.h>
#include <VistaFlowLibAux/Widgets/VfaControlHandle.h>
#include <VistaFlowLibAux/Widgets/VfaSphereHandle.h>
#include <VistaFlowLibAux/Widgets/VfaTexturedBoxHandle.h>

#include <cstdio>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;
/*============================================================================*/
/*  IMPLEMENTATION VfaSliderController                                        */
/*============================================================================*/
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
VfaSliderController::VfaSliderController( VfaSliderModel *pModel
										, VflRenderNode* pRenderNode )
	:	IVfaWidgetController(pRenderNode)
	,	m_pModel(pModel)
	,	m_pRenderNode(pRenderNode)
	,	m_iState(STATE_NOTHING_SELECTED)
	,	m_iActiveHandle(BTN_UNDEFINDED)
	,	m_iDragButton(0)
	,	m_fUpperDraggingSpeed(0.25f)
	,	m_fLowerDraggingSpeed(1.00f)
	,	m_bIsMoveable(true)

{
	//attach the model
	this->Observe(m_pModel);

	//create handles
	m_vecHandles.resize(5);

	VfaSphereHandle* pUpperHandel = new VfaSphereHandle(pRenderNode); 
	VfaSphereHandle* pLowerHandel = new VfaSphereHandle(pRenderNode); 
	VfaTexturedBoxHandle* pAcceptHandel = new VfaTexturedBoxHandle(pRenderNode); 
	VfaTexturedBoxHandle* pRefuseHandel = new VfaTexturedBoxHandle(pRenderNode); 
	VfaTexturedBoxHandle* pMovingHandel = new VfaTexturedBoxHandle(pRenderNode); 

	pUpperHandel->SetRadius(0.015f);
	pLowerHandel->SetRadius(0.015f);

	pAcceptHandel->SetSize(0.06f, 0.06f, 0.01f);
	pRefuseHandel->SetSize(0.06f, 0.06f, 0.01f);
	pMovingHandel->SetSize(0.06f, 0.06f, 0.01f);

	pAcceptHandel->SetNormalColor(VistaColor(VistaColor::DARK_GREEN));
	pRefuseHandel->SetNormalColor(VistaColor(VistaColor::DARK_RED));
	pMovingHandel->SetNormalColor(VistaColor(VistaColor::DARK_ORANGE));

	pAcceptHandel->SetHighlightedColor(VistaColor(VistaColor::GREEN));
	pRefuseHandel->SetHighlightedColor(VistaColor(VistaColor::RED));
	pMovingHandel->SetHighlightedColor(VistaColor(VistaColor::LIGHT_ORANGE));

	m_vecHandles[BTN_LOWER] = pUpperHandel;
	m_vecHandles[BTN_UPPER] = pLowerHandel;
	m_vecHandles[BTN_ACCEPT] = pAcceptHandel;
	m_vecHandles[BTN_REFUSE] = pRefuseHandel;
	m_vecHandles[BTN_MOVE] = pMovingHandel;

	this->AddControlHandle(m_vecHandles[BTN_LOWER]);
	this->AddControlHandle(m_vecHandles[BTN_UPPER]);
	this->AddControlHandle(m_vecHandles[BTN_ACCEPT]);
	this->AddControlHandle(m_vecHandles[BTN_REFUSE]);
	this->AddControlHandle(m_vecHandles[BTN_MOVE]);

	UpdateHandlePositions();
}

VfaSliderController::~VfaSliderController()
{
	this->ReleaseObserveable(m_pModel, IVistaObserveable::TICKET_NONE);

	//delete handles
	for (size_t n = 0; n < m_vecHandles.size(); n++)
			delete m_vecHandles[n];
}

/******************************************************************************/
/*  MODIFY HANDLES                                                            */
/******************************************************************************/
bool VfaSliderController::LoadHandleTexture(int iHandle, const string& strFileName)
{
	if(iHandle != BTN_ACCEPT && iHandle != BTN_REFUSE && iHandle != BTN_MOVE)
		return false;

	VfaTexturedBoxHandle* pHandel = 
		static_cast<VfaTexturedBoxHandle*>(m_vecHandles[iHandle]);
	return pHandel->LoadTexture(strFileName);
}
/******************************************************************************/
/*  GETTER                                                                    */
/******************************************************************************/
float VfaSliderController::GetUpperDraggingSpeed() const
{
	return m_fUpperDraggingSpeed;
}
float VfaSliderController::GetLowerDraggingSpeed() const
{
	return m_fLowerDraggingSpeed;
}

/******************************************************************************/
bool VfaSliderController::GetIsEnabled() const
{
	return m_iState==STATE_DISABLED;
}

/******************************************************************************/
int VfaSliderController::GetDragButton()const
{
	return m_iDragButton;
}

/******************************************************************************/
bool VfaSliderController::GetIsMoveable()const
{
	return m_bIsMoveable;
}

/******************************************************************************/
/*  SETTER                                                                    */
/******************************************************************************/
void VfaSliderController::SetUpperDraggingSpeed(float fSpeed)
{
	m_fUpperDraggingSpeed = fSpeed;
}
void VfaSliderController::SetLowerDraggingSpeed(float fSpeed)
{
	m_fLowerDraggingSpeed = fSpeed;
}

/******************************************************************************/
void VfaSliderController::SetIsEnabled(bool b)
{
	m_vecHandles[BTN_UPPER]->SetEnable(b);
	m_vecHandles[BTN_LOWER]->SetEnable(b);
	m_vecHandles[BTN_ACCEPT]->SetEnable(b);
	m_vecHandles[BTN_REFUSE]->SetEnable(b);

	m_vecHandles[BTN_UPPER]->SetVisible(b);
	m_vecHandles[BTN_LOWER]->SetVisible(b);

	static_cast<VfaTexturedBoxHandle*>(m_vecHandles[BTN_ACCEPT])->SetIsVisible(b);
	static_cast<VfaTexturedBoxHandle*>(m_vecHandles[BTN_REFUSE])->SetIsVisible(b);
	if(m_bIsMoveable)
	{
		m_vecHandles[BTN_MOVE]->SetEnable(b);
		static_cast<VfaTexturedBoxHandle*>(m_vecHandles[BTN_MOVE])->SetIsVisible(b);
	}
}

/******************************************************************************/
void VfaSliderController::SetDragButton(int i)
{
	m_iDragButton=i;
}
/******************************************************************************/
void VfaSliderController::SetIsMoveable(bool b)
{
	m_bIsMoveable=b;
	static_cast<VfaTexturedBoxHandle*>(m_vecHandles[BTN_MOVE])->SetIsVisible(b);
	m_vecHandles[BTN_MOVE]->SetEnable(b);
}
/******************************************************************************/
/*  WIDGETCONTROLLER INTERFACE IMPLEMENTATION                                 */
/******************************************************************************/
void VfaSliderController::OnSensorSlotUpdate(int iSlot,
	const VfaApplicationContextObject::sSensorFrame &oFrameData)
{

	switch(m_iState)
	{
	case STATE_DISABLED:
	case STATE_NOTHING_SELECTED:
	case STATE_ON_FOCUS:
		return;
	case STATE_MOVING_SLIDER:
		{
			VistaVector3D v3Direktion = 
				oFrameData.qOrientation.Rotate(VistaVector3D(0,0,-1));

			VistaVector3D	v3Position	 = m_pModel->GetPosition();
			VistaQuaternion	qOrientation = m_pModel->GetOrientation();

			VistaVector3D v3Right  = qOrientation.Rotate(VistaVector3D(1,0,0));
			VistaVector3D v3Normal = qOrientation.Rotate(VistaVector3D(0,0,1));

			float f = -(oFrameData.v3Position-v3Position).Dot(v3Normal)
				/	v3Direktion.Dot(v3Normal);

			VistaVector3D v3Proj = oFrameData.v3Position + v3Direktion*f;

			f = (v3Proj-v3Position).Dot(v3Right)+0.5f;

			f = (f-m_fStartValue)*m_fDraggingSpeed + m_fStartValue;

			m_pModel->SetSliderPos(f);
		}
		break;
	case STATE_MOVING_WIDGET:
		{
			if(m_bFirstMove)
			{
				m_v3Offset = m_pModel->GetPosition() - oFrameData.v3Position;
				m_v3Offset = oFrameData.qOrientation.GetInverted().Rotate(m_v3Offset);
				m_qRotation = m_pModel->GetOrientation();
				m_qRotation = oFrameData.qOrientation.GetInverted()*m_qRotation;
				m_bFirstMove = false;
			}
			else
			{
				VistaVector3D v3Pos = oFrameData.qOrientation.Rotate(m_v3Offset);
				v3Pos += oFrameData.v3Position;
				m_pModel->SetPosition(v3Pos);
				VistaQuaternion qOri = oFrameData.qOrientation*m_qRotation;
				m_pModel->SetOrientation(qOri);
			}
		}
		break;
	}
}

/******************************************************************************/
void VfaSliderController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	if( iSlot != m_iDragButton || m_iState==STATE_DISABLED )
		return;

	if( !bSet )
	{
		if( m_iState==STATE_MOVING_SLIDER || m_iState==STATE_MOVING_WIDGET)
		{
			m_iState = STATE_NOTHING_SELECTED;
			m_vecHandles[BTN_UPPER]->SetIsHighlighted(false);
			m_vecHandles[BTN_LOWER]->SetIsHighlighted(false);
			m_vecHandles[BTN_MOVE]->SetIsHighlighted(false);

			if( m_iActiveHandle!=BTN_UNDEFINDED )
			{
				m_iState = STATE_ON_FOCUS;
				m_vecHandles[m_iActiveHandle]->SetIsHighlighted(true);
			}
		}
	}
	else
	{
		if(m_iState==STATE_ON_FOCUS)
		{
			switch(m_iActiveHandle)
			{
			case BTN_UPPER:
			case BTN_LOWER:
				m_iState = STATE_MOVING_SLIDER;
				m_fStartValue = m_pModel->GetSliderPos();
				if( m_iActiveHandle==BTN_UPPER ) m_fDraggingSpeed = m_fUpperDraggingSpeed;
				if( m_iActiveHandle==BTN_LOWER ) m_fDraggingSpeed = m_fLowerDraggingSpeed;
				break;
			case BTN_ACCEPT:
				m_pModel->AcceptValue();
				break;
			case BTN_REFUSE:
				m_pModel->RefuseValue();
				break;
			case BTN_MOVE:
				if(!m_bIsMoveable)
					return;
				m_iState = STATE_MOVING_WIDGET;
				m_bFirstMove = true;
				break;
			}
		}
	}
}

/******************************************************************************/
void VfaSliderController::OnTimeSlotUpdate(const double dTime)
{
	return;
}

/******************************************************************************/
void VfaSliderController::OnFocus(IVfaControlHandle* pHandle)
{
	if (m_iState==STATE_DISABLED)
		return;

	for(size_t n =0; n < m_vecHandles.size(); ++n)
	{
		if (m_vecHandles[n]!=pHandle)
			continue;

		m_iActiveHandle = static_cast<int>(n);
		if(m_iState != STATE_MOVING_SLIDER && m_iState != STATE_MOVING_WIDGET)
		{
			m_iState=STATE_ON_FOCUS;
			m_vecHandles[n]->SetIsHighlighted(true);
		}
		return;
	}

	m_iActiveHandle = BTN_UNDEFINDED;
	if(m_iState != STATE_MOVING_SLIDER && m_iState != STATE_MOVING_WIDGET)
	{
		m_iState = STATE_NOTHING_SELECTED;
	}
}

/******************************************************************************/
void VfaSliderController::OnUnfocus()
{
	if( m_iState==STATE_DISABLED )
		return;
	if( m_iActiveHandle == BTN_UNDEFINDED )
		return;

	if(m_iState != STATE_MOVING_SLIDER && m_iState != STATE_MOVING_WIDGET)
	{
		m_vecHandles[m_iActiveHandle]->SetIsHighlighted(false);
		m_iState = STATE_NOTHING_SELECTED;
	}
	m_iActiveHandle = BTN_UNDEFINDED;
}

/******************************************************************************/
int VfaSliderController::GetSensorMask() const
{
	return (1<<VfaApplicationContextObject::SLOT_POINTER_VIS);
}

int VfaSliderController::GetCommandMask() const
{
	return(1 << m_iDragButton);
}

/******************************************************************************/
bool VfaSliderController::GetTimeUpdate() const
{
	return true;
}

/******************************************************************************/
/*  OBSERVER INTERFACE IMPLEMENTATION                                         */
/******************************************************************************/
void VfaSliderController::ObserverUpdate(IVistaObserveable *pObs,
	int iMsg, int iTicket)
{
	switch(iMsg)
	{
	case VfaSliderModel::MSG_SLIDER_POSITION_CHANGED:
	case VfaSliderModel::MSG_POSITION_CHANGED:
	case VfaSliderModel::MSG_ORIANTATION_CHANGED:
		UpdateHandlePositions();
		break;
	}
}

void VfaSliderController::UpdateHandlePositions()
{
	VistaVector3D	v3Position	 = m_pModel->GetPosition();
	VistaQuaternion	qOrientation = m_pModel->GetOrientation();

	VistaVector3D v3Right = qOrientation.Rotate(VistaVector3D(1,0,0));
	VistaVector3D v3Up    = qOrientation.Rotate(VistaVector3D(0,1,0));

	VistaVector3D v3Start = v3Position - 0.5f*v3Right;
	VistaVector3D v3End   = v3Position + 0.5f*v3Right;

	float fValue = m_pModel->GetSliderPos();

	VistaVector3D v3Pos = v3Start + fValue*v3Right;

	m_vecHandles[BTN_LOWER]->SetCenter(v3Pos - 0.03f*v3Up);
	m_vecHandles[BTN_UPPER]->SetCenter(v3Pos + 0.03f*v3Up);
	m_vecHandles[BTN_ACCEPT]->SetCenter(v3Position + 0.55f*v3Right + 0.14f*v3Up);
	m_vecHandles[BTN_REFUSE]->SetCenter(v3Position + 0.62f*v3Right + 0.14f*v3Up);
	m_vecHandles[BTN_MOVE]->SetCenter(  v3Position - 0.64f*v3Right + 0.14f*v3Up);

	static_cast<VfaTexturedBoxHandle*>(m_vecHandles[BTN_ACCEPT])->SetRotation(qOrientation);
	static_cast<VfaTexturedBoxHandle*>(m_vecHandles[BTN_REFUSE])->SetRotation(qOrientation);
	static_cast<VfaTexturedBoxHandle*>(m_vecHandles[BTN_MOVE])->SetRotation(qOrientation);
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
