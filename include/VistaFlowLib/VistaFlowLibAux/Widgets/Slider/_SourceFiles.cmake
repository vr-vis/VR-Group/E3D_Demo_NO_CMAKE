

set( RelativeDir "./Widgets/Slider" )
set( RelativeSourceGroup "Source Files\\Widgets\\Slider" )

set( DirFiles
	VfaSliderController.cpp
	VfaSliderController.h
	VfaSliderModel.cpp
	VfaSliderModel.h
	VfaSliderVis.cpp
	VfaSliderVis.h
	VfaSliderWidget.cpp
	VfaSliderWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

