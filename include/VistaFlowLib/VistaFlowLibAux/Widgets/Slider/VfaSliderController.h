/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

#ifndef SLIDER_CONTROLER_H
#define SLIDER_CONTROLER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>
#include <VistaFlowLib/Data/VflObserver.h>

#include <vector>
#include <string>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaCenterControlHandle;
class VfaSliderModel;
class VfaSliderVis;
class VflRenderNode;
class VfaSphereHandle;
class VfaTexturedBoxHandle;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaSliderController 
	:	public IVfaWidgetController
	,	public VflObserver
{
public:
	VfaSliderController( VfaSliderModel* pModel, VflRenderNode* pRenderNode );
	virtual ~VfaSliderController();
		
	enum Buttons
	{
		BTN_UNDEFINDED = -1,
		BTN_UPPER = 0,
		BTN_LOWER,
		BTN_ACCEPT,
		BTN_REFUSE,
		BTN_MOVE,
		BTN_LAST
	};

	bool LoadHandleTexture(int iHandle, const std::string& strFileName);

	float GetUpperDraggingSpeed() const;
	float GetLowerDraggingSpeed() const;

	bool GetIsEnabled() const;
	int GetDragButton() const;	

	bool GetIsMoveable() const;
	
	
	void SetUpperDraggingSpeed(float fSpeed);
	void SetLowerDraggingSpeed(float fSpeed);

	void SetIsEnabled(bool b);
	void SetDragButton(int i);

	void SetIsMoveable(bool b);
	
	// *** WidgetController interface ***
	virtual void OnFocus(IVfaControlHandle* pHandle);
	virtual void OnUnfocus();

	virtual void OnSensorSlotUpdate(int iSlot,
		const VfaApplicationContextObject::sSensorFrame &oFrameData);
	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);
	virtual void OnTimeSlotUpdate(const double dTime);

	virtual int  GetSensorMask()  const;
	virtual int  GetCommandMask() const;
	virtual bool GetTimeUpdate()  const;
		
	// *** IVistaObersver interface ***
	virtual void ObserverUpdate(IVistaObserveable *pObs, int iMsg, int iTicket);

protected:
	// enum for internal state control
	enum EInternalState{
		STATE_DISABLED,
		STATE_NOTHING_SELECTED,
		STATE_ON_FOCUS,
		STATE_MOVING_SLIDER,
		STATE_MOVING_WIDGET
	};

	void UpdateHandlePositions();

private:
	VfaSliderModel*	m_pModel;
	VflRenderNode*	m_pRenderNode;

	// handles for interaction
	std::vector<IVfaCenterControlHandle*> m_vecHandles;

	int m_iState;
	int m_iActiveHandle;

	float m_fStartValue;
	float m_fDraggingSpeed;

	float m_fUpperDraggingSpeed;
	float m_fLowerDraggingSpeed;

	bool m_bFirstMove;
	VistaVector3D m_v3Offset;
	VistaQuaternion m_qRotation;

	bool m_bIsMoveable;

	int m_iDragButton;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
