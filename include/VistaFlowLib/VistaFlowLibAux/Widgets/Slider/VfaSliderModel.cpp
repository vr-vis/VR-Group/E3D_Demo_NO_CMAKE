/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <cstring>

#include "VfaSliderModel.h"
#include <VistaBase/VistaStreamUtils.h>

#include <VistaAspects/VistaReflectionable.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

using namespace std;
/*============================================================================*/
/*  IMPLEMENTATION   VfaSliderModel                                           */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaSliderModel");
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
VfaSliderModel::VfaSliderModel()
	:	m_v3Position()
	,	m_qOrientation()
	,	m_fSliderPos(0.5f)
	,	m_fOldSliderPos(0.5f)
	,	m_fMinValue(0.0f)
	,	m_fMaxValue(1.0f)
	,	m_iNumSteps(-1)
	,	m_bUseLogarithmicScale(false)
{}

VfaSliderModel::~VfaSliderModel()
{}

/******************************************************************************/
/*  GETTER                                                                    */
/******************************************************************************/
VistaVector3D VfaSliderModel::GetPosition() const
{
	return m_v3Position;
}

VistaQuaternion VfaSliderModel::GetOrientation() const
{
	return m_qOrientation;
}

float VfaSliderModel::GetOldSliderPos() const
{
	return m_fOldSliderPos;
}

float VfaSliderModel::GetSliderPos() const
{
	return m_fSliderPos;
}

float VfaSliderModel::GetValue() const
{
	if(m_bUseLogarithmicScale && m_fMinValue >0 && m_fMaxValue >0)
	{
		//only use Logarithmic Scale if min and max are greater than 0.
		float fLogMin = log(m_fMinValue);
		float fLogMax = log(m_fMaxValue);

		float fLogValue = m_fSliderPos*(fLogMax-fLogMin) + fLogMin;

		return exp(fLogValue);
	}
	else
	{
		//use line scale else
		if(m_bUseLogarithmicScale && m_fMinValue <=0)
			vstr::errp() << "[VfaSliderModel] Min value must be "
			<< "greater then 0 when you're using logarithmic scale!"
			<< endl;
		if(m_bUseLogarithmicScale && m_fMaxValue <=0)
			vstr::errp() << "[VfaSliderModel] Max value must be "
			<< "greater then 0 when you're using logarithmic scale!"
			<< endl;

		return m_fSliderPos*(m_fMaxValue-m_fMinValue) + m_fMinValue;
	}
}

float VfaSliderModel::GetMinValue() const
{
	return m_fMinValue;
}

float VfaSliderModel::GetMaxValue() const
{
	return m_fMaxValue;
}

bool VfaSliderModel::GetUseLogarithmicScale() const
{
	return m_bUseLogarithmicScale;
}

/******************************************************************************/
/*  SETTER                                                                    */
/******************************************************************************/
void VfaSliderModel::SetPosition( VistaVector3D	v3Position )
{
	m_v3Position = v3Position;
	Notify(MSG_POSITION_CHANGED);
}

void VfaSliderModel::SetOrientation( VistaQuaternion qOrientation )
{
	m_qOrientation = qOrientation;
	Notify(MSG_ORIANTATION_CHANGED);
}

/******************************************************************************/
void VfaSliderModel::SetSliderPos( float fValue )
{
	if(fValue < 0.0f) fValue = 0.0f;
	if(fValue > 1.0f) fValue = 1.0f;
	if(m_iNumSteps > 0)
	{
		int i = int(fValue*(m_iNumSteps) +0.5f);
		fValue = float(i)/float(m_iNumSteps);
	}
	
	if(fValue != m_fSliderPos)
	{
		m_fSliderPos = fValue;
		Notify(MSG_SLIDER_POSITION_CHANGED);
	}
}

/******************************************************************************/
void VfaSliderModel::SetValue( float fValue )
{
	float fNewSliderPos;

	if(!m_bUseLogarithmicScale)
	{
		fNewSliderPos = (fValue-m_fMinValue)/(m_fMaxValue-m_fMinValue);
	}
	else
	{
		if(fValue <= 0.0f)
		{
			vstr::warnp() << "VfaSliderModel::SetValue - fValue must be "
						  << "greater then 0 when you're using logarithmic scale!"
						  << endl;
				return;
		}

		float fLogValue = log(fValue);
		float fLogMin	= log(m_fMinValue);
		float fLogMax	= log(m_fMaxValue);

		fNewSliderPos = (fLogValue-fLogMin)/(fLogMax-fLogMin);
	}
	if(fNewSliderPos < 0.0f || 1.0 < fNewSliderPos)
	{
		vstr::warnp() << "VfaSliderModel::SetValue - fValue must be "
					  << "between " << m_fMinValue << " and " << m_fMaxValue
					  << endl;

		//clamp slider position
		if(fNewSliderPos < 0.0f) fNewSliderPos = 0.0f;
		if(fNewSliderPos > 1.0f) fNewSliderPos = 1.0f;
	}

	SetSliderPos(fNewSliderPos);
	m_fOldSliderPos = fNewSliderPos;
}

void VfaSliderModel::SetMinValue( float fValue )
{
	if(m_bUseLogarithmicScale && fValue <= 0.0f)
	{
		vstr::warnp() << "[VfaSliderModel] Min value must be "
					  << "greater then 0 when you're using logarithmic scale!"
			          << endl;
		vstr::warni() << "Either set min value to a value > 0 or disable logarithmic scale!"
				      << endl;
	}

	m_fMinValue = fValue;
	Notify(MSG_MIN_VALUE_CHANGED);
}
void VfaSliderModel::SetMaxValue( float fValue )
{
	if(m_bUseLogarithmicScale && fValue <= 0.0f)
	{
		vstr::warnp() << "[VfaSliderModel] Max value must be "
			<< "greater then 0 when you're using logarithmic scale!"
			<< endl;
		vstr::warni() << "Either set max value to a value > 0 or disable logarithmic scale!"
			<< endl;
	}

	m_fMaxValue = fValue;
	Notify(MSG_MAX_VALUE_CHANGED);
}

/******************************************************************************/
void VfaSliderModel::SetNumSteps( int iNumSteps )
{
	m_iNumSteps = iNumSteps;
}

void VfaSliderModel::SetStepSize( float fStepSize )
{
	float f = (m_fMaxValue-m_fMinValue)*1/(fStepSize);
	m_iNumSteps = int(abs(f));
}

/******************************************************************************/
void VfaSliderModel::SetUseLogarithmicScale( bool b )
{
	if(b != m_bUseLogarithmicScale)
	{
		m_bUseLogarithmicScale = b;

		if(b && m_fMinValue <=0)
		{
			vstr::warnp() << "[VfaSliderModel] Min value must be "
				<< "greater then 0 when you're using logarithmic scale!"
				<< endl;
			vstr::warni() << "Either set min value to a value > 0 or disable logarithmic scale!"
				<< endl;
		}
		if(b && m_fMaxValue <=0)
		{
			vstr::warnp() << "[VfaSliderModel] Max value must be "
				<< "greater then 0 when you're using logarithmic scale!"
				<< endl;
			vstr::warni() << "Either set max value to a value > 0 or disable logarithmic scale!"
				<< endl;
		}
	}
}

/******************************************************************************/
void VfaSliderModel::AcceptValue()
{
	m_fOldSliderPos = m_fSliderPos;
	Notify(MSG_VALUE_ACCEPTED);
}

void VfaSliderModel::RefuseValue()
{
	SetSliderPos(m_fOldSliderPos);
	Notify(MSG_VALUE_REFUSED);
}
/******************************************************************************/
/*  REFLECTIONABLE INTERVACE                                                  */
/******************************************************************************/
std::string VfaSliderModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

int VfaSliderModel::AddToBaseTypeList( list<string> &rBtList ) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
