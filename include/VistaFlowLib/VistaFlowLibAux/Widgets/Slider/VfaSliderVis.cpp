/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "VfaSliderVis.h"
#include "VfaSliderModel.h"

//VistaFlowLib
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>

#include <VistaOGLExt/VistaGLLine.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;
/*============================================================================*/
/*  IMPLEMENTATION VfaSliderVis                                               */
/*============================================================================*/
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
VfaSliderVis::VfaSliderVis( VfaSliderModel* pModel, VflRenderNode* pRenderNode )
	:	m_pModel(pModel)
	,	m_pValueText(new Vfl3DTextLabel)
	,	m_pMinText(new Vfl3DTextLabel)
	,	m_pMaxText(new Vfl3DTextLabel)
{
	Observe(pModel);

	m_pValueText->Init();
	m_pMinText->Init();
	m_pMaxText->Init();

	m_pValueText->GetProperties()->SetUseLighting( false );
	m_pMinText->GetProperties()->SetUseLighting( false );
	m_pMaxText->GetProperties()->SetUseLighting( false );

	m_pValueText->SetTextFollowViewDir(false);
	m_pMinText->SetTextFollowViewDir(false);
	m_pMaxText->SetTextFollowViewDir(false);

	m_pValueText->SetTextSize(0.045f);
	m_pMinText->SetTextSize(0.045f);
	m_pMaxText->SetTextSize(0.045f);

	pRenderNode->AddRenderable(m_pValueText);
	pRenderNode->AddRenderable(m_pMinText);
	pRenderNode->AddRenderable(m_pMaxText);

	char aBuffer[64];
	sprintf(aBuffer, "%.2f", m_pModel->GetValue());
	m_pValueText->SetText(aBuffer);
	sprintf(aBuffer, "%.2f", m_pModel->GetMinValue());
	m_pMinText->SetText(aBuffer);
	sprintf(aBuffer, "%.2f", m_pModel->GetMaxValue());
	m_pMaxText->SetText(aBuffer);
}

VfaSliderVis::~VfaSliderVis()
{}

/******************************************************************************/
/*  GETTER                                                                    */
/******************************************************************************/
VfaSliderModel* VfaSliderVis::GetModel() const
{
	return m_pModel;
}

/******************************************************************************/
/*  RENDERABLE INTERFACE IMPLEMENTATION                                       */
/******************************************************************************/
void VfaSliderVis::SetVisible(bool b)
{
	m_pValueText->SetVisible(b);
	m_pMinText->SetVisible(b);
	m_pMaxText->SetVisible(b);

	IVflRenderable::SetVisible(b);
}

/******************************************************************************/
void VfaSliderVis::Update()
{
	VfaSliderVis::VfaSliderVisProps *pProps = this->GetProperties();

	if(pProps == NULL || !pProps->GetVisible())
		return;

	VistaVector3D	v3Position	 = m_pModel->GetPosition();
	VistaQuaternion	qOrientation = m_pModel->GetOrientation();

	m_v3Right  = qOrientation.Rotate(VistaVector3D(1, 0, 0));
	m_v3Up     = qOrientation.Rotate(VistaVector3D(0, 1, 0));
	m_v3Normal = qOrientation.Rotate(VistaVector3D(0, 0, 1));

	m_v3Start		 = v3Position - 0.5f*m_v3Right;
	m_v3End			 = v3Position + 0.5f*m_v3Right;
	m_v3SliderPos	 = v3Position + (m_pModel->GetSliderPos()-0.5f)*m_v3Right;
	m_v3OldSliderPos = v3Position + (m_pModel->GetOldSliderPos()-0.5f)*m_v3Right;

	m_pValueText->SetOrientation(qOrientation);
	m_pMinText->SetOrientation(qOrientation);
	m_pMaxText->SetOrientation(qOrientation);

	m_pValueText->SetPosition(	m_v3SliderPos - 0.10f*m_v3Up - 0.1f*m_v3Right + 0.01f*m_v3Normal);
	m_pMinText->SetPosition(	m_v3Start	  + 0.06f*m_v3Up - 0.1f*m_v3Right + 0.01f*m_v3Normal);
	m_pMaxText->SetPosition(	m_v3End		  + 0.06f*m_v3Up - 0.1f*m_v3Right + 0.01f*m_v3Normal);
}

/******************************************************************************/
void VfaSliderVis::DrawOpaque()
{
	VfaSliderVis::VfaSliderVisProps *pProps = this->GetProperties();

	if(pProps == NULL || !pProps->GetVisible())
		return;

	VistaGLLine::SetLineWidth(pProps->GetLineWidth());
	VistaGLLine::Enable(VistaGLLine::SHADER_CYLINDER);
	VistaGLLine::Begin();

	glColor3fv( &(pProps->GetLineColor()[0]) );
	glVertex3fv( &(m_v3Start[0]));
	glVertex3fv( &(m_v3End[0]));

	VistaGLLine::End();
	VistaGLLine::Disable();

	VistaGLLine::SetLineWidth(pProps->GetIndicatorWidth());
	VistaGLLine::Enable(VistaGLLine::SHADER_CYLINDER);
	VistaGLLine::Begin();

	glColor3fv( &(pProps->GetIndicatorColor()[0]) );
	glVertex3fv( &((m_v3Start + 0.03f*m_v3Up)[0]));
	glVertex3fv( &((m_v3Start - 0.03f*m_v3Up)[0]));

	glVertex3fv( &((m_v3SliderPos + 0.03f*m_v3Up)[0]));
	glVertex3fv( &((m_v3SliderPos - 0.03f*m_v3Up)[0]));

	glVertex3fv( &((m_v3End + 0.03f*m_v3Up)[0]));
	glVertex3fv( &((m_v3End - 0.03f*m_v3Up)[0]));

	VistaGLLine::End();
	VistaGLLine::SetLineWidth(pProps->GetOldValueIndicatorWidth());
	VistaGLLine::Begin();

	glColor3fv( &(pProps->GetOldValueIndicatorColor()[0]) );
	glVertex3fv( &((m_v3OldSliderPos + 0.02f*m_v3Up)[0]));
	glVertex3fv( &((m_v3OldSliderPos - 0.02f*m_v3Up)[0]));

	VistaGLLine::End();
	VistaGLLine::Disable();

	if(pProps->GetShowBackground())
	{
		glPushAttrib( GL_ENABLE_BIT ); // Lighting.
		glDisable( GL_LIGHTING );
		
		glBegin(GL_QUADS);
		glNormal3fv(&(m_v3Normal[0]));
		glColor3fv( &(pProps->GetBackgroundColor()[0]) );

		glVertex3fv( &((m_v3End   + 0.12f*m_v3Up + 0.12f*m_v3Right)[0]));
		glVertex3fv( &((m_v3Start + 0.12f*m_v3Up - 0.12f*m_v3Right)[0]));
		glVertex3fv( &((m_v3Start - 0.12f*m_v3Up - 0.12f*m_v3Right)[0]));
		glVertex3fv( &((m_v3End   - 0.12f*m_v3Up + 0.12f*m_v3Right)[0]));

		glEnd();

		glPopAttrib();

		VistaGLLine::SetLineWidth(pProps->GetBackgroundBorderWidth());
		VistaGLLine::Enable(VistaGLLine::SHADER_CYLINDER);
		VistaGLLine::Begin(VistaGLLine::VISTA_GL_LINE_STRIP);

		glColor3fv( &(pProps->GetBackgroundBorderColor()[0]) );

		glVertex3fv( &((m_v3End   + 0.12f*m_v3Up + 0.12f*m_v3Right)[0]));
		glVertex3fv( &((m_v3Start + 0.12f*m_v3Up - 0.12f*m_v3Right)[0]));
		glVertex3fv( &((m_v3Start - 0.12f*m_v3Up - 0.12f*m_v3Right)[0]));
		glVertex3fv( &((m_v3End   - 0.12f*m_v3Up + 0.12f*m_v3Right)[0]));
		glVertex3fv( &((m_v3End   + 0.12f*m_v3Up + 0.12f*m_v3Right)[0]));

		VistaGLLine::End();
		VistaGLLine::Disable();
	}	
}

/******************************************************************************/
unsigned int VfaSliderVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE | IVflRenderable::OLI_UPDATE;
}

/******************************************************************************/
VfaSliderVis::VfaSliderVisProps *VfaSliderVis::GetProperties() const
{
	return static_cast<VfaSliderVis::VfaSliderVisProps*>(
				IVflRenderable::GetProperties() );
}

/******************************************************************************/
IVflRenderable::VflRenderableProperties* VfaSliderVis::CreateProperties() const
{
	return new VfaSliderVis::VfaSliderVisProps();
}

/******************************************************************************/
/*  OBSERVER INTERFACE IMPLEMENTATION                                         */
/******************************************************************************/
void VfaSliderVis::ObserverUpdate(IVistaObserveable *pObs, int iMsg, int iTicket)
{
	if(pObs == m_pModel)
	{
		char aBuffer[64];
		switch (iMsg)
		{
		case VfaSliderModel::MSG_MIN_VALUE_CHANGED:
			sprintf(aBuffer, "%.2f", m_pModel->GetMinValue());
			m_pMinText->SetText(aBuffer);
			sprintf(aBuffer, "%.2f", m_pModel->GetValue());
			m_pValueText->SetText(aBuffer);
			break;
		case VfaSliderModel::MSG_MAX_VALUE_CHANGED:
			sprintf(aBuffer, "%.2f", m_pModel->GetMaxValue());
			m_pMaxText->SetText(aBuffer);
			sprintf(aBuffer, "%.2f", m_pModel->GetValue());
			m_pValueText->SetText(aBuffer);
			break;
		case VfaSliderModel::MSG_SLIDER_POSITION_CHANGED:
			sprintf(aBuffer, "%.2f", m_pModel->GetValue());
			m_pValueText->SetText(aBuffer);
			break;
		}
	}
	IVflRenderable::ObserverUpdate(pObs, iMsg, iTicket);
}

/*============================================================================*/
/*  IMPLEMENTATION VfaSliderVis::VfaSliderVisProps                            */
/*============================================================================*/
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
VfaSliderVis::VfaSliderVisProps::VfaSliderVisProps()
	:	m_oLineColor(VistaColor::BLUE)
	,	m_oIndicatorColor(VistaColor::RED)
	,	m_oOldValueIndicatorColor(VistaColor::DARK_RED)
	,	m_oBackgroundColor(VistaColor::VISTA_BLUE)
	,	m_oBackgroundBorderColor(VistaColor::LIGHT_GRAY)
	,	m_fLineWidth(0.01f)
	,	m_fIndicatorWidth(0.012f)
	,	m_fOldValueIndicatorWidth(0.008f)
	,	m_fBackgroundBorderWidth(0.004f)
	,	m_bShowBackground(true)
{ }

VfaSliderVis::VfaSliderVisProps::~VfaSliderVisProps()
{ }

/******************************************************************************/
/*  GETTER                                                                    */
/******************************************************************************/
const VistaColor& VfaSliderVis::VfaSliderVisProps::GetLineColor() const
{
	return m_oLineColor;
}
const VistaColor& VfaSliderVis::VfaSliderVisProps::GetIndicatorColor() const
{
	return m_oIndicatorColor;
}
const VistaColor& VfaSliderVis::VfaSliderVisProps::GetOldValueIndicatorColor() const
{
	return m_oOldValueIndicatorColor;
}
const VistaColor& VfaSliderVis::VfaSliderVisProps::GetBackgroundColor() const
{
	return m_oBackgroundColor;
}
const VistaColor& VfaSliderVis::VfaSliderVisProps::GetBackgroundBorderColor() const
{
	return m_oBackgroundBorderColor;
}

/******************************************************************************/
float VfaSliderVis::VfaSliderVisProps::GetLineWidth() const
{
	return m_fLineWidth;
}
float VfaSliderVis::VfaSliderVisProps::GetIndicatorWidth() const
{
	return m_fIndicatorWidth;
}
float VfaSliderVis::VfaSliderVisProps::GetOldValueIndicatorWidth() const
{
	return m_fOldValueIndicatorWidth;
}

float VfaSliderVis::VfaSliderVisProps::GetBackgroundBorderWidth() const
{
	return m_fBackgroundBorderWidth;
}

/******************************************************************************/
bool VfaSliderVis::VfaSliderVisProps::GetShowBackground() const
{
	return m_bShowBackground;
}

/******************************************************************************/
/*  SETTER                                                                    */
/******************************************************************************/
void VfaSliderVis::VfaSliderVisProps::SetLineColor( const VistaColor& oColor )
{
	m_oLineColor = oColor;
}
void VfaSliderVis::VfaSliderVisProps::SetIndicatorColor( const VistaColor& oColor )
{
	m_oIndicatorColor = oColor;
}
void VfaSliderVis::VfaSliderVisProps::SetOldValueIndicatorColor( const VistaColor& oColor )
{
	m_oOldValueIndicatorColor = oColor;
}
void VfaSliderVis::VfaSliderVisProps::SetBackgroundColor( const VistaColor& oColor )
{
	m_oBackgroundColor = oColor;
}
void VfaSliderVis::VfaSliderVisProps::SetBackgroundBorderColor( const VistaColor& oColor )
{
	m_oBackgroundBorderColor = oColor;
}

/******************************************************************************/
void VfaSliderVis::VfaSliderVisProps::SetLineWidth( float fWidth )
{
	m_fLineWidth = fWidth;
}
void VfaSliderVis::VfaSliderVisProps::SetIndicatorWidth( float fWidth )
{
	m_fIndicatorWidth = fWidth;
}
void VfaSliderVis::VfaSliderVisProps::SetOldValueIndicatorWidth( float fWidth )
{
	m_fOldValueIndicatorWidth = fWidth;
}
void VfaSliderVis::VfaSliderVisProps::SetBackgroundBorderWidth( float fWidth )
{
	m_fBackgroundBorderWidth = fWidth;
}

/******************************************************************************/
void VfaSliderVis::VfaSliderVisProps::SetShowBackground( bool b)
{
	m_bShowBackground = b;
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
