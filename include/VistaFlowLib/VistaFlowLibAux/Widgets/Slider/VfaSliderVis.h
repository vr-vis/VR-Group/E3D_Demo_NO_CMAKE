/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

#ifndef SLIDER_VIS_H
#define SLIDER_VIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <VistaBase/VistaColor.h>

#include <VistaFlowLib/Visualization/VflRenderable.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaSliderModel;
class VistaColor;
class Vfl3DTextLabel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaSliderVis : public IVflRenderable
{
public: 
	/**************************************************************************/
	/*  CONSTRUCTORS / DESTRUCTOR                                             */
	/**************************************************************************/
	explicit VfaSliderVis(VfaSliderModel *pModel, VflRenderNode *pRenderNode);
	virtual ~VfaSliderVis();

	/**************************************************************************/
	/*  GETTER                                                                */
	/**************************************************************************/
	VfaSliderModel *GetModel() const;


	/**************************************************************************/
	/*  PROPERTIES                                                            */
	/**************************************************************************/
	class VISTAFLOWLIBAUXAPI VfaSliderVisProps 
		:	public IVflRenderable::VflRenderableProperties
	{
	public:
		/**********************************************************************/
		/*  CONSTRUCTORS / DESTRUCTOR                                         */
		/**********************************************************************/
		VfaSliderVisProps();
		virtual ~VfaSliderVisProps();

		/**********************************************************************/
		/*  GETTER                                                            */
		/**********************************************************************/
		const VistaColor& GetLineColor()		 const;
		const VistaColor& GetIndicatorColor()	 const;
		const VistaColor& GetOldValueIndicatorColor() const;
		const VistaColor& GetBackgroundColor() const;
		const VistaColor& GetBackgroundBorderColor() const;

		float GetLineWidth()		 const;
		float GetIndicatorWidth()	 const;
		float GetOldValueIndicatorWidth() const;
		float GetBackgroundBorderWidth() const;

		bool GetShowBackground() const;
		/**********************************************************************/
		/*  SETTER                                                            */
		/**********************************************************************/
		void SetLineColor			  ( const VistaColor& oColor );
		void SetIndicatorColor		  ( const VistaColor& oColor );
		void SetOldValueIndicatorColor( const VistaColor& oColor );
		void SetBackgroundColor		  ( const VistaColor& oColor );
		void SetBackgroundBorderColor ( const VistaColor& oColor );

		void SetLineWidth			  ( float fWidth );
		void SetIndicatorWidth		  ( float fWidth );
		void SetOldValueIndicatorWidth( float fWidth );
		void SetBackgroundBorderWidth ( float fWidth );

		void SetShowBackground( bool b);

	private:
		VistaColor m_oLineColor;
		VistaColor m_oIndicatorColor;
		VistaColor m_oOldValueIndicatorColor;
		VistaColor m_oBackgroundColor;
		VistaColor m_oBackgroundBorderColor;

		float m_fLineWidth;
		float m_fIndicatorWidth;
		float m_fOldValueIndicatorWidth;
		float m_fBackgroundBorderWidth;

		bool m_bShowBackground;

	};
	/**************************************************************************/
	/*  RENDERABLE INTERFACE IMPLEMENTATION                                   */
	/**************************************************************************/
	virtual void SetVisible(bool b);

	virtual void Update();
	virtual void DrawOpaque();

	virtual unsigned int GetRegistrationMode() const;

	virtual VfaSliderVisProps *GetProperties() const;

	virtual void ObserverUpdate(IVistaObserveable *pObs, int iMsg, int iTicket);

protected:
	virtual VflRenderableProperties* CreateProperties() const;

private:
	VfaSliderModel *m_pModel;

	Vfl3DTextLabel *m_pValueText;
	Vfl3DTextLabel *m_pMinText;
	Vfl3DTextLabel *m_pMaxText;

	VistaVector3D m_v3Right;
	VistaVector3D m_v3Up;
	VistaVector3D m_v3Normal;

	VistaVector3D m_v3Start;
	VistaVector3D m_v3End;

	VistaVector3D m_v3SliderPos;
	VistaVector3D m_v3OldSliderPos;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
