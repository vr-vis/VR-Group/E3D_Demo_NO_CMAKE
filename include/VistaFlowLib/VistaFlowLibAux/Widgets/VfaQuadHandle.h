/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAQUADHANDLE_H
#define _VFAQUADHANDLE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaControlHandle.h"
#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VflRenderNode;
class VistaVector3D;
class VistaQuaternion;
class VfaQuadVis;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Box visualization which can be highlighted and tell in this way
 * that it can be grabbed and further moved.
 */
class VISTAFLOWLIBAUXAPI VfaQuadHandle : public IVfaCenterControlHandle
{
public: 
	VfaQuadHandle(VflRenderNode *pRenderNode);
	virtual ~VfaQuadHandle();

	void SetVisible(bool b);
	bool GetVisible() const;

	void SetSize(float w, float h);
	bool GetSize(float &w, float &h) const;

	// derived from IVfaCenterControlHandle
	void SetCenter(const VistaVector3D &v3Translation);
	void SetCenter(float fC[3]);
	void SetCenter(double dC[3]);

	void GetCenter(VistaVector3D &v3Translation);
	void GetCenter(float fC[3]);
	void GetCenter(double dC[3]);

	void SetNormal(const VistaVector3D &v3Normal);
	void GetNormal(VistaVector3D &v3Normal) const;


	void SetNormalColor(const VistaColor& color);
	VistaColor GetNormalColor() const;
	void SetNormalColor(float fColor[4]);
	void GetNormalColor(float fColor[4]) const;

	void SetHighlightColor(const VistaColor& color);
	VistaColor GetHighlightColor() const;
	void SetHighlightColor(float fColor[4]);
	void GetHighlightColor(float fColor[4]) const;

	void SetIsHighlighted(bool b);
	bool GetIsHighlighted() const;

	VfaQuadVis* GetQuadVis() const;

private:
	float m_fNormalColor[4];
	float m_fHighlightColor[4];

	VfaQuadVis* m_pQuadVis;
};


#endif // Include guard.

/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
