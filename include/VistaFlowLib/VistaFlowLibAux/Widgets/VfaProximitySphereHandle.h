/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAPROXIMITYBOXHANDLE_H
#define _VFAPROXIMITYBOXHANDLE_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaProximityHandle.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaSphereVis;
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A proximity handle which is touched, whenever the cursor position is inside
 * a given sphere.
 */
class VISTAFLOWLIBAUXAPI VfaProximitySphereHandle : public IVfaProximityHandle
{
public:
	VfaProximitySphereHandle(VflRenderNode *pRN);
	virtual ~VfaProximitySphereHandle();

	// redefine highlighting -> we need to highlight the handle's vis in here!
	virtual void SetIsHighlighted(bool b);

	void SetSphereRadius(float fRadius);
	float GetSphereRadius() const;

	void SetSphereCenter(const VistaVector3D &v3C);
	void SetSphereCenter(float fCenter[3]);
	void SetSphereCenter(double dCenter[3]);

	void GetSphereCenter(VistaVector3D &v3C) const;
	void GetSphereCenter(float fCenter[3]) const;
	void GetSphereCenter(double dCenter[3]) const;

	void SetHighlightLineWidth(float f);
	float GetHighlightLineWidth() const;

	void SetNormalLineWidth(float f);
	float GetNormalLineWidth() const;

	void SetHighlightColor(const VistaColor& color);
	VistaColor SetHighlightColor() const;
	void SetHighlightColor(float fC[4]);
	void GetHighlightColor(float fC[4]) const;

	void SetNormalColor(const VistaColor& color);
	VistaColor GetNormalColor() const;
	void SetNormalColor(float fC[4]);
	void GetNormalColor(float fC[4]) const;

	void SetVisible(bool b);
	bool GetVisible() const;

	virtual bool IsTouched(const VistaVector3D & v3Pos) const;

protected:
	
private:
	VfaSphereVis *m_pVis;

	float m_fNormalLW;
	float m_fHighlightLW;
	float m_fNormalColor[4];
	float m_fHighlightColor[4];
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaProximityBoxHandle_H
