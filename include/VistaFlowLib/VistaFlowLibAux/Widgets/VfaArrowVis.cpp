/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaArrowVis.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaArrowVis                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaArrowVis::VfaArrowVis()
:	m_fCylinderRadius(0.005f),
	m_fCylinderHeight(0.3f),
	m_fConeRadius(0.03f),
	m_fConeHeight(0.06f)
{
	m_v3Center = VistaVector3D(0.0, 0.0, 0.0);

	m_fRotate[0] = 0.0f;
	m_fRotate[1] = 0.0f;
	m_fRotate[2] = 0.0f;
	m_fRotate[3] = 1.0f;

	//blue
	m_fColor[0] = 0.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 1.0f;
	m_fColor[3] = 1.0f;

	quadratic = gluNewQuadric();

}

VfaArrowVis::~VfaArrowVis()
{
	gluDeleteQuadric(quadratic);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaArrowVis::DrawOpaque()
{

	glPushAttrib(GL_LIGHTING_BIT|GL_POLYGON_BIT|GL_TRANSFORM_BIT);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);


	if(m_bUseLighting)
	{
		glEnable(GL_LIGHTING);
		glDisable(GL_COLOR_MATERIAL);
		GLfloat fAmbient[] = {0.1f, 0.1f, 0.1f, 1.0f};
		GLfloat fSpecular[] = {1.0f,1.0f,1.0f,1.0f};
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,fAmbient);
		glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,m_fColor);
		glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,fSpecular);
		glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0);
	}
	else
	{
		glDisable(GL_LIGHTING);	
		glColor4fv(m_fColor);
	}

	glTranslatef(m_v3Center[0], m_v3Center[1], m_v3Center[2]);
	glRotatef(m_fRotate[0], m_fRotate[1], m_fRotate[2], m_fRotate[3]);
	gluQuadricNormals(quadratic, GLU_SMOOTH);
	gluQuadricTexture(quadratic, GL_TRUE);
	gluCylinder(quadratic, m_fCylinderRadius, m_fCylinderRadius, m_fCylinderHeight, 16, 16);
	glTranslatef(0.0, 0.0, m_fCylinderHeight);
	glutSolidCone(m_fConeRadius, m_fConeHeight, 16, 16);


	//glDisable(GL_LIGHTING);

	glPopMatrix();
	glPopAttrib();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaArrowVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaArrowVis::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaArrowVis::GetBounds(VistaBoundingBox &)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaArrowVis::CreateProperties() const
{
	return new IVflRenderable::VflRenderableProperties;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetArrowAttributes                                          */
/*                                                                            */
/*============================================================================*/
void VfaArrowVis::SetArrowAttributes(float f[4])
{
	m_fCylinderRadius = f[0];
	m_fCylinderHeight = f[1];
	m_fConeRadius = f[2];
	m_fConeHeight = f[3];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetArrowAttributes                                          */
/*                                                                            */
/*============================================================================*/
void VfaArrowVis::GetArrowAttributes(float f[4]) const
{
	f[0] = m_fCylinderRadius;
	f[1] = m_fCylinderHeight;
	f[2] = m_fConeRadius;
	f[3] = m_fConeHeight;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCylinderRadius                                           */
/*                                                                            */
/*============================================================================*/
void  VfaArrowVis::SetCylinderRadius(float fRad)
{
	m_fCylinderRadius = fRad;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCylinderRadius                                           */
/*                                                                            */
/*============================================================================*/
float VfaArrowVis::GetCylinderRadius()
{
	return m_fCylinderRadius;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCylinderHeight                                           */
/*                                                                            */
/*============================================================================*/
void  VfaArrowVis::SetCylinderHeight(float fHeight)
{
	m_fCylinderHeight = fHeight;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCylinderHeight                                           */
/*                                                                            */
/*============================================================================*/
float VfaArrowVis::GetCylinderHeight()
{
	return m_fCylinderHeight;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetConeRadius                                               */
/*                                                                            */
/*============================================================================*/
void  VfaArrowVis::SetConeRadius(float fRad)
{
	m_fConeRadius = fRad;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetConeRadius                                               */
/*                                                                            */
/*============================================================================*/
float VfaArrowVis::GetConeRadius()
{
	return m_fConeRadius;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetConeHeight                                               */
/*                                                                            */
/*============================================================================*/
void  VfaArrowVis::SetConeHeight(float fConeHeight)
{
	m_fConeHeight = fConeHeight;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetConeHeight                                               */
/*                                                                            */
/*============================================================================*/
float VfaArrowVis::GetConeHeight()
{
	return m_fConeHeight;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetCenter                                               */
/*                                                                            */
/*============================================================================*/
void VfaArrowVis::SetCenter(const VistaVector3D &v3Center)
{
	m_v3Center = v3Center;
}
void VfaArrowVis::GetCenter(VistaVector3D &v3Center) const
{
	v3Center = m_v3Center;
}
void VfaArrowVis::SetCenter(float fC[4])
{
	VistaVector3D v3(fC);
	m_v3Center = v3;
}
void VfaArrowVis::GetCenter(float fC[4]) const
{
	m_v3Center.GetValues(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRotate                                                   */
/*                                                                            */
/*============================================================================*/
void VfaArrowVis::SetRotate(const VistaVector3D &v3Normal)
{
	VistaVector3D vecZ(0.0,0.0,1.0);
	VistaQuaternion quaRotate(vecZ, v3Normal);
	VistaAxisAndAngle a = quaRotate.GetAxisAndAngle();

	m_fRotate[0] = a.m_fAngle*180.0f/Vista::Pi;
 	m_fRotate[1] = a.m_v3Axis[0];
	m_fRotate[2] = a.m_v3Axis[1];
	m_fRotate[3] = a.m_v3Axis[2];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRotate                                                   */
/*                                                                            */
/*============================================================================*/
void VfaArrowVis::GetRotate(float fRotate[4]) const
{
	fRotate[0] = m_fRotate[0];
	fRotate[1] = m_fRotate[1];
	fRotate[2] = m_fRotate[2];
	fRotate[3] = m_fRotate[3];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetColor                                                */
/*                                                                            */
/*============================================================================*/
void VfaArrowVis::SetColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);

	m_fColor[0] = fColor[0];
	m_fColor[1] = fColor[1];
	m_fColor[2] = fColor[2];
	m_fColor[3] = fColor[3];
}
VistaColor VfaArrowVis::GetColor() const
{
	VistaColor color(m_fColor);
	return color;
}

void VfaArrowVis::SetColor(float fColor[4])
{
	m_fColor[0] = fColor[0];
	m_fColor[1] = fColor[1];
	m_fColor[2] = fColor[2];
	m_fColor[3] = fColor[3];
}
void VfaArrowVis::GetColor(float fColor[4]) const
{
	fColor[0] = m_fColor[0];
	fColor[1] = m_fColor[1];
	fColor[2] = m_fColor[2];
	fColor[3] = m_fColor[3];
}

/*============================================================================*/
/*  NAME      :   Set/GetUseLighting                                          */
/*============================================================================*/
bool VfaArrowVis::SetUseLighting(bool b)
{
	if(m_bUseLighting == b)
		return false;

	m_bUseLighting = b;
	return true;
}

bool VfaArrowVis::GetUseLighting() const
{
	return m_bUseLighting;
}

/*============================================================================*/
/*  NAME      :   SetPointfromTo                                              */
/*============================================================================*/
void VfaArrowVis::SetPointFromTo( const VistaVector3D &pntFrom, const VistaVector3D &pntTo, bool autoSize )
{
	m_v3Center = pntFrom;
	const float l = (pntTo-pntFrom).GetLength();

	SetRotate(pntTo-pntFrom);
	SetCylinderHeight(l - GetConeHeight());

	if(autoSize)
	{
		// m_fCylinderRadius(0.005f),
		// m_fCylinderHeight(0.3f),
	    // m_fConeRadius(0.03f),
		// m_fConeHeight(0.06f)
		SetCylinderRadius(l * 0.005f/0.3f);
		SetConeRadius    (l *  0.03f/0.3f);
		SetConeHeight    (l *  0.06f/0.3f);
		SetCylinderHeight(l - GetConeHeight());
	}
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaArrowVis.cpp"		         						      */
/*============================================================================*/
