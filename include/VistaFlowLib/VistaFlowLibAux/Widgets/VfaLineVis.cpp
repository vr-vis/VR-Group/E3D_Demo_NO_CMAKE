/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <GL/glew.h>

#include "VfaLineVis.h"
#include "Line/VfaLineModel.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <cassert>
#include <cstring>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaLineVis                                           */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaLineVis::VfaLineVis()
	:	m_pModel(NULL)
{}

VfaLineVis::VfaLineVis(VfaLineModel *pModel)
:	m_pModel(pModel)
{}

VfaLineVis::~VfaLineVis()
{}

/*============================================================================*/
/*  NAME      :   GetModel                                                    */
/*============================================================================*/
VfaLineModel *VfaLineVis::GetModel() const
{
	return m_pModel;
}


/*============================================================================*/
/*  NAME      :   DrawOpaque                                                  */
/*============================================================================*/
void VfaLineVis::DrawOpaque()
{
	VfaLineVis::VfaLineVisProps *pProps = dynamic_cast<VfaLineVisProps*>(
		this->GetProperties());

	if(pProps == NULL || !pProps->GetVisible())
		return;

	glPushAttrib(GL_ENABLE_BIT 	// Lighting, depth test & blend flags.
		| GL_LINE_BIT 		// Line width.
		| GL_COLOR_BUFFER_BIT);	// Color value.

	glDisable(GL_LIGHTING);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	glLineWidth(pProps->GetWidth());

	float fC[4];
	pProps->GetColor(fC);
	glColor4fv(fC);
	
	float fPoint1[3], fPoint2[3];
	m_pModel->GetPoint1(fPoint1);
	m_pModel->GetPoint2(fPoint2);
	
	glBegin(GL_LINES);
		glVertex3f(fPoint1[0], fPoint1[1], fPoint1[2]);
		glVertex3f(fPoint2[0], fPoint2[1], fPoint2[2]);
	glEnd();

	glPopAttrib();
}
/*============================================================================*/
/*  NAME      :   GetRegistrationMode                                         */
/*============================================================================*/
unsigned int VfaLineVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*  NAME      :   GetProperties                                               */
/*============================================================================*/
VfaLineVis::VfaLineVisProps *VfaLineVis::GetProperties() const
{
	return static_cast<VfaLineVis::VfaLineVisProps *>(IVflRenderable::GetProperties());
}

/*============================================================================*/
/*  NAME      :   CreateProperties                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaLineVis::CreateProperties() const
{
	return new VfaLineVis::VfaLineVisProps;
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaLineVis::VfaLineVisProps                            */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaLineVis::VfaLineVisProps");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<VistaColor, VfaLineVis::VfaLineVisProps>(
	"COLOR", STR_REF_TYPENAME,
	&VfaLineVis::VfaLineVisProps::GetColor),
	new TVistaPropertyGet<float, VfaLineVis::VfaLineVisProps>(
	"LINE_WIDTH", STR_REF_TYPENAME,
	&VfaLineVis::VfaLineVisProps::GetWidth),
	NULL
};
//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<const VistaColor &, VistaColor, VfaLineVis::VfaLineVisProps>(
	"COLOR", STR_REF_TYPENAME,
	&VfaLineVis::VfaLineVisProps::SetColor),
	new TVistaPropertySet<float, float, VfaLineVis::VfaLineVisProps>(
	"LINE_WIDTH", STR_REF_TYPENAME,
	&VfaLineVis::VfaLineVisProps::SetWidth),
	NULL 
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaLineVis::VfaLineVisProps::VfaLineVisProps()
:	m_fLineWidth(0.5f)
{
	//blue border
	m_fColor[0] = 0.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 1.0f;
}
VfaLineVis::VfaLineVisProps::~VfaLineVisProps()
{}
/*============================================================================*/
/*  NAME      :   Set/GetWidth                                                */
/*============================================================================*/
bool VfaLineVis::VfaLineVisProps::SetWidth(float fLineWidth)
{
	if(m_fLineWidth == fLineWidth)
		return false;

	m_fLineWidth = fLineWidth;
	this->Notify(MSG_LINE_WIDTH_CHG);
	return true;
}
float VfaLineVis::VfaLineVisProps::GetWidth() const
{
	return m_fLineWidth;
}

/*============================================================================*/
/*  NAME      :   Set/GetColor                                                */
/*============================================================================*/
bool VfaLineVis::VfaLineVisProps::SetColor(float fColor[4])
{
	if(	fColor[0] == m_fColor[0] &&
		fColor[1] == m_fColor[1] &&
		fColor[2] == m_fColor[2] &&
		fColor[3] == m_fColor[3])
	{
		return false;
	}

	memcpy(m_fColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHG);
	return true;
}
bool VfaLineVis::VfaLineVisProps::SetColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	return this->SetColor(fColor);
}

void VfaLineVis::VfaLineVisProps::GetColor(float fColor[4]) const
{
	memcpy(fColor, m_fColor, 4*sizeof(float));
}
VistaColor VfaLineVis::VfaLineVisProps::GetColor() const
{
	return VistaColor(m_fColor);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaLineVis::VfaLineVisProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaLineVis::VfaLineVisProps::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaLineVis.cpp"                                              */
/*============================================================================*/



