/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef __VFABOXRANGESELECTMEDIATOR_H
#define __VFABOXRANGESELECTMEDIATOR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaBoxModel;
class VfaSphereModel;
class VfaSphereController;
class VfaDragBoxController;
class VfaTrajectoryRangeSelect;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* The VfaBoxRangeSelectMediator mediates between a box widget and a VfaTrajectoryRangeSelect.
* If your box widget is a VfaDragBoxWidget, you can use the widget's controller's state information to avoid
* unnecessary updated while resizing the box.
*
* The box defined by the widget is mediated into the bounding box search of VfaTrajectoryRangeSelect.
*/
class VISTAFLOWLIBAUXAPI VfaBoxRangeSelectMediator : VflObserver
{
public:

	VfaBoxRangeSelectMediator(VfaTrajectoryRangeSelect *pRangeSelect, VfaBoxModel* pWidgetModel, VfaDragBoxController* pDragBoxController = NULL);
	virtual ~VfaBoxRangeSelectMediator();

	// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);


private:
	VfaBoxModel* m_pWidgetModel;
	VfaTrajectoryRangeSelect* m_pRangeSelect;

	// optional dragbox controller
	VfaDragBoxController* m_pDragBoxController;
}; //end of local class


/**
* The VfaSphereRangeSelectMediator mediates between a sphere widget and a VfaTrajectoryRangeSelect.
* 
* The sphere defined by the widget is mediated into the spherical search of VfaTrajectoryRangeSelect.
*
* @todo This should either be moved to an own file or removed as deprecated.
*/
class VISTAFLOWLIBAUXAPI VfaSphereRangeSelectMediator : VflObserver
{
public:

	VfaSphereRangeSelectMediator(VfaTrajectoryRangeSelect *pRangeSelect, VfaSphereModel* pWidgetModel, VfaSphereController* pWidgetController);
	virtual ~VfaSphereRangeSelectMediator();

	// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);


private:
	VfaSphereModel* m_pWidgetModel;
	VfaSphereController* m_pWidgetController;

	VfaTrajectoryRangeSelect* m_pRangeSelect;

	bool	m_bChanged;
}; //end of local class

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFABOXRANGESELECTMEDIATOR_H
