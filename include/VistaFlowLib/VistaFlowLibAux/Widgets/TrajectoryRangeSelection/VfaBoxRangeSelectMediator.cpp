/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaBoxRangeSelectMediator.h"
#include "VfaTrajectoryRangeSelect.h"
#include <VistaFlowLibAux/Widgets/Box/VfaBoxModel.h>
#include <VistaFlowLibAux/Widgets/Box/VfaDragBoxController.h>
#include <VistaFlowLibAux/Widgets/Sphere/VfaSphereModel.h>
#include <VistaFlowLibAux/Widgets/Sphere/VfaSphereController.h>
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaBoxRangeSelectMediator::VfaBoxRangeSelectMediator(VfaTrajectoryRangeSelect *pRangeSelect, VfaBoxModel* pWidgetModel, VfaDragBoxController* pDragBoxController )
:VflObserver(),
m_pWidgetModel(pWidgetModel),
m_pRangeSelect(pRangeSelect),
m_pDragBoxController(pDragBoxController)
{
	this->Observe (m_pWidgetModel);
}

VfaBoxRangeSelectMediator::~VfaBoxRangeSelectMediator()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaBoxRangeSelectMediator::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if (pObserveable==m_pWidgetModel)
	{
		if (msg == VfaBoxModel::MSG_BOX_CHANGE)
		{
			if (m_pDragBoxController && (m_pDragBoxController->GetIsInteracting()))
				return;

			float fBounds[6];
			m_pWidgetModel->GetAABox (fBounds);
			m_pRangeSelect->SearchTrajectoriesWithinBounds(fBounds);
		}
	}
}


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaSphereRangeSelectMediator::VfaSphereRangeSelectMediator(VfaTrajectoryRangeSelect *pRangeSelect, VfaSphereModel* pWidgetModel, VfaSphereController* pWidgetController)
:VflObserver(),
m_pWidgetModel(pWidgetModel),
m_pWidgetController(pWidgetController),
m_pRangeSelect(pRangeSelect),
m_bChanged(false)
{
	this->Observe (m_pWidgetModel);
	this->Observe (m_pWidgetController);
}

VfaSphereRangeSelectMediator::~VfaSphereRangeSelectMediator()
{
	this->ReleaseObserveable (m_pWidgetModel);
	this->ReleaseObserveable (m_pWidgetController);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaSphereRangeSelectMediator::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if (pObserveable==m_pWidgetModel)
	{
		if ((msg == VfaSphereModel::MSG_CENTER_CHANGE)||(msg == VfaSphereModel::MSG_RADIUS_CHANGE)
			||(msg == VfaSphereModel::MSG_ORIENTATION_CHANGE))
		{
			//m_pRangeSelect->SetBoxBounds (m_pWidgetModel->GetVisMin(), m_pWidgetModel->GetVisMax());
			//VistaVector3D v3Center;
			//m_pWidgetModel->GetVisCenter (v3Center);
			//m_pRangeSelect->SearchTrajectoriesWithinRadius(v3Center, m_pWidgetModel->GetRadius());
			m_bChanged = true;
		}
	}
	if (pObserveable==m_pWidgetController)
	{
		if ((msg == VfaSphereController::WIDGETCONTROLLER_STATECHANGE)&&
			(m_pWidgetController->GetControllerState()==IVfaWidgetController::CS_NONE)&&
			m_bChanged)
		{
			VistaVector3D v3Center;
			m_pWidgetModel->GetCenter (v3Center);
			m_pRangeSelect->SearchTrajectoriesWithinRadius(v3Center, m_pWidgetModel->GetRadius());
			m_bChanged = false;
		}
	}

}
/*============================================================================*/
/*  END OF FILE "VtnBoxRangeSelectMediator.cpp"                               */
/*============================================================================*/


