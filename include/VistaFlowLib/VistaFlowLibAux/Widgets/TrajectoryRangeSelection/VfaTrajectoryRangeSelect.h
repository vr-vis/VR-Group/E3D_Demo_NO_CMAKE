/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFATRAJECTORYRANGESELECT_H
#define _VFATRAJECTORYRANGESELECT_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaBase/VistaVectorMath.h>
#include <vector>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include "VistaVisExt/Data/VveParticlePopulation.h"
#include <fstream>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisTiming;
class VveTimeMapper;
class VveKdTree;
class VveKdTreeResultVector;
class VfaTrajectoryInfo;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * This class provides several selection queries on a set of particles.
 * Given a VveParticleData, you can either query a bounding box (@see SearchTrajectoriesWithinBounds)
 * or a sphere (@see SearchTrajectoriesWithinRadius ).
 *
 * The result is a set of time intervals (in simulation time) during which particles move through the
 * specified spatial region (box or sphere).
 *
 * @todo This code is currently not working, as the VflVisTiming has no possibility to manage
 * multiple time intervals. This was done using VflModules/VflTimeNavigation/VflTimeNavigationModel before,
 * which was removed due to the integration of this class into VistaFlowLibAux in order to not introduce
 * a dependency to an optional module.
 */
class VISTAFLOWLIBAUXAPI VfaTrajectoryRangeSelect : public VflObserver
{
public:
	VfaTrajectoryRangeSelect (VflVisTiming *pVisTiming, VfaTrajectoryInfo* pTrajectoryView);
	virtual ~VfaTrajectoryRangeSelect();

	
	/**
	 * Method:      SetParticleData
	 *
	 * Set particle data as data basis. This will trigger the construction of
	 * a search tree for more efficient access.
	 *
	 * @param       pParticleData
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void SetParticleData (VveParticleDataCont* pParticleData);
	VveParticleDataCont	* GetParticleData () const;

	
	/**
	 * Method:      SearchTrajectoriesWithinBounds
	 *
	 * Search and select trajectories within axis-aligned bounds.
	 *
	 * @param       fBounds
	 * @return      bool
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool SearchTrajectoriesWithinBounds(float fBounds[6]);


	
	/**
	 * Method:      SearchTrajectoriesWithinRadius
	 *
	 * Search and select trajectories within sphere.
	 *
	 * @param       vecCenter
	 * @param       fRadius
	 * @return      bool
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool SearchTrajectoriesWithinRadius(const VistaVector3D & vecCenter, float fRadius);

	// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

protected:
	void BuildSearchTree ();

	
	/**
	 * Method:      ComputeRanges
	 *
	 * Compute temporal intervals based on spatial results and set as marked intervals 
	 * to the time model.
	 *
	 * @param       vecResults
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void ComputeRanges (const VveKdTreeResultVector & vecResults);

private:
	VflVisTiming *				m_pVisTiming;
	VfaTrajectoryInfo*			m_pTrajectoryView;

	VveParticleDataCont*		m_pParticleData;
	VveTimeMapper*				m_pTimeMapper;

	VveKdTree					*m_pKdTree;
	int							m_iTreeDimensions;
	float*						m_pRawData;
	float*						m_pTimeData;
	int*						m_pTrajectoryOffsets;
	float						m_fLargestDistanceBetweenParticles;

	std::vector<float>			m_vecIntervalPoints;

	VistaVector3D				m_vecMinBounds, m_vecMaxBounds;
	std::vector< std::set<int> > m_vecIntervalsToTrajectories;

	std::ofstream		        m_oSearchTree;
	std::ofstream		        m_oBuildTree;

};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VTNTRAJECTORYRANGESELECT_H



