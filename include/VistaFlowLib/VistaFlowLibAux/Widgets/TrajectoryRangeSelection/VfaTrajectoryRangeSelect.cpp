/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here
#include "VfaTrajectoryRangeSelect.h" 
#include "../TrajectoryDragging/VfaTrajectoryInfo.h" 

#include <VistaFlowLib/Visualization/VflVisTiming.h>

#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Data/VveKdTree.h>
#include <VistaMath/VistaBoundingBox.h>

#include <fstream>
#include "VistaVisExt/Data/VveParticleTrajectory.h"
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LOCAL FUNCTION                                              */
/*                                                                            */
/*============================================================================*/

bool lessDistanceFunction(const SKdtreeResult& lhs, const SKdtreeResult& rhs) 
{
	return lhs.fDistance < rhs.fDistance;
}
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaTrajectoryRangeSelect::VfaTrajectoryRangeSelect(VflVisTiming *pVisTiming, VfaTrajectoryInfo* pTrajectoryView)
:m_pVisTiming(pVisTiming),
m_pTimeMapper(NULL),
m_pParticleData(NULL),
m_pKdTree(NULL),
m_pRawData(NULL),
m_pTimeData(NULL),
m_pTrajectoryOffsets(NULL),
m_iTreeDimensions(3),
m_pTrajectoryView(pTrajectoryView)
{
	this->Observe (m_pVisTiming);
	//m_oBuildTree.open ("buildtree.txt");
	//m_oSearchTree.open ("searchtree.txt");
}

VfaTrajectoryRangeSelect::~VfaTrajectoryRangeSelect()
{
	this->ReleaseObserveable(m_pVisTiming);
	//m_oBuildTree.close();
	//m_oSearchTree.close();
	// cleanup
	delete [] m_pRawData;
	delete m_pKdTree;
	delete [] m_pTrajectoryOffsets;
	delete [] m_pTimeData;

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetParticleData                                             */
/*                                                                            */
/*============================================================================*/
void VfaTrajectoryRangeSelect::SetParticleData (VveParticleDataCont* pParticleData)
{
	m_pTimeMapper = pParticleData->GetTimeMapper();
	m_pParticleData = pParticleData;
	
	this->BuildSearchTree();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetParticleData                                             */
/*                                                                            */
/*============================================================================*/
VveParticleDataCont	* VfaTrajectoryRangeSelect::GetParticleData () const
{
	return m_pParticleData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   BuildSearchTree                                             */
/*                                                                            */
/*============================================================================*/
void VfaTrajectoryRangeSelect::BuildSearchTree ()
{
	if (!m_pParticleData)
		return;

	// clean up 
	delete [] m_pRawData;
	delete m_pKdTree;
	delete[] m_pTrajectoryOffsets;
	delete[] m_pTimeData;

	VveParticlePopulation *pPopulation = m_pParticleData->GetData()->GetData();

	// number of points in all trajectories
	int iPathlineCount = pPopulation->GetNumTrajectories();
	int iCount = 0;
	for (int j=0; j < iPathlineCount; ++j)
	{
		iCount += static_cast<int>(pPopulation->GetTrajectory(j)->GetSize());
	}

	vstr::outi() << "[VfaTrajectoryRangeSelect] Searching over "<<iCount<<" points\n";

	m_pRawData = new float [iCount*m_iTreeDimensions];
	m_pTrajectoryOffsets = new int [iPathlineCount+1];
	m_pTimeData = new float [iCount];

	// used as offset to access m_pRawData and m_pTimeData
	int iNumPoints = 0; 
	
	// used to calculate maximum time differents between paticle positions
	m_fLargestDistanceBetweenParticles = 0.0f;

	// iterate particle trajectories and copy position and time data
	for (int j=0; j < iPathlineCount; ++j)
	{
		VveParticleTrajectory *pTraj = pPopulation->GetTrajectory(j);
		VveParticleDataArrayBase *pPos = NULL, *pTime = NULL;

		if (pTraj &&
			pTraj->GetArrayByName(VveParticlePopulation::sPositionArrayDefault, pPos) && pPos &&
			pTraj->GetArrayByName(VveParticlePopulation::sTimeArrayDefault, pTime) && pTime	)
		{
			m_pTrajectoryOffsets[j] = iNumPoints;

			float fTime, fTimePrev;
			for (size_t i=0; i < pTraj->GetSize(); ++i, iNumPoints++ )
			{
				pPos->GetElementCopy(i, &m_pRawData[iNumPoints*m_iTreeDimensions], 3);
				fTime = pTime->GetElementValueAsFloat(i);
				m_pTimeData[iNumPoints] = fTime;

				if (i>0)
				{
					float fDistance = fTime - fTimePrev;
					if (fDistance > m_fLargestDistanceBetweenParticles)
						m_fLargestDistanceBetweenParticles = fDistance;
				}

				// Remember current time for the next iteration
				fTimePrev = fTime;
			}
		} 
		else
		{
			vstr::errp() << "[VfaTrajectoryRangeSelect] Particle data arrays named "
					  << "\'" << VveParticlePopulation::sPositionArrayDefault << "\'" << " or "
					  << "\'" << VveParticlePopulation::sTimeArrayDefault << "\'" 
					  << " could not be found in current particle trajectory!";
	}

	}
	m_pTrajectoryOffsets[iPathlineCount] = iNumPoints+1;

	vstr::outi() << "[VfaTrajectoryRangeSelect] Largest time difference between two positions "<<m_fLargestDistanceBetweenParticles<<"\n";

	m_pKdTree = new VveKdTree (m_pRawData, iCount, m_iTreeDimensions, false);

	//m_oBuildTree << iCount <<", "<<"\n";
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SearchTrajectoriesWithinBounds                              */
/*                                                                            */
/*============================================================================*/
bool VfaTrajectoryRangeSelect::SearchTrajectoriesWithinBounds(float fBounds[6])
{
	float fRadius = 0.0f;

	for (int j=0; j < 3; ++j)
	{
		if (fabs(fBounds[2*j+1]-fBounds[2*j])/2 > fRadius)
			fRadius = fabs(fBounds[2*j+1]-fBounds[2*j])/2;
	}

	VistaVector3D v3Center (fBounds[0]+ (fBounds[1]-fBounds[0])/2, fBounds[2]+ (fBounds[3]-fBounds[2])/2, fBounds[4]+ (fBounds[5]-fBounds[4])/2);
	
	// build query vector
	std::vector<float> qv;
	qv.resize(3);
	for (int j=0; j < 3; j++)
	{
		qv[j] = v3Center[j];
	}

	// search for nearest within radius (request needs r^2)
	VveKdTreeResultVector oResult;
	m_pKdTree->GetNearestByRadius (qv, fRadius*fRadius, oResult);

	// now, discard all solutions out of BB
	float fMin[3] = {fBounds[0],fBounds[2],fBounds[4]};
	float fMax[3] = {fBounds[1],fBounds[3],fBounds[5]};
	VistaBoundingBox bb (fMin, fMax);
	VveKdTreeResultVector oResultsWithinBox;
	for (size_t j=0; j < oResult.size(); ++j)
	{
		if (bb.Intersects (&m_pRawData[oResult[j].iIndex*m_iTreeDimensions]))
			oResultsWithinBox.push_back(oResult[j]);

	}
	this->ComputeRanges (oResultsWithinBox);

	//m_oSearchTree << "\n";

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SearchTrajectoriesWithinRadius                              */
/*                                                                            */
/*============================================================================*/

bool VfaTrajectoryRangeSelect::SearchTrajectoriesWithinRadius(const VistaVector3D & vecCenter, float fRadius)
{
	// build query vector
	std::vector<float> qv;
	qv.resize(3);
	for (int j=0; j < 3; j++)
	{
		qv[j] = vecCenter[j];
	}

	// search for nearest within radius (request needs r^2)
	VveKdTreeResultVector oResult;
	m_pKdTree->GetNearestByRadius (qv, fRadius*fRadius, oResult);

	this->ComputeRanges (oResult);

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeRanges					                              */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryRangeSelect::ComputeRanges (const VveKdTreeResultVector & vecResults)
{
	// make local copy
	VveKdTreeResultVector oResult = vecResults;

	// save simtime of particles in oResult instead of spatial distance
	std::set<int> setTouchedTrajectories;

	for (size_t j=0; j < oResult.size(); ++j)
	{
		//int k=0;
		//while (oResult[j].iIndex>= m_pTrajectoryOffsets[k])
		//	k++;
		//k = k-1;
		//setTouchedTrajectories.insert (k);
		// replace distance with time value
		oResult[j].fDistance = m_pTimeData[oResult[j].iIndex];
	}
	// sort results according to distance
	std::sort (oResult.begin(), oResult.end(), lessDistanceFunction);

	m_vecIntervalsToTrajectories.clear();
	if (oResult.size()>0)
	{
		float fMinTime = m_pTimeData[oResult.front().iIndex];
		float fMaxTime = m_pTimeData[oResult.back().iIndex];

		int iNumIntervals = 0;
		float fMaxDistance = m_fLargestDistanceBetweenParticles;
		
		std::set<int> setTrajectories;
		m_vecIntervalPoints.clear();
		m_vecIntervalPoints.push_back(fMinTime);
		
		int k=0;
		while (oResult[0].iIndex>= m_pTrajectoryOffsets[k])
			k++;
		setTrajectories.insert (std::set<int>::value_type(k-1));
		iNumIntervals++;
		for (size_t i=1; i < oResult.size(); ++i)
		{
			k=0;
			while (oResult[i].iIndex>= m_pTrajectoryOffsets[k])
				k++;
			setTrajectories.insert (std::set<int>::value_type(k-1));
			//if (oResult[i].iIndex-oResult[i-1].iIndex > 1)
			//{
			//	if (find (setTouchedTrajectories.begin(), setTouchedTrajectories.end(), oResult[i].iIndex)==setTouchedTrajectories.end())
			//	{
			if (oResult[i].fDistance-oResult[i-1].fDistance > fMaxDistance)
			{
				iNumIntervals++;
				m_vecIntervalPoints.push_back(oResult[i-1].fDistance);
				m_vecIntervalPoints.push_back(oResult[i].fDistance);
				m_vecIntervalsToTrajectories.push_back (setTrajectories);
				setTrajectories.clear();
			}
		}
		
		m_vecIntervalPoints.push_back(fMaxTime);
		m_vecIntervalsToTrajectories.push_back (setTrajectories);

		vstr::outi() << "[VfaTrajectoryRangeSelect] Found "<<oResult.size()<<" results ("<< iNumIntervals<<" intervals), min time "<<fMinTime<<" max time "<< fMaxTime <<"\n";
		if (m_vecIntervalPoints.size()>0)
		{
			// DEPRECATED!
			// here, we not some manager for multiple time intervals
			// clear all old intervals
			// m_pTimeNavigation->ClearVisTimeIntervals();

			//vstr::outi() << "[VfaTrajectoryRangeSelect] Intervals ";
			//for (int i=0; i < m_vecIntervalPoints.size(); i+=2)
			//{
			//	// add new intervals
			//	CVtnTimeNavigationModel::sTimeInterval oInterval;
			//	oInterval.dStartTime = m_pTimeMapper->GetVisualizationTime (m_vecIntervalPoints[i]);
			//	if (oInterval.dStartTime<0)
			//		oInterval.dStartTime = 0.0;
			//	oInterval.dEndTime = m_pTimeMapper->GetVisualizationTime (m_vecIntervalPoints[i+1]);
			//	if (oInterval.dEndTime<0)
			//		oInterval.dEndTime = 1.0;
			//	m_pTimeNavigation->AddVisTimeInterval (oInterval);
			//	vstr::outi() << "[" << m_vecIntervalPoints[i] << ", "<<m_vecIntervalPoints[i+1]<<"];";
			//}
			//vstr::outi() << "\n";

			// DEPRECATED!
			// here, we not some manager for multiple time intervals
			// activate first interval...this exists, as we have interval points
			//m_pTimeNavigation->SetVisTimeRangeByInterval (0);
		}
	}
	else
	{
		// DEPRECATED!
		// here, we not some manager for multiple time intervals
		// clear all old intervals
		//m_pTimeNavigation->ClearVisTimeIntervals();
		m_pVisTiming->SetVisTimeRange (0,1);
	}
}

void VfaTrajectoryRangeSelect::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
//	if ((msg == CVtnTimeNavigationModel::E_MSG_INTERVALCHANGE)||(msg == VflVisTiming::MSG_RANGE_CHG))
//	{
//		int iIntervalId = m_pTimeNavigation->GetCurrentVisTimeInterval();
//		if ((iIntervalId>=0) && (iIntervalId<m_vecIntervalsToTrajectories.size()))
//		{
//			std::vector<int> vecParticleIds;
//			vecParticleIds.assign (m_vecIntervalsToTrajectories[iIntervalId].begin(), m_vecIntervalsToTrajectories[iIntervalId].end());
//			m_pTrajectoryView->SetColoredIntervals(m_vecIntervalPoints, iIntervalId);
////			m_pTrajectoryView->SetDrawMode (VflTrajectoryInfo::TRAJECTORY_COLORTEMPORALREGIONS);
//			m_pTrajectoryView->SelectTrajectories(vecParticleIds);
//		}
//		else
//			m_pTrajectoryView->Reset();
//	}
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "          "                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

