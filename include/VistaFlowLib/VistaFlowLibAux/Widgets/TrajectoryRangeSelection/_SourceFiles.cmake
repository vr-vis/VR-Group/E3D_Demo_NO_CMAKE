

set( RelativeDir "./Widgets/TrajectoryRangeSelection" )
set( RelativeSourceGroup "Source Files\\Widgets\\TrajectoryRangeSelection" )

set( DirFiles
	VfaBoxRangeSelectMediator.cpp
	VfaBoxRangeSelectMediator.h
	VfaTrajectoryRangeSelect.cpp
	VfaTrajectoryRangeSelect.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

