/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAQUADVIS_H
#define _VFAQUADVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaPlaneModel;
class VistaTexture;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Simple Plane style visualization for showing a Plane which is supposed
 * to be defined by the client class.
 */
class VISTAFLOWLIBAUXAPI VfaQuadVis : public IVflRenderable
{
public: 
	VfaQuadVis(VfaPlaneModel *pModel);
	virtual ~VfaQuadVis();

	virtual VfaPlaneModel *GetModel() const;

	/**
	 * Provide a texture for rendering on the quad.
	 * Per default, texture coords will be (0.0,0.0) to (1.0,1.0) for
	 * the plane. Moreover, the texture will replace any color
	 * set for the plane!
	 *
	 * @todo Add interface for texture coords if needed.
	 */
	virtual void SetTexture(VistaTexture *pTex);
	virtual VistaTexture *GetTexture() const;

	virtual void DrawOpaque();

	virtual void DrawTransparent();

	virtual unsigned int GetRegistrationMode() const;

	class VISTAFLOWLIBAUXAPI VfaQuadVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		enum{
			MSG_COLOR_CHANGED = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_OPACITY_CHANGED,
			MSG_LINE_WIDTH_CHANGED,
			MSG_DRAW_BORDER_CHANGED,
			MSG_FILL_PLANE_CHANGED,
			MSG_LAST
		};
		VfaQuadVisProps();
		virtual ~VfaQuadVisProps();

		bool SetLineColor(float fColor[4]);
		bool SetLineColor(const VistaColor& color);
		void GetLineColor(float fColor[4]) const;
		VistaColor GetLineColor() const;

		bool SetFillColor(float fColor[4]);
		bool SetFillColor(const VistaColor& color);
		void GetFillColor(float fColor[4]) const;
		VistaColor GetFillColor() const;

		bool SetOpacity(float fAlpha);
		float GetOpacity() const;

		bool SetLineWidth(float fLineWidth);
		float GetLineWidth() const;

		bool SetDrawBorder(bool b);
		bool GetDrawBorder() const;
	
		bool SetFillPlane(bool b);
		bool GetFillPlane() const;

		void SetTextureCoordinates(float fMinS, float fMaxS, float fMinT, float fMaxT);
		void GetTextureCoordinates(float& fMinS, float& fMaxS, float& fMinT, float& fMaxT);
		void SetTextureCoordinates(float fCoords[4]);
		void GetTextureCoordinates(float fCoords[4]);

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		float	m_fLineColor[4];
		float	m_fFillColor[4];
		float	m_fOpacity;
		float	m_fLineWidth;
		bool m_bDrawBorder;
		bool m_bDrawFilled;

		float	m_fTexCoordsMinS;
		float	m_fTexCoordsMinT;
		float	m_fTexCoordsMaxS;
		float	m_fTexCoordsMaxT;

	};

	virtual VfaQuadVisProps *GetProperties() const;

protected:
	VfaQuadVis();

	virtual VflRenderableProperties* CreateProperties() const;

	void DrawQuad();


private:
	VfaPlaneModel *m_pModel;

	VistaTexture *m_pTexture;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaQuadVis_H
