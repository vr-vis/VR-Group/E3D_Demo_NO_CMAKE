/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES														              */
/*============================================================================*/
#include "VfaRaycastFocusStrategy.h"
#include "VfaRaycastHandle.h"
#include "VfaWidgetController.h"

#include <VistaAspects/VistaTransformable.h>
#include <VistaBase/VistaVectorMath.h>

using namespace std;


/*============================================================================*/
/* IMPLEMENTATION												              */
/*============================================================================*/
VfaRaycastFocusStrategy::VfaRaycastFocusStrategy()
{ }


VfaRaycastFocusStrategy::~VfaRaycastFocusStrategy()
{ }


bool VfaRaycastFocusStrategy::EvaluateFocus(
	std::vector<IVfaControlHandle*> &vecCtrlHandles)
{
	vecCtrlHandles.clear();

	std::map<float, IVfaControlHandle*> mapHandles;

	m_oPointer.qOrientation.Normalize();
	m_oPointer.v3Position[3] = 1.0f;

	// Transform direction from local pointer to global frame.
	VistaVector3D v3RayDir	= m_oPointer.qOrientation.Rotate(
		VistaVector3D(0.0f, 0.0f, -1.0f, 0.0f));
	v3RayDir.Normalize();
	v3RayDir[3] = 0.0f;

	float fParam = 0.0f;
	list<IVfaRaycastHandle*>::const_iterator li_cit = m_liHandles.begin();

	while(li_cit != m_liHandles.end())
	{
		// Test for intersection, and if an intersection was detected, sort the
		// result into a map.
		if((*li_cit)->Intersects(m_oPointer.v3Position, v3RayDir, fParam))
		{
			mapHandles.insert(pair<float, IVfaControlHandle*>(fParam, (*li_cit)));
		}

		++li_cit;
	}

	map<float, IVfaControlHandle*>::const_iterator map_cit = mapHandles.begin();

	// Copy result into vector.
	while(map_cit != mapHandles.end())
	{
		vecCtrlHandles.push_back((*map_cit).second);
		++map_cit;
	}

	return true;
}


void VfaRaycastFocusStrategy::RegisterSelectable(
	IVfaWidgetController* pSelectable)
{
	if(!pSelectable)
		return;

	IVfaRaycastHandle *pCasted = 0;

	// Loop over all handles and try to add them.
	for(size_t i=0; i<pSelectable->GetControlHandles().size(); ++i)
	{
		// We are only interested in raycast handles.
		pCasted = dynamic_cast<IVfaRaycastHandle*>(
			pSelectable->GetControlHandles()[i]);
		
		if(pCasted)
			m_liHandles.push_back(pCasted);
	};
}

void VfaRaycastFocusStrategy::UnregisterSelectable(
	IVfaWidgetController* pSelectable)

{
	if(!pSelectable)
		return;

	IVfaRaycastHandle *pCasted = 0;

	// Loop over all handles and try to add them.
	for(size_t i=0; i<pSelectable->GetControlHandles().size(); ++i)
	{
		// We are only interested in raycast handles.
		pCasted = dynamic_cast<IVfaRaycastHandle*>(
			pSelectable->GetControlHandles()[i]);

		if(pCasted)
			m_liHandles.remove(pCasted);
	};
}


void VfaRaycastFocusStrategy::OnSensorSlotUpdate(int iSlot,
	const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	if(iSlot != VfaApplicationContextObject::SLOT_POINTER_WORLD)
		return;

	m_oPointer = oFrameData;
}

void VfaRaycastFocusStrategy::OnCommandSlotUpdate(int iSlot,
	const bool bSet)
{ }

void VfaRaycastFocusStrategy::OnTimeSlotUpdate(const double dTime)
{ }


int VfaRaycastFocusStrategy::GetSensorMask() const
{
	return (1 << VfaApplicationContextObject::SLOT_POINTER_WORLD);
}

int VfaRaycastFocusStrategy::GetCommandMask() const
{
	return 0;
}

bool VfaRaycastFocusStrategy::GetTimeUpdate() const
{
	return false;
}


/*============================================================================*/
/* END OF FILE													              */
/*============================================================================*/
