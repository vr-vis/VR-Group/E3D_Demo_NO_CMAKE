/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef __VFATEXTUREDBOXVIS_H
#define __VFATEXTUREDBOXVIS_H

// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaBoxVis.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>


// ========================================================================== //
// === Forward Declarations
// ========================================================================== //
class VfaTexturedBoxModel;


// ========================================================================== //
// === Class Definition
// ========================================================================== //
class VISTAFLOWLIBAUXAPI VfaTexturedBoxVis : public VfaBoxVis
{
public:
	// ---------------------------------------------------------------------- //
	// --- Con-/Destructor
	// ---------------------------------------------------------------------- //
	//! Main constructor.
	explicit VfaTexturedBoxVis(VfaTexturedBoxModel *pModel);
	//! Virtual destructor.
	virtual ~VfaTexturedBoxVis();


	// ---------------------------------------------------------------------- //
	// --- Public
	// ---------------------------------------------------------------------- //
	//!
	VfaTexturedBoxModel* GetTexturedBoxModel() const;


	// ---------------------------------------------------------------------- //
	// --- IVflRenderable Interface
	// ---------------------------------------------------------------------- //
	//!
	virtual void DrawOpaque();


protected:
	// ---------------------------------------------------------------------- //
	// --- Con-/Destructor
	// ---------------------------------------------------------------------- //
	//! Inaccessible constructor.
	VfaTexturedBoxVis();

	virtual VflRenderableProperties* CreateProperties() const;

private:
	// ---------------------------------------------------------------------- //
	// --- Variables
	// ---------------------------------------------------------------------- //
	//!
	VfaTexturedBoxModel		*m_pTexturedBoxModel;
};

#endif // __VFATEXTUREDBOXVIS_H


// ========================================================================== //
// === End of File
// ========================================================================== //
