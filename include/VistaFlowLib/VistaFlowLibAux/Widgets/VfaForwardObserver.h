/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VfaForwardObserver_H
#define _VfaForwardObserver_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaWidgetModelBase.h"

#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaAspects/VistaObserveable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaConditionalForwarding;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/


class VISTAFLOWLIBAUXAPI VfaForwardObserver : public VflObserver
{
public:
	VfaForwardObserver(VfaConditionalForwarding *pObservable, VfaWidgetModelBase * pModel);
	virtual ~VfaForwardObserver();

	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

protected:
	
private:
	VfaWidgetModelBase *m_pModel;
	VfaConditionalForwarding *m_pForwardObservable;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaForwardObserver_H

