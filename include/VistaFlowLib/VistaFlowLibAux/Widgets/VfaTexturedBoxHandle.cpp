/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaTexturedBoxHandle.h"
#include "VfaTexturedBoxVis.h"
#include "Box/VfaTexturedBoxModel.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <cstring>

using namespace std;


// ========================================================================== //
// === Con-/Destructor
// ========================================================================== //
VfaTexturedBoxHandle::VfaTexturedBoxHandle(VflRenderNode *pRenderNode)
 : IVfaCenterControlHandle(),
   m_pTexBoxVis(new VfaTexturedBoxVis(new VfaTexturedBoxModel)),
   m_fScaleBuffer(1.0f)
{
	m_aNormalColor[0] = 1.0f;
	m_aNormalColor[1] = 0.0f;
	m_aNormalColor[2] = 0.0f;
	m_aNormalColor[3] = 1.0f;

	m_aHighlightColor[0] = 1.0f;
	m_aHighlightColor[1] = 0.75f;
	m_aHighlightColor[2] = 0.75f;
	m_aHighlightColor[3] = 1.0f;

	if(m_pTexBoxVis->Init())
		pRenderNode->AddRenderable(m_pTexBoxVis);

	m_pTexBoxVis->GetProperties()->SetDrawFilledFaces(true);
	m_pTexBoxVis->GetProperties()->SetFaceColor(m_aNormalColor);
	m_pTexBoxVis->GetProperties()->SetLineColor(m_aNormalColor);	
}

VfaTexturedBoxHandle::~VfaTexturedBoxHandle()
{
	m_pTexBoxVis->GetRenderNode()->RemoveRenderable(m_pTexBoxVis);
	delete m_pTexBoxVis->GetTexturedBoxModel();
	delete m_pTexBoxVis;
}


// ========================================================================== //
// === Public Interface
// ========================================================================== //
void VfaTexturedBoxHandle::SetIsVisible(bool bIsVisible)
{
	m_pTexBoxVis->SetVisible(bIsVisible);
}

bool VfaTexturedBoxHandle::GetIsVisible() const
{
	return m_pTexBoxVis->GetVisible();
}


void VfaTexturedBoxHandle::SetSize(float fWidth, float fHeight, float fDepth)
{
	if(fWidth < 0.0f || fHeight < 0.0f || fDepth < 0.0f)
		return;

	m_pTexBoxVis->GetTexturedBoxModel()->SetExtents(fWidth, fHeight, fDepth);
}

void VfaTexturedBoxHandle::SetSize(float fCubeSideLength)
{
	if(fCubeSideLength < 0.0f)
		return;

	m_pTexBoxVis->GetTexturedBoxModel()->SetExtents(fCubeSideLength,
		fCubeSideLength, fCubeSideLength);
}

void VfaTexturedBoxHandle::GetSize(float &fWidth, float &fHeight,
									float &fDepth) const
{
	m_pTexBoxVis->GetTexturedBoxModel()->GetExtents(fWidth, fHeight, fDepth);
}


void VfaTexturedBoxHandle::SetCenter(const VistaVector3D &v3Center)
{
	IVfaCenterControlHandle::SetCenter(v3Center);
	m_pTexBoxVis->GetTexturedBoxModel()->SetTranslation(v3Center);
}

void VfaTexturedBoxHandle::GetCenter(VistaVector3D &v3Center) const
{
	m_pTexBoxVis->GetTexturedBoxModel()->GetTranslation(v3Center);
}


void VfaTexturedBoxHandle::SetRotation(const VistaQuaternion &qRotation)
{
	m_pTexBoxVis->GetTexturedBoxModel()->SetRotation(qRotation);
}

void VfaTexturedBoxHandle::GetRotation(VistaQuaternion &qRotation) const
{
	m_pTexBoxVis->GetTexturedBoxModel()->GetRotation(qRotation);
}


void VfaTexturedBoxHandle::SetScale(float fScale)
{
	if(fScale == 0.0f)
		return;

	float aExtents[3];
	m_pTexBoxVis->GetTexturedBoxModel()->GetExtents(aExtents);

	for(int i=0; i<3; ++i)
		aExtents[i] = fScale * aExtents[i] / m_fScaleBuffer;

	m_pTexBoxVis->GetTexturedBoxModel()->SetExtents(aExtents);
	m_fScaleBuffer = fScale;
}

float VfaTexturedBoxHandle::GetScale() const
{
	return m_fScaleBuffer;
}


void VfaTexturedBoxHandle::SetNormalColor(float aRGB[3])
{
	memcpy(m_aNormalColor, aRGB, 3 * sizeof(float));

	// Clamp to [0.0f, 1.0f].
	for(int i=0; i<3; ++i)
	{
		m_aNormalColor[i] = std::max<float>(0.0f, m_aNormalColor[i]);
		m_aNormalColor[i] = std::min<float>(1.0f, m_aNormalColor[i]);
	}

	if(!GetIsHighlighted())
	{
		m_pTexBoxVis->GetProperties()->SetFaceColor(m_aNormalColor);
		m_pTexBoxVis->GetProperties()->SetLineColor(m_aNormalColor);
	}
}

void VfaTexturedBoxHandle::SetNormalColor(const VistaColor& rColor)
{
	m_aNormalColor[0] = rColor.GetRed();
	m_aNormalColor[1] = rColor.GetGreen();
	m_aNormalColor[2] = rColor.GetBlue();
	m_aNormalColor[3] = rColor.GetAlpha();

	if(!GetIsHighlighted())
	{
		m_pTexBoxVis->GetProperties()->SetFaceColor(m_aNormalColor);
		m_pTexBoxVis->GetProperties()->SetLineColor(m_aNormalColor);
	}
}

void VfaTexturedBoxHandle::GetNormalColor(float aRGB[3]) const
{
	memcpy(aRGB, m_aNormalColor, 3 * sizeof(float));
}

void VfaTexturedBoxHandle::SetHighlightedColor(float aRGB[3])
{
	memcpy(m_aHighlightColor, aRGB, 3 * sizeof(float));

	// Clamp to [0.0f, 1.0f].
	for(int i=0; i<3; ++i)
	{
		m_aHighlightColor[i] = std::max<float>(0.0f, m_aHighlightColor[i]);
		m_aHighlightColor[i] = std::min<float>(1.0f, m_aHighlightColor[i]);
	}

	if(GetIsHighlighted())
	{
		m_pTexBoxVis->GetProperties()->SetFaceColor(m_aHighlightColor);
		m_pTexBoxVis->GetProperties()->SetLineColor(m_aHighlightColor);
	}
}

void VfaTexturedBoxHandle::SetHighlightedColor(const VistaColor& rColor)
{
	m_aHighlightColor[0] = rColor.GetRed();
	m_aHighlightColor[1] = rColor.GetGreen();
	m_aHighlightColor[2] = rColor.GetBlue();
	m_aHighlightColor[3] = rColor.GetAlpha();

	if(GetIsHighlighted())
	{
		m_pTexBoxVis->GetProperties()->SetFaceColor(m_aHighlightColor);
		m_pTexBoxVis->GetProperties()->SetLineColor(m_aHighlightColor);
	}
}

void VfaTexturedBoxHandle::GetHighlightedColor(float aRGB[3]) const
{
	memcpy(aRGB, m_aHighlightColor, 3 * sizeof(float));
}


void VfaTexturedBoxHandle::SetIsHighlighted(bool bIsHighlighted)
{
	IVfaCenterControlHandle::SetIsHighlighted(bIsHighlighted);

	if(bIsHighlighted)
	{
		m_pTexBoxVis->GetProperties()->SetFaceColor(m_aHighlightColor);
		m_pTexBoxVis->GetProperties()->SetLineColor(m_aHighlightColor);
	}
	else
	{
		m_pTexBoxVis->GetProperties()->SetFaceColor(m_aNormalColor);
		m_pTexBoxVis->GetProperties()->SetLineColor(m_aNormalColor);
	}
}

bool VfaTexturedBoxHandle::GetIsHighlighted() const
{
	return IVfaCenterControlHandle::GetIsHighlighted();
}


bool VfaTexturedBoxHandle::LoadTexture(std::string strFilename)
{
	return m_pTexBoxVis->GetTexturedBoxModel()->LoadTexture(strFilename);
}


// ========================================================================== //
// === End of File
// ========================================================================== //
