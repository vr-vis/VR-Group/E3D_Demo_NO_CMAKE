/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <cstring>

#include "VfaProximitySphereHandle.h"

#include "VfaSphereVis.h"
#include "Sphere/VfaSphereModel.h"
#include "VfaWidgetTools.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaProximitySphereHandle::VfaProximitySphereHandle(VflRenderNode *pRN)
	:	IVfaProximityHandle(IVfaControlHandle::HANDLE_PROXIMITY_SPHERE),
		m_pVis(new VfaSphereVis(new VfaSphereModel))
{
	//define default appearence
	m_fNormalLW = 0.1f;
	m_fHighlightLW = 0.15f;
	m_fNormalColor[2] = m_fNormalColor[1] = 0.0f;
	m_fNormalColor[0] = m_fNormalColor[3] = 1.0f;
	m_fHighlightColor[2] = m_fHighlightColor[1] = 0.75f;
	m_fHighlightColor[0] = m_fHighlightColor[3] = 1.0f;

	//init a sphere visualization for the handle stuff
	if(m_pVis->Init())
		pRN->AddRenderable(m_pVis);

	//set default properties
	VfaSphereVis::VfaSphereVisProps *pProps = m_pVis->GetProperties();
	pProps->SetToSolid(true);
	pProps->SetColor(m_fNormalColor);
	pProps->SetLineWidth(m_fNormalLW);
	m_pVis->GetModel()->SetRadius(0.02f);
	m_pVis->Update();
}

VfaProximitySphereHandle::~VfaProximitySphereHandle()
{
	m_pVis->GetRenderNode()->RemoveRenderable(m_pVis);
	delete m_pVis;
}

/*============================================================================*/
/*  NAME:      SetIsHighlighted					  		   			          */
/*============================================================================*/
void VfaProximitySphereHandle::SetIsHighlighted(bool bIsHighlighted)
{
	VfaSphereVis::VfaSphereVisProps *pProps = m_pVis->GetProperties();
	if(bIsHighlighted)
	{
		pProps->SetColor(m_fHighlightColor);
		pProps->SetLineWidth(m_fHighlightLW);
	}
	else
	{
		pProps->SetColor(m_fNormalColor);
		pProps->SetLineWidth(m_fNormalLW);
	}
}

/*============================================================================*/
/*  NAME:      Set/GetSphereRadius				  		   			          */
/*============================================================================*/
void VfaProximitySphereHandle::SetSphereRadius(float fRadius)
{
	m_pVis->GetModel()->SetRadius(fRadius);
}
float VfaProximitySphereHandle::GetSphereRadius() const
{
	return m_pVis->GetModel()->GetRadius();
}

/*============================================================================*/
/*  NAME:      Set/GetSphereCenter				  		   			          */
/*============================================================================*/
void VfaProximitySphereHandle::SetSphereCenter(const VistaVector3D &v3C)
{
	m_pVis->GetModel()->SetCenter(v3C);
}
void VfaProximitySphereHandle::SetSphereCenter(float fCenter[3])
{
	m_pVis->GetModel()->SetCenter(fCenter);
}
void VfaProximitySphereHandle::SetSphereCenter(double dCenter[3])
{
	m_pVis->GetModel()->SetCenter(dCenter);
}
void VfaProximitySphereHandle::GetSphereCenter(VistaVector3D &v3C) const
{
	m_pVis->GetModel()->GetCenter(v3C);
}
void VfaProximitySphereHandle::GetSphereCenter(float fCenter[3]) const
{
	m_pVis->GetModel()->GetCenter(fCenter);
}
void VfaProximitySphereHandle::GetSphereCenter(double dCenter[3]) const
{
	m_pVis->GetModel()->GetCenter(dCenter);
}

/*============================================================================*/
/*  NAME:      IsTouched						  		   			          */
/*============================================================================*/
bool VfaProximitySphereHandle::IsTouched(const VistaVector3D & v3Pos) const
{
		float fPos[3] = {v3Pos[0], v3Pos[1], v3Pos[2]};
		float fCenter[3];
		this->GetSphereCenter(fCenter);
		return VfaWidgetTools::IsInsideSphere(fPos, this->GetSphereRadius(), fCenter);
}

/*============================================================================*/
/*  NAME:      Set/GetHighlightLineWidth				  		   			  */
/*============================================================================*/
void VfaProximitySphereHandle::SetHighlightLineWidth(float f)
{
	m_fHighlightLW = f;
}

float VfaProximitySphereHandle::GetHighlightLineWidth() const
{
	return m_fHighlightLW;
}

/*============================================================================*/
/*  NAME:      Set/GetNormalLineWidth					  		   			  */
/*============================================================================*/
void VfaProximitySphereHandle::SetNormalLineWidth(float f)
{
	m_fNormalLW = f;
}

float VfaProximitySphereHandle::GetNormalLineWidth() const
{
	return m_fNormalLW;
}

/*============================================================================*/
/*  NAME:      Set/GetHighlightColor					  		   			  */
/*============================================================================*/
void VfaProximitySphereHandle::SetHighlightColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetHighlightColor(fC);
}
VistaColor VfaProximitySphereHandle::SetHighlightColor() const
{
	VistaColor color(m_fHighlightColor);
	return color;
}
void VfaProximitySphereHandle::SetHighlightColor(float fC[4])
{
	memcpy(m_fHighlightColor, fC, 4*sizeof(float));
}

void VfaProximitySphereHandle::GetHighlightColor(float fC[4]) const
{
	memcpy(fC, m_fHighlightColor, 4*sizeof(float));
}

/*============================================================================*/
/*  NAME:      Set/GetNormalColor					  		   				  */
/*============================================================================*/
void VfaProximitySphereHandle::SetNormalColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetNormalColor(fC);
}
VistaColor VfaProximitySphereHandle::GetNormalColor() const
{
	VistaColor color(m_fNormalColor);
	return color;
}
void VfaProximitySphereHandle::SetNormalColor(float fC[4])
{
	memcpy(m_fNormalColor, fC, 4*sizeof(float));
}

void VfaProximitySphereHandle::GetNormalColor(float fC[4]) const
{
	memcpy(fC, m_fNormalColor, 4*sizeof(float));
}

/*============================================================================*/
/*  NAME:      Set/GetVisible							  		   			  */
/*============================================================================*/
void VfaProximitySphereHandle::SetVisible(bool b)
{
	m_pVis->GetProperties()->SetVisible(b);
}

bool VfaProximitySphereHandle::GetVisible() const
{
	return m_pVis->GetProperties()->GetVisible();
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "IVfaProximityBoxHandle.cpp"                                  */
/*============================================================================*/

