/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACONEVIS_H
#define _VFACONEVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * glutSolidCone and  glutWireCone render a solid or wireframe cone respectively
 * oriented along the Z axis.
 * The base of the cone is placed at Z = 0, and the top at Z =  height.
 * The cone is subdivided around the Z axis into slices, and along the Z axis into stacks.
 */
class VISTAFLOWLIBAUXAPI VfaConeVis : public IVflRenderable
{
public: 
	VfaConeVis();
	virtual ~VfaConeVis();

	/**
	* here the rendering is done
	*/
	virtual void DrawOpaque();


	virtual unsigned int GetRegistrationMode() const;


	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);

	bool SetCenter(const VistaVector3D& v3Center);
	bool SetCenter(float fCenter[3]);
	bool SetCenter(double dCenter[3]);

	void GetCenter(double dCenter[3]) const;
	void GetCenter(float fCenter[3]) const;
	void GetCenter(VistaVector3D& v3Center) const;
	

	bool SetHeight(float f);
	float GetHeight () const;

	bool SetRadius(float f);
	float GetRadius() const;

	void SetRotate(const VistaVector3D &v3Normal);
	void SetRotate(float angle, float x, float y, float z);

	void GetRotate(float fRotate[4]) const;

	void SetOrientation(const VistaQuaternion &qOri);

	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);
	
	class VISTAFLOWLIBAUXAPI VfaConeVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		enum{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_SOLID_CHG,
			MSG_LAST
		};

		VfaConeVisProps();
		virtual ~VfaConeVisProps();

		bool SetColor(float fColor[4]);
		bool SetColor(const VistaColor& color);

		void GetColor(float fColor[4]) const;
		VistaColor GetColor() const;

		bool SetUseLighting(bool b);
		bool GetUseLighting() const;

		bool SetToSolid(bool);
		bool GetIsSolid() const;

	protected:

	private:
		float m_fColor[4];
		bool m_bUseLighting;
		bool m_bSolid;
	};

	virtual VfaConeVisProps *GetProperties() const;
	
protected:
	virtual VflRenderableProperties* CreateProperties() const;

private:
	VistaVector3D m_v3Center;
	float m_fRotate[4];
	//cone properties
	float m_fConeRadius;
	float m_fConeHeight;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFACONEVIS_H
