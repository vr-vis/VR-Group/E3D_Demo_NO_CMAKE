/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaCylinderHandle.h"
#include "VfaCylinderVis.h"
#include "Cylinder/VfaCylinderModel.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaSphereHandle                                      */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCylinderHandle::VfaCylinderHandle(VflRenderNode *pRN)
:	IVfaCenterControlHandle(),
	m_pCylinderVis(new VfaCylinderVis(new VfaCylinderModel))
{
	//normal color
	m_fNormalColor[0] = 1.0f;
	m_fNormalColor[1] = 0.0f;
	m_fNormalColor[2] = 0.0f;
	m_fNormalColor[3] = 1.0f;
	
	//color if sphere highlighted
	m_fHighlightColor[0] = 1.0f;
	m_fHighlightColor[1] = 0.75f;
	m_fHighlightColor[2] = 0.75f;
	m_fHighlightColor[3] = 1.0f;

	if(m_pCylinderVis->Init())
		pRN->AddRenderable(m_pCylinderVis);
	//TODO
	//m_pCylinderVis->GetProperties()->SetToSolid(true);
	m_pCylinderVis->GetProperties()->SetColor(m_fNormalColor);
	m_pCylinderVis->GetProperties()->SetUseLighting(true);
}

VfaCylinderHandle::~VfaCylinderHandle()
{
	m_pCylinderVis->GetRenderNode()->RemoveRenderable(m_pCylinderVis);
	delete m_pCylinderVis->GetModel();
	delete m_pCylinderVis;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void VfaCylinderHandle::SetCenter(const VistaVector3D &v3Center)
{
	IVfaCenterControlHandle::SetCenter(v3Center);
	m_pCylinderVis->GetModel()->SetCenter(v3Center);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderHandle::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderHandle::GetBounds(VistaBoundingBox &)
{
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetVisible                                              */
/*                                                                            */
/*============================================================================*/
void VfaCylinderHandle::SetVisible(bool b)
{
	m_bIsVisible = b;
	this->m_pCylinderVis->SetVisible(b);

	if(!b)                            // if invisible, then disable this handle
	{
		m_bIsEnable = false;
		SetIsSelectionEnabled(false);
	}
}

bool VfaCylinderHandle::GetVisible() const
{
	if( m_bIsVisible != this->m_pCylinderVis->GetVisible())
		vstr::warnp() << "[VfaSphereHandle]: unsynchronized state of visibility" << endl;
	
	return this->m_pCylinderVis->GetVisible();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetEnable                                                   */
/*                                                                            */
/*============================================================================*/
void VfaCylinderHandle::SetEnable(bool b)
{
	IVfaCenterControlHandle::SetEnable(b);
	if(b)							// if enabled, then force visibility
	{
		m_bIsVisible = true;
		this->m_pCylinderVis->SetVisible(true);
	}
	else
	{
		m_bIsVisible = false;
		this->m_pCylinderVis->SetVisible(false);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRadius                                               */
/*                                                                            */
/*============================================================================*/
void VfaCylinderHandle::SetRadius(float f)
{
	m_pCylinderVis->GetModel()->SetRadius(f);
	m_fNormalRadius = f;
	m_fHighlightRadius = f;
}

float VfaCylinderHandle::GetRadius() const
{
	return m_pCylinderVis->GetModel()->GetRadius();
	return 0;
}


void VfaCylinderHandle::SetNormalRadius(float f)
{
	m_fNormalRadius = f;	
}
float VfaCylinderHandle::GetNormalRadius() const
{
	return m_fNormalRadius;
}

void VfaCylinderHandle::SetHighlightRadius(float f)
{
	m_fHighlightRadius = f;
}
float VfaCylinderHandle::GetHighlightRadius() const
{
	return m_fHighlightRadius;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNormalColor                                          */
/*                                                                            */
/*============================================================================*/
void VfaCylinderHandle::SetNormalColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetNormalColor(fC);
}

VistaColor VfaCylinderHandle::GetNormalColor() const
{
	VistaColor color(m_fNormalColor);
	return color;
}
void VfaCylinderHandle::SetNormalColor(float fNormalColor[4])
{
	m_fNormalColor[0] = fNormalColor[0];
	m_fNormalColor[1] = fNormalColor[1];
	m_fNormalColor[2] = fNormalColor[2];
	m_fNormalColor[3] = fNormalColor[3];
}

void VfaCylinderHandle::GetNormalColor(float fNormalColor[4]) const
{
	fNormalColor[0] = m_fNormalColor[0];
	fNormalColor[1] = m_fNormalColor[1];
	fNormalColor[2] = m_fNormalColor[2];
	fNormalColor[3] = m_fNormalColor[3];
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHighlightColor                                       */
/*                                                                            */
/*============================================================================*/
void VfaCylinderHandle::SetHighlightColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetHighlightColor(fC);
}

VistaColor VfaCylinderHandle::GetHighlightColor() const
{
	VistaColor color(m_fHighlightColor);
	return color;
}

void VfaCylinderHandle::SetHighlightColor(float fHighlightColor[4])
{
	m_fHighlightColor[0] = fHighlightColor[0];
	m_fHighlightColor[1] = fHighlightColor[1];
	m_fHighlightColor[2] = fHighlightColor[2];
	m_fHighlightColor[3] = fHighlightColor[3];
}

void VfaCylinderHandle::GetHighlightColor(float fHighlightColor[4]) const
{
	fHighlightColor[0] = m_fHighlightColor[0];
	fHighlightColor[1] = m_fHighlightColor[1];
	fHighlightColor[2] = m_fHighlightColor[2];
	fHighlightColor[3] = m_fHighlightColor[3];
}

/*============================================================================*/
/*  NAME      :   Get/SetIsHighlighted		                                  */
/*============================================================================*/
void VfaCylinderHandle::SetIsHighlighted(bool b)
{
	IVfaCenterControlHandle::SetIsHighlighted(b);
	
	if(b)
	{
		m_pCylinderVis->GetProperties()->SetColor(m_fHighlightColor);
		m_pCylinderVis->GetModel()->SetRadius(m_fHighlightRadius);
	}
	else
	{
		m_pCylinderVis->GetProperties()->SetColor(m_fNormalColor);
		m_pCylinderVis->GetModel()->SetRadius(m_fNormalRadius);
	}
}

bool VfaCylinderHandle::GetIsHighlighted() const
{
	return IVfaCenterControlHandle::GetIsHighlighted();
}



/*============================================================================*/
/*  NAME      :   GetSphereVis		                                          */
/*============================================================================*/
VfaCylinderVis* VfaCylinderHandle::GetCylinderVis() const
{
	return m_pCylinderVis;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaSphereHandle.cpp"									      */
/*============================================================================*/



