/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAWIDGETTOOLS_H
#define _VFAWIDGETTOOLS_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <math.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaVector3D;
class VistaQuaternion;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Just some static routines which are shared by many widgets...
 */
class VfaWidgetTools
{
public:

	/**
	 * Compute the distance between a given ray(defined by point and orientation) and
	 * point defined as VistaVector3D.
	 */
	static float GetDistanceRayPoint(const VistaVector3D &v3RayPoint, const VistaQuaternion &qRayOri, const VistaVector3D& v3Point);

	/**
	 * check whether x lies within a box
	 * NOTE: The box's boundary belongs to the box i.e. is seen as "inside" as well.
	 * NOTE: bounds is given in VTK notation
	 */
	static bool IsInsideBox(float x[3], float bounds[6]);

	/**
	 *
	 */
	static void ClampPositionToBounds(float fPos[3], float fConstraint[6]);

	/**
	 *
	 */
	static void ClampBoxToBounds(float fBox[6], float fConstraint[6]);

	/**
	*	check whether x lies in sphere
	*/
	static bool IsInsideSphere(float x[3], float fRadius, float fCenter[3]);

protected:
	VfaWidgetTools();
	virtual ~VfaWidgetTools();

private:

};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
inline bool VfaWidgetTools::IsInsideBox(float x[3], float fBounds[6])
{
	return	fBounds[0] <= x[0] && x[0] <= fBounds[1] &&
			fBounds[2] <= x[1] && x[1] <= fBounds[3] &&
			fBounds[4] <= x[2] && x[2] <= fBounds[5];
}

inline bool VfaWidgetTools::IsInsideSphere(float x[3], float fRadius, float fCenter[3])
{
	float fDistance = sqrt((pow(x[0]-fCenter[0],2)+
							pow(x[1]-fCenter[1],2)+
							pow(x[2]-fCenter[2],2)));
	return fDistance <= fRadius;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAWIDGETTOOLS_H

