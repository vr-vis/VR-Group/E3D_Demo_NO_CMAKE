/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAWIDGETCONTROLLER_H
#define _VFAWIDGETCONTROLLER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaSlotObserver.h"
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <vector>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaControlHandle;
class IVistaReflectionable;
class VflRenderNode;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * define a common interface for all widget controllers
 */

class VISTAFLOWLIBAUXAPI IVfaWidgetController : public IVfaSlotObserver,
												public IVistaObserveable
{
public:
	virtual ~IVfaWidgetController();

	/**
	 *
	 */
	virtual const std::vector<IVfaControlHandle*>& GetControlHandles() const;

	/**
	 *
	 */
	virtual void OnFocus(IVfaControlHandle* pHandle);
	virtual void OnUnfocus();

	/**
	 *
	 */
	virtual void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	virtual void OnUntouch();
	

	///**
	// *
	// */
	//virtual IVistaReflectionable* GetProperties() const = 0;

	enum eControllerState{ 
		CS_NONE = 0, 
		CS_TOUCH, 
		CS_FOCUS,
		CS_LAST
	};

	void SetControllerState (IVfaWidgetController::eControllerState iNewState,
		const std::vector<IVfaControlHandle*>& vecHandles);
	IVfaWidgetController::eControllerState GetControllerState() const;

	virtual void SetIsEnabled(bool b) = 0;
	virtual bool GetIsEnabled() const = 0;

	enum eMessages { WIDGETCONTROLLER_STATECHANGE = 0 };

	// TODO_HIGH: Document these functions.
	virtual bool ExchangeRenderNode(VflRenderNode *pRenderNode);
	virtual VflRenderNode* GetRenderNode() const;

protected:
	IVfaWidgetController(VflRenderNode *pRenderNode);

	void AddControlHandle(IVfaControlHandle*);
	void RemoveControlHandle(IVfaControlHandle *pHandle);

private:
	VflRenderNode *m_pRenderNode;

	IVfaWidgetController::eControllerState	m_iControllerState;	

	std::vector<IVfaControlHandle*>	m_vecControlHandles;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAWIDGETCONTROLLER_H
