/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#include "VfaCylinderController.h"

#include "VfaCylinderModel.h"
#include "VfaCylinderWidget.h"
#include "../VfaCylinderHandle.h"
#include "../VfaSphereHandle.h"
#include "../Sphere/VfaSphereModel.h"

#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaMath/VistaIndirectXform.h>

#include <cassert>
#include <algorithm>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCylinderController::VfaCylinderController(VfaCylinderModel *pModel, 
										   VflRenderNode *pRenderNode)
:	IVfaWidgetController(pRenderNode),
	m_pModel(pModel),
	m_iGrabButton(0),
	m_iResizeButton(1),
	m_pFocusHandle(NULL),
	m_pXform(new VistaIndirectXform),
	m_iCurrentInternalState(VfaCylinderController::SCS_NONE),
	m_bEnabled(true)
{
	m_pCtlProps = new VfaCylinderController::VfaCylinderControllerProps(this);
	//Set initial orientation
	m_pModel->GetRotation(m_oLastSensorFrame.qOrientation);
	
	// adding two Cylinder handles, one for the center, the other one as surfacePt
	m_vecHandles.resize(2);
	for(int i=0; i<2; ++i)
	{
		m_vecHandles[i] = new VfaSphereHandle(pRenderNode);
		this->AddControlHandle(m_vecHandles[i]);
	}
	
}

VfaCylinderController::~VfaCylinderController()
{
	delete m_pCtlProps;
	delete m_pXform;

	for(int unsigned i=0; i < m_vecHandles.size(); ++i)
	{
		delete m_vecHandles[i];
	}

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	for(unsigned int i=1; i < m_vecHandles.size(); ++i)
	{
		m_vecHandles[i]->SetVisible(m_bEnabled);
		m_vecHandles[i]->SetEnable(m_bEnabled);
	}
	m_vecHandles[0]->SetVisible(false);
	m_vecHandles[0]->SetEnable(false);
}
bool VfaCylinderController::GetIsEnabled() const
{
	return m_bEnabled;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnFocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::OnFocus(IVfaControlHandle* pHandle)
{
	// don't allow to change focus if some handle already has it
	if(m_iCurrentInternalState == SCS_NONE)
	{
		assert(dynamic_cast<VfaSphereHandle*>(pHandle));

		m_pFocusHandle = static_cast<VfaSphereHandle*>(pHandle);

		// search the focused handle(in the vector)
		m_iHandle =(int)(find(m_vecHandles.begin(), m_vecHandles.end(), m_pFocusHandle) - m_vecHandles.begin());

		m_pFocusHandle->SetIsHighlighted(true);
			
		if(m_iHandle == 0)
			m_iInternalDoState = SCS_MOVE;
		else if(m_iHandle == 1)
			m_iInternalDoState = SCS_RESIZE;
	}
	this->Notify(MSG_FOCUS);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUnfocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::OnUnfocus()
{
	// don't allow to change focus if some handle already has it
	if(m_iCurrentInternalState == SCS_NONE)
	{
		if(m_pFocusHandle)
		{
			m_pFocusHandle->SetIsHighlighted(false);
		}
		m_iHandle = -1;
	}
	this->Notify(MSG_UNFOCUS);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{
	this->Notify(MSG_TOUCH);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUntouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::OnUntouch()
{
	this->Notify(MSG_UNTOUCH);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::OnSensorSlotUpdate(int iSlot, 
				const VfaApplicationContextObject::sSensorFrame & oFrameData)
{

	if(iSlot == VfaApplicationContextObject::SLOT_POINTER_VIS)
	{
		// save sensor data
		m_oLastSensorFrame = oFrameData;
		// if we have a DO(here especially: move or resize) state, do something....
		if(m_iCurrentInternalState != SCS_NONE)
		{
			VistaVector3D v3MyPos;  // vis space
			VistaQuaternion qMyOri; // vis space
			m_pXform->Update(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation, 
				v3MyPos, qMyOri);

			// center was grabbed --> move widget
			if(m_iInternalDoState == SCS_MOVE)
			{
				m_pModel->SetCenter(v3MyPos);
				m_pModel->SetRotation(qMyOri);
			}
			// surfacePt was grabbed --> resize widget
			else if(m_iInternalDoState == SCS_RESIZE)
			{
					// resize Cylinder
					VistaVector3D vecCenter;
					m_pModel->GetCenter(vecCenter);	
					float fRadius =(vecCenter-v3MyPos).GetLength();
					m_pModel->SetRadius(fRadius);

					// rotate Cylinder representation
					VistaVector3D v3MyPos;  // vis space
					VistaQuaternion qMyOri; // vis space
					m_pXform->Update(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation, 
						v3MyPos, qMyOri);
					//m_pModel->SetRotation(qMyOri);

			}
		}
	}
	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	
	if(bSet)
	{
		// we can only be grabbed, if we have the focus
		if(this->GetControllerState() == CS_FOCUS)
		{
			// all(!) commands will be handled in the same way
			// if you grabb the center, you can move the widget
			// if you grab the surfacePt, you can change the radius of your widget
			// just choose the right handle and init the xform with its center position.
			int iHandle =(m_iInternalDoState == SCS_MOVE ? 0 : 1);
			
			VistaQuaternion qRot;
			VistaVector3D v3Center;
			m_vecHandles[iHandle]->GetCenter(v3Center);
			m_pModel->GetRotation(qRot);
			m_pXform->Init(	m_oLastSensorFrame.v3Position, 
							m_oLastSensorFrame.qOrientation,	
							v3Center, qRot);
			m_iCurrentInternalState = SCS_DO;
		}
	}
	else
	{
		// if the slot is released, goto state NONE
		m_iCurrentInternalState = SCS_NONE;
		
		// and if we have lost focus during other states, now react on that
		if(this->GetControllerState()!=CS_FOCUS)
		{
			if(m_pFocusHandle)
				m_pFocusHandle->SetIsHighlighted(false);
			m_iHandle = -1;

		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::OnTimeSlotUpdate(const double dTime)
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSensorMask			                                      */
/*                                                                            */
/*============================================================================*/
int VfaCylinderController::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_POINTER_VIS);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCommandMask			                                  */
/*                                                                            */
/*============================================================================*/
int VfaCylinderController::GetCommandMask() const
{
	return(1 << m_iGrabButton) |(1 << m_iResizeButton);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeUpdate				                                  */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderController::GetTimeUpdate() const
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaCylinderModel *pCylinderModel = dynamic_cast<VfaCylinderModel*>(pObserveable);

	if(!pCylinderModel)
		return;

	// get data from model
	VistaVector3D v3Center;
	VistaQuaternion qOri;
	pCylinderModel->GetCenter(v3Center);
	pCylinderModel->GetRotation(qOri);
	
	// rotate outer Cylinder handle on Cylinder representation
	VistaVector3D v3SurfacePt(0.0f, 1.0f, 0.0f);
	//v3SurfacePt = qOri.Rotate(v3SurfacePt);
	v3SurfacePt = pCylinderModel->GetRadius() * v3SurfacePt + v3Center;

	// reposition handles
	this->UpdateHandles(v3Center, v3SurfacePt);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaCylinderController::VfaCylinderControllerProps* VfaCylinderController::GetProperties() const
{
	return m_pCtlProps;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateHandles                                               */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::UpdateHandles(const VistaVector3D& v3Center, 
										 const VistaVector3D& v3SurfacePt)
{
	// set handle's position
	m_vecHandles[0]->SetCenter(v3Center); 
	m_vecHandles[1]->SetCenter(v3SurfacePt);
	
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaCylinderControllerProps                             */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaCylinderController::VfaCylinderControllerProps");

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCylinderController::VfaCylinderControllerProps::VfaCylinderControllerProps(VfaCylinderController *pCylinderCtrl)
: m_pCylinderCtrl(pCylinderCtrl)
{}

VfaCylinderController::VfaCylinderControllerProps::~VfaCylinderControllerProps()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleRadius                                         */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderController::VfaCylinderControllerProps::SetHandleRadius(float fRadius)
{
	if(fRadius != m_pCylinderCtrl->m_vecHandles[0]->GetRadius())
	{
		for(int i=0; i<2; ++i)
		{
			m_pCylinderCtrl->m_vecHandles[i]->SetRadius(fRadius);
		}
		Notify();
		return true;
	}
	return false;
}

float VfaCylinderController::VfaCylinderControllerProps::GetHandleRadius() const
{
	return m_pCylinderCtrl->m_vecHandles[0]->GetRadius();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleNormalColor                                    */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::VfaCylinderControllerProps::SetHandleNormalColor(
	const VistaColor& color)
{
	for(int i=0; i<2; ++i)
	{
		float fColors[3];
		color.GetValues(fColors);
		m_pCylinderCtrl->m_vecHandles[i]->SetNormalColor(fColors);
	}
}

VistaColor VfaCylinderController::VfaCylinderControllerProps::GetHandleNormalColor() const
{
	float fColors[3];
	m_pCylinderCtrl->m_vecHandles[0]->GetNormalColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaCylinderController::VfaCylinderControllerProps::SetHandleNormalColor(float fC[4])
{
	for (unsigned int i = 0; i < 2; ++i)
		m_pCylinderCtrl->m_vecHandles[i]->SetNormalColor(fC);
}

void VfaCylinderController::VfaCylinderControllerProps::GetHandleNormalColor(float fC[4]) const
{
	m_pCylinderCtrl->m_vecHandles[0]->GetNormalColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleHighlightColor                                 */
/*                                                                            */
/*============================================================================*/
void VfaCylinderController::VfaCylinderControllerProps::SetHandleHighlightColor(
	const VistaColor& color)
{
	for(int i=0; i<2; ++i)
	{
		float fColors[4];
		color.GetValues(fColors);
		m_pCylinderCtrl->m_vecHandles[i]->SetHighlightColor(fColors);
	}
}

VistaColor VfaCylinderController::VfaCylinderControllerProps::GetHandleHighlightColor() const
{
	float fColors[4];
	m_pCylinderCtrl->m_vecHandles[0]->GetHighlightColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaCylinderController::VfaCylinderControllerProps::SetHandleHighlightColor(float fC[4])
{
	for (unsigned int i = 0; i < 2; ++i)
		m_pCylinderCtrl->m_vecHandles[i]->SetHighlightColor(fC);
}

void VfaCylinderController::VfaCylinderControllerProps::GetHandleHighlightColor(float fC[4]) const
{
	m_pCylinderCtrl->m_vecHandles[0]->GetHighlightColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaCylinderController::VfaCylinderControllerProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaCylinderController::VfaCylinderControllerProps::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  END OF FILE "VfaCylinderController.cpp"                                     */
/*============================================================================*/
