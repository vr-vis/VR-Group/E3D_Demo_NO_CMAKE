/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFACYLINDERWIDGET_H
#define _VFACYLINDERWIDGET_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidget.h"
#include "VfaCylinderModel.h"
#include "VfaCylinderController.h"
#include "../VfaCylinderVis.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaCylinderModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*
 * A sphere widget will be composed here, namely a wirecube and
 * two spheres. Sphere controller controlls the whole interactions between user
 * and widget. Vis controller is responsible for drawing the scene. Event router 
 * catches incoming events and forward them to sphere controller. Visible properties
 * are all contained in widget property.
 */
class VISTAFLOWLIBAUXAPI VfaCylinderWidget : public IVfaWidget
{
public:
	VfaCylinderWidget(VflRenderNode *pRenderNode);
	virtual ~VfaCylinderWidget();

	/**
	 * Only react on InteractionEvents if enabled. Visibility of the widget is
	 * independent of this.
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;


	VfaCylinderModel* GetModel() const;

	VfaCylinderVis* GetView() const;

	VfaCylinderController* GetController() const;

protected:

private:
	VfaCylinderModel		*m_pCylinderModel;
	VfaCylinderVis			*m_pCylinderView;
	VfaCylinderController	*m_pCylinderController;
	bool					 m_bEnabled;
	bool					 m_bVisible;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASPHEREWIDGET_H
