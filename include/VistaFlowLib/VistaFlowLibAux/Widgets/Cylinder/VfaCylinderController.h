/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACYLINDERCONTROLLER_H
#define _VFACYLINDERCONTROLLER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetController.h"

#include <VistaBase/VistaVectorMath.h>

#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaCylinderHandle;
class VfaSphereHandle;
class VistaColor;
class VistaIndirectXform;
class VfaCylinderModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Controll the behavior
 */
class VISTAFLOWLIBAUXAPI VfaCylinderController : public VflObserver, public IVfaWidgetController
{
public:

	enum{
		MSG_TOUCH,
		MSG_UNTOUCH,
		MSG_FOCUS,
		MSG_UNFOCUS,
		MSG_LAST
	};

	VfaCylinderController(VfaCylinderModel *pModel, VflRenderNode *pRenderNode);
	virtual ~VfaCylinderController();

	/** 
	 * This is called on transition change into(out of) state FOCUS
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();

	/**
	 * This is called on transition change into(out of) state TOUCH
	 */
	void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	void OnUntouch();

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	void OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate(int iSlot, const bool bSet);
	void OnTimeSlotUpdate(const double dTime);
	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);


	class VISTAFLOWLIBAUXAPI VfaCylinderControllerProps : public IVistaReflectionable
	{
		friend class VfaCylinderController;
	public:
		
		VfaCylinderControllerProps(VfaCylinderController *pCylinderCtrl);
		virtual ~VfaCylinderControllerProps();

		bool SetHandleRadius(float fRadius);
		float GetHandleRadius() const;

		void SetHandleNormalColor(const VistaColor& color);
		VistaColor GetHandleNormalColor() const;
		void SetHandleNormalColor(float fC[4]);
		void GetHandleNormalColor(float fC[4]) const;


		void SetHandleHighlightColor(const VistaColor& color);
		VistaColor GetHandleHighlightColor() const;
		void SetHandleHighlightColor(float fC[4]);
		void GetHandleHighlightColor(float fC[4]) const;


		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;


	private:
		VfaCylinderController	*m_pCylinderCtrl;

	};


	VfaCylinderControllerProps* GetProperties() const;

protected:

	void UpdateHandles(const VistaVector3D &v3Min, const VistaVector3D &v3Max);

private:
	enum eCylinderControllerState { SCS_NONE, SCS_DO};
	VfaCylinderController::eCylinderControllerState m_iCurrentInternalState;
	enum eCylinderDoState {SCS_MOVE, SCS_RESIZE };
	VfaCylinderController::eCylinderDoState m_iInternalDoState;

	VfaCylinderModel				*m_pModel;

	int							m_iGrabButton;
	int							m_iResizeButton;

	vector<VfaSphereHandle*> m_vecHandles;
	VistaIndirectXform			*m_pXform;

	VfaCylinderControllerProps		*m_pCtlProps;

	// focused handle and its number
	VfaSphereHandle*		    m_pFocusHandle;
	int							m_iHandle;

	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;

	bool						m_bEnabled;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFACylinderCONTROLLER_H
