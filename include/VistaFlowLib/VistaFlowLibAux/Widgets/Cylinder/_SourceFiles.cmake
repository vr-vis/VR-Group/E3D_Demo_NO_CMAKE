

set( RelativeDir "./Widgets/Cylinder" )
set( RelativeSourceGroup "Source Files\\Widgets\\Cylinder" )

set( DirFiles
	VfaCylinderController.cpp
	VfaCylinderController.h
	VfaCylinderModel.cpp
	VfaCylinderModel.h
	VfaCylinderWidget.cpp
	VfaCylinderWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

