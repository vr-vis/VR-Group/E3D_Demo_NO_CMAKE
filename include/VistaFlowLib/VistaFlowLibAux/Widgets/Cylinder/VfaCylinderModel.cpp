/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaCylinderModel.h"


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaCylinderVisProps                                    */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaCylinderWidget::VfaCylinderWidgetProps");


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCylinderModel::VfaCylinderModel()
	:	m_fRadius(0.05f),
	    m_fHeight(0.05f)
{
	m_v3Center[0] = 0.0f;
	m_v3Center[1] = 0.0f;
	m_v3Center[2] = 0.0f;

	//delete quaternion
	m_qRotation = VistaQuaternion(0, 0, 0, 1);
}

VfaCylinderModel::~VfaCylinderModel()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter			                                          */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderModel::SetCenter(float fC[])
{
	VistaVector3D v3(fC);
	return this->SetCenter(v3);
}
bool VfaCylinderModel::SetCenter(double dC[])
{
	VistaVector3D v3(dC);
	return this->SetCenter(v3);
}
bool VfaCylinderModel::SetCenter(const VistaVector3D &v3C)
{
	if(m_v3Center == v3C)
		return false;

	m_v3Center = v3C;
	
	this->Notify(MSG_CENTER_CHANGE);
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter											          */
/*                                                                            */
/*============================================================================*/
void VfaCylinderModel::GetCenter(float fC[3]) const
{
	m_v3Center.GetValues(fC);
}

void VfaCylinderModel::GetCenter(double dC[3]) const
{
	m_v3Center.GetValues(dC);
}

void VfaCylinderModel::GetCenter(VistaVector3D &v3C) const
{
	v3C = m_v3Center;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRotation                                             */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderModel::SetRotation(const VistaQuaternion &qRotation)
{
	if(qRotation == m_qRotation)
		return false;

	m_qRotation = qRotation;

	this->Notify(MSG_ORIENTATION_CHANGE);
	return true;
}

void VfaCylinderModel::GetRotation(VistaQuaternion &qRot) const
{
	qRot = m_qRotation;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRadius(float f)										  */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderModel::SetRadius(float f)
{
	if(f < 0) // negativ radius? good joke :)
		return false;

	m_fRadius = f;

	this->Notify(MSG_RADIUS_CHANGE);
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRadius()												  */
/*                                                                            */
/*============================================================================*/
float VfaCylinderModel::GetRadius() const
{
	return m_fRadius;
}


bool VfaCylinderModel::SetHeight(float f)
{
	m_fHeight=f;
	return true;
}

float VfaCylinderModel::GetHeight() const
{
	return m_fHeight;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaCylinderModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaCylinderModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}


/*============================================================================*/
/*  END OF FILE "VfaCylinderWidget.cpp"                                         */
/*============================================================================*/
