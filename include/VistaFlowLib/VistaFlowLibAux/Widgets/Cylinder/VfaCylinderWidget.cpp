/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaCylinderWidget.h"


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCylinderWidget::VfaCylinderWidget(VflRenderNode *pRenderNode)
:	IVfaWidget(pRenderNode),
	m_bEnabled(true),
	m_pCylinderModel(new VfaCylinderModel)
{
	m_pCylinderView = new VfaCylinderVis(m_pCylinderModel);
	if( m_pCylinderView->Init())
		pRenderNode->AddRenderable(m_pCylinderView);

	float fC[4] = {0.0f, 0.0f, 1.0f, 1.0f};
	m_pCylinderView->GetProperties()->SetColor(fC);
	m_pCylinderView->GetProperties()->SetUseLighting(false);
	
	m_pCylinderController = new VfaCylinderController(m_pCylinderModel, pRenderNode);
	m_pCylinderController->Observe(m_pCylinderModel);
	//m_pCylinderView->Observe(GetModel());
	
	
	this->GetModel()->Notify();
}

VfaCylinderWidget::~VfaCylinderWidget()
{
	m_pCylinderView->GetRenderNode()->RemoveRenderable(m_pCylinderView);
	delete m_pCylinderView;

	delete m_pCylinderController;
	delete m_pCylinderModel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaCylinderWidget::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pCylinderController->SetIsEnabled(m_bEnabled);
}
bool VfaCylinderWidget::GetIsEnabled() const
{
	return m_bEnabled;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsVisible                                            */
/*                                                                            */
/*============================================================================*/
void VfaCylinderWidget::SetIsVisible(bool b)
{
	m_bVisible = b;
	m_pCylinderView->SetVisible(m_bVisible);
}
bool VfaCylinderWidget::GetIsVisible() const
{
	return m_bVisible;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel                                                    */
/*                                                                            */
/*============================================================================*/
VfaCylinderModel* VfaCylinderWidget::GetModel() const
{
	return m_pCylinderModel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetView                                                     */
/*                                                                            */
/*============================================================================*/
VfaCylinderVis* VfaCylinderWidget::GetView() const
{
	return m_pCylinderView;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetController                                               */
/*                                                                            */
/*============================================================================*/
VfaCylinderController* VfaCylinderWidget::GetController() const
{
	return m_pCylinderController;
}
/*============================================================================*/
/*  END OF FILE "VfaCylinderWidget.cpp"                                         */
/*============================================================================*/
