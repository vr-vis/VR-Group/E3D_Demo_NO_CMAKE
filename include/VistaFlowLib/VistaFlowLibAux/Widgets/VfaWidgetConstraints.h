/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAWIDGETCONSTRAINTS_H
#define _VFAWIDGETCONSTRAINTS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include<string>
#include<map>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaWidgetModelBase;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * The base class interface for constraints on widget models
 *
 * @todo Right now we only implement constraint checking and no constraint
 *       enforcement, i.e., the constraint checker will only check if a given
 *       constraint is respected or not. It will not determine a valid model
 *       parametrization which comes as close to the constraint as possible.
 *       This might, however, be useful some day...
 *       Currently, we only rely on the fact, that our sensor resolution is 
 *       good enough to come "close enough" to the constraint...
 * 
 * @todo this is a first shot at the implementation. If we get many different
 *       constraint/model pairs here and do not want to specify a new class
 *       for every single combination of types, we might come up with something
 *       more sophisticated here(involving a e.g. a generic functor design
 *       as done for the reflectionables).
 */
class VISTAFLOWLIBAUXAPI IVfaWidgetConstraint
{
public:
	/**
	 * this should just check whether the constraint is respected by
	 * the model's current state. Return false if the constraint is
	 * NOT respected. It is the calling setter's responsibility to
	 * re-establish the last known consistent state.
	 */
	virtual bool CheckConstraint(const VfaWidgetModelBase*const pModel) = 0;	

	/** 
	 * You can temporarily switch constraints on/off
	 * NOTE: Activity should be checked by the calling instance, i.e.
	 *       an implementation class need not check for GetIsActive 
	 *       in its CheckConstraint implementation.
	 */
	void SetIsActive(bool b);
	bool GetIsActive() const; 

protected:
	IVfaWidgetConstraint();
	virtual ~IVfaWidgetConstraint(); 
private:
	bool m_bIsActive;
};



/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaWidgetConstraints_H
