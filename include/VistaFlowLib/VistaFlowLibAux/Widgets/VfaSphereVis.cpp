/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifdef WIN32
#include <Windows.h>
#endif

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include "VfaSphereVis.h"

#include <VistaFlowLibAux/Widgets/Sphere/VfaSphereWidget.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <cassert>
#include <cstring>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaSphereVis                                         */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaSphereVis::VfaSphereVis()
	:	m_pModel(NULL)
{}

VfaSphereVis::VfaSphereVis(VfaSphereModel *pModel)
	:	m_pModel(pModel)
{}


VfaSphereVis::~VfaSphereVis()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel			                                          */
/*                                                                            */
/*============================================================================*/
VfaSphereModel *VfaSphereVis::GetModel() const
{
	return m_pModel;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaSphereVis::DrawOpaque()
{
	VfaSphereVisProps *pProps = this->GetProperties();
	assert(pProps != NULL);

	if(!pProps->GetVisible())
		return;

	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_LINE_BIT);
	glPushMatrix();

	glEnable(GL_NORMALIZE);
	glEnable(GL_DEPTH_TEST);

	float fColor[4] = {1.0f,1.0f,1.0f,1.0f};
	pProps->GetColor(fColor);

	if(pProps->GetUseLighting())
	{
		glEnable(GL_LIGHTING);
		glDisable(GL_COLOR_MATERIAL);
		GLfloat fAmbient[] = {0.1f, 0.1f, 0.1f, 1.0f};
		GLfloat fSpecular[] = {1.0f,1.0f,1.0f,1.0f};
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,fAmbient);
		glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,fColor);
		glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,fSpecular);
		glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0f);	
	}
	else
	{
		glDisable(GL_LIGHTING);
		glColor4fv(fColor);
	}
	
	float fCenter[3];
	m_pModel->GetCenter(fCenter);
	float fRadius = m_pModel->GetRadius();
	glTranslatef(fCenter[0], fCenter[1], fCenter[2]);

	VistaQuaternion qOri;
	m_pModel->GetRotation(qOri);
	glRotatef(qOri.GetAxisAndAngle().m_fAngle*180.0f/Vista::Pi, qOri[0], qOri[1], qOri[2]);

	glLineWidth(pProps->GetLineWidth());
	if(pProps->GetIsSolid())
		glutSolidSphere(fRadius,16,16);
	else
		glutWireSphere(fRadius,16,16);

	glPopMatrix();
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaSphereVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaSphereVis::VfaSphereVisProps *VfaSphereVis::GetProperties() const
{
	return static_cast<VfaSphereVis::VfaSphereVisProps *>(IVflRenderable::GetProperties());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaSphereVis::CreateProperties() const
{
	return new VfaSphereVis::VfaSphereVisProps();
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaSphereVis::VfaSphereVisProps                            */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaSphereVis::VfaSphereVisProps");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<VistaColor, VfaSphereVis::VfaSphereVisProps>(
	"COLOR", STR_REF_TYPENAME,
	&VfaSphereVis::VfaSphereVisProps::GetColor),
	new TVistaPropertyGet<float, VfaSphereVis::VfaSphereVisProps>(
	"LINE_WIDTH", STR_REF_TYPENAME,
	&VfaSphereVis::VfaSphereVisProps::GetLineWidth),
	NULL
};
//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<const VistaColor &, VistaColor, VfaSphereVis::VfaSphereVisProps>(
	"COLOR", STR_REF_TYPENAME,
	&VfaSphereVis::VfaSphereVisProps::SetColor),
	new TVistaPropertySet<float, float, VfaSphereVis::VfaSphereVisProps>(
	"LINE_WIDTH", STR_REF_TYPENAME,
	&VfaSphereVis::VfaSphereVisProps::SetLineWidth),
	NULL 
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaSphereVis::VfaSphereVisProps::VfaSphereVisProps()
:	m_bSolid(false),
	m_fLineWidth(0.1f),
	m_bUseLighting(false)
{
	//blue border
	m_fColor[0] = 0.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 1.0f;
	m_fColor[3] = 1.0f;
}
VfaSphereVis::VfaSphereVisProps::~VfaSphereVisProps()
{}


/*============================================================================*/
/*  NAME      :   Set/GetColor                                                */
/*============================================================================*/
bool VfaSphereVis::VfaSphereVisProps::SetColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor, VistaColor::RGBA);
	return this->SetColor(fColor);
}

VistaColor VfaSphereVis::VfaSphereVisProps::GetColor() const
{
	return VistaColor(m_fColor);
}

bool VfaSphereVis::VfaSphereVisProps::SetColor(float fColor[4])
{
	if(	fColor[0] == m_fColor[0] &&
		fColor[1] == m_fColor[1] &&
		fColor[2] == m_fColor[2] &&
		fColor[3] == m_fColor[3])
	{
		return false;
	}

	memcpy(m_fColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHG);
	return true;
}

void VfaSphereVis::VfaSphereVisProps::GetColor(float fColor[4]) const
{
	memcpy(fColor, m_fColor, 4*sizeof(float));
}

/*============================================================================*/
/*  NAME      :   Set/GetToSolid                                              */
/*============================================================================*/
bool VfaSphereVis::VfaSphereVisProps::SetToSolid(bool solid)
{
	if(m_bSolid == solid)
		return false;

	m_bSolid = solid;
	this->Notify(MSG_SOLID_CHG);
	return true;
}
bool VfaSphereVis::VfaSphereVisProps::GetIsSolid() const
{
	return m_bSolid;
}

/*============================================================================*/
/*  NAME      :   Set/GetUseLighting                                          */
/*============================================================================*/
bool VfaSphereVis::VfaSphereVisProps::SetUseLighting(bool b)
{
	if(m_bUseLighting == b)
		return false;

	m_bUseLighting = b;
	return true;
}

bool VfaSphereVis::VfaSphereVisProps::GetUseLighting() const
{
	return m_bUseLighting;
}


/*============================================================================*/
/*  NAME      :   Set/GetLineWidth                                            */
/*============================================================================*/

bool VfaSphereVis::VfaSphereVisProps::SetLineWidth(float f)
{
	if(f == m_fLineWidth)
		return false;

	m_fLineWidth = f;
	this->Notify(MSG_LINE_WIDTH_CHG);
	return true;
}

float VfaSphereVis::VfaSphereVisProps::GetLineWidth() const
{
	return m_fLineWidth;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaSphereVis::VfaSphereVisProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaSphereVis::VfaSphereVisProps::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaSphereVis.cpp"                                            */
/*============================================================================*/

