/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaQuadHandle.h"
#include "VfaQuadVis.h"
#include "Plane/VfaPlaneModel.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>

using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION					                                          */
/*============================================================================*/
VfaQuadHandle::VfaQuadHandle(VflRenderNode *pRenderNode)
:	IVfaCenterControlHandle()
, 	m_pQuadVis(new VfaQuadVis(new VfaPlaneModel))
{
	//normal color
	m_fNormalColor[0] = 1.0f;
	m_fNormalColor[1] = 0.0f;
	m_fNormalColor[2] = 0.0f;
	m_fNormalColor[3] = 1.0f;
	
	//color if sphere highlighted
	m_fHighlightColor[0] = 1.0f;
	m_fHighlightColor[1] = 0.75f;
	m_fHighlightColor[2] = 0.75f;
	m_fHighlightColor[3] = 1.0f;

	if(m_pQuadVis->Init())
		pRenderNode->AddRenderable(m_pQuadVis);

	m_pQuadVis->GetProperties()->SetFillPlane(true);
	m_pQuadVis->GetProperties()->SetDrawBorder(true);
	m_pQuadVis->GetProperties()->SetFillColor(m_fNormalColor);
	m_pQuadVis->GetProperties()->SetLineColor(m_fNormalColor);
}

VfaQuadHandle::~VfaQuadHandle()
{
	m_pQuadVis->GetRenderNode()->RemoveRenderable(m_pQuadVis);
	delete m_pQuadVis->GetModel();
	delete m_pQuadVis;
}


VfaQuadVis* VfaQuadHandle::GetQuadVis() const
{
	return m_pQuadVis;
}


void VfaQuadHandle::SetVisible(bool b)
{
	m_pQuadVis->SetVisible(b);
}

bool VfaQuadHandle::GetVisible() const
{
	return m_pQuadVis->GetVisible();
}


void VfaQuadHandle::SetCenter(const VistaVector3D &v3Translation)
{
	IVfaCenterControlHandle::SetCenter (v3Translation);
	m_pQuadVis->GetModel()->SetTranslation(v3Translation);
}

void VfaQuadHandle::SetCenter(float fC[3])
{
	IVfaCenterControlHandle::SetCenter(VistaVector3D(fC));
	m_pQuadVis->GetModel()->SetTranslation(fC);
}

void VfaQuadHandle::SetCenter(double dC[3])
{
	IVfaCenterControlHandle::SetCenter (VistaVector3D(dC));
	m_pQuadVis->GetModel()->SetTranslation(dC);
}

void VfaQuadHandle::GetCenter(VistaVector3D &v3Translation)
{
	IVfaCenterControlHandle::GetCenter (v3Translation);
}

void VfaQuadHandle::GetCenter(float fC[3])
{
	VistaVector3D v3;
	IVfaCenterControlHandle::GetCenter(v3);
	v3.GetValues(fC);
}

void VfaQuadHandle::GetCenter(double dC[3])
{
	VistaVector3D v3;
	IVfaCenterControlHandle::GetCenter(v3);
	v3.GetValues(dC);
}


void VfaQuadHandle::SetNormal(const VistaVector3D &v3Normal)
{
	m_pQuadVis->GetModel()->SetNormal(v3Normal);
}
void VfaQuadHandle::GetNormal(VistaVector3D &v3Normal) const
{
	m_pQuadVis->GetModel()->GetNormal(v3Normal);
}


void VfaQuadHandle::SetSize(float w, float h)
{
	m_pQuadVis->GetModel()->SetExtents(w, h);
}

bool VfaQuadHandle::GetSize(float &w, float &h) const
{
	m_pQuadVis->GetModel()->GetExtents(w, h);
	return true;
}


void VfaQuadHandle::SetNormalColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetNormalColor(fC);
}

VistaColor VfaQuadHandle::GetNormalColor() const
{
	VistaColor color(m_fNormalColor);
	return color;
}

void VfaQuadHandle::SetNormalColor(float fNormalColor[4])
{
	m_fNormalColor[0] = fNormalColor[0];
	m_fNormalColor[1] = fNormalColor[1];
	m_fNormalColor[2] = fNormalColor[2];
	m_fNormalColor[3] = fNormalColor[3];
	m_pQuadVis->GetModel()->Notify();
}

void VfaQuadHandle::GetNormalColor(float fNormalColor[4]) const
{
	fNormalColor[0] = m_fNormalColor[0];
	fNormalColor[1] = m_fNormalColor[1];
	fNormalColor[2] = m_fNormalColor[2];
	fNormalColor[3] = m_fNormalColor[3];
}


void VfaQuadHandle::SetHighlightColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetHighlightColor(fC);
}

VistaColor VfaQuadHandle::GetHighlightColor() const
{
	VistaColor color(m_fHighlightColor);
	return color;
}

void VfaQuadHandle::SetHighlightColor(float fHighlightColor[4])
{
	m_fHighlightColor[0] = fHighlightColor[0];
	m_fHighlightColor[1] = fHighlightColor[1];
	m_fHighlightColor[2] = fHighlightColor[2];
	m_fHighlightColor[3] = fHighlightColor[3];
	m_pQuadVis->GetModel()->Notify();
}

void VfaQuadHandle::GetHighlightColor(float fHighlightColor[4]) const
{
	fHighlightColor[0] = m_fHighlightColor[0];
	fHighlightColor[1] = m_fHighlightColor[1];
	fHighlightColor[2] = m_fHighlightColor[2];
	fHighlightColor[3] = m_fHighlightColor[3];
}


void VfaQuadHandle::SetIsHighlighted(bool bIsHighlighted)
{
	IVfaCenterControlHandle::SetIsHighlighted(bIsHighlighted);
	
	if(bIsHighlighted)
	{
		m_pQuadVis->GetProperties()->SetFillColor(m_fHighlightColor);
		m_pQuadVis->GetProperties()->SetLineColor(m_fHighlightColor);
	}
	else
	{
		m_pQuadVis->GetProperties()->SetFillColor(m_fNormalColor);
		m_pQuadVis->GetProperties()->SetLineColor(m_fNormalColor);
	}
}

bool VfaQuadHandle::GetIsHighlighted() const
{
	return IVfaCenterControlHandle::GetIsHighlighted();
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaQuadHandle.cpp"								              */
/*============================================================================*/



