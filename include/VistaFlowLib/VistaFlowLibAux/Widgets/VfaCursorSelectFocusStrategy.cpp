/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaCursorSelectFocusStrategy.h"
#include "VfaWidgetController.h"
#include "VfaControlHandle.h"

#include <VistaKernel/InteractionManager/VistaInteractionEvent.h>
#include <VistaKernel/InteractionManager/VistaInteractionContext.h>

#include <algorithm>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaCursorSelectFocusStrategy::VfaCursorSelectFocusStrategy(int iCommandSlot)
: m_iCursorPos(0),
  m_iCommandSlot(iCommandSlot)
{

}

VfaCursorSelectFocusStrategy::~VfaCursorSelectFocusStrategy()
{

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME    : EvaluateFocus                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCursorSelectFocusStrategy::EvaluateFocus(std::vector<IVfaControlHandle*> &vecControlHandles)
{
	// in this strategy, only one widget, i.e. one control handle, has the focus
	vecControlHandles.clear();

	// check if this is correct
	// if the iterator is at end(), we select no widget
	if(m_iterCurser != m_liWidgetCtrls.end())
		vecControlHandles.push_back((*m_iterCurser)->GetControlHandles().front());

	return true;
}



/*============================================================================*/
/*                                                                            */
/*  NAME    : RegisterSelectable                                              */
/*                                                                            */
/*============================================================================*/
void VfaCursorSelectFocusStrategy::RegisterSelectable(IVfaWidgetController* pSelectable)
{
	m_liWidgetCtrls.push_back(pSelectable);
	m_iterCurser = m_liWidgetCtrls.begin();
}

/*============================================================================*/
/*                                                                            */
/*  NAME    : UnregisterSelectable                                            */
/*                                                                            */
/*============================================================================*/
void VfaCursorSelectFocusStrategy::UnregisterSelectable(IVfaWidgetController* pSelectable)
{
	if(std::find(m_liWidgetCtrls.begin(), m_liWidgetCtrls.end(), pSelectable)!= m_liWidgetCtrls.end())
	{
		m_liWidgetCtrls.remove(pSelectable);

		m_iterCurser = m_liWidgetCtrls.begin();
	}
}


void VfaCursorSelectFocusStrategy::OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	return;
}

void VfaCursorSelectFocusStrategy::OnCommandSlotUpdate(int iSlot, const bool & bSet)
{
	if(iSlot != m_iCommandSlot)
		return;
	if(!bSet)
		return;

	// if we are at end(), start from beginning(this counts as an increase)
	if(m_iterCurser == m_liWidgetCtrls.end())
		m_iterCurser = m_liWidgetCtrls.begin();
	else
		// increase counter
		m_iterCurser++;

	return;
}

void VfaCursorSelectFocusStrategy::OnTimeSlotUpdate(const double & dTime)
{
}

int VfaCursorSelectFocusStrategy::GetSensorMask() const
{
	return 0;
}
int VfaCursorSelectFocusStrategy::GetCommandMask() const
{
	return(1 << m_iCommandSlot);
}

bool VfaCursorSelectFocusStrategy::GetTimeUpdate() const
{
	return false;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VfaCursorSelectFocusStrategy.cpp"                             */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/
