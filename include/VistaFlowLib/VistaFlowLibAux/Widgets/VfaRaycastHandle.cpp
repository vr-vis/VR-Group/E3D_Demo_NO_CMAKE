/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES														              */
/*============================================================================*/
#include "VfaRaycastHandle.h"

#include <VistaAspects/VistaTransformable.h>
#include <VistaBase/VistaVectorMath.h>


/*============================================================================*/
/* IMPLEMENTATION												              */
/*============================================================================*/
IVfaRaycastHandle::IVfaRaycastHandle(IVistaTransformable *pCoordFrame)
:	IVfaControlHandle(HANDLE_RAYCAST_HANDLE)
, 	m_pCoordFrame(pCoordFrame)
{ }

IVfaRaycastHandle::~IVfaRaycastHandle()
{ }


IVistaTransformable* IVfaRaycastHandle::GetCoordFrame() const
{
	return m_pCoordFrame;
}


void IVfaRaycastHandle::TransformGlobalToLocal(
	VistaVector3D &v3RayOrigin, VistaVector3D &v3RayDir) const
{
	if(!GetCoordFrame())
		return;

	// Build inverse transformation matrix of the handle's coord frame.
	// It is needed to transform position/orientation from the global to the
	// local coord frame. Remember: To a frame = inverse, from a frame = normal
	// transform matrix.
	float aMat[16];
	GetCoordFrame()->GetWorldTransform(aMat);
	VistaTransformMatrix oInvMat(aMat);
	oInvMat = oInvMat.GetInverted();

	// Transform origin from global to handle frame.
	v3RayOrigin = oInvMat.TransformPoint(v3RayOrigin);
	v3RayOrigin[3] = 1.0f;

	// Transform direction from global to handle frame.
	v3RayDir	= oInvMat.TransformVector(v3RayDir);
	v3RayDir[3] = 0.0f;
	v3RayDir.Normalize();
}


/*============================================================================*/
/* END OF FILE													              */
/*============================================================================*/
