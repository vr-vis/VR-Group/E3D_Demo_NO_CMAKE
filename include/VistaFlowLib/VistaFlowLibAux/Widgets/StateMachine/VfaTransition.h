/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFATRANSITION_H
#define VFATRANSITION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"

#include <VistaAspects/VistaObserveable.h>
#include <VistaAspects/VistaNameable.h>

#include <string>


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI IVfaTransition : public IVistaObserveable
	, public IVistaNameable
{
friend class VfaStateMachine;

public:
	enum
	{
		MSG_FIRING_TRANSITION = IVistaObserveable::MSG_LAST,	// Before Fire
		MSG_FIRED_TRANSITION,									// After Fire
		MSG_LAST
	};

	IVfaTransition();
	virtual ~IVfaTransition();

	virtual std::string GetNameForNameable() const;
	virtual void SetNameForNameable(const std::string &sNewName);

protected:
	virtual bool FireTransition();

	//! Gets called by the state machine when the transition is executed.
	virtual bool Fire() = 0;

private:
	std::string m_strName;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
