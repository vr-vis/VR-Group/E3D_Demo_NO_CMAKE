/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFASTATE_H
#define VFASTATE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"

#include <VistaAspects/VistaObserveable.h>
#include <VistaAspects/VistaNameable.h>
#include <string>


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI IVfaState : public IVistaObserveable, 
	public IVistaNameable
{
friend class VfaStateMachine;

public:
	enum
	{
		MSG_ENTERING_STATE = IVistaObserveable::MSG_LAST,	// Before Enter
		MSG_ENTERED_STATE,									// After Enter
		MSG_EXITING_STATE,									// Before Exit
		MSG_EXITED_STATE,									// After Exit
		MSG_LAST
	};

	IVfaState();
	virtual ~IVfaState();

	virtual std::string GetNameForNameable() const;
	virtual void SetNameForNameable(const std::string &sNewName);

protected:
	// This method is called by VfaStateMachine
	// It sends a message before Enter() is called(MSG_ENTERING_STATE)
	// and after Enter() was called(MSG_ENTERED_STATE)
	virtual bool EnterState();
	// This method is called by VfaStateMachine
	// It sends a message before Exit() is called(MSG_EXITING_STATE)
	// and after Exit() was called(MSG_EXITED_STATE)
	virtual bool ExitState();

	//! Gets called by the state machine via EnterState when entering the state.
	virtual bool Enter() = 0;
	//! Gets called by the state machine via ExitState when exiting the state.
	virtual bool Exit() = 0;
	
private:
	std::string m_strName;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
