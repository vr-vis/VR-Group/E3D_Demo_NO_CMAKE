/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaStateMachine.h"

#include "VfaState.h"
#include "VfaDeadState.h"
#include "VfaEmptyTransition.h"

#include <cassert>

// ========================================================================== //
// === Consts
// ========================================================================== //

// ========================================================================== //
// === Con-/Destructor
// ========================================================================== //
VfaStateMachine::VfaStateMachine()
:	m_pStartState( NULL )
,   m_pCurrentState( NULL )
,	m_pDeadTransition( new VfaEmptyTransition )
,   m_pDefaultTransition( m_pDeadTransition )
,   m_pLastSuccessfulTrans( NULL )
,   m_bTolerateTransFailure( false )
,	m_pDeadState( new VfaDeadState )
{
	this->AddState( m_pDeadState );

	this->AddTransition( m_pDeadTransition );
	this->WireStates( m_pDeadTransition, m_pDeadState, m_pDeadState );
}

VfaStateMachine::~VfaStateMachine()
{

}


/*============================================================================*/
/* IMPLEMENTATION	                                                          */
/*============================================================================*/
bool VfaStateMachine::AddState( IVfaState *pState, 
	bool bIsAcceptState /* = false */ )
{
	// We won't allow empty states!
	if(!pState)
		return false;

	std::set<IVfaState*>::iterator itState;

	// Check if there already is this state.
	itState = m_setStates.find(pState);

	// Found a state with that ID --> error!
	if(itState != m_setStates.end())
		return false;

	// Now add the (new!) state to the fsm.
	m_setStates.insert(pState);
	
	// Add an entry to the transition table.
	std::map<IVfaTransition*, IVfaState*> mapEmptyMap;
	std::pair<IVfaState*, std::map<IVfaTransition*, IVfaState*> > 
		oNewTransEntry(pState, mapEmptyMap);
	m_mapStateToTransition.insert(oNewTransEntry);

	// And in case it is meant to be an accept state, add it to that map, too!
	if(bIsAcceptState)
	{
		m_setAcceptStates.insert(pState);
	}

	return true;
}

bool VfaStateMachine::RemoveState( IVfaState *pState )
{
	std::set<IVfaState*>::iterator itState;

	// Search the state.
	itState = m_setStates.find(pState);

	// Didn't find the state --> error!
	if(itState == m_setStates.end())
		return false;

	// Check transition map.
	std::map<IVfaState*, std::map<IVfaTransition*, IVfaState*> >::iterator 
		itStateToTransMap = m_mapStateToTransition.find(pState);

	// If there was no transition entry, then an error occured!
	// After all we had that state in the fsm!
	assert(itStateToTransMap != m_mapStateToTransition.end());

	m_mapStateToTransition.erase(itStateToTransMap);

	// Finally, check the accept state map for an entry.
	std::set<IVfaState*>::iterator itAccState;
	itAccState = m_setAcceptStates.find(pState);

	// If there was an entry, delete it.
	if(itAccState != m_setAcceptStates.end())
	{
		m_setAcceptStates.erase(itAccState);
	}

	// Finally delete all transitions that lead into this state.
	itStateToTransMap = m_mapStateToTransition.begin();

	std::map<IVfaTransition*, IVfaState*>::iterator itTrans, itTransBefore;

	// Look through the transition maps of all states.
	while(itStateToTransMap != m_mapStateToTransition.end())
	{
		// 'itTrans' it the one we want to delete.
		// 'itTransBefore' is the one before that.
		// Why do we do that? Well, we want to delete by data and not by key
		// so we need to manually run through the map and check the data entry
		// of each map entry. In case we delete a map entry the map after that
		// will change and the iterator 'itTrans' becomes invalid. So we jump
		// back one entry and move forward once again to reach the next valid
		// entry and not miss anything.
		itTrans = (*itStateToTransMap).second.begin();
		itTransBefore = itTrans;

		// Look through every entry of each states transition map.
		while(itTrans != (*itStateToTransMap).second.end())
		{
			// If the target state is the one we deleted above remove the trans.
			if((*itTrans).second == pState)
			{
				if(itTrans == (*itStateToTransMap).second.begin())
				{
					(*itStateToTransMap).second.erase(itTrans);
					itTrans = (*itStateToTransMap).second.begin();
				}
				else
				{
					(*itStateToTransMap).second.erase(itTrans);
					itTrans = itTransBefore;
				}

				continue;
			}

			++itTrans;

			if(itTrans != (*itStateToTransMap).second.begin())
			{
				itTransBefore = itTrans;
				--itTransBefore;
			}
		}

		++itStateToTransMap;
	}

	// Delete the map entry.
	m_setStates.erase(pState);

	return true;
}


bool VfaStateMachine::AddTransition( IVfaTransition *pTrans )
{
	if(!pTrans)
		return false;

	// Check if this transition already exists.
	std::set<IVfaTransition*>::iterator itTrans;
	itTrans = m_setTransitions.find(pTrans);

	if(itTrans != m_setTransitions.end())
		return false;

	m_setTransitions.insert(pTrans);

	return true;
}

bool VfaStateMachine::RemoveTransition( IVfaTransition *pTrans )
{
	// Take a short cut out, if possible.
	// Find the transition.
	std::set<IVfaTransition*>::iterator itTrans;
	m_setTransitions.find(pTrans);

	// If you couldn't find it you cannot delete it!
	if(itTrans == m_setTransitions.end())
		return false;

	// Remove from transition data structure.
	m_setTransitions.erase(itTrans);

	// Next, we need to remove the entries form all states' transtion tables.
	std::map<IVfaState*, std::map<IVfaTransition*, IVfaState*> >::iterator 
		itTransMap = m_mapStateToTransition.begin();

	std::map<IVfaTransition*, IVfaState*>::iterator itTransMapEntry;

	while(itTransMap != m_mapStateToTransition.end())
	{
		// Look for a matching entry in that states table.
		itTransMapEntry = (*itTransMap).second.find(pTrans);

		// If there was one it needs to be deleted.
		if(itTransMapEntry != (*itTransMap).second.end())
		{
			(*itTransMap).second.erase(itTransMapEntry);
		}

		++itTransMap;
	}

	return true;
}


bool VfaStateMachine::SetDefaultTransition( IVfaTransition *pTrans )
{
	// Try to validate the existence of the supplied transition name.
	std::set<IVfaTransition*>::iterator itTrans;
	itTrans = m_setTransitions.find(pTrans);

	// The transition does not seem to exist.
	if(itTrans == m_setTransitions.end())
		return false;

	m_pDefaultTransition = pTrans;	

	return true;
}

IVfaTransition* VfaStateMachine::GetDefaultTransition() const
{
	return m_pDefaultTransition;
}


bool VfaStateMachine::WireStates( IVfaTransition* pTrans, 
	IVfaState* pFromState, 
	IVfaState* pToState )
{
	// First, check if the transition exists.
	std::set<IVfaTransition*>::iterator itTrans;
	itTrans = m_setTransitions.find(pTrans);

	// This transition does not exist, return failure.
	if(itTrans == m_setTransitions.end())
		return false;

	// Check if the from and to states exist.
	std::map<IVfaState*, std::map<IVfaTransition*, IVfaState*> >::iterator 
		itTransMap = m_mapStateToTransition.find(pToState);
	
	// Ooops, the to state does not exist.
	if(itTransMap == m_mapStateToTransition.end())
		return false;

	itTransMap = m_mapStateToTransition.find(pFromState);

	// Hmmm, the from state does not exist.
	if(itTransMap == m_mapStateToTransition.end())
		return false;

	// Look for the transition in the from state's transition map.
	std::map<IVfaTransition*, IVfaState*>::iterator itTransMapEntry;
	itTransMapEntry = (*itTransMap).second.find(pTrans);

	// The transtition already exists, we're leaving.
	if(itTransMapEntry != (*itTransMap).second.end())
		return false;

	// Let's add the (actually) new transition.
	std::pair<IVfaTransition*, IVfaState*> oNewEntry(pTrans, pToState);
	(*itTransMap).second.insert(oNewEntry);

	return true;
}

bool VfaStateMachine::UnwireStates( IVfaTransition* pTrans, 
	IVfaState* pFromState )
{
	// Let's find the transition map for the from state.
	std::map<IVfaState*, std::map<IVfaTransition*, IVfaState*> >::iterator 
	itTransMap = m_mapStateToTransition.find(pFromState);

	// Well, there was no such state!!
	if(itTransMap == m_mapStateToTransition.end())
		return false;

	// Next, let's search for the transition.
	std::map<IVfaTransition*, IVfaState*>::iterator itTrans;
	itTrans = (*itTransMap).second.find(pTrans);

	// Well, no such transition!
	if(itTrans == (*itTransMap).second.end())
		return false;

	// Delete the transition.
	(*itTransMap).second.erase(itTrans);

	return true;
}


bool VfaStateMachine::SetStartState( IVfaState* pState )
{
	// Let's look if the state exists.
	std::set<IVfaState*>::iterator itState;
	itState = m_setStates.find(pState);

	// No such state? Well, then it cannot be the start state.
	if(itState == m_setStates.end())
		return false;

	m_pStartState = pState;

	return true;
}


IVfaState* VfaStateMachine::GetStartState() const
{
	return m_pStartState;
}


void VfaStateMachine::SetTolerateTransitionFailure( bool bTolerateFailure )
{
	m_bTolerateTransFailure = bTolerateFailure;
}

bool VfaStateMachine::GetTolerateTransitionFailure() const
{
	return m_bTolerateTransFailure;
}

bool VfaStateMachine::Reset()
{
	// No start state? Then we cannot reset!
	if(m_pStartState == NULL)
		return false;
	if ( m_pCurrentState )
	{
		m_pCurrentState->ExitState();
	}
	// Reset the current state to the start state.
	m_pCurrentState = m_pStartState;

	// Get the state object.
	std::set<IVfaState*>::iterator itState;
	itState = m_setStates.find(m_pCurrentState);

	if(itState == m_setStates.end())
		return false;

	// Finally, activate the state object.
	m_pCurrentState->EnterState();

	this->Notify(MSG_STATE_MACHINE_RESET);

	return true;
}

bool VfaStateMachine::AttemptTransition( IVfaTransition* pTrans )
{
	// The transition that will be fired. Initially we assume the supplied
	// transition is the right one.
	IVfaTransition* pTakeTrans = pTrans;

	// Get the Trans map of the current state.
	std::map<IVfaState*, std::map<IVfaTransition*, IVfaState*> >::iterator 
	itTransMap = m_mapStateToTransition.find(m_pCurrentState);

	// Check if the requested transition exists.
	std::map<IVfaTransition*, IVfaState*>::iterator itTransMapEntry;
	itTransMapEntry = (*itTransMap).second.find(pTakeTrans);

	// Nope, no such transition.
	if(itTransMapEntry == (*itTransMap).second.end())
	{
		// Let's check for the universal default transition.
		pTakeTrans = this->GetDefaultTransition();
		itTransMapEntry == (*itTransMap).second.find(pTakeTrans);		
		
		// Well, no such transition either --> failure.
		if(itTransMapEntry == (*itTransMap).second.end())
		{
			// If failure toleration is off, we move to the dead state.
			if(!m_bTolerateTransFailure)
			{
				m_pCurrentState->ExitState();

				m_pCurrentState = m_pDeadState;
				m_pCurrentState->EnterState();

				m_pLastSuccessfulTrans = m_pDeadTransition;

				this->Notify(MSG_ENTERED_DEAD_STATE);
			}

			return false;
		}
	}

	// Get a ptr to the transition which will be used.
	std::set<IVfaTransition*>::iterator itTrans =
		m_setTransitions.find(pTakeTrans);
	// This _MUST_ work or something is broken in the code.
	assert(itTrans != m_setTransitions.end());

	// Exit the current state.
	m_pCurrentState->ExitState();

	// Fire the transition.
	pTakeTrans->FireTransition();
	
	// Enter the target state.
	m_pCurrentState = (*itTransMapEntry).second;

	m_pCurrentState->EnterState();

	m_pLastSuccessfulTrans = pTakeTrans;
	
	// Some notifications here.
	this->Notify(MSG_STATE_CHANGED);

	if(this->GetIsCurrentStateAcceptState())
		this->Notify(MSG_ENTERED_ACCEPT_STATE);

	return true;
}


IVfaState* VfaStateMachine::GetCurrentState() const
{
	return m_pCurrentState;
}

IVfaTransition* VfaStateMachine::GetLastSuccessfulTransition() const
{
	return m_pLastSuccessfulTrans;
}

IVfaState* VfaStateMachine::GetCurrentStateObject() const
{
	return m_pCurrentState;
}

IVfaTransition* VfaStateMachine::GetLastSuccessfulTransitionObject() const
{
	return m_pLastSuccessfulTrans;
}

bool VfaStateMachine::GetIsCurrentStateAcceptState() const
{
	std::set<IVfaState*>::const_iterator itAccState;
	itAccState = m_setAcceptStates.find(m_pCurrentState);

	if(itAccState == m_setAcceptStates.end())
		return false;

	return true;
}

bool VfaStateMachine::GetIsCurrentStateDeadState() const
{
	return (m_pCurrentState == m_pDeadState);
}


unsigned int VfaStateMachine::GetNumberOfStates() const
{
	return (unsigned int)m_setStates.size();
}

unsigned int VfaStateMachine::GetNumberOfTransitions() const
{
	return (unsigned int)m_setTransitions.size();
}

// ========================================================================== //
// === End of File
// ========================================================================== //
