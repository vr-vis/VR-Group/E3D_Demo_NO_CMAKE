/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaState.h"

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
IVfaState::IVfaState()
	: m_strName( "" )
{ }

IVfaState::~IVfaState()
{ }

bool IVfaState::EnterState()
{
	bool bResult;
	Notify( MSG_ENTERING_STATE );
	bResult = Enter();
	Notify( MSG_ENTERED_STATE );
	return bResult;
}

bool IVfaState::ExitState()
{
	bool bResult;
	Notify( MSG_EXITING_STATE );
	bResult = Exit();
	Notify( MSG_EXITED_STATE );
	return bResult;
}

std::string IVfaState::GetNameForNameable() const
{
	return m_strName;
}

void IVfaState::SetNameForNameable( const std::string &sNewName )
{
	m_strName = sNewName;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
