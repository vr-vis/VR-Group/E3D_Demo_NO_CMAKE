/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFASTATEMACHINE_H
#define VFASTATEMACHINE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"

#include <VistaAspects/VistaObserveable.h>

#include <map>
#include <set>


/*============================================================================*/
/* FORWARD DECLARATION                                                        */
/*============================================================================*/
class IVfaState;
class IVfaTransition;


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaStateMachine : public IVistaObserveable
{
public:
	enum eMessages
	{
		MSG_STATE_CHANGED = IVistaObserveable::MSG_LAST,
		MSG_TRANSITION_FIRED,
		MSG_ENTERED_ACCEPT_STATE,
		MSG_ENTERED_DEAD_STATE,
		MSG_STATE_MACHINE_RESET,
		MSG_LAST
	};

	enum eTickets
	{
		TICKET_STATE_MACHINE = IVistaObserveable::TICKET_LAST,
		TICKET_LAST
	};

	VfaStateMachine();
	virtual ~VfaStateMachine();

	bool AddState( IVfaState *pState, bool bIsAcceptState = false );
	bool RemoveState( IVfaState *pState );

	bool AddTransition( IVfaTransition *pTrans );
	bool RemoveTransition( IVfaTransition *pTrans );

	/*!
	 * NOTE: If a transition is attempted which does not work for the current
	 *		 state but the state owns a transition '__DEFAULT__' this transition
	 *		 will be fired for the involved state.
	 *		 The default default transition :P is an empty one!
	 */
	bool SetDefaultTransition( IVfaTransition *pTrans );
	IVfaTransition* GetDefaultTransition() const;

	bool WireStates( IVfaTransition* pTrans, 
		IVfaState* pFromState, 
		IVfaState* pToState );
	bool UnwireStates( IVfaTransition* pTrans, IVfaState* pFromState );

	bool SetStartState( IVfaState* pState );
	IVfaState* GetStartState() const;

	//! Switches the transition failure toleration on and off.
	/*!
	 * NOTE: Switching the toleration on will silently ignore failing
	 *		 transitions (e.g. unknown transitions) and continue as if
	 *		 nothing happened. Switching it off will move the fsm to
	 *		 a dead state from which it won't be able to return.
	 *		 Changes are applied immediately!
	 * DEFAULT: Toleration is off!
	 */
	void SetTolerateTransitionFailure( bool bTolerateFailure );
	bool GetTolerateTransitionFailure() const;

	//! Resets the state machine to its initial state.
	bool Reset();
	bool AttemptTransition( IVfaTransition* pTrans );

	IVfaState* GetCurrentState() const;
	IVfaTransition* GetLastSuccessfulTransition() const;

	IVfaState* GetCurrentStateObject() const;
	IVfaTransition* GetLastSuccessfulTransitionObject() const;

	bool GetIsCurrentStateAcceptState() const;
	bool GetIsCurrentStateDeadState() const;

	unsigned int GetNumberOfStates() const;
	unsigned int GetNumberOfTransitions() const;
	
private:
	//! Holds the ID together with the associated state (states owned by fsm).
	//std::map<int, IVfaState*> m_setStates;
	std::set<IVfaState*> m_setStates;
	//! Holds the ID together with the associated trans (trans owned by fsm).
	std::set<IVfaTransition*> m_setTransitions;
	//! Data structure holding states and their transition map.
	std::map<IVfaState*, std::map<IVfaTransition*, IVfaState*> > m_mapStateToTransition;
	//! A map holding the accept states.
	std::set<IVfaState*> m_setAcceptStates;
	
	IVfaState		*m_pStartState;
	IVfaTransition	*m_pDefaultTransition;
	IVfaState		*m_pCurrentState;
	IVfaTransition	*m_pLastSuccessfulTrans;

	bool m_bTolerateTransFailure;

	IVfaState *m_pDeadState;
	IVfaTransition *m_pDeadTransition;
	
};

#endif // Include guard.


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
