

set( RelativeDir "./Widgets/StateMachine" )
set( RelativeSourceGroup "Source Files\\Widgets\\StateMachine" )

set( DirFiles
	VfaDeadState.h
	VfaEmptyState.h
	VfaEmptyTransition.h
	VfaState.cpp
	VfaState.h
	VfaStateMachine.cpp
	VfaStateMachine.h
	VfaTransition.cpp
	VfaTransition.h	
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

