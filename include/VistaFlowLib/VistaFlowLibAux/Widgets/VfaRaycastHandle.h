/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFARAYCASTHANDLE_H
#define VFARAYCASTHANDLE_H

/*============================================================================*/
/* INCLUDES														              */
/*============================================================================*/
#include "../VistaFlowLibAuxConfig.h"
#include "VfaControlHandle.h"


/*============================================================================*/
/* FORWARD DECLARATION											              */
/*============================================================================*/
class IVistaTransformable;


/*============================================================================*/
/* CLASS DEFINITION												              */
/*============================================================================*/
/**
 * This is the base class for handles based on the VfaRaycastFocusStrategy.
 */
class VISTAFLOWLIBAUXAPI IVfaRaycastHandle : public IVfaControlHandle
{
public:
	//! Extended handle type list.
	enum eTypes
	{
		HANDLE_RAYCAST_HANDLE = IVfaControlHandle::HANDLE_FIRST_EXTERNAL,
		HANDLE_LAST
	};

	/**
	 * @param pCoordFrame The coordinate frame in which the handle resides in.
	 *		  It is used to transform rays from other coordinate frame into the
	 *		  handle's coord frame. So you better specify it correctly if
	 *		  neccessary.
	 * 
	 * NOTE: For FlowLib applications you can pass
	 *		 VflRenderNode::GetTransformable() here.
	 */
	IVfaRaycastHandle(IVistaTransformable *pCoordFrame);
	virtual ~IVfaRaycastHandle();


	//! Retrieves the coordinate frame of the handle.
	/**
	 * @return Returns a pointer to a transformable which is used to specify
	 *		   the handle's coordinate frame.
	 */
	IVistaTransformable* GetCoordFrame() const;

	//! Tests if the specified ray intersects the handle.
	/**
	 * @param v3RayOrigin The point from which the ray originated.
	 * @param v3RayDir The direction into which the ray travels. Should be
	 *		  normalized!!
	 * @param fParam The parameter value which can be used to calculate the
	 *		  exact intersection point. Only valid if the function returns
	 *		  'true'.
	 * @return Returns 'true' iff an intersection was detected.
	 *
	 * NOTE: The ray is assumed to be specified in the global coordinate frame!
	 *		 Each handle is then responsible to transform the ray from the
	 *		 global to the local coordinate frame, if neccessary. A convenience
	 *		 function is made available for this: \sa TransformGlobalToLocal().
	 */
	virtual bool Intersects(const VistaVector3D &v3RayOrigin,
		const VistaVector3D &v3RayDir, float &fParam) = 0;

	//! Transforms a ray from the global to the handle's local frame.
	/**
	 * @param v3RayOrigin The ray origin in the global coord frame.
	 * @param v3RayDir The ray direction in the global coord frame.
	 */
	void TransformGlobalToLocal(VistaVector3D &v3RayOrigin,
		VistaVector3D &v3RayDir) const;

private:
	//! Holds the coord frame of the handle (to transform global ray into it).
	IVistaTransformable	*m_pCoordFrame;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE													              */
/*============================================================================*/
