/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES																  */
/*============================================================================*/
#include "VfaWidgetController.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <algorithm>

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
IVfaWidgetController::IVfaWidgetController(VflRenderNode *pRenderNode) 
:	m_pRenderNode(pRenderNode),
	m_iControllerState(IVfaWidgetController::CS_NONE)
{

}

IVfaWidgetController::~IVfaWidgetController()
{

}

const std::vector<IVfaControlHandle*>& IVfaWidgetController::GetControlHandles() const
{
	return m_vecControlHandles;
}

void IVfaWidgetController::OnFocus(IVfaControlHandle* pHandle)
{
}

void IVfaWidgetController::OnUnfocus()
{
}

void IVfaWidgetController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{
}

void IVfaWidgetController::OnUntouch()
{
}


void IVfaWidgetController::AddControlHandle(IVfaControlHandle* pHandle)
{
	if(pHandle==NULL)
		return;
	m_vecControlHandles.push_back(pHandle);
}

void IVfaWidgetController::RemoveControlHandle(IVfaControlHandle *pHandle)
{
	if(pHandle==NULL)
		return;

	std::vector<IVfaControlHandle*>::iterator itFind = 
			std::find(m_vecControlHandles.begin(), m_vecControlHandles.end(), pHandle);
	if(itFind != m_vecControlHandles.end())
		m_vecControlHandles.erase(itFind);
}

void IVfaWidgetController::SetControllerState (IVfaWidgetController::eControllerState iNewState, const std::vector<IVfaControlHandle*>& vecHandles)
{
	// if we already have this state, do nothing
	if(iNewState == m_iControllerState)
		return;

	// "un"-calls for old state
	switch(m_iControllerState)
	{
	case CS_TOUCH: 
		this->OnUntouch();
		break;
	case CS_FOCUS: 
		// we don't allow a transition from TOUCH to FOCUS directly, so return here
		if(iNewState == CS_TOUCH)
			return;
		this->OnUnfocus();
		break;
	case CS_NONE: 
	default:
		// nothing
		;
	};

	// now, set new controller state
	m_iControllerState = iNewState;

	switch(m_iControllerState)
	{
	case CS_TOUCH: 
		this->OnTouch(vecHandles);
		break;
	case CS_FOCUS:
		this->OnFocus(vecHandles.front());
		break;
	case CS_NONE: 
	default:
		// nothing
		;
	};

	this->Notify(WIDGETCONTROLLER_STATECHANGE);

}
IVfaWidgetController::eControllerState IVfaWidgetController::GetControllerState() const
{
	return m_iControllerState;
}

bool IVfaWidgetController::ExchangeRenderNode(VflRenderNode *pRenderNode)
{
	m_pRenderNode = pRenderNode;

	return true;
}

VflRenderNode* IVfaWidgetController::GetRenderNode() const
{
	return m_pRenderNode;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Focus			                                              */
/*                                                                            */
/*============================================================================*/
//void IVfaWidgetController::Focus(IVfaControlHandles* pHandle)
//{
//}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeFocus			                                          */
/*                                                                            */
/*============================================================================*/
//void IVfaWidgetController::Defocus(IVfaControlHandles* pHandle)
//{
//}
/*============================================================================*/
/*  END OF FILE "VfaWidgetController.cpp"                                     */
/*============================================================================*/
