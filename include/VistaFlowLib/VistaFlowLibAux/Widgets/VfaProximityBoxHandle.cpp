/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <cstring>

#include "VfaProximityBoxHandle.h"

#include "VfaBoxVis.h"
#include "Box/VfaBoxModel.h"
#include "VfaWidgetTools.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaProximityBoxHandle::VfaProximityBoxHandle(VflRenderNode *pRenderNode)
	:	IVfaProximityHandle(IVfaControlHandle::HANDLE_PROXIMITY_BOX),
		m_pVis(new VfaBoxVis(new VfaBoxModel))
{
	//define default appearence
	m_fNormalLW = 1.0f;
	m_fHighlightLW = 3.0f;
	m_fNormalColor[0] = m_fNormalColor[1] = 0.0f;
	m_fNormalColor[2] = m_fNormalColor[3] = 1.0f;
	m_fHighlightColor[0] = m_fHighlightColor[1] = 0.4f;
	m_fHighlightColor[2] = m_fHighlightColor[3] = 1.0f;

	//init a box visualization for the handle stuff
	if(m_pVis->Init())
		pRenderNode->AddRenderable(m_pVis);

	//set default properties
	VfaBoxVis::VfaBoxVisProps *pProps = m_pVis->GetProperties();
	pProps->SetLineColor(m_fNormalColor);
	pProps->SetLineWidth(m_fNormalLW);
	pProps->SetDrawFilledFaces(false);
}

VfaProximityBoxHandle::~VfaProximityBoxHandle()
{
	m_pVis->GetRenderNode()->RemoveRenderable(m_pVis);
	delete m_pVis;
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaProximityBoxHandle  		   			          */
/*============================================================================*/
void VfaProximityBoxHandle::SetIsHighlighted(bool bIsHighlighted)
{
	VfaBoxVis::VfaBoxVisProps *pProps = m_pVis->GetProperties();
	if(bIsHighlighted)
	{
		pProps->SetLineColor(m_fHighlightColor);
		pProps->SetLineWidth(m_fHighlightLW);
	}
	else
	{
		pProps->SetLineColor(m_fNormalColor);
		pProps->SetLineWidth(m_fNormalLW);
	}
}

/*============================================================================*/
/*  NAME:      SetVisEnabled			  		   							  */
/*============================================================================*/
void VfaProximityBoxHandle::SetVisEnabled(bool b)
{
	m_pVis->SetVisible(b);
}

/*============================================================================*/
/*  NAME      Set/GetBox						  		   			          */
/*============================================================================*/
void VfaProximityBoxHandle::SetBox(float fBounds[6])
{
	//keep the bounds in the vis anyway!
	m_pVis->GetModel()->SetAABox(fBounds);
}
void VfaProximityBoxHandle::SetBox(const VistaVector3D& v3Min, 
									const VistaVector3D &v3Max)
{
	float fBounds[6] = {v3Min[0], v3Max[0], 
						v3Min[1], v3Max[1], 
						v3Min[2], v3Max[2]};
	m_pVis->GetModel()->SetAABox(fBounds);
}

void VfaProximityBoxHandle::GetBox(float fBounds[6]) const
{
	m_pVis->GetModel()->GetAABox(fBounds);
}
void VfaProximityBoxHandle::GetBox(VistaVector3D &v3Min, 
									VistaVector3D &v3Max) const
{
	float fBounds[6];
	m_pVis->GetModel()->GetAABox(fBounds);
	v3Min[0] = fBounds[0];
	v3Min[1] = fBounds[2];
	v3Min[2] = fBounds[4];

	v3Max[0] = fBounds[1];
	v3Min[1] = fBounds[3];
	v3Min[2] = fBounds[5];
}

/*============================================================================*/
/*  NAME      IsTouched							  		   			          */
/*============================================================================*/
bool VfaProximityBoxHandle::IsTouched(const VistaVector3D & v3Pos) const
{
	float fPos[3] = {v3Pos[0], v3Pos[1], v3Pos[2]};
	float fBounds[6];
	m_pVis->GetModel()->GetAABox(fBounds);
	return VfaWidgetTools::IsInsideBox(fPos, fBounds);
}

/*============================================================================*/
/*  NAME      Set/GetHighlightLineWidth							  		      */
/*============================================================================*/
void VfaProximityBoxHandle::SetHighlightLineWidth(float f)
{
	m_fHighlightLW = f;
}

float VfaProximityBoxHandle::GetHighlightLineWidth() const
{
	return m_fHighlightLW;
}

/*============================================================================*/
/*  NAME      Set/GetNormalLineWidth							  		      */
/*============================================================================*/
void VfaProximityBoxHandle::SeNormalLineWidth(float f)
{
	m_fNormalLW = f;
}

float VfaProximityBoxHandle::GetNormalLineWidth() const
{
	return m_fNormalLW;
}

/*============================================================================*/
/*  NAME:      Set/GetHighlightColor					  		   			  */
/*============================================================================*/
void VfaProximityBoxHandle::SetHighlightColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetHighlightColor(fC);
}
VistaColor VfaProximityBoxHandle::SetHighlightColor() const
{
	VistaColor color(m_fHighlightColor);
	return color;
}
void VfaProximityBoxHandle::SetHighlightColor(float fC[4])
{
	memcpy(m_fHighlightColor, fC, 4*sizeof(float));
}

void VfaProximityBoxHandle::GetHighlightColor(float fC[4]) const
{
	memcpy(fC, m_fHighlightColor, 4*sizeof(float));
}

/*============================================================================*/
/*  NAME:      Set/GetNormalColor					  		   				  */
/*============================================================================*/
void VfaProximityBoxHandle::SetNormalColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetNormalColor(fC);
}
VistaColor VfaProximityBoxHandle::GetNormalColor() const
{
	VistaColor color(m_fNormalColor);
	return color;
}
void VfaProximityBoxHandle::SetNormalColor(float fC[4])
{
	memcpy(m_fNormalColor, fC, 4*sizeof(float));
}

void VfaProximityBoxHandle::GetNormalColor(float fC[4]) const
{
	memcpy(fC, m_fNormalColor, 4*sizeof(float));
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "IVfaProximityBoxHandle.cpp"                                  */
/*============================================================================*/

