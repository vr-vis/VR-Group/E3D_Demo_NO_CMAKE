/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACROSSHAIRWIDGET_H
#define _VFACROSSHAIRWIDGET_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaCrosshairVis.h"
#include "VfaCrosshairController.h"
#include "VfaCrosshairModel.h"
#include "../VfaWidget.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaCrosshairController;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * integrates all the parts of a VfaCrosshair i.e. controller and 
 * visual representation and provides a common interface for application 
 * programmers.
 */
class VISTAFLOWLIBAUXAPI VfaCrosshairWidget : public IVfaWidget
{
public:
	/** 
	 * create a new widget.
	 * @param pEvMgr	The event manager to which we will listen for events.
	 * @param pGrMgr	This is needed for sensor to vis coord transformation.
	 * @param pInDev	The device to which the widget is attached.
	 * @param pRenderNode The RenderNode which is used to actually draw the icon.
	 */
	VfaCrosshairWidget(VflRenderNode *pRenderNode);

	virtual ~VfaCrosshairWidget();
	/**
	 * enable/disable widget
	 * a disabled widget will not handle events AND will not be visible!
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;

	
	/**
	 * retrieve the actual visual representation
	 */

	VfaCrosshairModel *GetModel() const;

	VfaCrosshairVis* GetView() const;

	VfaCrosshairController* GetController() const;

protected:


private:
	VfaCrosshairController *m_pWidgetCtl;
	VfaCrosshairVis *m_pVis;
	VfaCrosshairModel *m_pModel;
	bool					m_bEnabled;
	bool					m_bVisible;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFACROSSHAIRWIDGET_H
