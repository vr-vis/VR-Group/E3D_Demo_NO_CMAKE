

set( RelativeDir "./Widgets/Crosshair" )
set( RelativeSourceGroup "Source Files\\Widgets\\Crosshair" )

set( DirFiles
	VfaCrosshairController.cpp
	VfaCrosshairController.h
	VfaCrosshairModel.cpp
	VfaCrosshairModel.h
	VfaCrosshairVis.cpp
	VfaCrosshairVis.h
	VfaCrosshairWidget.cpp
	VfaCrosshairWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

