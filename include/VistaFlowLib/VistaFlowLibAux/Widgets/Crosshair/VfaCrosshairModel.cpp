/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <cstring>

#include "VfaCrosshairModel.h"

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaCrosshairVisProps                                 */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaCrosshairProps");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyArrayGet<VfaCrosshairModel, float>("CROSSHAIR_CENTER", STR_REF_TYPENAME, &VfaCrosshairModel::GetCenter),
	NULL
};
//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertyArraySet<VfaCrosshairModel, float>("CROSSHAIR_CENTER", STR_REF_TYPENAME, &VfaCrosshairModel::SetCenter),
	NULL 
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCrosshairModel::VfaCrosshairModel()
{
	m_v3Center[0] = 0.0f;
	m_v3Center[1] = 0.0f;
	m_v3Center[2] = 0.0f;

}

VfaCrosshairModel::~VfaCrosshairModel()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCrosshairModel::SetCenter(float fCenter[3])
{
	return this->SetCenter(VistaVector3D(fCenter));
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCrosshairModel::SetCenter(double dCenter[3])
{
	
	return this->SetCenter(VistaVector3D(dCenter));
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCrosshairModel::SetCenter(const VistaVector3D& v3C)
{
	if(m_v3Center == v3C)
		return false;

	this->StoreState();
	
	m_v3Center = v3C;

	//check if the new state is valid -> restore if it's not
	if(!this->CheckConstraints())
	{
		this->RestoreState();
		return false;
	}

	this->Notify(MSG_CENTER_CHANGE);
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairModel::GetCenter(float fCenter[3]) const
{
	m_v3Center.GetValues(fCenter);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairModel::GetCenter(double dCenter[3]) const
{
	dCenter[0] = m_v3Center[0];
	dCenter[1] = m_v3Center[1];
	dCenter[2] = m_v3Center[2];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairModel::GetCenter(VistaVector3D &v3C) const
{
	v3C = m_v3Center;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaCrosshairModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaCrosshairModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}

void VfaCrosshairModel::StoreState()
{
	if(!this->HasConstraints())
		return;

	//remember old state
	m_v3OldCenter = m_v3Center;
}

void VfaCrosshairModel::RestoreState()
{
	m_v3Center = m_v3OldCenter;
}
/*============================================================================*/
/*  END OF FILE "VfaCrosshairWidget.cpp"                                      */
/*============================================================================*/


