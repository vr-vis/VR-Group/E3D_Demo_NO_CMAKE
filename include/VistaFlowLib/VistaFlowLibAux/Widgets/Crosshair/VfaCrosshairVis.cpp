/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <GL/glew.h>

#include "VfaCrosshairVis.h"
#include "VfaCrosshairWidget.h"
#include "VfaCrosshairModel.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaCrosshairVis                                      */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCrosshairVis::VfaCrosshairVis()
	:	m_pModel (NULL)
{}

VfaCrosshairVis::VfaCrosshairVis(VfaCrosshairModel *pModel)
	:	m_pModel (pModel)
{}


VfaCrosshairVis::~VfaCrosshairVis()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCrosshairModel                                           */
/*                                                                            */
/*============================================================================*/
VfaCrosshairModel *VfaCrosshairVis::GetCrosshairModel() const
{
	return m_pModel;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairVis::DrawOpaque()
{
	VfaCrosshairVisProps *pProps = this->GetProperties();
	if(!pProps->GetVisible())
		return;
	//setup OpenGL to draw...
	//make modelview matrix active(so we can transform things)
	glMatrixMode(GL_MODELVIEW);
	//remember the state in which the lighting bit was before
	glPushAttrib(GL_ENABLE_BIT|GL_LINE_BIT);
	//disable lighting since we are drawing lines, which are not(per default) lighted
	glDisable(GL_LIGHTING);
	//save the current model view matrix state to the stack
	glPushMatrix();
	//translate to the current center position
	float fCenter[3];
	m_pModel->GetCenter(fCenter);
	glTranslatef(fCenter[0], fCenter[1], fCenter[2]);
	//draw lines of width 2.0f
	glLineWidth((m_bIsHighlighted ? 2.0f * pProps->GetLineWidth() : pProps->GetLineWidth()));
	//begin to draw the icon
	float fColor[3];
	glBegin(GL_LINES);
		//draw x-axis
		pProps->GetColorForAxis(0, fColor);
		glColor3fv(fColor);
		glVertex3f(-pProps->GetWingLength(), 0, 0);
		glVertex3f(pProps->GetWingLength(), 0, 0);
		//draw y-axis
		pProps->GetColorForAxis(1, fColor);
		glColor3fv(fColor);
		glVertex3f(0, -pProps->GetWingLength(), 0);
		glVertex3f(0, pProps->GetWingLength(), 0);
		//draw z-axis
		pProps->GetColorForAxis(2, fColor);
		glColor3fv(fColor);
		glVertex3f(0, 0, -pProps->GetWingLength());
		glVertex3f(0, 0, pProps->GetWingLength());
	glEnd();
	//retrieve old transformation state
	glPopMatrix();
	//retrieve old lighting state
	glPopAttrib();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaCrosshairVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetIsHighlighted                                            */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairVis::SetIsHighlighted(bool b)
{
	m_bIsHighlighted = b;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIsHighlighted                                            */
/*                                                                            */
/*============================================================================*/
bool VfaCrosshairVis::GetIsHighlighted() const
{
	return m_bIsHighlighted;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCrosshairVis::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCrosshairVis::GetBounds(VistaBoundingBox &)
{
	return false;
}
	
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaCrosshairVis::VfaCrosshairVisProps *VfaCrosshairVis::GetProperties() const
{
	return static_cast<VfaCrosshairVis::VfaCrosshairVisProps *>(IVflRenderable::GetProperties());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaCrosshairVis::CreateProperties() const
{
	return new VfaCrosshairVis::VfaCrosshairVisProps();
}



/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCrosshairVis::VfaCrosshairVisProps::VfaCrosshairVisProps()
	:	m_fLineWidth(2.0f),
		m_fWingLength(0.5f)
{
	//default color for axis is x=red y=green z=blue
	m_fColors[0][0] = 1.0f;
	m_fColors[0][1] = 0.0f;
	m_fColors[0][2] = 0.0f;
	m_fColors[1][0] = 0.0f;
	m_fColors[1][1] = 1.0f;
	m_fColors[1][2] = 0.0f;
	m_fColors[2][0] = 0.4f;
	m_fColors[2][1] = 0.4f;
	m_fColors[2][2] = 1.0f;

}
VfaCrosshairVis::VfaCrosshairVisProps::~VfaCrosshairVisProps()
{

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetWingLength                                               */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairVis::VfaCrosshairVisProps::SetWingLength(float f)
{
	m_fWingLength = f;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetWingLength                                               */
/*                                                                            */
/*============================================================================*/
float VfaCrosshairVis::VfaCrosshairVisProps::GetWingLength() const
{
	return m_fWingLength;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetColorForAxis                                         */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairVis::VfaCrosshairVisProps::SetColorForAxis(int iAxis, const VistaColor& color)
{
	float fColor[3];
	color.GetValues(fColor);

	m_fColors[iAxis][0] = fColor[0];
	m_fColors[iAxis][1] = fColor[1];
	m_fColors[iAxis][2] = fColor[2];
}
VistaColor VfaCrosshairVis::VfaCrosshairVisProps::GetColorForAxis(int iAxis) const
{
	float fColor[3];
	fColor[0] = m_fColors[iAxis][0];
	fColor[1] = m_fColors[iAxis][1];
	fColor[2] = m_fColors[iAxis][2];

	VistaColor color(fColor);
	return fColor;
}

void VfaCrosshairVis::VfaCrosshairVisProps::SetColorForAxis(int iAxis, float fColor[3])
{
	m_fColors[iAxis][0] = fColor[0];
	m_fColors[iAxis][1] = fColor[1];
	m_fColors[iAxis][2] = fColor[2];
}
void VfaCrosshairVis::VfaCrosshairVisProps::GetColorForAxis(int iAxis, float fColor[3]) const
{
	fColor[0] = m_fColors[iAxis][0];
	fColor[1] = m_fColors[iAxis][1];
	fColor[2] = m_fColors[iAxis][2];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetLineWidth                                                */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairVis::VfaCrosshairVisProps::SetLineWidth(float f)
{
	m_fLineWidth = f;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLineWidth                                                */
/*                                                                            */
/*============================================================================*/
float VfaCrosshairVis::VfaCrosshairVisProps::GetLineWidth() const
{
	return m_fLineWidth;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaCrosshairVis.cpp"                                         */
/*============================================================================*/



