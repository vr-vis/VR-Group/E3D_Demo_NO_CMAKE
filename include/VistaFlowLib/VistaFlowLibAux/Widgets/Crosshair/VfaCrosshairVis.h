/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACROSSHAIRVIS_H
#define _VFACROSSHAIRVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLibAux/Widgets/Crosshair/VfaCrosshairModel.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaVector3D;
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Simple crosshair style visualization for showing a single point which is supposed
 * to be provided by the client of this class.
 * The widget implements the IVflRenderable interface and thus depends on ViSTA FlowLib
 * rendering capabilities. It is drawn during the DrawOpaque pass of a VflVisController.
 */
class VISTAFLOWLIBAUXAPI VfaCrosshairVis : public IVflRenderable
{
public: 
	/**
	 * NOTE:	A crosshair vis is ALWAYS bound to a crosshair model whose
	 *			data it draws. It will register as an observer of this model
	 *			automatically so client classes should perform another 
	 *			registration.
	 */
	VfaCrosshairVis();
	VfaCrosshairVis(VfaCrosshairModel *pModel);
	virtual ~VfaCrosshairVis();
	
	VfaCrosshairModel *GetCrosshairModel() const;

	/**
	* Here the rendering is done
	*/
	virtual void DrawOpaque();

	/**
	* the crosshair only registers for opaque drawing since it does not have transparent components
	*/
	virtual unsigned int GetRegistrationMode() const;


	void SetIsHighlighted(bool b);
	bool GetIsHighlighted() const;

	/**
	 * redefine GetBounds in order to indicate that the widget should not be included
	 * in the computation of vis bounds(simply mark our bounds as invalid)
	 */
	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);	

	class VISTAFLOWLIBAUXAPI VfaCrosshairVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		VfaCrosshairVisProps();
		virtual ~VfaCrosshairVisProps();


		void SetWingLength(float f);
		float GetWingLength() const;


		void SetColorForAxis(int iAxis, float fColor[3]);
		void GetColorForAxis(int iAxis, float fColor[3]) const;

		void SetColorForAxis(int iAxis, const VistaColor& color);
		VistaColor GetColorForAxis(int iAxis) const;


		void SetLineWidth(float f);
		float GetLineWidth() const;

	private:
		float m_fWingLength;
		float m_fColors[3][3];
		float m_fLineWidth;

	};

	//void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	VfaCrosshairVisProps *GetProperties() const;

protected:

	virtual VflRenderableProperties* CreateProperties() const;


private:
	VfaCrosshairModel *m_pModel;
	bool m_bIsHighlighted;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _CROSSHAIRVIS_H
