/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACROSSHAIRMODEL_H
#define _VFACROSSHAIRMODEL_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetModelBase.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Simple model class
* the property class encapsulates all information which should
* be accessible via an user interface later on.
*
* DEFINED PROPS:
*
* - CROSSHAIR_CENTER
*
* 
*/
class VISTAFLOWLIBAUXAPI VfaCrosshairModel : public VfaWidgetModelBase
{
	friend class VfaCrosshairWidget;
public:
	/**
	* messages sent by this reflectionable upon property changes
	* 	MSG_CENTER_CHANGE  the crosshair's center has changed,
	*/
	enum{
		MSG_CENTER_CHANGE = IVistaReflectionable::MSG_LAST,
		MSG_LAST
	};

	VfaCrosshairModel();
	virtual ~VfaCrosshairModel();

	/**
	* set/get center position(in vis space)
	* NOTE : Provide access in various formats for convenience
	*			(VistaVector3D for ViSTA, float for GPU stuff, and double for VTK)
	*/
	bool SetCenter(float fCenter[3]);
	bool SetCenter(double dCenter[3]);
	bool SetCenter(const VistaVector3D& v3C);

	void GetCenter(float fCenter[3]) const;
	void GetCenter(double dCenter[3]) const;
	void GetCenter(VistaVector3D &v3C) const;

	virtual std::string GetReflectionableType() const;

protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	/**
	 * convenience methods for internal state management
	 * related to constraint checking
	 */
	void StoreState();
	void RestoreState();

private:
	VistaVector3D m_v3Center;
	VistaVector3D m_v3OldCenter;
}; //end of local class

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFACROSSHAIRMODEL_H
