/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACROSSHAIRCONTROLLER_H
#define _VFACROSSHAIRCONTROLLER_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetController.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaIndirectXform;
class VfaCrosshairWidget;
class VfaCrosshairVis;
class IVfaCenterControlHandle;
class VflRenderNode;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*!
 *
 */
class VISTAFLOWLIBAUXAPI VfaCrosshairController :	public VflObserver,
													public IVfaWidgetController
{
public:
	VfaCrosshairController(VfaCrosshairWidget *pWidget,
		VflRenderNode *pRenderNode);
	virtual ~VfaCrosshairController();

	
	/** 
	 * This is called on transition change into(out of) state FOCUS
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();

	/**
	 * This is called on transition change into(out of) state TOUCH
	 */
	void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	void OnUntouch();

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	void OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate(int iSlot, const bool bSet);
	void OnTimeSlotUpdate(const double dTime);
	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

	/**
	 * access to the follow flag.
	 * If the widget is in "follow mode" it will always follow the 
	 * user's inputs regardless of being grabbed. In this case 
	 * "grabs" will be ignored.
	 */
	bool GetIsFollowing() const;
	void SetIsFollowing(bool b);

	/**
	* access to the fixed offset vector used during follow mode
	*/
	bool SetFollowOffset(float fOfs[3]);
	bool SetFollowOffset(const VistaVector3D& v3Ofs);

	void GetFollowOffset(float fOfs[3]) const;
	void GetFollowOffset(VistaVector3D& v3Ofs) const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	/**
     * access to grab slot i.e. the application context command slot that has to be set in order
	 * to grab the widget.
	 */
	int GetGrabSlot() const;
	void SetGrabSlot(int i);

// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

protected:
	
private:
	VfaCrosshairWidget *m_pWidget;
	VfaCrosshairVis* m_pView;

	IVfaCenterControlHandle* m_pCenterHandle;

	bool m_bFollow;
	float m_fFollowOffset[3];

	bool m_bGrabbed;
	int m_iGrabSlot;

	VistaIndirectXform *m_pXForm;

	VistaVector3D m_v3Pos;
	VistaQuaternion m_qRot;

	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;

	bool						m_bEnabled;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaCrossHAIRCONTROLLER_H
