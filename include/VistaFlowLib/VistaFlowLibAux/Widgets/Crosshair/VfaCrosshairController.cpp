/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES				                                                  */
/*============================================================================*/
#include "VfaCrosshairController.h"
#include "VfaCrosshairWidget.h"
#include "VfaCrosshairVis.h"
#include "VfaCrosshairModel.h"
#include "../VfaWidgetTools.h"
#include "../VfaControlHandle.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaMath/VistaIndirectXform.h>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCrosshairController::VfaCrosshairController(VfaCrosshairWidget *pWidget,
												 VflRenderNode *pRenderNode) 
:	IVfaWidgetController(pRenderNode),
	m_pWidget(pWidget),
	m_bFollow(false),
	m_bGrabbed(false),
	m_iGrabSlot(0),
	m_pXForm(new VistaIndirectXform),
	m_pCenterHandle(new IVfaCenterControlHandle()),
	m_bEnabled(true)
{
	this->AddControlHandle(m_pCenterHandle);
	this->Observe(m_pWidget->GetModel());

	//set default follow position
	m_fFollowOffset[0] = 0.0f;
	m_fFollowOffset[1] = 0.0f;
	m_fFollowOffset[2] = 0.0f;

	m_pView = static_cast<VfaCrosshairVis*>(m_pWidget->GetView());
}

VfaCrosshairController::~VfaCrosshairController()
{
	delete m_pXForm;
	delete m_pCenterHandle;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pCenterHandle->SetEnable(m_bEnabled);
}
bool VfaCrosshairController::GetIsEnabled() const
{
	return m_bEnabled;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIsFollowing                                              */
/*                                                                            */
/*============================================================================*/
bool VfaCrosshairController::GetIsFollowing() const
{
	return m_bFollow;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetIsFollowing                                              */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairController::SetIsFollowing(bool b)
{
	m_bFollow = b;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFollowOffset                                         */
/*                                                                            */
/*============================================================================*/
bool VfaCrosshairController::SetFollowOffset(float fOfs[3])
{
	m_fFollowOffset[0] = fOfs[0];
	m_fFollowOffset[1] = fOfs[1];
	m_fFollowOffset[2] = fOfs[2];
	return true;
}
void VfaCrosshairController::GetFollowOffset(float fOfs[3])const
{
	fOfs[0] = m_fFollowOffset[0];
	fOfs[1] = m_fFollowOffset[1];
	fOfs[2] = m_fFollowOffset[2];
}
bool VfaCrosshairController::SetFollowOffset(const VistaVector3D& v3Ofs)
{
	float f[3];
	v3Ofs.GetValues(f);
	return this->SetFollowOffset(f);
}
void VfaCrosshairController::GetFollowOffset(VistaVector3D &v3Ofs) const
{
	float f[3];
	this->GetFollowOffset(f);
	v3Ofs[0] = f[0]; v3Ofs[1] = f[1]; v3Ofs[2] = f[2]; 
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGrabButton                                               */
/*                                                                            */
/*============================================================================*/
int VfaCrosshairController::GetGrabSlot() const
{
	return m_iGrabSlot;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetGrabButton                                               */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairController::SetGrabSlot(int i)
{
	m_iGrabSlot = i;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnFocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairController::OnFocus(IVfaControlHandle* pHandle)
{
	m_pView->SetIsHighlighted(true);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUnfocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairController::OnUnfocus()
{
	m_pView->SetIsHighlighted(false);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUntouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairController::OnUntouch()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairController::OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	if(iSlot == VfaApplicationContextObject::SLOT_POINTER_VIS)
	{
		m_oLastSensorFrame = oFrameData;
		if(m_bFollow || m_bGrabbed)
		{
			//move the crosshair to the appropriate position
			m_pXForm->Update(oFrameData.v3Position,oFrameData.qOrientation,m_v3Pos,m_qRot);

			if(m_bFollow)
			{
				VistaVector3D v3Ofs;
				this->GetFollowOffset(v3Ofs);
				m_pWidget->GetModel()->SetCenter(m_v3Pos+v3Ofs);
			}
			else
			{
				m_pWidget->GetModel()->SetCenter(m_v3Pos);
			}

		}
	}
	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	if(m_iGrabSlot != iSlot)
		return;


	if(bSet)
	{
		// we can only be grabbed, if we have the focus
		if(this->GetControllerState() == CS_FOCUS)
		{
			m_qRot[0] = 0; m_qRot[1] = 0; m_qRot[2] = 0; m_qRot[3] = 1;
			m_pWidget->GetModel()->GetCenter(m_v3Pos);
			m_pXForm->Init(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation, m_v3Pos, m_qRot);
			m_bGrabbed = true;
		}
	}
	else
	{
		m_bGrabbed = false;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*                                                                            */
/*============================================================================*/

void VfaCrosshairController::OnTimeSlotUpdate(const double dTime)
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSensorMask			                                      */
/*                                                                            */
/*============================================================================*/
int VfaCrosshairController::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_POINTER_VIS);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCommandMask			                                  */
/*                                                                            */
/*============================================================================*/
int VfaCrosshairController::GetCommandMask() const
{
	return(1 << m_iGrabSlot);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeUpdate			                                      */
/*                                                                            */
/*============================================================================*/

bool VfaCrosshairController::GetTimeUpdate() const
{
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate		                                      */
/*                                                                            */
/*============================================================================*/

void VfaCrosshairController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaCrosshairModel *pModel = dynamic_cast<VfaCrosshairModel*>(pObserveable);

	if(!pModel)
		return;

	VistaVector3D v3Center;
	pModel->GetCenter(v3Center);
	m_pCenterHandle->SetCenter(v3Center);
}

/*============================================================================*/
/*  END OF FILE "VfaCrosshairController.cpp"                                  */
/*============================================================================*/


