/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef __VFATEXTUREDQUADHANDLE_H
#define __VFATEXTUREDQUADHANDLE_H

// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaControlHandle.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <string>

using namespace std;


// ========================================================================== //
// === Forward Declarations
// ========================================================================== //
class VflRenderNode;
class VfaQuadVis;

class VistaVector3D;
class VistaTexture;


// ========================================================================== //
// === Class Definition
// ========================================================================== //
class VISTAFLOWLIBAUXAPI VfaTexturedQuadHandle : public IVfaCenterControlHandle
{
public:
	// ---------------------------------------------------------------------- //
	// --- Con-/Destructor
	// ---------------------------------------------------------------------- //
	//!
	VfaTexturedQuadHandle(VflRenderNode *pRenderNode);
	//!
	virtual ~VfaTexturedQuadHandle();


	// ---------------------------------------------------------------------- //
	// --- Public Interface
	// ---------------------------------------------------------------------- //
	//!
	void SetIsVisible(bool bIsVisible);
	//!
	bool GetIsVisible() const;

	//!
	void SetSize(float fWidth, float fHeight);
	//!
	void GetSize(float &fWidth, float &fHeight) const;

	//!
	void SetCenter(const VistaVector3D &v3Center);
	//!
	void GetCenter(VistaVector3D &v3Center) const;

	//!
	void SetNormal(const VistaVector3D &v3Normal);
	//!
	void GetNormal(VistaVector3D &v3Normal) const;

	//!
	void SetNormalColor(float aRGB[3]);
	//!
	void GetNormalColor(float aRGB[3]) const;

	//!
	void SetHighlightedColor(float aRGB[3]);
	//!
	void GetHighlightedColor(float aRGB[3]) const;

	//!
	void SetIsHighlighted(bool bIsHighlighted);
	//!
	bool GetIsHighlighted() const;

	//!
	void SetTexture(VistaTexture *pTex);

	//!
	VfaQuadVis* GetQuadVis() const;


protected:

private:
	// ---------------------------------------------------------------------- //
	// --- Variables
	// ---------------------------------------------------------------------- //
	//!
	float				m_aNormalColor[4],
						m_aHighlightColor[4];
	//!
	VfaQuadVis			*m_pQuadVis;
};

#endif // __VFATEXTUREDBOXHANDLE_H


// ========================================================================== //
// === End of File
// ========================================================================== //
