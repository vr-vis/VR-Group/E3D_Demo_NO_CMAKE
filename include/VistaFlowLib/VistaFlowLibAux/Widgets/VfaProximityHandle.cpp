/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaProximityHandle.h"
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
IVfaProximityHandle::IVfaProximityHandle(int iType)
	:	IVfaControlHandle(iType),
		m_bVisEnabled(true),
		m_fProximityDistance(0.2f)
{
}

IVfaProximityHandle::~IVfaProximityHandle()
{
}

/*============================================================================*/
/*  IMPLEMENTATION      IVfaProximityHandle  		   					      */
/*============================================================================*/
void IVfaProximityHandle::SetVisEnabled(bool b)
{
	m_bVisEnabled = b;
}

bool IVfaProximityHandle::GetVisEnabled() const
{
	return m_bVisEnabled;
}

void IVfaProximityHandle::SetProximityDistance(float f)
{
	m_fProximityDistance = f;
}

float IVfaProximityHandle::GetProximityDistance() const
{
	return m_fProximityDistance;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "IVfaProximityHandle.cpp"                                     */
/*============================================================================*/

