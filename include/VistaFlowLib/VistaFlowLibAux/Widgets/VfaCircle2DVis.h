/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VfaCircle2DVis_H
#define _VfaCircle2DVis_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VistaTexture;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*	visualization of a 2D circle
*/
class VISTAFLOWLIBAUXAPI VfaCircle2DVis : public IVflRenderable
{
public: 
	VfaCircle2DVis();
	virtual ~VfaCircle2DVis();

	/**
	* here the rendering is done
	*/
	virtual void DrawOpaque();


	virtual unsigned int GetRegistrationMode() const;

	bool SetCenter(const VistaVector3D& v3Center);
	bool SetCenter(float fCenter[3]);
	void GetCenter(float fCenter[3]) const;
	VistaVector3D GetCenter() const;

	bool SetRadius(float f);
	float GetRadius() const;
	
	void SetRotate(const VistaVector3D &v3Normal);
	void SetRotate(float angle, float x, float y, float z);

	void GetRotate(float fRotate[4]) const;



	/**
	 * Provide a texture for rendering on the circle.
	 */
	void SetTexture(VistaTexture *pTex);
	VistaTexture *GetTexture() const;

	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);
	
	class VISTAFLOWLIBAUXAPI VfaCircle2DVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		enum{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_LAST
		};

		VfaCircle2DVisProps();
		virtual ~VfaCircle2DVisProps();

		bool SetLineColor(float fColor[3]);
		bool SetLineColor(const VistaColor& color);
		void GetLineColor(float fColor[3]) const;
		VistaColor GetLineColor() const;

		bool SetLineWidth(float f);
		float GetLineWidth() const;

		bool SetIsSurrounded(bool b);
		bool GetIsSurrounded() const;

	protected:

	private:
		float m_fColor[3];
		float m_fLineWidth;
		bool m_bSurrounding;
	};

	VfaCircle2DVisProps *GetProperties() const;
	
protected:
	virtual VflRenderableProperties* CreateProperties() const;

private:
	float m_fCenter[3];
	float m_fCircleRadius;
	float m_fRotate[4];

	VistaTexture *m_pTexture;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaCircle2DVis_H
