/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES														              */
/*============================================================================*/
#include "VfaProximityFocusStrategy.h"
#include "VfaProximityHandle.h"
#include "VfaWidgetController.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <algorithm>

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaProximityFocusStrategy::VfaProximityFocusStrategy()
:	m_fProximityDistance(0.2f),
	m_pActiveHandle(NULL)
{

}

VfaProximityFocusStrategy::~VfaProximityFocusStrategy()
{

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME    : RegisterSelectable                                              */
/*                                                                            */
/*============================================================================*/
void VfaProximityFocusStrategy::RegisterSelectable(
	IVfaWidgetController* pSelectable)
{
	// register all handles of widget controller
	const std::vector<IVfaControlHandle*>& vecHandles =
		pSelectable->GetControlHandles();

	std::vector<IVfaControlHandle*>::const_iterator itCurrent =
		vecHandles.begin();
	std::vector<IVfaControlHandle*>::const_iterator itEnd =
		vecHandles.end();

	IVfaProximityHandle *pCasted = NULL;

	for(; itCurrent!=itEnd; ++itCurrent)
	{
		pCasted = dynamic_cast<IVfaProximityHandle*>(*itCurrent);
		if(pCasted)
		{
			m_vecHandles.push_back(pCasted);
			m_mapHandle2RenderNode[pCasted] = pSelectable->GetRenderNode();
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME    : UnregisterSelectable                                            */
/*                                                                            */
/*============================================================================*/
void VfaProximityFocusStrategy::UnregisterSelectable(IVfaWidgetController* pSelectable)
{
	// unregister all handles of widget controller
	const std::vector<IVfaControlHandle*>& vecHandles = pSelectable->GetControlHandles();

	std::vector<IVfaControlHandle*>::const_iterator itCurrent = vecHandles.begin();
	std::vector<IVfaControlHandle*>::const_iterator itEnd = vecHandles.end();
	std::vector<IVfaProximityHandle*>::iterator itFind;

	IVfaProximityHandle *pCasted;
	for(; itCurrent!=itEnd; ++itCurrent)
	{
		pCasted = dynamic_cast<IVfaProximityHandle*>(*itCurrent);
		if(!pCasted)
			continue;

		itFind = find(m_vecHandles.begin(), m_vecHandles.end(), pCasted);
		
		if(itFind != m_vecHandles.end())
			m_vecHandles.erase(itFind);

		m_mapHandle2RenderNode.erase(pCasted);
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME    : EvaluateFocus                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaProximityFocusStrategy::EvaluateFocus(
							std::vector<IVfaControlHandle*> &vecFocusHandles)
{
	//just check if we got an active handle and if so provide it
	vecFocusHandles.clear();

	if(m_pActiveHandle != NULL)
		vecFocusHandles.push_back(m_pActiveHandle);

	return !vecFocusHandles.empty();
}
/*============================================================================*/
/*                                                                            */
/*  NAME    : OnSensorSlotUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaProximityFocusStrategy::OnSensorSlotUpdate(int iSlot, 
			const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	//we are only interested in the cursor here!
	if(iSlot != VfaApplicationContextObject::SLOT_CURSOR_WORLD)
		return;
	//check if any of the registered handles is "touched"
	//per se we assume nothing's touched.
	m_pActiveHandle = NULL;

	VistaTransformMatrix	mInvTrans;
	VistaVector3D			v3LocalPos;

	for(unsigned int i=0; i<m_vecHandles.size(); ++i)
	{
		// Transform the global cursor position into the handle's local frame.
		m_mapHandle2RenderNode[m_vecHandles[i]]->GetInverseTransform(mInvTrans);
		v3LocalPos = mInvTrans.Transform(oFrameData.v3Position);
		
		//So this is the FCFS thing here -> the first handle that is touched
		//will be returned and nothing else!
		if(m_vecHandles[i]->IsTouched(v3LocalPos))
		{
			m_pActiveHandle = m_vecHandles[i];
			return;
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME    : OnSensorSlotUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaProximityFocusStrategy::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	return;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    : OnSensorSlotUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaProximityFocusStrategy::OnTimeSlotUpdate(const double dTime)
{
	return;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    : OnSensorSlotUpdate                                              */
/*                                                                            */
/*============================================================================*/
int VfaProximityFocusStrategy::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_CURSOR_WORLD);
}
/*============================================================================*/
/*                                                                            */
/*  NAME    : OnSensorSlotUpdate                                              */
/*                                                                            */
/*============================================================================*/
int VfaProximityFocusStrategy::GetCommandMask() const
{
	return 0;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    : OnSensorSlotUpdate                                              */
/*                                                                            */
/*============================================================================*/
bool VfaProximityFocusStrategy::GetTimeUpdate() const
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    : Get/SetGlobalProximityDistance                                  */
/*                                                                            */
/*============================================================================*/
void VfaProximityFocusStrategy::SetGlobalProximityDistance(float fD)
{
	m_fProximityDistance = fD;
	for(unsigned int i=0; i<m_vecHandles.size(); ++i)
		m_vecHandles[i]->SetProximityDistance(fD);
}

float VfaProximityFocusStrategy::GetGlobalProximityDistance() const
{
	return m_fProximityDistance;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VfaProximityFocusStrategy.cpp"                          */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/
