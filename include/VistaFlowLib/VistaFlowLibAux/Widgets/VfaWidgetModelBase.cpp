/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaWidgetModelBase.h"
#include "VfaWidgetConstraints.h"

#include <algorithm>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaWidgetModelBase::VfaWidgetModelBase()
{
}

VfaWidgetModelBase::~VfaWidgetModelBase()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetConstraint                                               */
/*                                                                            */
/*============================================================================*/
bool VfaWidgetModelBase::AddConstraint(IVfaWidgetConstraint *pConstr)
{
	std::list<IVfaWidgetConstraint*>::iterator itConstr = 
		std::find(m_liConstraints.begin(), m_liConstraints.end(), pConstr);
	
	if(itConstr != m_liConstraints.end())
		return false;

	m_liConstraints.push_back(pConstr);
	
	this->Notify(MSG_CONSTRAINT_CHANGED);
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemoveConstraint                                            */
/*                                                                            */
/*============================================================================*/
void VfaWidgetModelBase::RemoveConstraint(IVfaWidgetConstraint *pConstr)
{
	std::list<IVfaWidgetConstraint*>::iterator itConstr = 
		std::find(m_liConstraints.begin(), m_liConstraints.end(), pConstr);
	
	if(itConstr != m_liConstraints.end())
		m_liConstraints.erase(itConstr);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   HasConstraints                                              */
/*                                                                            */
/*============================================================================*/
bool VfaWidgetModelBase::HasConstraints() const
{
	return !m_liConstraints.empty();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumConstraints                                           */
/*                                                                            */
/*============================================================================*/
int VfaWidgetModelBase::GetNumConstraints() const
{
	return static_cast<int>(m_liConstraints.size());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetConstraint                                               */
/*                                                                            */
/*============================================================================*/
IVfaWidgetConstraint *VfaWidgetModelBase::GetConstraint(unsigned int i) const
{
	std::list<IVfaWidgetConstraint*>::const_iterator itCurrent = 
													m_liConstraints.begin();
	for(unsigned int k=0; k<i; ++k)
		++itCurrent;

	return *itCurrent;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CheckConstraints                                            */
/*                                                                            */
/*============================================================================*/
bool VfaWidgetModelBase::CheckConstraints() const
{
	if(m_liConstraints.empty())
		return true;

	std::list<IVfaWidgetConstraint*>::const_iterator itConstr = 
		m_liConstraints.begin();
	std::list<IVfaWidgetConstraint*>::const_iterator itEnd =
		m_liConstraints.end();

	for(; itConstr != itEnd; ++itConstr)
	{
		if(!(*itConstr)->GetIsActive())
			continue;
		if(!(*itConstr)->CheckConstraint(this))
			return false;
	}
	return true;
}
/*============================================================================*/
/*  END OF FILE "VfaWidgetModelBase.cpp"                                      */
/*============================================================================*/


