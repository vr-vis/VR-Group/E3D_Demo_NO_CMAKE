/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFABOXHANDLE_H
#define _VFABOXHANDLE_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VfaControlHandle.h"
#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VflRenderNode;
class VistaVector3D;
class VistaQuaternion;
class VfaBoxVis;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Box visualization which can be highlighted and tell in this way
 * that it can be grabbed and further moved.
 */
class VISTAFLOWLIBAUXAPI VfaBoxHandle : public IVfaCenterControlHandle
{
public: 
	VfaBoxHandle(VflRenderNode *pRenderNode);
	virtual ~VfaBoxHandle();

	void SetVisible(bool b);
	bool GetVisible() const;

	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);

	void SetSize(float w, float h, float d);
	bool GetSize(float &w, float &h, float &d) const;

	// derived from IVfaCenterControlHandle
	void SetCenter(const VistaVector3D &v3Translation);
	void SetCenter(float fC[3]);
	void SetCenter(double dC[3]);

	void GetCenter(VistaVector3D &v3Translation);
	void GetCenter(float fC[3]);
	void GetCenter(double dC[3]);

	void SetRotation(const VistaQuaternion &qRotation);
	void GetRotation(VistaQuaternion &qRotation) const;

	void SetScale(float fScale);
	float GetScale() const;


	void SetNormalColor(const VistaColor& color);
	VistaColor GetNormalColor() const;
	void SetNormalColor(float fColor[4]);
	void GetNormalColor(float fColor[4]) const;

	void SetHighlightColor(const VistaColor& color);
	VistaColor GetHighlightColor() const;
	void SetHighlightColor(float fColor[4]);
	void GetHighlightColor(float fColor[4]) const;

	void SetIsHighlighted(bool b);
	bool GetIsHighlighted() const;

	VfaBoxVis* GetBoxVis() const;

protected:
	
private:
	float m_fWidth;
	float m_fHeight;
	float m_fDepth;

	float m_fScale;

	float m_fNormalColor[4];
	float m_fHighlightColor[4];

	VfaBoxVis* m_pBoxVis;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaBoxHandle_H
