/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaBoxHandle.h"
#include "VfaBoxVis.h"
#include "Box/VfaBoxModel.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  IMPLEMENTATION      VfaBoxHandle                                         */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaBoxHandle::VfaBoxHandle(VflRenderNode *pRenderNode)
:	IVfaCenterControlHandle(),
	m_pBoxVis(new VfaBoxVis(new VfaBoxModel)),
	m_fWidth(0.05f),
	m_fHeight(0.05f),
	m_fDepth(0.05f),
	m_fScale(1.0f)
{
	//normal color
	m_fNormalColor[0] = 1.0f;
	m_fNormalColor[1] = 0.0f;
	m_fNormalColor[2] = 0.0f;
	m_fNormalColor[3] = 1.0f;
	
	//color if sphere highlighted
	m_fHighlightColor[0] = 1.0f;
	m_fHighlightColor[1] = 0.75f;
	m_fHighlightColor[2] = 0.75f;
	m_fHighlightColor[3] = 1.0f;

	if(m_pBoxVis->Init())
		pRenderNode->AddRenderable(m_pBoxVis);

	m_pBoxVis->GetProperties()->SetDrawFilledFaces(true);
	m_pBoxVis->GetProperties()->SetFaceColor(m_fNormalColor);
	m_pBoxVis->GetProperties()->SetLineColor(m_fNormalColor);
	m_pBoxVis->GetProperties()->SetDrawWithLighting(true);
}

VfaBoxHandle::~VfaBoxHandle()
{
	m_pBoxVis->GetRenderNode()->RemoveRenderable(m_pBoxVis);
	delete m_pBoxVis->GetModel();
	delete m_pBoxVis;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBoxVis                                                   */
/*                                                                            */
/*============================================================================*/
VfaBoxVis* VfaBoxHandle::GetBoxVis() const
{
	return m_pBoxVis;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaBoxHandle::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaBoxHandle::GetBounds(VistaBoundingBox &)
{
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetVisible                                              */
/*                                                                            */
/*============================================================================*/
void VfaBoxHandle::SetVisible(bool b)
{
	m_pBoxVis->SetVisible(b);
}

bool VfaBoxHandle::GetVisible() const
{
	return m_pBoxVis->GetVisible();
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetCenter			                                      */
/*                                                                            */
/*============================================================================*/
void VfaBoxHandle::SetCenter(const VistaVector3D &v3Translation)
{
	IVfaCenterControlHandle::SetCenter (v3Translation);
	m_pBoxVis->GetModel()->SetTranslation(v3Translation);
}
void VfaBoxHandle::SetCenter(float fC[3])
{
	IVfaCenterControlHandle::SetCenter(VistaVector3D(fC));
	m_pBoxVis->GetModel()->SetTranslation(fC);
}
void VfaBoxHandle::SetCenter(double dC[3])
{
	IVfaCenterControlHandle::SetCenter (VistaVector3D(dC));
	m_pBoxVis->GetModel()->SetTranslation(dC);
}

void VfaBoxHandle::GetCenter(VistaVector3D &v3Translation)
{
	IVfaCenterControlHandle::GetCenter (v3Translation);
}
void VfaBoxHandle::GetCenter(float fC[3])
{
	VistaVector3D v3;
	IVfaCenterControlHandle::GetCenter(v3);
	v3.GetValues(fC);
}
void VfaBoxHandle::GetCenter(double dC[3])
{
	VistaVector3D v3;
	IVfaCenterControlHandle::GetCenter(v3);
	v3.GetValues(dC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRotation                                             */
/*                                                                            */
/*============================================================================*/
void VfaBoxHandle::SetRotation(const VistaQuaternion &qRotation)
{
	m_pBoxVis->GetModel()->SetRotation(qRotation);
}
void VfaBoxHandle::GetRotation(VistaQuaternion &qRot) const
{
	m_pBoxVis->GetModel()->GetRotation(qRot);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetScale                                                */
/*                                                                            */
/*============================================================================*/
void VfaBoxHandle::SetScale(float fScale)
{
	float fExtents[3];
	m_pBoxVis->GetModel()->GetExtents (fExtents);
	for (register int i=0; i < 3; ++i)
		fExtents[i] = fScale*fExtents[i]/m_fScale;

	m_pBoxVis->GetModel()->SetExtents(fExtents);

	m_fScale = fScale;

}

float VfaBoxHandle::GetScale() const
{
	return m_fScale;
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetSize		                                              */
/*                                                                            */
/*============================================================================*/
void VfaBoxHandle::SetSize(float w, float h, float d)
{
	m_pBoxVis->GetModel()->SetExtents(w, h, d);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSize	                                                  */
/*                                                                            */
/*============================================================================*/
bool VfaBoxHandle::GetSize(float &w, float &h, float &d) const
{
	m_pBoxVis->GetModel()->GetExtents(w, h, d);
	return true;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNormalColor                                          */
/*                                                                            */
/*============================================================================*/
void VfaBoxHandle::SetNormalColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetNormalColor(fC);
}
VistaColor VfaBoxHandle::GetNormalColor() const
{
	VistaColor color(m_fNormalColor);
	return color;
}
void VfaBoxHandle::SetNormalColor(float fNormalColor[4])
{
	m_fNormalColor[0] = fNormalColor[0];
	m_fNormalColor[1] = fNormalColor[1];
	m_fNormalColor[2] = fNormalColor[2];
	m_fNormalColor[3] = fNormalColor[3];
}
void VfaBoxHandle::GetNormalColor(float fNormalColor[4]) const
{
	fNormalColor[0] = m_fNormalColor[0];
	fNormalColor[1] = m_fNormalColor[1];
	fNormalColor[2] = m_fNormalColor[2];
	fNormalColor[3] = m_fNormalColor[3];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHighlightColor                                       */
/*                                                                            */
/*============================================================================*/
void VfaBoxHandle::SetHighlightColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetHighlightColor(fC);
}
VistaColor VfaBoxHandle::GetHighlightColor() const
{
	VistaColor color(m_fHighlightColor);
	return color;
}
void VfaBoxHandle::SetHighlightColor(float fHighlightColor[4])
{
	m_fHighlightColor[0] = fHighlightColor[0];
	m_fHighlightColor[1] = fHighlightColor[1];
	m_fHighlightColor[2] = fHighlightColor[2];
	m_fHighlightColor[3] = fHighlightColor[3];
}
void VfaBoxHandle::GetHighlightColor(float fHighlightColor[4]) const
{
	fHighlightColor[0] = m_fHighlightColor[0];
	fHighlightColor[1] = m_fHighlightColor[1];
	fHighlightColor[2] = m_fHighlightColor[2];
	fHighlightColor[3] = m_fHighlightColor[3];
}

/*============================================================================*/
/*  NAME      :   Get/SetIsHighlighted		                                  */
/*============================================================================*/
void VfaBoxHandle::SetIsHighlighted(bool b)
{
	IVfaCenterControlHandle::SetIsHighlighted(b);
	
	
	if(b)
	{
		m_pBoxVis->GetProperties()->SetFaceColor(m_fHighlightColor);
		m_pBoxVis->GetProperties()->SetLineColor(m_fHighlightColor);
	}
	else
	{
		m_pBoxVis->GetProperties()->SetFaceColor(m_fNormalColor);
		m_pBoxVis->GetProperties()->SetLineColor(m_fNormalColor);
	}
}

bool VfaBoxHandle::GetIsHighlighted() const
{
	return IVfaCenterControlHandle::GetIsHighlighted();
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaBoxHandle.cpp"								              */
/*============================================================================*/



