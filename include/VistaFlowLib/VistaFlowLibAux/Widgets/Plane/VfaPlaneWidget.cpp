/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VfaPlaneWidget.h"

#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaPlaneWidget::VfaPlaneWidget(VflRenderNode *pRenderNode)
:	IVfaWidget(pRenderNode)
, 	m_pPlaneModel(new VfaPlaneModel)
,	m_pPlaneView(NULL)
,	m_pPlaneController(NULL)
, 	m_bEnabled(true)
,	m_bVisible(true)
{
	m_pPlaneView = new VfaQuadVis(m_pPlaneModel);
	m_pPlaneView->Init();
	pRenderNode->AddRenderable(m_pPlaneView);

	m_pPlaneController = new VfaPlaneController(m_pPlaneModel, pRenderNode);
	m_pPlaneController->Observe(m_pPlaneModel);
}

VfaPlaneWidget::~VfaPlaneWidget()
{
	if(m_pPlaneView)
		GetRenderNode()->RemoveRenderable(m_pPlaneView);
	
	delete m_pPlaneController;
	delete m_pPlaneView;
	delete m_pPlaneModel;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaPlaneWidget::SetIsEnabled(bool bIsEnabled)
{
	m_bEnabled = bIsEnabled;
	m_pPlaneController->SetIsEnabled(m_bEnabled);
}

bool VfaPlaneWidget::GetIsEnabled() const
{
	return m_bEnabled;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsVisible                                            */
/*                                                                            */
/*============================================================================*/
void VfaPlaneWidget::SetIsVisible(bool bIsVisible)
{
	m_bVisible = bIsVisible;
	m_pPlaneView->SetVisible(m_bVisible);
	m_pPlaneController->SetArrowVisible(bIsVisible);	
}

bool VfaPlaneWidget::GetIsVisible() const
{
	return m_bVisible;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel                                                    */
/*                                                                            */
/*============================================================================*/
VfaPlaneModel* VfaPlaneWidget::GetModel() const
{
	return m_pPlaneModel;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetView                                                     */
/*                                                                            */
/*============================================================================*/
VfaQuadVis* VfaPlaneWidget::GetView() const
{
	return m_pPlaneView;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetController                                               */
/*                                                                            */
/*============================================================================*/
VfaPlaneController* VfaPlaneWidget::GetController() const
{
	return m_pPlaneController;
}


/*============================================================================*/
/*  END OF FILE																  */
/*============================================================================*/
