/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaPlaneConstraints.h"
#include "VfaPlaneModel.h"
#include "../VfaWidgetTools.h"

#include <cassert>
#include <cstring>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


//=============================================================================
//*****************************************************************************
//                      VfaRestrictPlaneToAABounds
//*****************************************************************************
//=============================================================================


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaRestrictPlaneToAABounds::VfaRestrictPlaneToAABounds()
{
}

VfaRestrictPlaneToAABounds::~VfaRestrictPlaneToAABounds()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :    CheckConstraint                                            */
/*                                                                            */
/*============================================================================*/
bool VfaRestrictPlaneToAABounds::CheckConstraint(const VfaWidgetModelBase*const pModel)
{
	//make sure we are always working on a valid model type!
	assert(dynamic_cast<const VfaPlaneModel*const>(pModel) != NULL);
	const VfaPlaneModel *const pPlane = static_cast<const VfaPlaneModel*const>(pModel);

	float fPlaneCenter[3];
	pPlane->GetTranslation(fPlaneCenter);

	return VfaWidgetTools::IsInsideBox(fPlaneCenter, m_fBounds);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :    Set/Get Bounds                                             */
/*                                                                            */
/*============================================================================*/
void VfaRestrictPlaneToAABounds::SetBounds(float fBounds[6])
{
	memcpy(m_fBounds, fBounds, 6*sizeof(float));
}

void VfaRestrictPlaneToAABounds::GetBounds(float fBounds[6]) const
{
	memcpy(fBounds, m_fBounds, 6*sizeof(float));
}

//=============================================================================
//*****************************************************************************
//                      CRestrictPlaneToAxisAlignedPlane
//*****************************************************************************
//=============================================================================

VfaRestrictPlaneToAxisAlignedPlane::VfaRestrictPlaneToAxisAlignedPlane()
: m_iPlaneMode(XY_PLANE)
{
}

VfaRestrictPlaneToAxisAlignedPlane::~VfaRestrictPlaneToAxisAlignedPlane()
{
}

bool VfaRestrictPlaneToAxisAlignedPlane::CheckConstraint(const VfaWidgetModelBase*const pModel)
{
	//make sure we are always working on a valid model type!
	assert(dynamic_cast<const VfaPlaneModel*const>(pModel) != NULL);
	const VfaPlaneModel *const pPlane = static_cast<const VfaPlaneModel*const>(pModel);

	//check if the quaternion is still in NULL position
	VistaQuaternion qXY_Neutral(0.0f, 0.0f, 0.0f, 1.0f);
	VistaQuaternion qYZ_Neutral( 
		VistaAxisAndAngle(VistaVector3D(0.0f, 1.0f, 0.0f), 0.5f*Vista::Pi));
	VistaQuaternion qXZ_Neutral( 
		VistaAxisAndAngle(VistaVector3D(1.0f, 0.0f, 0.0f), 0.5f*Vista::Pi));

	VistaQuaternion qRot;
	pPlane->GetRotation(qRot);

	if(m_iPlaneMode == XY_PLANE)
		return qRot == qXY_Neutral;

	if(m_iPlaneMode == YZ_PLANE)
		return qRot == qYZ_Neutral;

	if(m_iPlaneMode == XZ_PLANE)
		return qRot == qXZ_Neutral;

	return false;
}

void VfaRestrictPlaneToAxisAlignedPlane::SetPlaneMode(int iMode)
{
	m_iPlaneMode = iMode;
}

int VfaRestrictPlaneToAxisAlignedPlane::GetPlaneMode() const
{
	return m_iPlaneMode;
}

//VfaPlaneConstraints::VfaPlaneConstraints(VfaPlaneController::VfaPlaneControllerProps *pProps)
//: m_pCtlProps(pProps),
//  m_v3LastMin(VistaVector3D(-0.5f, -0.5f, 0.0f)),
//  m_v3LastMax(VistaVector3D(0.5f, 0.5f, 0.0f))
//{
//
//}

///*============================================================================*/
///*                                                                            */
///*  NAME      :   CheckFixAspectRatio                                         */
///*                                                                            */
///*============================================================================*/
//void VfaPlaneConstraints::CheckFixAspectRatio(VistaVector3D &v3Min, 
//											 VistaVector3D &v3Max)
//{
//	if( m_pCtlProps->GetFixAspectRatio() )
//	{
//		VistaVector3D v3OldSize = m_v3LastMax - m_v3LastMin;
//
//		VistaVector3D v3NewSize = v3Max - v3Min;
//
//		float fRatioX = v3NewSize[0] / v3OldSize[0];
//		float fRatioY = v3NewSize[1] / v3OldSize[1];
//		float fRatioZ = v3NewSize[2] / v3OldSize[2];
//
//		float fRatio = max<float>(fRatioX, max<float>(fRatioY, fRatioZ));
//
//		if(m_v3LastMin[0] == v3Min[0])
//		{
//			v3Max[0] = v3Min[0] + fRatio * v3OldSize[0];
//		}
//		if(m_v3LastMin[1] == v3Min[1])
//		{
//			v3Max[1] = v3Min[1] + fRatio * v3OldSize[1];
//		}
//		if(m_v3LastMin[2] == v3Min[2])
//		{
//			v3Max[2] = v3Min[2] + fRatio * v3OldSize[2];
//		}
//
//		if(m_v3LastMax[0] == v3Max[0])
//		{
//			v3Min[0] = v3Max[0] - fRatio * v3OldSize[0];
//		}
//		if(m_v3LastMax[1] == v3Max[1])
//		{	
//			v3Min[1] = v3Max[1] - fRatio * v3OldSize[1];
//		}
//		if(m_v3LastMax[2] == v3Max[2])
//		{
//			v3Min[2] = v3Max[2] - fRatio * v3OldSize[2];
//		}
//	}
//}
//
///*============================================================================*/
///*                                                                            */
///*  NAME      :   CheckPenetration                                            */
///*                                                                            */
///*============================================================================*/
//void VfaPlaneConstraints::CheckPenetration(VistaVector3D &v3Min, VistaVector3D &v3Max)
//{
//	if( m_pCtlProps->GetAllowPenetration() )
//	{
//		VistaVector3D v3Temp;
//
//		v3Temp[0] = std::min<float>(v3Min[0], v3Max[0]);
//		v3Temp[1] = std::min<float>(v3Min[1], v3Max[1]);
//		v3Temp[2] = std::min<float>(v3Min[2], v3Max[2]);
//
//		v3Max[0] = std::max<float>(v3Min[0], v3Max[0]);
//		v3Max[1] = std::max<float>(v3Min[1], v3Max[1]);
//		v3Max[2] = std::max<float>(v3Min[2], v3Max[2]);
//
//		v3Min = v3Temp;
//	}
//	else if(v3Min != m_v3LastMin)
//	{
//		if(v3Min[0] > v3Max[0])
//			v3Min[0] = v3Max[0];
//		if(v3Min[1] > v3Max[1])
//			v3Min[1] = v3Max[1];
//		if(v3Min[2] > v3Max[2])
//			v3Min[2] = v3Max[2];
//	}
//	else if(v3Max != m_v3LastMax)
//	{
//		if(v3Max[0] < v3Min[0])
//			v3Max[0] = v3Min[0];
//		if(v3Max[1] < v3Min[1])
//			v3Max[1] = v3Min[1];
//		if(v3Max[2] < v3Min[2])
//			v3Max[2] = v3Min[2];
//	}
//}
//
///*============================================================================*/
///*                                                                            */
///*  NAME      :   EvaluateFixpoint                                            */
///*                                                                            */
///*============================================================================*/
//void VfaPlaneConstraints::EvaluateFixpoint(VistaVector3D &v3Min, VistaVector3D &v3Max)
//{
//	if(m_pCtlProps->GetFixpoint() == VfaPlaneController::VfaPlaneControllerProps::FIXPOINT_CORNER)
//		return;
//
//	VistaVector3D v3Center =(( m_v3LastMax - m_v3LastMin ) / 2 ) + m_v3LastMin;
//
//	//if(m_v3LastMin == v3Min)
//	//{
//	//	v3Min = v3Center - v3Max - v3Center;
//	//}
//	//else if(m_v3LastMax == v3Max)
//	//{
//	//	v3Max = v3Center + v3Center - v3Min;
//	//}
//
//	VistaVector3D v3TempMin(v3Min);
//
//	if(m_v3LastMin[0] == v3Min[0])
//	{
//		v3TempMin[0] = v3Center[0] -( v3Max[0] - v3Center[0] );
//	}
//	if(m_v3LastMin[1] == v3Min[1])
//	{
//		v3TempMin[1] = v3Center[1] -( v3Max[1] - v3Center[1] );
//	}
//	if(m_v3LastMin[2] == v3Min[2])
//	{
//		v3TempMin[2] = v3Center[2] -( v3Max[2] - v3Center[2] );
//	}
//
//	VistaVector3D v3TempMax(v3Max);
//
//	if(m_v3LastMax[0] == v3Max[0])
//	{
//		v3TempMax[0] = v3Center[0] +( v3Center[0] - v3Min[0] );
//	}
//	if(m_v3LastMax[1] == v3Max[1])
//	{
//		v3TempMax[1] = v3Center[1] +( v3Center[1] - v3Min[1] );
//	}
//	if(m_v3LastMax[2] == v3Max[2])
//	{
//		v3TempMax[2] = v3Center[2] +( v3Center[2] - v3Min[2] );
//	}
//
//	v3Min = v3TempMin;
//	v3Max = v3TempMax;
//}
/*============================================================================*/
/*  END OF FILE "VfaPlaneConstraints.cpp"                                     */
/*============================================================================*/
