/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAPLANECONTROLLER_H
#define _VFAPLANECONTROLLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaWidgetController.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaReflectionable.h>
#include <VistaFlowLib/Data/VflObserver.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaSphereVis;
class VistaColor;
class VistaIndirectXform;
class VfaPlaneModel;
class VflRenderNode;
class VfaPlaneConstraints;
class VfaArrowVis;
class VfaSphereHandle;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Controll the behavior on button press event, button release event and 
 * pre-loop event, also highlight.
 */
class VISTAFLOWLIBAUXAPI VfaPlaneController : public VflObserver, public IVfaWidgetController
{
public:
	VfaPlaneController(VfaPlaneModel *pModel, VflRenderNode *pRenderNode);
	virtual ~VfaPlaneController();

	/** 
	 * This is called on transition change into(out of) state FOCUS
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();

	/**
	 * This is called on transition change into(out of) state TOUCH
	 */
	void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	void OnUntouch();

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	void OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate(int iSlot, const bool bSet);
	void OnTimeSlotUpdate(const double dTime);
	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/** 
	 *
	 * Handle modes of plane widget according to the activated handles
	 *  
	 *      1 --------------- 2
	 *      |                 |
	 *      |        0        |
     *      |                 |
	 *      4 --------------- 3
	 *
	 * 1) plane can be resized and transformed, i.e. all 5 handles are active
	 *    Handle 0 is for the transformation
	 *    Handle 1-4 are for the resize                ---> DEFAULT
	 * 2) plane can only be resized, i.e. handle 0 is inactive
	 * 3) plane can only be transformed, i.e. handle 1-4 are inactive
	 */
	enum 
	{
		HM_INVALID = -1,
		HM_RESIZE_AND_TRANSFORM = 0,
		HM_ONLY_RESIZE,
		HM_ONLY_TRANSFORM,
		HM_LAST = 3
	} eHandleMode;

	int GetHandleMode() const;
	void SetHandleMode(int eMode);

	/**
	 *  toggle visibility of non active handles
	 *  default: non active handles are hiden
	 */
	void HideNonActiveHandles();
	void ShowAllHandles();

	void SetArrowVisible(bool bVis);
	bool GetArrowVisible();

	class VISTAFLOWLIBAUXAPI VfaPlaneControllerProps : public IVistaReflectionable
	{
		friend class VfaPlaneController;
	public:
		enum FIXPOINT{
			FIXPOINT_CORNER,
			FIXPOINT_CENTER
		};

		VfaPlaneControllerProps(VfaPlaneController *pPlaneCtrl);
		virtual ~VfaPlaneControllerProps();
		/*
		 * Use the current aspect ratio as a constraint for further
		 * modification.
		 */
		bool SetFixAspectRatio(bool bFixAspectRatio);
		bool GetFixAspectRatio() const;
		/*
		 * Allow turning the Plane inside out by moving m_v3PlaneMin beyond
		 * m_v3PlaneMax. Internally m_v3PlaneMin and m_v3PlaneMax are reset to keep
		 * Plane constraints.
		 */
		bool SetAllowPenetration(bool bAllowPenetration);
		bool GetAllowPenetration() const;
		/*
		 * Grabbing a corner of the Plane and changing the Plane size, this is the
		 * point which stays fix. This can eiter be the center of the Plane or the
		 * corner across the grabbed corner.
		 */
		bool SetFixpoint(FIXPOINT eFixpoint);
		FIXPOINT GetFixpoint() const;

		bool SetHandleRadius(float fRadius);
		float GetHandleRadius() const;

		bool SetArrowLength(float fLength);
		float GetArrowLength() const;

		bool SetArrowRadius(float fRadius);
		float GetArrowRadius() const;

		bool SetHandleNormalColor(const VistaColor& color);
		VistaColor GetHandleNormalColor() const;
		void SetHandleNormalColor(float fC[4]);
		void GetHandleNormalColor(float fC[4]) const;

		bool SetHandleHighlightColor(const VistaColor& color);
		VistaColor GetHandleHighlightColor() const;
		void SetHandleHighlightColor(float fC[4]);
		void GetHandleHighlightColor(float fC[4]) const;
		
		bool SetMiddleHandleNormalColor(const VistaColor& color);
		VistaColor GetMiddleHandleNormalColor() const;
		void SetMiddleHandleNormalColor(float fC[4]);
		void GetMiddleHandleNormalColor(float fC[4]) const;

		bool SetMiddleHandleHighlightColor(const VistaColor& color);
		VistaColor GetMiddleHandleHighlightColor() const;
		void SetMiddleHandleHighlightColor(float fC[4]);
		void GetMiddleHandleHighlightColor(float fC[4]) const;

		bool SetHandleMode(int eMode);
		int GetHandleMode() const;

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		VfaPlaneController	*m_pPlaneCtrl;

		bool				m_bFixAspectRatio;
		bool				m_bAllowPenetration;
		FIXPOINT			m_eFixpoint;
	};

	VfaPlaneControllerProps* GetProperties() const;
protected:
	void UpdateHandles();
private:
	enum ePlaneControllerState { 
		BCS_NONE, 
		BCS_MOVE, 
		BCS_RESIZE 
	};
	VfaPlaneController::ePlaneControllerState m_iCurrentInternalState;
	
	VfaPlaneModel	*m_pModel;
	int				m_iGrabButton;
	
	std::vector<VfaSphereHandle*>	m_vecHandles;
	VfaArrowVis*				m_pNormalArrow;
	VistaIndirectXform			*m_pXform;
	VfaPlaneControllerProps	*m_pCtlProps;
	
	// focused handle and its index
	VfaSphereHandle*		m_pFocusHandle;
	int						m_iHandle;

	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;
	bool						m_bEnabled;

	int						m_eHandleMode;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAPLANECONTROLLER_H
