/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaPlaneModel.h"
#include <cstring>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  IMPLEMENTATION      VfaPlaneVisProps                                       */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaPlaneModel");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<bool, VfaPlaneModel>(
		"AXIS_ALIGNED", STR_REF_TYPENAME,
		&VfaPlaneModel::GetIsAxisAligned),
	new TVistaPropertyGet<std::vector<float>, VfaPlaneModel>(
		"EXTENTS", STR_REF_TYPENAME,
		&VfaPlaneModel::GetExtents),
	new TVistaPropertyGet<VistaQuaternion, VfaPlaneModel>(
		"ROTATION", STR_REF_TYPENAME,
		&VfaPlaneModel::GetRotation),
	NULL
};
//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<const VistaVector3D &, VistaVector3D, VfaPlaneModel>(
		"PLANE_CENTER", STR_REF_TYPENAME,
		&VfaPlaneModel::SetTranslation),
	new TVistaPropertySet<const std::vector<float> &, std::vector<float>, VfaPlaneModel>(
		"EXTENTS", STR_REF_TYPENAME,
		&VfaPlaneModel::SetExtents),
	new TVistaPropertySet<const VistaQuaternion &, VistaQuaternion, VfaPlaneModel>(
		"ROTATION", STR_REF_TYPENAME,
		&VfaPlaneModel::SetRotation),
	NULL 
};
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaPlaneModel::VfaPlaneModel()
	:	m_v3Center(0.0f, 0.0f, 0.0f),
		m_v3Translation(VistaVector3D(0.0f, 0.0f, 0.0f)),
		m_qRotation(VistaQuaternion(0.0f, 0.0f, 0.0f, 1.0f))
{
	m_fExtents[0] = m_fExtents[1] = 1.0f;
}
VfaPlaneModel::~VfaPlaneModel()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTranslation                                          */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneModel::SetTranslation(float fTranslation[3])
{
	VistaVector3D v3Trans(fTranslation);
	return this->SetTranslation(v3Trans);
}
bool VfaPlaneModel::SetTranslation(double dTranslation[3])
{
	VistaVector3D v3Trans(dTranslation);
	return this->SetTranslation(v3Trans);
}

bool VfaPlaneModel::SetTranslation(const VistaVector3D &v3Translation)
{
	if(v3Translation == m_v3Translation)
		return false;

	this->StoreState();
	m_v3Translation = v3Translation;

	if(!this->CheckConstraints())
	{
		this->RestoreState();
		return false;
	}

	this->Notify(MSG_PLANE_CHANGED);
	return true;
}

void VfaPlaneModel::GetTranslation(float fTranslation[3]) const
{
	m_v3Translation.GetValues(fTranslation);
}

void VfaPlaneModel::GetTranslation(double dTranslation[3]) const
{
	m_v3Translation.GetValues(dTranslation);
}

void VfaPlaneModel::GetTranslation(VistaVector3D &v3Trans) const
{
	v3Trans = m_v3Translation;
}

VistaVector3D VfaPlaneModel:: GetTranslation() const
{
	return m_v3Translation;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRotation                                             */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneModel::SetRotation(const VistaQuaternion &qRotation)
{
	if(qRotation == m_qRotation)
		return false;

	this->StoreState();
	m_qRotation = qRotation;
	if(!this->CheckConstraints())
	{
		this->RestoreState();
		return false;
	}

	this->Notify(MSG_PLANE_CHANGED);
	return true;
}

void VfaPlaneModel::GetRotation(VistaQuaternion &qRot) const
{
	qRot = m_qRotation;
}

VistaQuaternion VfaPlaneModel::GetRotation() const
{
	return m_qRotation;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNormal                                               */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneModel::SetNormal(float fNormal[3])
{
	VistaVector3D v3Normal(fNormal);
	v3Normal[3] = 0.0f;
	return this->SetNormal(v3Normal);
}
bool VfaPlaneModel::SetNormal(double dNormal[3])
{
	VistaVector3D v3Normal(dNormal);
	v3Normal[3] = 0.0f;
	return this->SetNormal(v3Normal);
}

bool VfaPlaneModel::SetNormal(const VistaVector3D &v3Normal)
{
	this->StoreState();

	//set the rotation accordingly
	//standard normal is in z-direction
	VistaVector3D v3StdNormal(0.0f, 0.0f, 1.0f, 0.0f);
	//rotate normal into new direction
	m_qRotation = VistaQuaternion(v3StdNormal, v3Normal);

	if(!this->CheckConstraints())
	{
		this->RestoreState();
		return false;
	}

	this->Notify(MSG_PLANE_CHANGED);
	return true;
}

void VfaPlaneModel::GetNormal(float fNormal[3]) const
{
	m_qRotation.Rotate(VistaVector3D(0,0,-1)).GetValues(fNormal);
}
void VfaPlaneModel::GetNormal(double dNormal[3]) const
{
	m_qRotation.Rotate(VistaVector3D(0,0,-1)).GetValues(dNormal);
}

void VfaPlaneModel::GetNormal(VistaVector3D &v3Normal) const
{
	v3Normal = m_qRotation.Rotate(VistaVector3D(0,0,-1));
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetExtents                                              */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneModel::SetExtents(float fWidth, float fHeight)
{
	if(fWidth == m_fExtents[0] && fHeight == m_fExtents[1])
		return false;
	
	this->StoreState();
		
	m_fExtents[0] = fWidth;
	m_fExtents[1] = fHeight;

	if(!this->CheckConstraints())
	{
		this->RestoreState();
		return false;
	}

	this->Notify(MSG_PLANE_CHANGED);
	return true;
}

bool VfaPlaneModel::SetExtents(const float fExtents[2])
{
	return this->SetExtents(fExtents[0], fExtents[1]);
}

bool VfaPlaneModel::SetExtents(const std::vector<float> &vExtents)
{
	return this->SetExtents(vExtents.at(0), vExtents.at(1));
}

void VfaPlaneModel::GetExtents(float &fWidth, float &fHeight) const
{
	fWidth = m_fExtents[0];
	fHeight = m_fExtents[1];
}

void VfaPlaneModel::GetExtents(float fExtents[2]) const
{
	this->GetExtents(fExtents[0], fExtents[1]);
}

std::vector<float> VfaPlaneModel::GetExtents() const
{
	return std::vector<float>(m_fExtents, m_fExtents + sizeof(m_fExtents) / sizeof(float));
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCorner                                                   */
/*                                                                            */
/*============================================================================*/
void VfaPlaneModel::GetCorner(unsigned char i, float fCorner[3]) const
{
	VistaVector3D v3Corner;
	this->GetCorner(i, v3Corner);
	v3Corner.GetValues(fCorner);
}

void VfaPlaneModel::GetCorner(unsigned char i, VistaVector3D& v3Corner) const
{
	v3Corner = m_v3Center;

	v3Corner[0] +=(i & 0x01 ? 0.5f*m_fExtents[0] : -0.5f*m_fExtents[0]);
	v3Corner[1] +=(i & 0x02 ? 0.5f*m_fExtents[1] : -0.5f*m_fExtents[1]);

	v3Corner = m_v3Translation + m_qRotation.Rotate(v3Corner);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIsAxisAligned                                            */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneModel::GetIsAxisAligned() const
{
	VistaQuaternion qNeutral(0.0f, 0.0f, 0.0f, 1.0f);
	return m_qRotation == qNeutral;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   StoreState                                                  */
/*                                                                            */
/*============================================================================*/
void VfaPlaneModel::StoreState()
{
	if(!this->HasConstraints())
		return;

	//remember old state
	memcpy(m_fOldExtents, m_fExtents, 2*sizeof(float));
	m_v3OldTranslation = m_v3Translation;
	m_qOldRotation = m_qRotation;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RestoreState                                                */
/*                                                                            */
/*============================================================================*/
void VfaPlaneModel::RestoreState()
{
	m_v3Translation = m_v3OldTranslation;
	memcpy(m_fExtents, m_fOldExtents, 2*sizeof(float));
	m_qRotation = m_qOldRotation;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsProjectionInside	                                      */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneModel::IsProjectionInside(float fPos[3])
{
	VistaVector3D v3Pos(fPos);
	return this->IsProjectionInside(v3Pos);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsProjectionInside	                                      */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneModel::IsProjectionInside(const VistaVector3D &v3Pos)
{
	//unrotate offset from center
	VistaVector3D v3PlaneCoords(
		m_qRotation.GetComplexConjugated().Rotate(v3Pos-m_v3Translation));
	//vstr::debugi()<<""Projected plane coords "<<v3PlaneCoords<<endl;
	return	v3PlaneCoords[0] >= -0.5f * m_fExtents[0] &&
			v3PlaneCoords[0] <=  0.5f * m_fExtents[0] &&
			v3PlaneCoords[1] >= -0.5f * m_fExtents[1] &&
			v3PlaneCoords[1] <=  0.5f * m_fExtents[1];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaPlaneModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaPlaneModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  END OF FILE "VfaPlaneWidget.cpp"                                            */
/*============================================================================*/
