/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaPlaneController.h"
#include "VfaPlaneConstraints.h"
#include "VfaPlaneModel.h"
#include "VfaPlaneWidget.h"
#include "../VfaWidgetTools.h"
#include "../VfaSphereVis.h"
#include "../VfaArrowVis.h"
#include "../VfaSphereHandle.h"

#include <VistaKernel/InteractionManager/VistaIntentionSelect.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <VistaMath/VistaIndirectXform.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <algorithm>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaPlaneController::VfaPlaneController(VfaPlaneModel *pModel,
										 VflRenderNode *pRenderNode)
: IVfaWidgetController(pRenderNode),
  m_pModel(pModel),
  m_iGrabButton(0),
  m_pFocusHandle(NULL),
  m_pXform(new VistaIndirectXform),
  m_iCurrentInternalState(VfaPlaneController::BCS_NONE),
  m_bEnabled(true),
  m_eHandleMode(HM_RESIZE_AND_TRANSFORM)
{
	m_pCtlProps = new VfaPlaneController::VfaPlaneControllerProps(this);

	m_vecHandles.resize(5);
	for(unsigned int i=0; i<m_vecHandles.size(); ++i)
	{
		m_vecHandles[i] = new VfaSphereHandle(pRenderNode);
		float fColor[4] = {1.0f, 0.0f, 0.0f, 1.0f};
		m_vecHandles[i]->SetNormalColor(fColor);
		float fHColor[4] = {1.0f, 0.75f, 0.75f, 1.0f};
		m_vecHandles[i]->SetHighlightColor(fHColor);
		this->AddControlHandle(m_vecHandles[i]);
	}

	//Middle Handle is responsible for translation -> special color
	float fColor[4] = {0.0f, 0.0f, 1.0f, 1.0f};
	m_vecHandles[0]->SetNormalColor(fColor);
	float fHColor[4] = {0.75f, 0.75f, 1.0f, 1.0f};
	m_vecHandles[0]->SetHighlightColor(fHColor);

	m_pNormalArrow = new VfaArrowVis;
	if(m_pNormalArrow->Init())
	{
		pRenderNode->AddRenderable(m_pNormalArrow);
		m_pNormalArrow->SetUseLighting(true);
	}
}

VfaPlaneController::~VfaPlaneController()
{
	delete m_pCtlProps;
	delete m_pXform;

	for(unsigned int i=0; i<m_vecHandles.size(); ++i)
	{
		delete m_vecHandles[i];
	}
	
	m_pNormalArrow->GetRenderNode()->RemoveRenderable(m_pNormalArrow);
	delete m_pNormalArrow;

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::SetIsEnabled(bool b)
{
	if(b)
	{
		switch (m_eHandleMode)
		{
		case HM_ONLY_RESIZE:
			m_pNormalArrow->SetVisible(false);
			m_vecHandles[0]->SetEnable(false);
			m_vecHandles[0]->SetVisible(false);
			for(unsigned int i=1; i<m_vecHandles.size(); ++i)
				m_vecHandles[i]->SetEnable(true);
			break;
		case HM_ONLY_TRANSFORM:
			m_pNormalArrow->SetVisible(true);
			m_vecHandles[0]->SetEnable(true);
			for(unsigned int i=1; i<m_vecHandles.size(); ++i)
			{
				m_vecHandles[i]->SetEnable(false);
				m_vecHandles[i]->SetVisible(false);
			}
			break;
		case HM_RESIZE_AND_TRANSFORM:
		default:
			m_pNormalArrow->SetVisible(true);
			for(unsigned int i=0; i<m_vecHandles.size(); ++i)
				m_vecHandles[i]->SetEnable(true);
			break;
		}		
	}
	else
	{
		m_pNormalArrow->SetVisible(false);
		for(unsigned int i=0; i<m_vecHandles.size(); ++i)
			m_vecHandles[i]->SetEnable(false);
	}
	m_bEnabled = b;
}
bool VfaPlaneController::GetIsEnabled() const
{
	return m_bEnabled;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnFocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::OnFocus(IVfaControlHandle* pHandle)
{
	// don't allow to change focus if some handle already has it
	if(m_iCurrentInternalState == BCS_NONE)
	{
		m_pFocusHandle = static_cast<VfaSphereHandle*>(pHandle);

		// search focused handle in list
		m_iHandle =(int)(find(m_vecHandles.begin(), m_vecHandles.end(), m_pFocusHandle) - m_vecHandles.begin());

		m_pFocusHandle->SetIsHighlighted(true);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUnfocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::OnUnfocus()
{
	// don't allow to change focus if some handle already has it
	if(m_iCurrentInternalState == BCS_NONE)
	{
		if(m_pFocusHandle)
			m_pFocusHandle->SetIsHighlighted(false);
		m_iHandle = -1;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUntouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::OnUntouch()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	if(iSlot == VfaApplicationContextObject::SLOT_POINTER_VIS)
	{
		// save sensor data
		m_oLastSensorFrame = oFrameData;

		// if we have a move/resize state, move something....
		if(m_iCurrentInternalState != BCS_NONE)
		{
			VistaVector3D v3MyPos;
			VistaQuaternion qMyOri;
			m_pXform->Update(m_oLastSensorFrame.v3Position, 
							 m_oLastSensorFrame.qOrientation, 
							 v3MyPos, qMyOri);

			switch(m_iCurrentInternalState)
			{
			case BCS_MOVE:
				{
					//just apply what we have got from the indirect xform
					m_pModel->SetTranslation(v3MyPos);
					m_pModel->SetRotation(qMyOri);
					break;
				}
			case BCS_RESIZE:
				{
					//v3MyPos now holds the grabbed handle's new pos
					//difference to the center gives new extents
					VistaVector3D v3Trans;
					m_pModel->GetTranslation(v3Trans);
					//"unrotate" the offset by the box's rotation
					VistaQuaternion qRot;
					m_pModel->GetRotation(qRot);
					//@todo check if we rotated in the right direction here!
					v3MyPos = 2.0f*(qRot.GetComplexConjugated().Rotate(v3MyPos-v3Trans));
					m_pModel->SetExtents(fabs(v3MyPos[0]), fabs(v3MyPos[1]));
					break;
				}
			}
		}
	}
	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{

	if(bSet)
	{
		// we can only be grabbed, if we have the focus
		if(this->GetControllerState() == CS_FOCUS)
		{
			VistaVector3D v3Center;
			VistaQuaternion qRot;
			m_pModel->GetRotation(qRot);
				
			if(m_iHandle == 0)
			{
				if(m_eHandleMode == HM_RESIZE_AND_TRANSFORM || m_eHandleMode == HM_ONLY_TRANSFORM)
				{
					m_pModel->GetTranslation(v3Center);
					m_pXform->Init(	m_oLastSensorFrame.v3Position, 
						m_oLastSensorFrame.qOrientation, 
						v3Center, qRot);
					m_iCurrentInternalState = BCS_MOVE;
				}
			}
			else
			if(m_iHandle > 0)
			{
				if(m_eHandleMode == HM_RESIZE_AND_TRANSFORM || m_eHandleMode == HM_ONLY_RESIZE)
				{
					m_pFocusHandle->GetCenter(v3Center);
					// we use the xform in vis coordinates
					m_pXform->Init(	m_oLastSensorFrame.v3Position, 
						m_oLastSensorFrame.qOrientation, 
						v3Center, qRot);
					m_iCurrentInternalState = BCS_RESIZE;
				}
			}
		}
	}
	else
	{
		// if the slot is released, goto state NONE
		m_iCurrentInternalState = BCS_NONE;

		// and if we have lost focus during other states, now react on that
		if(this->GetControllerState()!=CS_FOCUS)
		{
			if(m_pFocusHandle)
				m_pFocusHandle->SetIsHighlighted(false);
			m_iHandle = -1;

		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::OnTimeSlotUpdate(const double dTime)
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSensorMask			                                      */
/*                                                                            */
/*============================================================================*/
int VfaPlaneController::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_POINTER_VIS);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCommandMask			                                  */
/*                                                                            */
/*============================================================================*/
int VfaPlaneController::GetCommandMask() const
{
	return(1 << m_iGrabButton);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeUpdate				                                  */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::GetTimeUpdate() const
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaPlaneModel *pPlaneModel = dynamic_cast<VfaPlaneModel*>(pObserveable);

	if(!pPlaneModel)
		return;

	// reposition handles
	this->UpdateHandles();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  Set/Get HandleMode                                           */
/*                                                                            */
/*============================================================================*/
int VfaPlaneController::GetHandleMode() const
{
	return m_eHandleMode;
}
void VfaPlaneController::SetHandleMode(int eMode)
{
	if (eMode > HM_INVALID && eMode < HM_LAST)
	{
		m_eHandleMode = eMode;

		// update visibility and state of activity
		SetIsEnabled(m_bEnabled);
	}

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  HideNonActiveHandles                                         */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::HideNonActiveHandles()
{
	for(unsigned int i=0; i<m_vecHandles.size(); ++i)
	{
		if(m_vecHandles[i]->GetEnable())
			m_vecHandles[i]->SetVisible(true);
		else
			m_vecHandles[i]->SetVisible(false);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  ShowAllHandles                                               */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::ShowAllHandles()
{
	for(unsigned int i=0; i<m_vecHandles.size(); ++i)
		m_vecHandles[i]->SetVisible(true);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaPlaneController::VfaPlaneControllerProps* VfaPlaneController::GetProperties() const
{
	return m_pCtlProps;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateHandles                                               */
/*                                                                            */
/*============================================================================*/
void VfaPlaneController::UpdateHandles()
{
	VistaVector3D v3HandleCenter, v3Normal;
	m_pModel->GetTranslation(v3HandleCenter);
	m_pModel->GetNormal(v3Normal);
	m_vecHandles[0]->SetCenter(v3HandleCenter);
	m_pNormalArrow->SetCenter(v3HandleCenter);
	m_pNormalArrow->SetRotate(v3Normal);
	for(int c=1; c<5; ++c)
	{
		m_pModel->GetCorner(c, v3HandleCenter);
		m_vecHandles[c]->SetCenter(v3HandleCenter);
	}
}

void VfaPlaneController::SetArrowVisible(bool bVis)
{
	m_pNormalArrow->SetVisible(bVis);
}
bool VfaPlaneController::GetArrowVisible()
{
	return m_pNormalArrow->GetVisible();
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaPlaneControllerProps                                */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaPlaneController::VfaPlaneControllerProps");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<bool, VfaPlaneController::VfaPlaneControllerProps>(
		"FIX_ASPECT_RATIO", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::GetFixAspectRatio),
	new TVistaPropertyGet<bool, VfaPlaneController::VfaPlaneControllerProps>(
		"ALLOW_PENETRATION", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::GetAllowPenetration),
	new TVistaPropertyGet<float, VfaPlaneController::VfaPlaneControllerProps>(
		"HANDLE_RADIUS", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::GetHandleRadius),
	new TVistaPropertyGet<float, VfaPlaneController::VfaPlaneControllerProps>(
		"ARROW_LENGTH", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::GetArrowLength),
	new TVistaPropertyGet<float, VfaPlaneController::VfaPlaneControllerProps>(
		"ARROW_RADIUS", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::GetArrowRadius),
	new TVistaPropertyGet<int, VfaPlaneController::VfaPlaneControllerProps>(
		"HANDLE_MODE", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::GetHandleMode),
	NULL
};

//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<bool, bool, VfaPlaneController::VfaPlaneControllerProps>(
		"FIX_ASPECT_RATIO", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::SetFixAspectRatio),
	new TVistaPropertySet<bool, bool, VfaPlaneController::VfaPlaneControllerProps>(
		"ALLOW_PENETRATION", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::SetAllowPenetration),
	new TVistaPropertySet<float, float, VfaPlaneController::VfaPlaneControllerProps>(
		"HANDLE_RADIUS", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::SetHandleRadius),
	new TVistaPropertySet<float, float, VfaPlaneController::VfaPlaneControllerProps>(
		"ARROW_LENGTH", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::SetArrowLength),
	new TVistaPropertySet<float, float, VfaPlaneController::VfaPlaneControllerProps>(
		"ARROW_RADIUS", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::SetArrowRadius),
	new TVistaPropertySet<int, int, VfaPlaneController::VfaPlaneControllerProps>(
		"HANDLE_MODE", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::SetHandleMode),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaPlaneController::VfaPlaneControllerProps>(
		"HANDLE_NORMAL_COLOR", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::SetHandleNormalColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaPlaneController::VfaPlaneControllerProps>(
		"HANDLE_HIGHLIGHT_COLOR", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::SetHandleHighlightColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaPlaneController::VfaPlaneControllerProps>(
		"TRANSLATION_HANDLE_NORMAL_COLOR", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::SetMiddleHandleNormalColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaPlaneController::VfaPlaneControllerProps>(
		"TRANSLATION_HANDLE_HIGHLIGHT_COLOR", STR_REF_TYPENAME,
		&VfaPlaneController::VfaPlaneControllerProps::SetMiddleHandleHighlightColor),
	NULL 
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaPlaneController::VfaPlaneControllerProps::VfaPlaneControllerProps(VfaPlaneController *pPlaneCtrl)
: m_pPlaneCtrl(pPlaneCtrl),
  m_bFixAspectRatio(false),
  m_bAllowPenetration(false),
  m_eFixpoint(FIXPOINT_CENTER)
{

}

VfaPlaneController::VfaPlaneControllerProps::~VfaPlaneControllerProps()
{

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFixAspectRatio                                       */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::VfaPlaneControllerProps::SetFixAspectRatio(bool bFixAspectRatio)
{
	if(bFixAspectRatio != m_bFixAspectRatio)
	{
		m_bFixAspectRatio = bFixAspectRatio;
		Notify();
		return true;
	}
	return false;
}

bool VfaPlaneController::VfaPlaneControllerProps::GetFixAspectRatio() const
{
	return m_bFixAspectRatio;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetAllowPenetration                                     */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::VfaPlaneControllerProps::SetAllowPenetration(bool bAllowPenetration)
{
	if(bAllowPenetration != m_bAllowPenetration)
	{
		m_bAllowPenetration = bAllowPenetration;
		Notify();
		return true;
	}
	return false;
}

bool VfaPlaneController::VfaPlaneControllerProps::GetAllowPenetration() const
{
	return m_bAllowPenetration;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFixpoint                                             */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::VfaPlaneControllerProps::SetFixpoint(FIXPOINT eFixpoint)
{
	if(eFixpoint != m_eFixpoint)
	{
		m_eFixpoint = eFixpoint;
		Notify();
		return true;
	}
	return false;
}

VfaPlaneController::VfaPlaneControllerProps::FIXPOINT 
	VfaPlaneController::VfaPlaneControllerProps::GetFixpoint() const
{
	return m_eFixpoint;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleRadius                                         */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::VfaPlaneControllerProps::SetHandleRadius(float fRadius)
{
	if(fRadius != m_pPlaneCtrl->m_vecHandles[0]->GetRadius())
	{
		for(unsigned int i=0; i<m_pPlaneCtrl->m_vecHandles.size(); ++i)
		{
			m_pPlaneCtrl->m_vecHandles[i]->SetRadius(fRadius);
		}
		//m_pPlaneCtrl->m_pNormalArrow->SetCylinderRadius(fRadius*0.9f);
		//m_pPlaneCtrl->m_pNormalArrow->SetConeHeight(fRadius*1.25f);

		Notify();
		return true;
	}
	return false;
}

float VfaPlaneController::VfaPlaneControllerProps::GetHandleRadius() const
{
	return m_pPlaneCtrl->m_vecHandles[0]->GetRadius();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetArrowLength                                          */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::VfaPlaneControllerProps::SetArrowLength(float fLength)
{
	m_pPlaneCtrl->m_pNormalArrow->SetCylinderHeight(fLength*0.75f);
	m_pPlaneCtrl->m_pNormalArrow->SetConeHeight(fLength*0.25f);

	Notify();
	
	return true;
}

float VfaPlaneController::VfaPlaneControllerProps::GetArrowLength() const
{
	float fConeHeight = m_pPlaneCtrl->m_pNormalArrow->GetConeHeight();
	float fCylinderHeight = m_pPlaneCtrl->m_pNormalArrow->GetCylinderHeight();

	return fConeHeight + fCylinderHeight;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetArrowRadius                                          */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::VfaPlaneControllerProps::SetArrowRadius(float fRadius)
{
	m_pPlaneCtrl->m_pNormalArrow->SetCylinderRadius(fRadius);
	m_pPlaneCtrl->m_pNormalArrow->SetConeRadius(fRadius*4.0f);

	Notify();
	
	return true;

}

float VfaPlaneController::VfaPlaneControllerProps::GetArrowRadius() const
{
	return m_pPlaneCtrl->m_pNormalArrow->GetCylinderRadius();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleNormalColor                                    */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::VfaPlaneControllerProps::SetHandleNormalColor(
	const VistaColor& color)
{
	if (m_pPlaneCtrl->m_vecHandles[1]->GetNormalColor() == color)
		return false;

	for(unsigned int i=1; i<m_pPlaneCtrl->m_vecHandles.size(); ++i)
	{
		float fColors[4];
		color.GetValues(fColors);
		m_pPlaneCtrl->m_vecHandles[i]->SetNormalColor(fColors);
	}

	return true;
}

VistaColor VfaPlaneController::VfaPlaneControllerProps::GetHandleNormalColor() const
{
	float fColors[4];
	m_pPlaneCtrl->m_vecHandles[1]->GetNormalColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaPlaneController::VfaPlaneControllerProps::SetHandleNormalColor(float fC[4])
{
	for(unsigned int i=1; i<m_pPlaneCtrl->m_vecHandles.size(); ++i)
	{
		m_pPlaneCtrl->m_vecHandles[i]->SetNormalColor(fC);
	}
}

void VfaPlaneController::VfaPlaneControllerProps::GetHandleNormalColor(float fC[4]) const
{
	m_pPlaneCtrl->m_vecHandles[1]->GetNormalColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleHighlightColor                                 */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::VfaPlaneControllerProps::SetHandleHighlightColor(
	const VistaColor& color)
{
	if (m_pPlaneCtrl->m_vecHandles[1]->GetHighlightColor() == color)
		return false;

	for(unsigned int i=1; i<m_pPlaneCtrl->m_vecHandles.size(); ++i)
	{
		float fColors[4];
		color.GetValues(fColors);
		m_pPlaneCtrl->m_vecHandles[i]->SetHighlightColor(fColors);
	}

	return true;
}

VistaColor VfaPlaneController::VfaPlaneControllerProps::GetHandleHighlightColor() const
{
	float fColors[4];
	m_pPlaneCtrl->m_vecHandles[1]->GetHighlightColor(fColors);
	VistaColor color(fColors);

	return color;
}

void VfaPlaneController::VfaPlaneControllerProps::SetHandleHighlightColor(float fC[4])
{
	for(unsigned int i=1; i<m_pPlaneCtrl->m_vecHandles.size(); ++i)
	{
		m_pPlaneCtrl->m_vecHandles[i]->SetHighlightColor(fC);
	}
}

void VfaPlaneController::VfaPlaneControllerProps::GetHandleHighlightColor(float fC[4]) const
{
	m_pPlaneCtrl->m_vecHandles[1]->GetHighlightColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMiddleHandleNormalColor                                    */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::VfaPlaneControllerProps::SetMiddleHandleNormalColor(
	const VistaColor& color)
{
	if (m_pPlaneCtrl->m_vecHandles[0]->GetNormalColor() == color)
		return false;

	float fColors[4];
	color.GetValues(fColors);
	m_pPlaneCtrl->m_vecHandles[0]->SetNormalColor(fColors);

	return true;
}

VistaColor VfaPlaneController::VfaPlaneControllerProps::GetMiddleHandleNormalColor() const
{
	float fColors[4];
	m_pPlaneCtrl->m_vecHandles[0]->GetNormalColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaPlaneController::VfaPlaneControllerProps::SetMiddleHandleNormalColor(float fC[4])
{
	m_pPlaneCtrl->m_vecHandles[0]->SetNormalColor(fC);
}

void VfaPlaneController::VfaPlaneControllerProps::GetMiddleHandleNormalColor(float fC[4]) const
{
	m_pPlaneCtrl->m_vecHandles[0]->GetNormalColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMiddleHandleHighlightColor                                 */
/*                                                                            */
/*============================================================================*/
bool VfaPlaneController::VfaPlaneControllerProps::SetMiddleHandleHighlightColor(
	const VistaColor& color)
{
	if (m_pPlaneCtrl->m_vecHandles[0]->GetHighlightColor() == color)
		return false;

	float fColors[4];
	color.GetValues(fColors);
	m_pPlaneCtrl->m_vecHandles[0]->SetHighlightColor(fColors);

	return true;
}

VistaColor VfaPlaneController::VfaPlaneControllerProps::GetMiddleHandleHighlightColor() const
{
	float fColors[4];
	m_pPlaneCtrl->m_vecHandles[0]->GetHighlightColor(fColors);
	VistaColor color(fColors);

	return color;
}

void VfaPlaneController::VfaPlaneControllerProps::SetMiddleHandleHighlightColor(float fC[4])
{
	m_pPlaneCtrl->m_vecHandles[0]->SetHighlightColor(fC);
}

void VfaPlaneController::VfaPlaneControllerProps::GetMiddleHandleHighlightColor(float fC[4]) const
{
	m_pPlaneCtrl->m_vecHandles[0]->GetHighlightColor(fC);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleMode                                           */
/*                                                                            */
/*============================================================================*/

bool VfaPlaneController::VfaPlaneControllerProps::SetHandleMode(int eMode)
{
	if (m_pPlaneCtrl->GetHandleMode() == eMode)
		return false;

	m_pPlaneCtrl->SetHandleMode(eMode);
	return true;
}

int VfaPlaneController::VfaPlaneControllerProps::GetHandleMode() const
{
	return m_pPlaneCtrl->GetHandleMode();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaPlaneController::VfaPlaneControllerProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaPlaneController::VfaPlaneControllerProps::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  END OF FILE "VfaPlaneController.cpp"                                        */
/*============================================================================*/
