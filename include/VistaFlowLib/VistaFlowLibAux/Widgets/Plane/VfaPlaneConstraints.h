/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

#ifndef _VFAPLANECONSTRAINTS_H
#define _VFAPLANECONSTRAINTS_H

#include "../VfaWidgetConstraints.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/**
 * This constraints proofs whether the center point of an (infinite) plane
 * model still lies inside the given bounds.
 */
class VISTAFLOWLIBAUXAPI VfaRestrictPlaneToAABounds : public IVfaWidgetConstraint
{
public:
	VfaRestrictPlaneToAABounds();
	virtual ~VfaRestrictPlaneToAABounds();

	virtual bool CheckConstraint(const VfaWidgetModelBase*const pModel);
	
	/**
	 *  Set/get the axis aligned bounds, fBounds is in VTK-notation
	 *  i.e. fBounds[6] = min_x, max_x, min_y, max_y, min_z, max_z
	 */
	void SetBounds(float fBounds[6]);
	void GetBounds(float fBounds[6]) const;

private:
	float m_fBounds[6];

};

class VISTAFLOWLIBAUXAPI VfaRestrictPlaneToAxisAlignedPlane : public IVfaWidgetConstraint 
{
public:
	VfaRestrictPlaneToAxisAlignedPlane();
	virtual ~VfaRestrictPlaneToAxisAlignedPlane();

	virtual bool CheckConstraint(const VfaWidgetModelBase*const pModel);

	enum EPlaneMode
	{
		XY_PLANE,
		XZ_PLANE,
		YZ_PLANE
	};

	void SetPlaneMode(int iMode);
	int GetPlaneMode() const;

private:
	int m_iPlaneMode;
};


//#include "VfaPlaneController.h"

//#include <VistaBase/VistaVectorMath.h>


//class VfaPlaneConstraints
//{
//public:
//	VfaPlaneConstraints(VfaPlaneController::VfaPlaneControllerProps *pProps);
//
//	// not checked!
//	void CheckFixAspectRatio(VistaVector3D &v3Min, VistaVector3D &v3Max);
//
//	// not checked!
//	void CheckPenetration(VistaVector3D &v3Min, VistaVector3D &v3Max);
//
//	void EvaluateFixpoint(VistaVector3D &v3Min, VistaVector3D &v3Max);
//
//	void SetLastMin(VistaVector3D v3Min)
//	{
//		m_v3LastMin = v3Min;
//	}
//
//	void SetLastMax(VistaVector3D v3Max)
//	{
//		m_v3LastMax = v3Max;
//	}
//
//private:
//	VfaPlaneController::VfaPlaneControllerProps *m_pCtlProps;
//	VistaVector3D				m_v3LastMin;
//	VistaVector3D				m_v3LastMax;
//};

#endif // _VFAPLANECONSTRAINTS_H
