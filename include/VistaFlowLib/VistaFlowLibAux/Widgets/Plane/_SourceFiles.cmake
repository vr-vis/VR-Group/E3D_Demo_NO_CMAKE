

set( RelativeDir "./Widgets/Plane" )
set( RelativeSourceGroup "Source Files\\Widgets\\Plane" )

set( DirFiles
	VfaPlaneConstraints.cpp
	VfaPlaneConstraints.h
	VfaPlaneController.cpp
	VfaPlaneController.h
	VfaPlaneModel.cpp
	VfaPlaneModel.h
	VfaPlaneWidget.cpp
	VfaPlaneWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

