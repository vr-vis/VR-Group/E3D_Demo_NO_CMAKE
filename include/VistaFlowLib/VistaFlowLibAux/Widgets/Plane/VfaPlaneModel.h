/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAPLANEMODEL_H
#define _VFAPLANEMODEL_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetModelBase.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaEventManager;
class VistaNewInteractionManager;
class VflVisController;
class VistaSceneGraph;
class VfaPlaneVis;
class VfaPlaneController;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * 
 */
class VISTAFLOWLIBAUXAPI VfaPlaneModel : public VfaWidgetModelBase
{
public:
	enum{
		MSG_PLANE_CHANGED = VfaWidgetModelBase::MSG_LAST,
		MSG_LAST
	};

	VfaPlaneModel();
	virtual ~VfaPlaneModel();

	bool SetTranslation(float fTranslation[3]);
	bool SetTranslation(double dTranslation[3]);
	bool SetTranslation(const VistaVector3D &v3Translation);

	void GetTranslation(float fTranslation[3]) const; 
	void GetTranslation(double dTranslation[3]) const; 
	void GetTranslation(VistaVector3D &v3Translation) const;
	VistaVector3D GetTranslation() const; 

	bool SetRotation(const VistaQuaternion &qRotation);
	void GetRotation(VistaQuaternion &qRot) const;
	VistaQuaternion GetRotation() const;

	/** 
	 * Specify the quad's rotation by providing the plane normal
	 * NOTE: It is assumed that the normal is already normalized,
	 *       so make sure to check beforehand, otherwise things
	 *       might get nasty.
	 */
	bool SetNormal(float fNormal[3]);
	bool SetNormal(double dNormal[3]);
	bool SetNormal(const VistaVector3D &v3Normal);

	void GetNormal(float fNormal[3]) const;
	void GetNormal(double dNormal[3]) const;
	void GetNormal(VistaVector3D &v3Normal) const;

	bool SetExtents(const float fWidth, const float fHeight);
	bool SetExtents(const float fExtents[2]);
	bool SetExtents(const std::vector<float> &vExtents);

	void GetExtents(float &fWidth, float &fHeight) const;
	void GetExtents(float fExtents[2]) const;
	std::vector<float> GetExtents() const;

	/** 
	 * enumeration for the quad's corner vertices
	 * NOTE: The standard plane lies in the xy-plane (i.e. positve z
	 *       direction is assumed as standard normal).
	 *
	 * NOTE: Mind the order here!
	 */
	enum ECorner{
		CORNER_LB = 0, //left, bottom
		CORNER_RB,
		CORNER_LT,
		CORNER_RT
	};
	/** 
	 * retrieve the corner coordinates in the defining space
	 * using the aforementioned indexing
	 */
	void GetCorner(unsigned char i, float fCorner[3]) const;
	void GetCorner(unsigned char i, VistaVector3D &v3Corner) const;

	/**
	 * @todo This currently only checks for the default unit quaternion
	 *		 and therefore neglects rotational symmetry along the axes!
	 */
	bool GetIsAxisAligned() const;

	/**
	 * for a given point in 3 space check if its projected position
	 * falls inside the given extents
	 */
	bool IsProjectionInside(float fPos[3]);
	bool IsProjectionInside(const VistaVector3D &v3Pos);

	virtual std::string GetReflectionableType() const;

protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	/**
	 * convenience methods for internal state management
	 * related to constraint checking
	 */
	void StoreState();
	void RestoreState();

private:
	VistaVector3D		m_v3Center;
	VistaVector3D		m_v3Translation;
	VistaQuaternion	m_qRotation;
	float				m_fExtents[2];

	// temp. storage
	VistaVector3D		m_v3OldTranslation;
	VistaQuaternion	m_qOldRotation;
	float				m_fOldExtents[2];
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAPLANEMODEL_H
