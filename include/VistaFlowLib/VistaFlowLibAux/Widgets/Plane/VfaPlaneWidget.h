/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAPLANEWIDGET_H
#define _VFAPLANEWIDGET_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaWidget.h"
#include "../VfaQuadVis.h"
#include "VfaPlaneController.h"
#include "VfaPlaneModel.h"


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * All parts of a Plane widget will be composed here, namely a wirecube and
 * nine spheres. Plane controller controlls the whole interactions between user
 * and widget. Vis controller is responsible for drawing the scene. Event router 
 * catches incoming events and forward them to Plane controller. Visible
 * properties are all contained in widget property.
 */
class VISTAFLOWLIBAUXAPI VfaPlaneWidget : public IVfaWidget
{
public:
	VfaPlaneWidget(VflRenderNode *pRenderNode);
	virtual ~VfaPlaneWidget();

	/**
	 * Only react on InteractionEvents if enabled. Visibility of the widget is
	 * independent of this.
	 */
	void SetIsEnabled(bool bIsEnabled);
	bool GetIsEnabled() const;

	void SetIsVisible(bool bIsVisible);
	bool GetIsVisible() const;


	virtual VfaPlaneModel* GetModel() const;
	virtual VfaQuadVis* GetView() const;
	virtual VfaPlaneController* GetController() const;

private:
	VfaPlaneModel*		m_pPlaneModel;
	VfaQuadVis*			m_pPlaneView;
	VfaPlaneController*	m_pPlaneController;

	bool				m_bEnabled;
	bool				m_bVisible;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
