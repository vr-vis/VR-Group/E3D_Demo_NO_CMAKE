/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFARULERVIS_H
#define _VFARULERVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaLineModel;
class VfaLineVis;
class Vfl3DTextLabel;
class VflRenderNode;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Ruler is a simple line with scales. End points and unit will be given 
 * by the user.
 */
class VISTAFLOWLIBAUXAPI VfaRulerVis : public IVflRenderable
{
public: 
	VfaRulerVis(VfaLineModel *pModel, VflRenderNode *pRenderNode);
	virtual ~VfaRulerVis();
	
	VfaLineModel *GetModel() const;

	virtual void Update();
	virtual void DrawOpaque();
	virtual unsigned int GetRegistrationMode() const;

	void SetText(const std::string &strText);
	void SetScaleUnit(const std::string &strUnit);

	class VISTAFLOWLIBAUXAPI VfaRulerVisProperties: public IVflRenderable::VflRenderableProperties
	{
	public:
		enum
		{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_LAST
		};
		
		VfaRulerVisProperties();
		virtual ~VfaRulerVisProperties();
		
		void SetWidth(float f);
		float GetWidth() const;

		bool SetColor(float fColor[4]);
		bool SetColor(const VistaColor& color);
		void GetColor(float fColor[4]) const;
		VistaColor GetColor() const;

		void SetTextSize(float f);
		float GetTextSize() const;


	protected:
	private:
		float		m_fColor[4];
		float		m_fTextSize;
		float m_fLineWidth;
	};
	virtual VfaRulerVisProperties *GetProperties() const;

protected:
	VfaRulerVis();
	virtual VflRenderableProperties* CreateProperties() const;

	void InternalUpdateText();

private:
	VfaLineModel	*m_pModel;
	VfaLineVis		*m_pLineVis;
	Vfl3DTextLabel *m_pText;
	std::string m_strUnit;

	VflRenderNode	*m_pRenderNode;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFARULERVIS_H
