/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456799012345679901234567990123456799012345679901234567990123456799012345679*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VfaTimeBuoyModel.h           RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  FlowLibAux                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1999-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VfaTimeBuoyModel                                                 */
/*                                                                            */
/*============================================================================*/
#ifndef _VFATIMEBUOYMODEL_H
#define _VFATIMEBUOYMODEL_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaFlowLibAux/Widgets/VfaWidget.h>
#include <VistaFlowLibAux/Widgets/VfaWidgetModelBase.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisTiming;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * The model part of the time buoy MVC widget. Its handles spatial information,
 * temporal information and a model state.
 *
 * Because a single instant passes infinitely fast, the marked time instant is stored
 * as a time range (@see SetTimeInstant or @see SetTimeRange).
 * 
 */
class VISTAFLOWLIBAUXAPI VfaTimeBuoyModel : public VfaWidgetModelBase, public VflObserver
{
public:
	enum{
		MSG_CENTER_CHANGE,
		MSG_ORIENTATION_CHANGE,
		MSG_RADIUS_CHANGE,
		MSG_HEIGHT_CHANGE,
		MSG_COLOR_CHANGE,
		MSG_STATE_CHANGE,
		MSG_OFFSET_CHANGE,
		MSG_LAST
	};


	VfaTimeBuoyModel(VflVisTiming *pVisTiming);
	virtual ~VfaTimeBuoyModel();

	//@{
	/**
	 * @name spatial information
	 *
	 * 
	 */
	bool SetCenter(float fC[3]);
	bool SetCenter(const VistaVector3D &v3C);

	void GetCenter(double dC[3]) const;
	void GetCenter(float fC[3]) const;
	void GetCenter(VistaVector3D &v3C) const;
	void GetVisCenter(VistaVector3D &v3C) const; // already converted to vis space
	void GetVisCenter(double center[3]) const;	  // already converted to vis space

	// movement operations
	bool SetTranslation(const VistaVector3D &v3Translation);
	VistaVector3D GetTranslation() const;

	bool SetRotation(const VistaQuaternion &qRotation);
	VistaQuaternion GetRotation() const;

	bool SetScale(float fScale);
	float GetScale() const;

	// TransformMatrix - needed to convert between both spaces
	VistaTransformMatrix GetTransformMatrix() const;
	//@}

	//@{
	/**
	 * @name time buoy layout
	 *
	 * @todo this should be moved to the view 
	 */
	// height of flag
	bool SetHeight(float f);
	float GetHeight() const;

	// radius of pole
	bool SetRadius(float r);
	float GetRadius() const;

	// specify banner and pole color
	bool SetColor (const float fColor[4]);
	bool GetColor (float fColor[4]) const;
	//@}


	/**
	 * Method:      SetTimeInstant
	 *
	 * Specify the buoy's temporal position, i.e. a time instant.
	 * This instant is stored in m_dMinValue and m_dMaxValue using -+m_dTimeInstantDelta.
	 *
	 * @param       dValue
	 * @return      bool
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool SetTimeInstant (double dValue);
	double GetTimeInstant () const;

	 
	/**
	 * Method:      GetDistanceToMarkedTimeInstant
	 *
	 * Get distance in vistime to marked time instant (i.e., current time - marked time)
	 *
	 * @return      double
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	double GetDistanceToMarkedTimeInstant () const;

	
	/**
	 * Method:      SetActiveDistance
	 *
	 * If the current time is within the active distance 
	 * of the marked time instant, the buoy goes into state STATE_ACTIVE.
	 *
	 * @param       dValue
	 * @return      bool
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool SetActiveDistance (double dValue);
	double GetActiveDistance () const;

	/**
	 * Method:      SetTimeRange
	 *
	 * Explicitly specify the buoy's temporal position by a time range, not an instant.
	 *
	 * @param       dMinValue
	 * @param       dMaxValue
	 * @return      bool
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool SetTimeRange (double dMinValue, double dMaxValue);
	bool GetTimeRange (double &dMinValue, double &dMaxValue) const;


	// active state
	enum eStates { 
		STATE_NONE=0,        /**< clear */
		STATE_ACTIVE,        /**< the marked time instant/interval is current */
		STATE_CHANGINGMARK   /**< the buoy changes its time value according to the current vis time */
	};
	/**
	 * Method:      SetState
	 *
	 * Set internal state.
	 *
	 * @param       iNewState
	 * @return      bool       returns true if the internal state was changed
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool SetState (eStates iNewState);
	eStates GetState () const;
	 
	/**
	 * Method:      SelectTimeBuoy
	 *
	 * Go to to the marked time instant/interval midpoint.
	 *
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void SelectTimeBuoy ();

	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	virtual std::string GetReflectionableType() const;

protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	/**
	 * Method:      CheckIsActive
	 *
	 * Checks wether this model is (temporally) within the specific active distance
	 * and sets the internal state accordingly.
	 *
	 * @return      bool    is true, if the internal state after this method is STATE_ACTIVE
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool CheckIsActive ();

private:
	VflVisTiming *   m_pVisTiming;

	float             m_fCenter[3];
	float             m_fHeight;
	float             m_fRadius;
	float             m_fColor[4];

	double            m_dMinValue;
	double            m_dMaxValue;
	double            m_dActiveDistance;

	double            m_dTimeInstantDelta;
	bool              m_bIsTimeInstant;

	eStates           m_eState;

	VistaVector3D		m_v3Translation;
	VistaQuaternion	m_qRotation;
	float				m_fScale;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFATIMEBUOYMODEL_H
