/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456799012345679901234567990123456799012345679901234567990123456799012345679*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VfaTimeBuoyWidget.h          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  FlowLibAux                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1999-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VfaTimeBuoyWidget                                                */
/*                                                                            */
/*============================================================================*/
#ifndef _VFATIMEBUOYWIDGET_H
#define _VFATIMEBUOYWIDGET_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/Widgets/VfaWidget.h>
#include "VfaTimeBuoyModel.h"
#include "VfaTimeBuoyVis.h"
#include "VfaTimeBuoyController.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VflVisTiming;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*
 * A TimeBuoy widget composes a model, a view and a controller into one workable widget.
 */
class VISTAFLOWLIBAUXAPI VfaTimeBuoyWidget : public IVfaWidget
{
public:
	VfaTimeBuoyWidget(VflRenderNode *pRenderNode, VflVisTiming *pVisTiming, VfaTrajectoryInfo* pTrajectoryView, VfaTrajectoryDraggingController *pTrajController);
	virtual ~VfaTimeBuoyWidget();

	/**
	 * Method:      SetIsEnabled
	 *
	 * Only react on InteractionEvents if enabled. Visibility of the widget is
	 * independent of this.
	 *
	 * @param       bEnabled
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	virtual void SetIsEnabled(bool bEnabled);
	virtual bool GetIsEnabled() const;

	/**
	 * Method:      SetIsVisible
	 *
	 * Changes visibility for all widget components.
	 *
	 * @param       bEnabled
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	virtual void SetIsVisible(bool bEnabled);
	virtual bool GetIsVisible() const;


	virtual IVistaReflectionable *GetModel() const;

	virtual IVflRenderable* GetView() const;

	virtual IVfaWidgetController* GetController() const;

protected:

private:
	VfaTimeBuoyModel		*m_pTimeBuoyModel;
	VfaTimeBuoyVis			*m_pTimeBuoyView;
	VfaTimeBuoyController	*m_pTimeBuoyController;
	bool					 m_bEnabled;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VTNTIMEBUOYWIDGET_H
