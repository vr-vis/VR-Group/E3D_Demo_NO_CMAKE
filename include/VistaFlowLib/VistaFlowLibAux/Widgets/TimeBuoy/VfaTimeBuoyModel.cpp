/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaTimeBuoyModel.h"
#include "VfaTimeBuoyWidget.h"
#include "VfaTimeBuoyVis.h"
#include "VfaTimeBuoyController.h"
#include <VistaFlowLibAux/Widgets/VfaSphereHandle.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

#include <VistaAspects/VistaObserver.h>
#include <VistaAspects/VistaPropertyAwareable.h>

#include <VistaBase/VistaVectorMath.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <VistaFlowLib/Visualization/VflVisController.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

#include <string.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

static const string STR_REF_TYPENAME("VfaTimeBuoyWidget::VfaTimeBuoyWidgetProps");


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTimeBuoyModel::VfaTimeBuoyModel(VflVisTiming *pVisTiming)
	:	m_pVisTiming (pVisTiming),
		m_fHeight(0.5f),
		m_fRadius(0.01f),
		m_v3Translation(VistaVector3D(0.0f, 0.0f, 0.0f)),
		m_qRotation(VistaQuaternion(0.0f, 0.0f, 0.0f, 1.0f)),
		m_fScale(1.0f),
		m_dMinValue(0.0f),
		m_dMaxValue(0.0f),
		m_eState(VfaTimeBuoyModel::STATE_NONE),
		m_dTimeInstantDelta(0.01f),
		m_bIsTimeInstant(false),
		m_dActiveDistance(0.0f)
{
	this->Observe(m_pVisTiming);
	m_fCenter[0] = 0.0f;
	m_fCenter[1] = 0.0f;
	m_fCenter[2] = 0.0f;

	m_fColor[0] = 0.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 0.0f;
	m_fColor[3] = 1.0f;

}

VfaTimeBuoyModel::~VfaTimeBuoyModel()
{
	this->ReleaseObserveable (m_pVisTiming);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTransformMatrix                                          */
/*                                                                            */
/*============================================================================*/
VistaTransformMatrix VfaTimeBuoyModel::GetTransformMatrix() const
{
	VistaTransformMatrix matTrans(GetRotation(), GetTranslation());
	VistaTransformMatrix matScale(GetScale());
	return matScale*matTrans;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter(float fC[])                                       */
/*                                                                            */
/*============================================================================*/
bool VfaTimeBuoyModel::SetCenter(float fC[])
{
	m_fCenter[0] = fC[0];
	m_fCenter[1] = fC[1];
	m_fCenter[2] = fC[2];
	this->Notify(MSG_CENTER_CHANGE);
	
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter (const VistaVector3D &v3C)                       */
/*                                                                            */
/*============================================================================*/
bool VfaTimeBuoyModel::SetCenter(const VistaVector3D &v3C)
{
	float fC[3];
	v3C.GetValues(fC);
	return this->SetCenter(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter (float fC[3])							          */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyModel::GetCenter(float fC[3]) const
{
	fC[0] = m_fCenter[0];
	fC[1] = m_fCenter[1];
	fC[2] = m_fCenter[2];
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter (double dC[3])							          */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyModel::GetCenter(double dC[3]) const
{
	dC[0] = m_fCenter[0];
	dC[1] = m_fCenter[1];
	dC[2] = m_fCenter[2];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter(VistaVector3D &v3C)							  */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyModel::GetCenter(VistaVector3D &v3C) const
{
	v3C[0] = m_fCenter[0];
	v3C[1] = m_fCenter[1];
	v3C[2] = m_fCenter[2];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisCenter (VistaVector3D &v3C)					      */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyModel::GetVisCenter(VistaVector3D &v3C) const
{
	VistaTransformMatrix matT = this->GetTransformMatrix();
	VistaVector3D vecCenter;
	this->GetCenter(vecCenter);
	v3C = matT.Transform(vecCenter);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisCenter (double center[3])						      */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyModel::GetVisCenter(double center[3]) const
{
	VistaVector3D vec;
	this->GetVisCenter(vec);
	center[0] = vec[0];
	center[1] = vec[1];
	center[2] = vec[2];
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetHeight													  */
/*                                                                            */
/*============================================================================*/
bool VfaTimeBuoyModel::SetHeight(float f)
{
	m_fHeight = f;

	this->Notify(MSG_HEIGHT_CHANGE);
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetHeight()												  */
/*                                                                            */
/*============================================================================*/
float VfaTimeBuoyModel::GetHeight() const
{
	return m_fHeight;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRadius													  */
/*                                                                            */
/*============================================================================*/
bool VfaTimeBuoyModel::SetRadius(float r)
{
	m_fRadius = r;

	this->Notify(MSG_RADIUS_CHANGE);
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRadius()												  */
/*                                                                            */
/*============================================================================*/
float VfaTimeBuoyModel::GetRadius() const
{
	return m_fRadius;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTranslation                                          */
/*                                                                            */
/*============================================================================*/
bool VfaTimeBuoyModel::SetTranslation(const VistaVector3D &v3Translation)
{
	if (v3Translation != m_v3Translation)
	{
		m_v3Translation = v3Translation;
		this->Notify(MSG_CENTER_CHANGE);
		return true;
	}
	return false;
}
VistaVector3D VfaTimeBuoyModel::GetTranslation() const
{
	return m_v3Translation;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRotation                                             */
/*                                                                            */
/*============================================================================*/
bool VfaTimeBuoyModel::SetRotation(const VistaQuaternion &qRotation)
{
	if (qRotation != m_qRotation)
	{
		m_qRotation = qRotation;
		this->Notify(MSG_ORIENTATION_CHANGE);
		return true;
	}
	return false;
}
VistaQuaternion VfaTimeBuoyModel::GetRotation() const
{
	return m_qRotation;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetScale                                                */
/*                                                                            */
/*============================================================================*/
bool VfaTimeBuoyModel::SetScale(float fScale)
{
	if (fScale != m_fScale)
	{
		m_fScale = fScale;
		this->Notify();
		return true;
	}
	return false;
}
float VfaTimeBuoyModel::GetScale() const
{
	return m_fScale;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetColor			                                          */
/*                                                                            */
/*============================================================================*/

bool VfaTimeBuoyModel::SetColor (const float fColor[4])
{
	memcpy (m_fColor, fColor, sizeof(float)*4);
	Notify (MSG_COLOR_CHANGE);
	return true;
}
bool VfaTimeBuoyModel::GetColor (float fColor[4]) const
{
	memcpy (fColor, m_fColor, sizeof(float)*4);
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   TimeInstant		                                          */
/*                                                                            */
/*============================================================================*/

bool VfaTimeBuoyModel::SetTimeInstant (double dValue)
{
 	 m_dMinValue =  std::max (0.0, dValue-m_dTimeInstantDelta);
	 m_dMaxValue = std::min (1.0, dValue+m_dTimeInstantDelta);
	 m_bIsTimeInstant = true;
	
	 this->CheckIsActive();

	 return true;
}

double VfaTimeBuoyModel::GetTimeInstant () const
{
	double dTimeInstant = -1.0f;
	if (m_bIsTimeInstant)
	{
		if (m_dMaxValue==1.0f)
			dTimeInstant = m_dMinValue+m_dTimeInstantDelta;
		else 
			dTimeInstant = m_dMaxValue-m_dTimeInstantDelta;
	}

	return dTimeInstant;
}

double VfaTimeBuoyModel::GetDistanceToMarkedTimeInstant () const
{
	if (m_bIsTimeInstant)
		return abs (this->GetTimeInstant()-m_pVisTiming->GetVisualizationTime());
	else
	{
		return 	std::min (abs (m_dMinValue-m_pVisTiming->GetVisualizationTime()), abs(m_dMaxValue-m_pVisTiming->GetVisualizationTime()));
	}
}

bool VfaTimeBuoyModel::SetActiveDistance (double dValue)
{
	if ((dValue<0.0)||(dValue>1.0))
		return false;

	m_dActiveDistance = dValue;
	return true;
}

double VfaTimeBuoyModel::GetActiveDistance () const
{
	return m_dActiveDistance;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   TimeRange			                                          */
/*                                                                            */
/*============================================================================*/

bool VfaTimeBuoyModel::SetTimeRange (double dMinValue, double dMaxValue)
{
	 m_dMinValue =  dMinValue;
	 m_dMaxValue = dMaxValue;
	 m_bIsTimeInstant = false;

	this->CheckIsActive();

	 return true;
}

bool VfaTimeBuoyModel::GetTimeRange (double &dMinValue, double &dMaxValue) const
{
	 dMinValue = m_dMinValue;
	 dMaxValue = m_dMaxValue;
	 return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetState												  */
/*                                                                            */
/*============================================================================*/


bool VfaTimeBuoyModel::SetState (VfaTimeBuoyModel::eStates s)
{
	if (s==m_eState)
		return false;

	m_eState = s;

	if (m_eState==STATE_CHANGINGMARK)
		this->SetTimeInstant (m_pVisTiming->GetVisualizationTime());

	Notify (MSG_STATE_CHANGE);
	return true;
}

VfaTimeBuoyModel::eStates VfaTimeBuoyModel::GetState () const
{
	return m_eState;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SelectTimeBuoy											  */
/*                                                                            */
/*============================================================================*/

void VfaTimeBuoyModel::SelectTimeBuoy ()
{
	m_pVisTiming->SetAnimationPlaying(false);
	if (m_bIsTimeInstant)
		m_pVisTiming->SetVisualizationTime(m_dMinValue+(m_dMaxValue-m_dMinValue)/2);
	else
		m_pVisTiming->SetVisualizationTime(m_dMinValue);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CheckIsActive												  */
/*                                                                            */
/*============================================================================*/

bool VfaTimeBuoyModel::CheckIsActive ()
{
	// get current vis time
	double dCurrentValue = m_pVisTiming->GetVisualizationTime();

	bool bWithinRange = false;

	if (m_bIsTimeInstant)
	{
		if ((GetTimeInstant()-m_dActiveDistance<=dCurrentValue) && (dCurrentValue<=GetTimeInstant()+m_dActiveDistance))
		{
			bWithinRange = true;
		}
	}
	else
		if ((m_dMinValue-m_dActiveDistance<=dCurrentValue) && (dCurrentValue<=m_dMaxValue+m_dActiveDistance))
		{
			bWithinRange = true;
		}

	// if within range, set state to ACTIVE, to NONE else
	if (bWithinRange != (m_eState==STATE_ACTIVE))
	{
		if (bWithinRange)
			SetState (STATE_ACTIVE);
		else
			SetState (STATE_NONE);
	}

	return (GetState()==STATE_ACTIVE);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate											  */
/*                                                                            */
/*============================================================================*/

void VfaTimeBuoyModel::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	// intercept visibility change to turn text on or off
	if(msg == VflVisTiming::MSG_VISTIME_CHG)
	{
		// if this buoy is not currently marking its time value
		if (m_eState != STATE_CHANGINGMARK)
		{
			this->CheckIsActive();
		}
		else
		{
			// in state CHANGINGTIME, mark the current  time instant
			this->SetTimeInstant (m_pVisTiming->GetVisualizationTime());
		}
	}

	
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaTimeBuoyModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaTimeBuoyModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return (int) rBtList.size();
}


/*============================================================================*/
/*  END OF FILE "VtnTimeBuoyWidget.cpp"                                         */
/*============================================================================*/
