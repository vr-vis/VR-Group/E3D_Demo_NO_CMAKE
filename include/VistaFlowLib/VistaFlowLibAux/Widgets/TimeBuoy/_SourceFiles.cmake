

set( RelativeDir "./Widgets/TimeBuoy" )
set( RelativeSourceGroup "Source Files\\Widgets\\TimeBuoy" )

set( DirFiles
	VfaTimeBuoyController.cpp
	VfaTimeBuoyController.h
	VfaTimeBuoyFactory.cpp
	VfaTimeBuoyFactory.h
	VfaTimeBuoyModel.cpp
	VfaTimeBuoyModel.h
	VfaTimeBuoyTrajectoryMediator.cpp
	VfaTimeBuoyTrajectoryMediator.h
	VfaTimeBuoyVis.cpp
	VfaTimeBuoyVis.h
	VfaTimeBuoyWidget.cpp
	VfaTimeBuoyWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

