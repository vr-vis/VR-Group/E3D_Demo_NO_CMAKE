/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456799012345679901234567990123456799012345679901234567990123456799012345679*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VfaTimeBuoyFactory.h         RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  FlowLibAux                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1999-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - CVtnTimeBuoyFactory								                  */
/*        - CVtnCreateTimeBuoyAction                                          */
/*                                                                            */
/*============================================================================*/
#ifndef _VFATIMEBUOYFACTORY_H
#define _VFATIMEBUOYFACTORY_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaAspects/VistaExplicitCallbackInterface.h>

#include <vector>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaTimeBuoyWidget;
class VflRenderNode;
class VfaWidgetManager;
class VflVisTiming;
class VfaTrajectoryInfo;
class VfaTrajectoryDraggingController;
class VfaApplicationContextObject;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*
 * The Time Buoy Factory is a helper factory for quick creation of time buoys.
 * After the huge constructor with all necessary information and a time buoy prototype,
 * this prototype is cloned by CreateNewBuoy.
 *
 * The factory keeps a list of colors (currently hardcoded) in order to assign different colors
 * to each new time buoy.
 *
 * The factory also keeps track of created time buoy widgets and deregisters and deletes them
 * on destruction of the factory.
 *
 */
class VISTAFLOWLIBAUXAPI VfaTimeBuoyFactory
{
public:
	/**
	 * Register all needed class as well as a time buoy prototype for cloning.
	 */
	VfaTimeBuoyFactory(VflRenderNode* pRenderNode, 
						VfaWidgetManager * pWdgManager, 
						VflVisTiming* pVisTiming, 
						VfaTrajectoryInfo* pTrajectoryView, 
						VfaTrajectoryDraggingController *pTrajController, 
						VfaApplicationContextObject* pAppContext, 
						int iSensorSlot,
						VfaTimeBuoyWidget *pPrototype);
	virtual ~VfaTimeBuoyFactory();

	/*
	* CreateNewBuoy ... creates a new buoy. Surprise.
	* The buoy factory keeps track of all created time buoy widgets and deletes them on destruction.
	*
	* @return VfaTimeBuoyWidget*		pointer to newly created time buoy. Deletion is taken care of by the factory.
	*/
	VfaTimeBuoyWidget* CreateNewBuoy();

protected:

private:
	VfaTimeBuoyWidget*	m_pPrototype;
	VflRenderNode*		m_pRenderNode;
	VfaWidgetManager*	m_pWidgetManager;
	VflVisTiming*		m_pVisTiming;
	VfaTrajectoryInfo* m_pTrajectoryView;
	VfaTrajectoryDraggingController *m_pTrajController;

	VfaApplicationContextObject* m_pAppContext;
	int							m_iSensorSlot;

	// keep track of created time buoys
	std::vector<VfaTimeBuoyWidget*>	m_vecCreatedWidgets;

	// set of colors to assign to each new widget
	std::vector<VistaColor> m_vecColors;
	int							m_iCurrentColor;
	
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/**
 * Helper class that implements an action to create a new buoy given a factory.
 */
class CVtnCreateTimeBuoyAction : public IVistaExplicitCallbackInterface
{
public:
	CVtnCreateTimeBuoyAction(VfaTimeBuoyFactory* pFactory)
		:m_pFactory(pFactory)
	{
	}
	~CVtnCreateTimeBuoyAction(){}


	bool Do()
	{
		m_pFactory->CreateNewBuoy();
		return true;
	}
private:
	VfaTimeBuoyFactory* m_pFactory;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFATIMEBUOYFACTORY_H
