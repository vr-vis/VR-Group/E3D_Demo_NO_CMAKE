/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456799012345679901234567990123456799012345679901234567990123456799012345679*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VfaTimeBuoyVis.h             RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :	FlowLibAux                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1999-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VfaTimeBuoyVis                                                   */
/*                                                                            */
/*============================================================================*/
#ifndef _VFATIMEBUOYVIS_H
#define _VFATIMEBUOYVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaTimeBuoyModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * View part of the MVC-style time buoy widget.
 */
class VISTAFLOWLIBAUXAPI VfaTimeBuoyVis : public IVflRenderable
{
public: 
	VfaTimeBuoyVis(VfaTimeBuoyModel* pModel);
	virtual ~VfaTimeBuoyVis();


	virtual void DrawOpaque();

	void OnTimeSlotUpdate (const double dTime);

	virtual unsigned int GetRegistrationMode() const;


	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/**
	 * Internal property class for visual properties.
	 */
	class VISTAFLOWLIBAUXAPI CVtnTimeBuoyVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		CVtnTimeBuoyVisProps();
		~CVtnTimeBuoyVisProps();

		void SetColor(const VistaColor& color);
		VistaColor GetColor() const;
		void SetColor(float fColor[4]);
		void GetColor(float fColor[4]) const;

		void SetOpacity(float fOpacity);
		float GetOpacity() const;

		/**
		 * Method:      SetSwingFrequency
		 *
		 * The swing frequency determines the speed with which the buoy
		 * swings when active.
		 *
		 * @param       dFrequency
		 * @return      void
		 * @author      av006wo
		 * @date        2010/09/29
		 */
		void SetSwingFrequency(double dFrequency);
		double GetSwingFrequency() const;

		/**
		 * Method:      SetUpAndDownFrequency
		 *
		 * The up-and-down frequency determines the speed with which the buoy
		 * jumps up and down when selected. 
		 *
		 * @param       dFrequency
		 * @return      void
		 * @author      av006wo
		 * @date        2010/09/29
		 */
		void SetUpAndDownFrequency(double dFrequency);
		double GetUpAndDownFrequency() const;

		/**
		 * Method:      SetFlickerFrequency
		 *
		 * The flicker frequency determines the speed with which the buoy
		 * flickers when in time travel mode. 
		 *
		 * @param       dFrequency
		 * @return      void
		 * @author      av006wo
		 * @date        2010/09/29
		 */
		void SetFlickerFrequency(double dFrequency);
		double GetFlickerFrequency() const;

	private:
		float	m_fColor[4];

		double m_dSwingFrequency;
		double m_dUpAndDownFrequency;
		double m_dFlickerFrequency;

	};

	CVtnTimeBuoyVisProps *GetTimeBuoyProperties() const;

	bool SetSelected (bool bSelected);

protected:
	virtual VflRenderableProperties* CreateProperties() const;


private:
	VfaTimeBuoyModel* m_pModel;

	VistaVector3D vecCenter;
	float m_fCenter[3];
	float m_fRadius;
	float m_fHeight;

	bool m_bIsActive;
	float	m_fCurrentSwingAngle;
	float	m_fCurrentSwingAmplitude;
	double m_dLastSwing;

	bool m_bSelected;
	float	m_fCurrentUpAndDownHeight;
	double m_dLastUpAndDown;

	bool m_bTimeMoving;
	bool m_bCurrentFlickeringState;
	double m_dLastFlicker;

	GLUquadricObj *m_pPole;
	
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFATIMEBUOYVIS_H
