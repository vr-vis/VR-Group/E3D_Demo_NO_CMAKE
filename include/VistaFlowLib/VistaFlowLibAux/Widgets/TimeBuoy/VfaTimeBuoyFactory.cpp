/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456799012345679901234567990123456799012345679901234567990123456799012345679*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VfaTimeBuoyWidget.CPP        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  FlowLibAux                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1999-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
#include "VfaTimeBuoyFactory.h"


#include "VfaTimeBuoyWidget.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLibAux/Widgets/VfaWidgetManager.h>
#include <VistaFlowLibAux/Interaction/VfaApplicationContextObject.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTimeBuoyFactory::VfaTimeBuoyFactory(VflRenderNode* pRenderNode, VfaWidgetManager * pWdgManager, VflVisTiming* pVisTiming, 
										 VfaTrajectoryInfo* pTrajectoryView, VfaTrajectoryDraggingController *pTrajController, 
										 VfaApplicationContextObject* pAppContext, int iSensorSlot, VfaTimeBuoyWidget *pPrototype)
:m_pPrototype(pPrototype),
m_pRenderNode(pRenderNode),
m_pWidgetManager(pWdgManager),
m_pVisTiming(pVisTiming),
m_pTrajectoryView(pTrajectoryView),
m_pTrajController(pTrajController),
m_pAppContext(pAppContext),
m_iSensorSlot(iSensorSlot),
m_iCurrentColor(0)
{
	m_vecColors.push_back(VistaColor::DARK_GREEN);
	m_vecColors.push_back(VistaColor::CORAL);
	m_vecColors.push_back(VistaColor::GREEN_YELLOW);
	m_vecColors.push_back(VistaColor::AQUAMARINE);
	m_vecColors.push_back(VistaColor::BRICK_RED);
	m_vecColors.push_back(VistaColor::INDIGO);
	m_vecColors.push_back(VistaColor::PINK);	
	m_vecColors.push_back(VistaColor::BLUE);
	m_vecColors.push_back(VistaColor::PURPLE);
	m_vecColors.push_back(VistaColor::DARK_SALMON);
	m_vecColors.push_back(VistaColor::SKY_BLUE);
	m_vecColors.push_back(VistaColor::WHITE);	
}

VfaTimeBuoyFactory::~VfaTimeBuoyFactory()
{
	for (size_t i=0; i < m_vecCreatedWidgets.size(); ++i)
	{
		m_pWidgetManager->DeregisterWidget (m_vecCreatedWidgets[i]);
		delete m_vecCreatedWidgets[i];
		m_vecCreatedWidgets[i] = NULL;
	}
}




/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel                                                    */
/*                                                                            */
/*============================================================================*/
VfaTimeBuoyWidget* VfaTimeBuoyFactory::CreateNewBuoy()
{
	VfaTimeBuoyWidget* pNewBuoy = new VfaTimeBuoyWidget (m_pRenderNode, m_pVisTiming, m_pTrajectoryView, m_pTrajController);
	
	VfaTimeBuoyController *pNewBuoyController, *pPrototypeController;
	VfaTimeBuoyModel *pNewBuoyModel, *pPrototypeModel;
	VfaTimeBuoyVis *pNewBuoyView, *pPrototypeView;
	
	pNewBuoyController = static_cast<VfaTimeBuoyController*>(pNewBuoy->GetController());
	pPrototypeController = static_cast<VfaTimeBuoyController*>(m_pPrototype->GetController());

	pNewBuoyModel = static_cast<VfaTimeBuoyModel*>(pNewBuoy->GetModel());
	pPrototypeModel = static_cast<VfaTimeBuoyModel*>(m_pPrototype->GetModel());

	pNewBuoyView = static_cast<VfaTimeBuoyVis*>(pNewBuoy->GetView());
	pPrototypeView = static_cast<VfaTimeBuoyVis*>(m_pPrototype->GetView());


	VfaApplicationContextObject::sSensorFrame oSensorFrame;
	m_pAppContext->GetSensorFrame (m_iSensorSlot, m_pRenderNode, oSensorFrame);
	
	pNewBuoyModel->SetTranslation (oSensorFrame.v3Position);
	pNewBuoyModel->SetRadius (pPrototypeModel->GetRadius());
	pNewBuoyModel->SetHeight (pPrototypeModel->GetHeight());
	pNewBuoyModel->SetActiveDistance (pPrototypeModel->GetActiveDistance());
	
	pNewBuoyController->GetProperties()->SetHandleRadius (pPrototypeController->GetProperties()->GetHandleRadius());
	pNewBuoyController->GetProperties()->SetHandleRadius (pPrototypeController->GetProperties()->GetHandleRadius());

	pNewBuoyController->SetConnectionRadius (pPrototypeController->GetConnectionRadius());


	pNewBuoyView->GetTimeBuoyProperties ()->SetFlickerFrequency (pPrototypeView->GetTimeBuoyProperties()->GetFlickerFrequency());
	pNewBuoyView->GetTimeBuoyProperties ()->SetSwingFrequency (pPrototypeView->GetTimeBuoyProperties()->GetSwingFrequency());
	pNewBuoyView->GetTimeBuoyProperties ()->SetUpAndDownFrequency (pPrototypeView->GetTimeBuoyProperties()->GetUpAndDownFrequency());

	// set valid time range to current time instant +/- 0.01
	pNewBuoyModel->SetTimeInstant (m_pRenderNode->GetVisTiming()->GetVisualizationTime());

	// choose one of the colors from the list
	float fColor [4] = {0.0,0.0,1.0,1.0};
	m_vecColors[m_iCurrentColor].GetValues(fColor);
	fColor[3] = 1.0f;
	pNewBuoyModel->SetColor (fColor);

	// fast forward within 1s
	m_pWidgetManager->RegisterWidget (pNewBuoy);

	m_vecCreatedWidgets.push_back(pNewBuoy);

	// increase color
	m_iCurrentColor++;
	if (m_iCurrentColor>=static_cast<int>(m_vecColors.size()))
		m_iCurrentColor = 0;
	return pNewBuoy;

}

/*============================================================================*/
/*  END OF FILE "VtnTimeBuoyTrajectoryMediator.cpp"                           */
/*============================================================================*/
