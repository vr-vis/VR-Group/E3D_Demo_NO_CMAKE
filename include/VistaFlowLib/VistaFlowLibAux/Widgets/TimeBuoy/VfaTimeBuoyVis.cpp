/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// Hint: Always include glew _before_ glut!
#include <GL/glew.h>
#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #if defined(USE_NATIVE_GLUT)
    #include <GL/glut.h>
  #else
    #include <GL/freeglut.h>
  #endif
#endif

#include "VfaTimeBuoyVis.h"
#include "VfaTimeBuoyModel.h"
#include "VfaTimeBuoyWidget.h"

#include <VistaAspects/VistaPropertyFunctor.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <cassert>
#include <cstring>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaTimeBuoyVis                                       */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTimeBuoyVis::VfaTimeBuoyVis(VfaTimeBuoyModel* pModel)
:m_bIsActive(false),
m_bSelected(false),
m_bTimeMoving(false),
m_pModel (pModel),
m_fCurrentSwingAngle(0.0f),
m_fCurrentSwingAmplitude(0.5),
m_dLastSwing(-1.0),
m_fCurrentUpAndDownHeight(0.0f),
m_dLastUpAndDown(-1.0),
m_bCurrentFlickeringState(false),
m_dLastFlicker(-1.0)
{
	m_pPole = gluNewQuadric();

}

VfaTimeBuoyVis::~VfaTimeBuoyVis()
{
	gluDeleteQuadric(m_pPole);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotUpdate                                            */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyVis::OnTimeSlotUpdate (const double dTime)
{
	if (m_bTimeMoving)
	{
		if (dTime > m_dLastFlicker+this->GetTimeBuoyProperties()->GetFlickerFrequency())
		{
			m_bCurrentFlickeringState = !m_bCurrentFlickeringState;
			m_dLastFlicker = dTime;
			//vstr::debugi() << "Flicker "<<m_bCurrentFlickeringState<<std::endl;
		}
	}
	

	if (m_bSelected)
	{
		float fFactor = static_cast<float>(
			(dTime-m_dLastUpAndDown)/this->GetTimeBuoyProperties()->GetUpAndDownFrequency());
		if (fFactor>1)
		{
			m_dLastUpAndDown = dTime;
			fFactor = 0.0;
		}

		m_fCurrentUpAndDownHeight = m_fHeight * 0.1f * sin(fFactor * 2.0f * Vista::Pi);
		//vstr::debugi() << "Up and down "<<m_fCurrentUpAndDownHeight<<std::endl;
	}
	

	if (m_bIsActive)
	{
		//float fSpeedFactor = 1+std::max<float> (0.0, log(std::max<float>(0.00000000000001, 1-(m_pModel->GetDistanceToMarkedTimeInstant()/m_pModel->GetActiveDistance())))/log(0.5));
		m_fCurrentSwingAmplitude = 0.5f*(1.0f-static_cast<float>(
			std::min<double>(1.0, m_pModel->GetDistanceToMarkedTimeInstant()/m_pModel->GetActiveDistance())));
		double dFactor = (dTime-m_dLastSwing)/(this->GetTimeBuoyProperties()->GetSwingFrequency());
		if (dFactor > 1.0)
		{
			m_dLastSwing = dTime;
			dFactor = 0.0;
		}

		//VistaVector3D((1.0f-m_pModel->GetDistanceToMarkedTimeInstant())*0.5*sin(m_iActiveCounter%iSubdivisions*(Vista::Pi/8)),1,(1.0f-m_pModel->GetDistanceToMarkedTimeInstant())*0.5*cos(m_iActiveCounter%iSubdivisions*(Vista::Pi/8))));

		m_fCurrentSwingAngle = static_cast<float>(dFactor) * 2.0f * Vista::Pi;
		//vstr::debugi() << "Swing angle "<<m_fCurrentSwingAngle<<" amplitude "<<m_fCurrentSwingAmplitude<<std::endl;
	}
	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyVis::DrawOpaque()
{
	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_ENABLE_BIT|GL_LIGHTING_BIT|GL_POLYGON_BIT);
	glPushMatrix();

	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	float fColor[4];
	this->GetTimeBuoyProperties()->GetColor().GetValues(fColor);
	fColor[3] = this->GetTimeBuoyProperties()->GetOpacity();

	// flicker transparent
	//if (m_bTimeMoving && ((m_iActiveCounter/10)%2 == 0))
	if (m_bCurrentFlickeringState)
	{
		fColor[3] = fColor[3]*0.1f;
	}
	GLfloat Ambient[] = {0.2f,0.2f,0.2f,1.0f};
	GLfloat Specular[] = {1.0f,1.0f,1.0f,1.0f};
	glDisable(GL_COLOR_MATERIAL);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,Ambient);
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,fColor);
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,Specular);
	glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0);

	//float fAnimationYOffset =m_bSelected?(m_fHeight*0.1*sin(m_iActiveCounter*Vista::Pi/8)):0.0f;
	glTranslatef(m_fCenter[0], m_fCenter[1]+m_fCurrentUpAndDownHeight, m_fCenter[2]);

	// rotate pole to (0,1,0) (up Y)
	VistaQuaternion qRotateUp (VistaVector3D(0,0,1), VistaVector3D(0,1,0));
	VistaAxisAndAngle a = qRotateUp.GetAxisAndAngle();
	glRotatef(a.m_fAngle*180.0f/Vista::Pi, a.m_v3Axis[0], a.m_v3Axis[1], a.m_v3Axis[2]);

	// sizes for banner and pole
	float fBannerWidth = m_fHeight/5;
	float fBannerSinus = m_fHeight/20;
	int iSubdivisions = 16;
	float fBannerLength = (float) (m_fHeight/2)/(float) iSubdivisions;

	// rotate around pole if active
	if (m_bIsActive)
	{
		//VistaQuaternion qRotateAround (VistaVector3D(0,1,0), VistaVector3D((1.0f-m_pModel->GetDistanceToMarkedTimeInstant())*0.5*sin(m_iActiveCounter%iSubdivisions*(Vista::Pi/8)),1,(1.0f-m_pModel->GetDistanceToMarkedTimeInstant())*0.5*cos(m_iActiveCounter%iSubdivisions*(Vista::Pi/8))));
		VistaQuaternion qRotateAround (VistaVector3D(0,1,0), VistaVector3D(m_fCurrentSwingAmplitude*sin(m_fCurrentSwingAngle),1,m_fCurrentSwingAmplitude*cos(m_fCurrentSwingAngle)));
		VistaAxisAndAngle b = qRotateAround.GetAxisAndAngle();
		glRotatef(b.m_fAngle*180.0f/Vista::Pi, b.m_v3Axis[0], b.m_v3Axis[1], b.m_v3Axis[2]);
	}

	// draw pole
	gluQuadricNormals(m_pPole, GLU_SMOOTH);
	gluQuadricTexture(m_pPole, GL_TRUE);
	gluCylinder(m_pPole, m_fRadius, m_fRadius, m_fHeight, 16, 16);
		
	// draw banner
	glBegin(GL_TRIANGLE_STRIP);
	for (int i=0; i < iSubdivisions; ++i)
	{
		//glVertex3f(fBannerSinus*sin(i*(Vista::Pi/8)-(m_iActiveCounter%iSubdivisions)), -fBannerLength*i, m_fHeight-fBannerWidth);  
		//glVertex3f(fBannerSinus*sin(i*(Vista::Pi/8)-(m_iActiveCounter%iSubdivisions)), -fBannerLength*i, m_fHeight);  
		glVertex3f(fBannerSinus*sin(i*(Vista::Pi/8)), -fBannerLength*i, m_fHeight-fBannerWidth);  
		glVertex3f(fBannerSinus*sin(i*(Vista::Pi/8)), -fBannerLength*i, m_fHeight);  
	}

	glEnd();


	glPopMatrix();
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaTimeBuoyVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyVis::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaTimeBuoyModel *pTimeBuoyModel = dynamic_cast<VfaTimeBuoyModel*>(pObserveable);

	if(!pTimeBuoyModel)
		return;


	// get position and sizes
	pTimeBuoyModel->GetVisCenter(vecCenter);
	m_fRadius = pTimeBuoyModel->GetRadius();
	m_fHeight = pTimeBuoyModel->GetHeight();

	// get color
	float fColor[4];
	pTimeBuoyModel->GetColor (fColor);
	VistaColor color (fColor);
	this->GetTimeBuoyProperties()->SetColor (color);
	this->GetTimeBuoyProperties()->SetOpacity(fColor[3]);

	vecCenter.GetValues(m_fCenter);

	// get status
	bool bIsActive = (pTimeBuoyModel->GetState()== VfaTimeBuoyModel::STATE_ACTIVE);
	if (m_bIsActive != bIsActive)
	{
		m_bIsActive = bIsActive;
		if (!m_bIsActive)
		{
			//vstr::debugi() << "Swing off "<<std::endl;
			m_fCurrentSwingAngle = 0.0f;
			m_dLastSwing = -1.0;
		}

	}

	// get time travel status
	bool bTimeMoving = (pTimeBuoyModel->GetState()== VfaTimeBuoyModel::STATE_CHANGINGMARK);
	if (m_bTimeMoving != bTimeMoving)
	{
		m_bTimeMoving = bTimeMoving;
		if (!m_bTimeMoving)
		{
			//vstr::debugi() << "Flicker off "<<std::endl;
			m_bCurrentFlickeringState = false;
			m_dLastFlicker = -1.0;
		}
	}	
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaTimeBuoyVis::CVtnTimeBuoyVisProps *VfaTimeBuoyVis::GetTimeBuoyProperties() const
{
	return static_cast<VfaTimeBuoyVis::CVtnTimeBuoyVisProps *>(GetProperties());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaTimeBuoyVis::CreateProperties() const
{
	return new VfaTimeBuoyVis::CVtnTimeBuoyVisProps();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetSelected		                                          */
/*                                                                            */
/*============================================================================*/
bool VfaTimeBuoyVis::SetSelected (bool bSelected)
{
	m_bSelected = bSelected;
	if (!m_bSelected)
	{
		vstr::outi() << "[VfaTimeBuoyVis] Up and down off "<<std::endl;
		m_fCurrentUpAndDownHeight = 0.0f;
		m_dLastUpAndDown = -1.0;
	}

	return true;
}


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTimeBuoyVis::CVtnTimeBuoyVisProps::CVtnTimeBuoyVisProps()
:m_dSwingFrequency (1.0),
m_dUpAndDownFrequency(1.0),
m_dFlickerFrequency(1.0)

{
	//blue border
	m_fColor[0] = 0.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 1.0f;
	m_fColor[3] = 1.0f;
}
VfaTimeBuoyVis::CVtnTimeBuoyVisProps::~CVtnTimeBuoyVisProps()
{

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetColor                                                */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyVis::CVtnTimeBuoyVisProps::SetColor(const VistaColor& color)
{
	color.GetValues(m_fColor);
}
void VfaTimeBuoyVis::CVtnTimeBuoyVisProps::SetColor(float fColor[4])
{
	memcpy(m_fColor, fColor, 4*sizeof(float));
}

VistaColor VfaTimeBuoyVis::CVtnTimeBuoyVisProps::GetColor() const
{
	return VistaColor(m_fColor);
}
void VfaTimeBuoyVis::CVtnTimeBuoyVisProps::GetColor(float fColor[4]) const
{
	memcpy(fColor, m_fColor, 4*sizeof(float));
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetOpacity                                              */
/*                                                                            */
/*============================================================================*/

void VfaTimeBuoyVis::CVtnTimeBuoyVisProps::SetOpacity(float fOpacity)
{
	m_fColor[3] = fOpacity;
}

float VfaTimeBuoyVis::CVtnTimeBuoyVisProps::GetOpacity() const
{
	return m_fColor[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFlickerFrequency                                     */
/*                                                                            */
/*============================================================================*/

void VfaTimeBuoyVis::CVtnTimeBuoyVisProps::SetFlickerFrequency(double dFrequency)
{
	m_dFlickerFrequency = dFrequency;
}

double VfaTimeBuoyVis::CVtnTimeBuoyVisProps::GetFlickerFrequency() const
{
	return m_dFlickerFrequency;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetUpAndDownFrequency                                   */
/*                                                                            */
/*============================================================================*/

void VfaTimeBuoyVis::CVtnTimeBuoyVisProps::SetUpAndDownFrequency(double dFrequency)
{
	m_dUpAndDownFrequency = dFrequency;
}

double VfaTimeBuoyVis::CVtnTimeBuoyVisProps::GetUpAndDownFrequency() const
{
	return m_dUpAndDownFrequency;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetSwingFrequency                                       */
/*                                                                            */
/*============================================================================*/

void VfaTimeBuoyVis::CVtnTimeBuoyVisProps::SetSwingFrequency(double dFrequency)
{
	m_dSwingFrequency = dFrequency;
}

double VfaTimeBuoyVis::CVtnTimeBuoyVisProps::GetSwingFrequency() const
{
	return m_dSwingFrequency;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VtnTimeBuoyVis.cpp"                                          */
/*============================================================================*/

