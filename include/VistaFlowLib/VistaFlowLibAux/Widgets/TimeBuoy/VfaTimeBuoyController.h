/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFATIMEBUOYCONTROLLER_H
#define _VFATIMEBUOYCONTROLLER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>

#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaReflectionable.h>
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaCenterControlHandle;
class VistaColor;
class VistaIndirectXform;
class VfaTimeBuoyModel;
class VfaTimeBuoyVis;
class VflRenderNode;
class Vfl3DTextLabel;
class VfaTrajectoryInfo;
class VfaTrajectoryDraggingController;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Controller part of the MVC widget
 */
class VISTAFLOWLIBAUXAPI VfaTimeBuoyController : public VflObserver, public IVfaWidgetController
{
public:
	VfaTimeBuoyController(VfaTimeBuoyModel *pModel, VfaTimeBuoyVis* pBuoyVis, VfaTrajectoryInfo* pTrajectoryView, VfaTrajectoryDraggingController *pTrajController, VflRenderNode *pRenderNode);
	virtual ~VfaTimeBuoyController();

	/** 
	 * This is called on transition change into (out of) state FOCUS
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();

	/**
	 * This is called on transition change into (out of) state TOUCH
	 */
	void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	void OnUntouch();

	/**
	 * These are called on update of all registered (sensor & command) slots
	 */
	void OnSensorSlotUpdate (int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate (int iSlot, const bool bSet);
	void OnTimeSlotUpdate (const double dTime);
	/**
	 * return an (or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	
	/**
	 * Method:      SetConnectionRadius
	 *
	 * Attach to the active trajectory if within connection radius
	 *
	 * @param       fRadius
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void SetConnectionRadius (float fRadius);
	float GetConnectionRadius () const;

// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);


	/**
	* Internal class for visual properties
	*/
	class VISTAFLOWLIBAUXAPI CVtnTimeBuoyControllerProps : public IVistaReflectionable
	{
		friend class VfaTimeBuoyController;
	public:
		
		CVtnTimeBuoyControllerProps(VfaTimeBuoyController *pBuoyCtrl);
		virtual ~CVtnTimeBuoyControllerProps();

		bool SetHandleRadius(float fRadius);
		float GetHandleRadius() const;

		void SetHandleNormalColor(const VistaColor& color);
		VistaColor GetHandleNormalColor() const;

		void SetHandleHighlightColor(const VistaColor& color);
		VistaColor GetHandleHighlightColor() const;


		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;


	private:
		VfaTimeBuoyController	*m_pTimeBuoyCtrl;

	};


	CVtnTimeBuoyControllerProps* GetProperties() const;

protected:


private:
	enum eTimeBuoyState {
		SCS_NONE,    /**< no specified state, starting state*/
		SCS_MOVE,    /**< move handle is used*/
		SCS_SELECT,  /**< select time instant handle is used*/
		SCS_DOCKING  /**< docking handle is used*/
	};

	VfaTimeBuoyController::eTimeBuoyState m_iCurrentInternalState;

	/** 
	 * m_bTimeMoveActive is true if the time buoy moves in time, i.e. the current time is continuously stored
	 * in the model's time instant.
	 */
	bool								   m_bTimeMoveActive;	

	VfaTimeBuoyModel				*m_pModel;
	VfaTimeBuoyVis					*m_pBuoyVis;

	// defined command slots
	int								m_iGrabButton;
	int								m_iTimeChangeButton;

	enum eHandles { INVALID_HANDLE = -1, HANDLE_MOVE = 0, HANDLE_SELECT, HANDLE_DOCKING };
	vector<IVfaCenterControlHandle*> m_vecHandles;
	VistaIndirectXform*             m_pXform;

	CVtnTimeBuoyControllerProps*     m_pCtlProps;

	// focused handle and its number
	IVfaCenterControlHandle*		m_pFocusHandle;
	int							    m_iHandle;

	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;

	bool						m_bEnabled;
	float						m_fCurrentVisTime;

	Vfl3DTextLabel *           m_pInfoText;
	float                       m_fInfoTextOffset[3];

	//@{
	/**
	 * These are needed to enable docking of the buoy to the active trajectory,
	 * which is provided by VfaTrajectoryDraggingController and shown by VfaTrajectoryInfo.
	 */
	VfaTrajectoryInfo*         m_pTrajectoryView;
	VfaTrajectoryDraggingController *m_pTrajectoryController;
	float				        m_fConnectionRadius;
	VistaVector3D		        m_v3LastTrajectoryPoint;
	//@}

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFATIMEBUOYCONTROLLER_H
