/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456799012345679901234567990123456799012345679901234567990123456799012345679*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VfaTimeBuoyWidget.CPP        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  FlowLibAux                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1999-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
#include "VfaTimeBuoyWidget.h"
#include "VfaTimeBuoyModel.h"
#include "VfaTimeBuoyVis.h"
#include "VfaTimeBuoyController.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLibAux/Widgets/VfaSphereHandle.h>

#include <VistaAspects/VistaObserver.h>
#include <VistaAspects/VistaPropertyAwareable.h>

#include <VistaBase/VistaVectorMath.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTimeBuoyWidget::VfaTimeBuoyWidget(VflRenderNode *pRenderNode, VflVisTiming *pVisTiming, VfaTrajectoryInfo* pTrajectoryView, VfaTrajectoryDraggingController *pTrajController)
: IVfaWidget(pRenderNode),
m_bEnabled(true)
{
	m_pTimeBuoyModel = new VfaTimeBuoyModel (pVisTiming);


	m_pTimeBuoyView = new VfaTimeBuoyVis (m_pTimeBuoyModel);
	if( m_pTimeBuoyView->Init())
		m_pRenderNode->AddRenderable(m_pTimeBuoyView);


	m_pTimeBuoyController = new VfaTimeBuoyController(m_pTimeBuoyModel, m_pTimeBuoyView, pTrajectoryView, pTrajController, m_pRenderNode);


	m_pTimeBuoyView->Observe(GetModel());
	m_pTimeBuoyController->Observe(GetModel());
	
	this->GetModel()->Notify();
}

VfaTimeBuoyWidget::~VfaTimeBuoyWidget()
{
	delete m_pTimeBuoyController;
	delete m_pTimeBuoyView;
	delete m_pTimeBuoyModel;
}




void VfaTimeBuoyWidget::SetIsVisible(bool b){
	m_pTimeBuoyView->SetVisible(b);
}

bool VfaTimeBuoyWidget::GetIsVisible() const{
	return m_pTimeBuoyView->GetVisible();
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyWidget::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pTimeBuoyView->SetVisible(m_bEnabled);
	m_pTimeBuoyController->SetIsEnabled (m_bEnabled);
}
bool VfaTimeBuoyWidget::GetIsEnabled() const
{
	return m_bEnabled;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel                                                    */
/*                                                                            */
/*============================================================================*/
IVistaReflectionable* VfaTimeBuoyWidget::GetModel() const
{
	return m_pTimeBuoyModel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetView                                                     */
/*                                                                            */
/*============================================================================*/
IVflRenderable* VfaTimeBuoyWidget::GetView() const
{
	return m_pTimeBuoyView;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetController                                               */
/*                                                                            */
/*============================================================================*/
IVfaWidgetController* VfaTimeBuoyWidget::GetController() const
{
	return m_pTimeBuoyController;
}
/*============================================================================*/
/*  END OF FILE "VtnTimeBuoyWidget.cpp"                                       */
/*============================================================================*/
