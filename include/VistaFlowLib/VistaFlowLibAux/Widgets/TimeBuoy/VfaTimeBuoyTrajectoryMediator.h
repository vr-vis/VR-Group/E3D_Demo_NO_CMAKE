/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456799012345679901234567990123456799012345679901234567990123456799012345679*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VfaTimeBuoyTrajectoryMediator.h          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  FlowLibAux                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1999-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VfaTimeBuoyTrajectoryMediator                                    */
/*                                                                            */
/*============================================================================*/
#ifndef __VFATIMEBUOYTRAJECTORYMEDIATOR_H
#define __VFATIMEBUOYTRAJECTORYMEDIATOR_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Data/VflObserver.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaTimeBuoyModel;
class VfaParticleHandleVis;
class VfaTrajectoryDraggingController;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*
 * A VfaTimeBuoyTrajectoryMediator observes a TimeBuoy (more precisely: its model).
 * Whenever the buoy comes close (within ConnectionRadius) to the currently selected trajectory
 * of the VfaTrajectoryDraggingController, the buoy and the trajectory are visually connected
 * using a VfaParticleHandleVis, and the time buoy marks the time value of the nearest spatio-temporal point.
 * 
 *
 */
class VfaTimeBuoyTrajectoryMediator : public VflObserver
{
public:
	VfaTimeBuoyTrajectoryMediator(VflRenderNode *pRN, VfaTrajectoryDraggingController *pTrajController, VfaTimeBuoyModel* pBuoyModel );
	virtual ~VfaTimeBuoyTrajectoryMediator();

	VfaTimeBuoyModel* GetModel() const;

	VfaParticleHandleVis* GetView() const;

	VfaTrajectoryDraggingController* GetController() const;

	void SetConnectionRadius (float fRadius);

	// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

protected:

private:
	VfaTimeBuoyModel		*m_pTimeBuoyModel;
	VfaParticleHandleVis			*m_pConnectionView;
	VfaTrajectoryDraggingController	*m_pTrajController;

	float				m_fConnectionRadius;
	
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // __VFATIMEBUOYWIDGET_H
