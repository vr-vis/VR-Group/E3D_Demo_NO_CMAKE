/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#include "VfaTimeBuoyController.h"
#include "VfaTimeBuoyModel.h"
#include "VfaTimeBuoyWidget.h"
#include "../TrajectoryDragging/VfaParticleHandleVis.h"
#include "../TrajectoryDragging/VfaTrajectoryDraggingController.h"
#include <VistaFlowLibAux/Widgets/VfaSphereHandle.h>
#include <VistaFlowLibAux/Widgets/VfaWidgetTools.h>

#include <VistaKernel/InteractionManager/VistaIntentionSelect.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <VistaMath/VistaIndirectXform.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>

#include <algorithm>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTimeBuoyController::VfaTimeBuoyController(VfaTimeBuoyModel *pModel, VfaTimeBuoyVis* pBuoyVis, VfaTrajectoryInfo* pTrajectoryView, VfaTrajectoryDraggingController *pTrajController, VflRenderNode *pRenderNode)
: IVfaWidgetController(pRenderNode),
  m_pModel(pModel),
  m_pBuoyVis(pBuoyVis),
  m_iGrabButton(0),
  m_iTimeChangeButton(-1),
  m_pFocusHandle(NULL),
  m_pXform(new VistaIndirectXform),
  m_iCurrentInternalState(SCS_NONE),
  m_bEnabled(true),
  m_bTimeMoveActive(false),
  m_pTrajectoryView(pTrajectoryView),
  m_pTrajectoryController(pTrajController),
  m_fConnectionRadius(0.0f)
{
	m_pCtlProps = new VfaTimeBuoyController::CVtnTimeBuoyControllerProps(this);
	
	// we have three handles, HANDLE_MOVE (at base), HANDLE_SELECT (at banner), HANDLE_DOCKING
	m_vecHandles.resize(3);
	
	m_vecHandles[HANDLE_MOVE] = new VfaSphereHandle(pRenderNode);
	this->AddControlHandle (m_vecHandles[0]);

	// banner handle should not be visible
	VfaSphereHandle* pBannerHandle = new VfaSphereHandle(pRenderNode);
	this->AddControlHandle (pBannerHandle);
	pBannerHandle->GetSphereVis()->SetVisible(false);
	VistaVector3D v3Center;
	m_pModel->GetCenter(v3Center);
	v3Center[2]+=m_pModel->GetHeight()/2;
	v3Center[1]+=m_pModel->GetHeight()*9/10;
	pBannerHandle->SetCenter (v3Center);
	m_vecHandles[HANDLE_SELECT] = pBannerHandle;

	m_vecHandles[HANDLE_DOCKING] = new VfaParticleHandle (pRenderNode);
	//this->AddControlHandle (m_vecHandles[2]);


	// info text
	m_pInfoText = new Vfl3DTextLabel;
	m_pInfoText->Init();
	pRenderNode->AddRenderable(m_pInfoText);
	m_pInfoText->SetText("");
	float fToolColor[4] = {1.0f, 1.0f, 0.4f, 1.0f};
	m_pInfoText->SetColor(fToolColor);
	m_pInfoText->SetTextSize(m_pModel->GetHeight()*0.15f);
	m_fInfoTextOffset[0] = 0.0f;
	m_fInfoTextOffset[1] = m_pModel->GetHeight()*1.6f;
	m_fInfoTextOffset[2] = 0.0f;
	m_pInfoText->SetPosition(m_fInfoTextOffset);

}

VfaTimeBuoyController::~VfaTimeBuoyController()
{
	delete m_pCtlProps;
	delete m_pXform;

	//info text
	this->GetRenderNode()->RemoveRenderable(m_pInfoText);
	delete m_pInfoText;

	for(size_t i=0; i<m_vecHandles.size(); ++i)
	{
		delete m_vecHandles[i];
	}

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	for(size_t i=0; i<m_vecHandles.size(); ++i)
	{
		m_vecHandles[i]->SetVisible(m_bEnabled);
		m_vecHandles[i]->SetIsSelectionEnabled(m_bEnabled);
	}
	// always make sphere of HANDLE_SELECT invisible
	static_cast<VfaSphereHandle*> (m_vecHandles[HANDLE_SELECT])->GetSphereVis()->SetVisible(false);
	m_pInfoText->SetVisible (m_bEnabled);

}
bool VfaTimeBuoyController::GetIsEnabled() const
{
	return m_bEnabled;
}

void VfaTimeBuoyController::SetConnectionRadius (float fRadius)
{
	m_fConnectionRadius = fRadius;
}

float VfaTimeBuoyController::GetConnectionRadius () const
{
	return m_fConnectionRadius;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnFocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::OnFocus(IVfaControlHandle* pHandle)
{
	// don't allow to change focus if some handle already has it
	if (m_iCurrentInternalState == SCS_NONE)
	{
		m_pFocusHandle = static_cast<IVfaCenterControlHandle*>(pHandle);

		// search focused handle in list
		m_iHandle = (int) (find(m_vecHandles.begin(), m_vecHandles.end(), m_pFocusHandle) - m_vecHandles.begin());

		if (m_iHandle < HANDLE_DOCKING)
			m_pFocusHandle->SetIsHighlighted(true);
		if (m_iHandle==HANDLE_SELECT)
		{
			m_pBuoyVis->SetSelected(true);
			VistaVector3D v3Center;
			m_pModel->GetVisCenter(v3Center);
			m_pInfoText->SetText("vis time "+VistaAspectsConversionStuff::ConvertToString (m_pModel->GetTimeInstant())+"\n"
				+"position "+VistaAspectsConversionStuff::ConvertToString (v3Center[0])+", "+
				VistaAspectsConversionStuff::ConvertToString (v3Center[1])+", "+
				VistaAspectsConversionStuff::ConvertToString (v3Center[2]));

		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUnfocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::OnUnfocus()
{
	// don't allow to change focus if some handle already has it
	if (m_iCurrentInternalState == SCS_NONE)
	{
		if (m_pFocusHandle && (m_iHandle < HANDLE_DOCKING))
			m_pFocusHandle->SetIsHighlighted(false);
		if (m_iHandle==HANDLE_SELECT)
		{
			m_pBuoyVis->SetSelected(false);
		}
		m_iHandle = INVALID_HANDLE;
		if (!m_bTimeMoveActive)
			m_pInfoText->SetText("");		
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUntouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::OnUntouch()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::OnSensorSlotUpdate (int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData)
{

	if (iSlot == VfaApplicationContextObject::SLOT_POINTER_VIS)
	{
		// save sensor data
		m_oLastSensorFrame = oFrameData;

		// center was grabbed --> move widget
		if (m_iCurrentInternalState == SCS_MOVE)
		{
			VistaVector3D v3MyPos;  // vis space
			VistaQuaternion qMyOri; // vis space
			m_pXform->Update(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation, v3MyPos, qMyOri);

			// don't change center here, because "only"
			// the widget space is translated/rotated here
			m_pModel->SetTranslation(v3MyPos);
			m_pModel->SetRotation(qMyOri);

		}
		if (m_iCurrentInternalState == SCS_DOCKING)
		{
			VistaVector3D v3MyPos;  // vis space
			VistaQuaternion qMyOri; // vis space
			m_pXform->Update(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation, v3MyPos, qMyOri);
			//static_cast<VfaParticleHandle*> (m_vecHandles[HANDLE_DOCKING])->SetCenter (v3MyPos);
			float fPosition[3];
			v3MyPos.GetValues(fPosition);
			float fNearestTrajectoryPosition[3];
			float fTimeValue;


			if(!m_pTrajectoryController)
				return;

			if (m_pTrajectoryController->GetClosestTrajectoryPoint (fPosition, fNearestTrajectoryPosition, fTimeValue))
			{
				static_cast<VfaParticleHandle*> (m_vecHandles[HANDLE_DOCKING])->SetCenter (VistaVector3D(fNearestTrajectoryPosition));
				m_pModel->SetTimeInstant(fTimeValue);
			}

		}

		
	}

	
	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::OnCommandSlotUpdate (int iSlot, const bool bSet)
{

	if (bSet)
	{
		// we can only be grabbed, if we have the focus
		if (this->GetControllerState() == CS_FOCUS)
		{
			// if we are currently in state TIMEMOVE, exit this state (i.e., we leave this state with every button press)
			if ((m_iHandle == HANDLE_MOVE) && (m_bTimeMoveActive))
			{
				m_pModel->SetState (VfaTimeBuoyModel::STATE_NONE);
				m_bTimeMoveActive = false;
				m_pInfoText->SetText("");
			}

			if (m_iCurrentInternalState == SCS_NONE)
			{
				// what handle is focused?
				if(m_iHandle == HANDLE_MOVE)
				{
					// grab button indicates we want to move the spatial position of the buoy
					if (iSlot == m_iGrabButton)
					{
						m_iCurrentInternalState = SCS_MOVE;
						m_pXform->Init(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation,	m_pModel->GetTranslation(), m_pModel->GetRotation());
						m_pInfoText->SetText("Move in space");
					}

					// time change button indicates we want to move the temporal position of the buoy
					if (iSlot == m_iTimeChangeButton)
					{
						m_bTimeMoveActive = true;
						m_pModel->SetState (VfaTimeBuoyModel::STATE_CHANGINGMARK);
						m_pInfoText->SetText("Move in time");
					}
				}
				// select buoy, that is "fly" to marked time instant
				if(m_iHandle == HANDLE_SELECT)
				{
					m_iCurrentInternalState = SCS_SELECT;
					m_pModel->SelectTimeBuoy();

				}
				// select docking point
				if(m_iHandle == HANDLE_DOCKING)
				{
					// grab button indicates we want to move the spatial position of the buoy
					if (iSlot == m_iGrabButton)
					{
						m_iCurrentInternalState = SCS_DOCKING;
						m_pXform->Init(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation,	m_pModel->GetTranslation(), m_pModel->GetRotation());
					}
				}
			}
			
		}
	}
	else
	{
		// if the slot is released, goto state NONE
		m_iCurrentInternalState = SCS_NONE;

		// and if we have lost focus during other states, now react on that
		if (this->GetControllerState()!=CS_FOCUS)
		{
			if (m_pFocusHandle && (m_iHandle < HANDLE_DOCKING))
				m_pFocusHandle->SetIsHighlighted(false);
			if (m_iHandle==HANDLE_SELECT)
			{
				m_pBuoyVis->SetSelected(false);
			}
			m_iHandle = INVALID_HANDLE;
		if (!m_bTimeMoveActive)
			m_pInfoText->SetText("");		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::OnTimeSlotUpdate (const double dTime)
{
	m_pBuoyVis->OnTimeSlotUpdate (dTime);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSensorMask			                                      */
/*                                                                            */
/*============================================================================*/
int VfaTimeBuoyController::GetSensorMask() const
{
	return (1 << VfaApplicationContextObject::SLOT_POINTER_VIS);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCommandMask			                                  */
/*                                                                            */
/*============================================================================*/
int VfaTimeBuoyController::GetCommandMask() const
{
	return (1 << m_iGrabButton) | (1 << m_iTimeChangeButton);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeUpdate				                                  */
/*                                                                            */
/*============================================================================*/
bool VfaTimeBuoyController::GetTimeUpdate() const
{
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaTimeBuoyModel *pTimeBuoyModel = dynamic_cast<VfaTimeBuoyModel*>(pObserveable);

	if(!pTimeBuoyModel)
		return;

	if (msg != VfaTimeBuoyModel::MSG_CENTER_CHANGE) //||(msg != CVtnTimeBuoyModel::MSG_STATE_CHANGE))
		return;

	// get data from model
	VistaVector3D v3Center;
	pTimeBuoyModel->GetCenter(v3Center);
	
	// reposition handles
	VistaTransformMatrix matT = m_pModel->GetTransformMatrix();

	v3Center = matT.Transform(v3Center);
	float fPosition[3];
	v3Center.GetValues (fPosition);	

	float fPt[3];	
	fPt[0] = v3Center[0] + m_fInfoTextOffset[0];
	fPt[1] = v3Center[1] + m_fInfoTextOffset[1];
	fPt[2] = v3Center[2] + m_fInfoTextOffset[2];
	//update text position
	m_pInfoText->SetPosition(fPt);

	// set handle's position
	m_vecHandles[HANDLE_MOVE]->SetCenter(v3Center); // transformation to vis-space
	if (m_iCurrentInternalState != SCS_DOCKING)
	{
		static_cast<VfaParticleHandle*> (m_vecHandles[HANDLE_DOCKING])->SetFocusPosition(v3Center);
	}

	v3Center[2] +=pTimeBuoyModel->GetHeight()/4;
	v3Center[1] +=pTimeBuoyModel->GetHeight()*9/10;
	m_vecHandles[HANDLE_SELECT]->SetCenter(v3Center);

	float fNearestTrajectoryPosition[3];
	float fTimeValue;


	if(!m_pTrajectoryController || (m_iCurrentInternalState == SCS_DOCKING))
		return;

	if (!m_pTrajectoryController->GetClosestTrajectoryPoint (fPosition, fNearestTrajectoryPosition, fTimeValue))
	{
		m_vecHandles[HANDLE_DOCKING]->SetVisible(false);
		static_cast<VfaParticleHandle*> (m_vecHandles[HANDLE_DOCKING])->SetIsHighlighted(false);
		return;
	}

	VistaVector3D v3TrajPoint (fNearestTrajectoryPosition);

	// skip, if the nearest trajectory point did not change
	if ((v3TrajPoint!=m_v3LastTrajectoryPoint))
	{
		float fDistance = (v3TrajPoint-v3Center).GetLength();
		if (fDistance< m_fConnectionRadius)
		{
			static_cast<VfaParticleHandle*> (m_vecHandles[HANDLE_DOCKING])->SetCenter (v3TrajPoint);

			m_vecHandles[HANDLE_DOCKING]->SetVisible(true);
			static_cast<VfaParticleHandle*> (m_vecHandles[HANDLE_DOCKING])->SetIsHighlighted(true);
			// only if not already docked!!!!
			pTimeBuoyModel->SetTimeInstant (fTimeValue);
			m_v3LastTrajectoryPoint = v3TrajPoint;
		}
		else
		{
			m_vecHandles[HANDLE_DOCKING]->SetVisible(false);
			static_cast<VfaParticleHandle*> (m_vecHandles[HANDLE_DOCKING])->SetIsHighlighted(false);
			m_v3LastTrajectoryPoint = VistaVector3D(0, 0, 0, 0);


		}
	}

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaTimeBuoyController::CVtnTimeBuoyControllerProps* VfaTimeBuoyController::GetProperties() const
{
	return m_pCtlProps;
}



/*============================================================================*/
/*  IMPLEMENTATION      CVtnTimeBuoyControllerProps                             */
/*============================================================================*/
static const string STR_REF_TYPENAME("CVtnTimeBuoyController::CVtnTimeBuoyControllerProps");

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTimeBuoyController::CVtnTimeBuoyControllerProps::CVtnTimeBuoyControllerProps(VfaTimeBuoyController *pTimeBuoyCtrl)
: m_pTimeBuoyCtrl(pTimeBuoyCtrl)
{}

VfaTimeBuoyController::CVtnTimeBuoyControllerProps::~CVtnTimeBuoyControllerProps()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleRadius                                         */
/*                                                                            */
/*============================================================================*/
bool VfaTimeBuoyController::CVtnTimeBuoyControllerProps::SetHandleRadius(float fRadius)
{
	if(fRadius != static_cast<VfaSphereHandle*>(m_pTimeBuoyCtrl->m_vecHandles[0])->GetRadius())
	{
		for(int i=0; i<VfaTimeBuoyController::HANDLE_DOCKING; ++i)
		{
			static_cast<VfaSphereHandle*>(m_pTimeBuoyCtrl->m_vecHandles[i])->SetRadius(fRadius);
		}
		static_cast<VfaParticleHandle*> (m_pTimeBuoyCtrl->m_vecHandles[HANDLE_DOCKING])->SetSize (fRadius);

		Notify();
		return true;
	}
	return false;
}

float VfaTimeBuoyController::CVtnTimeBuoyControllerProps::GetHandleRadius() const
{
	return static_cast<VfaSphereHandle*>(m_pTimeBuoyCtrl->m_vecHandles[0])->GetRadius();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleNormalColor                                    */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::CVtnTimeBuoyControllerProps::SetHandleNormalColor(
	const VistaColor& color)
{
	float fColors[3];
	color.GetValues(fColors);
	for(int i=0; i<VfaTimeBuoyController::HANDLE_DOCKING; ++i)
	{
		static_cast<VfaSphereHandle*>(m_pTimeBuoyCtrl->m_vecHandles[i])->SetNormalColor(fColors);
	}
	static_cast<VfaParticleHandle*> (m_pTimeBuoyCtrl->m_vecHandles[HANDLE_DOCKING])->SetFocusColor (fColors);
}

VistaColor VfaTimeBuoyController::CVtnTimeBuoyControllerProps::GetHandleNormalColor() const
{
	float fColors[3];
	static_cast<VfaSphereHandle*>(m_pTimeBuoyCtrl->m_vecHandles[0])->GetNormalColor(fColors);
	VistaColor color (fColors);

	return color;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleHighlightColor                                 */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyController::CVtnTimeBuoyControllerProps::SetHandleHighlightColor(
	const VistaColor& color)
{
	float fColors[3];
	color.GetValues(fColors);
	for(int i=0; i<VfaTimeBuoyController::HANDLE_DOCKING; ++i)
	{
		static_cast<VfaSphereHandle*>(m_pTimeBuoyCtrl->m_vecHandles[i])->SetHighlightColor(fColors);
	}
	static_cast<VfaParticleHandle*> (m_pTimeBuoyCtrl->m_vecHandles[HANDLE_DOCKING])->SetHighlightColor (fColors);

}

VistaColor VfaTimeBuoyController::CVtnTimeBuoyControllerProps::GetHandleHighlightColor() const
{
	float fColors[3];
	static_cast<VfaSphereHandle*>(m_pTimeBuoyCtrl->m_vecHandles[0])->GetHighlightColor(fColors);
	VistaColor color (fColors);

	return color;

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaTimeBuoyController::CVtnTimeBuoyControllerProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaTimeBuoyController::CVtnTimeBuoyControllerProps::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return (int) rBtList.size();
}
/*============================================================================*/
/*  END OF FILE "VtnTimeBuoyController.cpp"                                   */
/*============================================================================*/
