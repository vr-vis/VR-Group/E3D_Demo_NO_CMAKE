/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456799012345679901234567990123456799012345679901234567990123456799012345679*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VfaTimeBuoyWidget.CPP        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  FlowLibAux                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1999-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
#include "VfaTimeBuoyTrajectoryMediator.h"


#include "VfaTimeBuoyModel.h"
#include "../TrajectoryDragging/VfaTrajectoryDraggingController.h"
#include "../TrajectoryDragging/VfaParticleHandleVis.h"


#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTimeBuoyTrajectoryMediator::VfaTimeBuoyTrajectoryMediator(VflRenderNode *pRN, VfaTrajectoryDraggingController *pTrajController, VfaTimeBuoyModel* pBuoyModel)
:m_pTimeBuoyModel(pBuoyModel),
m_pTrajController(pTrajController),
m_fConnectionRadius(0.0)
{
	
	m_pConnectionView = new VfaParticleHandleVis;
	if( m_pConnectionView->Init())
	{
		pRN->AddRenderable(m_pConnectionView);
		m_pConnectionView->SetDrawRubberBand(true);
		m_pConnectionView->SetVisible (false);
	}

	this->Observe(GetModel());
	
}

VfaTimeBuoyTrajectoryMediator::~VfaTimeBuoyTrajectoryMediator()
{
	delete m_pConnectionView;
}


void VfaTimeBuoyTrajectoryMediator::SetConnectionRadius (float fRadius)
{
	m_fConnectionRadius = fRadius;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel                                                    */
/*                                                                            */
/*============================================================================*/
VfaTimeBuoyModel* VfaTimeBuoyTrajectoryMediator::GetModel() const
{
	return m_pTimeBuoyModel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetView                                                     */
/*                                                                            */
/*============================================================================*/
VfaParticleHandleVis* VfaTimeBuoyTrajectoryMediator::GetView() const
{
	return m_pConnectionView;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetController                                               */
/*                                                                            */
/*============================================================================*/
VfaTrajectoryDraggingController* VfaTimeBuoyTrajectoryMediator::GetController() const
{
	return m_pTrajController;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaTimeBuoyTrajectoryMediator::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	float fPosition[3];
	VistaVector3D v3BuoyPosition;
	m_pTimeBuoyModel->GetVisCenter (v3BuoyPosition);
	v3BuoyPosition.GetValues (fPosition);

	float fNearestTrajectoryPosition[3];
	float fTimeValue;

	if (!m_pTrajController->GetClosestTrajectoryPoint (fPosition, fNearestTrajectoryPosition, fTimeValue))
	{
		m_pConnectionView->SetVisible(false);
		return;
	}

	VistaVector3D v3TrajPoint (fNearestTrajectoryPosition);

	if ((v3TrajPoint-v3BuoyPosition).GetLength()< m_fConnectionRadius)
	{
		m_pConnectionView->SetCenterPosition (v3BuoyPosition);
		m_pConnectionView->SetFocusPosition (v3TrajPoint);
	
		m_pConnectionView->SetVisible(true);
		m_pTimeBuoyModel->SetTimeInstant (fTimeValue);
	}
	else
	{
		m_pConnectionView->SetVisible(false);

	}


}

/*============================================================================*/
/*  END OF FILE "VtnTimeBuoyTrajectoryMediator.cpp"                           */
/*============================================================================*/
