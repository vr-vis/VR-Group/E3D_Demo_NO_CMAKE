/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES														              */
/*============================================================================*/
#include "VfaRaycastPlaneHandle.h"

#include <VistaBase/VistaVectorMath.h>
#include <limits>

using namespace std;


/*============================================================================*/
/* IMPLEMENTATION												              */
/*============================================================================*/
VfaRaycastPlaneHandle::VfaRaycastPlaneHandle(IVistaTransformable *pCoordFrame)
:	IVfaRaycastHandle(pCoordFrame)
{
	m_v3Right	= VistaVector3D(1.0f, 0.0f, 0.0f, 0.0f);
	m_v3Up		= VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f);
	m_v3Normal	= VistaVector3D(0.0f, 0.0f, 1.0f, 0.0f);
	m_v3Origin	= VistaVector3D(0.0f, 0.0f, 0.0f, 1.0f);
}

VfaRaycastPlaneHandle::~VfaRaycastPlaneHandle()
{
}


bool VfaRaycastPlaneHandle::UpdateExtents(const VistaVector3D &v3Right,
	const VistaVector3D &v3Up, const VistaVector3D &v3Origin)
{
	if(v3Right.GetLength() < numeric_limits<float>::epsilon()
		|| v3Up.GetLength() < numeric_limits<float>::epsilon()
		|| v3Right.Dot(v3Up) > numeric_limits<float>::epsilon())
	{
		return false;
	}

	m_v3Right		= v3Right;
	m_v3Up			= v3Up;
	m_v3Normal		= v3Right.Cross(v3Up);
	m_v3Normal.Normalize();
	m_v3Normal[3]	= 0.0f;
	m_v3Origin		= v3Origin;

	return true;
}

void VfaRaycastPlaneHandle::RetrieveExtents(VistaVector3D &v3Right,
	VistaVector3D &v3Up, VistaVector3D &v3Origin) const
{
	v3Right		= m_v3Right;
	v3Up		= m_v3Up;
	v3Origin	= m_v3Origin;
}


bool VfaRaycastPlaneHandle::Intersects(const VistaVector3D &v3RayOrigin,
	const VistaVector3D &v3RayDir, float &fParam)
{
	if(!GetEnable())
		return false;

	VistaVector3D v3LocRayOrigin = v3RayOrigin;
	VistaVector3D v3LocRayDir = v3RayDir.GetNormalized();
	TransformGlobalToLocal(v3LocRayOrigin, v3LocRayDir);

	VistaVector3D v3PtVec = m_v3Origin - v3LocRayOrigin;
	float fDist = v3LocRayDir.Dot(m_v3Normal);

	/*------------------------------------------------------------------------*/
	// Pre-test.
	// If the ray dir and the plane are perpendicular to each other ...
	if(abs(fDist) < numeric_limits<float>::epsilon())
	{
		// ... and the point-vector is 0, there is a cut at fParam = 0.
		if(v3PtVec.GetLength() < numeric_limits<float>::epsilon())
		{
			fParam = 0.0f;
			return IntersectWithinPlane(v3LocRayOrigin, v3LocRayDir, fParam);
		}
		// ... and the point-vector is bigger 0, there is no cut present.
		else
		{
			return false;
		}
	}

	/*------------------------------------------------------------------------*/
	// Test one.
	fParam = m_v3Normal.Dot(v3PtVec) / fDist;

	if(fParam >= 0.0f)
		return IntersectWithinPlane(v3LocRayOrigin, v3LocRayDir, fParam);

	/*------------------------------------------------------------------------*/
	// Test two.
	// As the intersection test only works when the ray penetrates the plane
	// from "the front", we'll do another test that checks if the plane is
	// hit from "the back".
	m_v3Normal	*= -1.0f;
	fDist		*= -1.0f;
	fParam = m_v3Normal.Dot(v3PtVec) / fDist;

	if(fParam >= 0.0f)
		return IntersectWithinPlane(v3LocRayOrigin, v3LocRayDir, fParam);

	// If all tests fails, return false.
	return false;
}


bool VfaRaycastPlaneHandle::IntersectWithinPlane(
	const VistaVector3D &v3RayOrigin, const VistaVector3D &v3RayDir,
	float fParam) const
{
	VistaVector3D v3IntersectPos = v3RayOrigin + fParam * v3RayDir;

	const float cfNormal =
		(v3IntersectPos - m_v3Origin).Dot(m_v3Normal.GetNormalized());

	if(abs(cfNormal) > 0.01f)
		return false;

	const float cfRight =
		(v3IntersectPos - m_v3Origin).Dot(m_v3Right.GetNormalized());
	const float cfUp =
		(v3IntersectPos - m_v3Origin).Dot(m_v3Up.GetNormalized());
	
	if(cfRight >= 0.0f && cfRight <= m_v3Right.GetLength()
		&& cfUp >= 0.0f && cfUp <= m_v3Up.GetLength())
	{
		return true;
	}

	return false;
}


/*============================================================================*/
/* END OF FILE													              */
/*============================================================================*/
