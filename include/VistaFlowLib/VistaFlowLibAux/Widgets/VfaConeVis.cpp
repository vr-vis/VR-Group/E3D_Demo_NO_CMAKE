/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaConeVis.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
#include <cstring>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaConeVis                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaConeVis::VfaConeVis()
	:	m_fConeRadius(0.03f),
		m_fConeHeight(0.06f)
{
	m_v3Center[0] = 0.0f;
	m_v3Center[1] = 0.0f;
	m_v3Center[2] = 0.0f;

	m_fRotate[0] = 0.0f;
	m_fRotate[1] = 0.0f;
	m_fRotate[2] = 0.0f;
	m_fRotate[3] = 1.0f;
}

VfaConeVis::~VfaConeVis()
{}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaConeVis::DrawOpaque()
{
	VfaConeVisProps *pProps = this->GetProperties();

	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_LIGHTING_BIT|GL_POLYGON_BIT|GL_LIGHTING_BIT);
	glPushMatrix();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	
	float fColor[] = {1.0f,1.0f,1.0f,1.0f};
	pProps->GetColor(fColor);
	float fTransparency = fColor[3];

	if(pProps->GetUseLighting())
	{	
		glEnable(GL_LIGHTING);
		glDisable(GL_COLOR_MATERIAL);

		GLfloat fAmbient [] = {0.2f, 0.2f, 0.2f, fTransparency};
		GLfloat fSpecular[] = {1.0f, 1.0f, 1.0f, fTransparency};

		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,fAmbient);
		glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,fColor);
		glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,fSpecular);
		glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0f);
	}
	else
	{
		glDisable(GL_LIGHTING);
		glColor4fv(fColor);
	}

	glTranslatef(m_v3Center[0], m_v3Center[1], m_v3Center[2]);
	glRotatef(m_fRotate[0], m_fRotate[1], m_fRotate[2], m_fRotate[3]);
	
	if(pProps->GetIsSolid())
		glutSolidCone(m_fConeRadius, m_fConeHeight, 16, 16);
	else
		glutWireCone(m_fConeRadius, m_fConeHeight, 16, 16);

	glPopMatrix();
	glPopAttrib();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaConeVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaConeVis::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	//TODO
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaConeVis::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaConeVis::GetBounds(VistaBoundingBox &)
{
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetCenter                                               */
/*                                                                            */
/*============================================================================*/
bool VfaConeVis::SetCenter(const VistaVector3D& v3Center)
{
	if (v3Center == m_v3Center)
		return false;

	m_v3Center = v3Center;
	return true;

}
bool VfaConeVis::SetCenter(float fCenter[3])
{
	VistaVector3D vec (fCenter);
	return this->SetCenter(vec);
}
bool VfaConeVis::SetCenter(double dCenter[3])
{
	VistaVector3D vec (dCenter);
	return this->SetCenter(vec);
}

void VfaConeVis::GetCenter(double dCenter[3]) const
{
	m_v3Center.GetValues(dCenter);
}
void VfaConeVis::GetCenter(float fCenter[3]) const
{
	m_v3Center.GetValues(fCenter);
}
void VfaConeVis::GetCenter(VistaVector3D& v3Center) const
{
	v3Center = m_v3Center;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHeight                                               */
/*                                                                            */
/*============================================================================*/
bool VfaConeVis::SetHeight(float f)
{
	if(m_fConeHeight == f)
		return false;

	m_fConeHeight = f;
	return true;
}

float VfaConeVis::GetHeight()const
{
	return m_fConeHeight;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRadius                                               */
/*                                                                            */
/*============================================================================*/
bool VfaConeVis::SetRadius(float f)
{
	if(m_fConeRadius == f)
		return false;

	m_fConeRadius = f;
	return true;
}

float VfaConeVis::GetRadius()const
{
	return m_fConeRadius;
}




/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRotate                                               */
/*                                                                            */
/*============================================================================*/
void VfaConeVis::SetRotate(const VistaVector3D &v3Normal)
{
	VistaVector3D vecZ(0.0,0.0,1.0);
	VistaQuaternion quaRotate(vecZ, v3Normal);
	VistaAxisAndAngle a = quaRotate.GetAxisAndAngle();

	m_fRotate[0] = a.m_fAngle*180.0f/Vista::Pi;
 	m_fRotate[1] = a.m_v3Axis[0];
	m_fRotate[2] = a.m_v3Axis[1];
	m_fRotate[3] = a.m_v3Axis[2]; 
}
void VfaConeVis::GetRotate(float fRotate[4]) const
{
	fRotate[0] = m_fRotate[0];
	fRotate[1] = m_fRotate[1];
	fRotate[2] = m_fRotate[2];
	fRotate[3] = m_fRotate[3];
}

void VfaConeVis::SetRotate(float angle, float x, float y, float z)
{
	m_fRotate[0] = angle;
	m_fRotate[1] = x;
	m_fRotate[2] = y;
	m_fRotate[3] = z;

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRotate                                               */
/*                                                                            */
/*============================================================================*/
void VfaConeVis::SetOrientation (const VistaQuaternion &qOri)
{
	m_fRotate[0] = qOri.GetAxisAndAngle().m_fAngle*180.0f/Vista::Pi; //Vista::RadToDeg(acos(qOri[3])*2);
 	m_fRotate[1] = qOri[0];
	m_fRotate[2] = qOri[1];
	m_fRotate[3] = qOri[2];
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaConeVis::VfaConeVisProps *VfaConeVis::GetProperties() const
{
	return static_cast<VfaConeVis::VfaConeVisProps *>(IVflRenderable::GetProperties());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaConeVis::CreateProperties() const
{
	return new VfaConeVis::VfaConeVisProps();
}

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaConeVis::VfaConeVisProps::VfaConeVisProps()
	:	m_bUseLighting(true),
		m_bSolid(true)
{
	//blue border
	m_fColor[0] = 0.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 1.0f;
	m_fColor[3] = 1.0f;

}
VfaConeVis::VfaConeVisProps::~VfaConeVisProps()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetColor                                                */
/*                                                                            */
/*============================================================================*/
bool VfaConeVis::VfaConeVisProps::SetColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	return this->SetColor(fColor);
}

VistaColor VfaConeVis::VfaConeVisProps::GetColor() const
{
	VistaColor color(m_fColor);
	return color;
}

bool VfaConeVis::VfaConeVisProps::SetColor(float fColor[4])
{
	if(	fColor[0] == m_fColor[0] &&
		fColor[1] == m_fColor[1] &&
		fColor[2] == m_fColor[2] &&
		fColor[3] == m_fColor[3])
	{
		return false;
	}

	memcpy(m_fColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHG);
	return true;
}

void VfaConeVis::VfaConeVisProps::GetColor(float fColor[4]) const
{
	memcpy(fColor, m_fColor, 4*sizeof(float));
}

/*============================================================================*/
/*  NAME      :   Set/GetToSolid                                              */
/*============================================================================*/
bool VfaConeVis::VfaConeVisProps::SetToSolid(bool solid)
{
	if(m_bSolid == solid)
		return false;

	m_bSolid = solid;
	this->Notify(MSG_SOLID_CHG);
	return true;
}
bool VfaConeVis::VfaConeVisProps::GetIsSolid() const
{
	return m_bSolid;
}

/*============================================================================*/
/*  NAME      :   Set/GetUseLighting                                          */
/*============================================================================*/
bool VfaConeVis::VfaConeVisProps::SetUseLighting(bool b)
{
	if(m_bUseLighting == b)
		return false;

	m_bUseLighting = b;
	return true;
}

bool VfaConeVis::VfaConeVisProps::GetUseLighting() const
{
	return m_bUseLighting;
}



/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaConeVis.cpp"		         						      */
/*============================================================================*/
