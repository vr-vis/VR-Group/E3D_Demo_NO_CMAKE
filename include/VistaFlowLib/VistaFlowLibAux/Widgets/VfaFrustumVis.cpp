/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaFrustumVis.h"

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <math.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;



/*============================================================================*/
/*  IMPLEMENTATION      VfaFrustumVis                                        */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaFrustumVis::VfaFrustumVis()
{
    // chose some defaults
    this->UpdateShape(45.0f, 1.0f, 0.1f, 10000.0f);

	//blue border
	m_fColor[0] = 0.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 1.0f;
	m_fColor[3] = 1.0f;

}

VfaFrustumVis::~VfaFrustumVis()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaFrustumVis::DrawOpaque()
{
	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_LIGHTING_BIT | GL_POLYGON_BIT);
	glPushMatrix();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);


	GLfloat Ambient[] = {0.2f,0.2f,0.2f,1.0f};
	//GLfloat Specular[] = {1.0f,1.0f,1.0f,1.0f};

	//glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,Ambient);
	//glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,m_fColor);
	//glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,Specular);
	//glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0);

	glColor4fv(Ambient);

    glBegin(GL_LINE_LOOP);
	//near plane
		glVertex3f(m_vecNearTopLeft[0],m_vecNearTopLeft[1],m_vecNearTopLeft[2]);
		glVertex3f(m_vecNearTopRight[0],m_vecNearTopRight[1],m_vecNearTopRight[2]);
		glVertex3f(m_vecNearBottomRight[0],m_vecNearBottomRight[1],m_vecNearBottomRight[2]);
		glVertex3f(m_vecNearBottomLeft[0],m_vecNearBottomLeft[1],m_vecNearBottomLeft[2]);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//far plane
		glVertex3f(m_vecFarTopRight[0],m_vecFarTopRight[1],m_vecFarTopRight[2]);
		glVertex3f(m_vecFarTopLeft[0],m_vecFarTopLeft[1],m_vecFarTopLeft[2]);
		glVertex3f(m_vecFarBottomLeft[0],m_vecFarBottomLeft[1],m_vecFarBottomLeft[2]);
		glVertex3f(m_vecFarBottomRight[0],m_vecFarBottomRight[1],m_vecFarBottomRight[2]);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//bottom plane
		glVertex3f(m_vecNearBottomLeft[0],m_vecNearBottomLeft[1],m_vecNearBottomLeft[2]);
		glVertex3f(m_vecNearBottomRight[0],m_vecNearBottomRight[1],m_vecNearBottomRight[2]);
		glVertex3f(m_vecFarBottomRight[0],m_vecFarBottomRight[1],m_vecFarBottomRight[2]);
		glVertex3f(m_vecFarBottomLeft[0],m_vecFarBottomLeft[1],m_vecFarBottomLeft[2]);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//top plane
		glVertex3f(m_vecNearTopRight[0],m_vecNearTopRight[1],m_vecNearTopRight[2]);
		glVertex3f(m_vecNearTopLeft[0],m_vecNearTopLeft[1],m_vecNearTopLeft[2]);
		glVertex3f(m_vecFarTopLeft[0],m_vecFarTopLeft[1],m_vecFarTopLeft[2]);
		glVertex3f(m_vecFarTopRight[0],m_vecFarTopRight[1],m_vecFarTopRight[2]);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//left plane
		glVertex3f(m_vecNearTopLeft[0],m_vecNearTopLeft[1],m_vecNearTopLeft[2]);
		glVertex3f(m_vecNearBottomLeft[0],m_vecNearBottomLeft[1],m_vecNearBottomLeft[2]);
		glVertex3f(m_vecFarBottomLeft[0],m_vecFarBottomLeft[1],m_vecFarBottomLeft[2]);
		glVertex3f(m_vecFarTopLeft[0],m_vecFarTopLeft[1],m_vecFarTopLeft[2]);
	glEnd();

	glBegin(GL_LINE_LOOP);
	// right plane
		glVertex3f(m_vecNearBottomRight[0],m_vecNearBottomRight[1],m_vecNearBottomRight[2]);
		glVertex3f(m_vecNearTopRight[0],m_vecNearTopRight[1],m_vecNearTopRight[2]);
		glVertex3f(m_vecFarTopRight[0],m_vecFarTopRight[1],m_vecFarTopRight[2]);
		glVertex3f(m_vecFarBottomRight[0],m_vecFarBottomRight[1],m_vecFarBottomRight[2]);

	glEnd();


	//glEnable(GL_LIGHTING);

	glPopMatrix();
	glPopAttrib();

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaFrustumVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaFrustumVis::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaFrustumVis::GetBounds(VistaBoundingBox &)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaFrustumVis::CreateProperties() const
{
	return new IVflRenderable::VflRenderableProperties;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCamInternals                                           */
/*                                                                            */
/*============================================================================*/
void VfaFrustumVis::UpdateShape(float angle, float ratio, float nearD, float farD)
{
    m_fAngle = angle;
    m_fRatio = ratio;
    m_fNearD = nearD;
    m_fFarD = farD;

    // compute width and height of the near and far plane sections
	m_fTang =(float) tan(Vista::DegToRad(m_fAngle) * 0.5) ;
	m_fNearHeight = m_fNearD * m_fTang;
	m_fNearWidth = m_fNearHeight * m_fRatio; 
	m_fFarHeight = m_fFarD  * m_fTang;
	m_fFarWidth = m_fFarHeight * m_fRatio;

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateViewFrustum                                           */
/*                                                                            */
/*============================================================================*/
void VfaFrustumVis::UpdateViewFrustum(const VistaVector3D &vecViewPosition, const VistaVector3D &vecViewDirection, const VistaVector3D &vecUp, bool bForceUseUpVector)
{
	// save parameters
	m_vecPosition =vecViewPosition;
	m_vecDirection = vecViewDirection;
	m_vecUp = vecUp;

	VistaVector3D vecNearCenter,vecFarCenter,X,Y,Z;

    // compute the Z axis of camera
	// this axis points in the opposite direction from 
	// the looking direction
	//Z = -1*vecViewDirection;
	Z = -1*m_vecDirection;
    Z.Normalize();

    // X axis of camera with given "up" vector and Z axis
	//X = m_vecUp;
	X = m_vecUp.Cross(Z);
	X.Normalize();

    // the real "up" vector is the cross product of Z and X
	if(bForceUseUpVector)
		Y = m_vecUp;
	else
		Y = Z.Cross(X);

    // compute the centers of the near and far planes
	vecNearCenter = vecViewPosition - Z * m_fNearD;
	vecFarCenter = vecViewPosition - Z * m_fFarD;

    // compute the 4 corners of the frustum on the near plane
	m_vecNearTopLeft = vecNearCenter + Y * m_fNearHeight - X * m_fNearWidth;
	m_vecNearTopRight = vecNearCenter + Y * m_fNearHeight + X * m_fNearWidth;
	m_vecNearBottomLeft = vecNearCenter - Y * m_fNearHeight - X * m_fNearWidth;
	m_vecNearBottomRight = vecNearCenter - Y * m_fNearHeight + X * m_fNearWidth;
    
    // compute the 4 corners of the frustum on the far plane
	m_vecFarTopLeft = vecFarCenter + Y * m_fFarHeight - X * m_fFarWidth;
	m_vecFarTopRight = vecFarCenter + Y * m_fFarHeight + X * m_fFarWidth;
	m_vecFarBottomLeft = vecFarCenter - Y * m_fFarHeight - X * m_fFarWidth;
	m_vecFarBottomRight = vecFarCenter - Y * m_fFarHeight + X * m_fFarWidth;

	m_Planes[TOP].SetOrigin(m_vecNearTopRight); m_Planes[TOP].SetXDir(m_vecNearTopLeft); m_Planes[TOP].SetYDir(m_vecFarTopLeft);
	m_Planes[BOTTOM].SetOrigin(m_vecNearBottomLeft); m_Planes[BOTTOM].SetXDir(m_vecNearBottomRight); m_Planes[BOTTOM].SetYDir(m_vecFarBottomRight);

	m_Planes[LEFT].SetOrigin(m_vecNearTopLeft); m_Planes[LEFT].SetXDir(m_vecNearBottomLeft); m_Planes[LEFT].SetYDir(m_vecFarBottomLeft);
	m_Planes[RIGHT].SetOrigin(m_vecNearBottomRight); m_Planes[RIGHT].SetXDir(m_vecNearTopRight); m_Planes[RIGHT].SetYDir(m_vecFarBottomRight);
	
    m_Planes[NEARP].SetOrigin(m_vecNearTopLeft); m_Planes[NEARP].SetXDir(m_vecNearTopRight); m_Planes[NEARP].SetYDir(m_vecNearBottomRight);
	m_Planes[FARP].SetOrigin(m_vecFarTopRight); m_Planes[FARP].SetXDir(m_vecFarTopLeft); m_Planes[FARP].SetYDir(m_vecFarBottomLeft);
	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCorners                                                  */
/*                                                                            */
/*============================================================================*/

void VfaFrustumVis::GetCorners(VistaVector3D & vecNearTopLeft,
					 VistaVector3D & vecNearTopRight,
					 VistaVector3D & vecNearBottomLeft, 
					 VistaVector3D & vecNearBottomRight,
					 VistaVector3D & vecFarTopLeft,
					 VistaVector3D & vecFarTopRight,
					 VistaVector3D & vecFarBottomLeft,
					 VistaVector3D & vecFarBottomRight)
{
	vecNearTopLeft = m_vecNearTopLeft;
	vecNearTopRight = m_vecNearTopRight;
	vecNearBottomLeft = m_vecNearBottomLeft;
	vecNearBottomRight = m_vecNearBottomRight;
	vecFarTopLeft = m_vecFarTopLeft;
	vecFarTopRight = m_vecFarTopRight;
	vecFarBottomLeft = m_vecFarBottomLeft;
	vecFarBottomRight = m_vecFarBottomRight;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetColor                                                    */
/*                                                                            */
/*============================================================================*/
void VfaFrustumVis::SetColor(float fColor[4])
{
	m_fColor[0] = fColor[0];
	m_fColor[1] = fColor[1];
	m_fColor[2] = fColor[2];
	m_fColor[3] = fColor[3];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetColor                                                    */
/*                                                                            */
/*============================================================================*/
void VfaFrustumVis::GetColor(float fColor[4]) const
{
	fColor[0] = m_fColor[0];
	fColor[1] = m_fColor[1];
	fColor[2] = m_fColor[2];
	fColor[3] = m_fColor[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetUpVector                                                 */
/*                                                                            */
/*============================================================================*/

void VfaFrustumVis::GetUpVector(VistaVector3D & vUp)
{
	vUp = m_vecUp;
}

void VfaFrustumVis::GetViewPosition(VistaVector3D & vPos)
{
	vPos = m_vecPosition;
}

void VfaFrustumVis::GetViewDirection(VistaVector3D & vDir)
{
	vDir = m_vecDirection;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaFrustumVis.cpp"		         						      */
/*============================================================================*/
