/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaForwardObserver.h"
#include "VfaConditionalForwarding.h"
#include <VistaBase/VistaVectorMath.h>


#include <cassert>
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

VfaForwardObserver::VfaForwardObserver(VfaConditionalForwarding *pObservable,VfaWidgetModelBase *pModel)
	: m_pModel (pModel),
	  m_pForwardObservable(pObservable)
{
	m_pModel->AttachObserver(this);
}

VfaForwardObserver::~VfaForwardObserver()
{
	m_pModel->DetachObserver(this);
}


/*============================================================================*/
/* ObserverUpdate                                                             */
/*============================================================================*/
void VfaForwardObserver::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if(m_pModel == NULL)
	{
		return;
	}
	else
	{
		// informs VfaForwardObserver about the Notification of VfaWidgetModelBase
		m_pForwardObservable->IsTrigger();
	}
}


/*============================================================================*/
/*  END OF FILE "VfaForwardObserver.cpp"                                      */
/*============================================================================*/
