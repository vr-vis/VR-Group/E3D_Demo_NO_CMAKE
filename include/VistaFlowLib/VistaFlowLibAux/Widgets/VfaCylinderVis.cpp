/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaCylinderVis.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include "Cylinder/VfaCylinderModel.h"
#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
#include <cstring>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaCylinderVis                                       */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCylinderVis::VfaCylinderVis(VfaCylinderModel *pModel)
	:	m_fCylinderRadius(0.03f),
		m_fCylinderHeight(0.06f),
	    m_pModel(pModel)
{
	m_v3Center[0] = 0.0f;
	m_v3Center[1] = 0.0f;
	m_v3Center[2] = 0.0f;

	m_fRotate[0] = 0.0f;	
	m_fRotate[1] = 0.0f;
	m_fRotate[2] = 0.0f;
	m_fRotate[3] = 1.0f;

	quadratic = gluNewQuadric();
}

VfaCylinderVis::~VfaCylinderVis()
{
	gluDeleteQuadric(quadratic);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaCylinderVis::DrawOpaque()
{
	VfaCylinderVisProps *pProps = this->GetProperties();
	if(!pProps->GetVisible())
		return;

	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_LIGHTING_BIT|GL_POLYGON_BIT|GL_LIGHTING_BIT);
	glPushMatrix();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glEnable(GL_BLEND); 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	glDisable(GL_CULL_FACE);
	
	float fColor[] = {1.0f,1.0f,1.0f,0.2f};
	pProps->GetColor(fColor);

	if(pProps->GetUseLighting())
	{	
		glEnable(GL_LIGHTING);
		glDisable(GL_COLOR_MATERIAL);

		GLfloat fAmbient[] = {0.1f,0.1f,0.1f,1.0f};
		GLfloat fSpecular[] = {1.0f,1.0f,1.0f,1.0f};

		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,fAmbient);
		glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,fColor);
		glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,fSpecular);
		glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0);
	}
	else
	{
		glDisable(GL_LIGHTING);
		glColor4fv(fColor);
	}
	m_fCylinderRadius = m_pModel->GetRadius();
	m_fCylinderHeight = m_pModel->GetHeight();

	m_pModel->GetCenter(m_v3Center);

	glTranslatef(m_v3Center[0], m_v3Center[1], m_v3Center[2]);

	VistaQuaternion qOri;
	m_pModel->GetRotation(qOri);
	glRotatef(qOri.GetAxisAndAngle().m_fAngle*180.0f/Vista::Pi, qOri[0], qOri[1], qOri[2]);
	gluQuadricNormals(quadratic, GLU_SMOOTH);
	gluQuadricTexture(quadratic, GL_TRUE);
	
	gluCylinder(quadratic, m_fCylinderRadius, m_fCylinderRadius, m_fCylinderHeight, 32, 32);
	glTranslatef(0.0, 0.0, m_fCylinderHeight);


	glPopMatrix();
	glPopAttrib();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaCylinderVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}


VfaCylinderModel *VfaCylinderVis::GetModel() const
{
	return m_pModel;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaCylinderVis::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	//TODO
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderVis::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderVis::GetBounds(VistaBoundingBox &)
{
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetCenter                                               */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderVis::SetCenter(const VistaVector3D v3Center)
{
	if(v3Center == m_v3Center)
		return false;

	m_v3Center = v3Center;
	return true;
}
bool VfaCylinderVis::SetCenter(float fCenter[3])
{
	VistaVector3D vec (fCenter);
	return this->SetCenter(vec);
}
bool VfaCylinderVis::SetCenter(double dCenter[3])
{
	VistaVector3D vec (dCenter);
	return this->SetCenter(vec);
}

void VfaCylinderVis::GetCenter(VistaVector3D v3Center) const
{
	v3Center = m_v3Center;
}
void VfaCylinderVis::GetCenter(float fCenter[3]) const
{
	m_v3Center.GetValues(fCenter);
}
void VfaCylinderVis::GetCenter(double dCenter[3]) const
{
	m_v3Center.GetValues(dCenter);
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHeight                                               */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderVis::SetHeight(float f)
{
	if(m_fCylinderHeight == f)
		return false;

	m_fCylinderHeight = f;
	return true;
}

float VfaCylinderVis::GetHeight()const
{
	return m_fCylinderHeight;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRadius                                               */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderVis::SetRadius(float f)
{
	if(m_fCylinderRadius == f)
		return false;

	m_fCylinderRadius = f;
	return true;
}

float VfaCylinderVis::GetRadius()const
{
	return m_fCylinderRadius;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRotate                                                   */
/*                                                                            */
/*============================================================================*/
void VfaCylinderVis::SetRotate(const VistaVector3D &v3Normal)
{
	VistaVector3D vecZ(0.0,0.0,1.0);
	VistaQuaternion quaRotate(vecZ, v3Normal);
	VistaAxisAndAngle a = quaRotate.GetAxisAndAngle();

	m_fRotate[0] = a.m_fAngle*180.0f/Vista::Pi;
 	m_fRotate[1] = a.m_v3Axis[0];
	m_fRotate[2] = a.m_v3Axis[1];
	m_fRotate[3] = a.m_v3Axis[2];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRotate                                                   */
/*                                                                            */
/*============================================================================*/
void VfaCylinderVis::GetRotate(float fRotate[4]) const
{
	fRotate[0] = m_fRotate[0];
	fRotate[1] = m_fRotate[1];
	fRotate[2] = m_fRotate[2];
	fRotate[3] = m_fRotate[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaCylinderVis::VfaCylinderVisProps *VfaCylinderVis::GetProperties() const
{
	return static_cast<VfaCylinderVis::VfaCylinderVisProps *>(IVflRenderable::GetProperties());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaCylinderVis::CreateProperties() const
{
	return new VfaCylinderVis::VfaCylinderVisProps();
}

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCylinderVis::VfaCylinderVisProps::VfaCylinderVisProps()
	:	m_bUseLighting(true)
{
	//blue border
	m_fColor[0] = 0.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 1.0f;
}
VfaCylinderVis::VfaCylinderVisProps::~VfaCylinderVisProps()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetColor                                                */
/*                                                                            */
/*============================================================================*/
bool VfaCylinderVis::VfaCylinderVisProps::SetColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	return this->SetColor(fColor);
}

VistaColor VfaCylinderVis::VfaCylinderVisProps::GetColor() const
{
	return VistaColor(m_fColor);
}

bool VfaCylinderVis::VfaCylinderVisProps::SetColor(float fColor[4])
{
	if(	fColor[0] == m_fColor[0] &&
		fColor[1] == m_fColor[1] &&
		fColor[2] == m_fColor[2] &&
		fColor[3] == m_fColor[3])
	{
		return false;
	}

	memcpy(m_fColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHG);
	return true;
}

void VfaCylinderVis::VfaCylinderVisProps::GetColor(float fColor[4]) const
{
	memcpy(fColor, m_fColor, 4*sizeof(float));
}

/*============================================================================*/
/*  NAME      :   Set/GetUseLighting                                          */
/*============================================================================*/
bool VfaCylinderVis::VfaCylinderVisProps::SetUseLighting(bool b)
{
	if(m_bUseLighting == b)
		return false;

	m_bUseLighting = b;
	return true;
}

bool VfaCylinderVis::VfaCylinderVisProps::GetUseLighting() const
{
	return m_bUseLighting;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaCylinderVis.cpp"		         					      */
/*============================================================================*/
