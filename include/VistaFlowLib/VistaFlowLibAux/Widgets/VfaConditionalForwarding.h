/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VfaConditionalForwarding_H
#define _VfaConditionalForwarding_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaForwardObserver.h"

#include <VistaAspects/VistaObserveable.h>
#include <VistaFlowLibAux/Widgets/VfaWidgetModelBase.h>
#include <VistaFlowLibAux/Interaction/VfaApplicationContextObject.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaConditionalForwarding : public IVistaObserveable
{
public:
	enum
	{
		MSG_BUTTON_IS_PRESSED = IVistaObserveable::MSG_LAST,
		MSG_LAST
	};

	/**
	*	VfaWidgetModelBase				 will be observed
	*	VfaApplicationContextObject	 knows about button state
	*	int iMyButtonId					 this button will be tested (if pressed or not)
	*/
	VfaConditionalForwarding(VfaWidgetModelBase *pModel,
				VfaApplicationContextObject *pApplicationObject, int iMyButtonId);
	virtual ~VfaConditionalForwarding();

	/**
	*	will be called by VfaForwardObserver, if VfaWidgetModelBase sends a Notify
	*	and sends a new Notification, if specified button is pressed
	*/
	void IsTrigger();

	void SetIsActive(bool bIsActive);
	bool GetIsActive() const;
	
protected:
	
private:
	VfaApplicationContextObject *m_pApplicationObject;
	int m_iButtonId;
	VfaForwardObserver *m_pForwardObserver;

	bool m_bIsActive;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaConditionalForwarding_H

