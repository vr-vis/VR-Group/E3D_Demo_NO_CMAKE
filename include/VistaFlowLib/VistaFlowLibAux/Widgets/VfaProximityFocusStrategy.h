/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAPROXIMITYFOCUSSTRATEGY_H
#define _VFAPROXIMITYFOCUSSTRATEGY_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaFocusStrategy.h"

#include <map>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaProximityHandle;
class IVfaWidgetController;
class VflRenderNode;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*!
 * The proximity focus strategy is based on - well- proximity, i.e. handles are
 * grabbed(and focus is shifted) by being spatially close to or inside of the
 * handle. It relies on specific implementations for such handles. Currently,
 * no scoring or stickiness is implemented, but handles are evaluated in an 
 * FCFS fashion, i.e., the first handle that indicates being touched will
 * gain the focus.
 *
 */
class VISTAFLOWLIBAUXAPI VfaProximityFocusStrategy : public IVfaFocusStrategy
{
public:
	VfaProximityFocusStrategy();
	virtual ~VfaProximityFocusStrategy();

	virtual void RegisterSelectable(IVfaWidgetController* pSelectable);
	virtual void UnregisterSelectable(IVfaWidgetController* pSelectable);
	
	/**
	 * Determine which one of the registered handles has
	 * been selected (if any).
	 */
	bool EvaluateFocus(std::vector<IVfaControlHandle*> &vecControlHandles);

	/**
	 * Just take notice of the current sensor pos/ori
	 */
	virtual void OnSensorSlotUpdate(int iSlot, 
			const VfaApplicationContextObject::sSensorFrame & oFrameData);

	/**
	 *
	 */
	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);

	/**
	 *
	 */
	virtual void OnTimeSlotUpdate(const double dTime);

	/**
	 * return an(or'ed) bitmask defining which sensor 
	 * slots/command slots are used/handled by this controller
	 */
	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;

	/** 
	 * The global proximity distance defines when proximity
	 * handles managed by this strategy feel "approximated".
	 * This only influences handles which are not touched "from the inside".
	 */
	void SetGlobalProximityDistance(float fD);
	float GetGlobalProximityDistance() const;
	
protected:

private:
	std::vector<IVfaProximityHandle*>				m_vecHandles;
	IVfaProximityHandle								*m_pActiveHandle;
	float											m_fProximityDistance;
	std::map<IVfaProximityHandle*, VflRenderNode*>	m_mapHandle2RenderNode;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VfaProximityFocusStrategy_H
