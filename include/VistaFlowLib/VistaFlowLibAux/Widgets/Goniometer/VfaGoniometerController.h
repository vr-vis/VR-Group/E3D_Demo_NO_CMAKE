#ifndef _GONIOMETERCONTROLER_H
#define _GONIOMETERCONTROLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <vector>
#include <string>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
using namespace std;


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaSphereHandle;
class VfaGoniometerModel;
class VfaGoniometerVis;
class VflRenderNode;
class Vfl3DTextLabel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Controll the behavior on button press event, button release event and 
 * pre-loop event, also highlight.
 */
class VISTAFLOWLIBAUXAPI VfaGoniometerController : public IVfaWidgetController,
	public VflObserver
{
public:
	VfaGoniometerController (VfaGoniometerModel *pModel, 
		VflRenderNode *pRenderNode);
	virtual ~VfaGoniometerController();

	virtual void OnFocus(IVfaControlHandle* pHandle);
	virtual void OnUnfocus();

	virtual void OnSensorSlotUpdate(int iSlot,
		const VfaApplicationContextObject::sSensorFrame &oFrameData);
	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);
	virtual void OnTimeSlotUpdate(const double dTime);

	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetShowText(bool b);
	bool GetShowText() const;

	void SetTextSize(float f);
	float GetTextSize() const;

	int GetDragButton() const;	
	void SetDragButton(int i);

	//********** OBSERVER INTERFACE IMPLEMENTATION *********************//
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int iMsg, int iTicket);

protected:
	/**
	 * enum for internal state control
	 */
	enum EInternalState{
		STATE_DISABLED,
		STATE_NOTHING_SELECTED,
		STATE_ON_FOCUS,
		STATE_MOVING
	};
private:
	VfaGoniometerModel		*m_pModel;
	VflRenderNode		*m_pRenderNode;

	// handles for interaction
	vector<VfaSphereHandle*> m_pSphereHandles;

	int m_iDragButton;

	int m_iState;
	int m_iActiveHandle;

	bool m_bFirstMove;
	float m_fLastDistance;
	VistaVector3D m_v3RIGHT;
	VistaVector3D m_v3LEFT;
	VistaVector3D m_v3SPINDLE;
	bool m_bShowText;

	//enumeration for handles
	enum
	{
		HND_UNDEFINDED = -1,
		HND_SPHERE_LEFT = 0,
		HND_SPHERE_MIDI,
		HND_SPHERE_RIGHT,
		HND_SPHERE_SPINDLE_LEFT,
		HND_SPHERE_SPINDLE_RIGHT
	};
	Vfl3DTextLabel *m_p3DTextUserAligned;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
