
#include <cstring>

#include "VfaGoniometerModel.h"
#include <VistaAspects/VistaReflectionable.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaBase/VistaVector3D.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  IMPLEMENTATION      CGoniometerModel                                      */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaGoniometerModel");
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaGoniometerModel::VfaGoniometerModel()
	:	m_v3Point1(0.5f, 0.0f, 0.0f),
		m_v3Point2(0.0f, 0.5f, 0.0f),
		m_v3MidPoint(0.0f, 0.0f, 0.0f),
		m_fLength1(0.5f),
		m_fLength2(0.5f),
		m_fAngle(Vista::DegToRad(90))
{}

VfaGoniometerModel::~VfaGoniometerModel()
{}

/*============================================================================*/
/*  NAME      :   Set/GetPoint1		                                          */
/*============================================================================*/
void VfaGoniometerModel::SetPoint1(float fPt[])
{
	this->SetPoint1(VistaVector3D(fPt));
	this->ComputeLength();
	this->Notify(MSG_GONIOMETER_CHANGE);
}
void VfaGoniometerModel::SetPoint1(double dPt[])
{
	this->SetPoint1(VistaVector3D(dPt));
	this->ComputeLength();
	this->Notify(MSG_GONIOMETER_CHANGE);
}
void VfaGoniometerModel::SetPoint1(const VistaVector3D &v3C)
{
	m_v3Point1 = v3C;
	this->ComputeLength();
	this->Notify(MSG_GONIOMETER_CHANGE);
}

void VfaGoniometerModel::GetPoint1(float fPt[]) const
{
	m_v3Point1.GetValues(fPt);
}
void VfaGoniometerModel::GetPoint1(double dPt[]) const
{
	dPt[0] = m_v3Point1[0];
	dPt[1] = m_v3Point1[1];
	dPt[2] = m_v3Point1[2];
}
void VfaGoniometerModel::GetPoint1(VistaVector3D &v3P) const
{
	v3P = m_v3Point1;
}

/*============================================================================*/
/*  NAME      :   Set/GetPoint2		                                          */
/*============================================================================*/
void VfaGoniometerModel::SetPoint2(float fPt[])
{
	this->SetPoint2(VistaVector3D(fPt));
	this->ComputeLength();
	this->Notify(MSG_GONIOMETER_CHANGE);
}
void VfaGoniometerModel::SetPoint2(double dPt[])
{
	this->SetPoint2(VistaVector3D(dPt));
	this->ComputeLength();
	this->Notify(MSG_GONIOMETER_CHANGE);
}
void VfaGoniometerModel::SetPoint2(const VistaVector3D &v3C)
{
	m_v3Point2 = v3C;
	this->ComputeLength();
	this->Notify(MSG_GONIOMETER_CHANGE);
}

void VfaGoniometerModel::GetPoint2(float fPt[]) const
{
	m_v3Point2.GetValues(fPt);
}
void VfaGoniometerModel::GetPoint2(double dPt[]) const
{
	dPt[0] = m_v3Point2[0];
	dPt[1] = m_v3Point2[1];
	dPt[2] = m_v3Point2[2];
}
void VfaGoniometerModel::GetPoint2(VistaVector3D &v3P) const
{
	v3P = m_v3Point2;
}
/*============================================================================*/
/*  NAME      :   SetPoints     		                                      */
/*============================================================================*/
void VfaGoniometerModel::SetPoints(const VistaVector3D &v3C1,const VistaVector3D &v3C2)
{
	m_v3Point1 = v3C1;
	m_v3Point2 = v3C2;
	this->ComputeLength();
	this->Notify(MSG_GONIOMETER_CHANGE);
}
/*============================================================================*/
/*  NAME      :   Set/GetMidPoint		                                      */
/*============================================================================*/
void VfaGoniometerModel::SetMidPoint(float fPt[])
{
	this->SetMidPoint(VistaVector3D(fPt));
	this->ComputeLength();
	this->Notify(MSG_GONIOMETER_CHANGE);
}
void VfaGoniometerModel::SetMidPoint(double dPt[])
{
	this->SetMidPoint(VistaVector3D(dPt));
	this->ComputeLength();
	this->Notify(MSG_GONIOMETER_CHANGE);
}
void VfaGoniometerModel::SetMidPoint(const VistaVector3D &v3C)
{
	m_v3MidPoint = v3C;
	this->ComputeLength();
	this->Notify(MSG_GONIOMETER_CHANGE);
}

void VfaGoniometerModel::GetMidPoint(float fPt[]) const
{
	m_v3MidPoint.GetValues(fPt);
}
void VfaGoniometerModel::GetMidPoint(double dPt[]) const
{
	dPt[0] = m_v3MidPoint[0];
	dPt[1] = m_v3MidPoint[1];
	dPt[2] = m_v3MidPoint[2];
}
void VfaGoniometerModel::GetMidPoint(VistaVector3D &v3P) const
{
	v3P = m_v3MidPoint;
}
/*============================================================================*/
/*  NAME      :   GetLength			                                          */
/*============================================================================*/
float VfaGoniometerModel::GetLength1() const
{
	return m_fLength1;
}
float VfaGoniometerModel::GetLength2() const
{
	return m_fLength2;
}
float VfaGoniometerModel::GetAngle() const
{
	return m_fAngle;
}
float VfaGoniometerModel::GetAngleD() const
{
	return Vista::RadToDeg(m_fAngle);
}
/*============================================================================*/
/*  NAME      :   Recalculate...	                                          */
/*============================================================================*/
void VfaGoniometerModel::ComputeLength()
{
	m_fLength1 = (m_v3Point1-m_v3MidPoint).GetLength();
	m_fLength2 = (m_v3Point2-m_v3MidPoint).GetLength();
	m_fAngle   =  acos(((m_v3Point1-m_v3MidPoint).Dot(m_v3Point2-m_v3MidPoint))/(m_fLength1*m_fLength2));
}
/*============================================================================*/
/*  NAME      :   GetReflectionableType                                       */
/*============================================================================*/
std::string VfaGoniometerModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*  NAME      :   AddToBaseTypeList                                           */
/*============================================================================*/
int VfaGoniometerModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
