#ifndef _GONIOMETERMODLE_H
#define _GONIOMETERMODLE_H

#include <VistaFlowLibAux/Widgets/VfaWidgetModelBase.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaGoniometerModel : public VfaWidgetModelBase
{
public:
	enum{
		MSG_GONIOMETER_CHANGE = VfaWidgetModelBase::MSG_LAST,
		MSG_LAST
	};

	VfaGoniometerModel();
	virtual ~VfaGoniometerModel();

	void SetPoint1(float fPt[3]);
	void SetPoint1(double dPt[3]);
	void SetPoint1(const VistaVector3D &v3C);
	void GetPoint1(float fPt[3]) const;
	void GetPoint1(double dPt[3]) const;
	void GetPoint1(VistaVector3D &v3P) const;

	void SetPoint2(float fPt[3]);
	void SetPoint2(double dPt[3]);
	void SetPoint2(const VistaVector3D &v3C);
	void GetPoint2(float fPt[]) const;
	void GetPoint2(double dPt[]) const;
	void GetPoint2(VistaVector3D &v3P) const;

	void SetPoints(const VistaVector3D &v3C1,const VistaVector3D &v3C2);

	void SetMidPoint(float fPt[3]);
	void SetMidPoint(double dPt[3]);
	void SetMidPoint(const VistaVector3D &v3C);
	void GetMidPoint(float fPt[]) const;
	void GetMidPoint(double dPt[]) const;
	void GetMidPoint(VistaVector3D &v3P) const;

	float GetLength1() const;
	float GetLength2() const;
	float GetAngle() const;
	float GetAngleD() const;

	virtual std::string GetReflectionableType() const;

protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	void ComputeLength();

private:
	VistaVector3D m_v3Point1;
	VistaVector3D m_v3Point2;
	VistaVector3D m_v3MidPoint;
	
	float m_fLength1;
	float m_fLength2;
	float m_fAngle;
	float m_fWidth;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
