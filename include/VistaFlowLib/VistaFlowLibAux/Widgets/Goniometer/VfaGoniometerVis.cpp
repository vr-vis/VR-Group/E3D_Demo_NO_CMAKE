
#include <GL/glew.h>

#include "VfaGoniometerVis.h"
#include "VfaGoniometerModel.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <cassert>
#include <cstring>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaGoniometerVis::VfaGoniometerVis(VfaGoniometerModel *pModel)
								:	m_pModel(pModel)
{}

VfaGoniometerVis::~VfaGoniometerVis()
{}

/*============================================================================*/
/*  NAME      :   GetModel                                                    */
/*============================================================================*/
VfaGoniometerModel *VfaGoniometerVis::GetModel() const
{
	return m_pModel;
}


/*============================================================================*/
/*  NAME      :   DrawOpaque                                                  */
/*============================================================================*/
void VfaGoniometerVis::DrawOpaque()
{
	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_ENABLE_BIT|GL_LINE_BIT);
	glPushMatrix();

	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	float fC[4]={0.0f,0.0f,0.0f,1.0f};
	float fPoint1[3], fPoint2[3], fMidPoint[3],fNormal[3] ;
	m_pModel->GetPoint1(fPoint1);
	m_pModel->GetPoint2(fPoint2);
	m_pModel->GetMidPoint(fMidPoint);
	float fLength1, fLength2, fLength3;
	fLength1=m_pModel->GetLength1();
	fLength2=m_pModel->GetLength2();
	fLength3= min(	(min(fLength1,fLength2)*0.9f),
					(fLength1*fLength2*0.5f
						+ 2*min(fLength1,fLength2)
					)*0.15f + 0.2f);
	fNormal[0]=fPoint1[1]*fPoint2[2]-fPoint1[2]*fPoint2[1];
	fNormal[1]=fPoint1[2]*fPoint2[0]-fPoint1[0]*fPoint2[2];
	fNormal[2]=fPoint1[0]*fPoint2[1]-fPoint1[1]*fPoint2[0];

	glLineWidth(2);

	this->GetProperties()->GetLineColor(fC);
	glBegin(GL_LINES);
	glColor4fv(fC);
	glVertex3f(fMidPoint[0], fMidPoint[1], fMidPoint[2]);
	glVertex3f(fPoint1[0], fPoint1[1], fPoint1[2]);	
	glVertex3f(fMidPoint[0], fMidPoint[1], fMidPoint[2]);
	glVertex3f(fPoint2[0], fPoint2[1], fPoint2[2]);	
	glEnd();

	this->GetProperties()->GetAngleColor(fC);
	glColor4fv(fC);
	glBegin(GL_TRIANGLE_FAN);
	glNormal3f (fNormal[0],fNormal[1], fNormal[2]);
	glVertex3f ( fMidPoint[0], fMidPoint[1], fMidPoint[2]); 
	int Steps = int(m_pModel->GetAngleD()/10)+1;
	for (int i=0;i<=Steps;++i){
		float tmp_f[4];
		tmp_f[0]=	(		    i *(fPoint1[0]-fMidPoint[0])/fLength1
						+(Steps-i)*(fPoint2[0]-fMidPoint[0])/fLength2
					)/Steps;
		tmp_f[1]=	(		    i *(fPoint1[1]-fMidPoint[1])/fLength1
						+(Steps-i)*(fPoint2[1]-fMidPoint[1])/fLength2
					)/Steps;
		tmp_f[2]=	(		    i *(fPoint1[2]-fMidPoint[2])/fLength1
						+(Steps-i)*(fPoint2[2]-fMidPoint[2])/fLength2
					)/Steps;
		tmp_f[3]=sqrt(tmp_f[0]*tmp_f[0]+tmp_f[1]*tmp_f[1]+tmp_f[2]*tmp_f[2]);
		tmp_f[0]=tmp_f[0]/tmp_f[3]*fLength3+fMidPoint[0];
		tmp_f[1]=tmp_f[1]/tmp_f[3]*fLength3+fMidPoint[1];
		tmp_f[2]=tmp_f[2]/tmp_f[3]*fLength3+fMidPoint[2];
		glVertex3f ( tmp_f[0],tmp_f[1], tmp_f[2]);
	}
	glEnd();

	glPopMatrix();
	glPopAttrib();
}
/*============================================================================*/
/*  NAME      :   GetRegistrationMode                                         */
/*============================================================================*/
unsigned int VfaGoniometerVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*  NAME      :   GetProperties                                               */
/*============================================================================*/
VfaGoniometerVis::CGoniometerVisProps *VfaGoniometerVis::GetProperties() const
{
	return static_cast<VfaGoniometerVis::CGoniometerVisProps *>(IVflRenderable::GetProperties());
}

/*============================================================================*/
/*  NAME      :   CreateProperties                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaGoniometerVis::CreateProperties() const
{
	return new VfaGoniometerVis::CGoniometerVisProps;
}

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaGoniometerVis::CGoniometerVisProps::CGoniometerVisProps()
{
	m_fLineColor[0]=0.0f;
	m_fLineColor[1]=0.0f;
	m_fLineColor[2]=0.0f;
	m_fLineColor[3]=1.0f;

	m_fAngleColor[0]=0.0f;
	m_fAngleColor[1]=0.0f;
	m_fAngleColor[2]=1.0f;
	m_fAngleColor[3]=1.0f;
}
VfaGoniometerVis::CGoniometerVisProps::~CGoniometerVisProps()
{}
/*============================================================================*/
/*  NAME      :   Set/GetWidth                                                */
/*============================================================================*/
void VfaGoniometerVis::CGoniometerVisProps::SetWidth(float f)
{
	m_fLineWidth = f;
}
float VfaGoniometerVis::CGoniometerVisProps::GetWidth() const
{
	return m_fLineWidth;
}

/*============================================================================*/
/*  NAME      :   Set/GetLineColor                                            */
/*============================================================================*/
void VfaGoniometerVis::CGoniometerVisProps::SetLineColor(float fColor[4])
{
	memcpy(m_fLineColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHG);

}
void VfaGoniometerVis::CGoniometerVisProps::SetLineColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	this->SetLineColor(fColor);
}

void VfaGoniometerVis::CGoniometerVisProps::GetLineColor(float fColor[4]) const
{
	memcpy(fColor, m_fLineColor, 4*sizeof(float));
}
VistaColor VfaGoniometerVis::CGoniometerVisProps::GetLineColor() const
{
	return VistaColor(m_fLineColor);
}
/*============================================================================*/
/*  NAME      :   Set/GetAngleColor                                           */
/*============================================================================*/
void VfaGoniometerVis::CGoniometerVisProps::SetAngleColor(float fColor[4])
{
	memcpy(m_fAngleColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHG);

}
void VfaGoniometerVis::CGoniometerVisProps::SetAngleColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	this->SetAngleColor(fColor);
}

void VfaGoniometerVis::CGoniometerVisProps::GetAngleColor(float fColor[4]) const
{
	memcpy(fColor, m_fAngleColor, 4*sizeof(float));
}
VistaColor VfaGoniometerVis::CGoniometerVisProps::GetAngleColor() const
{
	return VistaColor(m_fLineColor);
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
