
#include "VfaGoniometerController.h"
#include "VfaGoniometerModel.h"
#include <VistaFlowLibAux/Widgets/VfaWidgetTools.h>
#include "VfaGoniometerVis.h"
#include <VistaFlowLibAux/Widgets/VfaSphereHandle.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <cstdio>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaGoniometerController::VfaGoniometerController(VfaGoniometerModel *pModel, VflRenderNode *pRenderNode)
											:	IVfaWidgetController(pRenderNode),
												m_pModel(pModel),
												m_pRenderNode(pRenderNode),
												m_iState(STATE_NOTHING_SELECTED),
												m_iActiveHandle(HND_UNDEFINDED),
												m_iDragButton(0),
												m_bShowText(true)
{
	//attach the model
	this->Observe(m_pModel);

	//create handles
	m_pSphereHandles.resize(5);

	m_pSphereHandles[HND_SPHERE_LEFT] = new VfaSphereHandle(pRenderNode);
	m_pSphereHandles[HND_SPHERE_LEFT]->SetRadius(0.02f);
	this->AddControlHandle(m_pSphereHandles[HND_SPHERE_LEFT]);

	m_pSphereHandles[HND_SPHERE_MIDI] = new VfaSphereHandle(pRenderNode);
	m_pSphereHandles[HND_SPHERE_MIDI]->SetRadius(0.04f);
	this->AddControlHandle(m_pSphereHandles[HND_SPHERE_MIDI]);

	m_pSphereHandles[HND_SPHERE_RIGHT] = new VfaSphereHandle(pRenderNode);
	m_pSphereHandles[HND_SPHERE_RIGHT]->SetRadius(0.02f);
	this->AddControlHandle(m_pSphereHandles[HND_SPHERE_RIGHT]);

	m_pSphereHandles[HND_SPHERE_SPINDLE_LEFT] = new VfaSphereHandle(pRenderNode);
	m_pSphereHandles[HND_SPHERE_SPINDLE_LEFT]->SetRadius(0.02f);
	this->AddControlHandle(m_pSphereHandles[HND_SPHERE_SPINDLE_LEFT]);

	m_pSphereHandles[HND_SPHERE_SPINDLE_RIGHT] = new VfaSphereHandle(pRenderNode);
	m_pSphereHandles[HND_SPHERE_SPINDLE_RIGHT]->SetRadius(0.02f);
	this->AddControlHandle(m_pSphereHandles[HND_SPHERE_SPINDLE_RIGHT]);

	//create TextLable
	m_p3DTextUserAligned = new Vfl3DTextLabel;
	if(!m_p3DTextUserAligned->Init())
		return ;
	char str[255];
	sprintf(str,"    Angle: %.2f",m_pModel->GetAngleD());
	m_p3DTextUserAligned->SetText(str);
	m_p3DTextUserAligned->SetTextSize(0.06f);
	float fPos[3] = {0.0f, 0.0f, 0.0f};
	float fColor[4] = {0.0f, 0.0f, 0.0f, 1.0f};
	m_p3DTextUserAligned->SetPosition(fPos);
	m_p3DTextUserAligned->SetColor(fColor);
	pRenderNode->AddRenderable(m_p3DTextUserAligned);
	m_p3DTextUserAligned->SetTextFollowViewDir(true);

}

VfaGoniometerController::~VfaGoniometerController()
{
	this->ReleaseObserveable(m_pModel, IVistaObserveable::TICKET_NONE);

	if (m_p3DTextUserAligned != NULL)
	{
		m_pRenderNode->RemoveRenderable(m_p3DTextUserAligned);
		delete m_p3DTextUserAligned;
	}
	//delete handles
	for (int i = 0; i < 5; i++)
		delete m_pSphereHandles[i];

}


/*============================================================================*/
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*============================================================================*/
void VfaGoniometerController::OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame &oFrameData)
{
	if(m_iState!=STATE_MOVING)
		return;

	VistaVector3D v3Direktion =oFrameData.qOrientation.GetAxisAndAngle().m_v3Axis;

	if(m_bFirstMove)
	{
		VistaVector3D v3Pt1, v3Pt2;
		v3Pt1=oFrameData.v3Position;
		switch(m_iActiveHandle)
		{
			case HND_SPHERE_LEFT:
				m_pModel->GetPoint1(v3Pt2);
			  break;
			case HND_SPHERE_RIGHT:
				m_pModel->GetPoint2(v3Pt2);
			  break;
			case HND_SPHERE_MIDI:
				m_pModel->GetMidPoint(v3Pt2);
			  break;
			case HND_SPHERE_SPINDLE_LEFT:
				{
					VistaVector3D tmp;
					m_pModel->GetPoint1(m_v3LEFT);
					m_pModel->GetPoint2(m_v3RIGHT);
					m_pModel->GetMidPoint(tmp);
					v3Pt2=tmp-((m_v3LEFT-tmp).GetNormalized())*0.07f;
					m_v3SPINDLE = v3Pt2;
				}
			  break;
			case HND_SPHERE_SPINDLE_RIGHT:
				{
					VistaVector3D tmp;
					m_pModel->GetPoint1(m_v3LEFT);
					m_pModel->GetPoint2(m_v3RIGHT);
					m_pModel->GetMidPoint(tmp);
					v3Pt2=tmp-((m_v3RIGHT-tmp).GetNormalized())*0.07f;
					m_v3SPINDLE = v3Pt2;
				}
			  break;
		}
		m_fLastDistance=(v3Pt2-v3Pt1).GetLength();
		m_bFirstMove=false;
	}
	else
	{
		VistaVector3D v3NewPos=oFrameData.v3Position+(v3Direktion.GetNormalized())*m_fLastDistance;
		m_pSphereHandles[m_iActiveHandle]->SetCenter(v3NewPos);
		switch(m_iActiveHandle)
		{
			case HND_SPHERE_LEFT:
				m_pModel->SetPoint1(v3NewPos);
			  break;
			case HND_SPHERE_RIGHT:
				m_pModel->SetPoint2(v3NewPos);
			  break;
			case HND_SPHERE_MIDI:
				{
					VistaVector3D v3Pt1, v3Pt2, v3Pt3;
					m_pModel->GetPoint1(v3Pt1);
					m_pModel->GetPoint2(v3Pt2);
					m_pModel->GetMidPoint(v3Pt3);
					m_pModel->SetMidPoint(v3NewPos);
					v3NewPos=v3NewPos-v3Pt3;
					m_pModel->SetPoint1(v3Pt1+v3NewPos);
					m_pModel->SetPoint2(v3Pt2+v3NewPos);
				}
			  break;
			case HND_SPHERE_SPINDLE_LEFT:
				{
					VistaVector3D v3Midle;
					VistaVector3D v3Pt1, v3Pt2, v3Spindle, v3normal;
					m_pModel->GetMidPoint(v3Midle);
					v3Pt1    =m_v3LEFT   -v3Midle;
					v3Pt2    =m_v3RIGHT  -v3Midle;
					v3Spindle=m_v3SPINDLE-v3Midle;
					v3NewPos =v3NewPos   -v3Midle;

					v3normal =v3Pt2.GetNormalized();
					v3Spindle=v3normal.Cross(v3Spindle.Cross(v3normal));
					v3NewPos =v3normal.Cross( v3NewPos.Cross(v3normal));

					float angle = acos(((v3Spindle).Dot(v3NewPos))/((v3Spindle).GetLength()*(v3NewPos).GetLength()));
					VistaQuaternion quaternion(VistaAxisAndAngle(v3normal,angle));

					if(((quaternion.Rotate(v3Spindle).GetNormalized())-(v3NewPos.GetNormalized())).GetLength()>0.01){
						angle=Vista::DegToRad(360.0f)-angle;
						quaternion=VistaQuaternion(VistaAxisAndAngle(v3normal,angle));
					}
					m_pModel->SetPoint1(v3Midle+quaternion.Rotate(v3Pt1));
				}
			  break;
			case HND_SPHERE_SPINDLE_RIGHT:
				{
					VistaVector3D v3Midle;
					VistaVector3D v3Pt1, v3Pt2, v3Spindle, v3normal;
					m_pModel->GetMidPoint(v3Midle);
					v3Pt1    =m_v3LEFT   -v3Midle;
					v3Pt2    =m_v3RIGHT  -v3Midle;
					v3Spindle=m_v3SPINDLE-v3Midle;
					v3NewPos =v3NewPos   -v3Midle;

					v3normal =v3Pt1.GetNormalized();
					v3Spindle=v3normal.Cross(v3Spindle.Cross(v3normal));
					v3NewPos =v3normal.Cross( v3NewPos.Cross(v3normal));

					float angle = acos(((v3Spindle).Dot(v3NewPos))/((v3Spindle).GetLength()*(v3NewPos).GetLength()));
					VistaQuaternion quaternion(VistaAxisAndAngle(v3normal,angle));

					if(((quaternion.Rotate(v3Spindle).GetNormalized())-(v3NewPos.GetNormalized())).GetLength()>0.01){
						angle=Vista::DegToRad(360.0f)-angle;
						quaternion=VistaQuaternion(VistaAxisAndAngle(v3normal,angle));
					}
					m_pModel->SetPoint2(v3Midle+quaternion.Rotate(v3Pt2));
				}
			  break;
		}
	}
}


/*============================================================================*/
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*============================================================================*/
void VfaGoniometerController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	if(iSlot != m_iDragButton)
		return;
	if(m_iState==STATE_DISABLED)
		return;

	if(!bSet)
	{
		if(m_iState==STATE_MOVING)
		{
			m_iState=STATE_ON_FOCUS;
			if (	m_iActiveHandle==HND_SPHERE_SPINDLE_LEFT 
				||	m_iActiveHandle==HND_SPHERE_SPINDLE_RIGHT)
			{
				m_pSphereHandles[m_iActiveHandle]->SetIsHighlighted(false);
			    m_iActiveHandle=HND_UNDEFINDED;
				m_iState=STATE_NOTHING_SELECTED;
			}
		}
	}
	else
	{
		if(m_iState==STATE_ON_FOCUS)
		{
			m_iState=STATE_MOVING;
			m_bFirstMove=true;
		}
	}
}

/*============================================================================*/
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*============================================================================*/
void VfaGoniometerController::OnTimeSlotUpdate(const double dTime)
{}

/*============================================================================*/
/*  NAME      :   OnFocus				                                      */
/*============================================================================*/
void VfaGoniometerController::OnFocus(IVfaControlHandle* pHandle)
{
	if (m_iState==STATE_DISABLED)
		return;
	if (m_iState==STATE_MOVING)
		return;
	
	if (m_pSphereHandles[HND_SPHERE_LEFT]==pHandle)
	{
		m_iActiveHandle=HND_SPHERE_LEFT;
		m_iState=STATE_ON_FOCUS;
		m_pSphereHandles[HND_SPHERE_LEFT]->SetIsHighlighted(true);
	}
	else if (m_pSphereHandles[HND_SPHERE_RIGHT]==pHandle)
	{
		m_iActiveHandle=HND_SPHERE_RIGHT;
		m_iState=STATE_ON_FOCUS;
		m_pSphereHandles[HND_SPHERE_RIGHT]->SetIsHighlighted(true);
	}
	else if (m_pSphereHandles[HND_SPHERE_MIDI]==pHandle)
	{
		m_iActiveHandle=HND_SPHERE_MIDI;
		m_iState=STATE_ON_FOCUS;
		m_pSphereHandles[HND_SPHERE_MIDI]->SetIsHighlighted(true);
	}
	else if (m_pSphereHandles[HND_SPHERE_SPINDLE_LEFT]==pHandle)
	{
		m_iActiveHandle=HND_SPHERE_SPINDLE_LEFT;
		m_iState=STATE_ON_FOCUS;
		m_pSphereHandles[HND_SPHERE_SPINDLE_LEFT]->SetIsHighlighted(true);
	}
	else if (m_pSphereHandles[HND_SPHERE_SPINDLE_RIGHT]==pHandle)
	{
		m_iActiveHandle=HND_SPHERE_SPINDLE_RIGHT;
		m_iState=STATE_ON_FOCUS;
		m_pSphereHandles[HND_SPHERE_SPINDLE_RIGHT]->SetIsHighlighted(true);
	}
	else
	{
		m_iActiveHandle=HND_UNDEFINDED;
		m_iState=STATE_NOTHING_SELECTED;
	}
}

/*============================================================================*/
/*  NAME      :   OnUnfocus				                                      */
/*============================================================================*/
void VfaGoniometerController::OnUnfocus()
{

	if (	m_iState==STATE_DISABLED
		||	m_iState==STATE_MOVING
		||	m_iActiveHandle==HND_UNDEFINDED)
		return;
	
	m_pSphereHandles[m_iActiveHandle]->SetIsHighlighted(false);
	m_iActiveHandle=HND_UNDEFINDED;
	m_iState=STATE_NOTHING_SELECTED;
}

/*============================================================================*/
/*  NAME      :   GetSensorMask/CommandMask                                   */
/*============================================================================*/
int VfaGoniometerController::GetSensorMask() const
{
	return (1<<VfaApplicationContextObject::SLOT_POINTER_VIS);
}
int VfaGoniometerController::GetCommandMask() const
{
	return(1<<m_iDragButton);
}
/*============================================================================*/
/*  NAME      :   GetTimeUpdate				                                  */
/*============================================================================*/
bool VfaGoniometerController::GetTimeUpdate() const
{
	return true;
}
/*============================================================================*/
/*  NAME      :   Set/GetIsEnabled                                            */
/*============================================================================*/
void VfaGoniometerController::SetIsEnabled(bool b)
{
	m_pSphereHandles[HND_SPHERE_LEFT ]->SetEnable(b);
	m_pSphereHandles[HND_SPHERE_RIGHT]->SetEnable(b);
	m_pSphereHandles[HND_SPHERE_MIDI ]->SetEnable(b);
	m_pSphereHandles[HND_SPHERE_SPINDLE_LEFT ]->SetEnable(b);
	m_pSphereHandles[HND_SPHERE_SPINDLE_RIGHT]->SetEnable(b);

	m_pSphereHandles[HND_SPHERE_LEFT ]->SetVisible(b);
	m_pSphereHandles[HND_SPHERE_RIGHT]->SetVisible(b);
	m_pSphereHandles[HND_SPHERE_MIDI ]->SetVisible(b);
	m_pSphereHandles[HND_SPHERE_SPINDLE_LEFT ]->SetVisible(b);
	m_pSphereHandles[HND_SPHERE_SPINDLE_RIGHT]->SetVisible(b);

	m_p3DTextUserAligned->SetVisible(b);

	if(!b)
	{
		m_iState=STATE_DISABLED;
		return;
	}
	m_iState=STATE_NOTHING_SELECTED;
	m_pModel->Notify(VfaGoniometerModel::MSG_GONIOMETER_CHANGE);
}
bool VfaGoniometerController::GetIsEnabled() const
{
	return m_iState==STATE_DISABLED;
}

/*============================================================================*/
/*  NAME    :   Get/SetShowText                                               */
/*============================================================================*/
void VfaGoniometerController::SetShowText(bool b){
	m_bShowText=b;
	m_p3DTextUserAligned->SetVisible(b);
}

bool VfaGoniometerController::GetShowText() const
{
	return m_bShowText;
}

/*============================================================================*/
/*  NAME    :   Get/SetTextSize                                               */
/*============================================================================*/
void VfaGoniometerController::SetTextSize(float f){
	if (m_p3DTextUserAligned != NULL)
	{
		m_p3DTextUserAligned->SetTextSize(f);
	}
}

float VfaGoniometerController::GetTextSize() const
{
	if (m_p3DTextUserAligned != NULL)
	{
		return m_p3DTextUserAligned->GetTextSize();
	}
	return 0.0f;
}
/*============================================================================*/
/*  NAME    :   Get/SetDragButton                                             */
/*============================================================================*/
void VfaGoniometerController::SetDragButton(int i)
{
	m_iDragButton=i;
}
int VfaGoniometerController::GetDragButton()const
{
	return m_iDragButton;
}
/*============================================================================*/
/*  NAME    :    ObserverUpdate                                               */
/*============================================================================*/
void VfaGoniometerController::ObserverUpdate(IVistaObserveable *pObservable,
											  int iMsg, int iTicket)
{

	if(iMsg != VfaGoniometerModel::MSG_GONIOMETER_CHANGE)
		return;

	VistaVector3D v3Pt1, v3Pt2,v3Pt3,v3Pt4,v3Pt5;
	m_pModel->GetPoint1(v3Pt1);
	m_pModel->GetPoint2(v3Pt2);
	m_pModel->GetMidPoint(v3Pt3);
	v3Pt4=v3Pt3-((v3Pt1-v3Pt3).GetNormalized())*0.07f;
	v3Pt5=v3Pt3-((v3Pt2-v3Pt3).GetNormalized())*0.07f;

	m_pSphereHandles[HND_SPHERE_LEFT]->SetCenter(v3Pt1);
	m_pSphereHandles[HND_SPHERE_RIGHT]->SetCenter(v3Pt2);
	m_pSphereHandles[HND_SPHERE_MIDI]->SetCenter(v3Pt3);
	m_pSphereHandles[HND_SPHERE_SPINDLE_LEFT]->SetCenter(v3Pt4);
	m_pSphereHandles[HND_SPHERE_SPINDLE_RIGHT]->SetCenter(v3Pt5);

	if(m_bShowText)
	{
		char str[255];
		sprintf(str,"    Angle: %.2f�",m_pModel->GetAngleD());
		m_p3DTextUserAligned->SetText(str);
		float fPos[3] = {v3Pt3[0], v3Pt3[1], v3Pt3[2]};
		m_p3DTextUserAligned->SetPosition(fPos);
	}
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
