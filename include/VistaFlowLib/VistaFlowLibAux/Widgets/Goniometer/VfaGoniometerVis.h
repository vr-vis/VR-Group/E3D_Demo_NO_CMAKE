#ifndef _GONIOMETERVIS_H
#define _GONIOMETERVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaVector3D;
class VistaColor;
class VfaGoniometerModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A simple line defined by two points
 */
class VISTAFLOWLIBAUXAPI VfaGoniometerVis : public IVflRenderable
{
public: 
	explicit VfaGoniometerVis(VfaGoniometerModel *pModel);
	virtual ~VfaGoniometerVis();

	VfaGoniometerModel *GetModel() const;

	virtual void DrawOpaque();
	virtual unsigned int GetRegistrationMode() const;

	class VISTAFLOWLIBAUXAPI CGoniometerVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:

		enum
		{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_LAST
		};

		CGoniometerVisProps();
		virtual ~CGoniometerVisProps();

		void SetWidth(float f);
		float GetWidth() const;

		void SetLineColor(float fColor[4]);
		void SetLineColor(const VistaColor& color);
		void GetLineColor(float fColor[4]) const;
		VistaColor GetLineColor() const;

		void SetAngleColor(float fColor[4]);
		void SetAngleColor(const VistaColor& color);
		void GetAngleColor(float fColor[4]) const;
		VistaColor GetAngleColor() const;

	protected:
	private:
		float m_fLineWidth;
		float m_fLineColor[4];
		float m_fAngleColor[4];
	};
	virtual CGoniometerVisProps *GetProperties() const;
protected:
	virtual VflRenderableProperties* CreateProperties() const;

private:
	VfaGoniometerModel *m_pModel;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
