

set( RelativeDir "./Widgets/Goniometer" )
set( RelativeSourceGroup "Source Files\\Widgets\\Goniometer" )

set( DirFiles
	VfaGoniometerController.cpp
	VfaGoniometerController.h
	VfaGoniometerModel.cpp
	VfaGoniometerModel.h
	VfaGoniometerVis.cpp
	VfaGoniometerVis.h
	VfaGoniometerWidget.cpp
	VfaGoniometerWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

