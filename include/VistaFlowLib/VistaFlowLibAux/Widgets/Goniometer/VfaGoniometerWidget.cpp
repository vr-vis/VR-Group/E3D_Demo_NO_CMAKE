#include <cstring>
#include "VfaGoniometerWidget.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaGoniometerWidget::VfaGoniometerWidget(VflRenderNode *pRenderNode)
						:	IVfaWidget(pRenderNode),
							m_pModel(new VfaGoniometerModel)
{
	m_pCtr = new VfaGoniometerController(m_pModel, pRenderNode);

	m_pVis = new VfaGoniometerVis(m_pModel);

	m_pVis->Init();
	pRenderNode->AddRenderable(m_pVis);
}

VfaGoniometerWidget::~VfaGoniometerWidget()
{
	delete m_pCtr;
	delete m_pModel;
}

/*============================================================================*/
/*  NAME: Set/GetIsEndabled                                                   */
/*============================================================================*/
void VfaGoniometerWidget::SetIsEnabled(bool b)
{
	m_pCtr->SetIsEnabled(b);
	if (b)
		m_pVis->SetVisible(true);	
}
bool VfaGoniometerWidget::GetIsEnabled() const
{
	return m_pCtr->GetIsEnabled();
}

/*============================================================================*/
/*  NAME: Set/GetIsVisible                                                    */
/*============================================================================*/
void VfaGoniometerWidget::SetIsVisible(bool b)
{
	m_pVis->SetVisible(b);
	if (!b)
		m_pCtr->SetIsEnabled(false);
}
bool VfaGoniometerWidget::GetIsVisible() const
{
	return m_pVis->GetVisible();
}

/*============================================================================*/
/*  NAME: GetModel/View/Controller	                                          */
/*============================================================================*/
VfaGoniometerModel *VfaGoniometerWidget::GetModel() const
{
	return m_pModel;
}
VfaGoniometerVis *VfaGoniometerWidget::GetView() const
{
	return m_pVis;
}
VfaGoniometerController *VfaGoniometerWidget::GetController() const
{
	return m_pCtr;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
