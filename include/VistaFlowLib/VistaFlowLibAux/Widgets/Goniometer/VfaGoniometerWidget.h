#ifndef _GONIOMETERWIDGET_H
#define _GONIOMETERWIDGET_H

#include <VistaFlowLibAux/Widgets/VfaWidget.h>
#include <VistaFlowLibAux/Widgets/VfaRulerVis.h>
#include "VfaGoniometerVis.h"
#include "VfaGoniometerModel.h"
#include "VfaGoniometerController.h"
#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaGoniometerWidget : public IVfaWidget
{
public:


	VfaGoniometerWidget(VflRenderNode *pRenderNode);
	virtual ~VfaGoniometerWidget();

	/** 
	 * NOTE : Handle visibility and reaction to events separately.
	 *        Thus the widget may be visible but not being interacted with!
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;

	VfaGoniometerModel		*GetModel() const;
	VfaGoniometerVis			*GetView() const;
	VfaGoniometerController	*GetController() const;

private:
	VfaGoniometerController	*m_pCtr;
	VfaGoniometerVis			*m_pVis;
	VfaGoniometerModel		*m_pModel;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
