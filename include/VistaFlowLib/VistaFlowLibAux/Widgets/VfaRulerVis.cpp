/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <GL/glew.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include "VfaRulerVis.h"
#include "VfaLineVis.h"
#include "Line/VfaLineModel.h"

#include <cassert>
#include <cstring>
#include <sstream>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaRulerVis                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaRulerVis::VfaRulerVis()
	:	m_pModel(NULL),
		m_strUnit("")
{}

VfaRulerVis::VfaRulerVis(VfaLineModel *pModel, VflRenderNode *pRenderNode)
	:	m_pModel(pModel),
		m_pRenderNode(pRenderNode)
{
	m_pLineVis = new VfaLineVis(m_pModel);
	m_pLineVis->Init();

	m_pText = new Vfl3DTextLabel;
	if(!m_pText->Init())
		return;
	m_pText->SetTextFollowViewDir(true);
	m_pText->SetRenderNode(m_pRenderNode);
	m_pText->SetTextSize(0.05f);
	m_pText->SetText("");
}

VfaRulerVis::~VfaRulerVis()
{
	delete m_pLineVis;
	delete m_pText;
}

/*============================================================================*/
/*  NAME      :   GetModel	                                                  */
/*============================================================================*/
VfaLineModel *VfaRulerVis::GetModel() const
{
	return m_pModel;
}

/*============================================================================*/
/*  NAME      :   Update                                                      */
/*============================================================================*/
void VfaRulerVis::Update()
{
	m_pText->Update();
}

/*============================================================================*/
/*  NAME      :   SetText                                                     */
/*============================================================================*/
void VfaRulerVis::SetText(const std::string &strText)
{
	m_pText->SetText(strText);
}

/*============================================================================*/
/*  NAME      :   SetScaleUnit                                                */
/*============================================================================*/
void VfaRulerVis::SetScaleUnit(const std::string &strUnit)
{
	m_strUnit = strUnit;
}
/*============================================================================*/
/*  NAME      :   DrawOpaque                                                  */
/*============================================================================*/
void VfaRulerVis::DrawOpaque()
{
	VfaRulerVis::VfaRulerVisProperties *pProps = dynamic_cast
					<VfaRulerVis::VfaRulerVisProperties*>(this->GetProperties());

	if(pProps == NULL)
		return;

	//set new parameter to line and text, and draw line
	float fC[4];	pProps->GetColor(fC);
	m_pLineVis->GetProperties()->SetColor(fC);
	m_pLineVis->GetProperties()->SetWidth(pProps->GetWidth());

	float fPt1[3],fPt2[3];
	m_pModel->GetPoint1(fPt1);
	m_pModel->GetPoint2(fPt2);

	m_pLineVis->GetModel()->SetPoint1(fPt1);
	m_pLineVis->GetModel()->SetPoint2(fPt2);

	m_pLineVis->DrawOpaque();

	this->InternalUpdateText();
	m_pText->DrawOpaque();
}

/*============================================================================*/
/*  NAME      :   InternalUpdateText                                          */
/*============================================================================*/
void VfaRulerVis::InternalUpdateText()
{
	VfaRulerVis::VfaRulerVisProperties *pProps = dynamic_cast
					<VfaRulerVis::VfaRulerVisProperties*>(this->GetProperties());

	// only three decimal places should be visible
	int iLength = (int) (m_pModel->GetLength()*1000);
	float fLength = iLength/1000.0f;

	if(fLength == 0.0f)
		return;

	std::ostringstream os;
	os << fLength;

	m_pText->SetTextSize(pProps->GetTextSize());
	m_pText->SetText(os.str() + " " + m_strUnit);

	// position text:
	VistaVector3D v3Min, v3Max, v3MP;
	m_pText->GetBounds(v3Min, v3Max);
	m_pModel->GetMidPoint(v3MP);

	m_pText->SetPosition(v3MP);

}

/*============================================================================*/
/*  NAME      :   GetRegistrationMode                                         */
/*============================================================================*/
unsigned int VfaRulerVis::GetRegistrationMode() const
{
	//return IVflRenderable::OLI_DRAW_OPAQUE | IVflRenderable::OLI_UPDATE;
	return m_pText->GetRegistrationMode() | IVflRenderable::OLI_DRAW_OPAQUE;
}


/*============================================================================*/
/*  NAME      :   GetProperties                                               */
/*============================================================================*/
VfaRulerVis::VfaRulerVisProperties *VfaRulerVis::GetProperties() const
{
	return static_cast<VfaRulerVis::VfaRulerVisProperties *>(
		IVflRenderable::GetProperties());
}

/*============================================================================*/
/*  NAME      :   CreateProperties                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaRulerVis::CreateProperties() const
{
	return new VfaRulerVis::VfaRulerVisProperties;
}


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaRulerVis::VfaRulerVisProperties::VfaRulerVisProperties()
	: m_fTextSize (0.05f),
	  m_fLineWidth(0.5f)
{
	// blue border
	m_fColor[0] = m_fColor[1] = 0.0f;
	m_fColor[2] = 1.0f;
}
VfaRulerVis::VfaRulerVisProperties::~VfaRulerVisProperties()
{}

/*============================================================================*/
/*  NAME      :   Set/GetWidth                                                */
/*============================================================================*/
void VfaRulerVis::VfaRulerVisProperties::SetWidth(float f)
{
	m_fLineWidth = f;
}
float VfaRulerVis::VfaRulerVisProperties::GetWidth() const
{
	return m_fLineWidth;
}

/*============================================================================*/
/*  NAME      :   Set/GetColor                                                */
/*============================================================================*/
bool VfaRulerVis::VfaRulerVisProperties::SetColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	return this->SetColor(fColor);
}

VistaColor VfaRulerVis::VfaRulerVisProperties::GetColor() const
{
	return VistaColor(m_fColor);
}

bool VfaRulerVis::VfaRulerVisProperties::SetColor(float fColor[4])
{
	if(	fColor[0] == m_fColor[0] &&
		fColor[1] == m_fColor[1] &&
		fColor[2] == m_fColor[2] &&
		fColor[3] == m_fColor[3])
	{
		return false;
	}

	memcpy(m_fColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHG);
	return true;
}

void VfaRulerVis::VfaRulerVisProperties::GetColor(float fColor[4]) const
{
	memcpy(fColor, m_fColor, 4*sizeof(float));
}

/*============================================================================*/
/*  NAME      :   Get/SetTextSize                                             */
/*============================================================================*/
void VfaRulerVis::VfaRulerVisProperties::SetTextSize(float f)
{
	m_fTextSize = f;
}
float VfaRulerVis::VfaRulerVisProperties::GetTextSize() const
{
	return m_fTextSize;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/
/*============================================================================*/
/*  END OF FILE "VfaRulerVis.cpp"                                             */
/*============================================================================*/



