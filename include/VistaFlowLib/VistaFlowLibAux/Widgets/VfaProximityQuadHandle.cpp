/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <cstring>

#include "VfaProximityQuadHandle.h"

#include "VfaWidgetTools.h"
#include "VfaQuadVis.h"
#include "Plane/VfaPlaneModel.h"
#include <VistaOGLExt/VistaTexture.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaProximityQuadHandle::VfaProximityQuadHandle(VflRenderNode *pRN)
	:	IVfaProximityHandle(IVfaControlHandle::HANDLE_PROXIMITY_QUAD),
		m_pVis(new VfaQuadVis(new VfaPlaneModel)),
		m_bDrawTransparentHighlight(true),
		m_bFillPlane(false)
{
	//define default appearence
	m_fNormalLW = 1.0f;
	m_fHighlightLW = 3.0f;
	m_fNormalColor[0] = m_fNormalColor[1] = 0.0f;
	m_fNormalColor[2] = m_fNormalColor[3] = 1.0f;
	m_fHighlightColor[0] = m_fHighlightColor[1] = 0.4f;
	m_fHighlightColor[2] = m_fHighlightColor[3] = 1.0f;

	//init a box visualization for the handle stuff
	m_pVis->Init();
	pRN->AddRenderable(m_pVis);

	VfaQuadVis::VfaQuadVisProps *pProps = m_pVis->GetProperties();
	pProps->SetDrawBorder(true);
	pProps->SetFillPlane(m_bFillPlane);
	pProps->SetLineColor(m_fNormalColor);
	pProps->SetFillColor(m_fNormalColor);
	pProps->SetLineWidth(m_fNormalLW);
	pProps->SetOpacity(0.4f);
}

VfaProximityQuadHandle::~VfaProximityQuadHandle()
{
	m_pVis->GetRenderNode()->RemoveRenderable(m_pVis);
	delete m_pVis;
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaProximityQuadHandle  		   			          */
/*============================================================================*/
void VfaProximityQuadHandle::SetIsHighlighted(bool bIsHighlighted)
{
	//if we are not visible(for whatever reason) -> ignore it
	if(	!m_pVis->GetProperties()->GetVisible() ||
		!this->GetVisEnabled())
	{
		return;
	}
	
	VfaQuadVis::VfaQuadVisProps *pProps = m_pVis->GetProperties();
	if(bIsHighlighted)
	{
		pProps->SetFillPlane(true|m_bFillPlane);
		pProps->SetFillColor(m_fHighlightColor);
		pProps->SetLineWidth(m_fHighlightLW);
	}
	else
	{
		pProps->SetFillPlane(false|m_bFillPlane);
		pProps->SetFillColor(m_fNormalColor);
		pProps->SetLineWidth(m_fNormalLW);
	}
}

void VfaProximityQuadHandle::SetVisEnabled(bool b)
{
	m_pVis->SetVisible(b);
}

void VfaProximityQuadHandle::SetPoint(float fPt[3])
{
	m_pVis->GetModel()->SetTranslation(fPt);
}
void VfaProximityQuadHandle::SetPoint(double dPt[3])
{
	m_pVis->GetModel()->SetTranslation(dPt);
}
void VfaProximityQuadHandle::SetPoint(const VistaVector3D& v3Pt)
{
	m_pVis->GetModel()->SetTranslation(v3Pt);
}

void VfaProximityQuadHandle::GetPoint(float fPt[3]) const
{
	m_pVis->GetModel()->GetTranslation(fPt);
}
void VfaProximityQuadHandle::GetPoint(double dPt[3]) const
{
	m_pVis->GetModel()->GetTranslation(dPt);
}

void VfaProximityQuadHandle::GetPoint(VistaVector3D &v3Pt) const
{
	m_pVis->GetModel()->GetTranslation(v3Pt);
}

void VfaProximityQuadHandle::SetNormal(float fNormal[3])
{
	m_pVis->GetModel()->SetNormal(fNormal);
}
void VfaProximityQuadHandle::SetNormal(double dNormal[3])
{
	m_pVis->GetModel()->SetNormal(dNormal);
}

void VfaProximityQuadHandle::SetNormal(const VistaVector3D& v3Normal)
{
	m_pVis->GetModel()->SetNormal(v3Normal);
}

void VfaProximityQuadHandle::GetNormal(float fNormal[3]) const
{
	m_pVis->GetModel()->GetNormal(fNormal);
}
void VfaProximityQuadHandle::GetNormal(double dNormal[3]) const
{
	m_pVis->GetModel()->GetNormal(dNormal);
}
void VfaProximityQuadHandle::GetNormal(VistaVector3D& v3Normal) const
{
	m_pVis->GetModel()->GetNormal(v3Normal);
}

bool VfaProximityQuadHandle::SetRotation(const VistaQuaternion &qRotation)
{
	return m_pVis->GetModel()->SetRotation(qRotation);
}
void VfaProximityQuadHandle::GetRotation(VistaQuaternion &qRot) const
{
	m_pVis->GetModel()->GetRotation(qRot);
}

void VfaProximityQuadHandle::SetExtent(float fW, float fH)
{
	m_pVis->GetModel()->SetExtents(fW, fH);
}

void VfaProximityQuadHandle::GetExtent(float& fWidth, float& fHeight) const
{
	m_pVis->GetModel()->GetExtents(fWidth, fHeight);
}

bool VfaProximityQuadHandle::IsTouched(const VistaVector3D & v3Pos) const
{
	VistaVector3D v3Center, v3Normal;
	m_pVis->GetModel()->GetTranslation(v3Center);
	m_pVis->GetModel()->GetNormal(v3Normal);
	//1) check if pt is in range 
	//   (NOTE: we can only get close to the plane from the "outside")
	float fDist = (v3Pos-v3Center) * v3Normal;
	bool bInRange = (fDist > 0) && (fDist < this->GetProximityDistance());
	/*
	if(bInRange && m_pVis->GetPlaneModel()->IsProjectionInside(v3Pos))
		vstr::outi()<<"plane touched, range = "<<fDist<<endl;
	*/
	//2) check if projection of p is inside the quad
	return bInRange && m_pVis->GetModel()->IsProjectionInside(v3Pos);
}

void VfaProximityQuadHandle::SetHighlightLineWidth(float f)
{
	m_fHighlightLW = f;
}

float VfaProximityQuadHandle::GetHighlightLineWidth() const
{
	return m_fHighlightLW;
}

void VfaProximityQuadHandle::SetNormalLineWidth(float f)
{
	m_fNormalLW = f;
}

float VfaProximityQuadHandle::GetNormalLineWidth() const
{
	return m_fNormalLW;
}

void VfaProximityQuadHandle::SetHighlightColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	this->SetHighlightColor(fColor);
}

VistaColor VfaProximityQuadHandle::GetHighlightColor() const
{
	VistaColor color(m_fHighlightColor);
	return color;
}

void VfaProximityQuadHandle::SetHighlightColor(float fC[4])
{
	memcpy(m_fHighlightColor, fC, 4*sizeof(float));
}

void VfaProximityQuadHandle::GetHighlightColor(float fC[4]) const
{
	memcpy(fC, m_fHighlightColor, 4*sizeof(float));
}

void VfaProximityQuadHandle::SetNormalColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	this->SetNormalColor(fColor);
}

VistaColor VfaProximityQuadHandle::GetNormalColor() const
{
	VistaColor color(m_fNormalColor);
	return color;
}
void VfaProximityQuadHandle::SetNormalColor(float fC[4])
{
	memcpy(m_fNormalColor, fC, 4*sizeof(float));
}

void VfaProximityQuadHandle::GetNormalColor(float fC[4]) const
{
	memcpy(fC, m_fNormalColor, 4*sizeof(float));
}


void VfaProximityQuadHandle::SetLineColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	this->SetLineColor(fColor);
}

VistaColor VfaProximityQuadHandle::GetLineColor() const
{
	VistaColor color(m_fLineColor);
	return color;
}
void VfaProximityQuadHandle::SetLineColor(float fC[4])
{
	memcpy(m_fLineColor, fC, 4*sizeof(float));

	VfaQuadVis::VfaQuadVisProps *pProps = m_pVis->GetProperties();
	pProps->SetLineColor(fC);
}

void VfaProximityQuadHandle::GetLineColor(float fC[4]) const
{
	memcpy(fC, m_fLineColor, 4*sizeof(float));
}

void VfaProximityQuadHandle::SetDrawTransparentHighlight(bool b)
{
	m_bDrawTransparentHighlight = b;
}

bool VfaProximityQuadHandle::GetDrawTransparentHighlight() const
{
	return m_bDrawTransparentHighlight;
}

void VfaProximityQuadHandle::SetTexture(VistaTexture *pTex)
{
	m_pVis->SetTexture(pTex);
}
VistaTexture *VfaProximityQuadHandle::GetTexture() const
{
	return m_pVis->GetTexture();
}

bool VfaProximityQuadHandle::SetFillPlane(bool bFill)
{	
	
	if (m_bFillPlane == bFill)
		return false;

	m_bFillPlane = bFill;
	VfaQuadVis::VfaQuadVisProps *pProps = m_pVis->GetProperties();
	pProps->SetFillPlane(m_bFillPlane);
	return true;
}

bool VfaProximityQuadHandle::GetFillPlane()
{
	return m_bFillPlane;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCorner                                                   */
/*                                                                            */
/*============================================================================*/
void VfaProximityQuadHandle::GetCorner(unsigned char i, float fCorner[3]) const
{
	m_pVis->GetModel()->GetCorner(i, fCorner);
}

void VfaProximityQuadHandle::GetCorner(unsigned char i, VistaVector3D& v3Corner) const
{
	m_pVis->GetModel()->GetCorner(i, v3Corner);
}



/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "IVfaProximityQuadHandle.cpp"                                  */
/*============================================================================*/

