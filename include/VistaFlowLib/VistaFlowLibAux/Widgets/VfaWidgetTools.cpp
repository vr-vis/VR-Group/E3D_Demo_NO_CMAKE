/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaWidgetTools.h"

#include <VistaBase/VistaVectorMath.h>

#include <algorithm>
#include <cassert>

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaWidgetTools::VfaWidgetTools()
{
}

VfaWidgetTools::~VfaWidgetTools()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDistanceRayPoint                                         */
/*                                                                            */
/*============================================================================*/
float VfaWidgetTools::GetDistanceRayPoint(const VistaVector3D &v3RayPoint, 
										   const VistaQuaternion &qRayOri, 
										   const VistaVector3D& v3Point)
{
	/*                                                                 
	 *       *\  v3Point                                                              
	 *       | \                                                        
	 *       |  \  -> v3RayToPoint                                                       
	 *       |   \                                                      
	 *       *----* v3RayPoint
	 *         ^
	 *         |
	 *        qRayOri/v3Dir   ||v3Dir|| = fRayProjection = v3Dir * v3RayToPoint
	 */

	//get direction given by the quaternion
	VistaVector3D v3Dir(qRayOri.Rotate(VistaVector3D(0,0,-1)));
	v3Dir.Normalize();
	VistaVector3D v3RayToPoint(v3Point-v3RayPoint);
	float fRayProjection = v3Dir * v3RayToPoint;
	//if angle is >90� return directly(we only test rays)
	if(fRayProjection <= 0)
		return false;
	float fDistToPt = v3RayToPoint.GetLength();
	//determine distance between ray and point
	return sqrt((fDistToPt-fRayProjection) *(fDistToPt + fRayProjection));
}

void VfaWidgetTools::ClampPositionToBounds(float fPos[3], float fConstraint[6])
{
	//just make sure we are working with proper input
	/* //NOTE: These assertions may fire due to rounding errors e.g. for 
	   //cases where fConstraint[2*i](almost)= fConstraint[2*i+1]
	assert(	fConstraint[0] <= fConstraint[1] &&
			fConstraint[2] <= fConstraint[3] &&
			fConstraint[4] <= fConstraint[5] );
	*/
	fPos[0] = std::min<float>(std::max(fConstraint[0], fPos[0]), fConstraint[1]);
	fPos[1] = std::min<float>(std::max(fConstraint[2], fPos[1]), fConstraint[3]);
	fPos[2] = std::min<float>(std::max(fConstraint[4], fPos[2]), fConstraint[5]);
}

void VfaWidgetTools::ClampBoxToBounds(float fBox[6], float fConstraint[6])
{
	//just make sure we are working with proper input
	/*
	assert(	fConstraint[0] <= fConstraint[1] &&
			fConstraint[2] <= fConstraint[3] &&
			fConstraint[4] <= fConstraint[5] );
	*/
	fBox[0] = std::min<float>(std::max(fConstraint[0], fBox[0]), fConstraint[1]);
	fBox[1] = std::min<float>(std::max(fConstraint[0], fBox[1]), fConstraint[1]);
	fBox[2] = std::min<float>(std::max(fConstraint[2], fBox[2]), fConstraint[3]);
	fBox[3] = std::min<float>(std::max(fConstraint[2], fBox[3]), fConstraint[3]);
	fBox[4] = std::min<float>(std::max(fConstraint[4], fBox[4]), fConstraint[5]);
	fBox[5] = std::min<float>(std::max(fConstraint[4], fBox[5]), fConstraint[5]);
}

/*============================================================================*/
/*  END OF FILE "VfaWidgetTools.cpp"                                          */
/*============================================================================*/
