/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACYLINDERVIS_H
#define _VFACYLINDERVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaCylinderModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Cylinder is composed of Cylinder and cylinder and can be used for showing direction,  
 * for example the normal of a plane.
 */
class VISTAFLOWLIBAUXAPI VfaCylinderVis : public IVflRenderable
{
public: 
	VfaCylinderVis(VfaCylinderModel *pModel);
	virtual ~VfaCylinderVis();

	/**
	* here the rendering is done
	*/
	virtual void DrawOpaque();


	virtual unsigned int GetRegistrationMode() const;

	VfaCylinderModel *GetModel() const;

	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);

	bool SetCenter(const VistaVector3D v3Center);
	bool SetCenter(float fCenter[3]);
	bool SetCenter(double dCenter[3]);

	void GetCenter(float fCenter[3]) const;
	void GetCenter(double dCenter[3]) const;
	void GetCenter(VistaVector3D v3Center) const;

	bool SetHeight(float f);
	float GetHeight () const;

	bool SetRadius(float f);
	float GetRadius() const;

	void SetRotate(const VistaVector3D &v3Normal);
	void GetRotate(float fRotate[4]) const;

	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);
	
	class VISTAFLOWLIBAUXAPI VfaCylinderVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		enum{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_SOLID_CHG,
			MSG_LAST
		};

		VfaCylinderVisProps();
		virtual ~VfaCylinderVisProps();

		bool SetColor(float fColor[4]);
		bool SetColor(const VistaColor& color);

		void GetColor(float fColor[4]) const;
		VistaColor GetColor() const;

		bool SetUseLighting(bool b);
		bool GetUseLighting() const;

	protected:

	private:
		float m_fColor[4];
		bool m_bUseLighting;
	};

	virtual VfaCylinderVisProps *GetProperties() const;
	
protected:
	virtual VflRenderableProperties* CreateProperties() const;

	GLUquadricObj *quadratic;

private:
	VistaVector3D m_v3Center;
	float m_fRotate[4];
	
	//Cylinder properties
	float m_fCylinderRadius;
	float m_fCylinderHeight;

	VfaCylinderModel *m_pModel;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaCylinderVis_H
