/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaTransformableWidgetModel.h"

#include <cstring>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaInterProcComm/Concurrency/VistaSemaphore.h>
#include <VistaAspects/VistaTransformable.h>
#include <VistaBase/VistaVectorMath.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

static const string STR_REF_TYPENAME("VfaTransformableWidgetModel");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyArrayGet<VfaTransformableWidgetModel, float, 3>("POSITION", STR_REF_TYPENAME, &VfaTransformableWidgetModel::GetPosition),
	new TVistaPropertyArrayGet<VfaTransformableWidgetModel, float, 4>("ORIENTATION", STR_REF_TYPENAME, &VfaTransformableWidgetModel::GetOrientation),
	NULL
};
//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertyArraySet<VfaTransformableWidgetModel, float, 3>("POSITION", STR_REF_TYPENAME, &VfaTransformableWidgetModel::SetPosition),
	new TVistaPropertyArraySet<VfaTransformableWidgetModel, float, 4>("ORIENTATION", STR_REF_TYPENAME, &VfaTransformableWidgetModel::SetOrientation),
	NULL 
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTransformableWidgetModel::VfaTransformableWidgetModel()
:VfaWidgetModelBase(),
m_bIgnoreWidgetTransform(true),
m_pRenderNode(NULL),
m_pWidgetParent(NULL),
m_fScale(1.0f)
{
	m_fPosition[0] = 0.0f;
	m_fPosition[1] = 0.0f;
	m_fPosition[2] = 0.0f;

	m_fOrientation[0] = 0.0f;
	m_fOrientation[1] = 0.0f;
	m_fOrientation[2] = 0.0f;
	m_fOrientation[3] = 1.0f;

}

VfaTransformableWidgetModel::~VfaTransformableWidgetModel()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRenderNode												  */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::SetRenderNode(VflRenderNode* pRN)
{
	m_pRenderNode = pRN;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetWidgetParent                                             */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::SetWidgetParent(IVistaTransformable* pT)
{
	m_pWidgetParent = pT;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SyncTransforms                                              */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::SyncTransforms()
{
	bool bChange = false;
	if(m_pWidgetParent)
	{
		VistaVector3D v3Translation;
		m_pWidgetParent->GetTranslation(v3Translation[0], v3Translation[1], v3Translation[2]);
		m_oWidgetRefFrame.SetTranslation(v3Translation);

		VistaQuaternion qRot;
		m_pWidgetParent->GetRotation(qRot[0], qRot[1], qRot[2], qRot[3]);
		m_oWidgetRefFrame.SetRotation(qRot);

		// assume uniform scale
		float fScale[3];
		m_pWidgetParent->GetScale(fScale);
		m_oWidgetRefFrame.SetScale(fScale[0]);
		bChange = true;
	}

	if(m_pRenderNode)
	{
		m_oVisRefFrame.SetTranslation(m_pRenderNode->GetTranslation());
		m_oVisRefFrame.SetRotation(m_pRenderNode->GetRotation());

		// assume uniform scale
		float fScale;
		m_pRenderNode->GetScaleUniform(fScale);
		m_oVisRefFrame.SetScale(fScale);
		bChange = true;
	}

	if(bChange)
	{
		this->Notify(MSG_TRANSFORM_CHANGE);
	}

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPosition                                                 */
/*                                                                            */
/*============================================================================*/
bool VfaTransformableWidgetModel::SetPosition(float fPosition[3])
{
	memcpy(m_fPosition, fPosition, sizeof(float)*3);
	this->Notify(MSG_POSITION_CHANGE);
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPosition                                                 */
/*                                                                            */
/*============================================================================*/
bool VfaTransformableWidgetModel::SetPosition(double dC[3])
{
	float f[3] = {(float)dC[0], (float)dC[1], (float)dC[2]};
	return this->SetPosition(f);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPosition                                                 */
/*                                                                            */
/*============================================================================*/
bool VfaTransformableWidgetModel::SetPosition(const VistaVector3D& v3C)
{
	float f[3];
	v3C.GetValues(f);
	return this->SetPosition(f);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPosition                                                 */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::GetPosition(float fPosition[3]) const
{
	memcpy(fPosition, m_fPosition, sizeof(float)*3);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPosition                                                 */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::GetPosition(double dPosition[3]) const
{
	float f[3];
	this->GetPosition(f);
	dPosition[0] = f[0]; dPosition[1] = f[1]; dPosition[2] = f[2]; 
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPosition                                                 */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::GetPosition(VistaVector3D &v3C) const
{
	float f[3];
	this->GetPosition(f);
	v3C[0] = f[0]; v3C[1] = f[1]; v3C[2] = f[2]; 
}

VistaVector3D VfaTransformableWidgetModel::GetPosition() const
{
	return VistaVector3D( m_fPosition );
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetOrientation                                              */
/*                                                                            */
/*============================================================================*/

bool VfaTransformableWidgetModel::SetOrientation(float fOrientation[4])
{
	memcpy(m_fOrientation, fOrientation, 4*sizeof(float));
	this->Notify(MSG_ORIENTATION_CHANGE);
	return true;
}
bool VfaTransformableWidgetModel::SetOrientation(const VistaQuaternion & q)
{
	q.GetValues(m_fOrientation);
	this->Notify(MSG_ORIENTATION_CHANGE);
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetOrientation                                              */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::GetOrientation(float fOrientation[4]) const
{
	memcpy(fOrientation, m_fOrientation, 4*sizeof(float));

}
void VfaTransformableWidgetModel::GetOrientation(VistaQuaternion & q) const
{
	q.SetValues(m_fOrientation);
}

VistaQuaternion VfaTransformableWidgetModel::GetOrientation() const
{
	return VistaQuaternion(m_fOrientation);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Scale			                                              */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::SetScale(float fScale)
{
	if(m_fScale ==fScale)
		return;

	m_fScale = fScale;
	Notify(MSG_SCALE_CHANGE);
}
float VfaTransformableWidgetModel::GetScale() const
{
	return m_fScale;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTransform			                                      */
/*                                                                            */
/*============================================================================*/

void VfaTransformableWidgetModel::GetTransform(VistaTransformMatrix & matrix)
{
	VistaQuaternion q(m_fOrientation);
	VistaVector3D v(m_fPosition); 
	VistaTransformMatrix matTrans(q, v);
	VistaTransformMatrix matScale(m_fScale);
	matrix = matScale*matTrans;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPositionVisSpace                                         */
/*                                                                            */
/*============================================================================*/
bool VfaTransformableWidgetModel::SetPositionVisSpace(float fPosition[3])
{
	if(m_bIgnoreWidgetTransform)
		return SetPosition(fPosition);

	VistaVector3D vecPositionVisSpace(fPosition);

	// transform from vis space to widget space
	VistaVector3D vecPositionWidgetSpace = m_oWidgetRefFrame.TransformPositionToFrame((m_oVisRefFrame.TransformPositionFromFrame(vecPositionVisSpace)));
	vecPositionWidgetSpace.GetValues(m_fPosition);
	this->Notify(MSG_POSITION_CHANGE);
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPositionVisSpace                                         */
/*                                                                            */
/*============================================================================*/
bool VfaTransformableWidgetModel::SetPositionVisSpace(double dC[3])
{
	if(m_bIgnoreWidgetTransform)
		return SetPosition(dC);

	float f[3] = {(float)dC[0], (float)dC[1], (float)dC[2]};
	return this->SetPositionVisSpace(f);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPositionVisSpace                                         */
/*                                                                            */
/*============================================================================*/
bool VfaTransformableWidgetModel::SetPositionVisSpace(const VistaVector3D& v3C)
{
	if(m_bIgnoreWidgetTransform)
		return SetPosition(v3C);

	float f[3];
	v3C.GetValues(f);
	return this->SetPositionVisSpace(f);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPositionVisSpace                                         */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::GetPositionVisSpace(float fPosition[3]) const
{
	if(m_bIgnoreWidgetTransform)
		return GetPosition(fPosition);

	VistaVector3D vecPositionWidgetSpace(m_fPosition);

	// transform from widget space to vis space
	VistaVector3D vecPositionVisSpace = m_oVisRefFrame.TransformPositionToFrame((m_oWidgetRefFrame.TransformPositionFromFrame(vecPositionWidgetSpace)));

	vecPositionVisSpace.GetValues(fPosition);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPositionVisSpace                                         */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::GetPositionVisSpace(double dPosition[3]) const
{
	if(m_bIgnoreWidgetTransform)
		return GetPosition(dPosition);

	float f[3];
	this->GetPositionVisSpace(f);
	dPosition[0] = f[0]; dPosition[1] = f[1]; dPosition[2] = f[2]; 
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPositionVisSpace                                         */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::GetPositionVisSpace(VistaVector3D &v3C) const
{
	if(m_bIgnoreWidgetTransform)
		return GetPosition(v3C);

	float f[3];
	this->GetPositionVisSpace(f);
	v3C[0] = f[0]; v3C[1] = f[1]; v3C[2] = f[2]; 
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetOrientationVisSpace                                      */
/*                                                                            */
/*============================================================================*/

bool VfaTransformableWidgetModel::SetOrientationVisSpace(float fOrientation[4])
{
	if(m_bIgnoreWidgetTransform)
		return SetOrientation(fOrientation);

	VistaQuaternion qOriVisSpace(fOrientation);
	VistaQuaternion qOriWidgetSpace = m_oWidgetRefFrame.TransformOrientationToFrame((m_oVisRefFrame.TransformOrientationFromFrame(qOriVisSpace)));
	qOriWidgetSpace.Normalize();

	qOriWidgetSpace.GetValues(m_fOrientation);	
	this->Notify(MSG_ORIENTATION_CHANGE);
	return true;
}
bool VfaTransformableWidgetModel::SetOrientationVisSpace(const VistaQuaternion & q)
{
	if(m_bIgnoreWidgetTransform)
		return SetOrientation(q);

	float fOrientation[4];
	q.GetValues(fOrientation);

	return SetOrientationVisSpace(fOrientation);

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetOrientationVisSpace                                      */
/*                                                                            */
/*============================================================================*/
void VfaTransformableWidgetModel::GetOrientationVisSpace(float fOrientation[4]) const
{
	if(m_bIgnoreWidgetTransform)
		return GetOrientation(fOrientation);

	VistaQuaternion q;
	GetOrientationVisSpace(q);
	q.GetValues(fOrientation);

}
void VfaTransformableWidgetModel::GetOrientationVisSpace(VistaQuaternion & q) const
{
	if(m_bIgnoreWidgetTransform)
		return GetOrientation(q);

	VistaQuaternion qWidgetSpace(m_fOrientation);
	VistaQuaternion qOriVisSpace = m_oVisRefFrame.TransformOrientationToFrame((m_oWidgetRefFrame.TransformOrientationFromFrame(qWidgetSpace)));
	qOriVisSpace.Normalize();

	q = qOriVisSpace;
}




/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetGetDoIgnoreWidgetTransform                               */
/*                                                                            */
/*============================================================================*/

void VfaTransformableWidgetModel::SetDoIgnoreWidgetTransform(bool bIgnore)
{
	m_bIgnoreWidgetTransform = bIgnore;
}
bool VfaTransformableWidgetModel::GetDoIgnoreWidgetTransform() const
{
	return m_bIgnoreWidgetTransform;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaTransformableWidgetModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaTransformableWidgetModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  END OF FILE "VfaTransformableWidgetModel.cpp"                             */
/*============================================================================*/


