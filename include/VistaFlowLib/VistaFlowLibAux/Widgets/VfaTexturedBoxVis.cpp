/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaTexturedBoxVis.h"

#include "Box/VfaTexturedBoxModel.h"
#include <VistaOGLExt/VistaTexture.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

using namespace std;


// ========================================================================== //
// === Includes
// ========================================================================== //
// This table is from the VfaBoxVis.cpp. Needs to be adapted if anything
// in the VfaBoxModel class changes. Maybe refactor this to be stored at a more
// centralized spot.
static const unsigned int __FACES[6][4] =
{
	{0,2,6,4}, //left
	{1,5,7,3}, //right
	{0,1,5,4}, //bottom
	{2,3,7,6}, //top
	{2,3,1,0}, //back
	{4,5,7,6}, //front
};

static const float __TEXCOORDS[4][2] =
{
	{0.0f, 0.0f}, // Left, bottom.
	{1.0f, 0.0f}, // Right, bottom.
	{1.0f, 1.0f}, // Right, top.
	{0.0f, 1.0f}  // Left, top.
};


// ========================================================================== //
// === Con-/Destructor
// ========================================================================== //
VfaTexturedBoxVis::VfaTexturedBoxVis(VfaTexturedBoxModel *pModel)
 : VfaBoxVis(pModel),
   m_pTexturedBoxModel(pModel)
{

}

VfaTexturedBoxVis::~VfaTexturedBoxVis()
{

}


// ========================================================================== //
// === Public Interface
// ========================================================================== //
VfaTexturedBoxModel* VfaTexturedBoxVis::GetTexturedBoxModel() const
{
	return m_pTexturedBoxModel;
}


void VfaTexturedBoxVis::DrawOpaque()
{
	if(!this->GetProperties()->GetVisible())
		return;

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT | GL_LIGHTING_BIT | GL_POINT_BIT | GL_LINE_BIT );

	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);
		
	glLineWidth(GetProperties()->GetLineWidth());

	glPushMatrix();

	if(this->GetProperties()->GetDrawFilledFaces())
	{
		float fColor[3];
		GetProperties()->GetFaceColor().GetValues(fColor);
		glColor3fv(&fColor[0]);

		float fVertex[3];
		for(int f=0; f<6; ++f) //for all faces
		{
			// Only front face (f == 5). See __FACES for details.
			if(f == 5)
			{
				glEnable(GL_TEXTURE_2D);
				glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
				m_pTexturedBoxModel->GetTexture()->Bind();
			}
			
			glBegin(GL_QUADS);
				for(int v=0; v<4; ++v) //for the vertices of this face
				{
					// Only texture the front face (that is f==5).
					// Look at the definition of __FACES for more details.
					if(f == 5)
					{
						glTexCoord2f(__TEXCOORDS[v][0], __TEXCOORDS[v][1]);
					}
					
					//lookup vertex id, get coords and send down the pipe
					m_pTexturedBoxModel->GetCorner(__FACES[f][v], fVertex);
					glVertex3fv(fVertex);
				}
			glEnd();

			// See above, only front face please.
			if(f == 5)
			{
				glBindTexture(GL_TEXTURE_2D, 0);
				glDisable(GL_TEXTURE_2D);
			}
		}
		
			
	}
	else // only the outline now
	{
		float fColor[3];
		GetProperties()->GetLineColor().GetValues(fColor);
		glColor3fv(&fColor[0]);

		float fVertex[3];
		for(int f=0; f<6; ++f) //for all faces
		{
			//draw a line loop
			glBegin(GL_LINE_LOOP);
				for(int v=0; v<4; ++v) //for the vertices of this face
				{
					//lookup vertex id, get coords and send down the pipe
					m_pTexturedBoxModel->GetCorner(__FACES[f][v], fVertex);
					glVertex3fv(fVertex);
				}
			glEnd();
		}
	}

	glPopMatrix();

	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaTexturedBoxVis::CreateProperties() const
{
    VfaBoxVis::VfaBoxVisProps *pProps =  new VfaBoxVis::VfaBoxVisProps;
	pProps->SetDrawFilledFaces(true);

	return pProps;
}

// ========================================================================== //
// === End of File
// ========================================================================== //
