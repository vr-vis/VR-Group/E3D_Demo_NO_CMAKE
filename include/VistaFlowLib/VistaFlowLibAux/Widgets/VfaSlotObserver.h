/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFASLOTOBSERVER_H
#define _VFASLOTOBSERVER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VistaFlowLibAuxConfig.h"
#include <VistaFlowLibAux/Interaction/VfaApplicationContextObject.h>

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * The IVfaSlotObserver is an interface class for all kind of objects which like
 * to register at the VfaWidgetManager for updates of application context slots.
 * Three types of slots exist:
 *	 - sensor slots (have a position and orientation)
 *	 - command slots (are true or false, i.e. a bool)
 *	 - a special time slot which is called frequently by time updates
 *	   (i.e., in the current implementation per frame)
 *
 * The sensor/command mask should return all slots the object is interested in
 * bitwise code. E.g.: "I am interested in sensors nr. 0 and nr. 4"
 * => return 2^0 + 2^4 = sensor mask 17
 *
 * All interfaces are optional, i.e., they have a "neutral" default
 * implementation so that they can be ignored easily.
 */
class VISTAFLOWLIBAUXAPI IVfaSlotObserver
{
public:
	virtual ~IVfaSlotObserver();

	virtual void OnSensorSlotUpdate( int iSlot,
		const VfaApplicationContextObject::sSensorFrame & oFrameData );
	virtual void OnCommandSlotUpdate( int iSlot, const bool bState );
	virtual void OnTimeSlotUpdate( const double dTime );

	/**
	 * Return an(or'ed) bitmask defining which sensor slots/command slots are
	 * used/handled by this controller
	 */
	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;

protected:
	IVfaSlotObserver();
};

#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
