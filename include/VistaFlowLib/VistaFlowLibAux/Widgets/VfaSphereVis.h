/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFASPHEREVIS_H
#define _VFASPHEREVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaSphereModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Simple Sphere style visualization for showing a Sphere which is supposed
 * to be defined by the client class.
 */
class VISTAFLOWLIBAUXAPI VfaSphereVis : public IVflRenderable
{
public: 
	/**
	 * NOTE:	A sphere vis is ALWAYS bound to a sphere model whose
	 *			data it draws. It will register as an observer of this model
	 *			automatically so client classes should perform another 
	 *			registration.
	 */
	VfaSphereVis(VfaSphereModel *pModel);
	virtual ~VfaSphereVis();


	VfaSphereModel *GetModel() const;

	/**
	* here the rendering is done
	*/
	virtual void DrawOpaque();

	virtual unsigned int GetRegistrationMode() const;

	
	//void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/**
	*
	*/
	class VISTAFLOWLIBAUXAPI VfaSphereVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		enum{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_SOLID_CHG,
			MSG_LIGHTING_CHG,
			MSG_LINE_WIDTH_CHG,
			MSG_CENTER_CHG,
			MSG_RADIUS_CHG,
			MSG_LAST
		};

		VfaSphereVisProps();
		virtual ~VfaSphereVisProps();

		bool SetColor(const VistaColor& color);
		VistaColor GetColor() const;
		bool SetColor(float fColor[4]);
		void GetColor(float fColor[4]) const;

		bool SetToSolid(bool solid);
		bool GetIsSolid() const;

		bool SetUseLighting(bool b);
		bool GetUseLighting() const;

		bool SetLineWidth(float f);
		float GetLineWidth() const;

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		bool m_bUseLighting;

		float m_fColor[4];
		float m_fLineWidth;
		bool m_bSolid;
	};

	virtual VfaSphereVisProps *GetProperties() const;

protected:
	VfaSphereVis();

	virtual VflRenderableProperties* CreateProperties() const;
	

private:
	VfaSphereModel *m_pModel;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASPHEREVIS_H
