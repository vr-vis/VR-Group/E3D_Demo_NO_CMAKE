/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef __VFATEXTUREDBOXHANDLE_H
#define __VFATEXTUREDBOXHANDLE_H

// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaControlHandle.h"

#include <VistaBase/VistaColor.h>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <string>

using namespace std;


// ========================================================================== //
// === Forward Declarations
// ========================================================================== //
class VflRenderNode;
class VistaVector3D;
class VistaQuaternion;
class VfaTexturedBoxVis;


// ========================================================================== //
// === Class Definition
// ========================================================================== //
class VISTAFLOWLIBAUXAPI VfaTexturedBoxHandle : public IVfaCenterControlHandle
{
public:
	// ---------------------------------------------------------------------- //
	// --- Con-/Destructor
	// ---------------------------------------------------------------------- //
	//!
	VfaTexturedBoxHandle(VflRenderNode *pRenderNode);
	//!
	virtual ~VfaTexturedBoxHandle();


	// ---------------------------------------------------------------------- //
	// --- Public Interface
	// ---------------------------------------------------------------------- //
	//!
	void SetIsVisible(bool bIsVisible);
	//!
	bool GetIsVisible() const;

	//!
	void SetSize(float fWidth, float fHeight, float fDepth);
	//!
	void SetSize(float fCubeSideLength);
	//!
	void GetSize(float &fWidth, float &fHeight, float &fDepth) const;

	//!
	void SetCenter(const VistaVector3D &v3Center);
	//!
	void GetCenter(VistaVector3D &v3Center) const;

	//!
	void SetRotation(const VistaQuaternion &qRotation);
	//!
	void GetRotation(VistaQuaternion &qRotation) const;

	//!
	void SetScale(float fScale);
	//!
	float GetScale() const;

	//!
	void SetNormalColor(float aRGB[3]);
	void SetNormalColor(const VistaColor& rColor);
	//!
	void GetNormalColor(float aRGB[3]) const;

	//!
	void SetHighlightedColor(float aRGB[3]);
	void SetHighlightedColor(const VistaColor& rColor);
	//!
	void GetHighlightedColor(float aRGB[3]) const;

	//!
	void SetIsHighlighted(bool bIsHighlighted);
	//!
	bool GetIsHighlighted() const;

	//!
	bool LoadTexture(std::string strFilename);

	//!
	VfaTexturedBoxVis* GetTexturedBoxVis() const;


protected:

private:
	// ---------------------------------------------------------------------- //
	// --- Variables
	// ---------------------------------------------------------------------- //
	//!
	float				m_fScaleBuffer;
	//!
	float				m_aNormalColor[4],
						m_aHighlightColor[4];
	//!
	VfaTexturedBoxVis	*m_pTexBoxVis;
};

#endif // __VFATEXTUREDBOXHANDLE_H


// ========================================================================== //
// === End of File
// ========================================================================== //
