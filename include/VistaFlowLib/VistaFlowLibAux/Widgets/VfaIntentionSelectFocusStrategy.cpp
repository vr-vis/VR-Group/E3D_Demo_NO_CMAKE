/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaIntentionSelectFocusStrategy.h"

#include <VistaAspects/VistaTransformable.h>

#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/GraphicsManager/VistaGraphicsManager.h>
#include <VistaKernel/GraphicsManager/VistaTransformNode.h>
#include <VistaKernel/InteractionManager/VistaIntentionSelect.h>
#include <VistaKernel/InteractionManager/VistaInteractionEvent.h>
#include <VistaKernel/InteractionManager/VistaInteractionContext.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaFlowLibAux/Widgets/VfaControlHandle.h>
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>

#include <VistaKernel/GraphicsManager/VistaSceneGraph.h>

using namespace std;


/*============================================================================*/
/* GLOBAL CLASSES												              */
/*============================================================================*/
class VfaIntentionSelectPosAdapter : public IVistaIntentionSelectAdapter
{
public:
	VfaIntentionSelectPosAdapter()
	{ }
	
	virtual ~VfaIntentionSelectPosAdapter()
	{ }
	
	virtual void SetPosition(const VistaVector3D &v3Pos)
	{
		m_v3Position = v3Pos;
	}

	virtual bool GetPosition(VistaVector3D &v3Pos, const VistaReferenceFrame &oReferenceFrame) const
	{
		v3Pos = m_v3Position;

		return false;
	}

private:
	VistaVector3D		m_v3Position;
};


/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaIntentionSelectFocusStrategy::VfaIntentionSelectFocusStrategy()
: m_pSelection(new VistaIntentionSelect),
  m_pHandle(NULL),
  m_fConeLength(3.0f),
  m_fConeRadius(0.2f)
{
	VistaEvenCone selectionVolume( m_fConeLength, m_fConeRadius );
	m_pSelection->SetSelectionVolume(selectionVolume);
//	m_pSelection->SetStickyness(0.75f);
//  m_pSelection->SetSnappiness(0.25f);

	this->SetStickynessToSnappinessAlpha(0.75f);
}

VfaIntentionSelectFocusStrategy::~VfaIntentionSelectFocusStrategy()
{
	delete m_pSelection;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME    : EvaluateFocus                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaIntentionSelectFocusStrategy::EvaluateFocus(
                std::vector<IVfaControlHandle*> &vecControlHandles)
{
  // Update cone transform.
  m_pSelection->SetConeTransform(m_oPointer.v3Position,
                                 m_oPointer.qOrientation);

  // Update handles' global position.
  vector<sControlHandleMapping>::iterator it = m_vecPosMap.begin();

  float aGlobalTrans[16];
  VistaTransformMatrix mGlobalTrans;
  VistaVector3D v3PosBuffer;

  // Loop over all WidgetControllers.
  while(it != m_vecPosMap.end())
  {
		// Retrieve the global transformation of the WidgetController's
		// RenderNode.
		(*it).pOwner->GetRenderNode()->GetTransformable()
						->GetWorldTransform(aGlobalTrans);
		mGlobalTrans = VistaTransformMatrix(aGlobalTrans);

		// Loop over all ControlHandles of the WidetController.
		for(size_t uiCurIdx=0; uiCurIdx<(*it).vecLocal.size(); ++uiCurIdx)
		{
			// Retrieve the local position from the current ControlHandle
			// owned by the current WidgetController.
			(*it).vecLocal[uiCurIdx]->GetCenter(v3PosBuffer);
			v3PosBuffer[3] = 1.0f;

			// Transform it to the global frame and set it as the position
			// of the corresponding PosAdapter.
			v3PosBuffer = mGlobalTrans.Transform(v3PosBuffer);
			(*it).vecGlobal[uiCurIdx]->SetPosition(v3PosBuffer);

			(*it).vecGlobal[uiCurIdx]->SetIsSelectionEnabled(
				(*it).vecLocal[uiCurIdx]->GetEnable());
		}
		++it;
	}

    // Update scores.
    std::vector<IVistaIntentionSelectAdapter*> q;
    m_pSelection->Update(q);

    vecControlHandles.clear();

    // Copy scored selections into handles vector, these are sorted by score,
    // first is focus.
    const size_t sz = q.size();
    for(size_t idx = 0; idx < sz; ++idx)
    {
        VfaIntentionSelectPosAdapter *pPosAdapter = dynamic_cast<VfaIntentionSelectPosAdapter*>(q[idx]);
        for(size_t i=0; i<m_vecPosMap.size(); ++i)
        {
            for(size_t j=0; j<m_vecPosMap[i].vecGlobal.size(); ++j)
                if(m_vecPosMap[i].vecGlobal[j] == pPosAdapter)
                    vecControlHandles.push_back(m_vecPosMap[i].vecLocal[j]);
        }
    }

    return(!vecControlHandles.empty());
}


/*============================================================================*/
/*                                                                            */
/*  NAME    : RegisterSelectable                                              */
/*                                                                            */
/*============================================================================*/
void VfaIntentionSelectFocusStrategy::RegisterSelectable(
	IVfaWidgetController* pSelectable)
{
	// Look, whether the widget controller was registered already or not.
	size_t uiIdx = 0;
	for(; uiIdx < m_vecPosMap.size(); ++uiIdx)
	{
		if(m_vecPosMap[uiIdx].pOwner == pSelectable)
			break;
	}

	// In case it wasn't, the uiIdx should run through up to size().
	// If this happens, we need to add the WidgetController and all its
	// ControlHandles.
	if(uiIdx == m_vecPosMap.size())
	{
		// Retrieve all handles from the WidgetController.
		const std::vector<IVfaControlHandle*>& vecHandles =
			pSelectable->GetControlHandles();

		// Create a new mapping and set the owner of the handles to the
		// specified WidgetController.
		sControlHandleMapping oNewMapping;
		oNewMapping.pOwner = pSelectable;

		// Retrieve the global transformation of the WidgetController's
		// RenderNode, so that it can be used to transform the ControlHandles
		// position into the global frame.
		float aGlobalTrans[16];
		pSelectable->GetRenderNode()
			->GetTransformable()->GetWorldTransform(aGlobalTrans);
		VistaTransformMatrix mGlobalTrans(aGlobalTrans);
		
		IVfaCenterControlHandle			*pCur			= 0;
		VfaIntentionSelectPosAdapter	*pNewAdapter	= 0;
		VistaVector3D					v3PosBuffer;

		// Loop over all handles...
		for(size_t uiCurIdx = 0; uiCurIdx < vecHandles.size(); ++uiCurIdx)
		{
			pCur = dynamic_cast<IVfaCenterControlHandle*>(vecHandles[uiCurIdx]);

			if(pCur)
			{
				oNewMapping.vecLocal.push_back(pCur);
				
				// ... and create a PosAdapter for each of them.
				pNewAdapter = new VfaIntentionSelectPosAdapter;
				oNewMapping.vecGlobal.push_back(pNewAdapter);

				// Next, set the PosAdapter's position to the globalized
				// position of the corresponding CenterControlHandle.
				// Remember: This need to be updated on each evaluation run!
				pCur->GetCenter(v3PosBuffer);
				v3PosBuffer[3] = 1.0f;

				v3PosBuffer = mGlobalTrans.Transform(v3PosBuffer);
				pNewAdapter->SetPosition(v3PosBuffer);

				// Finally, register the PosAdapter with the underlying
				// VistaIntentionSelect instance.
				m_pSelection->RegisterNode(pNewAdapter);
			}
		}

		m_vecPosMap.push_back(oNewMapping);
	}

	/* TODO_LOW: Remove! Old vis space code.
	for(std::vector<IVfaControlHandle*>::const_iterator it = vecHandles.begin();
		it != vecHandles.end(); ++it)
	{
		if((*it)->GetType()==IVfaControlHandle::HANDLE_CENTER)
			m_pSelection->RegisterNode(static_cast<IVfaCenterControlHandle*>(*it));
	}
	*/
}

/*============================================================================*/
/*                                                                            */
/*  NAME    : UnregisterSelectable                                            */
/*                                                                            */
/*============================================================================*/
void VfaIntentionSelectFocusStrategy::UnregisterSelectable(
	IVfaWidgetController* pSelectable)
{
	// Find the WidgetController in the local data structure.
	vector<sControlHandleMapping>::iterator it = m_vecPosMap.begin();	
	for(size_t uiIdx=0; uiIdx<m_vecPosMap.size(); ++uiIdx)
	{
		if((*it).pOwner == pSelectable)
			break;

		++it;
	}

	// If it wasn't found we'll return.
	if(it == m_vecPosMap.end())
		return;

	// Else, all the WidgetController's PosAdapters need to be deregistered
	// from the VistaIntentionSelect instance...
	for(size_t uiCurIdx = 0; uiCurIdx<(*it).vecGlobal.size(); ++uiCurIdx)
	{
		m_pSelection->UnregisterNode((*it).vecGlobal[uiCurIdx]);
		delete (*it).vecGlobal[uiCurIdx];
	}

	// ... and the WidgetController's entry needs to be purges from the local
	// data structure.
	m_vecPosMap.erase(it);
	
	/* TODO_LOW: Remove! Old vis space code.
	// register all handles of widget controller
	std::vector<IVfaControlHandle*> pHandles = pSelectable->GetControlHandles();
	
	for(std::vector<IVfaControlHandle*>::iterator it = pHandles.begin();
		it != pHandles.end(); ++it)
	{
		if((*it)->GetType()==IVfaControlHandle::HANDLE_CENTER)
			m_pSelection->UnregisterNode(static_cast<IVfaCenterControlHandle*>(*it));
	}
	*/
}

/*============================================================================*/
/*                                                                            */
/*  NAME    : Change selection cone                                           */
/*                                                                            */
/*============================================================================*/
void VfaIntentionSelectFocusStrategy::SetSelectionConeLength(float fLength)
{
	m_fConeLength = fLength;
	VistaEvenCone selectionVolume( m_fConeLength, m_fConeRadius );
	m_pSelection->SetSelectionVolume(selectionVolume);
}

float VfaIntentionSelectFocusStrategy::GetSelectionConeLength() const
{
	return m_fConeLength;
}

void VfaIntentionSelectFocusStrategy::SetSelectionConeRadius(float fRadius)
{
	m_fConeRadius = fRadius;
	VistaEvenCone selectionVolume( m_fConeLength, m_fConeRadius );
	m_pSelection->SetSelectionVolume(selectionVolume);

}

float VfaIntentionSelectFocusStrategy::GetSelectionConeRadius() const
{
	return m_fConeRadius;
}

bool VfaIntentionSelectFocusStrategy::SetStickynessToSnappinessAlpha(
	float fAlpha)
{
	if(fAlpha < 0.0f || fAlpha > 1.0f)
		return false;

	m_fStickSnapAlpha = fAlpha;

	m_pSelection->SetStickyness(m_fStickSnapAlpha);
    m_pSelection->SetSnappiness(1.0f - m_fStickSnapAlpha);

	return true;
}

float VfaIntentionSelectFocusStrategy::GetStickynessToSnappinessAlpha() const
{
	return m_fStickSnapAlpha;
}

/*============================================================================*/
/*                                                                            */
/*  NAME    : Slot updates			                                          */
/*                                                                            */
/*============================================================================*/
void VfaIntentionSelectFocusStrategy::OnSensorSlotUpdate(int iSlot,
	const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	/*
	* Remark: if the selection strategy works in Vis-Space, the event cone should be scaled according to the VisController
	*
	*/
	if(iSlot != VfaApplicationContextObject::SLOT_POINTER_WORLD)
		return;

	m_oPointer = oFrameData;

	return;
}

void VfaIntentionSelectFocusStrategy::OnCommandSlotUpdate(int iSlot,
														   const bool bSet)
{
	return;
}

void VfaIntentionSelectFocusStrategy::OnTimeSlotUpdate(const double dTime)
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME    : Slot masks			                                          */
/*                                                                            */
/*============================================================================*/
int VfaIntentionSelectFocusStrategy::GetSensorMask() const
{
	// we are interested in the pointer sensor in world coordinates
	return(1 << VfaApplicationContextObject::SLOT_POINTER_WORLD);
}
int VfaIntentionSelectFocusStrategy::GetCommandMask() const
{
	return 0;
}
bool VfaIntentionSelectFocusStrategy::GetTimeUpdate() const
{
	return false;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VfaIntentionSelectFocusStrategy.cpp"                          */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/
