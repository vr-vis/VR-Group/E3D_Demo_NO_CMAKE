/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaTexturedQuadHandle.h"
#include "VfaQuadVis.h"
#include "Plane/VfaPlaneModel.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaOGLExt/VistaTexture.h>
#include <cstring>

using namespace std;


// ========================================================================== //
// === Con-/Destructor
// ========================================================================== //
VfaTexturedQuadHandle::VfaTexturedQuadHandle(VflRenderNode *pRenderNode)
 : IVfaCenterControlHandle(),
   m_pQuadVis(new VfaQuadVis(new VfaPlaneModel))
{
	m_aNormalColor[0] = 1.0f;
	m_aNormalColor[1] = 0.0f;
	m_aNormalColor[2] = 0.0f;
	m_aNormalColor[3] = 1.0f;

	m_aHighlightColor[0] = 1.0f;
	m_aHighlightColor[1] = 0.75f;
	m_aHighlightColor[2] = 0.75f;
	m_aHighlightColor[3] = 1.0f;

	if(m_pQuadVis->Init())
		pRenderNode->AddRenderable(m_pQuadVis);

	m_pQuadVis->GetProperties()->SetFillPlane(true);
	m_pQuadVis->GetProperties()->SetDrawBorder(true);
	m_pQuadVis->GetProperties()->SetFillColor(m_aNormalColor);
	m_pQuadVis->GetProperties()->SetLineColor(m_aNormalColor);	
}

VfaTexturedQuadHandle::~VfaTexturedQuadHandle()
{
	m_pQuadVis->GetRenderNode()->RemoveRenderable(m_pQuadVis);
	delete m_pQuadVis->GetModel();
	delete m_pQuadVis;
}


// ========================================================================== //
// === Public Interface
// ========================================================================== //
void VfaTexturedQuadHandle::SetIsVisible(bool bIsVisible)
{
	m_pQuadVis->SetVisible(bIsVisible);
}

bool VfaTexturedQuadHandle::GetIsVisible() const
{
	return m_pQuadVis->GetVisible();
}


void VfaTexturedQuadHandle::SetSize(float fWidth, float fHeight)
{
	if(fWidth < 0.0f || fHeight < 0.0f)
		return;

	m_pQuadVis->GetModel()->SetExtents(fWidth, fHeight);
}

void VfaTexturedQuadHandle::GetSize(float &fWidth, float &fHeight) const
{
	m_pQuadVis->GetModel()->GetExtents(fWidth, fHeight);
}


void VfaTexturedQuadHandle::SetCenter(const VistaVector3D &v3Center)
{
	IVfaCenterControlHandle::SetCenter(v3Center);
	m_pQuadVis->GetModel()->SetTranslation(v3Center);
}

void VfaTexturedQuadHandle::GetCenter(VistaVector3D &v3Center) const
{
	m_pQuadVis->GetModel()->GetTranslation(v3Center);
}


void VfaTexturedQuadHandle::SetNormal(const VistaVector3D &v3Normal)
{
	m_pQuadVis->GetModel()->SetNormal(v3Normal);
}

void VfaTexturedQuadHandle::GetNormal(VistaVector3D &v3Normal) const
{
	m_pQuadVis->GetModel()->GetNormal(v3Normal);
}


void VfaTexturedQuadHandle::SetNormalColor(float aRGB[3])
{
	memcpy(m_aNormalColor, aRGB, 3 * sizeof(float));

	// Clamp to [0.0f, 1.0f].
	for(int i=0; i<3; ++i)
	{
		m_aNormalColor[i] = std::max<float>(0.0f, m_aNormalColor[i]);
		m_aNormalColor[i] = std::min<float>(1.0f, m_aNormalColor[i]);
	}
}

void VfaTexturedQuadHandle::GetNormalColor(float aRGB[3]) const
{
	memcpy(aRGB, m_aNormalColor, 3 * sizeof(float));
}


void VfaTexturedQuadHandle::SetHighlightedColor(float aRGB[3])
{
	memcpy(m_aHighlightColor, aRGB, 3 * sizeof(float));

	// Clamp to [0.0f, 1.0f].
	for(int i=0; i<3; ++i)
	{
		m_aHighlightColor[i] = std::max<float>(0.0f, m_aHighlightColor[i]);
		m_aHighlightColor[i] = std::min<float>(1.0f, m_aHighlightColor[i]);
	}
}

void VfaTexturedQuadHandle::GetHighlightedColor(float aRGB[3]) const
{
	memcpy(aRGB, m_aHighlightColor, 3 * sizeof(float));
}


void VfaTexturedQuadHandle::SetIsHighlighted(bool bIsHighlighted)
{
	IVfaCenterControlHandle::SetIsHighlighted(bIsHighlighted);

	if(bIsHighlighted)
	{
		m_pQuadVis->GetProperties()->SetFillColor(m_aHighlightColor);
		m_pQuadVis->GetProperties()->SetLineColor(m_aHighlightColor);
	}
	else
	{
		m_pQuadVis->GetProperties()->SetFillColor(m_aNormalColor);
		m_pQuadVis->GetProperties()->SetLineColor(m_aNormalColor);
	}
}

bool VfaTexturedQuadHandle::GetIsHighlighted() const
{
	return IVfaCenterControlHandle::GetIsHighlighted();
}


void VfaTexturedQuadHandle::SetTexture(VistaTexture *pTex)
{
	m_pQuadVis->SetTexture(pTex);
}

VfaQuadVis* VfaTexturedQuadHandle::GetQuadVis() const
{
	return m_pQuadVis;
}

// ========================================================================== //
// === End of File
// ========================================================================== //
