/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <GL/glew.h>

#include "VfaBoxVis.h"

#include "Box/VfaBoxModel.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <cassert>
#include <cstring>

using namespace std;


//for vertex enumeration cf. VfaBoxModel
static const unsigned int __FACES[6][4] ={
	{0,4,6,2}, //left
	{1,3,7,5}, //right
	{0,1,5,4}, //bottom
	{2,6,7,3}, //top
	{0,2,3,1,}, //back
	{4,5,7,6}, //front
};

static const float __NORMALS[6][3] = {
	{-1.0f,  0.0f,  0.0f},
	{ 1.0f,  0.0f,  0.0f},
	{ 0.0f, -1.0f,  0.0f}, 
	{ 0.0f,  1.0f,  0.0f},
	{ 0.0f,  0.0f, -1.0f},
	{ 0.0f,  0.0f,  1.0f},
};
/*============================================================================*/
/*  IMPLEMENTATION      VfaBoxVis                                            */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaBoxVis::VfaBoxVis()
	:	m_pModel(NULL)
{
}

VfaBoxVis::VfaBoxVis(VfaBoxModel* pModel)
	:	m_pModel(pModel)
{
}

VfaBoxVis::~VfaBoxVis()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel	                                                  */
/*                                                                            */
/*============================================================================*/
VfaBoxModel *VfaBoxVis::GetModel() const
{
	return m_pModel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaBoxVis::DrawOpaque()
{
	VfaBoxVisProps *pProps = this->GetProperties();
	assert(pProps != NULL);

	if(!pProps->GetVisible())
		return;

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT | GL_LIGHTING_BIT | GL_POINT_BIT | GL_LINE_BIT );
	

	if(pProps->GetDrawWithLighting())
	{
		glEnable(GL_LIGHTING);
		glEnable(GL_COLOR_MATERIAL);
	}
	else
	{
		glDisable(GL_LIGHTING);
	}
	
	glDisable(GL_CULL_FACE);
	
	glLineWidth(GetProperties()->GetLineWidth());

	glPushMatrix();
	if(this->GetProperties()->GetDrawFilledFaces())
	{
		float fColor[4];
		GetProperties()->GetFaceColor(fColor);
		fColor[3] = 1.0f;
		glColor4fv(fColor);

		float fVertex[3];
		glBegin(GL_QUADS);
			for(int f=0; f<6; ++f) //for all faces
			{
				for(int v=0; v<4; ++v) //for the vertices of this face
				{
					//lookup vertex id, get coords and send down the pipe
					m_pModel->GetCorner(__FACES[f][v], fVertex);
					glNormal3fv(&(__NORMALS[f][0]));
					glVertex3fv(fVertex);
				}
			}
		glEnd();
			
	}
	else // only the outline now
	{
		glDisable(GL_LIGHTING);

		float fColor[4];
		GetProperties()->GetLineColor(fColor);
		glColor4fv(fColor);

		float fVertex[3];
		for(int f=0; f<6; ++f) //for all faces
		{
			//draw a line loop
			glBegin(GL_LINE_LOOP);
				for(int v=0; v<4; ++v) //for the vertices of this face
				{
					//lookup vertex id, get coords and send down the pipe
					m_pModel->GetCorner(__FACES[f][v], fVertex);
					glVertex3fv(fVertex);
				}
			glEnd();
		}
	}


	glPopMatrix();
	glPopAttrib();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaBoxVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
//VfaBoxVis::VfaBoxVisProps *VfaBoxVis::GetProperties() const
VfaBoxVis::VfaBoxVisProps *VfaBoxVis::GetProperties() const
{
	return static_cast<VfaBoxVis::VfaBoxVisProps *>(IVflRenderable::GetProperties());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaBoxVis::CreateProperties() const
{
	return new VfaBoxVis::VfaBoxVisProps;
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaBoxVis::VfaBoxVisProps                             */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaBoxVis::VfaBoxVisProps");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<VistaColor, VfaBoxVis::VfaBoxVisProps>(
		"COLOR", STR_REF_TYPENAME,
		&VfaBoxVis::VfaBoxVisProps::GetLineColor),
	new TVistaPropertyGet<float, VfaBoxVis::VfaBoxVisProps>(
		"LINE_WIDTH", STR_REF_TYPENAME,
		&VfaBoxVis::VfaBoxVisProps::GetLineWidth),
	NULL
};
//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<const VistaColor &, VistaColor, VfaBoxVis::VfaBoxVisProps>(
		"COLOR", STR_REF_TYPENAME,
		&VfaBoxVis::VfaBoxVisProps::SetLineColor),
	new TVistaPropertySet<float, float, VfaBoxVis::VfaBoxVisProps>(
		"LINE_WIDTH", STR_REF_TYPENAME,
		&VfaBoxVis::VfaBoxVisProps::SetLineWidth),
	NULL 
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaBoxVis::VfaBoxVisProps::VfaBoxVisProps()
:	m_fLineWidth(2.0f),
	m_bDrawFilledFaces(false),
	m_bDrawWithLighting(false)
{
	//blue border
	m_fLineColor[0] = 0.0f;
	m_fLineColor[1] = 0.0f;
	m_fLineColor[2] = 1.0f;
	m_fLineColor[3] = 1.0f;

	m_fFaceColor[0] = 0.0f;
	m_fFaceColor[1] = 0.0f;
	m_fFaceColor[2] = 1.0f;
	m_fFaceColor[3] = 1.0f;
}
VfaBoxVis::VfaBoxVisProps::~VfaBoxVisProps()
{

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDrawFilledFaces                                      */
/*                                                                            */
/*============================================================================*/
bool VfaBoxVis::VfaBoxVisProps::SetDrawFilledFaces(bool b)
{
	if(m_bDrawFilledFaces == b)
		return false;

	m_bDrawFilledFaces = b;
	this->Notify(MSG_FILLED_FACE_CHG);
	return true;
}
bool VfaBoxVis::VfaBoxVisProps::GetDrawFilledFaces() const
{
	return m_bDrawFilledFaces;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDrawWithLighting                                     */
/*                                                                            */
/*============================================================================*/
bool VfaBoxVis::VfaBoxVisProps::SetDrawWithLighting(bool b)
{
	if(m_bDrawWithLighting == b)
		return false;

	m_bDrawWithLighting = b;
	this->Notify(MSG_DRAW_LIGHTING_CHG);
	return true;
}
bool VfaBoxVis::VfaBoxVisProps::GetDrawWithLighting() const
{
	return m_bDrawWithLighting;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLineColor                                            */
/*                                                                            */
/*============================================================================*/
bool VfaBoxVis::VfaBoxVisProps::SetLineColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor, VistaColor::RGBA);
	return this->SetLineColor(fColor);
}

VistaColor VfaBoxVis::VfaBoxVisProps::GetLineColor() const
{
	return VistaColor(m_fLineColor);
}

bool VfaBoxVis::VfaBoxVisProps::SetLineColor(float fColor[4])
{
	fColor[3] = 1.0f;
	if(	fColor[0] == m_fLineColor[0] &&
		fColor[1] == m_fLineColor[1] &&
		fColor[2] == m_fLineColor[2] &&
		fColor[3] == m_fLineColor[3])
	{
		return false;
	}

	memcpy(m_fLineColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHG);
	return true;
}

void VfaBoxVis::VfaBoxVisProps::GetLineColor(float fColor[4]) const
{
	memcpy(fColor, m_fLineColor, 4*sizeof(float));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFaceColor                                            */
/*                                                                            */
/*============================================================================*/
bool VfaBoxVis::VfaBoxVisProps::SetFaceColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor, VistaColor::RGBA);
	return this->SetFaceColor(fColor);
}

VistaColor VfaBoxVis::VfaBoxVisProps::GetFaceColor() const
{
	return VistaColor(m_fFaceColor);
}

bool VfaBoxVis::VfaBoxVisProps::SetFaceColor(float fColor[4])
{
	if(	fColor[0] == m_fFaceColor[0] &&
		fColor[1] == m_fFaceColor[1] &&
		fColor[2] == m_fFaceColor[2] &&
		fColor[3] == m_fFaceColor[3])
	{
		return false;
	}

	memcpy(m_fFaceColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHG);
	return true;
}

void VfaBoxVis::VfaBoxVisProps::GetFaceColor(float fColor[4]) const
{
	memcpy(fColor, m_fFaceColor, 4*sizeof(float));
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLineWidth                                            */
/*                                                                            */
/*============================================================================*/
bool VfaBoxVis::VfaBoxVisProps::SetLineWidth(float fLineWidth)
{
  	if(m_fLineWidth == fLineWidth)
		return false;

	m_fLineWidth = fLineWidth;
	this->Notify(MSG_LINE_WIDTH_CHG);
	return true;
}
float VfaBoxVis::VfaBoxVisProps::GetLineWidth() const
{
	return m_fLineWidth;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaBoxVis::VfaBoxVisProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaBoxVis::VfaBoxVisProps::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaBoxVis.cpp"                                               */
/*============================================================================*/

