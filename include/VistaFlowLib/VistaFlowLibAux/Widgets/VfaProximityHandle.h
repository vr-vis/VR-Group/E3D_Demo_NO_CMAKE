/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAPROXIMITYHANDLE_H
#define _VFAPROXIMITYHANDLE_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaControlHandle.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Interface for "proximity selected" handles, i.e., handles that react on
 * an input sensor's position only. If the given position is "close to"
 * or "inside" of the handle it will react by being "touched".
 */
class VISTAFLOWLIBAUXAPI IVfaProximityHandle : public IVfaControlHandle
{
public:
	/** 
	 * A proximity handle creates a visualization for itself. However,
	 * it can be used as "pure virtual" handle with disabled visualization.
	 * This is useful in cases where the widget using the handle has its own
	 * visualization which would coincide with the handle. 
	 */
	virtual void SetVisEnabled(bool b);
	bool GetVisEnabled() const;

	virtual void SetProximityDistance(float f);
	float GetProximityDistance() const;

	virtual bool IsTouched(const VistaVector3D & v3Pos) const = 0;
protected:
	IVfaProximityHandle(int iType);
	virtual ~IVfaProximityHandle();
private:
	bool m_bVisEnabled;
	float m_fProximityDistance;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAPROXIMITYHANDLE_H
