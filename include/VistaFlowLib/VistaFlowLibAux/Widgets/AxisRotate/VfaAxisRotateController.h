/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAAXISROTATECONTROLLER_H
#define _VFAAXISROTATECONTROLLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// widget
#include <VistaFlowLibAux/Widgets/VfaWidget.h>

//controller
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaBase/VistaVectorMath.h>
#include <string>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaAxisRotateWidget;
class VfaAxisRotateVis;
class VfaSphereHandle;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
* This controller for the axis rotate widget uses a ray-plane interaction to move the lever.
* In more detail, the ray defined by the input device's direction is cut with the plane spanned by the  
* lever rotation (i.e., the plane normal is parallel to the rotation axis vector).
* This intersection point defines the new rotation angle.
*
* This interaction method enables a fast rotation with the input device with simple wrist rotation.
* A drawback is, that interaction with a low plane-ray angle result in awkward intersection-point-jumps,
* which can be handled by @see{SetConstrainedMinimumAngle}.

*	\image HTML VistaFlowLibAux/wristrotation.jpg "Idea of wrist rotation manipulation."
*/

class VISTAFLOWLIBAUXAPI VfaAxisRotateController : public VflObserver, public IVfaWidgetController
{
public:
	VfaAxisRotateController(VflRenderNode *pRenderNode, VfaAxisRotateWidget *pWidget);
	virtual ~VfaAxisRotateController();

	
	/** 
	 * This is called on transition change into (out of) state FOCUS
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();

	/**
	 * This is called on transition change into (out of) state TOUCH
	 */
	void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	void OnUntouch();

	/**
	 * These are called on update of all registered (sensor & command) slots
	 */
	void OnSensorSlotUpdate (int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate (int iSlot, const bool bSet);
	void OnTimeSlotUpdate (const double dTime);

	/**
	 * return an (or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

	/** A disabled controller does not process input. */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	
	
	/**
	 * Method:      SetGrabSlot
	 *
	 * Access to the grab slot i.e. the application context command slot that has to be set in order
	 * to grab the widget.
	 *
	 * @attention This slot has to be set BEFORE the widget is registered at the widget manager because there the
	 * registration for a specific slot takes place.
	 *
	 * @param       i
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/28
	 */
	void SetGrabSlot(int i);
	int GetGrabSlot() const;

	
	/**
	 * Method:      SetAngleSnapping
	 *
	 * Snap movement to multiples of fAngle (>= 0).
	 * Set to 0.0f to disable.
	 *
	 * @param       fAngle
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/28
	 */
	void SetAngleSnapping (float fAngle);
	float GetAngleSnapping () const;

	
	/**
	 * Method:      SetConstrainedMinimumAngle
	 *
	 * All movements with the angle between ray and plane lower than
	 * fAngle are constrained. This is necessary to avoid nearly arbitrary
	 * jumps on the plane due to input device jittering if the ray-plane-angle 
	 * is close to zero.
	 *
	 * @param       fAngle
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/28
	 */
	void SetConstrainedMinimumAngle (float fAngle);
	float GetConstrainedMinimumAngle () const;

// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/**
	 * Method:      SetHandleRadius
	 *
	 * Set the sphere handles radius that sits on top of the lever.
	 *
	 * @param       fRadius
	 * @return      bool
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool SetHandleRadius (float fRadius);
protected:
	
private:
	VfaAxisRotateWidget *      m_pWidget;
	VfaAxisRotateVis*          m_pView;

	VfaSphereHandle*           m_pHandle;

	bool                        m_bActiveMoving;
	int                         m_iGrabSlot;
	float						m_fAngleSnapping;
	float						m_fConstrainedMinimumAngle;

	VfaApplicationContextObject::sSensorFrame m_oPointerFrame;
	VistaTransformMatrix		m_matWidgetTrans;

	bool						m_bEnabled;

};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFAAXISROTATECONTROLLER_H



