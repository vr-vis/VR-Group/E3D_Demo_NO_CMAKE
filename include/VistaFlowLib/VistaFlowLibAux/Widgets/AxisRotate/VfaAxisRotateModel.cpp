/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#include "VfaAxisRotateModel.h" 
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/


/*============================================================================*/
/* WIDGET MODEL				                                                  */
/*============================================================================*/
static const std::string STR_REF_TYPENAME("VfaAxisRotate");

VfaAxisRotateModel::VfaAxisRotateModel()
:VfaWidgetModelBase(),
m_fAngle(0.0f),
m_fScale(1.0f)
{
	m_fAngleRanges [0] = -1.0f;
	m_fAngleRanges[1] = -1.0f;

	m_fPosition[0] = 0.0f;
	m_fPosition[1] = 0.0f;
	m_fPosition[2] = 0.0f;
}
VfaAxisRotateModel::~VfaAxisRotateModel()
{
}



std::string VfaAxisRotateModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

void VfaAxisRotateModel::SetPosition (float x, float y, float z)
{
	m_fPosition[0] = x;
	m_fPosition[1] = y;
	m_fPosition[2] = z;
	Notify (MSG_POSITION_CHANGED);
}

void VfaAxisRotateModel::GetPosition (float &x, float &y, float &z) const
{
	x = m_fPosition[0];
	y = m_fPosition[1];
	z = m_fPosition[2];
}

void VfaAxisRotateModel::GetPosition (VistaVector3D & v) const
{
	v.SetValues(m_fPosition);
}

void VfaAxisRotateModel::SetScale (float s)
{
	m_fScale = s;
}
float VfaAxisRotateModel::GetScale () const
{
	return m_fScale;
}

void VfaAxisRotateModel::SetAngle (float a)
{
	if (a == m_fAngle)
		return;

	// clamp angle to range
	if ((m_fAngleRanges[0] != -1.0f) && (a < m_fAngleRanges[0]))
		a = m_fAngleRanges[0];

	if ((m_fAngleRanges[1] != -1.0f) && (a > m_fAngleRanges[1]))
		a = m_fAngleRanges[1];

	m_fAngle = a;
	Notify (MSG_AXIS_CHANGED);
}
float VfaAxisRotateModel::GetAngle () const
{
	return m_fAngle;
}

void VfaAxisRotateModel::SetAngleRanges (float fMinAngle, float fMaxAngle)
{
	 m_fAngleRanges[0] = fMinAngle;
	 m_fAngleRanges[1] = fMaxAngle;

	 // set angle anew to enforce new ranges
	 this->SetAngle (this->GetAngle());
}
bool VfaAxisRotateModel::GetAngleRanges (float &fMinAngle, float &fMaxAngle) const
{
	fMinAngle = m_fAngleRanges[0];
	fMaxAngle = m_fAngleRanges[1];
	return true;
}


void VfaAxisRotateModel::SetOrientation (const VistaQuaternion & q)
{
	m_qOrientation = q;
}
void VfaAxisRotateModel::GetOrientation (VistaQuaternion & q) const
{
	q = m_qOrientation;
}

int VfaAxisRotateModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

