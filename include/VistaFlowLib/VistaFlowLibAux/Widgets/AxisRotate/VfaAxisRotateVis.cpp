/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here
#include <GL/glew.h>

#include "VfaAxisRotateVis.h" 
#include "VfaAxisRotateModel.h" 
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>
#include <VistaFlowLibAux/Widgets/VfaSphereHandle.h>


#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <iomanip>
#include <cstring>
#include <sstream>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/


/*============================================================================*/
/* WIDGET VIS				                                                  */
/*============================================================================*/
VfaAxisRotateVis::VfaAxisRotateVis(VflRenderNode* pRenderNode)
:m_bIsHighlighted(false),
m_bIsConstrained(false),
m_fScale(1.0f),
m_pAngleInfo(new Vfl3DTextLabel())
{
	m_pQuadratic = gluNewQuadric();

	// init 3D text
	m_pAngleInfo->Init();
	m_pAngleInfo->SetTextFollowViewDir(true);
	m_pAngleInfo->SetRenderNode(pRenderNode);
	m_pAngleInfo->SetTextSize(0.25f);
	m_pAngleInfo->SetVisible(false);
	float fColor [4] = {1.0f,1.0f,1.0f,1.0f};
	m_pAngleInfo->SetColor (fColor);

}
VfaAxisRotateVis::~VfaAxisRotateVis()
{
	gluDeleteQuadric(m_pQuadratic);
	delete m_pAngleInfo;
}
	

void VfaAxisRotateVis::DrawOpaque()
{
	if(!this->GetProperties()->GetVisible())
		return;

	// draw text in vis coordinate system
	VfaAxisRotateVisProps *props = static_cast<VfaAxisRotateVisProps*>(GetProperties());
	float v3Point[3];
	props->GetStartPosition(v3Point);
	VistaVector3D vecPosition (v3Point);

	vecPosition.Normalize();
	vecPosition = vecPosition * (props->GetLeverLength()*props->GetRotationArcLength()*1.6f);
	vecPosition = m_oWidgetRefFrame.TransformPositionFromFrame (vecPosition);
	m_pAngleInfo->SetPosition(vecPosition);
	std::stringstream oStrStream;
	oStrStream << std::setprecision(5) << m_fAngle << " deg";
	
	m_pAngleInfo->SetText (oStrStream.str());
	m_pAngleInfo->DrawOpaque();

	// draw cylinder
	glPushAttrib(GL_LIGHTING_BIT);

	glEnable(GL_LIGHTING);

	glPushMatrix();

	GLfloat Ambient[] = {0.2f,0.2f,0.2f,1.0f};
	GLfloat Specular[] = {1.0f,1.0f,1.0f,1.0f};
	
	// set color	
	float r,g,b,a;
	if (!m_bIsConstrained)
		if (m_bIsHighlighted)
			props->GetHighlightColor (r,g,b,a);
		else
			props->GetColor (r,g,b,a);
	else
		props->GetConstraintColor (r,g,b,a);

	GLfloat fColor [] = {r,g,b,1.0f};

	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,Ambient);
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,fColor);
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,Specular);
	glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0);

	// apply transformation
	glTranslatef(m_vCenter[0], m_vCenter[1], m_vCenter[2]);
	glRotatef(Vista::RadToDeg(acos(m_qOrientation[3])*2), 
		m_qOrientation[0], m_qOrientation[1], m_qOrientation[2]);
	
	glScalef(m_fScale,m_fScale,m_fScale);

	VistaQuaternion qAxis (VistaAxisAndAngle(VistaVector3D(0,0,1), Vista::DegToRad(m_fAngle))); 
	// rotate to handle's angle
	glRotatef(Vista::RadToDeg(acos(qAxis[3])*2), 
		qAxis[0], qAxis[1], qAxis[2]);

	// rotate to upright position
	VistaQuaternion q(VistaVector3D(0,0,1), VistaVector3D(0,1,0));
	glRotatef(Vista::RadToDeg(acos(q[3])*2), 
		q[0], q[1], q[2]);

	// draw handle cylinder
	gluQuadricNormals(m_pQuadratic, GLU_SMOOTH);
	gluQuadricTexture(m_pQuadratic, GL_TRUE);
	gluCylinder(m_pQuadratic, GetProperties()->GetLeverRadius(), GetProperties()->GetLeverRadius(), GetProperties()->GetLeverLength(), 16, 16);
	
	glPopMatrix();

	glPopAttrib();
}

void VfaAxisRotateVis::Update()
{
	m_pAngleInfo->Update();
}

void VfaAxisRotateVis::DrawTransparent ()
{

	if(!this->GetProperties()->GetVisible())
		return;

	if (!m_bIsHighlighted)
		return;

	VfaAxisRotateVisProps *props = static_cast<VfaAxisRotateVisProps*>(GetProperties());

	glPushMatrix();
	glPushAttrib( GL_LINE_BIT | GL_LIGHTING_BIT );
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);
	glDisable(GL_ALPHA_TEST);

	// get element main color
	float r,g,b,a;
	if (!m_bIsConstrained)
		props->GetHighlightColor (r,g,b,a);
	else
		props->GetConstraintColor (r,g,b,a);

	glColor4f(r,g,b,a);

	// stipple lines 
	glLineWidth( props->GetLineWidth() );
	glLineStipple( 5, 0x5555 );
	glEnable( GL_LINE_STIPPLE );

	// transform into widget coordinate system
	glTranslatef(m_vCenter[0], m_vCenter[1], m_vCenter[2]);
	glRotatef(Vista::RadToDeg(acos(m_qOrientation[3])*2), 
		m_qOrientation[0], m_qOrientation[1], m_qOrientation[2]);
	glScalef(m_fScale,m_fScale,m_fScale);

	// draw rotation center sphere
	glutSolidSphere( props->GetRotationCenterSize(), 12, 12 );	

	// get vectors for start and last "lines"
	float v3StartPoint[3];
	props->GetStartPosition(v3StartPoint);
	float v3LastPoint[3];
	props->GetLastPosition(v3LastPoint);

	VistaVector3D v3StartPos (v3StartPoint);
	VistaVector3D v3LastPos (v3LastPoint);


	// draw connections from center to initial and current cursor position
	glBegin( GL_LINES );
	glVertex3fv( &v3LastPos[0] );
	glVertex3f( 0.0f, 0.0f, 0.0f );

	glVertex3fv( &v3StartPos[0] );
	glVertex3f( 0.0f, 0.0f, 0.0f );
	glEnd();				

	// determine rotation for angular arc
	VistaVector3D v3RotationAxis (0,0,1);

	v3StartPos.Normalize();
	v3LastPos.Normalize();

	float fSegmentAngle = m_fAngle;

	// Make arc smaller than the connection lines
	float fLength = props->GetLeverLength()*props->GetRotationArcLength();
	v3StartPos = v3StartPos* fLength;
	v3LastPos = v3LastPos* fLength;

	// draw angular arc
	glColor4f( r, g, b, 0.5f * a );

	// start at min angle, if set
	VistaVector3D v3ArcHelp = v3StartPos;
	if (props->GetMinAngle()!=-1)
	{
		VistaQuaternion qArcStartRotation( VistaAxisAndAngle( v3RotationAxis,
			Vista::DegToRad( props->GetMinAngle() ) ) );
		v3ArcHelp = qArcStartRotation.Rotate (v3ArcHelp);
	}

	// rotate around qRotation, until fSegmentAngle reached
	VistaQuaternion qRotation( VistaAxisAndAngle( v3RotationAxis,
		Vista::DegToRad( props->GetRotationArcDegreesPerSegment() ) ) );

	glBegin( GL_TRIANGLE_FAN );
	glVertex3f( 0.0f, 0.0f, 0.0f );
	glVertex3fv( &v3ArcHelp[0] );

	for( float fAngle = (props->GetMinAngle()!=-1) ? props->GetMinAngle() + props->GetRotationArcDegreesPerSegment() : props->GetRotationArcDegreesPerSegment();
		fAngle < fSegmentAngle;
		fAngle = fAngle+props->GetRotationArcDegreesPerSegment() )
	{
		v3ArcHelp = qRotation.Rotate( v3ArcHelp );
		glVertex3fv( &v3ArcHelp[0] );
	}
	glVertex3fv( &v3LastPos[0] );		
	glEnd();


	VistaVector3D v3MinBounds (v3StartPos);

	// if min angle set, draw lines and arc for range
	if ((props->GetMinAngle()!=-1))
	{
		// compute min/max bound lines
		VistaQuaternion qMinRotation( VistaAxisAndAngle( v3RotationAxis,
			Vista::DegToRad( props->GetMinAngle() ) ) );
		v3MinBounds = qMinRotation.Rotate (v3StartPos);
		v3MinBounds.Normalize();
		v3MinBounds *= fLength;

		props->GetConstraintColor (r,g,b,a);

		glColor4f( r, g, b, a );

		glBegin( GL_LINES );
		glVertex3fv( &v3MinBounds[0] );
		glVertex3f( 0.0f, 0.0f, 0.0f );
		glEnd();	

	}

	VistaVector3D v3MaxBounds (v3StartPos);

	// if max angle set, draw lines and arc for range
	if ((props->GetMaxAngle()!=-1))
	{
		// compute min/max bound lines
		VistaQuaternion qMaxRotation( VistaAxisAndAngle( v3RotationAxis,
			Vista::DegToRad( props->GetMaxAngle() ) ) );
		v3MaxBounds = qMaxRotation.Rotate (v3StartPos);
		v3MaxBounds.Normalize();
		v3MaxBounds *= fLength;

		props->GetConstraintColor (r,g,b,a);

		glColor4f( r, g, b, a );

		glBegin( GL_LINES );
		glVertex3fv( &v3MaxBounds[0] );
		glVertex3f( 0.0f, 0.0f, 0.0f );
		glEnd();	

	}

	// draw between min, max angle, not inside, but outside
	// to indicate the possible range
	if ((props->GetMinAngle()!=-1)||(props->GetMaxAngle()!=-1))
	{
		float fMinAngle = (props->GetMinAngle()!=-1) ? props->GetMinAngle() : 0;
		float fMaxAngle = (props->GetMaxAngle()==-1)||(props->GetMaxAngle()==0) ? 360 : props->GetMaxAngle();

		glColor4f( r, g, b, 0.5f*a );

		VistaQuaternion qBackwardRotation( VistaAxisAndAngle( v3RotationAxis,
			-1*Vista::DegToRad( props->GetRotationArcDegreesPerSegment() ) ) );

		bool bBeforeZero = (fMinAngle>=0);
		// draw angular arc
		glBegin( GL_TRIANGLE_FAN );
		glVertex3f( 0.0f, 0.0f, 0.0f );
		glVertex3fv( &v3MinBounds[0] );
		float fAngle = fMinAngle-props->GetRotationArcDegreesPerSegment();
		while (bBeforeZero || (fAngle > fMaxAngle))
		{
			v3MinBounds = qBackwardRotation.Rotate( v3MinBounds );
			glVertex3fv( &v3MinBounds[0] );
			fAngle -= props->GetRotationArcDegreesPerSegment();
			if (fAngle<0)
			{
				bBeforeZero=false;
				fAngle = 360+fAngle;
			}
		}

		glVertex3fv( &v3MaxBounds[0] );		
		glEnd();
	}

	// draw constraint warning
	if (m_bIsConstrained)
	{
		float fContact[3];
		props->GetContactPosition(fContact);
		VistaVector3D v3Contact (fContact);
		float fDevice[3];
		props->GetDevicePosition(fDevice);
		VistaVector3D v3Device (fDevice);

		float fConstraintAngle = props->GetConstraintMinAngle();

		// vector between intersection point and device projected to plane
		VistaVector3D v3ArcHelp = v3Device;

		// a little bit left
		VistaQuaternion qArcStartRotation( VistaAxisAndAngle( VistaVector3D(0,1,0),
			Vista::DegToRad( -1*fConstraintAngle ) ) );
		VistaVector3D v3ArcStart = qArcStartRotation.Rotate (v3ArcHelp);
		
		// a little bit right
		VistaQuaternion qArcEndRotation( VistaAxisAndAngle( VistaVector3D(0,1,0),
				Vista::DegToRad( fConstraintAngle ) ) );
		VistaVector3D v3ArcEnd = qArcEndRotation.Rotate (v3ArcHelp);

		// draw constraint fan
		VistaQuaternion qRotation( VistaAxisAndAngle( VistaVector3D(0,1,0),
			Vista::DegToRad( props->GetRotationArcDegreesPerSegment() ) ) );

		glColor4f( r, g, b, a );

		glBegin( GL_TRIANGLE_FAN );
		glVertex3f( 0.0f, 0.0f, 0.0f );
		glVertex3fv( &v3ArcStart[0] );

		for( float fAngle = -1*fConstraintAngle;
			fAngle < fConstraintAngle;
			fAngle = fAngle+props->GetRotationArcDegreesPerSegment() )
		{
			v3ArcStart = qRotation.Rotate( v3ArcStart );
			glVertex3fv( &v3ArcStart[0] );
		}
		glVertex3fv( &v3ArcEnd[0] );		
		glEnd();
	}


	glDisable( GL_LINE_STIPPLE );
	glEnable(GL_LIGHTING);
	glEnable (GL_CULL_FACE);

	glPopAttrib();
	glPopMatrix();
}

unsigned int VfaAxisRotateVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE | IVflRenderable::OLI_DRAW_TRANSPARENT | IVflRenderable::OLI_UPDATE;
}

void VfaAxisRotateVis::SetIsHighlighted(bool b)
{
	m_bIsHighlighted = b;
	m_pAngleInfo->SetVisible(m_bIsHighlighted); 
}
bool VfaAxisRotateVis::GetIsHighlighted() const
{
	return m_bIsHighlighted;
}

void VfaAxisRotateVis::SetIsConstrained(bool b)
{
	m_bIsConstrained = b;
}
bool VfaAxisRotateVis::GetIsConstrained() const
{
	return m_bIsConstrained;
}

bool VfaAxisRotateVis::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
bool VfaAxisRotateVis::GetBounds(VistaBoundingBox &)
{
	return false;
}

void VfaAxisRotateVis::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaAxisRotateModel *pModel = dynamic_cast<VfaAxisRotateModel*>(pObserveable);

	if(!pModel)
		return;

	// get position, orientation and scale in vis space
	pModel->GetPosition(m_vCenter);
	pModel->GetOrientation (m_qOrientation);
	m_fScale = pModel->GetScale();

	// get current angle
	m_fAngle = pModel->GetAngle ();

	float fMin, fMax;
	if (pModel->GetAngleRanges (fMin, fMax))
	{
		this->GetProperties()->SetMinAngle (fMin);
		this->GetProperties()->SetMaxAngle (fMax);
	}
}

VfaAxisRotateVis::VfaAxisRotateVisProps *VfaAxisRotateVis::GetProperties() const
{
	return static_cast<VfaAxisRotateVis::VfaAxisRotateVisProps *> (GetProperties());
}

IVflRenderable::VflRenderableProperties* VfaAxisRotateVis::CreateProperties() const
{
	return new VfaAxisRotateVis::VfaAxisRotateVisProps();
}

VfaAxisRotateVis::VfaAxisRotateVisProps::VfaAxisRotateVisProps()
:m_fLeverRadius(0.1f),
  m_fLeverLength(1.0f),
  m_fLineWidth(2.0f),
  m_fRotationCenterSize(1.0f),
  m_fRotationArcLength(0.0f),
  m_fRotationArcDegreesPerSegment(1.0f),
  m_fMaxAngle(-1.0f),
  m_fMinAngle(-1.0f),
  m_fConstraintMinAngle(0.0f)
{
	m_aColor[0] = 0.0f;
    m_aColor[1] = 0.0f;
    m_aColor[2] = 0.0f;
    m_aColor[3] = 1.0f;

	m_aHighlightColor[0] = 0.0f;
    m_aHighlightColor[1] = 0.0f;
    m_aHighlightColor[2] = 0.0f;
    m_aHighlightColor[3] = 1.0f;

    m_aOutofRangeColor[0] = 0.0f;
    m_aOutofRangeColor[1] = 0.0f;
    m_aOutofRangeColor[2] = 0.0f;
    m_aOutofRangeColor[3] = 1.0f;

	m_v3LastPosition[0] = 0.0f;
	m_v3LastPosition[1] = 0.0f;
	m_v3LastPosition[2] = 0.0f;

	m_v3StartPosition[0] = 0.0f;
	m_v3StartPosition[1] = 0.0f;
	m_v3StartPosition[2] = 0.0f;

	m_v3ContactPosition[0] = 0.0f;
	m_v3ContactPosition[1] = 0.0f;
	m_v3ContactPosition[2] = 0.0f;

	m_v3DevicePosition[0] = 0.0f;
	m_v3DevicePosition[1] = 0.0f;
	m_v3DevicePosition[2] = 0.0f;

}
VfaAxisRotateVis::VfaAxisRotateVisProps::~VfaAxisRotateVisProps()
{
}


void VfaAxisRotateVis::VfaAxisRotateVisProps::SetLeverRadius (float fRadius)
{
	m_fLeverRadius = fRadius;
}
float VfaAxisRotateVis::VfaAxisRotateVisProps::GetLeverRadius () const
{
	return m_fLeverRadius;
}

void VfaAxisRotateVis::VfaAxisRotateVisProps::SetLeverLength (float l)
{
	m_fLeverLength = l;
	Notify (MSG_LENGTH_CHANGED);
}
float VfaAxisRotateVis::VfaAxisRotateVisProps::GetLeverLength () const
{
	return m_fLeverLength;
}

void  VfaAxisRotateVis::VfaAxisRotateVisProps::GetStartPosition(float fStartPosition[3]) const
{
	memcpy (fStartPosition,m_v3StartPosition,3*sizeof(float));
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetStartPosition(float fStartPosition[3])
{
	bool bChg = false;
	for(int i=0; i<3; ++i)
		bChg |= (compAndAssignFunc(fStartPosition[i], m_v3StartPosition[i]) ? true : false);
	if(bChg)
	{
		this->Notify(MSG_STARTPOSITION_CHANGED);
		return true;
	}
	return false;
}

void  VfaAxisRotateVis::VfaAxisRotateVisProps::GetLastPosition(float fLastPosition[3]) const
{
	memcpy (fLastPosition,m_v3LastPosition,3*sizeof(float));
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetLastPosition(float fLastPosition[3])
{
	bool bChg = false;
	for(int i=0; i<3; ++i)
		bChg |= (compAndAssignFunc(fLastPosition[i], m_v3LastPosition[i]) ? true : false);
	if(bChg)
	{
		this->Notify(MSG_LASTPOSITION_CHANGED);
		return true;
	}
	return false;
}

void  VfaAxisRotateVis::VfaAxisRotateVisProps::GetContactPosition(float fContactPosition[3]) const
{
	memcpy (fContactPosition,m_v3ContactPosition,3*sizeof(float));
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetContactPosition(float fContactPosition[3])
{
	bool bChg = false;
	for(int i=0; i<3; ++i)
		bChg |= (compAndAssignFunc(fContactPosition[i], m_v3ContactPosition[i]) ? true : false);
	if(bChg)
	{
		this->Notify(MSG_CONTACTPOSITION_CHANGED);
		return true;
	}
	return false;
}

void  VfaAxisRotateVis::VfaAxisRotateVisProps::GetDevicePosition(float fDevicePosition[3]) const
{
	memcpy (fDevicePosition,m_v3DevicePosition,3*sizeof(float));
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetDevicePosition(float fDevicePosition[3])
{
	bool bChg = false;
	for(int i=0; i<3; ++i)
		bChg |= (compAndAssignFunc(fDevicePosition[i], m_v3DevicePosition[i]) ? true : false);
	if(bChg)
	{
		this->Notify(MSG_DEVICEPOSITION_CHANGED);
		return true;
	}
	return false;
}

float VfaAxisRotateVis::VfaAxisRotateVisProps::GetLineWidth() const
{
	return m_fLineWidth;
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetLineWidth(float fWidth)
{
	if(compAndAssignFunc<float>(fWidth, m_fLineWidth))
	{
		Notify(MSG_LINE_WIDTH_CHANGE);
		return true;
	}
	return false;
}


float VfaAxisRotateVis::VfaAxisRotateVisProps::GetMinAngle() const
{
	return m_fMinAngle;
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetMinAngle(float fWidth)
{
	if(compAndAssignFunc<float>(fWidth, m_fMinAngle))
	{
		Notify(MSG_MINANGLE_CHANGE);
		return true;
	}
	return false;
}

float VfaAxisRotateVis::VfaAxisRotateVisProps::GetMaxAngle() const
{
	return m_fMaxAngle;
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetMaxAngle(float fWidth)
{
	if(compAndAssignFunc<float>(fWidth, m_fMaxAngle))
	{
		Notify(MSG_MAXANGLE_CHANGE);
		return true;
	}
	return false;
}

float VfaAxisRotateVis::VfaAxisRotateVisProps::GetConstraintMinAngle() const
{
	return m_fConstraintMinAngle;
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetConstraintMinAngle(float fWidth)
{
	if(compAndAssignFunc<float>(fWidth, m_fConstraintMinAngle))
	{
		Notify(MSG_CONSTRAINTMINANGLE_CHANGE);
		return true;
	}
	return false;
}
float VfaAxisRotateVis::VfaAxisRotateVisProps::GetRotationCenterSize() const
{
	return m_fRotationCenterSize;
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetRotationCenterSize(float fWidth)
{
	if(compAndAssignFunc<float>(fWidth, m_fRotationCenterSize))
	{
		Notify(MSG_ROTATION_CENTER_SIZE_CHANGE);
		return true;
	}
	return false;
}

float VfaAxisRotateVis::VfaAxisRotateVisProps::GetRotationArcLength() const
{
	return m_fRotationArcLength;
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetRotationArcLength(float fWidth)
{
	if(compAndAssignFunc<float>(fWidth, m_fRotationArcLength))
	{
		Notify(MSG_ROTATION_ARCLENGTH_CHANGE);
		return true;
	}
	return false;
}

float VfaAxisRotateVis::VfaAxisRotateVisProps::GetRotationArcDegreesPerSegment() const
{
	return m_fRotationArcDegreesPerSegment;
}

bool  VfaAxisRotateVis::VfaAxisRotateVisProps::SetRotationArcDegreesPerSegment(float fWidth)
{
	if(compAndAssignFunc<float>(fWidth, m_fRotationArcDegreesPerSegment))
	{
		Notify(MSG_ROTATIONARCDEGREES_CHANGE);
		return true;
	}
	return false;
}

bool VfaAxisRotateVis::VfaAxisRotateVisProps::SetColor(
												float r, float g, float b, float a)
{
	bool bChange = (compAndAssignFunc<float>(r, m_aColor[0]) == 1) ? true : false;
	bChange |= ((compAndAssignFunc<float>(g, m_aColor[1]) == 1) ? true : false) || bChange;
	bChange |= ((compAndAssignFunc<float>(b, m_aColor[2]) == 1) ? true : false) || bChange;
	bChange |= ((compAndAssignFunc<float>(a, m_aColor[3]) == 1) ? true : false) || bChange;
	if(bChange)
	{
		Notify(MSG_COLOR_CHANGE);
		return true;
	}

	return false;
}

bool VfaAxisRotateVis::VfaAxisRotateVisProps::GetColor(
											float &r, float &g, float &b, float &a) const
{
	r = m_aColor[0];
	g = m_aColor[1];
	b = m_aColor[2];
	a = m_aColor[3];
	return true;
}

bool VfaAxisRotateVis::VfaAxisRotateVisProps::SetHighlightColor(
												float r, float g, float b, float a)
{
	bool bChange = (compAndAssignFunc<float>(r, m_aHighlightColor[0]) == 1) ? true : false;
	bChange |= ((compAndAssignFunc<float>(g, m_aHighlightColor[1]) == 1) ? true : false) || bChange;
	bChange |= ((compAndAssignFunc<float>(b, m_aHighlightColor[2]) == 1) ? true : false) || bChange;
	bChange |= ((compAndAssignFunc<float>(a, m_aHighlightColor[3]) == 1) ? true : false) || bChange;
	if(bChange)
	{
		Notify(MSG_HIGHTLIGHTCOLOR_CHANGE);
		return true;
	}

	return false;
}

bool VfaAxisRotateVis::VfaAxisRotateVisProps::GetHighlightColor(
											float &r, float &g, float &b, float &a) const
{
	r = m_aHighlightColor[0];
	g = m_aHighlightColor[1];
	b = m_aHighlightColor[2];
	a = m_aHighlightColor[3];
	return true;
}

bool VfaAxisRotateVis::VfaAxisRotateVisProps::SetConstraintColor(
												float r, float g, float b, float a)
{
	bool bChange = (compAndAssignFunc<float>(r, m_aOutofRangeColor[0]) == 1) ? true : false;
	bChange |= ((compAndAssignFunc<float>(g, m_aOutofRangeColor[1]) == 1) ? true : false) || bChange;
	bChange |= ((compAndAssignFunc<float>(b, m_aOutofRangeColor[2]) == 1) ? true : false) || bChange;
	bChange |= ((compAndAssignFunc<float>(a, m_aOutofRangeColor[3]) == 1) ? true : false) || bChange;
	if(bChange)
	{
		Notify(MSG_OUT_OF_RANGE_COLOR_CHANGE);
		return true;
	}

	return false;
}

bool VfaAxisRotateVis::VfaAxisRotateVisProps::GetConstraintColor(
											float &r, float &g, float &b, float &a) const
{
	r = m_aOutofRangeColor[0];
	g = m_aOutofRangeColor[1];
	b = m_aOutofRangeColor[2];
	a = m_aOutofRangeColor[3];
	return true;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "          "                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

