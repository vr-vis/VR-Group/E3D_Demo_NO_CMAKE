/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef __VFATIMEAXISROTATIONMEDIATOR_H
#define __VFATIMEAXISROTATIONMEDIATOR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaAxisRotateModel;
class VflVisTiming;
class VveTimeMapper;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* This mediator handles mediation between a AxisRotateWidget and a vis timing.
* Movement of the widget is transformed into time and vice versa.
*
* Therefore, a time mapper (to translate visualization time into simulation time) and a constant angular velocity (to compute time from angular movement)
* are necessary. For instance, the target is the blood pump's impeller with an angular velocity of
* 460 degrees/second.
*
* @author  Marc Wolter
* @date    September 2010
*
*/
class VISTAFLOWLIBAUXAPI VfaTimeAxisRotationMediator : public VflObserver
{
public:

	VfaTimeAxisRotationMediator(VflVisTiming *pVisTiming, VfaAxisRotateModel* pWidgetModel);
	virtual ~VfaTimeAxisRotationMediator();

	/**
     * Set/get time mapper.
	 * 
     */  
	void SetTimeMapper (VveTimeMapper* pTimeMapper);
	VveTimeMapper* GetTimeMapper () const;

	/**
     * Set/get velocity in simulation time.
	 * 
     */  
	void SetAngularVelocity (float fV);
	float GetAngularVelocity () const;

	// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);


private:
	VflVisTiming *			m_pVisTiming;
	VfaAxisRotateModel*	m_pWidgetModel;
	
	bool                    m_bIsTimeModelUpdate;
	bool                    m_bIsWidgetUpdate;

	VveTimeMapper*         m_pTargetTimeMapper;

	float		            m_fAngularVelocity;
	float		            m_fLastAngle;

}; //end of local class


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // __VFATIMEAXISROTATIONMEDIATOR_H
