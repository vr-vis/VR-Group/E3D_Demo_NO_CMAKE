/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAAXISROTATEMODEL_H
#define _VFAAXISROTATEMODEL_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

// model
#include <VistaFlowLibAux/Widgets/VfaWidgetModelBase.h>

#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * VfaAxisRotateModel is the model part of a MVC widget for rotation around an axis.
 * The main data member is retrieved via GetAngle, which is the current rotation angle.
 */
class VISTAFLOWLIBAUXAPI VfaAxisRotateModel : public VfaWidgetModelBase
{
	friend class VfaAxisRotateWidget;
public:
	/**
	* messages sent by this reflectionable upon property changes
	* 	
	*/
	enum{
		MSG_AXIS_CHANGED = VfaWidgetModelBase::MSG_LAST, /**< the lever was moved */
		MSG_POSITION_CHANGED,                             /**< the widget has moved */
		MSG_LAST
	};

	VfaAxisRotateModel();
	virtual ~VfaAxisRotateModel();

	virtual std::string GetReflectionableType() const;

	/*
	* position of widget
	*
	*/
	void SetPosition (float x, float y, float z);
	void GetPosition (float &x, float &y, float &z) const;
	void GetPosition (VistaVector3D & v) const;


	/*
	* orientation of widget
	*
	*/
	void SetOrientation (const VistaQuaternion & q);
	void GetOrientation (VistaQuaternion & q) const;

	/*
	* scale
	*
	*/
	void SetScale (float fScale);
	float GetScale () const;

	
	/**
	 * Method:      SetAngle
	 *
	 * Sets current angle. If the given value is out of the current min-max-range (@see SetAngleRanges),
	 * the value is clamped into the valid range.
	 *
	 * @param       fAngle
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/28
	 */
	void SetAngle (float fAngle);

	float GetAngle () const;

	/**
	 * Method:      SetAngleRanges
	 *
	 * Set valid angle range, values outside this range are not allowed.
	 * Does not check if fMinAngle or fMaxAngle are valid values.
	 *
	 * @param[in]   fMinAngle
	 * @param[in]   fMaxAngle
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/28
	 */
	void SetAngleRanges (float fMinAngle, float fMaxAngle);
	
	bool GetAngleRanges (float &fMinAngle, float &fMaxAngle) const;



protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;


private:
	float							 m_fPosition[3];
	float							 m_fAngle;
	float							 m_fAngleRanges[2];
	VistaQuaternion				 m_qOrientation;
	float							 m_fScale;

}; //end of local class

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFAAXISROTATEMODEL_H



