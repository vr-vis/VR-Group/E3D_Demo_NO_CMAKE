/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaAxisRotateWidget.h" 

#include <VistaFlowLib/Visualization/VflRenderNode.h>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/


/*============================================================================*/
/* WIDGET					                                                  */
/*============================================================================*/
VfaAxisRotateWidget::VfaAxisRotateWidget(VflRenderNode *pRenderNode)
:IVfaWidget(pRenderNode),
m_pVis(new VfaAxisRotateVis(pRenderNode)),
m_pModel(new VfaAxisRotateModel()),
m_bEnabled(true),
m_bVisible(true)
{
	m_pVis->Init();
	pRenderNode->AddRenderable(m_pVis);

	m_pWidgetCtl = new VfaAxisRotateController(pRenderNode, this);

	m_pVis->Observe(GetModel());
	GetModel()->Notify();

}

VfaAxisRotateWidget::~VfaAxisRotateWidget()
{
	delete m_pWidgetCtl;
	delete m_pVis;
	delete m_pModel;

}


void VfaAxisRotateWidget::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pWidgetCtl->SetIsEnabled (m_bEnabled);
}

bool VfaAxisRotateWidget::GetIsEnabled() const
{
	return m_bEnabled;
}

void VfaAxisRotateWidget::SetIsVisible(bool b)
{
	m_bVisible = b;
	m_pVis->SetVisible(m_bVisible);
}

bool VfaAxisRotateWidget::GetIsVisible() const
{
	return m_bVisible;
}
	
VfaAxisRotateModel *VfaAxisRotateWidget::GetModel() const
{
	return m_pModel;
}
VfaAxisRotateVis* VfaAxisRotateWidget::GetView() const
{
	return m_pVis;
}
VfaAxisRotateController* VfaAxisRotateWidget::GetController() const
{
	return m_pWidgetCtl;
}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "          "                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

