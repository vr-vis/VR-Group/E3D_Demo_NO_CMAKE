/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAAXISROTATEWIDGET_H
#define _VFAAXISROTATEWIDGET_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VfaAxisRotateModel.h"
#include "VfaAxisRotateController.h"
#include "VfaAxisRotateVis.h"

// widget
#include <VistaFlowLibAux/Widgets/VfaWidget.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * VfaAxisRotateWidget combines a specific Model, View and Controller
 * to construct a working widget.
 *
 *  The widget's purpose is to manage a rotation movement. The view consists of a
 *  visual lever, that is manipulated by a sphere handle on top of the lever.
 *  The result is an angle value between 0 and 360 (which can be restricted to a smaller range).
 *
 *	\image HTML VistaFlowLibAux/axisrotate.jpg "Screenshot of axis rotate widget"
 */
class VISTAFLOWLIBAUXAPI VfaAxisRotateWidget : public IVfaWidget
{
public:
	
	VfaAxisRotateWidget (VflRenderNode *pRenderNode);
	virtual ~VfaAxisRotateWidget();

	/**
	 * Set enable state of entire widget.
	 * A disabled widget will not handle events
	 */
	void SetIsEnabled(bool b);

	/** Get enabled state of entire widget */
	bool GetIsEnabled() const;

	/** show widget */
	void SetIsVisible(bool b);

	/** hide widget */
	bool GetIsVisible() const;	

	/**
	* @name Get main MVC components.
	*/
	//@{
	VfaAxisRotateVis* GetView() const;

	VfaAxisRotateModel *GetModel() const;

	VfaAxisRotateController* GetController() const;
	//@}

private:
	VfaAxisRotateController  *m_pWidgetCtl;
	VfaAxisRotateVis		  *m_pVis;

	VfaAxisRotateModel		*m_pModel;
	bool					m_bEnabled;
	bool					m_bVisible;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFAAXISROTATEWIDGET_H



