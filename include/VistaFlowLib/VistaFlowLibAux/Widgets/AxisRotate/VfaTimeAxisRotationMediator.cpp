/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaTimeAxisRotationMediator.h"
#include "VfaAxisRotateWidget.h"
#include <VistaFlowLib/Visualization/VflVisTiming.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTimeAxisRotationMediator::VfaTimeAxisRotationMediator(VflVisTiming *pVisTiming, VfaAxisRotateModel* pWidgetModel)
:m_pVisTiming (pVisTiming),
m_pWidgetModel(pWidgetModel),
m_bIsTimeModelUpdate(false),
m_bIsWidgetUpdate(false),
m_pTargetTimeMapper(NULL),
m_fLastAngle(0.0f),
m_fAngularVelocity(0.0f)
{
	this->Observe(m_pVisTiming);
	this->Observe (m_pWidgetModel);

}

VfaTimeAxisRotationMediator::~VfaTimeAxisRotationMediator()
{
	this->ReleaseObserveable(m_pVisTiming);
	this->ReleaseObserveable(m_pWidgetModel);
}

void VfaTimeAxisRotationMediator::SetTimeMapper (VveTimeMapper* pTimeMapper)
{
	m_pTargetTimeMapper = pTimeMapper;
}

VveTimeMapper* VfaTimeAxisRotationMediator::GetTimeMapper () const
{
	return m_pTargetTimeMapper;
}

void VfaTimeAxisRotationMediator::SetAngularVelocity (float fV)
{
	m_fAngularVelocity = fV;
}
float VfaTimeAxisRotationMediator::GetAngularVelocity () const
{
	return m_fAngularVelocity;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaTimeAxisRotationMediator::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if (pObserveable==m_pWidgetModel && !m_bIsTimeModelUpdate)
	{
		m_bIsWidgetUpdate = true;
		if (msg == VfaAxisRotateModel::MSG_AXIS_CHANGED)
		{
			//stop animation for position control
			if (m_pVisTiming->GetAnimationPlaying())
				m_pVisTiming->SetAnimationPlaying(false);

			float fValue = m_pWidgetModel->GetAngle();
			
			 //get difference angle between current (euler angle) and old (m_fAngle)
			float fDifference = fValue-m_fLastAngle;

			// if this difference is larger than 180 degrees,
			// we have a wrap-around the 0/360 degree point
			if (fabs(fDifference) > 180)
			{
				if (fValue<m_fLastAngle)
					fDifference = fValue +(360-m_fLastAngle);
				else
					fDifference = -1*((360-fValue)+m_fLastAngle);
			}

			double dSim = m_pTargetTimeMapper->GetSimulationTime(m_pVisTiming->GetVisualizationTime());
			dSim += (fDifference)/m_fAngularVelocity;

			double dVisStart, dVisEnd;
			m_pVisTiming->GetVisTimeRange (dVisStart, dVisEnd);
			double dSimStart = m_pTargetTimeMapper->GetSimulationTime(dVisStart);
			double dSimEnd  = m_pTargetTimeMapper->GetSimulationTime(dVisEnd);

			bool m_bConstraintMet = false;

			// cut at valid borders
			if (dSim > dSimEnd)
			{
				// wrap around if full vis range
				if ((dVisStart==0.0) && (dVisEnd==1.0))
					dSim = dSimStart;
				else
				{
					// else: constrain
					dSim = dSimEnd;
					m_bConstraintMet = true;
				}
			}

			if (dSim < dSimStart)
			{
				// wrap around if full vis range
				if ((dVisStart==0.0) && (dVisEnd==1.0))
					dSim = dSimEnd;
				else
				{
					// else: constrain
					dSim = dSimStart;
					m_bConstraintMet = true;
				}
			}
			m_pVisTiming->SetVisualizationTime(m_pTargetTimeMapper->GetVisualizationTime(dSim));
			// set value as last value
			m_fLastAngle= fValue;
		}
		
		m_bIsWidgetUpdate = false;
	}
	if (pObserveable==m_pVisTiming && !m_bIsWidgetUpdate)
	{
		m_bIsTimeModelUpdate = true;
		if (msg == VflVisTiming::MSG_VISTIME_CHG)
		{
			// compute from value an angle
			float fAngle = (float) fmod (m_pTargetTimeMapper->GetSimulationTime(m_pVisTiming->GetVisualizationTime())*m_fAngularVelocity, 360.0);
			m_fLastAngle = fAngle;
			m_pWidgetModel->SetAngle (fAngle);
			
		}
		if (msg == VflVisTiming::MSG_RANGE_CHG)
		{
			// get vis ranges
			double dVisStart, dVisEnd;
			m_pVisTiming->GetVisTimeRange (dVisStart, dVisEnd);

			// get sim ranges
			double dSimStart = m_pTargetTimeMapper->GetSimulationTime(dVisStart);
			double dSimEnd  = m_pTargetTimeMapper->GetSimulationTime(dVisEnd);


			float fLeftValue = -1.0f;
			float fRightValue = -1.0f;
			double dSimTime = m_pTargetTimeMapper->GetSimulationTime(m_pVisTiming->GetVisualizationTime());
			// if any range is set, evaluate it here and adapt ranges of the widget model
			if ((dVisStart!=0.0)||(dVisEnd!=1.0))
			{
				// check, if the current dSim value is in the "same rotation" as the min/max sim values
				// if yes, set the appropriate bound on min/max angle
				float fFullRotation = 360.0f/m_fAngularVelocity;

				if (floor (dSimTime/fFullRotation) == floor (dSimStart/fFullRotation))
					fLeftValue = (float) fmod (dSimStart*m_fAngularVelocity, 360.0);
				
				if (floor(dSimTime/fFullRotation) == floor (dSimEnd/fFullRotation))
					fRightValue = (float) fmod (dSimEnd*m_fAngularVelocity, 360.0);
				
				
			}
			m_pWidgetModel->SetAngleRanges (fLeftValue, fRightValue);
		}
		m_bIsTimeModelUpdate =false;
	}
}
/*============================================================================*/
/*  END OF FILE "VtnTimeAxisRotationMediator.cpp"                             */
/*============================================================================*/


