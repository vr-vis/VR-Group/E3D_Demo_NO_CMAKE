

set( RelativeDir "./Widgets/AxisRotate" )
set( RelativeSourceGroup "Source Files\\Widgets\\AxisRotate" )

set( DirFiles
	VfaAxisRotateController.cpp
	VfaAxisRotateController.h
	VfaAxisRotateModel.cpp
	VfaAxisRotateModel.h
	VfaAxisRotateVis.cpp
	VfaAxisRotateVis.h
	VfaAxisRotateWidget.cpp
	VfaAxisRotateWidget.h
	VfaTimeAxisRotationMediator.cpp
	VfaTimeAxisRotationMediator.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

