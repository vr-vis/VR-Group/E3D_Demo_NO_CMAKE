/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAAXISROTATEVIS_H
#define _VFAAXISROTATEVIS_H

#if defined(DARWIN)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// widget
#include <VistaFlowLibAux/Widgets/VfaWidget.h>


// vis
#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <VistaBase/VistaVectorMath.h>
#include <string>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisController;
class VfaSphereHandle;
class Vfl3DTextLabel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/


/**
* VfaAxisRotateVis is a view for the axis rotation widget. It draws a lever as cylinder,
* as well as a visual arc showing the current rotation angle and a textual angle information.
*
*/
class VISTAFLOWLIBAUXAPI VfaAxisRotateVis : public IVflRenderable
{
public: 
	VfaAxisRotateVis(VflRenderNode *pRenderNode);
	virtual ~VfaAxisRotateVis();
	

	//Here the rendering is done
	void DrawOpaque();
	void DrawTransparent();
	void Update();

	/** Retrieve registration mode for rendering */
	unsigned int GetRegistrationMode() const;


	/**
	 * If the widget is highlighted it is drawn in a more noticeable color.
	 * @see VfaAxisRotateVisProps::SetHightlightColor
	 */
	void SetIsHighlighted(bool b);
	bool GetIsHighlighted() const;

	/**
	 * If the widget is constrained it is drawn in a warning color and
	 * also shows the current minimum constraint angle.
	 *
	 *	\image HTML VistaFlowLibAux/constraint.jpg "Constraint warning"
	 *
	 * @see VfaAxisRotateVisProps::SetOutofRangeColor
	 */
	void SetIsConstrained(bool b);
	bool GetIsConstrained() const;

	/**
	 * Redefine GetBounds in order to indicate that the widget should not be included
	 * in the computation of vis bounds (simply mark our bounds as invalid)
	 *
	 * @todo not implemented
	 */
	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);	

	/**
	 * Internal properties class for visual properties only.
	 *
	 */
	class VISTAFLOWLIBAUXAPI VfaAxisRotateVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		VfaAxisRotateVisProps();
		~VfaAxisRotateVisProps();

		void SetLeverRadius (float fRadius);
		float GetLeverRadius () const;

		/**
		* Method:      SetLeverLength
		*
		* Set length of lever in current coordinate system.
		*
		* @param       fLen
		* @return      void
		* @author      av006wo
		* @date        2010/09/28
		*/
		void SetLeverLength (float fLen);
		float GetLeverLength () const;

		/**
		 * Method:      SetStartPosition
		 *
		 *  This is the position from which the arc starts. It is shown with a stippled line.
		 *
		 * @param       fStartPosition
		 * @return      bool
		 * @author      av006wo
		 * @date        2010/09/28
		 */
		bool SetStartPosition(float fStartPosition[3]);
		void GetStartPosition(float fStartPosition[3]) const;

		/**
		 * Method:      SetLastPosition
		 *
		 * This is the position at which the arc ends. It is shown with a stippled line.
		 * The positions are in widget space.
		 *
		 * @param       fLastPosition
		 * @return      bool
		 * @author      av006wo
		 * @date        2010/09/28
		 */
		bool SetLastPosition(float fLastPosition[3]);
		void GetLastPosition(float fLastPosition[3]) const;

		float GetLineWidth() const;
		bool  SetLineWidth(float fWidth);

		
		/**
		 * Method:      SetColor
		 *
		 * Color for the lever.
		 *
		 * @param       r
		 * @param       g
		 * @param       b
		 * @param       a
		 * @return      bool
		 * @author      av006wo
		 * @date        2010/09/28
		 */
		bool  SetColor(float r, float g, float b, float a);
		bool  GetColor(float &r, float &g, float &b, float &a) const;

		
		/**
		 * Method:      SetHighlightColor
		 *
		 *  Color for the arc and highlighted lever.
		 *
		 * @param       r
		 * @param       g
		 * @param       b
		 * @param       a
		 * @return      bool
		 * @author      av006wo
		 * @date        2010/09/28
		 */
		bool  SetHighlightColor(float r, float g, float b, float a);
		bool  GetHighlightColor(float &r, float &g, float &b, float &a) const;

		/**
		 * Method:      SetRotationCenterSize
		 *
		 * The rotation center (around ReferencePoint) is shown by a sphere, which size can be specified here.
		 *
		 * @param       fRotationCenterSize
		 * @return      bool
		 * @author      av006wo
		 * @date        2010/09/28
		 */
		bool  SetRotationCenterSize(float fRotationCenterSize);
		float GetRotationCenterSize() const;

		
		
		/**
		 * Method:      SetRotationArcLength
		 *
		 * Set length (i.e. radius) of arc.
		 *
		 * @param       fRotationArcLength
		 * @return      bool
		 * @author      av006wo
		 * @date        2010/09/28
		 */
		bool  SetRotationArcLength(float fRotationArcLength);
		float GetRotationArcLength() const;

		
		
		/**
		 * Method:      SetRotationArcDegreesPerSegment
		 *
		 * Set degrees of each segment of the arc. The smaller the angle, the more segments will be drawn,
		 * resulting in smoother arc roundings.
		 *
		 * @param       fRotationArcDegreesPerSegment
		 * @return      bool
		 * @author      av006wo
		 * @date        2010/09/28
		 */
		bool  SetRotationArcDegreesPerSegment(float fRotationArcDegreesPerSegment);
		float GetRotationArcDegreesPerSegment() const;

		
		/**
		 * Method:      SetMinAngle
		 *
		 *  This is the minimum possible angle between start and last position. The area
		 * between Max and Min which can not be reached is drawn using OutofRangeColor.
		 *
		 * Set these values to -1 if not used (default).
		 *
		 * @param       fAngle
		 * @return      bool
		 * @author      av006wo
		 * @date        2010/09/28
		 */
		bool  SetMinAngle(float fAngle);
		float GetMinAngle() const;
		
		/**
		 * Method:      SetMaxAngle
		 *
		 *  This is the maximum possible angle between start and last position. The area
		 * between Max and Min which can not be reached is drawn using OutofRangeColor.
		 *
		 * Set these values to -1 if not used (default).
		 *
		 * @param       fAngle
		 * @return      bool
		 * @author      av006wo
		 * @date        2010/09/28
		 */
		bool  SetMaxAngle(float fAngle);
		float GetMaxAngle() const;

		bool  SetConstraintMinAngle(float fAngle);
		float GetConstraintMinAngle() const;

		bool  SetConstraintColor(float r, float g, float b, float a);
		bool  GetConstraintColor(float &r, float &g, float &b, float &a) const;

		/** for debugging purposes */
		void GetContactPosition(float fContactPosition[3]) const;
		bool SetContactPosition(float fContactPosition[3]);

		/** for debugging purposes */
		void GetDevicePosition(float fDevicePosition[3]) const;
		bool SetDevicePosition(float fDevicePosition[3]);

		enum
		{
			MSG_LINE_WIDTH_CHANGE = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_STARTPOSITION_CHANGED,
			MSG_LASTPOSITION_CHANGED,
			MSG_CONTACTPOSITION_CHANGED,
			MSG_DEVICEPOSITION_CHANGED,
			MSG_ROTATION_CENTER_SIZE_CHANGE,
			MSG_ROTATION_ARCLENGTH_CHANGE,
			MSG_ROTATIONARCDEGREES_CHANGE,
			MSG_COLOR_CHANGE,
			MSG_HIGHTLIGHTCOLOR_CHANGE,
			MSG_OUT_OF_RANGE_COLOR_CHANGE,
			MSG_MINANGLE_CHANGE,
			MSG_MAXANGLE_CHANGE,
			MSG_CONSTRAINTMINANGLE_CHANGE,
			MSG_LENGTH_CHANGED,                               /**< the lever's length has changed */
			MSG_LAST
		};
	private:
		float	m_fLeverRadius;
		float	m_fLeverLength;

		float   m_aColor[4], m_aHighlightColor[4], m_aOutofRangeColor[4];
        float   m_v3LastPosition[3], m_v3StartPosition[3];
        float   m_v3ContactPosition[3], m_v3DevicePosition[3];
		float	m_v3RotationAxis[3];
		float	m_fAngle;
		float	m_fMinAngle;
		float	m_fMaxAngle;
		float	m_fConstraintMinAngle;
        float   m_fLineWidth;
        float   m_fRotationCenterSize;
        float   m_fRotationArcLength;
        float   m_fRotationArcDegreesPerSegment;
	};

	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	VfaAxisRotateVisProps *GetProperties() const;
protected:
	virtual VflRenderableProperties* CreateProperties() const;


private:
	GLUquadricObj *       m_pQuadratic;

	VistaVector3D        m_vCenter;
	VistaQuaternion      m_qOrientation;
	float                 m_fScale;

	/** local copy of model value*/
	float                 m_fAngle;

	bool                  m_bIsHighlighted;
	bool                  m_bIsConstrained;

	VistaReferenceFrame  m_oWidgetRefFrame;
	Vfl3DTextLabel*      m_pAngleInfo;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFAAXISROTATEVIS_H



