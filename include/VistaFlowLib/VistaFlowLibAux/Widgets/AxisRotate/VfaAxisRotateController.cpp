/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here
#include <GL/glew.h>

#include "VfaAxisRotateWidget.h" 

#include <VistaFlowLibAux/Widgets/VfaSphereHandle.h>


#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <iomanip>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* WIDGET CONTROLLER		                                                  */
/*============================================================================*/
VfaAxisRotateController::VfaAxisRotateController(VflRenderNode* pRenderNode, VfaAxisRotateWidget *pWidget)
:IVfaWidgetController(pRenderNode),
m_pWidget(pWidget),
m_pHandle(new VfaSphereHandle(pRenderNode)),
m_iGrabSlot(0),
m_bActiveMoving(false),
m_fAngleSnapping(0.0f),
m_fConstrainedMinimumAngle(0.0f)
{
	m_pView = m_pWidget->GetView();
	this->AddControlHandle(m_pHandle);
	// some default value, should be changed later by SetHandleRadius
	m_pHandle->SetRadius (0.2f);

	this->Observe (m_pWidget->GetModel());


}
VfaAxisRotateController::~VfaAxisRotateController()
{
	this->ReleaseObserveable(m_pWidget->GetModel());
}


void VfaAxisRotateController::OnFocus(IVfaControlHandle* pHandle)
{
	// highlight handle & view, propagate highlight color to handle
	m_pHandle->SetIsHighlighted(true);
	float fColor[4];
	m_pWidget->GetView()->GetProperties()->GetHighlightColor(fColor[0], fColor[1],fColor[2],fColor[3]);
	m_pHandle->SetHighlightColor (fColor);

	m_pWidget->GetView()->SetIsHighlighted (true);

}
void VfaAxisRotateController::OnUnfocus()
{
	// only de-highlight if the user currently doesn't move the lever
	if (!m_bActiveMoving)
	{
		// un-highlight handle & view, propagate highlight color to handle
		m_pHandle->SetIsHighlighted(false);
		m_pWidget->GetView()->SetIsHighlighted (false);
		m_pWidget->GetView()->SetIsConstrained(false);

		// this sets back the handle's highlight color... as a SphereHandle does not have a constraint color
		// the highlight color of the handle is hijacked in case of an active constraint
		float fColor[4];
		m_pWidget->GetView()->GetProperties()->GetHighlightColor(fColor[0], fColor[1],fColor[2],fColor[3]);
		m_pHandle->SetHighlightColor (fColor);

	}

}

void VfaAxisRotateController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{
}
void VfaAxisRotateController::OnUntouch()
{
}

void VfaAxisRotateController::OnSensorSlotUpdate (int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	m_oPointerFrame = oFrameData;

	// if the user currently grabs the lever, she can move it
	if (m_bActiveMoving)
	{
		// Plane and ray interaction (see class documentation)

		// ray in widget space
		VistaVector3D vecDirection (0,0,-1);
		vecDirection = m_oPointerFrame.qOrientation.Rotate (vecDirection);
		vecDirection = m_matWidgetTrans.GetInverted().Transform(vecDirection);
		VistaVector3D vecDevPos = m_matWidgetTrans.GetInverted().Transform(m_oPointerFrame.v3Position);
		VistaRay oRay (vecDevPos, vecDirection);

		// plane in widget space
		VistaVector3D vecAlongPlane (0,1,0);
		VistaVector3D vecNormal (0,0,1);
		VistaVector3D vecUp = vecAlongPlane.Cross (vecNormal);
		VistaPlane oPlane (VistaVector3D(0,0,0), vecAlongPlane, vecUp, vecNormal);

		// give device position on plane to view for debugging
		float fDevPos [3];
		oPlane.CalcNearestPointOnPlane(vecDevPos).GetValues(fDevPos);
		m_pView->GetProperties()->SetDevicePosition (fDevPos);

		// intersection point of ray and plane
		VistaVector3D v3PosWidgetSpace;
		oPlane.CalcIntersection (oRay, v3PosWidgetSpace);

		// get plane-ray angle to test with m_fConstrainedMinimumAngle 
		float fPlaneRayAngle = acos ((float) vecNormal.Dot (vecDirection) /(vecDirection.GetLength()*vecNormal.GetLength()));

		if ((fabs(Vista::RadToDeg(fPlaneRayAngle))<90.0+m_fConstrainedMinimumAngle)
			&&(fabs(Vista::RadToDeg(fPlaneRayAngle))>90.0-m_fConstrainedMinimumAngle))
		{
			m_pWidget->GetView()->SetIsConstrained(true);
			// in addition, set handle color to out of range
			float fColor[4];
			m_pWidget->GetView()->GetProperties()->GetConstraintColor(fColor[0], fColor[1],fColor[2],fColor[3]);
			m_pHandle->SetHighlightColor (fColor);
			return;
		}
		else
		{
			m_pWidget->GetView()->SetIsConstrained(false);
			// this sets back the handle's highlight color... as a SphereHandle does not have a constraint color
			// the highlight color of the handle is hijacked in case of an active constraint
			float fColor[4];
			m_pWidget->GetView()->GetProperties()->GetHighlightColor(fColor[0], fColor[1],fColor[2],fColor[3]);
			m_pHandle->SetHighlightColor (fColor);
		}

		// delete z-component => transform to x-y-plane
		v3PosWidgetSpace[2] = 0.0f;

		float fLeverLength = m_pWidget->GetView()->GetProperties()->GetLeverLength();

		// for debugging purposes
		float fContactPos[3];
		v3PosWidgetSpace.GetValues(fContactPos);
		m_pView->GetProperties()->SetContactPosition (fContactPos);

		// set direction to Length (circle around origin)
		v3PosWidgetSpace = v3PosWidgetSpace.GetNormalized () * fLeverLength;

		// set rotation from origin position to new position
		VistaQuaternion quatRotation (VistaVector3D(0,fLeverLength,0), v3PosWidgetSpace);

		float fPosWidgetSpace [3];
		v3PosWidgetSpace.GetValues(fPosWidgetSpace);

		// get yaw
		float fOldAngle = m_pWidget->GetModel()->GetAngle();
		float fEulerAngle = Vista::RadToDeg (quatRotation.GetAngles().c);

		// wrap aroudn 360 degrees if < zero
		if (fEulerAngle<0)
			fEulerAngle = 360+fEulerAngle;

		// check ranges before setting values
		float fMin, fMax;
		m_pWidget->GetModel()->GetAngleRanges(fMin, fMax);

		if (m_fAngleSnapping > 0.0f)
		{
			fEulerAngle = (floor (fEulerAngle/m_fAngleSnapping))*m_fAngleSnapping;
			
			// get "zero position"
			VistaVector3D v3ArcHelp = VistaVector3D (0, fLeverLength, 0);
			
			// rotate around fEulerAngle degrees  around rotation axis
			VistaQuaternion qRotation( VistaAxisAndAngle( VistaVector3D(0,0,1),
				Vista::DegToRad( fEulerAngle ) ) );

			// correct widget space position to corrected angle
			VistaVector3D vCorrectedPosition = qRotation.Rotate (v3ArcHelp);
			vCorrectedPosition.GetValues (fPosWidgetSpace);
		}

		if (((fMin == -1.0f)||(fEulerAngle>=fMin))
			&&((fMax == -1.0f)||(fEulerAngle<=fMax)))
		{
			// this changes the model data
			m_pWidget->GetModel()->SetAngle(fEulerAngle);
			m_pWidget->GetView()->GetProperties()->SetLastPosition(fPosWidgetSpace);
		}

	}

}
void VfaAxisRotateController::OnCommandSlotUpdate (int iSlot, const bool bSet)
{
	if (iSlot != m_iGrabSlot)
		return;

	if (bSet)
	{
		if (this->GetControllerState()==IVfaWidgetController::CS_FOCUS)
		{
			// set start position (always 0 degree)
			m_bActiveMoving = true;
			float fStart [3] = {0,m_pWidget->GetView()->GetProperties()->GetLeverLength(),0};
			m_pWidget->GetView()->GetProperties()->SetStartPosition(fStart);
		}
	}
	else
	{
		m_bActiveMoving = false;
		if (this->GetControllerState()!=CS_FOCUS)
		{
			m_pHandle->SetIsHighlighted(false);
			m_pWidget->GetView()->SetIsHighlighted (false);
			m_pWidget->GetView()->SetIsConstrained(false);
			// this sets back the handle's highlight color... as a SphereHandle does not have a constraint color
			// the highlight color of the handle is hijacked in case of an active constraint
			float fColor[4];
			m_pWidget->GetView()->GetProperties()->GetHighlightColor(fColor[0], fColor[1],fColor[2],fColor[3]);
			m_pHandle->SetHighlightColor (fColor);
		}
	}
}
void VfaAxisRotateController::OnTimeSlotUpdate (const double dTime)
{
}

int VfaAxisRotateController::GetSensorMask() const
{
	return (1 << VfaApplicationContextObject::SLOT_POINTER_VIS);
}
int VfaAxisRotateController::GetCommandMask() const
{
	return (1 << m_iGrabSlot);
}
bool VfaAxisRotateController::GetTimeUpdate() const
{
	return false;
}


void VfaAxisRotateController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pHandle->SetIsSelectionEnabled(m_bEnabled);
	m_pHandle->SetEnable (false);
}
bool VfaAxisRotateController::GetIsEnabled() const
{
	return m_bEnabled;
}

int VfaAxisRotateController::GetGrabSlot() const
{
	return m_iGrabSlot;
}
void VfaAxisRotateController::SetGrabSlot(int i)
{
	m_iGrabSlot = i;
}

void VfaAxisRotateController::SetAngleSnapping (float fAngle)
{
	// make sure this is positive
	m_fAngleSnapping = fabs(fAngle);
}
float VfaAxisRotateController::GetAngleSnapping () const
{
	return m_fAngleSnapping;
}

void VfaAxisRotateController::SetConstrainedMinimumAngle (float fAngle)
{
	m_fConstrainedMinimumAngle = fabs(fAngle);
	// propagate to view for debugging
	m_pView->GetProperties()->SetConstraintMinAngle (m_fConstrainedMinimumAngle);
}
float VfaAxisRotateController::GetConstrainedMinimumAngle () const
{
	return m_fConstrainedMinimumAngle;
}

void VfaAxisRotateController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaAxisRotateModel *pModel = dynamic_cast<VfaAxisRotateModel*>(pObserveable);

	if(!pModel)
		return;

	// handle only transformation changes
	VistaVector3D v3Center;
	pModel->GetPosition(v3Center);
	VistaQuaternion qOri;
	pModel->GetOrientation(qOri);
	m_matWidgetTrans = VistaTransformMatrix (qOri, v3Center);

	// get rotation axis in vis space
	VistaVector3D vAxis (0,0,1);
	VistaQuaternion qAxisRotation (VistaAxisAndAngle (vAxis, Vista::DegToRad(pModel->GetAngle())));
	
	VistaVector3D vHandlePoint (0, m_pView->GetProperties()->GetLeverLength(), 0);
	vHandlePoint = qAxisRotation.Rotate(vHandlePoint);

	// transform from widget space to vis space
	vHandlePoint = m_matWidgetTrans.Transform (vHandlePoint);
	m_pHandle->SetCenter (vHandlePoint);
}

bool VfaAxisRotateController::SetHandleRadius( float fRadius )
{
	if (!m_pHandle)
		return false;

	m_pHandle->SetRadius (fRadius);
	return true;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "          "                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

