/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFATEXTBOXCONTROLLER_H
#define VFATEXTBOXCONTROLLER_H

/*============================================================================*/
/* INCLUDES	                                                                  */
/*============================================================================*/
#include "../VfaWidgetController.h"

#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaBase/VistaVectorMath.h>

#include <vector>
#include <string>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaTextBoxModel;
class VfaTextBoxView;
class VfaTexturedBoxHandle;
class VfaStateMachine;
class IVfaState;
class IVfaTransition;

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaTextBoxController : public IVfaWidgetController, public VflObserver
{
public:
	VfaTextBoxController( VfaTextBoxModel* pModel, VfaTextBoxView* pView,
		VflRenderNode* pRenderNode );
	virtual ~VfaTextBoxController();

	void SetInteractionSlot(int iSlot);
	int GetInteractionSlot() const;

	enum Buttons
	{	
		BTN_NONE = -1,
		BTN_MOVE_HANDLE,
		BTN_RESIZE_HANDLE,
		BTN_SCROLL_HANDLE,
		BTN_ACCEPT_HANDLE,
		BTN_CANCEL_HANDLE,
		BTN_LAST
	};

	bool LoadHandleTexture(int iHandle, const std::string& strFileName);

	// *** IVfaWidgetController interface. ***
	virtual void OnFocus( IVfaControlHandle* pHandle );
	virtual void OnUnfocus();

	virtual void SetIsEnabled(bool bIsEnabled);
	virtual bool GetIsEnabled() const;

	// *** IVfaSlotObserver interface. ***
	virtual void OnSensorSlotUpdate( int iSlot,
		const VfaApplicationContextObject::sSensorFrame &oFrameData );
	virtual void OnCommandSlotUpdate( int iSlot, const bool bSet );
	virtual void OnTimeSlotUpdate( const double dTime );

	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;

	// *** IVistaObserver interface. ***
	virtual void ObserverUpdate( IVistaObserveable *pObserveable, int msg,
		int ticket );

private:
	void SetupHandles(VflRenderNode *pRenderNode);
	void UpdateHandles();
	void SetupStateMachine();
	
	void MoveWidget( const VistaVector3D &v3PointerPos,
		const VistaQuaternion &qPointerDir );
	void ResizeWidget( const VistaVector3D &v3PointerPos,
		const VistaQuaternion &qPointerDir );
	void ScrollText( const VistaVector3D &v3PointerPos,
		const VistaQuaternion &qPointerDir );


	bool								m_bIsEnabled;
	VfaTextBoxModel*					m_pModel;
	VfaTextBoxView*						m_pView;

	VfaTexturedBoxHandle*				m_pActiveHandle;
	VfaTexturedBoxHandle*				m_pFocusedHandle;
	std::vector<VfaTexturedBoxHandle*>	m_vecCastedHandles;

	VfaStateMachine*					m_pStateMachine;
	IVfaState*							m_pIdleState;
	IVfaState*							m_pMoveWidgetState;
	IVfaState*							m_pResizeWidgetState;
	IVfaState*							m_pScrollTextState;
	IVfaState*							m_pResizeHandleFocusedState;
	IVfaState*							m_pMoveHandleFocusedState;
	IVfaState*							m_pScrollHandleFocusedState;
	IVfaTransition*						m_pFocusMoveHandle;
	IVfaTransition*						m_pFocusResizeHandle;
	IVfaTransition*						m_pFocusScrollHandle;
	IVfaTransition*						m_pButtonDownHandle;
	IVfaTransition*						m_pButtonUpHandle;
	IVfaTransition*						m_pUnfocusHandle;

	int									m_iSelSlot;
	//! A timestamp for velocity-based position updates of the scroll pos.
	double								m_dTimestamp;
	//! Distance of a clicked handle from the pointer origin in pointer dir.
	float								m_fDistFromPointer;
	//! A flag which carries over a button press to the sensor slot update func.
	bool								m_bNeedCacheUpdate;
	//! A null pos for scrolling (up/down movement relative to the null pos).
	VistaVector3D						m_v3ScrollNullPos;

	//! The Position and orientation of the Widget relative to the pointer
	//! Used when the Widget gets moved
	VistaVector3D						m_v3PositionOffset;
	VistaQuaternion						m_qOrientationOffset;
	

	//! Some variables for transformation stuff (user orientation).
	VistaVector3D						m_v3Right;
	VistaVector3D						m_v3Up;
	VistaVector3D						m_v3Normal;

	bool								m_bOrientTowardsUserCache;
	bool								m_bWdgtRefFramePosDirty;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
