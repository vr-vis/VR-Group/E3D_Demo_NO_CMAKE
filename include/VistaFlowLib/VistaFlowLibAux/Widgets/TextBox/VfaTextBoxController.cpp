/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaTextBoxController.h"
#include "VfaTextBoxModel.h"
#include "VfaTextBoxView.h"

#include "../StateMachine/VfaStateMachine.h"
#include "../StateMachine/VfaEmptyState.h"
#include "../StateMachine/VfaEmptyTransition.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLibAux/Widgets/VfaTexturedBoxHandle.h>

#include <string>
#include <cassert>

using namespace std;


/*============================================================================*/
/* CONSTANT VALUES                                                            */
/*============================================================================*/
enum {
	S_IDLE,
	S_MOVE_HANDLE_FOCUSED,
	S_RESIZE_HANDLE_FOCUSED,
	S_SCROLL_HANDLE_FOCUSED,
	S_MOVE_WIDGET,
	S_RESIZE_WIDGET,
	S_SCROLL_TEXT,
	T_FOCUS_MOVE_HANDLE,
	T_FOCUS_RESIZE_HANDLE,
	T_FOCUS_SCROLL_HANDLE,
	T_BUTTON_DOWN,
	T_BUTTON_UP,
	T_UNFOCUS
};

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
VfaTextBoxController::VfaTextBoxController( VfaTextBoxModel* pModel,
	VfaTextBoxView* pView, VflRenderNode* pRenderNode )
:	IVfaWidgetController( pRenderNode )
,	m_bIsEnabled( true )
,	m_pModel( pModel )
,	m_pView( pView )
,	m_pActiveHandle( NULL )
,	m_pFocusedHandle( NULL )
,	m_pStateMachine( NULL )
,	m_pIdleState( new VfaEmptyState )
,	m_pMoveWidgetState( new VfaEmptyState )
,	m_pResizeWidgetState( new VfaEmptyState )
,	m_pScrollTextState( new VfaEmptyState )
,	m_pResizeHandleFocusedState( new VfaEmptyState )
,	m_pMoveHandleFocusedState( new VfaEmptyState )
,	m_pScrollHandleFocusedState( new VfaEmptyState )
,	m_pFocusMoveHandle( new VfaEmptyTransition )
,	m_pFocusResizeHandle( new VfaEmptyTransition )
,	m_pFocusScrollHandle( new VfaEmptyTransition )
,	m_pButtonDownHandle( new VfaEmptyTransition )
,	m_pButtonUpHandle( new VfaEmptyTransition )
,	m_pUnfocusHandle( new VfaEmptyTransition )
,	m_iSelSlot( 0 )
,	m_dTimestamp( 0.0 )
,	m_fDistFromPointer( 0.0f )
, 	m_bNeedCacheUpdate( false )
, 	m_bWdgtRefFramePosDirty( false )
{
	assert( m_pModel && m_pView && GetRenderNode() );
	m_pModel->AttachObserver( this );

	SetupHandles( GetRenderNode() );
	SetupStateMachine();	

	m_v3Right		= VistaVector3D( 1.0f, 0.0f, 0.0f, 0.0f );
	m_v3Up			= VistaVector3D( 0.0f, 1.0f, 0.0f, 0.0f );
	m_v3Normal		= VistaVector3D( 0.0f, 0.0f, 1.0f, 0.0f );
}

VfaTextBoxController::~VfaTextBoxController()
{
	delete m_pStateMachine;
	m_pModel->DetachObserver(this);

	for( size_t i=0; i<m_vecCastedHandles.size(); ++i )
	{
		if(m_vecCastedHandles[i])
		{
			RemoveControlHandle(m_vecCastedHandles[i]);
			delete m_vecCastedHandles[i];
		}
	}
}


void VfaTextBoxController::SetInteractionSlot(int iSlot)
{
	m_iSelSlot = iSlot;
}

int VfaTextBoxController::GetInteractionSlot() const
{
	return m_iSelSlot;
}

bool VfaTextBoxController::LoadHandleTexture(	int iHandle, 
												const string& strFileName )
{
	if(iHandle <= BTN_NONE || iHandle >= BTN_LAST)
		return false;

	return m_vecCastedHandles[iHandle]->LoadTexture(strFileName);
}

void VfaTextBoxController::OnFocus(IVfaControlHandle *pHandle)
{
	if(!m_bIsEnabled)
		return;

	m_pFocusedHandle = dynamic_cast<VfaTexturedBoxHandle*>(pHandle);
	
	assert(m_pFocusedHandle);

	m_pModel->Notify(VfaTextBoxModel::MSG_ACTIVATED);

	if(m_pStateMachine->GetCurrentState() == m_pIdleState)
	{
		if(pHandle == m_vecCastedHandles[BTN_MOVE_HANDLE])
		{
			m_vecCastedHandles[BTN_MOVE_HANDLE]->SetIsHighlighted(true);
			m_pStateMachine->AttemptTransition(m_pFocusMoveHandle);
			// m_pActiveHandle = m_vecCastedHandles[BTN_MOVE_HANDLE];
		}
		else if(pHandle == m_vecCastedHandles[BTN_RESIZE_HANDLE])
		{
			m_vecCastedHandles[BTN_RESIZE_HANDLE]->SetIsHighlighted(true);
			m_pStateMachine->AttemptTransition(m_pFocusResizeHandle);
			// m_pActiveHandle = m_vecCastedHandles[BTN_RESIZE_HANDLE];
		}
		else if(pHandle == m_vecCastedHandles[BTN_SCROLL_HANDLE])
		{
			m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetIsHighlighted(true);
			m_pStateMachine->AttemptTransition(m_pFocusScrollHandle);
			// m_pActiveHandle = m_vecCastedHandles[BTN_SCROLL_HANDLE];
		}
		else
		{
			m_pFocusedHandle->SetIsHighlighted(true);
		}
	}
}

void VfaTextBoxController::OnUnfocus()
{
	if(!m_bIsEnabled)
		return;

	m_pFocusedHandle->SetIsHighlighted(false);
	m_pFocusedHandle = 0;
	
	if(m_pStateMachine->AttemptTransition(m_pUnfocusHandle))
	{
		if(m_pActiveHandle)
		{
			m_pActiveHandle->SetIsHighlighted(false);
			m_pActiveHandle = 0;
		}
	}

	if(!m_pFocusedHandle && !m_pActiveHandle)
		m_pModel->Notify(VfaTextBoxModel::MSG_DEACTIVATED);
}


void VfaTextBoxController::SetIsEnabled(bool bIsEnabled)
{
	if(bIsEnabled == m_bIsEnabled)
		return;
	
	m_bIsEnabled = bIsEnabled;
	
	for(size_t i=0; i<m_vecCastedHandles.size(); ++i)
	{
		m_vecCastedHandles[i]->SetEnable(bIsEnabled);
		m_vecCastedHandles[i]->SetIsVisible(bIsEnabled);

		if(!m_bIsEnabled)
			m_vecCastedHandles[i]->SetIsHighlighted(false);
	}

	if(m_bIsEnabled)
		m_pStateMachine->Reset();
	else if(!m_bIsEnabled)
		m_pModel->Notify(VfaTextBoxModel::MSG_DEACTIVATED);
}

bool VfaTextBoxController::GetIsEnabled() const
{
	return m_bIsEnabled;
}


void VfaTextBoxController::OnSensorSlotUpdate(int iSlot,
	const VfaApplicationContextObject::sSensorFrame &oFrameData)
{
	if(!m_bIsEnabled)
		return;

	if(iSlot == VfaApplicationContextObject::SLOT_VIEWER_VIS)
	{
		if(m_pModel->GetOrientTowardsUser())
		{
			VistaVector3D v3WdgtPos;
			m_pModel->GetPosition(v3WdgtPos);
			
			VistaTransformMatrix matInvTrans;
			GetRenderNode()->GetInverseTransform(matInvTrans);

			m_v3Up = matInvTrans.Transform(VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f));
			m_v3Up.Normalize();

			m_v3Normal = GetRenderNode()->GetLocalViewPosition() - v3WdgtPos;
			m_v3Normal.Normalize();

			m_v3Right = m_v3Up.Cross(m_v3Normal);
			m_v3Right.Normalize();

			m_v3Up = m_v3Normal.Cross(m_v3Right);
			m_v3Up.Normalize();

			float aMat[4][4] = {
				m_v3Right[0], m_v3Up[0], m_v3Normal[0], 0.0f,
				m_v3Right[1], m_v3Up[1], m_v3Normal[1], 0.0f,
				m_v3Right[2], m_v3Up[2], m_v3Normal[2], 0.0f,
						0.0f,	   0.0f,		  0.0f, 1.0f
			};

			VistaTransformMatrix matTrans(aMat);
			VistaQuaternion qWdgtOri(matTrans);
			qWdgtOri.Normalize();

			m_pModel->SetOrientation(qWdgtOri);

			for(size_t i=0; i<m_vecCastedHandles.size(); ++i)
				m_vecCastedHandles[i]->SetRotation(qWdgtOri);
		}

		UpdateHandles();
	} // SLOT_VIEWER_VIS
	else if(iSlot == VfaApplicationContextObject::SLOT_POINTER_VIS)
	{
		// Somehow the SLOT_POINTER_VIS pointer position is strange/wrong.
		// (Mainly when calculation the nearest pos on the beam from the handle
		// to be selected.)
		// So instead we'll use the world position and translate the pointer
		// position by the world translation. This leads to the desired results!
		// Orientation seems to be just fine.
		VistaVector3D v3PtrPos = oFrameData.v3Position;			

		VistaReferenceFrame oReferenceFrame;
		oReferenceFrame.SetTranslation(oFrameData.v3Position);
		oReferenceFrame.SetRotation(oFrameData.qOrientation);

		if(m_pStateMachine->GetCurrentState() == m_pMoveWidgetState)
		{
			// This flag is 'true' if a button has just been pressed.
			// Since this might lead to a handle being activated, we need
			// to update the distance of the handle into pointer dir.
			if(m_bNeedCacheUpdate)
			{
				m_bNeedCacheUpdate = false;

				VistaVector3D v3HandlePos;

				m_vecCastedHandles[BTN_MOVE_HANDLE]->GetPosition(v3HandlePos, oReferenceFrame);
				
				m_fDistFromPointer = (v3HandlePos-v3PtrPos).Dot(
					oFrameData.qOrientation.GetViewDir());

				VistaQuaternion qInvPtrOri = 
					oFrameData.qOrientation.GetInverted();

				m_v3PositionOffset = m_pModel->GetPosition() - v3PtrPos;
				m_v3PositionOffset = qInvPtrOri.Rotate(m_v3PositionOffset);

				m_qOrientationOffset = qInvPtrOri*m_pModel->GetOrientation();
			}

			this->MoveWidget(v3PtrPos, oFrameData.qOrientation);

			m_bWdgtRefFramePosDirty = true;
		}
		else if(m_pStateMachine->GetCurrentState() == m_pResizeWidgetState)
		{
			// Se comment above for explanation of this.
			if(m_bNeedCacheUpdate)
			{
				m_bNeedCacheUpdate = false;

				VistaVector3D v3HandlePos;
				m_vecCastedHandles[BTN_RESIZE_HANDLE]->GetPosition(v3HandlePos, oReferenceFrame);
				
				m_fDistFromPointer = (v3HandlePos-v3PtrPos).Dot(
					oFrameData.qOrientation.GetViewDir());
			}

			this->ResizeWidget(v3PtrPos, oFrameData.qOrientation);

			m_bWdgtRefFramePosDirty = true;
		}
		else if(m_pStateMachine->GetCurrentState() == m_pScrollTextState)
		{
			// Se comment above for explanation of this.
			if(m_bNeedCacheUpdate)
			{
				m_bNeedCacheUpdate = false;

				m_v3ScrollNullPos = oFrameData.v3Position;
			}

			this->ScrollText(v3PtrPos, oFrameData.qOrientation);			
		}
		
		float aBoxSize[2], aTextSize[2];
		m_pModel->GetTextBoxSize(aBoxSize);
		m_pView->GetTextDims(aTextSize);

		if(aTextSize[1] <= aBoxSize[1])
		{
			m_pModel->SetScrollPos(0.0f);

			if(m_vecCastedHandles[BTN_SCROLL_HANDLE]->GetEnable())
			{
				m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetEnable(false);
				m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetIsVisible(false);
			}
		}
		else if(!m_vecCastedHandles[BTN_SCROLL_HANDLE]->GetEnable())
		{
			m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetEnable(true);
			m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetIsVisible(true);
		}
	} // SLOT_POINTER_WORLD
}

void VfaTextBoxController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	if(!m_bIsEnabled)
		return;	
	
	if(iSlot != m_iSelSlot)
		return;

	if(bSet)
	{
		if(m_pStateMachine->AttemptTransition(m_pButtonDownHandle))
		{
			m_pActiveHandle = m_pFocusedHandle;

			if(!m_pActiveHandle)
				return;

			// Reset timestamp. Mainly used for velocity-based scrolling calcs.
			m_dTimestamp = GetRenderNode()->GetVisTiming()->GetCurrentClock();
			m_bNeedCacheUpdate = true;

			if(m_pStateMachine->GetCurrentState() == m_pResizeWidgetState)
			{
				m_bOrientTowardsUserCache = m_pModel->GetOrientTowardsUser();
				m_pModel->SetOrientTowardsUser(false);
			}
		}
		else if(m_pFocusedHandle)
		{
			if(m_pFocusedHandle == m_vecCastedHandles[BTN_ACCEPT_HANDLE])
				m_pModel->Notify(VfaTextBoxModel::MSG_ACCEPT_BUTTON_PRESSED);
			else if(m_pFocusedHandle == m_vecCastedHandles[BTN_CANCEL_HANDLE])
				m_pModel->Notify(VfaTextBoxModel::MSG_CANCEL_BUTTON_PRESSED);
		}
	}
	else
	{
		// NOTE: Simply unfocusing handles based on the OnUnfocus() calls
		//		 won't work with this widget. This is because the handles
		//		 are moved based on the pointer position but they are not(!)
		//		 necessarily moved to the pointer position, so unfocus calls
		//		 will be very common. Unfortunately that does not signal the
		//		 end of interaction with the handle but the release of a mouse
		//		 button does! So we will unfocus handle if the mouse button
		//		 is released and not a second earlier.
		//		 For more help ask sp841227 (Sebastian Pick) and refer to the
		//		 'Moving windows around while losing the title bar
		//		 focus'-problem!
		if(m_pStateMachine->AttemptTransition(m_pButtonUpHandle))
		{
			if(m_pStateMachine->GetCurrentState() == m_pResizeHandleFocusedState)
			{
				m_pModel->SetOrientTowardsUser(m_bOrientTowardsUserCache);
			}

			if(m_pActiveHandle && m_pActiveHandle != m_pFocusedHandle)
			{
				m_pStateMachine->AttemptTransition(m_pUnfocusHandle);

				m_pActiveHandle->SetIsHighlighted(false);
				m_pActiveHandle = m_pFocusedHandle;

				if(m_pFocusedHandle)
				{
					if(m_vecCastedHandles[BTN_MOVE_HANDLE] == m_pFocusedHandle)
						m_pStateMachine->AttemptTransition(m_pFocusMoveHandle);
					else if(m_vecCastedHandles[BTN_RESIZE_HANDLE] == m_pFocusedHandle)
						m_pStateMachine->AttemptTransition(m_pFocusResizeHandle);
					else if(m_vecCastedHandles[BTN_SCROLL_HANDLE] == m_pFocusedHandle)
						m_pStateMachine->AttemptTransition(m_pFocusScrollHandle);
				}
			}
		}
	}
}

void VfaTextBoxController::OnTimeSlotUpdate(const double dTime)
{
	// NOTE: Unused!
}


int VfaTextBoxController::GetSensorMask() const
{
	return (1 << VfaApplicationContextObject::SLOT_POINTER_VIS)
		| (1 << VfaApplicationContextObject::SLOT_VIEWER_VIS);
}

int VfaTextBoxController::GetCommandMask() const
{
	return (1 << m_iSelSlot);
}

bool VfaTextBoxController::GetTimeUpdate() const
{
	return false;
}


void VfaTextBoxController::ObserverUpdate(IVistaObserveable *pObserveable,
											 int msg, int ticket)
{
	if(!m_bIsEnabled)
		return;

	if(pObserveable == m_pModel && 
		msg == VfaTransformableWidgetModel::MSG_ORIENTATION_CHANGE)
	{
		VistaQuaternion qWdgtOri;
		m_pModel->GetOrientation(qWdgtOri);

		m_v3Right  = qWdgtOri.Rotate( VistaVector3D( 1.0f, 0.0f, 0.0f, 0.0f ) );
		m_v3Up     = qWdgtOri.Rotate( VistaVector3D( 0.0f, 1.0f, 0.0f, 0.0f ) );
		m_v3Normal = qWdgtOri.Rotate( VistaVector3D( 0.0f, 0.0f, 1.0f, 0.0f ) );

		UpdateHandles();
	}
}

void VfaTextBoxController::SetupStateMachine()
{
	m_pStateMachine = new VfaStateMachine();

	// Add states.
	m_pStateMachine->AddState(m_pIdleState);
	m_pStateMachine->AddState(m_pMoveHandleFocusedState);
	m_pStateMachine->AddState(m_pResizeHandleFocusedState);
	m_pStateMachine->AddState(m_pScrollHandleFocusedState);
	m_pStateMachine->AddState(m_pMoveWidgetState);
	m_pStateMachine->AddState(m_pScrollTextState);
	m_pStateMachine->AddState(m_pResizeWidgetState);


	// Add Transitions.
	m_pStateMachine->AddTransition(m_pFocusMoveHandle);
	m_pStateMachine->AddTransition(m_pFocusResizeHandle);
	m_pStateMachine->AddTransition(m_pFocusScrollHandle);
	m_pStateMachine->AddTransition(m_pButtonDownHandle);
	m_pStateMachine->AddTransition(m_pButtonUpHandle);
	m_pStateMachine->AddTransition(m_pUnfocusHandle);

	// Wire states.
	m_pStateMachine->WireStates(m_pFocusMoveHandle,
		m_pIdleState, m_pMoveHandleFocusedState);
	m_pStateMachine->WireStates(m_pUnfocusHandle,
		m_pMoveHandleFocusedState, m_pIdleState);
	m_pStateMachine->WireStates(m_pButtonDownHandle,
		m_pMoveHandleFocusedState, m_pMoveWidgetState);
	m_pStateMachine->WireStates(m_pButtonUpHandle,
		m_pMoveWidgetState, m_pMoveHandleFocusedState);

	m_pStateMachine->WireStates(m_pFocusResizeHandle,
		m_pIdleState, m_pResizeHandleFocusedState);
	m_pStateMachine->WireStates(m_pUnfocusHandle,
		m_pResizeHandleFocusedState, m_pIdleState);
	m_pStateMachine->WireStates(m_pButtonDownHandle,
		m_pResizeHandleFocusedState, m_pResizeWidgetState);
	m_pStateMachine->WireStates(m_pButtonUpHandle,
		m_pResizeWidgetState, m_pResizeHandleFocusedState);

	m_pStateMachine->WireStates(m_pFocusScrollHandle,
		m_pIdleState, m_pScrollHandleFocusedState);
	m_pStateMachine->WireStates(m_pUnfocusHandle,
		m_pScrollHandleFocusedState, m_pIdleState);
	m_pStateMachine->WireStates(m_pButtonDownHandle,
		m_pScrollHandleFocusedState, m_pScrollTextState);
	m_pStateMachine->WireStates(m_pButtonUpHandle,
		m_pScrollTextState, m_pScrollHandleFocusedState);

	// Some state machine state changes.
	m_pStateMachine->SetTolerateTransitionFailure(true);
	m_pStateMachine->SetStartState(m_pIdleState);
	m_pStateMachine->Reset();
}


void VfaTextBoxController::SetupHandles(VflRenderNode *pRenderNode)
{
	// Check if handles already init'ed.	
	if(!m_vecCastedHandles.empty())
		return;

	// Some colors for the handles.
	const float	cfSideLength = 2.0f * 0.03f;

	for(size_t i=0; i<BTN_LAST; ++i)
	{
		m_vecCastedHandles.push_back(new VfaTexturedBoxHandle(pRenderNode));
		m_vecCastedHandles.back()->SetEnable(m_bIsEnabled);
		m_vecCastedHandles.back()->SetIsVisible(m_bIsEnabled);
		m_vecCastedHandles.back()->SetSize(cfSideLength, cfSideLength, 0.01f);

		this->AddControlHandle(m_vecCastedHandles.back());
	}

	// Setup the handles' positions.
	float fTextSize = m_pModel->GetTextSize();
	float aBoxSize[2];
	m_pModel->GetTextBoxSize(aBoxSize);

	m_vecCastedHandles[BTN_MOVE_HANDLE]->SetCenter(
		VistaVector3D(0.0f, aBoxSize[1], 0.0f));
	m_vecCastedHandles[BTN_RESIZE_HANDLE]->SetCenter(
		VistaVector3D(aBoxSize[0], 0.0f, 0.0f));
	m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetCenter(
		VistaVector3D(aBoxSize[0] + 0.75f * fTextSize, aBoxSize[1] * 0.5f, 0.0f));
	m_vecCastedHandles[BTN_ACCEPT_HANDLE]->SetCenter(
		VistaVector3D(aBoxSize[0] - 1.2f * cfSideLength, aBoxSize[1], 0.0f));
	m_vecCastedHandles[BTN_CANCEL_HANDLE]->SetCenter(
		VistaVector3D(aBoxSize[0], aBoxSize[1], 0.0f));

	/*
	// Setup the handles' textures.
	m_vecCastedHandles[BTN_MOVE_HANDLE]->LoadTexture(m_pModel->GetTexturePath() + "/icon_move.tga");
	m_vecCastedHandles[BTN_RESIZE_HANDLE]->LoadTexture(m_pModel->GetTexturePath() + "/icon_resize.tga");
	m_vecCastedHandles[BTN_SCROLL_HANDLE]->LoadTexture(m_pModel->GetTexturePath() + "/icon_scroll.tga");
	m_vecCastedHandles[BTN_ACCEPT_HANDLE]->LoadTexture(m_pModel->GetTexturePath() + "/icon_accept.tga");
	m_vecCastedHandles[BTN_CANCEL_HANDLE]->LoadTexture(m_pModel->GetTexturePath() + "/icon_cancel.tga");
	/*/
	// Setup the handles' colors.
	m_vecCastedHandles[BTN_MOVE_HANDLE]->SetNormalColor(VistaColor(VistaColor::DARK_ORANGE));
	m_vecCastedHandles[BTN_RESIZE_HANDLE]->SetNormalColor(VistaColor(VistaColor::GOLD));
	m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetNormalColor(VistaColor(VistaColor::DARK_BLUE));
	m_vecCastedHandles[BTN_ACCEPT_HANDLE]->SetNormalColor(VistaColor(VistaColor::DARK_GREEN));
	m_vecCastedHandles[BTN_CANCEL_HANDLE]->SetNormalColor(VistaColor(VistaColor::DARK_RED));

	m_vecCastedHandles[BTN_MOVE_HANDLE]->SetHighlightedColor(VistaColor(VistaColor::LIGHT_ORANGE));
	m_vecCastedHandles[BTN_RESIZE_HANDLE]->SetHighlightedColor(VistaColor(VistaColor::YELLOW));
	m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetHighlightedColor(VistaColor(VistaColor::BLUE));
	m_vecCastedHandles[BTN_ACCEPT_HANDLE]->SetHighlightedColor(VistaColor(VistaColor::GREEN));
	m_vecCastedHandles[BTN_CANCEL_HANDLE]->SetHighlightedColor(VistaColor(VistaColor::RED));
	//*/
}

void VfaTextBoxController::UpdateHandles()
{
	float aBoxSize[2];
	m_pModel->GetTextBoxSize(aBoxSize);
	// Watch it, dirty way of retrieval. Using the same variable for all
	// side lengths. But since they're supposed to be the same, who cares?!
	float fSideLength, fHeight;
	m_vecCastedHandles[0]->GetSize(fSideLength, fSideLength, fHeight);

	VistaVector3D v3WdgtPos;
	VistaQuaternion v3WdgtOri;
	m_pModel->GetPosition(v3WdgtPos);
	m_pModel->GetOrientation(v3WdgtOri);

	m_vecCastedHandles[BTN_MOVE_HANDLE]->SetCenter(v3WdgtPos
		- (0.5f * aBoxSize[0] + 0.4f * fSideLength) * m_v3Right
		+ (0.5f * aBoxSize[1] + 0.4f * fSideLength) * m_v3Up);
	m_vecCastedHandles[BTN_RESIZE_HANDLE]->SetCenter(v3WdgtPos
		+ (0.5f * aBoxSize[0] + 0.4f * fSideLength) * m_v3Right
		- (0.5f * aBoxSize[1] + 0.4f * fSideLength) * m_v3Up);
	m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetCenter(v3WdgtPos
		+ (0.5f * aBoxSize[0] + 0.75f * m_pModel->GetTextSize()) * m_v3Right);
	m_vecCastedHandles[BTN_ACCEPT_HANDLE]->SetCenter(v3WdgtPos
		+ (0.5f * aBoxSize[0] - 1.2f * fSideLength) * m_v3Right
		+ (0.5f * aBoxSize[1] + 0.4f * fSideLength) * m_v3Up);
	m_vecCastedHandles[BTN_CANCEL_HANDLE]->SetCenter(v3WdgtPos
		+ (0.5f * aBoxSize[0]) * m_v3Right
		+ (0.5f * aBoxSize[1] + 0.4f * fSideLength) * m_v3Up);

	m_vecCastedHandles[BTN_MOVE_HANDLE]->SetRotation(v3WdgtOri);
	m_vecCastedHandles[BTN_RESIZE_HANDLE]->SetRotation(v3WdgtOri);
	m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetRotation(v3WdgtOri);
	m_vecCastedHandles[BTN_ACCEPT_HANDLE]->SetRotation(v3WdgtOri);
	m_vecCastedHandles[BTN_CANCEL_HANDLE]->SetRotation(v3WdgtOri);
}

void VfaTextBoxController::MoveWidget(const VistaVector3D &v3PointerPos,
										 const VistaQuaternion &qPointerDir)
{
	float aBoxSize[2];
	m_pModel->GetTextBoxSize(aBoxSize);

	// Determine new widget pos.
	VistaVector3D v3WdgtPos = v3PointerPos + qPointerDir.Rotate(m_v3PositionOffset);
	
// 	float fSideLength;
// 	m_vecCastedHandles[BTN_MOVE_HANDLE]->GetSize(fSideLength,
// 		fSideLength, fSideLength);
// 
// 	// Take into acount the handles relative pos to the widget pos.
// 	v3WdgtPos += (0.5f * aBoxSize[0] + 0.4f * fSideLength) * m_v3Right;
// 	v3WdgtPos -= (0.5f * aBoxSize[1] + 0.4f * fSideLength) * m_v3Up;
	m_pModel->SetPosition(v3WdgtPos);

	m_pModel->SetOrientation(qPointerDir*m_qOrientationOffset);

	UpdateHandles();
}

void VfaTextBoxController::ResizeWidget(const VistaVector3D &v3PointerPos,
										   const VistaQuaternion &qPointerDir)
{
	float aNewBoxSize[2], aOldBoxSize[2];

	// This position is the one which will be used to determine how much
	// the size of the widget has to change. It is the point on the interaction
	// beam that was closest to the handle when the button got pressed.
	VistaVector3D v3IntPos = v3PointerPos
		+ m_fDistFromPointer * qPointerDir.GetViewDir();

	VistaVector3D v3WdgtPos;
	m_pModel->GetPosition(v3WdgtPos);
	m_pModel->GetTextBoxSize(aOldBoxSize);

	// Calculate the new text field size based on the old size and
	// the relative changes.
	aNewBoxSize[0] = aOldBoxSize[0] + (v3IntPos - (0.5f * aOldBoxSize[0] * m_v3Right + v3WdgtPos)).Dot(m_v3Right);
	aNewBoxSize[1] = aOldBoxSize[1] + ((v3WdgtPos - 0.5f * aOldBoxSize[1] * m_v3Up) - v3IntPos).Dot(m_v3Up);

	// TODO_LOW: Correction for the resize handle.
	float fSideLength;
	m_vecCastedHandles[BTN_RESIZE_HANDLE]->GetSize(fSideLength,
		fSideLength, fSideLength);
	aNewBoxSize[0] -= 0.4f * fSideLength;
	aNewBoxSize[1] -= 0.4f * fSideLength;

	m_pModel->SetTextBoxSize(aNewBoxSize);
	// In some cases (e.g. single-lined text field) it might be, that the
	// box size values passed to the model are being clamped, so re-read
	// them to be sure.
	m_pModel->GetTextBoxSize(aNewBoxSize);

	// Calculate the new widget position.	
	v3WdgtPos += 0.5f * (aNewBoxSize[0] - aOldBoxSize[0]) * m_v3Right;
	v3WdgtPos -= 0.5f * (aNewBoxSize[1] - aOldBoxSize[1]) * m_v3Up;
	m_pModel->SetPosition(v3WdgtPos);

	UpdateHandles();
}

void VfaTextBoxController::ScrollText(const VistaVector3D &v3PointerPos,
										 const VistaQuaternion &qPointerDir)
{
	// First, check if scrolling is needed.
	float aTextSize[2], aBoxSize[2];

	m_pView->GetTextDims(aTextSize);
	m_pModel->GetTextBoxSize(aBoxSize);

	if(aTextSize[1] <= aBoxSize[1])
	{
		m_pModel->SetScrollPos(0.0f);
		return;
	}

	// Determine the scroll position which equals "bottom reached".
	const float fMaxScrollPos = (aTextSize[1]-aBoxSize[1])/aTextSize[1];

	// Time stuff.
	const double dTDelta =
		GetRenderNode()->GetVisTiming()->GetCurrentClock() - m_dTimestamp;
	m_dTimestamp = GetRenderNode()->GetVisTiming()->GetCurrentClock();

	// Widget pparameters.
	VistaVector3D v3WdgtPos;
	m_pModel->GetPosition(v3WdgtPos);

	float fTextSize = m_pModel->GetTextSize();

	// Scroll knob default position.
	VistaVector3D v3NullPos = v3WdgtPos 
		+ (0.5f * aBoxSize[0] + 0.75f * fTextSize) * m_v3Right;
					
	// Scroll parameters.
	VistaVector3D v3ScrollDir = 0.5f * aBoxSize[1] * m_v3Up;
	float fScrollLength = 0.5f * aBoxSize[1];
	v3ScrollDir.Normalize();

	// Input parametes.
	// Scrolling only depends on the device moving up or down.
	// (Int stands for 'Interaction' here.)
	VistaTransformMatrix mInvTrans;
	GetRenderNode()->GetInverseTransform(mInvTrans);

	VistaVector3D v3IntPos = v3PointerPos - m_v3ScrollNullPos;
	VistaVector3D v3IntScrollDir = 0.5f * aBoxSize[1] * 
		mInvTrans.Transform(VistaVector3D( 0.0f, 1.0f, 0.0f, 0.0f ));
	
	float fScroll = 24.0f * v3IntPos.Dot(v3IntScrollDir) / fScrollLength; // 10.0f --> 3.0f
	
	fScroll = fScroll > 1.0f ? 1.0f : fScroll;
	fScroll = fScroll < -1.0f ? -1.0f : fScroll;

	// If scroll value is too small (10%) we set it to 0 to allow a dead zone.
	fScroll = abs(fScroll) <= 0.1f ? 0.0f : fScroll;

	// Handle position update.			
	m_vecCastedHandles[BTN_SCROLL_HANDLE]->SetCenter(v3NullPos + fScroll * fScrollLength * v3ScrollDir);
	
	// Scroll update.
	const float fScrollVel = m_pModel->GetScrollSpeed();
	const float fConstSpeedFac = 1.0f / aTextSize[1];
	float fNewScrollPos = m_pModel->GetScrollPos();

	fNewScrollPos += -1.0f * fScroll 
					 * fConstSpeedFac * fScrollVel * float(dTDelta);
	
	fNewScrollPos = fNewScrollPos < 0.0f ? 0.0f : fNewScrollPos;
	fNewScrollPos =
		fNewScrollPos > fMaxScrollPos ? fMaxScrollPos : fNewScrollPos;	

	m_pModel->SetScrollPos(fNewScrollPos);
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
