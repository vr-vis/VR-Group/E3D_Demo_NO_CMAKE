/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES	                                                                  */
/*============================================================================*/
#include <GL/glew.h>

#include "VfaTextBoxView.h"
#include "VfaTextBoxModel.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>
#include <VistaFlowLib/Visualization/VflVisController.h>
#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLLine.h>

#include <cstring>
#include <cassert>

using namespace std;


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
VfaTextBoxView::VfaTextBoxView( VfaTextBoxModel *pTextInputModel )
:	m_bIsInited( false )
,   m_pModel( pTextInputModel )
,   m_pLabel( NULL )
,   m_bNeedUpdate( true )
,   m_bNeedTextureRedraw( true )
,   m_pTexGen( NULL )
,   m_pOrtho( NULL )
{
	assert( m_pModel );
	m_pModel->AttachObserver( this );

	m_pOrtho = new VflRenderToTexture::C2DViewAdapter;
	m_pOrtho->Init();

	// Texture generator init (dummy values).
	m_pTexGen = new VflRenderToTexture( 1024, 1024 );
	m_pTexGen->Init();
	m_pTexGen->SetTarget( m_pOrtho );
}

VfaTextBoxView::~VfaTextBoxView()
{
	delete m_pOrtho;
	delete m_pTexGen;
	delete m_pLabel;
	m_pModel->DetachObserver( this );
}


// ========================================================================== //
// === IVflRenderable Interface
// ========================================================================== //
bool VfaTextBoxView::Init()
{
	if(!IVflRenderable::Init())
		return false;
	
	// The supplied text input model needs to be valid.
	if(!m_pModel)
		return false;

	GetProperties()->SetTextSize(m_pModel->GetTextSize());
	
	// Label creation and setup.
	if(m_pLabel)
		delete m_pLabel;
	
	m_pLabel = new Vfl3DTextLabel();

	m_pLabel->Init();
	m_pLabel->SetTextFollowViewDir(false);
	m_pLabel->SetVisible(true);
	m_pLabel->SetTextSize(GetProperties()->GetTextSize());
	m_pLabel->SetText("");
	m_pLabel->SetPosition(VistaVector3D(0.0f, 0.0f, 0.0f));
	m_pLabel->Update();
	m_pLabel->GetProperties()->SetUseLighting(false);

	m_bIsInited = true;
	return true;
}

void VfaTextBoxView::Update()
{
	if(!m_bNeedUpdate || !m_bIsInited || !this->GetRenderNode())
		return;

	// Update the text.
	VistaColor color = GetProperties()->GetTextColor();
	float fTextSize = this->GetProperties()->GetTextSize();

	m_pLabel->SetText(ClampTextToBox(m_pModel->GetText()));
	m_pLabel->SetTextSize(fTextSize);
	m_pLabel->SetColor(color);
	m_pLabel->Update();

	m_bNeedUpdate			= false;
	m_bNeedTextureRedraw	= true;

	VistaVector3D v3Min, v3Max;
	m_pLabel->GetBounds(v3Min, v3Max);
	
	m_aLabelSize[0] = v3Max[0]-v3Min[0] + 0.2f * fTextSize;
	m_aLabelSize[1] = v3Max[1]-v3Min[1] + 0.2f * fTextSize;

	const int iWidth	= 1024;
	const int iHeight	= iWidth * int(m_aLabelSize[1] / m_aLabelSize[0]);

	m_pTexGen->Resize(iWidth, iHeight);

	color = VistaColor( VistaColor::BLACK );
	m_pTexGen->SetBackgroundColor(&color[0], 0.0f);
	
	m_pOrtho->SetOrthoParams(0.0f, m_aLabelSize[0], 0.0f, m_aLabelSize[1],
							 -1.0f, 1.0f);
	m_pOrtho->SetRenderNode(this->GetRenderNode());

	m_pLabel->SetRenderNode(this->GetRenderNode());
	// Moving the text label so that __all__ lines of it are visible.
	m_pLabel->SetPosition(VistaVector3D(
		-0.1f * fTextSize,
		(v3Max[1] - v3Min[1]) - GetProperties()->GetTextSize(),
		0.0f));	
	m_pLabel->Update();
}

void VfaTextBoxView::DrawOpaque()
{
	if(!m_bIsInited || !m_pTexGen || !GetProperties()->GetVisible())
		return;
	
	// Save matrix before loading identity.
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	
	glPushAttrib(GL_ENABLE_BIT	// For lighting.
		     | GL_LINE_BIT	// For line width.
		     | GL_CURRENT_BIT	// For color.
		     | GL_DEPTH_BUFFER_BIT // For depth test func.
		     | GL_TEXTURE_BIT ); // For texture env mode.
	glDisable(GL_LIGHTING);


	// TODO_HIGH: Implement the following if branch.
	//			  If the text labels have been updated we need to redraw
	//			  the texture here.
	if(m_bNeedTextureRedraw)
	{
		if(m_pLabel->GetRenderNode())
		{
			m_pOrtho->RegisterTarget(m_pLabel);
			m_pTexGen->Draw2D();
			m_pOrtho->UnregisterTarget(m_pLabel);
			m_bNeedTextureRedraw = false;
		}
	}

	VistaVector3D		v3Center;
	VistaQuaternion		qOri;
	float				fScale;

	m_pModel->GetPosition(v3Center);
	m_pModel->GetOrientation(qOri);
	fScale = m_pModel->GetScale();
	
	glTranslatef(v3Center[0], v3Center[1], v3Center[2]);
	glRotatef(Vista::RadToDeg(acos(qOri[3])*2), qOri[0], qOri[1], qOri[2]);
	glScalef(fScale, fScale, fScale);
	
	float aBoxSize[2];
	m_pModel->GetTextBoxSize(aBoxSize);
	
	// Draw the proxy quad...
	float fScrollOffset = m_pModel->GetScrollPos();

	VistaColor color = GetProperties()->GetBackgroundColor();
	glColor4fv(&color[0]);
	glEnable(GL_TEXTURE_2D);
	m_pTexGen->GetTexture()->Bind();
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f,
					 -1.0f * (aBoxSize[1] - m_aLabelSize[1]) / m_aLabelSize[1]
						- fScrollOffset);
		glVertex3f(-0.5f * aBoxSize[0], -0.5f * aBoxSize[1], 0.0f);

		glTexCoord2f(aBoxSize[0] / m_aLabelSize[0],
					 -1.0f * (aBoxSize[1] - m_aLabelSize[1]) / m_aLabelSize[1]
						- fScrollOffset);
		glVertex3f( 0.5f * aBoxSize[0], -0.5f * aBoxSize[1], 0.0f);

		glTexCoord2f(aBoxSize[0] / m_aLabelSize[0],
					 1.0f - fScrollOffset);
		glVertex3f( 0.5f * aBoxSize[0],  0.5f * aBoxSize[1], 0.0f);

		glTexCoord2f(0.0f,
					 1.0f - fScrollOffset);
		glVertex3f(-0.5f * aBoxSize[0],  0.5f * aBoxSize[1], 0.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);

	// ... and its frame.
	color = GetProperties()->GetFrameColor();
	glColor4fv(&color[0]);
	VistaGLLine::SetLineWidth(GetProperties()->GetFrameWidth());
	VistaGLLine::Enable(VistaGLLine::SHADER_CYLINDER);
	VistaGLLine::Begin(VistaGLLine::VISTA_GL_LINE_STRIP);
		glVertex3f(-0.5f * aBoxSize[0], -0.5f * aBoxSize[1], 0.0f);
		glVertex3f( 0.5f * aBoxSize[0], -0.5f * aBoxSize[1], 0.0f);
		glVertex3f( 0.5f * aBoxSize[0],  0.5f * aBoxSize[1], 0.0f);
		glVertex3f(-0.5f * aBoxSize[0],  0.5f * aBoxSize[1], 0.0f);
		glVertex3f(-0.5f * aBoxSize[0], -0.5f * aBoxSize[1], 0.0f);
	VistaGLLine::End();
	VistaGLLine::Disable();

	// Optional: If needed a slider proxy (quad) is drawn.
	// Use the same color as for the frame.
	if(aBoxSize[1] < m_aLabelSize[1])
	{
		float fTextSize = this->GetProperties()->GetTextSize();

		glBegin(GL_QUADS);
			glVertex3f(0.5f * aBoxSize[0] + 0.5f * fTextSize,
					   -0.5f * aBoxSize[1], 0.0f);
			glVertex3f(0.5f * aBoxSize[0] + 1.0f * fTextSize,
					   -0.5f * aBoxSize[1], 0.0f);
			glVertex3f(0.5f * aBoxSize[0] + 1.0f * fTextSize,
					   0.5f * aBoxSize[1], 0.0f);
			glVertex3f(0.5f * aBoxSize[0] + 0.5f * fTextSize,
					   0.5f * aBoxSize[1], 0.0f);
		glEnd();
	}
	
	// Set text color for cursor and label.
	color = GetProperties()->GetTextColor();
	
	// TODO_LOW: Needs to be done differently since the bounding box doesn't
	//			 tell anything about the text orientation and hence the
	//			 cursor pos calculation is totally nuts.
	// Now, the cursor needs to be drawn, but only if requested.
	if(GetProperties()->GetIsCursorOn())
	{
		VistaVector3D v3MinBounds, v3MaxBounds;
		m_pLabel->GetBounds(v3MinBounds, v3MaxBounds);


		glColor4fv(&color[0]);
		VistaGLLine::SetLineWidth(GetProperties()->GetCursorWidth());
		VistaGLLine::Enable(VistaGLLine::SHADER_CYLINDER);
		VistaGLLine::Begin();

		glVertex3f(v3MaxBounds[0]+0.005f, v3MinBounds[1]-0.005f, 0.0f);
		glVertex3f(v3MaxBounds[0]+0.005f, v3MaxBounds[1]+0.005f, 0.0f);

		VistaGLLine::End();
		VistaGLLine::Disable();
	}

	// Restore the previous OGL state.
	glPopAttrib();

	glPopMatrix();
}

unsigned int VfaTextBoxView::GetRegistrationMode() const
{
	return IVflRenderable::OLI_UPDATE | IVflRenderable::OLI_DRAW_OPAQUE;
}

IVflRenderable::VflRenderableProperties*
VfaTextBoxView::CreateProperties() const
{
	return new VfaTextInputVisProperties;
}


// ========================================================================== //
// === IVistaObserver Interface
// ========================================================================== //
void VfaTextBoxView::ObserverUpdate(IVistaObserveable *pObs, int msg,
									  int ticket)
{
	VistaColor color;

	switch(msg)
	{
	case VfaTextBoxModel::MSG_TEXT_CHANGED:
	case VfaTextBoxModel::MSG_TEXT_BOX_SIZE_CHANGED:
		m_bNeedUpdate = true;
		break;
	case VfaTextBoxModel::MSG_TEXT_SIZE_CHANGED:
		if(m_pLabel)
		{
			m_pLabel->SetTextSize(m_pModel->GetTextSize());
			m_pLabel->Update();
		}
		break;
	default:
		break;
	};
}


// ========================================================================== //
// === Public Interface
// ========================================================================== //
VfaTextBoxView::VfaTextInputVisProperties*
	VfaTextBoxView::GetProperties() const
{
	return static_cast<VfaTextBoxView::VfaTextInputVisProperties*>(
		IVflRenderable::GetProperties());
}


bool VfaTextBoxView::GetTextDims(float aTextDims[]) const
{
	memcpy(aTextDims, m_aLabelSize, 2*sizeof(float));

	return true;
}


// ========================================================================== //
// === Helper Functions
// ========================================================================== //
std::string VfaTextBoxView::ClampTextToBox(const std::string &strText)
{
	if(!this->GetRenderNode() || strText == "")
		return "";

	if(!m_pModel->GetIsMultilined())
		return strText;
	
	// Variables.
	string strClamped("");
	float aBoxSize[2];
	VistaVector3D v3Min, v3Max;
	
	// Box size init.
	m_pModel->GetTextBoxSize(aBoxSize);
	
	// Text label init.
	float fSizeOrig;
	
	fSizeOrig = m_pLabel->GetTextSize();
	
	string::const_iterator itTransfer = strText.begin();
	int iLastSpace = -1;

	while(itTransfer != strText.end())
	{
		// Transfer a complete word.
		while(itTransfer != strText.end() && *itTransfer != ' ')
		{
			strClamped.push_back(*itTransfer);
			++itTransfer;
		}

		// Put the current text into the tester label.
		m_pLabel->SetText(strClamped);
		m_pLabel->GetBounds(v3Min, v3Max);
		
		// Check if we exceeded the x-dim of the text box.
		float fWidth = v3Max[0]-v3Min[0]
						+ 0.2f * this->GetProperties()->GetTextSize();
		
		// TODO_MID: This loop seems to be the evil!
		if(fWidth > aBoxSize[0])
		{
			if(iLastSpace > 0 && iLastSpace < (int)strClamped.size())
			{
				strClamped.insert(iLastSpace, "\n");
				//strClamped.erase(iLastSpace+1, 1);
			}
		}

		if(itTransfer == strText.end())
		{
			break;
		}
		else if(*itTransfer == ' ')
		{
			strClamped.push_back(' ');
			iLastSpace = (int)strClamped.size()-1;
			++itTransfer;
		}
	}

	return strClamped;
}


// ========================================================================== //
// === Helper Class - CVfaTextInputVisProperties
// ========================================================================== //
VfaTextBoxView::VfaTextInputVisProperties::VfaTextInputVisProperties()
:	m_fFrameWidth( 0.004f )
,   m_fCursorWidth( 0.005f )
,   m_bIsCursorOn( false )
{
	// @todo Text size init missing.

	m_colorBack = VistaColor( VistaColor::VISTA_BLUE );
	m_colorFrame = VistaColor( VistaColor::LIGHT_GRAY );
	m_colorText = VistaColor( VistaColor::WHITE );
}

VfaTextBoxView::VfaTextInputVisProperties::~VfaTextInputVisProperties()
{ }


bool VfaTextBoxView::VfaTextInputVisProperties::SetBackgroundColor(
	const VistaColor& color )
{
	m_colorBack = color;
	m_colorBack.ClampValues();
	Notify( MSG_BG_COLOR_CHANGED );
	return true;
}

VistaColor VfaTextBoxView::VfaTextInputVisProperties::GetBackgroundColor() const
{
	return m_colorBack;
}


bool VfaTextBoxView::VfaTextInputVisProperties::SetFrameColor(
	const VistaColor& color )
{
	m_colorFrame = color;
	m_colorFrame.ClampValues();
	Notify( MSG_FRAME_COLOR_CHANGED );
	return true;
}

VistaColor VfaTextBoxView::VfaTextInputVisProperties::GetFrameColor() const
{
	return m_colorFrame;
}


bool VfaTextBoxView::VfaTextInputVisProperties::SetTextColor(
	const VistaColor& color )
{
	m_colorText = color;
	m_colorText.ClampValues();
	Notify( MSG_TEXT_COLOR_CHANGED );
	return true;
}

VistaColor VfaTextBoxView::VfaTextInputVisProperties::GetTextColor() const
{
	return m_colorText;
}


bool VfaTextBoxView::VfaTextInputVisProperties::SetFrameWidth( float fWidth )
{
	m_fFrameWidth = fWidth;
	Notify( MSG_FRAME_WIDTH_CHANGED );
	return true;
}

float VfaTextBoxView::VfaTextInputVisProperties::GetFrameWidth() const
{
	return m_fFrameWidth;
}


bool VfaTextBoxView::VfaTextInputVisProperties::SetTextSize(float fSize)
{
	m_fTextSize = fSize;
	Notify( MSG_TEXT_SIZE_CHANGED );
	return true;
}

float VfaTextBoxView::VfaTextInputVisProperties::GetTextSize() const
{
	return m_fTextSize;
}


bool VfaTextBoxView::VfaTextInputVisProperties::SetCursorWidth(float fWidth)
{
	m_fCursorWidth = fWidth;
	Notify( MSG_CURSOR_WIDTH_CHANGED );
	return true;
}

float VfaTextBoxView::VfaTextInputVisProperties::GetCursorWidth() const
{
	return m_fCursorWidth;
}

bool VfaTextBoxView::VfaTextInputVisProperties::SetIsCursorOn(
	bool bIsCursorOn )
{
	m_bIsCursorOn = bIsCursorOn;
	Notify( MSG_CURSOR_STATE_CHANGED );
	return true;
}

bool VfaTextBoxView::VfaTextInputVisProperties::GetIsCursorOn() const
{
	return m_bIsCursorOn;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
