/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VfaTextSourceMediator.h"
#include "VfaTextSource.h"
#include "VfaTextBoxModel.h"

#include <VistaKernel/InteractionManager/VistaKeyboardSystemControl.h>

#include <string>
#include <cassert>

using namespace std;

/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
VfaKeyboardToTextBox::VfaKeyboardToTextBox( VfaTextViaKeyboard* pSource,
	VfaTextBoxModel* pTarget )
:	m_pSource( pSource )
,	m_pTarget( pTarget )
{
	assert( m_pSource && m_pTarget );
	m_pSource->AttachObserver( this );
}

VfaKeyboardToTextBox::~VfaKeyboardToTextBox()
{
	m_pSource->DetachObserver( this );
}


void VfaKeyboardToTextBox::ObserverUpdate( IVistaObserveable* pObserveable,
	int nMsg, int nTicket )
{
	if( nMsg == VfaTextViaKeyboard::MSG_NON_CHAR_KEY_RECEIVED )
	{
		const int iKeyId = m_pSource->GetLastNonCharKeyId();
	
		switch( iKeyId )
		{
		case VISTA_KEY_LEFTARROW:
			{
				const size_t nCursorPos = m_pTarget->GetCursorPosition();
				m_pTarget->SetCursorPositon( nCursorPos - 1 );
			}
			break;
		case VISTA_KEY_RIGHTARROW:
			{
				const size_t nCursorPos = m_pTarget->GetCursorPosition();
				m_pTarget->SetCursorPositon( nCursorPos + 1 );
			}
			break;
		case VISTA_KEY_HOME:
			{
				m_pTarget->SetCursorPositon( 0 );
			}
			break;
		case VISTA_KEY_END:
			{
				const string sText = m_pTarget->GetText();
				m_pTarget->SetCursorPositon( sText.size() );
			}
			break;
		case VISTA_KEY_BACKSPACE:
			{
				const size_t nCursorPos = m_pTarget->GetCursorPosition();
				if( nCursorPos > 0 )
				{
					string sText = m_pTarget->GetText();
					sText.erase( nCursorPos - 1, 1 );
					
					m_pTarget->SetCursorPositon( nCursorPos - 1 );
					m_pTarget->SetText( sText );				
				}
			}
			break;
		case VISTA_KEY_DELETE:
			{
				string sText = m_pTarget->GetText();
				const size_t nCursorPos = m_pTarget->GetCursorPosition();
				if( nCursorPos < sText.size() )
				{
					sText.erase( nCursorPos, 1 );
					m_pTarget->SetText( sText );				
				}
			}
			break;
		case VISTA_KEY_ENTER:
			{
				m_pTarget->Notify( VfaTextBoxModel::MSG_ACCEPT_BUTTON_PRESSED );
			}
			break;
		case VISTA_KEY_ESC:
			{
				m_pTarget->Notify( VfaTextBoxModel::MSG_CANCEL_BUTTON_PRESSED );
			}
			break;
		default:
			break;
		}
	}
	else if( nMsg == VfaTextViaKeyboard::MSG_TEXT_RECEIVED )
	{
		const string sNewText = m_pSource->GetLastText();
		const size_t nCursorPos = m_pTarget->GetCursorPosition();

		string sCurrText = m_pTarget->GetText();
		sCurrText.insert( nCursorPos, sNewText );

		m_pTarget->SetText( sCurrText );
		m_pTarget->SetCursorPositon( nCursorPos + 1 );
	}
}

////////////////////////////////////////////////////////////////////////////////

VfaSpeechToTextBox::VfaSpeechToTextBox( VfaTextViaSpeech* pSource,
	VfaTextBoxModel* pTarget )
:	m_pSource( pSource )
,	m_pTarget( pTarget )
,	m_bUseSpeechCommands( false )
{
	assert( m_pSource && m_pTarget );
	m_pSource->AttachObserver( this );
}

VfaSpeechToTextBox::~VfaSpeechToTextBox()
{
	m_pSource->DetachObserver( this );
}


void VfaSpeechToTextBox::SetUseSpeechCommands( bool bUse )
{
	m_bUseSpeechCommands = bUse;
}

bool VfaSpeechToTextBox::GetUseSpeechCommands() const
{
	return m_bUseSpeechCommands;
}


void VfaSpeechToTextBox::SetAcceptCommand( const std::string& sCommandString )
{
	m_sAcceptCommand = sCommandString;
}

std::string VfaSpeechToTextBox::GetAcceptCommand() const
{
	return m_sAcceptCommand;
}


void VfaSpeechToTextBox::SetAbortCommand( const std::string& sCommandString )
{
	m_sAbortCommand = sCommandString;
}

std::string VfaSpeechToTextBox::GetAbortCommand() const
{
	return m_sAbortCommand;
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

void VfaSpeechToTextBox::ObserverUpdate( IVistaObserveable* pObserveable,
	int nMsg, int nTicket )
{
	if( nMsg == VfaTextViaSpeech::MSG_TEXT_RECEIVED )
	{
		const string sNewText = m_pSource->GetLastText();

		if( m_bUseSpeechCommands )
		{
			if( !m_sAcceptCommand.empty() && sNewText == m_sAcceptCommand )
			{
				m_pTarget->Notify( VfaTextBoxModel::MSG_ACCEPT_BUTTON_PRESSED );
				return;
			}
			else if( !m_sAbortCommand.empty() && sNewText == m_sAbortCommand )
			{
				m_pTarget->Notify( VfaTextBoxModel::MSG_CANCEL_BUTTON_PRESSED );
				return;
			}
		}
				
		m_pTarget->SetText( sNewText );
		m_pTarget->SetCursorPositon( sNewText.size() );
	}
}
