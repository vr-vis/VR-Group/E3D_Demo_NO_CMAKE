/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFATEXTSOURCE_H
#define VFATEXTSOURCE_H

/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"

#include <VistaAspects/VistaObserveable.h>
#include <VistaKernel/EventManager/VistaEventHandler.h>
#include <VistaInterProcComm/Concurrency/VistaMutex.h>

#include <string>


/*============================================================================*/
/* CLASS DEFINITIONS														  */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI IVfaTextSource : public IVistaObserveable
{
public:
	enum
	{
		MSG_ENABLED_STATE_CHANGED = IVistaObserveable::MSG_LAST,
		MSG_TEXT_RECEIVED,
		MSG_LAST
	};

	virtual ~IVfaTextSource();

	virtual void SetIsEnabled( bool bIsEnabled ) = 0;
	virtual bool GetIsEnabled() const = 0;

	virtual std::string GetLastText() = 0;

protected:
	IVfaTextSource();
};


////////////////////////////////////////////////////////////////////////////////

class VISTAFLOWLIBAUXAPI VfaTextViaKeyboard : public IVfaTextSource
{
friend class DirectKeyInput;

public:
	enum
	{
		MSG_NON_CHAR_KEY_RECEIVED = IVfaTextSource::MSG_LAST,
		MSG_LAST
	};

	VfaTextViaKeyboard();
	virtual ~VfaTextViaKeyboard();
	
	// *** IVfaTextSource interface. ***
	virtual void SetIsEnabled( bool bIsEnabled );
	virtual bool GetIsEnabled() const;
	
	virtual std::string GetLastText();
	
	int GetLastNonCharKeyId() const;	

protected:
	// Used by the DirectKeyInput below.
	void ReceiveKey( int iKeyId );

private:
	class DirectKeyInput;
	DirectKeyInput*		m_pKeyboardInput;

	bool				m_bIsEnabled;
	std::string			m_sLastText;
	int					m_iLastNonCharKey;
};

////////////////////////////////////////////////////////////////////////////////

class VISTAFLOWLIBAUXAPI VfaTextViaSpeech : public IVfaTextSource,
											public VistaEventHandler
{
friend class SpeechServer;

public:
	VfaTextViaSpeech( const std::string& sHostname = "127.0.0.1",
		const unsigned short uiPort = 55555 );
	virtual ~VfaTextViaSpeech();

	// *** IVfaTextSource interface. ***
	//@TODO GetIsEnabled/GetIsEnabled is declared in IVfaTextSource and VistaEventHandler
	virtual void SetIsEnabled( bool bIsEnabled );
	virtual bool GetIsEnabled() const;

	virtual std::string GetLastText();

	// *** VistaEventHandler interface. ***
	virtual void HandleEvent( VistaEvent *pEvent );

protected:
	// Used by the SpeechServer below.
	void ReceiveSpeech( const std::string& sSpeech );

private:
	class SpeechServer;
	SpeechServer*		m_pSpeechServer;

	bool				m_bIsEnabled;
	std::string			m_sLastText;

	VistaMutex			m_oTextMutex;
	bool				m_bLastTextAnnounced;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

