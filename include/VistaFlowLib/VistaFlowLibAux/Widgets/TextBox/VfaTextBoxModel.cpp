/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES	                                                                  */
/*============================================================================*/
#include "VfaTextBoxModel.h"
#include <VistaBase/VistaVectorMath.h>
#include <cstring>

using namespace std;


/*============================================================================*/
/* CONSTANTS	                                                              */
/*============================================================================*/
const float VfaTextBoxModel::MIN_LINE_HEIGHT_FAC = 1.8f;


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
VfaTextBoxModel::VfaTextBoxModel( bool bIsSingleLine /* = true */ )
:	m_bIsSingleLine( bIsSingleLine )
,   m_fTextSize( 0.04f )
,   m_fScrollVelocity( 0.5f )
,   m_fScrollPos( 0.0f )
,   m_bIsMultilined( false )
,   m_bOrientTowardsUser( false )
,	m_nCursorPos( 0 )
{
	m_aMinSize[0]	= m_fTextSize * 25.0f;
	m_aMinSize[1]	= m_fTextSize * MIN_LINE_HEIGHT_FAC;

	m_aSize[0]		= m_aMinSize[0];
	m_aSize[1]		= m_aMinSize[1];		
}

VfaTextBoxModel::~VfaTextBoxModel()
{ }

bool VfaTextBoxModel::SetText(const std::string &strText)
{
	if( strText == m_sText )
		return false;

	m_sText = strText;
	Notify( MSG_TEXT_CHANGED );
	
	if( m_nCursorPos > m_sText.size() )
	{
		SetCursorPositon( m_sText.size() );
	}

	return true;
}

std::string VfaTextBoxModel::GetText() const
{
	return m_sText;
}


bool VfaTextBoxModel::AppendString( const std::string &strText )
{
	if( strText.empty() )
		return false;

	SetText( m_sText.append( strText ) );
	return true;
}


bool VfaTextBoxModel::ResetText()
{
	return SetText( "" );
}


bool VfaTextBoxModel::SetTextBoxMinSize( float aMinSize[2] )
{
	float aRealSize[2];
	aRealSize[0] = aMinSize[0] < 0.0f ? 0.0f : aMinSize[0];
	aRealSize[1] = aMinSize[1] < 0.0f ? 0.0f : aMinSize[1];

	if( 0 != memcmp( aRealSize, m_aMinSize, 2 * sizeof( aRealSize[0] ) ) )
	{
		memcpy( m_aMinSize, aRealSize, 2 * sizeof( aRealSize[0] ) );
		Notify( MSG_TEXT_BOX_MIN_SIZE_CHANGED );
	}

	return true;
}

bool VfaTextBoxModel::GetTextBoxMinSize( float aMinSize[2] ) const
{
	memcpy( aMinSize, m_aMinSize, 2 * sizeof( aMinSize[0] ) );
	return true;	
}


bool VfaTextBoxModel::SetTextBoxSize( float aSize[2] )
{
	if( !GetIsMultilined() )
		aSize[1] = MIN_LINE_HEIGHT_FAC * GetTextSize();

	float aRealSize[2];
	aRealSize[0] = aSize[0] < m_aMinSize[0] ? m_aMinSize[0] : aSize[0];
	aRealSize[1] = aSize[1] < m_aMinSize[1] ? m_aMinSize[1] : aSize[1];

	if( 0 != memcmp( aRealSize, m_aSize, 2 * sizeof( aRealSize[0] ) ) )
	{
		memcpy( m_aSize, aRealSize, 2 * sizeof( m_aSize[0] ) );
		Notify( MSG_TEXT_BOX_SIZE_CHANGED );
	}

	return true;
}

bool VfaTextBoxModel::GetTextBoxSize( float aSize[2] ) const
{
	memcpy( aSize, m_aSize, 2 * sizeof( aSize[0] ) );
	return true;
}


bool VfaTextBoxModel::SetTextSize( float fSize )
{
	if(fSize == m_fTextSize)
		return false;

	m_fTextSize = fSize;
	Notify(MSG_TEXT_SIZE_CHANGED);

	return true;
}

float VfaTextBoxModel::GetTextSize() const
{
	return m_fTextSize;
}


bool VfaTextBoxModel::SetScrollPos(float fScrollPos)
{
	if(fScrollPos == m_fScrollPos)
		return false;

	m_fScrollPos = fScrollPos;
	Notify(MSG_SCROLL_POSITION_CHANGED);

	return true;
}

float VfaTextBoxModel::GetScrollPos() const
{
	return m_fScrollPos;
}


bool VfaTextBoxModel::SetScrollSpeed( float fVelocity )
{
	if( fVelocity == m_fScrollVelocity )
		return false;

	m_fScrollVelocity = fVelocity;
	Notify(MSG_SCROLL_SPEED_CHANGED);

	return true;
}

float VfaTextBoxModel::GetScrollSpeed() const
{
	return m_fScrollVelocity;
}


bool VfaTextBoxModel::SetIsMultilined( bool bIsMultilined )
{
	if( bIsMultilined == m_bIsMultilined )
		return false;

	m_bIsMultilined = bIsMultilined;
	Notify( MSG_MULTILINED_PARAM_CHANGED );

	if( !m_bIsMultilined )
	{
		float aBoxSize[2];

		this->GetTextBoxSize(aBoxSize);
		aBoxSize[1] = MIN_LINE_HEIGHT_FAC * this->GetTextSize();
		this->SetTextBoxSize(aBoxSize);

		this->GetTextBoxMinSize(aBoxSize);
		aBoxSize[1] = MIN_LINE_HEIGHT_FAC * this->GetTextSize();
		this->SetTextBoxMinSize(aBoxSize);
	}
	
	return true;
}

bool VfaTextBoxModel::GetIsMultilined() const
{
	return m_bIsMultilined;
}


bool VfaTextBoxModel::SetOrientTowardsUser(bool bEnable)
{
	if( bEnable == m_bOrientTowardsUser )
		return false;

	m_bOrientTowardsUser = bEnable;
	Notify( MSG_ORIENT_TOWARDS_USER_FLAG_CHANGED );

	return true;
}

bool VfaTextBoxModel::GetOrientTowardsUser() const
{
	return m_bOrientTowardsUser;
}


bool VfaTextBoxModel::SetCursorPositon( size_t nCursorPos )
{
	// '==' is permitted, as it indicates 'after the last character'.
	if( nCursorPos > m_sText.size() )
		return false;

	if( nCursorPos != m_nCursorPos )
	{
		m_nCursorPos = nCursorPos;
		Notify( MSG_CURSOR_POS_CHANGED );
	}
	return true;
}

size_t VfaTextBoxModel::GetCursorPosition() const
{
	return m_nCursorPos;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
