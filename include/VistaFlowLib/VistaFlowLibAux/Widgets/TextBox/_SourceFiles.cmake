

set( RelativeDir "./Widgets/TextBox" )
set( RelativeSourceGroup "Source Files\\Widgets\\TextBox" )

set( DirFiles
	VfaTextBox.cpp
	VfaTextBox.h
	VfaTextBoxController.cpp
	VfaTextBoxController.h
	VfaTextBoxModel.cpp
	VfaTextBoxModel.h
	VfaTextBoxView.cpp
	VfaTextBoxView.h
	VfaTextSource.cpp
	VfaTextSource.h
	VfaTextSourceMediator.cpp
	VfaTextSourceMediator.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

