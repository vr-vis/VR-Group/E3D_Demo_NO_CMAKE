/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES	                                                                  */
/*============================================================================*/
#include "VfaTextBox.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <cassert>


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
VfaTextBox::VfaTextBox( VflRenderNode *pRenderNode)
:	IVfaWidget(pRenderNode)
,   m_pModel( NULL )
, 	m_pView( NULL )
,	m_pController( NULL )   
{
	assert( GetRenderNode() );	
	
	m_pModel = new VfaTextBoxModel();

	m_pView = new VfaTextBoxView( m_pModel );
	m_pView->Init();
	GetRenderNode()->AddRenderable( m_pView );

	m_pController = new VfaTextBoxController( m_pModel, m_pView,
		GetRenderNode() );
}

VfaTextBox::~VfaTextBox()
{
	if( m_pView )
		GetRenderNode()->RemoveRenderable( m_pView );
}


void VfaTextBox::SetIsEnabled( bool bIsEnabled )
{
	m_pController->SetIsEnabled( bIsEnabled );
	if( bIsEnabled )
		m_pView->SetVisible( bIsEnabled );	
}

bool VfaTextBox::GetIsEnabled() const
{
	return m_pController->GetIsEnabled();
}


void VfaTextBox::SetIsVisible( bool bIsVisible )
{
	m_pView->SetVisible( bIsVisible );
	if( !bIsVisible )
		m_pController->SetIsEnabled( false );
}

bool VfaTextBox::GetIsVisible() const
{
	return m_pView->GetVisible();
}


VfaTextBoxModel* VfaTextBox::GetModel() const
{
	return m_pModel;
}

VfaTextBoxView* VfaTextBox::GetView() const
{
	return m_pView;
}

VfaTextBoxController* VfaTextBox::GetController() const
{
	return m_pController;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
