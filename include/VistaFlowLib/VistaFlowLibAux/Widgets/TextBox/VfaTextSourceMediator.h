/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFATEXTSOURCEMEDIATOR_H
#define VFATEXTSOURCEMEDIATOR_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/
class VfaTextBoxModel;
class VfaTextViaKeyboard;
class VfaTextViaSpeech;


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include <VistaFlowLib/Data/VflObserver.h>
#include <string>


/*============================================================================*/
/* CLASS DEFINITIONS														  */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaKeyboardToTextBox : public VflObserver
{
public:
	VfaKeyboardToTextBox( VfaTextViaKeyboard* pSource,
		VfaTextBoxModel* pTarget );
	virtual ~VfaKeyboardToTextBox();

	// *** IVistaObserver interface. ***
	virtual void ObserverUpdate( IVistaObserveable* pObserveable, int nMsg,
		int nTicket );

private:
	VfaTextViaKeyboard*		m_pSource;
	VfaTextBoxModel*		m_pTarget;
};

////////////////////////////////////////////////////////////////////////////////

class VISTAFLOWLIBAUXAPI VfaSpeechToTextBox : public VflObserver
{
public:
	VfaSpeechToTextBox( VfaTextViaSpeech* pSource,
		VfaTextBoxModel* pTarget );
	virtual ~VfaSpeechToTextBox();

	void SetUseSpeechCommands( bool bUse );
	bool GetUseSpeechCommands() const;

	void SetAcceptCommand( const std::string& sCommandString );
	std::string GetAcceptCommand() const;

	void SetAbortCommand( const std::string& sCommandString );
	std::string GetAbortCommand() const;

	// *** IVistaObserver interface. ***
	virtual void ObserverUpdate( IVistaObserveable* pObserveable, int nMsg,
		int nTicket );

private:
	VfaTextViaSpeech*	m_pSource;
	VfaTextBoxModel*	m_pTarget;

	bool				m_bUseSpeechCommands;
	std::string			m_sAcceptCommand;
	std::string			m_sAbortCommand;
};


#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

