/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFATEXTINPUTVIS_H
#define VFATEXTINPUTVIS_H

/*============================================================================*/
/* INCLUDES	                                                                  */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"

#include <VistaBase/VistaColor.h>

#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLib/Visualization/VflRenderToTexture.h>

#include <string>


/*============================================================================*/
/* FORWARD DECLARATION                                                        */
/*============================================================================*/
class VfaTextBoxModel;
class Vfl3DTextLabel;


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaTextBoxView : public IVflRenderable
{
public:
	VfaTextBoxView( VfaTextBoxModel *pTextInputModel );
	virtual ~VfaTextBoxView();


	// *** IVflRenderable interface. ***
	virtual bool Init();
	virtual void Update();
	virtual void DrawOpaque();
	
	virtual unsigned int GetRegistrationMode() const;


	// *** IVistaObserver interface. ***
	virtual void ObserverUpdate( IVistaObserveable *pObs, int msg, int ticket );

	
	class VISTAFLOWLIBAUXAPI VfaTextInputVisProperties : public VflRenderableProperties
	{
	public:
		VfaTextInputVisProperties();
		virtual ~VfaTextInputVisProperties();

		enum
		{
			MSG_BG_COLOR_CHANGED = VflRenderableProperties::MSG_LAST,
			MSG_FRAME_COLOR_CHANGED,
			MSG_TEXT_COLOR_CHANGED,
			MSG_FRAME_WIDTH_CHANGED,
			MSG_TEXT_SIZE_CHANGED,
			MSG_CURSOR_WIDTH_CHANGED,
			MSG_CURSOR_STATE_CHANGED,
			MSG_LAST
		};

		bool SetBackgroundColor( const VistaColor& color );
		VistaColor GetBackgroundColor() const;

		bool SetFrameColor( const VistaColor& color );
		VistaColor GetFrameColor() const;

		bool SetTextColor( const VistaColor& color );
		VistaColor GetTextColor() const;

		bool SetFrameWidth( float fWidth );
		float GetFrameWidth() const;

		bool SetTextSize( float fSize );
		float GetTextSize() const;

		bool SetCursorWidth( float fWidth );
		float GetCursorWidth() const;

		bool SetIsCursorOn( bool bIsCursorOn );
		bool GetIsCursorOn() const;

	private:
		VistaColor		m_colorBack;
		VistaColor		m_colorFrame;
		VistaColor		m_colorText;
		
		float			m_fFrameWidth;
		float			m_fTextSize;

		float			m_fCursorWidth;
		bool			m_bIsCursorOn;
	};


	virtual VfaTextInputVisProperties* GetProperties() const;

	bool GetTextDims(float aTextDims[2]) const;


protected:
	// *** IVfaRenderable interface. ***
	virtual VflRenderableProperties* CreateProperties() const;


private:
	//! Ensure that the displayed text stays within the text field.
	std::string ClampTextToBox(const std::string &strText);

		
	bool								m_bIsInited;
	VfaTextBoxModel*					m_pModel;
	Vfl3DTextLabel*						m_pLabel;
	bool								m_bNeedUpdate;
	bool								m_bNeedTextureRedraw;
	
	//! For baking the labels into textures.
	VflRenderToTexture*					m_pTexGen;
	//! View adapter to set up an orthographic projection.
	VflRenderToTexture::C2DViewAdapter*	m_pOrtho;
	
	float								m_aLabelSize[2];
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
