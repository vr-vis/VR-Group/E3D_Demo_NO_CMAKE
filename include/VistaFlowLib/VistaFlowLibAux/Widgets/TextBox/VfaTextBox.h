/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFATEXTBOX_H
#define VFATEXTBOX_H

/*============================================================================*/
/* INCLUDES	                                                                  */
/*============================================================================*/
#include "../VfaWidget.h"

// Required for covariance to work.
#include "VfaTextBoxModel.h"
#include "VfaTextBoxView.h"
#include "VfaTextBoxController.h"

#include <string>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaTextBox : public IVfaWidget
{
public:
	/**
	 * Texture path's trailing '/' is optional.
	 */
	VfaTextBox( VflRenderNode *pRenderNode);
	virtual ~VfaTextBox();

	// *** IVfaWidget interface. ***
	virtual void SetIsEnabled(bool bIsEnabled);
	virtual bool GetIsEnabled() const;

	virtual void SetIsVisible(bool bIsVisible);
	virtual bool GetIsVisible() const;

	// Using co-variance to change return type.
	virtual VfaTextBoxModel* GetModel() const;
	virtual VfaTextBoxView* GetView() const;
	virtual VfaTextBoxController* GetController() const;
	
private:
	VfaTextBoxModel* m_pModel;
	VfaTextBoxView* m_pView;
	VfaTextBoxController* m_pController;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
