/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFATEXTBOXMODEL_H
#define VFATEXTBOXMODEL_H

/*============================================================================*/
/* INCLUDES	                                                                  */
/*============================================================================*/
#include "../VfaTransformableWidgetModel.h"
#include <VistaBase/VistaColor.h>
#include <string>


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaTextBoxModel : public VfaTransformableWidgetModel
{
public:
	static const float MIN_LINE_HEIGHT_FAC;

	enum eMessages
	{
		MSG_ACTIVATED = VfaTransformableWidgetModel::MSG_LAST,
		MSG_DEACTIVATED,		
		MSG_TEXT_CHANGED,
		MSG_FLUSH_TEXT,
		MSG_TEXT_BOX_MIN_SIZE_CHANGED,
		MSG_TEXT_BOX_SIZE_CHANGED,
		MSG_TEXT_SIZE_CHANGED,
		MSG_SCROLL_SPEED_CHANGED,
		MSG_SCROLL_POSITION_CHANGED,
		MSG_MULTILINED_PARAM_CHANGED,
		MSG_ORIENT_TOWARDS_USER_FLAG_CHANGED,
		MSG_ACCEPT_BUTTON_PRESSED,
		MSG_CANCEL_BUTTON_PRESSED,
		MSG_CURSOR_POS_CHANGED,
		MSG_LAST
	};

	enum eTickets
	{
		// Identificator we pass to the model so it can auth itself when sending
		// us a message.
		TICKET_TEXT_INPUT_MODEL = VfaWidgetModelBase::TICKET_LAST,
		TICKET_LAST
	};

	explicit VfaTextBoxModel(bool bIsSingleLine = true );
	virtual ~VfaTextBoxModel();

	bool SetText( const std::string &strText );
	std::string GetText() const;

	bool AppendString( const std::string &strLine );
	
	bool ResetText();

	bool SetTextBoxMinSize( float aMinSize[2] );
	bool GetTextBoxMinSize( float aMinSize[2] ) const;

	bool SetTextBoxSize( float aSize[2] );
	bool GetTextBoxSize( float aSize[2] ) const;

	bool SetTextSize( float fSize );
	float GetTextSize() const;

	bool SetScrollPos( float fScrollPos );
	float GetScrollPos() const;

	bool SetScrollSpeed( float fVelocity );
	float GetScrollSpeed() const;

	bool SetIsMultilined( bool bIsMultilined );
	bool GetIsMultilined() const;

	bool SetOrientTowardsUser( bool bEnable );
	bool GetOrientTowardsUser() const;

	bool SetCursorPositon( size_t nCursorPos );
	size_t GetCursorPosition() const;
	

private:
	std::string				m_sText;
	
	float					m_aMinSize[2];
	float					m_aSize[2];
	
	bool					m_bIsSingleLine;
	float					m_fTextSize;
	
	float					m_fScrollVelocity;
	float					m_fScrollPos;
	
	bool					m_bIsMultilined;
	bool					m_bOrientTowardsUser;

	size_t					m_nCursorPos;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
