/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VfaTextSource.h"

#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/InteractionManager/VistaKeyboardSystemControl.h>
#include <VistaKernel/EventManager/VistaEventManager.h>
#include <VistaKernel/EventManager/VistaSystemEvent.h>
#include <VistaInterProcComm/Concurrency/VistaThread.h>
#include <VistaInterProcComm/IPNet/VistaTCPServer.h>
#include <VistaInterProcComm/IPNet/VistaTCPSocket.h>
#include <VistaInterProcComm/IPNet/VistaSocketAddress.h>
#include <VistaInterProcComm/IPNet/VistaIPAddress.h>

#include <cassert>

using namespace std;


/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
IVfaTextSource::IVfaTextSource()
{ }

IVfaTextSource::~IVfaTextSource()
{ }

////////////////////////////////////////////////////////////////////////////////

class VfaTextViaKeyboard::DirectKeyInput
:	public VistaKeyboardSystemControl::IVistaDirectKeySink
{
public:
	DirectKeyInput() { }
	virtual ~DirectKeyInput() { }

	void SetTarget( VfaTextViaKeyboard* pTarget )
	{
		m_pTarget = pTarget;
	}

	virtual bool HandleKeyPress( int nKey, int nMod, bool bIsKeyRepeat = false ) 
	{
		if( m_pTarget )
			m_pTarget->ReceiveKey( nKey );
		return true;
	}

private:
	VfaTextViaKeyboard* m_pTarget;
};


VfaTextViaKeyboard::VfaTextViaKeyboard()
:	m_pKeyboardInput( new DirectKeyInput() )
,	m_bIsEnabled( false )
,	m_sLastText( "" ) // For completness' sake.
,	m_iLastNonCharKey( -1 )
{
	assert( m_pKeyboardInput );
	m_pKeyboardInput->SetTarget( this );
}

VfaTextViaKeyboard::~VfaTextViaKeyboard()
{
	if( m_bIsEnabled )
		SetIsEnabled( false );
	delete m_pKeyboardInput;
}


void VfaTextViaKeyboard::SetIsEnabled( const bool bIsEnabled )
{
	if( bIsEnabled != m_bIsEnabled )
	{
		m_bIsEnabled = bIsEnabled;

		if( m_bIsEnabled )
		{
			GetVistaSystem()->GetKeyboardSystemControl()->SetDirectKeySink(
				m_pKeyboardInput );
		}
		else
		{
			// @todo Is it really good to NULL this? Maybe the state that was
			//		 encountered when activating the source should be restored.
			//		 Mind, however, that a non-NULL ptr on activation would be
			//		 unclean state anyways. Maybe activation should fail if the
			//		 current direct key sink is non-NULL.
			GetVistaSystem()->GetKeyboardSystemControl()->SetDirectKeySink(
				NULL );
		}

		Notify( MSG_ENABLED_STATE_CHANGED );
	}
}

bool VfaTextViaKeyboard::GetIsEnabled() const
{
	return m_bIsEnabled;
}


std::string VfaTextViaKeyboard::GetLastText()
{
	return m_sLastText;
}


int VfaTextViaKeyboard::GetLastNonCharKeyId() const
{
	return m_iLastNonCharKey;
}


void VfaTextViaKeyboard::ReceiveKey( int iKeyId )
{
	// Valid char range (from ' ' to '~').
	if( iKeyId >= 32 && iKeyId <= 126 )
	{
		m_sLastText = static_cast<char>( iKeyId );
		Notify( MSG_TEXT_RECEIVED );
	}
	else
	{
		m_iLastNonCharKey = iKeyId;
		Notify( MSG_NON_CHAR_KEY_RECEIVED );
	}
}

////////////////////////////////////////////////////////////////////////////////

class VfaTextViaSpeech::SpeechServer : public VistaThread
{
public:
	SpeechServer( const std::string sHostname, const unsigned short uiPort )
	:	m_bKeepRunning( true )
	,	m_pTCPServer( new VistaTCPServer( sHostname, static_cast<int>( uiPort ) ) )
	,	m_pTarget( NULL )
	{
		assert( m_pTCPServer );
	}
	virtual ~SpeechServer()
	{
		m_bKeepRunning = false;
		
		m_pTCPServer->StopAccept();
		// @todo Stop accept does not cancel GetNextClient() calls. This needs
		//		 fixing before the below join can work in scenarios in which
		//		 no connection will be encountered ever before the application
		//		 closes. As a hack, an abort is used in the meantime.
		//Join();
		Abort();

		delete m_pTCPServer;		
	}

	void SetTarget( VfaTextViaSpeech* pTarget )
	{
		m_pTarget = pTarget;
	}

	virtual void ThreadBody()
	{
		VistaTCPSocket*	pTCPSocket = NULL;

		while( m_bKeepRunning )
		{
			if( pTCPSocket )
			{
				char pData[2048];
				// Timeout is so that join doesn't take forever. This will
				// eventually hang an application when it tries to delete this
				// class. Now there will be only a lag of 1000 msec.
				const int nNumRecvBytes = pTCPSocket->ReceiveRaw(
					pData, 2048, 1000 );

				if( nNumRecvBytes > 0 )
				{
					string sSpeech = string( pData );
				
					if( m_pTarget )
					{
						m_pTarget->ReceiveSpeech( sSpeech );
					}
				}
			}
			else
			{
				pTCPSocket = m_pTCPServer->GetNextClient();

				if( pTCPSocket )
				{
					pTCPSocket->SetIsBlocking( true );
				}
			}
		}
		
		if( pTCPSocket )
			pTCPSocket->CloseSocket();
		delete pTCPSocket;
	}

private:
	bool				m_bKeepRunning;
	VistaTCPServer*		m_pTCPServer;
	VfaTextViaSpeech*	m_pTarget;
};

VfaTextViaSpeech::VfaTextViaSpeech(
	const std::string& sHostname /*= "127.0.0.1"*/,
	const unsigned short uiPort /*= "12345" */ )
:	m_pSpeechServer( new SpeechServer( sHostname, uiPort ) )
,	m_bIsEnabled( true )
,	m_sLastText( "" ) // For completness' sake.
,	m_bLastTextAnnounced( false )
{
	assert( m_pSpeechServer );
	m_pSpeechServer->SetTarget( this );
	m_pSpeechServer->Run();

	VistaEventManager* pEvMngr = GetVistaSystem()->GetEventManager();
	bool bSuccess = pEvMngr->AddEventHandler( this, VistaSystemEvent::GetTypeId(),
		VistaSystemEvent::VSE_PREGRAPHICS );

	assert( bSuccess );
}

VfaTextViaSpeech::~VfaTextViaSpeech()
{
	delete m_pSpeechServer;

	VistaEventManager* pEvMngr = GetVistaSystem()->GetEventManager();
	pEvMngr->RemEventHandler( this, VistaSystemEvent::GetTypeId(),
		VistaSystemEvent::VSE_PREGRAPHICS );
}

void VfaTextViaSpeech::SetIsEnabled( const bool bIsEnabled )
{
	if( bIsEnabled != m_bIsEnabled )
	{
		m_bIsEnabled = bIsEnabled;
		Notify( MSG_ENABLED_STATE_CHANGED );
	}
}

bool VfaTextViaSpeech::GetIsEnabled() const
{
	return m_bIsEnabled;
}


std::string VfaTextViaSpeech::GetLastText()
{
	// Imagine someone accesses the current text while new text is arriving.
	VistaMutexLock lock( m_oTextMutex );
	return m_sLastText;
}


void VfaTextViaSpeech::HandleEvent( VistaEvent *pEvent )
{
	VistaMutexLock lock( m_oTextMutex );

	if( !m_bLastTextAnnounced && !m_sLastText.empty() )
	{
		Notify( MSG_TEXT_RECEIVED );
		m_bLastTextAnnounced = true;
	}
}


void VfaTextViaSpeech::ReceiveSpeech( const std::string& sSpeech )
{
	if( !m_bIsEnabled )
		return;

	VistaMutexLock lock( m_oTextMutex );
	
	m_sLastText = sSpeech;
	m_bLastTextAnnounced = false;
}

/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
