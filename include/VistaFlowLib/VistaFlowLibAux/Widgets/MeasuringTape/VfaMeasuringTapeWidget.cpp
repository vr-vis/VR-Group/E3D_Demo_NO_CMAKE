#include <cstring>
#include "VfaMeasuringTapeWidget.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaMeasuringTapeWidget::VfaMeasuringTapeWidget(VflRenderNode *pRenderNode,
	bool MeasuringTape)
:	IVfaWidget(pRenderNode),
	m_pModel(new VfaMeasuringTapeModel)
{
	m_pCtr = new VfaMeasuringTapeController(m_pModel, pRenderNode, MeasuringTape);

	m_pVis = new VfaMeasuringTapeVis(m_pModel);

	m_pVis->Init();
	pRenderNode->AddRenderable(m_pVis);

	if (MeasuringTape)
	{
		float Color1[4],Color2[4],Color3[4],Color4[4];
		//blue border
		Color1[0] = 1.0f;
		Color1[1] = 0.0f;
		Color1[2] = 0.0f;
		//blue border
		Color2[0] = 1.0f;
		Color2[1] = 1.0f;
		Color2[2] = 1.0f;
		//blue border
		Color3[0] = 0.0f;
		Color3[1] = 0.0f;
		Color3[2] = 0.0f;
		//blue border
		Color4[0] = 0.0f;
		Color4[1] = 0.0f;
		Color4[2] = 1.0f;
		VfaMeasuringTapeVis::CMeasuringTapeVisProps* pProps;
		pProps=this->GetView()->GetProperties();
		pProps->SetColors(Color1, Color2, Color3, Color4);
	}
}

VfaMeasuringTapeWidget::~VfaMeasuringTapeWidget()
{
	delete m_pCtr;
	delete m_pModel;
}

/*============================================================================*/
/*  NAME: Set/GetIsEndabled                                                   */
/*============================================================================*/
void VfaMeasuringTapeWidget::SetIsEnabled(bool b)
{
	m_pCtr->SetIsEnabled(b);
	if (b)
		m_pVis->SetVisible(true);
}
bool VfaMeasuringTapeWidget::GetIsEnabled() const
{
	return m_pCtr->GetIsEnabled();
}

/*============================================================================*/
/*  NAME: Set/GetIsVisible                                                    */
/*============================================================================*/
void VfaMeasuringTapeWidget::SetIsVisible(bool b)
{
	m_pVis->SetVisible(b);
	if (!b)
		m_pCtr->SetIsEnabled(false);
}
bool VfaMeasuringTapeWidget::GetIsVisible() const
{
	return m_pVis->GetVisible();
}


/*============================================================================*/
/*  NAME: GetModel/View/Controller                                            */
/*============================================================================*/
VfaMeasuringTapeModel *VfaMeasuringTapeWidget::GetModel() const
{
	return m_pModel;
}
VfaMeasuringTapeVis *VfaMeasuringTapeWidget::GetView() const
{
	return m_pVis;
}
VfaMeasuringTapeController *VfaMeasuringTapeWidget::GetController() const
{
	return m_pCtr;
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
