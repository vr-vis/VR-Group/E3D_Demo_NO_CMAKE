#ifndef _MEASURINGTAPECONTROLER_H
#define _MEASURINGTAPECONTROLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <vector>
#include <string>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaSphereHandle;
class VfaMeasuringTapeModel;
class VfaMeasuringTapeVis;
class VflRenderNode;
class Vfl3DTextLabel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Controll the behavior on button press event, button release event and 
 * pre-loop event, also highlight.
 */
class VISTAFLOWLIBAUXAPI VfaMeasuringTapeController
:	public IVfaWidgetController,
	public VflObserver
{
public:
	VfaMeasuringTapeController (VfaMeasuringTapeModel *pModel,
		VflRenderNode *pRenderNode, bool MeasuringTape);
	virtual ~VfaMeasuringTapeController();

	
	virtual void OnFocus(IVfaControlHandle* pHandle);
	virtual void OnUnfocus();

	virtual void OnSensorSlotUpdate(int iSlot,
		const VfaApplicationContextObject::sSensorFrame &oFrameData);
	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);
	virtual void OnTimeSlotUpdate(const double dTime);

	virtual int  GetSensorMask() const;
	virtual int  GetCommandMask() const;
	virtual bool GetTimeUpdate() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetShowText(bool b);
	bool GetShowText() const;

	void SetTextSize(float f);
	float GetTextSize() const;

	int GetDragButton() const;	
	void SetDragButton(int i);

	//********** OBSERVER INTERFACE IMPLEMENTATION *********************//
	virtual void ObserverUpdate(IVistaObserveable *pObs, int iMsg, int iTicket);

protected:
	/**
	 * enum for internal state control
	 */
	enum EInternalState{
		STATE_DISABLED,
		STATE_NOTHING_SELECTED,
		STATE_ON_FOCUS,
		STATE_MOVING
	};
private:
	VfaMeasuringTapeModel		*m_pModel;
	VflRenderNode		*m_pRenderNode;

	// handles for interaction
	vector<VfaSphereHandle*> m_vecSphereHandles;

	int m_iDragButton;

	int m_iState;
	int m_iActiveHandle;

	bool m_bFirstMove;
	bool m_bShowText;
	float m_fLastDistance;

	//enumeration for handles
	enum
	{
		HND_UNDEFINDED = -1,
		HND_SPHERE_LEFT = 0,
		HND_SPHERE_MIDI,
		HND_SPHERE_RIGHT
	};

	Vfl3DTextLabel *m_p3DTextUserAligned;

};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
