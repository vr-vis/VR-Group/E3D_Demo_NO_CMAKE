#include <cstring>

#include "VfaMeasuringTapeModel.h"
#include <VistaAspects/VistaReflectionable.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  IMPLEMENTATION      CMeasuringTapeModel                                   */
/*============================================================================*/
static const string STR_REF_TYPENAME("CMeasuringTapeModel");
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaMeasuringTapeModel::VfaMeasuringTapeModel()
	:	m_vecPoint1(-0.5f, 0.0f, 0.0f),
		m_vecPoint2( 0.5f, 0.0f, 0.0f),
		m_fLength(1.0f)
{}

VfaMeasuringTapeModel::~VfaMeasuringTapeModel()
{}

/*============================================================================*/
/*  NAME      :   Set/GetPoint1		                                          */
/*============================================================================*/
void VfaMeasuringTapeModel::SetPoint1(float fPt[])
{
	this->SetPoint1(VistaVector3D(fPt));
}
void VfaMeasuringTapeModel::SetPoint1(const VistaVector3D &v3C)
{
	m_vecPoint1 = v3C;
	this->ComputeLength();
	this->Notify(MSG_TAPE_CHANGE);
}

void VfaMeasuringTapeModel::GetPoint1(float fPt[]) const
{
	m_vecPoint1.GetValues(fPt);
}
void VfaMeasuringTapeModel::GetPoint1(VistaVector3D &v3P) const
{
	v3P = m_vecPoint1;
}

/*============================================================================*/
/*  NAME      :   Set/GetPoint2		                                          */
/*============================================================================*/
void VfaMeasuringTapeModel::SetPoint2(float fPt[])
{
	this->SetPoint2(VistaVector3D(fPt));
}
void VfaMeasuringTapeModel::SetPoint2(const VistaVector3D &v3C)
{
	m_vecPoint2 = v3C;
	this->ComputeLength();
	this->Notify(MSG_TAPE_CHANGE);
}

void VfaMeasuringTapeModel::GetPoint2(float fPt[]) const
{
	m_vecPoint2.GetValues(fPt);
}
void VfaMeasuringTapeModel::GetPoint2(VistaVector3D &v3P) const
{
	v3P = m_vecPoint2;
}
/*============================================================================*/
/*  NAME      :   GetLength			                                          */
/*============================================================================*/
float VfaMeasuringTapeModel::GetLength() const
{
	return m_fLength;
}
/*============================================================================*/
/*  NAME      :   Recalculate...	                                          */
/*============================================================================*/
void VfaMeasuringTapeModel::ComputeLength()
{
	m_fLength = (m_vecPoint1 - m_vecPoint2).GetLength();
}
/*============================================================================*/
/*  NAME      :   GetReflectionableType                                       */
/*============================================================================*/
std::string VfaMeasuringTapeModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*  NAME      :   AddToBaseTypeList                                           */
/*============================================================================*/
int VfaMeasuringTapeModel::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
