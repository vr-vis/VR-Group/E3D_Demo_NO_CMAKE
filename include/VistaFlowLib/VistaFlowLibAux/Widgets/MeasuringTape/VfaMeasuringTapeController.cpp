#include "VfaMeasuringTapeController.h"
#include "VfaMeasuringTapeModel.h"
#include <VistaFlowLibAux/Widgets/VfaWidgetTools.h>
#include "VfaMeasuringTapeVis.h"
#include <VistaFlowLibAux/Widgets/VfaSphereHandle.h>

#include <VistaMath/VistaIndirectXform.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <cstdio>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaMeasuringTapeController::VfaMeasuringTapeController(VfaMeasuringTapeModel *pModel, VflRenderNode *pRenderNode, bool MeasuringTape)
													:	IVfaWidgetController(pRenderNode),
														m_pModel(pModel),
														m_pRenderNode(pRenderNode),
														m_iState(STATE_NOTHING_SELECTED),
														m_iActiveHandle(HND_UNDEFINDED),
														m_iDragButton(0),
														m_bShowText(MeasuringTape)
{
	//attach the model
	this->Observe(m_pModel);

	//create handles
	m_vecSphereHandles.resize(3);

	m_vecSphereHandles[HND_SPHERE_LEFT] = new VfaSphereHandle(pRenderNode);
	m_vecSphereHandles[HND_SPHERE_LEFT]->SetRadius(0.02f);
	this->AddControlHandle(m_vecSphereHandles[HND_SPHERE_LEFT]);

	m_vecSphereHandles[HND_SPHERE_MIDI] = new VfaSphereHandle(pRenderNode);
	m_vecSphereHandles[HND_SPHERE_MIDI]->SetRadius(0.02f);
	this->AddControlHandle(m_vecSphereHandles[HND_SPHERE_MIDI]);

	m_vecSphereHandles[HND_SPHERE_RIGHT] = new VfaSphereHandle(pRenderNode);
	m_vecSphereHandles[HND_SPHERE_RIGHT]->SetRadius(0.02f);
	this->AddControlHandle(m_vecSphereHandles[HND_SPHERE_RIGHT]);

	//create TextLabel
	m_p3DTextUserAligned = new Vfl3DTextLabel;
	if(!m_p3DTextUserAligned->Init())
		return ;
	
	char str[255];
	sprintf(str,"   Length: %.2f",m_pModel->GetLength());
	m_p3DTextUserAligned->SetText(str);
	m_p3DTextUserAligned->SetTextSize(0.05f);
	float fColor[4] = {0.0f, 0.0f, 0.0f, 1.0f};
	m_p3DTextUserAligned->SetColor(fColor);
	pRenderNode->AddRenderable(m_p3DTextUserAligned);
	m_p3DTextUserAligned->SetTextFollowViewDir(true);

	if(!MeasuringTape)
		m_p3DTextUserAligned->SetVisible(false);
}

VfaMeasuringTapeController::~VfaMeasuringTapeController()
{
	this->ReleaseObserveable(m_pModel, IVistaObserveable::TICKET_NONE);

	if (m_p3DTextUserAligned != NULL)
	{
		m_pRenderNode->RemoveRenderable(m_p3DTextUserAligned);
		delete m_p3DTextUserAligned;
	}

	//delete handles
	for (int i = 0; i < 3; i++)
			delete m_vecSphereHandles[i];
}


/*============================================================================*/
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*============================================================================*/
void VfaMeasuringTapeController::OnSensorSlotUpdate(int iSlot,
	const VfaApplicationContextObject::sSensorFrame &oFrameData)
{

	if(m_iState!=STATE_MOVING)
		return;

	VistaVector3D v3Direktion =oFrameData.qOrientation.GetAxisAndAngle().m_v3Axis;
	if(m_bFirstMove)
	{
		VistaVector3D v3Pt1, v3Pt2;
		v3Pt1=oFrameData.v3Position;
		switch (m_iActiveHandle)
		{
			case HND_SPHERE_LEFT:
				m_pModel->GetPoint1(v3Pt2);
			  break;
			case HND_SPHERE_RIGHT:
				m_pModel->GetPoint2(v3Pt2);
			  break;
			case HND_SPHERE_MIDI:
				{
					VistaVector3D v3tmp;
					m_pModel->GetPoint1(v3Pt2);
					m_pModel->GetPoint2(v3tmp);
					v3Pt2=(v3Pt2+v3tmp)*0.5;
				}
			  break;
		}
		m_fLastDistance=(v3Pt2-v3Pt1).GetLength();
		m_bFirstMove=false;
	}
	else
	{
		VistaVector3D v3NewPos=oFrameData.v3Position +
			(v3Direktion.GetNormalized())*m_fLastDistance;
		m_vecSphereHandles[m_iActiveHandle]->SetCenter(v3NewPos);
		switch (m_iActiveHandle)
		{
			case HND_SPHERE_LEFT:
				m_pModel->SetPoint1(v3NewPos);
			  break;
			case HND_SPHERE_RIGHT:
				m_pModel->SetPoint2(v3NewPos);
			  break;
			case HND_SPHERE_MIDI:
				{
					VistaVector3D v3Pt1, v3Pt2, v3Pt3;
					m_pModel->GetPoint1(v3Pt1);
					m_pModel->GetPoint2(v3Pt2);
					v3Pt3=(v3Pt1+v3Pt2)*0.5f;
					v3NewPos=v3NewPos-v3Pt3;
					m_pModel->SetPoint1(v3Pt1+v3NewPos);
					m_pModel->SetPoint2(v3Pt2+v3NewPos);
				}
			  break;
		}
	}
}

/*============================================================================*/
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*============================================================================*/
void VfaMeasuringTapeController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	if(iSlot != m_iDragButton)
		return;
	if (m_iState==STATE_DISABLED)
		return;

	VistaVector3D vecPt1, vecPt2, vecMP;
	m_pModel->GetPoint1(vecPt1);
	m_pModel->GetPoint2(vecPt2);

	if(!bSet)
	{
		if(m_iState==STATE_MOVING)
			m_iState=STATE_ON_FOCUS;

	}
	else
	{
		if(m_iState==STATE_ON_FOCUS)
		{
			m_iState=STATE_MOVING;
			m_bFirstMove=true;
		}
	}
}

/*============================================================================*/
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*============================================================================*/
void VfaMeasuringTapeController::OnTimeSlotUpdate(const double dTime)
{
	return;
}

/*============================================================================*/
/*  NAME      :   OnFocus				                                      */
/*============================================================================*/
void VfaMeasuringTapeController::OnFocus(IVfaControlHandle* pHandle)
{
	if (m_iState==STATE_MOVING)
		return;
	if (m_iState==STATE_DISABLED)
		return;
	
	if (m_vecSphereHandles[HND_SPHERE_LEFT]==pHandle)
	{
		m_iActiveHandle=HND_SPHERE_LEFT;
		m_iState=STATE_ON_FOCUS;
		m_vecSphereHandles[HND_SPHERE_LEFT]->SetIsHighlighted(true);
	}
	else if (m_vecSphereHandles[HND_SPHERE_MIDI]==pHandle)
	{
		m_iActiveHandle=HND_SPHERE_MIDI;
		m_iState=STATE_ON_FOCUS;
		m_vecSphereHandles[HND_SPHERE_MIDI]->SetIsHighlighted(true);
	}
	else if (m_vecSphereHandles[HND_SPHERE_RIGHT]==pHandle)
	{
		m_iActiveHandle=HND_SPHERE_RIGHT;
		m_iState=STATE_ON_FOCUS;
		m_vecSphereHandles[HND_SPHERE_RIGHT]->SetIsHighlighted(true);
	}
	else
	{
		m_iActiveHandle=HND_UNDEFINDED;
		m_iState=STATE_NOTHING_SELECTED;
	}
}
/*============================================================================*/
/*  NAME      :   OnUnfocus				                                      */
/*============================================================================*/
void VfaMeasuringTapeController::OnUnfocus()
{
	if (m_iState==STATE_MOVING)
		return;
	if (m_iState==STATE_DISABLED)
		return;
	
	m_vecSphereHandles[m_iActiveHandle]->SetIsHighlighted(false);
	m_iActiveHandle=HND_UNDEFINDED;
	m_iState=STATE_NOTHING_SELECTED;
}


/*============================================================================*/
/*  NAME      :   GetSensorMask/CommandMask                                   */
/*============================================================================*/
int VfaMeasuringTapeController::GetSensorMask() const
{
	return (1<<VfaApplicationContextObject::SLOT_POINTER_VIS);
}

int VfaMeasuringTapeController::GetCommandMask() const
{
	return(1 << m_iDragButton);
}

/*============================================================================*/
/*  NAME      :   GetTimeUpdate				                                  */
/*============================================================================*/
bool VfaMeasuringTapeController::GetTimeUpdate() const
{
	return true;
}
/*============================================================================*/
/*  NAME      :   Set/GetIsEnabled                                            */
/*============================================================================*/
void VfaMeasuringTapeController::SetIsEnabled(bool b)
{
	m_vecSphereHandles[HND_SPHERE_LEFT]->SetEnable(b);
	m_vecSphereHandles[HND_SPHERE_MIDI]->SetEnable(b);
	m_vecSphereHandles[HND_SPHERE_RIGHT]->SetEnable(b);

	m_vecSphereHandles[HND_SPHERE_LEFT]->SetVisible(b);
	m_vecSphereHandles[HND_SPHERE_MIDI]->SetVisible(b);
	m_vecSphereHandles[HND_SPHERE_RIGHT]->SetVisible(b);


	if (m_p3DTextUserAligned != NULL&&!b)
		m_p3DTextUserAligned->SetVisible(false);
	else if(m_p3DTextUserAligned != NULL&&b&&m_bShowText)
		m_p3DTextUserAligned->SetVisible(true);

	if(!b)
	{
		m_iState=STATE_DISABLED;
		return;
	}
	else
	{
		m_iState=STATE_NOTHING_SELECTED;
	}

	m_pModel->Notify(VfaMeasuringTapeModel::MSG_TAPE_CHANGE);
}
bool VfaMeasuringTapeController::GetIsEnabled() const
{
	return m_iState==STATE_DISABLED;
}
/*============================================================================*/
/*  NAME    :   Get/SetShowText                                               */
/*============================================================================*/
void VfaMeasuringTapeController::SetShowText(bool b)
{
	m_bShowText=b;
	m_p3DTextUserAligned->SetVisible(b);
}

bool VfaMeasuringTapeController::GetShowText() const
{
	return m_bShowText;
}
/*============================================================================*/
/*  NAME    :   Get/SetTextSize                                               */
/*============================================================================*/
void VfaMeasuringTapeController::SetTextSize(float f)
{
	if (m_p3DTextUserAligned != NULL)
	{
		m_p3DTextUserAligned->SetTextSize(f);
	}
}

float VfaMeasuringTapeController::GetTextSize() const
{
	if (m_p3DTextUserAligned != NULL)
	{
		return m_p3DTextUserAligned->GetTextSize();
	}
	return 0.0f;
}
/*============================================================================*/
/*  NAME    :   Get/SetDragButton                                             */
/*============================================================================*/
void VfaMeasuringTapeController::SetDragButton(int i)
{
	m_iDragButton=i;
}
int VfaMeasuringTapeController::GetDragButton()const
{
	return m_iDragButton;
}
/*============================================================================*/
/*  NAME    :    ObserverUpdate                                               */
/*============================================================================*/
void VfaMeasuringTapeController::ObserverUpdate(IVistaObserveable *pObs,
	int iMsg, int iTicket)
{
	if(iMsg != VfaMeasuringTapeModel::MSG_TAPE_CHANGE)
		return;

	VistaVector3D vecPt1, vecPt2;
	m_pModel->GetPoint1(vecPt1);
	m_pModel->GetPoint2(vecPt2);
	m_vecSphereHandles[HND_SPHERE_LEFT]->SetCenter(vecPt1);
	m_vecSphereHandles[HND_SPHERE_MIDI]->SetCenter((vecPt1+vecPt2)*0.5f);
	m_vecSphereHandles[HND_SPHERE_RIGHT]->SetCenter(vecPt2);

	if(m_bShowText)
	{
		char str[255];
		sprintf(str,"   Length: %.2f",m_pModel->GetLength());
		m_p3DTextUserAligned->SetText(str);
		float fPos[3] = {
			(vecPt1[0]+vecPt2[0])*0.5f,
			(vecPt1[1]+vecPt2[1])*0.5f,
			(vecPt1[2]+vecPt2[2])*0.5f
		};
		m_p3DTextUserAligned->SetPosition(fPos);
	}
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
