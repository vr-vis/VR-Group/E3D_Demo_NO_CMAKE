

set( RelativeDir "./Widgets/MeasuringTape" )
set( RelativeSourceGroup "Source Files\\Widgets\\MeasuringTape" )

set( DirFiles
	VfaMeasuringTapeController.cpp
	VfaMeasuringTapeController.h
	VfaMeasuringTapeModel.cpp
	VfaMeasuringTapeModel.h
	VfaMeasuringTapeVis.cpp
	VfaMeasuringTapeVis.h
	VfaMeasuringTapeWidget.cpp
	VfaMeasuringTapeWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

