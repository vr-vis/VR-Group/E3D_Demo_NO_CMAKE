#ifndef _MEASURINGTAPEWIDGET_H
#define _MEASURINGTAPEWIDGET_H
 

#include <VistaFlowLibAux/Widgets/VfaWidget.h>
#include <VistaFlowLibAux/Widgets/VfaRulerVis.h>
#include "VfaMeasuringTapeVis.h"
#include "VfaMeasuringTapeModel.h"
#include "VfaMeasuringTapeController.h"
#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VflRenderNode;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaMeasuringTapeWidget : public IVfaWidget
{
public:

	VfaMeasuringTapeWidget(VflRenderNode *pRenderNode,bool MeasuringTape=true);
	virtual ~VfaMeasuringTapeWidget();

	/** 
	 * NOTE : Handle visibility and reaction to events separately.
	 *        Thus the widget may be visible but not being interacted with!
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    

	VfaMeasuringTapeModel			*GetModel() const;
	VfaMeasuringTapeVis			*GetView() const;
	VfaMeasuringTapeController		*GetController() const;

private:
	VfaMeasuringTapeController		*m_pCtr;
	VfaMeasuringTapeVis			*m_pVis;
	VfaMeasuringTapeModel			*m_pModel;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
