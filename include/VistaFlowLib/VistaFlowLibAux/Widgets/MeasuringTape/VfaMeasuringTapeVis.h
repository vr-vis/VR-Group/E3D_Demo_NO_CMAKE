#ifndef _MEASURINGTAPEVIS_H
#define _MEASURINGTAPEVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaMeasuringTapeModel;
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A simple line defined by two points
 */
class VISTAFLOWLIBAUXAPI VfaMeasuringTapeVis : public IVflRenderable
{
public: 
	explicit VfaMeasuringTapeVis(VfaMeasuringTapeModel *pModel);
	virtual ~VfaMeasuringTapeVis();

	VfaMeasuringTapeModel *GetModel() const;

	virtual void DrawOpaque();
	virtual unsigned int GetRegistrationMode() const;

	class VISTAFLOWLIBAUXAPI CMeasuringTapeVisProps
	:	public IVflRenderable::VflRenderableProperties
	{
	public:

		enum
		{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_LINE_WIDGET_CHG,
			MSG_LAST
		};

		CMeasuringTapeVisProps();
		virtual ~CMeasuringTapeVisProps();

		void	SetWidth(float f);
		float	GetWidth() const;

		bool	SetColor(const float fColor[4], const int index);
		bool	GetColor(float fColor[4], int index) const;

		bool	SetColor(const VistaColor *pColor, const int index);
		VistaColor GetColor(int index) const;

		bool	SetColors(const float fColor1[4], const float fColor2[4],
					const float fColor3[4], const float fColor4[4]);
		void	GetColors(float fColor1[4],float fColor2[4],float fColor3[4],
					float fColor4[4]) const;


	protected:
	private:
		float m_fLineWidth;
		float m_fColor1[4];
		float m_fColor2[4];
		float m_fColor3[4];
		float m_fColor4[4];
	};
	virtual CMeasuringTapeVisProps *GetProperties() const;

protected:
	virtual VflRenderableProperties* CreateProperties() const;

private:
	VfaMeasuringTapeModel *m_pModel;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
