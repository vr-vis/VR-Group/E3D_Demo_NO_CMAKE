#ifndef _MEASURINGTAPEMODEL_H
#define _MEASURINGTAPEMODEL_H

#include <VistaFlowLibAux/Widgets/VfaWidgetModelBase.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaMeasuringTapeModel : public VfaWidgetModelBase
{
public:
	enum{
		MSG_TAPE_CHANGE = VfaWidgetModelBase::MSG_LAST,
		MSG_LAST
	};

	VfaMeasuringTapeModel();
	virtual ~VfaMeasuringTapeModel();

	void SetPoint1(float fPt[3]);
	void SetPoint1(const VistaVector3D &v3C);
	void GetPoint1(float fPt[3]) const;
	void GetPoint1(VistaVector3D &v3P) const;

	void SetPoint2(float fPt[3]);
	void SetPoint2(const VistaVector3D &v3C);
	void GetPoint2(float fPt[3]) const;
	void GetPoint2(VistaVector3D &v3P) const;

	float GetLength() const;

	virtual std::string GetReflectionableType() const;

protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	void ComputeLength();

private:
	VistaVector3D m_vecPoint1;
	VistaVector3D m_vecPoint2;
	
	float m_fLength;
	float m_fWidth;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
