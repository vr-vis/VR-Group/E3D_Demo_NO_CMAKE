#include <GL/glew.h>

#include "VfaMeasuringTapeVis.h"
#include "VfaMeasuringTapeModel.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <cassert>
#include <cstring>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaMeasuringTapeVis::VfaMeasuringTapeVis(VfaMeasuringTapeModel *pModel)
:	m_pModel(pModel)
{}

VfaMeasuringTapeVis::~VfaMeasuringTapeVis()
{}
/*============================================================================*/
/*  NAME      :   GetModel                                                    */
/*============================================================================*/
VfaMeasuringTapeModel *VfaMeasuringTapeVis::GetModel() const
{
	return m_pModel;
}


/*============================================================================*/
/*  NAME      :   DrawOpaque                                                  */
/*============================================================================*/
void VfaMeasuringTapeVis::DrawOpaque()
{
	VfaMeasuringTapeVis::CMeasuringTapeVisProps *pProps =this->GetProperties();

	if(pProps == NULL)
		return;

	if(!pProps->GetVisible())
		return;

	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_ENABLE_BIT|GL_LINE_BIT);
	glPushMatrix();

	glDisable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);

	float fC[4];
	float fPoint1[3], fPoint2[3];
	m_pModel->GetPoint1(fPoint1);
	m_pModel->GetPoint2(fPoint2);
	
	float lenght=m_pModel->GetLength();
	float fNormal[3];

	fNormal[0]=(fPoint2[0]-fPoint1[0])/lenght;
	fNormal[1]=(fPoint2[1]-fPoint1[1])/lenght;
	fNormal[2]=(fPoint2[2]-fPoint1[2])/lenght;

	int i;
	glLineWidth(pProps->GetWidth());
	glBegin(GL_LINES);
	for(i=0;i<((lenght*10)-1);++i)
	{
		if(i%2==0)
		{
			if(i%20<10) pProps->GetColor(fC,0);
			else		pProps->GetColor(fC,2);	
		}else{
			if(i%20<10) pProps->GetColor(fC,1);
			else		pProps->GetColor(fC,3);
		}
		glColor4fv(fC);
		glVertex3f(fPoint1[0]+i*fNormal[0]*0.1f, fPoint1[1]+i*fNormal[1]*0.1f, fPoint1[2]+i*fNormal[2]*0.1f);
		glVertex3f(fPoint1[0]+(i+1)*fNormal[0]*0.1f, fPoint1[1]+(i+1)*fNormal[1]*0.1f, fPoint1[2]+(i+1)*fNormal[2]*0.1f);	
	}
	if(i%2==0)
	{
		if(i%20<10) pProps->GetColor(fC,0);
		else		pProps->GetColor(fC,2);	
	}else{
		if(i%20<10) pProps->GetColor(fC,1);
		else		pProps->GetColor(fC,3);
	}
	glColor4fv(fC);
	glVertex3f(fPoint1[0]+i*fNormal[0]*0.1f, fPoint1[1]+i*fNormal[1]*0.1f, fPoint1[2]+i*fNormal[2]*0.1f);
	glVertex3f(fPoint2[0], fPoint2[1], fPoint2[2]);	
	glEnd();

	glPopMatrix();
	glPopAttrib();
}
/*============================================================================*/
/*  NAME      :   GetRegistrationMode                                         */
/*============================================================================*/
unsigned int VfaMeasuringTapeVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*  NAME      :   GetProperties                                               */
/*============================================================================*/
VfaMeasuringTapeVis::CMeasuringTapeVisProps *VfaMeasuringTapeVis::GetProperties() const
{
	return static_cast<VfaMeasuringTapeVis::CMeasuringTapeVisProps *>(IVflRenderable::GetProperties());
}

/*============================================================================*/
/*  NAME      :   CreateProperties                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaMeasuringTapeVis::CreateProperties() const
{
	return new VfaMeasuringTapeVis::CMeasuringTapeVisProps;
}

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaMeasuringTapeVis::CMeasuringTapeVisProps::CMeasuringTapeVisProps()
						:	m_fLineWidth(0.5f)
{
	m_fColor1[0] = 1.0f;
	m_fColor1[1] = 0.0f;
	m_fColor1[2] = 0.0f;
	m_fColor1[3] = 1.0f;

	m_fColor2[0] = 1.0f;
	m_fColor2[1] = 0.0f;
	m_fColor2[2] = 0.0f;
	m_fColor2[3] = 1.0f;

	m_fColor3[0] = 1.0f;
	m_fColor3[1] = 0.0f;
	m_fColor3[2] = 0.0f;
	m_fColor3[3] = 1.0f;

	m_fColor4[0] = 1.0f;
	m_fColor4[1] = 0.0f;
	m_fColor4[2] = 0.0f;
	m_fColor4[3] = 1.0f;

}
VfaMeasuringTapeVis::CMeasuringTapeVisProps::~CMeasuringTapeVisProps()
{}
/*============================================================================*/
/*  NAME      :   Set/GetWidth                                                */
/*============================================================================*/
void VfaMeasuringTapeVis::CMeasuringTapeVisProps::SetWidth(float f)
{
	m_fLineWidth = f;
}
float VfaMeasuringTapeVis::CMeasuringTapeVisProps::GetWidth() const
{
	return m_fLineWidth;
}

/*============================================================================*/
/*  NAME      :   Set/GetColor                                               */
/*============================================================================*/
bool VfaMeasuringTapeVis::CMeasuringTapeVisProps::SetColor(	  const float fColor[4]
															, const int index)
{
	switch (index)
	{
		case 0:
			memcpy(m_fColor1, fColor, 4*sizeof(float));
			break;
		case 1:
			memcpy(m_fColor2, fColor, 4*sizeof(float));
			break;
		case 2:
			memcpy(m_fColor3, fColor, 4*sizeof(float));
			break;
		case 3:
			memcpy(m_fColor4, fColor, 4*sizeof(float));
			break;
		default:
			return false;
	}
	this->Notify(MSG_COLOR_CHG);
	return true;
}

bool VfaMeasuringTapeVis::CMeasuringTapeVisProps::GetColor(	  float fColor[4]
															, int index) const
{
	switch (index)
	{
		case 0:
			memcpy(fColor, m_fColor1, 4*sizeof(float));
			break;
		case 1:
			memcpy(fColor, m_fColor2, 4*sizeof(float));
			break;
		case 2:
			memcpy(fColor, m_fColor3, 4*sizeof(float));
			break;
		case 3:
			memcpy(fColor, m_fColor4, 4*sizeof(float));
			break;
		default:
			return false;
	}
	return true;
}


bool VfaMeasuringTapeVis::CMeasuringTapeVisProps::SetColor(const VistaColor *pColor, const int index)
{
	float fColor[4];
	pColor->GetValues(fColor, VistaColor::RGBA);
	return this->SetColor(fColor, index);
}
VistaColor VfaMeasuringTapeVis::CMeasuringTapeVisProps::GetColor(int index) const
{
	float fColor[4];
	this->GetColor(fColor, index);
	return VistaColor(fColor);
}

/*============================================================================*/
/*  NAME      :   Set/GetColors                                               */
/*============================================================================*/
bool VfaMeasuringTapeVis::CMeasuringTapeVisProps::SetColors(	  const float fColor1[4]
															, const float fColor2[4]
															, const float fColor3[4]
															, const float fColor4[4]	)
{
	bool ret=true;
	ret&=this->SetColor(fColor1,0);
	ret&=this->SetColor(fColor2,1);
	ret&=this->SetColor(fColor3,2);
	ret&=this->SetColor(fColor4,3);
	return ret;
}

void VfaMeasuringTapeVis::CMeasuringTapeVisProps::GetColors(	  float fColor1[4]
															 ,float fColor2[4]
															 ,float fColor3[4]
															 ,float fColor4[4]	) const
{
	memcpy(fColor1, m_fColor1, 4*sizeof(float));
	memcpy(fColor2, m_fColor2, 4*sizeof(float));
	memcpy(fColor3, m_fColor3, 4*sizeof(float));
	memcpy(fColor4, m_fColor4, 4*sizeof(float));
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
