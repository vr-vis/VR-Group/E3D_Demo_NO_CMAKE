/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VfaWidgetManager.h"
#include "VfaWidget.h"
#include "VfaWidgetController.h"
#include "VfaFocusStrategy.h"

#include <VistaFlowLibAux/Widgets/VfaControlHandle.h>
#include <VistaFlowLibAux/Interaction/VfaApplicationContextObject.h>


#include <VistaKernel/EventManager/VistaEventManager.h>
#include <VistaKernel/EventManager/VistaEvent.h>
#include <VistaKernel/EventManager/VistaSystemEvent.h>
#include <VistaKernel/InteractionManager/VistaInteractionEvent.h>
#include <VistaKernel/InteractionManager/VistaInteractionContext.h>
#include <VistaKernel/GraphicsManager/VistaGraphicsManager.h>

#include <VistaFlowLib/Visualization/VflVisController.h>

#include <algorithm>
#include <cassert>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaWidgetManager::VfaWidgetManager(VfaApplicationContextObject* pAppContext)
: m_pFocusStrategy(NULL),
  m_pApplicationContext(pAppContext),
  m_pFocusedHandle(NULL),
  m_bEnabled(true)
{
	//prevents setting 0 pointer accidentally
	assert(m_pApplicationContext);
	if(m_pApplicationContext)
		this->Observe(m_pApplicationContext);
}

VfaWidgetManager::~VfaWidgetManager()
{
	this->ReleaseObserveable(m_pApplicationContext);
	delete m_pFocusStrategy;
}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME    : SetFocusStrategy                                                */
/*                                                                            */
/*============================================================================*/
void VfaWidgetManager::SetFocusStrategy(IVfaFocusStrategy *pFocusStrategy)
{
	if(!pFocusStrategy)
		return;

	delete m_pFocusStrategy;

	m_pFocusStrategy = pFocusStrategy;

	for(std::list<IVfaWidgetController*>::const_iterator it =
		m_listWidgetCtrls.begin(); it != m_listWidgetCtrls.end(); ++it)
	{
		m_pFocusStrategy->RegisterSelectable(*it);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME    : GetFocusStrategy                                                */
/*                                                                            */
/*============================================================================*/
IVfaFocusStrategy* VfaWidgetManager::GetFocusStrategy() const
{
	return m_pFocusStrategy;
}

/*============================================================================*/
/*                                                                            */
/*  NAME    : RegisterWidget                                                  */
/*                                                                            */
/*============================================================================*/
void VfaWidgetManager::RegisterWidget(IVfaWidget *pWidget)
{
	// register widget controller
	m_listWidgetCtrls.push_back(pWidget->GetController());
	m_pFocusStrategy->RegisterSelectable(pWidget->GetController());

	// register all handles of widget controller
	std::vector<IVfaControlHandle*> pHandles = pWidget->GetController()->GetControlHandles();

	for(std::vector<IVfaControlHandle*>::const_iterator it = pHandles.begin();
		it != pHandles.end(); ++it)
	{
		m_mapHandle2Ctrl.insert( std::make_pair( *it, pWidget->GetController() ) );
		//m_pFocusStrategy->RegisterSelectable(*it);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME    : DeregisterWidget                                                */
/*                                                                            */
/*============================================================================*/
void VfaWidgetManager::DeregisterWidget(IVfaWidget *pWidget)
{
	// remove widget's controller
	m_listWidgetCtrls.remove(pWidget->GetController());

	// remove handles
	std::vector<IVfaControlHandle*> pHandles = pWidget->GetController()->GetControlHandles();

	for(std::vector<IVfaControlHandle*>::const_iterator it = pHandles.begin();
		it != pHandles.end(); ++it)
	{
		m_mapHandle2Ctrl.erase(*it);
		//m_pFocusStrategy->UnregisterSelectable(*it);
	}
	m_pFocusStrategy->UnregisterSelectable(pWidget->GetController());
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   EvaluateFocus                                               */
/*                                                                            */
/*============================================================================*/
void VfaWidgetManager::EvaluateFocus()
{
	// evaluate focus strategy for touched/focussed handles
	std::vector<IVfaControlHandle*> vecTouchedHandles;
	m_pFocusStrategy->EvaluateFocus(vecTouchedHandles);

	// if we have at least one element, this becomes focused
	if(vecTouchedHandles.size()>0)
	{
		// release old focus, if it existed and is not the same
		if(m_pFocusedHandle &&(m_pFocusedHandle != vecTouchedHandles.front()))
		{
			m_mapHandle2Ctrl[m_pFocusedHandle]->SetControllerState(IVfaWidgetController::CS_NONE, vecTouchedHandles);
		}
		// now, give focus to new focused handle
		m_pFocusedHandle = vecTouchedHandles.front();
		m_mapHandle2Ctrl[m_pFocusedHandle]->SetControllerState(IVfaWidgetController::CS_FOCUS, vecTouchedHandles);

		// delete first element from touch list, as it is focused
		vecTouchedHandles.erase(vecTouchedHandles.begin());
	}
	else
	{
		// if none is touched, none has focus, either
		if(m_pFocusedHandle)
		{
			if(m_mapHandle2Ctrl[m_pFocusedHandle])
				m_mapHandle2Ctrl[m_pFocusedHandle]->SetControllerState(IVfaWidgetController::CS_NONE, vecTouchedHandles);
		}
		m_pFocusedHandle = NULL;

	}

	// set state NONE to all widgets, which were touched before, but not any more
	for(unsigned int i=0; i < m_vecCurrentTouchedHandles.size(); ++i)
	{
		if(find(vecTouchedHandles.begin(), vecTouchedHandles.end(), m_vecCurrentTouchedHandles[i]) == vecTouchedHandles.end())
			if(m_mapHandle2Ctrl[m_vecCurrentTouchedHandles[i]])
				m_mapHandle2Ctrl[m_vecCurrentTouchedHandles[i]]->SetControllerState(IVfaWidgetController::CS_NONE, vecTouchedHandles);
	}
	m_vecCurrentTouchedHandles.clear();

	// now, set state TOUCHED to all newly touched widgets
	if(vecTouchedHandles.size()>0)
	{
		// all other elements become touched
		for(unsigned int i=0; i < vecTouchedHandles.size(); ++i)
		{
			if(m_mapHandle2Ctrl[vecTouchedHandles[i]])
				m_mapHandle2Ctrl[vecTouchedHandles[i]]->SetControllerState(IVfaWidgetController::CS_TOUCH, vecTouchedHandles);
		}
		// save touched handles
		m_vecCurrentTouchedHandles = vecTouchedHandles;
	}
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/

void  VfaWidgetManager::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if (!m_bEnabled)
		return;

	if(pObserveable == m_pApplicationContext)
	{
		// call the appropriate handling method
		switch(msg)
		{
		case VfaApplicationContextObject::MSG_SENSORCHANGE:
			OnSensorSlotChange();
			break;
		case VfaApplicationContextObject::MSG_COMMANDCHANGE:
			OnCommandSlotChange();
			break;
		case VfaApplicationContextObject::MSG_TIMECHANGE:
			OnTimeSlotChange();
			break;
		default:
			assert (false);
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnSensorSlotChange                                          */
/*                                                                            */
/*============================================================================*/

void VfaWidgetManager::OnSensorSlotChange()
{
	// TODO_HIGH: Comment on global/local slots.
	int iChangedSlot = m_pApplicationContext->GetChangedSensorSlot();
	int iSlotMask = (1 << iChangedSlot);
	// First update focus strategy. Since focus strategies are _ALWAYS_ supposed
	// to work in world space, we'll not pass in a RenderNode with the
	// sensor-frame-retrieval function call.
	if((m_pFocusStrategy->GetSensorMask() & iSlotMask) > 0)
	{
		VfaApplicationContextObject::sSensorFrame oFrame;
		oFrame = m_pApplicationContext->GetSensorFrame(iChangedSlot, 0);

		m_pFocusStrategy->OnSensorSlotUpdate(iChangedSlot, oFrame);
	}
	
	// after sensors have changed, check if focus has changed
	this->EvaluateFocus();

	// propagate data to all widget controller
	for(std::list<IVfaWidgetController*>::const_iterator itController = m_listWidgetCtrls.begin();
		itController  != m_listWidgetCtrls.end(); itController++)
	{
		if(((*itController)->GetSensorMask() & iSlotMask) > 0)
		{
			(*itController)->OnSensorSlotUpdate(iChangedSlot,
				m_pApplicationContext->GetSensorFrame(iChangedSlot,
					(*itController)->GetRenderNode()));
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnCommandSlotChange                                         */
/*                                                                            */
/*============================================================================*/

void  VfaWidgetManager::OnCommandSlotChange()
{
	int iChangedSlot = m_pApplicationContext->GetChangedCommandSlot();
	int iSlotMask =(1 << iChangedSlot);

	// first update focus strategy
	if((m_pFocusStrategy->GetCommandMask() & iSlotMask) > 0)
	{
		m_pFocusStrategy->OnCommandSlotUpdate(iChangedSlot, m_pApplicationContext->GetCommandState(iChangedSlot));
	}

	// propagate data to all widget controller
	for(std::list<IVfaWidgetController*>::iterator itController = m_listWidgetCtrls.begin();
		itController  != m_listWidgetCtrls.end(); itController++)
	{
		if(((*itController)->GetCommandMask() & iSlotMask) > 0)
		{
			(*itController)->OnCommandSlotUpdate(iChangedSlot, m_pApplicationContext->GetCommandState(iChangedSlot));
		}
	}

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotChange			                                  */
/*                                                                            */
/*============================================================================*/

void  VfaWidgetManager::OnTimeSlotChange()
{
	double dTime = m_pApplicationContext->GetTime();

	// first update focus strategy
	if(m_pFocusStrategy->GetTimeUpdate())
	{
		m_pFocusStrategy->OnTimeSlotUpdate(dTime);
	}

	// propagate data to all widget controller
	for(std::list<IVfaWidgetController*>::const_iterator itController = m_listWidgetCtrls.begin();
		itController  != m_listWidgetCtrls.end(); itController++)
	{
		if((*itController)->GetTimeUpdate())
		{
			(*itController)->OnTimeSlotUpdate(dTime);
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetEnabled				                                  */
/*                                                                            */
/*============================================================================*/

void VfaWidgetManager::SetEnabled (bool bEnabled)
{
	m_bEnabled = bEnabled;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIsEnabled				                                  */
/*                                                                            */
/*============================================================================*/
bool VfaWidgetManager::GetIsEnabled () const
{
	return m_bEnabled;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCurrentHandle			                                  */
/*                                                                            */
/*============================================================================*/
IVfaControlHandle* VfaWidgetManager::GetCurrentHandle() const
{
	return m_pFocusedHandle;
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VfaWidgetManager.cpp"                                         */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/
