/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAFOCUSSTRATEGY_H
#define _VFAFOCUSSTRATEGY_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaSlotObserver.h"

#include <vector>
#include <map>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaInteractionEvent;
class IVfaControlHandle;
class IVfaWidgetController;
class VfaSelectable;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI IVfaFocusStrategy : public IVfaSlotObserver
{
public:

	virtual ~IVfaFocusStrategy() {};

	/**
	 * Evaluate whether any control handles are touched. The first touched widget
	 * is defined as focus.
	 *
	 * Returns true, if at least one handle was touched(and is therefore focussed)
	 * , false if no handles were touched.
	 */
	virtual bool EvaluateFocus(std::vector<IVfaControlHandle*> &vecControlHandles) = 0;

	/*
	 *
	 */
	virtual void RegisterSelectable(IVfaWidgetController* pSelectable) = 0;

	/*
	 *
	 */
	virtual void UnregisterSelectable(IVfaWidgetController* pSelectable) = 0;

	
private:

};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFAFOCUSSTRATEGY_H
