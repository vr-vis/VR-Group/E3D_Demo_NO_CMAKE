/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* PRAGMAS                                                                    */
/*============================================================================*/
#ifdef WIN32
    #pragma warning(disable:4786)
#endif
#include <GL/glew.h>

#include "VfaTrajectoryInfo.h"
#include "VfaTrajectoryDraggingWidget.h"

#include <VistaFlowLib/Tools/VflPropertyLoader.h>
#include <VistaFlowLib/Tools/VflStandardLUTs.h>


#include <VistaFlowLib/Visualization/Particle/VflVisParticles.h>
#include <VistaFlowLib/Visualization/Particle/VflVirtualTubelets.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaBase/VistaVectorMath.h>

#include <VistaAspects/VistaAspectsUtils.h>

#include <VistaVisExt/Data/VveParticleTrajectory.h>

#ifdef WIN32
#include <windows.h>
#endif
#if defined(DARWIN)
	#include <OpenGL/gl.h>
	#include <GLUT/glut.h>
#else
	#include <GL/gl.h>
	#include <GL/glut.h>
#endif

#include <cassert>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
// using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTrajectoryInfo::VfaTrajectoryInfo(VflRenderNode* pRenderNode)
:	IVflRenderable()
,	m_pOriginalParticlePopulation(NULL)
,	m_pParticleData(NULL)
,	m_iDrawMode(VfaTrajectoryInfo::TRAJECTORY_UNIFORMCOLOR)
,	m_pRenderNode(pRenderNode)
,	m_pVisParticles(NULL)
,	m_fColoredDistance(0.01f)
,	m_pStaticRenderer (NULL)
,	m_fFixedRadius(0.05f)
,	m_fHighestLUTChannelValue(1.0f)
,	m_pSharedLut(NULL)
,	m_sPositionArray(VveParticlePopulation::sPositionArrayDefault)
,	m_sTimeArray(VveParticlePopulation::sTimeArrayDefault)
{ }

VfaTrajectoryInfo::~VfaTrajectoryInfo()
{
	if (m_pStaticRenderer)
	{
		m_pVisParticles->RemoveRenderer (m_pStaticRenderer);
		delete m_pStaticRenderer;
	}

	if (m_pVisParticles)
	{
		m_pRenderNode->RemoveRenderable(m_pVisParticles);
		delete m_pVisParticles;
	}


	//delete m_pOriginalParticlePopulation;
	delete m_pParticleData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawMode		                                              */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryInfo::SetDrawMode (int iDrawMode)
{
	if ((iDrawMode>=0)&&(iDrawMode < TRAJECTORY_LAST))
		m_iDrawMode = iDrawMode;

	VflVtkLookupTable *pLUT = m_pVisParticles->GetLookupTable();
	std::list<float> vPLF;
		
	switch (m_iDrawMode)
	{
	case TRAJECTORY_UNIFORMCOLOR : 
		//VflStandardLUTs::SetWhiteRedLUT(pLUT, 256);
		pLUT->SetHueRange(0.0, 0.0);
		pLUT->SetSaturationRange(1,1);
		pLUT->SetValueRange(1.0,1.0);
		pLUT->SetAlphaRange(1.0,1.0);
		pLUT->SetValueCount( 256 );
		vPLF.push_back(2);
		vPLF.push_back(0);
	
		vPLF.push_back(0.0); vPLF.push_back(m_fHighestLUTChannelValue); vPLF.push_back(1.0); vPLF.push_back(m_fHighestLUTChannelValue); vPLF.push_back(-1);
		vPLF.push_back(0.0); vPLF.push_back(m_fHighestLUTChannelValue); vPLF.push_back(1.0); vPLF.push_back(m_fHighestLUTChannelValue); vPLF.push_back(-1);
		vPLF.push_back(0.0); vPLF.push_back(m_fHighestLUTChannelValue); vPLF.push_back(1.0); vPLF.push_back(m_fHighestLUTChannelValue); vPLF.push_back(-1);
		vPLF.push_back(0.0); vPLF.push_back(1.0); vPLF.push_back(1.0); vPLF.push_back(1.0);
		m_pVisParticles->GetLookupTable()->SetPiecewiseLinearFunction(vPLF);
		break;
	case TRAJECTORY_COLORBYDISTANCE : 
		pLUT->SetHueRange(0.0, 0.0);
		pLUT->SetSaturationRange(1.0,0);
		pLUT->SetValueRange(1.0,1.0);
		pLUT->SetAlphaRange(1.0,1.0);
		pLUT->SetValueCount( 256 );
		if (BuildDistancePLF (0.0f, vPLF))
			m_pVisParticles->GetLookupTable()->SetPiecewiseLinearFunction(vPLF);
		break;
	case TRAJECTORY_COLORTEMPORALREGIONS:
		pLUT->SetHueRange(0.0, 0.0);
		pLUT->SetSaturationRange(1.0,0);
		pLUT->SetValueRange(1.0,1.0);
		pLUT->SetAlphaRange(1.0,1.0);
		pLUT->SetValueCount( 256 );
		if (BuildIntervalPLF (vPLF))
			m_pVisParticles->GetLookupTable()->SetPiecewiseLinearFunction(vPLF);
		break;
	}
}
int VfaTrajectoryInfo::GetDrawMode () const
{
	return m_iDrawMode;
}

void VfaTrajectoryInfo::SetColoredDistance (float fColoredDistance)
{
	m_fColoredDistance = fColoredDistance;
}

float VfaTrajectoryInfo::GetColoredDistance () const
{
	return m_fColoredDistance;
}

void VfaTrajectoryInfo::SetColoredIntervals (const std::vector<float> & vecIntervalPoints)
{
	m_vecIntervalPoints.resize(vecIntervalPoints.size());
	for (size_t i=0; i < vecIntervalPoints.size(); ++i)
	{
		m_vecIntervalPoints[i] = static_cast<float>(
			m_pParticleData->GetTimeMapper()->GetVisualizationTime(vecIntervalPoints[i]));
	}
	
}

bool VfaTrajectoryInfo::GetColoredIntervals (std::vector<float> & vecIntervalPoints) const
{
	vecIntervalPoints.resize (m_vecIntervalPoints.size());
	for (size_t i=0; i < m_vecIntervalPoints.size(); ++i)
	{
		vecIntervalPoints[i] = static_cast<float>(
			m_pParticleData->GetTimeMapper()->GetSimulationTime(m_vecIntervalPoints[i]));
	}
	return true;
}

void VfaTrajectoryInfo::SetParticleRadius (float fParticleRadius)
{
	m_fFixedRadius = fParticleRadius;
	 // if m_pVisParticles not init yet, you cannot access the props
	if( m_pVisParticles )
    {
		VflVisParticles::VflVisParticlesProperties *pParticleProps
			= dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(
				m_pVisParticles->GetProperties());

		if(pParticleProps)  
		{
			pParticleProps->SetRadiusAbsolute(m_fFixedRadius);
			pParticleProps->SetUseParticleRadius (false);
		}
    }
}

void VfaTrajectoryInfo::SetHighestLUTChannelValue (float fHighValue)
{
	m_fHighestLUTChannelValue = fHighValue;
}

float VfaTrajectoryInfo::GetParticleRadius () const
{
	return m_fFixedRadius;
}

unsigned int VfaTrajectoryInfo::GetRegistrationMode() const
{
	return OLI_DRAW_OPAQUE;
}

IVflRenderable::VflRenderableProperties    *VfaTrajectoryInfo::CreateProperties() const
{
	return new IVflRenderable::VflRenderableProperties ();
}


void VfaTrajectoryInfo::SetLookupTable( VflVtkLookupTable *pLut )
{
	assert(pLut);

	m_pSharedLut = pLut;

	if (m_pVisParticles)
		m_pVisParticles->SetLookupTable(pLut);
}

void VfaTrajectoryInfo::SetVisible(bool bVisible)
{
	m_pVisParticles->SetVisible (bVisible);
	IVflRenderable::SetVisible (bVisible);
}
bool VfaTrajectoryInfo::GetVisible() const
{
	return IVflRenderable::GetVisible();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetParticlePopulation                                       */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryInfo::SetParticleData (VveParticleDataCont *pParticleData)
{
	m_pOriginalParticlePopulation = pParticleData->GetData()->GetData();

	m_pParticleData = new VveParticleDataCont(pParticleData->GetTimeMapper());
	m_pParticleData->SetData(new VveParticlePopulationItem);
	m_pParticleData->GetData()->SetData(m_pOriginalParticlePopulation);

	m_pVisParticles = new VflVisParticles();
	m_pVisParticles->SetParticleData (m_pParticleData);
	m_pVisParticles->SetLookupTable(new VflVtkLookupTable);
    
	// set particle data
	m_pVisParticles->Init();
	m_pRenderNode->AddRenderable(m_pVisParticles);
	m_pVisParticles->SetVisible(true);

    VflVisParticles::VflVisParticlesProperties *pParams
        = dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(m_pVisParticles->GetProperties());
    assert(pParams);

    pParams->SetRadiusAbsolute(m_fFixedRadius);
    pParams->SetUseParticleRadius (false);
    //pParticleProps->SetRadiusAbsolute(0.1);

    pParams->SetTimeArray(m_sTimeArray);
    pParams->SetPositionArray(m_sPositionArray);

    // Use time data array as scalar data array
    pParams->SetScalarArray( m_sTimeArray);


	if ((m_pVisParticles->GetLookupTable()==NULL) && m_pSharedLut)
	{
		m_pVisParticles->SetLookupTable (m_pSharedLut);
	}

    //m_pOriginalParticlePopulation->UpdateInformation();
	if (m_pVisParticles->GetLookupTable())
	{
        float fMin, fMax;
        m_pOriginalParticlePopulation->GetBounds(pParams->GetScalarArray(), &fMin, &fMax, 1);
		m_pVisParticles->GetLookupTable()->SetTableRange(fMin, fMax);
	}

	m_pStaticRenderer = new VflVirtualTubelets(m_pVisParticles);
	if (m_pStaticRenderer->Init())
		m_pVisParticles->AddRenderer (m_pStaticRenderer);

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SelectTrajectory		                                      */
/*                                                                            */
/*============================================================================*/
bool VfaTrajectoryInfo::SelectTrajectory (unsigned int i)
{
	m_iSelectedTrajectory = i;
	
	if (m_pStaticRenderer)
	{
		std::vector<int> vSelectedParticle;
		vSelectedParticle.push_back(m_iSelectedTrajectory);
		m_pStaticRenderer->SetSelectedTrajectories(vSelectedParticle);
	}
	return true;
}


bool VfaTrajectoryInfo::SelectTrajectories (std::vector<int> vecParticleIds)
{
	if (m_pStaticRenderer)
	{
		m_pStaticRenderer->SetSelectedTrajectories(vecParticleIds);
	}
	return true;

}

void VfaTrajectoryInfo::Reset ()
{
	m_vecIntervalPoints.clear();
	if (m_pStaticRenderer)
	{
		std::vector<int> vSelectedParticle;
		m_pStaticRenderer->SetSelectedTrajectories(vSelectedParticle);
	}
}

bool VfaTrajectoryInfo::ColorTrajectory (float fVisTime)
{
	if (m_iDrawMode != TRAJECTORY_COLORBYDISTANCE)
		return false;
	
	// change piecewise linear function
	std::list<float> vPLF;

	if (BuildDistancePLF (fVisTime, vPLF))
		m_pVisParticles->GetLookupTable()->SetPiecewiseLinearFunction(vPLF);

	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VfaTrajectoryInfo::GetType() const
{
    return IVflRenderable::GetType()+std::string("::VflTrajectoryInfo");
}

std::string VfaTrajectoryInfo::GetFactoryType()
{
	return std::string("TRAJECTORY_INFO");
}


bool VfaTrajectoryInfo::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   BuildDistancePLF                                            */
/*                                                                            */
/*============================================================================*/

bool VfaTrajectoryInfo::BuildDistancePLF (float fDistinctValue, std::list<float> & vFunction)
{
	vFunction.clear();
	// 256 colors
	vFunction.push_back(256);
	// RGBA
	vFunction.push_back(VflVtkLookupTable::PLF_RGBA);

	double dVisMin = 0.0f;
	double dVisMax = 1.0f;
	m_pRenderNode->GetVisTiming()->GetVisTimeRange(dVisMin,dVisMax);

	float fEpsilon = 0.000001f;
	// red channel
	if (dVisMin>0.0)
	{
		vFunction.push_back(0.0f);
		vFunction.push_back(0.0f);
	}
	vFunction.push_back(static_cast<float>(dVisMin));
	vFunction.push_back(0.0f);
	vFunction.push_back(static_cast<float>(dVisMin+fEpsilon));
	vFunction.push_back(m_fHighestLUTChannelValue);

	vFunction.push_back(static_cast<float>(dVisMax-fEpsilon));
	vFunction.push_back(m_fHighestLUTChannelValue);
	vFunction.push_back(static_cast<float>(dVisMax));
	vFunction.push_back(0.0f);

	if (dVisMax<1.0)
	{
		vFunction.push_back(1.0f);
		vFunction.push_back(0.0f);
	}
	vFunction.push_back(-1.0f);

	// green channel
	if (dVisMin>0.0)
	{
		vFunction.push_back(0.0f);
		vFunction.push_back(0.0f);
	}
	vFunction.push_back(static_cast<float>(dVisMin));
	vFunction.push_back(0.0f);
	vFunction.push_back(static_cast<float>(dVisMin+fEpsilon));
	vFunction.push_back(m_fHighestLUTChannelValue);

	if (fDistinctValue-m_fColoredDistance > dVisMin)
	{
		vFunction.push_back(fDistinctValue-m_fColoredDistance);
		vFunction.push_back(m_fHighestLUTChannelValue);
	}

	vFunction.push_back(fDistinctValue);
	vFunction.push_back (0.0f);

	if (fDistinctValue+m_fColoredDistance < dVisMax)
	{
		vFunction.push_back(fDistinctValue+m_fColoredDistance);
		vFunction.push_back(m_fHighestLUTChannelValue);
	}

	vFunction.push_back(static_cast<float>(dVisMax-fEpsilon));
	vFunction.push_back(m_fHighestLUTChannelValue);
	vFunction.push_back(static_cast<float>(dVisMax));
	vFunction.push_back(0.0f);

	if (dVisMax<1.0)
	{
		vFunction.push_back(1.0f);
		vFunction.push_back(0.0f);
	}
	vFunction.push_back(-1.0f);

	// blue channel
	if (dVisMin>0.0)
	{
		vFunction.push_back(0.0f);
		vFunction.push_back(0.0f);
	}
	vFunction.push_back(static_cast<float>(dVisMin));
	vFunction.push_back(0.0f);
	vFunction.push_back(static_cast<float>(dVisMin+fEpsilon));
	vFunction.push_back(m_fHighestLUTChannelValue);

	if (fDistinctValue-m_fColoredDistance > dVisMin)
	{
		vFunction.push_back(fDistinctValue-m_fColoredDistance);
		vFunction.push_back(m_fHighestLUTChannelValue);
	}

	vFunction.push_back(fDistinctValue);
	vFunction.push_back(0.0f);

	if (fDistinctValue+m_fColoredDistance < dVisMax)
	{
		vFunction.push_back(fDistinctValue+m_fColoredDistance);
		vFunction.push_back(m_fHighestLUTChannelValue);
	}

	vFunction.push_back(static_cast<float>(dVisMax-fEpsilon));
	vFunction.push_back(m_fHighestLUTChannelValue);
	vFunction.push_back(static_cast<float>(dVisMax));
	vFunction.push_back(0.0f);

	if (dVisMax<1.0)
	{
		vFunction.push_back(1.0f);
		vFunction.push_back(0.0f);
	}
	vFunction.push_back(-1.0f);

	// alpha channel
	vFunction.push_back(0.0f);
	vFunction.push_back(m_fHighestLUTChannelValue);
	vFunction.push_back(-1.0f);

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   BuildIntervalPLF                                            */
/*                                                                            */
/*============================================================================*/

bool VfaTrajectoryInfo::BuildIntervalPLF (std::list<float> & vFunction)
{
	vFunction.clear();
	// 256 colors
	vFunction.push_back(256);
	// RGBA
	vFunction.push_back(VflVtkLookupTable::PLF_RGBA);

	// red channel
	vFunction.push_back(0.0f);
	vFunction.push_back (m_fHighestLUTChannelValue);

	vFunction.push_back(1.0f);
	vFunction.push_back (m_fHighestLUTChannelValue);
	vFunction.push_back(-1.0f);

	float fEpsilon = 0.000001f;
	// green channel
	vFunction.push_back(0.0f);
	vFunction.push_back (m_fHighestLUTChannelValue);
	
	for (size_t i=0; i < m_vecIntervalPoints.size(); i+=2)
	{
		if (m_vecIntervalPoints[i]-fEpsilon>0.0)
		{
			vFunction.push_back(m_vecIntervalPoints[i]-fEpsilon);
			vFunction.push_back (m_fHighestLUTChannelValue);
		}

		vFunction.push_back(m_vecIntervalPoints[i]);
		vFunction.push_back (0.0f);

		vFunction.push_back(m_vecIntervalPoints[i+1]);
		vFunction.push_back (0.0f);
		
		if (m_vecIntervalPoints[i+1]+fEpsilon<1.0)
		{
			vFunction.push_back(m_vecIntervalPoints[i+1]+fEpsilon);
			vFunction.push_back (m_fHighestLUTChannelValue);
		}

	}
	vFunction.push_back(1.0f);
	vFunction.push_back (m_fHighestLUTChannelValue);
	vFunction.push_back(-1.0f);

	// blue channel
	vFunction.push_back(0.0f);
	vFunction.push_back (m_fHighestLUTChannelValue);
	for (size_t i=0; i < m_vecIntervalPoints.size(); i+=2)
	{
		if (m_vecIntervalPoints[i]-fEpsilon>0.0)
		{
			vFunction.push_back(m_vecIntervalPoints[i]-fEpsilon);
			vFunction.push_back (m_fHighestLUTChannelValue);
		}
		vFunction.push_back(m_vecIntervalPoints[i]);
		vFunction.push_back (0.0f);

		vFunction.push_back(m_vecIntervalPoints[i+1]);
		vFunction.push_back (0.0f);
		
		if (m_vecIntervalPoints[i+1]+fEpsilon<1.0)
		{
			vFunction.push_back(m_vecIntervalPoints[i+1]+fEpsilon);
			vFunction.push_back (m_fHighestLUTChannelValue);
		}
	}
	vFunction.push_back(1.0f);
	vFunction.push_back (m_fHighestLUTChannelValue);
	vFunction.push_back(-1.0f);

	// alpha channel
	vFunction.push_back(0.0f);
	vFunction.push_back (m_fHighestLUTChannelValue);
	vFunction.push_back(-1.0f);

	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryInfo::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	
		VfaTrajectoryDraggingModel *pModel = dynamic_cast<VfaTrajectoryDraggingModel*>(pObserveable);

		if(!pModel)
			return;

        bool bUpdateParticleData = false;

		if (msg == VfaTrajectoryDraggingModel::MSG_POPULATION_CHANGED)
		{
			bUpdateParticleData = true;
		}
        if( msg == VfaTrajectoryDraggingModel::MSG_POSITION_ARRAY_CHANGED )
        {
            this->SetPositionArray(pModel->GetPositionArray());
            bUpdateParticleData = true;
        }

        if( msg == VfaTrajectoryDraggingModel::MSG_TIME_ARRAY_CHANGED )
        {
            this->SetTimeArray(pModel->GetTimeArray());
            bUpdateParticleData = true;
        }

		if( bUpdateParticleData )
        {
			// @todo This call will cause some problems when no particle data
			//		 has been set yet.
			this->SetParticleData(pModel->GetParticleData());
		}


		if (msg == VfaTrajectoryDraggingModel::MSG_SELECTION_CHANGED)
		{
			this->SelectTrajectory(pModel->GetSelectedTrajectory());
		}
		if ((msg == VfaTrajectoryDraggingModel::MSG_VISTIME_CHANGED)&&(m_iDrawMode == TRAJECTORY_COLORBYDISTANCE))
		{
			// change piecewise linear function
			std::list<float> vPLF;
			if (BuildDistancePLF(static_cast<float>(pModel->GetCurrentVisualizationTime()), vPLF))
				m_pVisParticles->GetLookupTable()->SetPiecewiseLinearFunction(vPLF);
		}


}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPositionArray                                            */
/*                                                                            */
/*============================================================================*/
bool VfaTrajectoryInfo::SetPositionArray( std::string sPositionArray )
{
    if( compAndAssignFunc<std::string>(sPositionArray, m_sPositionArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTimeArray                                                */
/*                                                                            */
/*============================================================================*/
bool VfaTrajectoryInfo::SetTimeArray( std::string sTimeArray )
{
    if (compAndAssignFunc<std::string>(sTimeArray, m_sTimeArray))
    {
        return true;
    }
    return false;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflTrajectoryInfo.cpp"                                           */
/*============================================================================*/
