/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VTNTRAJECTORYDRAGGINGMODEL_H
#define _VTNTRAJECTORYDRAGGINGMODEL_H

#include <deque>
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaFlowLib/Data/VflObserver.h>

// model
#include <VistaFlowLibAux/Widgets/VfaWidgetModelBase.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VflVisTiming;
class VveTimeMapper;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * Model for the dragging mechanism. 
 *
 *
 */
class VISTAFLOWLIBAUXAPI VfaTrajectoryDraggingModel : public VfaWidgetModelBase, public VflObserver
{
	friend class VfaTrajectoryDraggingWidget;
public:
	/**
	* messages sent by this reflectionable upon property changes
	* 	MSG_AXIS_CHANGED  the handle was moved
	*/
	enum{
		MSG_DRAGGED = VfaWidgetModelBase::MSG_LAST,
		MSG_POPULATION_CHANGED,
		MSG_SELECTION_CHANGED,
		MSG_FOCUS_CHANGED,
		MSG_POINT_CHANGED,
		MSG_VISTIME_CHANGED,
		MSG_REFERENCE_CHANGED,
        MSG_POSITION_ARRAY_CHANGED,
        MSG_SCALAR_ARRAY_CHANGED,
        MSG_TIME_ARRAY_CHANGED,		
		MSG_LAST
	};

	VfaTrajectoryDraggingModel(VflVisTiming *pVisTiming);
	virtual ~VfaTrajectoryDraggingModel();

	virtual std::string GetReflectionableType() const;

	/*
	*	Here, the particle data structure used for dragging is specified.
	*	This data contains already a valid time mapper.
	*
	*/
	void SetParticleData (VveParticleDataCont* pParticleData);
	VveParticleDataCont	* GetParticleData () const;
	VveParticlePopulation	* GetParticlePopulation () const;
	VveTimeMapper* GetTimeMapper () const;

	/*
    *   Get/ Set the name of the used particle data arrays
    */
    std::string GetPositionArray() const;
    bool SetPositionArray(std::string sPositionArray);
    std::string GetScalarArray() const;
    bool SetScalarArray(std::string sScalarArray);
    std::string GetTimeArray() const;
    bool SetTimeArray(std::string sTimeArray);

	/*
	*
	*/
	double GetCurrentVisualizationTime () const;

	/*
	* Select/unselect a trajectory of the particle population.
	*/
	bool SelectTrajectory (int i);
	bool UnselectTrajectory ();
	int GetSelectedTrajectory () const;

	/**
	 * Signals that a trajectory has been (un)focused.
	 */
	bool FocusTrajectory (int i);
	bool UnfocusTrajectory();
	int GetFocusedTrajectory() const;

	/*
	* Select a point on the selected trajectory. Only valid if a trajectory
	* was selected before.
	* Remark: Selecting a point will cause a change in visualization time!
	*/
	bool SelectPointOnTrajectory (unsigned int i);
	int GetSelectedPointOnTrajectory () const;

	bool GetSelectedPointOnTrajectory (float fPos[3]) const;

	/*
	* using this method, vis time is not update...this just changes the member variable
	*/
	void SetSelectedPointOnTrajectory (unsigned int i);


	// observer update
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/*
	* Get current direction of dragging, that is -1 (backward in time), 
	* 0 no motion (within epsilon), or 1 (forward in time)
	*/
	int GetDragDirection ();

	/*
	* Activate trajectory based rate control. If activated, and animation is running, 
	* the animation speed is controlled such that the object with the currently selected trajectory
	* is displayed at constant velocity.
	*
	* In order to use this, the particle population must have valid velocity values.
	*
	*/
	void SetActivateRateControl (bool bSet);
	bool GetIsRateControlActivated () const;

protected:
	// change vis time in time model
	void SetCurrentVisualizationTime(double dVis);

	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	// get selected particle's position velocity ... necessary for rate control
	float GetSelectedParticleVelocity () const;

private:
	VflVisTiming *				m_pVisTiming;
	// queue for last selection to determine drag direction
	std::deque<float>			m_deqSelectedTimeInstants;
	
	// population
	VveParticleDataCont         *m_pParticleData;
	VveParticlePopulation       *m_pParticlePopulation;
	VveTimeMapper               *m_pTimeMapper;

    // names of used ParticleDataArrays
    std::string                 m_sTimeArray;
    std::string                 m_sPositionArray;
    std::string                 m_sScalarArray;

	// selections
	int							m_iSelectedTrajectory;
	int							m_iFocusedTrajectory;
	int							m_iSelectedPointOnTrajectory;

	double						m_dCurrentVisTime;

	// base velocity of particle at point of selection
	float						m_fSelectedParticleBaseVelocity;

	bool						m_bRateControlActivated;

}; 



/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VTNTRAJECTORYDRAGGINGMODEL_H



