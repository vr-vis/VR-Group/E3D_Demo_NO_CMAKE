/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaParticleHandleVis.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#if defined(DARWIN)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaParticleHandleVis                                 */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaParticleHandle::VfaParticleHandle(VflRenderNode *pRenderNode)
:	IVfaCenterControlHandle (),
	m_pHandleVis (new VfaParticleHandleVis())
{
	//focus color
	m_fFocusColor[0] = 0.0f;
	m_fFocusColor[1] = 1.0f;
	m_fFocusColor[2] = 0.0f;
	m_fFocusColor[3] = 1.0f;
	
	//color if sphere highlighted
	m_fHighlightColor[0] = 1.0f;
	m_fHighlightColor[1] = 0.27f;
	m_fHighlightColor[2] = 0.0f;
	m_fHighlightColor[3] = 1.0f;

	if(m_pHandleVis->Init())
	{
		pRenderNode->AddRenderable(m_pHandleVis);
		m_pHandleVis->SetColor (m_fFocusColor);
	}
}

VfaParticleHandle::~VfaParticleHandle()
{
	m_pHandleVis->GetRenderNode()->RemoveRenderable(m_pHandleVis);
	delete m_pHandleVis;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetIsHighlighted                                            */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandle::SetIsHighlighted(bool b)
{
	IVfaCenterControlHandle::SetIsHighlighted (b);

	if (b)
	{
		m_pHandleVis->SetColor (m_fHighlightColor);
		m_pHandleVis->SetDrawRubberBand(true);
	}
	else
	{
		m_pHandleVis->SetColor (m_fFocusColor);
		m_pHandleVis->SetDrawRubberBand(false);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetVisible		                                          */
/*                                                                            */
/*============================================================================*/

void VfaParticleHandle::SetVisible(bool b)
{
	m_pHandleVis->SetVisible(b);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetSize	                                                  */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandle::SetSize(float f)
{
	m_pHandleVis->SetSize (f);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSize	                                                  */
/*                                                                            */
/*============================================================================*/
float VfaParticleHandle::GetSize() const
{
	return m_pHandleVis->GetSize();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetWarningLength                                            */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandle::SetWarningLength(float f)
{
	m_pHandleVis->SetWarningLength(f);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetWarningLength	                                          */
/*                                                                            */
/*============================================================================*/
float VfaParticleHandle::GetWarningLength() const
{
	return m_pHandleVis->GetWarningLength();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetFocusColor                                              */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandle::SetFocusColor(float fFocusColor[3])
{
	m_fFocusColor[0] = fFocusColor[0];
	m_fFocusColor[1] = fFocusColor[1];
	m_fFocusColor[2] = fFocusColor[2];
	if (!this->GetIsHighlighted())
		m_pHandleVis->SetColor (m_fFocusColor);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFocusColor                                              */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandle::GetFocusColor(float fFocusColor[3]) const
{
	fFocusColor[0] = m_fFocusColor[0];
	fFocusColor[1] = m_fFocusColor[1];
	fFocusColor[2] = m_fFocusColor[2];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetHighlightColor                                           */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandle::SetHighlightColor(float fHighlightColor[3])
{
	m_fHighlightColor[0] = fHighlightColor[0];
	m_fHighlightColor[1] = fHighlightColor[1];
	m_fHighlightColor[2] = fHighlightColor[2];
	if (this->GetIsHighlighted())
		m_pHandleVis->SetColor (m_fFocusColor);

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetHighlightColor                                           */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandle::GetHighlightColor(float fHighlightColor[3]) const
{
	fHighlightColor[0] = m_fHighlightColor[0];
	fHighlightColor[1] = m_fHighlightColor[1];
	fHighlightColor[2] = m_fHighlightColor[2];
}

void VfaParticleHandle::SetFocusPosition (const VistaVector3D & v3FocusPoint)
{
	m_v3FocusPoint = v3FocusPoint;
	m_pHandleVis->SetFocusPosition(m_v3FocusPoint);
}

void VfaParticleHandle::SetCenter(const VistaVector3D &v3DCenter)
{
	IVfaCenterControlHandle::SetCenter (v3DCenter);
	m_pHandleVis->SetCenterPosition (v3DCenter);
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaParticleHandleVis::VfaParticleHandleVis()
:	m_fSize(1.0f),
	m_fWarningLength(0.0f),
	m_bDrawRubberBand(false)
{
	//focus color
	m_fColor[0] = 0.0f;
	m_fColor[1] = 1.0f;
	m_fColor[2] = 0.0f;
	m_fColor[3] = 1.0f;
	
	m_pQuadratic = gluNewQuadric();

}

VfaParticleHandleVis::~VfaParticleHandleVis()
{
	gluDeleteQuadric(m_pQuadratic);
}

void VfaParticleHandleVis::SetDrawRubberBand(bool b)
{
	m_bDrawRubberBand = b;
}
bool VfaParticleHandleVis::GetDrawRubberBand() const
{
	return m_bDrawRubberBand;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandleVis::DrawTransparent()
{
	glMatrixMode(GL_MODELVIEW);

	if(!m_bDrawRubberBand)
	{
		glPushAttrib(GL_LIGHTING_BIT | GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_ENABLE_BIT);
		glPushMatrix();

		glDisable(GL_LIGHTING);
		glEnable (GL_DEPTH_TEST);
		glDisable(GL_ALPHA_TEST);

		glColor4f(m_fColor[0], m_fColor[1], m_fColor[2], 0.5f);
		glTranslatef(m_v3CenterPoint[0], m_v3CenterPoint[1], m_v3CenterPoint[2]);

		glutSolidSphere(m_fSize,32,32);

		glPopMatrix();
		glPopAttrib();

	}
	else
	{
		// reset transformations and states
		glPushAttrib(GL_LIGHTING_BIT | GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_ENABLE_BIT);
		glPushMatrix();

		glTranslatef(m_v3FocusPoint[0], m_v3FocusPoint[1], m_v3FocusPoint[2]);

		glEnable(GL_LIGHTING);
		glEnable(GL_NORMALIZE);
		glDisable(GL_COLOR_MATERIAL);

		GLfloat fAmbient[] = {0.2f,0.2f,0.2f,1.0f};
		GLfloat fSpecular[] = {1,1,1,1.0};

		float fLocalFocusColor [4] = {m_fColor[0], m_fColor[1], m_fColor[2], 1.0f};
		
		VistaVector3D vecRubberBand (m_v3FocusPoint-m_v3CenterPoint);
		if (vecRubberBand.GetLength()>m_fWarningLength)
		{
			fLocalFocusColor[0] = 1.0f;
			fLocalFocusColor[1] = 1.0f;
			fLocalFocusColor[2] = 0.0f;
		}

		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,fAmbient);
		glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,fLocalFocusColor);
		glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,fSpecular);
		glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0);

		// draw focus point
		//glColor4f(fLocalFocusColor[0], fLocalFocusColor[1], fLocalFocusColor[2], 1.0f);
		// the focus sphere is a little bit smaller than the selection sphere
		glutSolidSphere(m_fSize*0.75f,32,32);

		// draw "rubber band" from focus point to center
		glDisable(GL_LIGHTING);
		glColor4f(fLocalFocusColor[0], fLocalFocusColor[1], fLocalFocusColor[2], 0.5f);
		gluQuadricNormals(m_pQuadratic, GLU_SMOOTH);
		gluQuadricTexture(m_pQuadratic, GL_TRUE);
		VistaQuaternion qRotFocusToSelection (VistaVector3D(0,0,-1), vecRubberBand);

		glRotatef(Vista::RadToDeg(acos(qRotFocusToSelection[3])*2), 
			qRotFocusToSelection[0], qRotFocusToSelection[1], qRotFocusToSelection[2]);

		// the cylinder between selection and focus is half the width of the selection sphere
		gluCylinder(m_pQuadratic, m_fSize*0.5, m_fSize*0.5, vecRubberBand.GetLength()-m_fSize*0.5, 16, 16);

		glPopMatrix();
		glPopAttrib();
	}

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaParticleHandleVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_TRANSPARENT;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaParticleHandleVis::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaParticleHandleVis::GetBounds(VistaBoundingBox &)
{
	return false;
}
	
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaParticleHandleVis::CreateProperties() const
{
	return new IVflRenderable::VflRenderableProperties;
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetSize	                                                  */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandleVis::SetSize(float f)
{
	m_fSize = f;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSize	                                                  */
/*                                                                            */
/*============================================================================*/
float VfaParticleHandleVis::GetSize() const
{
	return m_fSize;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetWarningLength                                            */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandleVis::SetWarningLength(float f)
{
	m_fWarningLength = f;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetWarningLength	                                          */
/*                                                                            */
/*============================================================================*/
float VfaParticleHandleVis::GetWarningLength() const
{
	return m_fWarningLength;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetHighlightColor                                           */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandleVis::SetColor(float fColor[3])
{
	m_fColor[0] = fColor[0];
	m_fColor[1] = fColor[1];
	m_fColor[2] = fColor[2];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetHighlightColor                                           */
/*                                                                            */
/*============================================================================*/
void VfaParticleHandleVis::GetColor(float fColor[3]) const
{
	fColor[0] = m_fColor[0];
	fColor[1] = m_fColor[1];
	fColor[2] = m_fColor[2];
}

void VfaParticleHandleVis::SetFocusPosition (const VistaVector3D & v3FocusPoint)
{
	m_v3FocusPoint = v3FocusPoint;
}

void VfaParticleHandleVis::SetCenterPosition (const VistaVector3D & v3CenterPoint)
{
	m_v3CenterPoint = v3CenterPoint;
}
/*============================================================================*/
/*  END OF FILE "VfaParticleHandleVis.cpp"								      */
/*============================================================================*/



