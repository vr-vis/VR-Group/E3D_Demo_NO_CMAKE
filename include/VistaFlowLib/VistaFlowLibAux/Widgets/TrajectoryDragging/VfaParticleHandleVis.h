/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAPARTICLEHANDLEVIS_H
#define _VFAPARTICLEHANDLEVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include <VistaFlowLibAux/Widgets/VfaControlHandle.h>

#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaParticleHandleVis;
class VflRenderNode;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This is a special handle and visualization for the trajectory dragging mechanism.
 * It consists of a colored sphere around the selected point of interest, a second sphere
 * to depict the focus point (which is the actively moved focus of the input device)
 * and a rubber band in between.
 *
 *	\image HTML VistaFlowLibAux/dragging.jpg "Visualization of the VfaParticleHandle".
 */
class VISTAFLOWLIBAUXAPI VfaParticleHandle : public IVfaCenterControlHandle
{
public: 
	VfaParticleHandle(VflRenderNode *pRenderNode);
	virtual ~VfaParticleHandle();

	/**
	 * Method:      SetSize
	 *
	 * Specify size of handle visualization.
	 *
	 * @param       fSize
	 * @return      void
	 * @author      av006wo
	 * @date        2010/10/07
	 */
	void SetSize(float fSize);
	float GetSize() const;

	/**
	 * Method:      SetFocusPosition
	 *
	 * Set position of focus point.
	 *
	 * @param       v3FocusPoint
	 * @return      void
	 * @author      av006wo
	 * @date        2010/10/07
	 */
	void SetFocusPosition (const VistaVector3D & v3FocusPoint);

	/**
	 * Method:      SetCenter
	 *
	 * Set center point of handle (which is also the IVfaCenterHandle's center that 
	 * is used by the selection strategy).
	 *
	 * @param       v3DCenter
	 * @return      void
	 * @author      av006wo
	 * @date        2010/10/07
	 */
	void SetCenter(const VistaVector3D &v3DCenter);

	
	/**
	 * Method:      SetIsHighlighted
	 *
	 * Changes to/from highlight mode, i.e. changes to/from highlight  color.
	 * In highlight mode, the rubber band is drawn.
	 *
	 * @param       bHighlight
	 * @return      void
	 * @author      av006wo
	 * @date        2010/10/07
	 */
	void SetIsHighlighted(bool bHighlight);

	void SetVisible(bool b);

	
	/**
	 * Method:      SetWarningLength
	 *
	 * Change color if rubber band is longer than fLength.
	 *
	 * @param       fLength
	 * @return      void
	 * @author      av006wo
	 * @date        2010/10/07
	 */
	void SetWarningLength(float fLength);
	float GetWarningLength() const;


	
	/**
	 * Method:      SetFocusColor
	 *
	 * Color of focus position.
	 *
	 * @param       fColor
	 * @return      void
	 * @author      av006wo
	 * @date        2010/10/07
	 */
	void SetFocusColor(float fColor[3]);
	void GetFocusColor(float fColor[3]) const;

	
	/**
	 * Method:      SetHighlightColor
	 *
	 * Of sphere around selected position (center position).
	 *
	 * @param       fColor
	 * @return      void
	 * @author      av006wo
	 * @date        2010/10/07
	 */
	void SetHighlightColor(float fColor[3]);
	void GetHighlightColor(float fColor[3]) const;

private:
	float m_fFocusColor[4];
	float m_fHighlightColor[4];

	VistaVector3D m_v3FocusPoint;

	VfaParticleHandleVis* m_pHandleVis;

};

/**
 *  VfaParticleHandleVis is the renderable part (or view) of a VfaParticleHandle
 *
 */
class VISTAFLOWLIBAUXAPI VfaParticleHandleVis : public IVflRenderable
{
public:
	VfaParticleHandleVis();
	virtual ~VfaParticleHandleVis();

	virtual void DrawTransparent();

	virtual unsigned int GetRegistrationMode() const;

	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);

	void SetDrawRubberBand(bool b);
	bool GetDrawRubberBand() const;

	/*
	* Of sphere around selected position
	*/
	void SetColor(float fColor[3]);
	void GetColor(float fColor[3]) const;

	void SetSize(float f);
	float GetSize() const;

	/*
	* Change color if rubber band is longer than f.
	*
	*/
	void SetWarningLength(float f);
	float GetWarningLength() const;

	void SetFocusPosition (const VistaVector3D & v3FocusPoint);
	void SetCenterPosition (const VistaVector3D & v3CenterPoint);

protected:
	virtual VflRenderableProperties* CreateProperties() const;

private:
	float m_fSize;
	float m_fColor[4];

	float m_fWarningLength;

	bool m_bDrawRubberBand;

	VistaVector3D m_v3FocusPoint;
	VistaVector3D m_v3CenterPoint;

	GLUquadricObj *m_pQuadratic;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAPARTICLEHANDLEVIS_H
