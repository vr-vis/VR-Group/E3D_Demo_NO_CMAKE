/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES														              */
/*============================================================================*/
#include "VfaTrajectoryDraggingWidget.h"


/*============================================================================*/
/* IMPLEMENTATION			                                                  */
/*============================================================================*/
VfaTrajectoryDraggingWidget::VfaTrajectoryDraggingWidget(
	VflRenderNode *pRenderNode, VflVisTiming *pVisTiming,
	VfaTrajectoryInfo* pTrajectoryView, float fSize)
:	IVfaWidget(pRenderNode)
,	m_pVis(pTrajectoryView)
,	m_pModel(new VfaTrajectoryDraggingModel(pVisTiming))
,	m_bEnabled(true)
,	m_fSize(fSize)
{
	m_pWidgetCtl = new VfaTrajectoryDraggingController(pRenderNode, this);
	m_pWidgetCtl->SetHandleSize(m_fSize);
	
	m_pVis->SetParticleRadius(m_fSize);
	m_pVis->Observe(GetModel(), VfaTrajectoryInfo::DRAGGING);
	
	GetModel()->Notify();
}

VfaTrajectoryDraggingWidget::~VfaTrajectoryDraggingWidget()
{
	delete m_pWidgetCtl;
	delete m_pModel;
}

void VfaTrajectoryDraggingWidget::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pVis->SetVisible(m_bEnabled);
	m_pWidgetCtl->SetIsEnabled (m_bEnabled);
}

bool VfaTrajectoryDraggingWidget::GetIsEnabled() const
{
	return m_bEnabled;
}

void VfaTrajectoryDraggingWidget::SetIsVisible(bool b)
{
	m_pVis->SetVisible(b);
}

bool VfaTrajectoryDraggingWidget::GetIsVisible() const
{
	return m_pVis->GetVisible();
}
	
VfaTrajectoryDraggingModel *VfaTrajectoryDraggingWidget::GetModel() const
{
	return m_pModel;
}
VfaTrajectoryInfo* VfaTrajectoryDraggingWidget::GetView() const
{
	return m_pVis;
}
VfaTrajectoryDraggingController* VfaTrajectoryDraggingWidget::GetController() const
{
	return m_pWidgetCtl;
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

