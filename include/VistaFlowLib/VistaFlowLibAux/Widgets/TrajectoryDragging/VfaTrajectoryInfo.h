/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFATRAJECTORYINFO_H
#define _VFATRAJECTORYINFO_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLib/Visualization/VflVtkLookupTable.h>

#include <vector>
#include <string>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisParticles;
class VflVirtualTubelets;
class VflRenderNode;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
* VflTrajectoryInfo is a renderable helper to display a full trajectory path.
* Trajectory data is given in form a SVveParticlePolution, from which only a selected trajectory
* (specified by index in the population) is displayed.
*
* The whole trajectory is shown independent of current vis time. Three display modes are available,
* TRAJECTORY_UNIFORMCOLOR, TRAJECTORY_COLORBYDISTANCE and TRAJECTORY_COLORTEMPORALREGIONS. 
*
* As an example, TRAJECTORY_COLORBYDISTANCE gives the trajectory a colored depending on the distance to the current selected point
* (i.e., near = red, far away = white).
*	\image HTML VistaFlowLibAux/colorbydistance.jpg "Highlighting of close parts using TRAJECTORY_COLORBYDISTANCE".
*	
*
*/
class VISTAFLOWLIBAUXAPI VfaTrajectoryInfo : public IVflRenderable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VfaTrajectoryInfo(VflRenderNode* pRenderNode);

    virtual ~VfaTrajectoryInfo();

    /**
     */

  
	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);

	void SetVisible(bool bVisible);
	bool GetVisible() const;

	enum eDrawMode {
		TRAJECTORY_UNIFORMCOLOR = 0,		/**< the trajectories get a solid uniform color*/
		TRAJECTORY_COLORBYDISTANCE,			/**< the trajectories are colored based on the distance to the current particle*/
		TRAJECTORY_COLORTEMPORALREGIONS,	/**< the trajectories are colored based on a given set of temporal regions (intervals)*/
		TRAJECTORY_LAST
	};

	enum eObservationMessage {
		DRAGGING = 0,
		RANGE,
		MESSAGE_LAST
	};

	/**
	 * Method:      SetLookupTable
	 *
	 * This sets the particle data's lookup table. If this method is called before particle data
	 * is registered, the LUT is stored until particle data is registered.
	 *
	 * @param       pLut
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void SetLookupTable (VflVtkLookupTable *pLut);

	/**
	 * Method:      SetDrawMode
	 *
	 * Choose a value from eDrawMode.
	 * In order to work, a valid lookup table has to be registered (@see SetLookupTable).
	 *
	 * @param       iDrawMode
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void SetDrawMode (int iDrawMode);
	int GetDrawMode () const;


	/**
	 * Method:      SetColoredDistance
	 *
	 * Parameter useful for draw mode TRAJECTORY_COLORBYDISTANCE.
	 * Trajectory parts within fColoredDistance of the current position
	 * (in vis coordinates) are rendered with a more noticeable color.
	 *
	 * @param       fColoredDistance
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void SetColoredDistance (float fColoredDistance);
	float GetColoredDistance () const;

	
	/**
	 * Method:      SetColoredIntervals
	 *
	 * Parameter useful for draw mode TRAJECTORY_COLORTEMPORALREGIONS.
	 * The specified time intervals are rendered in white, all trajectory parts outside in black.
	 *
	 * @param       vecIntervalPoints      list of time instants in simtime, which pairwise define intervals
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void SetColoredIntervals (const std::vector<float> & vecIntervalPoints);
	bool GetColoredIntervals (std::vector<float> & vecIntervalPoints) const;

	
	/**
	 * Method:      SetParticleRadius
	 *
	 * @attention Must be called before SetParticleData
	 *
	 * @param       fParticleRadius
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void SetParticleRadius (float fParticleRadius);
	float GetParticleRadius () const;

	/**
	 * Method:      SetHighestLUTChannelValue
	 *
	 * Specify the highest channel value (the same for R,G,B) that is used
	 * to construct the LUTs for TRAJECTORY_COLORBYDISTANCE and TRAJECTORY_COLORTEMPORALREGIONS.
	 *
	 * The default is 1.0, choose a lower value to reduce brightness in the displayed colors.
	 *
	 * @param       fHighValue
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void SetHighestLUTChannelValue (float fHighValue);

	virtual std::string GetType() const;
	static std::string GetFactoryType();
	virtual unsigned int GetRegistrationMode() const;

	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/**
	 * Method:      SelectTrajectory
	 *
	 * Select a single trajectory that is shown.
	 *
	 * @param       iParticleId      position of the selected trajectory in the particle population vector
	 * @return      bool
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool SelectTrajectory (unsigned int iParticleId);

	/**
	 * Method:      SelectTrajectories
	 *
	 * Select a set of trajectories that are shown.
	 *
	 * @param       vecParticleIds    vector of (positions of the selected trajectories in the particle population vector)
	 * @return      bool
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool SelectTrajectories (std::vector<int> vecParticleIds);

	/**
	 * Method:      Reset
	 *
	 * Reset all selections, that is no trajectory is selected and shown.
	 *
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	void Reset();

	/**
	 * Method:      ColorTrajectory
	 *
	 * In draw mode TRAJECTORY_COLORBYDISTANCE, enforce coloring of the current trajectories
	 * by the specified vis time.
	 *
	 * If the vis time changes, this is typically overwritten in ObserverUpdate.
	 *
	 * @param       fVisTime
	 * @return      bool			false if current draw mode does not equal TRAJECTORY_COLORBYDISTANCE
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	bool ColorTrajectory (float fVisTime);

    /*
    *  Set the name of the used particle data arrays
    */
    bool SetPositionArray(std::string sPositionArray);
    bool SetTimeArray(std::string sTimeArray);

protected:
	virtual VflRenderableProperties    *CreateProperties() const;

	void SetParticleData (VveParticleDataCont *pParticleData);

	// build piecewise lookup function that colors trajectory to distance
	bool BuildDistancePLF (float fDistinctValue, std::list<float> & vFunction);

	// build piecewise lookup function that colors trajectory to distance
	bool BuildIntervalPLF (std::list<float> & vFunction);

private:
	VflRenderNode               *m_pRenderNode;
	VflVtkLookupTable     *m_pSharedLut;
	VveParticleDataCont         *m_pParticleData;
	VveParticlePopulation	    *m_pOriginalParticlePopulation;

    // names of used ParticleDataArrays
    std::string                 m_sTimeArray;
    std::string                 m_sPositionArray;
    
	int							m_iSelectedTrajectory;

	int							m_iDrawMode;
	
	float						m_fColoredDistance;
	std::vector<float>			m_vecIntervalPoints;
	
		float						m_fFixedRadius;
	float						m_fHighestLUTChannelValue;

	VflVisParticles              *m_pVisParticles;
	VflVirtualTubelets			 *m_pStaticRenderer;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFATRAJECTORYINFO_H
