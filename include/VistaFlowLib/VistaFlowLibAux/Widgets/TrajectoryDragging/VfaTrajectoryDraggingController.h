/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFATRAJECTORYDRAGGINGCONTROLLER_H
#define _VFATRAJECTORYDRAGGINGCONTROLLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaBase/VistaVectorMath.h>
#include <VistaBase/VistaTimer.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>

#include <string>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaTrajectoryDraggingWidget;
class VfaTrajectoryInfo;
class VfaParticleHandle;
class VveKdTreeResultVector;
class VveKdTree;
class VveTimeMapper;
class VistaIndirectXform;
class VfaPointerManager;
class Vfl3DTextLabel;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaTrajectoryDraggingController : public VflObserver, public IVfaWidgetController
{
public:
	VfaTrajectoryDraggingController(VflRenderNode *pRenderNode, VfaTrajectoryDraggingWidget *pWidget);
	virtual ~VfaTrajectoryDraggingController();
	
	/** 
	 * This is called on transition change into (out of) state FOCUS
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();

	/**
	 * These are called on update of all registered (sensor & command) slots
	 */
	void OnSensorSlotUpdate (int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate (int iSlot, const bool bSet);
	void OnTimeSlotUpdate (const double dTime);

	/**
	 * return an (or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;


	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	/**
     * access to select slot i.e. the application context command slot that has to be set in order
	 * to select a trajectory
	 */
	int GetSelectSlot() const;
	void SetSelectSlot(int i);


// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/**
	* These parameters come from Dragicevic et al. 2008:
	* 
	*	k is some kind of stickyness
	*	kd the penalty to drag against the current direction
	*
	*/
	void SetParameterDragicevicStrategy (float k, float kd);
	void GetParameterDragicevicStrategy (float &k, float &kd) const ;

	/**
	* Set kd-tree search radius (for efficiency). This is the squared radius r^2.
	*
	*/
	void SetParameterSearchRadius (float r);
	float GetParameterSearchRadius () const ;

	/**
	* Set handle size
	*
	*/
	void SetHandleSize (float r);
	float GetHandleSize () const ;

	/**
	* Restrict focus point to bounding box.
	* Set a bounding box, which may not be left by the focus point (if SetUseFocusBoundingBox==true).
	*
	*/
	void SetBoundingBox (const VistaVector3D &minB, const VistaVector3D &maxB);

	void SetUseFocusBoundingBox (bool bUseBB);
	bool GetUseFocusBoundingBox () const;

	/**
	* optional: give pointer manager, controller will disable pointer on selection
	*
	*/
	void SetPointerManager (VfaPointerManager*);

	
	/**
	 * Method:      GetClosestTrajectoryPoint
	 *
	 * Get closest 4D point (as 3d point and time instant) on the selected trajectory to a given 3D input position.
	 *
	 * @param[in]   vInPos 3d position 
	 * @param[out]  vOutPos nearest 3d position on selected trajectory
	 * @param[out]  fTimeInstant belonging to the vOutPos spatial position
	 * @return      bool is true if a closest point was found
	 * @author      av006wo
	 * @date        2010/10/07
	 */
	bool GetClosestTrajectoryPoint (const float* vInPos, float* vOutPos, float & fTimeInstant);

private:
	/**
	 * Method:      GetBestTrajectoryPoint
	 *
	 * Internal method to decide how to choose "best" point on the selected trajectory.
	 * "Best" means here: the closest point to the input position and the current time value
	 * decided by Dragicevic et al.'s method.
	 *
	 * @param[in]   vInPos 3d position 
	 * @param[out]  vOutPos "best" 3d position on selected trajectory
	 * @param       iId trajectory point id of "best" position
	 * @return      bool
	 * @author      av006wo
	 * @date        2010/10/07
	 */
	bool GetBestTrajectoryPoint (const float* vInPos, float* vOutPos, int & iId);

	// optional, controls visibility of beam, if used
	VfaPointerManager*           m_pVfaPointerManager;

	// mvc components
	VfaTrajectoryDraggingWidget *m_pWidget;
	VfaTrajectoryInfo*           m_pView;

	// stores a handle for each trajectory
	std::vector<VfaParticleHandle*> m_vecHandles;

	// only one trajectory can have the focus at a time
	VfaParticleHandle*			    m_pFocusHandle;
	int								m_iFocusHandleIndex;

	float						    m_fHandleSize;

	VistaIndirectXform				*m_pXForm;
	int                             m_iSelectSlot;
	
	// generell enabled flag for whole controller
	bool						      m_bEnabled;

	// restrict movement to within bounding box if desired
	bool						m_bUseFocusBoundingBox;
	VistaVector3D					m_v3MinBoundingBox;
	VistaVector3D					m_v3MaxBoundingBox;

	VfaApplicationContextObject::sSensorFrame m_oPointerFrame;
	VfaApplicationContextObject::sSensorFrame m_oViewerFrame;

	// helper structures
	std::vector < std::vector<int> > m_mapTimeIndicesToTrajectoryIndex;

	// variables for Dragicevic strategy
	float						m_fRadiusForWeightedStrategy;
	float						m_fDragicevicK;
	float						m_fDragicevicKd;


	// kd tree helper functions
	int KdTreeSearch(const float *pPos, int k, VveKdTreeResultVector &liResults) const;
	void BuildIndexMapping ();
	void BuildTree ();

	// kd-tree for trajectory
	VveKdTree					*m_pKdTree;
	float*						m_pRawData;
	int						    m_iTreeDimensions;
	float*						m_pArcLength;

	// movement-dependent variables
	int							m_iLastBestTrajectoryPoint;
	bool						m_bActiveMoving;
	VistaTimer					m_oTimer;
	double						m_dActivationTime;

	// additional visual debug output
	Vfl3DTextLabel				*m_pInfoText;
	float						m_fInfoTextOffset[3];
};


#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
