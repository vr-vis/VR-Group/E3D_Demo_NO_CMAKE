/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFATRAJECTORYDRAGGINGWIDGET_H
#define _VFATRAJECTORYDRAGGINGWIDGET_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaTrajectoryDraggingModel.h"
#include "VfaTrajectoryDraggingController.h"
#include "../TrajectoryDragging/VfaTrajectoryInfo.h"

// widget
#include <VistaFlowLibAux/Widgets/VfaWidget.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisController;
class VflVisTiming;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaTrajectoryDraggingWidget : public IVfaWidget
{
public:
	
	VfaTrajectoryDraggingWidget (VflRenderNode *pRenderNode,
								  VflVisTiming *pVisTiming, 
								  VfaTrajectoryInfo* pTrajectoryView, 
								  float fSize = 1.0f);

	virtual ~VfaTrajectoryDraggingWidget();

	
	/**
	 * Method:      SetIsEnabled
	 *
	 * Enable/disable widget. Calls Enable/disable on all components.
	 * A disabled widget will not handle events.
	 *
	 * @param       bEnabled
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	virtual void SetIsEnabled(bool bEnabled);
	virtual bool GetIsEnabled() const;

	/**
	 * Method:      SetIsVisible
	 *
	 * Show/hide widget. Calls show/hide on all components.
	 * A hidden widget will not be rendered.
	 *
	 * @param       bVisible
	 * @return      void
	 * @author      av006wo
	 * @date        2010/09/29
	 */
	virtual void SetIsVisible(bool bVisible);
	virtual bool GetIsVisible() const;

	//@{
	/**
	 * retrieve the MVC components
	 */
	VfaTrajectoryInfo* GetView() const;

	VfaTrajectoryDraggingModel *GetModel() const;

	VfaTrajectoryDraggingController* GetController() const;
	//@}

private:
	VfaTrajectoryDraggingController  *m_pWidgetCtl;
	VfaTrajectoryInfo				  *m_pVis;

	VfaTrajectoryDraggingModel		*m_pModel;
	bool							 m_bEnabled;
	float							 m_fSize;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFATRAJECTORYDRAGGINGWIDGET_H



