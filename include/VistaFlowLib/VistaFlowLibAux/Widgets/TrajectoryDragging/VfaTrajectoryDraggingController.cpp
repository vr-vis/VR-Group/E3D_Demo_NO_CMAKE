/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES					                                                  */
/*============================================================================*/
#include "VfaTrajectoryDraggingController.h" 
#include "VfaTrajectoryDraggingWidget.h" 
#include "VfaTrajectoryDraggingModel.h" 
#include "VfaTrajectoryInfo.h" 
#include "VfaParticleHandleVis.h" 

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>

#include <VistaFlowLibAux/Interaction/VfaPointerManager.h>

#include <VistaVisExt/Data/VveKdTree.h>
#include "VistaVisExt/Data/VveParticleTrajectory.h"

#include <VistaMath/VistaGeometries.h>

#include <VistaBase/VistaTimer.h>
#include <VistaMath/VistaIndirectXform.h>

#include <math.h>
#include <string.h>


/*============================================================================*/
/* LOCAL FUNCTIONS			                                                  */
/*============================================================================*/
bool lessTestFunction(const SKdtreeResult& lhs, const SKdtreeResult& rhs) 
{
	return lhs.fDistance < rhs.fDistance;
}


/*============================================================================*/
/* IMPLEMENTATION			                                                  */
/*============================================================================*/
VfaTrajectoryDraggingController::VfaTrajectoryDraggingController(
	VflRenderNode *pRenderNode, VfaTrajectoryDraggingWidget *pWidget)
:	IVfaWidgetController(pRenderNode)
,	m_pWidget(pWidget)
,	m_pFocusHandle(NULL)
,	m_iFocusHandleIndex(-1)
,	m_fRadiusForWeightedStrategy(0.0f)
,	m_iSelectSlot(0)
,	m_pXForm(new VistaIndirectXform)
,	m_pKdTree(NULL)
,	m_pRawData(NULL)
,	m_pArcLength(NULL)
,	m_iLastBestTrajectoryPoint(-1)
,	m_bActiveMoving(false)
,	m_iTreeDimensions(3)
,	m_fDragicevicK(1.0f)
,	m_fDragicevicKd(1.0f)
,	m_bUseFocusBoundingBox(false)
,	m_pVfaPointerManager(NULL)
,	m_dActivationTime(0.0f)
,	m_fHandleSize(0.0f)
{
	m_pView = m_pWidget->GetView();
	this->Observe (m_pWidget->GetModel());

	m_pInfoText = new Vfl3DTextLabel;
	m_pInfoText->Init();
	this->GetRenderNode()->AddRenderable(m_pInfoText);
	m_pInfoText->SetText("");
	float fToolColor[4] = {1.0f, 1.0f, 0.4f, 1.0f};
	m_pInfoText->SetColor(fToolColor);
	m_pInfoText->SetTextSize(0.2f);
	m_fInfoTextOffset[0] = 0.0f;
	m_fInfoTextOffset[1] = 0.24f;
	m_fInfoTextOffset[2] = 0.0f;
	m_pInfoText->SetPosition(m_fInfoTextOffset);

}
VfaTrajectoryDraggingController::~VfaTrajectoryDraggingController()
{
	for(size_t i=0; i<m_vecHandles.size(); ++i)
	{
		delete m_vecHandles[i];
	}

	//info text
	this->GetRenderNode()->RemoveRenderable(m_pInfoText);
	delete m_pInfoText;

	delete m_pXForm;
	// clean up 
	delete [] m_pRawData;
	delete [] m_pArcLength;
	delete m_pKdTree;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Focus			                                              */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryDraggingController::OnFocus(IVfaControlHandle* pHandle)
{
	if (!m_bActiveMoving)
	{
		m_pFocusHandle= dynamic_cast<VfaParticleHandle*> (pHandle);

		// set focused handle visible only, not highlight
		if (m_pFocusHandle)
			m_pFocusHandle->SetVisible(true);

		// search focused handle in list
		m_iFocusHandleIndex = static_cast<int>( 
			find(m_vecHandles.begin(), m_vecHandles.end(), m_pFocusHandle)
			- m_vecHandles.begin()
			);
		m_pWidget->GetModel()->FocusTrajectory(m_iFocusHandleIndex);
	}

}
void VfaTrajectoryDraggingController::OnUnfocus()
{
	if (!m_bActiveMoving)
	{
		// hide focus point and show pointer
		if (m_pFocusHandle)
		{
			m_pFocusHandle->SetIsHighlighted(false);
			m_pFocusHandle->SetVisible(false);
			if (!m_pWidget->GetModel()->GetIsRateControlActivated())
			m_pInfoText->SetText("");
			m_pWidget->GetModel()->UnfocusTrajectory();
			m_iFocusHandleIndex = -1;
		}
		m_pFocusHandle = NULL;
		if (m_pVfaPointerManager)
			m_pVfaPointerManager->SetPointerVisible(true);
	}

}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnSensorSlotUpdate                                          */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryDraggingController::OnSensorSlotUpdate (int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	if (iSlot == VfaApplicationContextObject::SLOT_VIEWER_VIS)
	{
		m_oViewerFrame = oFrameData;
		return;
	}

	if (iSlot == VfaApplicationContextObject::SLOT_POINTER_VIS)
		m_oPointerFrame = oFrameData;

	if (m_bActiveMoving)
	{
		// move handle
		VistaVector3D vPos;
		VistaQuaternion qOri;
		m_pXForm->Update(m_oPointerFrame.v3Position, m_oPointerFrame.qOrientation,vPos,qOri);

		float aPos [3];
		vPos.GetValues (aPos);

		// restrict movement to boundingbox ?
		bool bOutsideBoundingBox = false;
		if (m_bUseFocusBoundingBox)
		{
			if (m_v3MinBoundingBox != m_v3MaxBoundingBox)
			{
				for (int i=0; i < 3; ++i)
				{
					if (aPos[i] < m_v3MinBoundingBox[i])
					{
						aPos[i] = m_v3MinBoundingBox[i];
						bOutsideBoundingBox = true;
					}
				}
				for (int i=0; i < 3; ++i)
				{
					if (aPos[i] > m_v3MaxBoundingBox[i])
					{
						aPos[i] = m_v3MaxBoundingBox[i];
						bOutsideBoundingBox = true;
					}
				}

				//save values back to vector for further use
				vPos.SetValues(aPos);
			}
		}

		// update focus point
		m_pFocusHandle->SetFocusPosition (vPos);

		// get "closest trajectory point" and set as new handle center
		float aTrajectoryPoint[3];
		if (this->GetBestTrajectoryPoint (aPos,aTrajectoryPoint, m_iLastBestTrajectoryPoint))
		{
			// inform model that a new point on the selected trajectory was selected
			// if this returns false, the selected point does not have a valid time value (e.g., out of range)
			if (m_pWidget->GetModel()->SelectPointOnTrajectory (m_iLastBestTrajectoryPoint))
			{
				// update selected point
				m_pFocusHandle->SetCenter(VistaVector3D(aTrajectoryPoint[0],aTrajectoryPoint[1],aTrajectoryPoint[2]));
			}
		}
		else
			m_iLastBestTrajectoryPoint = -1;

	}

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnCommandSlotUpdate                                         */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryDraggingController::OnCommandSlotUpdate (int iSlot, const bool bSet)
{
	if (iSlot != m_iSelectSlot)
		return;

	if (bSet)
	{
		if (this->GetControllerState()==IVfaWidgetController::CS_FOCUS)
		{
			// init xform, in vis space
			VistaQuaternion qOri;
			VistaVector3D v3Center;
			m_pFocusHandle->GetCenter(v3Center);
			m_pXForm->Init(m_oPointerFrame.v3Position, m_oPointerFrame.qOrientation, v3Center, qOri);
			m_bActiveMoving = true;
			m_dActivationTime = m_oTimer.GetSystemTime();

			assert( m_iFocusHandleIndex != -1 );

			// select the trajectory corresponding to this handle (have the same indices)
			m_pWidget->GetModel()->SelectTrajectory(m_iFocusHandleIndex);
			int iTimeIdx = m_pWidget->GetModel()->GetTimeMapper()->GetTimeIndex (m_pWidget->GetModel()->GetCurrentVisualizationTime());
			int iIdx = m_mapTimeIndicesToTrajectoryIndex[m_pWidget->GetModel()->GetSelectedTrajectory()] [iTimeIdx];
			m_pWidget->GetModel()->SelectPointOnTrajectory (iIdx);
			// show trajectory view
			m_pWidget->GetView()->SetVisible(true);
			m_pWidget->GetView()->SelectTrajectory(m_pWidget->GetModel()->GetSelectedTrajectory());

			// show focus point and hide pointer
			m_pFocusHandle->SetIsHighlighted(true);
			m_pFocusHandle->SetFocusPosition (v3Center);
			if (m_pVfaPointerManager)
				m_pVfaPointerManager->SetPointerVisible(false);			
		}
	}
	else
	{
		if (m_bActiveMoving)
		{
			m_bActiveMoving = false;
			if (m_oTimer.GetSystemTime()-m_dActivationTime < 0.5)
			{
				m_pWidget->GetModel()->UnselectTrajectory();
				m_pWidget->GetView()->Reset();
				m_pWidget->GetView()->SetVisible(false);
			}
		}
		if (this->GetControllerState()!=CS_FOCUS)
		{
			if (m_pFocusHandle)
			{
				m_pFocusHandle->SetIsHighlighted(false);
				m_pFocusHandle->SetVisible(false);
				m_pWidget->GetModel()->UnfocusTrajectory();
				m_iFocusHandleIndex = -1;
			}
			m_pFocusHandle = NULL;
			if (m_pVfaPointerManager)
				m_pVfaPointerManager->SetPointerVisible(true);
		}

	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotUpdate                                            */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryDraggingController::OnTimeSlotUpdate (const double dTime)
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetMasks			                                          */
/*                                                                            */
/*============================================================================*/

int VfaTrajectoryDraggingController::GetSensorMask() const
{
	return (1 << VfaApplicationContextObject::SLOT_POINTER_VIS) | (1 << VfaApplicationContextObject::SLOT_VIEWER_VIS);
}
int VfaTrajectoryDraggingController::GetCommandMask() const
{
	return (1 << m_iSelectSlot);
}
bool VfaTrajectoryDraggingController::GetTimeUpdate() const
{
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Enable			                                          */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryDraggingController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	for(size_t i=0; i<m_vecHandles.size(); ++i)
	{
		m_vecHandles[i]->SetVisible (m_bEnabled);
		m_vecHandles[i]->SetEnable(m_bEnabled);
	}
}
bool VfaTrajectoryDraggingController::GetIsEnabled() const
{
	return m_bEnabled;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SelectSlot		                                          */
/*                                                                            */
/*============================================================================*/

int VfaTrajectoryDraggingController::GetSelectSlot() const
{
	return m_iSelectSlot;
}
void VfaTrajectoryDraggingController::SetSelectSlot(int i)
{
	m_iSelectSlot = i;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBoundingBox		                                      */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryDraggingController::SetBoundingBox (const VistaVector3D &minB, const VistaVector3D &maxB)
{
	m_v3MinBoundingBox = minB;
	m_v3MaxBoundingBox = maxB;

}

void VfaTrajectoryDraggingController::SetUseFocusBoundingBox (bool bUseBB)
{
	m_bUseFocusBoundingBox = bUseBB;
}
bool VfaTrajectoryDraggingController::GetUseFocusBoundingBox () const
{
	return m_bUseFocusBoundingBox;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPointerManager		                                      */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryDraggingController::SetPointerManager (VfaPointerManager* pMng)
{
	m_pVfaPointerManager = pMng;
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   3D closest point with time instant						  */
/*                                                                            */
/*============================================================================*/

bool VfaTrajectoryDraggingController::GetClosestTrajectoryPoint(
	const float* vInPos, float* vOutPos, float & fTimeInstant)
{
	if (m_pWidget->GetModel()->GetSelectedTrajectory()<0)
		return false;

	std::list<int> liTreeSteps;

	float vTempPos[4];
	memcpy (vTempPos, vInPos, sizeof(float)*3);
	const double dCurVisTime = m_pWidget->GetModel()->GetCurrentVisualizationTime();
	vTempPos[3] = static_cast<float>(
		m_pWidget->GetModel()->GetTimeMapper()->GetSimulationTime(dCurVisTime));

	// get closest point from kd tree
	VveKdTreeResultVector vecResults;
	int iClosestId = KdTreeSearch(vTempPos, 5, vecResults);

	if (iClosestId < 0)
	{
		vstr::warnp() << "[VfaTrajectoryDraggingController] No ID found\n";
		return false;
	}

    VveParticleTrajectory* pTraj = m_pWidget->GetModel()->GetParticlePopulation()->GetTrajectory( 
                                                    m_pWidget->GetModel()->GetSelectedTrajectory());
    VveParticleDataArrayBase* pPosArray = NULL;
    VveParticleDataArrayBase* pTimeArray = NULL;

    if( pTraj && 
        pTraj->GetArrayByName(m_pWidget->GetModel()->GetPositionArray(), pPosArray) && pPosArray &&
        pTraj->GetArrayByName(m_pWidget->GetModel()->GetTimeArray(), pTimeArray) && pTimeArray )
    {
        pPosArray->GetElementCopy(iClosestId, vOutPos, 3);
        fTimeInstant = static_cast<float>(
			m_pWidget->GetModel()->GetTimeMapper()->GetVisualizationTime(
				pTimeArray->GetElementValueAsFloat(iClosestId)));
		return true;
    }
    return false;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   parameter for distance function                             */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryDraggingController::SetParameterDragicevicStrategy (float k, float kd)
{
	// dragicevic's constant
	m_fDragicevicK = k;
	m_fDragicevicKd = kd;
}

void VfaTrajectoryDraggingController::GetParameterDragicevicStrategy (float &k, float &kd) const
{
	// dragicevic's constant
	k = m_fDragicevicK;
	kd = m_fDragicevicKd;
}

void VfaTrajectoryDraggingController::SetParameterSearchRadius (float r)
{
	m_fRadiusForWeightedStrategy = r;
	// udpate warning length for all handles
	for (size_t i=0; i < m_vecHandles.size(); ++i)
	{
		m_vecHandles [i]->SetWarningLength(m_fRadiusForWeightedStrategy);
	}
}
float VfaTrajectoryDraggingController::GetParameterSearchRadius () const 
{
	return m_fRadiusForWeightedStrategy;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   handle size				                                  */
/*                                                                            */
/*============================================================================*/
void VfaTrajectoryDraggingController::SetHandleSize (float r)
{
	m_fHandleSize = r;
	for (size_t i=0; i < m_vecHandles.size(); ++i)
		m_vecHandles [i]->SetSize(m_fHandleSize);

	//m_pView->SetParticleRadius(0.5f*r);     // big NO NO...! ;-)
}
float VfaTrajectoryDraggingController::GetHandleSize () const 
{
	return m_fHandleSize;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   3D arc-length closest point                                 */
/*                                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBestTrajectoryPoint									  */
/*                                                                            */
/*============================================================================*/


bool VfaTrajectoryDraggingController::GetBestTrajectoryPoint (const float* vInPos, float* vOutPos, int & iId)
{
	std::list<int> liTreeSteps;

	float vTempPos[4];
	memcpy (vTempPos, vInPos, sizeof(float)*3);
	const double dCurVisTime = m_pWidget->GetModel()->GetCurrentVisualizationTime();
	const float fCurrentSimTime = static_cast<float>(
		m_pWidget->GetModel()->GetTimeMapper()->GetSimulationTime(dCurVisTime));
	vTempPos[3] = fCurrentSimTime;
	
	float fCurrentArcLength = 0.0f;
	////vTempPos[3] = 0.0f;
	// arc-length of current selected point
	int iCurrentSelectedPoint = m_pWidget->GetModel()->GetSelectedPointOnTrajectory();
	
	if (iCurrentSelectedPoint>=0)
		fCurrentArcLength = m_pArcLength[iCurrentSelectedPoint];
	else
	{
		// no point selected, get the arc-length of the current particle point on the selected
		if (m_pWidget->GetModel()->GetSelectedTrajectory()>=0)
		{
			// get partice index for current selected trajectory
			int iTimeIdx = m_pWidget->GetModel()->GetTimeMapper()->GetTimeIndex (m_pWidget->GetModel()->GetCurrentVisualizationTime());
			int iIdx = m_mapTimeIndicesToTrajectoryIndex[m_pWidget->GetModel()->GetSelectedTrajectory()] [iTimeIdx];
			if (iIdx>=0)
				fCurrentArcLength = m_pArcLength[iIdx];
		}
	}

	vTempPos[3] = fCurrentArcLength; 

	// get closest point within radius
	VveKdTreeResultVector vecResults;
	
	std::vector<float> qv;
	qv.resize(m_iTreeDimensions);
	for (int j=0; j < m_iTreeDimensions; ++j)
		qv[j] = vTempPos[j];

	
	// 1. get nearest candidate
	VveKdTreeResultVector oResult;
	m_pKdTree->GetNNearest (qv, 1, oResult);

	if (oResult.empty())
	{
		// even now nothing found => return false
		return false;
	}

	// 2. search by radius with nearest candidate + possibly m_fDragicevicKd depending on dragging direction
	m_pKdTree->GetNearestByRadius (qv, (oResult[0].fDistance+m_fDragicevicKd)*(oResult[0].fDistance+m_fDragicevicKd), oResult);

	// @todo This was added because an empty result could occur when Kd was set to 0.
	//		 It is not clear why this happen, but the following at least fixes the
	//		 crash occuring further down in the code when an empty result was processed.
	if (oResult.empty())
	{
		// even now nothing found => return false
		return false;
	}

	// 3. these results are evaluated according to their "real" distance (including m_fDragicevicKd)
    VveParticleTrajectory* pTraj = m_pWidget->GetModel()->GetParticlePopulation()->GetTrajectory( 
                                                        m_pWidget->GetModel()->GetSelectedTrajectory());
    VveParticleDataArrayBase* pPosArray = NULL;
    VveParticleDataArrayBase* pTimeArray = NULL;

    if( !pTraj || 
        !pTraj->GetArrayByName(m_pWidget->GetModel()->GetPositionArray(), pPosArray) || !pPosArray ||
        !pTraj->GetArrayByName(m_pWidget->GetModel()->GetTimeArray(), pTimeArray) || !pTimeArray )
    {
        return false;
    }

	int iDragDirection = m_pWidget->GetModel()->GetDragDirection();
	float kd = 0.0f;

	VistaVector3D vViewDir = m_oViewerFrame.qOrientation.Rotate (VistaVector3D(0,0,-1));
	
	// compute "real" distance function
	for (size_t i=0; i < oResult.size(); ++i)
	{
        //oParticleInstant = oTrajectory.vecTrajectory[oResult[i].iIndex];
		float fTime = pTimeArray->GetElementValueAsFloat(oResult[i].iIndex);
        float aPos[3]; 
        pPosArray->GetElementCopy(oResult[i].iIndex, aPos);

		// dragicevic
		// additional value kd for inversed sign 
		if ( ((iDragDirection<=0)&&(fTime-fCurrentSimTime<=0))
			||((iDragDirection>=0)&&(fTime-fCurrentSimTime>=0)))
			kd = 0.0f;
		else
			kd = m_fDragicevicKd;

		float fArcLength = m_pArcLength[oResult[i].iIndex];
		oResult[i].fDistance = sqrt ( pow (aPos[0]-qv[0],2)+ 
									  pow (aPos[1]-qv[1],2)+ 
									  pow (aPos[2]-qv[2],2)+ 
									  pow (fArcLength-fCurrentArcLength,2))+kd; 
	}

	// sort resulting nearest neighbor according to distance
	std::sort (oResult.begin(), oResult.end(), lessTestFunction);

	int iClosestId = oResult.front().iIndex;

	if (iClosestId < 0)
	{
		vstr::warnp() << "[VfaTrajectoryDraggingController] No ID found\n";
		return false;
	}

    pPosArray->GetElementCopy(iClosestId, vOutPos);

	//vstr::debugi() << "[VfaTrajectoryDraggingController] [WEIGHTED] Selected ID "<<iClosestId<<" from "<<oResult.size()<<" candidates, dist "<<oResult.front().fDistance<<" sim time "<<oTrajectory.vecTrajectory[iClosestId].fTime<<std::endl;

	m_iLastBestTrajectoryPoint = iClosestId;
	iId = iClosestId;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   BuildTree				                                      */
/*                                                                            */
/*============================================================================*/
void VfaTrajectoryDraggingController::BuildTree ()
{
	// clean up 
	delete [] m_pRawData;
	delete m_pKdTree;
	delete [] m_pArcLength;

	// copy all trajectory points of selected trajectory into a helper structure
    VveParticleTrajectory* pTraj = m_pWidget->GetModel()->GetParticlePopulation()->GetTrajectory( 
                                                m_pWidget->GetModel()->GetSelectedTrajectory());
    VveParticleDataArrayBase* pPosArray = NULL;
    VveParticleDataArrayBase* pTimeArray = NULL;

    if( !pTraj || 
        !pTraj->GetArrayByName(m_pWidget->GetModel()->GetPositionArray(), pPosArray) || !pPosArray ||
        !pTraj->GetArrayByName(m_pWidget->GetModel()->GetTimeArray(), pTimeArray) || !pTimeArray )
    {
        vstr::outi() << "[VfaTrajectoryDraggingController::BuildTree] position data array "
					 << "\'" << m_pWidget->GetModel()->GetPositionArray() << "\'"
					 << " and/or time data array "
					 << "\'" << m_pWidget->GetModel()->GetTimeArray() << "\'"
					 << " not found!"
					 << std::endl;
        return;
    }

	int iCount = static_cast<int>(pTimeArray->GetSize());

	m_pRawData = new float [iCount*m_iTreeDimensions];
	m_pArcLength = new float [iCount];

	float fArcLength=0.0f;
	for (int i=0; i<iCount; ++i)
	{
        float aPosA[3];
        pPosArray->GetElementCopy(i, aPosA);
        float fTime = pTimeArray->GetElementValueAsFloat(i);

		m_pRawData[i*m_iTreeDimensions+0] = aPosA[0];
		m_pRawData[i*m_iTreeDimensions+1] = aPosA[1];
		m_pRawData[i*m_iTreeDimensions+2] = aPosA[2];
		
		if (i>0)
		{
			float aPosB[3];
			pPosArray->GetElementCopy(i-1, aPosB);
			fArcLength += sqrt ((aPosA[0]-aPosB[0])*(aPosA[0]-aPosB[0])+
								(aPosA[1]-aPosB[1])*(aPosA[1]-aPosB[1])+
								(aPosA[2]-aPosB[2])*(aPosA[2]-aPosB[2]));
		}
		if (m_iTreeDimensions==4)
		{
			m_pRawData[i*m_iTreeDimensions+3] = fTime;
			//m_pRawData[i*m_iTreeDimensions+3] = m_fDragicevicK*fArcLength;
		}

		// save the arc-length multiplied with the constant K "stickyness"
		m_pArcLength[i] = m_fDragicevicK*fArcLength;

	}
	m_pKdTree = new VveKdTree (m_pRawData, iCount, m_iTreeDimensions, false);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   BuildIndexMapping		                                      */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryDraggingController::BuildIndexMapping ()
{
	// build a helper map for time indices to trajectory points,
	// this helps to search the position on each trajectory for a given time index

	m_mapTimeIndicesToTrajectoryIndex.clear();
	int iTimeIndexCount = m_pWidget->GetModel()->GetTimeMapper()->GetNumberOfTimeIndices();
	float fSimTimeForTimeIndex;

	// one entry for each trajectory
	m_mapTimeIndicesToTrajectoryIndex.resize(m_pWidget->GetModel()->GetParticlePopulation()->GetNumTrajectories());

	// for each trajectory, store where the position for a time index begins
	for (int iTrajectory = 0; iTrajectory < m_pWidget->GetModel()->GetParticlePopulation()->GetNumTrajectories(); ++iTrajectory)
	{
        VveParticleTrajectory* pTraj = m_pWidget->GetModel()->GetParticlePopulation()->GetTrajectory( iTrajectory );
        VveParticleDataArrayBase* pTimeArray = NULL;

        if( !pTraj || !pTraj->GetArrayByName(m_pWidget->GetModel()->GetTimeArray(), pTimeArray) || !pTimeArray )
            continue;
        
        int iParticleCount = static_cast<int>(pTimeArray->GetSize());

		int j=0;
		// for each time index
		for (int i=0; i < iTimeIndexCount; ++i)
		{
			fSimTimeForTimeIndex = static_cast<float>(m_pWidget->GetModel()->GetTimeMapper()->GetSimulationTime(i));
			while ((j<iParticleCount) && (pTimeArray->GetElementValueAsFloat(j)<=fSimTimeForTimeIndex))
			{
				++j;
			}
			m_mapTimeIndicesToTrajectoryIndex[iTrajectory].push_back(j-1);
		}
	}
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   KdTreeSearch                                                */
/*                                                                            */
/*============================================================================*/
int VfaTrajectoryDraggingController::KdTreeSearch(const float *pPos,
											   int k, VveKdTreeResultVector &vecResults) const
{
	std::vector<float> qv;
	qv.resize(m_iTreeDimensions);
	for (int j=0; j < m_iTreeDimensions; ++j)
		qv[j] = pPos[j];

	VveKdTreeResultVector oResult;
	m_pKdTree->GetNNearest (qv, k,oResult);

	// sort resulting k nearest neighbor according to distance
	std::sort (oResult.begin(), oResult.end(), lessTestFunction);

	vecResults = oResult;

	return oResult[0].iIndex;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/

void VfaTrajectoryDraggingController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaTrajectoryDraggingModel *pModel = dynamic_cast<VfaTrajectoryDraggingModel*>(pObserveable);

	if(!pModel)
		return;

	if (msg == VfaTrajectoryDraggingModel::MSG_POPULATION_CHANGED)
	{
		// if population has changed, create handles for each trajectory
		// MW: TODO: handles are not cleaned up when created once, so you cannot overwrite
		// an existing trajectory

		m_vecHandles.resize (m_pWidget->GetModel()->GetParticlePopulation()->GetNumTrajectories());
		for (size_t i=0; i < m_vecHandles.size(); ++i)
		{
			m_vecHandles [i] = new VfaParticleHandle (this->GetRenderNode());
			this->AddControlHandle(m_vecHandles [i]);
			m_vecHandles [i]->SetSize(m_fHandleSize);
			m_vecHandles [i]->SetVisible(false);
			float fColor [4] = {0.0f, 1.0f, 0.0f, 1.0f};
			m_vecHandles [i]->SetHighlightColor(fColor);
			m_vecHandles [i]->SetWarningLength(m_fRadiusForWeightedStrategy);
		}

		BuildIndexMapping();

		//SVveParticlePopulation* pPop = m_pWidget->GetModel()->GetParticlePopulation();
		//if (pPop)
		//{
		//	std::ofstream ofVelocities ("velocities.txt");
		//	double dVelocity = 0.0;
		//	int iIdx = 0;
		//	for (int i=0; i <  m_pWidget->GetModel()->GetTimeMapper()->GetNumberOfTimeIndices(); ++i)
		//	{
		//		ofVelocities << m_pWidget->GetModel()->GetTimeMapper()->GetSimulationTime((int) i) << " ";
		//		for (int j=0; j < m_mapTimeIndicesToTrajectoryIndex.size(); ++j)
		//		{
		//			iIdx = m_mapTimeIndicesToTrajectoryIndex[j][i];
		//			dVelocity = sqrt (pPop->vecPopulation[j].vecTrajectory[iIdx].aVel[0]*pPop->vecPopulation[j].vecTrajectory[iIdx].aVel[0]+
		//							  pPop->vecPopulation[j].vecTrajectory[iIdx].aVel[1]*pPop->vecPopulation[j].vecTrajectory[iIdx].aVel[1]+
		//							  pPop->vecPopulation[j].vecTrajectory[iIdx].aVel[2]*pPop->vecPopulation[j].vecTrajectory[iIdx].aVel[2]);
		//			ofVelocities << dVelocity << " ";
		//		}
		//		ofVelocities << "\n";
		//	}
		//	ofVelocities.close();
		//}
	}

	if(msg == VfaTrajectoryDraggingModel::MSG_POSITION_ARRAY_CHANGED
		|| msg == VfaTrajectoryDraggingModel::MSG_TIME_ARRAY_CHANGED )
	{
		BuildIndexMapping();
	}

	if (msg == VfaTrajectoryDraggingModel::MSG_SELECTION_CHANGED)
	{
		if (m_pWidget->GetModel()->GetSelectedTrajectory()>=0)
		{
			// a new trajectory was selected, build kdtree on demand
			// construct kd tree for searching the trajectory
			BuildTree();

            VveParticleTrajectory* pTraj = m_pWidget->GetModel()->GetParticlePopulation()->GetTrajectory( m_pWidget->GetModel()->GetSelectedTrajectory());

			m_pWidget->GetView()->SetVisible(true);
			m_pWidget->GetView()->SelectTrajectory(m_pWidget->GetModel()->GetSelectedTrajectory());
		}
		else
		{
			// no trajectory selected ... remove focus handle and hide view
			//if (m_pFocusHandle)
			//{
			//	m_pFocusHandle->SetIsHighlighted(false);
			//	m_pFocusHandle->SetVisible(false);
			//}
			//m_pFocusHandle = NULL;
			//m_pWidget->GetView()->SetVisible(false);
		}

	}

	if (msg == VfaTrajectoryDraggingModel::MSG_VISTIME_CHANGED)
	{
		// update positions of all trajectory heads
		int iTimeIdx = m_pWidget->GetModel()->GetTimeMapper()->GetTimeIndex (m_pWidget->GetModel()->GetCurrentVisualizationTime());
		
		const int nNumHandles = static_cast<int>(m_vecHandles.size());
		for (int i=0; i<nNumHandles; ++i)
		{
			if ((iTimeIdx < 0)||(static_cast<size_t>(iTimeIdx) >= m_mapTimeIndicesToTrajectoryIndex[i].size()))
				continue;
			int iTrajectoryIdx = m_mapTimeIndicesToTrajectoryIndex[i] [iTimeIdx];
			if (iTrajectoryIdx < 0)
			{
				m_vecHandles[i]->SetEnable(false);
				continue;
			}

			if (!m_vecHandles[i]->GetEnable())
				m_vecHandles[i]->SetEnable(true);

            //oTrajectory = m_pWidget->GetModel()->GetParticlePopulation()->vecPopulation[i];
            VveParticleTrajectory* pTraj = m_pWidget->GetModel()->GetParticlePopulation()->GetTrajectory(i);
            VveParticleDataArrayBase* pPosArray = NULL;
            if( pTraj && pTraj->GetArrayByName(m_pWidget->GetModel()->GetPositionArray(), pPosArray) && pPosArray )
            {
                float aPos[3];
                pPosArray->GetElementCopy(iTrajectoryIdx, aPos);

			// if we have a selected trajectory, and time changes, change select point on trajectory, too
			if (pModel->GetSelectedTrajectory()==i)
			{
				pModel->SetSelectedPointOnTrajectory (iTrajectoryIdx);
			}
			 // we assume the trajectories and the trajectory head are in the same reference frame
			    m_vecHandles[i]->SetCenter(VistaVector3D(aPos[0], aPos[1], aPos[2]));

				m_pWidget->GetView()->ColorTrajectory(static_cast<float>(m_pWidget->GetModel()->GetCurrentVisualizationTime()));
            }

		}

		if (m_pFocusHandle)
		{
			float fPnt[3];
			VistaVector3D v3Center;
			m_pFocusHandle->GetCenter (v3Center);
			fPnt[0] =v3Center[0]+m_fInfoTextOffset[0];
			fPnt[1] =v3Center[1]+m_fInfoTextOffset[1];
			fPnt[2] =v3Center[2]+m_fInfoTextOffset[2];
			m_pInfoText->SetPosition(fPnt);

			//m_pInfoText->SetText("particle no. "+VistaAspectsConversionStuff::ConvertToString(pModel->GetSelectedTrajectory()));
		}
		

	}
	if ((msg == VfaTrajectoryDraggingModel::MSG_REFERENCE_CHANGED)||(msg == VfaTrajectoryDraggingModel::MSG_VISTIME_CHANGED))
	{
		if (m_pWidget->GetModel()->GetIsRateControlActivated() && (m_pWidget->GetModel()->GetSelectedTrajectory()>=0))
		{
			float fPnt[3];
			VistaVector3D v3Center;
			VfaParticleHandle* pHdl = m_vecHandles[m_pWidget->GetModel()->GetSelectedTrajectory()];
			pHdl->GetCenter (v3Center);
			fPnt[0] =v3Center[0]+m_fInfoTextOffset[0];
			fPnt[1] =v3Center[1]+m_fInfoTextOffset[1];
			fPnt[2] =v3Center[2]+m_fInfoTextOffset[2];
			m_pInfoText->SetPosition(fPnt);

			m_pInfoText->SetText("reference");
		}
		else
		{	
			m_pInfoText->SetText("");
		}
	}

}


/*============================================================================*/
/* END OF FILE			                                                      */
/*============================================================================*/
