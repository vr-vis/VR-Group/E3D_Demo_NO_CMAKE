/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaTrajectoryDraggingModel.h" 
#include <VistaBase/VistaStreamUtils.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>
#include <cmath>

using namespace std;


/*============================================================================*/
/* GLOBAL VARIABLES			                                                  */
/*============================================================================*/
static const std::string STR_REF_TYPENAME("VfaTrajectoryDragging");
static const int DEQUE_SIZE = 10;


/*============================================================================*/
/* WIDGET MODEL				                                                  */
/*============================================================================*/
VfaTrajectoryDraggingModel::VfaTrajectoryDraggingModel(VflVisTiming *pVisTiming)
:	VfaWidgetModelBase()
,	m_pVisTiming (pVisTiming)
,	m_pTimeMapper(NULL)
,	m_pParticlePopulation(NULL)
,	m_pParticleData(NULL)
,	m_iSelectedTrajectory(-1)
,	m_iFocusedTrajectory(-1)
,	m_iSelectedPointOnTrajectory(-1)
,	m_dCurrentVisTime(0.0f)
,	m_fSelectedParticleBaseVelocity(-1.0f)
,	m_bRateControlActivated(false)
,	m_sPositionArray(VveParticlePopulation::sPositionArrayDefault)
,	m_sScalarArray(VveParticlePopulation::sScalarArrayDefault)
,	m_sTimeArray(VveParticlePopulation::sTimeArrayDefault)
{
	this->Observe(m_pVisTiming);
}

VfaTrajectoryDraggingModel::~VfaTrajectoryDraggingModel()
{
	this->ReleaseObserveable(m_pVisTiming);
}


void VfaTrajectoryDraggingModel::SetParticleData (VveParticleDataCont* pParticleData)
{
	m_pParticlePopulation = pParticleData->GetData()->GetData();
	m_pTimeMapper = pParticleData->GetTimeMapper();
	
	m_pParticleData = pParticleData;

	m_iSelectedTrajectory = -1;
	m_iSelectedPointOnTrajectory = -1;
	Notify (MSG_POPULATION_CHANGED);
}

VveParticleDataCont *VfaTrajectoryDraggingModel::GetParticleData () const
{
	return m_pParticleData;
}

VveParticlePopulation *VfaTrajectoryDraggingModel::GetParticlePopulation () const
{
	return m_pParticlePopulation;
}
VveTimeMapper* VfaTrajectoryDraggingModel::GetTimeMapper () const
{
	return m_pTimeMapper;
}


bool VfaTrajectoryDraggingModel::SelectTrajectory (int i)
{
	if (!m_pParticlePopulation)
		return false;

	if (i < 0 || i >= m_pParticlePopulation->GetNumTrajectories())
		return false;

	// For efficiency's sake.
	if (m_iSelectedTrajectory == i)
		return true;

	m_iSelectedTrajectory = i;
	m_iSelectedPointOnTrajectory = -1;
	vstr::debugi() << "[VfaTrajectoryDraggingModel] Selected trajectory "
		<< i << endl;
	
	Notify (MSG_SELECTION_CHANGED);
	return true;
}

bool VfaTrajectoryDraggingModel::UnselectTrajectory ()
{
	if (!m_pParticlePopulation)
		return false;

	vstr::debugi() << "[VfaTrajectoryDraggingModel] Unselected trajectory "
		<< m_iSelectedTrajectory << endl;

	m_iSelectedTrajectory = -1;
	m_iSelectedPointOnTrajectory = -1;

	Notify (MSG_SELECTION_CHANGED);
	return true;
}

int VfaTrajectoryDraggingModel::GetSelectedTrajectory () const
{
	return m_iSelectedTrajectory;
}


bool VfaTrajectoryDraggingModel::FocusTrajectory(int i)
{
	if(!m_pParticlePopulation)
		return false;
	
	if(i<0 || i>=m_pParticlePopulation->GetNumTrajectories())
		return false;

	if(m_iFocusedTrajectory == i)
		return true;
	
	m_iFocusedTrajectory = i;
	vstr::debugi() << "[VfaTrajectoryDraggingModel] Focused trajectory "
		<< i << endl;

	Notify(MSG_FOCUS_CHANGED);
	return true;
}

bool VfaTrajectoryDraggingModel::UnfocusTrajectory()
{
	if(!m_pParticlePopulation)
		return false;

	vstr::debugi() << "[VfaTrajectoryDraggingModel] Unselected trajectory "
		<< m_iFocusedTrajectory << endl;
	m_iFocusedTrajectory = -1;

	Notify(MSG_FOCUS_CHANGED);
	return true;
}

int VfaTrajectoryDraggingModel::GetFocusedTrajectory() const
{
	return m_iFocusedTrajectory;
}


bool VfaTrajectoryDraggingModel::SelectPointOnTrajectory (unsigned int i)
{
	if (!m_pParticlePopulation)
		return false;

	if (m_iSelectedTrajectory<0)
		return false;

	//SVveParticleTrajectory oTrajectory = m_pParticlePopulation->vecPopulation[m_iSelectedTrajectory];
    VveParticleTrajectory* pTraj = m_pParticlePopulation->GetTrajectory(m_iSelectedTrajectory);
    VveParticleDataArrayBase *pTimeArray = NULL;

    if( pTraj && pTraj->GetArrayByName(m_sTimeArray, pTimeArray) && pTimeArray )
    {
	    if ((i<0)||(i>=pTimeArray->GetSize()))
		return false;

	// disable the animation playback upon time navigation
	if( m_pVisTiming->GetAnimationPlaying() )
		m_pVisTiming->SetAnimationPlaying(false);

	// set visualization time
	// this returns false if the given vis time is out of range
	    if (!m_pVisTiming->SetVisualizationTime(
                m_pTimeMapper->GetVisualizationTime(pTimeArray->GetElementValueAsFloat(i))))
		return false;

	// save new selected point on selected trajectory
	this->SetSelectedPointOnTrajectory (i);

	    m_deqSelectedTimeInstants.push_front(pTimeArray->GetElementValueAsFloat(m_iSelectedPointOnTrajectory));
	if (m_deqSelectedTimeInstants.size()>= DEQUE_SIZE)
		m_deqSelectedTimeInstants.pop_back();

	return true;
    }
    return false;
}

void VfaTrajectoryDraggingModel::SetSelectedPointOnTrajectory (unsigned int i)
{
	m_iSelectedPointOnTrajectory = i;
	Notify(MSG_POINT_CHANGED);
}

int VfaTrajectoryDraggingModel::GetSelectedPointOnTrajectory () const
{
	return m_iSelectedPointOnTrajectory;
}

bool VfaTrajectoryDraggingModel::GetSelectedPointOnTrajectory (float fPos[3]) const
{
	if ((m_iSelectedPointOnTrajectory==-1)||(m_iSelectedTrajectory==-1)||(!m_pParticlePopulation))
		return false;

    VveParticleTrajectory* pTraj = m_pParticlePopulation->GetTrajectory(m_iSelectedTrajectory);
    VveParticleDataArrayBase *pPosArray = NULL;

    if( pTraj && pTraj->GetArrayByName(m_sPositionArray, pPosArray) && pPosArray )
    {
        pPosArray->GetElementCopy(m_iSelectedPointOnTrajectory, fPos, 3);
	return true;
    }
    return false;
}

float VfaTrajectoryDraggingModel::GetSelectedParticleVelocity () const
{
	// @todo Think about the functions return value.
	if ((m_iSelectedPointOnTrajectory==-1)||(m_iSelectedTrajectory==-1)||(!m_pParticlePopulation))
		return -1.0f;

    VveParticleTrajectory* pTraj = m_pParticlePopulation->GetTrajectory(m_iSelectedTrajectory);
    VveParticleDataArrayBase *pScalarArray = NULL;

    if( pTraj && pTraj->GetArrayByName(m_sPositionArray, pScalarArray) && pScalarArray )
    {
        return pScalarArray->GetElementValueAsFloat(m_iSelectedPointOnTrajectory);
    }
	return -1.0f;
}

void VfaTrajectoryDraggingModel::SetCurrentVisualizationTime (double dVis)
{
	m_dCurrentVisTime = dVis; 
	Notify (MSG_VISTIME_CHANGED);
}

double VfaTrajectoryDraggingModel::GetCurrentVisualizationTime () const
{
	return m_dCurrentVisTime;
}


int VfaTrajectoryDraggingModel::GetDragDirection ()
{
	if (m_deqSelectedTimeInstants.size()==0)
		return 0;
	float fSum = 0.0f;
	std::deque<float>::iterator it = m_deqSelectedTimeInstants.begin();
	std::deque<float>::iterator itSuccessor = m_deqSelectedTimeInstants.begin();
	// this must work, as deque is not empty
	itSuccessor++;
	for (; it != m_deqSelectedTimeInstants.end(), itSuccessor != m_deqSelectedTimeInstants.end(); ++it, ++itSuccessor)
	{
		// add difference newest minus older date
		fSum += ((*it) - (*itSuccessor));
	}

	// decide direction:
	// if the sum is close to zero, the time instants have not changed in the deque => no movement, direction zero
	int iDirection = 0;

	// if the sum is larger than zero, the time instants have changed forward in time in the deque => forward movement, direction one
	if (fSum>0.0005)
		iDirection =1;
	// if the sum is smaller than zero, the time instants have changed backward in time in the deque => backward movement, direction minus one
	if (fSum<-0.0005)
		iDirection =-1;
	return iDirection;

}

void VfaTrajectoryDraggingModel::SetActivateRateControl (bool bSet)
{
	if (m_bRateControlActivated==bSet)
		return;

	m_bRateControlActivated = bSet;

	if (m_bRateControlActivated)
	{
		if (this->GetSelectedPointOnTrajectory()>-1)
		{
			m_fSelectedParticleBaseVelocity = this->GetSelectedParticleVelocity();
			m_pVisTiming->SetAnimationSpeed(1.0f);
		}
	}
	else
	{
		m_fSelectedParticleBaseVelocity = -1.0f;
		m_pVisTiming->SetAnimationSpeed(1.0f);
	}
	Notify(MSG_REFERENCE_CHANGED);
}

bool VfaTrajectoryDraggingModel::GetIsRateControlActivated () const
{
	return m_bRateControlActivated;
}


std::string VfaTrajectoryDraggingModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}



int VfaTrajectoryDraggingModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}

void VfaTrajectoryDraggingModel::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{

	if (pObserveable == m_pVisTiming)
	{
		if (msg==VflVisTiming::MSG_VISTIME_CHG)
		{
			this->SetCurrentVisualizationTime (m_pVisTiming->GetVisualizationTime());

			// if animation is playing and we have a selected particle with a valid m_fSelectedParticleBaseVelocity
			if (m_pVisTiming->GetAnimationPlaying() && m_bRateControlActivated && (this->GetSelectedPointOnTrajectory()>-1) && (m_fSelectedParticleBaseVelocity >= 0))
			{
				// get current particle velocity
				float fCurrentVel = this->GetSelectedParticleVelocity();
				if (fCurrentVel>0)
				{
					// set loop time factor to the quotient of base and current velocity
					// this ensures a "constantly" animated velocity of this selected particle
					// remark: greater than one -> faster, less than one -> slower
					m_pVisTiming->SetAnimationSpeed(m_fSelectedParticleBaseVelocity/fCurrentVel);

				}
			}
		}
		//if (m_bRateControlActivated && (msg==VflVisTiming::MSG_ANIMSTATUS_CHG))
		//{
		//	if (this->GetTimeModel()->GetAnimationPlaying() && this->GetSelectedPointOnTrajectory()>-1)
		//	{
		//		m_fSelectedParticleBaseVelocity = this->GetSelectedParticleVelocity();
		//		this->GetTimeModel()->SetLoopTimeFactor(1.0f);
		//	}
		//	else
		//	{
		//		m_fSelectedParticleBaseVelocity = -1.0f;
		//		this->GetTimeModel()->SetLoopTimeFactor(1.0f);
		//	}
		//}
	}
	
}

std::string VfaTrajectoryDraggingModel::GetPositionArray() const
{
    return m_sPositionArray;
}

std::string VfaTrajectoryDraggingModel::GetScalarArray() const
{
    return m_sScalarArray;
}

std::string VfaTrajectoryDraggingModel::GetTimeArray() const
{
    return m_sTimeArray;
}

bool VfaTrajectoryDraggingModel::SetPositionArray( std::string sPositionArray )
{
    if(compAndAssignFunc<std::string>(sPositionArray, m_sPositionArray))
    {
        Notify(MSG_POSITION_ARRAY_CHANGED);
        return true;
    }
    return false;
}

bool VfaTrajectoryDraggingModel::SetScalarArray( std::string sScalarArray )
{
    if(compAndAssignFunc<std::string>(sScalarArray, m_sScalarArray))
    {
        Notify(MSG_SCALAR_ARRAY_CHANGED);
        return true;
    }
    return false;
}

bool VfaTrajectoryDraggingModel::SetTimeArray( std::string sTimeArray )
{
    if(compAndAssignFunc<std::string>(sTimeArray, m_sTimeArray))
    {
        Notify(MSG_TIME_ARRAY_CHANGED);
        return true;
    }
    return false;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "          "                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

