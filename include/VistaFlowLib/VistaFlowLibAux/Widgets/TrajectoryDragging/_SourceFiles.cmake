

set( RelativeDir "./Widgets/TrajectoryDragging" )
set( RelativeSourceGroup "Source Files\\Widgets\\TrajectoryDragging" )

set( DirFiles
	VfaParticleHandleVis.cpp
	VfaParticleHandleVis.h
	VfaTrajectoryDraggingController.cpp
	VfaTrajectoryDraggingController.h
	VfaTrajectoryDraggingModel.cpp
	VfaTrajectoryDraggingModel.h
	VfaTrajectoryDraggingWidget.cpp
	VfaTrajectoryDraggingWidget.h
	VfaTrajectoryInfo.cpp
	VfaTrajectoryInfo.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

