/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaCircle2DVis.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaOGLExt/VistaTexture.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
#include <cstring>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaCircle2DVis                                       */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCircle2DVis::VfaCircle2DVis()
	:	m_pTexture(NULL)	
{
	m_fCenter[0] = 0.0f;
	m_fCenter[1] = 0.0f;
	m_fCenter[2] = 0.0f;

	m_fCircleRadius = 0.03f;

	m_fRotate[0] = m_fRotate[1] = m_fRotate[2] = 0;
	m_fRotate[3] = 1;
}

VfaCircle2DVis::~VfaCircle2DVis()
{}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaCircle2DVis::DrawOpaque()
{
	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_LIGHTING_BIT|GL_POLYGON_BIT|
		GL_LIGHTING_BIT |GL_LINE_BIT);
		
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);

	glPushMatrix();

	// define line color
	float fColor[3];
	this->GetProperties()->GetLineColor(fColor);
	glColor3fv(fColor);

	//if we have a texture  -> set it up now
	if(m_pTexture != NULL)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_TEXTURE_BIT);
		glEnable(GL_TEXTURE_2D);
		//draw only the texture and do not blend with quad's color
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		//bind it
		m_pTexture->Bind();
	}
	

	float dTwoPI = 2 * Vista::Pi;
    float dArcSeg = dTwoPI / 36;
    
	VistaVector3D v3Center = this->GetCenter();
	float fRadius = this->GetRadius();

	// build circle with texture
	glTranslatef (v3Center[0], v3Center[1], v3Center[2]);
	glRotatef(m_fRotate[0], m_fRotate[1], m_fRotate[2], m_fRotate[3]);

	glBegin (GL_TRIANGLE_FAN);
		for (float theta = 0; theta < dTwoPI; theta += dArcSeg) 
		{
			float x = ( sin(theta) * fRadius);
            float y = ( cos(theta) * fRadius);
		
			// we have to transform the unit circle (-1...1) to 1...0, because we need
			// the texture coordinates
           	glTexCoord2f(0.5f*sin(theta)+0.5f,0.5f*cos(theta)+0.5f); 
			glVertex2f(x, y);			
		}        
 	glEnd(); 

	//restore texture state
	if(m_pTexture != NULL)
			glPopAttrib();

	// draw circular ring if needed
	if (this->GetProperties()->GetIsSurrounded())
	{
		float fLineWidth = this->GetProperties()->GetLineWidth();
		//glLineWidth(fLineWidth);
		glBegin (GL_LINE_LOOP);
			for (float theta = 0; theta < dTwoPI; theta += dArcSeg) 
			{
				float z = ( cos(theta) * fRadius*(1.0f + fLineWidth/100));
				float w = ( sin(theta) * fRadius*(1.0f + fLineWidth/100));
				glVertex2f(z, w);
			}
		glEnd();
	}
	glPopMatrix();
	glPopAttrib();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaCircle2DVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaCircle2DVis::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetCenter                                               */
/*                                                                            */
/*============================================================================*/
bool VfaCircle2DVis::SetCenter(const VistaVector3D& v3Center)
{
	float fCenter[3];
	v3Center.GetValues(fCenter);	
	return this->SetCenter(fCenter);
}
VistaVector3D VfaCircle2DVis::GetCenter() const
{
	return VistaVector3D(m_fCenter);
}

bool VfaCircle2DVis::SetCenter(float fCenter[3])
{
	if(	m_fCenter[0] == fCenter[0] &&
		m_fCenter[1] == fCenter[1] &&
		m_fCenter[2] == fCenter[2])
	{
		return false;
	}
	memcpy(m_fCenter, fCenter, 3*sizeof(float));
	return true;
}
void VfaCircle2DVis::GetCenter(float fCenter[3]) const
{
	memcpy(fCenter, m_fCenter, 3*sizeof(float));
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRadius                                               */
/*                                                                            */
/*============================================================================*/
bool VfaCircle2DVis::SetRadius(float f)
{
	if(m_fCircleRadius == f)
		return false;

	m_fCircleRadius = f;
	return true;
}

float VfaCircle2DVis::GetRadius()const
{
	return m_fCircleRadius;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTexture                                              */
/*                                                                            */
/*============================================================================*/
void VfaCircle2DVis::SetTexture(VistaTexture *pTex)
{
	m_pTexture = pTex;
}
VistaTexture *VfaCircle2DVis::GetTexture() const
{
	return m_pTexture;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRotate                                               */
/*                                                                            */
/*============================================================================*/
void VfaCircle2DVis::SetRotate(const VistaVector3D &v3Normal)
{
	VistaVector3D vecZ(0.0,0.0,1.0);
	VistaQuaternion quaRotate(vecZ, v3Normal);
	VistaAxisAndAngle a = quaRotate.GetAxisAndAngle();

	m_fRotate[0] = a.m_fAngle*180.0f/Vista::Pi;
 	m_fRotate[1] = a.m_v3Axis[0];
	m_fRotate[2] = a.m_v3Axis[1];
	m_fRotate[3] = a.m_v3Axis[2]; 
}
void VfaCircle2DVis::GetRotate(float fRotate[4]) const
{
	fRotate[0] = m_fRotate[0];
	fRotate[1] = m_fRotate[1];
	fRotate[2] = m_fRotate[2];
	fRotate[3] = m_fRotate[3];
}

void VfaCircle2DVis::SetRotate(float angle, float x, float y, float z)
{
	m_fRotate[0] = angle;
	m_fRotate[1] = x;
	m_fRotate[2] = y;
	m_fRotate[3] = z;

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaCircle2DVis::VfaCircle2DVisProps *VfaCircle2DVis::GetProperties() const
{
	return static_cast<VfaCircle2DVis::VfaCircle2DVisProps *>
		(IVflRenderable::GetProperties());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaCircle2DVis::CreateProperties() const
{
	return new VfaCircle2DVis::VfaCircle2DVisProps();
}

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCircle2DVis::VfaCircle2DVisProps::VfaCircle2DVisProps()
{
	//blue border
	m_fColor[0] = 1.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 0.0f;

	m_fLineWidth = 1.0f;

	m_bSurrounding = true;
}
VfaCircle2DVis::VfaCircle2DVisProps::~VfaCircle2DVisProps()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetColor                                                */
/*                                                                            */
/*============================================================================*/
bool VfaCircle2DVis::VfaCircle2DVisProps::SetLineColor(const VistaColor& color)
{
	float fColor[3];
	color.GetValues(fColor);
	return this->SetLineColor(fColor);
}

VistaColor VfaCircle2DVis::VfaCircle2DVisProps::GetLineColor() const
{
	return VistaColor(m_fColor);
}

bool VfaCircle2DVis::VfaCircle2DVisProps::SetLineColor(float fColor[3])
{
	if(	fColor[0] == m_fColor[0] &&
		fColor[1] == m_fColor[1] &&
		fColor[2] == m_fColor[2])
	{
		return false;
	}

	memcpy(m_fColor, fColor, 3*sizeof(float));
	this->Notify(MSG_COLOR_CHG);
	return true;
}

void VfaCircle2DVis::VfaCircle2DVisProps::GetLineColor(float fColor[]) const
{
	memcpy(fColor, m_fColor, 3*sizeof(float));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetLineWidth                                            */
/*                                                                            */
/*============================================================================*/
bool VfaCircle2DVis::VfaCircle2DVisProps::SetLineWidth(float f)
{
	if (m_fLineWidth == f)
		return false;

	m_fLineWidth = f;
	return true;

}
float VfaCircle2DVis::VfaCircle2DVisProps::GetLineWidth() const
{
	return m_fLineWidth;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetIsSurrounded                                         */
/*                                                                            */
/*============================================================================*/
bool VfaCircle2DVis::VfaCircle2DVisProps::SetIsSurrounded(bool b)
{
	if ( m_bSurrounding == b)
		return false;

	m_bSurrounding = b;
	return true;
}
bool VfaCircle2DVis::VfaCircle2DVisProps::GetIsSurrounded() const
{
	return m_bSurrounding;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaCircle2DVis.cpp"		         					      */
/*============================================================================*/
