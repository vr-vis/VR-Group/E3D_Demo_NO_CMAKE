/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#include "VfaListMenuController.h"

#include "../Menu/VfaMenuModel.h"
#include "VfaListMenuWidget.h"
#include "../VfaProximityQuadHandle.h"
#include "../VfaQuadVis.h"
#include "../VfaLineVis.h"
#include "../Line/VfaLineModel.h"
#include "../Plane/VfaPlaneModel.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaMath/VistaIndirectXform.h>
#include <VistaBase/VistaTimer.h>
#include <VistaOGLExt/VistaTexture.h>

#include <VistaKernel/GraphicsManager/VistaGraphicsManager.h>
#include <VistaFlowLibAux/Interaction/VfaPointerManager.h>
#include <cassert>
#include <cstdio>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaListMenuController::VfaListMenuController(VfaMenuModel *pModel,
											   VflRenderNode* pRenderNode,
											   VfaPointerManager *pManagPts,
											   float fWidgetScale)
  :		VfaMenuController(pModel, pRenderNode->GetVisTiming(), pRenderNode),
		m_iGrabButton(0),
		m_iOpeningButton(1),
		m_bEnabled(true),
		m_bCloseAfterSelection(true),
		m_bStopAdjust(false),
		m_pRenderNode(pRenderNode),
		m_iLastState(MENU_INACTIVE),
		m_iState(MENU_INACTIVE),
		m_iActiveHnd(HND_UNDEFINED),
		m_pManagPts(pManagPts)
{
	// create props
	m_pCtlProps = new VfaListMenuController::VfaListMenuControllerProps(this);
	
	//attach to the model
	this->Observe(m_pModel);

	m_fNormalColor[0] = 0.55f;
	m_fNormalColor[1] = 1.0f;
	m_fNormalColor[2] = 0.5f;

	m_fHighlightColor[0] = 1.0f;
	m_fHighlightColor[1] = 0.0f;
	m_fHighlightColor[2] = 0.0f;

	// create handles
	int iNrOfUsedHandles = m_pModel->GetNumIds();
	m_vecHandle.resize(iNrOfUsedHandles);
	
	for (int i = 0; i < iNrOfUsedHandles; ++i)
	{
		m_vecHandle[i] = new VfaProximityQuadHandle(pRenderNode);
		m_vecHandle[i]->SetProximityDistance(0.1f * fWidgetScale);
		m_vecHandle[i]->SetNormalLineWidth(0.2f);
		m_vecHandle[i]->SetHighlightLineWidth(5.0f);
		m_vecHandle[i]->SetFillPlane(true);
		m_vecHandle[i]->SetLineColor(m_fNormalColor);
		m_vecHandle[i]->SetVisible(false);
		m_vecHandle[i]->SetExtent(0.0f, 0.0f);
		this->AddControlHandle(m_vecHandle[i]);
	}	

	m_pLineModel = new VfaLineModel;
	m_pLineVis = new VfaLineVis(m_pLineModel);
	m_pLineVis->SetVisible(false);
	if(m_pLineVis->Init())
		pRenderNode->AddRenderable(m_pLineVis);
	m_pLineVis->GetProperties()->SetWidth(5.0f);
	m_pLineVis->GetProperties()->SetColor(m_fNormalColor);
}

VfaListMenuController::~VfaListMenuController()
{
	this->ReleaseObserveable(m_pModel, IVistaObserveable::TICKET_NONE);
	int iNumHandles = m_pModel->GetNumIds();
	for (int i = 0; i < iNumHandles; ++i)
			delete m_vecHandle[i];

	delete m_pCtlProps;	
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*  NAME      :   Set/GetIsEnabled                                            */
/*============================================================================*/
void VfaListMenuController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	
	if(!b)
	{
		m_iLastState = m_iState;
		this->SetMenuState(MENU_DISABLED);
	}
	else
		this->SetMenuState(m_iLastState); // return to the state before being switched off
}
bool VfaListMenuController::GetIsEnabled() const
{
	return m_bEnabled;
}
/*============================================================================*/
/*  NAME      :   Get/IsClosingAfterSelection                                 */
/*============================================================================*/
void VfaListMenuController::IsClosingAfterSelection(bool b)
{
	m_bCloseAfterSelection = b;
}
bool VfaListMenuController::GetClosingAfterSelection()
{
	return m_bCloseAfterSelection;
}

/*============================================================================*/
/*  NAME      :   OnFocus			                                          */
/*============================================================================*/
void VfaListMenuController::OnFocus(IVfaControlHandle* pHandle)
{
	// ignore focus and unfocus, if we are currently inactive
	if(m_iState == MENU_INACTIVE)
	   return;


	//when we get the focus for some handle 
	//we must not have an active handle before (i.e. this should be ensured by
	//::OnUnfocus
	if(m_iActiveHnd != HND_UNDEFINED)
		return;

	int iNrOfHandles = m_pModel->GetNumIds();
	for (int i = 0; i<  iNrOfHandles; ++i)
	{
		if(pHandle == m_vecHandle[i])
		{
			m_iActiveHnd = i;
			m_vecHandle[i]->SetIsHighlighted(true);
			break;
		}
	}

}

/*============================================================================*/
/*  NAME      :   OnUnfocus			                                          */
/*============================================================================*/
void VfaListMenuController::OnUnfocus()
{ 
		if(m_iActiveHnd < HND_UNDEFINED)
		return;

	m_vecHandle[m_iActiveHnd]->SetIsHighlighted(false);
	m_iActiveHnd = HND_UNDEFINED;
}

/*============================================================================*/
/*  NAME      :   OnTouch			                                          */
/*============================================================================*/
void VfaListMenuController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{}

/*============================================================================*/
/*  NAME      :   OnUntouch			                                          */
/*============================================================================*/
void VfaListMenuController::OnUntouch()
{}
/*============================================================================*/
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*============================================================================*/
void VfaListMenuController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	//we are only interested in our buttons
	if(iSlot != m_iGrabButton && iSlot != m_iOpeningButton)
		return;

	// reaction on button release --> menu should be fix henceforth
	if(!bSet)
	{
		if(iSlot == m_iOpeningButton)
			this->StartEvent(MSG_MOVE_BT_RELEASE);
		m_bStopAdjust = true;
		return;
	}
	
	//save state
	m_iLastState = m_iState;

	if (iSlot == m_iGrabButton)
	{
		if(m_iLastState != MENU_ACTIVE)
			return;

		if(m_iActiveHnd != HND_UNDEFINED)
		{
			this->SetMenuState(MENU_TARGET_TOUCHED);
			m_vecHandle[m_iActiveHnd]->SetLineColor(m_fHighlightColor);
			return;
		}
	}
	else if (iSlot == m_iOpeningButton)
	{
		if(m_iLastState == MENU_INACTIVE)
		{	
			//m_pManagPts->SetPointerVisible(false);
			m_bStopAdjust = false;
			this->SetMenuState(MENU_CHANGING_SIZE);
			m_pModel->SetIsOpen(false);
			this->StartEvent(MSG_START_PROCESS);
		}
		else if(m_iLastState == MENU_ACTIVE)
		{
			this->DeleteVisibleFeedback();
			this->StartEvent(MSG_MOVE_BT_PRESS);
			m_pModel->SetIsOpen(true);
			m_bStopAdjust = false;
		}
	}
}

/*============================================================================*/
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*============================================================================*/
void VfaListMenuController::OnSensorSlotUpdate(int iSlot, 
				const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	VistaVector3D v3;
	m_vecHandle[0]->GetNormal(v3);

	if(iSlot != VfaApplicationContextObject::SLOT_CURSOR_VIS)
			return;

	// save sensor data and update model if necessary
	if(!m_bStopAdjust || m_iState == MENU_CHANGING_SIZE)
	{
		m_oLastSensorFrame.qOrientation = oFrameData.qOrientation;
		m_oLastSensorFrame.v3Position = oFrameData.v3Position;
		AdjustItems();
	}


	// when we are currently "idle" -> nothing to do...
	if(m_iState == MENU_INACTIVE || m_iState == MENU_CHANGING_SIZE || m_iState == MENU_ACTIVE)
		return;
		

	// target is touched
	if(m_iState == MENU_TARGET_TOUCHED)
	{
		m_pModel->SetActiveId(m_iActiveHnd);
		this->SetMenuState(MENU_ACTIVE); // return to an idle state
		this->StartEvent(MSG_ACK_TIME); // Ack-Time --> Time to wait before Menu is closed (if this is wished)
		return;
	}

}




/*============================================================================*/
/*  NAME      :   DeleteVisibleFeedback  	                                  */
/*============================================================================*/
void VfaListMenuController::DeleteVisibleFeedback()
{
	int iNrItems = m_pModel->GetNumIds();
	for (int i = 0; i < iNrItems; i++)
		m_vecHandle[i]->SetIsHighlighted(false); // reset line color and width
	
	// all items should get back their normal size
	this->AdjustExtend(iNrItems);
}

/*============================================================================*/
/*  NAME      :   CloseMenuAfterSelection	                                  */
/*============================================================================*/
void VfaListMenuController::CloseMenuAfterSelection()
{
	int iActive = m_pModel->GetActiveId();
	m_vecHandle[iActive]->SetLineColor(m_fNormalColor);

	if(iActive == 0)
		return;

	if(!m_bCloseAfterSelection)
		return;

	this->Close();
}

/*============================================================================*/
/*  NAME      :   Close					                                      */
/*============================================================================*/
void VfaListMenuController::Close()
{
	m_bStopAdjust = true;
	m_pModel->SetIsOpen(true);

	this->SetMenuState(MENU_CHANGING_SIZE);
	this->StartEvent(MSG_START_PROCESS);
	
	m_pManagPts->SetPointerVisible(true);
}

/*============================================================================*/
/*  NAME      :   GetSensorMask			                                      */
/*============================================================================*/
int VfaListMenuController::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_CURSOR_VIS);
}

/*============================================================================*/
/*  NAME      :   GetCommandMask			                                  */
/*============================================================================*/
int VfaListMenuController::GetCommandMask() const
{
	return (1 << m_iGrabButton) | (1 << m_iOpeningButton);
}

/*============================================================================*/
/*  NAME      :   GetTimeUpdate				                                  */
/*============================================================================*/
bool VfaListMenuController::GetTimeUpdate() const
{
	return true;
}

/*============================================================================*/
/*  NAME      :   UpdateSize	  										      */
/*============================================================================*/
void VfaListMenuController::UpdateSize(double dDeltaTime)
{
	float fWidth = this->GetFinalWidth();
	float fHeight = this->GetFinalHeight();

	// test whether opening/closing process is realy an animation
	if (this->GetTimeToOpenCloseMenu() == 0.0f)
	{
		 if(m_iState == MENU_CHANGING_SIZE)
		 {
				// menu size should grow
				if(!m_pModel->GetIsOpen())
				{
					this->SetCurrentWidth(fWidth);
					this->SetCurrentHeight(fHeight);
					m_pModel->SetMenuOpen();
				}
				else // menu size should decrease
				{
					this->SetCurrentWidth(0.0f);
					this->SetCurrentHeight(0.0f);
					m_pModel->SetMenuClosed();
				}
		 }
		return;	
	}


	//timefunction
	float fCoefficient = (float) dDeltaTime/this->GetTimeToOpenCloseMenu();
	
	// time is mapped to a scalar from 0 to 1
	if(fCoefficient >= 0.0f && fCoefficient <= 1.0f)
	{
		m_bChangeByLastPass = true;

		// menu size should grow
		if(!m_pModel->GetIsOpen())
		{
			this->SetCurrentWidth(fCoefficient * fWidth);
			this->SetCurrentHeight(fCoefficient * fHeight);
			return;
		}
		else // menu size should decrease
		{
			this->SetCurrentWidth(fWidth - (fCoefficient * fWidth));
			this->SetCurrentHeight(fHeight - (fCoefficient * fHeight));
			return;
		}
	}
	else if(m_bChangeByLastPass)
	{
		m_bChangeByLastPass = false;

		if(m_pModel->GetIsOpen())
		{
			this->SetCurrentWidth(0.0f);
			this->SetCurrentHeight(0.0f);
			m_pModel->SetMenuClosed();
		}
		else
			m_pModel->SetMenuOpen();
	}
}

/*============================================================================*/
/*  NAME      :   ObserverUpdate                                              */
/*============================================================================*/
void VfaListMenuController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if (msg == VfaMenuModel::MSG_TIME_TO_CLOSE_AFTER_SELECTION)
		CloseMenuAfterSelection();
	else if(msg == VfaMenuModel::MSG_CLOSE)
		Close();
	else if(msg == VfaMenuModel::MSG_MENU_CLOSED)
	{
		this->AdjustItems(); // to be sure, items size is zero
		this->SetMenuState(MENU_INACTIVE);
	}
	else if(msg == VfaMenuModel::MSG_MENU_OPEN)
	{
		this->SetMenuState(MENU_ACTIVE);
	}
	
	// we are only interested in changes
	if( msg != VfaMenuModel::MSG_NUMBER_TOKEN_CHANGE)
		return;	

	this->AdjustItems();
}

/*============================================================================*/
/*  NAME      :   OpenMenu		                                               */
/*============================================================================*/
void VfaListMenuController::OpenMenu ()
{
	m_bStopAdjust = false;
	m_pModel->SetIsOpen(false);

	this->SetMenuState(MENU_CHANGING_SIZE);
	this->StartEvent(MSG_START_PROCESS);
}

/*============================================================================*/
/*  NAME      :   SetGrabButton                                               */
/*============================================================================*/
void VfaListMenuController::SetGrabButton (int iSlot)
{
	m_iGrabButton = iSlot;
}

/*============================================================================*/
/*  NAME      :   SetOpenButton                                               */
/*============================================================================*/
void VfaListMenuController::SetOpenButton (int iSlot)
{
	m_iOpeningButton = iSlot;
}

/*============================================================================*/
/*  NAME      :   GetProperties                                               */
/*============================================================================*/
VfaListMenuController::VfaListMenuControllerProps* VfaListMenuController::GetProperties() const
{
	return m_pCtlProps;
}


/*============================================================================*/
/*  NAME      :   Set/GetFinalSize	                                          */
/*============================================================================*/
bool VfaListMenuController::SetFinalSize(float fWidth, float fHeight)
{
	return SetFinalWidth(fWidth) && SetFinalHeight(fHeight);
}
void VfaListMenuController::GetFinalSize(float &fWidth, float &fHeight) const
{
	fWidth = m_fFinalWidth;
	fHeight = m_fFinalHeight;
}

/*============================================================================*/
/*  NAME      :   Set/GetCurrentSize	                                      */
/*============================================================================*/
bool VfaListMenuController::SetCurrentSize(float fWidth, float fHeight)
{
	return SetCurrentWidth(fWidth) && SetCurrentHeight(fHeight);
}
void VfaListMenuController::GetCurrentSize(float &fWidth, float &fHeight) const
{
	fWidth = m_fCurrentWidth;
	fHeight = m_fCurrentHeight;
}

/*============================================================================*/
/*  NAME      :   Set/GetFinalWidth	                                          */
/*============================================================================*/
bool VfaListMenuController::SetFinalWidth(float fWidth)
{
		if(m_fFinalWidth == fWidth)
			return false;

		m_fFinalWidth = fWidth;
		return true;
}
float VfaListMenuController::GetFinalWidth() const
{
	return m_fFinalWidth;
}

/*============================================================================*/
/*  NAME      :   Set/GetFinalHeight	                                          */
/*============================================================================*/
bool VfaListMenuController::SetFinalHeight(float fHeight)
{
		if(m_fFinalHeight == fHeight)
			return false;

		m_fFinalHeight = fHeight;
		return true;
}
float VfaListMenuController::GetFinalHeight() const
{
	return m_fFinalHeight;
}

/*============================================================================*/
/*  NAME      :   Set/GetCurrentWidth	                                          */
/*============================================================================*/
bool VfaListMenuController::SetCurrentWidth(float fWidth)
{
		if(m_fCurrentWidth == fWidth)
			return false;

		m_fCurrentWidth = fWidth;
		return true;
}
float VfaListMenuController::GetCurrentWidth() const
{
	return m_fCurrentWidth;
}

/*============================================================================*/
/*  NAME      :   Set/GetCurrentHeight	                                          */
/*============================================================================*/
bool VfaListMenuController::SetCurrentHeight(float fHeight)
{
		if(m_fCurrentHeight == fHeight)
			return false;

		m_fCurrentHeight = fHeight;
		return true;
}
float VfaListMenuController::GetCurrentHeight() const
{
	return m_fCurrentHeight;
}


/*============================================================================*/
/*  NAME      :   AdjustQuadHandles                                           */
/*============================================================================*/
void VfaListMenuController::AdjustItems()
{
	// we only need to adjust when values changed or when we see the menu
	if(m_iState == MENU_INACTIVE)
		return; 
	if (m_bStopAdjust && m_iState == MENU_ACTIVE)
		return;

	if (!m_vecHandle[0]->GetVisible())
	{
		for (int i = 0; i < m_pModel->GetNumIds(); ++i)
				m_vecHandle[i]->SetVisible(true);
	}
	
	// now adjust
	int iNrOfHandles = m_pModel->GetNumIds();

	this->AdjustExtend(iNrOfHandles);
	this->AdjustTranslation(iNrOfHandles);
	this->AdjustRotation(iNrOfHandles);
}

/*============================================================================*/
/*  NAME      :   AdjustExtend		                                          */
/*============================================================================*/
void VfaListMenuController::AdjustExtend(int iNrOfHandles)
{
	float fWidth = this->GetCurrentWidth();
	// compute the height of one quad (condition: all quads should have the same size)
	float fHeight = this->GetCurrentHeight()/iNrOfHandles;

	for(int i = 0; i < iNrOfHandles; ++i)
		m_vecHandle[i]->SetExtent(fWidth, fHeight);
}
/*============================================================================*/
/*  NAME      :   AdjustRotation	                                          */
/*============================================================================*/
void VfaListMenuController::AdjustRotation(int iNrOfHandles)
{
	//VistaVector3D vec = m_oLastSensorFrame.qOrientation.GetDirection();
	//vec.Normalize(); //it is assumed that the normal is already normalized

	// to be able to get inverse direction
	VistaVector3D vec(0,1,0);
	VistaQuaternion qRot = VistaQuaternion(VistaAxisAndAngle
							(m_oLastSensorFrame.qOrientation.Rotate(vec), Vista::DegToRad(-180)));

	VistaQuaternion quat = qRot * m_oLastSensorFrame.qOrientation;


	for (int i = 0; i < iNrOfHandles; ++i)
		m_vecHandle[i]->SetRotation(quat);
		//m_vecHandle[i]->SetNormal(-vec);	
}
/*============================================================================*/
/*  NAME      :   AdjustTranslation                                           */
/*============================================================================*/
void VfaListMenuController::AdjustTranslation(int iNrOfHandles)
{
	//the translation of the quad is the center, so compute it
	float fHalfHeight = this->GetCurrentHeight()/(iNrOfHandles*2.0f);
	float fHalfWidth = this->GetCurrentWidth()*0.5f;

 //
 //    _______     _
 //  �|		  |    |  fHalfHeight
 //   |   +	  |	   -
 //   |_______|				� : startpoint -> here your input device is located when open the menu
 //   |	      |				+ : Center of quad
 //   |   +   |
 //   |_______|
 //   |  ...  |


	// compute center of first quad
	VistaVector3D v3Pos;
	v3Pos[0] = fHalfWidth;
	v3Pos[1] = fHalfHeight;
	v3Pos[2] = 0;
	v3Pos[3] = 0; // because we want to rotate it later
	v3Pos += m_oLastSensorFrame.v3Position;
	//v3Pos = m_oLastSensorFrame.qOrientation.Rotate(v3Pos) + m_oLastSensorFrame.v3Position;
	m_vecHandle[0]->SetPoint(v3Pos);

	// now get two corners lying upon each other to compute the direction
	VistaVector3D v3Corner1, v3Corner2;
	m_vecHandle[0]->GetCorner(VfaPlaneModel::CORNER_LT, v3Corner1);
	m_vecHandle[0]->GetCorner(VfaPlaneModel::CORNER_LB, v3Corner2);
	VistaVector3D v3Dir = (v3Corner1-v3Corner2).GetNormalized();
	
	// compute all missing positions
	for(int i=0, j=2; i < iNrOfHandles; ++i, j+=2)
	{
		VistaVector3D v3QuadCenter = v3Pos - fHalfHeight*j*v3Dir;
		m_vecHandle[i]->SetPoint(v3QuadCenter);
	}
}


/*============================================================================*/
/*  NAME      :   Set/GetTextureForId		                                  */
/*============================================================================*/
bool VfaListMenuController::SetTextureForId(int id, VistaTexture *pTex)
{
	if (id < 0 || id >= m_pModel->GetNumIds())
		return false;

	m_vecHandle[id]->SetTexture(pTex);

	return  true;
}
VistaTexture * VfaListMenuController::GetTextureForId(int id) const
{
	if (id < 0 || id >= m_pModel->GetNumIds())
		return NULL;
	
	return m_vecHandle[id]->GetTexture();
}

/*============================================================================*/
/*  NAME    :    Set/GetVisEnabled                                            */
/*============================================================================*/
void VfaListMenuController::SetVisEnabled(bool b)
{
	if(b == this->GetVisEnabled())
		return;

	int iNrOfHandles = m_pModel->GetNumIds();
	
	for (int i = 0; i <  iNrOfHandles; i++)
		m_vecHandle[i]->SetVisible(b);
}
bool VfaListMenuController::GetVisEnabled() const
{
	return	m_vecHandle[0]->GetVisible();
}

/*============================================================================*/
/*  NAME    :    SetRulerState                                                */
/*============================================================================*/
void VfaListMenuController::SetMenuState(int iNewState)
{
	int iOldState = m_iState;
	m_iState = iNewState;
	this->PrintState(iOldState, iNewState);
}
/*============================================================================*/
/*  NAME    :    PrintState                                                   */
/*============================================================================*/
void VfaListMenuController::PrintState(int iOldState, int iCurrentState) const
{
	std::string strOldState = "UNKNOWN";
	std::string strNewState = "UNKNOWN";
	switch(iOldState)
	{
		case MENU_INACTIVE: strOldState = "INACTIVE"; break;
		case MENU_ACTIVE: strOldState = "ACTIVE"; break;
		case MENU_CHANGING_SIZE: strOldState = "CHANGING SIZE"; break;
		case MENU_TARGET_TOUCHED: strOldState = "TARGET_TOUCHED"; break;
	}
	switch(iCurrentState)	
	{
		case MENU_INACTIVE: strNewState = "INACTIVE"; break;
		case MENU_ACTIVE: strNewState = "ACTIVE"; break;
		case MENU_CHANGING_SIZE: strNewState = "CHANGING SIZE"; break;
		case MENU_TARGET_TOUCHED: strNewState = "TARGET_TOUCHED"; break;
	}
	vstr::outi()<<"[VfaListMenuController] MenuState change : "<<strOldState.c_str()<<" -> "<<strNewState.c_str()<<endl;
}


/*============================================================================*/
/*  IMPLEMENTATION      VfaListMenuControllerProps                        */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaListMenuController::VfaListMenuControllerProps");

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaListMenuController::VfaListMenuControllerProps::VfaListMenuControllerProps(VfaListMenuController *pMenuCtrl)
:	m_pMenuCtrl(pMenuCtrl)
{}

VfaListMenuController::VfaListMenuControllerProps::~VfaListMenuControllerProps()
{}

/*============================================================================*/
/*  NAME      :   GetReflectionableType                                       */
/*============================================================================*/
std::string VfaListMenuController::VfaListMenuControllerProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaListMenuController::VfaListMenuControllerProps::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}


/*============================================================================*/
/*  END OF FILE																  */
/*============================================================================*/

