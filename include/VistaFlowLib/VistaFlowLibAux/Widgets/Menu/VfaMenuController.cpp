/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES																  */
/*============================================================================*/
#include "VfaMenuController.h"

#include "VfaMenuModel.h"
#include "VfaMenuWidget.h"
#include "../VfaProximityQuadHandle.h"
#include "../Plane/VfaPlaneModel.h"
#include "../Line/VfaLineModel.h"
#include "../VfaLineVis.h"
#include "../VfaQuadVis.h"
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>
#include <VistaFlowLibAux/Widgets/VfaCircle2DVis.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaMath/VistaIndirectXform.h>
#include <VistaBase/VistaTimer.h>
#include <VistaOGLExt/VistaTexture.h>

#include <VistaKernel/GraphicsManager/VistaGraphicsManager.h>
#include <VistaFlowLibAux/Interaction/VfaPointerManager.h>
#include <cassert>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaMenuController::VfaMenuController(VfaMenuModel *pModel,
									   VflVisTiming *pVisTime,
									   VflRenderNode *pRenderNode)
:	IVfaWidgetController(pRenderNode),
	m_pModel(pModel),
    m_bChangeByLastPass(false),
    m_pVisTime(pVisTime)
{
	m_fStartTime = 0;
	m_fAckTime = 0;
}

VfaMenuController::~VfaMenuController()
{
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void VfaMenuController::OnTimeSlotUpdate(const double dTime)
{
	double fTime = dTime;//m_pVisTime->GetCurrentClock(); // get current time
	UpdateSize(fTime - m_fStartTime); // delta t
	ControllAckTime(fTime - m_fAckTime);
}

/*============================================================================*/
/*  NAME      :   ControllAckTime  										      */
/*============================================================================*/
void VfaMenuController::ControllAckTime(double fDeltaTime)
{
	// wait some seconds before the menu will be closed after selection
	
	double fCoefficient = fDeltaTime/this->GetAckTimer();
	
	if(fCoefficient >= 3.0f && fCoefficient <= 4.0f)
	{
		if (m_pModel->GetIsOpen())
		{
			m_pModel->CloseMenuAfterSelection();
			m_fAckTime = 0;
		}
	}
}

/*============================================================================*/
/*  NAME      :   StartEvent	  										      */
/*============================================================================*/
bool VfaMenuController::StartEvent(int iMsg)
{
	double f = 0;

	switch(iMsg)
	{
	case MSG_START_PROCESS:
		m_fStartTime = m_pVisTime->GetCurrentClock();
		break;
	case MSG_ACK_TIME:
		m_fAckTime = m_pVisTime->GetCurrentClock();
		break;
	case MSG_MOVE_BT_PRESS:
		m_fMoveTime = m_pVisTime->GetCurrentClock();
		break;
	case MSG_MOVE_BT_RELEASE:
		f = m_pVisTime->GetCurrentClock() - m_fMoveTime;
		if( f < 0.5f)
		{
			m_fMoveTime = 0;
			m_pModel->CloseMenu();
		}
		break;
	default:
		return false;
	}
	return true;
}

/*============================================================================*/
/*  NAME      :   Get/SetTimeToOpenCloseMenu								  */
/*============================================================================*/
bool VfaMenuController::SetTimeToOpenCloseMenu(float fTime)
{
	// Attention:
	// zero is possible, perhaps someone wants to use this menu 
	// without opening/closing animation
	if (fTime < 0 || m_fOpeningClosingTime == fTime)
		return false;

	m_fOpeningClosingTime = fTime;
	return true;
}
float VfaMenuController::GetTimeToOpenCloseMenu() const
{
	return m_fOpeningClosingTime;
}

/*============================================================================*/
/*  NAME      :   Get/SetAckTimer										      */
/*============================================================================*/
void VfaMenuController::SetAckTimer(float fTime)
{
	m_fFinalAckTime = fTime;
}
float VfaMenuController::GetAckTimer() const
{
	return m_fFinalAckTime;
}

/*============================================================================*/
/*  NAME      :   SetChangeInformation									      */
/*============================================================================*/
void VfaMenuController::SetChangeInformation()
{
	m_pModel->SomethingChanged();
}

/*============================================================================*/
/*  END OF FILE "VfaMenuController.cpp"								          */
/*============================================================================*/
