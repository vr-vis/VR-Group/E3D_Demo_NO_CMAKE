/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaRingAdapter.h"
#include "VfaContextMenuIconAdapter.h"
#include "../VfaCircle2DVis.h"

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaRingAdapter::VfaRingAdapter(VfaCircle2DVis *pVis,
								 VflRenderNode *pRenderNode)
	: IVfaContextMenuIconAdapter(pVis),
	  m_pRenderNode(pRenderNode)
{
	m_pVis = pVis;
	if (!m_pVis->Init())
		return;

	m_pVis->GetProperties()->SetLineColor(m_fNormalColor);
	m_pVis->GetProperties()->SetLineWidth(m_fNormalLineWidth);
}
VfaRingAdapter::~VfaRingAdapter()
{}

/*============================================================================*/
/*  IMPLEMENTATION						                                      */
/*============================================================================*/
/*============================================================================*/
/*  NAME: Clone							                                      */
/*============================================================================*/
IVfaContextMenuIconAdapter* VfaRingAdapter::Clone()
{
	VfaRingAdapter *pRingAdapter = new VfaRingAdapter(new VfaCircle2DVis,
		m_pRenderNode);
	pRingAdapter->SetHighlightColor(this->GetHighlightColor());
	pRingAdapter->SetNormalColor(this->GetNormalColor());
	pRingAdapter->SetNormalLineWidth(this->GetNormalLineWidth());
	pRingAdapter->SetHighlightLineWidth(this->GetHighlightLineWidth());
	m_pRenderNode->AddRenderable(pRingAdapter->GetVis());
	return pRingAdapter;
}

/*============================================================================*/
/*  NAME: Set/GetPosition							                          */
/*============================================================================*/
bool VfaRingAdapter::SetPosition (const VistaVector3D &v3Pos)
{
	return m_pVis->SetCenter(v3Pos);
}
void VfaRingAdapter::GetPosition (VistaVector3D &v3Pos) const
{
	v3Pos = m_pVis->GetCenter();
}
bool VfaRingAdapter::SetPosition (float fC[3])
{
	return m_pVis->SetCenter(fC);
}
void VfaRingAdapter::GetPosition (float fC[3]) const
{
	m_pVis->GetCenter(fC);
}

/*============================================================================*/
/*  NAME: Set/GetSize								                          */
/*============================================================================*/
bool VfaRingAdapter::SetSize (float fSize)
{
	return m_pVis->SetRadius(fSize/2);
}
float VfaRingAdapter::GetSize () const
{
	return m_pVis->GetRadius()*2;
}

/*============================================================================*/
/*  NAME: Set/GetOrientation						                          */
/*============================================================================*/
bool VfaRingAdapter::SetOrientation (const VistaVector3D &v3Normal)
{
	m_pVis->SetRotate(v3Normal);
	return true;
}
void VfaRingAdapter::GetOrientation (VistaVector3D &v3Normal) const
{
	float fC[3];
	this->GetOrientation(fC);
	v3Normal = VistaVector3D(fC);
}
bool VfaRingAdapter::SetOrientation (float fC[3])
{
	VistaVector3D v3Ori (fC);
	return this->SetOrientation(v3Ori);
}
void VfaRingAdapter::GetOrientation (float fC[3]) const
{
	m_pVis->GetRotate(fC);
}

/*============================================================================*/
/*  NAME: Set/GetRotation						                              */
/*============================================================================*/
bool VfaRingAdapter::SetRotation(const VistaQuaternion &qRotation)
{
	VistaAxisAndAngle axisAndAngle = qRotation.GetAxisAndAngle();
	
	m_pVis->SetRotate(Vista::RadToDeg(axisAndAngle.m_fAngle), axisAndAngle.m_v3Axis[0],
								axisAndAngle.m_v3Axis[1],axisAndAngle.m_v3Axis[2]);
	return true;
}
void VfaRingAdapter::GetRotation(VistaQuaternion &qRot) const
{
	float fV[4];
	m_pVis->GetRotate(fV);
	qRot = VistaQuaternion(VistaAxisAndAngle(VistaVector3D(fV[1], fV[2], fV[3]),
								Vista::DegToRad(fV[0])));
}

/*============================================================================*/
/*  NAME: Set/GetIsHighlighted						                          */
/*============================================================================*/
bool VfaRingAdapter::SetIsHighlighted (bool bHighlight)
{
	if(bHighlight)
	{
		m_pVis->GetProperties()->SetLineColor(m_fHighlightColor);
		m_pVis->GetProperties()->SetLineWidth(m_fHighlightLineWidth);
	}
	else
	{
		m_pVis->GetProperties()->SetLineColor(m_fNormalColor);
		m_pVis->GetProperties()->SetLineWidth(m_fNormalLineWidth);
	}
	return true;
}
bool VfaRingAdapter::GetIsHighlighted () const
{
	if (m_pVis->GetProperties()->GetLineWidth() == m_fNormalLineWidth)
		return false;
	else 
		return true;
}



/*============================================================================*/
/*  NAME: Set/GetTexture							                          */
/*============================================================================*/
bool VfaRingAdapter::SetTexture(VistaTexture *pTex)
{
	m_pVis->SetTexture(pTex);
	return true;
}
VistaTexture* VfaRingAdapter::GetTexture() const
{
	return m_pVis->GetTexture();
}

/*============================================================================*/
/*  END OF FILE "VfaRingAdapter.cpp"										  */
/*============================================================================*/
