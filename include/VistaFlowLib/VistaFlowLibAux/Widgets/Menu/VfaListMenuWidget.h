/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFALISTEMENUWIDGET_H
#define _VFALISTEMENUWIDGET_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidget.h"
#include "../Menu/VfaMenuModel.h"
#include "../Menu/VfaMenuWidget.h"
#include "VfaListMenuController.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLibAux/Interaction/VfaPointerManager.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaMenuModel;
class VfaListMenuController;
class VfaPointerManager;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* A ListMenuWigdet is a Widget which represents a menu with n items in a list.
* It is used to choose s.th. i.e. a function during runtime by clicking. Each item represents a function,
* which is specified by the user as Callback. 
*
* You can open&close the menu whenever you want to, to choose a special item.
* You also can move the menu around in 3D space by dragging.
*
* During runtime you can decide whether the menu should stay open after choosing an item or not.
* Therefor a default-item is implemented (VfaCallbackFixingMenu).
*
* To use this menu you need n *.tga pictures for your own functions. Additional please check out 
* "fixed_sized.tga" and "notFixed_sized.tga" (see: SimpleMenuDemo/TGA) - or create your own symbols.
*/
class VISTAFLOWLIBAUXAPI VfaListMenuWidget : public VfaMenuWidget
{
public:
	VfaListMenuWidget(VflRenderNode *pRenderNode,
							VfaPointerManager *pManagPts,
							int iNrOfIds, float fWidgetScale,  bool bUnFix = true);
	virtual ~VfaListMenuWidget();

	/**
	 * Only react on InteractionEvents if enabled. Visibility of the widget is
	 * independent of this.
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;

	VfaListMenuController* GetController() const;

	void SetTextureForId(int id, VistaTexture *pTex);
	virtual void IsClosingAfterSelection( bool b);
	virtual bool GetClosingAfterSelection();

protected:

private:
	VfaListMenuController	    *m_pMenuController;
	bool						m_bEnabled;
	bool						m_bVisible;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFALISTEMENUWIDGET_H
