/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaCallbackNavigationFactor.h"
#include <VistaFlowLibAux/Widgets/Menu/VfaContextMenuController.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCallbackNavigationFactor::VfaCallbackNavigationFactor(VfaContextMenuController *pCtr, 
														   int iMode, float fDiff)
	: m_pCtr(pCtr),
	  m_iMode(iMode),
	  m_fDiff(fDiff)
{}

VfaCallbackNavigationFactor::~VfaCallbackNavigationFactor()
{}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/* NAME: Do		                                                              */
/*============================================================================*/
bool VfaCallbackNavigationFactor::Do()
{
	float fCurrentFactor = m_pCtr->GetFactorInputDeviceRotation();
	float fNewFactor = 0.0f;

	switch(m_iMode)
	{
	case 0:
		fNewFactor = fCurrentFactor + m_fDiff;
		break;
	case 1: 
		if(fCurrentFactor - m_fDiff < 1.0f)
			fNewFactor = 1.0f;
		else
			fNewFactor = fCurrentFactor - m_fDiff;
		break;
	default:
		return false;
	}

#ifdef TESTOUTPUT
	vstr::debugi() << "[VfaCallbackNavigationFactor] fNewFactor: "<<fNewFactor << endl;
#endif

	m_pCtr->SetFactorInputDeviceRotation(fNewFactor);
	return true;
}


/*============================================================================*/
/*  END OF FILE "VfaCallbackNavigationFactor.cpp"							  */
/*============================================================================*/
