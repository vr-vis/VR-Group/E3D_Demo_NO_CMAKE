/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAQUADADAPTER_H
#define _VFAQUADADAPTER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include "VfaContextMenuIconAdapter.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaQuadVis;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*	the menu icons form will be quadratic
*/
class VISTAFLOWLIBAUXAPI VfaQuadAdapter : public IVfaContextMenuIconAdapter
{
public:
	VfaQuadAdapter(VfaQuadVis *pVis, VflRenderNode *pRenderNode);
	virtual ~VfaQuadAdapter();

	virtual IVfaContextMenuIconAdapter* Clone();

	virtual bool SetPosition (const VistaVector3D &v3Pos);
	virtual void GetPosition (VistaVector3D &v3Pos) const;
	virtual bool SetPosition (float fC[3]);
	virtual void GetPosition (float fC[3]) const;

	virtual bool SetSize (float fSize);
	virtual float GetSize () const;

	virtual bool SetOrientation (const VistaVector3D &v3Normal);
	virtual void GetOrientation (VistaVector3D &v3Normal) const;
	virtual bool SetOrientation (float fC[3]);
	virtual void GetOrientation (float fC[3]) const;

	/** to avoid set the rotation accordingly standard normal is in z-direction */
	virtual bool SetRotation(const VistaQuaternion &qRotation);
	virtual void GetRotation(VistaQuaternion &qRot) const;

	virtual bool SetIsHighlighted (bool bHighlight);
	virtual bool GetIsHighlighted () const;

	virtual bool SetTexture(VistaTexture *pTex);
	virtual VistaTexture *GetTexture() const;

protected:

private:
	VfaQuadVis		*m_pVis;
	VflRenderNode	*m_pRenderNode;
	
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAQUADADAPTER_H
