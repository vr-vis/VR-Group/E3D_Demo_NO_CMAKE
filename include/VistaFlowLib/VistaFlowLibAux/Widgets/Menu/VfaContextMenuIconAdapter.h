/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFACONTEXTMENUICONADAPTER_H
#define _VFACONTEXTMENUICONADAPTER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidget.h"

#include <VistaFlowLib/Visualization/VflVisController.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaOGLExt/VistaTexture.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisController;
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*	Interface used to easily change the appearance of the menu icons.
*	Just write your own subclass or use one of the existing ones.
*/
class VISTAFLOWLIBAUXAPI IVfaContextMenuIconAdapter
{
public:
	IVfaContextMenuIconAdapter(IVflRenderable* pRenderable);
	virtual ~IVfaContextMenuIconAdapter();

	virtual IVfaContextMenuIconAdapter* Clone() = 0;

	virtual bool SetPosition (const VistaVector3D &v3Pos) = 0;
	virtual void GetPosition (VistaVector3D &v3Pos) const = 0;
	virtual bool SetPosition (float fC[3]) = 0;
	virtual void GetPosition (float fC[3]) const = 0;

	virtual bool SetSize (float fSize) = 0;
	virtual float GetSize () const = 0;

	virtual bool SetOrientation (const VistaVector3D &v3Normal) = 0;
	virtual void GetOrientation (VistaVector3D &v3Normal) const = 0;
	virtual bool SetOrientation (float fC[3]) = 0;
	virtual void GetOrientation (float fC[3]) const = 0;

	/** to avoid set the rotation accordingly standard normal is in z-direction */
	virtual bool SetRotation(const VistaQuaternion &qRotation) = 0;
	virtual void GetRotation(VistaQuaternion &qRot) const = 0;

	virtual IVflRenderable* GetVis() const;

	virtual bool SetIsHighlighted (bool bHighlight) = 0;
	virtual bool GetIsHighlighted () const = 0;

	virtual void SetVisible (bool bVisible);
	virtual bool GetVisible ();

	bool SetLineColor(float fColor[3]);
	bool SetLineColor(const VistaColor& color);
	void GetLineColor(float fColor[]) const;
	VistaColor GetLineColor() const;
	
	virtual bool SetNormalColor (const VistaColor& color);
	virtual VistaColor GetNormalColor() const;
	virtual bool SetNormalColor (float fColor[3]);
	virtual void GetNormalColor(float fColor[]) const;

	virtual bool SetHighlightColor (const VistaColor& color);
	virtual VistaColor GetHighlightColor() const;
	virtual bool SetHighlightColor (float fColor[3]);
	virtual void GetHighlightColor(float fColor[]) const;

	virtual bool SetNormalLineWidth (float fLineWidth);
	virtual float GetNormalLineWidth() const;

	virtual bool SetHighlightLineWidth (float fLineWidth);
	virtual float GetHighlightLineWidth() const;

	virtual bool SetTexture(VistaTexture *pTex) = 0;
	virtual VistaTexture *GetTexture() const = 0;

	
protected:
	float m_fHighlightColor[3];
	float m_fNormalColor[3];
	float m_fHighlightLineWidth;
	float m_fNormalLineWidth;

private:
	IVflRenderable *m_pRenderable;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFACONTEXTMENUICONADAPTER_H

