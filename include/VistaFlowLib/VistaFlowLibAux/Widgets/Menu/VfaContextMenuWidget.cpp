/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES																  */
/*============================================================================*/
#include "VfaContextMenuWidget.h"
#include "../Menu/VfaMenuWidget.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaContextMenuWidget::VfaContextMenuWidget(VflRenderNode *pRenderNode,
											 VfaPointerManager *pManagPts, 
											 int iNrOfIds,
											 IVfaContextMenuIconAdapter *pAdapt,
											 bool bUnFix /*=true*/)	
:	VfaMenuWidget(iNrOfIds, pRenderNode, bUnFix),
	m_bEnabled(true)
{
	m_pMenuController = new VfaContextMenuController(m_pMenuModel,
		pRenderNode, pManagPts, pAdapt);
	m_pMenuController->Observe(m_pMenuModel);
	
	
	this->GetModel()->Notify();
	this->GetController()->Notify();
}

VfaContextMenuWidget::~VfaContextMenuWidget()
{
	delete m_pMenuController;
}
/*============================================================================*/
/*  NAME      :   Set/GetIsEnabled                                            */
/*============================================================================*/
void VfaContextMenuWidget::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pMenuController->SetIsEnabled(m_bEnabled);
}
bool VfaContextMenuWidget::GetIsEnabled() const
{
	return m_bEnabled;
}
/*============================================================================*/
/*  NAME      :   Set/GetIsVisible                                            */
/*============================================================================*/
void VfaContextMenuWidget::SetIsVisible(bool b)
{
	m_bVisible = b;
	m_pMenuController->SetVisEnabled(m_bVisible);
}
bool VfaContextMenuWidget::GetIsVisible() const
{
	return m_bVisible;
}
/*============================================================================*/
/*  NAME      :   GetController                                               */
/*============================================================================*/
VfaContextMenuController* VfaContextMenuWidget::GetController() const
{
	return m_pMenuController;
}


/*============================================================================*/
/*  NAME      :   SetTextureForId                                             */
/*============================================================================*/
void VfaContextMenuWidget::SetTextureForId(int id, VistaTexture *pTex)
{
	m_pMenuController->SetTextureForId(id, pTex);
}


/*============================================================================*/
/*  NAME      :   IsClosingAfterSelection                                     */
/*============================================================================*/

void VfaContextMenuWidget::IsClosingAfterSelection( bool b)
{
	m_pMenuController->IsClosingAfterSelection(b);
}
bool VfaContextMenuWidget::GetClosingAfterSelection()
{
	return m_pMenuController->GetClosingAfterSelection();
}
/*============================================================================*/
/*  END OF FILE "VfaContextMenuWidget.cpp"                                   */
/*============================================================================*/
