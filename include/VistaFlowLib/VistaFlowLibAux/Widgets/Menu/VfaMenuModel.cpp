/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaMenuModel.h"
#include "../VfaQuadVis.h"
#include "../Plane/VfaPlaneModel.h"
#include <VistaOGLExt/VistaTexture.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaMenuModel	                                      */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaMenuModel");

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaMenuModel::VfaMenuModel(int iNrOfIds)
	:	m_bOpen(false),
		m_bOpening(false),
		m_iNrOfIds(iNrOfIds)
{
	for (int i = 0; i < iNrOfIds; ++i)
		m_strMessages.push_back("empty");

}

VfaMenuModel::~VfaMenuModel()
{}


/*============================================================================*/
/*  NAME      :   CloseMenuAfterSelection								      */
/*============================================================================*/
void VfaMenuModel::CloseMenuAfterSelection()
{
	Notify(MSG_TIME_TO_CLOSE_AFTER_SELECTION);
}
/*============================================================================*/
/*  NAME      :   CloseMenu												      */
/*============================================================================*/
void VfaMenuModel::CloseMenu()
{
	Notify(MSG_CLOSE);
}
/*============================================================================*/
/*  NAME      :   SetMenuClosed/Open									      */
/*============================================================================*/
void VfaMenuModel::SetMenuClosed()
{
	this->SetIsOpen(false);
	Notify(MSG_MENU_CLOSED);
}
void VfaMenuModel::SetMenuOpen()
{
	this->SetIsOpen(true);
	Notify(MSG_MENU_OPEN);
}

/*============================================================================*/
/*  NAME      :   SomethingChanged											  */
/*============================================================================*/
void VfaMenuModel::SomethingChanged()
{
	Notify(MSG_MENU_CHANGES);
}

/*============================================================================*/
/*  NAME      :   Get/SetNumIds()											  */
/*============================================================================*/
void VfaMenuModel::SetNumIds(int i)
{
	// current element number is th wished one
	if (m_iNrOfIds == i)
		return;

	// save it
	m_iNrOfIds = i;
	this->Notify(MSG_NUMBER_TOKEN_CHANGE);
}

int VfaMenuModel::GetNumIds() const
{
	return m_iNrOfIds;
}

/*============================================================================*/
/*  NAME      :   Get/SetIsOpen[ing]()										  */
/*============================================================================*/
void VfaMenuModel::SetIsOpen(bool b)
{
	m_bOpen = b;
}
bool VfaMenuModel::GetIsOpen() const
{
	return m_bOpen;
}

/*============================================================================*/
/*  NAME      :   Get/SetActiveId()											  */
/*============================================================================*/
bool VfaMenuModel::SetActiveId(int id)
{
	if (id < 0 || id >= m_iNrOfIds)
		return false;
	
	m_iActiveId = id;
	this->Notify(MSG_TOKEN_CHOSEN);
	return true;
}

int VfaMenuModel::GetActiveId() const
{
	return m_iActiveId;
}

/*============================================================================*/
/*  NAME      :   Get/SetFocusedId											  */
/*============================================================================*/
bool VfaMenuModel::SetFocusedId(int id)
{
	// @todo This function should go hand in hand with the Unfocused* version
	//		 or some inconsistent state might occur.
	if (id < 0 || id >= m_iNrOfIds)
		return false;
	
	m_iFocusedId = id;
	this->Notify(MSG_TOKEN_FOCUSED);
	return true;
}
int VfaMenuModel::GetFocusedId() const
{
	return m_iFocusedId;
}
/*============================================================================*/
/*  NAME      :   Get/SetUnfocusedId										  */
/*============================================================================*/
bool VfaMenuModel::SetUnfocusedId(int id)
{
	if (id < 0 || id >= m_iNrOfIds)
		return false;
	
	m_iLastFocusedId = id;
	this->Notify(MSG_TOKEN_UNFOCUSED);
	return true;
}
int VfaMenuModel::GetUnfocusedId() const
{
	return m_iLastFocusedId;
}

/*============================================================================*/
/*  NAME      :   Get/SetMessageForId()										  */
/*============================================================================*/
bool VfaMenuModel::SetMessageForId(int id, const std::string &msg)
{
	if (id < 0 || id >= m_iNrOfIds)
		return false;

	m_strMessages[id] = msg;
	return true;
}

const std::string VfaMenuModel::GetMessageForId(int id) const
{
	if (id < 0 || id >= m_iNrOfIds)
		return std::string("");

	return m_strMessages[id];
}

const std::string VfaMenuModel::GetMessageForActiveId() const
{
	return m_strMessages[m_iActiveId];
}

void VfaMenuModel::GetMessageForId(int id, std::string &msg) const
{
	if (id < 0 || id >= m_iNrOfIds)
		msg = "";

	msg = m_strMessages[id];
}
void VfaMenuModel::GetMessageForActiveId(std::string &msg) const
{
	msg = m_strMessages[m_iActiveId];
}


/*============================================================================*/
/*  NAME      :   GetReflectionableType                                       */
/*============================================================================*/
std::string VfaMenuModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*  NAME      :   AddToBaseTypeList                                           */
/*============================================================================*/
int VfaMenuModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}


/*============================================================================*/
/*  END OF FILE "VfaMenuModel.cpp"	                                          */
/*============================================================================*/
