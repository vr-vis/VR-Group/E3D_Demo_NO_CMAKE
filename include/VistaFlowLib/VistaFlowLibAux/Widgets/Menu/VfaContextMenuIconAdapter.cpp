/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaContextMenuIconAdapter.h"
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <cstring>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
IVfaContextMenuIconAdapter::IVfaContextMenuIconAdapter(IVflRenderable *pRenderable)
	:m_pRenderable(pRenderable)
{
	m_fNormalColor[0] = 0.55f;
	m_fNormalColor[1] = 1.0f;
	m_fNormalColor[2] = 0.5f;
	
	m_fHighlightColor[0] = 1.0f;
	m_fHighlightColor[0] = 0.0f;
	m_fHighlightColor[0] = 0.0f;

}
IVfaContextMenuIconAdapter::~IVfaContextMenuIconAdapter()
{}

/*============================================================================*/
/*  IMPLEMENTATION			                                                  */
/*============================================================================*/

/*============================================================================*/
/*  NAME: Set/GetNormalColor			                                      */
/*============================================================================*/
bool IVfaContextMenuIconAdapter::SetNormalColor (const VistaColor& color)
{
	float fColor[3];
	color.GetValues(fColor);
	return this->SetNormalColor(fColor);
}
VistaColor IVfaContextMenuIconAdapter::GetNormalColor() const
{
	return VistaColor(m_fNormalColor);
}

bool IVfaContextMenuIconAdapter::SetNormalColor (float fColor[3])
{
	if(	fColor[0] == m_fNormalColor[0] &&
		fColor[1] == m_fNormalColor[1] &&
		fColor[2] == m_fNormalColor[2])
	{
		return false;
	}

	memcpy(m_fNormalColor, fColor, 3*sizeof(float));
	return true;
}
void IVfaContextMenuIconAdapter::GetNormalColor(float fColor[]) const
{
	memcpy(fColor, m_fNormalColor, 3*sizeof(float));
}

/*============================================================================*/
/*  NAME: Set/GetHighlightColor			                                      */
/*============================================================================*/
bool IVfaContextMenuIconAdapter::SetHighlightColor (const VistaColor& color)
{
	float fColor[3];
	color.GetValues(fColor);
	return this->SetHighlightColor(fColor);
}
VistaColor IVfaContextMenuIconAdapter::GetHighlightColor() const
{
	return VistaColor(m_fHighlightColor);
}

bool IVfaContextMenuIconAdapter::SetHighlightColor (float fColor[3])
{
	if(	fColor[0] == m_fHighlightColor[0] &&
		fColor[1] == m_fHighlightColor[1] &&
		fColor[2] == m_fHighlightColor[2])
	{
		return false;
	}

	memcpy(m_fHighlightColor, fColor, 3*sizeof(float));
	return true;
}
void IVfaContextMenuIconAdapter::GetHighlightColor(float fColor[]) const
{
	memcpy(fColor, m_fHighlightColor, 3*sizeof(float));
}

/*============================================================================*/
/*  NAME: Set/GetNormalLineWidth			                                  */
/*============================================================================*/
bool IVfaContextMenuIconAdapter::SetNormalLineWidth (float fLineWidth)
{
	if (m_fNormalLineWidth == fLineWidth)
		return false;

	m_fNormalLineWidth = fLineWidth;
	return true;

}
float IVfaContextMenuIconAdapter::GetNormalLineWidth() const
{
	return m_fNormalLineWidth;
}
/*============================================================================*/
/*  NAME: Set/GetHighlightLineWidth			                                  */
/*============================================================================*/
bool IVfaContextMenuIconAdapter::SetHighlightLineWidth (float fLineWidth)
{
	if (m_fHighlightLineWidth == fLineWidth)
		return false;

	m_fHighlightLineWidth = fLineWidth;
	return true;
}
float IVfaContextMenuIconAdapter::GetHighlightLineWidth() const
{
	return m_fHighlightLineWidth;
}


/*============================================================================*/
/*  NAME: GetVis									                          */
/*============================================================================*/
IVflRenderable* IVfaContextMenuIconAdapter::GetVis() const
{
	return m_pRenderable;
}

/*============================================================================*/
/*  NAME: Set/GetVisible							                          */
/*============================================================================*/
void IVfaContextMenuIconAdapter::SetVisible (bool bVisible)
{
	m_pRenderable->SetVisible(bVisible);
}
bool IVfaContextMenuIconAdapter::GetVisible ()
{
	return m_pRenderable->GetVisible();
}

/*============================================================================*/
/*  END OF FILE "VfaContextMenuIconAdapter.cpp"                              */
/*============================================================================*/
