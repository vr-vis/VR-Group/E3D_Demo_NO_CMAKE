/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaMenuWidget.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>

using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaMenuWidget::VfaMenuWidget(int iNrOfIds, VflRenderNode *pRenderNode, bool bUnFix /*=true*/)
:	IVfaWidget(pRenderNode)
{
	if (bUnFix)
		m_pMenuModel = new VfaMenuModel(iNrOfIds+1);	// one item more as desired, used for the "fix-option"
	else
		m_pMenuModel = new VfaMenuModel(iNrOfIds);	
	this->GetModel()->Notify();
}

VfaMenuWidget::~VfaMenuWidget()
{
	delete m_pMenuModel;
}
/*============================================================================*/
/*  NAME      :   GetModel                                                    */
/*============================================================================*/
VfaMenuModel* VfaMenuWidget::GetModel() const
{
	return m_pMenuModel;
}


/*============================================================================*/
/*  NAME      :   GetView                                                     */
/*============================================================================*/
IVflRenderable* VfaMenuWidget::GetView() const
{
	return NULL;
}


/*============================================================================*/
/*  END OF FILE "VfaMenuWidget.cpp"								          */
/*============================================================================*/
