/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAMenuCONTROLLER_H
#define _VFAMenuCONTROLLER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetController.h"

#include <VistaAspects/VistaReflectionable.h>
#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaMenuModel;
class VistaTimer;
class VistaIndirectXform;
class VistaTexture;
class VfaLineModel;
class VfaLineVis;
class VfaPlaneModel;
class VfaQuadVis;
class VfaPointerManager;
class Vfl3DTextLabel;
class VfaCircle2DVis;
class VistaPlane;
class VflVisTiming;
class VflRenderNode;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaMenuController :	public IVfaWidgetController,
												public VflObserver
{
public:
	VfaMenuController(VfaMenuModel *pModel, VflVisTiming *pVisTime,
		VflRenderNode *pRenderNode);
	virtual ~VfaMenuController();
	
	virtual void OnTimeSlotUpdate(const double dTime);

	virtual void UpdateSize(double fDeltaTime) = 0;

	/** time in sec*/
	bool SetTimeToOpenCloseMenu(float fTime);
	float GetTimeToOpenCloseMenu() const;

	/** time in sec*/
	void SetAckTimer(float fTimer);
	float GetAckTimer() const;

	/** control time to close the menu after selection */
	void ControllAckTime(double fDeltaTimer);

	enum
	{
		MSG_START_PROCESS = 0,
		MSG_ACK_TIME,
		MSG_MOVE_BT_PRESS,
		MSG_MOVE_BT_RELEASE
	};

	bool StartEvent(int iMsg);

	void SetChangeInformation();

protected:
	
	VfaMenuModel *m_pModel;
	
	/**  time to control the opening/closing process */
	double m_fStartTime;
	/** time to control the closing after selection */
	double m_fAckTime;
	/** time to control whether button press is command for closing or just for moving */
	double m_fMoveTime; 

	/** auxiliary variable: just to control internal states*/
	bool m_bChangeByLastPass;

	/** timer to control all movements */
	VflVisTiming *m_pVisTime;

	float m_fOpeningClosingTime;
	float m_fFinalAckTime;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaMenuController_H
