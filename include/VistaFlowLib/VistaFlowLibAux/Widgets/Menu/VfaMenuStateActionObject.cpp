/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

 


#include "VfaMenuStateActionObject.h"
#include "VfaContextMenuController.h"

#include <VistaKernel/VistaSystem.h>
#include <VistaAspects/VistaReflectionable.h>
#include <VistaDataFlowNet/VdfnObjectRegistry.h>

#include <cassert>

using namespace std;

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

REFL_IMPLEMENT_FULL(VfaMenuStateActionObject, IVdfnActionObject);

namespace
{
	IVistaPropertySetFunctor *SaSetter[] =
	{
		new TActionSetFunctor<VfaMenuStateActionObject, 
								 int, int>(
									&VfaMenuStateActionObject::SetStateChange,
									NULL,
									"entry_index", 
									SsReflectionName, 
									"sets the change of e.g. the wheel of the mouse"),

		new TActionSetFunctor<VfaMenuStateActionObject, 
								 bool, bool>(
									&VfaMenuStateActionObject::ResetState,
									NULL,
									"reset", 
									SsReflectionName, 
									"reset entry_index, set it back to zero"),
		NULL
	};
}

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaMenuStateActionObject::VfaMenuStateActionObject
										(const string &strName,
										VdfnObjectRegistry *pReg,
										VfaContextMenuController *pContextMenuCtr)
:	m_iEntryState(0),
	m_iResetDiff(0),
	m_pContextMenuCtr(pContextMenuCtr),
	m_pReg(pReg)
{
	// register object for DFN accessibility
	m_pReg->SetObject(strName, this, NULL);
	SetNameForNameable(strName);

	m_pContextMenuCtr->SetMouseActive(true);
	m_pContextMenuCtr->SetOnFocus(m_iEntryState);
}

VfaMenuStateActionObject::~VfaMenuStateActionObject()
{
	// unregister object from DFN
	assert(m_pReg->RemObject(GetNameForNameable()));
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/* NAME: SetWheelChange                                                       */
/*============================================================================*/
bool VfaMenuStateActionObject::SetStateChange( int iChange )
{
	if( compAndAssignFunc<int>( iChange, m_iEntryState ) )
	{
		if( m_pContextMenuCtr->SetOnFocus(m_iEntryState-m_iResetDiff))
		{
			Notify(MSG_STATE_CHANGE);
			return true;
		}
	}
	return false;
}

/*============================================================================*/
/* NAME: ResetWheelState                                                      */
/*============================================================================*/
bool VfaMenuStateActionObject::ResetState( bool b )
{
	if (b)
	{
		m_iResetDiff = m_iEntryState;
		if( m_pContextMenuCtr->SetOnFocus(m_iEntryState-m_iResetDiff))
		{
			Notify(MSG_STATE_RESET);
			return true;
		}
	}

	return false;
}

/*============================================================================*/
/* NAME: Set/GetController                                                    */
/*============================================================================*/
bool VfaMenuStateActionObject::SetController(VfaContextMenuController *pContextMenuCtr)
{
	m_pContextMenuCtr->SetMouseActive(false);
	

	m_pContextMenuCtr = pContextMenuCtr;
	m_iEntryState = 0;
	m_iResetDiff = 0;
	
	m_pContextMenuCtr->SetMouseActive(true);
	m_pContextMenuCtr->SetOnFocus(0);
	ResetState(true);

	return true;

}
VfaContextMenuController* VfaMenuStateActionObject::GetController() const
{
	return m_pContextMenuCtr;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VfaMenuStateActionObject.cpp"								  */
/*============================================================================*/


