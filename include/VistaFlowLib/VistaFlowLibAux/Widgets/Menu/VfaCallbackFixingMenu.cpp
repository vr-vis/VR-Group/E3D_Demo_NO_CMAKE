/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES					                                                  */
/*============================================================================*/
#include "VfaCallbackFixingMenu.h"
#include <VistaFlowLibAux/Widgets/Menu/VfaMenuWidget.h>

using namespace std;

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaCallbackFixingMenu::VfaCallbackFixingMenu(VfaMenuWidget *pWidget,
	VistaTexture *pNoFixTexture, VistaTexture *pFixTexture)
:	m_pWidget(pWidget),
	m_pNoFixTexture(pNoFixTexture),
	m_pFixTexture(pFixTexture)
{
	if(m_pWidget->GetClosingAfterSelection())
	{
		m_pWidget->SetTextureForId(0, m_pNoFixTexture);
	}
	else
	{
		m_pWidget->SetTextureForId(0, m_pFixTexture);
	}
}

VfaCallbackFixingMenu::~VfaCallbackFixingMenu()
{
	delete m_pNoFixTexture;
	delete m_pFixTexture;
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/* NAME: Do			                                                          */
/*============================================================================*/
bool VfaCallbackFixingMenu::Do()
{
	if (m_pWidget->GetClosingAfterSelection())
	{
		m_pWidget->SetTextureForId(0, m_pFixTexture);
		m_pWidget->IsClosingAfterSelection(false);
	}
	else
	{
		m_pWidget->SetTextureForId(0, m_pNoFixTexture);
		m_pWidget->IsClosingAfterSelection(true);
	}

	return true;
}


/*============================================================================*/
/*  END OF FILE "VfaCallbackFixingMenu.cpp"								  */
/*============================================================================*/
