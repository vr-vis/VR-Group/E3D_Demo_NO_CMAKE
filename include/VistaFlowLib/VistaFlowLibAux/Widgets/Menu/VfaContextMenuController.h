/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFACONTEXTMENUCONTROLLER_H
#define _VFACONTEXTMENUCONTROLLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaMenuController.h"
#include "VfaContextMenuIconAdapter.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaMath/VistaGeometries.h>

#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaFlowLib/Visualization/VflVisController.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaMenuModel;
class VflRenderNode;
class VistaIndirectXform;
class VistaTexture;
class VfaLineModel;
class VfaLineVis;
class VfaPlaneModel;
class VfaPointerManager;
class Vfl3DTextLabel;
class VistaPlane;
class VfaPartialDiskVis;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 *	Controll the behavior of the context menu.
 *	See also the following state machine:
 *
 *			    start/end
 *					|
 *				MENU_INACTIVE
 *					| |
 *				MENU_CHANGING_SIZE	<-|
 *					| |				  |
 *				MENU_ACTIVE			  | (default, but can be switched of by
 *					| |				  |  'm_bCloseAfterSelection')
 *					| |				  |
 *				MENU_TARGET_TOUCHED  _|
 *
 */
class VISTAFLOWLIBAUXAPI VfaContextMenuController : public VfaMenuController
{
public:
	

	/**
	*	Constructor: 
	*	usual parameter values for widgets
	*	VfaPointerManager --> fade out/in your pointers 
	*	IVfaContectMenuIconAdapter --> choose a visualization for your items
	*/
	VfaContextMenuController(	VfaMenuModel *pModel,
								VflRenderNode *pRenderNode,
								VfaPointerManager *pManagPts,
								IVfaContextMenuIconAdapter *pAdapt);
	
	virtual ~VfaContextMenuController();

	/** 
	 * This is called on transition change into(out of) state FOCUS
	 */
	virtual void OnFocus(IVfaControlHandle* pHandle);
	virtual void OnUnfocus();

	/**
	 * This is called on transition change into(out of) state TOUCH
	 */
	virtual void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	virtual void OnUntouch();

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	virtual void OnSensorSlotUpdate(int iSlot, 
		const VfaApplicationContextObject::sSensorFrame & oFrameData);
	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);


	/**
	 * return an(or'ed) bitmask defining which sensor slots/command
	 * slots are used/handled by this controller
	 */
	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;


	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetVisEnabled(bool b);
	bool GetVisEnabled() const;

	bool SetItemExtendFactor(float fFactor);
	float GetItemExtendFactor() const;

	void SetGrabButton (int iSlot);
	void SetOpenButton (int iSlot);

	void OpenMenu ();
	
	/**
	 * Close Menu without drect menu command and define
	 * whether it is enabled or disabled after closing
	 */
	void CloseMenu(bool bEnable);

	/**
	 * Decide whether the menu closes automatically after selection
	 * or if it stays open
	 */
	void IsClosingAfterSelection(bool b);
	bool GetClosingAfterSelection();


	bool SetTextureForId(int id, VistaTexture *pTex);
	VistaTexture *GetTextureForId(int id) const;
	
	/*
	* used to factorize the angle of the input device's orientation
	* see code of OnSensorSlotUpdate
	*/
	bool SetFactorInputDeviceRotation(float f);
	float GetFactorInputDeviceRotation() const;

	bool SetMouseActive(bool b);
	bool GetMouseActive() const;

	bool SetOnFocus(int iActive);

	void UpdateSize(double dDeltaTime);
	
	bool SetCenter(float fC[3]);
	bool SetCenter(const VistaVector3D &v3C);
	void GetCenter(double dC[3]) const;
	void GetCenter(float fC[3]) const;
	void GetCenter(VistaVector3D &v3C) const;
	
	bool SetOrientation(const VistaQuaternion &quat);
	void GetOrientation(VistaQuaternion &quat) const;
	
	/**
	 * define the inner radius of the partial disk (define radius of the hole)
	 */
	bool SetFinalInnerRadius(float f);
	float GetFinalInnerRadius() const;
	
	/**
	 * define the radius of the partial disk
	 * (innerRadius+Radius = radius of the whole partial disk)
	 */
	bool SetFinalRingRadius(float f);
	float GetFinalRingRadius() const;

	/*DON'T use those methods - they are used by UpdateSize*/
	bool SetCurrentInnerRadius(float f);
	float GetCurrentInnerRadius() const;
	
	/*DON'T use those methods - they are used by UpdateSize*/
	bool SetCurrentRingRadius(float f);
	float GetCurrentRingRadius() const;

	/*ONLY use this method when you need to or must adjust the widget from outside*/
	void AdjustMenu();

 //IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);


	class VISTAFLOWLIBAUXAPI VfaContextMenuControllerProps : public IVistaReflectionable
	{
		friend class VfaContextMenuController;
	public:
		
		VfaContextMenuControllerProps(VfaContextMenuController *pMenuCtrl);
		virtual ~VfaContextMenuControllerProps();

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	private:
		VfaContextMenuController	*m_pMenuCtrl;
	};


	VfaContextMenuControllerProps* GetProperties() const;

protected:
	void SetMenuState(int iNewState);
	void PrintState(int iOldState, int iCurrentState) const;

	/** reset menu */
	void DeleteVisibleFeedback();
	/** dye the line of iItem red */
	void ColorItem(int iItem);
	/** scale up/down the item iItem*/
	void SizeItem(int iItem, bool bNormal);
	
	// adjust the handles
	void AdjustItems();
	void AdjustRotation(int iNrOfHandles);
	void AdjustTranslation(int iNrOfHandles);
	void AdjustExtend(int iNrOfHandles);

	void ComputeMenuPlane();

	/**
	 * enum for internal state control
	 */
	enum EInternalState{
		MENU_INACTIVE=0,
		MENU_CHANGING_SIZE,
		MENU_ACTIVE,
		MENU_TARGET_TOUCHED,
		MENU_DISABLED
	};

private:
	void CloseMenuAfterSelection();
	void Close();
	void UpdateMouseMode();

	VflRenderNode					*m_pRenderNode;
	
	std::vector<IVfaContextMenuIconAdapter*>	 m_vecItems;

	int								m_iActiveItem;
	const static int				ITEM_UNDEFINED = -1;
	
	int								m_iGrabButton;
	int								m_iOpeningButton;

	int								m_iState;
	int								m_iLastState;

	float							m_fFactorInputDeviceRotation;
	
	
	VfaContextMenuControllerProps	*m_pCtlProps;

	bool							m_bEnabled;
	bool							m_bHasToBeSetToEnable;
	bool							m_bStopAdjust;
	bool							m_bCloseAfterSelection;

	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;
	VistaTransformMatrix			m_matTrans;
	VistaPlane						*m_pPlane;

	float							m_fFactorExtend;
	
	VfaLineModel					*m_pLineModel;
	VfaLineVis					    *m_pLineVis;
	VfaPlaneModel					*m_pPlaneModel;
	VfaQuadVis					    *m_pQuadVis;
	Vfl3DTextLabel					*m_pText;
	VfaPointerManager				*m_pManagPts;

	bool							m_bUseMouse;

	float							m_fCenter[3];

	float							m_fCurrentInnerRadius;
	float							m_fCurrentRingRadius;
	float							m_fFinalInnerRadius;
	float							m_fFinalRingRadius;
	VistaQuaternion					m_qQuat;

	VfaPartialDiskVis *m_pPartialDiskVis;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFACONTEXTMENUCONTROLLER_H
