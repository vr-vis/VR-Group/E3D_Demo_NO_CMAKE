

set( RelativeDir "./Widgets/Menu" )
set( RelativeSourceGroup "Source Files\\Widgets\\Menu" )

set( DirFiles
	VfaCallbackFixingMenu.cpp
	VfaCallbackFixingMenu.h
	VfaCallbackNavigationFactor.cpp
	VfaCallbackNavigationFactor.h
	VfaContextMenuController.cpp
	VfaContextMenuController.h
	VfaContextMenuIconAdapter.cpp
	VfaContextMenuIconAdapter.h
	VfaContextMenuWidget.cpp
	VfaContextMenuWidget.h
	VfaListMenuController.cpp
	VfaListMenuController.h
	VfaListMenuWidget.cpp
	VfaListMenuWidget.h
	VfaMenuController.cpp
	VfaMenuController.h
	VfaMenuItemCollection.cpp
	VfaMenuItemCollection.h
	VfaMenuModel.cpp
	VfaMenuModel.h
	VfaMenuStateActionObject.cpp
	VfaMenuStateActionObject.h
	VfaMenuWidget.cpp
	VfaMenuWidget.h
	VfaQuadAdapter.cpp
	VfaQuadAdapter.h
	VfaRingAdapter.cpp
	VfaRingAdapter.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

