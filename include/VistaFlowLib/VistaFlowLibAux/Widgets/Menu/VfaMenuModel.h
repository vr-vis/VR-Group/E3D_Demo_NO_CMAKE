/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAMENUMODEL_H
#define _VFAMENUMODEL_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidget.h"
#include "../VfaWidgetModelBase.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <vector>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaEventManager;
class VistaNewInteractionManager;
class VflVisController;
class VfaQuadVis;
class VistaTexture;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*	model of a menu
*   just gives info about number/message of items and menu state (open or close)
*/
class VISTAFLOWLIBAUXAPI VfaMenuModel : public VfaWidgetModelBase
{
public:
	enum{
		MSG_NUMBER_TOKEN_CHANGE,
		MSG_TOKEN_CHOSEN,
		MSG_TIME_TO_CLOSE_AFTER_SELECTION,
		MSG_MENU_OPEN,
		MSG_MENU_CLOSED,
		MSG_CLOSE,
		MSG_MENU_CHANGES,
		MSG_TOKEN_FOCUSED,
		MSG_TOKEN_UNFOCUSED,
		MSG_LAST,
	};


	VfaMenuModel(int iNrOfIds);
	virtual ~VfaMenuModel();

	void CloseMenuAfterSelection();
	void CloseMenu();
	void SetMenuClosed();
	void SetMenuOpen();

	/** information about menu's status */
	void SetIsOpen(bool b);
	bool GetIsOpen() const;

	/** set number of ids */
	void SetNumIds(int i);
	int GetNumIds() const;

	bool SetActiveId(int id);
	int GetActiveId() const;

	bool SetFocusedId(int id);
	bool SetUnfocusedId(int id);
	int  GetFocusedId() const;
	int  GetUnfocusedId() const;
	
	bool SetMessageForId(int id, const std::string &msg);
	const std::string GetMessageForId(int id) const;
	const std::string GetMessageForActiveId() const;
	void GetMessageForId(int id, std::string& msg) const;
	void GetMessageForActiveId(std::string& msg) const;

	virtual std::string GetReflectionableType() const;

	/*to throw a notifictaion when something, probably the position has changed*/
	void SomethingChanged();


protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	

private:

	bool m_bOpening;
	bool m_bOpen;
	
	int m_iNrOfIds;

	int m_iActiveId;
	
	int m_iFocusedId;
	int m_iLastFocusedId;

	std::vector<std::string> m_strMessages;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAMENUMODEL_H
