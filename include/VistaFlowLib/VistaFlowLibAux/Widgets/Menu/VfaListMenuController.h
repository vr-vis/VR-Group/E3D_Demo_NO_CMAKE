/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef __VFALISTEMENUCONTROLLER_H
#define __VFALISTEMENUCONTROLLER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaMenuController.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaMath/VistaGeometries.h>

#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaMenuModel;
class VfaListMenuVis;
class VflRenderNode;
class VistaTimer;
class VistaIndirectXform;
class VistaTexture;
class VfaLineModel;
class VfaLineVis;
class VfaPlaneModel;
class VfaQuadVis;
class VfaPointerManager;

class VfaProximityQuadHandle;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 *	Controll the behavior of the context menu.
 *	See also the following state machine:
 *
 *			    start/end
 *					|
 *				MENU_INACTIVE
 *					| |
 *				MENU_CHANGING_SIZE	<-|
 *					| |
 *				MENU_ACTIVE			  |(default, but can be switched of by 'm_bCloseAfterSelection')
 *					| |
 *				MENU_TARGET_TOUCHED  _|
 *
 */
class VISTAFLOWLIBAUXAPI VfaListMenuController : public VfaMenuController
{
public:
	
	
	/**
	*	Constructor: 
	*	usual parameter values for widgets
	*	VfaPointerManager --> fade out/in your pointers 
	*	IVfaContectMenuIconAdapter --> choose a visualization for your items
	*/
	VfaListMenuController(VfaMenuModel *pModel, 
							VflRenderNode *pRenderNode,
							VfaPointerManager *pManagPts, float fWidgetScale);
	
	virtual ~VfaListMenuController();

	/** 
	 * This is called on transition change into(out of) state FOCUS
	 */
	virtual void OnFocus(IVfaControlHandle* pHandle);
	virtual void OnUnfocus();

	/**
	 * This is called on transition change into(out of) state TOUCH
	 */
	virtual void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	virtual void OnUntouch();

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	virtual void OnSensorSlotUpdate(int iSlot, 
									const VfaApplicationContextObject::sSensorFrame & oFrameData);
	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);

	/**
	 * return an(or'ed) bitmask defining which sensor slots/command
	 * slots are used/handled by this controller
	 */
	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;

	void UpdateSize(double dDeltaTime);

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetVisEnabled(bool b);
	bool GetVisEnabled() const;

	void SetGrabButton (int iSlot);
	void SetOpenButton (int iSlot);

	void OpenMenu ();

	/** Decide whether the menu closes automatically after selection or if it stays open*/
	void IsClosingAfterSelection(bool b);
	bool GetClosingAfterSelection();


	bool SetTextureForId(int id, VistaTexture *pTex);
	VistaTexture *GetTextureForId(int id) const;
	

	bool SetFinalSize(float fWidth, float fHeight);
	void GetFinalSize(float &fWidth, float &fHeight) const;

	bool SetFinalWidth(float fWidth);
	float GetFinalWidth() const;

	bool SetFinalHeight(float fHeight);
	float GetFinalHeight() const;

	/*DON'T use those methods - they are used by UpdateSize*/
	bool SetCurrentSize(float fWidth, float fHeight);
	void GetCurrentSize(float &fWidth, float &fHeight) const;

	/*DON'T use those methods - they are used by UpdateSize*/
	bool SetCurrentWidth(float fWidth);
	float GetCurrentWidth() const;

	/*DON'T use those methods - they are used by UpdateSize*/
	bool SetCurrentHeight(float fHeight);
	float GetCurrentHeight() const;

	
// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);


	class VISTAFLOWLIBAUXAPI VfaListMenuControllerProps : public IVistaReflectionable
	{
		friend class VfaListMenuController;
	public:
		
		VfaListMenuControllerProps(VfaListMenuController *pMenuCtrl);
		virtual ~VfaListMenuControllerProps();

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	private:
		VfaListMenuController	*m_pMenuCtrl;
	};


	VfaListMenuControllerProps* GetProperties() const;

protected:
	void SetMenuState(int iNewState);
	void PrintState(int iOldState, int iCurrentState) const;

	/** reset menu */
	void DeleteVisibleFeedback();
	/** dye the line of iItem red */
	void ColorItem(int iItem);
	/** scale up/down the item iItem*/
	void SizeItem(int iItem, bool bNormal);
	
	// adjust the handles
	void AdjustItems();
	void AdjustRotation(int iNrOfHandles);
	void AdjustTranslation(int iNrOfHandles);
	void AdjustExtend(int iNrOfHandles);

		
	/**
	 * enum for internal state control
	 */
	enum EInternalState{
		MENU_INACTIVE=0,
		MENU_CHANGING_SIZE,
		MENU_ACTIVE,
		MENU_TARGET_TOUCHED,
		MENU_DISABLED
	};

private:
	void CloseMenuAfterSelection();
	void Close();

	VflRenderNode					*m_pRenderNode;
	
	VfaLineModel					*m_pLineModel;
	VfaLineVis						*m_pLineVis;

	vector<VfaProximityQuadHandle*>	m_vecHandle;

	float m_fNormalColor[3];
	float m_fHighlightColor[3];

	int								m_iActiveHnd;
	const static int				HND_UNDEFINED = -1;
	
	int								m_iGrabButton;
	int								m_iOpeningButton;

	int								m_iState;
	int								m_iLastState;
	
	VfaListMenuControllerProps	*m_pCtlProps;

	bool							m_bEnabled;
	bool							m_bStopAdjust;
	bool							m_bCloseAfterSelection;

	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;
		
	VfaPointerManager				*m_pManagPts;


	float m_fFinalWidth;
	float m_fFinalHeight;
	float m_fCurrentWidth;
	float m_fCurrentHeight;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFALISTMENUCONTROLLER_H
