/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#include "VfaContextMenuController.h"

#include "../Menu/VfaMenuModel.h"
#include "VfaContextMenuWidget.h"
#include "../VfaProximityQuadHandle.h"
#include "../Plane/VfaPlaneModel.h"
#include "../Line/VfaLineModel.h"
#include "../VfaLineVis.h"
#include "../VfaQuadVis.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>
#include <VistaFlowLibAux/Widgets/VfaCircle2DVis.h>


#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaMath/VistaIndirectXform.h>
#include <VistaBase/VistaTimer.h>
#include <VistaOGLExt/VistaTexture.h>

#include <VistaKernel/GraphicsManager/VistaGraphicsManager.h>
#include <VistaFlowLibAux/Interaction/VfaPointerManager.h>

#include <cassert>
#include <cstring>
#include <cstdio>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

class VfaPartialDiskVis : public IVflRenderable
{
public:
	VfaPartialDiskVis(VfaContextMenuController *pCtr)
	:	m_pCtr(pCtr),
		m_uiMenuDisplayList(0),
		m_fOldInnerRadius(0.0f),
		m_fOldOuterRadius(0.0f)
	{
		quadratic = gluNewQuadric();
		m_uiMenuDisplayList = glGenLists(1);
	}

	virtual ~VfaPartialDiskVis()
	{
		gluDeleteQuadric( quadratic );
		glDeleteLists(m_uiMenuDisplayList, 1);
	}

	unsigned int GetRegistrationMode() const
	{
		return IVflRenderable::OLI_DRAW_OPAQUE;
	}

	void DrawOpaque()
	{
		if(!this->GetProperties()->GetVisible())
			return;
		
		glPushAttrib(GL_ENABLE_BIT);
		glPushMatrix();
		glDisable(GL_CULL_FACE);

		// @todo disabled lighting should do better
		glDisable(GL_LIGHTING);
		
		float fColor[4];
		this->GetProperties()->GetColor(fColor);
		glColor4fv(fColor);

		/*
		glDisable(GL_COLOR_MATERIAL);
		GLfloat fAmbient[] = {0.2f, 0.2f, 0.2f, 1.0f};
		GLfloat fSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};

		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,fAmbient);
		glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,fColor);
		glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,fSpecular);
		glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0);
		*/
		
		// set disk's center;
		float fCenter[4];
		m_pCtr->GetCenter(fCenter);
		glTranslatef(fCenter[0], fCenter[1], fCenter[2]);

		// rotate
		VistaQuaternion qOri;
		m_pCtr->GetOrientation(qOri);
		glRotatef(Vista::RadToDeg(acos(qOri[3])*2), qOri[0], qOri[1], qOri[2]);
			 
		// last but not least draw the partial disk itself....
		// inner radius of the partial disk
		float fInnerRadius = m_pCtr->GetCurrentInnerRadius();
		// outer radius, defined as inner radius + ring radius
		float fOuterRadius = m_pCtr->GetCurrentRingRadius() + fInnerRadius;

		if(m_fOldInnerRadius != fInnerRadius || m_fOldOuterRadius != fOuterRadius)
		{
			m_fOldInnerRadius = fInnerRadius;
			m_fOldOuterRadius = fOuterRadius;
			
			gluQuadricDrawStyle(quadratic, GLU_FILL);
			gluQuadricNormals(quadratic,  GLU_SMOOTH);
		
			// 180 : sweep angle to draw a half(!) circle
			glNewList(m_uiMenuDisplayList, GL_COMPILE_AND_EXECUTE);
				gluPartialDisk(quadratic, fInnerRadius, fOuterRadius,
					50, 50, 270, 180);
			glEndList();
		}
		else
		{
			glCallList(m_uiMenuDisplayList);
		}		
			
		glPopMatrix();
		glPopAttrib();
	}

	class VfaPartialDiskVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		enum{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_LAST
		};

		VfaPartialDiskVisProps()
		{
			//blue border
			m_fColor[0] = 0.0f;
			m_fColor[1] = 0.0f;
			m_fColor[2] = 0.8f;
			m_fColor[3] = 1.0f;
		}
		virtual ~VfaPartialDiskVisProps(){}

		bool SetColor(float fColor[4])
		{
			if(	fColor[0] == m_fColor[0] &&
				fColor[1] == m_fColor[1] &&
				fColor[2] == m_fColor[2] &&
				fColor[3] == m_fColor[3])
			{
				return false;
			}

			memcpy(m_fColor, fColor, 4*sizeof(float));
			this->Notify(MSG_COLOR_CHG);
			return true;
		}

		bool SetColor(const VistaColor& color)
		{
			float fColor[4];
			color.GetValues(fColor);
			fColor[3] = 1.0f;
			return this->SetColor(fColor);
		}

		void GetColor(float fColor[4]) const
		{
			memcpy(fColor, m_fColor, 4*sizeof(float));
		}

		VistaColor GetColor() const
		{
			return VistaColor(m_fColor[0],m_fColor[1],m_fColor[2]);
		}

	private:
		float m_fColor[4];
	};

	VfaPartialDiskVis::VfaPartialDiskVisProps* GetProperties() const
	{
		return static_cast<VfaPartialDiskVis::VfaPartialDiskVisProps *>(IVflRenderable::GetProperties());
	}

protected:
	virtual VflRenderableProperties* CreateProperties() const
	{
		return new VfaPartialDiskVis::VfaPartialDiskVisProps();
	}

private:
	VfaContextMenuController *m_pCtr;
	GLUquadricObj *quadratic; // used to draw partial disk

	GLuint	m_uiMenuDisplayList;
	float	m_fOldInnerRadius;
	float	m_fOldOuterRadius;
};



/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaContextMenuController::VfaContextMenuController(VfaMenuModel *pModel,
													 VflRenderNode *pRenderNode,
													 VfaPointerManager *pManagPts,
													 IVfaContextMenuIconAdapter *pAdapt)
:	VfaMenuController(pModel, pRenderNode->GetVisTiming(), pRenderNode),
		m_iActiveItem(ITEM_UNDEFINED),
		m_iGrabButton(0),
		m_iOpeningButton(1),
		m_iState(MENU_INACTIVE),
		m_iLastState(MENU_INACTIVE),
		m_fFactorInputDeviceRotation(1.0f),
		m_bEnabled(true),
		m_bHasToBeSetToEnable(true),
		m_bStopAdjust(false),
		m_pRenderNode(pRenderNode),
		m_bCloseAfterSelection(true),
		m_pPlane(NULL),
		m_fFactorExtend (1.25f),
		m_pLineModel(NULL),
		m_pLineVis(NULL),
		m_pPlaneModel(NULL),
		m_pQuadVis(NULL),
		m_pText(NULL),
		m_pManagPts(pManagPts),
		m_bUseMouse(false),
		m_fCurrentInnerRadius(0.0f),
		m_fCurrentRingRadius(0.0f),
		m_fFinalInnerRadius(0.125f),
		m_fFinalRingRadius(0.025f)
{
	// create props
	m_pCtlProps = new VfaContextMenuController::VfaContextMenuControllerProps(this);
	
	//attach to the model
	this->Observe(m_pModel);

	m_pPartialDiskVis = new VfaPartialDiskVis(this);
	if (m_pPartialDiskVis->Init())
		pRenderNode->AddRenderable(m_pPartialDiskVis);
	
	// create handles
	int iNrOfUsedHandles = m_pModel->GetNumIds();
	m_vecItems.resize(iNrOfUsedHandles);
	
	for (int i = 0; i < iNrOfUsedHandles; ++i)
	{
		m_vecItems[i] = pAdapt->Clone();
		m_vecItems[i]->SetVisible(false);
		m_vecItems[i]->SetIsHighlighted(false);
	}	

	m_pLineModel = new VfaLineModel;
	m_pLineVis = new VfaLineVis(m_pLineModel);
	m_pLineVis->SetVisible(false);
	if(m_pLineVis->Init())
		pRenderNode->AddRenderable(m_pLineVis);
	m_pLineVis->GetProperties()->SetWidth(5.0f);
	m_pLineVis->GetProperties()->SetColor(m_vecItems[0]->GetNormalColor());

#ifdef PLANE
	m_pPlaneModel = new VfaPlaneModel;
	m_pQuadVis = new VfaQuadVis(m_pPlaneModel);
	m_pQuadVis->SetVisible(false);
	if(m_pQuadVis->Init())
		pVC->RegisterVisObject(m_pQuadVis);
	m_pQuadVis->GetProperties()->SetLineColor(m_vecItems[0]->GetNormalColor());
	m_pQuadVis->GetProperties()->SetLineWidth(2.0f);
#endif

	m_pText = new Vfl3DTextLabel;
	m_pText->Init();
	m_pText->SetText("X");
	float fColor[] = {1.0f, 0.0f, 0.0f, 1.0f};
	m_pText->SetColor(fColor);
	m_pText->SetTextSize(0.05f);
	m_pText->SetVisible(false);
	m_pText->SetTextFollowViewDir(false);
	pRenderNode->AddRenderable(m_pText);

	this->AdjustItems();
}

VfaContextMenuController::~VfaContextMenuController()
{
//	this->ReleaseObserveable(m_pModel, IVistaObserveable::TICKET_NONE);
	int iNumHandles = m_pModel->GetNumIds();
	for (int i = 0; i < iNumHandles; ++i)
			delete m_vecItems[i];

	delete m_pLineModel;
	delete m_pLineVis;
	delete m_pText;

#ifdef PLANE
	delete m_pPlaneModel;
	delete m_pQuadVis;
#endif

	if(m_pPlane != NULL)
		delete m_pPlane;
	delete m_pCtlProps;	
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*  NAME      :   Set/GetIsEnabled                                            */
/*============================================================================*/
void VfaContextMenuController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	
	if(!b)
	{
		m_iLastState = m_iState;
		this->SetMenuState(MENU_DISABLED);
	}
	else
		this->SetMenuState(m_iLastState); // return to the state before being switched off
}
bool VfaContextMenuController::GetIsEnabled() const
{
	return m_bEnabled;
}
/*============================================================================*/
/*  NAME      :   Get/IsClosingAfterSelection                                 */
/*============================================================================*/
void VfaContextMenuController::IsClosingAfterSelection(bool b)
{
	m_bCloseAfterSelection = b;
}
bool VfaContextMenuController::GetClosingAfterSelection()
{
	return m_bCloseAfterSelection;
}

/*============================================================================*/
/*  NAME      :   OnFocus			                                          */
/*============================================================================*/
void VfaContextMenuController::OnFocus(IVfaControlHandle* pHandle)
{
	// we don't need this method here
	
}

/*============================================================================*/
/*  NAME      :   OnUnfocus			                                          */
/*============================================================================*/
void VfaContextMenuController::OnUnfocus()
{ 
	// we don't need this method here
}

/*============================================================================*/
/*  NAME      :   OnTouch			                                          */
/*============================================================================*/
void VfaContextMenuController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{}

/*============================================================================*/
/*  NAME      :   OnUntouch			                                          */
/*============================================================================*/
void VfaContextMenuController::OnUntouch()
{}
/*============================================================================*/
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*============================================================================*/
void VfaContextMenuController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	//we are only interested in our buttons
	if(iSlot != m_iGrabButton && iSlot != m_iOpeningButton)
		return;

	// we are only interested in the command if our menu is enabled
	if(m_iState == MENU_DISABLED)
		return;

	// reaction on button release --> menu should be fix henceforth
	if(!bSet)
	{
		if(iSlot == m_iOpeningButton)
		{
			this->StartEvent(MSG_MOVE_BT_RELEASE);
			m_bStopAdjust = true;
		}

		// target is touched
		if(m_iState == MENU_TARGET_TOUCHED && iSlot == m_iGrabButton)
		{
			this->SetMenuState(MENU_ACTIVE); // return to an idle state
			this->StartEvent(MSG_ACK_TIME); // Ack-Time --> Time to wait before Menu is closed (if this is wished)
			return;
		}
		return;
	}
	
	//save state
	m_iLastState = m_iState;

	if (iSlot == m_iGrabButton)
	{
		if(m_iLastState != MENU_ACTIVE)
			return;

		if(m_iActiveItem != ITEM_UNDEFINED)
		{
			this->SetMenuState(MENU_TARGET_TOUCHED);
			m_pModel->SetActiveId(m_iActiveItem);
			this->ColorItem(m_iActiveItem);

			return;
		}
	}
	else if (iSlot == m_iOpeningButton)
	{
		if(m_iLastState == MENU_INACTIVE)
		{	
			m_pManagPts->SetPointerVisible(false);
			m_bStopAdjust = false;
			this->SetMenuState(MENU_CHANGING_SIZE);
			m_pModel->SetIsOpen(false);
			this->StartEvent(MSG_START_PROCESS);
		}
		else if(m_iLastState == MENU_ACTIVE)
		{
			this->DeleteVisibleFeedback();
			this->StartEvent(MSG_MOVE_BT_PRESS);
			m_pText->SetVisible(false);
			m_pModel->SetIsOpen(true);
			m_bStopAdjust = false;
		}
	}
}
/*============================================================================*/
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*============================================================================*/
void VfaContextMenuController::OnSensorSlotUpdate(int iSlot, 
				const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	if(iSlot != VfaApplicationContextObject::SLOT_CURSOR_VIS)
		return;

	// save sensor data and update model if necessary
	if(!m_bStopAdjust || m_iState == MENU_CHANGING_SIZE)
	{
		VistaVector3D vecOffset = m_pRenderNode->GetLocalViewDirection();
		vecOffset.Normalize();
		m_oLastSensorFrame.qOrientation = oFrameData.qOrientation;
		m_oLastSensorFrame.v3Position = oFrameData.v3Position;
		this->SetCenter(m_oLastSensorFrame.v3Position);
		this->SetOrientation(m_oLastSensorFrame.qOrientation);
		AdjustItems();
	}


	// when we are currently "idle" -> nothing to do...
	if(m_iState == MENU_INACTIVE || m_iState == MENU_CHANGING_SIZE)
	{
		return;
	}


	if(m_iState == MENU_ACTIVE)
	{

		if(m_bUseMouse)
		{
			UpdateMouseMode();
			return;
		}

		// first of all decide whether we need to change the "speedometer needles" visibility
		if(!m_bStopAdjust)
		{ 
			if(m_pLineVis->GetVisible())
				m_pLineVis->SetVisible(false);
			return;
		}
		if (m_bStopAdjust && !m_pLineVis->GetVisible())
		{
			m_pLineVis->SetVisible(true);
		}
		this->ComputeMenuPlane();

		

		// define startpoint of our "speedometer needle"
		VistaVector3D v3Center = m_oLastSensorFrame.v3Position;
		m_pLineModel->SetPoint1(v3Center);


		// to compute the endpoint we have to make several steps...
		// first of all get current direction and compute a new point:
		// g(x) = centerOfMenu + r*direction --> p = centerOfMenu + 1*direction
		VistaVector3D v3Up (0,1,0);
		
		VistaQuaternion quat = oFrameData.qOrientation;
		quat[0] *= m_fFactorInputDeviceRotation;
		v3Up = (quat).Rotate(v3Up);
		v3Up = m_oLastSensorFrame.v3Position + v3Up;

		
		// now we need the project p on our plane
		VistaVector3D v3NearestPt = m_pPlane->CalcNearestPointOnPlane(v3Up);
		
		VistaVector3D v3Offset = (v3NearestPt - m_oLastSensorFrame.v3Position).
										GetNormalized()*0.75*this->GetFinalInnerRadius();
		v3NearestPt = m_oLastSensorFrame.v3Position+v3Offset;


		
#ifdef PLANE
//		 just a visual feedback of our computings
		VistaVector3D v3Trans;
		m_matTrans.GetTranslation(v3Trans);
		m_pPlaneModel->SetTranslation(v3Trans);
		
		m_pPlaneModel->SetNormal(m_pPlane->GetNormVector());
		m_pPlaneModel->SetExtents(2.5*this->GetFinalInnerRadius(),2.5*this->GetFinalInnerRadius());
		if(!m_pQuadVis->GetVisible())
			m_pQuadVis->SetVisible(true);
#endif

		// now pay attention in which part of the circle the needle is able to be rotated
		// 180° should fit to the menus orientation
		// fit --> show the new needle position otherwise don't update
		VistaVector3D v3Real;
		v3Real = m_matTrans.GetInverted().Transform(v3NearestPt);
	
		if (v3Real[1] < 0)
		{
			m_oLastSensorFrame.v3Position[3] = 0;
			VistaVector3D v3RealCenter = m_matTrans.GetInverted().Transform(m_oLastSensorFrame.v3Position);
			v3RealCenter[0] += (m_pText->GetTextSize()/2);

			m_pText->SetPosition(m_matTrans.Transform(v3RealCenter));

			VistaVector3D v3XDir = m_pPlane->GetXDir().GetNormalized();
			v3XDir[3] = 0.0f;
			VistaVector3D v3YDir = m_pPlane->GetYDir().GetNormalized();
			v3YDir[3] = 0.0f;
			VistaVector3D v3ZDir = v3XDir.Cross(v3YDir).GetNormalized();
			v3ZDir[3] = 0.0f;
			
			VistaTransformMatrix mat;
			mat.SetColumn( 0, v3XDir );
			mat.SetColumn( 1, v3YDir );
			mat.SetColumn( 2, v3ZDir );

			m_pText->SetOrientation(VistaQuaternion(mat));
			m_pText->SetVisible(true);
			return;
		}
		else
		{
			m_pText->SetVisible(false);
			m_pLineVis->GetProperties()->SetColor(m_vecItems[0]->GetNormalColor());
		}
		m_pLineModel->SetPoint2(v3NearestPt);



		// here we know that our actual shown needle is oriented to the menu
		// now we have to find out, which item is in focus
		// we use angles to set the right item on focus
				
		int iNrOfHandles = m_pModel->GetNumIds();
		// current angle
		VistaVector3D vec(0,0,0,0);
		vec[0] = this->GetCurrentInnerRadius() + 0.5f*this->GetCurrentRingRadius();
		float fCurAlpha = acos(v3Real.Dot(vec)/(vec.GetLength()*v3Real.GetLength()));

		// angle to turn further to reach each item on the menus semicircle (used in AdjustTranslation)
		float alpha = Vista::DegToRad(180/(iNrOfHandles+1.0f));

		// prevent selection in boundary area
		if (fCurAlpha < (alpha/2) || (Vista::DegToRad(180)-fCurAlpha) < (alpha/2))
		{
			this->AdjustExtend(iNrOfHandles);
			return;
		}

		// subduct the boundray area from the current position to find out, in which "piece of cake" 
		// we currently are to identify the current focused item
		fCurAlpha-=(alpha/2);

		int iNr = static_cast<int>(fCurAlpha/alpha); // number of focused item
		m_iActiveItem = (iNrOfHandles - 1) - iNr;
		//m_iActiveItem = iNr;

		// exception handling -> those handles do not exist
		if(iNr < 0 || iNr >= iNrOfHandles)
		{
			this->AdjustExtend(iNrOfHandles);
			return;
		}
		
		// increase the focused one
		for (int i = 0; i < iNrOfHandles ; ++i)
		{
			if(i!= m_iActiveItem)
			{
				this->SizeItem(i, true);
			}
			else
			{
				this->SizeItem(i, false);
			}
		}
	}
	

}

/*============================================================================*/
/*  NAME      :   Set/GetFactorInputDeviceRotation							  */
/*============================================================================*/
bool VfaContextMenuController::SetFactorInputDeviceRotation(float f)
{
	if(m_fFactorInputDeviceRotation == f)
		return false;

	m_fFactorInputDeviceRotation = f;
	return true;
}
float VfaContextMenuController::GetFactorInputDeviceRotation() const
{
	return m_fFactorInputDeviceRotation;
}

/*============================================================================*/
/*  NAME      :   ComputeMenuPlane  										  */
/*============================================================================*/
void VfaContextMenuController::ComputeMenuPlane()
{
	// Now we compute the plane, the menu lays in.
	// Except the menu's center we need two more points. Therefore it would be easy to take the
	// center of two items (because we computed them already) - but who asserts us,
	// that every user uses this menu with at least two items? No one!
	// So we compute two points independently of the number of items used here.
	// But pay attention: The three points may NOT lay on one straight line! :)
	VistaVector3D v3Point1, v3Point2, v3Normal;
	v3Point1[0] += this->GetFinalInnerRadius();
	v3Point1[3] = 0;
	
	v3Point2[0] = v3Point1[0]*cos(Vista::DegToRad(100.0f)) - v3Point1[1]*sin(Vista::DegToRad(100.0f));
	v3Point2[1] = v3Point1[1]*cos(Vista::DegToRad(100.0f)) + v3Point1[0]*sin(Vista::DegToRad(100.0f));
	v3Point2[2] = v3Point1[2];
	v3Point2[3] = 0;

	// now adjust orientation of our points
	v3Point1 =	m_oLastSensorFrame.qOrientation.Rotate(v3Point1);
	v3Point2 =	m_oLastSensorFrame.qOrientation.Rotate(v3Point2);
			
	v3Normal = -v3Point1.Cross(v3Point2);

	m_pPlane  = new VistaPlane(m_oLastSensorFrame.v3Position,					// point 0
						    m_oLastSensorFrame.v3Position-v3Point1,			// direction vector 1
						    m_oLastSensorFrame.v3Position-v3Point2,			// direction vector 2
						    v3Normal);

	// needed to show the menu's plane and ... (see below)
	m_matTrans = VistaTransformMatrix (m_oLastSensorFrame.qOrientation, 
										m_oLastSensorFrame.v3Position);
}

/*============================================================================*/
/*  NAME      :   DeleteVisibleFeedback  	                                  */
/*============================================================================*/
void VfaContextMenuController::DeleteVisibleFeedback()
{
	int iNrItems = m_pModel->GetNumIds();
	for (int i = 0; i < iNrItems; i++)
		m_vecItems[i]->SetIsHighlighted(false); // reset line color and width
	
	// all items should get back their normal size
	this->AdjustExtend(iNrItems);
}
/*============================================================================*/
/*  NAME      :   ColorItem				  	                                  */
/*============================================================================*/
void VfaContextMenuController::ColorItem(int iItem)
{
	m_vecItems[iItem]->SetIsHighlighted(true);
}
/*============================================================================*/
/*  NAME      :   SizeItem						                              */
/*============================================================================*/
void VfaContextMenuController::SizeItem(int iItem, bool bNormal)
{
	float f = this->GetCurrentRingRadius();

	if (!bNormal)
		f*=m_fFactorExtend;

	if (iItem == 0)
	{
		m_vecItems[iItem]->SetSize(f * m_fFactorExtend * 0.75f);
		return;
	}


	m_vecItems[iItem]->SetSize(f * m_fFactorExtend);
}

/*============================================================================*/
/*  NAME      :   CloseMenuAfterSelection	                                  */
/*============================================================================*/
void VfaContextMenuController::CloseMenuAfterSelection()
{
	if(m_pModel->GetActiveId() == 0)
	{
		this->DeleteVisibleFeedback();
		return;
	}

	if(!m_bCloseAfterSelection)
	{
		this->DeleteVisibleFeedback();
		return;
	}

	this->Close();
}

/*============================================================================*/
/*  NAME      :   Close					                                      */
/*============================================================================*/
void VfaContextMenuController::Close()
{
	m_bStopAdjust = true;
	m_pModel->SetIsOpen(true);

	this->SetMenuState(MENU_CHANGING_SIZE);
	this->StartEvent(MSG_START_PROCESS);
	
	m_pLineVis->SetVisible(false);
	m_pText->SetVisible(false);

	// It might be possible that an entry was focused when the menu was closed.
	// To force this entry to unfocus will perform a call to unfocus to the
	// last set focused id.
	const int nFocusedId = m_pModel->GetFocusedId();
	m_pModel->SetUnfocusedId(nFocusedId);

	// to be sure, handle isn't highlighted the next time we open menu
	this->DeleteVisibleFeedback();

	m_pManagPts->SetPointerVisible(true);
}

/*============================================================================*/
/*  NAME      :   Set/GetItemExtendFactor			                          */
/*============================================================================*/
bool VfaContextMenuController::SetItemExtendFactor(float fFactor)
{
	if(m_fFactorExtend == fFactor)
		return false;

	m_fFactorExtend = fFactor;
	return true;
}
float VfaContextMenuController::GetItemExtendFactor() const
{
	return m_fFactorExtend;
}


/*============================================================================*/
/*  NAME      :   Set/GetMouseActive				                          */
/*============================================================================*/
bool VfaContextMenuController::SetMouseActive(bool b)
{
	if (m_bUseMouse == b)
		return false;

	m_bUseMouse = b;
	return true;

}
bool VfaContextMenuController::GetMouseActive() const
{
	return m_bUseMouse;
}

/*============================================================================*/
/*  NAME      :   SetOnFocus						                          */
/*============================================================================*/
bool VfaContextMenuController::SetOnFocus(int iActive)
{
	if (m_iState != MENU_ACTIVE)
		return false; 

	int iNr = m_pModel->GetNumIds();
	
	if (iActive < 0 || iActive >= iNr)
	{
		m_iActiveItem = ITEM_UNDEFINED;
		this->UpdateMouseMode();
		this->AdjustExtend(m_pModel->GetNumIds());
		return false;
	}


	m_pModel->SetUnfocusedId(m_iActiveItem);
	m_pModel->SetFocusedId(iActive);

	m_iActiveItem = iActive;


	this->UpdateMouseMode();
	
	for (int i = 0; i< iNr; ++i)
	{
		if(i == iActive)
		{
			this->SizeItem(i, false);
		}
		else
			this->SizeItem(i, true);
	}
	return true;
}

/*============================================================================*/
/*  NAME      :   UpdateMouseMode		                                      */
/*============================================================================*/
void VfaContextMenuController::UpdateMouseMode()
{
	this->ComputeMenuPlane();
	if(m_iActiveItem == ITEM_UNDEFINED)
	{
		VistaVector3D v3Pos;
		this->GetCenter(v3Pos);
		m_pText->SetPosition(v3Pos);
		if(!m_pText->GetVisible())
			m_pText->SetVisible(true);
		if(m_pLineVis->GetVisible())
		m_pLineVis->SetVisible(false);
		return;
	}
	else
	{
		if(m_pText->GetVisible())
		{
			m_pText->SetVisible(false);
		}

		VistaVector3D v3Pt2, v3Center;
		this->GetCenter(v3Center);
		m_pLineModel->SetPoint1(v3Center);
		m_vecItems[m_iActiveItem]->GetPosition(v3Pt2);
		v3Pt2 = m_matTrans.GetInverted().Transform(v3Pt2);
		v3Center = m_matTrans.GetInverted().Transform(v3Center);
		v3Pt2 = (v3Pt2-v3Center).GetNormalized()*0.75f*this->GetFinalInnerRadius();
		v3Pt2 = m_matTrans.Transform(v3Pt2);
		m_pLineModel->SetPoint2(v3Pt2);

		if(!m_pLineVis->GetVisible())
			m_pLineVis->SetVisible(true);
	}

}

/*============================================================================*/
/*  NAME      :   GetSensorMask			                                      */
/*============================================================================*/
int VfaContextMenuController::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_CURSOR_VIS);
}

/*============================================================================*/
/*  NAME      :   GetCommandMask			                                  */
/*============================================================================*/
int VfaContextMenuController::GetCommandMask() const
{
	return (1 << m_iGrabButton) | (1 << m_iOpeningButton);
}

/*============================================================================*/
/*  NAME      :   GetTimeUpdate				                                  */
/*============================================================================*/
bool VfaContextMenuController::GetTimeUpdate() const
{
	return true;
}
/*============================================================================*/
/*  NAME      :   ObserverUpdate                                              */
/*============================================================================*/
void VfaContextMenuController::ObserverUpdate(IVistaObserveable *pObserveable,
	int msg, int ticket)
{
	if (msg == VfaMenuModel::MSG_TIME_TO_CLOSE_AFTER_SELECTION)
	{
		CloseMenuAfterSelection();
	}
	else if(msg == VfaMenuModel::MSG_CLOSE)
	{
		Close();
	}
	else if(msg == VfaMenuModel::MSG_MENU_CLOSED)
	{
		this->AdjustItems(); // to be sure, items size is zero
		this->DeleteVisibleFeedback();
		this->SetMenuState(MENU_INACTIVE);

		if (!m_bHasToBeSetToEnable)
			this->SetIsEnabled(false);
		
		return;
	}
	else if(msg == VfaMenuModel::MSG_MENU_OPEN)
	{
		if(!m_bUseMouse)
			m_pLineVis->SetVisible(true);
		else 
			this->UpdateMouseMode();
		
		this->SetMenuState(MENU_ACTIVE);
		this->AdjustItems();
	}
	
	// we are only interested in changes
	if( msg != VfaMenuModel::MSG_NUMBER_TOKEN_CHANGE)
		return;	

	this->AdjustItems();
}

/*============================================================================*/
/*  NAME      :   OpenMenu		                                               */
/*============================================================================*/
void VfaContextMenuController::OpenMenu ()
{
	m_iLastState = m_iState;

	this->DeleteVisibleFeedback();
	m_pManagPts->SetPointerVisible(false);
	m_bStopAdjust = false;
	this->SetMenuState(MENU_CHANGING_SIZE);
	m_pModel->SetIsOpen(false);
	this->StartEvent(MSG_START_PROCESS);

}

/*============================================================================*/
/*  NAME      :   CloseMenu		                                               */
/*============================================================================*/
void VfaContextMenuController::CloseMenu(bool bEnable)
{
/*	m_iLastState = m_iState;
	this->SetMenuState(MENU_CHANGING_SIZE);
	m_pText->SetVisible(false);
	m_pLineVis->SetVisible(false);
	this->DeleteVisibleFeedback();
	m_pManagPts->SetPointerVisible(true);
		
	this->SetCurrentInnerRadius(0.0f);
	this->SetCurrentRingRadius (0.0f);
	this->AdjustItems();
	m_pModel->SetMenuClosed();
*/
	m_iLastState = m_iState;
	m_bHasToBeSetToEnable = bEnable;

	this->DeleteVisibleFeedback();
	m_pText->SetVisible(false);
	m_pLineVis->SetVisible(false);
	m_pManagPts->SetPointerVisible(true);
	m_bStopAdjust = false;
	this->SetMenuState(MENU_CHANGING_SIZE);
	m_pModel->SetIsOpen(true);
	this->StartEvent(MSG_START_PROCESS);

}

/*============================================================================*/
/*  NAME      :   SetGrabButton                                               */
/*============================================================================*/
void VfaContextMenuController::SetGrabButton (int iSlot)
{
	m_iGrabButton = iSlot;
}

/*============================================================================*/
/*  NAME      :   SetOpenButton                                               */
/*============================================================================*/
void VfaContextMenuController::SetOpenButton (int iSlot)
{
	m_iOpeningButton = iSlot;
}

/*============================================================================*/
/*  NAME      :   GetProperties                                               */
/*============================================================================*/
VfaContextMenuController::VfaContextMenuControllerProps*
VfaContextMenuController::GetProperties() const
{
	return m_pCtlProps;
}


/*============================================================================*/
/*  NAME      :   AdjustItems		                                          */
/*============================================================================*/
void VfaContextMenuController::AdjustItems()
{
	// we only need to adjust when values changed or when we see the menu
	if(m_iState == MENU_INACTIVE)
		return; 
	if (m_bStopAdjust && m_iState == MENU_ACTIVE)
		return;

	if (!m_vecItems[0]->GetVisible())
	{
		for (int i = 0; i < m_pModel->GetNumIds(); ++i)
				m_vecItems[i]->SetVisible(true);
	}
	
	// now adjust
	int iNrOfHandles = m_pModel->GetNumIds();
	this->AdjustExtend(iNrOfHandles);
	this->AdjustTranslation(iNrOfHandles);
	this->AdjustRotation(iNrOfHandles);
	this->SetChangeInformation();
}

/*============================================================================*/
/*  NAME      :   AdjustMenu		                                          */
/*============================================================================*/
void VfaContextMenuController::AdjustMenu()
{
	m_oLastSensorFrame.qOrientation = m_qQuat;
	m_oLastSensorFrame.v3Position = VistaVector3D(m_fCenter[0], m_fCenter[1],
		m_fCenter[2]+0.01f, 1.0f); 

	int iNrOfHandles = m_pModel->GetNumIds();
	this->AdjustTranslation(iNrOfHandles);
	this->AdjustRotation(iNrOfHandles);
}

/*============================================================================*/
/*  NAME      :   AdjustExtend		                                          */
/*============================================================================*/
void VfaContextMenuController::AdjustExtend(int iNrOfHandles)
{
//	float f = m_pModel->GetRingRadius();

	for(int i = 0; i < iNrOfHandles; ++i)
		this->SizeItem(i, true);
}
/*============================================================================*/
/*  NAME      :   AdjustRotation	                                          */
/*============================================================================*/
void VfaContextMenuController::AdjustRotation(int iNrOfHandles)
{
	for(int i = 0; i < iNrOfHandles; ++i)
	{
		m_vecItems[i]->SetRotation(m_oLastSensorFrame.qOrientation);
	}
}
/*============================================================================*/
/*  NAME      :   AdjustTranslation                                           */
/*============================================================================*/
void VfaContextMenuController::AdjustTranslation(int iNrOfHandles)
{
	// current distance between center of partial disk and quadcenter
	const float m_fDiff = this->GetCurrentInnerRadius()
		+ 0.5f * this->GetCurrentRingRadius();
	
	// angle to turn further to reach quads new position
	// attention angle has to be in radian, not in degrees
	const float alpha = 180.0f / static_cast<float>(iNrOfHandles + 1)
		* (Vista::Pi / 180.0f);
	
	// variable to save the angle for current rotation, because we will need
	// a multiple of alpha
	float alphaXTimes = 0.0f;

	// startpoint, assuming that the origion is the menu's center
	VistaVector3D v3Start(m_fDiff, 0.0f, 0.01f);	
	
	// now computer center for all quads
	// holds the position offset from the widget center for the current quad
	// set the fourth component to 0.0 to indicate it is an offset _vector_
	VistaVector3D v3Rotated(0.0f, 0.0f, 0.0f, 0.0f);
	for(int i = 0; i < iNrOfHandles; ++i)
	{	
		// angle to rotate around
		alphaXTimes = static_cast<float>(i + 1) * alpha;

		// get new position of quad (rotation in x-y-plane)
		v3Rotated[0] = -1.0f * v3Start[0] * cos(alphaXTimes);
		v3Rotated[1] = v3Start[0] * sin(alphaXTimes);
		v3Rotated[2] = v3Start[2];
		
		// now get the position in vis space
		v3Rotated =	m_oLastSensorFrame.qOrientation.Rotate(v3Rotated);
		
		// set new point, but attention: now we need the real menu's center
		// so: translate the current quad-center		
		m_vecItems[i]->SetPosition(v3Rotated + m_oLastSensorFrame.v3Position);
	}
}

/*============================================================================*/
/*  NAME      :   UpdateSize     										      */
/*============================================================================*/
void VfaContextMenuController::UpdateSize(double dDeltaTime)
{
	float fInnerRadius = this->GetFinalInnerRadius();
	float fRingRadius = this->GetFinalRingRadius();

	// test whether opening/closing process is realy an animation
	if (this->GetTimeToOpenCloseMenu() == 0.0f)
	{
	//	vstr::debugi() << "0.0 no animation"<< endl;
	//	vstr::debugi() << "State " << m_iState << endl;
		 if(m_iState == MENU_CHANGING_SIZE)
		 {
				// menu size should grow
				if(!m_pModel->GetIsOpen())
				{
#ifdef DEBUG
					vstr::debugi() << "[VfaContextMenuCollector] Open menu" << endl;
#endif
					this->SetCurrentInnerRadius(fInnerRadius);
					this->SetCurrentRingRadius (fRingRadius);
					m_pModel->SetMenuOpen();
				}
				else // menu size should decrease
				{
#ifdef DEBUG
					vstr::debugi() << "[VfaContextMenuCollector] close menu" << endl;
#endif
					this->SetCurrentInnerRadius(0.0f);
					this->SetCurrentRingRadius (0.0f);
					m_pModel->SetMenuClosed();
				}
		 }
		m_bStopAdjust = true;
		return;	
	}


	//timefunction
	float fCoefficient = (float) dDeltaTime/this->GetTimeToOpenCloseMenu();
	
	// time is mapped to a scalar from 0 to 1
	if(fCoefficient >= 0.0f && fCoefficient <= 1.0f)
	{
		m_bChangeByLastPass = true;

		// menu size should grow
		if(!m_pModel->GetIsOpen())
		{
			this->SetCurrentInnerRadius(fCoefficient * fInnerRadius);
			this->SetCurrentRingRadius (fCoefficient * fRingRadius);
			return;
		}
		else // menu size should decrease
		{
			this->SetCurrentInnerRadius(fInnerRadius - (fCoefficient * fInnerRadius));
			this->SetCurrentRingRadius (fRingRadius -  (fCoefficient * fRingRadius ));
			return;
		}
	}
	else if(m_bChangeByLastPass)
	{
		m_bChangeByLastPass = false;

		if(m_pModel->GetIsOpen())
		{
			this->SetCurrentInnerRadius(0.0f);
			this->SetCurrentRingRadius(0.0f);
			m_pModel->SetMenuClosed();
		}
		else
			m_pModel->SetMenuOpen();
	}
}

/*============================================================================*/
/*  NAME      :   SetCenter(float fC[])                                       */
/*============================================================================*/
bool VfaContextMenuController::SetCenter(float fC[])
{
	m_fCenter[0] = fC[0];
	m_fCenter[1] = fC[1];
	m_fCenter[2] = fC[2];
	
	return true;
}
bool VfaContextMenuController::SetCenter(const VistaVector3D &v3C)
{
	float fC[3];
	v3C.GetValues(fC);
	return this->SetCenter(fC);
}

/*============================================================================*/
/*  NAME      :   GetCenter(float fC[3])							          */
/*============================================================================*/
void VfaContextMenuController::GetCenter(float fC[3]) const
{
	fC[0] = m_fCenter[0];
	fC[1] = m_fCenter[1];
	fC[2] = m_fCenter[2];
}
void VfaContextMenuController::GetCenter(double dC[3]) const
{
	dC[0] = m_fCenter[0];
	dC[1] = m_fCenter[1];
	dC[2] = m_fCenter[2];
}
void VfaContextMenuController::GetCenter(VistaVector3D &v3C) const
{
	v3C[0] = m_fCenter[0];
	v3C[1] = m_fCenter[1];
	v3C[2] = m_fCenter[2];
}

/*============================================================================*/
/*  NAME      :   Set/GetOrientation										  */
/*============================================================================*/
bool VfaContextMenuController::SetOrientation(const VistaQuaternion &quat)
{
	m_qQuat = quat;
	return true;
}
void VfaContextMenuController::GetOrientation(VistaQuaternion &quat) const
{
	quat = m_qQuat;
}

/*============================================================================*/
/*  NAME      :   Set/GetCurrentInnerRadius(float f)						  */
/*============================================================================*/
bool VfaContextMenuController::SetCurrentInnerRadius(float f)
{
	if(f < 0) // negativ radius? good joke :)
		return false;

	m_fCurrentInnerRadius = f;
	return true;
}
float VfaContextMenuController::GetCurrentInnerRadius() const
{
	return m_fCurrentInnerRadius;
}
/*============================================================================*/
/*  NAME      :   Set/GetFinalInnerRadius(float f)							  */
/*============================================================================*/
bool VfaContextMenuController::SetFinalInnerRadius(float f)
{
	if(f < 0) // negative radius? good joke :)
		return false;

	m_fFinalInnerRadius = f;
	return true;
}
float VfaContextMenuController::GetFinalInnerRadius() const
{
	return m_fFinalInnerRadius;
}
/*============================================================================*/
/*  NAME      :   Set/GetFinalRingRadius(float f)							  */
/*============================================================================*/
bool VfaContextMenuController::SetFinalRingRadius(float f)
{
	if(f < 0) // negative radius? good joke :)
		return false;

	m_fFinalRingRadius = f;
	return true;
}
float VfaContextMenuController::GetFinalRingRadius() const
{
	return m_fFinalRingRadius;
}
/*============================================================================*/
/*  NAME      :   Set/GetCurrentRingRadius(float f)							  */
/*============================================================================*/
bool VfaContextMenuController::SetCurrentRingRadius(float f)
{
	if(f < 0) // negative radius? good joke :)
		return false;

	m_fCurrentRingRadius = f;
	return true;
}
float VfaContextMenuController::GetCurrentRingRadius() const
{
	return m_fCurrentRingRadius;
}



/*============================================================================*/
/*  NAME      :   Set/GetTextureForId		                                  */
/*============================================================================*/
bool VfaContextMenuController::SetTextureForId(int id, VistaTexture *pTex)
{
	if (id < 0 || id >= m_pModel->GetNumIds())
		return false;

	m_vecItems[id]->SetTexture(pTex);

	return  true;
}
VistaTexture * VfaContextMenuController::GetTextureForId(int id) const
{
	if (id < 0 || id >= m_pModel->GetNumIds())
		return NULL;
	
	return m_vecItems[id]->GetTexture();
}

/*============================================================================*/
/*  NAME    :    Set/GetVisEnabled                                            */
/*============================================================================*/
void VfaContextMenuController::SetVisEnabled(bool b)
{
	if(b == this->GetVisEnabled())
		return;

	int iNrOfHandles = m_pModel->GetNumIds();
	
	for (int i = 0; i <  iNrOfHandles; i++)
		m_vecItems[i]->SetVisible(b);
}
bool VfaContextMenuController::GetVisEnabled() const
{
	return	m_vecItems[0]->GetVisible();
}

/*============================================================================*/
/*  NAME    :    SetRulerState                                                */
/*============================================================================*/
void VfaContextMenuController::SetMenuState(int iNewState)
{
	int iOldState = m_iState;
	m_iState = iNewState;
	this->PrintState(iOldState, iNewState);
}
/*============================================================================*/
/*  NAME    :    PrintState                                                   */
/*============================================================================*/
void VfaContextMenuController::PrintState(int iOldState, int iCurrentState) const
{
	std::string strOldState = "UNKNOWN";
	std::string strNewState = "UNKNOWN";
	switch(iOldState)
	{
		case MENU_INACTIVE: strOldState = "INACTIVE"; break;
		case MENU_ACTIVE: strOldState = "ACTIVE"; break;
		case MENU_CHANGING_SIZE: strOldState = "CHANGING SIZE"; break;
		case MENU_TARGET_TOUCHED: strOldState = "TARGET_TOUCHED"; break;
		case MENU_DISABLED: strOldState = "MENU_DISABLED"; break;
		default:
			break;
	}
	switch(iCurrentState)	
	{
		case MENU_INACTIVE: strNewState = "INACTIVE"; break;
		case MENU_ACTIVE: strNewState = "ACTIVE"; break;
		case MENU_CHANGING_SIZE: strNewState = "CHANGING SIZE"; break;
		case MENU_TARGET_TOUCHED: strNewState = "TARGET_TOUCHED"; break;
		case MENU_DISABLED: strNewState = "MENU_DISABLED"; break;
		default:
			break;
	}
#ifndef NDEBUG
	vstr::outi()<<"[VfaContextMenuController] MenuState change : "<<strOldState.c_str()<<" -> "<<strNewState.c_str()<<endl;
#endif
}


/*============================================================================*/
/*  IMPLEMENTATION      VfaContextMenuControllerProps                        */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaContextMenuController::VfaContextMenuControllerProps");

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaContextMenuController::VfaContextMenuControllerProps::VfaContextMenuControllerProps(VfaContextMenuController *pMenuCtrl)
:	m_pMenuCtrl(pMenuCtrl)
{}

VfaContextMenuController::VfaContextMenuControllerProps::~VfaContextMenuControllerProps()
{}

/*============================================================================*/
/*  NAME      :   GetReflectionableType                                       */
/*============================================================================*/
std::string VfaContextMenuController::VfaContextMenuControllerProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaContextMenuController::VfaContextMenuControllerProps::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  END OF FILE "VfaContextMenuController.cpp"                                */
/*============================================================================*/
