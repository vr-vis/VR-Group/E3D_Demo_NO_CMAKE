/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAMENUITEMCOLLECTION_H
#define _VFAMENUITEMCOLLECTION_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <vector>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaMenuWidget;
class VistaTexture;
class IVistaExplicitCallbackInterface;
class VfaCallbackFixingMenu;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*  Used to fill the items of the menu one by one.
*/
class VISTAFLOWLIBAUXAPI VfaMenuItemCollection : public VflObserver
{
public:
	VfaMenuItemCollection(VfaMenuWidget *pWidget,
		const std::string &strFileNameFix, const std::string &strFileNameNoFix);
	virtual ~VfaMenuItemCollection();
	
	int AddItem(const std::string &strMsg, const std::string &strFileName,
			IVistaExplicitCallbackInterface *pCallback);

	int GetNumItems();

	VistaTexture* CreateTexture(const std::string &strMsg);

// IVistaObserver
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg,
		int ticket);


protected:

private:
	VfaMenuWidget			*m_pWidget;
	VistaTexture			*m_pTexture;
	std::vector<IVistaExplicitCallbackInterface*> m_vecCallback;

	VfaCallbackFixingMenu	*m_pCallbackFix;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAMENUITEMCOLLECTION_H
