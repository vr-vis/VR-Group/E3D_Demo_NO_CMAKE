/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES					                                                  */
/*============================================================================*/
#include "VfaMenuItemCollection.h"
#include "VfaMenuWidget.h"
#include "VfaCallbackFixingMenu.h"

#include <VistaAspects/VistaExplicitCallbackInterface.h>
#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaOGLUtils.h>

#include <cassert>

using namespace std;

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaMenuItemCollection::VfaMenuItemCollection(VfaMenuWidget *pWidget,
	const std::string &strFileNameFix, const std::string &strFileNameNoFix)
:   m_pWidget(pWidget),
	m_pTexture(0),
	m_pCallbackFix(0)
{
	// attach to the model
	this->Observe(m_pWidget->GetModel());

	assert(!strFileNameFix.empty() && !strFileNameNoFix.empty());

	// fill menu with a default texture
	static GLubyte checkImage[64][64][4];
	int c = 0;
	for (int i = 0; i< 64; ++i)
	{
		for (int j = 0; j< 64; ++j)
		{
			c = ((((i&0x8)==0)^((j&0x8)==0)))*255;
			checkImage[i][j][0] = (GLubyte) c;
			checkImage[i][j][1] = (GLubyte) c;
			checkImage[i][j][2] = (GLubyte) c;
			checkImage[i][j][3] = (GLubyte) 255;
		}
	}

	// note: the texture c'tor implicitlity binds the texture after creation
	m_pTexture = new VistaTexture(GL_TEXTURE_2D);
	
	glTexParameteri(m_pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(m_pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(m_pTexture->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(m_pTexture->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(m_pTexture->GetTarget(), 0, GL_RGBA, 64, 64, 0, GL_RGBA,
		GL_UNSIGNED_BYTE, checkImage);
	
	m_pTexture->Unbind();

	const int iNr = m_pWidget->GetModel()->GetNumIds();
	for (int i = 1; i < iNr; ++i)
	{
		m_pWidget->SetTextureForId(i, m_pTexture);
	}

	// default item
	m_pCallbackFix = new VfaCallbackFixingMenu(m_pWidget,
		this->CreateTexture(strFileNameNoFix),
		this->CreateTexture(strFileNameFix));

	m_vecCallback.push_back(m_pCallbackFix);
	m_pWidget->GetModel()->SetMessageForId(0, "menu (not) fixed");
}

VfaMenuItemCollection::~VfaMenuItemCollection()
{
	delete m_pTexture;
	delete m_pCallbackFix;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/* NAME: AddItem                                                              */
/*============================================================================*/
int VfaMenuItemCollection::AddItem(const std::string &strMsg,
								  const std::string &strFileName,
								  IVistaExplicitCallbackInterface *pCallback)
{
	// exception handling: only add new items if maximum in not reached
	unsigned int ids = m_pWidget->GetModel()->GetNumIds();
	if (m_vecCallback.size() >= ids)
		return -1;

	// add new callback
	m_vecCallback.push_back(pCallback);
	// index of last element = Nr of Ele - 1
	int iItemNr = static_cast<int>(m_vecCallback.size()-1);

	// add message (currently only used in debug mode)
	m_pWidget->GetModel()->SetMessageForId(iItemNr, strMsg);
		
	m_pWidget->SetTextureForId(iItemNr, this->CreateTexture(strFileName));

	return iItemNr+1;
}

/*============================================================================*/
/* NAME: CreateTexture                                                        */
/*============================================================================*/
VistaTexture* VfaMenuItemCollection::CreateTexture(
	const std::string &strFileName)
{
	// image meta data
	int iWidth		= -1;
	int iHeight		= -1;
	int iChannels	= -1;

	// read texture data from image file
	unsigned char* pTexData = 
		VistaOGLUtils::LoadImageFromFile(strFileName, iWidth, iHeight,
			iChannels);

	// note: implicit bind on texture creation
	VistaTexture *pTexture = new VistaTexture(GL_TEXTURE_2D);
		
	glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP);
	
	if(iChannels == 3)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, iWidth, iHeight, 0, GL_RGB,
			GL_UNSIGNED_BYTE, pTexData);
	}
	else if(iChannels == 4)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, iWidth, iHeight, 0, GL_RGBA,
			GL_UNSIGNED_BYTE, pTexData);
	}
	else
	{
		assert(!"Files with !=3 and !=4 color components are not supported.");
	}

	// let's auto generate the mip map levels for the texture
	glGenerateMipmap(GL_TEXTURE_2D);

	// don't forget to release the texture to avoid state ogl pollution
	pTexture->Unbind();

	// @todo: check code! data allocation and deallocation happens at two
	//		  different locations (probably not in the same module).
	//		  is this the desired way?
	return pTexture;
}

/*============================================================================*/
/* NAME: GetNumItems	                                                      */
/*============================================================================*/
int VfaMenuItemCollection::GetNumItems()
{
	return m_pWidget->GetModel()->GetNumIds();
}

/*============================================================================*/
/* NAME: ObserverUpdate                                                       */
/*============================================================================*/
void VfaMenuItemCollection::ObserverUpdate(IVistaObserveable *pObserveable,
	int msg, int ticket)
{
	// only interested if a token (an item) is chosen
	if (msg != VfaMenuModel::MSG_TOKEN_CHOSEN)
		return;
	
#ifdef DEBUG
	std::string msgActiveId = m_pWidget->GetModel()->GetMessageForActiveId();
	vstr::debugi() << "[VfaMEnuItemCollection] Chosen item: " << m_pWidget->GetModel()->GetActiveId() << " " << msgActiveId << endl;
#endif

	// do, whatever the user wants to do here
	int i = m_pWidget->GetModel()->GetActiveId();
	if(i < static_cast<int>(m_vecCallback.size()))
		m_vecCallback[i]->Do();
	else
		vstr::warnp() << "[VfaMEnuItemCollection] PAY ATTENTION: Item is empty. No Action possible."<< endl;
}
/*============================================================================*/
/*  END OF FILE "VfaMenuItemCollection.cpp"								  */
/*============================================================================*/
