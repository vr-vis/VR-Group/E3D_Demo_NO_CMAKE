/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACALLBACKNAVIGATIONFACTOR_H
#define _VFACALLBACKNAVIGATIONFACTOR_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaExplicitCallbackInterface.h>
#include <VistaOGLExt/VistaTexture.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaContextMenuController;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*    To choose items in contextMenu you just look at the orientation (oFrameData.qOrientation)
*    of your input device (if you use phantom, flystick etc.)
*	 callback changes the factor, which is multiplied with the angle (oFrameData.qOrientation[0])
*/
class VISTAFLOWLIBAUXAPI VfaCallbackNavigationFactor : public IVistaExplicitCallbackInterface
{
public:

	enum{
		INCREASE_FACTOR = 0,
		DECREASE_FACTOR
	};

	VfaCallbackNavigationFactor(VfaContextMenuController *pCtr,
							int iMode, float fDiff);
	virtual ~VfaCallbackNavigationFactor();

	// IVistaObserver
	bool Do();

protected:
private:
	VfaContextMenuController *m_pCtr;
	int						  m_iMode;
	float					  m_fDiff;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFACALLBACKNAVIGATIONFACTOR_H
