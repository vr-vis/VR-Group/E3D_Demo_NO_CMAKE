/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACALLBACKFIXINGMENU_H
#define _VFACALLBACKFIXINGMENU_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaExplicitCallbackInterface.h>
#include <VistaOGLExt/VistaTexture.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaMenuWidget;
class VistaTexture;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*	 callback to fix and release the menu
*    User needn't deal with this class
*/
class VISTAFLOWLIBAUXAPI VfaCallbackFixingMenu : public IVistaExplicitCallbackInterface
{
public:
	VfaCallbackFixingMenu(VfaMenuWidget *pWidgtet,
							VistaTexture *pNoFixTexture,
							VistaTexture *pFixTexture);
	virtual ~VfaCallbackFixingMenu();

	// IVistaObserver
	bool Do();

protected:
private:
	VfaMenuWidget *m_pWidget;

	VistaTexture* m_pNoFixTexture;
	VistaTexture* m_pFixTexture;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFACALLBACKFIXINGMENU_H

