/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaQuadAdapter.h"
#include "VfaContextMenuIconAdapter.h"
#include "../VfaQuadVis.h"
#include "../Plane/VfaPlaneModel.h"

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaQuadAdapter::VfaQuadAdapter(VfaQuadVis *pVis, VflRenderNode *pRenderNode)
	: IVfaContextMenuIconAdapter(pVis),
		m_pVis(pVis),
		m_pRenderNode(pRenderNode)
{
	m_pVis->Init();
	m_pVis->GetProperties()->SetFillPlane(true);
	m_pVis->GetProperties()->SetLineColor(m_fNormalColor);
	m_pVis->GetProperties()->SetLineWidth(m_fNormalLineWidth);
}

VfaQuadAdapter::~VfaQuadAdapter()
{}

/*============================================================================*/
/*  IMPLEMENTATION						                                      */
/*============================================================================*/
/*============================================================================*/
/*  NAME: Clone							                                      */
/*============================================================================*/
IVfaContextMenuIconAdapter* VfaQuadAdapter::Clone()
{
	VfaQuadAdapter *pQuadAdapter = new VfaQuadAdapter(new VfaQuadVis(new VfaPlaneModel),
													m_pRenderNode);
	pQuadAdapter->SetHighlightColor(this->GetHighlightColor());
	pQuadAdapter->SetNormalColor(this->GetNormalColor());
	pQuadAdapter->SetNormalLineWidth(this->GetNormalLineWidth());
	pQuadAdapter->SetHighlightLineWidth(this->GetHighlightLineWidth());
	m_pRenderNode->AddRenderable(pQuadAdapter->GetVis());
	return pQuadAdapter;
}

/*============================================================================*/
/*  NAME: Set/GetPosition							                          */
/*============================================================================*/
bool VfaQuadAdapter::SetPosition (const VistaVector3D &v3Pos)
{
	return m_pVis->GetModel()->SetTranslation(v3Pos);
}
void VfaQuadAdapter::GetPosition (VistaVector3D &v3Pos) const
{
	m_pVis->GetModel()->GetTranslation(v3Pos);
}
bool VfaQuadAdapter::SetPosition (float fC[3])
{
	return m_pVis->GetModel()->SetTranslation(fC);
}
void VfaQuadAdapter::GetPosition (float fC[3]) const
{
	m_pVis->GetModel()->GetTranslation(fC);
}

/*============================================================================*/
/*  NAME: Set/GetSize								                          */
/*============================================================================*/
bool VfaQuadAdapter::SetSize (float fSize)
{
	return m_pVis->GetModel()->SetExtents(fSize, fSize);
}
float VfaQuadAdapter::GetSize () const
{
	float fWidth, fHights;
	m_pVis->GetModel()->GetExtents(fWidth, fHights);
	return fWidth;
}

/*============================================================================*/
/*  NAME: Set/GetOrientation						                          */
/*============================================================================*/
bool VfaQuadAdapter::SetOrientation (const VistaVector3D &v3Normal)
{
	m_pVis->GetModel()->SetNormal(v3Normal);
	return true;
}
void VfaQuadAdapter::GetOrientation (VistaVector3D &v3Normal) const
{
	float fC[3];
	this->GetOrientation(fC);
	v3Normal = VistaVector3D(fC);
}
bool VfaQuadAdapter::SetOrientation (float fC[3])
{
	VistaVector3D v3Ori (fC);
	return this->SetOrientation(v3Ori);
}
void VfaQuadAdapter::GetOrientation (float fC[3]) const
{
	m_pVis->GetModel()->GetNormal(fC);
}

/*============================================================================*/
/*  NAME: Set/GetRotation							                          */
/*============================================================================*/
bool VfaQuadAdapter::SetRotation(const VistaQuaternion &qRotation)
{
	return m_pVis->GetModel()->SetRotation(qRotation);
}
void VfaQuadAdapter::GetRotation(VistaQuaternion &qRot) const
{
	m_pVis->GetModel()->GetRotation(qRot);
}


/*============================================================================*/
/*  NAME: Set/GetIsHighlighted						                          */
/*============================================================================*/
bool VfaQuadAdapter::SetIsHighlighted (bool bHighlight)
{
	if(bHighlight)
	{
		m_pVis->GetProperties()->SetLineColor(m_fHighlightColor);
		m_pVis->GetProperties()->SetLineWidth(m_fHighlightLineWidth);
	}
	else
	{
		m_pVis->GetProperties()->SetLineColor(m_fNormalColor);
		m_pVis->GetProperties()->SetLineWidth(m_fNormalLineWidth);
	}
	return true;
}
bool VfaQuadAdapter::GetIsHighlighted () const
{
	if (m_pVis->GetProperties()->GetLineWidth() == m_fNormalLineWidth)
		return false;
	else 
		return true;
}



/*============================================================================*/
/*  NAME: Set/GetTexture							                          */
/*============================================================================*/
bool VfaQuadAdapter::SetTexture(VistaTexture *pTex)
{
	m_pVis->SetTexture(pTex);
	return true;
}
VistaTexture* VfaQuadAdapter::GetTexture() const
{
	return m_pVis->GetTexture();
}

/*============================================================================*/
/*  END OF FILE "VfaQuadAdapter.cpp"										  */
/*============================================================================*/
