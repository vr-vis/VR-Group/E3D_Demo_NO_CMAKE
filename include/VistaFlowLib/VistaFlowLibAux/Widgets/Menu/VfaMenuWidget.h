/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAMENUWIDGET_H
#define _VFAMENUWIDGET_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidget.h"
#include "../Menu/VfaMenuModel.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*!
 *	superclass for all menu widgets
 */
class VISTAFLOWLIBAUXAPI VfaMenuWidget : public IVfaWidget
{
public:
	VfaMenuWidget(int iNrOfIds, VflRenderNode *pRenderNode, bool bUnFix = true);
	virtual ~VfaMenuWidget();

	VfaMenuModel* GetModel() const;
	IVflRenderable* GetView() const;

	virtual void SetTextureForId(int id, VistaTexture *pTex) = 0;
	virtual void IsClosingAfterSelection( bool b) = 0;
	virtual bool GetClosingAfterSelection() = 0;

	
protected:
	VfaMenuModel		*m_pMenuModel;

private:
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAMENUWIDGET_H
