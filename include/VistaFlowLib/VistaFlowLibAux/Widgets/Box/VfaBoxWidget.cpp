/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaBoxWidget.h"
#include "VfaBoxModel.h"
#include "../VfaBoxVis.h"
#include "../VfaSphereVis.h"
#include "../VfaSphereHandle.h"
#include "VfaBoxController.h"

#include <VistaAspects/VistaObserver.h>
#include <VistaAspects/VistaPropertyAwareable.h>

#include <VistaBase/VistaVectorMath.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaBoxWidget::VfaBoxWidget(VflRenderNode *pRenderNode)
:	IVfaWidget(pRenderNode),
	m_pBoxModel(new VfaBoxModel),
	m_bEnabled(true)
{
	m_pBoxView = new VfaBoxVis(m_pBoxModel);
	m_pBoxController = new VfaBoxController(m_pBoxModel, pRenderNode);

	if(m_pBoxView->Init())
		pRenderNode->AddRenderable(m_pBoxView);

	float fC[4] = {0.0f, 0.0f, 1.0f, 1.0f};
	m_pBoxView->GetProperties()->SetLineColor(fC);
	m_pBoxView->GetProperties()->SetFaceColor(fC);
	m_pBoxView->GetProperties()->SetDrawFilledFaces(false);
	
	m_pBoxController->Observe(GetModel());

	m_pBoxModel->Notify();
}

VfaBoxWidget::~VfaBoxWidget()
{
	delete m_pBoxController;
	delete m_pBoxModel;
	m_pBoxView->GetRenderNode()->RemoveRenderable(m_pBoxView);
	delete m_pBoxView;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaBoxWidget::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pBoxView->SetVisible(m_bEnabled);
	m_pBoxController->SetIsEnabled(m_bEnabled);
}
bool VfaBoxWidget::GetIsEnabled() const
{
	return m_bEnabled;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsVisible                                            */
/*                                                                            */
/*============================================================================*/
void VfaBoxWidget::SetIsVisible(bool b)
{
	m_bVisible = b;
	m_pBoxView->SetVisible(m_bVisible);
}
bool VfaBoxWidget::GetIsVisible() const
{
	return m_bVisible;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel                                                    */
/*                                                                            */
/*============================================================================*/
VfaBoxModel* VfaBoxWidget::GetModel() const
{
	return m_pBoxModel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetView                                                     */
/*                                                                            */
/*============================================================================*/
VfaBoxVis* VfaBoxWidget::GetView() const
{
	return m_pBoxView;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetController                                               */
/*                                                                            */
/*============================================================================*/
VfaBoxController* VfaBoxWidget::GetController() const
{
	return m_pBoxController;
}
/*============================================================================*/
/*  END OF FILE "VfaBoxWidget.cpp"                                            */
/*============================================================================*/
