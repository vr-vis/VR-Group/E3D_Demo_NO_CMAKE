/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFADRAGBOXWIDGET_H
#define _VFADRAGBOXWIDGET_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidget.h"
#include "../VfaBoxVis.h"
//we need these includes to get the inheritance info right.
#include "VfaBoxModel.h"
#include "VfaDragBoxController.h"
#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VflRenderNode;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A DragBoxWidget is a box-shaped widget which behaves like a "lasso" style
 * 3D object. It is used to mark a cuboid region in 3D space by 
 * clicking&dragging. In addition, the entire box may be translated 
 * (click&drag inside of a once created box) or each dimension may be resized 
 * independently (click&drag next to the respective face)
 */
class VISTAFLOWLIBAUXAPI VfaDragBoxWidget : public IVfaWidget
{
public:
	VfaDragBoxWidget(VflRenderNode *pRenderNode, float fWidgetScale=1.0f);
	virtual ~VfaDragBoxWidget(); 

	/** 
	 * NOTE : Handle visibility and reaction to events separately.
	 *        Thus the widget may be visible but not being interacted with!
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;

	bool GetIsInteracting() const;
	
	virtual VfaBoxModel *GetModel() const;
	virtual VfaBoxVis* GetView() const;
	virtual VfaDragBoxController* GetController() const;

	void SetDragButton(int iButtonID);
	int GetDragButton() const;

	/**
	 * provide a reflectionable interface to control the
	 * main visual properties of this widget
	 *
	 * NOTE: the box's properties are no longer supported here!
	 *
	 * @TODO fix/complete this
	 */
	class VISTAFLOWLIBAUXAPI VfaDragBoxWidgetProps : public IVistaReflectionable
	{
		friend class VfaDragBoxWidget;

	public:
		enum{
			MSG_CLAMPING_CHANGED = IVistaReflectionable::MSG_LAST,
			MSG_BOX_COLOR_CHANGED,
			MSG_SHOW_LAST_CHANGED,
			MSG_LAST_COLOR_CHANGED,
			MSG_HIGHLIGHT_COLOR_CHANGED,
			MSG_LAST
		};

		VfaDragBoxWidgetProps(VfaDragBoxWidget *pWidget);
		virtual ~VfaDragBoxWidgetProps();

		/**
		 * control whether the widget will clamp the possible box size to 
		 * the given dimension
		 */
		bool SetClampBox(float fBox[6]);
		void GetClampBox(float fBox[6]) const;

		bool SetClamping(bool b);
		bool GetClamping() const;
		/**
		 * the user may choose to draw the last box as long 
		 * as the new one is not fixed yet.
		 */
		bool SetShowLastState(bool b);
		bool GetShowLastState() const;

		bool SetCurrentBoxColor(const VistaColor &c);
		VistaColor GetCurrentBoxColor() const;
		void SetCurrentBoxColor(float fC[4]);
		void GetCurrentBoxColor(float fC[4]) const;

		bool SetLastBoxColor(const VistaColor &c);
		VistaColor GetLastBoxColor() const;
		void SetLastBoxColor(float fC[4]);
		void GetLastBoxColor(float fC[4]) const;

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;


	private:
		VfaDragBoxWidget *m_pWidget;
	};

	VfaDragBoxWidgetProps *GetProperties() const;
protected:

private:
	VfaBoxModel *m_pBoxModel;
	VfaDragBoxController *m_pWidgetCtl;
	VfaBoxVis *m_pBoxVis;
	
	VfaDragBoxWidgetProps *m_pProps;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaDragBoxWIDGET_H
