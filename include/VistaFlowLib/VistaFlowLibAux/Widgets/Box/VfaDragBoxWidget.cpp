/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <cstring>

#include "VfaDragBoxWidget.h"

#include "VfaDragBoxController.h"
#include "VfaBoxModel.h"
#include "VfaBoxConstraints.h"

#include "../VfaBoxVis.h"


#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaDragBoxWidget::VfaDragBoxWidget(VflRenderNode *pRenderNode, float fWidgetScale)
:	IVfaWidget(pRenderNode),
	m_pBoxModel(new VfaBoxModel),
	m_pWidgetCtl(new VfaDragBoxController(m_pBoxModel, pRenderNode, fWidgetScale)),
	m_pBoxVis(new VfaBoxVis(m_pBoxModel)),
	m_pProps(NULL)
{
	//
	m_pProps = new VfaDragBoxWidget::VfaDragBoxWidgetProps(this);
	//create empty box
	float fB[6] = {-0.5f, 0.5f, -0.5f, 0.5f, -0.5f, 0.5f};
	m_pBoxModel->SetAABox(fB);

	m_pBoxVis->Init();
	pRenderNode->AddRenderable(m_pBoxVis);
}

VfaDragBoxWidget::~VfaDragBoxWidget()
{
	//m_pBoxVis->GetVisController()->UnregisterVisObject(m_pBoxVis);
	//delete m_pBoxVis;

	delete m_pWidgetCtl;
	delete m_pBoxModel;
	delete m_pProps;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxWidget::SetIsEnabled(bool b)
{
	//propagate this to the widget controller in order to set 
	//the vis just right
	m_pWidgetCtl->SetIsEnabled(b);
	m_pBoxVis->SetVisible(b);
}
bool VfaDragBoxWidget::GetIsEnabled() const
{
	return m_pWidgetCtl->GetIsEnabled();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsVisible                                            */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxWidget::SetIsVisible(bool b)
{
	m_pBoxVis->SetVisible(b);
	m_pWidgetCtl->SetAreItemsVisible(b);
}
bool VfaDragBoxWidget::GetIsVisible() const
{
	return m_pWidgetCtl->GetAreItemsVisible();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIsInteracting                                            */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxWidget::GetIsInteracting() const
{
	return m_pWidgetCtl->GetIsInteracting();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel                                                    */
/*                                                                            */
/*============================================================================*/
VfaBoxModel *VfaDragBoxWidget::GetModel() const
{
	return m_pBoxModel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetWidgetController                                         */
/*                                                                            */
/*============================================================================*/
VfaDragBoxController* VfaDragBoxWidget::GetController() const
{
	return m_pWidgetCtl;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetView                                                     */
/*                                                                            */
/*============================================================================*/
VfaBoxVis *VfaDragBoxWidget::GetView() const
{
	return m_pBoxVis;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetGrabButton                                               */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxWidget::SetDragButton(int iButtonID)
{
	m_pWidgetCtl->SetDragButton(iButtonID);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGrabButton                                               */
/*                                                                            */
/*============================================================================*/
int VfaDragBoxWidget::GetDragButton() const
{
	return m_pWidgetCtl->GetDragButton();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaDragBoxWidget::VfaDragBoxWidgetProps *VfaDragBoxWidget::GetProperties() const
{
	return m_pProps;
}
/*============================================================================*/
/*  IMPLEMENTATION      VfaDragBoxVisProps                                   */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaDragBoxWidget::VfaDragBoxWidgetProps");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	NULL
};
//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	NULL 
};
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaDragBoxWidget::VfaDragBoxWidgetProps::VfaDragBoxWidgetProps(
													VfaDragBoxWidget *pWidget)
	:	m_pWidget(pWidget)
{
}

VfaDragBoxWidget::VfaDragBoxWidgetProps::~VfaDragBoxWidgetProps()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetClampBox                                                 */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxWidget::VfaDragBoxWidgetProps::SetClampBox(float fBox[6])
{
	//check if we already got a bounds constraint there
	VfaRestrictBoxToAABounds *pC = NULL;
	m_pWidget->GetModel()->GetConstraint(pC);
	//if we don't have one -> create one!
	if(pC == NULL)
	{
		pC = new VfaRestrictBoxToAABounds;
		m_pWidget->GetModel()->AddConstraint(pC);
	}
	//finally set the constraint
	pC->SetBounds(fBox);
	
	this->Notify(MSG_CLAMPING_CHANGED);
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetClampBox                                                 */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxWidget::VfaDragBoxWidgetProps::GetClampBox(float fBox[6]) const
{
	memset(fBox, 0, 6*sizeof(fBox[0]));
	VfaRestrictBoxToAABounds *pC = NULL;
	m_pWidget->GetModel()->GetConstraint(pC);
	if(pC == NULL)
		return;
	
	pC->GetBounds(fBox);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetClamping                                                  */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxWidget::VfaDragBoxWidgetProps::SetClamping(bool b)
{
	VfaRestrictBoxToAABounds *pC = NULL;
	m_pWidget->GetModel()->GetConstraint(pC);
	if(pC == NULL)
		return false;

	pC->SetIsActive(b);
	this->Notify(MSG_CLAMPING_CHANGED);
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetClamping                                                  */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxWidget::VfaDragBoxWidgetProps::GetClamping() const
{
	//return m_pWidget->GetWidgetController()->GetEnforceBoxContstraint();
	VfaRestrictBoxToAABounds *pC = NULL;
	m_pWidget->GetModel()->GetConstraint(pC);
	return pC != NULL && pC->GetIsActive();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetShowLastState                                            */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxWidget::VfaDragBoxWidgetProps::SetShowLastState(bool b)
{
	m_pWidget->GetController()->SetShowLastState(b);
	this->Notify(MSG_SHOW_LAST_CHANGED);
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetShowLastState                                            */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxWidget::VfaDragBoxWidgetProps::GetShowLastState() const
{
	return m_pWidget->GetController()->GetShowLastState();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetCurrentsColor                                        */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxWidget::VfaDragBoxWidgetProps::SetCurrentBoxColor(
													const VistaColor &c)
{
	float fColor[4];
	c.GetValues(fColor);
	m_pWidget->GetView()->GetProperties()->SetFaceColor(fColor);
	m_pWidget->GetView()->GetProperties()->SetLineColor(fColor);
	this->Notify(MSG_BOX_COLOR_CHANGED);
	return true;
}
VistaColor VfaDragBoxWidget::VfaDragBoxWidgetProps::GetCurrentBoxColor() const
{
	float fColor[4];
	m_pWidget->GetView()->GetProperties()->GetFaceColor(fColor);
	return VistaColor(fColor);
}
void VfaDragBoxWidget::VfaDragBoxWidgetProps::SetCurrentBoxColor(float fC[4])
{
	m_pWidget->GetView()->GetProperties()->SetFaceColor(fC);
	m_pWidget->GetView()->GetProperties()->SetLineColor(fC);
	this->Notify(MSG_BOX_COLOR_CHANGED);
}
void VfaDragBoxWidget::VfaDragBoxWidgetProps::GetCurrentBoxColor(float fC[4]) const
{
	m_pWidget->GetView()->GetProperties()->GetFaceColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLastsColor                                           */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxWidget::VfaDragBoxWidgetProps::SetLastBoxColor(const VistaColor &c)
{
	float fColor[4];
	c.GetValues(fColor);
	m_pWidget->GetController()->SetLastBoxColor(fColor);
	this->Notify(MSG_LAST_COLOR_CHANGED);
	return true;
}
VistaColor VfaDragBoxWidget::VfaDragBoxWidgetProps::GetLastBoxColor() const
{
	float fColor[4];
	m_pWidget->GetController()->GetLastBoxColor(fColor);
	return VistaColor(fColor);
}
void VfaDragBoxWidget::VfaDragBoxWidgetProps::SetLastBoxColor(float fC[4])
{
	m_pWidget->GetController()->SetLastBoxColor(fC);
	this->Notify(MSG_LAST_COLOR_CHANGED);
}
void VfaDragBoxWidget::VfaDragBoxWidgetProps::GetLastBoxColor(float fC[4]) const
{
	m_pWidget->GetController()->GetLastBoxColor(fC);
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaDragBoxWidget::VfaDragBoxWidgetProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaDragBoxWidget::VfaDragBoxWidgetProps::AddToBaseTypeList(
										std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  END OF FILE "VfaDragBoxWidget.cpp"                                            */
/*============================================================================*/


