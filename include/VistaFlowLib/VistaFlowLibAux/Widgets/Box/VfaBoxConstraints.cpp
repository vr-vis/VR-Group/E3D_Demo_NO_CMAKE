/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaBoxConstraints.h"
#include "VfaBoxModel.h"
#include "../VfaWidgetTools.h"

#include <cassert>
#include <cstring>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaRestrictBoxToAxisAlignedBox::VfaRestrictBoxToAxisAlignedBox()
{
}

VfaRestrictBoxToAxisAlignedBox::~VfaRestrictBoxToAxisAlignedBox()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CheckConstraint                                             */
/*                                                                            */
/*============================================================================*/
bool VfaRestrictBoxToAxisAlignedBox::CheckConstraint(const VfaWidgetModelBase*const pModel)
{
	//make sure we are always working on a valid model type!
	assert(dynamic_cast<const VfaBoxModel*const>(pModel) != NULL);
	const VfaBoxModel *const pBox = static_cast<const VfaBoxModel*const>(pModel);

	//check if the quaternion is still in NULL position
	VistaQuaternion qNeutral(0.0f, 0.0f, 0.0f, 1.0f);
	VistaQuaternion qRot;
	pBox->GetRotation(qRot);
	
	return qRot == qNeutral;
}


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaRestrictBoxToCube::VfaRestrictBoxToCube()
{
}

VfaRestrictBoxToCube::~VfaRestrictBoxToCube()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CheckConstraint                                             */
/*                                                                            */
/*============================================================================*/
bool VfaRestrictBoxToCube::CheckConstraint(const VfaWidgetModelBase*const pModel)
{
	//make sure we are always working on a valid model type!
	assert(dynamic_cast<const VfaBoxModel*const>(pModel) != NULL);
	const VfaBoxModel *const pBox = static_cast<const VfaBoxModel*const>(pModel);

	float fExtents[3];
	pBox->GetExtents(fExtents);
	return fExtents[0] == fExtents[1] && fExtents[0] == fExtents[2];
}


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaRestrictBoxToAABounds::VfaRestrictBoxToAABounds()
{
}

VfaRestrictBoxToAABounds::~VfaRestrictBoxToAABounds()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CheckConstraint                                             */
/*                                                                            */
/*============================================================================*/
bool VfaRestrictBoxToAABounds::CheckConstraint(const VfaWidgetModelBase*const pModel)
{
	//make sure we are always working on a valid model type!
	assert(dynamic_cast<const VfaBoxModel*const>(pModel) != NULL);
	const VfaBoxModel *const pBox = static_cast<const VfaBoxModel*const>(pModel);

	if(!pBox->GetIsAxisAligned())
		return false;

	//for all corners -> check if they are inside
	float fCornerPos[3];
	for(int c=0; c<8; ++c)
	{
		pBox->GetCorner(c, fCornerPos);
		if(!VfaWidgetTools::IsInsideBox(fCornerPos, m_fBounds))
			return false;
	}
	return true;
}

void VfaRestrictBoxToAABounds::SetBounds(float fBounds[6])
{
	memcpy(m_fBounds, fBounds, 6*sizeof(float));
}

void VfaRestrictBoxToAABounds::GetBounds(float fBounds[6]) const
{
	memcpy(fBounds, m_fBounds, 6*sizeof(float));
}
/*============================================================================*/
/*  END OF FILE "VfaBoxConstraints.cpp"                                       */
/*============================================================================*/
