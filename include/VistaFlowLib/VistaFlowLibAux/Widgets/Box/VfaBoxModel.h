/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFABOXMODEL_H
#define _VFABOXMODEL_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetModelBase.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A box model defines an arbitrary box in 3-space.
 */
class VISTAFLOWLIBAUXAPI VfaBoxModel : public VfaWidgetModelBase
{
public:
	enum{
		MSG_BOX_CHANGE = VfaWidgetModelBase::MSG_LAST,
		MSG_LAST
	};


	VfaBoxModel();
	virtual ~VfaBoxModel();

	/**
	 * Set an axis aligned box by specifying its bounds.
	 */
	bool SetAABox(float fBounds[6]);
	/**
	 * Get box back provided it is axis aligned.
	 *
	 * NOTE: This interface works only for axis aligned boxes.
	 *		 For everything else, the result will be undefined!
	 *       Use the center/extents/rotation interface in these cases.
	 */
	void GetAABox(float fBounds[6]) const;

	/**
	 * The translation defines where the box is positioned inside
	 * the defining coordinate frame.
	 */
	bool SetTranslation(float fTrans[3]);
	bool SetTranslation(double dTrans[3]);
	bool SetTranslation(const VistaVector3D &v3Trans);

	void GetTranslation(float fTrans[3]) const;
	void GetTranslation(double dTrans[3]) const;
	void GetTranslation(VistaVector3D &v3Trans) const;
	VistaVector3D GetTranslation() const;

	/** 
	 * Get/Set the box's rotation in its defining space.
	 * NOTE: The box always rotates about its center.
	 */ 
	bool SetRotation(const VistaQuaternion &qRotation);
	void GetRotation(VistaQuaternion &qRot) const;
	VistaQuaternion GetRotation() const;

	/**
	 * The box's extents define the size of the box in each
	 * of the spatial directions. 
	 * NOTE: Extents are no directed quantity, i.e. the extents
	 *	     are rotation invariant. In particular, the sum
	 *		 of half of the extents and the center position will
	 *		 generally not be one of the box's vertices.
	 *		 If you need vertex access, use the interface below!
	 */
	bool SetExtents(float fExtents[3]);
	bool SetExtents(float fWidth, float fHeight, float fLength);
	bool SetExtents(const VistaVector3D &v3Extents);

	void GetExtents(float fExtents[3]) const;
	void GetExtents(float& fWidth, float& fHeight, float& fLength) const;
	VistaVector3D GetExtents() const;

	/**
	 * enum for the box's corners
	 * NOTE: the order is important here. The lowest bit will
	 *       be used for left/right, the second lowest for top/bottom,
	 *       and the third lowest for front/back discrimination
	 */
	enum ECorner{
		CORNER_LLB = 0, //left lower back
		CORNER_RLB = 1, 
		CORNER_LTB = 2,
		CORNER_RTB = 3,
		CORNER_LLF = 4, 
		CORNER_RLF = 5, 
		CORNER_LTF = 6,
		CORNER_RTF = 7
	};
	/** 
	 * Retrieve the coords of a given corner(incl. rotation)
	 */
	void GetCorner(unsigned char i, float fCorner[3]) const;
	void GetCorner(unsigned char i, VistaVector3D &v3Corner) const;

	/**
	 * @todo This currently only checks for the default unit quaternion
	 *		 and therefore neglects rotational symmetry along the axes!
	 */
	bool GetIsAxisAligned() const;

	virtual std::string GetReflectionableType() const;

protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	/**
	 * convenience methods for internal state management
	 * related to constraint checking
	 */
	void StoreState();
	void RestoreState();

private:
	VistaVector3D m_v3Center;
	float m_fExtents[3];
	VistaQuaternion m_qRotation;
	VistaVector3D m_v3Translation;
	
	//more convenience stuff for constraint checking...
	VistaVector3D m_v3OldTranslation;
	VistaQuaternion m_qOldRotation;
	float m_fOldExtents[3];

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFABOXMODEL_H
