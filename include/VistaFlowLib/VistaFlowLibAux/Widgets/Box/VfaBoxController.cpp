/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaBoxController.h"
#include "VfaBoxConstraints.h"
#include "VfaBoxModel.h"
#include "VfaBoxWidget.h"
#include "../VfaWidgetTools.h"
#include "../VfaSphereHandle.h"
#include "../VfaSphereVis.h"

#include <VistaKernel/InteractionManager/VistaIntentionSelect.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <VistaMath/VistaIndirectXform.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <algorithm>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaBoxController::VfaBoxController(VfaBoxModel *pModel,
									 VflRenderNode* pRenderNode)
: IVfaWidgetController(pRenderNode),
  m_pModel(pModel),
  m_iInteractionButton(0),
  m_pFocusHandle(NULL),
  m_iTranslationHandle(0),
  m_pXform(new VistaIndirectXform),
  m_iCurrentInternalState(VfaBoxController::BCS_NONE),
  m_bEnabled(true)
{
	m_pCtlProps = new VfaBoxController::VfaBoxControllerProps(this);
	
	m_vecHandles.resize(8);
	for(int i=0; i<8; ++i)
	{
		m_vecHandles[i] = new VfaSphereHandle(pRenderNode);
		float fColor[4] = {1.0f, 0.0f, 0.0f, 1.0f};
		m_vecHandles[i]->SetNormalColor(fColor);
		float fHColor[4] = {1.0f, 0.75f, 0.75f, 1.0f};
		m_vecHandles[i]->SetHighlightColor(fHColor);
		this->AddControlHandle(m_vecHandles[i]);
	}

	float fColor[4] = {0.0f, 0.0f, 1.0f, 1.0f};
	m_vecHandles[m_iTranslationHandle]->SetNormalColor(fColor);
	float fHColor[4] = {0.75f, 0.75f, 1.0f, 1.0f};
	m_vecHandles[m_iTranslationHandle]->SetHighlightColor(fHColor);
}

VfaBoxController::~VfaBoxController()
{
	delete m_pCtlProps;
	delete m_pXform;

	for(unsigned int i=0; i<m_vecHandles.size(); ++i)
	{
		delete m_vecHandles[i];
	}

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaBoxController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	for(unsigned int i=0; i<m_vecHandles.size(); ++i)
	{
		m_vecHandles[i]->SetVisible(m_bEnabled);
		m_vecHandles[i]->SetEnable(m_bEnabled);
	}

}
bool VfaBoxController::GetIsEnabled() const
{
	return m_bEnabled;
}

void VfaBoxController::SetInteractionButton(int iBtId)
{
	if(iBtId > 8*sizeof(int))
		return;
	m_iInteractionButton = iBtId;
}
int  VfaBoxController::GetInteractionButton() const
{
	return m_iInteractionButton;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnFocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaBoxController::OnFocus(IVfaControlHandle* pHandle)
{
	// don't allow to change focus if some handle already has it
	if(m_iCurrentInternalState == BCS_NONE)
	{
		m_pFocusHandle = static_cast<VfaSphereHandle*>(pHandle);

		// search focused handle in list
		m_iHandle =(int)(find(	m_vecHandles.begin(), 
								m_vecHandles.end(), 
								m_pFocusHandle) - m_vecHandles.begin());

		m_pFocusHandle->SetIsHighlighted(true);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUnfocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaBoxController::OnUnfocus()
{
	// don't allow to change focus if some handle already has it
	if(m_iCurrentInternalState == BCS_NONE)
	{
		if(m_pFocusHandle)
			m_pFocusHandle->SetIsHighlighted(false);
		m_iHandle = -1;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaBoxController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUntouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaBoxController::OnUntouch()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaBoxController::OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	if(iSlot == VfaApplicationContextObject::SLOT_POINTER_VIS)
	{
		// save sensor data
		m_oLastSensorFrame = oFrameData;

		// if we have a move/resize state, move something....
		if(m_iCurrentInternalState != BCS_NONE)
		{
			VistaVector3D v3MyPos;
			VistaQuaternion qMyOri;
			m_pXform->Update(	m_oLastSensorFrame.v3Position, 
								m_oLastSensorFrame.qOrientation, 
								v3MyPos, qMyOri);
			switch(m_iCurrentInternalState)
			{
			case BCS_MOVE:
				{
					m_pModel->SetTranslation(v3MyPos);
					m_pModel->SetRotation(qMyOri);
					break;
				}
			case BCS_RESIZE:
				{
					//v3MyPos now holds the grabbed handle's new ps
					//difference to the center gives new extents
					VistaVector3D v3Trans;
					m_pModel->GetTranslation(v3Trans);
					//"unrotate" the offset by the box's rotation
					VistaQuaternion qRot;
					m_pModel->GetRotation(qRot);
					//@todo check if we rotated in the right direction here!
					v3MyPos = 2.0f*(qRot.GetComplexConjugated().Rotate(v3MyPos-v3Trans));
					m_pModel->SetExtents(fabs(v3MyPos[0]), fabs(v3MyPos[1]), fabs(v3MyPos[2]));
					break;
				}
			}
		}
	}	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaBoxController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	if(bSet)
	{
		// we can only be grabbed, if we have the focus
		if(this->GetControllerState() == CS_FOCUS && iSlot == m_iInteractionButton)
		{
			if(m_iHandle == m_iTranslationHandle)
			{
				VistaVector3D v3Center;
				m_pModel->GetTranslation(v3Center);
				VistaQuaternion qRot;
				m_pModel->GetRotation(qRot);
					
				m_pXform->Init(	m_oLastSensorFrame.v3Position, 
								m_oLastSensorFrame.qOrientation, 
								v3Center, qRot);
				m_iCurrentInternalState = BCS_MOVE;
			}
			else
			{
				VistaVector3D oHandleCenter;
				m_pFocusHandle->GetCenter(oHandleCenter);
				VistaQuaternion qNeutral;
				m_pXform->Init(	m_oLastSensorFrame.v3Position, 
								m_oLastSensorFrame.qOrientation,
								oHandleCenter, qNeutral);
				m_iCurrentInternalState = BCS_RESIZE;
			}
		}
	}
	else
	{
		// if the slot is released, goto state NONE
		m_iCurrentInternalState = BCS_NONE;

		// and if we have lost focus during other states, now react on that
		if(this->GetControllerState()!=CS_FOCUS)
		{
			if(m_pFocusHandle)
				m_pFocusHandle->SetIsHighlighted(false);
			m_iHandle = -1;

		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*                                                                            */
/*============================================================================*/
void VfaBoxController::OnTimeSlotUpdate(const double dTime)
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSensorMask			                                      */
/*                                                                            */
/*============================================================================*/
int VfaBoxController::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_POINTER_VIS);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCommandMask			                                  */
/*                                                                            */
/*============================================================================*/
int VfaBoxController::GetCommandMask() const
{
	return(1 << m_iInteractionButton);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeUpdate				                                  */
/*                                                                            */
/*============================================================================*/
bool VfaBoxController::GetTimeUpdate() const
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaBoxController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaBoxModel *pBoxModel = dynamic_cast<VfaBoxModel*>(pObserveable);

	if(!pBoxModel)
		return;

	/*
	// get data from model
	VistaVector3D v3Min = pBoxModel->GetMin();
	VistaVector3D v3Max = pBoxModel->GetMax();

	// apply constraints

	//MW: check constraint implementation, not everything does work, yet!

	m_pConstraints->EvaluateFixpoint(v3Min, v3Max);
	m_pConstraints->CheckPenetration(v3Min, v3Max);
	m_pConstraints->CheckFixAspectRatio(v3Min, v3Max);
	// save old values
	m_pConstraints->SetLastMin(v3Min);
	m_pConstraints->SetLastMax(v3Max);

	// update data in model
	pBoxModel->SetMin(v3Min);
	pBoxModel->SetMax(v3Max);
	*/

	// reposition handles
	UpdateHandles();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaBoxController::VfaBoxControllerProps* VfaBoxController::GetProperties() const
{
	return m_pCtlProps;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateHandles                                               */
/*                                                                            */
/*============================================================================*/
void VfaBoxController::UpdateHandles()
{
	VistaVector3D v3Corner;
	for(int i=0; i<8; ++i)
	{
		m_pModel->GetCorner(i, v3Corner);
		m_vecHandles[i]->SetCenter(v3Corner);
	}
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaBoxControllerProps                                */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaBoxController::VfaBoxControllerProps");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
//	new TVistaPropertyArrayGet<VfaBoxWidget::VfaBoxWidgetProps, float>(
//		"BOX_ORIENTATION", STR_REF_TYPENAME,
//		&VfaBoxWidget::VfaBoxWidgetProps::GetOri),
	new TVistaPropertyGet<bool, VfaBoxController::VfaBoxControllerProps>(
		"FIX_ASPECT_RATIO", STR_REF_TYPENAME,
		&VfaBoxController::VfaBoxControllerProps::GetFixAspectRatio),
	new TVistaPropertyGet<bool, VfaBoxController::VfaBoxControllerProps>(
		"ALLOW_PENETRATION", STR_REF_TYPENAME,
		&VfaBoxController::VfaBoxControllerProps::GetAllowPenetration),
	new TVistaPropertyGet<float, VfaBoxController::VfaBoxControllerProps>(
		"HANDLE_RADIUS", STR_REF_TYPENAME,
		&VfaBoxController::VfaBoxControllerProps::GetHandleRadius),
	NULL
};

//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
//	new TVistaPropertyArraySet<VfaBoxWidget::VfaBoxWidgetProps, float>(
//		"BOX_ORIENTATION", STR_REF_TYPENAME,
//		&VfaBoxWidget::VfaBoxWidgetProps::SetOri,
//		&VistaAspectsConversionStuff::ConvertToFloatList),
	new TVistaPropertySet<bool, bool, VfaBoxController::VfaBoxControllerProps>(
		"FIX_ASPECT_RATIO", STR_REF_TYPENAME,
		&VfaBoxController::VfaBoxControllerProps::SetFixAspectRatio),
	new TVistaPropertySet<bool, bool, VfaBoxController::VfaBoxControllerProps>(
		"ALLOW_PENETRATION", STR_REF_TYPENAME,
		&VfaBoxController::VfaBoxControllerProps::SetAllowPenetration),
	new TVistaPropertySet<float, float, VfaBoxController::VfaBoxControllerProps>(
		"HANDLE_RADIUS", STR_REF_TYPENAME,
		&VfaBoxController::VfaBoxControllerProps::SetHandleRadius),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaBoxController::VfaBoxControllerProps>(
		"HANDLE_NORMAL_COLOR", STR_REF_TYPENAME,
		&VfaBoxController::VfaBoxControllerProps::SetHandleNormalColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaBoxController::VfaBoxControllerProps>(
		"HANDLE_HIGHLIGHT_COLOR", STR_REF_TYPENAME,
		&VfaBoxController::VfaBoxControllerProps::SetHandleHighlightColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaBoxController::VfaBoxControllerProps>(
		"TRANSLATION_HANDLE_NORMAL_COLOR", STR_REF_TYPENAME,
		&VfaBoxController::VfaBoxControllerProps::SetTranslationHandleNormalColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaBoxController::VfaBoxControllerProps>(
		"TRANSLATION_HANDLE_HIGHLIGHT_COLOR", STR_REF_TYPENAME,
		&VfaBoxController::VfaBoxControllerProps::SetTranslationHandleHighlightColor),
	NULL 
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaBoxController::VfaBoxControllerProps::VfaBoxControllerProps(VfaBoxController *pBoxCtrl)
: m_pBoxCtrl(pBoxCtrl),
  m_bFixAspectRatio(false),
  m_bAllowPenetration(false),
  m_eFixpoint(FIXPOINT_CORNER)
{

}

VfaBoxController::VfaBoxControllerProps::~VfaBoxControllerProps()
{

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFixAspectRatio                                       */
/*                                                                            */
/*============================================================================*/
bool VfaBoxController::VfaBoxControllerProps::SetFixAspectRatio(bool bFixAspectRatio)
{
	if(bFixAspectRatio != m_bFixAspectRatio)
	{
		m_bFixAspectRatio = bFixAspectRatio;
		Notify();
		return true;
	}
	return false;
}

bool VfaBoxController::VfaBoxControllerProps::GetFixAspectRatio() const
{
	return m_bFixAspectRatio;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetAllowPenetration                                     */
/*                                                                            */
/*============================================================================*/
bool VfaBoxController::VfaBoxControllerProps::SetAllowPenetration(bool bAllowPenetration)
{
	if(bAllowPenetration != m_bAllowPenetration)
	{
		m_bAllowPenetration = bAllowPenetration;
		Notify();
		return true;
	}
	return false;
}

bool VfaBoxController::VfaBoxControllerProps::GetAllowPenetration() const
{
	return m_bAllowPenetration;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFixpoint                                             */
/*                                                                            */
/*============================================================================*/
bool VfaBoxController::VfaBoxControllerProps::SetFixpoint(FIXPOINT eFixpoint)
{
	if(eFixpoint != m_eFixpoint)
	{
		m_eFixpoint = eFixpoint;
		Notify();
		return true;
	}
	return false;
}

VfaBoxController::VfaBoxControllerProps::FIXPOINT 
	VfaBoxController::VfaBoxControllerProps::GetFixpoint() const
{
	return m_eFixpoint;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleRadius                                         */
/*                                                                            */
/*============================================================================*/
bool VfaBoxController::VfaBoxControllerProps::SetHandleRadius(float fRadius)
{
	if(fRadius != m_pBoxCtrl->m_vecHandles[0]->GetRadius())
	{
		for(int i=0; i<8; ++i)
		{
			m_pBoxCtrl->m_vecHandles[i]->SetRadius(fRadius);
		}
		Notify();
		return true;
	}
	return false;
}

float VfaBoxController::VfaBoxControllerProps::GetHandleRadius() const
{
	return m_pBoxCtrl->m_vecHandles[0]->GetRadius();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleNormalColor                                    */
/*                                                                            */
/*============================================================================*/
bool VfaBoxController::VfaBoxControllerProps::SetHandleNormalColor(
	const VistaColor& color)
{
	//the math is for making sure not to take the Translation handle
	if (m_pBoxCtrl->m_vecHandles[(m_pBoxCtrl->m_iTranslationHandle + 1) % 8]->GetNormalColor() == color)
		return false;

	for(int i=0; i<8; ++i)
	{
		if (i != m_pBoxCtrl->m_iTranslationHandle)
		{
			float fColors[4];
			color.GetValues(fColors);
			m_pBoxCtrl->m_vecHandles[i]->SetNormalColor(fColors);
		}
	}

	return true;
}

VistaColor VfaBoxController::VfaBoxControllerProps::GetHandleNormalColor() const
{
	float fColors[4];
	m_pBoxCtrl->m_vecHandles[(m_pBoxCtrl->m_iTranslationHandle + 1) % 8]->GetNormalColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaBoxController::VfaBoxControllerProps::SetHandleNormalColor(float fC[4])
{
	for(int i=0; i<8; ++i)
		if (i != m_pBoxCtrl->m_iTranslationHandle)
			m_pBoxCtrl->m_vecHandles[i]->SetNormalColor(fC);
}

void VfaBoxController::VfaBoxControllerProps::GetHandleNormalColor(float fC[4]) const
{
	m_pBoxCtrl->m_vecHandles[(m_pBoxCtrl->m_iTranslationHandle + 1) % 8]->GetNormalColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleHighlightColor                                 */
/*                                                                            */
/*============================================================================*/
bool VfaBoxController::VfaBoxControllerProps::SetHandleHighlightColor(
	const VistaColor& color)
{
	if (m_pBoxCtrl->m_vecHandles[(m_pBoxCtrl->m_iTranslationHandle + 1) % 8]->GetHighlightColor() == color)
		return false;

	for(int i=0; i<8; ++i)
	{
		if (i != m_pBoxCtrl->m_iTranslationHandle)
		{
			float fColors[4];
			color.GetValues(fColors);
			m_pBoxCtrl->m_vecHandles[i]->SetHighlightColor(fColors);
		}
	}

	return true;
}

VistaColor VfaBoxController::VfaBoxControllerProps::GetHandleHighlightColor() const
{
	float fColors[4];
	m_pBoxCtrl->m_vecHandles[(m_pBoxCtrl->m_iTranslationHandle + 1) % 8]->GetHighlightColor(fColors);
	VistaColor color(fColors);

	return color;
}

void VfaBoxController::VfaBoxControllerProps::SetHandleHighlightColor(float fC[4])
{
	for(int i=0; i<8; ++i)
		if (i != m_pBoxCtrl->m_iTranslationHandle)
			m_pBoxCtrl->m_vecHandles[i]->SetHighlightColor(fC);
}

void VfaBoxController::VfaBoxControllerProps::GetHandleHighlightColor(float fC[4]) const
{
	m_pBoxCtrl->m_vecHandles[(m_pBoxCtrl->m_iTranslationHandle + 1) % 8]->GetHighlightColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTranslationHandleNormalColor                         */
/*                                                                            */
/*============================================================================*/
bool VfaBoxController::VfaBoxControllerProps::SetTranslationHandleNormalColor(
	const VistaColor& color)
{
//	if (m_pBoxCtrl->m_vecHandles[m_pBoxCtrl->m_iTranslationHandle]->GetNormalColor() == color)
//		return false;

	float fColors[4];
	color.GetValues(fColors);
	m_pBoxCtrl->m_vecHandles[m_pBoxCtrl->m_iTranslationHandle]->SetNormalColor(fColors);
	
	return true;
}

VistaColor VfaBoxController::VfaBoxControllerProps::GetTranslationHandleNormalColor() const
{
	float fColors[4];
	m_pBoxCtrl->m_vecHandles[m_pBoxCtrl->m_iTranslationHandle]->GetNormalColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaBoxController::VfaBoxControllerProps::SetTranslationHandleNormalColor(float fC[4])
{
	m_pBoxCtrl->m_vecHandles[m_pBoxCtrl->m_iTranslationHandle]->SetNormalColor(fC);
}

void VfaBoxController::VfaBoxControllerProps::GetTranslationHandleNormalColor(float fC[4]) const
{
	m_pBoxCtrl->m_vecHandles[m_pBoxCtrl->m_iTranslationHandle]->GetNormalColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTranslationHandleHighlightColor                      */
/*                                                                            */
/*============================================================================*/
bool VfaBoxController::VfaBoxControllerProps::SetTranslationHandleHighlightColor(
	const VistaColor& color)
{
//	if (m_pBoxCtrl->m_vecHandles[m_pBoxCtrl->m_iTranslationHandle]->GetHighlightColor() == color)
//		return false;

	float fColors[4];
	color.GetValues(fColors);
	m_pBoxCtrl->m_vecHandles[m_pBoxCtrl->m_iTranslationHandle]->SetHighlightColor(fColors);

	return true;
}

VistaColor VfaBoxController::VfaBoxControllerProps::GetTranslationHandleHighlightColor() const
{
	float fColors[4];
	m_pBoxCtrl->m_vecHandles[m_pBoxCtrl->m_iTranslationHandle]->GetHighlightColor(fColors);
	VistaColor color(fColors);

	return color;
}

void VfaBoxController::VfaBoxControllerProps::SetTranslationHandleHighlightColor(float fC[4])
{
	m_pBoxCtrl->m_vecHandles[m_pBoxCtrl->m_iTranslationHandle]->SetHighlightColor(fC);
}

void VfaBoxController::VfaBoxControllerProps::GetTranslationHandleHighlightColor(float fC[4]) const
{
	m_pBoxCtrl->m_vecHandles[m_pBoxCtrl->m_iTranslationHandle]->GetHighlightColor(fC);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaBoxController::VfaBoxControllerProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaBoxController::VfaBoxControllerProps::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  END OF FILE "VfaBoxController.cpp"                                        */
/*============================================================================*/
