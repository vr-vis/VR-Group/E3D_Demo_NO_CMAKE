/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef __VFATEXTUREDBOXMODEL_H
#define __VFATEXTUREDBOXMODEL_H

// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaBoxModel.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>


// ========================================================================== //
// === Forward Declarations
// ========================================================================== //
class VistaTexture;


// ========================================================================== //
// === Class Definition
// ========================================================================== //
class VISTAFLOWLIBAUXAPI VfaTexturedBoxModel : public VfaBoxModel
{
public:
	enum eMessages
	{
		MSG_TEXTURE_CHANGE = VfaBoxModel::MSG_LAST,
		MSG_LAST
	};

	// ---------------------------------------------------------------------- //
	// --- Con-/Destructor
	// ---------------------------------------------------------------------- //
	//!
	VfaTexturedBoxModel();
	virtual ~VfaTexturedBoxModel();


	// ---------------------------------------------------------------------- //
	// --- Public Interface
	// ---------------------------------------------------------------------- //
	//!
	/*!
		NOTE: This routine binds a texture to whatever texture unit is currently
			  actvive and later unbinds *ANY* texture from that particular
			  texture unit. So as a side effect, whatever was bound to the
			  tex unit at the 2D target will be gone after the function returns!
			  Mind that before making a call to the function.
	 */
	bool LoadTexture(std::string strFilename) const;
	//!
	VistaTexture* GetTexture() const;
	//! 
	bool SetTexture(VistaTexture* pTex);


protected:

private:
	// ---------------------------------------------------------------------- //
	// --- Public Interface
	// ---------------------------------------------------------------------- //
	//!
	VistaTexture			*m_pTexture;
};

#endif // __VFATEXTUREDBOXMODEL_H


// ========================================================================== //
// === End of File
// ========================================================================== //
