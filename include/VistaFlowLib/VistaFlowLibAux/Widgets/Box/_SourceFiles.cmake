

set( RelativeDir "./Widgets/Box" )
set( RelativeSourceGroup "Source Files\\Widgets\\Box" )

set( DirFiles
	VfaBoxConstraints.cpp
	VfaBoxConstraints.h
	VfaBoxController.cpp
	VfaBoxController.h
	VfaBoxModel.cpp
	VfaBoxModel.h
	VfaBoxWidget.cpp
	VfaBoxWidget.h
	VfaDragBoxController.cpp
	VfaDragBoxController.h
	VfaDragBoxWidget.cpp
	VfaDragBoxWidget.h
	VfaTexturedBoxModel.cpp
	VfaTexturedBoxModel.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

