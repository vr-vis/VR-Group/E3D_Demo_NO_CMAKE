/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFADRAGBOXCONTROLLER_H
#define _VFADRAGBOXCONTROLLER_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetController.h"
#include <VistaAspects/VistaObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaVector3D;
class VistaQuaternion;
class VfaProximityBoxHandle;
class VfaProximityQuadHandle;
class VfaCrosshairVis;
class VfaBoxVis;
class VfaBoxModel;
class VflRenderNode;
class Vfl3DTextLabel;
class VistaTimer;
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Controll the behavior on button press event, button release event and 
 * pre-loop event, also highlight.
 */
class VISTAFLOWLIBAUXAPI VfaDragBoxController :	public IVfaWidgetController, public IVistaObserver
{
public:
	VfaDragBoxController(	VfaBoxModel *pModel, 
							VflRenderNode *pRenderNode,
							float fWidgetScale = 1.0f);
	virtual ~VfaDragBoxController();


	virtual void OnSensorSlotUpdate(int iSlot, 
					const VfaApplicationContextObject::sSensorFrame &oFrameData);

	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);

	virtual void OnTimeSlotUpdate(const double dTime);

	/**
	 *
	 */
	virtual void OnFocus(IVfaControlHandle* pHandle);
	virtual void OnUnfocus();

	/**
	 *
	 */
	virtual void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	virtual void OnUntouch();

	/**
	 *
	 */
	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;
	/*
	virtual bool OnMovement(const VistaVector3D &v3Pos, 
							const VistaQuaternion &qOri);
	virtual bool OnButtonPress(	int iButton, 
								const VistaVector3D &v3Pos, 
								const VistaQuaternion &qOri);
	virtual bool OnButtonRelease(	int iButton, 
									const VistaVector3D &v3Pos, 
									const VistaQuaternion &qOri);
	virtual bool OnPreLoop(	const VistaVector3D &v3Pos, 
							const VistaQuaternion &qOri);
	virtual int GetEventMask() const;
	*/
	/**
	 * check whether the widget is currently being changed
	 */
	bool GetIsInteracting() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetAreItemsVisible(bool b);
	bool GetAreItemsVisible();

	int GetDragButton() const;
	void SetDragButton(int i);

	void SetPlaneInteractionDist(float d);
	float GetPlaneInteractionDist() const;

	void SetShowLastState(bool b);
	bool GetShowLastState() const;

	//void SetCurrentBoxColor(float f[4]);
	//void GetCurrentBoxColor(float f[4]) const;

	void SetLastBoxColor(const VistaColor& color);
	VistaColor GetLastBoxColor() const;
	void SetLastBoxColor(float f[4]);
	void GetLastBoxColor(float f[4]) const;

	void SetHighlightColor(const VistaColor& color);
	VistaColor GetHighlightColor() const;
	void SetHighlightColor(float f[4]);
	void GetHighlightColor(float f[4]) const;
	
	void SetInactiveColor(const VistaColor& color);
	VistaColor GetInactiveColor() const;
	void SetInactiveColor(float f[4]);
	void GetInactiveColor(float f[4]) const;

	void SetHighlightLineWidth(float f);
	float GetHighlightLineWidth() const;

	void SetNormalLineWidth(float f);
	float GetNormalLineWidth() const;

	void SetShowToolTip(bool b);
	bool GetShowToolTip() const;

	void SetToolTipDelay(double fDelayInSeconds);
	double GetToolTipDelay() const;

	void SetToolTipMoveRadius(float fRadius);
	float GetToolTipMoveRadius() const;
	
	void SetToolTipOffset(float fOfs[3]);
	void GetToolTipOffset(float fOfs[3]) const;

	//********** OBSERVER INTERFACE IMPLEMENTATION *********************//
	virtual bool ObserveableDeleteRequest(IVistaObserveable *pObserveable, int eTicket);
	virtual void ObserveableDelete(IVistaObserveable *pObserveable, int eTicket);
	virtual void ReleaseObserveable(IVistaObserveable *pObserveable, int eTicket);
	virtual bool Observes(IVistaObserveable *pObserveable);
	virtual void Observe(IVistaObserveable *pObservable, int eTicket=IVistaObserveable::TICKET_NONE);
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int iMsg, int iTicket);

protected:
	/**
	 * enum for internal state control
	 */
	enum EInternalState{
		BOX_UNDEFINED=0,
		BOX_DRAGGING,
		BOX_DEFINED,
		BOX_MOVING_FACE,
		BOX_MOVING,
		BOX_DISABLED
	};

	void AlignQuadsToModelFaces();

	void SetDragBoxState(int iNewState);

	void PrintState(int iOldState, int iCurrentState) const;

private:
	VfaBoxModel *m_pModel;
	VflRenderNode *m_pRenderNode;

	/**
	 * handles for interaction
	 */
	VfaProximityBoxHandle *m_pBoxHandle;
	VfaProximityQuadHandle *m_pQuadHandles[6];

	/**
	 * some renderables to show/enhance the controller state
	 */
	VfaBoxVis			*m_pLastBox;
	Vfl3DTextLabel		*m_pToolTip;

	int m_iState;
	//cache the last known state before we went to out-of-focus
	int m_iLastState;
	int m_iDragButton;

	//enumeration for handles
	//mainly for readability -> mind the order here
	enum{
		HND_UNDEFINED=-2,
		HND_BOX = -1,
		HND_PLANE_LEFT = 0,
		HND_PLANE_RIGHT = 1,
		HND_PLANE_BOTTOM, 
		HND_PLANE_TOP,
		HND_PLANE_FRONT,
		HND_PLANE_BACK
	};
	int m_iActiveHnd;

	//color when the widget is highlighted for translation/selection
	float m_fHighlightColor[4];
	//color when the widget is in focus but not about to be translated
	float m_fStandardColor[4];
	//color when the widget is out of focus
	float m_fInactiveColor[4];

	float m_fHighlightLineWidth;
	float m_fNormalLineWidth;

	float m_fLastKnownSensorPos[3];
	float m_fStartDragPos[3];

	int m_iDragFace;
	float m_fDragFaceOfs;

	float m_fTranslateStartOffset[3];
	float m_fTranslationConstraint[6];
	bool m_bUseTranslationConstraint;

	bool m_bShowLastState;

	bool m_bShowToolTip;
	float m_fLastToolTipPos[3];
	float m_fToolTipOffset[3];
	float m_fToolTipMoveRadius;
	
	double m_fToolTipTimeSum;
	double m_fLastTime;
	double m_fToolTipDelay;
	
	VistaTimer *m_pTimer;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFADRAGBOXCONTROLLER_H
