/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <cstring>

#include "VfaBoxModel.h"
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  IMPLEMENTATION      VfaBoxVisProps                                       */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaBoxModel");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<bool, VfaBoxModel>(
		"AXIS_ALIGNED", STR_REF_TYPENAME,
		&VfaBoxModel::GetIsAxisAligned),
	new TVistaPropertyGet<VistaVector3D, VfaBoxModel>(
		"TRANSLATION", STR_REF_TYPENAME,
		&VfaBoxModel::GetTranslation),
	new TVistaPropertyGet<VistaQuaternion, VfaBoxModel>(
		"ROTATION", STR_REF_TYPENAME,
		&VfaBoxModel::GetRotation),
	new TVistaPropertyGet<VistaVector3D, VfaBoxModel>(
		"SIZE", STR_REF_TYPENAME,
		&VfaBoxModel::GetExtents),
	NULL
};
//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<const VistaVector3D &, VistaVector3D, VfaBoxModel>(
		"TRANSLATION", STR_REF_TYPENAME,
		&VfaBoxModel::SetTranslation),
	new TVistaPropertySet<const VistaQuaternion &, VistaQuaternion, VfaBoxModel>(
		"ROTATION", STR_REF_TYPENAME,
		&VfaBoxModel::SetRotation),
	new TVistaPropertySet<const VistaVector3D &, VistaVector3D, VfaBoxModel>(
		"SIZE", STR_REF_TYPENAME,
		&VfaBoxModel::SetExtents),
	NULL 
};
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaBoxModel::VfaBoxModel()
	:	m_v3Center(0.0f, 0.0f, 0.0f),
		m_qRotation(0.0f, 0.0f, 0.0f, 1.0f),
		m_v3Translation(0.0f, 0.0f, 0.0f)
{
	m_fExtents[0] = m_fExtents[1] = m_fExtents[2] = 1.0f;
}

VfaBoxModel::~VfaBoxModel()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetAABox                                                    */
/*                                                                            */
/*============================================================================*/
bool VfaBoxModel::SetAABox(float fBounds[6])
{
	this->StoreState();

	//delete quaternion
	m_qRotation = VistaQuaternion(0, 0, 0, 1);
	
	//determine translation and extends
	m_v3Translation[0] = 0.5f * (fBounds[1] + fBounds[0]);
	m_fExtents[0] = fBounds[1] - fBounds[0];

	m_v3Translation[1] = 0.5f * (fBounds[3] + fBounds[2]);
	m_fExtents[1] = fBounds[3] - fBounds[2];

	m_v3Translation[2] = 0.5f * (fBounds[5] + fBounds[4]);
	m_fExtents[2] = fBounds[5] - fBounds[4];

	//check if the new state is valid -> restore if it's not
	if(!this->CheckConstraints())
	{
		this->RestoreState();
		return false;
	}

	this->Notify(MSG_BOX_CHANGE);
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetAABox                                                    */
/*                                                                            */
/*============================================================================*/
void VfaBoxModel::GetAABox(float fBounds[6]) const
{
	if(!this->GetIsAxisAligned())
		return;

	fBounds[0] = m_v3Translation[0] - 0.5f * m_fExtents[0];
	fBounds[1] = m_v3Translation[0] + 0.5f * m_fExtents[0];
	fBounds[2] = m_v3Translation[1] - 0.5f * m_fExtents[1];
	fBounds[3] = m_v3Translation[1] + 0.5f * m_fExtents[1];
	fBounds[4] = m_v3Translation[2] - 0.5f * m_fExtents[2];
	fBounds[5] = m_v3Translation[2] + 0.5f * m_fExtents[2];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTranslation                                          */
/*                                                                            */
/*============================================================================*/
bool VfaBoxModel::SetTranslation(float fTrans[3])
{
	VistaVector3D v3Trans(fTrans);
	return this->SetTranslation(v3Trans);
}
bool VfaBoxModel::SetTranslation(double dTrans[3])
{
	VistaVector3D v3Trans(dTrans);
	return this->SetTranslation(v3Trans);
}

bool VfaBoxModel::SetTranslation(const VistaVector3D &v3Trans)
{
	if(v3Trans == m_v3Translation)
		return false;

	this->StoreState();

	m_v3Translation = v3Trans;
	
	//check if the new state is valid -> restore if it's not
	if(!this->CheckConstraints())
		this->RestoreState();

	this->Notify(MSG_BOX_CHANGE);
	return true;
}

void VfaBoxModel::GetTranslation(float fTrans[3]) const
{
	m_v3Translation.GetValues(fTrans);
}
void VfaBoxModel::GetTranslation(double dTrans[3]) const
{
	dTrans[0] = m_v3Translation[0];
	dTrans[1] = m_v3Translation[1];
	dTrans[2] = m_v3Translation[2];
}
void VfaBoxModel::GetTranslation(VistaVector3D &v3Trans) const
{
	v3Trans = m_v3Translation;
}

VistaVector3D VfaBoxModel::GetTranslation() const
{
	return m_v3Translation;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRotation                                             */
/*                                                                            */
/*============================================================================*/
bool VfaBoxModel::SetRotation(const VistaQuaternion &qRotation)
{
	if(qRotation == m_qRotation)
		return false;

	this->StoreState();

	m_qRotation = qRotation;

	//check if the new state is valid -> restore if it's not
	if(!this->CheckConstraints())
		this->RestoreState();

	this->Notify(MSG_BOX_CHANGE);
	return true;
}

void VfaBoxModel::GetRotation(VistaQuaternion &qRot) const
{
	qRot = m_qRotation;
}

VistaQuaternion VfaBoxModel::GetRotation() const
{
	return m_qRotation;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetExtents                                                  */
/*                                                                            */
/*============================================================================*/
bool VfaBoxModel::SetExtents(float fExtents[3])
{
	this->StoreState();

	memcpy(m_fExtents, fExtents, 3*sizeof(float));

	//check if the new state is valid -> restore if it's not
	if(!this->CheckConstraints())
		this->RestoreState();

	this->Notify(MSG_BOX_CHANGE);
	return true;
}

bool VfaBoxModel::SetExtents(float fWidth, float fHeight, float fLength)
{
	float fExtents[3] = {fWidth, fHeight, fLength};
	return this->SetExtents(fExtents);
}

bool VfaBoxModel::SetExtents(const VistaVector3D &v3Extents)
{
	float fExtents[3] = {v3Extents[0], v3Extents[1], v3Extents[2]};
	return this->SetExtents(fExtents);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetExtents                                                  */
/*                                                                            */
/*============================================================================*/
void VfaBoxModel::GetExtents(float fExtents[3]) const
{
	memcpy(fExtents, m_fExtents, 3*sizeof(float));
}

void VfaBoxModel::GetExtents(float& fWidth, float& fHeight, float& fLength) const
{
	fWidth   = m_fExtents[0];
	fHeight  = m_fExtents[1];
	fLength  = m_fExtents[2];
}

VistaVector3D VfaBoxModel::GetExtents() const
{
	return VistaVector3D(m_fExtents);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCorner                                                   */
/*                                                                            */
/*============================================================================*/
void VfaBoxModel::GetCorner(unsigned char i, float fCorner[3]) const
{
	VistaVector3D v3C;
	this->GetCorner(i, v3C);
	v3C.GetValues(fCorner);
}

void VfaBoxModel::GetCorner(unsigned char i, VistaVector3D &v3Corner) const
{
	//we start with the center and add/subtract the extents later
	v3Corner = m_v3Center;

	/*
	//NOTE: lowest 3 bits are used for left/right, bottom/top, and
	//      back/front discrimination...
	v3Corner +=(i & 0x00000001 ? -m_v3HalfExtents[0] : m_v3HalfExtents[0]);
	v3Corner +=(i & 0x00000002 ? -m_v3HalfExtents[1] : m_v3HalfExtents[1]);
	v3Corner +=(i & 0x00000004 ? -m_v3HalfExtents[2] : m_v3HalfExtents[2]);
	*/

	v3Corner[0] +=(i & 0x01 ? 0.5f*m_fExtents[0] : -0.5f*m_fExtents[0]);
	v3Corner[1] +=(i & 0x02 ? 0.5f*m_fExtents[1] : -0.5f*m_fExtents[1]);
	v3Corner[2] +=(i & 0x04 ? 0.5f*m_fExtents[2] : -0.5f*m_fExtents[2]);

	//finally apply rotation
	v3Corner = m_v3Translation + m_qRotation.Rotate(v3Corner);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsAxisAligned                                        */
/*                                                                            */
/*============================================================================*/
bool VfaBoxModel::GetIsAxisAligned() const
{
	VistaQuaternion qNeutral(0.0f, 0.0f, 0.0f, 1.0f);
	VistaVector3D v3Id(0.0f, 0.0f, 0.0f);
	return m_qRotation == qNeutral;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaBoxModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaBoxModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}

void VfaBoxModel::StoreState()
{
	if(!this->HasConstraints())
		return;

	//remember old state
	memcpy(m_fOldExtents, m_fExtents, 3*sizeof(float));
	m_v3OldTranslation = m_v3Translation;
	m_qOldRotation = m_qRotation;
}

void VfaBoxModel::RestoreState()
{
	m_v3Translation = m_v3OldTranslation;
	memcpy(m_fExtents, m_fOldExtents, 3*sizeof(float));
	m_qRotation = m_qOldRotation;
}
/*============================================================================*/
/*  END OF FILE "VfaBoxWidget.cpp"                                            */
/*============================================================================*/
