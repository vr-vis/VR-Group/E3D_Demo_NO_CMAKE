/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <cstring>
#include <cstdio>
#include <cassert>

#include "VfaDragBoxController.h"
#include "VfaBoxModel.h"
#include "VfaBoxConstraints.h"
#include "../VfaWidgetTools.h"
#include "../VfaBoxVis.h"
#include "../VfaQuadVis.h"
#include "../Crosshair/VfaCrosshairVis.h"
#include "../VfaProximityBoxHandle.h"
#include "../VfaProximityQuadHandle.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaBase/VistaTimer.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaDragBoxController::VfaDragBoxController(VfaBoxModel *pModel, 
											 VflRenderNode *pRenderNode,
											 float fWidgetScale /*=1.0f*/) 
	:	IVfaWidgetController(pRenderNode),
		m_pModel(pModel),
		m_pRenderNode(pRenderNode),
		m_pBoxHandle(NULL),
		m_pLastBox(NULL),
		m_pToolTip(NULL),
		m_iState(BOX_UNDEFINED),
		m_iLastState(BOX_UNDEFINED),
		m_iDragButton(0),
		m_iActiveHnd(HND_UNDEFINED),
		m_fHighlightLineWidth(2.0f),
		m_fNormalLineWidth(1.0f),
		m_iDragFace(0),
		m_fDragFaceOfs(0.0f),
		m_bUseTranslationConstraint(false),
		m_bShowLastState(true),
		m_fToolTipDelay(0.75f),
		m_fToolTipTimeSum(0.0f),
		m_fToolTipMoveRadius(fWidgetScale*0.02f),
		m_pTimer(new VistaTimer),
		m_fLastTime(0.0)
{
	//make sure we can work with that box
	assert(m_pModel->GetIsAxisAligned());


	//attach to the model
	this->Observe(m_pModel);

	//check if box's volume is != 0 -> if so we directly goto state DEFINED
	float fBox[6];
	m_pModel->GetAABox(fBox);
	float fV = {(fBox[1] - fBox[0]) *
		(fBox[3] - fBox[2]) *
		(fBox[5] - fBox[4]) };
	if(fV > 0.0f)
		this->SetDragBoxState(BOX_DEFINED);

	//initialize colors
	m_fHighlightColor[0] = 0.8f;
	m_fHighlightColor[1] = 0.8f;
	m_fHighlightColor[2] = 1.0f;
	m_fHighlightColor[3] = 1.0f;

	m_fStandardColor[0] = 0.2f;
	m_fStandardColor[1] = 0.2f;
	m_fStandardColor[2] = 1.0f;
	m_fStandardColor[3] = 1.0f;

	m_fInactiveColor[0] = 0.4f;
	m_fInactiveColor[1] = 0.4f;
	m_fInactiveColor[2] = 0.5f;
	m_fInactiveColor[3] = 1.0f;

	//====
	//CREATE HANDLES
	//====
	VistaVector3D v3Center, v3Pt, v3N;
	v3Center[0] = 0.5f * (fBox[0] + fBox[1]);
	v3Center[1] = 0.5f * (fBox[2] + fBox[3]);
	v3Center[2] = 0.5f * (fBox[4] + fBox[5]);


	//1) Proximity box
	m_pBoxHandle = new VfaProximityBoxHandle(pRenderNode);
	m_pBoxHandle->SetBox(fBox);
	this->AddControlHandle(m_pBoxHandle);

	//2) Proximity planes
	//LEFT
	m_pQuadHandles[HND_PLANE_LEFT] = new VfaProximityQuadHandle(pRenderNode);
	m_pQuadHandles[HND_PLANE_LEFT]->SetProximityDistance(0.1f * fWidgetScale);
	this->AddControlHandle(m_pQuadHandles[HND_PLANE_LEFT]);

	//RIGHT
	m_pQuadHandles[HND_PLANE_RIGHT] = new VfaProximityQuadHandle(pRenderNode);
	m_pQuadHandles[HND_PLANE_RIGHT]->SetProximityDistance(0.1f * fWidgetScale);
	this->AddControlHandle(m_pQuadHandles[HND_PLANE_RIGHT]);

	//BOTTOM
	m_pQuadHandles[HND_PLANE_BOTTOM] = new VfaProximityQuadHandle(pRenderNode);
	m_pQuadHandles[HND_PLANE_BOTTOM]->SetProximityDistance(0.1f * fWidgetScale);
	this->AddControlHandle(m_pQuadHandles[HND_PLANE_BOTTOM]);

	//TOP
	m_pQuadHandles[HND_PLANE_TOP] = new VfaProximityQuadHandle(pRenderNode);
	m_pQuadHandles[HND_PLANE_TOP]->SetProximityDistance(0.1f * fWidgetScale);
	this->AddControlHandle(m_pQuadHandles[HND_PLANE_TOP]);

	//BACK
	m_pQuadHandles[HND_PLANE_BACK] = new VfaProximityQuadHandle(pRenderNode);
	m_pQuadHandles[HND_PLANE_BACK]->SetProximityDistance(0.1f * fWidgetScale);
	this->AddControlHandle(m_pQuadHandles[HND_PLANE_BACK]);

	//FRONT
	m_pQuadHandles[HND_PLANE_FRONT] = new VfaProximityQuadHandle(pRenderNode);
	m_pQuadHandles[HND_PLANE_FRONT]->SetProximityDistance(0.1f * fWidgetScale);
	this->AddControlHandle(m_pQuadHandles[HND_PLANE_FRONT]);

	this->AlignQuadsToModelFaces();
	//====
	// CREATE HELPERS 
	//====
	//Initialize cursor
	/*m_pCursor = new VfaCrosshairVis;
	m_pCursor->Init();
	m_pVisCtl->RegisterVisObject(m_pCursor);

	VfaCrosshairVis::VfaCrosshairVisProps *pCProps = m_pCursor->GetProperties();
	pCProps->SetWingLength(fWidgetScale*0.1f);
	pCProps->SetLineWidth(m_fHighlightLineWidth);
	*/
	//Initialize vis rep for "old" box (shown when dragging a new box)
	m_pLastBox = new VfaBoxVis(new VfaBoxModel);
	m_pLastBox->Init();
	m_pRenderNode->AddRenderable(m_pLastBox);
	float fOldBoxColor[4]={0.6f, 0.6f, 0.6f, 1.0f};
	VfaBoxVis::VfaBoxVisProps *pBProps = m_pLastBox->GetProperties();
	pBProps->SetVisible(false);
	pBProps->SetLineColor(fOldBoxColor);
	pBProps->SetLineWidth(m_fNormalLineWidth);
	pBProps->SetDrawFilledFaces(false);
	
	//create ToolTip
	m_pToolTip = new Vfl3DTextLabel;
	m_pToolTip->Init();
	m_pRenderNode->AddRenderable(m_pToolTip);
	m_pToolTip->SetText("create new box");
	float fToolColor[4] = {1.0f, 1.0f, 0.4f, 1.0f};
	m_pToolTip->SetColor(fToolColor);
	m_pToolTip->SetTextSize(fWidgetScale*0.05f);
	m_fToolTipOffset[0] = fWidgetScale*0.05f;
	m_fToolTipOffset[1] = fWidgetScale*0.05f;
	m_fToolTipOffset[2] = 0.0f;
	m_pToolTip->SetPosition(m_fToolTipOffset);

	//
	m_fTranslateStartOffset[0] = 0.0f;
	m_fTranslateStartOffset[1] = 0.0f;
	m_fTranslateStartOffset[2] = 0.0f;

	m_fTranslationConstraint[0] = 0.0f;
	m_fTranslationConstraint[1] = 0.0f;
	m_fTranslationConstraint[2] = 0.0f;
	m_fTranslationConstraint[3] = 0.0f;
	m_fTranslationConstraint[4] = 0.0f;
	m_fTranslationConstraint[5] = 0.0f;

	//
	m_fLastKnownSensorPos[0] = 0.0f;
	m_fLastKnownSensorPos[1] = 0.0f;
	m_fLastKnownSensorPos[2] = 0.0f;

	//remember when we last did this!
	m_fLastTime = m_pTimer->GetMicroTime();
}

VfaDragBoxController::~VfaDragBoxController()
{
	this->ReleaseObserveable(m_pModel, IVistaObserveable::TICKET_NONE);

	//delete handles
	delete m_pBoxHandle;
	for(int i=0; i<6; ++i)
		delete m_pQuadHandles[i];
	//cursor
//	m_pVisCtl->UnregisterVisObject(m_pCursor);
//	delete m_pCursor;
	//last box and its model
	delete m_pLastBox->GetModel();
	m_pRenderNode->RemoveRenderable(m_pLastBox);
	delete m_pLastBox;
	//tooltip
	m_pRenderNode->RemoveRenderable(m_pToolTip);
	delete m_pToolTip;
	delete m_pTimer;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::OnSensorSlotUpdate(int iSlot, const 
					VfaApplicationContextObject::sSensorFrame &oFrameData)
{
	//@todo -> adapt to the new CURSOR_VIS thingee here!
	if(iSlot != VfaApplicationContextObject::SLOT_CURSOR_VIS)
		return;

	//remember last position for tooltip show state
	memcpy(	m_fLastToolTipPos, m_fLastKnownSensorPos, 
			3*sizeof(m_fLastToolTipPos[0]));
	
	//get new sensor position and save it
	oFrameData.v3Position.GetValues(m_fLastKnownSensorPos);
	float fPt[3], *fPos=m_fLastKnownSensorPos;
	
	//update ToolTip position
	fPt[0] = fPos[0] + m_fToolTipOffset[0];
	fPt[1] = fPos[1] + m_fToolTipOffset[1];
	fPt[2] = fPos[2] + m_fToolTipOffset[2];
	m_pToolTip->SetPosition(fPt);
		
	float fBounds[6];
	m_pModel->GetAABox(fBounds);

	//when we are currently "idle" -> nothing to do...
	if(m_iState == BOX_UNDEFINED || m_iState == BOX_DEFINED)
		return;

	if(m_iState == BOX_DRAGGING)
	{
		//update the dragging state i.e. update the box's bounds
		//always keep start point fixed -> second point defines the AA box
		float fNewBounds[6] = {
			std::min<float>(m_fStartDragPos[0], fPos[0]),
			std::max<float>(m_fStartDragPos[0], fPos[0]),
			std::min<float>(m_fStartDragPos[1], fPos[1]),
			std::max<float>(m_fStartDragPos[1], fPos[1]),
			std::min<float>(m_fStartDragPos[2], fPos[2]),
			std::max<float>(m_fStartDragPos[2], fPos[2])
		};
		//NOTE: this will trigger a box update automatically via the observer
		//      pattern. It will also enforce any size constraints.
		m_pModel->SetAABox(fNewBounds);
	}
	else if(m_iState == BOX_MOVING)
	{
		//set box's center based on its original offset relative to
		//the cursor position
		fPos[0] += m_fTranslateStartOffset[0];
		fPos[1] += m_fTranslateStartOffset[1];
		fPos[2] += m_fTranslateStartOffset[2];

		//check constraints if needed here
		if(m_bUseTranslationConstraint)
			VfaWidgetTools::ClampPositionToBounds(fPos, m_fTranslationConstraint);

		m_pModel->SetTranslation(fPos);
	}
	else if(m_iState == BOX_MOVING_FACE)
	{
		//move box's face
		float fNewBounds[6];
		//assume bounds stay put
		memcpy(fNewBounds, fBounds, 6*sizeof(fBounds[0]));

		switch(m_iActiveHnd)
		{
		case HND_PLANE_LEFT:
			//modifying min_x only (clamp to right face!)
			fNewBounds[0] = std::min<float>(fPos[0]+m_fDragFaceOfs, fBounds[1]);
			break;
		case HND_PLANE_RIGHT:
			//modifying max_x only (clamp to left face!)
			fNewBounds[1] = std::max<float>(fPos[0]+m_fDragFaceOfs, fBounds[0]);
			break;
		case HND_PLANE_BOTTOM:
			fNewBounds[2] = std::min<float>(fPos[1]+m_fDragFaceOfs, fBounds[3]);
			break;
		case HND_PLANE_TOP:
			fNewBounds[3] = std::max<float>(fPos[1]+m_fDragFaceOfs, fBounds[2]);
			break;
		case HND_PLANE_BACK:
			fNewBounds[4] = std::min<float>(fPos[2]+m_fDragFaceOfs, fBounds[5]);
			break;
		case HND_PLANE_FRONT:
			fNewBounds[5] = std::max<float>(fPos[2]+m_fDragFaceOfs, fNewBounds[4]);
			break;
		default:
			//this SHALL NEVER happen!
			assert(0 && "Unknown FACE!");
		}
		//NOTE: this will trigger a box update automatically via the observer
		//      pattern. It will also enforce any size constraints.
		m_pModel->SetAABox(fNewBounds);
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
//	printf("Button <%d> %s.\n", iSlot, (bSet ? "pressed" : "released"));
	//we are only interested in this one button
	if(iSlot != m_iDragButton)
		return;

	//always disable the ToolTip once the widget is activated
	m_pToolTip->SetVisible(false);
	m_fToolTipTimeSum = 0.0f;

	float fBounds[6];
	m_pModel->GetAABox(fBounds);
	
	if(!bSet)
	{
		//we got a button release
		//-> only interested if we have picked something up
		if(	m_iState != BOX_DRAGGING &&
			m_iState != BOX_MOVING &&
			m_iState != BOX_MOVING_FACE)
			return;

		//we stop interacting -> hide the last box's shape and change state
		m_pLastBox->SetVisible(false);
		this->SetDragBoxState(BOX_DEFINED);
		m_bUseTranslationConstraint = false;
		// give a last notify, as now we have a defined state
		m_pModel->Notify(VfaBoxModel::MSG_BOX_CHANGE);
	}
	else//we got a button press
	{
		//button presses are handled in either UNDEFINED (initial) or DEFINED state
		if(m_iState == BOX_UNDEFINED)
		{
			//we have not yet defined a box -> start DRAGGING
			this->SetDragBoxState(BOX_DRAGGING);

			//remember where we started and reset box to be empty!
			fBounds[0] = fBounds[1] = m_fStartDragPos[0] = m_fLastKnownSensorPos[0];
			fBounds[2] = fBounds[3] = m_fStartDragPos[1] = m_fLastKnownSensorPos[1];
			fBounds[4] = fBounds[5] = m_fStartDragPos[2] = m_fLastKnownSensorPos[2];

			//
			m_pModel->SetAABox(fBounds);
		}
		else if(m_iState == BOX_DEFINED)
		{
			//we had a box defined already 
			//-> display the current status using LastBox
			m_pLastBox->GetModel()->SetAABox(fBounds);
			m_pLastBox->GetProperties()->SetVisible(m_bShowLastState);

			//check if the box handle is active -> start translating the box
			if(m_iActiveHnd == HND_BOX)
			{
				this->SetDragBoxState(BOX_MOVING);
				//switch off the highlighting
				m_pBoxHandle->SetIsHighlighted(false);
				//remember offset between center and pos
				float fCenter[3];
				m_pModel->GetTranslation(fCenter);
				m_fTranslateStartOffset[0] = fCenter[0] - m_fLastKnownSensorPos[0];
				m_fTranslateStartOffset[1] = fCenter[1] - m_fLastKnownSensorPos[1];
				m_fTranslateStartOffset[2] = fCenter[2] - m_fLastKnownSensorPos[2];

				VfaRestrictBoxToAABounds *pConstr = NULL;
				m_pModel->GetConstraint(pConstr);
				//compute translation contstraint if the box is constraint in any way!
				if(pConstr != NULL)
				{
					float fConstrBounds[6];
					pConstr->GetBounds(fConstrBounds);
					m_bUseTranslationConstraint = true;

					float fExtents[3];
					m_pModel->GetExtents(fExtents);

					m_fTranslationConstraint[0] = fConstrBounds[0] + 0.5f * fExtents[0];
					m_fTranslationConstraint[1] = fConstrBounds[1] - 0.5f * fExtents[0];
					m_fTranslationConstraint[2] = fConstrBounds[2] + 0.5f * fExtents[1];
					m_fTranslationConstraint[3] = fConstrBounds[3] - 0.5f * fExtents[1];
					m_fTranslationConstraint[4] = fConstrBounds[4] + 0.5f * fExtents[2];
					m_fTranslationConstraint[5] = fConstrBounds[5] - 0.5f * fExtents[2];
				}
			}
			//check if close to a face -> start moving that face
			else if(m_iActiveHnd >= HND_PLANE_LEFT) 
			{
				this->SetDragBoxState(BOX_MOVING_FACE);
				//remember delta to wall
				switch(m_iActiveHnd)
				{
				case HND_PLANE_LEFT: 
					m_fDragFaceOfs = fBounds[0] - m_fLastKnownSensorPos[0];
					break;
				case HND_PLANE_RIGHT:
					m_fDragFaceOfs = fBounds[1] - m_fLastKnownSensorPos[0];
					break;
				case HND_PLANE_BOTTOM:
					m_fDragFaceOfs = fBounds[2] - m_fLastKnownSensorPos[1];
					break;
				case HND_PLANE_TOP:
					m_fDragFaceOfs = fBounds[3] - m_fLastKnownSensorPos[1];
					break;
				case HND_PLANE_BACK:
					m_fDragFaceOfs = fBounds[4] - m_fLastKnownSensorPos[2];
					break;
				case HND_PLANE_FRONT:
					m_fDragFaceOfs = fBounds[5] - m_fLastKnownSensorPos[2];
					break;
				default:
					//this MUST NEVER happen
					assert(0 && "Unknown face!");
				}
			}
			//outside the box and not close to face -> start dragging another box
			else
			{
				//we had a box defined already 
				//-> display the current status using LastBox
				this->SetDragBoxState(BOX_DRAGGING);
				
				m_pLastBox->GetModel()->SetAABox(fBounds);
				m_pLastBox->GetProperties()->SetVisible(m_bShowLastState);
				
				m_fStartDragPos[0] = fBounds[0] = fBounds[1] = m_fLastKnownSensorPos[0];
				m_fStartDragPos[1] = fBounds[2] = fBounds[3] = m_fLastKnownSensorPos[1];
				m_fStartDragPos[2] = fBounds[4] = fBounds[5] = m_fLastKnownSensorPos[2];
				m_pModel->SetAABox(fBounds);
			}
		}//->if BOX_DEFINED
	}//->if(bSet)
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::OnTimeSlotUpdate(const double dTime)
{
	//update the tooltip
	//if we are close enough to last pos -> inc ToolTip counter
	float fPt[3] = {
		m_fLastKnownSensorPos[0] - m_fLastToolTipPos[0],
		m_fLastKnownSensorPos[1] - m_fLastToolTipPos[1],
		m_fLastKnownSensorPos[2] - m_fLastToolTipPos[2] };

	float fDist = sqrt(fPt[0]*fPt[0] + fPt[1]*fPt[1] + fPt[2]*fPt[2]);
	//if we have stayed within the \epsilon-ball with radius m_fToolTipMoveRadius
	if(fDist < m_fToolTipMoveRadius)
	{
		//==> increase the time sum
		m_fToolTipTimeSum += (m_pTimer->GetMicroTime() - m_fLastTime);
//		printf("time: %.3f delay: %.3f\n", m_fToolTipTimeSum, m_fToolTipDelay);
		//if we have stayed put for long enough and are not currently 
		//interacting or disabled ==> show the tooltip
		if(	m_fToolTipTimeSum > m_fToolTipDelay &&
			m_iState != BOX_MOVING &&
			m_iState != BOX_MOVING_FACE &&
			m_iState != BOX_DRAGGING &&
			m_iState != BOX_DISABLED)
		{
			m_pToolTip->SetVisible(true);
		}
	}
	else
	{
//		printf("x");
		//we have moved to fast/far ==> hide ToolTip again
		m_pToolTip->SetVisible(false);
		m_fToolTipTimeSum = 0.0f;
	}
	//anyway, update the last time we did this!
	m_fLastTime = m_pTimer->GetMicroTime();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnFocus				                                      */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::OnFocus(IVfaControlHandle* pHandle)
{
	// ignore focus and onfocus, if we currently manipulate the widget
	if(	m_iState == BOX_DRAGGING ||
		m_iState == BOX_MOVING ||
		m_iState == BOX_MOVING_FACE)
		return;

	//when we get the focus for some handle 
	//we must not have a active handle before (i.e. this should be ensured by
	//::OnUnfocus
	//assert(m_iActiveHnd == HND_UNDEFINED);
	if (m_iActiveHnd != HND_UNDEFINED)
		return;

	//determine which handle is grabbed
	if(pHandle == m_pBoxHandle)
	{
		m_iActiveHnd = HND_BOX;
		m_pBoxHandle->SetIsHighlighted(true);
		//update ToolTip -> we'd move the box now
		m_pToolTip->SetText("move box");
	}
	else
	{
		for(int h=0; h<6; ++h)
		{
			if(m_pQuadHandles[h] == pHandle)
			{
				m_iActiveHnd = h;
				break;
			}
		}
		//if either handle has been it -> highlight it!
		if(m_iActiveHnd != HND_UNDEFINED)
		{
			m_pQuadHandles[m_iActiveHnd]->SetIsHighlighted(true);
			//update ToolTip -> a face would be moved now
			m_pToolTip->SetText("move face");
		}
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUnfocus				                                      */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::OnUnfocus()
{
	// ignore focus and onfocus, if we currently manipulate the widget
	if(	m_iState == BOX_DRAGGING ||
		m_iState == BOX_MOVING ||
		m_iState == BOX_MOVING_FACE)
		return;

	//this shall never happen!
	//assert(m_iActiveHnd != HND_UNDEFINED);
	if (m_iActiveHnd == HND_UNDEFINED)
		return;

	//release the current handle
	if(m_iActiveHnd == HND_BOX)
		m_pBoxHandle->SetIsHighlighted(false);
	else
		m_pQuadHandles[m_iActiveHnd]->SetIsHighlighted(false);
	
	m_iActiveHnd = HND_UNDEFINED;



	//if we have nothing picked up -> that's it
	//if(	m_iState != BOX_DRAGGING &&
	//	m_iState == BOX_MOVING &&
	//	m_iState == BOX_MOVING_FACE)
	//	return;

	//if no handle is active we'd create a new box
	//m_pToolTip->SetText("create new box");

	//we stop interacting -> hide the last box's shape and change state
	//m_pLastBox->SetVisible(false);
	//m_bUseTranslationConstraint = false;
	//this->SetDragBoxState(BOX_DEFINED);
	//
	//make sure that listeners to the box model get the latest changes
	//m_pModel->Notify(VfaBoxModel::MSG_BOX_CHANGE);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTouch				                                      */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::OnTouch(
					const std::vector<IVfaControlHandle*> &vecHandles)
{
	return;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUntouch				                                      */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::OnUntouch()
{
	return;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSensorMask			                                      */
/*                                                                            */
/*============================================================================*/
int VfaDragBoxController::GetSensorMask() const
{
	return (1<<VfaApplicationContextObject::SLOT_CURSOR_VIS);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCommandMask			                                  */
/*                                                                            */
/*============================================================================*/
int VfaDragBoxController::GetCommandMask() const
{
	return(1 << m_iDragButton);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeUpdate				                                  */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxController::GetTimeUpdate() const
{
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDragButton                                               */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxController::GetIsInteracting() const
{
	return m_iState != BOX_UNDEFINED && m_iState != BOX_DEFINED;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetIsEnabled                                                */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetIsEnabled(bool b)
{
	if(!b)
	{
		m_pLastBox->SetVisible(false);
		m_pToolTip->SetVisible(false);
		//always return to either of the two "idle" states
		m_iLastState = std::max<int>(BOX_UNDEFINED, m_iState);
		this->SetDragBoxState(BOX_DISABLED);

		// give a last notify, as now we have a clear undefined state
		m_pModel->Notify(VfaBoxModel::MSG_BOX_CHANGE);

	}
	else
	{
		//re-init the tooltip counter
		m_fLastTime = 0;
		//return to state before being switched off.
		this->SetDragBoxState(m_iLastState);
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIsEnabled                                                */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxController::GetIsEnabled() const
{
	return m_iState != BOX_DISABLED;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetArtItemsVisible                                          */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetAreItemsVisible(bool b)
{
	m_pBoxHandle->SetVisEnabled(b);
	
	for(int h=0; h<6; ++h)
		m_pQuadHandles[h]->SetVisEnabled(b);

	if(!b)
		m_pLastBox->GetProperties()->SetVisible(b);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetAreItemsVisible                                          */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxController::GetAreItemsVisible()
{
	return m_pBoxHandle->GetVisEnabled();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDragButton                                               */
/*                                                                            */
/*============================================================================*/
int VfaDragBoxController::GetDragButton() const
{
	return m_iDragButton;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDragButton                                               */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetDragButton(int i)
{
	m_iDragButton = i;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPlaneInteractionDist                                     */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetPlaneInteractionDist(float d)
{
	for(int h=0; h<6; ++h)
		m_pQuadHandles[h]->SetProximityDistance(d);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPlaneInteractionDist                                     */
/*                                                                            */
/*============================================================================*/
float VfaDragBoxController::GetPlaneInteractionDist() const
{
	return m_pQuadHandles[0]->GetProximityDistance();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetShowLastState                                            */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetShowLastState(bool b)
{
	m_bShowLastState = b;
	m_pLastBox->SetVisible(m_bShowLastState);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetShowLastState                                            */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxController::GetShowLastState() const
{
	return m_bShowLastState;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   SetCurrentBoxColor                                            */
/*                                                                            */
/*============================================================================*/
/*
void VfaDragBoxController::SetCurrentBoxColor(float f[4])
{
	memcpy(m_fStandardColor, f, 4*sizeof(m_fStandardColor[0]));
	if(!this->GetIsInteracting() && this->GetIsEnabled())
	{
		static_cast<VfaBoxVis::CBoxVisProps*>(
			m_pBoxVis->GetProperties())->SetLineColor(m_fStandardColor);
	}
}
*/
/*============================================================================*/
/*                                                                            */
/*  NAME    :   GetCurrentBoxColor                                            */
/*                                                                            */
/*============================================================================*/
/*
void VfaDragBoxController::GetCurrentBoxColor(float f[4]) const
{
	memcpy(f, m_fStandardColor, 4*sizeof(m_fStandardColor[0]));
}
*/
/*============================================================================*/
/*                                                                            */
/*  NAME    :   Set/GetLastBoxColor                                           */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetLastBoxColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	m_pLastBox->GetProperties()->SetLineColor(fC);
	
}
VistaColor VfaDragBoxController::GetLastBoxColor() const
{
	float fC[4];
	m_pLastBox->GetProperties()->GetLineColor(fC);
	
	VistaColor color(fC);
	return color;
}

void VfaDragBoxController::SetLastBoxColor(float f[4])
{
	m_pLastBox->GetProperties()->SetLineColor(f);
}
void VfaDragBoxController::GetLastBoxColor(float f[4]) const
{
	m_pLastBox->GetProperties()->GetLineColor(f);
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   SetHighlightColor                                             */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetHighlightColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetHighlightColor(fC);
	
}
VistaColor VfaDragBoxController::GetHighlightColor() const
{
	float fC[4];
	this->GetHighlightColor(fC);

	VistaColor color(fC);
	return color;
}
void VfaDragBoxController::SetHighlightColor(float f[4])
{
	memcpy(m_fHighlightColor, f, 4*sizeof(m_fHighlightColor[0]));
}
void VfaDragBoxController::GetHighlightColor(float f[4]) const
{
	memcpy(f, m_fHighlightColor, 4*sizeof(m_fHighlightColor[0]));
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   Set/GetInactiveColor                                          */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetInactiveColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	this->SetInactiveColor(fC);
	
}
VistaColor VfaDragBoxController::GetInactiveColor() const
{
	float fC[4];
	this->GetInactiveColor(fC);

	VistaColor color(fC);
	return color;
}
void VfaDragBoxController::SetInactiveColor(float f[4])
{
	memcpy(m_fInactiveColor, f, 4*sizeof(m_fInactiveColor[0]));
}
void VfaDragBoxController::GetInactiveColor(float f[4]) const
{
	memcpy(f, m_fInactiveColor, 4*sizeof(m_fInactiveColor[0]));
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   SetHighlightLineWidth                                         */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetHighlightLineWidth(float f)
{
	m_fHighlightLineWidth = f;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   GetActiveLineWidth                                            */
/*                                                                            */
/*============================================================================*/
float VfaDragBoxController::GetHighlightLineWidth() const
{
	return m_fHighlightLineWidth;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   SetNormalLineWidth                                            */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetNormalLineWidth(float f)
{
	m_fNormalLineWidth = f;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   GetNormalLineWidth                                            */
/*                                                                            */
/*============================================================================*/
float VfaDragBoxController::GetNormalLineWidth() const
{
	return m_fNormalLineWidth;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   SetShowToolTip                                                */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetShowToolTip(bool b)
{
	m_bShowToolTip = b;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   GetShowToolTip                                                */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxController::GetShowToolTip() const
{
	return m_bShowToolTip;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   SetToolTipDelay                                               */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetToolTipDelay(double fDelayInSeconds)
{
	m_fToolTipDelay = fDelayInSeconds;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   GetToolTipDelay                                               */
/*                                                                            */
/*============================================================================*/
double VfaDragBoxController::GetToolTipDelay() const
{
	return m_fToolTipDelay;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   SetToolTipMoveRadius                                          */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetToolTipMoveRadius(float fRadius)
{
	m_fToolTipMoveRadius = fRadius;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   GetToolTipMoveRadius                                          */
/*                                                                            */
/*============================================================================*/
float VfaDragBoxController::GetToolTipMoveRadius() const
{
	return m_fToolTipMoveRadius;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   SetToolTipOffset                                              */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::SetToolTipOffset(float fOfs[3])
{
	memcpy(m_fToolTipOffset, fOfs, 3*sizeof(m_fToolTipOffset[0]));
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   GetToolTipOffset                                              */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::GetToolTipOffset(float fOfs[3]) const
{
	memcpy(fOfs, m_fToolTipOffset, 3*sizeof(m_fToolTipOffset[0]));
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :   ObserveableDeleteRequest                                      */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxController::ObserveableDeleteRequest(
	IVistaObserveable *pObserveable, int eTicket)
{
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :  ObserveableDelete                                              */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::ObserveableDelete(
	IVistaObserveable *pObserveable, int eTicket)
{
	return;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :     ReleaseObserveable                                          */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::ReleaseObserveable(
	IVistaObserveable *pObserveable, int eTicket)
{
	pObserveable->DetachObserver(this);
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :     Observes                                                    */
/*                                                                            */
/*============================================================================*/
bool VfaDragBoxController::Observes(IVistaObserveable *pObserveable)
{
	return dynamic_cast<VfaBoxModel*>(pObserveable) == m_pModel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :    Observe                                                      */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::Observe(IVistaObserveable *pObservable, 
									int eTicket/*=IVistaObserveable::TICKET_NONE*/)
{
	pObservable->AttachObserver(this, eTicket);
}
/*============================================================================*/
/*                                                                            */
/*  NAME    :    ObserverUpdate                                               */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::ObserverUpdate(IVistaObserveable *pObservable, 
										   int iMsg, int iTicket)
{
	//we are only interested in box changes
	if(iMsg != VfaBoxModel::MSG_BOX_CHANGE)
		return;
	//
	float fBounds[6], fCnt[3];
	m_pModel->GetAABox(fBounds);
	m_pModel->GetTranslation(fCnt);

	//if we are still in UNDEFINED
	if(m_iState == BOX_UNDEFINED)
	{
		//check if box's volume is still 0
		float fV = {(fBounds[1] - fBounds[0]) *
					(fBounds[3] - fBounds[2]) *
					(fBounds[5] - fBounds[4]) };

		//if not -> goto DEFINED!
		if(fV > 0.0f)
		{
			m_iState = BOX_DEFINED;
			m_iLastState = BOX_DEFINED;
		}
	}
	//box has changed -> update the handles
	m_pBoxHandle->SetBox(fBounds);
	this->AlignQuadsToModelFaces();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AlignQuadsToModelFaces                                      */
/*                                                                            */
/*============================================================================*/
void VfaDragBoxController::AlignQuadsToModelFaces()
{
	VistaVector3D v3Pt1, v3Pt2, v3N(1.0f, 0.0f, 0.0f, 0.0f);
	float fSize[2];

	//LEFT
	m_pModel->GetCorner(VfaBoxModel::CORNER_LLB, v3Pt1);
	m_pModel->GetCorner(VfaBoxModel::CORNER_LTF, v3Pt2);
	fSize[0] = v3Pt2[2] - v3Pt1[2];
	fSize[1] = v3Pt2[1] - v3Pt1[1];
	v3Pt1 = 0.5f * (v3Pt1 + v3Pt2);
	v3N[0] = -1.0f; v3N[1] = 0.0f, v3N[2] = 0.0f;
	m_pQuadHandles[HND_PLANE_LEFT]->SetNormal(v3N);
	m_pQuadHandles[HND_PLANE_LEFT]->SetPoint(v3Pt1);
	m_pQuadHandles[HND_PLANE_LEFT]->SetExtent(fSize[0], fSize[1]);
	//RIGHT
	m_pModel->GetCorner(VfaBoxModel::CORNER_RLB, v3Pt1);
	m_pModel->GetCorner(VfaBoxModel::CORNER_RTF, v3Pt2);
	fSize[0] = v3Pt2[2] - v3Pt1[2];
	fSize[1] = v3Pt2[1] - v3Pt1[1];
	v3Pt1 = 0.5f * (v3Pt1 + v3Pt2);
	v3N[0] = 1.0f; v3N[1] = 0.0f, v3N[2] = 0.0f;
	m_pQuadHandles[HND_PLANE_RIGHT]->SetNormal(v3N);
	m_pQuadHandles[HND_PLANE_RIGHT]->SetPoint(v3Pt1);
	m_pQuadHandles[HND_PLANE_RIGHT]->SetExtent(fSize[0], fSize[1]);
	//BOTTOM
	m_pModel->GetCorner(VfaBoxModel::CORNER_LLB, v3Pt1);
	m_pModel->GetCorner(VfaBoxModel::CORNER_RLF, v3Pt2);
	fSize[0] = v3Pt2[0] - v3Pt1[0];
	fSize[1] = v3Pt2[2] - v3Pt1[2];
	v3Pt1 = 0.5f * (v3Pt1 + v3Pt2);
	v3N[0] = 0.0f; v3N[1] = -1.0f, v3N[2] = 0.0f;
	m_pQuadHandles[HND_PLANE_BOTTOM]->SetNormal(v3N);
	m_pQuadHandles[HND_PLANE_BOTTOM]->SetPoint(v3Pt1);
	m_pQuadHandles[HND_PLANE_BOTTOM]->SetExtent(fSize[0], fSize[1]);
	//TOP
	m_pModel->GetCorner(VfaBoxModel::CORNER_LTB, v3Pt1);
	m_pModel->GetCorner(VfaBoxModel::CORNER_RTF, v3Pt2);
	fSize[0] = v3Pt2[0] - v3Pt1[0];
	fSize[1] = v3Pt2[2] - v3Pt1[2];
	v3Pt1 = 0.5f * (v3Pt1 + v3Pt2);
	v3N[0] = 0.0f; v3N[1] = 1.0f, v3N[2] = 0.0f;
	m_pQuadHandles[HND_PLANE_TOP]->SetNormal(v3N);
	m_pQuadHandles[HND_PLANE_TOP]->SetPoint(v3Pt1);
	m_pQuadHandles[HND_PLANE_TOP]->SetExtent(fSize[0], fSize[1]);
	//BACK
	m_pModel->GetCorner(VfaBoxModel::CORNER_LLB, v3Pt1);
	m_pModel->GetCorner(VfaBoxModel::CORNER_RTB, v3Pt2);
	fSize[0] = v3Pt2[0] - v3Pt1[0];
	fSize[1] = v3Pt2[1] - v3Pt1[1];
	v3Pt1 = 0.5f * (v3Pt1 + v3Pt2);
	v3N[0] = 0.0f; v3N[1] = 0.0f, v3N[2] = -1.0f;
	m_pQuadHandles[HND_PLANE_BACK]->SetNormal(v3N);
	m_pQuadHandles[HND_PLANE_BACK]->SetPoint(v3Pt1);
	m_pQuadHandles[HND_PLANE_BACK]->SetExtent(fSize[0], fSize[1]);
	//FRONT
	m_pModel->GetCorner(VfaBoxModel::CORNER_LLF, v3Pt1);
	m_pModel->GetCorner(VfaBoxModel::CORNER_RTF, v3Pt2);
	fSize[0] = v3Pt2[0] - v3Pt1[0];
	fSize[1] = v3Pt2[1] - v3Pt1[1];
	v3Pt1 = 0.5f * (v3Pt1 + v3Pt2);
	v3N[0] = 0.0f; v3N[1] = 0.0f, v3N[2] = 1.0f;
	m_pQuadHandles[HND_PLANE_FRONT]->SetNormal(v3N);
	m_pQuadHandles[HND_PLANE_FRONT]->SetPoint(v3Pt1);
	m_pQuadHandles[HND_PLANE_FRONT]->SetExtent(fSize[0], fSize[1]);
}


void VfaDragBoxController::SetDragBoxState(int iNewState)
{
	int iOldState = m_iState;
	m_iState = iNewState;
	this->PrintState(iOldState, iNewState);
}

void VfaDragBoxController::PrintState(int iOldState, int iCurrentState) const
{
	std::string strOldState = "UNKNOWN";
	std::string strNewState = "UNKNOWN";
	switch(iOldState)
	{
		case BOX_UNDEFINED: strOldState = "UNDEFINED"; break;
		case BOX_DRAGGING: strOldState = "DRAGGING"; break;
		case BOX_DEFINED: strOldState = "DEFINED"; break;
		case BOX_MOVING_FACE: strOldState = "MOVING_FACE"; break;
		case BOX_MOVING: strOldState = "MOVING"; break;
		case BOX_DISABLED: strOldState = "BOX_DISABLED"; break;
	}
	switch(iCurrentState)	
	{
		case BOX_UNDEFINED: strNewState = "UNDEFINED"; break;
		case BOX_DRAGGING: strNewState = "DRAGGING"; break;
		case BOX_DEFINED: strNewState = "DEFINED"; break;
		case BOX_MOVING_FACE: strNewState = "MOVING_FACE"; break;
		case BOX_MOVING: strNewState = "MOVING"; break;
		case BOX_DISABLED: strNewState = "BOX_DISABLED"; break;
	}
	vstr::outi()<<"[VfaDragBoxController] DragBoxState change : "<<strOldState.c_str()<<" -> "<<strNewState.c_str()<<endl;
}

#if 0
int VfaDragBoxController::DetermineTouchedFace(float fPos[3], float fBounds[6]) const
{
	//minimum is base point for left, bottom and back faces
	float fBase[3] = {fBounds[0], fBounds[2], fBounds[4]};
	if(IsCloseToFace(VfaBoxModel::FACE_LEFT, fPos, fBase, fBounds))
	{
		return VfaBoxModel::FACE_LEFT;
	}
	if(IsCloseToFace(VfaBoxModel::FACE_BOTTOM, fPos, fBase, fBounds))
	{
		return VfaBoxModel::FACE_BOTTOM;
	}
	if(IsCloseToFace(VfaBoxModel::FACE_BACK, fPos, fBase, fBounds))
	{
		return VfaBoxModel::FACE_BACK;
	}

	//max is close to right, top and front faces
	fBase[0] = fBounds[1];
	fBase[1] = fBounds[3];
	fBase[2] = fBounds[5];
	if(IsCloseToFace(VfaBoxModel::FACE_RIGHT, fPos, fBase, fBounds))
	{
		return VfaBoxModel::FACE_RIGHT;
	}
	if(IsCloseToFace(VfaBoxModel::FACE_TOP, fPos, fBase, fBounds))
	{
		return VfaBoxModel::FACE_TOP;
	}
	if(IsCloseToFace(VfaBoxModel::FACE_FRONT, fPos, fBase, fBounds))
	{
		return VfaBoxModel::FACE_FRONT;
	}

	//still here? we ain't close to anybody
	return -1;
}

bool VfaDragBoxController::IsCloseToFace(int iFace, float fPos[3], 
										  float fBase[3], float fBounds[6]) const
{
	float fURange[2], fVRange[2], fU, fV, fD;
	//determine the right 2D coordinates for each of the possible faces
	//and compute the (signed) distance to the respective plane
	switch(iFace)
	{
	case VfaBoxModel::FACE_LEFT:
		//2D: z -> u and y -> v
		fURange[0] = fBounds[4]; fURange[1] = fBounds[5];
		fVRange[0] = fBounds[2]; fVRange[1] = fBounds[3];
		fU = fPos[2]; fV = fPos[1];
		//we have to be to the left of Base
		fD = fBase[0] - fPos[0];
		break;
	case VfaBoxModel::FACE_RIGHT:
		//2D: z -> u and y -> v
		fURange[0] = fBounds[4]; fURange[1] = fBounds[5];
		fVRange[0] = fBounds[2]; fVRange[1] = fBounds[3];
		fU = fPos[2]; fV = fPos[1];
		//we have to be to the right of Base
		fD = fPos[0] - fBase[0];
		break;
	case VfaBoxModel::FACE_TOP:
		//2D: x -> u and z -> v
		fURange[0] = fBounds[0]; fURange[1] = fBounds[1];
		fVRange[0] = fBounds[4]; fVRange[1] = fBounds[5];
		fU = fPos[0]; fV = fPos[2];
		//we have to be to the top of Base
		fD = fPos[1] - fBase[1];
		break;
	case VfaBoxModel::FACE_BOTTOM:
		//2D: x -> u and z -> v
		fURange[0] = fBounds[0]; fURange[1] = fBounds[1];
		fVRange[0] = fBounds[4]; fVRange[1] = fBounds[5];
		fU = fPos[0]; fV = fPos[2];
		//we have to be to the bottom of Base
		fD = fBase[1] - fPos[1];
		break;
	case VfaBoxModel::FACE_FRONT:
		//2D: x -> u and y -> v
		fURange[0] = fBounds[0]; fURange[1] = fBounds[1];
		fVRange[0] = fBounds[2]; fVRange[1] = fBounds[3];
		fU = fPos[0]; fV = fPos[1];
		//we have to be to the front of Base
		fD = fPos[2] - fBase[2];
		break;
	case VfaBoxModel::FACE_BACK:
		//2D: x -> u and y -> v
		fURange[0] = fBounds[0]; fURange[1] = fBounds[1];
		fVRange[0] = fBounds[2]; fVRange[1] = fBounds[3];
		fU = fPos[0]; fV = fPos[1];
		//we have to be to the front of Base
		fD = fBase[2] - fPos[2];
		break;
	};

	//         inside the horizontal (u) range
	return	fURange[0] < fU && fU < fURange[1] &&
		// inside the vertical (v) range
		fVRange[0] < fV && fV < fVRange[1] &&
		// distance to plane is > 0 (on the right side) 
		// and < minDist (close enough)
		fD > 0.0 && fD < m_fPlaneInteractionDist;
}

void VfaDragBoxController::HighlightBox()
{
	//use box vis to highlight the selection state of entire box
	static_cast<VfaBoxVis::CBoxVisProps*>(
		m_pBoxVis->GetProperties())->SetLineColor(m_fHighlightColor);
	static_cast<VfaBoxVis::CBoxVisProps*>(
		m_pBoxVis->GetProperties())->SetLineWidth(m_fHighlightLineWidth);
}

void VfaDragBoxController::LowlightBox()
{
	static_cast<VfaBoxVis::CBoxVisProps*>(
		m_pBoxVis->GetProperties())->SetLineColor(m_fStandardColor);
	static_cast<VfaBoxVis::CBoxVisProps*>(
		m_pBoxVis->GetProperties())->SetLineWidth(m_fNormalLineWidth);
}
#endif

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnMovement                                                  */
/*                                                                            */
/*============================================================================*/
#if 0
bool VfaDragBoxController::OnMovement(const VistaVector3D &v3Pos, 
									   const VistaQuaternion &qOri)
{
	return true;
}
#endif
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnButtonPress                                               */
/*                                                                            */
/*============================================================================*/
#if 0
bool VfaDragBoxController::OnButtonPress(int iButton, const VistaVector3D &v3Pos, 
										  const VistaQuaternion &qOri)
{
	//no focus -> no fun
	if(m_iState == BOX_OUT_OF_FOCUS)
		return false;

	// we are only interested in our drag button iff we are 
	// awaiting a new button press
	if(iButton != m_iDragButton || 
		m_iState == BOX_DRAGGING ||
		m_iState == BOX_MOVING ||
		m_iState == BOX_MOVING_FACE)
		return false;

	//always disable the ToolTip once the widget is activated
	m_pToolTip->SetVisible(false);
	m_fToolTipTimeSum = 0.0f;

	float fBounds[6], fPos[3];
	m_pModel->GetBounds(fBounds);
	v3Pos.GetValues(fPos);

	/*
	//always clamp pos to be within bounds of current constraints
	VfaBoxBoundsConstraint *pConstr = 
	dynamic_cast<VfaBoxBoundsConstraint*>(m_pModel->GetConstraint());
	if(pConstr)
	{
	float fConstrBounds[6];
	pConstr->GetBoundsConstraint(fConstrBounds);
	VfaWidgetTools::ClampPositionToBounds(fPos, fConstrBounds);
	}
	*/

	//button presses are handled in either UNDEFINED (initial) or DEFINED state
	if( m_iState == BOX_UNDEFINED)
	{
		//we have not yet defined a box -> start DRAGGING
		m_iState = BOX_DRAGGING;

		//remember where we started and reset box to be empty!
		fBounds[0] = fBounds[1] = m_fStartDragPos[0] = fPos[0];
		fBounds[2] = fBounds[3] = m_fStartDragPos[1] = fPos[1];
		fBounds[4] = fBounds[5] = m_fStartDragPos[2] = fPos[2];

		//
		m_pModel->SetBounds(fBounds);
	}
	else if(m_iState == BOX_DEFINED)
	{
		//we had a box defined already 
		//-> display the current status using LastBox
		m_pLastBox->SetBoxBounds(fBounds);
		m_pLastBox->SetVisible(m_bShowLastState);

		//check if inside the box -> start translating the box
		if(VfaWidgetTools::IsInsideBox(fPos, fBounds))
		{
			m_iState = BOX_MOVING;
			//switch off the highlighting
			this->LowlightBox();
			//remember offset between center and pos
			float fCenter[3];
			m_pModel->GetCenter(fCenter);
			m_fTranslateStartOffset[0] = fCenter[0] - fPos[0];
			m_fTranslateStartOffset[1] = fCenter[1] - fPos[1];
			m_fTranslateStartOffset[2] = fCenter[2] - fPos[2];

			//compute translation contstraint if the box is constraint in any way!
			VfaBoxBoundsConstraint *pConstr = 
				dynamic_cast<VfaBoxBoundsConstraint*>(m_pModel->GetConstraint());
			if(pConstr)
			{
				float fConstrBounds[6];
				pConstr->GetBoundsConstraint(fConstrBounds);
				m_bUseTranslationConstraint = true;

				float fExtents[3];
				m_pModel->GetExtents(fExtents);

				m_fTranslationConstraint[0] = fConstrBounds[0] + 0.5f * fExtents[0];
				m_fTranslationConstraint[1] = fConstrBounds[1] - 0.5f * fExtents[0];
				m_fTranslationConstraint[2] = fConstrBounds[2] + 0.5f * fExtents[1];
				m_fTranslationConstraint[3] = fConstrBounds[3] - 0.5f * fExtents[1];
				m_fTranslationConstraint[4] = fConstrBounds[4] + 0.5f * fExtents[2];
				m_fTranslationConstraint[5] = fConstrBounds[5] - 0.5f * fExtents[2];
				//VfaWidgetTools::ClampPositionToBounds(fPos, fConstrBounds);
			}
		}
		//check if close to a face -> start moving that face
		else 
		{
			int iFace = this->DetermineTouchedFace(fPos, fBounds);
			if(iFace >= 0)
			{
				m_iState = BOX_MOVING_FACE;
				//remember drag face
				m_iDragFace = iFace;
				//remember delta to wall
				switch(iFace)
				{
				case VfaBoxModel::FACE_LEFT: 
					m_fDragFaceOfs = fBounds[0] - fPos[0];
					break;
				case VfaBoxModel::FACE_RIGHT:
					m_fDragFaceOfs = fBounds[1] - fPos[0];
					break;
				case VfaBoxModel::FACE_BOTTOM:
					m_fDragFaceOfs = fBounds[2] - fPos[1];
					break;
				case VfaBoxModel::FACE_TOP:
					m_fDragFaceOfs = fBounds[3] - fPos[1];
					break;
				case VfaBoxModel::FACE_BACK:
					m_fDragFaceOfs = fBounds[4] - fPos[2];
					break;
				case VfaBoxModel::FACE_FRONT:
					m_fDragFaceOfs = fBounds[5] - fPos[2];
					break;
				default:
					//this MUST NEVER happen
					assert(0 && "Unknown face!");
				}
			}
			//outside the box and not close to face -> start dragging another box
			else
			{
				m_iState = BOX_DRAGGING;
				m_fStartDragPos[0] = fBounds[0] = fBounds[1] = fPos[0];
				m_fStartDragPos[1] = fBounds[2] = fBounds[3] = fPos[1];
				m_fStartDragPos[2] = fBounds[4] = fBounds[5] = fPos[2];
				m_pModel->SetBounds(fBounds);
			}
		}
	}
	return true;
}
#endif
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnButtonRelease                                             */
/*                                                                            */
/*============================================================================*/
#if 0
bool VfaDragBoxController::OnButtonRelease(int iButton, 
											const VistaVector3D &v3Pos, 
											const VistaQuaternion &qOri)
{
	//no focus -> no fun
	if(m_iState == BOX_OUT_OF_FOCUS)
		return false;

	//only interested if we got the right button
	if(iButton != m_iDragButton)
		return false;
	//only interested if we have picked something up
	if(	m_iState != BOX_DRAGGING &&
		m_iState != BOX_MOVING &&
		m_iState != BOX_MOVING_FACE)
		return false;

	//we stop interacting -> hide the last box's shape and change state
	m_pLastBox->SetVisible(false);
	m_iState = BOX_DEFINED;
	m_bUseTranslationConstraint = false;

	//make sure that listeners to the box model get the latest changes
	m_pModel->Notify(VfaBoxModel::MSG_BOUNDS_CHANGED);

	//NOTE: technically speaking we skip the last update pass here which 
	//      should not be too much of an issue since we miss at most one frame!
	return false;
}
#endif
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnPreLoop                                                   */
/*                                                                            */
/*============================================================================*/
#if 0
bool VfaDragBoxController::OnPreLoop(const VistaVector3D &v3Pos, 
									  const VistaQuaternion &qOri)
{
	//no focus -> no fun
	if(m_iState == BOX_OUT_OF_FOCUS)
		return false;

	float fPos[3];
	v3Pos.GetValues(fPos);
	//clamp pos to be withing bounds if necessary
	//if(m_bEnforceConstraint)
	//	VfaWidgetTools::ClampPositionToBounds(fPos, m_fBoxConstraint);


	//first update the tooltip
	//if we are close enough to last pos -> inc ToolTip counter
	float fPt[3] = {
		fPos[0] - m_fLastToolTipPos[0],
		fPos[1] - m_fLastToolTipPos[1],
		fPos[2] - m_fLastToolTipPos[2] };
		float fDist = sqrt(fPt[0] * fPt[0] + fPt[1] * fPt[1] + fPt[2] * fPt[2]);
		if(fDist < m_fToolTipMoveRadius)
		{
			m_fToolTipTimeSum += (m_pTimer->GetTime() - m_fLastTime);
			m_fLastTime = m_pTimer->GetTime();
			if(m_fToolTipTimeSum > m_fToolTipDelay &&
				m_iState != BOX_MOVING &&
				m_iState != BOX_MOVING_FACE &&
				m_iState != BOX_DRAGGING)
				m_pToolTip->SetVisible(true);
		}
		else
		{
			m_fToolTipTimeSum = 0.0f;
			//not close enough -> hide ToolTip
			m_pToolTip->SetVisible(false);
		}
		//update ToolTip pos
		fPt[0] = fPos[0] + m_fToolTipOffset[0];
		fPt[1] = fPos[1] + m_fToolTipOffset[1];
		fPt[2] = fPos[2] + m_fToolTipOffset[2];
		m_pToolTip->SetPosition(fPt);
		//remember last position
		memcpy(m_fLastToolTipPos, fPos, 3*sizeof(m_fLastToolTipPos[0]));

		//make sure the cursor always follows the pt pos
		m_pCursor->SetCenter(fPos);

		float fBounds[6];
		m_pModel->GetBounds(fBounds);

		//when we are currently "idle" check if we have to highlight something
		if(m_iState == BOX_UNDEFINED || m_iState == BOX_DEFINED)
		{
			//check if we are inside the box to highlight it (for translation)
			if(VfaWidgetTools::IsInsideBox(fPos, fBounds))
			{
				//we have closed in from the outside -> the plane may be highlighted
				//->switch it off again
				m_pPlane->SetVisible(false);
				//highlight the box
				this->HighlightBox();
				//update ToolTip
				m_pToolTip->SetText("move box");
			}
			//check if we are close to (the outside) of a plane to highlight it
			else 
			{
				//determine if there is a plane and switch of just this one!
				int iFace = this->DetermineTouchedFace(fPos, fBounds);
				if(iFace >= 0)
				{
					this->AlignPlaneToModelFace(iFace);
					m_pPlane->SetVisible(true);
					m_pToolTip->SetText("move face");
				}
				else
				{
					m_pPlane->SetVisible(false);
					m_pToolTip->SetText("create new box");
				}

				//we are outside of the box so lowlight it once again!
				this->LowlightBox();
			}
		}
		else if(m_iState == BOX_DRAGGING)
		{
			//update the dragging state i.e. update the box's bounds
			//always keep start point fixed -> second point defines the AA box
			float fNewBounds[6] = {
				std::min<float>(m_fStartDragPos[0], fPos[0]),
				std::max<float>(m_fStartDragPos[0], fPos[0]),
				std::min<float>(m_fStartDragPos[1], fPos[1]),
				std::max<float>(m_fStartDragPos[1], fPos[1]),
				std::min<float>(m_fStartDragPos[2], fPos[2]),
				std::max<float>(m_fStartDragPos[2], fPos[2])
			};
			//NOTE: this will trigger a box update automatically via the observer
			//      pattern. This will also enforce any size constraints.
			m_pModel->SetBounds(fNewBounds);
		}
		else if(m_iState == BOX_MOVING)
		{
			//set box's center based on its original offset relative to
			//the cursor position
			fPos[0] += m_fTranslateStartOffset[0];
			fPos[1] += m_fTranslateStartOffset[1];
			fPos[2] += m_fTranslateStartOffset[2];

			//check constraints if needed here
			if(m_bUseTranslationConstraint)
				VfaWidgetTools::ClampPositionToBounds(fPos, m_fTranslationConstraint);

			m_pModel->SetCenter(fPos);
			//make sure nobody uses the pos hereafter
			return false;
		}
		else if(m_iState == BOX_MOVING_FACE)
		{
			//move box's face
			float fNewBounds[6];
			//assum bounds stay put
			memcpy(fNewBounds, fBounds, 6*sizeof(fBounds[0]));

			switch(m_iDragFace)
			{
			case VfaBoxModel::FACE_LEFT:
				//modifying min_x only (clamp to right face!)
				fNewBounds[0] = std::min<float>(fPos[0]+m_fDragFaceOfs, fBounds[1]);
				break;
			case VfaBoxModel::FACE_RIGHT:
				//modifying max_x only (clamp to left face!)
				fNewBounds[1] = std::max<float>(fPos[0]+m_fDragFaceOfs, fBounds[0]);
				break;
			case VfaBoxModel::FACE_BOTTOM:
				fNewBounds[2] = std::min<float>(fPos[1]+m_fDragFaceOfs, fBounds[3]);
				break;
			case VfaBoxModel::FACE_TOP:
				fNewBounds[3] = std::max<float>(fPos[1]+m_fDragFaceOfs, fBounds[2]);
				break;
			case VfaBoxModel::FACE_BACK:
				fNewBounds[4] = std::min<float>(fPos[2]+m_fDragFaceOfs, fBounds[5]);
				break;
			case VfaBoxModel::FACE_FRONT:
				fNewBounds[5] = std::max<float>(fPos[2]+m_fDragFaceOfs, fNewBounds[4]);
				break;
			default:
				//this SHALL NEVER happen!
				assert(0 && "Unknown FACE!");
			}
			//NOTE: this will trigger a box update automatically via the observer
			//      pattern. This will also enforce any size constraints.
			m_pModel->SetBounds(fNewBounds);
		}
		//NEVER take the event from the BUS!
		return false;
}
#endif
/*============================================================================*/
/*  END OF FILE "VfaDragBoxController.cpp"                                    */
/*============================================================================*/
