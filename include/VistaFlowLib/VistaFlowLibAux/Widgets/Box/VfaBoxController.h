/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFABOXCONTROLLER_H
#define _VFABOXCONTROLLER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetController.h"
#include "../VfaSphereHandle.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaReflectionable.h>
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaSphereHandleVis;
class VistaColor;
class VistaIndirectXform;
class VfaBoxModel;
class VflRenderNode;
class VfaBoxConstraints;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Controll the behavior on button press event, button release event and 
 * pre-loop event, also highlight.
 */
class VISTAFLOWLIBAUXAPI VfaBoxController : public VflObserver, public IVfaWidgetController
{
public:
	VfaBoxController(VfaBoxModel *pModel, VflRenderNode *pRenderNode);
	virtual ~VfaBoxController();

	/** 
	 * This is called on transition change into(out of) state FOCUS
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();

	/**
	 * This is called on transition change into(out of) state TOUCH
	 */
	void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	void OnUntouch();

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	void OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate(int iSlot, const bool bSet);
	void OnTimeSlotUpdate(const double dTime);
	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetInteractionButton(int iBtId);
	int  GetInteractionButton() const;

// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);


	class VISTAFLOWLIBAUXAPI VfaBoxControllerProps : public IVistaReflectionable
	{
		friend class VfaBoxController;
	public:
		enum FIXPOINT{
			FIXPOINT_CORNER,
			FIXPOINT_CENTER
		};


		VfaBoxControllerProps(VfaBoxController *pBoxCtrl);
		virtual ~VfaBoxControllerProps();


		/*
		 * Use the current aspect ratio as a constraint for further
		 * modification.
		 */
		bool SetFixAspectRatio(bool bFixAspectRatio);
		bool GetFixAspectRatio() const;

		/*
		 * Allow turning the box inside out by moving m_v3BoxMin beyond
		 * m_v3BoxMax. Internally m_v3BoxMin and m_v3BoxMax are reset to keep
		 * box constraints.
		 */
		bool SetAllowPenetration(bool bAllowPenetration);
		bool GetAllowPenetration() const;

		/*
		 * Grabbing a corner of the box and changing the box size, this is the
		 * point which stays fix. This can eiter be the center of the box or the
		 * corner across the grabbed corner.
		 */
		bool SetFixpoint(FIXPOINT eFixpoint);
		FIXPOINT GetFixpoint() const;

		bool SetHandleRadius(float fRadius);
		float GetHandleRadius() const;

		bool SetHandleNormalColor(const VistaColor& color);
		VistaColor GetHandleNormalColor() const;
		void SetHandleNormalColor(float fC[4]);
		void GetHandleNormalColor(float fC[4]) const;

		bool SetHandleHighlightColor(const VistaColor& color);
		VistaColor GetHandleHighlightColor() const;
		void SetHandleHighlightColor(float fC[4]);
		void GetHandleHighlightColor(float fC[4]) const;

		bool SetTranslationHandleNormalColor(const VistaColor& color);
		VistaColor GetTranslationHandleNormalColor() const;
		void SetTranslationHandleNormalColor(float fC[4]);
		void GetTranslationHandleNormalColor(float fC[4]) const;

		bool SetTranslationHandleHighlightColor(const VistaColor& color);
		VistaColor GetTranslationHandleHighlightColor() const;
		void SetTranslationHandleHighlightColor(float fC[4]);
		void GetTranslationHandleHighlightColor(float fC[4]) const;


		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;


	private:
		VfaBoxController	*m_pBoxCtrl;

		bool				m_bFixAspectRatio;
		bool				m_bAllowPenetration;
		FIXPOINT			m_eFixpoint;
	};


	VfaBoxControllerProps* GetProperties() const;

protected:

	void UpdateHandles();

private:
	enum eBoxControllerState { BCS_NONE, BCS_MOVE, BCS_RESIZE };
	VfaBoxController::eBoxControllerState	m_iCurrentInternalState;

	VfaBoxModel				*m_pModel;

	int							m_iInteractionButton;

	vector<VfaSphereHandle*>	m_vecHandles;
	VistaIndirectXform			*m_pXform;

	VfaBoxControllerProps		*m_pCtlProps;
	
	// focused handle and its number
	VfaSphereHandle*		m_pFocusHandle;
	int							m_iHandle;

	// translation handle number
	const int					m_iTranslationHandle;

	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;

	bool						m_bEnabled;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFABOXCONTROLLER_H
