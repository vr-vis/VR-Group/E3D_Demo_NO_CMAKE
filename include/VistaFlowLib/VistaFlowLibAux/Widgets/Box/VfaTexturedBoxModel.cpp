/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaTexturedBoxModel.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaOGLUtils.h>


// ========================================================================== //
// === Con-/Destructors
// ========================================================================== //
VfaTexturedBoxModel::VfaTexturedBoxModel()
 : m_pTexture(0)
{
	m_pTexture = new VistaTexture(GL_TEXTURE_2D);
}

VfaTexturedBoxModel::~VfaTexturedBoxModel()
{
	if(m_pTexture)
		delete m_pTexture;
	m_pTexture = 0;
}


// ========================================================================== //
// === Public Interface
// ========================================================================== //
bool VfaTexturedBoxModel::LoadTexture(std::string strFilename) const
{
	// Design by contract: To comply to what stands written in the header file
	// a call to this function must never return without unbinding whatever
	// is bound to currently active tex unit at the 2D target to not lead to
	// unpredictable side effects. Hence we'll simply unbind anything bound
	// to the current tex unit at the 2D target to be sure.
	glBindTexture(GL_TEXTURE_2D, 0);

	if(strFilename == "" || !m_pTexture)
		return false;

	int iWidth, iHeight, iNumChannels;

	// Load the data from the filename supplied.
	unsigned char *pData = VistaOGLUtils::LoadImageFromFile(strFilename,
		iWidth, iHeight, iNumChannels);

	if(!pData)
		return false;

	// Create the actual texture.
	m_pTexture->Bind();
	if(iNumChannels == 3)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, iNumChannels, iWidth, iHeight, 0,
			GL_RGB, GL_UNSIGNED_BYTE, pData);
	}
	else if(iNumChannels == 4)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, iNumChannels, iWidth, iHeight, 0,
			GL_RGBA, GL_UNSIGNED_BYTE, pData);
	}
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glGenerateMipmapEXT(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Delete the data loaded using the oglext utils.
	VistaOGLUtils::Delete(pData);

	return true;
}

VistaTexture* VfaTexturedBoxModel::GetTexture() const
{
	return m_pTexture;
}

bool VfaTexturedBoxModel::SetTexture(VistaTexture* pTex)
{
	if(pTex == NULL)
		return false;

	m_pTexture = pTex;
	return true;
}


// ========================================================================== //
// === End of File
// ========================================================================== //
