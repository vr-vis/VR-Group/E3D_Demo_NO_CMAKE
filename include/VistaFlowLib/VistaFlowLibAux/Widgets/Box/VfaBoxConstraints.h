/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFABOXCONSTRAINTS_H
#define _VFABOXCONSTRAINTS_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetConstraints.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaRestrictBoxToAxisAlignedBox : public IVfaWidgetConstraint
{
public:
	VfaRestrictBoxToAxisAlignedBox();
	virtual ~VfaRestrictBoxToAxisAlignedBox();

	virtual bool CheckConstraint(const VfaWidgetModelBase*const pModel);

private:
	
};

class VISTAFLOWLIBAUXAPI VfaRestrictBoxToCube : public IVfaWidgetConstraint
{
public:
	VfaRestrictBoxToCube();
	virtual ~VfaRestrictBoxToCube();

	virtual bool CheckConstraint(const VfaWidgetModelBase*const pModel);
	
private:

};


class VISTAFLOWLIBAUXAPI VfaRestrictBoxToAABounds : public IVfaWidgetConstraint
{
public:
	VfaRestrictBoxToAABounds();
	virtual ~VfaRestrictBoxToAABounds();

	virtual bool CheckConstraint(const VfaWidgetModelBase*const pModel);

	void SetBounds(float fBounds[6]);
	void GetBounds(float fBounds[6]) const;
	
private:
	float m_fBounds[6];
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFABOXCONSTRAINTS_H

