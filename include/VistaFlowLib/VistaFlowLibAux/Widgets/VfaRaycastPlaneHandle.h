/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFARAYCASTPLANEHANDLE_H
#define VFARAYCASTPLANEHANDLE_H

/*============================================================================*/
/* INCLUDES														              */
/*============================================================================*/
#include "../VistaFlowLibAuxConfig.h"
#include "VfaRaycastHandle.h"


/*============================================================================*/
/* FORWARD DECLARATION											              */
/*============================================================================*/
class IVistaTransformable;


/*============================================================================*/
/* CLASS DEFINITION												              */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaRaycastPlaneHandle : public IVfaRaycastHandle
{
public:
	VfaRaycastPlaneHandle(IVistaTransformable *pCoordFrame);
	virtual ~VfaRaycastPlaneHandle();


	//! Updates the extent of the plane.
	/**
	 * @param v3Right The right vector of the plane, also encoding the width.
	 * @param v3Up The up vector of the plane, also encoding the height.
	 * @param v3Origin The origin of the plane.
	 * @return Returns 'true' iff the vectors were accepted.
	 *
	 * NOTE: The plane is assumed to be specified in its own coordinate frame.
	 */
	virtual bool UpdateExtents(const VistaVector3D &v3Right,
		const VistaVector3D &v3Up, const VistaVector3D &v3Origin);
	//! Retrieves the planes extents.
	/**
	 * @param v3Right The right vector of the plane.
	 * @param v3Up The up vector of the plane.
	 * @param v3Origin The origin of the plane.
	 */
	virtual void RetrieveExtents(VistaVector3D &v3Right, VistaVector3D &v3Up,
		VistaVector3D &v3Origin) const;


	// *** IVfaRaycastHandle interface. ***
	virtual bool Intersects(const VistaVector3D &v3RayOrigin,
		const VistaVector3D &v3RayDir, float &fParam);


protected:
	//! Checks whether an intersection lies within the bounds of the plane.
	/**
	 * @param v3RayOrigin The origin of the ray whose intersection to test.
	 * @param v3RayDir The direction of the ray whose intersection to test.
	 * @param fParam The parameter defining the position on the ray at which
	 *		  the intersection occured.
	 * @return Returns 'true' iff the intersection lies within the plane.
	 */
	bool IntersectWithinPlane(const VistaVector3D &v3RayOrigin,
		const VistaVector3D &v3RayDir, float fParam) const;


private:
	VistaVector3D m_v3Right;
	VistaVector3D m_v3Up;
	VistaVector3D m_v3Normal;
	VistaVector3D m_v3Origin;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE													              */
/*============================================================================*/
