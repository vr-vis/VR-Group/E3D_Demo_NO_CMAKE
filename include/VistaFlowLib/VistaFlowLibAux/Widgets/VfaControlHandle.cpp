/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaControlHandle.h"
#include <VistaBase/VistaVectorMath.h>

using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION															  */
/*============================================================================*/
IVfaControlHandle::IVfaControlHandle(int iType)
:	m_bIsHighlighted(false)
,   m_iType(iType)
,   m_bIsEnable(true)
,   m_bIsVisible(true)
{ }

IVfaControlHandle::~IVfaControlHandle()
{ }


int IVfaControlHandle::GetType()
{
	return m_iType;
}


void IVfaControlHandle::SetIsHighlighted(bool bIsHighlighted)
{
	m_bIsHighlighted = bIsHighlighted;
}

bool IVfaControlHandle::GetIsHighlighted() const
{
	return  m_bIsHighlighted;
}


void IVfaControlHandle::SetVisible(bool bIsVisible)
{
	m_bIsVisible = bIsVisible;
	
	// To avoid illogical behaviour, invisible handles are deactivated.
	if(!bIsVisible)
		m_bIsEnable = false;
}

bool IVfaControlHandle::GetVisible()
{
	return m_bIsVisible;
}


void IVfaControlHandle::SetEnable(bool bIsEnabled)
{
	m_bIsEnable = bIsEnabled;
	
	// Same as with SetVisible, to avoid illogical behavious, handles that are
	// made active will also be made visible.
	if(bIsEnabled)
		m_bIsVisible = true;
}

bool IVfaControlHandle::GetEnable()
{
	return m_bIsEnable;
}

////////////////////////////////////////////////////////////////////////////////

IVfaCenterControlHandle::IVfaCenterControlHandle()
:	IVfaControlHandle(HANDLE_CENTER)
{ }

IVfaCenterControlHandle::~IVfaCenterControlHandle()
{ }


void IVfaCenterControlHandle::SetCenter(const VistaVector3D &v3Center)
{
	m_v3Center = v3Center;
}

void IVfaCenterControlHandle::GetCenter(VistaVector3D &v3Center) const
{
	v3Center = m_v3Center;
}

const VistaVector3D& IVfaCenterControlHandle::GetCenter() const
{
	return m_v3Center;
}


bool IVfaCenterControlHandle::GetPosition(VistaVector3D &pTrans, const VistaReferenceFrame& oReferenceFrame) const
{
	pTrans = m_v3Center;
	return true;
}


void IVfaCenterControlHandle::SetVisible(bool bIsVisible)
{
	m_bIsVisible = bIsVisible;
	
	// To avoid illogical behaviour, invisible handles are deactivated.
	if(!m_bIsVisible)
		SetIsSelectionEnabled(false);	
}


void IVfaCenterControlHandle::SetEnable(bool bIsEnabled)
{
	SetIsSelectionEnabled(bIsEnabled);
	m_bIsEnable = bIsEnabled;
	
	// Same as with SetVisible, to avoid illogical behavious, handles that are
	// made active will also be made visible.
	if(bIsEnabled)
		m_bIsVisible = true;
}

bool IVfaCenterControlHandle::GetEnable()
{
	bool bIsEnabled = GetIsSelectionEnabled();
	
	if(m_bIsEnable != bIsEnabled)
	{
		vstr::warnp() << "[IVfaCenterControlHandle]: "
				<<"unsynchronized state of activation" << endl;
	}

	return bIsEnabled;
}


/*============================================================================*/
/*  END OF FILE																  */
/*============================================================================*/
