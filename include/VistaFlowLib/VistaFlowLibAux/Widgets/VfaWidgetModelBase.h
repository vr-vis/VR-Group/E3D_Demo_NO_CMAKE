/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAWIDGETMODELBASE_H
#define _VFAWIDGETMODELBASE_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaAspects/VistaReflectionable.h>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <list>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaWidgetConstraint;
class VistaSemaphore;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Define a common base class for widget models which are per se Reflectionable
 * and may be subject to(external) constraints
 *
 */
class VISTAFLOWLIBAUXAPI VfaWidgetModelBase : public IVistaReflectionable
{
public:
	enum EObsMessage{
		MSG_CONSTRAINT_CHANGED = IVistaReflectionable::MSG_LAST,
		MSG_LAST
	};

	bool AddConstraint(IVfaWidgetConstraint *pConstr);
	void RemoveConstraint(IVfaWidgetConstraint *pConstr);
	bool HasConstraints() const;
	int GetNumConstraints() const;
	IVfaWidgetConstraint *GetConstraint(unsigned int i) const;

	/**
	 * Return the first instance of a constraint that matches
	 * the given type (assuming that there is at most one such
	 * constraint at any given time)
	 *
	 * NOTE: This is kinda dirty but it is a query which is frequently made.
	 */
	template<typename CConstrType>
		void GetConstraint(CConstrType*&) const;
	
protected:
	VfaWidgetModelBase();
	virtual ~VfaWidgetModelBase(); 

	/**
	 * Convenience function to evaluate all active constraints
	 */
	bool CheckConstraints() const;

private:
	std::list<IVfaWidgetConstraint*> m_liConstraints;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
template<typename CConstrType>
inline void VfaWidgetModelBase::GetConstraint(CConstrType*& pConstr) const
{
	pConstr=NULL;
	std::list<IVfaWidgetConstraint*>::const_iterator 
							itCurrent = m_liConstraints.begin();
	std::list<IVfaWidgetConstraint*>::const_iterator 
							itEnd = m_liConstraints.end();
	for(;itCurrent != itEnd; ++itCurrent)
	{
		pConstr = dynamic_cast<CConstrType*>(*itCurrent);
		if(pConstr != NULL)
			return;
	}
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAWIDGETMODELBASE_H
