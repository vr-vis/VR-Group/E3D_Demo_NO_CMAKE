

set( RelativeDir "./Widgets/StepSelector" )
set( RelativeSourceGroup "Source Files\\Widgets\\StepSelector" )

set( DirFiles
	VfaStepSelectorController.cpp
	VfaStepSelectorController.h
	VfaStepSelectorModel.cpp
	VfaStepSelectorModel.h
	VfaStepSelectorView.cpp
	VfaStepSelectorView.h
	VfaStepSelectorWidget.cpp
	VfaStepSelectorWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

