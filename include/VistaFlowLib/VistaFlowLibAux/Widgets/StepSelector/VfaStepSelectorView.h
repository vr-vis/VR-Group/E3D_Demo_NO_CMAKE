/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFASTEPSELECTORVIEW_H
#define VFASTEPSELECTORVIEW_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include "../../../VistaFlowLib/Visualization/VflRenderable.h"


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaStepSelectorModel;


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaStepSelectorView : public IVflRenderable
{
public:
	VfaStepSelectorView( VfaStepSelectorModel* pModel );
	virtual ~VfaStepSelectorView();

	// Observer interfave.
	virtual void ObserverUpdate(IVistaObserveable *pObs, int nMsg, int nTicket);

	// Renderable interface.
	virtual void DrawTransparent();
	virtual unsigned int GetRegistrationMode() const;

	class VISTAFLOWLIBAUXAPI VfaStepSelectorViewProps
	:	public VflRenderableProperties
	{
	public:
		enum eMessages
		{
			MSG_FADE_IN_TIME_CHANGED = VflRenderableProperties::MSG_LAST,
			MSG_FADE_OUT_TIME_CHANGED,
			MSG_STAY_OPEN_TIME_CHANGED,
			MSG_TILE_SIZE_CHANGED,
			MSG_BACKGROUND_COLOR_CHANGED,
			MSG_SELECTION_COLOR_CHANGED,
			MSG_BORDER_COLOR_CHANGED,
			MSG_FULL_ALPHA_CHANGED,
			MSG_LAST
		};

		VfaStepSelectorViewProps();
		virtual ~VfaStepSelectorViewProps();

		bool SetFadeInTime( double dTimeInSecs );
		double GetFadeInTime() const;

		bool SetFadeOutTime( double dTimeInSecs );
		double GetFadeOutTime() const;

		bool SetStayOpenTime( double dTimeInSecs );
		double GetStayOpenTime() const;

		bool SetTileSize( float fSizeInMeters );
		float GetTileSize() const;

		bool SetBackgroundColor( float aColorRGB[3] );
		void GetBackgroundColor( float aColorRGB[3] ) const;

		bool SetSelectionColor( float aColorRGB[3] );
		void GetSelectionColor( float aColorRGB[3] ) const;

		bool SetBorderColor( float aColorRGB[3] );
		void GetBorderColor( float aColorRGB[3] ) const;

		bool SetFullAlpha( float fFullAlpha );
		float GetFullAlpha() const;

	private:
		/**
		 * Checks a color to lie within range [0,1]^3 and applies it to the
		 * given target throwing the specified message.
		 */
		bool CheckAndSetColor( float aColorRGB[3], float aTarget[3], int nMsg );

		double		m_dFadeInTimeInSecs;
		double		m_dFadeOutTimeInSecs;
		double		m_dStayOpenTimeInSecs;
		float		m_fTileSizeInMeters;
		float		m_aBackColorRGB[3];
		float		m_aSelColorRGB[3];
		float		m_aBorderColorRGB[3];
		float		m_fFullAlpha;
	};

	VfaStepSelectorViewProps* GetTypedProps() const;

protected:
	IVflRenderable::VflRenderableProperties* CreateProperties() const;

private:
	enum EMenuState
	{
		E_CLOSED = 0,
		E_OPEN,
		E_FADING_IN,
		E_FADING_OUT
	};
	
	//! Updates the menu state and returns the alpha value used for drawing it.
	float UpdateMenuState();
	/**
	 * Puts the menu into the specified state in case the given menu remained
	 * in the current state longer than the given threshold time.
	 * Returns the normalized time spent in the current (not the new!) state
	 * so far.
	 */
	float ProcessStateTime( double dTargetStateTimeInSecs,
		EMenuState eNextState );

	VfaStepSelectorModel*	m_pModel;

	int						m_nActiveTick;
	int						m_nTickRangeLow;
	int						m_nTickRangeHigh;
	int						m_nNumberOfTicks;

	// Stuff for fading in and out the menu.
	EMenuState				m_eCurrentMenuState;
	double					m_dInitTimeStamp;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

