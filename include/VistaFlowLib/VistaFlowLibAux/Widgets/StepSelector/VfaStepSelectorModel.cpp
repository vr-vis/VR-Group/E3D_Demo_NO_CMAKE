/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaStepSelectorModel.h"


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
VfaStepSelectorModel::VfaStepSelectorModel()
:	m_nTickRangeLow(0)
,	m_nTickRangeHigh(4)
,	m_nActiveTick(0)
{
}

VfaStepSelectorModel::~VfaStepSelectorModel()
{
}


bool VfaStepSelectorModel::SetTickRange( const int nLow, const int nHigh )
{
	if( nLow > nHigh )
		return false;

	m_nTickRangeLow		= nLow;
	m_nTickRangeHigh	= nHigh;

	Notify( MSG_TICK_RANGE_CHANGED );

	// We need to check if the active tick became invalid and reset it
	// accordingly.
	if( m_nActiveTick < m_nTickRangeLow || m_nActiveTick > m_nTickRangeHigh )
	{
		// In case the active tick is out of range we reset it to the new lower
		// tick range bound.
		SetActiveTick( m_nTickRangeLow );
	}

	return true;
}

void VfaStepSelectorModel::GetTickRange( int& nLow, int& nHigh )
{
	nLow	= m_nTickRangeLow;
	nHigh	= m_nTickRangeHigh;
}


bool VfaStepSelectorModel::SetActiveTick( const int nActiveTick )
{
	Notify( MSG_TICK_CHANGE_REQUEST );

	// Range check.
	if( nActiveTick < m_nTickRangeLow || nActiveTick > m_nTickRangeHigh )
	{
		return false;
	}

	m_nActiveTick = nActiveTick;

	Notify( MSG_ACTIVE_TICK_CHANGED );

	return true;
}

int VfaStepSelectorModel::GetActiveTick() const
{
	return m_nActiveTick;
}


bool VfaStepSelectorModel::TickOneUp()
{
	return SetActiveTick( m_nActiveTick + 1 );
}

bool VfaStepSelectorModel::TickOneDown()
{
	return SetActiveTick( m_nActiveTick - 1 );
}


void VfaStepSelectorModel::IndicateSelection()
{
	Notify( MSG_TICK_SELECTED );
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

