/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaStepSelectorWidget.h"
#include "VfaStepSelectorModel.h"
#include "VfaStepSelectorView.h"
#include "VfaStepSelectorController.h"

#include "../../../VistaFlowLib/Visualization/VflRenderNode.h"

#include <cassert>

using namespace std;


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
VfaStepSelectorWidget::VfaStepSelectorWidget( VflRenderNode *pRenderNode )
:	IVfaWidget( pRenderNode )
,	m_bIsEnabled( true )
,	m_bIsVisible(true)
,	m_pModel( NULL )
,	m_pView( NULL )
,	m_pController( NULL )
{
	assert( NULL != GetRenderNode() && "Absolutely necessary!" );
	
	m_pModel = new VfaStepSelectorModel;

	m_pView	= new VfaStepSelectorView( m_pModel );
	GetRenderNode()->AddRenderable( m_pView );
	m_pView->Init();	

	m_pController = new VfaStepSelectorController( GetRenderNode(),
		m_pModel );
}

VfaStepSelectorWidget::~VfaStepSelectorWidget()
{
	delete m_pController;
	
	GetRenderNode()->RemoveRenderable( m_pView );
	delete m_pView;
		
	delete m_pModel;
}


void VfaStepSelectorWidget::SetIsEnabled( bool bEnabledState )
{
	m_bIsEnabled = bEnabledState;
	
	m_pController->SetIsEnabled( bEnabledState );
}

bool VfaStepSelectorWidget::GetIsEnabled() const
{
	return m_bIsEnabled;
}


void VfaStepSelectorWidget::SetIsVisible( bool bVisibleState )
{
	m_bIsVisible = bVisibleState;
	
	m_pView->SetVisible( bVisibleState );
}

bool VfaStepSelectorWidget::GetIsVisible() const
{
	return m_bIsVisible;
}


IVistaReflectionable* VfaStepSelectorWidget::GetModel() const
{
	return m_pModel;
}

IVflRenderable* VfaStepSelectorWidget::GetView() const
{
	return m_pView;
}

IVfaWidgetController* VfaStepSelectorWidget::GetController() const
{
	return m_pController;
}


VfaStepSelectorModel* VfaStepSelectorWidget::GetTypedModel() const
{
	return dynamic_cast<VfaStepSelectorModel*>( m_pModel );
}

VfaStepSelectorView* VfaStepSelectorWidget::GetTypedView() const
{
	return dynamic_cast<VfaStepSelectorView*>( m_pView );
}

VfaStepSelectorController* VfaStepSelectorWidget::GetTypedController() const
{
	return dynamic_cast<VfaStepSelectorController*>( m_pController );
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

