/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFASTEPSELECTORMODEL_H
#define VFASTEPSELECTORMODEL_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaTransformableWidgetModel.h"


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaStepSelectorModel
:	public VfaTransformableWidgetModel
{
public:
	enum eMessages
	{
		MSG_TICK_RANGE_CHANGED = VfaWidgetModelBase::MSG_LAST,
		MSG_TICK_CHANGE_REQUEST, //< This is needed by the view!
		MSG_ACTIVE_TICK_CHANGED,
		MSG_TICK_SELECTED,
		MSG_LAST
	};

	VfaStepSelectorModel();
	virtual ~VfaStepSelectorModel();

	bool SetTickRange( const int nLow, const int nHigh );
	void GetTickRange( int& nLow, int& nHigh );

	bool SetActiveTick( const int nActiveTick );
	int GetActiveTick() const;

	bool TickOneUp();
	bool TickOneDown();

	void IndicateSelection();

private:
	int		m_nTickRangeLow;
	int		m_nTickRangeHigh;
	int		m_nActiveTick;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

