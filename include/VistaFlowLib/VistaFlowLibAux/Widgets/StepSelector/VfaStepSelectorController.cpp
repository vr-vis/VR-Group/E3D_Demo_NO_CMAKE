/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaStepSelectorController.h"
#include "VfaStepSelectorModel.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <cassert>

using namespace std;


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
VfaStepSelectorController::VfaStepSelectorController(
	VflRenderNode* pRenderNode, VfaStepSelectorModel* pModel )
:	IVfaWidgetController( pRenderNode )
,	m_bIsEnabled( true )
,	m_pModel( pModel )
,	m_nTickUpCmdSlotId( -1 )
,	m_nTickDownCmdSlotId( -1 )
,	m_nTickSelectCmdSlotId( -1 )
{
	assert( NULL != m_pModel );
}

VfaStepSelectorController::~VfaStepSelectorController()
{
}


bool VfaStepSelectorController::SetTickUpCommandSlotId( int nSlotId )
{
	// Note: No check if same as other used slot id. This might lead to some
	//		 strange configurations that are not rejected,
	//		 e.g. tick_up_id == tick_down_id. Nevertheless, this is allowed
	//		 to allow for a maximum of flexibility.
	m_nTickUpCmdSlotId = nSlotId;

	return true;
}

int VfaStepSelectorController::GetTickUpCommandSlotId() const
{
	return m_nTickUpCmdSlotId;
}


bool VfaStepSelectorController::SetTickDownCommandSlotId( int nSlotId )
{
	// See comment in SetTickUpCommandSlotId().
	m_nTickDownCmdSlotId = nSlotId;

	return true;
}

int VfaStepSelectorController::GetTickDownCommandSlotId() const
{
	return m_nTickDownCmdSlotId;
}


bool VfaStepSelectorController::SetTickSelectCommandSlotId( int nSlotId )
{
	// See comment in SetTickUpCommandSlotId().
	m_nTickSelectCmdSlotId = nSlotId;

	return true;
}

int VfaStepSelectorController::GetTickSelectCommandSlotId() const
{
	return m_nTickSelectCmdSlotId;
}


void VfaStepSelectorController::SetIsEnabled( bool bEnabledState )
{
	m_bIsEnabled = bEnabledState;
}

bool VfaStepSelectorController::GetIsEnabled() const
{
	return m_bIsEnabled;
}


void VfaStepSelectorController::OnSensorSlotUpdate( int iSlot,
	const VfaApplicationContextObject::sSensorFrame& oFrameData )
{
	if( VfaApplicationContextObject::SLOT_CURSOR_VIS != iSlot )
	{
		return;
	}

	m_pModel->SetPosition( oFrameData.v3Position );

	const VistaVector3D v3LocalViewPos =
		GetRenderNode()->GetLocalViewPosition();

	// Normal.
	VistaVector3D v3Normal =
		(v3LocalViewPos - oFrameData.v3Position).GetNormalized();
	// Up.
	VistaVector3D v3Up = GetRenderNode()->GetInverseTransform().TransformVector(
		VistaVector3D( 0.0f, 1.0f, 0.0f, 0.0f ) );
	v3Up.Normalize();
	// Right.
	VistaVector3D v3Right = v3Up.Cross( v3Normal );
	v3Right.Normalize();
	// Recalc up.
	v3Up = v3Normal.Cross( v3Right );
	v3Up.Normalize();
	// Create orientation.
	float aOri[16] = 
		{ 
			v3Right[0], v3Up[0], v3Normal[0], 0.0f,
			v3Right[1], v3Up[1], v3Normal[1], 0.0f,
			v3Right[2], v3Up[2], v3Normal[2], 0.0f,
			      0.0f,    0.0f,         0.0, 1.0f		
		};
	VistaTransformMatrix matOri( aOri );
	VistaQuaternion qOri( matOri );
		
	m_pModel->SetOrientation( qOri );
}

void VfaStepSelectorController::OnCommandSlotUpdate( int iSlot,
	const bool bSet )
{
	if( false == m_bIsEnabled || false == bSet )
		return;

	if( iSlot == m_nTickDownCmdSlotId )
	{
		m_pModel->TickOneDown();
	}
	else if( iSlot == m_nTickUpCmdSlotId )
	{
		m_pModel->TickOneUp();
	}
	else if( iSlot == m_nTickSelectCmdSlotId )
	{
		m_pModel->IndicateSelection();
	}
}

void VfaStepSelectorController::OnTimeSlotUpdate( const double dTime )
{
	// Intentionally left empty.
}


int VfaStepSelectorController::GetSensorMask() const
{
	return (1 << VfaApplicationContextObject::SLOT_CURSOR_VIS);
}

int VfaStepSelectorController::GetCommandMask() const
{
	int nCmdMask = 0;

	// Only assemble the _active_ command slots.
	if( -1 < m_nTickUpCmdSlotId )
	{
		nCmdMask |= 1 << m_nTickUpCmdSlotId;
	}

	if( -1 < m_nTickDownCmdSlotId )
	{
		nCmdMask |= 1 << m_nTickDownCmdSlotId;
	}

	if( -1 < m_nTickSelectCmdSlotId )
	{
		nCmdMask |= 1 << m_nTickSelectCmdSlotId;
	}

	return nCmdMask;
}

bool VfaStepSelectorController::GetTimeUpdate() const
{
	return false;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

