/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFASTEPSELECTORWIDGET_H
#define VFASTEPSELECTORWIDGET_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaWidget.h"


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaStepSelectorModel;
class VfaStepSelectorView;
class VfaStepSelectorController;


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaStepSelectorWidget : public IVfaWidget
{
public:
	VfaStepSelectorWidget( VflRenderNode *pRenderNode );
	virtual ~VfaStepSelectorWidget();

	virtual void SetIsEnabled( bool bEnabledState );
	virtual bool GetIsEnabled() const;

	virtual void SetIsVisible( bool bVisibleState );
	virtual bool GetIsVisible() const;

	virtual IVistaReflectionable* GetModel() const;
	virtual IVflRenderable* GetView() const;
	virtual IVfaWidgetController* GetController() const;

	VfaStepSelectorModel* GetTypedModel() const;
	VfaStepSelectorView* GetTypedView() const;
	VfaStepSelectorController* GetTypedController() const;

private:
	bool						m_bIsEnabled;
	bool						m_bIsVisible;

	VfaStepSelectorModel*		m_pModel;
	VfaStepSelectorView*		m_pView;
	VfaStepSelectorController*	m_pController;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

