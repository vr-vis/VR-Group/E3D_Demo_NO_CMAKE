/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "VfaStepSelectorView.h"
#include "VfaStepSelectorModel.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

#include <cassert>
#include <iostream>

using namespace std;


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
VfaStepSelectorView::VfaStepSelectorView( VfaStepSelectorModel* pModel )
:	m_pModel( pModel )
,	m_nActiveTick( -1 )
,	m_nTickRangeLow( -1 )
,	m_nTickRangeHigh( -1 )
,	m_nNumberOfTicks( 0 )
,	m_eCurrentMenuState( E_CLOSED )
,	m_dInitTimeStamp( 0.0 )
{
	assert( NULL != m_pModel );

	m_nActiveTick = m_pModel->GetActiveTick();
	m_pModel->GetTickRange( m_nTickRangeLow, m_nTickRangeHigh );
	m_nNumberOfTicks = m_nTickRangeHigh - m_nTickRangeLow + 1;

	m_pModel->AttachObserver( this );
}

VfaStepSelectorView::~VfaStepSelectorView()
{
	m_pModel->DetachObserver( this );
}


void VfaStepSelectorView::ObserverUpdate( IVistaObserveable *pObs, int nMsg,
	int nTicket )
{
	// Only process model messages here, nothing from the props object!
	if( pObs != m_pModel )
		return;

	switch( nMsg )
	{
	case VfaStepSelectorModel::MSG_TICK_CHANGE_REQUEST:
	case VfaStepSelectorModel::MSG_ACTIVE_TICK_CHANGED:
		m_nActiveTick = m_pModel->GetActiveTick();
		// Trigger menu to open (only if not alread opening or already opened).
		if( E_OPEN != m_eCurrentMenuState )
		{
			m_eCurrentMenuState = E_FADING_IN;
		}
		// @todo This might need adaption if a fade-in is triggered while the
		//		 menu is currently fading-out. A simple re-param should do though.
		m_dInitTimeStamp = GetRenderNode()->GetVisTiming()->GetCurrentClock();
		break;
	case VfaStepSelectorModel::MSG_TICK_RANGE_CHANGED:
		m_pModel->GetTickRange( m_nTickRangeLow, m_nTickRangeHigh );
		m_nNumberOfTicks = m_nTickRangeHigh - m_nTickRangeLow + 1;
		break;
	default:
		// Something else, do not care.
		break;
	}
}


void VfaStepSelectorView::DrawTransparent()
{
	// First of all, we'll update the menu state to get the right fade-in value.
	const float fColorAlpha = UpdateMenuState();

	if( false == GetVisible() || E_CLOSED == m_eCurrentMenuState )
	{
		return;
	}

	glPushAttrib( GL_ENABLE_BIT		// Blending, lighting.
		| GL_CURRENT_BIT 			// Current color.
		| GL_COLOR_BUFFER_BIT		// (Blending), blend function.
		| GL_LINE_BIT );			// Line width.

	glEnable( GL_BLEND );
	glDisable( GL_LIGHTING );

	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	// Set the right widget position and orientation.
	float fPos[3];
	m_pModel->GetPosition( fPos );
	float fOri[4];
	m_pModel->GetOrientation( fOri );

	glTranslatef( fPos[0], fPos[1], fPos[2]);
	
	float fDenom = sqrt(1.0f - fOri[3] * fOri[3]);
	if(fDenom > numeric_limits<float>::epsilon())
	{
		glRotatef(
			Vista::RadToDeg(2.0f * acos(fOri[3])), 
			fOri[0] / fDenom,
			fOri[1] / fDenom,
			fOri[2] / fDenom
			);
	}

	const float fTileSize = GetTypedProps()->GetTileSize();
	const float fWidth	= m_nNumberOfTicks * fTileSize;
	const float	fHeight = fTileSize;

	const float fLeft	= -0.5f * fWidth;
	const float fRight	=  0.5f * fWidth;
	const float fBottom = -0.5f * fHeight;
	const float fTop	=  0.5f * fHeight;

	//--------------------------------------------------------------------------
	// Draw background quad.

	// Set color, don't forget the right fade-in value (alpha).
	// This variable's first 3 components will be updated with the colors of the
	// element that will be rendered next. The alpha value is global for all of
	// them!!
	float aCurrentColor[4] = { 0.0f, 0.0f, 0.0f,
		GetTypedProps()->GetFullAlpha() * fColorAlpha };

	// Load and set color for widget background.
	GetTypedProps()->GetBackgroundColor( aCurrentColor );
	glColor4fv( aCurrentColor );
	
	glBegin( GL_QUADS );
		glVertex3f( fLeft , fBottom, 0.0f );
		glVertex3f( fRight, fBottom, 0.0f );
		glVertex3f( fRight, fTop   , 0.0f );
		glVertex3f( fLeft , fTop   , 0.0f );
	glEnd();

	//--------------------------------------------------------------------------
	// Draw marker for the currently active tick.
	
	const float fLinePos = fLeft
		+ static_cast<float>( m_nActiveTick - m_nTickRangeLow ) * fTileSize;

	// Color for selected (active) tick.
	GetTypedProps()->GetSelectionColor( aCurrentColor );
	glColor4fv( aCurrentColor );
	
	glBegin( GL_QUADS );
		glVertex3f( fLinePos,			  fBottom, 0.001f );
		glVertex3f( fLinePos + fTileSize, fBottom, 0.001f );
		glVertex3f( fLinePos + fTileSize, fTop,	   0.001f );
		glVertex3f( fLinePos,			  fTop,	   0.001f );
	glEnd();

	//--------------------------------------------------------------------------
	// Draw one segment for each tick.
	
	// Color for tick segments border.
	GetTypedProps()->GetBorderColor( aCurrentColor );
	glColor4fv( aCurrentColor );
	
	// @todo Magic value, but does not need changing?
	glLineWidth( 2.0f );

	glBegin( GL_LINES );
		// Bottom horizontal line from menu's left to right.
		glVertex3f( fLeft , fBottom, 0.002f );
		glVertex3f( fRight, fBottom, 0.002f );
		// Same for the top.
		glVertex3f( fLeft , fTop   , 0.002f );
		glVertex3f( fRight, fTop   , 0.002f );
		// Draw vertical line segments.
		for( int i=0; i<=m_nNumberOfTicks; ++i )
		{
			glVertex3f( fLeft + static_cast<float>( i ) * fTileSize,
				fBottom, 0.002f );
			glVertex3f( fLeft + static_cast<float>( i ) * fTileSize,
				fTop, 0.002f );
		}
	glEnd();


	// Finally, restore OpenGL state.
	glPopAttrib();
}


unsigned int VfaStepSelectorView::GetRegistrationMode() const
{
	return OLI_DRAW_TRANSPARENT;
}


VfaStepSelectorView::VfaStepSelectorViewProps*
VfaStepSelectorView::GetTypedProps() const
{
	return dynamic_cast<VfaStepSelectorViewProps*>( GetProperties() );
}


IVflRenderable::VflRenderableProperties*
VfaStepSelectorView::CreateProperties() const
{
	return new VfaStepSelectorViewProps;
}


float VfaStepSelectorView::UpdateMenuState()
{
	float fColorAlpha = 0.0f;

	switch( m_eCurrentMenuState )
	{
	case E_FADING_IN:
		{
			const double dFadeInTime = GetTypedProps()->GetFadeInTime();
			const float fNormalizedStateTime = ProcessStateTime( dFadeInTime,
				E_OPEN );
			
			fColorAlpha = min( 1.0f, fNormalizedStateTime );
		}
		break;
	case E_FADING_OUT:
		{
			const double dFadeOutTime = GetTypedProps()->GetFadeOutTime();
			const float fNormalizedStateTime = ProcessStateTime( dFadeOutTime,
				E_CLOSED );

			fColorAlpha = fNormalizedStateTime >= 1.0f ?
				0.0f : 1.0f - fNormalizedStateTime;
		}
		break;
	case E_OPEN:
		{
			const double dStayOpenTime = GetTypedProps()->GetStayOpenTime();
			const float fNormalizedStateTime = ProcessStateTime( dStayOpenTime,
				E_FADING_OUT );

			fColorAlpha = max( 1.0f, fNormalizedStateTime );
		}
		break;
	case E_CLOSED:
		break;
	default:
		break;
	}

	return fColorAlpha;
}

float VfaStepSelectorView::ProcessStateTime( double dTargetStateTimeInSecs,
	EMenuState eNextState )
{
	const VflVisTiming* pVisTiming = GetRenderNode()->GetVisTiming();
	const double dCurrentTime = pVisTiming->GetCurrentClock();
	const double dTimeDelta = dCurrentTime - m_dInitTimeStamp;

	if( dTimeDelta > dTargetStateTimeInSecs )
	{
		m_dInitTimeStamp	= dCurrentTime;
		m_eCurrentMenuState	= eNextState;
	}

	return static_cast<float>( dTimeDelta / dTargetStateTimeInSecs );
}


/*============================================================================*/
/* PROPERTIY CLASS IMPLEMENTATION                                             */
/*============================================================================*/
VfaStepSelectorView::VfaStepSelectorViewProps::VfaStepSelectorViewProps()
:	m_dFadeInTimeInSecs( 0.0 )
,	m_dFadeOutTimeInSecs( 0.2 )
,	m_dStayOpenTimeInSecs( 1.0 )
,	m_fTileSizeInMeters( 0.1f )
,	m_fFullAlpha( 1.0f )
{
	m_aBackColorRGB[0] = 0.8f;
	m_aBackColorRGB[1] = 0.8f;
	m_aBackColorRGB[2] = 1.0f;

	m_aSelColorRGB[0] = 0.2f;
	m_aSelColorRGB[1] = 0.2f;
	m_aSelColorRGB[2] = 1.0f;
}

VfaStepSelectorView::VfaStepSelectorViewProps::~VfaStepSelectorViewProps()
{
}


bool VfaStepSelectorView::VfaStepSelectorViewProps::SetFadeInTime(
	double dTimeInSecs )
{
	if( 0.0 > dTimeInSecs )
		return false;

	m_dFadeInTimeInSecs = dTimeInSecs;

	Notify( MSG_FADE_IN_TIME_CHANGED );

	return true;
}

double VfaStepSelectorView::VfaStepSelectorViewProps::GetFadeInTime() const
{
	return m_dFadeInTimeInSecs;
}


bool VfaStepSelectorView::VfaStepSelectorViewProps::SetFadeOutTime(
	double dTimeInSecs )
{
	if( 0.0 > dTimeInSecs )
		return false;

	m_dFadeOutTimeInSecs = dTimeInSecs;

	Notify( MSG_FADE_OUT_TIME_CHANGED );

	return true;
}

double VfaStepSelectorView::VfaStepSelectorViewProps::GetFadeOutTime() const
{
	return m_dFadeOutTimeInSecs;
}


bool VfaStepSelectorView::VfaStepSelectorViewProps::SetStayOpenTime(
	double dTimeInSecs )
{
	if( 0.0 > dTimeInSecs )
		return false;

	m_dStayOpenTimeInSecs = dTimeInSecs;

	Notify( MSG_STAY_OPEN_TIME_CHANGED );

	return true;
}

double VfaStepSelectorView::VfaStepSelectorViewProps::GetStayOpenTime() const
{
	return m_dStayOpenTimeInSecs;
}


bool VfaStepSelectorView::VfaStepSelectorViewProps::SetTileSize( 
	float fSizeInMeters )
{
	if( 0.0f > fSizeInMeters )
		return false;

	m_fTileSizeInMeters = fSizeInMeters;

	Notify( MSG_TILE_SIZE_CHANGED );

	return true;
}

float VfaStepSelectorView::VfaStepSelectorViewProps::GetTileSize() const
{
	return m_fTileSizeInMeters;
}


bool VfaStepSelectorView::VfaStepSelectorViewProps::SetBackgroundColor(
	float aColorRGB[3] )
{
	return CheckAndSetColor( aColorRGB, m_aBackColorRGB,
		MSG_BACKGROUND_COLOR_CHANGED );
}

void VfaStepSelectorView::VfaStepSelectorViewProps::GetBackgroundColor(
	float aColorRGB[3] ) const
{
	for( int i=0; i<3; ++i )
	{
		aColorRGB[i] = m_aBackColorRGB[i];
	}
}


bool VfaStepSelectorView::VfaStepSelectorViewProps::SetSelectionColor(
	float aColorRGB[3] )
{
	return CheckAndSetColor( aColorRGB, m_aSelColorRGB,
		MSG_SELECTION_COLOR_CHANGED );
}

void VfaStepSelectorView::VfaStepSelectorViewProps::GetSelectionColor(
	float aColorRGB[3] ) const
{
	for( int i=0; i<3; ++i )
	{
		aColorRGB[i] = m_aSelColorRGB[i];
	}
}

bool VfaStepSelectorView::VfaStepSelectorViewProps::SetBorderColor(
	float aColorRGB[3] )
{
	return CheckAndSetColor( aColorRGB, m_aBorderColorRGB,
		MSG_BORDER_COLOR_CHANGED );
}

void VfaStepSelectorView::VfaStepSelectorViewProps::GetBorderColor(
	float aColorRGB[3] ) const
{
	for( int i=0; i<3; ++i )
	{
		aColorRGB[i] = m_aBorderColorRGB[i];
	}
}


bool VfaStepSelectorView::VfaStepSelectorViewProps::SetFullAlpha( float fFullAlpha )
{
	if( 0.0f > fFullAlpha || 1.0f < fFullAlpha )
	{
		return false;
	}

	m_fFullAlpha = fFullAlpha;

	Notify( MSG_FULL_ALPHA_CHANGED );

	return true;
}

float VfaStepSelectorView::VfaStepSelectorViewProps::GetFullAlpha() const
{
	return m_fFullAlpha;
}


bool VfaStepSelectorView::VfaStepSelectorViewProps::CheckAndSetColor(
	float aColorRGB[3], float aTarget[3], int nMsg )
{
	for( int i=0; i<3; ++i )
	{
		if( 0.0f > aColorRGB[i] || 1.0f < aColorRGB[i] )
		{
			return false;
		}
	}

	for( int i=0; i<3; ++i )
	{
		aTarget[i] = aColorRGB[i];
	}

	Notify( nMsg );	

	return true;
}




/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

