

set( RelativeDir "./Widgets" )
set( RelativeSourceGroup "Source Files\\Widgets" )
set( SubDirs
	 AxisRotate
	 Box
	 Button
	 CircleMenu
	 Cylinder
	 Crosshair
	 Goniometer
	 Slider
	 Line
	 LUTDisplay
	 MeasuringTape
	 Menu
	 Plane
	 Sphere
	 StateMachine
	 StepSelector
	 TextBox
	 TimeBuoy
	 TrajectoryDragging
	 TrajectoryRangeSelection )


set( DirFiles
	VfaArrowVis.cpp
	VfaArrowVis.h
	VfaBoxHandle.cpp
	VfaBoxHandle.h
	VfaBoxVis.cpp
	VfaBoxVis.h
	VfaCircle2DVis.cpp
	VfaCircle2DVis.h
	VfaConditionalForwarding.cpp
	VfaConditionalForwarding.h
	VfaConeVis.cpp
	VfaConeVis.h
	VfaControlHandle.cpp
	VfaControlHandle.h
	VfaCursorSelectFocusStrategy.cpp
	VfaCursorSelectFocusStrategy.h
	VfaCylinderVis.cpp
	VfaCylinderVis.h
	VfaCylinderHandle.cpp
	VfaCylinderHandle.h
	VfaFocusStrategy.h
	VfaForwardObserver.cpp
	VfaForwardObserver.h
	VfaFrustumVis.cpp
	VfaFrustumVis.h
	VfaIntentionSelectFocusStrategy.cpp
	VfaIntentionSelectFocusStrategy.h
	VfaLineVis.cpp
	VfaLineVis.h
	VfaProximityBoxHandle.cpp
	VfaProximityBoxHandle.h
	VfaProximityFocusStrategy.cpp
	VfaProximityFocusStrategy.h
	VfaProximityHandle.cpp
	VfaProximityHandle.h
	VfaProximityQuadHandle.cpp
	VfaProximityQuadHandle.h
	VfaProximitySphereHandle.cpp
	VfaProximitySphereHandle.h
	VfaQuadHandle.cpp
	VfaQuadHandle.h
	VfaQuadVis.cpp
	VfaQuadVis.h
	VfaRaycastFocusStrategy.cpp
	VfaRaycastFocusStrategy.h
	VfaRaycastHandle.cpp
	VfaRaycastHandle.h
	VfaRaycastPlaneHandle.cpp
	VfaRaycastPlaneHandle.h
	VfaRulerVis.cpp
	VfaRulerVis.h
	VfaSlotObserver.cpp
	VfaSlotObserver.h
	VfaSphereHandle.cpp
	VfaSphereHandle.h
	VfaSphereVis.cpp
	VfaSphereVis.h
	VfaTexturedBoxHandle.cpp
	VfaTexturedBoxHandle.h
	VfaTexturedBoxVis.cpp
	VfaTexturedBoxVis.h
	VfaTexturedQuadHandle.cpp
	VfaTexturedQuadHandle.h
	VfaTransformableWidgetModel.cpp
	VfaTransformableWidgetModel.h
	VfaWidget.cpp
	VfaWidget.h
	VfaWidgetConstraints.cpp
	VfaWidgetConstraints.h
	VfaWidgetController.cpp
	VfaWidgetController.h
	VfaWidgetManager.cpp
	VfaWidgetManager.h
	VfaWidgetModelBase.cpp
	VfaWidgetModelBase.h
	VfaWidgetTools.cpp
	VfaWidgetTools.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()

