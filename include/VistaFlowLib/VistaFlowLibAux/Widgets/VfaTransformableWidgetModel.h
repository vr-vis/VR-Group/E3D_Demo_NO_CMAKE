/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFATRANSFORMABLEWIDGETMODEL_H
#define _VFATRANSFORMABLEWIDGETMODEL_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaWidgetModelBase.h"
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaBase/VistaVectorMath.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVistaTransformable;
class VflRenderNode;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Define a base class for widget models which are transformable, i.e.
 * they have a position and orientation which can be changed.
 * 
 * 
 *
 */

class VISTAFLOWLIBAUXAPI VfaTransformableWidgetModel : public VfaWidgetModelBase
{
public:
	/**
	* messages sent by this reflectionable upon property changes
	* 	MSG_POSITION_CHANGE  the widget's position has changed,
	* 	MSG_ORIENTATION_CHANGE  the widget's orientation has changed,
	*/
	enum{
		MSG_POSITION_CHANGE = IVistaReflectionable::MSG_LAST,
		MSG_ORIENTATION_CHANGE,
		MSG_SCALE_CHANGE,
		MSG_TRANSFORM_CHANGE,
		MSG_LAST
	};

	VfaTransformableWidgetModel();
	virtual ~VfaTransformableWidgetModel(); 

	void SetRenderNode(VflRenderNode* pRN);
	void SetWidgetParent(IVistaTransformable*);
	void SyncTransforms();
	/*
	* this is in widget space
	*/
	bool SetPosition(float fPosition[3]);
	bool SetPosition(double dPosition[3]);
	bool SetPosition(const VistaVector3D& v3C);

	void GetPosition(float fPosition[3]) const;
	void GetPosition(double dPosition[3]) const;
	void GetPosition(VistaVector3D &v3C) const;
	VistaVector3D GetPosition() const;

	bool SetOrientation(float fOrientation[4]);
	bool SetOrientation(const VistaQuaternion & q);
	void GetOrientation(float fOrientation[4]) const;
	void GetOrientation(VistaQuaternion & q) const;
	VistaQuaternion GetOrientation() const;

	void SetScale(float fScale);
	float GetScale() const;

	void GetTransform(VistaTransformMatrix & matrix);

	/*
	* this is in vis space
	*/
	bool SetPositionVisSpace(float fPosition[3]);
	bool SetPositionVisSpace(double dPosition[3]);
	bool SetPositionVisSpace(const VistaVector3D& v3C);

	void GetPositionVisSpace(float fPosition[3]) const;
	void GetPositionVisSpace(double dPosition[3]) const;
	void GetPositionVisSpace(VistaVector3D &v3C) const;

	bool SetOrientationVisSpace(float fOrientation[4]);
	bool SetOrientationVisSpace(const VistaQuaternion & q);
	void GetOrientationVisSpace(float fOrientation[4]) const;
	void GetOrientationVisSpace(VistaQuaternion & q) const;

	void SetDoIgnoreWidgetTransform(bool bIgnore);
	bool GetDoIgnoreWidgetTransform() const;

	virtual std::string GetReflectionableType() const;

protected:

	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

private:

	VflRenderNode*	m_pRenderNode;
	IVistaTransformable* m_pWidgetParent;

	VistaReferenceFrame m_oVisRefFrame;
	VistaReferenceFrame m_oWidgetRefFrame;

	bool				  m_bIgnoreWidgetTransform;

	// position and orientation are stored in widget space
	float m_fPosition[3];
	float m_fOrientation[4];
	float m_fScale;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFATRANSFORMABLEWIDGETMODEL_H
