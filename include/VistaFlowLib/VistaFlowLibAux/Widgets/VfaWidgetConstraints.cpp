/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaWidgetConstraints.h"
#include "VfaWidgetTools.h"

#include <typeinfo>
#include <iostream>
#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
IVfaWidgetConstraint::IVfaWidgetConstraint()
	:	m_bIsActive(true)
{
}

IVfaWidgetConstraint::~IVfaWidgetConstraint()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Enforce                                                     */
/*                                                                            */
/*============================================================================*/
void IVfaWidgetConstraint::SetIsActive(bool b)
{
	m_bIsActive = b;
}

bool IVfaWidgetConstraint::GetIsActive() const
{
	return m_bIsActive;
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   EnforceBoundsForPoint                                       */
/*                                                                            */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaWidgetConstraints.cpp"                                    */
/*============================================================================*/


