/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES			                                                      */
/*============================================================================*/
#include "VfaWidget.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>

using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
IVfaWidget::IVfaWidget(VflRenderNode *pRenderNode)
:	m_pRenderNode(pRenderNode)
{ }

IVfaWidget::~IVfaWidget()
{ }


/*============================================================================*/
/*  IMPLEMENTATION			                                                  */
/*============================================================================*/
bool IVfaWidget::ExchangeRenderNode(VflRenderNode *pRenderNode)
{
	if(!pRenderNode)
		return false;

	// Swap the RenderNode of the widget's view.
	if(GetView())
	{
		if( m_pRenderNode )
			m_pRenderNode->RemoveRenderable(GetView());
		m_pRenderNode = pRenderNode;
		m_pRenderNode->AddRenderable(GetView());
	}
	
	// Swap the RenderNode of the WidgetContoller.
	if(GetController())
	{
		GetController()->ExchangeRenderNode(pRenderNode);
	}
	
	return true;
}

VflRenderNode* IVfaWidget::GetRenderNode() const
{
	return m_pRenderNode;
}


/*============================================================================*/
/*  END OF FILE "VfaWidget.cpp"                                               */
/*============================================================================*/
