/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAFRUSTUMVIS_H
#define _VFAFRUSTUMVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaMath/VistaGeometries.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * The frustum consists of 6 planes. You can change position/orientation independent from ist shape.
 * 
 *
 *
 */
class VISTAFLOWLIBAUXAPI VfaFrustumVis : public IVflRenderable
{
public: 
	VfaFrustumVis();
	virtual ~VfaFrustumVis();

	virtual void DrawOpaque();

	virtual unsigned int GetRegistrationMode() const;

	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);
	
    /*
    * Update view position, direction and up vector(i.e. move frustum)
    *
    */
	void UpdateViewFrustum(const VistaVector3D &vecViewPosition, const VistaVector3D &vecViewDirection, const VistaVector3D &vecUp, bool bForceUseUpVector = false);

    /*
    * Update frustums shape(angle, ratio, near and far plane)
    *
    */
	void UpdateShape(float angle, float ratio, float nearD, float farD);

	void SetColor(float fColor[4]);
	void GetColor(float fColor[4]) const;

	void GetCorners(VistaVector3D & vecNearTopLeft,
					 VistaVector3D & vecNearTopRight,
					 VistaVector3D & vecNearBottomLeft, 
					 VistaVector3D & vecNearBottomRight,
					 VistaVector3D & vecFarTopLeft,
					 VistaVector3D & vecFarTopRight,
					 VistaVector3D & vecFarBottomLeft,
					 VistaVector3D & vecFarBottomRight);
	/*
	* Getter for vectors
	*
	*/
	void GetUpVector(VistaVector3D & vUp);
	void GetViewPosition(VistaVector3D & vPos);
	void GetViewDirection(VistaVector3D & vDir);

	float GetAngle() const;
	float GetRatio() const;
	float GetNearDistance() const;
	float GetFarDistance() const;

protected:
	virtual VflRenderableProperties* CreateProperties() const;

private:
	enum {
		TOP = 0,
		BOTTOM,
		LEFT,
		RIGHT,
		NEARP,
		FARP
	};

    // frustum planes
	VistaPlane           m_Planes[6];

    // frustum corners
	VistaVector3D        m_vecNearTopLeft,m_vecNearTopRight,m_vecNearBottomLeft,m_vecNearBottomRight,
						  m_vecFarTopLeft,m_vecFarTopRight,m_vecFarBottomLeft,m_vecFarBottomRight;

    // camera internals
	float       m_fNearD, m_fFarD, m_fRatio, m_fAngle, m_fTang;

    // height/width of near/far plane
	float       m_fNearWidth, m_fNearHeight, m_fFarWidth, m_fFarHeight;

    // color
	float m_fColor[4];

	//vectors
	VistaVector3D m_vecPosition, m_vecDirection, m_vecUp;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
inline float VfaFrustumVis::GetAngle() const
{
	return m_fAngle;
}
inline float VfaFrustumVis::GetRatio() const
{
	return m_fRatio;
}
inline float VfaFrustumVis::GetNearDistance() const
{
	return m_fNearD;
}
inline float VfaFrustumVis::GetFarDistance() const
{
	return m_fFarD;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaFrustumVis_H
