/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef __VFACIRCLEMENUVIEW_H
#define __VFACIRCLEMENUVIEW_H

// ========================================================================== //
// === Includes
// ========================================================================== //
#include "../../VistaFlowLibAuxConfig.h"
#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <vector>


// ========================================================================== //
// === Forward Declarations
// ========================================================================== //
class VfaCircleMenuModel;
class VistaTexture;


// ========================================================================== //
// === Class Definition
// ========================================================================== //
class VISTAFLOWLIBAUXAPI VfaCircleMenuView : public IVflRenderable
{
public:
	//! Main constructor.
	VfaCircleMenuView(VfaCircleMenuModel *pModel);
	//! Virtual destructor.
	virtual ~VfaCircleMenuView();

	class VfaCircleMenuViewProps : public VflRenderableProperties
	{
	public:
		enum eMessages
		{
			MSG_PRIMARY_COLOR_CHANGED = VflRenderableProperties::MSG_LAST,
			MSG_SECONDARY_COLOR_CHANGED,
			MSG_SELECT_COLOR_CHANGED,
			MSG_TRANSPARENCY_CHANGED,
			MSG_LAST
		};

		VfaCircleMenuViewProps();
		virtual ~VfaCircleMenuViewProps();

		bool SetDefaults();

		bool SetPrimaryColor(float aColor[3]);
		void GetPrimaryColor(float aColor[3]);

		bool SetSecondaryColor(float aColor[3]);
		void GetSecondaryColor(float aColor[3]);

		bool SetSelectColor(float aColor[3]);
		void GetSelectColor(float aColor[3]);

		bool SetTransparency(float fAlpha);
		float GetTransparency() const;

		// TODO_MID: Something for font size and/or circle diameter.
		//			 Maybe the diameter should be based on the font size?!

	private:
		bool CopyColorBuffer(float aSource[3], float aTarget[3], int iMsg);
		
		float	m_aPriColor[3],
				m_aSecColor[3],
				m_aSelColor[3],
				m_fAlpha;
	};
	

	//! Convenience function to retrieve a property object by typed pointer.
	VfaCircleMenuViewProps* GetTypedProps() const;


	// ---------------------------------------------------------------------- //
	// --- IVflRenderable Interface
	// ---------------------------------------------------------------------- //
	virtual bool Init();
	virtual void Update();
	virtual void DrawOpaque();
	virtual void DrawTransparent();

	virtual unsigned int GetRegistrationMode() const;


	// ---------------------------------------------------------------------- //
	// --- IVistaObserver Interface
	// ---------------------------------------------------------------------- //
	virtual void ObserverUpdate(IVistaObserveable *pObs, int msg, int ticket);


protected:
	// ---------------------------------------------------------------------- //
	// --- IVflRenderable Interface
	// ---------------------------------------------------------------------- //
	virtual VflRenderableProperties* CreateProperties() const;


private:
	enum eLocalTickets
	{
		LOCAL_TICKET_MODEL = 0
	};

	struct sVertex
	{
		float	m_fX, m_fY, m_fZ;
	};

	struct sIcon
	{
		sVertex m_sPosition;
		float	m_fInverseAspect;
	};

	//! Draws the circle menu.
	void DrawCircleMenu();
	//! Function which builds the geometry of the circle menu.
	void BuildGeometry();
	//! Function which loads, generates, and places the icons on the widget.
	void PrepareIcons();


	//! Init status flag.
	bool							m_bIsInited;
	//! The associated circle menu model which will be visualized.
	VfaCircleMenuModel				*m_pModel;
	//! Vector of vertices for the outer circle.
	std::vector<sVertex>			m_vecOuterVerts;
	//! Vector of vertices for the inner circle.
	std::vector<sVertex>			m_vecInnerVerts;
	//! The steps size of a slice, i.e. the number of vertices used to built it.
	int								m_nNumOfSteps;
	//! Vector of texture representing icons for the circle menu.
	std::vector<VistaTexture*>		m_vecIcons;
	//! The positions of for every icon. Pos corresponds to the texture vec.
	std::vector<sIcon>				m_vecIconPositions;
	//! The radius of the bounding circle of _every_ icon.
	float							m_fIconBCircleRadius;
	//! Flag to visualization dirty.
	bool							m_bVisDirty;

	//! Elapsed start up time.
	double							m_dStartTime;
};

#endif // __VFACIRCLEMENUVIEW_H


// ========================================================================== //
// === End of File
// ========================================================================== //
