/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef __VFADIRECTIONFOCUSSTRATEGY_H
#define __VFADIRECTIONFOCUSSTRATEGY_H

// ========================================================================== //
// === Includes
// ========================================================================== //
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaFocusStrategy.h"

#include <VistaBase/VistaVectorMath.h>

#include <vector>
#include <map>


// ========================================================================== //
// === Forward Declarations
// ========================================================================== //
class VfaDirectionHandle;
class VflRenderNode;


// ========================================================================== //
// === Class Definition
// ========================================================================== //
class VISTAFLOWLIBAUXAPI VfaDirectionFocusStrategy : public IVfaFocusStrategy
{
public:
	VfaDirectionFocusStrategy();
	virtual ~VfaDirectionFocusStrategy();

	// ---------------------------------------------------------------------- //
	// --- IVfaFocusStrategy Interface
	// ---------------------------------------------------------------------- //
	virtual bool EvaluateFocus(
		std::vector<IVfaControlHandle*> &vecControlHandles);
	
	virtual void RegisterSelectable(IVfaWidgetController *pSelectable);
	virtual void UnregisterSelectable(IVfaWidgetController *pSelectable);


	// ---------------------------------------------------------------------- //
	// --- IVfaSlotObserver Interface
	// ---------------------------------------------------------------------- //
	virtual void OnSensorSlotUpdate(int iSlot,
		const VfaApplicationContextObject::sSensorFrame &oFrameData);
	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);
	virtual void OnTimeSlotUpdate(const double dTime);

	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;

private:
	std::vector<VfaDirectionHandle*>				m_vecHandles;
	VistaVector3D									m_v3PointerPos;
	std::map<VfaDirectionHandle*, VflRenderNode*>	m_mapHandle2RenderNode;
};

#endif // __VFADIRECTIONFOCUSSTRATEGY_H


// ========================================================================== //
// === End of File
// ========================================================================== //
