/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaCircleMenuWidget.h"

#include "VfaCircleMenuModel.h"
#include "VfaCircleMenuView.h"
#include "VfaCircleMenuController.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <cassert>


// ========================================================================== //
// === Con-/Destructor
// ========================================================================== //
VfaCircleMenuWidget::VfaCircleMenuWidget(VflRenderNode *pRenderNode)
:	IVfaWidget(pRenderNode)
,	m_bIsEnabled(true)
,	m_bIsVisible(true)
,	m_pModel(NULL)
,	m_pView(NULL)
,	m_pController(NULL)
{
	// Model.
	m_pModel = new VfaCircleMenuModel;

	// View
	m_pView = new VfaCircleMenuView(m_pModel);
	m_pView->Init();
	GetRenderNode()->AddRenderable(m_pView);
	m_pView->SetVisible(m_bIsVisible);

	// Controller.
	m_pController = new VfaCircleMenuController(GetRenderNode(), m_pModel,
		m_pView);
	m_pController->SetIsEnabled(m_bIsEnabled);
}

VfaCircleMenuWidget::~VfaCircleMenuWidget()
{
	delete m_pController;
	if( GetRenderNode() )
		GetRenderNode()->RemoveRenderable( m_pView );
	delete m_pView;
	delete m_pModel;
}


// ========================================================================== //
// === Public Interface
// ========================================================================== //
VfaCircleMenuModel* VfaCircleMenuWidget::GetTypedModel() const
{
	return m_pModel;
}

VfaCircleMenuView* VfaCircleMenuWidget::GetTypedView() const
{
	return m_pView;
}

VfaCircleMenuController* VfaCircleMenuWidget::GetTypedController() const
{
	return m_pController;
}


// ========================================================================== //
// === IVfaWidget Interface
// ========================================================================== //
void VfaCircleMenuWidget::SetIsEnabled(bool bIsEnabled)
{
	if(m_bIsEnabled == bIsEnabled)
		return;

	m_bIsEnabled = bIsEnabled;

	m_pController->SetIsEnabled(m_bIsEnabled);
}

bool VfaCircleMenuWidget::GetIsEnabled() const
{
	return m_bIsEnabled;
}


void VfaCircleMenuWidget::SetIsVisible(bool bIsVisible)
{
	assert(false && "Implement this!");
}

bool VfaCircleMenuWidget::GetIsVisible() const
{
	assert(false && "Implement this!");

	return false;
}


IVistaReflectionable* VfaCircleMenuWidget::GetModel() const
{
	return m_pModel;
}

IVflRenderable* VfaCircleMenuWidget::GetView() const
{
	return m_pView;
}

IVfaWidgetController* VfaCircleMenuWidget::GetController() const
{
	return m_pController;
}


// ========================================================================== //
// === End of File
// ========================================================================== //
