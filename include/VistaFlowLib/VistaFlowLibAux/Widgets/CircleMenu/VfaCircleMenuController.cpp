/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaCircleMenuController.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include "VfaCircleMenuModel.h"
#include "VfaCircleMenuView.h"
#include "VfaDirectionHandle.h"
#include <VistaFlowLibAux/Widgets/VfaSphereHandle.h>

#include <cmath>
#include <iostream>

using namespace std;


// ========================================================================== //
// === Con-/Destructor
// ========================================================================== //
VfaCircleMenuController::VfaCircleMenuController(VflRenderNode *pRenderNode,
	VfaCircleMenuModel *pModel, VfaCircleMenuView *pView)
:	IVfaWidgetController(pRenderNode)
,	m_bIsEnabled(true)
,	m_iIntSlot(0)
,	m_pModel(pModel)
,	m_pView(pView)
,	m_bHandlesUpToDate(true)
,	m_pIndicator(NULL)
{
	// Create handles.
	VfaDirectionHandle *pNewDirHandle = 0;
	
	// The max number of handles is 8, so reserve that many handles.
	for(int i=0; i<8; ++i)
	{
		pNewDirHandle = new VfaDirectionHandle;
		m_vecCastedHandles.push_back(pNewDirHandle);
	}
	
	// Register handles.
	for(int i=0; i<(int)m_vecCastedHandles.size(); ++i)
	{
		this->AddControlHandle(m_vecCastedHandles[i]);
	}

	// Observe the model for changes.
	if(m_pModel)
	{
		m_pModel->AttachObserver(this, LOCAL_TICKET_MODEL);
	}

	// Some math setup.
	m_v3Normal	= VistaVector3D(0.0f, 0.0f, 1.0f, 0.0f);
	m_v3Up		= VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f);
	m_v3Right	= VistaVector3D(1.0f, 0.0f, 0.0f, 0.0f);
	m_v3Y		= VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f);

	// Set the handles up, i.e. enable as many of them as there are slots in
	// the model.
	this->SetupHandles();

	// Set up the indicator.
	m_pIndicator = new VfaSphereHandle(GetRenderNode());
	m_pIndicator->SetCenter(VistaVector3D(0.0f, 0.0f, 0.0f));
	m_pIndicator->SetRadius(0.01f);
	
	// TODO_LOW: Maybe there needs to be some observer to detect changes?!
	//float aColor[4];
	//m_pView->GetTypedProps()->GetSecondaryColor(aColor);
	//aColor[3] = m_pView->GetTypedProps()->GetTransparency();
	//m_pIndicator->SetNormalColor(aColor);
	// For now: highlight color!
	//m_pIndicator->SetIsHighlighted(true);
}

VfaCircleMenuController::~VfaCircleMenuController()
{
	if(m_pModel)
	{
		m_pModel->DetachObserver(this);
	}

	for(int i=0; i<(int)m_vecCastedHandles.size(); ++i)
	{
		this->RemoveControlHandle(m_vecCastedHandles[i]);
		delete m_vecCastedHandles[i];
	}
}


// ========================================================================== //
// === Public Interface
// ========================================================================== //
bool VfaCircleMenuController::SetInteractionSlot(int iSlot)
{
	if(iSlot == m_iIntSlot || iSlot < 0)
		return false;

	m_iIntSlot = iSlot;

	return true;
}

int VfaCircleMenuController::GetInteractionSlot() const
{
	return m_iIntSlot;
}


// ========================================================================== //
// === IVfaWidgetController Interface
// ========================================================================== //
void VfaCircleMenuController::SetIsEnabled(bool bIsEnabled)
{
	m_bIsEnabled = bIsEnabled;

	m_pIndicator->SetVisible(false);
	m_pView->SetVisible(false);

	for(size_t i=0; i<m_vecCastedHandles.size(); ++i)
	{
		if(bIsEnabled && i >= size_t(m_pModel->GetNumberOfSlots()))
			break;

		m_vecCastedHandles[i]->SetEnable(bIsEnabled);
	}
}

bool VfaCircleMenuController::GetIsEnabled() const
{
	return m_bIsEnabled;
}


void VfaCircleMenuController::OnFocus(IVfaControlHandle *pHandle)
{
	if(!m_bIsEnabled)
		return;
	
	VfaDirectionHandle *pCasted = dynamic_cast<VfaDirectionHandle*>(pHandle);

	if(pCasted)
	{
		for(int i=0; i<m_pModel->GetNumberOfSlots(); ++i)
		{
			if(pCasted == m_vecCastedHandles[i])
				m_pModel->SetActiveSlot(i);
		}
	}
}

void VfaCircleMenuController::OnUnfocus()
{
	if(!m_bIsEnabled)
		return;

	m_pModel->SetActiveSlot(-1);
}


// ========================================================================== //
// === IVfaSlotObserver Interface
// ========================================================================== //
void VfaCircleMenuController::OnSensorSlotUpdate(int iSlot,
	const VfaApplicationContextObject::sSensorFrame &oFrameData)
{
	if(!m_bIsEnabled)
		return;

	switch(iSlot)
	{
	case VfaApplicationContextObject::SLOT_POINTER_VIS:
		{
			if(!m_bHandlesUpToDate)
			{
				// Determine circle menu position.
				m_pModel->SetPosition(oFrameData.v3Position
					+ oFrameData.qOrientation.GetViewDir());					

				// Determine circle menu orientation.
				VistaTransformMatrix matInvTrans;
				GetRenderNode()->GetInverseTransform(matInvTrans);

				m_v3Up = matInvTrans.Transform(m_v3Y);
				m_v3Up.Normalize();

				if(std::abs(oFrameData.qOrientation.GetViewDir().GetNormalized().Dot(m_v3Up)) > 0.99f)
				{
					m_v3Up = oFrameData.qOrientation.Rotate(VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f));
					m_v3Up.Normalize();
				}

				m_v3Normal = -1.0f * oFrameData.qOrientation.GetViewDir();
				m_v3Normal.Normalize();

				m_v3Right = m_v3Up.Cross(m_v3Normal);
				m_v3Right.Normalize();

				m_v3Up = m_v3Normal.Cross(m_v3Right);
				m_v3Up.Normalize();
				
				float aMat[4][4] = {
					m_v3Right[0], m_v3Up[0], m_v3Normal[0], 0.0f,
					m_v3Right[1], m_v3Up[1], m_v3Normal[1], 0.0f,
					m_v3Right[2], m_v3Up[2], m_v3Normal[2], 0.0f,
							0.0f,	   0.0f,		  0.0f,	1.0f
				};
				

				VistaTransformMatrix oMat(aMat);
				VistaQuaternion qWdgtOri(oMat);
				qWdgtOri.Normalize();
				
				m_pModel->SetOrientation(qWdgtOri);

				// Setup widget handles.
				for(int i=0; i<(int)m_vecCastedHandles.size(); ++i)
				{
					m_vecCastedHandles[i]->SetNullPosition(oFrameData.v3Position);
				}

				m_v3NullPos = oFrameData.v3Position;

				SetupHandles();

				m_bHandlesUpToDate = true;
			}

			VistaVector3D v3PtrMoveDir = oFrameData.v3Position - m_v3NullPos,
						   v3WdgtPos;
			
			const float cfUp	= v3PtrMoveDir.Dot(m_v3Up);
			const float cfRight	= v3PtrMoveDir.Dot(m_v3Right);

			m_pModel->GetPosition(v3WdgtPos);
			m_pIndicator->SetCenter(v3WdgtPos + cfUp * m_v3Up + cfRight * m_v3Right);
		}
		break;
	case VfaApplicationContextObject::SLOT_VIEWER_VIS:
		{
			/*if(m_pModel->GetOrientTowardsUser())
			{
				VistaVector3D v3WdgtPos;
				m_pModel->GetPosition(v3WdgtPos);

				VistaTransformMatrix matInvTrans;
				m_pVisCtrl->GetInverseTransform(matInvTrans);
				
				m_v3Up = matInvTrans.Transform(m_v3Y);
				m_v3Up.Normalize();

				m_v3Normal = m_pVisCtrl->GetViewPosition() - v3WdgtPos;
				m_v3Normal.Normalize();

				m_v3Right = m_v3Up.Cross(m_v3Normal);
				m_v3Right.Normalize();

				m_v3Up = m_v3Normal.Cross(m_v3Right);
				m_v3Up.Normalize();

				float aMat[4][4] = {
					m_v3Right[0], m_v3Up[0], m_v3Normal[0], 0.0f,
					m_v3Right[1], m_v3Up[1], m_v3Normal[1], 0.0f,
					m_v3Right[2], m_v3Up[2], m_v3Normal[2], 0.0f,
							0.0f,	   0.0f,		  0.0f,	1.0f
				};

				VistaTransformMatrix oMat(aMat);
				VistaQuaternion qWdgtOri(oMat);
				qWdgtOri.Normalize();
				
				m_pModel->SetOrientation(qWdgtOri);
			}*/
		}
		break;
	default:
		break;
	}
}

void VfaCircleMenuController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	if(!m_bIsEnabled)
		return;

	if(iSlot != m_iIntSlot)
		return;
	
	if(bSet)
	{
		m_pIndicator->SetVisible(true);
		m_pView->SetVisible(true);
		m_bHandlesUpToDate = false;
		m_pModel->Notify(VfaCircleMenuModel::MSG_MENU_OPENED);
	}
	else if(!bSet)
	{
		m_pModel->Click();
		m_pIndicator->SetVisible(false);
		m_pView->SetVisible(false);
		m_pModel->Notify(VfaCircleMenuModel::MSG_MENU_CLOSED);
	}
}

void VfaCircleMenuController::OnTimeSlotUpdate(const double dTime)
{
	// Note: Unused.
}


int VfaCircleMenuController::GetSensorMask() const
{
	return ((1 << VfaApplicationContextObject::SLOT_POINTER_VIS)
			| (1 << VfaApplicationContextObject::SLOT_VIEWER_VIS));
}

int VfaCircleMenuController::GetCommandMask() const
{
	return (1 << m_iIntSlot);
}

bool VfaCircleMenuController::GetTimeUpdate() const
{
	return false;
}


// ========================================================================== //
// === IVistaObserver Interface
// ========================================================================== //
void VfaCircleMenuController::ObserverUpdate(IVistaObserveable *pObs, int msg,
											  int ticket)
{
	if(ticket == LOCAL_TICKET_MODEL)
	{
		if(msg == VfaCircleMenuModel::MSG_NUMBER_OF_SLOTS_CHANGED
			|| msg == VfaCircleMenuModel::MSG_CIRCLE_DIAMETER_CHANGED
			|| msg == VfaCircleMenuModel::MSG_DEAD_ZONE_SIZE_CHANGED)
		{
			this->SetupHandles();
		}
	}
}


// ========================================================================== //
// === Helper Functions
// ========================================================================== //
void VfaCircleMenuController::SetupHandles()
{
	const int nNumOfSlots		= m_pModel->GetNumberOfSlots();
	const float fRadPerSlice	= 2.0f * Vista::Pi / float(nNumOfSlots);

	// We wanna rotate clockwise to the viewer dir so we need to rotate
	// counter-clockwise around the normal since it is defined as -(viewer dir).
	VistaAxisAndAngle oAxisAngle(m_v3Normal, -fRadPerSlice);
	VistaQuaternion qRot(oAxisAngle);
	
	// Starting from the upwards dir of the widget move clockwise for
	// nNumOfSlots in steps of fDegreePerSlice steps to determine the
	// direction of eacht handle.
	VistaVector3D v3CurrentDir = m_v3Up;
	v3CurrentDir[3] = 0.0f;

	for(int i=0; i<8; ++i)
	{
		if(i<nNumOfSlots)
		{
			m_vecCastedHandles[i]->SetEnable(true);
			m_vecCastedHandles[i]->SetRight(m_v3Right);
			m_vecCastedHandles[i]->SetUp(m_v3Up);
			m_vecCastedHandles[i]->SetDirection(v3CurrentDir);
			m_vecCastedHandles[i]->SetMinimumDistance(
				m_pModel->GetDeadZoneSize() * 0.5f * m_pModel->GetCircleDiameter());
			
			// Note, we are rotating vectors. But they can actually be seen as
			// point vectors and hence be handled as points. So everything is fine
			// here.
			v3CurrentDir = qRot.Rotate(v3CurrentDir);
			v3CurrentDir.Normalize();
			v3CurrentDir[3] = 0.0f;
		}
		else
		{
			m_vecCastedHandles[i]->SetEnable(false);
		}
	}
}


// ========================================================================== //
// === End of File
// ========================================================================== //
