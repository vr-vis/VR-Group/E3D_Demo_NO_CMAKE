/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef __VFACIRCLEMENUMODEL_H
#define __VFACIRCLEMENUMODEL_H

// ========================================================================== //
// === Includes
// ========================================================================== //
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaTransformableWidgetModel.h"

#include <string>
#include <vector>


// ========================================================================== //
// === Class Definition
// ========================================================================== //
class VISTAFLOWLIBAUXAPI VfaCircleMenuModel : public VfaTransformableWidgetModel
{
public:
	enum eMessages
	{
		MSG_NUMBER_OF_SLOTS_CHANGED = VfaTransformableWidgetModel::MSG_LAST,
		MSG_ACTIVE_SLOT_CHANGED,
		MSG_ACTIVE_SLOT_WAS_CLICKED,
		MSG_ALL_SLOTS_DEACTIVATED,
		MSG_CIRCLE_DIAMETER_CHANGED,
		MSG_DEAD_ZONE_SIZE_CHANGED,
		MSG_SLOT_ICON_CHANGED,
		MSG_STARTUP_TIME_CHANGED,
		MSG_MENU_OPENED,
		MSG_MENU_CLOSED,
		MSG_ORIENT_TOWARDS_USER_FLAG_CHANGED,
		MSG_LAST
	};

	//! Default constructor.
	VfaCircleMenuModel();
	//! Virtual destructor.
	virtual ~VfaCircleMenuModel();

	
	//! Sets the number of slots (min = 1, max = 8).
	bool SetNumberOfSlots(int nNumSlots);
	//! Gets the current number of slots.
	int GetNumberOfSlots() const;

	//! Sets the active slot.
	bool SetActiveSlot(int iActiveSlot);
	//! Gets the active slot.
	int GetActiveSlot() const;

	//! Sets the clicked slot to the active slot.
	bool Click();

	//! Sets the diameter of the widget.
	bool SetCircleDiameter(float fDiameter);
	//! Gets the current diameter.
	float GetCircleDiameter() const;

	//! Sets the relative size of the dead zone compared to the circle diameter.
	bool SetDeadZoneSize(float fRelSize);
	//! Gets the relative size of the dead zone.
	float GetDeadZoneSize() const;

	//! Sets the icon texture file of a menu slot.
	bool SetSlotIcon(int iSlot, std::string strFilename);
	//! Gets the icon texture file of a menu slot.
	bool GetSlotIcon(int iSlot, std::string &strFilename);

	//! Sets the start up time in msec.
	bool SetStartupTime(int iMsec);
	//! Gets the current start up time (in msec).
	int	GetStartupTime() const;

	//!
	bool SetOrientTowardsUser(bool bEnable);
	//!
	bool GetOrientTowardsUser() const;
	

private:
	//! Number of slots used in the circle menu.
	int							m_nNumOfSlots;
	//! Currently active slot, -1 if none is active.
	int							m_iActiveSlot;
	//! The slot which was last clicked.
	int							m_iClickedSlot;
	//! The diameter of the circle menu.
	float						m_fDiameter;
	//! The relative size of the dead zone (in % of the circle diameter).
	float						m_fDeadZoneSize;
	//! Some string to fill in the widget slots.
	std::vector<std::string>	m_vecSlotIcons;
	//! Sets the start up time in milliseconds.
	int							m_iStartupTime;
	//!
	bool						m_bOrientTowardsUser;
};

#endif // __VFACIRCLEMENUMODEL_H

// ========================================================================== //
// === End of File
// ========================================================================== //
