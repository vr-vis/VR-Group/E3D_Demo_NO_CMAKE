/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaDirectionHandle.h"
#include <limits>


// ========================================================================== //
// === Con-/Destructor
// ========================================================================== //
VfaDirectionHandle::VfaDirectionHandle(int iType /* = HANDLE_2D_DIRECTION */)
:	IVfaControlHandle(iType)
,   m_fMinDist(-1.0f)
,   m_fMaxDist(-1.0f)
{
	m_bIsVisible	= false;
	m_bIsEnable		= true;

	m_v3Right		= VistaVector3D(1.0f, 0.0f, 0.0f, 0.0f);
	m_v3Up			= VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f);
	m_v3Normal		= VistaVector3D(0.0f, 0.0f, 1.0f, 0.0f);

	m_v3DirVector	= VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f);
	m_v3NullPos		= VistaVector3D(0.0f, 0.0f, 0.0f, 1.0f);
}

VfaDirectionHandle::~VfaDirectionHandle()
{ }


// ========================================================================== //
// === Public Interface
// ========================================================================== //
void VfaDirectionHandle::SetUp(const VistaVector3D &v3Up)
{
	if(v3Up.GetLengthSquared() < std::numeric_limits<float>::epsilon())
		return;

	m_v3Up = v3Up;
	m_v3Up.Normalize();
}

void VfaDirectionHandle::GetUp(VistaVector3D &v3Up) const
{
	v3Up = m_v3Up;
}


void VfaDirectionHandle::SetRight(const VistaVector3D &v3Right)
{
	if(v3Right.GetLengthSquared() < std::numeric_limits<float>::epsilon())
		return;

	m_v3Right = v3Right;
	m_v3Right.Normalize();
}

void VfaDirectionHandle::GetRight(VistaVector3D &v3Right) const
{
	v3Right = m_v3Right;
}


void VfaDirectionHandle::SetDirection(const VistaVector3D &v3DirVec)
{
	m_v3DirVector = v3DirVec;
	m_v3DirVector.Normalize();
}

void VfaDirectionHandle::GetDirection(VistaVector3D &v3DirVec) const
{
	v3DirVec = m_v3DirVector;
}


void VfaDirectionHandle::SetNullPosition(const VistaVector3D &v3NullPos)
{
	m_v3NullPos = v3NullPos;
}

void VfaDirectionHandle::GetNullPosition(VistaVector3D &v3NullPos) const
{
	v3NullPos = m_v3NullPos;
}


void VfaDirectionHandle::SetMinimumDistance(float fMinDist)
{
	if(m_fMaxDist >= 0.0f && fMinDist >= m_fMaxDist)
		return;

	m_fMinDist = fMinDist;
}

float VfaDirectionHandle::GetMinimumDistance() const
{
	return m_fMinDist;
}


void VfaDirectionHandle::SetMaximumDistance(float fMaxDist)
{
	if(m_fMinDist >= 0.0f && fMaxDist <= m_fMinDist)
		return;

	m_fMaxDist = fMaxDist;
}

float VfaDirectionHandle::GetMaximumDistance() const
{
	return m_fMaxDist;
}


// ========================================================================== //
// === IVfaControlHandle Interface
// ========================================================================== //
void VfaDirectionHandle::SetEnable(bool bIsEnabled)
{
	m_bIsEnable = bIsEnabled;
}

bool VfaDirectionHandle::GetEnable() const
{
	return m_bIsEnable;
}


bool VfaDirectionHandle::GetVisible() const
{
	return false;
}


// ========================================================================== //
// === End of File
// ========================================================================== //
