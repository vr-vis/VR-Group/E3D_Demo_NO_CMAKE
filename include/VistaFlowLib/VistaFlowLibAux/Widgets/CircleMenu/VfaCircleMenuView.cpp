/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// ========================================================================== //
// === Includes
// ========================================================================== //
#include <GL/glew.h>

#include "VfaCircleMenuView.h"
#include "VfaCircleMenuModel.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaOGLExt/VistaOGLUtils.h>
#include <VistaOGLExt/VistaTexture.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

#include <limits>
#include <cmath>
#include <string>
#include <cstring>

using namespace std;


// ========================================================================== //
// === Con-/Destructor
// ========================================================================== //
VfaCircleMenuView::VfaCircleMenuView(VfaCircleMenuModel *pModel)
:	m_bIsInited(false)
,   m_pModel(pModel)
,   m_fIconBCircleRadius(0.0f)
,   m_bVisDirty(true)
{
	if(m_pModel)
		m_pModel->AttachObserver(this, LOCAL_TICKET_MODEL);
}

VfaCircleMenuView::~VfaCircleMenuView()
{
	if(m_pModel)
	{
		m_pModel->DetachObserver(this);
	}
}


// ========================================================================== //
// === Public Interface
// ========================================================================== //
VfaCircleMenuView::VfaCircleMenuViewProps*
VfaCircleMenuView::GetTypedProps() const
{
	return static_cast<VfaCircleMenuView::VfaCircleMenuViewProps*>(
		IVflRenderable::GetProperties());
}


// ========================================================================== //
// === IVflRenderable Interface
// ========================================================================== //
bool VfaCircleMenuView::Init()
{
	if(!m_pModel)
		return false;

	// This will also create appropriate properties if necessary.
	if(!IVflRenderable::Init())
		return false;

	// Success.
	m_bIsInited = true;
	return true;
}

void VfaCircleMenuView::Update()
{
	if(m_bVisDirty)
	{
		this->BuildGeometry();
		this->PrepareIcons();

		m_bVisDirty = false;
	}
}

void VfaCircleMenuView::DrawOpaque()
{
	if(this->GetTypedProps()->GetTransparency() >= 1.0f - std::numeric_limits<float>::epsilon())
		this->DrawCircleMenu();
}

void VfaCircleMenuView::DrawTransparent()
{
	if(this->GetTypedProps()->GetTransparency() < 1.0f - std::numeric_limits<float>::epsilon())
		this->DrawCircleMenu();
}


unsigned int VfaCircleMenuView::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE
		| IVflRenderable::OLI_DRAW_TRANSPARENT
		| IVflRenderable::OLI_UPDATE;
}

IVflRenderable::VflRenderableProperties*
VfaCircleMenuView::CreateProperties() const
{
	return new VfaCircleMenuViewProps;
}

// ========================================================================== //
// === IVistaObserver Interface
// ========================================================================== //
void VfaCircleMenuView::ObserverUpdate(IVistaObserveable *pObs, int msg,
										int ticket)
{
	if(ticket == LOCAL_TICKET_MODEL)
	{
		if(msg == VfaCircleMenuModel::MSG_CIRCLE_DIAMETER_CHANGED
			|| msg == VfaCircleMenuModel::MSG_DEAD_ZONE_SIZE_CHANGED
			|| msg == VfaCircleMenuModel::MSG_NUMBER_OF_SLOTS_CHANGED
			|| msg == VfaCircleMenuModel::MSG_SLOT_ICON_CHANGED)
		{
			m_bVisDirty = true;
		}
		else if(msg == VfaCircleMenuModel::MSG_MENU_OPENED)
		{
			m_dStartTime =
				this->GetRenderNode()->GetVisTiming()->GetCurrentClock();
		}
	}
}

// ========================================================================== //
// === Helper Functions
// ========================================================================== //
void VfaCircleMenuView::DrawCircleMenu()
{
	if(!m_bIsInited || !GetProperties()->GetVisible())
		return;

	double dTime = this->GetRenderNode()->GetVisTiming()->GetCurrentClock();
	float fRadiusMod = float(1000.0 * (dTime - m_dStartTime) /
						double(m_pModel->GetStartupTime()));
	
	fRadiusMod = fRadiusMod > 1.0f ? 1.0f : fRadiusMod;

	VistaVector3D v3Pos;
	m_pModel->GetPosition(v3Pos);
	VistaQuaternion qOri;
	m_pModel->GetOrientationVisSpace(qOri);
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glTranslatef(v3Pos[0], v3Pos[1], v3Pos[2]);
	glRotatef(Vista::RadToDeg(acos(qOri[3])*2.0f), qOri[0], qOri[1], qOri[2]);
	glScalef(fRadiusMod, fRadiusMod, fRadiusMod);

	glPushAttrib(GL_ENABLE_BIT				// For lighting & texturing.
					| GL_CURRENT_BIT		// For color.
					| GL_LINE_BIT			// For line width.
					| GL_COLOR_BUFFER_BIT);	// For blending params.
	glDisable(GL_LIGHTING);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Some variables for slice rendering.
	int	iVertex			= -1;
	int iNextVertex		= -1;
	
	// Load necessary colors.
	float	aPriCol[4],
			aSecCol[4],
			aSelCol[4];
	float	fAlpha = this->GetTypedProps()->GetTransparency();

	this->GetTypedProps()->GetPrimaryColor(aPriCol);
	aPriCol[3] = fAlpha;
	this->GetTypedProps()->GetSecondaryColor(aSecCol);
	aSecCol[3] = fAlpha;
	this->GetTypedProps()->GetSelectColor(aSelCol);
	aSelCol[3] = fAlpha;

	// Render slices.
	for(int h=0; h<m_pModel->GetNumberOfSlots(); ++h)
	{
		glBegin(GL_TRIANGLES);	
		if(m_pModel->GetActiveSlot() == h)
			glColor4fv(aSelCol);
		else
			glColor4fv(aPriCol);
		
		// First the inner vertices.
		for(int i=0; i<m_nNumOfSteps; ++i)
		{
			iVertex = h * m_nNumOfSteps + i;
			iNextVertex = iVertex + 1;
			iNextVertex %= (int)m_vecInnerVerts.size();
			
			// First triangle.
			glVertex3f(	m_vecInnerVerts[iVertex].m_fX,
						m_vecInnerVerts[iVertex].m_fY,
						m_vecInnerVerts[iVertex].m_fZ);
			glVertex3f(	m_vecOuterVerts[iNextVertex].m_fX,
						m_vecOuterVerts[iNextVertex].m_fY,
						m_vecOuterVerts[iNextVertex].m_fZ);
			glVertex3f(	m_vecOuterVerts[iVertex].m_fX,
						m_vecOuterVerts[iVertex].m_fY,
						m_vecOuterVerts[iVertex].m_fZ);

			// Second triangle.
			glVertex3f(	m_vecInnerVerts[iVertex].m_fX,
						m_vecInnerVerts[iVertex].m_fY,
						m_vecInnerVerts[iVertex].m_fZ);
			glVertex3f(	m_vecInnerVerts[iNextVertex].m_fX,
						m_vecInnerVerts[iNextVertex].m_fY,
						m_vecInnerVerts[iNextVertex].m_fZ);
			glVertex3f(	m_vecOuterVerts[iNextVertex].m_fX,
						m_vecOuterVerts[iNextVertex].m_fY,
						m_vecOuterVerts[iNextVertex].m_fZ);
		}	
		glEnd();

		glColor4fv(aSecCol);
		glLineWidth(2.0f);

		glBegin(GL_LINE_LOOP);
			for(int i=0; i<=m_nNumOfSteps; ++i)
			{
				iVertex = h * m_nNumOfSteps + i;
				iVertex %= (int)m_vecInnerVerts.size();

				glVertex3f( m_vecInnerVerts[iVertex].m_fX,
							m_vecInnerVerts[iVertex].m_fY,
							m_vecInnerVerts[iVertex].m_fZ);
			}

			for(int i=m_nNumOfSteps; i>=0; --i)
			{
				iVertex = h * m_nNumOfSteps + i;
				iVertex %= (int)m_vecOuterVerts.size();

				glVertex3f(	m_vecOuterVerts[iVertex].m_fX,
							m_vecOuterVerts[iVertex].m_fY,
							m_vecOuterVerts[iVertex].m_fZ);
			}
		glEnd();
	}

	// Render texture quads.
	float	fIconAngle  = 0.0f,
			fLowerLeftX = -1.0f,
			fLowerLeftY = -1.0f;

	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glColor4f(1.0f, 1.0f, 1.0f, fAlpha);

	for(int i=0; i<(int)m_vecIconPositions.size(); ++i)
	{
		fIconAngle	= atanf(m_vecIconPositions[i].m_fInverseAspect);
		fLowerLeftX = -1.0f * cosf(fIconAngle) * m_fIconBCircleRadius;
		fLowerLeftY = -1.0f * sinf(fIconAngle) * m_fIconBCircleRadius;

		m_vecIcons[i]->Bind();
		glBegin(GL_QUADS);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f( m_vecIconPositions[i].m_sPosition.m_fX + fLowerLeftX,
						m_vecIconPositions[i].m_sPosition.m_fY + fLowerLeftY,
						m_vecIconPositions[i].m_sPosition.m_fZ + 0.001f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f( m_vecIconPositions[i].m_sPosition.m_fX - fLowerLeftX,
						m_vecIconPositions[i].m_sPosition.m_fY + fLowerLeftY,
						m_vecIconPositions[i].m_sPosition.m_fZ + 0.001f);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f( m_vecIconPositions[i].m_sPosition.m_fX - fLowerLeftX,
						m_vecIconPositions[i].m_sPosition.m_fY - fLowerLeftY,
						m_vecIconPositions[i].m_sPosition.m_fZ + 0.001f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f( m_vecIconPositions[i].m_sPosition.m_fX + fLowerLeftX,
						m_vecIconPositions[i].m_sPosition.m_fY - fLowerLeftY,
						m_vecIconPositions[i].m_sPosition.m_fZ + 0.001f);
		glEnd();
	}
	
	glPopAttrib();

	glPopMatrix();
}

void VfaCircleMenuView::BuildGeometry()
{
	const int iMaxSteps		= 96;
	m_nNumOfSteps			= iMaxSteps / m_pModel->GetNumberOfSlots();
	const float fOuterRadius = 0.5f * m_pModel->GetCircleDiameter();
	const float fInnerRadius = m_pModel->GetDeadZoneSize() * fOuterRadius;
	const int	iSteps		= m_nNumOfSteps * m_pModel->GetNumberOfSlots();	
	const float fPI			= 2.0f * 4.0f * atan(1.0f);
	const float fRadPerStep = fPI / iSteps;
	const float fRadStart	= fPI / m_pModel->GetNumberOfSlots();
	
	VistaVector3D v3AngleBisector(0.0f, 1.0f, 0.0f);
	VistaAxisAndAngle oStart(VistaVector3D(0.0f, 0.0f, 1.0f),
							  0.5f * fRadStart);
	VistaAxisAndAngle oOuterStep(VistaVector3D(0.0f, 0.0f, 1.0f),
									-fRadPerStep);
		
	// Calculate the outer vertices.
	VistaVector3D v3UnitVector = v3AngleBisector;
	VistaQuaternion qRot = VistaQuaternion(oStart);

	v3UnitVector = qRot.Rotate(v3UnitVector);
	v3UnitVector.Normalize();

	// This angle is used to calculate the icon size and position within the
	// circle menu. It is the angle between the angle bisector and the edge
	// of one menu 'slice'.
	float fSinHalfAngle = v3AngleBisector.Cross(v3UnitVector).GetLength();
	m_fIconBCircleRadius = fOuterRadius;
	m_fIconBCircleRadius -= fOuterRadius / (fSinHalfAngle + 1.0f);
	m_fIconBCircleRadius *= 0.9f;
	
	m_vecOuterVerts.clear();
	m_vecInnerVerts.clear();
	m_vecIconPositions.clear();

	qRot = VistaQuaternion(oOuterStep);

	for(int i=0; i<iSteps; ++i)
	{
		// Determine pos of outer vertex.
		sVertex oNewOuterVert;
		oNewOuterVert.m_fX = fOuterRadius * v3UnitVector[0];
		oNewOuterVert.m_fY = fOuterRadius * v3UnitVector[1];
		oNewOuterVert.m_fZ = fOuterRadius * v3UnitVector[2];
		m_vecOuterVerts.push_back(oNewOuterVert);

		// Determine pos of inner vertex.
		sVertex oNewInnerVert;
		oNewInnerVert.m_fX = fInnerRadius * v3UnitVector[0];
		oNewInnerVert.m_fY = fInnerRadius * v3UnitVector[1];
		oNewInnerVert.m_fZ = fInnerRadius * v3UnitVector[2];
		m_vecInnerVerts.push_back(oNewInnerVert);

		// Icon position determination.
		// But only if we got a new slice!
		if(i%m_nNumOfSteps == 0)
		{
			sIcon oNewIcon;
			oNewIcon.m_sPosition.m_fX = (fOuterRadius - m_fIconBCircleRadius)
										* v3AngleBisector[0];
			oNewIcon.m_sPosition.m_fY = (fOuterRadius - m_fIconBCircleRadius)
										* v3AngleBisector[1];
			oNewIcon.m_sPosition.m_fZ = (fOuterRadius - m_fIconBCircleRadius)
										* v3AngleBisector[2];
			m_vecIconPositions.push_back(oNewIcon);
		}

		// Vector updates.
		v3UnitVector = qRot.Rotate(v3UnitVector);
		v3UnitVector.Normalize();
		v3AngleBisector = qRot.Rotate(v3AngleBisector);
		v3AngleBisector.Normalize();
	}
}

void VfaCircleMenuView::PrepareIcons()
{
	// Get any old texture out of the way before building the new ones.
	for(int i=0; i<(int)m_vecIcons.size(); ++i)
	{
		delete m_vecIcons[i];
	}

	m_vecIcons.clear();

	// Build a sufficient number of new textures.
	unsigned char *pData = 0;
	int iWidth, iHeight, iChannels, iFormat;
	string strFilename;
	
	// Load the textures.
	for(int i=0; i<(int)m_pModel->GetNumberOfSlots(); ++i)
	{
		m_vecIcons.push_back(new VistaTexture(GL_TEXTURE_2D));
		
		if(m_pModel->GetSlotIcon(i, strFilename))
		{
			if(strFilename == "")
				continue;
			
			pData = VistaOGLUtils::LoadImageFromTga(strFilename,
				iWidth, iHeight, iChannels);

			iFormat = iChannels == 3 ? GL_RGB : GL_RGBA;

			if(pData)
			{
				m_vecIcons.back()->Bind();
				glTexImage2D(GL_TEXTURE_2D, 0, iChannels, iWidth, iHeight, 0,
					iFormat, GL_UNSIGNED_BYTE, pData);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				// Remember the texture's aspect so we can resize the proxy quad
				// accordingly.
				m_vecIconPositions.at(i).m_fInverseAspect =
					float(iHeight) / float(iWidth);

				VistaOGLUtils::Delete(pData);
			}
		}
	}

	glBindTexture(GL_TEXTURE_2D, 0);
}


// ========================================================================== //
// === Helper Class
// ========================================================================== //
VfaCircleMenuView::VfaCircleMenuViewProps::VfaCircleMenuViewProps()
{
	this->SetDefaults();
}

VfaCircleMenuView::VfaCircleMenuViewProps::~VfaCircleMenuViewProps()
{
	
}


bool VfaCircleMenuView::VfaCircleMenuViewProps::SetDefaults()
{
	float aPriColor[4]	= {0.0f, 0.0f, 0.5f};
	float aSecColor[4]	= {1.0f, 1.0f, 1.0f};
	float aSelColor[4]	= {0.5f, 0.5f, 1.0f};
	float fAlpha		= 1.0f;

	this->SetPrimaryColor(aPriColor);
	this->SetSecondaryColor(aSecColor);
	this->SetSelectColor(aSelColor);
	this->SetTransparency(fAlpha);

	return true;
}


bool VfaCircleMenuView::VfaCircleMenuViewProps::SetPrimaryColor(
	float aColor[3])
{
	return this->CopyColorBuffer(aColor, m_aPriColor,
		MSG_PRIMARY_COLOR_CHANGED);
}

void VfaCircleMenuView::VfaCircleMenuViewProps::GetPrimaryColor(
	float aColor[3])
{
	memcpy(aColor, m_aPriColor, 3*sizeof(float));
}


bool VfaCircleMenuView::VfaCircleMenuViewProps::SetSecondaryColor(
	float aColor[3])
{
	return this->CopyColorBuffer(aColor, m_aSecColor,
		MSG_SECONDARY_COLOR_CHANGED);
}

void VfaCircleMenuView::VfaCircleMenuViewProps::GetSecondaryColor(
	float aColor[3])
{
	memcpy(aColor, m_aSecColor, 3*sizeof(float));
}


bool VfaCircleMenuView::VfaCircleMenuViewProps::SetSelectColor(
	float aColor[3])
{
	return this->CopyColorBuffer(aColor, m_aSelColor,
		MSG_SELECT_COLOR_CHANGED);
}

void VfaCircleMenuView::VfaCircleMenuViewProps::GetSelectColor(
	float aColor[3])
{
	memcpy(aColor, m_aSelColor, 3*sizeof(float));
}


bool VfaCircleMenuView::VfaCircleMenuViewProps::SetTransparency(
	float fAlpha)
{
	if(m_fAlpha == fAlpha || fAlpha < 0.0f || fAlpha > 1.0f)
		return false;

	m_fAlpha = fAlpha;

	this->Notify(MSG_TRANSPARENCY_CHANGED);

	return true;
}

float VfaCircleMenuView::VfaCircleMenuViewProps::GetTransparency() const
{
	return m_fAlpha;
}


bool VfaCircleMenuView::VfaCircleMenuViewProps::CopyColorBuffer(
	float aSource[3], float aTarget[3], int iMsg)
{
	for(int i=0; i<3; ++i)
	{
		if(aSource[i] < 0.0f || aSource[i] > 1.0f)
			return false;
	}

	if(0 == memcmp(aSource, aTarget, 3*sizeof(float)))
		return false;

	memcpy(aTarget, aSource, 3*sizeof(float));

	this->Notify(iMsg);

	return true;
}


// ========================================================================== //
// === End of File
// ========================================================================== //
