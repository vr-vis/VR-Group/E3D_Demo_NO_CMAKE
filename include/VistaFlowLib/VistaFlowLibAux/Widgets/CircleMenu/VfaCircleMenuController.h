/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef __VFACIRCLEMENUCONTROLLER_H
#define __VFACIRCLEMENUCONTROLLER_H

// ========================================================================== //
// === Includes
// ========================================================================== //
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaWidgetController.h"

#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaBase/VistaVectorMath.h>

#include <vector>


// ========================================================================== //
// === Forward Declarations
// ========================================================================== //
class VflRenderNode;
class VfaCircleMenuModel;
class VfaCircleMenuView;
class VfaDirectionHandle;
class VfaSphereHandle;


// ========================================================================== //
// === Class Definition
// ========================================================================== //
class VISTAFLOWLIBAUXAPI VfaCircleMenuController
	:	public IVfaWidgetController
	,	public VflObserver
{
public:
	VfaCircleMenuController(VflRenderNode *pRenderNode,
		VfaCircleMenuModel *pModel, VfaCircleMenuView *pView);
	virtual ~VfaCircleMenuController();


	//! Sets the slot which is used for interaction.
	bool SetInteractionSlot(int iSlot);
	//! Gets the current slot which is used for interaction.
	int GetInteractionSlot() const;


	// ---------------------------------------------------------------------- //
	// --- IVfaWidgetController Interface
	// ---------------------------------------------------------------------- //
	virtual void SetIsEnabled(bool bIsEnabled);
	virtual bool GetIsEnabled() const;

	virtual void OnFocus(IVfaControlHandle *pHandle);
	virtual void OnUnfocus();


	// ---------------------------------------------------------------------- //
	// --- IVfaSlotObserver Interface
	// ---------------------------------------------------------------------- //
	virtual void OnSensorSlotUpdate(int iSlot,
		const VfaApplicationContextObject::sSensorFrame &oFrameData);
	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);
	virtual void OnTimeSlotUpdate(const double dTime);

	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;


	// ---------------------------------------------------------------------- //
	// --- IVistaObserver 
	// ---------------------------------------------------------------------- //
	virtual void ObserverUpdate(IVistaObserveable *pObs, int msg, int ticket);


private:
	enum eLocalTickets
	{
		LOCAL_TICKET_MODEL = 0
	};

	//! This function sets up the handles to match the slot number in the model.
	void SetupHandles();


	//! Enabled state flag.
	bool								m_bIsEnabled;
	//! Slot used for interaction.
	int									m_iIntSlot;
	//! Associated model.
	VfaCircleMenuModel					*m_pModel;
	//! Associated view.
	VfaCircleMenuView					*m_pView;
	//! Vector of casted handles.
	std::vector<VfaDirectionHandle*>	m_vecCastedHandles;
	//! Flag to update the handles' null pos.
	bool								m_bHandlesUpToDate;
	//! Sphere handle as indicator (not a 'focusable' handle!).
	VfaSphereHandle					*m_pIndicator;
	
	//! Local copy of the handles null position.
	VistaVector3D						m_v3NullPos;
	//! Normal of the widget.
	VistaVector3D						m_v3Normal;
	//! Up vector of the widget.
	VistaVector3D						m_v3Up;
	//! Right vector of the widget.
	VistaVector3D						m_v3Right;
	//!
	VistaVector3D						m_v3Y;
};

#endif // __VFACIRCLEMENUCONTROLLER_H


// ========================================================================== //
// === End of File
// ========================================================================== //
