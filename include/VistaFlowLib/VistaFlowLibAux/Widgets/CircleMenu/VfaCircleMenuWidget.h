/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef __VFACIRCLEMENUWIDGET_H
#define __VFACIRCLEMENUWIDGET_H

// ========================================================================== //
// === Includes
// ========================================================================== //
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaWidget.h"


// ========================================================================== //
// === Forward Declarations
// ========================================================================== //
class VfaCircleMenuModel;
class VfaCircleMenuView;
class VfaCircleMenuController;

class IVistaReflectionable;
class IVflRenderable;
class IVfaWidgetController;

class VflRenderNode;


// ========================================================================== //
// === Class Definition
// ========================================================================== //
class VISTAFLOWLIBAUXAPI VfaCircleMenuWidget : public IVfaWidget
{
public:
	//! Main constructor.
	VfaCircleMenuWidget(VflRenderNode *pRenderNode);
	//! Virtual destructor.
	virtual ~VfaCircleMenuWidget();

	//! Convenience function to retrieve a typed model.
	VfaCircleMenuModel*			GetTypedModel() const;
	//! Convenience function to retrieve a typed view.
	VfaCircleMenuView*			GetTypedView() const;
	//! Convenience function to retrieve a typed controller.
	VfaCircleMenuController*	GetTypedController() const;

	// ---------------------------------------------------------------------- //
	// --- IVfaWidget Interface
	// ---------------------------------------------------------------------- //
	virtual void SetIsEnabled(bool bIsEnabled);
	virtual bool GetIsEnabled() const;

	virtual void SetIsVisible(bool bIsVisible);
	virtual bool GetIsVisible() const;

	virtual IVistaReflectionable*	GetModel() const;
	virtual IVflRenderable*			GetView() const;
	virtual IVfaWidgetController*	GetController() const;

private:
	bool						m_bIsEnabled;
	bool						m_bIsVisible;
	VfaCircleMenuModel*			m_pModel;
	VfaCircleMenuView*			m_pView;
	VfaCircleMenuController*	m_pController;
};

#endif // __VFACIRCLEMENUWIDGET_H


// ========================================================================== //
// === End of File
// ========================================================================== //
