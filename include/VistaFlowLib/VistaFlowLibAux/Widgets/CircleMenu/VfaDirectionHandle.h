/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef __VFADIRECTIONHANDLE_H
#define __VFADIRECTIONHANDLE_H

// ========================================================================== //
// === Includes
// ========================================================================== //
#include "../../VistaFlowLibAuxConfig.h"
#include "../VfaControlHandle.h"

#include <VistaBase/VistaVectorMath.h>


// ========================================================================== //
// === Class Definition
// ========================================================================== //
class VISTAFLOWLIBAUXAPI VfaDirectionHandle : public IVfaControlHandle
{
public:
	enum eTypes
	{
		HANDLE_2D_DIRECTION = IVfaControlHandle::HANDLE_FIRST_EXTERNAL,
		HANDLE_3D_DIRECTION,
		HANDLE_LAST
	};

	//! Default constructor.
	VfaDirectionHandle(int iType = HANDLE_2D_DIRECTION);
	//! Virtual destructor.
	virtual ~VfaDirectionHandle();


	void SetUp(const VistaVector3D &v3Up);
	void GetUp(VistaVector3D &v3Up) const;
	
	void SetRight(const VistaVector3D &v3Right);
	void GetRight(VistaVector3D &v3Right) const;
	
	//! Sets the direction vector of the handle.
	void SetDirection(const VistaVector3D &v3DirVec);
	//! Gets the direction vector of the handle.
	void GetDirection(VistaVector3D &v3DirVec) const;

	//! Sets the null pos used to calculate the pointer direction.
	/*!
	 * NOTE: For direction handles of one widget, this null position should
	 *		 be the same for all handles.
	 *
	 * TODO: Maybe the focus strat should use the null position to disambiguate
	 *		 the situation in case there are multiple widgets using direction
	 *		 handles. E.g. closest widget's handles are active, others ignored.
	 */
	void SetNullPosition(const VistaVector3D &v3NullPos);
	//! Gets the current null pos of the handle.
	void GetNullPosition(VistaVector3D &v3NullPos) const;

	//! Sets the distance the ptr needs to the handle's null pos to activate it.
	void SetMinimumDistance(float fMinDist);
	//! Gets the current min distance.
	float GetMinimumDistance() const;

	//! Sets the distance beyond which the ptr cannot activate the handle.
	void SetMaximumDistance(float fMaxDist);
	//! Gets the current max distance.
	float GetMaximumDistance() const;


	// ---------------------------------------------------------------------- //
	// --- IVfaControlHandle Interface
	// ---------------------------------------------------------------------- //
	virtual void SetEnable(bool bIsEnabled);
	virtual bool GetEnable() const;

	//! This is an invisible handle, so the function simply returns 'false'.
	virtual bool GetVisible() const;

private:
	VistaVector3D		m_v3Normal;
	VistaVector3D		m_v3Up;
	VistaVector3D		m_v3Right;
	//! Holds the direction vector of the handle (default: (0,1,0)).
	VistaVector3D		m_v3DirVector;
	//! Holds the null pos which is used to calculate the pointer movement dir.
	VistaVector3D		m_v3NullPos;
	//! The minimum ptr-handle distance (set to negative value to deactivate).
	float				m_fMinDist;
	//! The maximum ptr-handle distance (set to negative value to deactivate).
	float				m_fMaxDist;
};

#endif // __VFADIRECTIONHANDLE_H


// ========================================================================== //
// === End of File
// ========================================================================== //
