

set( RelativeDir "./Widgets/CircleMenu" )
set( RelativeSourceGroup "Source Files\\Widgets\\CircleMenu" )

set( DirFiles
	VfaCircleMenuController.cpp
	VfaCircleMenuController.h
	VfaCircleMenuModel.cpp
	VfaCircleMenuModel.h
	VfaCircleMenuView.cpp
	VfaCircleMenuView.h
	VfaCircleMenuWidget.cpp
	VfaCircleMenuWidget.h
	VfaDirectionFocusStrategy.cpp
	VfaDirectionFocusStrategy.h
	VfaDirectionHandle.cpp
	VfaDirectionHandle.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

