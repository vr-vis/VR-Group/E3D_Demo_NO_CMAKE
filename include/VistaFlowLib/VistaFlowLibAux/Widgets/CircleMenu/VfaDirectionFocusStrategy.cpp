/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaDirectionFocusStrategy.h"

#include "VfaDirectionHandle.h"
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <map>

using namespace std;


// ========================================================================== //
// === Con-/Destructor
// ========================================================================== //
VfaDirectionFocusStrategy::VfaDirectionFocusStrategy()
{ }

VfaDirectionFocusStrategy::~VfaDirectionFocusStrategy()
{ }


// ========================================================================== //
// === IVfaFocusStrategy Interface
// ========================================================================== //
bool VfaDirectionFocusStrategy::EvaluateFocus(
	std::vector<IVfaControlHandle*> &vecControlHandles)
{
	// Clear the output container.
	vecControlHandles.clear();

	// No handles --> nothing to do!
	if(m_vecHandles.size() < 1)
		return false;

	// Walk through all handles and determine their order.
	// Note: All dir handles will be touched! It is kind of in the nature of
	//		 this type of handles. But after all, we are only interested in
	//		 the focused one so it should not matter that much here.
	VistaVector3D v3HandleDir, v3PointerDir, v3NullPos, v3Up, v3Right;
	float fDot, fDist;
	multimap<float, IVfaControlHandle*> mapSortedHandles;

	VistaTransformMatrix	mInvTrans;
	VistaVector3D			v3LocalPointerPos;

	for(int i=0; i<(int)m_vecHandles.size(); ++i)
	{
		if(!m_vecHandles[i]->GetEnable())
			continue;

		m_mapHandle2RenderNode[m_vecHandles[i]]
			->GetWorldInverseTransform(mInvTrans);
		v3LocalPointerPos = mInvTrans.Transform(m_v3PointerPos);
		v3LocalPointerPos[3] = 1.0f;

		// Calculate pointer movement dir for the current handle.
		m_vecHandles[i]->GetDirection(v3HandleDir);
		m_vecHandles[i]->GetRight(v3Right);
		m_vecHandles[i]->GetUp(v3Up);
		m_vecHandles[i]->GetNullPosition(v3NullPos);
		v3PointerDir = v3LocalPointerPos - v3NullPos;
		
		const float cfUp	= v3PointerDir.Dot(v3Up);
		const float cfRight = v3PointerDir.Dot(v3Right); 

		v3PointerDir = cfUp * v3Up + cfRight * v3Right;

		// Check if distance limits are obeyed.
		fDist = v3PointerDir.GetLength();

		if(m_vecHandles[i]->GetMinimumDistance() >= 0.0f
			&& fDist < m_vecHandles[i]->GetMinimumDistance())
		{
			continue;
		}

		if(m_vecHandles[i]->GetMaximumDistance() >= 0.0f
			&& fDist > m_vecHandles[i]->GetMaximumDistance())
		{
			continue;
		}

		// Now calculate the deviation of pointer dir and handle dir.
		// Note: Maybe the unnormalized pointer dir can be used to give
		//		 distance penalties.
		v3PointerDir.Normalize();
		fDot = v3PointerDir.Dot(v3HandleDir);

		// Add handle and dot product result to the multimap.
		// Note: Dot result is multiplied by -1.0f as to position best result,
		//		 i.e. near 1.0f result, in the front of the map!
		pair<float, IVfaControlHandle*> oNewEntry(-fDot, m_vecHandles[i]);
		mapSortedHandles.insert(oNewEntry);
	}

	// Last but not least, copy the elements from the map to the result vector.
	multimap<float, IVfaControlHandle*>::iterator itCurEntry =
		mapSortedHandles.begin();

	while(itCurEntry != mapSortedHandles.end())
	{
		vecControlHandles.push_back((*itCurEntry).second);

		++itCurEntry;
	}
	
	return true;
}

void VfaDirectionFocusStrategy::RegisterSelectable(
	IVfaWidgetController *pSelectable)
{
	if(!pSelectable)
		return;

	vector<IVfaControlHandle*>::const_iterator itCurHandle =
		pSelectable->GetControlHandles().begin();

	VfaDirectionHandle *pCasted = 0;
	bool bNewHandle = true;
	
	while(itCurHandle != pSelectable->GetControlHandles().end())
	{
		pCasted = dynamic_cast<VfaDirectionHandle*>(*itCurHandle);

		// We only want direction handles!
		if(!pCasted)
		{
			++itCurHandle;
			continue;
		}

		// Check if handle already exists.
		for(int i=0; i<(int)m_vecHandles.size(); ++i)
		{
			if(pCasted == m_vecHandles[i])
			{
				bNewHandle = false;
				break;
			}
		}

		// If it is a new handle, register it.
		if(bNewHandle)
		{
			m_vecHandles.push_back(pCasted);
			m_mapHandle2RenderNode[pCasted] = pSelectable->GetRenderNode();
		}

		// Cleanup for next cycle.
		bNewHandle = true;
		++itCurHandle;
	}
}

void VfaDirectionFocusStrategy::UnregisterSelectable(
	IVfaWidgetController *pSelectable)
{
	if(!pSelectable)
		return;

	vector<IVfaControlHandle*>::const_iterator itCurHandle =
		pSelectable->GetControlHandles().begin();

	VfaDirectionHandle *pCasted = 0;
	vector<VfaDirectionHandle*>::iterator itOwnHandle;

	while(itCurHandle != pSelectable->GetControlHandles().end())
	{
		pCasted = dynamic_cast<VfaDirectionHandle*>(*itCurHandle);

		// Not a dir handle? Can't be registered here.
		if(!pCasted)
		{
			++itCurHandle;
			continue;
		}

		// Look for the handle.
		itOwnHandle = m_vecHandles.begin();

		while(itOwnHandle != m_vecHandles.end())
		{
			if(pCasted == *itOwnHandle)
			{
				m_vecHandles.erase(itOwnHandle);
				m_mapHandle2RenderNode.erase(pCasted);
				break;
			}

			++itOwnHandle;
		}

		++itCurHandle;
	}
}


// ========================================================================== //
// === IVfaSlotObserver Interface
// ========================================================================== //
void VfaDirectionFocusStrategy::OnSensorSlotUpdate(int iSlot,
	const VfaApplicationContextObject::sSensorFrame &oFrameData)
{
	if(iSlot != VfaApplicationContextObject::SLOT_POINTER_WORLD)
		return;

	m_v3PointerPos		= oFrameData.v3Position;
	// Just to be sure, make it a point.
	m_v3PointerPos[3]	= 1.0f;
}

void VfaDirectionFocusStrategy::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	// Note: Unused.
}

void VfaDirectionFocusStrategy::OnTimeSlotUpdate(const double dTime)
{
	// Note: Unused.
}


int VfaDirectionFocusStrategy::GetSensorMask() const
{
	return (1 << VfaApplicationContextObject::SLOT_POINTER_WORLD);
}

int VfaDirectionFocusStrategy::GetCommandMask() const
{
	return 0;
}

bool VfaDirectionFocusStrategy::GetTimeUpdate() const
{
	// Same fate for you, dude.
	return false;
}


// ========================================================================== //
// === End of File
// ========================================================================== //
