/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// ========================================================================== //
// === Includes
// ========================================================================== //
#include "VfaCircleMenuModel.h"

using namespace std;


// ========================================================================== //
// === Con-/Destructor
// ========================================================================== //
VfaCircleMenuModel::VfaCircleMenuModel()
:	m_nNumOfSlots(4)
,   m_iActiveSlot(-1)
,   m_fDiameter(0.25f)
,   m_fDeadZoneSize(0.10f)
,   m_iStartupTime(100)
,   m_bOrientTowardsUser(false)
{
	m_vecSlotIcons.resize(8);

	for(int i=0; i<(int)m_vecSlotIcons.size(); ++i)
	{
		m_vecSlotIcons[i] = "";
	}
}

VfaCircleMenuModel::~VfaCircleMenuModel()
{

}


// ========================================================================== //
// === Public Interface
// ========================================================================== //
bool VfaCircleMenuModel::SetNumberOfSlots(int nNumSlots)
{
	if(nNumSlots < 2 || nNumSlots > 8 || nNumSlots == m_nNumOfSlots)
	{
		return false;
	}

	m_nNumOfSlots = nNumSlots;

	this->Notify(MSG_NUMBER_OF_SLOTS_CHANGED);
	
	// When the number of slots changes the currently active slot becomes
	// invalid (in case there is one) due to the circle menu being re-layouted.
	// Hence, we'll simply deactivate (reset) the active slot.
	this->SetActiveSlot(-1);

	return true;
}

int VfaCircleMenuModel::GetNumberOfSlots() const
{
	return m_nNumOfSlots;
}


bool VfaCircleMenuModel::SetActiveSlot(int iActiveSlot)
{
	if(iActiveSlot < -1 || iActiveSlot > this->GetNumberOfSlots()-1
		|| iActiveSlot == m_iActiveSlot)
	{
		return false;
	}

	m_iActiveSlot = iActiveSlot;

	if(m_iActiveSlot == -1)
	{
		this->Notify(MSG_ALL_SLOTS_DEACTIVATED);
	}
	else
	{
		this->Notify(MSG_ACTIVE_SLOT_CHANGED);
	}

	return true;
}

int VfaCircleMenuModel::GetActiveSlot() const
{
	return m_iActiveSlot;
}


bool VfaCircleMenuModel::Click()
{
	if(m_iActiveSlot >= 0 && m_iActiveSlot <= m_nNumOfSlots)
	{
		this->Notify(MSG_ACTIVE_SLOT_WAS_CLICKED);

		return true;
	}

	return false;
}


bool VfaCircleMenuModel::SetCircleDiameter(float fDiameter)
{
	if(fDiameter == m_fDiameter)
		return false;

	m_fDiameter = fDiameter;

	this->Notify(MSG_CIRCLE_DIAMETER_CHANGED);

	return true;
}

float VfaCircleMenuModel::GetCircleDiameter() const
{
	return m_fDiameter;
}


bool VfaCircleMenuModel::SetDeadZoneSize(float fRelSize)
{
	if(fRelSize == m_fDeadZoneSize)
		return false;

	m_fDeadZoneSize = fRelSize;

	this->Notify(MSG_DEAD_ZONE_SIZE_CHANGED);

	return true;
}

float VfaCircleMenuModel::GetDeadZoneSize() const
{
	return m_fDeadZoneSize;
}


bool VfaCircleMenuModel::SetSlotIcon(int iSlot, std::string strFilename)
{
	if(iSlot < 0 || iSlot >= 8)
		return false;

	if(strFilename == m_vecSlotIcons[iSlot])
		return false;

	m_vecSlotIcons[iSlot] = strFilename;

	this->Notify(MSG_SLOT_ICON_CHANGED);

	return true;
}

bool VfaCircleMenuModel::GetSlotIcon(int iSlot, std::string &strFilename)
{
	if(iSlot < 0 || iSlot >= 8)
		return false;

	strFilename = m_vecSlotIcons[iSlot];

	return true;
}


bool VfaCircleMenuModel::SetStartupTime(int iMsec)
{
	if(iMsec == m_iStartupTime)
		return false;

	m_iStartupTime = iMsec;

	this->Notify(MSG_STARTUP_TIME_CHANGED);

	return true;
}

int VfaCircleMenuModel::GetStartupTime() const
{
	return m_iStartupTime;
}


bool VfaCircleMenuModel::SetOrientTowardsUser(bool bEnable)
{
	if(m_bOrientTowardsUser == bEnable)
		return false;

	m_bOrientTowardsUser = bEnable;

	Notify(MSG_ORIENT_TOWARDS_USER_FLAG_CHANGED);

	return true;
}

bool VfaCircleMenuModel::GetOrientTowardsUser() const
{
	return m_bOrientTowardsUser;
}


// ========================================================================== //
// === End of File
// ========================================================================== //
