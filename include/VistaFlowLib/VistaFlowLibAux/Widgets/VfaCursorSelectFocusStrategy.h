/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACURSORSELECTFOCUSSTRATEGY_H
#define _VFACURSORSELECTFOCUSSTRATEGY_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaFocusStrategy.h"
#include <list>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaInteractionEvent;
class IVfaNewWidgetController;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/*
* CursorSelectStrategy is a focus strategy which has a single widget(or none) in focus and never only touches
* widgets. The focused widget is changed by the command slot iCommandSlot. The strategy goes stepwise through
* the list of all registered widgets(not control handles!) on each command slot activation. At the end of the list,
* no widget is focused, and with the next command slot activation, the first widget gets selected and so on...
*
*/
class VISTAFLOWLIBAUXAPI VfaCursorSelectFocusStrategy : public IVfaFocusStrategy
{
public:
	VfaCursorSelectFocusStrategy(int iCommandSlot);
	virtual ~VfaCursorSelectFocusStrategy();

	bool EvaluateFocus(std::vector<IVfaControlHandle*> &vecControlHandles);

	void RegisterSelectable(IVfaWidgetController* pSelectable);

	void UnregisterSelectable(IVfaWidgetController* pSelectable);

	void OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);

	void OnCommandSlotUpdate(int iSlot, const bool & bSet);

	void OnTimeSlotUpdate(const double & dTime);

	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;
protected:

private:
	int m_iCursorPos;
	int m_iCommandSlot;
	std::list<IVfaWidgetController*> m_liWidgetCtrls;
	std::list<IVfaWidgetController*>::iterator m_iterCurser;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFACURSORSELECTFOCUSSTRATEGY_H
