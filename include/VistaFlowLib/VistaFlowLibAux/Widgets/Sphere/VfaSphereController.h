/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFASPHERECONTROLLER_H
#define _VFASPHERECONTROLLER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetController.h"

#include <VistaBase/VistaVectorMath.h>

#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaSphereHandle;
class VistaColor;
class VistaIndirectXform;
class VfaSphereModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Controll the behavior
 */
class VISTAFLOWLIBAUXAPI VfaSphereController : public VflObserver, public IVfaWidgetController
{
public:
	enum Messages
	{
		MSG_FOCUS_RECEIVED = IVfaWidgetController::MSG_LAST,
		MSG_FOCUS_LOST,
		MSG_LAST
	};
	
	VfaSphereController(VfaSphereModel *pModel, VflRenderNode *pRenderNode);
	virtual ~VfaSphereController();

	/** 
	 * This is called on transition change into(out of) state FOCUS
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();

	/**
	 * This is called on transition change into(out of) state TOUCH
	 */
	void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	void OnUntouch();

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	void OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate(int iSlot, const bool bSet);
	void OnTimeSlotUpdate(const double dTime);
	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);


	class VISTAFLOWLIBAUXAPI VfaSphereControllerProps : public IVistaReflectionable
	{
		friend class VfaSphereController;
	public:
		
		VfaSphereControllerProps(VfaSphereController *pSphereCtrl);
		virtual ~VfaSphereControllerProps();

		bool SetHandleRadius(float fRadius);
		float GetHandleRadius() const;

		bool SetResizeHandleNormalColor(const VistaColor& color);
		VistaColor GetResizeHandleNormalColor() const;
		void SetResizeHandleNormalColor(float fC[4]);
		void GetResizeHandleNormalColor(float fC[4]) const;

		bool SetResizeHandleHighlightColor(const VistaColor& color);
		VistaColor GetResizeHandleHighlightColor() const;
		void SetResizeHandleHighlightColor(float fC[4]);
		void GetResizeHandleHighlightColor(float fC[4]) const;

		bool SetTranslationHandleNormalColor(const VistaColor& color);
		VistaColor GetTranslationHandleNormalColor() const;
		void SetTranslationHandleNormalColor(float fC[4]);
		void GetTranslationHandleNormalColor(float fC[4]) const;

		bool SetTranslationHandleHighlightColor(const VistaColor& color);
		VistaColor GetTranslationHandleHighlightColor() const;
		void SetTranslationHandleHighlightColor(float fC[4]);
		void GetTranslationHandleHighlightColor(float fC[4]) const;

		bool GetResizeHandleVisible() const;
		void SetResizeHandleVisible(bool bVisible);
		void SetTranslationHandleVisible(bool bVisible);
		bool GetTranslationHandleVisible() const;

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;


	private:
		VfaSphereController	*m_pSphereCtrl;

	};


	VfaSphereControllerProps* GetProperties() const;

protected:

	void UpdateHandles(const VistaVector3D &v3Min, const VistaVector3D &v3Max);

private:
	enum eSphereControllerState { SCS_NONE, SCS_DO};
	VfaSphereController::eSphereControllerState m_iCurrentInternalState;
	enum eSphereDoState {SCS_MOVE, SCS_RESIZE };
	VfaSphereController::eSphereDoState m_iInternalDoState;

	VfaSphereModel				*m_pModel;

	int							m_iInteractionButton;

	//enumeration for handles
	enum
	{
		HND_SPHERE_CENTER = 0,
		HND_SPHERE_SURFACE = 1
	};

	vector<VfaSphereHandle*> m_vecHandles;
	VistaIndirectXform			*m_pXform;

	VfaSphereControllerProps		*m_pCtlProps;

	// focused handle and its number
	VfaSphereHandle*		m_pFocusHandle;
	int							m_iHandle;

	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;

	bool						m_bEnabled;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASphereCONTROLLER_H
