/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#include "VfaSphereController.h"

#include "VfaSphereModel.h"
#include "VfaSphereWidget.h"
#include "../VfaSphereHandle.h"

#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaMath/VistaIndirectXform.h>

#include <cassert>
#include <algorithm>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaSphereController::VfaSphereController(VfaSphereModel *pModel, 
										   VflRenderNode *pRenderNode)
:	IVfaWidgetController(pRenderNode),
	m_pModel(pModel),
	m_iInteractionButton(0),
	m_pFocusHandle(NULL),
	m_pXform(new VistaIndirectXform),
	m_iCurrentInternalState(VfaSphereController::SCS_NONE),
	m_bEnabled(true)
{
	m_pCtlProps = new VfaSphereController::VfaSphereControllerProps(this);
	
	// adding two sphere handles, one for the center, the other one as surfacePt
	m_vecHandles.resize(2);

	m_vecHandles[HND_SPHERE_CENTER] = new VfaSphereHandle(pRenderNode);
	m_vecHandles[HND_SPHERE_CENTER]->SetNormalColor(VistaColor(0.0f, 0.0f, 1.0f, 1.0f));
	m_vecHandles[HND_SPHERE_CENTER]->SetHighlightColor(VistaColor(0.75f, 0.75f, 1.0f, 1.0f));
	this->AddControlHandle(m_vecHandles[HND_SPHERE_CENTER]);

	m_vecHandles[HND_SPHERE_SURFACE] = new VfaSphereHandle(pRenderNode);
	m_vecHandles[HND_SPHERE_SURFACE]->SetNormalColor(VistaColor(1.0f, 0.0f, 0.0f, 1.0f));
	m_vecHandles[HND_SPHERE_SURFACE]->SetHighlightColor(VistaColor(1.0f, 0.75f, 0.75f, 1.0f));
	this->AddControlHandle(m_vecHandles[HND_SPHERE_SURFACE]);
}

VfaSphereController::~VfaSphereController()
{
	delete m_pCtlProps;
	delete m_pXform;

	for(int unsigned i=0; i < m_vecHandles.size(); ++i)
	{
		delete m_vecHandles[i];
	}

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaSphereController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	for(unsigned int i=0; i < m_vecHandles.size(); ++i)
	{
		m_vecHandles[i]->SetVisible(m_bEnabled);
		m_vecHandles[i]->SetEnable(m_bEnabled);
	}

}
bool VfaSphereController::GetIsEnabled() const
{
	return m_bEnabled;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnFocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaSphereController::OnFocus(IVfaControlHandle* pHandle)
{
	// don't allow to change focus if some handle already has it
	if(m_iCurrentInternalState == SCS_NONE)
	{
		assert(dynamic_cast<VfaSphereHandle*>(pHandle));

		m_pFocusHandle = static_cast<VfaSphereHandle*>(pHandle);

		// search the focused handle(in the vector)
		m_iHandle =(int)(find(m_vecHandles.begin(), m_vecHandles.end(), m_pFocusHandle) - m_vecHandles.begin());

		m_pFocusHandle->SetIsHighlighted(true);
			
		if(m_iHandle == 0)
			m_iInternalDoState = SCS_MOVE;
		else if(m_iHandle == 1)
			m_iInternalDoState = SCS_RESIZE;
	}

	Notify( MSG_FOCUS_RECEIVED );
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUnfocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaSphereController::OnUnfocus()
{
	// don't allow to change focus if some handle already has it
	if(m_iCurrentInternalState == SCS_NONE)
	{
		if(m_pFocusHandle)
		{
			m_pFocusHandle->SetIsHighlighted(false);
		}
		m_iHandle = -1;
	}

	Notify( MSG_FOCUS_LOST );
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaSphereController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUntouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaSphereController::OnUntouch()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaSphereController::OnSensorSlotUpdate(int iSlot, 
				const VfaApplicationContextObject::sSensorFrame & oFrameData)
{

	if(iSlot == VfaApplicationContextObject::SLOT_POINTER_VIS)
	{
		// save sensor data
		m_oLastSensorFrame = oFrameData;

		// if we have a DO(here especially: move or resize) state, do something....
		if(m_iCurrentInternalState != SCS_NONE)
		{
			VistaVector3D v3MyPos;  // vis space
			VistaQuaternion qMyOri; // vis space
			m_pXform->Update(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation, 
				v3MyPos, qMyOri);

			// center was grabbed --> move widget
			if(m_iInternalDoState == SCS_MOVE)
			{
				m_pModel->SetCenter(v3MyPos);
				m_pModel->SetRotation(qMyOri);
			}
			// surfacePt was grabbed --> resize widget
			else if(m_iInternalDoState == SCS_RESIZE)
			{
					// resize sphere
					VistaVector3D vecCenter;
					m_pModel->GetCenter(vecCenter);	
					float fRadius =(vecCenter-v3MyPos).GetLength();
					m_pModel->SetRadius(fRadius);

					// rotate sphere representation
					VistaVector3D v3MyPos;  // vis space
					VistaQuaternion qMyOri; // vis space
					m_pXform->Update(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation, 
						v3MyPos, qMyOri);
					m_pModel->SetRotation(qMyOri);			
			}
		}
	}
	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaSphereController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{

	if(bSet)
	{
		// we can only be grabbed, if we have the focus
		if(this->GetControllerState() == CS_FOCUS)
		{
			// all(!) commands will be handled in the same way
			// if you grabb the center, you can move the widget
			// if you grab the surfacePt, you can change the radius of your widget
			// just choose the right handle and init the xform with its center position.
			int iHandle =(m_iInternalDoState == SCS_MOVE ? 0 : 1);
			
			VistaQuaternion qRot;
			VistaVector3D v3Center;
			m_vecHandles[iHandle]->GetCenter(v3Center);
			m_pModel->GetRotation(qRot);
			m_pXform->Init(	m_oLastSensorFrame.v3Position, 
							m_oLastSensorFrame.qOrientation,	
							v3Center, qRot);
			
			m_iCurrentInternalState = SCS_DO;
		}
	}
	else
	{
		// if the slot is released, goto state NONE
		m_iCurrentInternalState = SCS_NONE;

		// and if we have lost focus during other states, now react on that
		if(this->GetControllerState()!=CS_FOCUS)
		{
			if(m_pFocusHandle)
				m_pFocusHandle->SetIsHighlighted(false);
			m_iHandle = -1;

		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*                                                                            */
/*============================================================================*/
void VfaSphereController::OnTimeSlotUpdate(const double dTime)
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSensorMask			                                      */
/*                                                                            */
/*============================================================================*/
int VfaSphereController::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_POINTER_VIS);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCommandMask			                                  */
/*                                                                            */
/*============================================================================*/
int VfaSphereController::GetCommandMask() const
{
	return(1 << m_iInteractionButton);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeUpdate				                                  */
/*                                                                            */
/*============================================================================*/
bool VfaSphereController::GetTimeUpdate() const
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaSphereController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaSphereModel *pSphereModel = dynamic_cast<VfaSphereModel*>(pObserveable);

	if(!pSphereModel)
		return;

	// get data from model
	VistaVector3D v3Center;
	VistaQuaternion qOri;
	pSphereModel->GetCenter(v3Center);
	pSphereModel->GetRotation(qOri);
	
	// rotate outer sphere handle on sphere representation
	VistaVector3D v3SurfacePt(1.0f, 0.0f, 0.0f);
	v3SurfacePt = qOri.Rotate(v3SurfacePt);
	v3SurfacePt = pSphereModel->GetRadius() * v3SurfacePt + v3Center;

	// reposition handles
	this->UpdateHandles(v3Center, v3SurfacePt);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaSphereController::VfaSphereControllerProps* VfaSphereController::GetProperties() const
{
	return m_pCtlProps;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateHandles                                               */
/*                                                                            */
/*============================================================================*/
void VfaSphereController::UpdateHandles(const VistaVector3D& v3Center, 
										 const VistaVector3D& v3SurfacePt)
{
	// set handle's position
	m_vecHandles[HND_SPHERE_CENTER]->SetCenter(v3Center); 
	m_vecHandles[HND_SPHERE_SURFACE]->SetCenter(v3SurfacePt);
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaSphereControllerProps                             */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaSphereController::VfaSphereControllerProps");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<float, VfaSphereController::VfaSphereControllerProps>(
	"HANDLE_RADIUS", STR_REF_TYPENAME,
	&VfaSphereController::VfaSphereControllerProps::GetHandleRadius),
	NULL
};

//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<float, float, VfaSphereController::VfaSphereControllerProps>(
		"HANDLE_RADIUS", STR_REF_TYPENAME,
		&VfaSphereController::VfaSphereControllerProps::SetHandleRadius),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaSphereController::VfaSphereControllerProps>(
		"HANDLE_NORMAL_COLOR", STR_REF_TYPENAME,
		&VfaSphereController::VfaSphereControllerProps::SetResizeHandleNormalColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaSphereController::VfaSphereControllerProps>(
		"HANDLE_HIGHLIGHT_COLOR", STR_REF_TYPENAME,
		&VfaSphereController::VfaSphereControllerProps::SetResizeHandleHighlightColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaSphereController::VfaSphereControllerProps>(
		"TRANSLATION_HANDLE_NORMAL_COLOR", STR_REF_TYPENAME,
		&VfaSphereController::VfaSphereControllerProps::SetTranslationHandleNormalColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaSphereController::VfaSphereControllerProps>(
		"TRANSLATION_HANDLE_HIGHLIGHT_COLOR", STR_REF_TYPENAME,
		&VfaSphereController::VfaSphereControllerProps::SetTranslationHandleHighlightColor),
	NULL 
};
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaSphereController::VfaSphereControllerProps::VfaSphereControllerProps(VfaSphereController *pSphereCtrl)
: m_pSphereCtrl(pSphereCtrl)
{}

VfaSphereController::VfaSphereControllerProps::~VfaSphereControllerProps()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleRadius                                         */
/*                                                                            */
/*============================================================================*/
bool VfaSphereController::VfaSphereControllerProps::SetHandleRadius(float fRadius)
{
	if(fRadius != m_pSphereCtrl->m_vecHandles[0]->GetRadius())
	{
		for(int i=0; i<2; ++i)
		{
			m_pSphereCtrl->m_vecHandles[i]->SetRadius(fRadius);
		}
		Notify();
		return true;
	}
	return false;
}

float VfaSphereController::VfaSphereControllerProps::GetHandleRadius() const
{
	return m_pSphereCtrl->m_vecHandles[0]->GetRadius();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetResizeHandleNormalColor                              */
/*                                                                            */
/*============================================================================*/
bool VfaSphereController::VfaSphereControllerProps::SetResizeHandleNormalColor(
	const VistaColor& color)
{
	if (m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->GetNormalColor() == color)
		return false;

	float fColors[4];
	color.GetValues(fColors, VistaColor::RGBA);
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->SetNormalColor(fColors);

	return true;
}

VistaColor VfaSphereController::VfaSphereControllerProps::GetResizeHandleNormalColor() const
{
	float fColors[4];
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->GetNormalColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaSphereController::VfaSphereControllerProps::SetResizeHandleNormalColor(float fC[4])
{
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->SetNormalColor(fC);
}

void VfaSphereController::VfaSphereControllerProps::GetResizeHandleNormalColor(float fC[4]) const
{
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->GetNormalColor(fC);
}




/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetResizeHandleHighlightColor                           */
/*                                                                            */
/*============================================================================*/
bool VfaSphereController::VfaSphereControllerProps::SetResizeHandleHighlightColor(
	const VistaColor& color)
{
	if (m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->GetHighlightColor() == color)
		return false;

	float fColors[4];
	color.GetValues(fColors, VistaColor::RGBA);
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->SetHighlightColor(fColors);

	return true;
}

VistaColor VfaSphereController::VfaSphereControllerProps::GetResizeHandleHighlightColor() const
{
	float fColors[4];
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->GetHighlightColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaSphereController::VfaSphereControllerProps::SetResizeHandleHighlightColor(float fC[4])
{
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->SetHighlightColor(fC);
}

void VfaSphereController::VfaSphereControllerProps::GetResizeHandleHighlightColor(float fC[4]) const
{
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->GetHighlightColor(fC);
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTranslationHandleNormalColor                         */
/*                                                                            */
/*============================================================================*/
bool VfaSphereController::VfaSphereControllerProps::SetTranslationHandleNormalColor(
	const VistaColor& color)
{
	if (m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->GetNormalColor() == color)
		return false;

	float fColors[4];
	color.GetValues(fColors, VistaColor::RGBA);
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->SetNormalColor(fColors);

	return true;
}

VistaColor VfaSphereController::VfaSphereControllerProps::GetTranslationHandleNormalColor() const
{
	float fColors[4];
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->GetNormalColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaSphereController::VfaSphereControllerProps::SetTranslationHandleNormalColor(float fC[4])
{
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->SetNormalColor(fC);
}

void VfaSphereController::VfaSphereControllerProps::GetTranslationHandleNormalColor(float fC[4]) const
{
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->GetNormalColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTranslationHandleNormalColor                         */
/*                                                                            */
/*============================================================================*/
bool VfaSphereController::VfaSphereControllerProps::SetTranslationHandleHighlightColor(
	const VistaColor& color)
{
	if (m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->GetHighlightColor() == color)
		return false;

	float fColors[4];
	color.GetValues(fColors, VistaColor::RGBA);
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->SetHighlightColor(fColors);
	
	return true;
}

VistaColor VfaSphereController::VfaSphereControllerProps::GetTranslationHandleHighlightColor() const
{
	float fColors[4];
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->GetHighlightColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaSphereController::VfaSphereControllerProps::SetTranslationHandleHighlightColor(float fC[4])
{
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->SetHighlightColor(fC);
}

void VfaSphereController::VfaSphereControllerProps::GetTranslationHandleHighlightColor(float fC[4]) const
{
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->GetHighlightColor(fC);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaSphereController::VfaSphereControllerProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaSphereController::VfaSphereControllerProps::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}






void VfaSphereController::VfaSphereControllerProps::SetResizeHandleVisible(bool bVisible)
{
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->SetVisible(bVisible);
}

bool VfaSphereController::VfaSphereControllerProps::GetResizeHandleVisible() const
{
	return m_pSphereCtrl->m_vecHandles[HND_SPHERE_SURFACE]->GetVisible();
}

void VfaSphereController::VfaSphereControllerProps::SetTranslationHandleVisible(bool bVisible)
{
	m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->SetVisible(bVisible);
}

bool VfaSphereController::VfaSphereControllerProps::GetTranslationHandleVisible() const
{
	return m_pSphereCtrl->m_vecHandles[HND_SPHERE_CENTER]->GetVisible();
}



/*============================================================================*/
/*  END OF FILE "VfaSphereController.cpp"                                     */
/*============================================================================*/
