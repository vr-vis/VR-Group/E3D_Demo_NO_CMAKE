/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFASPHEREMODEL_H
#define _VFASPHEREMODEL_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidget.h"
#include "../VfaWidgetModelBase.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaEventManager;
class VistaNewInteractionManager;
class VflVisController;
class VistaSceneGraph;
class VfaSphereVis;
class VfaSphereController;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 *	Please pay ATTENTION:
 *	scale, rotation and translation are all given in vis space
 *	sphere widget points(center, surfacePt) are given in widget space
 *
 *	conversion-matrix: M = S*T (S = Scale, T = Translation and Rotation)
 *	conversion from vis to widget space: M.GetInverted().Transform(....)
 *	conversion from widget to vis space: M.Transfrom(....)
 *
 */
class VISTAFLOWLIBAUXAPI VfaSphereModel : public VfaWidgetModelBase
{
public:
	enum{
		MSG_CENTER_CHANGE,
		MSG_ORIENTATION_CHANGE,
		MSG_RADIUS_CHANGE,
		MSG_COLOR_CHANGE,
		MSG_OFFSET_CHANGE,
		MSG_LAST
	};


	VfaSphereModel();
	virtual ~VfaSphereModel();

	bool SetCenter(float fC[3]);
	bool SetCenter(double dC[3]);
	bool SetCenter(const VistaVector3D &v3C);

	void GetCenter(float fC[3]) const;
	void GetCenter(double dC[3]) const;
	void GetCenter(VistaVector3D &v3C) const;
	VistaVector3D GetCenter() const;
	
	bool SetRotation(const VistaQuaternion &qRotation);
	void GetRotation (VistaQuaternion &qRot) const;
	VistaQuaternion GetRotation () const;

	bool SetRadius(const float f);
	float GetRadius() const;


	virtual std::string GetReflectionableType() const;


protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;


private:
	VistaVector3D m_v3Center;
	VistaQuaternion m_qRotation;
	float m_fRadius;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASPHEREMODEL_H
