/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VfaFollowSphereController_H
#define _VfaFollowSphereController_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetController.h"

#include <VistaBase/VistaVectorMath.h>

#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaSphereHandle;
class VistaColor;
class VistaIndirectXform;
class VfaSphereModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Controll the behavior
 */
class VISTAFLOWLIBAUXAPI VfaFollowSphereController : public VflObserver, public IVfaWidgetController
{
public:
	VfaFollowSphereController(VfaSphereModel *pModel,
		VflRenderNode *pRenderNode);
	virtual ~VfaFollowSphereController();

	/** 
	 * This is called on transition change into(out of) state FOCUS
	 * BUT: this widget is always grabbed, so always in focus
	 *		so we needn't to implement those methods
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();

	/**
	 * This is called on transition change into(out of) state TOUCH
	 */
	void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	void OnUntouch();

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	void OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate(int iSlot, const bool bSet);
	void OnTimeSlotUpdate(const double dTime);
	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;


// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);


	class VISTAFLOWLIBAUXAPI VfaFollowSphereControllerProps : public IVistaReflectionable
	{
		friend class VfaFollowSphereController;
	public:
		
		VfaFollowSphereControllerProps(VfaFollowSphereController *pSphereCtrl);
		virtual ~VfaFollowSphereControllerProps();

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;


	private:
		VfaFollowSphereController	*m_pSphereCtrl;

	};


	VfaFollowSphereControllerProps* GetProperties() const;

protected:

	void UpdateHandles(const VistaVector3D &v3Min, const VistaVector3D &v3Max);

private:
	VfaSphereModel				*m_pModel;
	VistaIndirectXform			*m_pXform;
	VfaFollowSphereControllerProps		*m_pCtlProps;

	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;

	bool						m_bEnabled;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaFollowSphereController_H
