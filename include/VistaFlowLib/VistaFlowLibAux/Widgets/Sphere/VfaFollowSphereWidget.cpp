/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaFollowSphereWidget.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaFollowSphereWidget::VfaFollowSphereWidget(VflRenderNode *pRenderNode)
:	IVfaWidget(pRenderNode),
	m_bEnabled(true),
	m_pSphereModel(new VfaSphereModel)
{
	m_pSphereView = new VfaSphereVis(m_pSphereModel);
	if( m_pSphereView->Init())
		pRenderNode->AddRenderable(m_pSphereView);

	float fC[4] = {0.0f, 0.0f, 1.0f, 1.0f};
	m_pSphereView->GetProperties()->SetColor(fC);
	m_pSphereView->GetProperties()->SetUseLighting(false);
	
	m_pFollowSphereController =
		new VfaFollowSphereController(m_pSphereModel, pRenderNode);
	m_pFollowSphereController->Observe(m_pSphereModel);
	
	this->GetModel()->Notify();
}

VfaFollowSphereWidget::~VfaFollowSphereWidget()
{
	m_pSphereView->GetRenderNode()->RemoveRenderable(m_pSphereView);
	delete m_pSphereView;

	delete m_pFollowSphereController;
	delete m_pSphereModel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereWidget::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pFollowSphereController->SetIsEnabled(m_bEnabled);
}
bool VfaFollowSphereWidget::GetIsEnabled() const
{
	return m_bEnabled;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsVisible                                            */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereWidget::SetIsVisible(bool b)
{
	m_bVisible = b;
	m_pSphereView->SetVisible(m_bVisible);
}
bool VfaFollowSphereWidget::GetIsVisible() const
{
	return m_bVisible;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel                                                    */
/*                                                                            */
/*============================================================================*/
VfaSphereModel* VfaFollowSphereWidget::GetModel() const
{
	return m_pSphereModel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetView                                                     */
/*                                                                            */
/*============================================================================*/
VfaSphereVis* VfaFollowSphereWidget::GetView() const
{
	return m_pSphereView;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetController                                               */
/*                                                                            */
/*============================================================================*/
VfaFollowSphereController* VfaFollowSphereWidget::GetController() const
{
	return m_pFollowSphereController;
}
/*============================================================================*/
/*  END OF FILE "VfaFollowSphereWidget.cpp"                                   */
/*============================================================================*/
