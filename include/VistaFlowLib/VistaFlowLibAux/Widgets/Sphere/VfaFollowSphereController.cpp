/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES																  */
/*============================================================================*/
#include "VfaFollowSphereController.h"

#include "VfaSphereModel.h"
#include "VfaSphereWidget.h"
#include "../VfaSphereHandle.h"

#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaMath/VistaIndirectXform.h>

#include <cassert>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaFollowSphereController::VfaFollowSphereController(VfaSphereModel *pModel,
	VflRenderNode *pRenderNode)
:	IVfaWidgetController(pRenderNode),
	m_pModel(pModel),
	m_pXform(new VistaIndirectXform),
	m_bEnabled(true)
{
	m_pCtlProps = new VfaFollowSphereController::VfaFollowSphereControllerProps(this);
}

VfaFollowSphereController::~VfaFollowSphereController()
{
	delete m_pCtlProps;
	delete m_pXform;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
}
bool VfaFollowSphereController::GetIsEnabled() const
{
	return m_bEnabled;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnFocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereController::OnFocus(IVfaControlHandle* pHandle)
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUnfocus			                                          */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereController::OnUnfocus()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereController::OnTouch(
	const std::vector<IVfaControlHandle*>& vecHandles)
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnUntouch			                                          */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereController::OnUntouch()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereController::OnSensorSlotUpdate(int iSlot, 
				const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	// only update the model, if the sphere is enabled
	if (iSlot == VfaApplicationContextObject::SLOT_CURSOR_VIS && this->GetIsEnabled())
	{
		// save sensor data
		m_oLastSensorFrame = oFrameData;

		VistaVector3D v3MyPos;  // vis space
		VistaQuaternion qMyOri; // vis space
		m_pXform->Update(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation, v3MyPos, qMyOri);

		m_pModel->SetCenter(v3MyPos);
	}
	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	VistaQuaternion qRot;
	VistaVector3D v3Center;
	m_pModel->GetCenter(v3Center);

	m_pXform->Init(	m_oLastSensorFrame.v3Position, 
					m_oLastSensorFrame.qOrientation,	
					v3Center, qRot);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereController::OnTimeSlotUpdate(const double dTime)
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSensorMask			                                      */
/*                                                                            */
/*============================================================================*/
int VfaFollowSphereController::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_CURSOR_VIS);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCommandMask			                                  */
/*                                                                            */
/*============================================================================*/
int VfaFollowSphereController::GetCommandMask() const
{
	// default value --> to recognize that something is wrong
	return -1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeUpdate				                                  */
/*                                                                            */
/*============================================================================*/
bool VfaFollowSphereController::GetTimeUpdate() const
{
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	VfaSphereModel *pSphereModel = dynamic_cast<VfaSphereModel*>(pObserveable);

	if(!pSphereModel)
		return;

	// get data from model
	VistaVector3D v3Center;
	pSphereModel->GetCenter(v3Center);
	
	VistaVector3D v3SurfacePt(1.0f, 0.0f, 0.0f);
	v3SurfacePt = pSphereModel->GetRadius() * v3SurfacePt + v3Center;

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaFollowSphereController::VfaFollowSphereControllerProps* VfaFollowSphereController::GetProperties() const
{
	return m_pCtlProps;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateHandles                                               */
/*                                                                            */
/*============================================================================*/
void VfaFollowSphereController::UpdateHandles(const VistaVector3D& v3Center, 
										 const VistaVector3D& v3SurfacePt)
{}

/*============================================================================*/
/*  IMPLEMENTATION      VfaFollowSphereControllerProps                       */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaFollowSphereController::VfaFollowSphereControllerProps");

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaFollowSphereController::VfaFollowSphereControllerProps::VfaFollowSphereControllerProps(VfaFollowSphereController *pSphereCtrl)
: m_pSphereCtrl(pSphereCtrl)
{}

VfaFollowSphereController::VfaFollowSphereControllerProps::~VfaFollowSphereControllerProps()
{}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaFollowSphereController::VfaFollowSphereControllerProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaFollowSphereController::VfaFollowSphereControllerProps::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  END OF FILE																  */
/*============================================================================*/
