/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFASPHEREWIDGET_H
#define _VFASPHEREWIDGET_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidget.h"
#include "VfaSphereModel.h"
#include "VfaSphereController.h"
#include "../VfaSphereVis.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaSphereModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*
 * A sphere widget will be composed here, namely a wirecube and
 * two spheres. Sphere controller controlls the whole interactions between user
 * and widget. Vis controller is responsible for drawing the scene. Event router 
 * catches incoming events and forward them to sphere controller. Visible properties
 * are all contained in widget property.
 */
class VISTAFLOWLIBAUXAPI VfaSphereWidget : public IVfaWidget
{
public:
	VfaSphereWidget(VflRenderNode *pRenderNode);
	virtual ~VfaSphereWidget();

	/**
	 * Only react on InteractionEvents if enabled. Visibility of the widget is
	 * independent of this.
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;


	VfaSphereModel* GetModel() const;

	VfaSphereVis* GetView() const;

	VfaSphereController* GetController() const;

protected:

private:
	VfaSphereModel			*m_pSphereModel;
	VfaSphereVis			*m_pSphereView;
	VfaSphereController	*m_pSphereController;
	bool					 m_bEnabled;
	bool					 m_bVisible;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASPHEREWIDGET_H
