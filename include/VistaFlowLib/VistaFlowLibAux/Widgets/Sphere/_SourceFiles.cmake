

set( RelativeDir "./Widgets/Sphere" )
set( RelativeSourceGroup "Source Files\\Widgets\\Sphere" )

set( DirFiles
	VfaFollowSphereController.cpp
	VfaFollowSphereController.h
	VfaFollowSphereWidget.cpp
	VfaFollowSphereWidget.h
	VfaSphereController.cpp
	VfaSphereController.h
	VfaSphereModel.cpp
	VfaSphereModel.h
	VfaSphereWidget.cpp
	VfaSphereWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

