/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAWIDGETMANAGER_H
#define _VFAWIDGETMANAGER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <list>
#include <map>
#include <set>
#include <vector>
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaFocusStrategy;
class IVfaWidget;
class VfaApplicationContextObject;
class IVfaControlHandle;
class IVfaWidgetController;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaWidgetManager : public VflObserver
{
public:

	VfaWidgetManager(VfaApplicationContextObject* pAppContext);
	virtual ~VfaWidgetManager();

	/**
	 * 
	 */
	void SetFocusStrategy(IVfaFocusStrategy *pFocusStrategy);
	
	/**
	 *
	 */
	IVfaFocusStrategy* GetFocusStrategy() const;
	/**
	 * 
	 */
	void RegisterWidget(IVfaWidget *pWidget);
	
	/**
	 * 
	 */
	void DeregisterWidget(IVfaWidget *pWidget);

	// Observer interface
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/**
	 * 
	 */
	void SetEnabled (bool bEnabled);
	bool GetIsEnabled () const;

	IVfaControlHandle*	GetCurrentHandle() const;

protected:

	/**
	 * 
	 */
	void EvaluateFocus();

	/**
	 * 
	 */
	void OnSensorSlotChange();

	/**
	 * 
	 */
	void OnCommandSlotChange();

	/**
	 * 
	 */
	void OnTimeSlotChange();
private:
	/*
	* Remember which controller should be informed on which handle touch/focus
	*
	*/
	std::map<IVfaControlHandle*, IVfaWidgetController*> m_mapHandle2Ctrl;

	/*
	* All widget controllers
	*/
	std::list<IVfaWidgetController*>	m_listWidgetCtrls;

	IVfaControlHandle*					m_pFocusedHandle;
	std::vector<IVfaControlHandle*>		m_vecCurrentTouchedHandles;

	IVfaFocusStrategy					*m_pFocusStrategy;

	VfaApplicationContextObject			*m_pApplicationContext;

	bool								m_bEnabled;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFAWIDGETMANAGER_H
