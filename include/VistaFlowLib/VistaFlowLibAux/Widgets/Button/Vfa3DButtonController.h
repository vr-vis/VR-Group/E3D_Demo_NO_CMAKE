/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFA3DBUTTONCONTROLLER_H
#define _VFA3DBUTTONCONTROLLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// widget
#include <VistaFlowLibAux/Widgets/VfaWidget.h>

// model
#include <VistaFlowLibAux/Widgets/VfaTransformableWidgetModel.h>

// vis
#include <VistaFlowLib/Visualization/VflRenderable.h>

//controller
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>
#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaBase/VistaVectorMath.h>
#include <string>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class IVistaExplicitCallbackInterface;
class Vfa3DButtonWidget;
class Vfa3DButtonVis;
class IVfaCenterControlHandle;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * 
 */
class VISTAFLOWLIBAUXAPI Vfa3DButtonController :	public VflObserver,
													public IVfaWidgetController
{
public:
	Vfa3DButtonController(Vfa3DButtonWidget *pWidget,
		VflRenderNode *pRenderNode);
	virtual ~Vfa3DButtonController();

	
	/** 
	 * This is called on transition change into(out of) state FOCUS
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();

	/**
	 * This is called on transition change into(out of) state TOUCH
	 */
	void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	void OnUntouch();

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	void OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate(int iSlot, const bool bSet);
	void OnTimeSlotUpdate(const double dTime);
	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetButtonDown( bool b );
	void SetButtonUp( bool b );
	void Click();

	/**
     * access to grab slot i.e. the application context command slot that has to be set in order
	 * to grab the widget.
	 */
	int GetPressSlot() const;
	void SetPressSlot(int i);

	void SetOrientToViewer(bool bToViewer);
	bool GetOrientToViewer() const;

// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

protected:
	
private:
	Vfa3DButtonWidget		*m_pWidget;
	Vfa3DButtonVis			*m_pView;

	IVfaCenterControlHandle	*m_pCenterHandle;

	bool					m_bPressed;
	int						m_iPressSlot;

	bool					m_bOrientToViewer;
	VfaApplicationContextObject::sSensorFrame m_oViewerFrame;
	VistaVector3D			m_v3Right;
	VistaVector3D			m_v3Up;
	VistaVector3D			m_v3OppView;
	VistaVector3D			m_v3Y;
	VistaReferenceFrame	m_oViewerRefFrame;


	bool					m_bEnabled;

};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFA3DBUTTONCONTROLLER_H



