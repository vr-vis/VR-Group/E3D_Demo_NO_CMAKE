/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "Vfa3DButtonWidget.h" 

#include <VistaFlowLib/Visualization/VflRenderNode.h>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/


/*============================================================================*/
/* WIDGET					                                                  */
/*============================================================================*/
Vfa3DButtonWidget::Vfa3DButtonWidget(VflRenderNode *pRenderNode,
									   const std::string & sFilename,
									   const std::string & sSecondFilename)
:	IVfaWidget(pRenderNode),
	m_pVis(new Vfa3DButtonVis(sFilename, sSecondFilename)),
	m_pModel(new Vfa3DButtonModel(sSecondFilename!="")),
	m_bEnabled(true)
{
	m_pVis->Init();
	pRenderNode->AddRenderable(m_pVis);

	m_pWidgetCtl = new Vfa3DButtonController(this, pRenderNode);

	m_pVis->Observe(GetModel());
	GetModel()->Notify();

}

Vfa3DButtonWidget::Vfa3DButtonWidget( VflRenderNode *pRenderNode, VistaTexture *pFirst, VistaTexture *pSecond /*= NULL*/ )
:	IVfaWidget(pRenderNode),
	m_pVis(new Vfa3DButtonVis(pFirst, pSecond)),
	m_pModel(new Vfa3DButtonModel(pSecond != NULL)),
	m_bEnabled(true)
{
	m_pVis->Init();
	pRenderNode->AddRenderable(m_pVis);

	m_pWidgetCtl = new Vfa3DButtonController(this, pRenderNode);

	m_pVis->Observe(GetModel());
	GetModel()->Notify();
}

Vfa3DButtonWidget::~Vfa3DButtonWidget()
{
	delete m_pWidgetCtl;
	delete m_pVis;
	delete m_pModel;

}


void Vfa3DButtonWidget::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pWidgetCtl->SetIsEnabled(m_bEnabled);
}

bool Vfa3DButtonWidget::GetIsEnabled() const
{
	return m_bEnabled;
}

void Vfa3DButtonWidget::SetIsVisible(bool b)
{
	m_bVisible = b;
	m_pVis->SetVisible(m_bVisible);
}

bool Vfa3DButtonWidget::GetIsVisible() const
{
	return m_bVisible;
}

	
Vfa3DButtonModel *Vfa3DButtonWidget::GetModel() const
{
	return m_pModel;
}
Vfa3DButtonVis* Vfa3DButtonWidget::GetView() const
{
	return m_pVis;
}
Vfa3DButtonController* Vfa3DButtonWidget::GetController() const
{
	return m_pWidgetCtl;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "          "                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

