/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFA3DBUTTONVIS_H
#define _VFA3DBUTTONVIS_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

// vis
#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <VistaBase/VistaVectorMath.h>

#include <string>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisController;
class Vfa3DButtonWidget;
class VistaTexture;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI Vfa3DButtonVis : public IVflRenderable
{
public: 
	Vfa3DButtonVis(const std::string & sFilename, const std::string & sSecondFilename);
	Vfa3DButtonVis(VistaTexture *pTexFirst, VistaTexture *pTexSecond);
	virtual ~Vfa3DButtonVis();
	
	bool Init();

	//Here the rendering is done
	virtual void DrawOpaque();

	//the crosshair only registers for opaque drawing since it does not have transparent components
	virtual unsigned int GetRegistrationMode() const;


	void SetIsHighlighted(bool b);
	bool GetIsHighlighted() const;

	/**
	 * redefine GetBounds in order to indicate that the widget should not be included
	 * in the computation of vis bounds(simply mark our bounds as invalid)
	 */
	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);	

	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

protected:
	virtual VflRenderableProperties* CreateProperties() const;


private:
	VistaVector3D   m_vCenter;
	VistaQuaternion m_qOrientation;
	float			m_fScale;

	//unsigned char* m_pTextureData;
	//GLuint		   m_iTextureID;
	std::string    m_sTextureFile;
	VistaTexture  *m_pTexture;

	//unsigned char* m_pSecondTextureData;
	//GLuint		   m_iSecondTextureID;
	std::string    m_sSecondTextureFile;
	VistaTexture  *m_pSecondTexture;

	bool m_bSecondTextureActive;
	bool m_bIsHighlighted;

};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFA3DBUTTONVIS_H



