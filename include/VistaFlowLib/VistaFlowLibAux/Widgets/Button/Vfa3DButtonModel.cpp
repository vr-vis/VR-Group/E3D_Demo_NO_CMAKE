/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here
#include <GL/glew.h>

#include "Vfa3DButtonModel.h" 


// callbacks
#include <VistaAspects/VistaExplicitCallbackInterface.h>
#include <string>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/


/*============================================================================*/
/* WIDGET MODEL				                                                  */
/*============================================================================*/
static const std::string STR_REF_TYPENAME("CVtn3DButton");

Vfa3DButtonModel::Vfa3DButtonModel(bool bHasSecondState)
:VfaTransformableWidgetModel(),
m_pCallback(NULL),
m_pSecondCallback(NULL),
m_bPressed(false),
m_bHasSecondState(bHasSecondState),
m_bSecondStateActive(false)
{
}
Vfa3DButtonModel::~Vfa3DButtonModel()
{
}

bool Vfa3DButtonModel::SetButtonPressed(bool bPressed)
{
	// state change from not pressed to pressed
	if((!m_bPressed) && bPressed)
	{
		if (m_bSecondStateActive && m_pSecondCallback)
			m_pSecondCallback->Do();
		else
		if (m_pCallback)
			m_pCallback->Do();

		if (m_bHasSecondState)
		{
			m_bSecondStateActive = !m_bSecondStateActive;
			Notify (MSG_STATE_CHANGE);
		}

	}

	m_bPressed = bPressed;

	return true;
}

bool Vfa3DButtonModel::GetButtonPressed() const
{
	return m_bPressed;
}

bool Vfa3DButtonModel::SetCallback(IVistaExplicitCallbackInterface* pCallback)
{
	m_pCallback = pCallback;
	return true;
}

IVistaExplicitCallbackInterface* Vfa3DButtonModel::GetCallback() const
{
	return m_pCallback;
}

bool Vfa3DButtonModel::GetSecondStateActive () const
{
	return m_bSecondStateActive;
}

bool Vfa3DButtonModel::SetSecondStateActive(bool bActive)
{
	m_bSecondStateActive = bActive;
	return true;
}

bool Vfa3DButtonModel::SetSecondCallback(IVistaExplicitCallbackInterface* pCallback)
{
	m_pSecondCallback = pCallback;
	return true;
}

IVistaExplicitCallbackInterface* Vfa3DButtonModel::GetSecondCallback() const
{
	return m_pSecondCallback;
}
std::string Vfa3DButtonModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}


int Vfa3DButtonModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

