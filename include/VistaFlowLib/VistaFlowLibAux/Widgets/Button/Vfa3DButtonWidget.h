/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFA3DBUTTONWIDGET_H
#define _VFA3DBUTTONWIDGET_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// widget
#include <VistaFlowLibAux/Widgets/VfaWidget.h>
#include "Vfa3DButtonModel.h" 
#include "Vfa3DButtonVis.h" 
#include "Vfa3DButtonController.h" 

#include <VistaBase/VistaVectorMath.h>
#include <string>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VistaTexture;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI Vfa3DButtonWidget : public IVfaWidget
{
public:
	
	Vfa3DButtonWidget(VflRenderNode *pRenderNode,
		const std::string & sFilename, const std::string & sSecondFilename = "");
	
	//! @param   pRenderNode parent render node
	//! @param   pFirst  texture for the button
	//! @param   pSecond second-state texture for the button (optional)
	Vfa3DButtonWidget(VflRenderNode *pRenderNode,
		VistaTexture *pFirst, VistaTexture *pSecond = NULL);
	virtual ~Vfa3DButtonWidget();

	/**
	 * enable/disable widget
	 * a disabled widget will not handle events AND will not be visible!
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;

	
	/**
	 * retrieve the actual visual representation
	 */

	Vfa3DButtonModel *GetModel() const;

	Vfa3DButtonVis* GetView() const;

	Vfa3DButtonController* GetController() const;

private:
	Vfa3DButtonController  *m_pWidgetCtl;
	Vfa3DButtonVis			*m_pVis;
	Vfa3DButtonModel		*m_pModel;
	bool					m_bEnabled;
	bool					m_bVisible;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFA3DBUTTONWIDGET_H



