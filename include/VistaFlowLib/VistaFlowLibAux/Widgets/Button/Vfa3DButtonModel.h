/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFA3DBUTTONMODEL_H
#define _VFA3DBUTTONMODEL_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

// model
#include <VistaFlowLibAux/Widgets/VfaTransformableWidgetModel.h>

#include <string>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVistaExplicitCallbackInterface;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * 
 */
class VISTAFLOWLIBAUXAPI Vfa3DButtonModel : public VfaTransformableWidgetModel
{
	friend class CVtn3DButtonWidget;
public:
	/**
	* messages sent by this reflectionable upon property changes
	* 	MSG_BUTTON_CHANGE  the button got selected
	*/
	enum{
		MSG_BUTTON_CHANGE = VfaTransformableWidgetModel::MSG_LAST,
		MSG_STATE_CHANGE,
		MSG_LAST
	};

	Vfa3DButtonModel(bool bHasSecondState);
	virtual ~Vfa3DButtonModel();

	/**
	* SetButtonPressed
	* 
	*/
	bool SetButtonPressed(bool bPressed);
	bool GetButtonPressed() const;

	/**
	* SetSecondStateActive
	* 
	*/
	bool GetSecondStateActive () const;
	bool SetSecondStateActive(bool bActive);

	/**
	* Callbacks
	* 
	*/
	bool SetCallback(IVistaExplicitCallbackInterface* pCallback);
	IVistaExplicitCallbackInterface* GetCallback() const;
	bool SetSecondCallback(IVistaExplicitCallbackInterface* pCallback);
	IVistaExplicitCallbackInterface* GetSecondCallback() const;

	virtual std::string GetReflectionableType() const;


protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;


private:
	IVistaExplicitCallbackInterface* m_pCallback;
	IVistaExplicitCallbackInterface* m_pSecondCallback;
	bool m_bPressed;
	bool m_bHasSecondState;
	bool m_bSecondStateActive;

}; //end of local class

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFA3DBUTTONMODEL_H



