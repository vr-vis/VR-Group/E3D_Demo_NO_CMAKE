

set( RelativeDir "./Widgets/Button" )
set( RelativeSourceGroup "Source Files\\Widgets\\Button" )

set( DirFiles
	Vfa3DButtonController.cpp
	Vfa3DButtonController.h
	Vfa3DButtonModel.cpp
	Vfa3DButtonModel.h
	Vfa3DButtonVis.cpp
	Vfa3DButtonVis.h
	Vfa3DButtonWidget.cpp
	Vfa3DButtonWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

