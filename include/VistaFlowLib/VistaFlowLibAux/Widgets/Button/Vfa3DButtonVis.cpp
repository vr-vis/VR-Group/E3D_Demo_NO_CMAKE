/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here
#include "Vfa3DButtonVis.h" 
#include "Vfa3DButtonModel.h" 


#include <VistaOGLExt/VistaOGLUtils.h>
#include <VistaOGLExt/VistaTexture.h>

#include <cassert>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* WIDGET VIS				                                                  */
/*============================================================================*/
Vfa3DButtonVis::Vfa3DButtonVis(const std::string & sFilename, const std::string & sSecondFilename)
:m_bIsHighlighted(false),
m_fScale(1.0f),
m_sTextureFile(sFilename),
m_sSecondTextureFile(sSecondFilename),
m_bSecondTextureActive(false),
m_pTexture(NULL),
m_pSecondTexture(NULL)
{
}

Vfa3DButtonVis::Vfa3DButtonVis(VistaTexture *pTexture, VistaTexture *pSecondTexture)
:m_bIsHighlighted(false),
m_fScale(1.0f),
m_sTextureFile(""),
m_sSecondTextureFile(""),
m_bSecondTextureActive(false),
m_pTexture(pTexture),
m_pSecondTexture(pSecondTexture)
{
}


Vfa3DButtonVis::~Vfa3DButtonVis()
{
	// if we have created textures (i.e. the texture filename is set) release them 
	if(m_sTextureFile != "")
		delete m_pTexture;
	if(m_sSecondTextureFile != "")
		delete m_pSecondTexture;
}


bool Vfa3DButtonVis::Init()
{
	bool bReturn = IVflRenderable::Init();

	if(!m_sTextureFile.empty())
	{
		// load texture
		int iWidth;
		int iHeight;
		int iChannels;
		unsigned char *data = VistaOGLUtils::LoadImageFromFile(m_sTextureFile, iWidth, iHeight, iChannels);
		
		if(iChannels > 4 || iChannels < 3)
		{
			delete [] data;
			return false;
		}

		m_pTexture = new VistaTexture(GL_TEXTURE_2D);
		m_pTexture->UploadTexture(iWidth, iHeight, data, true, (iChannels == 3 ? GL_RGB : GL_RGBA));

		// data is uploaded to GPU RAM, we don't need it anymore
		delete [] data;
	}

	if(!m_sSecondTextureFile.empty())
	{
		// load texture
		int iWidth;
		int iHeight;
		int iChannels;
		unsigned char *data = VistaOGLUtils::LoadImageFromFile(m_sSecondTextureFile, iWidth, iHeight, iChannels);

		if(iChannels > 4 || iChannels < 3)
		{
			delete [] data;
			return false;
		}

		m_pSecondTexture = new VistaTexture(GL_TEXTURE_2D);
		m_pSecondTexture->UploadTexture(iWidth, iHeight, data, false, (iChannels == 3 ? GL_RGB : GL_RGBA));

		// data is uploaded to GPU RAM, we don't need it anymore
		delete [] data;
	}


	return bReturn;
}


void Vfa3DButtonVis::DrawOpaque()
{
	//setup OpenGL to draw...
	//make modelview matrix active(so we can transform things)
	glMatrixMode(GL_MODELVIEW);
	glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT | GL_POLYGON_BIT);
	//save the current model view matrix state to the stack
	glPushMatrix();

	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);

	// enable alpha test to allow non-rect buttons without introducing blending.
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.0);
	
	//translate to the current center position
	glTranslatef(m_vCenter[0], m_vCenter[1], m_vCenter[2]);
	glRotatef(Vista::RadToDeg(acos(m_qOrientation[3])*2), 
	m_qOrientation[0], m_qOrientation[1], m_qOrientation[2]);

	glScalef(m_fScale, m_fScale, m_fScale);


	//begin to draw the icon
	if(m_bIsHighlighted)
	{
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		// offset to overdraw the textured quad
		glEnable(GL_POLYGON_OFFSET_LINE);
		// offset is in screen coordinates
		glPolygonOffset(-1.0, -1.0);
		glColor3f(1.0, 0.0, 0.0);
		glBegin( GL_QUADS );
		glVertex3f(-0.5f, -0.5f, 0.0f);
		glVertex3f(-0.5f, 0.5f, 0.0f);
		glVertex3f(0.5f,0.5f, 0.0f);
		glVertex3f(0.5f,-0.5f, 0.0f);
		glEnd();
		glDisable(GL_POLYGON_OFFSET_LINE);
	}

	glColor3f(1.0,1.0,1.0);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	VistaTexture *pCurTex
		= m_bSecondTextureActive && m_pSecondTexture
		? m_pSecondTexture
		: m_pTexture;

	if(pCurTex)
	{
		pCurTex->Enable();
		// MODULATE and REPLACE would do the same here, as button color is hard-coded to WHITE
		// changed to REPLACE as this is cheaper.
		//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		// we first set glTexEnv so that a custom VistaTexture-implementation
		// may set another state within it's Bind() call...although this is a "texture unit" state, no "texture" state!
		pCurTex->Bind();
	}

	glBegin(GL_QUADS);
	glNormal3f(0,0,1);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-0.5, -0.5, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-0.5, 0.5, 0);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(0.5,0.5, 0);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(0.5,-0.5, 0);
	glEnd();
	if(pCurTex)
		pCurTex->Unbind();

	//retrieve old transformation state
	glPopMatrix();

	//retrieve old lighting state
	glPopAttrib();
}

unsigned int Vfa3DButtonVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}


void Vfa3DButtonVis::SetIsHighlighted(bool b)
{
	m_bIsHighlighted = b;
}
bool Vfa3DButtonVis::GetIsHighlighted() const
{
	return m_bIsHighlighted;
}

bool Vfa3DButtonVis::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
bool Vfa3DButtonVis::GetBounds(VistaBoundingBox &)
{
	return false;
}

void Vfa3DButtonVis::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	Vfa3DButtonModel *pModel = dynamic_cast<Vfa3DButtonModel*>(pObserveable);

	if(!pModel)
		return;

	pModel->GetPosition(m_vCenter);
	pModel->GetOrientation(m_qOrientation);
	m_fScale = pModel->GetScale();
	if (msg==Vfa3DButtonModel::MSG_STATE_CHANGE)
	{
		m_bSecondTextureActive = pModel->GetSecondStateActive();
	}
}



IVflRenderable::VflRenderableProperties* Vfa3DButtonVis::CreateProperties() const
{
	return new IVflRenderable::VflRenderableProperties();
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "          "                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

