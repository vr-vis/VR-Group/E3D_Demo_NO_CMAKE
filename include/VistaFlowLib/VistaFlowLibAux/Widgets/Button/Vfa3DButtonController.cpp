/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "Vfa3DButtonController.h" 
#include "Vfa3DButtonVis.h" 
#include "Vfa3DButtonWidget.h" 
#include <VistaFlowLibAux/Widgets/VfaControlHandle.h>


#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaOGLExt/VistaOGLUtils.h>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* WIDGET CONTROLLER		                                                  */
/*============================================================================*/
Vfa3DButtonController::Vfa3DButtonController(Vfa3DButtonWidget *pWidget,
											   VflRenderNode *pRenderNode)
:	IVfaWidgetController(pRenderNode),
	m_pWidget(pWidget),
	m_pCenterHandle(new IVfaCenterControlHandle()),
	m_iPressSlot(0),
	m_bOrientToViewer(false)
{
	m_pView = m_pWidget->GetView();
	this->AddControlHandle(m_pCenterHandle);
	this->Observe(m_pWidget->GetModel());

	m_v3Right = VistaVector3D(1.0, 0.0, 0.0);
	m_v3Up = VistaVector3D(0.0, 1.0, 0.0);
	m_v3OppView = VistaVector3D(0.0, 0.0, 1.0);
	m_v3Y = VistaVector3D(0.0, 1.0, 0.0);

}
Vfa3DButtonController::~Vfa3DButtonController()
{
	delete m_pCenterHandle;
}


void Vfa3DButtonController::OnFocus(IVfaControlHandle* pHandle)
{
	m_pView->SetIsHighlighted(true);
}
void Vfa3DButtonController::OnUnfocus()
{
	m_pView->SetIsHighlighted(false);
}

void Vfa3DButtonController::OnTouch(const std::vector<IVfaControlHandle*>& vecHandles)
{
}
void Vfa3DButtonController::OnUntouch()
{
}

void Vfa3DButtonController::OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	if(iSlot != VfaApplicationContextObject::SLOT_VIEWER_VIS)
		return;

	if(!m_bOrientToViewer)
		return;

	m_oViewerFrame = oFrameData;
	m_oViewerRefFrame.SetTranslation(m_oViewerFrame.v3Position);
	m_oViewerRefFrame.SetRotation(m_oViewerFrame.qOrientation);

	VistaVector3D v3AltUp(0.0f, 0.0f, 1.0f);

	m_v3OppView	= -1*m_oViewerRefFrame.TransformPositionToFrame(m_oViewerFrame.qOrientation.Rotate(VistaVector3D(0,0,-1)));

	if(m_v3Y.Dot(m_v3OppView) > 0.95f)
		m_v3Right	= m_v3Y.Cross(v3AltUp);
	else
		m_v3Right	= m_v3Y.Cross(m_v3OppView);

	m_v3Up		= m_v3OppView.Cross(m_v3Right);

	m_v3OppView.Normalize();
	m_v3Up.Normalize();
	m_v3Right.Normalize();

	//create rotation matrix for text
		float fMatrix[4][4] = {
			m_v3Right[0], m_v3Up[0], m_v3OppView[0], 0,
			m_v3Right[1], m_v3Up[1], m_v3OppView[1], 0,
			m_v3Right[2], m_v3Up[2], m_v3OppView[2], 0,
			0,			  0,		 0,				 1
		};
	VistaTransformMatrix m(fMatrix);
	VistaQuaternion q(m);
	q = m_oViewerRefFrame.TransformOrientationFromFrame(q);
	m_pWidget->GetModel()->SetOrientationVisSpace(q);


}
void Vfa3DButtonController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	if((iSlot == m_iPressSlot)&&(this->GetControllerState()==IVfaWidgetController::CS_FOCUS))
	{
		m_pWidget->GetModel()->SetButtonPressed(bSet);
	}
}
void Vfa3DButtonController::OnTimeSlotUpdate(const double dTime)
{
}

int Vfa3DButtonController::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_VIEWER_VIS);
}
int Vfa3DButtonController::GetCommandMask() const
{
	return(1 << m_iPressSlot);
}
bool Vfa3DButtonController::GetTimeUpdate() const
{
	return false;
}


void Vfa3DButtonController::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pCenterHandle->SetEnable(m_bEnabled);
}
bool Vfa3DButtonController::GetIsEnabled() const
{
	return m_bEnabled;
}

void Vfa3DButtonController::SetButtonDown( bool b )
{
	m_pWidget->GetModel()->SetButtonPressed(b);
}
void Vfa3DButtonController::SetButtonUp( bool b )
{
	m_pWidget->GetModel()->SetButtonPressed(!b);
}

void Vfa3DButtonController::Click()
{
	m_pWidget->GetModel()->SetButtonPressed(true);
	m_pWidget->GetModel()->SetButtonPressed(false);
}

int Vfa3DButtonController::GetPressSlot() const
{
	return m_iPressSlot;
}
void Vfa3DButtonController::SetPressSlot(int i)
{
	m_iPressSlot = i;
}

void Vfa3DButtonController::SetOrientToViewer(bool bToViewer)
{
	m_bOrientToViewer = bToViewer;
}
bool Vfa3DButtonController::GetOrientToViewer() const
{
	return m_bOrientToViewer;
}

void Vfa3DButtonController::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	Vfa3DButtonModel *pModel = dynamic_cast<Vfa3DButtonModel*>(pObserveable);

	if(!pModel)
		return;

	VistaVector3D v3Center;
	pModel->GetPosition(v3Center);
	m_pCenterHandle->SetCenter(v3Center);


}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "          "                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

