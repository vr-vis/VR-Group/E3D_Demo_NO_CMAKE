/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAINTENTIONSELECTFOCUSSTRATEGY_H
#define _VFAINTENTIONSELECTFOCUSSTRATEGY_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaFocusStrategy.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <map>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaInteractionEvent;
class VistaIntentionSelect;
class VfaSelectable;
class VistaSceneGraph;
class VistaCone;
class IVfaCenterControlHandle;

class VfaIntentionSelectPosAdapter;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*!
 * The IntentionSelect focus strategy uses the VistaKernel mechanism of
 * IntentionSelect. The pointer device (in global space) controls a cone of
 * variable length and base radius. IntentionSelect selects registered
 * control handles based on this cone. The "best" handle gets the focus,
 * all others get touched.
 *
 * REMARK: The cone is in global space!
 */
class VISTAFLOWLIBAUXAPI VfaIntentionSelectFocusStrategy : public IVfaFocusStrategy
{
public:
	VfaIntentionSelectFocusStrategy();
	virtual ~VfaIntentionSelectFocusStrategy();

	bool EvaluateFocus(std::vector<IVfaControlHandle*> &vecControlHandles);

	void RegisterSelectable(IVfaWidgetController* pSelectable);
	void UnregisterSelectable(IVfaWidgetController* pSelectable);

	/*
	* Change length and radius of selection cone
	*
	*/
	void SetSelectionConeLength(float fLength);
	float GetSelectionConeLength() const;
	void SetSelectionConeRadius(float fRadius);
	float GetSelectionConeRadius() const;

	bool SetStickynessToSnappinessAlpha(float fAlpha);
	float GetStickynessToSnappinessAlpha() const;

	void OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame & oFrameData);

	void OnCommandSlotUpdate(int iSlot, const bool bSet);

	void OnTimeSlotUpdate(const double dTime);

	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

protected:
	/*!
	 * This struct helps mapping local CenterControlHandle positions to global
	 * positions using the render node of the owning WidetController.
	 */
	struct sControlHandleMapping
	{
		IVfaWidgetController						*pOwner;

		std::vector<IVfaCenterControlHandle*>		vecLocal;
		std::vector<VfaIntentionSelectPosAdapter*>	vecGlobal;
	};

private:
	VistaIntentionSelect						*m_pSelection;
	float										m_fConeLength;
	float										m_fConeRadius;
	float										m_fStickSnapAlpha;

	IVfaCenterControlHandle						*m_pHandle;

	VfaApplicationContextObject::sSensorFrame	m_oPointer;

	std::vector<sControlHandleMapping>			m_vecPosMap;	
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFAINTENTIONSELECTFOCUSSTRATEGY_H
