/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaConditionalForwarding.h"

#include <VistaBase/VistaVectorMath.h>
#include <cassert>
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaConditionalForwarding::VfaConditionalForwarding(VfaWidgetModelBase *pModel,
													 VfaApplicationContextObject *pApplicationObject,
													 int iMyButtonId)
	: m_pApplicationObject(pApplicationObject),
	  m_iButtonId(iMyButtonId),
	  m_bIsActive(true)
{
	m_pForwardObserver = new VfaForwardObserver(this, pModel);
}

VfaConditionalForwarding::~VfaConditionalForwarding()
{
	delete m_pForwardObserver;
}

/*============================================================================*/
/* NAME: Set/GetIsActive						                              */
/*============================================================================*/
void VfaConditionalForwarding::SetIsActive(bool bActive)
{
	m_bIsActive = bActive;
}
bool VfaConditionalForwarding::GetIsActive() const
{
	return m_bIsActive;
}

/*============================================================================*/
/* NAME: IsTrigger								                              */
/*============================================================================*/
void VfaConditionalForwarding::IsTrigger()
{
	if(!m_bIsActive)
		return;
	
	// is the specified button pressed?
	if(!m_pApplicationObject->GetCommandState(m_iButtonId))
		return;
	
	this->Notify(MSG_BUTTON_IS_PRESSED);
}

/*============================================================================*/
/*  END OF FILE "VfaConditionalForwarding.cpp"                                */
/*============================================================================*/
