

set( RelativeDir "./Widgets/Line" )
set( RelativeSourceGroup "Source Files\\Widgets\\Line" )

set( DirFiles
	VfaLineController.cpp
	VfaLineController.h
	VfaLineModel.cpp
	VfaLineModel.h
	VfaLineWidget.cpp
	VfaLineWidget.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

