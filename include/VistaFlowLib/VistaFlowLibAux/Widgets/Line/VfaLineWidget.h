/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFALINEWIDGET_H
#define _VFALINEWIDGET_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidget.h"
#include "../VfaRulerVis.h"
#include "../VfaLineVis.h"
#include "../Line/VfaLineModel.h"
#include "VfaLineController.h"
#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VflRenderNode;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* A ruler wigdet contains namelay a line with scales (VfaLineModel), three spheres and also a 3D
* text. 3D text shows the ruler's length.
*/
class VISTAFLOWLIBAUXAPI VfaLineWidget : public IVfaWidget
{
public:
	enum{
		VANILLA_LINE = 0,
		LABELED_LINE
	};

	VfaLineWidget(VflRenderNode *pRenderNode, int iStatus = VANILLA_LINE);
	virtual ~VfaLineWidget();

	/** 
	 * NOTE : Handle visibility and reaction to events separately.
	 *        Thus the widget may be visible but not being interacted with!
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;

	bool GetIsInteracting() const;

	int GetStatus() const;
	
	VfaLineModel *GetModel() const;
	IVflRenderable *GetView() const;
	VfaLineController *GetController() const;

	void SetDragButton(int iButtonId);
	int GetDragButton() const;

	/**
	 *	provide a reflectionable interface to control the
	 *	main (!) visual properties of this widget
	 */
	class VISTAFLOWLIBAUXAPI VfaLineWidgetProperties : public IVistaReflectionable
	{
		friend class VistaRulerWidget;

	public:
		enum
		{
			MSG_LINE_COLOR_CHANGED = IVistaReflectionable::MSG_LAST,
			MSG_LAST_COLOR_CHANGED,
			MSG_HIGHLIGHT_COLOR_CHANGED,
			MSG_NORMAL_COLOR_CHANGED,
			MSG_LAST		
		};

		VfaLineWidgetProperties(VfaLineWidget *pWidget);
		virtual ~VfaLineWidgetProperties();
		
		bool SetLineColor(const VistaColor &c);
		VistaColor GetLineColor() const;
		void SetLineColor(float fC[4]);
		void GetLineColor(float fC[4]) const;

		bool SetLastRulerColor(const VistaColor &c);
		VistaColor GetLastRulerColor() const;
		void SetLastRulerColor(float fC[4]);
		void GetLastRulerColor(float fC[4]) const;
	
		bool SetHighlightHandleColor(const VistaColor &c);
		VistaColor GetHighlightHandleColor() const;
		void SetHighlightHandleColor(float fC[4]);
		void GetHighlightHandleColor(float fC[4]) const;

		bool SetNormalHandleColor(const VistaColor &c);
		VistaColor GetNormalHandleColor() const;
		void SetNormalHandleColor(float fC[4]);
		void GetNormalHandleColor(float fC[4]) const;

		virtual std::string GetReflectionableType() const;
		

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		int m_iWidgetStatus;
		VfaLineWidget *m_pWidget;
	
	};

	VfaLineWidgetProperties *GetProperties() const;

protected:


private:
	VfaLineController 			*m_pCtr;
	IVflRenderable				*m_pVis;
	VfaLineModel				*m_pModel;

	int m_iStatus;
	
	VfaLineWidgetProperties	*m_pProps;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaLineWidget_H
