/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <cstring>

#include "VfaLineModel.h"
#include <VistaAspects/VistaReflectionable.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
/*============================================================================*/
/*  IMPLEMENTATION      VfaBoxVisProps                                       */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaLineModel");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<std::vector<float>, VfaLineModel>(
		"LINE_CENTER", STR_REF_TYPENAME,
		&VfaLineModel::GetMidPoint),
	NULL
};
//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<const std::vector<float> &, std::vector<float>, VfaLineModel>(
		"LINE_CENTER_AND_EXTENTS", STR_REF_TYPENAME,
		&VfaLineModel::SetMidPoint),
	new TVistaPropertySet<const std::vector<float> &, std::vector<float>, VfaLineModel>(
		"LINE_CENTER", STR_REF_TYPENAME,
		&VfaLineModel::SetMidPoint),
	new TVistaPropertySet<const float, float, VfaLineModel>(
		"LENGTH", STR_REF_TYPENAME,
		&VfaLineModel::SetLength),
	new TVistaPropertySet<const VistaQuaternion &, VistaQuaternion, VfaLineModel>(
		"ROTATION", STR_REF_TYPENAME,
		&VfaLineModel::Rotate),
	NULL 
};
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaLineModel::VfaLineModel()
	:	m_vecPoint1(-1.0f, 0.0f, 0.0f),
		m_vecPoint2(1.0f, 0.0f, 0.0f),
		m_vecMidPoint(0.0f, 0.0f, 0.0f),
		m_fLength(2.0f)
{}

VfaLineModel::~VfaLineModel()
{}

/*============================================================================*/
/*  NAME      :   Set/GetPoint1		                                          */
/*============================================================================*/
void VfaLineModel::SetPoint1(float fPt[])
{
	this->SetPoint1(VistaVector3D(fPt));
}
void VfaLineModel::SetPoint1(double dPt[])
{
	this->SetPoint1(VistaVector3D(dPt));
}
void VfaLineModel::SetPoint1(const VistaVector3D &v3C)
{
	m_vecPoint1 = v3C;
	this->ComputeMidPoint();
}

void VfaLineModel::GetPoint1(float fPt[]) const
{
	m_vecPoint1.GetValues(fPt);
}
void VfaLineModel::GetPoint1(double dPt[]) const
{
	m_vecPoint1.GetValues(dPt);
}
void VfaLineModel::GetPoint1(VistaVector3D &v3P) const
{
	v3P = m_vecPoint1;
}

/*============================================================================*/
/*  NAME      :   Set/GetPoint2		                                          */
/*============================================================================*/
void VfaLineModel::SetPoint2(float fPt[])
{
	this->SetPoint2(VistaVector3D(fPt));
}
void VfaLineModel::SetPoint2(double dPt[])
{
	this->SetPoint2(VistaVector3D(dPt));
}
void VfaLineModel::SetPoint2(const VistaVector3D &v3C)
{
	m_vecPoint2 = v3C;
	this->ComputeMidPoint();
}

void VfaLineModel::GetPoint2(float fPt[]) const
{
	m_vecPoint2.GetValues(fPt);
}
void VfaLineModel::GetPoint2(double dPt[]) const
{
	m_vecPoint2.GetValues(dPt);
}
void VfaLineModel::GetPoint2(VistaVector3D &v3P) const
{
	v3P = m_vecPoint2;
}

/*============================================================================*/
/*  NAME      :   Set/GetMidPoint                                             */
/*============================================================================*/
void VfaLineModel::SetMidPoint(float fPt[], float fPt1[], float fPt2[])
{
	this->SetMidPoint(VistaVector3D(fPt),	VistaVector3D(fPt1), VistaVector3D(fPt2));
}
bool VfaLineModel::SetMidPoint(const std::vector<float> &fPts)
{
	if (fPts.size() == 9)
	{
		if (m_vecMidPoint == VistaVector3D(fPts[0],fPts[1],fPts[2]) &&
			m_vecPoint1 == VistaVector3D(fPts[3],fPts[4],fPts[5]) &&
			m_vecPoint2 == VistaVector3D(fPts[6],fPts[7],fPts[8]))
			return false;

		this->SetMidPoint(VistaVector3D(fPts[0],fPts[1],fPts[2]), VistaVector3D(fPts[3],fPts[4],fPts[5]), VistaVector3D(fPts[6],fPts[7],fPts[8]));

		return true;
	}
	else if (fPts.size() == 3)
	{
		if (m_vecMidPoint == VistaVector3D(fPts[0],fPts[1],fPts[2]))
			return false;

		this->SetMidPoint(VistaVector3D(fPts[0],fPts[1],fPts[2]), m_vecPoint1, m_vecPoint2);
		return true;
	}
	else
		return false;

}
void VfaLineModel::SetMidPoint(double dPt[], double dPt1[], double dPt2[])
{
	this->SetMidPoint(VistaVector3D(dPt),	VistaVector3D(dPt1), VistaVector3D(dPt2));
}
void VfaLineModel::SetMidPoint(const VistaVector3D &v3C, 
								const VistaVector3D &v3V1, const VistaVector3D &v3V2)
{
	m_vecMidPoint = v3C;
	m_vecPoint1 = m_vecMidPoint+v3V1;
	m_vecPoint2 = m_vecMidPoint+v3V2;
	this->ComputeLength();
	this->Notify(MSG_LINE_CHANGE);
}

void VfaLineModel::GetMidPoint(float fPt[]) const
{
	m_vecMidPoint.GetValues(fPt);
}
void VfaLineModel::GetMidPoint(double dPt[]) const
{
	m_vecMidPoint.GetValues(dPt);
}
void VfaLineModel::GetMidPoint(VistaVector3D &v3P) const
{
	v3P = m_vecMidPoint;
}
std::vector<float> VfaLineModel::GetMidPoint() const
{
	std::vector<float> vPt;
	vPt.push_back(m_vecMidPoint[0]);
	vPt.push_back(m_vecMidPoint[1]);
	vPt.push_back(m_vecMidPoint[2]);
	return vPt;
}

/*============================================================================*/
/*  NAME      :   GetLength			                                          */
/*============================================================================*/
float VfaLineModel::GetLength() const
{
	return m_fLength;
}

/*============================================================================*/
/*  NAME      :   SetLength			                                          */
/*============================================================================*/
bool VfaLineModel::SetLength(const float fL)
{
	if (m_fLength == fL)
		return false;

	m_vecPoint1 *= fL / (2.0f * m_vecPoint1.GetLength());
	m_vecPoint2 *= fL / (2.0f * m_vecPoint2.GetLength());
	this->SetMidPoint(m_vecMidPoint, m_vecPoint1, m_vecPoint2);
	return true;
}

/*============================================================================*/
/*  NAME      :   Rotate			                                          */
/*============================================================================*/
bool VfaLineModel::Rotate(const VistaQuaternion &qRotation)
{
	this->SetMidPoint(
		m_vecMidPoint, 
		qRotation.Rotate(m_vecPoint1-m_vecMidPoint), 
		qRotation.Rotate(m_vecPoint2-m_vecMidPoint));

	return true;
}

/*============================================================================*/
/*  NAME      :   Recalculate...	                                          */
/*============================================================================*/
void VfaLineModel::ComputeMidPoint()
{
	this->ComputeLength();

	m_vecMidPoint = 0.5f * (m_vecPoint1 + m_vecPoint2);

	this->Notify(MSG_LINE_CHANGE);
}

void VfaLineModel::ComputeLength()
{
	m_fLength = (m_vecPoint1 - m_vecPoint2).GetLength();
}
/*============================================================================*/
/*  NAME      :   GetReflectionableType                                       */
/*============================================================================*/
std::string VfaLineModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}
/*============================================================================*/
/*  NAME      :   AddToBaseTypeList                                           */
/*============================================================================*/
int VfaLineModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  END OF FILE "VfaBoxWidget.cpp"                                            */
/*============================================================================*/
