/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES																  */
/*============================================================================*/
#include "VfaLineController.h"
#include "../Line/VfaLineModel.h"
#include "../VfaWidgetTools.h"
#include "../VfaLineVis.h"
#include "../VfaSphereHandle.h"

#include <VistaMath/VistaIndirectXform.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <cstdio>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaLineController::VfaLineController(VfaLineModel *pModel,
									   VflRenderNode *pRenderNode)
:	IVfaWidgetController(pRenderNode),
	m_pModel(pModel),
	m_pRenderNode(pRenderNode),
	m_iState(LINE_UNDEFINED),
	m_iLastState(LINE_UNDEFINED),
	m_iDragButton(0),
	m_iActiveHandle(HND_UNDEFINDED),
	m_pXform(new VistaIndirectXform),
	m_bShowLastState(true),
	m_bMoveEndPointsIndependently(false)
{
	m_pCtlProps = new VfaLineController::VfaLineControllerProps(this);

	//attach the model
	this->Observe(m_pModel);

	//create handles
	m_pSphereHandles.resize(3);

	m_pSphereHandles[HND_SPHERE_LEFT] = new VfaSphereHandle(pRenderNode);
	m_pSphereHandles[HND_SPHERE_LEFT]->SetRadius(0.02f);
	float fRNColor[4] = {1.0f, 0.0f, 0.0f, 1.0f};
	m_pSphereHandles[HND_SPHERE_LEFT]->SetNormalColor(fRNColor);
	float fRHColor[4] = {1.0f, 0.75f, 0.75f, 1.0f};
	m_pSphereHandles[HND_SPHERE_LEFT]->SetHighlightColor(fRHColor);
	this->AddControlHandle(m_pSphereHandles[HND_SPHERE_LEFT]);

	m_pSphereHandles[HND_SPHERE_MIDDLE] = new VfaSphereHandle(pRenderNode);
	m_pSphereHandles[HND_SPHERE_MIDDLE]->SetRadius(0.02f);
	float fMNColor[4] = {0.0f, 0.0f, 1.0f, 1.0f};
	m_pSphereHandles[HND_SPHERE_MIDDLE]->SetNormalColor(fMNColor);
	float fMHColor[4] = {0.75f, 0.75f, 1.0f, 1.0f};
	m_pSphereHandles[HND_SPHERE_MIDDLE]->SetHighlightColor(fMHColor);
	this->AddControlHandle(m_pSphereHandles[HND_SPHERE_MIDDLE]);

	m_pSphereHandles[HND_SPHERE_RIGHT] = new VfaSphereHandle(pRenderNode);
	m_pSphereHandles[HND_SPHERE_RIGHT]->SetRadius(0.02f);
	float fLNColor[4] = {1.0f, 0.0f, 0.0f, 1.0f};
	m_pSphereHandles[HND_SPHERE_RIGHT]->SetNormalColor(fLNColor);
	float fLHColor[4] = {1.0f, 0.75f, 0.75f, 1.0f};
	m_pSphereHandles[HND_SPHERE_RIGHT]->SetHighlightColor(fLHColor);
	this->AddControlHandle(m_pSphereHandles[HND_SPHERE_RIGHT]);

	
	//initialize vis rep for last LINE (shown when dragging a new box
	m_pLastLine = new VfaLineVis(new VfaLineModel);
	if(m_pLastLine->Init())
		m_pRenderNode->AddRenderable(m_pLastLine);
	float fLastLineColor[3]= {0.6f, 0.6f, 0.6f};
	VfaLineVis::VfaLineVisProps *pRProps = m_pLastLine->GetProperties();
	pRProps->SetColor(fLastLineColor);
	pRProps->SetVisible(false);
		

	//check if LINE's length != 0 -> if so we directly goto state DEFINED
	if(m_pModel->GetLength() != 0)
		this->SetLineState(LINE_DEFINED);
	else
		this->SetArtItemsVisible(false);
}

VfaLineController::~VfaLineController()
{
	this->ReleaseObserveable(m_pModel, IVistaObserveable::TICKET_NONE);

	//delete handles
	for (int i = 0; i < 3; i++)
			delete m_pSphereHandles[i];

	delete m_pLastLine->GetModel();
	m_pRenderNode->RemoveRenderable(m_pLastLine);
	delete m_pLastLine;
	delete m_pXform;
}


/*============================================================================*/
/*  NAME      :   OnSensorSlotUpdate	                                      */
/*============================================================================*/
void VfaLineController::OnSensorSlotUpdate(int iSlot, const 
					VfaApplicationContextObject::sSensorFrame &oFrameData)
{
	if(iSlot == VfaApplicationContextObject::SLOT_POINTER_VIS)
	{
		// save sensor data
		m_oLastSensorFrame = oFrameData;

		// if we have a move/resize state, move something....
		if(m_iState != LINE_DEFINED || m_iState != LINE_UNDEFINED)
		{
			VistaVector3D v3MyPos, v3MyOldPos, vecPt1, vecPt2;
			VistaQuaternion qMyOri;
			m_pXform->Update(	m_oLastSensorFrame.v3Position, 
				m_oLastSensorFrame.qOrientation, 
				v3MyPos, qMyOri);
			m_pModel->GetMidPoint(v3MyOldPos);
			m_pModel->GetPoint1(vecPt1);
			m_pModel->GetPoint2(vecPt2);
			switch(m_iState)
			{
			case LINE_MOVING:
				{
					m_pModel->SetPoint1(vecPt1 + (v3MyPos - v3MyOldPos) );
					m_pModel->SetPoint2(vecPt2 + (v3MyPos - v3MyOldPos) );

					//TODO: Maybe deal with orientation (as it is done in BoxController)
					break;
				}
			case LINE_DRAGGING:
				{

					if(!m_bMoveEndPointsIndependently)
					{
						VistaVector3D dif1, dif2;
						if ((v3MyPos - vecPt1).GetLength() < ((v3MyPos - vecPt2).GetLength()))
						{
							dif1 = v3MyPos - vecPt1;
							dif2 = -dif1;
						}
						else
						{
							dif2 = v3MyPos - vecPt2;
							dif1 = -dif2;
						}

						m_pModel->SetPoint1(vecPt1 + dif1);
						m_pModel->SetPoint2(vecPt2 + dif2);

					}
					else
					{
						if(m_iActiveHandle == HND_SPHERE_LEFT)
							m_pModel->SetPoint1(v3MyPos);
						else if(m_iActiveHandle == HND_SPHERE_RIGHT)
							m_pModel->SetPoint2(v3MyPos);
					}

					

					//TODO: Maybe deal with orientation (as it is done in BoxController)
					break;
				}
			case LINE_MOVING_SPHERE:
				{
					vstr::errp()<<"[VfaLineController] LINE_MOVING_SPHERE state not implemented!"<<endl;
				}
			}
		}
	}
}

/*============================================================================*/
/*  NAME      :   OnCommandSlotUpdate	                                      */
/*============================================================================*/
void VfaLineController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	//we are only interested in our button
	if(iSlot != m_iDragButton)
		return;

	if(bSet)
	{
		// we can only be grabbed, if we have the focus
		if(this->GetControllerState() == CS_FOCUS)
			if(m_iState == LINE_UNDEFINED)
			{	
				//actually, this shouldn't be the case
				//vstr::errp() << "Line undefined! Line length is 0; This case has to be implemented or the internal states of VfaLineController should be simplified!" << std::endl;
			}
			else if (m_iState == LINE_DEFINED)
			{
				VistaVector3D v3Center, vecPt1;
				m_pModel->GetPoint1(vecPt1);
				m_pModel->GetMidPoint(v3Center);
				vecPt1-=v3Center;
				vecPt1.Normalize();
				VistaQuaternion rQuat(v3Center, vecPt1);

				if(m_iActiveHandle == HND_UNDEFINDED)
				{

					vstr::errp() << "[VfaLineController] Active handle is undefined! Should never be the case!" << std::endl;
				}
				else if (m_iActiveHandle == HND_SPHERE_MIDDLE)
				{
					m_pXform->Init(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation,
						v3Center, rQuat);
					this->SetLineState(LINE_MOVING);
				}
				else if (m_iActiveHandle == HND_SPHERE_LEFT || m_iActiveHandle == HND_SPHERE_RIGHT)
				{
					m_pSphereHandles[m_iActiveHandle]->GetCenter(v3Center);
					m_pXform->Init(m_oLastSensorFrame.v3Position, m_oLastSensorFrame.qOrientation,
						v3Center, rQuat);
					this->SetLineState(LINE_DRAGGING);
				}
			}
	}
	else
	{
		// if the slot is released, goto state DEFINED
		this->SetLineState(LINE_DEFINED);

		// and if we have lost focus during other states, now react on that
		if(this->GetControllerState()!=CS_FOCUS)
			this->OnUnfocus();
	}

	return;
}

/*============================================================================*/
/*  NAME      :   OnTimeSlotUpdate		                                      */
/*============================================================================*/
void VfaLineController::OnTimeSlotUpdate(const double dTime)
{
	return;
}

/*============================================================================*/
/*  NAME      :   OnFocus				                                      */
/*============================================================================*/
void VfaLineController::OnFocus(IVfaControlHandle* pHandle)
{
	// ignore focus and onfocus, if we currently manipulate the widget
	if(	m_iState == LINE_DRAGGING || m_iState == LINE_MOVING_SPHERE || m_iState == LINE_MOVING)
					return;

	//when we get the focus for some handle 
	//we must not have a active handle before (i.e. this should be ensured by
	//::OnUnfocus
	if (m_iActiveHandle != HND_UNDEFINDED)
		return;

	//determine which handle is grabbed
	for(int i = 0; i < 3; i++)
	{
		if(m_pSphereHandles[i] == pHandle)
		{
			m_iActiveHandle = i;
			break;
		}
	}

	if(m_iActiveHandle != HND_UNDEFINDED)
		m_pSphereHandles[m_iActiveHandle]->SetIsHighlighted(true);
}

/*============================================================================*/
/*  NAME      :   OnUnfocus				                                      */
/*============================================================================*/
void VfaLineController::OnUnfocus()
{
	// ignore focus and onfocus, if we currently manipulate the widget
	if(m_iState == LINE_DRAGGING || m_iState == LINE_MOVING_SPHERE || m_iState == LINE_MOVING)
			return;

	if (m_iActiveHandle == HND_UNDEFINDED)
		return;

	// release the current handle
	m_pSphereHandles[m_iActiveHandle]->SetIsHighlighted(false);
	m_iActiveHandle = HND_UNDEFINDED;
}

/*============================================================================*/
/*  NAME      :   OnTouch				                                      */
/*============================================================================*/
void VfaLineController::OnTouch(
					const std::vector<IVfaControlHandle*> &vecHandles)
{
	return;
}
/*============================================================================*/
/*  NAME      :   OnUntouch				                                      */
/*============================================================================*/
void VfaLineController::OnUntouch()
{
	return;
}
/*============================================================================*/
/*  NAME      :   GetSensorMask			                                      */
/*============================================================================*/
int VfaLineController::GetSensorMask() const
{
	return (1<<VfaApplicationContextObject::SLOT_POINTER_VIS);
}

/*============================================================================*/
/*  NAME      :   GetCommandMask			                                  */
/*============================================================================*/
int VfaLineController::GetCommandMask() const
{
	return(1 << m_iDragButton);
}

/*============================================================================*/
/*  NAME      :   GetTimeUpdate				                                  */
/*============================================================================*/
bool VfaLineController::GetTimeUpdate() const
{
	return true;
}

/*============================================================================*/
/*  NAME      :   GetDragButton                                               */
/*============================================================================*/
bool VfaLineController::GetIsInteracting() const
{
	return m_iState != LINE_UNDEFINED && m_iState != LINE_DEFINED;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIsEnabled                                            */
/*                                                                            */
/*============================================================================*/
void VfaLineController::SetIsEnabled(bool b)
{
	m_pSphereHandles[HND_SPHERE_LEFT]->SetEnable(b);
	m_pSphereHandles[HND_SPHERE_MIDDLE]->SetEnable(b);
	m_pSphereHandles[HND_SPHERE_RIGHT]->SetEnable(b);

	this->SetArtItemsVisible(b);
	m_pLastLine->GetProperties()->SetVisible(false);
	m_pLastLine->Update();
	
	if(b)
	{
		this->SetLineState(m_iLastState);
		return;
	}

	//always return to either of the two "idle" states
	m_iLastState = std::max<int>(LINE_UNDEFINED, m_iState);
	this->SetLineState(LINE_DISABLED);

	// give a last notify, as now we have a clear undefined state
	m_pModel->Notify(VfaLineModel::MSG_LINE_CHANGE);
}
bool VfaLineController::GetIsEnabled() const
{
	return m_iState != LINE_DISABLED;
}

/*============================================================================*/
/*  NAME      :   Set/GetArtItemsVisible                                      */
/*============================================================================*/
void VfaLineController::SetArtItemsVisible(bool b)
{
	for(int i = 0; i < 3; i++)
		m_pSphereHandles[i]->SetVisible(b);
	m_pLastLine->GetProperties()->SetVisible(b);
}
bool VfaLineController::GetAreItemsVisible()
{
	return m_pSphereHandles[0]->GetVisible();
}

/*============================================================================*/
/*  NAME      :   Set/GetDragButton                                           */
/*============================================================================*/
int VfaLineController::GetDragButton() const
{
	return m_iDragButton;
}
void VfaLineController::SetDragButton(int i)
{
	m_iDragButton = i;
}

/*============================================================================*/
/*  NAME      :   Set/GetShowLastState                                        */
/*============================================================================*/
void VfaLineController::SetShowLastState(bool b)
{
	m_bShowLastState = b;
	m_pLastLine->SetVisible(m_bShowLastState);
}
bool VfaLineController::GetShowLastState() const
{
	return m_bShowLastState;
}

/*============================================================================*/
/*  NAME    :   Set/GetLastLineColor                                          */
/*============================================================================*/
void VfaLineController::SetLastLineColor(const VistaColor& color)
{
	float fColors[4];
	color.GetValues(fColors);
	m_pLastLine->GetProperties()->SetColor(fColors);
}

VistaColor VfaLineController::GetLastLineColor() const
{
	float fColors[4];
	m_pLastLine->GetProperties()->GetColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaLineController::SetLastLineColor(float f[4])
{
	m_pLastLine->GetProperties()->SetColor(f);
}
void VfaLineController::GetLastLineColor(float f[4]) const
{
	m_pLastLine->GetProperties()->GetColor(f);
}

/*============================================================================*/
/*  NAME    :   Set/GetMoveEndPointsIndependently                             */
/*============================================================================*/
void VfaLineController::SetMoveEndPointsIndependently( bool bIndependently )
{
	m_bMoveEndPointsIndependently = bIndependently;
}

bool VfaLineController::GetMoveEndPointsIndependently()
{
	return m_bMoveEndPointsIndependently;
}

/*============================================================================*/
/*  NAME    :   ObserveableDeleteRequest                                      */
/*============================================================================*/
bool VfaLineController::ObserveableDeleteRequest(IVistaObserveable *pObserveable, int eTicket)
{
	return true;
}
/*============================================================================*/
/*  NAME    :  ObserveableDelete                                              */
/*============================================================================*/
void VfaLineController::ObserveableDelete( IVistaObserveable *pObserveable, int eTicket)
{
	return;
}
/*============================================================================*/
/*  NAME    :     ReleaseObserveable                                          */
/*============================================================================*/
void VfaLineController::ReleaseObserveable(IVistaObserveable *pObserveable, int eTicket)
{
	pObserveable->DetachObserver(this);
}
/*============================================================================*/
/*  NAME    :     Observes                                                    */
/*============================================================================*/
bool VfaLineController::Observes(IVistaObserveable *pObserveable)
{
	return dynamic_cast<VfaLineModel*>(pObserveable) == m_pModel;
}
/*============================================================================*/
/*  NAME    :    Observe                                                      */
/*============================================================================*/
void VfaLineController::Observe(IVistaObserveable *pObservable, 
									int eTicket/*=IVistaObserveable::TICKET_NONE*/)
{
	pObservable->AttachObserver(this, eTicket);
}

/*============================================================================*/
/*  NAME    :    ObserverUpdate                                               */
/*============================================================================*/
void VfaLineController::ObserverUpdate(IVistaObserveable *pObservable, 
										   int iMsg, int iTicket)
{
	VfaLineModel *pLineModel = dynamic_cast<VfaLineModel*>(pObservable);
	if(!pLineModel)
		return;

	VistaVector3D v3Corner;
	m_pModel->GetPoint1(v3Corner);
	m_pSphereHandles[HND_SPHERE_LEFT]->SetCenter(v3Corner);

	m_pModel->GetPoint2(v3Corner);
	m_pSphereHandles[HND_SPHERE_RIGHT]->SetCenter(v3Corner);

	m_pModel->GetMidPoint(v3Corner);
	m_pSphereHandles[HND_SPHERE_MIDDLE]->SetCenter(v3Corner);

}

/*============================================================================*/
/*  NAME    :    SetLineState                                                */
/*============================================================================*/
void VfaLineController::SetLineState(int iNewState)
{
	int iOldState = m_iState;
	m_iState = iNewState;
	//this->PrintState(iOldState, iNewState);
}
/*============================================================================*/
/*  NAME    :    ObserverUpdate                                               */
/*============================================================================*/
void VfaLineController::PrintState(int iOldState, int iCurrentState) const
{
	std::string strOldState = "UNKNOWN";
	std::string strNewState = "UNKNOWN";
	switch(iOldState)
	{
		case LINE_UNDEFINED: strOldState = "UNDEFINED"; break;
		case LINE_DRAGGING: strOldState = "DRAGGING"; break;
		case LINE_DEFINED: strOldState = "DEFINED"; break;
		case LINE_MOVING_SPHERE: strOldState = "MOVING_SPHERE"; break;
		case LINE_MOVING: strOldState = "MOVING"; break;
		case LINE_DISABLED: strOldState = "LINE_DISABLED"; break;
	}
	switch(iCurrentState)	
	{
		case LINE_UNDEFINED: strNewState = "UNDEFINED"; break;
		case LINE_DRAGGING: strNewState = "DRAGGING"; break;
		case LINE_DEFINED: strNewState = "DEFINED"; break;
		case LINE_MOVING_SPHERE: strNewState = "MOVING_SPHERE"; break;
		case LINE_MOVING: strNewState = "MOVING"; break;
		case LINE_DISABLED: strNewState = "LINE_DISABLED"; break;
	}
	vstr::outi()<<"[VfaLineController] LINEState change : "<<strOldState.c_str()<<" -> "<<strNewState.c_str()<<endl;

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaLineController::VfaLineControllerProps* VfaLineController::GetProperties() const
{
	return m_pCtlProps;
}


/*============================================================================*/
/*  IMPLEMENTATION      VfaLineControllerProps                                */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaLineController::VfaLineControllerProps");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<float, VfaLineController::VfaLineControllerProps>(
	"HANDLE_RADIUS", STR_REF_TYPENAME,
	&VfaLineController::VfaLineControllerProps::GetSphereHandleRadius),
	NULL
};

//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<float, float, VfaLineController::VfaLineControllerProps>(
	"HANDLE_RADIUS", STR_REF_TYPENAME,
	&VfaLineController::VfaLineControllerProps::SetSphereHandleRadius),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaLineController::VfaLineControllerProps>(
	"HANDLE_NORMAL_COLOR", STR_REF_TYPENAME,
	&VfaLineController::VfaLineControllerProps::SetNormalHandleColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaLineController::VfaLineControllerProps>(
	"HANDLE_HIGHLIGHT_COLOR", STR_REF_TYPENAME,
	&VfaLineController::VfaLineControllerProps::SetHighlightHandleColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaLineController::VfaLineControllerProps>(
	"TRANSLATION_HANDLE_NORMAL_COLOR", STR_REF_TYPENAME,
	&VfaLineController::VfaLineControllerProps::SetNormalMiddleHandleColor),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaLineController::VfaLineControllerProps>(
	"TRANSLATION_HANDLE_HIGHLIGHT_COLOR", STR_REF_TYPENAME,
	&VfaLineController::VfaLineControllerProps::SetHighlightMiddleHandleColor),
	NULL 
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaLineController::VfaLineControllerProps::VfaLineControllerProps(VfaLineController *pLineCtrl)
: m_pLineCtrl(pLineCtrl)
{

}

VfaLineController::VfaLineControllerProps::~VfaLineControllerProps()
{

}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHandleRadius                                         */
/*                                                                            */
/*============================================================================*/
bool VfaLineController::VfaLineControllerProps::SetHandleRadius(float fRadius)
{
	if(fRadius != m_pLineCtrl->m_pSphereHandles[0]->GetRadius())
	{
		for(int i=0; i<3; ++i)
		{
			m_pLineCtrl->m_pSphereHandles[i]->SetRadius(fRadius);
		}
		Notify();
		return true;
	}
	return false;
}

float VfaLineController::VfaLineControllerProps::GetHandleRadius() const
{
	return m_pLineCtrl->m_pSphereHandles[0]->GetRadius();
}

/*============================================================================*/
/*  NAME    :   Get/SetHighlightHandleColor                                   */
/*============================================================================*/
bool VfaLineController::VfaLineControllerProps::SetHighlightHandleColor(const VistaColor& color)
{
	if (m_pLineCtrl->m_pSphereHandles[HND_SPHERE_LEFT]->GetHighlightColor() == color)
		return false;

	float fColors[4];
	color.GetValues(fColors);

	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_LEFT]->SetHighlightColor(fColors);
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_RIGHT]->SetHighlightColor(fColors);

	return true;
}

VistaColor VfaLineController::VfaLineControllerProps::GetHighlightHandleColor() const
{
	float fColors[4];
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_LEFT]->GetHighlightColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaLineController::VfaLineControllerProps::SetHighlightHandleColor(float f[4])
{
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_LEFT]->SetHighlightColor(f);
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_RIGHT]->SetHighlightColor(f);
}
void VfaLineController::VfaLineControllerProps::GetHighlightHandleColor(float f[4]) const
{
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_LEFT]->GetHighlightColor(f);
}

/*============================================================================*/
/*  NAME    :   Get/SetNormalHandleColor									  */
/*============================================================================*/
bool VfaLineController::VfaLineControllerProps::SetNormalHandleColor(const VistaColor& color)
{
	if (m_pLineCtrl->m_pSphereHandles[HND_SPHERE_LEFT]->GetNormalColor() == color)
		return false;

	float fColors[4];
	color.GetValues(fColors);

	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_LEFT]->SetNormalColor(fColors);
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_RIGHT]->SetNormalColor(fColors);

	return true;
}

VistaColor VfaLineController::VfaLineControllerProps::GetNormalHandleColor() const
{
	float fColors[4];
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_LEFT]->GetNormalColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaLineController::VfaLineControllerProps::SetNormalHandleColor(float f[4])
{
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_LEFT]->SetNormalColor(f);
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_RIGHT]->SetNormalColor(f);
}
void VfaLineController::VfaLineControllerProps::GetNormalHandleColor(float f[4]) const
{
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_LEFT]->GetNormalColor(f);
}

/*============================================================================*/
/*  NAME    :   Get/SetHighlightMiddleHandleColor                             */
/*============================================================================*/
bool VfaLineController::VfaLineControllerProps::SetHighlightMiddleHandleColor(const VistaColor& color)
{
	if (m_pLineCtrl->m_pSphereHandles[HND_SPHERE_MIDDLE]->GetHighlightColor() == color)
		return false;

	float fColors[4];
	color.GetValues(fColors);

	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_MIDDLE]->SetHighlightColor(fColors);

	return true;
}

VistaColor VfaLineController::VfaLineControllerProps::GetHighlightMiddleHandleColor() const
{
	float fColors[4];
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_MIDDLE]->GetHighlightColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaLineController::VfaLineControllerProps::SetHighlightMiddleHandleColor(float f[4])
{
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_MIDDLE]->SetHighlightColor(f);
}
void VfaLineController::VfaLineControllerProps::GetHighlightMiddleHandleColor(float f[4]) const
{
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_MIDDLE]->GetHighlightColor(f);
}

/*============================================================================*/
/*  NAME    :   Get/SetNormalMiddleHandleColor									  */
/*============================================================================*/
bool VfaLineController::VfaLineControllerProps::SetNormalMiddleHandleColor(const VistaColor& color)
{
	if (m_pLineCtrl->m_pSphereHandles[HND_SPHERE_MIDDLE]->GetNormalColor() == color)
		return false;

	float fColors[4];
	color.GetValues(fColors);

	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_MIDDLE]->SetNormalColor(fColors);

	return true;
}

VistaColor VfaLineController::VfaLineControllerProps::GetNormalMiddleHandleColor() const
{
	float fColors[4];
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_MIDDLE]->GetNormalColor(fColors);
	VistaColor color(fColors);

	return color;
}
void VfaLineController::VfaLineControllerProps::SetNormalMiddleHandleColor(float f[4])
{
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_MIDDLE]->SetNormalColor(f);
}
void VfaLineController::VfaLineControllerProps::GetNormalMiddleHandleColor(float f[4]) const
{
	m_pLineCtrl->m_pSphereHandles[HND_SPHERE_MIDDLE]->GetNormalColor(f);
}

/*============================================================================*/
/*  NAME    :   Get/SetSphereHandleRadius                                     */
/*============================================================================*/
bool VfaLineController::VfaLineControllerProps::SetSphereHandleRadius(const float fRadius)
{
	if(fRadius != m_pLineCtrl->m_pSphereHandles[0]->GetRadius())
	{
		for (int i = 0; i < 3; i++)
			m_pLineCtrl->m_pSphereHandles[i]->SetRadius(fRadius);
		Notify();
		return true;
	}
	return false;
}
float VfaLineController::VfaLineControllerProps::GetSphereHandleRadius() const
{
	return m_pLineCtrl->m_pSphereHandles[0]->GetRadius();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaLineController::VfaLineControllerProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaLineController::VfaLineControllerProps::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/*  END OF FILE "VfaLineController.cpp"                                      */
/*============================================================================*/
