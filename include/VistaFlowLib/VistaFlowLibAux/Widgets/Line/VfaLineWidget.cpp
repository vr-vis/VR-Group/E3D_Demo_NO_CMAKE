/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <cstring>
#include <cassert>

#include "VfaLineWidget.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaLineWidget::VfaLineWidget(VflRenderNode *pRenderNode,
							   int iStatus /*= VANILLA_LINE*/)
:	IVfaWidget(pRenderNode),
	m_pModel(new VfaLineModel),
	m_pProps(NULL),
	m_iStatus(iStatus)
{
	m_pCtr = new VfaLineController(m_pModel, pRenderNode);


	// enstprechende Vis ini
	switch(iStatus)
	{
	case VANILLA_LINE:
			m_pVis = new VfaLineVis(m_pModel);
			break;
	case LABELED_LINE:
			m_pVis = new VfaRulerVis(m_pModel, pRenderNode);
			break;
	default:
			//this SHALL NEVER happen!
			assert(0 && "Unknown Status!");
	}

	m_pProps = new VfaLineWidget::VfaLineWidgetProperties(this);

	m_pVis->Init();
	pRenderNode->AddRenderable(m_pVis);
}

VfaLineWidget::~VfaLineWidget()
{
	delete m_pCtr;
	delete m_pModel;
	delete m_pProps;
}

/*============================================================================*/
/*  NAME: Set/GetIsEndabled                                                   */
/*============================================================================*/
void VfaLineWidget::SetIsEnabled(bool b)
{
	m_pCtr->SetIsEnabled(b);	
}
bool VfaLineWidget::GetIsEnabled() const
{
	return m_pCtr->GetIsEnabled();
}

/*============================================================================*/
/*  NAME: Set/GetIsVisible                                                    */
/*============================================================================*/
void VfaLineWidget::SetIsVisible(bool b)
{
	m_pVis->SetVisible(b);
	m_pCtr->SetArtItemsVisible(b);
}
bool VfaLineWidget::GetIsVisible() const
{
	return m_pVis->GetVisible();
}

/*============================================================================*/
/*  NAME: GetIsInteracting                                                    */
/*============================================================================*/
bool VfaLineWidget::GetIsInteracting() const
{
	return m_pCtr->GetIsInteracting();
}

/*============================================================================*/
/*  NAME: GetModel		                                                      */
/*============================================================================*/
VfaLineModel *VfaLineWidget::GetModel() const
{
	return m_pModel;
}
/*============================================================================*/
/*  NAME: GetView		                                                      */
/*============================================================================*/
IVflRenderable *VfaLineWidget::GetView() const
{
	return m_pVis;
}
/*============================================================================*/
/*  NAME: GetController		                                                  */
/*============================================================================*/
VfaLineController *VfaLineWidget::GetController() const
{
	return m_pCtr;
}

/*============================================================================*/
/*  NAME: GetStatus									                          */
/*============================================================================*/
int VfaLineWidget::GetStatus() const
{
	return m_iStatus;
}

/*============================================================================*/
/*  NAME: Set/GetDragButton                                                   */
/*============================================================================*/
void VfaLineWidget::SetDragButton(int iButtonId)
{
	m_pCtr->SetDragButton(iButtonId);
}
int VfaLineWidget::GetDragButton() const
{
	return m_pCtr->GetDragButton();
}

/*============================================================================*/
/*  NAME      :   GetProperties                                               */
/*============================================================================*/
VfaLineWidget::VfaLineWidgetProperties *VfaLineWidget::GetProperties() const
{
	return m_pProps;
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaLineWidgetProps                                  */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaLineWidget::VfaLineWidgetProps");

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaLineWidget::VfaLineWidgetProperties::VfaLineWidgetProperties(VfaLineWidget *pWidget)
	:	m_pWidget(pWidget)
{
	m_iWidgetStatus = m_pWidget->GetStatus();
}

VfaLineWidget::VfaLineWidgetProperties::~VfaLineWidgetProperties()
{}

/*============================================================================*/
/*  NAME      :   Set/GetLineColor		                                      */
/*============================================================================*/
bool VfaLineWidget::VfaLineWidgetProperties::SetLineColor(const VistaColor &c)
{
	float fColor[3];
	c.GetValues(fColor);

	if(m_iWidgetStatus == VfaLineWidget::VANILLA_LINE)
	{
		VfaLineVis *pVis = dynamic_cast<VfaLineVis*> (m_pWidget->GetView());
		pVis->GetProperties()->SetColor(fColor);
	}
	else if (m_iWidgetStatus == VfaLineWidget::LABELED_LINE)
	{
		VfaRulerVis *pVis = dynamic_cast<VfaRulerVis*> (m_pWidget->GetView());
		pVis->GetProperties()->SetColor(fColor);
	}

	this->Notify(MSG_LINE_COLOR_CHANGED);
	return true;
}
VistaColor VfaLineWidget::VfaLineWidgetProperties::GetLineColor() const
{
	float fColor[3];
	if(m_iWidgetStatus == VfaLineWidget::VANILLA_LINE)
	{
		VfaLineVis *pVis = dynamic_cast<VfaLineVis*> (m_pWidget->GetView());
		pVis->GetProperties()->GetColor(fColor);
	}
	else if (m_iWidgetStatus == VfaLineWidget::LABELED_LINE)
	{
		VfaRulerVis *pVis = dynamic_cast<VfaRulerVis*> (m_pWidget->GetView());
		pVis->GetProperties()->GetColor(fColor);
	}
	return VistaColor(fColor);
}
void VfaLineWidget::VfaLineWidgetProperties::SetLineColor(float fC[4])
{
	if(m_iWidgetStatus == VfaLineWidget::VANILLA_LINE)
	{
		VfaLineVis *pVis = dynamic_cast<VfaLineVis*> (m_pWidget->GetView());
		pVis->GetProperties()->SetColor(fC);
	}
	else if (m_iWidgetStatus == VfaLineWidget::LABELED_LINE)
	{
		VfaRulerVis *pVis = dynamic_cast<VfaRulerVis*> (m_pWidget->GetView());
		pVis->GetProperties()->SetColor(fC);
	}

	this->Notify(MSG_LINE_COLOR_CHANGED);
}
void VfaLineWidget::VfaLineWidgetProperties::GetLineColor(float fC[4]) const
{
	if(m_iWidgetStatus == VfaLineWidget::VANILLA_LINE)
	{
		VfaLineVis *pVis = dynamic_cast<VfaLineVis*> (m_pWidget->GetView());
		pVis->GetProperties()->GetColor(fC);
	}
	else if (m_iWidgetStatus == VfaLineWidget::LABELED_LINE)
	{
		VfaRulerVis *pVis = dynamic_cast<VfaRulerVis*> (m_pWidget->GetView());
		pVis->GetProperties()->GetColor(fC);
	}
}

/*============================================================================*/
/*  NAME      :   Set/GetLastRulerColor                                       */
/*============================================================================*/
bool VfaLineWidget::VfaLineWidgetProperties::SetLastRulerColor(const VistaColor &c)
{
	float fColor[3];
	c.GetValues(fColor);
	m_pWidget->GetController()->SetLastLineColor(fColor);
	this->Notify(MSG_LAST_COLOR_CHANGED);
	return true;
}
VistaColor VfaLineWidget::VfaLineWidgetProperties::GetLastRulerColor() const
{
	float fColor[3];
	m_pWidget->GetController()->GetLastLineColor(fColor);
	return VistaColor(fColor);
}
void VfaLineWidget::VfaLineWidgetProperties::SetLastRulerColor(float fC[4])
{
	m_pWidget->GetController()->SetLastLineColor(fC);
	this->Notify(MSG_LAST_COLOR_CHANGED);
}
void VfaLineWidget::VfaLineWidgetProperties::GetLastRulerColor(float fC[4]) const
{
	m_pWidget->GetController()->GetLastLineColor(fC);
}

/*============================================================================*/
/*  NAME      :   Get/SetHighlightHandleColor                                 */
/*============================================================================*/
bool VfaLineWidget::VfaLineWidgetProperties::SetHighlightHandleColor(const VistaColor &c)
{
	float fColor[3];
	c.GetValues(fColor);
	m_pWidget->GetController()->GetProperties()->SetHighlightHandleColor(fColor);
	this->Notify(MSG_HIGHLIGHT_COLOR_CHANGED);
	return true;
}
VistaColor VfaLineWidget::VfaLineWidgetProperties::GetHighlightHandleColor() const
{
	float fColor[3];
	m_pWidget->GetController()->GetProperties()->GetHighlightHandleColor(fColor);
	return VistaColor(fColor);
}
void VfaLineWidget::VfaLineWidgetProperties::SetHighlightHandleColor(float fC[4])
{
	m_pWidget->GetController()->GetProperties()->SetHighlightHandleColor(fC);
	this->Notify(MSG_NORMAL_COLOR_CHANGED);
}
void VfaLineWidget::VfaLineWidgetProperties::GetHighlightHandleColor(float fC[4]) const
{
	m_pWidget->GetController()->GetProperties()->GetHighlightHandleColor(fC);
}
/*============================================================================*/
/*  NAME      :   Get/SetNormalHandleColor	                                  */
/*============================================================================*/
bool VfaLineWidget::VfaLineWidgetProperties::SetNormalHandleColor(const VistaColor &c)
{
	float fColor[3];
	c.GetValues(fColor);
	m_pWidget->GetController()->GetProperties()->SetNormalHandleColor(fColor);
	this->Notify(MSG_NORMAL_COLOR_CHANGED);
	return true;
}
VistaColor VfaLineWidget::VfaLineWidgetProperties::GetNormalHandleColor() const
{
	float fColor[3];
	m_pWidget->GetController()->GetProperties()->GetNormalHandleColor(fColor);
	return VistaColor(fColor);
}
void VfaLineWidget::VfaLineWidgetProperties::SetNormalHandleColor(float fC[4])
{
	m_pWidget->GetController()->GetProperties()->SetNormalHandleColor(fC);
	this->Notify(MSG_NORMAL_COLOR_CHANGED);
}
void VfaLineWidget::VfaLineWidgetProperties::GetNormalHandleColor(float fC[4]) const
{
	m_pWidget->GetController()->GetProperties()->GetNormalHandleColor(fC);
}

/*============================================================================*/
/*  NAME      :   GetReflectionableType                                       */
/*============================================================================*/
std::string VfaLineWidget::VfaLineWidgetProperties::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*  NAME      :   AddToBaseTypeList                                           */
/*============================================================================*/
int VfaLineWidget::VfaLineWidgetProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}


/*============================================================================*/
/*  END OF FILE "VfaLineWidget.cpp"                                          */
/*============================================================================*/


