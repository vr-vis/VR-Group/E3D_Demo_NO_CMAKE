/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFALINEMODEL_H
#define _VFALINEMODEL_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetModelBase.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/*
 * There are two possibilities to set the model of the line:
 * - SetMidPoint(middle, extension1, extension2), which sets the center of the 
 *   line and relative to this point the extensions
 * - SetMidpoint(Array of length 3) + SetLength(float) + Rotate(quaternion), 
 *   which set the midpoint, length, orientation
 */
class VISTAFLOWLIBAUXAPI VfaLineModel : public VfaWidgetModelBase
{
public:
	enum{
		MSG_LINE_CHANGE = VfaWidgetModelBase::MSG_LAST,
		MSG_LAST
	};

	VfaLineModel();
	virtual ~VfaLineModel();

	void SetPoint1(float fPt[3]);
	void SetPoint1(double dPt[3]);
	void SetPoint1(const VistaVector3D &v3C);
	void GetPoint1(float fPt[3]) const;
	void GetPoint1(double dPt[3]) const;
	void GetPoint1(VistaVector3D &v3P) const;

	void SetPoint2(float fPt[3]);
	void SetPoint2(double dPt[3]);
	void SetPoint2(const VistaVector3D &v3C);
	void GetPoint2(float fPt[]) const;
	void GetPoint2(double dPt[]) const;
	void GetPoint2(VistaVector3D &v3P) const;

	void SetMidPoint(float fPt[3], float fPt1[3], float fPt2[3]);
	bool SetMidPoint(const std::vector<float> &fPts);	//awaits 3 or 9 arguments
	void SetMidPoint(double dPt[3], double dPt1[3], double dPt2[3]);
	void SetMidPoint(const VistaVector3D &v3C, const VistaVector3D &v3V1, const VistaVector3D &v3V2);
	void GetMidPoint(float fPt[3]) const;
	void GetMidPoint(double dPt[3]) const;
	void GetMidPoint(VistaVector3D &v3P) const;
	std::vector<float> GetMidPoint() const;

	bool SetLength(const float fL);
	float GetLength() const;

	bool Rotate(const VistaQuaternion &qRotation);

	virtual std::string GetReflectionableType() const;

protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	void ComputeMidPoint();
	void ComputeLength();

private:
	VistaVector3D m_vecPoint1;
	VistaVector3D m_vecPoint2;
	VistaVector3D m_vecMidPoint;
	
	float m_fLength;
	float m_fWidth;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaLineModel_H
