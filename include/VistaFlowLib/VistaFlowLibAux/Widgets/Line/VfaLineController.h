/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFALINECONTROLLER_H
#define _VFALINECONTROLLER_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VfaWidgetController.h"
#include <VistaAspects/VistaReflectionable.h>
#include <VistaAspects/VistaObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <vector>
#include <string>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
using namespace std;


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaVector3D;
class VistaQuaternion;
class VfaSphereHandle;
class VfaLineModel;
class VfaLineVis;
class VflRenderNode;
class VistaIndirectXform;
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Controll the behavior on button press event, button release event and 
 * pre-loop event, also highlight.
 * 
 * Note: This class should be reworked, since it has inconsistencies 
 * (regarding last line attribute) and a small bug occurring out of that
 * (at the first touch the line is in state undefined although it shouldn't).
 */
class VISTAFLOWLIBAUXAPI VfaLineController : public IVfaWidgetController, public IVistaObserver
{
public:
	VfaLineController (VfaLineModel *pModel, VflRenderNode *pRenderNode);
	virtual ~VfaLineController();

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	virtual void OnSensorSlotUpdate(int iSlot, const VfaApplicationContextObject::sSensorFrame &oFrameData);
	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);
	virtual void OnTimeSlotUpdate(const double dTime);

	/** 
	 * This is called on transition change into(out of) state FOCUS
	 */
	virtual void OnFocus(IVfaControlHandle* pHandle);
	virtual void OnUnfocus();

	/**
	 * This is called on transition change into(out of) state TOUCH
	 */
	virtual void OnTouch(const std::vector<IVfaControlHandle*>& vecHandles);
	virtual void OnUntouch();

	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots are used/handled by this controller
	 */
	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;

	bool GetIsInteracting() const;

	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetArtItemsVisible(bool b);
	bool GetAreItemsVisible();

	int GetDragButton() const;	
	void SetDragButton(int i);

	void SetShowLastState(bool b);
	bool GetShowLastState() const;

	void SetLastLineColor(const VistaColor& color);
	VistaColor GetLastLineColor() const;
	void SetLastLineColor(float f[4]);
	void GetLastLineColor(float f[4]) const;

	/* Set/Get whether the endpoints should be moved independently of each 
	 * other (instead of mirroring the other point correspondingly). */
	void SetMoveEndPointsIndependently(bool bIndependently);
	bool GetMoveEndPointsIndependently();

	//********** OBSERVER INTERFACE IMPLEMENTATION *********************//
	virtual bool ObserveableDeleteRequest(IVistaObserveable *pObserveable, int eTicket);
	virtual void ObserveableDelete(IVistaObserveable *pObserveable, int eTicket);
	virtual void ReleaseObserveable(IVistaObserveable *pObserveable, int eTicket);
	virtual bool Observes(IVistaObserveable *pObserveable);
	virtual void Observe(IVistaObserveable *pObservable, int eTicket=IVistaObserveable::TICKET_NONE);
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int iMsg, int iTicket);
	
	class VISTAFLOWLIBAUXAPI VfaLineControllerProps : public IVistaReflectionable
	{
		friend class VfaLineController;
	public:

		VfaLineControllerProps(VfaLineController *pLineCtrl);
		virtual ~VfaLineControllerProps();

		bool SetHandleRadius(float fRadius);
		float GetHandleRadius() const;

		bool SetHighlightHandleColor(const VistaColor& color);
		VistaColor GetHighlightHandleColor() const;
		void SetHighlightHandleColor(float f[4]);
		void GetHighlightHandleColor(float f[4]) const;

		bool SetNormalHandleColor(const VistaColor& color);
		VistaColor GetNormalHandleColor() const;
		void SetNormalHandleColor(float f[4]);
		void GetNormalHandleColor(float f[4]) const;

		bool SetHighlightMiddleHandleColor(const VistaColor& color);
		VistaColor GetHighlightMiddleHandleColor() const;
		void SetHighlightMiddleHandleColor(float f[4]);
		void GetHighlightMiddleHandleColor(float f[4]) const;

		bool SetNormalMiddleHandleColor(const VistaColor& color);
		VistaColor GetNormalMiddleHandleColor() const;
		void SetNormalMiddleHandleColor(float f[4]);
		void GetNormalMiddleHandleColor(float f[4]) const;

		bool SetSphereHandleRadius(const float fRadius);
		float GetSphereHandleRadius() const;

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		VfaLineController	*m_pLineCtrl;
	};

	VfaLineControllerProps* GetProperties() const;

protected:
	/**
	 * enum for internal state control
	 */
	enum EInternalState{
		LINE_UNDEFINED=0,
		LINE_DRAGGING,
		LINE_DEFINED,
		LINE_MOVING,
		LINE_MOVING_SPHERE,
		LINE_DISABLED,
	};

	void SetLineState(int iNewState);
	void PrintState(int iOldState, int iCurrentState) const;

private:
	VfaLineModel		*m_pModel;
	VflRenderNode		*m_pRenderNode;

	// handles for interaction
	vector<VfaSphereHandle*> m_pSphereHandles;

	// enhance controller state
	VfaLineVis			*m_pLastLine;

	VfaLineControllerProps		*m_pCtlProps;

	int m_iState;
	//cache the last known state before we went to out-of-focus
	int m_iLastState;
	int m_iDragButton;

	//enumeration for handles
	enum
	{
		HND_UNDEFINDED = -1,
		HND_SPHERE_LEFT = 0,
		HND_SPHERE_MIDDLE,
		HND_SPHERE_RIGHT
	};
	int m_iActiveHandle;

	bool m_bShowLastState;

	float m_fTranslateStartOffset[3];

	bool m_bMoveEndPointsIndependently;

	VistaIndirectXform			*m_pXform;
	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaLineController_H
