/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <GL/glew.h>

#include "VfaQuadVis.h"
#include "Plane/VfaPlaneModel.h"

#include <VistaAspects/VistaPropertyFunctor.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <VistaOGLExt/VistaTexture.h>

#include <cassert>
#include <cstring>

using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION															  */
/*============================================================================*/
VfaQuadVis::VfaQuadVis()
:	m_pModel(NULL)
,	m_pTexture(NULL)
{ }

VfaQuadVis::VfaQuadVis(VfaPlaneModel *pModel)
:	m_pModel(pModel)
,	m_pTexture(NULL)
{ }

VfaQuadVis::~VfaQuadVis()
{ }


VfaPlaneModel *VfaQuadVis::GetModel() const
{
	return m_pModel;
}

void VfaQuadVis::SetTexture(VistaTexture *pTex)
{
	m_pTexture = pTex;
}

VistaTexture *VfaQuadVis::GetTexture() const
{
	return m_pTexture;
}

void VfaQuadVis::DrawOpaque()
{
	//transparent rendering -> skip
	if(this->GetProperties()->GetOpacity() < 1.0f)
		return;

	this->DrawQuad();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VfaQuadVis::DrawTransparent()
{
	//opaque rendering -> skip
	if(this->GetProperties()->GetOpacity() >= 1.0f)
		return;

	this->DrawQuad();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaQuadVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_TRANSPARENT | IVflRenderable::OLI_DRAW_OPAQUE;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaQuadVis::VfaQuadVisProps *VfaQuadVis::GetProperties() const
{
	return static_cast<VfaQuadVis::VfaQuadVisProps *>(
		IVflRenderable::GetProperties());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaQuadVis::CreateProperties() const
{
	return new VfaQuadVis::VfaQuadVisProps();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawQuad                                                    */
/*                                                                            */
/*============================================================================*/
void VfaQuadVis::DrawQuad()
{
	glPushAttrib(GL_LIGHTING_BIT | GL_POINT_BIT | GL_LINE_BIT | GL_ENABLE_BIT);

	glDisable(GL_LIGHTING);
	// disable culling
	glDisable(GL_CULL_FACE);
	glDisable(GL_ALPHA_TEST);

	float fColor[4];
	VfaQuadVisProps *pProps = this->GetProperties();
	fColor[3] = pProps->GetOpacity();

	glLineWidth(pProps->GetLineWidth());
	glPushMatrix();

	//draw Plane
	float fNormal[3], fVertex[3];
	m_pModel->GetNormal(fNormal);
	//draw the filled plane itself
	if(pProps->GetFillPlane())
	{
		float afTexCoordsLimits[4];
		pProps->GetTextureCoordinates(afTexCoordsLimits);

		float fTexCoords[] = {
			afTexCoordsLimits[0], afTexCoordsLimits[2],
			afTexCoordsLimits[1], afTexCoordsLimits[2],
			afTexCoordsLimits[1], afTexCoordsLimits[3],
			afTexCoordsLimits[0], afTexCoordsLimits[3]
		};

		//if we have a texture  -> set it up now
		if(m_pTexture != NULL)
		{
			glPushAttrib(GL_ENABLE_BIT | GL_TEXTURE_BIT);
			glEnable(GL_TEXTURE_2D);
			//draw only the texture and do not blend with quad's color
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			//bind it
			m_pTexture->Bind();
		}
	
		pProps->GetFillColor(fColor);
		glColor3fv(fColor);

		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
		glBegin(GL_QUADS);
			glTexCoord2fv(&(fTexCoords[0]));
			glNormal3fv(fNormal);
			m_pModel->GetCorner(0, fVertex);
			glVertex3fv(fVertex);
			
			glTexCoord2fv(&(fTexCoords[2]));
			glNormal3fv(fNormal);
			m_pModel->GetCorner(1, fVertex);
			glVertex3fv(fVertex);
			
			glTexCoord2fv(&(fTexCoords[4]));
			glNormal3fv(fNormal);
			m_pModel->GetCorner(3, fVertex);
			glVertex3fv(fVertex);

			glTexCoord2fv(&(fTexCoords[6]));
			glNormal3fv(fNormal);
			m_pModel->GetCorner(2, fVertex);
			glVertex3fv(fVertex);
		glEnd();

		if(m_pTexture != NULL)
		{
			//restore texture state
			glPopAttrib();
		}
	}

	//draw the border if neccessary
	if(pProps->GetDrawBorder())
	{
		pProps->GetLineColor().GetValues(fColor);
		glColor3fv(fColor);
		glBegin(GL_LINE_LOOP);
			m_pModel->GetCorner(0, fVertex);
			glVertex3fv(fVertex);
			m_pModel->GetCorner(1, fVertex);
			glVertex3fv(fVertex);
			m_pModel->GetCorner(3, fVertex);
			glVertex3fv(fVertex);
			m_pModel->GetCorner(2, fVertex);
			glVertex3fv(fVertex);
		glEnd();
	}

	glPopMatrix();
	glPopAttrib();
}
/*============================================================================*/
/*  IMPLEMENTATION      VfaQuadVisProps								          */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaQuadVis::VfaQuadVisProps");
//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<bool, VfaQuadVis::VfaQuadVisProps>(
		"SHOWBORDER", STR_REF_TYPENAME,
		&VfaQuadVis::VfaQuadVisProps::GetDrawBorder),
	new TVistaPropertyGet<float, VfaQuadVis::VfaQuadVisProps>(
		"BORDERWIDTH", STR_REF_TYPENAME,
		&VfaQuadVis::VfaQuadVisProps::GetLineWidth),
	new TVistaPropertyGet<VistaColor, VfaQuadVis::VfaQuadVisProps>(
		"BORDERCOLOR", STR_REF_TYPENAME,
		&VfaQuadVis::VfaQuadVisProps::GetLineColor),
	NULL
};
//setter stuff
static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<bool, bool, VfaQuadVis::VfaQuadVisProps>(
		"SHOWBORDER", STR_REF_TYPENAME,
		&VfaQuadVis::VfaQuadVisProps::SetDrawBorder),
	new TVistaPropertySet<float, float, VfaQuadVis::VfaQuadVisProps>(
		"BORDERWIDTH", STR_REF_TYPENAME,
		&VfaQuadVis::VfaQuadVisProps::SetLineWidth),
	new TVistaPropertySet<const VistaColor&, VistaColor, VfaQuadVis::VfaQuadVisProps>(
		"BORDERCOLOR", STR_REF_TYPENAME,
		&VfaQuadVis::VfaQuadVisProps::SetLineColor),
	NULL 
};
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaQuadVis::VfaQuadVisProps::VfaQuadVisProps()
	:	m_fLineWidth(2.0f),
		m_bDrawBorder(true),
		m_bDrawFilled(false),
		m_fOpacity(1.0f)
{
	// blue border
	m_fLineColor[0] = m_fFillColor[0] = 0.0f;
	m_fLineColor[1] = m_fFillColor[1] = 0.0f;
	m_fLineColor[2] = m_fFillColor[2] = 1.0f;
	m_fLineColor[3] = m_fFillColor[3] = 1.0f;

	// tex coords init
	m_fTexCoordsMinS = 0.0f;
	m_fTexCoordsMaxS = 1.0f;
	m_fTexCoordsMinT = 0.0f;
	m_fTexCoordsMaxT = 1.0f;
}
VfaQuadVis::VfaQuadVisProps::~VfaQuadVisProps()
{

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLineColor                                            */
/*                                                                            */
/*============================================================================*/
bool VfaQuadVis::VfaQuadVisProps::SetLineColor(float fColor[4])
{
	if( fColor[0] == m_fLineColor[0] &&
		fColor[1] == m_fLineColor[1] &&
		fColor[2] == m_fLineColor[2] &&
		fColor[3] == m_fLineColor[3])
		return false;
	memcpy(m_fLineColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHANGED);
	return true;
}

bool VfaQuadVis::VfaQuadVisProps::SetLineColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	return this->SetLineColor(fC);
}
VistaColor VfaQuadVis::VfaQuadVisProps::GetLineColor() const
{
	return VistaColor(m_fLineColor);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFillColor                                            */
/*                                                                            */
/*============================================================================*/
bool VfaQuadVis::VfaQuadVisProps::SetFillColor(float fColor[4])
{
	if( fColor[0] == m_fFillColor[0] &&
		fColor[1] == m_fFillColor[1] &&
		fColor[2] == m_fFillColor[2] &&
		fColor[3] == m_fFillColor[3])
		return false;
	memcpy(m_fFillColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHANGED);
	return true;
}

bool VfaQuadVis::VfaQuadVisProps::SetFillColor(const VistaColor& color)
{
	float fC[4];
	color.GetValues(fC);
	return this->SetFillColor(fC);
}

void VfaQuadVis::VfaQuadVisProps::GetFillColor(float fColor[4]) const
{
	memcpy(fColor, m_fFillColor, 4*sizeof(float));
}

VistaColor VfaQuadVis::VfaQuadVisProps::GetFillColor() const
{
	return VistaColor(m_fFillColor);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLineWidth                                            */
/*                                                                            */
/*============================================================================*/
bool VfaQuadVis::VfaQuadVisProps::SetLineWidth(float fLineWidth)
{
	if(m_fLineWidth != fLineWidth)
	{
		m_fLineWidth = fLineWidth;
		return true;
	}
	this->Notify(MSG_LINE_WIDTH_CHANGED);
	return false;
}
float VfaQuadVis::VfaQuadVisProps::GetLineWidth() const
{
	return m_fLineWidth;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDrawBorder                                               */
/*                                                                            */
/*============================================================================*/
bool VfaQuadVis::VfaQuadVisProps::SetDrawBorder(bool b)
{
	if(b == m_bDrawBorder)
		return false;
	m_bDrawBorder = b;
	this->Notify(MSG_DRAW_BORDER_CHANGED);
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDrawBorder                                               */
/*                                                                            */
/*============================================================================*/
bool VfaQuadVis::VfaQuadVisProps::GetDrawBorder() const
{
	return m_bDrawBorder;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetFillPlane                                                */
/*                                                                            */
/*============================================================================*/
bool VfaQuadVis::VfaQuadVisProps::SetFillPlane(bool b)
{
	if(b == m_bDrawFilled)
		return false;

	m_bDrawFilled = b;
	this->Notify(MSG_FILL_PLANE_CHANGED);
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFillPlane                                                */
/*                                                                            */
/*============================================================================*/
bool VfaQuadVis::VfaQuadVisProps::GetFillPlane() const
{
	return m_bDrawFilled;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetOpacity                                                  */
/*                                                                            */
/*============================================================================*/
bool VfaQuadVis::VfaQuadVisProps::SetOpacity(float fAlpha)
{
	if(fAlpha==m_fOpacity)
		return false;

	m_fOpacity = fAlpha;
	this->Notify(MSG_OPACITY_CHANGED);
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetOpacity                                                  */
/*                                                                            */
/*============================================================================*/
float VfaQuadVis::VfaQuadVisProps::GetOpacity() const
{ 
	return m_fOpacity;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VfaQuadVis::VfaQuadVisProps::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VfaQuadVis::VfaQuadVisProps::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}

void VfaQuadVis::VfaQuadVisProps::SetTextureCoordinates( float fMinS, float fMaxS, float fMinT, float fMaxT )
{
	m_fTexCoordsMinS = fMinS;
	m_fTexCoordsMaxS = fMaxS;
	m_fTexCoordsMinT = fMinT;
	m_fTexCoordsMaxT = fMaxT;
}

void VfaQuadVis::VfaQuadVisProps::SetTextureCoordinates( float fCoords[4] )
{
	SetTextureCoordinates(fCoords[0], fCoords[1], fCoords[2], fCoords[3]);
}

void VfaQuadVis::VfaQuadVisProps::GetTextureCoordinates( float& fMinS, float& fMaxS, float& fMinT, float& fMaxT )
{
	fMinS = m_fTexCoordsMinS;
	fMaxS = m_fTexCoordsMaxS;
	fMinT = m_fTexCoordsMinT;
	fMaxT = m_fTexCoordsMaxT;
}

void VfaQuadVis::VfaQuadVisProps::GetTextureCoordinates( float fCoords[4] )
{
	GetTextureCoordinates(fCoords[0], fCoords[1], fCoords[2], fCoords[3]);
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaQuadVis.cpp"                                               */
/*============================================================================*/

