/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFALINEVIS_H
#define _VFALINEVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaVector3D;
class VistaColor;
class VfaLineModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A simple line defined by two points
 */
class VISTAFLOWLIBAUXAPI VfaLineVis : public IVflRenderable
{
public: 
	VfaLineVis(VfaLineModel *pModel);
	virtual ~VfaLineVis();

	VfaLineModel *GetModel() const;

	virtual void DrawOpaque();
	virtual unsigned int GetRegistrationMode() const;


	class VISTAFLOWLIBAUXAPI VfaLineVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:

		enum
		{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_LINE_WIDGET_CHG,
			MSG_LINE_WIDTH_CHG,
			MSG_LAST
		};

		VfaLineVisProps();
		virtual ~VfaLineVisProps();

		bool SetWidth(float f);
		float GetWidth() const;

		bool SetColor(float fColor[4]);
		bool SetColor(const VistaColor& color);
		void GetColor(float fColor[4]) const;
		VistaColor GetColor() const;

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		float m_fLineWidth;
		float m_fColor[4];
	};
	virtual VfaLineVisProps *GetProperties() const;

protected:
	VfaLineVis();
	virtual VflRenderableProperties* CreateProperties() const;
	


private:
	VfaLineModel *m_pModel;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFALINEVIS_H
