/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VfaSlotObserver.h"


/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
IVfaSlotObserver::IVfaSlotObserver()
{ }

IVfaSlotObserver::~IVfaSlotObserver()
{ }


void IVfaSlotObserver::OnSensorSlotUpdate( int iSlot,
	const VfaApplicationContextObject::sSensorFrame & oFrameData )
{ }

void IVfaSlotObserver::OnCommandSlotUpdate( int iSlot, const bool bState )
{ }

void IVfaSlotObserver::OnTimeSlotUpdate( const double dTime )
{ }

int IVfaSlotObserver::GetSensorMask() const
{
	return 0;
}

int IVfaSlotObserver::GetCommandMask() const
{
	return 0;
}

bool IVfaSlotObserver::GetTimeUpdate() const
{
	return false;
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
