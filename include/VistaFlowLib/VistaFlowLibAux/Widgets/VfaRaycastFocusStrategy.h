/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	IdeaVflRaycastFocusStrat.h   RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  IdeaVflBindings			     RR  R    WW  WW  TT  HH  HH  */
/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFARAYCASTFOCUSSTRATEGY_H
#define VFARAYCASTFOCUSSTRATEGY_H

/*============================================================================*/
/* INCLUDES														              */
/*============================================================================*/
#include "../VistaFlowLibAuxConfig.h"
#include "VfaFocusStrategy.h"

#include <list>


/*============================================================================*/
/* FORWARD DECLARATION											              */
/*============================================================================*/
class IVfaRaycastHandle;


/*============================================================================*/
/* CLASS DEFINITION												              */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaRaycastFocusStrategy : public IVfaFocusStrategy
{
public:
	VfaRaycastFocusStrategy();
	virtual ~VfaRaycastFocusStrategy();


	// *** IVfaFocusStrategy interface. ***
	virtual bool EvaluateFocus(std::vector<IVfaControlHandle*> &vecCtrlHandles);
	virtual void RegisterSelectable(IVfaWidgetController* pSelectable);
	virtual void UnregisterSelectable(IVfaWidgetController* pSelectable);

	// *** IVfaSlotObserver interface. ***
	virtual void OnSensorSlotUpdate(int iSlot,
		const VfaApplicationContextObject::sSensorFrame & oFrameData);
	virtual void OnCommandSlotUpdate(int iSlot, const bool bSet);
	virtual void OnTimeSlotUpdate(const double dTime);

	virtual int GetSensorMask() const;
	virtual int GetCommandMask() const;
	virtual bool GetTimeUpdate() const;

private:
	std::list<IVfaRaycastHandle*> m_liHandles;
	VfaApplicationContextObject::sSensorFrame m_oPointer;
};

#endif // Include guard.


/*============================================================================*/
/* END OF FILE													              */
/*============================================================================*/

