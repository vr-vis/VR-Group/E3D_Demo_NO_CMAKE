/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFABOXVIS_H
#define _VFABOXVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaBoxModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Simple box style visualization for showing a box which is supposed
 * to be defined by the client class.
 */
class VISTAFLOWLIBAUXAPI VfaBoxVis : public IVflRenderable
{
public: 
	/**
	 * NOTE:	A box vis is ALWAYS bound to a box model whose
	 *			data it draws. It will register as an observer of this model
	 *			automatically so client classes should perform another 
	 *			registration.
	 */
	VfaBoxVis(VfaBoxModel *pModel);
	virtual ~VfaBoxVis();

	VfaBoxModel *GetModel() const;

	virtual void DrawOpaque();

	virtual unsigned int GetRegistrationMode() const;
	
	//void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/**
	 *
	 */
	class VISTAFLOWLIBAUXAPI VfaBoxVisProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		enum{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_FILLED_FACE_CHG,
			MSG_DRAW_LIGHTING_CHG,
			MSG_LINE_WIDTH_CHG,
			MSG_LAST
		};

		VfaBoxVisProps();
		virtual ~VfaBoxVisProps();

		bool SetLineColor(float fColor[4]);
		bool SetLineColor(const VistaColor& color);
		void GetLineColor(float fColor[4]) const;
		VistaColor GetLineColor() const;

		bool SetFaceColor(float fColor[4]);
		bool SetFaceColor(const VistaColor& color);
		void GetFaceColor(float fColor[4]) const;
		VistaColor GetFaceColor() const;

		bool SetDrawFilledFaces(bool b);
		bool GetDrawFilledFaces() const;

		bool SetDrawWithLighting(bool b);
		bool GetDrawWithLighting() const;

		bool SetLineWidth(float fLineWidth);
		float GetLineWidth() const;

		virtual std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		float	m_fLineWidth;
		bool	m_bDrawFilledFaces;
		bool	m_bDrawWithLighting;
		float	m_fLineColor[4];
		float	m_fFaceColor[4];
		
	};

	//VfaBoxVisProps *GetProperties() const;
	virtual VfaBoxVisProps *GetProperties() const;

protected:
	VfaBoxVis();

	virtual VflRenderableProperties* CreateProperties() const;

private:
	VfaBoxModel *m_pModel;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFABOXVIS_H
