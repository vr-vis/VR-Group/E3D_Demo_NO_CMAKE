/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFABASEAPPLICATION_H
#define _VFABASEAPPLICATION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>
#include <string>
#include <VistaBase/VistaVectorMath.h>

#include <VistaBase/VistaVersion.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VistaSystem;
class IVistaExplicitCallbackInterface;
class IVistaTransformable;
class VistaTransformNode;

class VflVisController;
class VflRenderNode;
class VflAxisInfo;
class VflBoundsInfo;

class VistaLightNode;

class VfaSystemByContextUpdater;
class VfaDisplayEventObserver;
class VfaVistaSystemAbstraction;
class VistaVirtualPlatformAdapter;

class VistaUserPlatform;

class VfaApplicationContextObject;

class IVistaExplicitCallbackInterface;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VfaBaseApplication is a convenience class which creates a ready-to-use
 * ViSTA FlowLib based appp. It initializes 
 *
 *
 */
class VISTAFLOWLIBAUXAPI VfaBaseApplication
{
public:
	VfaBaseApplication(VistaSystem *pSystem,
		                  bool bCreateBoundsInfo=true,
						  bool bCreateAxisInfo=true,
						  VistaLightNode *pLight=NULL);
	virtual ~VfaBaseApplication();

	VflVisController* GetVisController() const;
	VflRenderNode* GetRenderNode() const;
	VistaTransformNode* GetFlowLibRoot() const;
	VfaApplicationContextObject* GetApplicationContext() const;
	IVistaTransformable* GetLightTransform() const;
	IVistaTransformable* GetVisRefPointTransform() const;
	VistaVirtualPlatformAdapter* GetCameraAdapter() const;

	VfaVistaSystemAbstraction* GetSystemAbstraction() const;
	VfaSystemByContextUpdater* GetUpdateHandler() const;

	bool RegisterAction(int nKey,
		                IVistaExplicitCallbackInterface *pAction,
						const std::string &strExplain = "");
	IVistaExplicitCallbackInterface *UnRegisterAction(int nKey);

	bool ResetTransformation();
	bool SetTargetBounds(const VistaVector3D &v3Min,
		const VistaVector3D &v3Max);
	bool GetTargetBounds(VistaVector3D &v3Min,
		VistaVector3D &v3Max) const;
	VistaSystem* GetVistaSystem() const;
private:
	VflVisController	*m_pController;
	VflRenderNode		*m_pRenderNode;
	VistaTransformNode *m_pFlowLibRoot;
	VflAxisInfo		*m_pAxisInfo;
	VflBoundsInfo		*m_pBoundsInfo;

	// interaction
	VfaSystemByContextUpdater      *m_pUpdateHandler;
	VfaDisplayEventObserver        *m_pDisplayUpdate;
	VfaVistaSystemAbstraction      *m_pSystemAbstraction;
	IVistaExplicitCallbackInterface *m_pCtrlScene;
	VistaVirtualPlatformAdapter *m_pCamAdapter;

	IVistaTransformable		*m_pVisRefPoint;

	IVistaTransformable		*m_pLightTransform;
	VistaLightNode         *m_pLight;

	VfaApplicationContextObject* m_pApplicationContext;

	std::vector<IVistaExplicitCallbackInterface *> m_vecKeyboardActions;

	VistaVector3D			m_v3TargetBoundsMin,
							m_v3TargetBoundsMax;
	VistaSystem *m_pSystem;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFABASEAPPLICATION_H



