/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAVISTASYSTEMABSTRACTION_H
#define _VFAVISTASYSTEMABSTRACTION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflSystemAbstraction.h>

#include <string>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaSystem;
class VistaDisplayManager;
class Vista2DText;
class VistaVector3D;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaVistaSystemAbstraction : public IVflSystemAbstraction
{
public:
	VfaVistaSystemAbstraction(VistaSystem *pSystem);

	class VISTAFLOWLIBAUXAPI VfaVista2DTextAdapter : public IVflSystemAbstraction::IVflOverlayText
	{
		friend class VfaVistaSystemAbstraction;
	public:
		virtual ~VfaVista2DTextAdapter();

		virtual bool GetPosition(float &x, float &y) const;
		virtual bool SetPosition(float x, float y);
		virtual bool GetText(std::string &sText) const;
		virtual bool SetText(const std::string &sText);
		virtual bool GetSize(float &fTextSize) const;
		virtual bool SetSize(float fTextSize);

		virtual bool SetEnabled(bool bEnableState);
		virtual bool GetEnabled() const;
		virtual bool GetColor(float &r, float &g, float &b) const;
		virtual bool SetColor(float r, float g, float b);

		virtual bool Update();
	private:
		VfaVista2DTextAdapter(Vista2DText *pText,
			                    VistaDisplayManager *pDm)
			: m_pVistaText(pText),
			  m_pDm(pDm)
		{}
		Vista2DText *m_pVistaText;
		VistaDisplayManager *m_pDm;
	};

	virtual double GetFrameClock() const;
	virtual float  GenerateRandomNumber(int nLowerBound, int nUpperBound) const;

	/**
	 * valid fonts are TYPEWRITER, SERIF, SANS
	 */
	virtual IVflOverlayText  *AddOverlayText( const std::string &sFontHint = "TYPEWRITER", 
												const std::string strViewportName = "" );
	virtual bool              DeleteOverlayText(IVflOverlayText  *);

	virtual bool IsLeader() const;
	virtual bool IsFollower() const;

	virtual std::string GetClientName() const;

protected:
	VfaVistaSystemAbstraction();
private:
	VistaSystem *m_pSystem;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFLVISTASYSTEMABSTRACTION_H

