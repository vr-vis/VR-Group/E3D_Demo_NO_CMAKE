

set( RelativeDir "." )
set( RelativeSourceGroup "Source Files" )
set( SubDirs DfnNodes Interaction Navigation Occlusion Widgets )

set( DirFiles
	VfaActiveSeeder.cpp
	VfaActiveSeeder.h
	VfaBaseApplication.cpp
	VfaBaseApplication.h
	VfaBasicActions.cpp
	VfaBasicActions.h
	VfaDisplayEventObserver.cpp
	VfaDisplayEventObserver.h
	VfaPointGenerator.h
	VfaSeeder.cpp
	VfaSeeder.h
	VfaSystemByContextUpdater.cpp
	VfaSystemByContextUpdater.h
	VfaSystemUpdateHandler.cpp
	VfaSystemUpdateHandler.h
	VfaVistaSystemAbstraction.cpp
	VfaVistaSystemAbstraction.h
	VistaFlowLibAuxConfig.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()

