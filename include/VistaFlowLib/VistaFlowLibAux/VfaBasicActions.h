/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFABASICACTIONS_H
#define _VFABASICACTIONS_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaAspects/VistaExplicitCallbackInterface.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/Tools/VflTimeInfo.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VflVisTiming;
class VveTimeMapper;
class VveTimeMapper;
class IVflRenderable;
class VfaPointerManager;
class VflVisParticles;
class VflVisVolume;

class VflRenderNode;

class IVfaWidget;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * A composite action triggers a list of previously registered actions.
 * For instance, if you need to execute a VfaToggleAnimation action and a 
 * VfaRenderableEnableToggle action with a key press simultaneously, 
 * just register both actions at a VfaCompositeAction and execute this composite on key press
 */
class VISTAFLOWLIBAUXAPI VfaCompositeAction : public IVistaExplicitCallbackInterface
{
public:
	VfaCompositeAction();

	virtual ~VfaCompositeAction();

	int RegisterAction (IVistaExplicitCallbackInterface* pAction);
	bool UnregisterAction (int iID);

	bool Do();
protected:
	std::vector<IVistaExplicitCallbackInterface*> m_vecActions;
};


// #############################################################################
// TIME MANIPULATION
// #############################################################################

/**
 * Base class for Time-Index manipulation in a step-wise action.
 * Needs an instance of a VisTiming(you usually get that from the
 * VisController you want to manipulate) and a TimeMapper, that
 * can map vis time to the current time index and back.
 */
class VISTAFLOWLIBAUXAPI IVfaTimeIndexStepAction : public IVistaExplicitCallbackInterface
{
public:
	IVfaTimeIndexStepAction(VflVisTiming *pTiming, 
		                    VveTimeMapper *pTime);

	virtual ~IVfaTimeIndexStepAction();

protected:
	VflVisTiming  *m_pVisTiming;
	VveTimeMapper *m_pTimeMapper;
};

/**
 * Moves "forward" in the time line.
 * Note that this implementation stops the animation-loop
 * of the given VisTiming.
 */
class VISTAFLOWLIBAUXAPI VfaTimeIndexForwardAction : public IVfaTimeIndexStepAction
{
public:
	VfaTimeIndexForwardAction(VflVisTiming *pTiming, 
		                    VveTimeMapper *pTime);
	bool Do();
};


/**
 * create an instance of this class to jump to a specific vis time
 * e.g. 0 for the start, 1 for the end of the animation
 */
class VISTAFLOWLIBAUXAPI VfaJumpToVisTimeAction : public IVfaTimeIndexStepAction
{
public:
    VfaJumpToVisTimeAction(VflVisTiming *pTiming,
                            float nVisTims);
    bool Do();

private:
    float m_nVisTime;
};

class VISTAFLOWLIBAUXAPI VfaChangeVisTimeAction : public IVfaTimeIndexStepAction
{
public:
    VfaChangeVisTimeAction(VflVisTiming *pTiming, double dDelta);
    bool Do();

private:
    double m_dDelta;
};


/**
 * Moves "backward" in the time line.
 * Note that this implementation stops the animation-loop
 * of the given VisTiming.
 */
class VISTAFLOWLIBAUXAPI VfaTimeIndexBackwardAction : public IVfaTimeIndexStepAction
{
public:
	VfaTimeIndexBackwardAction(VflVisTiming *pTiming, 
		                    VveTimeMapper *pTime);
	bool Do();
};

/**
 * Base class for loop time manipulation. A delta(in seconds)
 * can be given(always positive) and changed which is applied
 * to the VflVisTiming::SetLoopTime() method.
 */
class VISTAFLOWLIBAUXAPI VfaLoopTimeChange : public IVistaExplicitCallbackInterface
{
public:
	VfaLoopTimeChange(VflVisTiming *pVisTiming, double dDelta = 0.5);
	virtual ~VfaLoopTimeChange();

	double GetDelta() const;
	void  SetDelta(double dDelta);

	bool Do();
protected:
	VflVisTiming*	m_pVisTiming;
	double          m_dDelta;
};

/**
 * Increases/decreases the animation speed by a certain amount.
 */
class VISTAFLOWLIBAUXAPI VfaAnimationSpeedChange : public IVistaExplicitCallbackInterface
{
public:
	VfaAnimationSpeedChange(VflVisTiming *pVisTiming, double dDelta = 0.05);
	virtual ~VfaAnimationSpeedChange();

	double GetDelta() const;
	void  SetDelta(double dDelta);

	bool Do();
protected:
	VflVisTiming*	m_pVisTiming;
	double          m_dDelta;
};

/** 
 * Toggles the activate loop animation in vis timing.
 */
class VISTAFLOWLIBAUXAPI VfaToggleAnimation : public IVistaExplicitCallbackInterface
{
public:
	VfaToggleAnimation(VflVisTiming *pTiming);

	bool Do();

protected:
private:
	VflVisTiming *m_pTiming;
};
// #############################################################################
// RENDERER MANIPULATION
// #############################################################################


class VISTAFLOWLIBAUXAPI VfaForwardSwitchRendererAction : public IVistaExplicitCallbackInterface
{
public:
	VfaForwardSwitchRendererAction(VflVisParticles *pVis);
	virtual ~VfaForwardSwitchRendererAction();


	bool Do();

protected:
private:
	VflVisParticles *m_pVis;
};

/**
 * Switch volume renderer to the next render strategy
 */
class VISTAFLOWLIBAUXAPI VfaForwardSwitchVolumeRenderAction : public IVistaExplicitCallbackInterface
{
public:
	VfaForwardSwitchVolumeRenderAction(VflVisVolume *pVis);
	virtual ~VfaForwardSwitchVolumeRenderAction();


	bool Do();

protected:
private:
	VflVisVolume *m_pVis;
};


class VISTAFLOWLIBAUXAPI VfaRenderableEnableToggle : public IVistaExplicitCallbackInterface
{
public:
	VfaRenderableEnableToggle(IVflRenderable *pRen);
	
	bool Do();
private:
	IVflRenderable * m_pObj;
};




class VISTAFLOWLIBAUXAPI VfaRenderableTimeInfo : public IVistaExplicitCallbackInterface
{
public:
	VfaRenderableTimeInfo(VflTimeInfo *helper);

		bool Do();
private:
	 VflTimeInfo * m_phelper;
};


class VISTAFLOWLIBAUXAPI VfaNextPointer : public IVistaExplicitCallbackInterface
{
public:
	VfaNextPointer(VfaPointerManager *pPM);

		bool Do();
private:
	 VfaPointerManager * m_pPointerManager;
	 std::list<std::string> m_listPointerModes;
	 std::list<std::string>::iterator	m_sCurrentMode;
};

/** 
 * Dump the view paramters from the vis controller to a file.
 */
class VISTAFLOWLIBAUXAPI VfaDumpViewParams : public IVistaExplicitCallbackInterface
{
public:
	VfaDumpViewParams(VflRenderNode *pRenderNode, const std::string &strFName);
	virtual ~VfaDumpViewParams();

	virtual bool Do();
private:
	VflRenderNode *m_pRenderNode;
	std::string m_strFName;
};

/**
 * restore a viscontroller's viewing parameters from a given file.
 */
class VISTAFLOWLIBAUXAPI VfaRestoreViewParams : public IVistaExplicitCallbackInterface
{
public:
	VfaRestoreViewParams(VflRenderNode *pRenderNode, const std::string &strFName);
	virtual ~VfaRestoreViewParams();

	virtual bool Do();
private:
	VflRenderNode *m_pRenderNode;
	std::string m_strFName;
};

// #############################################################################
// NAVIGATION
// #############################################################################

class VISTAFLOWLIBAUXAPI VfaAxisRotate : public IVistaExplicitCallbackInterface
{
public:
	VfaAxisRotate(VflRenderNode* m_pController, 
                           const VistaVector3D& vecAxis, 
                           float fAngle);
	virtual ~VfaAxisRotate();

	bool Do();
private:
	VflRenderNode		*m_pRenderNode;
	VistaQuaternion	m_quatAxisAndAngle;
};


class VISTAFLOWLIBAUXAPI VfaScale : public IVistaExplicitCallbackInterface
{
public:
	VfaScale(VflRenderNode* pRenderNode, float fScale);
	virtual ~VfaScale();

	bool Do();
private:
	VflRenderNode		*m_pRenderNode;
	float				m_fScale;
};

// #############################################################################
// Widgets
// #############################################################################

class VISTAFLOWLIBAUXAPI VfaToggleWidget : public IVistaExplicitCallbackInterface
{
public:
	VfaToggleWidget(IVfaWidget* pWidget);
	virtual ~VfaToggleWidget();

	bool Do();
private:
	IVfaWidget*  m_pWidget;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFABASICACTIONS_H
