/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VDFNNONLINEARFUNCTIONNODE_H
#define _VDFNNONLINEARFUNCTIONNODE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaDataFlowNet/VdfnSerializer.h>
#include <VistaDataFlowNet/VdfnNode.h>
#include <VistaDataFlowNet/VdfnPort.h>

#include <VistaDataFlowNet/VdfnNodeFactory.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VdfnNonlinearFunctionNode : public IVdfnNode
{
public:
	/*
	* Computes an z-offset based on the current z-value of the input position.
	* If input_z is smaller than m_fLinearDistance, only a constant m_fOffset is added.
	* If input_z is larger than m_fLinearDistance, it is additionally increased by a non-linear
	* function m_fCoefficient*(|input_z|-m_fLinearDistance)^2, so in total m_fOffset + m_fCoefficient*(|input_z|-m_fLinearDistance)^2.
	*
	* The x,y, components of the output port are always zero.
	*
	* This node returns an offset only, so you are responsible for adding this offset to the desired position.
	* The reason is, that you have more control about the necessary transformations.
	*
	*
	* This node enables an easy implementation of the Go-Go (Poupyrev 1996) technique. If you use this offset for a virtual hand or cursor
	* and use the "real hand" position as the input vector, this node computes the offset of the "virtual hand".
	*
	*/

	VdfnNonlinearFunctionNode(float fLinearDistance, float fOffset, float fCoefficient);
	virtual ~VdfnNonlinearFunctionNode();

protected:
	virtual bool DoEvalNode();
	virtual bool PrepareEvaluationRun();

private:
	// distance from which on non-linear scaling occurs
	float m_fLinearDistance;

	// constant offset
	float m_fOffset;

	// scaling multiplier for non-linear behaviour
	float m_fCoefficient;

	TVdfnPort<VistaVector3D> *m_pInVec;
	TVdfnPort<VistaVector3D> *m_pOutVec;
};

class VISTAFLOWLIBAUXAPI VdfnNonlinearFunctionNodeCreate : public VdfnNodeFactory::IVdfnNodeCreator
{
public:
	VdfnNonlinearFunctionNodeCreate();
	virtual IVdfnNode *CreateNode( const VistaPropertyList &oParams ) const;
private:
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VDFNNONLINEARFUNCTIONNODE_H

