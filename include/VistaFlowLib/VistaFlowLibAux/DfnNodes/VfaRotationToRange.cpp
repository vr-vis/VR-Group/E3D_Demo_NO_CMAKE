/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VfaRotationToRange.h" 
#include <VistaDataFlowNet/VdfnUtil.h>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaRotationToRangeNode::VfaRotationToRangeNode()
:m_pValueOutPort(new TVdfnPort<double> ),
m_fOldAngle(-1.0)
{
	RegisterInPortPrototype( "ori_in", new TVdfnPortTypeCompare<TVdfnPort<VistaQuaternion> > );
	RegisterOutPort( "value", m_pValueOutPort );
}

VfaRotationToRangeNode::~VfaRotationToRangeNode()
{
}

bool VfaRotationToRangeNode::GetIsValid() const
{ 
	return (m_pOriInPort!=NULL);
}
bool VfaRotationToRangeNode::PrepareEvaluationRun()
{
	m_pOriInPort = VdfnUtil::GetInPortTyped<TVdfnPort<VistaQuaternion>*>( "ori_in", this );
	return GetIsValid();
}

bool VfaRotationToRangeNode::DoEvalNode()
{
	VistaVector3D vAxis(0,0,-1);

	const VistaQuaternion & quatOri = m_pOriInPort->GetValueConstRef();

	VistaVector3D v3 = quatOri.Rotate( vAxis );

	VistaVector3D v3Proj(v3[Vista::X], 0, v3[Vista::Z]); // project to x-z plane
	v3Proj.Normalize();

	double & dAngle = m_pValueOutPort->GetValueRef();
	dAngle = ::fabs(v3Proj.Dot(vAxis));

	dAngle = (1-dAngle);

	//vstr::outi() << "nIdxJump = " << nIdxJump << std::endl;
	if(v3Proj[Vista::X] < 0)
	{
		// differentiate between left / right
		// < 0 -> left
		dAngle = -1*dAngle;
	}

	if( fabs(dAngle) < 0.001)
	{
		dAngle = 0.0;		
	}
	if ((m_fOldAngle != dAngle)&&(dAngle != 0.0f))
	{
		m_pValueOutPort->IncUpdateCounter();
		m_fOldAngle = static_cast<float>(dAngle);
	}

	//vstr::outi() << "nAngle= " << nAngle << "(" << Vista::RadToDeg(::acos(nAngle)) << ") x " <<v3Proj[Vista::X]<< std::endl;

	return true;
}

/*============================================================================*/
/* Creator				                                                      */
/*============================================================================*/
VfaRotationToRangeNodeCreate::VfaRotationToRangeNodeCreate()
{ }

IVdfnNode *VfaRotationToRangeNodeCreate::CreateNode( const VistaPropertyList& ) const
{
	return new VfaRotationToRangeNode();
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

