/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFATIMENAVIGATIONNODE_H
#define _VFATIMENAVIGATIONNODE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaDataFlowNet/VdfnSerializer.h>
#include <VistaDataFlowNet/VdfnNode.h>
#include <VistaDataFlowNet/VdfnPort.h>
#include <VistaDataFlowNet/VdfnNodeFactory.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisTiming;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*
* This is a Dfn node to control the time management of a specific VflVisTiming
* It provides several in-ports, which can be connected with the data flow of your choice:
*
* boolean in-ports ( react to changes from false to true)
*	play			: start animation
*	stop			: pause animation
*	selectrangebegin: select current vis time as begin of the visualization time range
*	selectrangeend  : select current vis time as end of the visualization time range
*   selectrange		: select two visualization time instants which form a range; the first "true" marks the first instant,
*					  the second "true" marks the second instant, and the time model is told to restrict the
*					  vis scene to the time interval [first marker, second marker] 
*					  (where first and second are exchanged if first > second).
*					  To delete a range (i.e., select [0,1]), double set to "true", that is choose identical
*					  first and second values.

*	toggleanimation : change between start/stop
*
* double-valued inports:
*	absoluterate	: input should be a value between [-1,1], animation is shown at 10^value speed
*   absoluteposition: input should be a value between [0,1], sets the absolute visualization time
*   relativeposition: input should be a value between [-1,1], increases the visualization time by value
*					  wrapped into the valid visualization time interval [0,1]
*	TODO: should warp into [vis time start, vis time end]
*
*/
class VISTAFLOWLIBAUXAPI VfaTimeNavigationNode : public IVdfnNode
{
public:

	VfaTimeNavigationNode(VflVisTiming *pVisTiming);
	~VfaTimeNavigationNode ();

	virtual bool GetIsValid() const;
	virtual bool PrepareEvaluationRun();

protected:

	virtual bool   DoEvalNode();

private:
	VflVisTiming *m_pVisTiming;

	// boolean ports (see above for documentation)
	TVdfnPort<bool>*	m_pPlayPort;
	TVdfnPort<bool>*	m_pStopPort;
	TVdfnPort<bool>*	m_pToggleAnimationPort;
	TVdfnPort<bool>*	m_pSelectRangePort;
	TVdfnPort<bool>*	m_pSelectRangeBeginPort;
	TVdfnPort<bool>*	m_pSelectRangeEndPort;
	TVdfnPort<bool>*	m_pNextPort;
	TVdfnPort<bool>*	m_pPreviousPort;
	TVdfnPort<bool>*	m_pFirstPort;
	TVdfnPort<bool>*	m_pLastPort;

	// double ports (see above for documentation)
	TVdfnPort<double>*	m_pAbsoluteRatePort;
	TVdfnPort<double>*	m_pRelativePositionPort;
	TVdfnPort<double>*	m_pAbsolutePositionPort;

	// revision counters for change detection
	unsigned int		m_iPlayPortRevision;
	unsigned int		m_iStopPortRevision;
	unsigned int		m_iToggleAnimationPortRevision;
	unsigned int		m_iSelectRangePortRevision;
	unsigned int		m_iSelectRangeBeginPortRevision;
	unsigned int		m_iSelectRangeEndPortRevision;
	unsigned int		m_iNextPortRevision;
	unsigned int		m_iPreviousPortRevision;
	unsigned int		m_iFirstPortRevision;
	unsigned int		m_iLastPortRevision;
	unsigned int		m_iAbsoluteRatePortRevision;
	unsigned int		m_iAbsolutePositionPortRevision;
	unsigned int		m_iRelativePositionPortRevision;

	// tmp variables for m_pSelectRangePort
	// state variables if first interval point is already selected
	enum eSelectedRangeModes { NORANGE = 0, ONEPOINT };
	int					m_iSelectedRangeMode;
	// selected visualization time value
	double				m_dSelectedRangeValue;


};

class VISTAFLOWLIBAUXAPI VfaTimeNavigationNodeCreate : public VdfnNodeFactory::IVdfnNodeCreator
{
public:
	VfaTimeNavigationNodeCreate(VflVisTiming *pVisTiming);
	virtual IVdfnNode *CreateNode( const VistaPropertyList &oParams ) const;
private:
	VflVisTiming *m_pVisTiming;

};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFATIMENAVIGATIONNODE_H
