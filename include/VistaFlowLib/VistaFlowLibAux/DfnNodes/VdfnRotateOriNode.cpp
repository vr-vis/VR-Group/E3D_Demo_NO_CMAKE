/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <iostream>

#include "VdfnRotateOriNode.h"

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/


/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VdfnRotateOriNode::VdfnRotateOriNode() :
	m_pInQuat(NULL),
	m_pInRot(NULL),
	m_pOut(new TVdfnPort<VistaQuaternion>)
{
	RegisterInPortPrototype( "orientation", 
							 new TVdfnPortTypeCompare<TVdfnPort<VistaQuaternion> > );
	RegisterInPortPrototype( "rotation", 
							 new TVdfnPortTypeCompare<TVdfnPort<VistaQuaternion> > );

	RegisterOutPort( "orientation", m_pOut );
}

VdfnRotateOriNode::~VdfnRotateOriNode()
{
}
 
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
bool VdfnRotateOriNode::PrepareEvaluationRun()
{
	m_pInQuat = dynamic_cast<TVdfnPort<VistaQuaternion>*>(GetInPort("orientation"));
	m_pInRot = dynamic_cast<TVdfnPort<VistaQuaternion>*>(GetInPort("rotation"));
	return GetIsValid();
}


bool VdfnRotateOriNode::DoEvalNode()
{
// 	VistaQuaternion qRot = 
// 		m_pInRot->GetValueConstRef().GetComplexConjugated() *
// 		m_pInQuat->GetValueConstRef() *
// 		m_pInRot->GetValueConstRef();

	VistaQuaternion qRot = 
		m_pInRot->GetValueConstRef() *
		m_pInQuat->GetValueConstRef();

//	VistaQuaternion qRot = 
//		m_pInQuat->GetValueConstRef() *
//		m_pInRot->GetValueConstRef();

	qRot.Normalize();
	
	m_pOut->SetValue( qRot, GetUpdateTimeStamp() );

	return true;
}

VdfnRotateOriNodeCreate::VdfnRotateOriNodeCreate()
{
}

IVdfnNode *VdfnRotateOriNodeCreate::CreateNode( const VistaPropertyList &oParams ) const
{
	return new VdfnRotateOriNode();
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/


/*============================================================================*/
/* END OF FILE ""                                                             */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/
