/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAROTATIONTORANGE_H
#define _VFAROTATIONTORANGE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaDataFlowNet/VdfnSerializer.h>
#include <VistaDataFlowNet/VdfnNode.h>
#include <VistaDataFlowNet/VdfnPort.h>
#include <VistaDataFlowNet/VdfnNodeFactory.h>

#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*
*	This node takes an orientation as input and computes the angle between 0,0,-1 and
*   the vector obtained by applying the orientation onto 0,0,-1 (projected to x-z-plane).
*	The output is a value between [-1,1], which is 0 at null rotation; negative, if the orientation "rotates to 
*	the left"; positive else.
*
*/
class VISTAFLOWLIBAUXAPI VfaRotationToRangeNode : public IVdfnNode
{
public:
	

	VfaRotationToRangeNode();
	~VfaRotationToRangeNode();

	virtual bool PrepareEvaluationRun();
	virtual bool GetIsValid() const;

protected:
private:
	virtual bool   DoEvalNode();

	TVdfnPort<VistaQuaternion>*	m_pOriInPort;
	TVdfnPort<double>*	m_pValueOutPort;

	float		m_fOldAngle;
};

class VISTAFLOWLIBAUXAPI VfaRotationToRangeNodeCreate : public VdfnNodeFactory::IVdfnNodeCreator
{
public:
	VfaRotationToRangeNodeCreate();
	virtual IVdfnNode *CreateNode( const VistaPropertyList &oParams ) const;
private:

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFAROTATIONTORANGE_H
