/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VdfnNonlinearFunctionNode.h" 
#include <VistaAspects/VistaPropertyAwareable.h>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VdfnNonlinearFunctionNode::VdfnNonlinearFunctionNode(float fLinearDistance, float fOffset, float fCoefficient) :
	m_pInVec(NULL),
	m_pOutVec(new TVdfnPort<VistaVector3D>),
	m_fLinearDistance(fLinearDistance),
	m_fOffset(fOffset),
	m_fCoefficient(fCoefficient)
{
	RegisterInPortPrototype( "vector", 
							 new TVdfnPortTypeCompare<TVdfnPort<VistaVector3D> > );
	RegisterOutPort( "vector", m_pOutVec );

	if (m_fCoefficient<0.0f)
		m_fCoefficient=0.0f;
}

VdfnNonlinearFunctionNode::~VdfnNonlinearFunctionNode()
{
}
 
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
bool VdfnNonlinearFunctionNode::PrepareEvaluationRun()
{
	m_pInVec = dynamic_cast<TVdfnPort<VistaVector3D>*>(GetInPort("vector"));
	return GetIsValid();
}


bool VdfnNonlinearFunctionNode::DoEvalNode()
{
	// get input z component
	float fZ = m_pInVec->GetValueConstRef()[2];

	// add constant offset
	float fOutZ = m_fOffset;

	// if this distance is larger than m_fLinearDistance, increase offset
	if (fabs(fZ)>=m_fLinearDistance)
	{
		fOutZ += m_fCoefficient*pow(fabs(fZ)-m_fLinearDistance,2);
	}

	// return offset only
	VistaVector3D v3Output (0.0, 0.0, fOutZ);

	m_pOutVec->SetValue( v3Output,
					  GetUpdateTimeStamp() );						  

	return true;
}

VdfnNonlinearFunctionNodeCreate::VdfnNonlinearFunctionNodeCreate()
{
}

IVdfnNode *VdfnNonlinearFunctionNodeCreate::CreateNode( const VistaPropertyList &oParams ) const
{
	const VistaPropertyList &subs = oParams.GetSubListConstRef("param");
	float fD = subs.GetValueOrDefault<float>("distance", 0.0f);
	float fO = subs.GetValueOrDefault<float>("offset", 0.0f);
	float fC = subs.GetValueOrDefault<float>("coefficient", 0.0f);

	return new VdfnNonlinearFunctionNode(fD, fO, fC);
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "MYDEMO.CPP"                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

