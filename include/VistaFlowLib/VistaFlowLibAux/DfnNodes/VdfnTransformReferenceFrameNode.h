/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VDFNTRANSFORMREFERENCEFRAME_H
#define _VDFNTRANSFORMREFERENCEFRAME_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaDataFlowNet/VdfnSerializer.h>
#include <VistaDataFlowNet/VdfnNode.h>
#include <VistaDataFlowNet/VdfnPort.h>
#include <VistaDataFlowNet/VdfnNodeFactory.h>

#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/*
*  VdfnTransformReferenceFrame is a DFN node for indirect X transforms.
*  It requires a position and orientation input, as well as a "grab command"(bool).
*  As long as the grab flag is set, a registered transformable is transformed
*  by an indirect X transform.
*
*/
class VISTAFLOWLIBAUXAPI VdfnTransformReferenceFrameNode : public IVdfnNode
{
public:

	VdfnTransformReferenceFrameNode();
	virtual ~VdfnTransformReferenceFrameNode();

	
	virtual bool GetIsValid() const;
	virtual bool PrepareEvaluationRun();

	void SetDirection(bool bToFrame);

protected:

	virtual bool   DoEvalNode();

private:
	TVdfnPort<VistaQuaternion>      *m_pInOrientation;
	TVdfnPort<VistaVector3D>        *m_pInPosition;

	TVdfnPort<VistaQuaternion>      *m_pInRefFrameOrientation;
	TVdfnPort<VistaVector3D>        *m_pInRefFramePosition;

	TVdfnPort<VistaQuaternion>      *m_pOutOrientation;
	TVdfnPort<VistaVector3D>        *m_pOutPosition;

	VistaReferenceFrame			m_oReferenceFrame;
	bool							m_bToFrame;
};

/*
* Creator for nodes of type VdfnTransformReferenceFrame
*
*/
class VISTAFLOWLIBAUXAPI VdfnTransformReferenceFrameNodeCreate : public VdfnNodeFactory::IVdfnNodeCreator
{
public:
	VdfnTransformReferenceFrameNodeCreate();
	virtual IVdfnNode *CreateNode( const VistaPropertyList &oParams ) const;
private:
	
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VdfnTransformReferenceFrame_H
