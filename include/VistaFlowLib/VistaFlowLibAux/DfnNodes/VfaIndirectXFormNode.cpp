/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <iostream>

#include "VfaIndirectXFormNode.h" 

#include <VistaDataFlowNet/VdfnObjectRegistry.h>
#include <VistaDataFlowNet/VdfnUtil.h>

#include <VistaMath/VistaIndirectXform.h>

#include <VistaAspects/VistaTransformable.h>
#include <VistaAspects/VistaPropertyAwareable.h>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
const std::string VfaIndirectXFormNode::SPositionInPortName("position");
const std::string VfaIndirectXFormNode::SOrientationInPortName("orientation");
const std::string VfaIndirectXFormNode::SGrabCmdInPortName("grab");



VfaIndirectXFormNode::VfaIndirectXFormNode()
: IVdfnNode(), 
  m_pTransformable(NULL),
  m_bGrabbed(false),
  m_pXform(new VistaIndirectXform())
{
	RegisterInPortPrototype( SPositionInPortName, new TVdfnPortTypeCompare<TVdfnPort<VistaVector3D> > );
	RegisterInPortPrototype( SOrientationInPortName, new TVdfnPortTypeCompare<TVdfnPort<VistaQuaternion> > );
	RegisterInPortPrototype( SGrabCmdInPortName, new TVdfnPortTypeCompare<TVdfnPort<bool> > );

}

VfaIndirectXFormNode::~VfaIndirectXFormNode()
{
	delete m_pXform;
}

bool VfaIndirectXFormNode::GetIsValid() const
{ 
	return(m_pTransformable && m_pInGrab && m_pInOrientation && m_pInPosition);
}

bool VfaIndirectXFormNode::PrepareEvaluationRun()
{
	m_pInGrab   = dynamic_cast<TVdfnPort<bool>*>( GetInPort( SGrabCmdInPortName ) );
	m_pInOrientation = dynamic_cast<TVdfnPort<VistaQuaternion>*>( GetInPort( SOrientationInPortName ) );
	m_pInPosition    = dynamic_cast<TVdfnPort<VistaVector3D>*>( GetInPort( SPositionInPortName ) );

	return GetIsValid();
}


bool   VfaIndirectXFormNode::DoEvalNode()
{
	const VistaQuaternion &qOri = m_pInOrientation->GetValueConstRef();
	const VistaVector3D &vPos = m_pInPosition->GetValueConstRef();

	bool bReleased = false;
	bool bGrabIndicated = m_pInGrab->GetValueConstRef();

	if(!m_bGrabbed && bGrabIndicated)
	{
		float fPosition[3];
		m_pTransformable->GetTranslation(fPosition);
		float fRotation[4];
		m_pTransformable->GetRotation(fRotation[0], fRotation[1], fRotation[2], fRotation[3]);
		m_pXform->Init(vPos, qOri, VistaVector3D(fPosition), VistaQuaternion(fRotation));
		m_bGrabbed = true;
	}
	else
	if(m_bGrabbed && !bGrabIndicated)
	{
		// release grab
		bReleased = true;
	}

	// we only need position and/or orientation if the node is grabbed
	if(m_bGrabbed)
	{
		VistaVector3D vNewPos;
		VistaQuaternion qNewOri;
		m_pXform->Update(vPos, qOri, vNewPos, qNewOri);
		m_pTransformable->SetTranslation(vNewPos[0], vNewPos[1], vNewPos[2]);
		m_pTransformable->SetRotation(qNewOri[0],qNewOri[1],qNewOri[2],qNewOri[3]);
	}


	if(bReleased)
		m_bGrabbed = false;

	return true;
}

IVistaTransformable *VfaIndirectXFormNode::GetTransformTarget() const
{
	return m_pTransformable;
}

void VfaIndirectXFormNode::SetTransformTarget( IVistaTransformable *pTransform )
{
	m_pTransformable = pTransform;
}
/*============================================================================*/
/* Creator				                                                      */
/*============================================================================*/

VfaIndirectXFormNodeCreate::VfaIndirectXFormNodeCreate(VdfnObjectRegistry *pReg)
:m_pReg(pReg)
{
}

IVdfnNode *VfaIndirectXFormNodeCreate::CreateNode( const VistaPropertyList &oParams ) const
{
	VfaIndirectXFormNode *pNode = new VfaIndirectXFormNode( );

	const VistaPropertyList &subs = oParams.GetSubListConstRef("param");
	std::string strObj = subs.GetValueOrDefault<std::string>( "object", "" );
	IVistaTransformable *pObj =m_pReg->GetObjectTransform( strObj );
	if(pObj)
		pNode->SetTransformTarget(pObj);

	return pNode;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "MYDEMO.CPP"                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

