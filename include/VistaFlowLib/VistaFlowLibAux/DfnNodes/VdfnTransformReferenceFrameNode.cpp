/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <iostream>

#include "VdfnTransformReferenceFrameNode.h" 

#include <VistaDataFlowNet/VdfnObjectRegistry.h>
#include <VistaDataFlowNet/VdfnUtil.h>

#include <VistaAspects/VistaPropertyAwareable.h>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/


VdfnTransformReferenceFrameNode::VdfnTransformReferenceFrameNode()
: IVdfnNode(), 
  m_pInOrientation(NULL),
  m_pInPosition(NULL),
  m_pInRefFrameOrientation(NULL),
  m_pInRefFramePosition(NULL),
  m_pOutOrientation(new TVdfnPort<VistaQuaternion>),
  m_pOutPosition(new TVdfnPort<VistaVector3D>),
  m_bToFrame(false)
{
	RegisterInPortPrototype( "position", new TVdfnPortTypeCompare<TVdfnPort<VistaVector3D> > );
	RegisterInPortPrototype( "orientation", new TVdfnPortTypeCompare<TVdfnPort<VistaQuaternion> > );

	RegisterInPortPrototype( "refframe_position", new TVdfnPortTypeCompare<TVdfnPort<VistaVector3D> > );
	RegisterInPortPrototype( "refframe_orientation", new TVdfnPortTypeCompare<TVdfnPort<VistaQuaternion> > );

	RegisterOutPort( "position", m_pOutPosition );
	RegisterOutPort( "orientation", m_pOutOrientation );

}

VdfnTransformReferenceFrameNode::~VdfnTransformReferenceFrameNode()
{
	
}

bool VdfnTransformReferenceFrameNode::GetIsValid() const
{ 
	return(m_pInRefFrameOrientation && m_pInRefFramePosition && m_pInOrientation && m_pInPosition);
}

bool VdfnTransformReferenceFrameNode::PrepareEvaluationRun()
{
	m_pInOrientation = dynamic_cast<TVdfnPort<VistaQuaternion>*>( GetInPort( "orientation" ) );
	m_pInPosition    = dynamic_cast<TVdfnPort<VistaVector3D>*>( GetInPort( "position" ) );
	m_pInRefFrameOrientation = dynamic_cast<TVdfnPort<VistaQuaternion>*>( GetInPort( "refframe_orientation" ) );
	m_pInRefFramePosition    = dynamic_cast<TVdfnPort<VistaVector3D>*>( GetInPort( "refframe_position" ) );

	return GetIsValid();
}


bool   VdfnTransformReferenceFrameNode::DoEvalNode()
{
	const VistaQuaternion &qOri = m_pInOrientation->GetValueConstRef();
	const VistaVector3D &vPos = m_pInPosition->GetValueConstRef();

	VistaVector3D vecPos = vPos;
	VistaQuaternion quatOri = qOri;

	const VistaQuaternion &qRefOri = m_pInRefFrameOrientation->GetValueConstRef();
	const VistaVector3D &vRefPos = m_pInRefFramePosition->GetValueConstRef();
	m_oReferenceFrame.SetTranslation(vRefPos);
	m_oReferenceFrame.SetRotation(qRefOri);

	// we assume same scale here !

	if(m_bToFrame)
		m_oReferenceFrame.TransformToFrame(vecPos, quatOri);
	else
		m_oReferenceFrame.TransformFromFrame(vecPos, quatOri);

	quatOri.Normalize();
	m_pOutPosition->SetValue(vecPos, GetUpdateTimeStamp());
	m_pOutOrientation->SetValue(quatOri, GetUpdateTimeStamp());

	return true;
}

void VdfnTransformReferenceFrameNode::SetDirection(bool bToFrame)
{
	m_bToFrame = bToFrame;
}

/*============================================================================*/
/* Creator				                                                      */
/*============================================================================*/

VdfnTransformReferenceFrameNodeCreate::VdfnTransformReferenceFrameNodeCreate()
{
}

IVdfnNode *VdfnTransformReferenceFrameNodeCreate::CreateNode( const VistaPropertyList &oParams ) const
{
	VdfnTransformReferenceFrameNode *pNode = new VdfnTransformReferenceFrameNode( );

	const VistaPropertyList &subs = oParams.GetSubListConstRef("param");
	std::string strDir = subs.GetValueOrDefault<std::string>("dir", "");
	if(strDir == "to")
		pNode->SetDirection(true);
	if(strDir == "from")
		pNode->SetDirection(false);


	return pNode;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "MYDEMO.CPP"                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

