/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAINDIRECTXFORMNODE_H
#define _VFAINDIRECTXFORMNODE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaDataFlowNet/VdfnSerializer.h>
#include <VistaDataFlowNet/VdfnNode.h>
#include <VistaDataFlowNet/VdfnPort.h>
#include <VistaDataFlowNet/VdfnNodeFactory.h>

#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaObserveable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVistaTransformable;
class VdfnObjectRegistry;
class VistaIndirectXform;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/*
*  VfaIndirectXFormNode is a DFN node for indirect X transforms.
*  It requires a position and orientation input, as well as a "grab command"(bool).
*  As long as the grab flag is set, a registered transformable is transformed
*  by an indirect X transform.
*
*/
class VISTAFLOWLIBAUXAPI VfaIndirectXFormNode : public IVdfnNode
{
public:

	VfaIndirectXFormNode();
	virtual ~VfaIndirectXFormNode();

	IVistaTransformable *GetTransformTarget() const;
	void SetTransformTarget( IVistaTransformable * );

	virtual bool GetIsValid() const;
	virtual bool PrepareEvaluationRun();

	static const std::string SOrientationInPortName;
	static const std::string SPositionInPortName;
	static const std::string SGrabCmdInPortName;

protected:

	virtual bool   DoEvalNode();

private:
	bool							m_bGrabbed;

	VistaIndirectXform				*m_pXform;

	IVistaTransformable*	m_pTransformable;

	TVdfnPort<bool>					 *m_pInGrab;
	TVdfnPort<VistaQuaternion>      *m_pInOrientation;
	TVdfnPort<VistaVector3D>        *m_pInPosition;

};

/*
* Creator for nodes of type VfaIndirectXFormNode
*
*/
class VISTAFLOWLIBAUXAPI VfaIndirectXFormNodeCreate : public VdfnNodeFactory::IVdfnNodeCreator
{
public:
	VfaIndirectXFormNodeCreate(VdfnObjectRegistry *pReg);
	virtual IVdfnNode *CreateNode( const VistaPropertyList &oParams ) const;
private:
	VdfnObjectRegistry *m_pReg;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFAINDIRECTXFORMNODE_H
