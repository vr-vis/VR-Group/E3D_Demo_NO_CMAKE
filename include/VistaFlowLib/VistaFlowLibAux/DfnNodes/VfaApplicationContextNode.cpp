/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <iostream>

#include "VfaApplicationContextNode.h" 
#include "../Interaction/VfaApplicationContextObject.h" 

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaDataFlowNet/VdfnObjectRegistry.h>
#include <VistaDataFlowNet/VdfnUtil.h>

#include <VistaAspects/VistaPropertyAwareable.h>

#include <limits.h>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/



VfaApplicationContextNode::VfaApplicationContextNode()
: IVdfnNode(), 
  m_pObject(NULL),
  m_pTimePort(NULL),
  m_iTimePortRevision(0)
{
	RegisterInPortPrototype( "time",
		new TVdfnPortTypeCompare<TVdfnPort<double> > );
}

VfaApplicationContextNode::~VfaApplicationContextNode()
{
}

void VfaApplicationContextNode::SetApplicationContextObject(VfaApplicationContextObject* pObject)
{
	m_pObject= pObject;

	std::vector<std::string>  vecSensors;
	m_pObject->GetSensors(vecSensors);
	for(unsigned int i=0; i < vecSensors.size(); ++i)
	{
		this->RegisterSensorPort(i, vecSensors[i]);
	}
	std::vector<std::string>  vecCommands;
	m_pObject->GetCommands(vecCommands);
	for(unsigned int i=0; i < vecCommands.size(); ++i)
	{
		this->RegisterCommandPort(i, vecCommands[i]);
	}

}

bool VfaApplicationContextNode::RegisterCommandPort(int iSlot, const std::string& sCommandName)
{
	sCommandHlp oCmd;
	oCmd.sName = sCommandName;
	oCmd.pCommandPort = NULL;
	oCmd.iCmdIdx = iSlot;
	oCmd.iRevision = 0;

	// register in port
	RegisterInPortPrototype( oCmd.sName, new TVdfnPortTypeCompare<TVdfnPort<bool> > );

	if(static_cast<int>(m_vecCommandIds.size())<=iSlot)
		m_vecCommandIds.resize(iSlot+1);

	// and save command helper
	m_vecCommandIds[iSlot] = oCmd;

	return true;
}

bool VfaApplicationContextNode::RegisterSensorPort(int iSlot, const std::string& sSensorName)
{
	sSensorHlp oSensor;
	oSensor.sName = sSensorName;
	oSensor.pPositionPort = NULL;
	oSensor.pOrientationPort = NULL;
	oSensor.pValidPort = NULL;
	oSensor.sPositionPortName = sSensorName+"_pos";
	oSensor.sOrientationPortName = sSensorName+"_ori";
	oSensor.sValidPortName = sSensorName+"_valid";
	oSensor.iSensorIdx = iSlot;
	oSensor.iPositionRevision = 0;
	oSensor.iOrientationRevision = 0;

	// register in ports
	RegisterInPortPrototype( oSensor.sPositionPortName, new TVdfnPortTypeCompare<TVdfnPort<VistaVector3D> > );
	RegisterInPortPrototype( oSensor.sOrientationPortName, new TVdfnPortTypeCompare<TVdfnPort<VistaQuaternion> > );
	RegisterInPortPrototype( oSensor.sValidPortName, new TVdfnPortTypeCompare<TVdfnPort<bool> > );

	if(static_cast<int>(m_vecSensorPorts.size())<=iSlot)
		m_vecSensorPorts.resize(iSlot+1);

	// and save command helper
	m_vecSensorPorts[iSlot] = oSensor;
	
	return true;
}



bool VfaApplicationContextNode::GetIsValid() const
{ 
	return(m_pObject!=NULL);
}

bool VfaApplicationContextNode::PrepareEvaluationRun()
{
	// check and connect all sensor ports(positon & orientation)
	for(unsigned int i=0; i < m_vecSensorPorts.size(); ++i)
	{
		m_vecSensorPorts[i].pPositionPort = VdfnUtil::GetInPortTyped<TVdfnPort<VistaVector3D>*>( m_vecSensorPorts[i].sPositionPortName, this );
		m_vecSensorPorts[i].pOrientationPort = VdfnUtil::GetInPortTyped<TVdfnPort<VistaQuaternion>*>( m_vecSensorPorts[i].sOrientationPortName, this );
		m_vecSensorPorts[i].pValidPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( m_vecSensorPorts[i].sValidPortName, this );
	}
	// check and connect all command ports
	for(unsigned int i=0; i < m_vecCommandIds.size(); ++i)
	{
		m_vecCommandIds[i].pCommandPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( m_vecCommandIds[i].sName, this );
	}

	m_pTimePort = VdfnUtil::GetInPortTyped<TVdfnPort<double>*>( "time", this );
	return GetIsValid();
}


bool   VfaApplicationContextNode::DoEvalNode()
{
	for(unsigned int i=0; i < m_vecSensorPorts.size(); ++i)
	{
		// assume validity
		bool bValid = true;

		if(m_vecSensorPorts[i].pValidPort)
		{
			bValid = m_vecSensorPorts[i].pValidPort->GetValueConstRef();
		}

		if(bValid && m_vecSensorPorts[i].pPositionPort && m_vecSensorPorts[i].pOrientationPort)
		{
			const VistaVector3D &vPosIn = m_vecSensorPorts[i].pPositionPort->GetValueConstRef();
			const VistaQuaternion &qOriIn = m_vecSensorPorts[i].pOrientationPort->GetValueConstRef();

			// update ports, if revision has changed
			if((m_vecSensorPorts[i].pPositionPort->GetUpdateCounter()>m_vecSensorPorts[i].iPositionRevision)
					||(m_vecSensorPorts[i].pOrientationPort->GetUpdateCounter()>m_vecSensorPorts[i].iOrientationRevision))
			{
				m_pObject->SetSensorFrame(m_vecSensorPorts[i].iSensorIdx, vPosIn, qOriIn);

				m_vecSensorPorts[i].iPositionRevision = m_vecSensorPorts[i].pPositionPort->GetUpdateCounter();
				m_vecSensorPorts[i].iOrientationRevision = m_vecSensorPorts[i].pOrientationPort->GetUpdateCounter();
			}

			// Since we don't know when a RenderNode, and hence its local frame,
			// changes we'll generally trigger an update of all local ports here.
			m_pObject->UpdateLocalFramesTick(m_vecSensorPorts[i].iSensorIdx);
		}

	}

	for(unsigned int i=0; i < m_vecCommandIds.size(); ++i)
	{
		if(m_vecCommandIds[i].pCommandPort && m_vecCommandIds[i].pCommandPort->GetUpdateCounter()>m_vecCommandIds[i].iRevision)
		{
			
			const bool & bSet = m_vecCommandIds[i].pCommandPort->GetValueConstRef();

			m_pObject->SetCommandState(i, bSet);
			m_vecCommandIds[i].iRevision = m_vecCommandIds[i].pCommandPort->GetUpdateCounter();
		}
	}

	if(m_pTimePort &&(m_pTimePort->GetUpdateCounter() > m_iTimePortRevision))
	{
		const double & dTime = m_pTimePort->GetValueConstRef();
		m_pObject->SetTime(dTime);
		m_iTimePortRevision = m_pTimePort->GetUpdateCounter();
	}


	return true;
}

/*============================================================================*/
/* Creator				                                                      */
/*============================================================================*/

VfaApplicationContextNodeCreate::VfaApplicationContextNodeCreate(
	VdfnObjectRegistry *pReg)
: m_pReg(pReg)
{
}

IVdfnNode *VfaApplicationContextNodeCreate::CreateNode(
	const VistaPropertyList &oParams) const
{
	VfaApplicationContextNode *pNode = new VfaApplicationContextNode;

	const VistaPropertyList &subs = oParams.GetSubListConstRef("param");
	std::string strObj = subs.GetValueOrDefault<std::string>("object", "");
	VfaApplicationContextObject *pObj =
		dynamic_cast<VfaApplicationContextObject*>(m_pReg->GetObject(strObj));

	if(pObj)
		pNode->SetApplicationContextObject(pObj);

	return pNode;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "MYDEMO.CPP"                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

