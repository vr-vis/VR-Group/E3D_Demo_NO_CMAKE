/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <iostream>

#include "VfaTimeNavigationNode.h" 
#include <VistaFlowLib/Visualization/VflVisTiming.h>

#include <VistaDataFlowNet/VdfnUtil.h>
#include <VistaAspects/VistaPropertyAwareable.h>

#include <limits.h>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/



VfaTimeNavigationNode::VfaTimeNavigationNode(VflVisTiming *pVisTiming)
: IVdfnNode(), 
    m_pVisTiming(pVisTiming),
	m_iPlayPortRevision (0),
	m_iStopPortRevision (0),
	m_iSelectRangePortRevision (0),
	m_iSelectRangeBeginPortRevision (0),
	m_iSelectRangeEndPortRevision (0),
	m_iNextPortRevision (0),
	m_iPreviousPortRevision (0),
	m_iFirstPortRevision (0),
	m_iLastPortRevision (0),
	m_iAbsoluteRatePortRevision (0),
	m_iAbsolutePositionPortRevision (0),
	m_iRelativePositionPortRevision (0),
	m_iToggleAnimationPortRevision(0),
	m_iSelectedRangeMode (VfaTimeNavigationNode::NORANGE),
	m_dSelectedRangeValue(-1.0f)
{
	RegisterInPortPrototype( "play", new TVdfnPortTypeCompare<TVdfnPort<bool> > );
	RegisterInPortPrototype( "stop", new TVdfnPortTypeCompare<TVdfnPort<bool> > );
	RegisterInPortPrototype( "toggleanimation", new TVdfnPortTypeCompare<TVdfnPort<bool> > );
	RegisterInPortPrototype( "selectrange", new TVdfnPortTypeCompare<TVdfnPort<bool> > );
	RegisterInPortPrototype( "selectrangebegin", new TVdfnPortTypeCompare<TVdfnPort<bool> > );
	RegisterInPortPrototype( "selectrangeend", new TVdfnPortTypeCompare<TVdfnPort<bool> > );
	RegisterInPortPrototype( "next", new TVdfnPortTypeCompare<TVdfnPort<bool> > );
	RegisterInPortPrototype( "previous", new TVdfnPortTypeCompare<TVdfnPort<bool> > );
	RegisterInPortPrototype( "first", new TVdfnPortTypeCompare<TVdfnPort<bool> > );
	RegisterInPortPrototype( "last", new TVdfnPortTypeCompare<TVdfnPort<bool> > );

	RegisterInPortPrototype( "absoluteposition", new TVdfnPortTypeCompare<TVdfnPort<double> > );
	RegisterInPortPrototype( "relativeposition", new TVdfnPortTypeCompare<TVdfnPort<double> > );
	RegisterInPortPrototype( "absoluterate", new TVdfnPortTypeCompare<TVdfnPort<double> > );

}

VfaTimeNavigationNode::~VfaTimeNavigationNode()
{
}


bool VfaTimeNavigationNode::GetIsValid() const
{ 
	return (m_pVisTiming!=NULL);
}

bool VfaTimeNavigationNode::PrepareEvaluationRun()
{
	m_pPlayPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( "play", this );
	m_pStopPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( "stop", this );
	m_pSelectRangePort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( "selectrange", this );
	m_pSelectRangeBeginPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( "selectrangebegin", this );
	m_pSelectRangeEndPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( "selectrangeend", this );
	m_pToggleAnimationPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( "toggleanimation", this );
	m_pNextPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( "next", this );
	m_pPreviousPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( "previous", this );
	m_pFirstPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( "first", this );
	m_pLastPort = VdfnUtil::GetInPortTyped<TVdfnPort<bool>*>( "last", this );

	m_pAbsoluteRatePort = VdfnUtil::GetInPortTyped<TVdfnPort<double>*>( "absoluterate", this );
	m_pRelativePositionPort = VdfnUtil::GetInPortTyped<TVdfnPort<double>*>( "relativeposition", this );
	m_pAbsolutePositionPort = VdfnUtil::GetInPortTyped<TVdfnPort<double>*>( "absoluteposition", this );
	return GetIsValid();
}


bool   VfaTimeNavigationNode::DoEvalNode()
{
	if (m_pPlayPort && m_pPlayPort->GetUpdateCounter() > m_iPlayPortRevision)
	{
		const bool &bSet = m_pPlayPort->GetValueConstRef();
		if (bSet)
		{
			if (!m_pVisTiming->GetAnimationPlaying())
				m_pVisTiming->SetAnimationPlaying (true);
			m_iPlayPortRevision = m_pPlayPort->GetUpdateCounter();
		}
	}
	if (m_pStopPort && m_pStopPort->GetUpdateCounter() > m_iStopPortRevision)
	{
		const bool &bSet = m_pStopPort->GetValueConstRef();
		if (bSet)
		{
			if (m_pVisTiming->GetAnimationPlaying())
				m_pVisTiming->SetAnimationPlaying (false);
			m_iStopPortRevision = m_pStopPort->GetUpdateCounter();
		}
	}
	if (m_pToggleAnimationPort && m_pToggleAnimationPort->GetUpdateCounter() > m_iToggleAnimationPortRevision)
	{
		const bool &bSet = m_pToggleAnimationPort->GetValueConstRef();
		if (bSet)
		{
			m_pVisTiming->ToggleAnimation();
			m_iToggleAnimationPortRevision = m_pToggleAnimationPort->GetUpdateCounter();
		}
	}
	//if (m_pNextPort && m_pNextPort->GetUpdateCounter() > m_iNextPortRevision)
	//{
	//	const bool &bSet = m_pNextPort->GetValueConstRef();
	//	if (bSet)
	//	{
	//		if (!m_pVisTiming->GetAnimationPlaying())
	//			m_pVisTiming->SetAnimationPlaying (false);
	//		m_pVisTiming->SkipNext(m_sTarget);
	//		m_iNextPortRevision = m_pNextPort->GetUpdateCounter();
	//	}
	//}
	//if (m_pPreviousPort && m_pPreviousPort->GetUpdateCounter() > m_iPreviousPortRevision)
	//{
	//	const bool &bSet = m_pPreviousPort->GetValueConstRef();
	//	if (bSet)
	//	{
	//		if (!m_pVisTiming->GetAnimationPlaying())
	//			m_pVisTiming->SetAnimationPlaying (false);
	//		m_pVisTiming->SkipPrevious(m_sTarget);
	//		m_iPreviousPortRevision = m_pPreviousPort->GetUpdateCounter();
	//	}
	//}
	if (m_pFirstPort && m_pFirstPort->GetUpdateCounter() > m_iFirstPortRevision)
	{
		const bool &bSet = m_pFirstPort->GetValueConstRef();
		if (bSet)
		{
			if (!m_pVisTiming->GetAnimationPlaying())
				m_pVisTiming->SetAnimationPlaying (false);
			m_pVisTiming->First();
			m_iFirstPortRevision = m_pFirstPort->GetUpdateCounter();
		}
	}
	if (m_pLastPort && m_pLastPort->GetUpdateCounter() > m_iLastPortRevision)
	{
		const bool &bSet = m_pLastPort->GetValueConstRef();
		if (bSet)
		{
			if (!m_pVisTiming->GetAnimationPlaying())
				m_pVisTiming->SetAnimationPlaying (false);
			m_pVisTiming->Last();
			m_iLastPortRevision = m_pLastPort->GetUpdateCounter();
		}
	}
	if (m_pAbsolutePositionPort && m_pAbsolutePositionPort->GetUpdateCounter() > m_iAbsolutePositionPortRevision)
	{
		const double &dValue = m_pAbsolutePositionPort->GetValueConstRef();
		if (dValue>=0 && dValue<=1)
		{
			m_pVisTiming->SetAnimationPlaying (false);
			m_pVisTiming->SetVisualizationTime (dValue);
			m_iAbsolutePositionPortRevision = m_pAbsolutePositionPort->GetUpdateCounter();
		}
	}
	if (m_pAbsoluteRatePort && m_pAbsoluteRatePort->GetUpdateCounter() > m_iAbsoluteRatePortRevision)
	{
		const double &dValue = m_pAbsoluteRatePort->GetValueConstRef();
		if (dValue>=-1 && dValue<=1)
		{
			m_pVisTiming->SetAnimationPlaying (true);
			float fRateFactor = (float) pow (10.0, dValue);
			m_pVisTiming->SetAnimationSpeed (fRateFactor);
			m_iAbsoluteRatePortRevision = m_pAbsoluteRatePort->GetUpdateCounter();
		}
	}
	if (m_pRelativePositionPort && m_pRelativePositionPort->GetUpdateCounter() > m_iRelativePositionPortRevision)
	{
		const double &dValue = m_pRelativePositionPort->GetValueConstRef();
		if (dValue>=-1 && dValue<=1)
		{
			m_pVisTiming->SetAnimationPlaying (false);
			double dVisTime = m_pVisTiming->GetVisualizationTime ();
			dVisTime = fabs (fmod ((dVisTime+dValue), 1.0));
			m_pVisTiming->SetVisualizationTime (dVisTime);
			m_iRelativePositionPortRevision = m_pRelativePositionPort->GetUpdateCounter();
		}
	}
	if (m_pSelectRangeBeginPort && m_pSelectRangeBeginPort->GetUpdateCounter() > m_iSelectRangeBeginPortRevision)
	{
		const bool &bSet = m_pSelectRangeBeginPort->GetValueConstRef();
		if (bSet)
		{
			double dCurrentVisTime = m_pVisTiming->GetVisualizationTime ();
			double dStart, dEnd;
			m_pVisTiming->GetVisTimeRange (dStart, dEnd);
			m_pVisTiming->SetVisTimeRange (dCurrentVisTime, dEnd, true);
			m_iSelectRangeBeginPortRevision = m_pSelectRangeBeginPort->GetUpdateCounter();
		}
	}
	if (m_pSelectRangeEndPort && m_pSelectRangeEndPort->GetUpdateCounter() > m_iSelectRangeEndPortRevision)
	{
		const bool &bSet = m_pSelectRangeEndPort->GetValueConstRef();
		if (bSet)
		{
			double dCurrentVisTime = m_pVisTiming->GetVisualizationTime ();
			double dStart, dEnd;
			m_pVisTiming->GetVisTimeRange (dStart, dEnd);
			m_pVisTiming->SetVisTimeRange (dCurrentVisTime, dEnd, true);
			m_iSelectRangeEndPortRevision = m_pSelectRangeEndPort->GetUpdateCounter();
		}
	}
	if (m_pSelectRangePort && m_pSelectRangePort->GetUpdateCounter() > m_iSelectRangePortRevision)
	{
		const bool &bSet = m_pSelectRangePort->GetValueConstRef();
		if (bSet)
		{
			double dCurrentVisTime = m_pVisTiming->GetVisualizationTime ();
			switch (m_iSelectedRangeMode)
			{
			case NORANGE : m_dSelectedRangeValue = dCurrentVisTime; 
						   m_iSelectedRangeMode = ONEPOINT;
						   vstr::outi() << "[VfaTimeNavigationNode] First interval point "<<m_dSelectedRangeValue<<" selected\n";
						   break;
			case ONEPOINT : if (m_dSelectedRangeValue != dCurrentVisTime)
							{
								if (m_dSelectedRangeValue<dCurrentVisTime)
									m_pVisTiming->SetVisTimeRange (m_dSelectedRangeValue, dCurrentVisTime, true);
								else
									m_pVisTiming->SetVisTimeRange (dCurrentVisTime, m_dSelectedRangeValue, true);
								vstr::outi() << "[VfaTimeNavigationNode] Range "<<m_dSelectedRangeValue<<" to "<<dCurrentVisTime<<" selected\n";
							}
							else
							{
								m_pVisTiming->SetVisTimeRange (0.0,1.0,true);
								vstr::outi() << "[VfaTimeNavigationNode] Deleted range\n";
							}
							m_dSelectedRangeValue = -1.0f;
							m_iSelectedRangeMode = NORANGE;
							break;
			}
			m_iSelectRangePortRevision = m_pSelectRangePort->GetUpdateCounter();
		}
	}

	return true;
}

/*============================================================================*/
/* Creator				                                                      */
/*============================================================================*/

VfaTimeNavigationNodeCreate::VfaTimeNavigationNodeCreate(VflVisTiming *pVisTiming)
:m_pVisTiming(pVisTiming)
{
}

IVdfnNode *VfaTimeNavigationNodeCreate::CreateNode( const VistaPropertyList &oParams ) const
{
	VfaTimeNavigationNode *pNode = new VfaTimeNavigationNode( m_pVisTiming);

	return pNode;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "MYDEMO.CPP"                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

