/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <iostream>

#include "VdfnRotateVectorNode.h"

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/


/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VdfnRotateVectorNode::VdfnRotateVectorNode() :
	m_pInVec(NULL),
	m_pInRot(NULL),
	m_pOut(new TVdfnPort<VistaVector3D>)
{
	RegisterInPortPrototype( "vector", 
							 new TVdfnPortTypeCompare<TVdfnPort<VistaVector3D> > );
	RegisterInPortPrototype( "rotation", 
							 new TVdfnPortTypeCompare<TVdfnPort<VistaQuaternion> > );

	RegisterOutPort( "vector", m_pOut );
}

VdfnRotateVectorNode::~VdfnRotateVectorNode()
{
}
 
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
bool VdfnRotateVectorNode::PrepareEvaluationRun()
{
	m_pInVec = dynamic_cast<TVdfnPort<VistaVector3D>*>(GetInPort("vector"));
	m_pInRot = dynamic_cast<TVdfnPort<VistaQuaternion>*>(GetInPort("rotation"));
	return GetIsValid();
}


bool VdfnRotateVectorNode::DoEvalNode()
{
	m_pOut->SetValue( m_pInRot->GetValueConstRef().Rotate(
						  m_pInVec->GetValueConstRef()),
					  GetUpdateTimeStamp() );						  

	return true;
}

VdfnRotateVectorNodeCreate::VdfnRotateVectorNodeCreate()
{
}

IVdfnNode *VdfnRotateVectorNodeCreate::CreateNode( const VistaPropertyList &oParams ) const
{
	return new VdfnRotateVectorNode();
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/


/*============================================================================*/
/* END OF FILE ""                                                             */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/
