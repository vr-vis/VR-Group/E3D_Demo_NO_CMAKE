/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAAPPLICATIONCONTEXTNODE_H
#define _VFAAPPLICATIONCONTEXTNODE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaDataFlowNet/VdfnSerializer.h>
#include <VistaDataFlowNet/VdfnNode.h>
#include <VistaDataFlowNet/VdfnPort.h>
#include <VistaDataFlowNet/VdfnNodeFactory.h>

#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaObserveable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaApplicationContextObject;
class VdfnObjectRegistry;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaApplicationContextNode : public IVdfnNode
{
public:
	VfaApplicationContextNode();
	~VfaApplicationContextNode();

	void SetApplicationContextObject(VfaApplicationContextObject* pObject);

	virtual bool GetIsValid() const;
	virtual bool PrepareEvaluationRun();

protected:
	bool RegisterCommandPort(int iSlot, const std::string& sCommandName);
	bool RegisterSensorPort(int iSlot, const std::string& sSensorName);

	virtual bool   DoEvalNode();

private:
	VistaReferenceFrame			m_oVisRefFrame;

	VfaApplicationContextObject	*m_pObject;

	struct sSensorHlp {
		int								iSensorIdx;
		TVdfnPort<VistaVector3D>*		pPositionPort;
		TVdfnPort<VistaQuaternion>*	pOrientationPort;
		TVdfnPort<bool>*				pValidPort;
		std::string						sName;
		std::string						sPositionPortName;
		std::string						sOrientationPortName;
		std::string						sValidPortName;
		unsigned int					iPositionRevision;
		unsigned int					iOrientationRevision;
	};
	std::vector<sSensorHlp> m_vecSensorPorts;

	struct sCommandHlp {
		int								iCmdIdx;
		TVdfnPort<bool>*				pCommandPort;
		std::string						sName;
		unsigned int					iRevision;
	};
	std::vector<sCommandHlp>		m_vecCommandIds;

	TVdfnPort<double>*				m_pTimePort;
	unsigned int					m_iTimePortRevision;
};

class VISTAFLOWLIBAUXAPI VfaApplicationContextNodeCreate : public VdfnNodeFactory::IVdfnNodeCreator
{
public:
	VfaApplicationContextNodeCreate(VdfnObjectRegistry *pReg);
	virtual IVdfnNode *CreateNode( const VistaPropertyList &oParams ) const;

private:
	VdfnObjectRegistry *m_pReg;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_RAYTRANSFORM_H
