/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VDFNVALUETOTRIGGERNODE_H
#define _VDFNVALUETOTRIGGERNODE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <iostream>

#include <VistaDataFlowNet/VdfnConfig.h>

#include <VistaDataFlowNet/VdfnNode.h>
#include <VistaDataFlowNet/VdfnPort.h>
#include <VistaDataFlowNet/VdfnNodeFactory.h>
#include <VistaDataFlowNet/VdfnUtil.h>

#include <VistaAspects/VistaPropertyAwareable.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

template<class T>
class VdfnValueToTriggerNode : public IVdfnNode
{
public:
	typedef T(*ToValueFct)(const std::string &);
	typedef std::map<T,std::pair<std::string,TVdfnPort<bool>* > > MapType;

	VdfnValueToTriggerNode(MapType map) 
		: IVdfnNode(),
		  m_pIn(NULL)
	{
		m_mMap = map;

		for( typename MapType::iterator it = m_mMap.begin() ; 
			 it != m_mMap.end() ; it++ )
		{
			TVdfnPort<bool> *pPort = new TVdfnPort<bool>;
			(*it).second.second = pPort;
			RegisterOutPort((*it).second.first, pPort );
		}

		RegisterInPortPrototype( "value", new TVdfnPortTypeCompare<TVdfnPort<T> >);
	}


	virtual ~VdfnValueToTriggerNode() 
	{
		// IVdfnNode destructor already deletes out-ports for us!
		/*for( typename MapType::iterator it = m_mMap.begin() ; 
			 it != m_mMap.end() ; it++ )
		{
			TVdfnPort<bool> *pPort =(*it).second.second;
			delete pPort;
			}*/
	}

	bool PrepareEvaluationRun()
	{
		m_pIn = VdfnUtil::GetInPortTyped<TVdfnPort<T>*>("value", this);
		return GetIsValid();
	}
protected:
	bool DoEvalNode()
	{
/*		typename MapType::iterator it = m_mMap.find(m_pIn->GetValueConstRef());
		if( it != m_mMap.end() )
		{
			TVdfnPort<bool> *pPort =(*it).second.second;
			pPort->GetValueRef() = true;
			pPort->IncUpdateCounter();
			return true;
		}
		return false;
*/
		for( typename MapType::iterator it = m_mMap.begin() ;
			 it != m_mMap.end() ; it++ )
		{
			TVdfnPort<bool> *pPort =(*it).second.second;
			if((*it).first == m_pIn->GetValueConstRef() )
			{
#ifdef DEBUG
//				vstr::debugi() << "dispatching value to outport " <<(*it).second.first 
//						  << std::endl;
#endif
				if(pPort->GetValueConstRef() == false)
					pPort->SetValue( true, GetUpdateTimeStamp() );				
			}
			else
			{
				if(pPort->GetValueConstRef() == true)
					pPort->SetValue( false, GetUpdateTimeStamp() );
			}
		}
		return true;
	}
private:
	TVdfnPort<T>  *m_pIn;

    // maps values of type T to a name and corresponding outport
	MapType m_mMap; 
};

template<class T>
class VdfnValueToTriggerNodeCreate : public VdfnNodeFactory::IVdfnNodeCreator
{
public:
	typedef typename VdfnValueToTriggerNode<T>::ToValueFct ToValueFct;
	VdfnValueToTriggerNodeCreate(ToValueFct fct)
		: m_CFct(fct)
	{}

	virtual IVdfnNode *CreateNode( const VistaPropertyList &oParams ) const
	{
		const VistaPropertyList &oSubs = oParams.GetSubListConstRef("param");

		typename VdfnValueToTriggerNode<T>::MapType map;

		VistaProperty oMapping = oSubs("mapping");
		if(oMapping.GetPropertyType() == VistaProperty::PROPT_PROPERTYLIST)
		{
			for(VistaPropertyList::const_iterator cit = oMapping.GetPropertyListConstRef().begin();
				cit != oMapping.GetPropertyListConstRef().end(); ++cit)
				{
					T value = m_CFct((*cit).second.GetValue());
					std::string name =(*cit).first;

					map[ value ].first = name;

#ifdef DEBUG
					vstr::debugi() << "[ValueToTrigger] registered outport " << name
							  << " for value " << value << std::endl;
#endif
				}
		}	
		else
		{
			std::list<std::string> liStrings;
			if( oSubs.GetValue("mapping", liStrings) )
			{
				std::list<std::string>::const_iterator it = liStrings.begin();
				while( it != liStrings.end() )
				{
					T value = m_CFct(*it);
					it++;
					map[value].first = *it;
					it++;
				}
			}
		}
		return new VdfnValueToTriggerNode<T>(map);
	}

private:
	ToValueFct m_CFct;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VDFNVALUETOTRIGGERNODE_H

