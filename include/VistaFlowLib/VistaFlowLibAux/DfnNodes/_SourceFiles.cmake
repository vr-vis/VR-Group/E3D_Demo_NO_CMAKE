

set( RelativeDir "./DfnNodes" )
set( RelativeSourceGroup "Source Files\\DfnNodes" )

set( DirFiles
	VdfnAdditionNode.cpp
	VdfnAdditionNode.h
	VdfnMultiplicationNode.cpp
	VdfnMultiplicationNode.h
	VdfnNonlinearFunctionNode.cpp
	VdfnNonlinearFunctionNode.h
	VdfnRotateOriNode.cpp
	VdfnRotateOriNode.h
	VdfnRotateVectorNode.cpp
	VdfnRotateVectorNode.h
	VdfnTransformReferenceFrameNode.cpp
	VdfnTransformReferenceFrameNode.h
	VdfnValueToTriggerNode.cpp
	VdfnValueToTriggerNode.h
	VfaApplicationContextNode.cpp
	VfaApplicationContextNode.h
	VfaIndirectXFormNode.cpp
	VfaIndirectXFormNode.h
	VfaRotationToRange.cpp
	VfaRotationToRange.h
	VfaTimeNavigationNode.cpp
	VfaTimeNavigationNode.h
	VfaVisControllerTransformerNode.cpp
	VfaVisControllerTransformerNode.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

