/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VDFNADDITIONNODE_H
#define _VDFNADDITIONNODE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaDataFlowNet/VdfnConfig.h>

#include <VistaDataFlowNet/VdfnNode.h>
#include <VistaDataFlowNet/VdfnPort.h>
#include <VistaDataFlowNet/VdfnNodeFactory.h>
#include <VistaDataFlowNet/VdfnUtil.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

template<class tLVal, class tRVal>
class VdfnAdditionNode : public IVdfnNode
{
public:
	VdfnAdditionNode() 
		: IVdfnNode(),
		  m_pLVal(NULL),
		  m_pRVal(NULL),
		  m_pRet(new TVdfnPort<tLVal>)
	{
		RegisterInPortPrototype( "l_value", new TVdfnPortTypeCompare<TVdfnPort<tLVal> >);
		RegisterInPortPrototype( "r_value", new TVdfnPortTypeCompare<TVdfnPort<tRVal> >);
		RegisterOutPort( "value", m_pRet );
	}


	virtual ~VdfnAdditionNode() {}

	bool PrepareEvaluationRun()
	{
		m_pLVal = VdfnUtil::GetInPortTyped<TVdfnPort<tLVal>*>("l_value", this);
		m_pRVal = VdfnUtil::GetInPortTyped<TVdfnPort<tRVal>*>("r_value", this);
		return GetIsValid();
	}
protected:
	bool DoEvalNode()
	{
		m_pRet->GetValueRef() = 
			m_pLVal->GetValueConstRef() + m_pRVal->GetValueConstRef();
		m_pRet->IncUpdateCounter();	
		return true;
	}
private:
	TVdfnPort<tLVal> *m_pLVal;
	TVdfnPort<tRVal> *m_pRVal;
	TVdfnPort<tLVal> *m_pRet;
};

template<class tLVal, class tRVal>
class VdfnAdditionNodeCreate : public VdfnNodeFactory::IVdfnNodeCreator
{
public:
	VdfnAdditionNodeCreate()
		: VdfnNodeFactory::IVdfnNodeCreator()
	{}

	virtual IVdfnNode *CreateNode( const VistaPropertyList &oParams ) const
	{
		return new VdfnAdditionNode<tLVal, tRVal>();
	}

private:
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VDFNADDITIONNODE_H

