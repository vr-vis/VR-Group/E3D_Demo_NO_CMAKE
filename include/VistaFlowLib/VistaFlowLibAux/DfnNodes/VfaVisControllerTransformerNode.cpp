/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <iostream>

#include "VfaVisControllerTransformerNode.h" 

#include <VistaDataFlowNet/VdfnObjectRegistry.h>
#include <VistaDataFlowNet/VdfnUtil.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaMath/VistaIndirectXform.h>

#include <VistaAspects/VistaTransformable.h>
#include <VistaAspects/VistaPropertyAwareable.h>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
const std::string VfaVisControllerTransformerNode::SPositionInPortName("position");
const std::string VfaVisControllerTransformerNode::SOrientationInPortName("orientation");
const std::string VfaVisControllerTransformerNode::SGrabCmdInPortName("grab");



VfaVisControllerTransformerNode::VfaVisControllerTransformerNode()
: IVdfnNode(), 
  m_pRenderNode(NULL),
  m_bGrabbed(false),
  m_pXform(new VistaIndirectXform())
{
	RegisterInPortPrototype( SPositionInPortName, new TVdfnPortTypeCompare<TVdfnPort<VistaVector3D> > );
	RegisterInPortPrototype( SOrientationInPortName, new TVdfnPortTypeCompare<TVdfnPort<VistaQuaternion> > );
	RegisterInPortPrototype( SGrabCmdInPortName, new TVdfnPortTypeCompare<TVdfnPort<bool> > );

}

VfaVisControllerTransformerNode::~VfaVisControllerTransformerNode()
{
	delete m_pXform;
}

bool VfaVisControllerTransformerNode::GetIsValid() const
{ 
	return(m_pRenderNode && m_pInGrab && m_pInOrientation && m_pInPosition);
}

bool VfaVisControllerTransformerNode::PrepareEvaluationRun()
{
	m_pInGrab   = dynamic_cast<TVdfnPort<bool>*>( GetInPort( SGrabCmdInPortName ) );
	m_pInOrientation = dynamic_cast<TVdfnPort<VistaQuaternion>*>( GetInPort( SOrientationInPortName ) );
	m_pInPosition    = dynamic_cast<TVdfnPort<VistaVector3D>*>( GetInPort( SPositionInPortName ) );

	return GetIsValid();
}


bool   VfaVisControllerTransformerNode::DoEvalNode()
{
	const VistaQuaternion &qOri = m_pInOrientation->GetValueConstRef();
	const VistaVector3D &vPos = m_pInPosition->GetValueConstRef();

	bool bReleased = false;
	bool bGrabIndicated = m_pInGrab->GetValueConstRef();

	if(!m_bGrabbed && bGrabIndicated)
	{
		VistaVector3D vecTranslation;
		m_pRenderNode->GetTranslation(vecTranslation);
		VistaQuaternion quatRotation;
		m_pRenderNode->GetRotation(quatRotation);
		m_pXform->Init(vPos, qOri, vecTranslation, quatRotation);
		m_bGrabbed = true;
	}
	else
	if(m_bGrabbed && !bGrabIndicated)
	{
		// release grab
		bReleased = true;
	}

	// we only need position and/or orientation if the node is grabbed
	if(m_bGrabbed)
	{
		VistaVector3D vNewPos;
		VistaQuaternion qNewOri;
		m_pXform->Update(vPos, qOri, vNewPos, qNewOri);
		m_pRenderNode->SetTranslation(vNewPos);
		m_pRenderNode->SetRotation(qNewOri);
	}


	if(bReleased)
		m_bGrabbed = false;

	return true;
}

VflRenderNode *VfaVisControllerTransformerNode::GetRenderNode() const
{
	return m_pRenderNode;
}

void VfaVisControllerTransformerNode::SetRenderNode( VflRenderNode *pRN )
{
	m_pRenderNode = pRN;
}
/*============================================================================*/
/* Creator				                                                      */
/*============================================================================*/

VfaVisControllerTransformerNodeCreate::VfaVisControllerTransformerNodeCreate(VdfnObjectRegistry *pReg)
:m_pReg(pReg)
{
}

IVdfnNode *VfaVisControllerTransformerNodeCreate::CreateNode( const VistaPropertyList &oParams ) const
{
	VfaVisControllerTransformerNode *pNode = new VfaVisControllerTransformerNode( );

	const VistaPropertyList &subs = oParams.GetSubListConstRef("param");
	std::string strObj;
	if( subs.GetValue("object", strObj) )
	{
		VflRenderNode *pRN =dynamic_cast<VflRenderNode *>(m_pReg->GetObject( strObj ));
		if(pRN)
			pNode->SetRenderNode(pRN);
	}

	return pNode;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "MYDEMO.CPP"                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

