/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VDFNROTATEORINODE_H_
#define _VDFNROTATEORINODE_H_

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaDataFlowNet/VdfnSerializer.h>
#include <VistaDataFlowNet/VdfnNode.h>
#include <VistaDataFlowNet/VdfnPort.h>

#include <VistaDataFlowNet/VdfnNodeFactory.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VdfnRotateOriNode : public IVdfnNode
{
public:
	VdfnRotateOriNode();
	virtual ~VdfnRotateOriNode();

protected:
	virtual bool DoEvalNode();
	virtual bool PrepareEvaluationRun();

private:
	TVdfnPort<VistaQuaternion> *m_pInQuat;
	TVdfnPort<VistaQuaternion> *m_pInRot;
	TVdfnPort<VistaQuaternion> *m_pOut;
};

class VISTAFLOWLIBAUXAPI VdfnRotateOriNodeCreate : public VdfnNodeFactory::IVdfnNodeCreator
{
public:
	VdfnRotateOriNodeCreate();
	virtual IVdfnNode *CreateNode( const VistaPropertyList &oParams ) const;
private:
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VDFNROTATEORINODE_H_
