/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VfaBasicActions.h" 

#include <VistaFlowLibAux/Widgets/VfaWidget.h>
#include <VistaFlowLibAux/Interaction/VfaPointerManager.h>

#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/Volume/VflVisVolume.h>
#include <VistaFlowLib/Visualization/Particle/VflVisParticles.h>

#include <VistaAspects/VistaTransformable.h>
#include <iostream>
#include <fstream>


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
VfaCompositeAction::VfaCompositeAction()
{ }

VfaCompositeAction::~VfaCompositeAction()
{ }

int VfaCompositeAction::RegisterAction(IVistaExplicitCallbackInterface* pAction)
{
	m_vecActions.push_back(pAction);
	return static_cast<int>(m_vecActions.size()-1);
}

bool VfaCompositeAction::UnregisterAction(int iID)
{
	if ((iID < 0) || (iID >= static_cast<int>(m_vecActions.size())))
		return false;
	m_vecActions.erase(m_vecActions.begin()+iID);
	return true;
}

bool VfaCompositeAction::Do()
{
	bool bResult = true;
	for (std::vector<IVistaExplicitCallbackInterface*>::iterator i = m_vecActions.begin(); i != m_vecActions.end(); i++)
	{
		bResult = bResult | (*i)->Do();
	}
	return bResult;
}
// #############################################################################
// TIME MANIPULATION
// #############################################################################


IVfaTimeIndexStepAction::IVfaTimeIndexStepAction(VflVisTiming *pTiming, 
		                    VveTimeMapper *pTime)
		: m_pVisTiming(pTiming),
          m_pTimeMapper(pTime)
{}

IVfaTimeIndexStepAction::~IVfaTimeIndexStepAction() 
{}

VfaJumpToVisTimeAction::VfaJumpToVisTimeAction(VflVisTiming *pTiming,
                        float nVisTime)
                        : IVfaTimeIndexStepAction(pTiming, NULL),
                          m_nVisTime(nVisTime)
{
}

bool VfaJumpToVisTimeAction::Do()
{
    return m_pVisTiming->SetVisualizationTime(m_nVisTime);
}




VfaTimeIndexForwardAction::VfaTimeIndexForwardAction(VflVisTiming *pTiming, 
		                    VveTimeMapper *pTime)
							: IVfaTimeIndexStepAction(pTiming, pTime)
{}

VfaTimeIndexBackwardAction::VfaTimeIndexBackwardAction(VflVisTiming *pTiming, 
		                    VveTimeMapper *pTime)
							: IVfaTimeIndexStepAction(pTiming, pTime)
{}

bool VfaTimeIndexForwardAction::Do()
{
	double dVisTime = m_pVisTiming->GetVisualizationTime();
	
	// stop animation, we want to switch to step mode
	m_pVisTiming->SetAnimationPlaying(false, dVisTime);

	int iIndex = m_pTimeMapper->GetTimeIndex(dVisTime);

	iIndex =(iIndex+1)%m_pTimeMapper->GetNumberOfTimeIndices();
    dVisTime = m_pTimeMapper->GetVisualizationTime(iIndex);
	return m_pVisTiming->SetVisualizationTime(dVisTime);
}

bool VfaTimeIndexBackwardAction::Do()
{
	double dVisTime = m_pVisTiming->GetVisualizationTime();
	
	// stop animation, we want to switch to step mode
	m_pVisTiming->SetAnimationPlaying(false, dVisTime);

	int iIndex = m_pTimeMapper->GetTimeIndex(dVisTime);
    if(--iIndex < 0)
        iIndex = m_pTimeMapper->GetNumberOfTimeIndices()-1;
    else
        iIndex = iIndex%m_pTimeMapper->GetNumberOfTimeIndices();

    dVisTime = m_pTimeMapper->GetVisualizationTime(iIndex);
	return m_pVisTiming->SetVisualizationTime(dVisTime);
}


VfaChangeVisTimeAction::VfaChangeVisTimeAction(VflVisTiming *pTiming, double dDelta)
:	IVfaTimeIndexStepAction(pTiming, NULL)
,	m_dDelta(dDelta)
{
    if(m_dDelta < -1.0)
		m_dDelta = -1.0;
    else if(m_dDelta > 1.0)
		m_dDelta = 1.0;        
}

bool VfaChangeVisTimeAction::Do()
{
    double dVis = m_pVisTiming->GetVisualizationTime() + m_dDelta;
    if(dVis < 0.0)
        dVis += 1.0;

    if(dVis > 1.0)
        dVis -= 1.0;

    return m_pVisTiming->SetVisualizationTime( dVis );
}



VfaToggleAnimation::VfaToggleAnimation(VflVisTiming *pTiming)
:	m_pTiming(pTiming)
{ }

bool VfaToggleAnimation::Do()
{
	m_pTiming->ToggleAnimation();
	return true;
}


// LOOP TIME MANIPULATION

VfaLoopTimeChange::VfaLoopTimeChange(VflVisTiming *pVisTiming,
	double dDelta /* = 0.5 */ )
:	m_pVisTiming(pVisTiming)
,	m_dDelta(dDelta)
{ }

VfaLoopTimeChange::~VfaLoopTimeChange()
{ }


double VfaLoopTimeChange::GetDelta() const
{
	return m_dDelta;
}

void  VfaLoopTimeChange::SetDelta(double dDelta)
{
	m_dDelta = dDelta;
}


bool VfaLoopTimeChange::Do()
{
	return m_pVisTiming->SetBaseLoopTime( std::max(0.0,
		m_pVisTiming->GetBaseLoopTime() + GetDelta() ) );
}


// ANIMATION SPEED MANIPULATION

VfaAnimationSpeedChange::VfaAnimationSpeedChange(VflVisTiming *pVisTiming,
	double dDelta /* = 0.05 */ )
:	m_pVisTiming(pVisTiming)
,	m_dDelta(dDelta)
{ }

VfaAnimationSpeedChange::~VfaAnimationSpeedChange()
{ }


double VfaAnimationSpeedChange::GetDelta() const
{
	return m_dDelta;
}

void  VfaAnimationSpeedChange::SetDelta(double dDelta)
{
	m_dDelta = dDelta;
}


bool VfaAnimationSpeedChange::Do()
{
	return m_pVisTiming->SetAnimationSpeed( std::max(0.0,
		m_pVisTiming->GetAnimationSpeed() + GetDelta() ) );
}

// #############################################################################
// RENDERER MANIPULATION
// #############################################################################

VfaRenderableEnableToggle::VfaRenderableEnableToggle(IVflRenderable *pRen)
: IVistaExplicitCallbackInterface(),
m_pObj(pRen)
{ }

bool VfaRenderableEnableToggle::Do()
{
	(m_pObj)->SetVisible(!m_pObj->GetVisible());
	return true;
}

VfaForwardSwitchRendererAction::VfaForwardSwitchRendererAction(VflVisParticles *pVis)
		: m_pVis(pVis)	
{ }


VfaForwardSwitchRendererAction::~VfaForwardSwitchRendererAction()
{ }

bool VfaForwardSwitchRendererAction::Do()
{
	m_pVis->SetCurrentRenderer((m_pVis->GetCurrentRenderer()+1) % m_pVis->GetNumberOfRenderers());
	return true;
}


VfaForwardSwitchVolumeRenderAction::VfaForwardSwitchVolumeRenderAction(VflVisVolume *pVis)
:	m_pVis(pVis)	
{ }


VfaForwardSwitchVolumeRenderAction::~VfaForwardSwitchVolumeRenderAction()
{ }

bool VfaForwardSwitchVolumeRenderAction::Do()
{
	m_pVis->SetCurrentRenderer(
		(m_pVis->GetCurrentRenderer()+1) % m_pVis->GetRendererCount());
	return true;
}


VfaRenderableTimeInfo::VfaRenderableTimeInfo(VflTimeInfo *helper)
: IVistaExplicitCallbackInterface(),
m_phelper(helper)
{ }

bool VfaRenderableTimeInfo::Do()
{
	(m_phelper)->SetVisible(!m_phelper->GetVisible());
	return true;
}


VfaNextPointer::VfaNextPointer(VfaPointerManager *pPM)
:	m_pPointerManager(pPM)
{
	if(m_pPointerManager)
	{
		m_pPointerManager->GetListRegisteredModes(m_listPointerModes);

		// insert empyt entry to disable all
		m_listPointerModes.push_back("");
		m_sCurrentMode = m_listPointerModes.begin();
		m_pPointerManager->ActivatePointer(*m_sCurrentMode);
	}
}

bool VfaNextPointer::Do()
{
	m_sCurrentMode++;
	if(m_sCurrentMode == m_listPointerModes.end())
		m_sCurrentMode = m_listPointerModes.begin();

	if(*m_sCurrentMode=="")
		m_pPointerManager->DeactivatePointer();
	else
		m_pPointerManager->ActivatePointer(*m_sCurrentMode);

	return true;
}

VfaDumpViewParams::VfaDumpViewParams(VflRenderNode *pRenderNode, 
	const std::string &strFName) 
		: m_pRenderNode(pRenderNode), m_strFName(strFName) 
{ }

VfaDumpViewParams::~VfaDumpViewParams()
{ }

bool VfaDumpViewParams::Do()
{
	vstr::outi() << "[VfaBasicActions] Dumping view params to " << m_strFName << std::endl;
	VistaVector3D v3Pos = m_pRenderNode->GetTranslation();
	VistaQuaternion qOri = m_pRenderNode->GetRotation();
	// Adapted to RenderNode scale()-interface.
	// float fScale = m_pRenderNode->GetScale();
	float fScale;
	m_pRenderNode->GetScaleUniform(fScale);

    std::ofstream outFile(m_strFName.c_str());
	outFile << v3Pos[0] << "," << v3Pos[1] << "," << v3Pos[2] << std::endl;
	outFile << qOri[0] << "," << qOri[1] << "," << qOri[2] << "," << qOri[3] << std::endl;
	outFile << fScale << std::endl;
	return true;
}

VfaRestoreViewParams::VfaRestoreViewParams(VflRenderNode *pRenderNode, 
	const std::string &strFName) 
:	m_pRenderNode(pRenderNode)
,	m_strFName(strFName) 
{ }

VfaRestoreViewParams::~VfaRestoreViewParams()
{ }

bool VfaRestoreViewParams::Do()
{
	vstr::outi() << "[VfaBasicActions] Reading view params from " << m_strFName << std::endl;
	VistaVector3D v3Pos;
	VistaQuaternion qOri;
	float fScale;

	char buffer[4000];
    std::ifstream inFile(m_strFName.c_str());
	inFile.getline(buffer,4000);
	sscanf(buffer, "%f,%f,%f", &(v3Pos[0]), &(v3Pos[1]), &(v3Pos[2]));
	inFile.getline(buffer,4000);
	sscanf(buffer, "%f,%f,%f,%f", &(qOri[0]), &(qOri[1]), &(qOri[2]), &(qOri[3]));
	inFile >> fScale;

	m_pRenderNode->SetTranslation(v3Pos);
	m_pRenderNode->SetRotation(qOri);
	m_pRenderNode->SetScaleUniform(fScale);
	return true;
}

// #############################################################################
// NAVIGATION
// #############################################################################
VfaAxisRotate::VfaAxisRotate(VflRenderNode* pRenderNode, 
	const VistaVector3D& vecAxis, float fAngle)
:	m_pRenderNode(pRenderNode)
,	m_quatAxisAndAngle(VistaAxisAndAngle(vecAxis, fAngle))
{ }

VfaAxisRotate::~VfaAxisRotate()
{ }

bool VfaAxisRotate::Do()
{
    return m_pRenderNode->Rotate(m_quatAxisAndAngle);
}

VfaScale::VfaScale(VflRenderNode* pRenderNode, float fScale)
:	m_pRenderNode(pRenderNode)
,	m_fScale(fScale)
{ }

VfaScale::~VfaScale()
{ }

bool VfaScale::Do()
{
    return m_pRenderNode->ScaleUniformly(m_fScale);
}

VfaToggleWidget::VfaToggleWidget(IVfaWidget* pWidget)
:m_pWidget(pWidget)
{ }

VfaToggleWidget::~VfaToggleWidget()
{ }

bool VfaToggleWidget::Do()
{
	bool b = !m_pWidget->GetIsEnabled();
	m_pWidget->SetIsEnabled(b);
	m_pWidget->SetIsVisible(b);
    return true;
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

