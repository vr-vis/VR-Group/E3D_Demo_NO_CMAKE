/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFATARGETPOINTER_H
#define _VFATARGETPOINTER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaBeamPointer.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

//This class was written to remove the VfaBeampointer which is just a box.
//VfaTargetPointer should just be nicer. 
//drawopaque method is copyed from VFA/Widgets/VfaArrowVis
class VISTAFLOWLIBAUXAPI VfaTargetPointer : public VfaBeamPointer
{
public:
	VfaTargetPointer();
	virtual ~VfaTargetPointer();

	virtual void DrawOpaque();

	/**
	 * Returns the line width
	 * 
	 * @param   
	 * @return  --
	 */    
	float	GetLineWidth() const;
	/**
	 * Sets the line width
	 * 
	 * @param   float fLineWidth
	 * @return  --
	 */    
	void	SetLineWidth(float fLineWidth);
private:
	float				m_fLineWidth;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif 
