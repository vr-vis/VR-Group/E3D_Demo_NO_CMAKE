/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaApplicationStateManager.h"
#include <algorithm>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      IVfaApplicationState                                  */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IVfaApplicationState                                        */
/*                                                                            */
/*============================================================================*/


IVfaApplicationState::IVfaApplicationState()
{
}

IVfaApplicationState::~IVfaApplicationState()
{
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaApplicationStateManager	                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaApplicationStateManager::VfaApplicationStateManager()
:m_pActiveState(NULL),
m_sCurrentStateName("NONE")
{
}

VfaApplicationStateManager::~VfaApplicationStateManager()
{
	
}

/*============================================================================*/
/*																			  */
/*  RegisterState			                                                  */
/*																			  */
/*============================================================================*/

void VfaApplicationStateManager::RegisterState(const std::string& sStateName, 
							IVfaApplicationState *pState)
{
	if(m_mapStates.find(sStateName) == m_mapStates.end())
	{
		m_mapStates[sStateName] = pState;

		// per default, deactivate all states
		pState->Deactivate();
		
		// if this is the first state, choose it as active state
		if(m_pActiveState == NULL)
		{
			this->SwitchState (sStateName);
		}
	}
}

/*============================================================================*/
/*																			  */
/*  DeregisterState			                                                  */
/*																			  */
/*============================================================================*/

void VfaApplicationStateManager::DeregisterState(const std::string& sStateName)
{
	std::map<std::string, IVfaApplicationState*>::iterator itEntry = m_mapStates.find(sStateName);
	if(itEntry != m_mapStates.end())
	{
		m_mapStates.erase(itEntry);
	}
}

/*============================================================================*/
/*																			  */
/*  SwitchState				                                                  */
/*																			  */
/*============================================================================*/

bool VfaApplicationStateManager::SwitchState(const std::string& sStateName)
{
	std::map<std::string, IVfaApplicationState*>::iterator itEntry = m_mapStates.find(sStateName);
	if(itEntry != m_mapStates.end())
	{
		// deactivate old state
		if(m_pActiveState != NULL)
			m_pActiveState->Deactivate();

		m_pActiveState = itEntry->second;
		m_sCurrentStateName = sStateName;
		// activate new state
		m_pActiveState->Activate();
		return true;
	}

	return false;
}

/*============================================================================*/
/*																			  */
/*  GetListRegisteredStates	                                                  */
/*																			  */
/*============================================================================*/

bool VfaApplicationStateManager::GetListRegisteredStates(std::list<std::string> & sListStates) const
{
	sListStates.clear();
	std::map<std::string, IVfaApplicationState*>::const_iterator itEntry = m_mapStates.begin();
	while(itEntry != m_mapStates.end())
	{
		sListStates.push_back(itEntry->first);
		itEntry++;
	}

	return true;
}

/*============================================================================*/
/*																			  */
/*  GetStateByName			                                                  */
/*																			  */
/*============================================================================*/

IVfaApplicationState* const VfaApplicationStateManager::GetStateByName(
	const std::string &sStateName) const
{
	std::map<std::string, IVfaApplicationState*>::const_iterator itEntry =
		m_mapStates.find(sStateName);
	if(itEntry != m_mapStates.end())
	{
		return (*itEntry).second;
	}
	else
	{
		return NULL;
	}
}

/*============================================================================*/
/*																			  */
/*  GetCurrentState			                                                  */
/*																			  */
/*============================================================================*/

bool VfaApplicationStateManager::GetCurrentState (std::string & sStateName) const
{
	if(m_pActiveState == NULL)
		return false;

	sStateName = m_sCurrentStateName;
	return true;
}

std::string VfaApplicationStateManager::GetCurrentState() const
{
	if(m_pActiveState == NULL)
		return "";
	return m_sCurrentStateName;
}
/*============================================================================*/
/*  IMPLEMENTATION      VfaNextStateAction			                          */
/*============================================================================*/
VfaNextStateAction::VfaNextStateAction(VfaApplicationStateManager* pStateManager)
:m_pStateManager(pStateManager)
{
}

VfaNextStateAction::~VfaNextStateAction()
{
}

bool VfaNextStateAction::Do()
{
	if (!m_pStateManager)
		return false;

	std::list<std::string> sListStates;
	m_pStateManager->GetListRegisteredStates (sListStates);
	std::string sCurrentState;
	if (!m_pStateManager->GetCurrentState (sCurrentState))
		return false;

	std::list<std::string>::iterator itState = std::find (sListStates.begin(), sListStates.end(), sCurrentState);
	if (itState == sListStates.end())
		return false;

	itState++;
	if (itState == sListStates.end())
		itState = sListStates.begin();

	m_pStateManager->SwitchState(*itState);
	return true;
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaPreviousStateAction			                      */
/*============================================================================*/
VfaPreviousStateAction::VfaPreviousStateAction(VfaApplicationStateManager* pStateManager)
:m_pStateManager(pStateManager)
{
}

VfaPreviousStateAction::~VfaPreviousStateAction()
{
}

bool VfaPreviousStateAction::Do()
{
	if (!m_pStateManager)
		return false;

	std::list<std::string> sListStates;
	m_pStateManager->GetListRegisteredStates (sListStates);
	std::string sCurrentState;
	if (!m_pStateManager->GetCurrentState (sCurrentState))
		return false;

	std::list<std::string>::iterator itState = std::find (sListStates.begin(), sListStates.end(), sCurrentState);
	if (itState == sListStates.end())
		return false;

	if (itState == sListStates.begin())
		itState = sListStates.end();
	itState--;
	

	m_pStateManager->SwitchState(*itState);
	return true;
}

/*============================================================================*/
/*  IMPLEMENTATION      VfaSelectStateAction			                      */
/*============================================================================*/
VfaSelectStateAction::VfaSelectStateAction(VfaApplicationStateManager* pStateManager, const std::string & sStateName)
:m_pStateManager(pStateManager),
m_sStateName (sStateName)
{
}

VfaSelectStateAction::~VfaSelectStateAction()
{
}

bool VfaSelectStateAction::Do()
{
	if (!m_pStateManager)
		return false;

	m_pStateManager->SwitchState(m_sStateName);
	return true;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/


/*============================================================================*/
/*  END OF FILE "VfaApplicationStateManager.cpp"						      */
/*============================================================================*/
