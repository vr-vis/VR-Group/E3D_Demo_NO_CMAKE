/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaTransformableMediator.h"
#include <VistaAspects/VistaReflectionable.h>
#include <VistaAspects/VistaTransformable.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaTransformableMediator::VfaTransformableMediator(IVistaTransformable* pTransformable, IVistaReflectionable * pReflectionable, 
		const std::string & sPositionPropertyName, const std::string & sOrientationPropertyName)
:m_pTransformable(pTransformable),
m_pTargetReflectionable(pReflectionable),
m_sPositionPropertyName(sPositionPropertyName),
m_sOrientationPropertyName(sOrientationPropertyName)
{
	this->Observe(m_pTargetReflectionable);

	// init reflectionable with transformable's position and orientation
	UpdateReflectionable();
}

VfaTransformableMediator::~VfaTransformableMediator()
{

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateReflectionable                                        */
/*                                                                            */
/*============================================================================*/
void VfaTransformableMediator::UpdateReflectionable()
{
	float fPosition[3];
	std::list<float> listFloats;
	if(m_pTransformable->GetTranslation(fPosition))
	{
		VistaAspectsConversionStuff::ConvertToList(fPosition, 3, listFloats);
		m_pTargetReflectionable->SetPropertyByName(m_sPositionPropertyName, VistaAspectsConversionStuff::ConvertToString(listFloats));
	}

	listFloats.clear();
	float fOrientation[4];
	if(m_pTransformable->GetRotation(fOrientation))
	{
		VistaAspectsConversionStuff::ConvertToList(fOrientation, 4, listFloats);
		m_pTargetReflectionable->SetPropertyByName(m_sOrientationPropertyName, VistaAspectsConversionStuff::ConvertToString(listFloats));
	}

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaTransformableMediator::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if(pObserveable == m_pTargetReflectionable)
	{
		VistaProperty oProp = m_pTargetReflectionable->GetPropertyByName(m_sPositionPropertyName);
		if(!oProp.GetIsNilProperty())
		{
			float fPosition[3];
			VistaAspectsConversionStuff::ConvertStringTo3Float(oProp.GetValue(), fPosition[0], fPosition[1], fPosition[2]);
			m_pTransformable->SetTranslation(fPosition);
		}
		oProp = m_pTargetReflectionable->GetPropertyByName(m_sOrientationPropertyName);
		if(!oProp.GetIsNilProperty())
		{

			float fOrientation[4];
			VistaAspectsConversionStuff::ConvertStringTo4Float(oProp.GetValue(), fOrientation[0], fOrientation[1], fOrientation[2], fOrientation[3]);
			m_pTransformable->SetRotation(fOrientation);
		}
	}
}
/*============================================================================*/
/*  END OF FILE "VfaCrosshairWidget.cpp"                                      */
/*============================================================================*/


