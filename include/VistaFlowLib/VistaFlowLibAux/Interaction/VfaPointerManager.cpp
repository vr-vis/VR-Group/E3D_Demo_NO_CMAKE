/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaPointerManager.h"
#include <VistaBase/VistaVectorMath.h>

#include <cassert>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaPointerManager	                                  */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaPointerManager::VfaPointerManager(VfaApplicationContextObject* pAppContext)
	:	m_pAppContext(pAppContext)
{
	//m_v3Position = VistaVector3D(0.0, 0.0, 0.0);
	this->Observe(m_pAppContext);
}

VfaPointerManager::~VfaPointerManager()
{
	
}

void VfaPointerManager::RegisterPointer(const std::string& sModeName, 
										 IVfaPointer *pPointer,
										 int iFrame)
{
	if(m_mapPointerModes.find(sModeName) == m_mapPointerModes.end())
	{

		m_mapPointerModes[sModeName] = SPointerInfo(pPointer, iFrame);
		pPointer->SetVisible(false);
	}
}

void VfaPointerManager::DeregisterPointer(const std::string& sModeName)
{
	std::map<std::string, SPointerInfo>::iterator itEntry = m_mapPointerModes.find(sModeName);
	if(itEntry != m_mapPointerModes.end())
	{
		m_mapPointerModes.erase(itEntry);
	}
}

bool VfaPointerManager::ActivatePointer(const std::string& sModeName)
{
	std::map<std::string, SPointerInfo>::iterator itEntry = m_mapPointerModes.find(sModeName);
	if(itEntry != m_mapPointerModes.end())
	{
		// disable old pointer
		if(m_oActivePointer.pPointer != NULL)
			m_oActivePointer.pPointer->SetVisible(false);

		m_oActivePointer = itEntry->second;
		// inform new pointer about current pointer transform
		if(m_oActivePointer.pPointer != NULL)
		{
			m_oActivePointer.pPointer->SetVisible(true);
			this->UpdateActivePointer();
		}
		return true;
	}

	return false;
}

bool VfaPointerManager::DeactivatePointer()
{
	if(m_oActivePointer.pPointer != NULL)
		m_oActivePointer.pPointer->SetVisible(false);

	m_oActivePointer.pPointer = NULL;
	return true;
}


void VfaPointerManager::SetPointerVisible(bool bVisible)
{
	if(m_oActivePointer.pPointer != NULL)
		return m_oActivePointer.pPointer->SetVisible(bVisible);
}

bool VfaPointerManager::GetPointerVisible() const
{
	if(m_oActivePointer.pPointer != NULL)
		return m_oActivePointer.pPointer->GetVisible();
	else
		return false;
}

bool VfaPointerManager::GetListRegisteredModes(std::list<std::string> & sListModes)
{
	sListModes.clear();
	std::map<std::string, SPointerInfo>::iterator itEntry = m_mapPointerModes.begin();
	while(itEntry != m_mapPointerModes.end())
	{
		sListModes.push_back(itEntry->first);
		itEntry++;
	}

	return true;
}

void VfaPointerManager::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if(msg == VfaApplicationContextObject::MSG_SENSORCHANGE)
	{
		if ((m_oActivePointer.pPointer != NULL)&&
			(m_oActivePointer.iFrame==m_pAppContext->GetChangedSensorSlot()))
			this->UpdateActivePointer();		
	}
}

void VfaPointerManager::UpdateActivePointer()
{
	//this should have been tested by the caller!
	assert(m_oActivePointer.pPointer != NULL);

	VfaApplicationContextObject::sSensorFrame oFrame;
	
	// get the frame info in the pointer's render node coordinates
	m_pAppContext->GetSensorFrame(m_oActivePointer.iFrame,
		m_oActivePointer.pPointer->GetRenderNode(), oFrame);
	
	m_oActivePointer.pPointer->SetPosition(oFrame.v3Position);
	m_oActivePointer.pPointer->SetOrientation(oFrame.qOrientation);
}
/*============================================================================*/
/*  IMPLEMENTATION      IVfaPointer                                           */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IVfaPointer                                                 */
/*                                                                            */
/*============================================================================*/


IVfaPointer::IVfaPointer()
{
}

IVfaPointer::~IVfaPointer()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void IVfaPointer::SetPosition(const VistaVector3D &v3Pos)
{
	m_v3Position = v3Pos;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void IVfaPointer::GetPosition(VistaVector3D &v3Pos) const
{
	v3Pos = m_v3Position;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRotate                                                   */
/*                                                                            */
/*============================================================================*/
void IVfaPointer::SetOrientation(const VistaQuaternion &qOri)
{
	m_qOrientation = qOri;


}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRotate                                                   */
/*                                                                            */
/*============================================================================*/
void IVfaPointer::GetOrientation(VistaQuaternion &qOri) const
{
	qOri = m_qOrientation;

}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/
VfaPointerManager::SPointerInfo::SPointerInfo() 
	:	pPointer(NULL), 
		iFrame(VfaApplicationContextObject::SLOT_POINTER_VIS)
{
}
VfaPointerManager::SPointerInfo::SPointerInfo(IVfaPointer *pP, int iF)
	:	pPointer(pP), 
		iFrame(iF)
{
}

/*============================================================================*/
/*  END OF FILE "VfaPointerVis.cpp"		         						      */
/*============================================================================*/
