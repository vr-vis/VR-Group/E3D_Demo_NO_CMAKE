/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAAPPLICATIONSTATEINFO_H
#define _VFAAPPLICATIONSTATEINFO_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLib/Visualization/VflSystemAbstraction.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflPropertyLoader;
class VfaApplicationStateManager;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaApplicationStateInfo : public IVflRenderable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
	VfaApplicationStateInfo(VfaApplicationStateManager *pStateManager);

    virtual ~VfaApplicationStateInfo();

    virtual void Draw2D();

    virtual std::string GetType() const;

	class VfaApplicationStateInfoProperties : 
		public IVflRenderable::VflRenderableProperties
    {
    public:
		VfaApplicationStateInfoProperties();
        virtual ~VfaApplicationStateInfoProperties();

		virtual std::string GetReflectionableType() const;

		bool GetTextPosition(float &fX, float &fY) const;
		bool SetTextPosition(float fX, float fY);

		bool GetTextColor(float &fRed, float &fGreen, float &fBlue) const;
		bool SetTextColor(float fRed, float fGreen, float fBlue);

		bool   SetText(const std::string &sText);
		std::string GetText() const;

		bool GetDisplayOnCurrentNode() const;
		bool SetDisplayOnCurrentNode(bool bDraw);

		std::list<std::string> GetNodeNamesForDisplay() const;
		bool SetNodeNamesForDisplay(const std::list<std::string> &liNodeNames);

		enum
		{
			MSG_NODEDISPLAYCHANGE = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_DISPLAYONCURRENTNODE,
 
			MSG_POSITIONCHANGE,
			MSG_COLORCHANGE,
			MSG_TEXTCHANGE,

			MSG_LAST
		};

	protected:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;
    private:
		VfaApplicationStateInfoProperties(const VfaApplicationStateInfoProperties &)
		{
		}
		VfaApplicationStateInfoProperties &operator=(const VfaApplicationStateInfoProperties &)
		{
			return *this;
		}
		IVflSystemAbstraction::IVflOverlayText *m_pText;

		bool m_bDisplayOnCurrentNode;
		std::list<std::string> m_liNodeNamesForDisplay;
        friend class VfaApplicationStateInfo;
    };

	virtual VfaApplicationStateInfoProperties *GetProperties() const;

    static std::string GetFactoryType();

	virtual unsigned int GetRegistrationMode() const;
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	

protected:
	virtual VflRenderableProperties    *CreateProperties() const;

	VfaApplicationStateManager *m_pStateManager;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAAPPLICATIONSTATEINFO_H
