/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFASELECTCONEPOINTER_H
#define _VFASELECTCONEPOINTER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VistaFlowLibAuxConfig.h"
#include "VfaPointerManager.h"
#include <VistaFlowLibAux/Widgets/VfaConeVis.h>
#include <VistaFlowLibAux/Widgets/VfaSphereVis.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaObserver.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaIntentionSelectFocusStrategy;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A renderable tool to visualize the selection cone from a IntentionSelect
 * strategy.
 */
class VISTAFLOWLIBAUXAPI VfaSelectConePointer : public IVfaPointer
{
public: 
	VfaSelectConePointer(VfaIntentionSelectFocusStrategy* pIntentionSelect);
	virtual ~VfaSelectConePointer();

	virtual void DrawTransparent();

	virtual unsigned int GetRegistrationMode() const;
	
	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);

	void SetColor(const VistaColor& color);
	VistaColor GetColor() const;
	void SetColor(float fColor[4]);
	void GetColor(float fColor[4]) const;
	
protected:
	virtual VflRenderableProperties* CreateProperties() const;

private:
	VfaIntentionSelectFocusStrategy* m_pIntentionSelect;

	VfaConeVis *m_pConeVis;
	VfaSphereVis *m_pSphereVis;
};


#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
