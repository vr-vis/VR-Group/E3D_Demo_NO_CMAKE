/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAAPPLICATIONSTATEMANAGER_H
#define _VFAAPPLICATIONSTATEMANAGER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaExplicitCallbackInterface.h>
#include <map>
#include <list>
#include <string>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * IVfaApplicationState is the base class for a state. It can be activated and deactivated.
 * Consistency and functions of a state must be implemented by: you!
 * If you have OpenGL experience, you should be used to it.... :)
 */
class VISTAFLOWLIBAUXAPI IVfaApplicationState
{
public: 
	IVfaApplicationState();
	virtual ~IVfaApplicationState();

	virtual void Activate () = 0;
	virtual void Deactivate () = 0;

private:
};
/**
 * In a VfaApplicationStateManager, you can register and manage states.
 * All it does it deactivate the old state and activate the new state on a state switch.
 *
 * The basic idea is to have a central instance where "states" are registered and activated, such that
 * only one state is active at a time. 
 * Implementation of states based on IVfaApplicationState is very application-dependend,
 * therefore the base class for states is quite basic and empty.
 *
 */
class VISTAFLOWLIBAUXAPI VfaApplicationStateManager
{
public: 
	VfaApplicationStateManager();
	virtual ~VfaApplicationStateManager();

	/**
	 * 
	 */
	bool SwitchState(const std::string& sStateName);
	

	
	/**
	 * 
	 */
	void RegisterState(	const std::string& sStateName, 
							IVfaApplicationState *pState);
	
	/**
	 * 
	 */
	void DeregisterState(const std::string& sStateName);

	/**
	 * 
	 */
	bool GetListRegisteredStates(std::list<std::string> & sListStates) const;

	/**
	 * Returns a pointer to the state with the name sStateName
	 * or NULL, if there is no such state.
	 */
	IVfaApplicationState* const GetStateByName(const std::string &sStateName) const;

	/**
	 * 
	 */
	bool GetCurrentState (std::string & sStateName) const;

	/**
	 *
	 */
	std::string GetCurrentState() const;

private:
	
	std::map<std::string, IVfaApplicationState*> m_mapStates;
	std::string									 m_sCurrentStateName;
	IVfaApplicationState*						 m_pActiveState;
	
};


/**
 * This is a common action for states...get the next one.
 *
 *
 */
class VISTAFLOWLIBAUXAPI VfaNextStateAction : public IVistaExplicitCallbackInterface
{
public:
	VfaNextStateAction(VfaApplicationStateManager* pStateManager);
	virtual ~VfaNextStateAction();

	bool Do();
private:
	VfaApplicationStateManager*  m_pStateManager;
};

/**
 * This is a common action for states...get the previous one.
 *
 *
 */
class VISTAFLOWLIBAUXAPI VfaPreviousStateAction : public IVistaExplicitCallbackInterface
{
public:
	VfaPreviousStateAction(VfaApplicationStateManager* pStateManager);
	virtual ~VfaPreviousStateAction();

	bool Do();
private:
	VfaApplicationStateManager*  m_pStateManager;
};

/**
 * This is a common action for states...get the next one.
 *
 *
 */
class VISTAFLOWLIBAUXAPI VfaSelectStateAction : public IVistaExplicitCallbackInterface
{
public:
	VfaSelectStateAction(VfaApplicationStateManager* pStateManager, const std::string & sStateName);
	virtual ~VfaSelectStateAction();

	bool Do();
private:
	VfaApplicationStateManager*  m_pStateManager;
	std::string					  m_sStateName;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAAPPLICATIONSTATEMANAGER_H
