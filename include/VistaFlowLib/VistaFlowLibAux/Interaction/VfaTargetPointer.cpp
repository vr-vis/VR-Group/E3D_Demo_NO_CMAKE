/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <GL/glew.h>

#include "VfaTargetPointer.h"
#ifdef WIN32
	#include <Windows.h>
#endif

#if defined(DARWIN)
  #include <OpenGL/gl.h>
#else
  #include <GL/gl.h>
#endif

void VfaTargetPointer::DrawOpaque()
{
	VistaVector3D vecPosition;
	VistaQuaternion qOri;
	this->GetPosition(vecPosition);
	this->GetOrientation(qOri);

	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_LIGHTING_BIT|GL_POLYGON_BIT|GL_LINE_BIT);
	glPushMatrix();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);

	glLineWidth (m_fLineWidth);
	float fColor[4];
	this->GetColor(fColor);
	glColor3f(fColor[0], fColor[1], fColor[2]);

	glTranslatef(vecPosition[0], vecPosition[1], vecPosition[2]);
	glRotatef(Vista::RadToDeg(acos(qOri[3])*2), 
		qOri[0], qOri[1], qOri[2]);
	
	glTranslatef(0.0, 0.0, this->GetLength());

	glBegin(GL_LINE_LOOP);
	for(int i=0;i<100;i++)
		glVertex3f(this->GetWidth()*cos(i*2*Vista::Pi/100),this->GetWidth()*sin(i*2*Vista::Pi/100), 0.0f);
	glEnd();
	glBegin(GL_LINES);
		glVertex3f(-this->GetWidth()*1.2f, 0.0f, 0.0f);
		glVertex3f(this->GetWidth()*1.2f, 0.0f, 0.0f);
		glVertex3f(0.0f,-this->GetWidth()*1.2f, 0.0f);
		glVertex3f(0.0f, this->GetWidth()*1.2f, 0.0f);
	glEnd();

	//glDisable(GL_LIGHTING);

	glPopMatrix();
	glPopAttrib();
}

VfaTargetPointer::VfaTargetPointer()
:VfaBeamPointer(),
m_fLineWidth(0.1f)
{
}

VfaTargetPointer::~VfaTargetPointer()
{
}

float	VfaTargetPointer::GetLineWidth() const
{
	return m_fLineWidth;
}

void	VfaTargetPointer::SetLineWidth(float fLineWidth)
{
	m_fLineWidth = fLineWidth;
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaArrowPointer.cpp"		         						      */
/*============================================================================*/
