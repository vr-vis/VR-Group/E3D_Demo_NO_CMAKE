/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACOMMANDTOACTIONHANDLER_H
#define _VFACOMMANDTOACTIONHANDLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Data/VflObserver.h>
#include <map>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVistaExplicitCallbackInterface;
class VfaApplicationContextObject;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaCommandToActionHandler : public VflObserver
{
public:
	VfaCommandToActionHandler(VfaApplicationContextObject *pAppContext);
	virtual ~VfaCommandToActionHandler();

	bool RegisterAction(int iCommandID, IVistaExplicitCallbackInterface *);
	bool GetIsRegistered(IVistaExplicitCallbackInterface *) const;
	bool UnregisterAction(int iCommandID, bool bDelete);
	IVistaExplicitCallbackInterface *GetCallback(int iCommandID) const;

	// Observer interface
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

protected:
private:
	std::map<int, IVistaExplicitCallbackInterface*> m_mpActions;
	VfaApplicationContextObject *m_pAppContext;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFACOMMANDTOACTIONHANDLER_H

