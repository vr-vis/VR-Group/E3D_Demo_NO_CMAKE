/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* PRAGMAS                                                                    */
/*============================================================================*/
#ifdef WIN32
    #pragma warning(disable:4786)
#endif

#include <GL/glew.h>

#include "VfaApplicationStateInfo.h"

#include <VistaFlowLibAux/Interaction/VfaApplicationStateManager.h>

#include <VistaFlowLib/Visualization/VflVisController.h>

#include <VistaFlowLib/Visualization/VflSystemAbstraction.h>

#ifdef WIN32
	#include <windows.h>
#endif

#if defined(DARWIN)
  #include <OpenGL/gl.h>  
#else
  #include <GL/gl.h>
#endif

#include <algorithm>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaApplicationStateInfo::VfaApplicationStateInfo(VfaApplicationStateManager *pStateManager)
: m_pStateManager(pStateManager)
{
}

VfaApplicationStateInfo::~VfaApplicationStateInfo()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void VfaApplicationStateInfo::Draw2D()
{
	VfaApplicationStateInfo::VfaApplicationStateInfoProperties *props
		= static_cast<VfaApplicationStateInfo::VfaApplicationStateInfoProperties*>(GetProperties());

	std::string strResult;

	m_pStateManager->GetCurrentState(strResult);

	// here: we bypass the official setter, as this will try
	// to compare the incoming and the current text
	// and utter a notify. This might be too slow, so
	// we, well... bypass it ;)

	if((*props).m_pText)
	{
		(*props).m_pText->SetText("ApplicationState: " + strResult);
		(*props).Notify(VfaApplicationStateInfoProperties::MSG_TEXTCHANGE);
		(*props).m_pText->Update();
	}
}


std::string VfaApplicationStateInfo::GetFactoryType()
{
	return std::string("ComponentInfo");
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VfaApplicationStateInfo::GetType() const
{
    return IVflRenderable::GetType()+string("::VfaApplicationStateInfo");
}

IVflRenderable::VflRenderableProperties *VfaApplicationStateInfo::CreateProperties() const
{
	return new VfaApplicationStateInfoProperties();
}

void VfaApplicationStateInfo::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	// intercept visibility change to turn text on or off
	if(msg == VflRenderableProperties::MSG_VISIBILITY)
	{
		VfaApplicationStateInfoProperties *p = dynamic_cast<VfaApplicationStateInfoProperties*>(pObserveable);
		if(p)
		{
			p->m_pText->SetEnabled(p->GetVisible());
		}
	}

	// in any case, pass the update to the super-class
	IVflRenderable::ObserverUpdate(pObserveable, msg, ticket);
}

unsigned int VfaApplicationStateInfo::GetRegistrationMode() const
{
    return IVflRenderable::OLI_DRAW_2D;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaApplicationStateInfo::VfaApplicationStateInfoProperties 
	*VfaApplicationStateInfo::GetProperties() const
{
	return static_cast<VfaApplicationStateInfoProperties*>(
		IVflRenderable::GetProperties());
}

/*============================================================================*/
/*  methods of VfaApplicationStateInfo::VfaApplicationStateInfoProperties   */
/*============================================================================*/
static const string SsReflectionType("VfaApplicationStateInfo");
static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, VfaApplicationStateInfo::VfaApplicationStateInfoProperties, VistaProperty::PROPT_BOOL>
		("DISPLAYONCURRENTNODE", SsReflectionType,
		&VfaApplicationStateInfo::VfaApplicationStateInfoProperties::GetDisplayOnCurrentNode),
	new TVistaPropertyGet<std::string, VfaApplicationStateInfo::VfaApplicationStateInfoProperties, VistaProperty::PROPT_STRING>
	    ("INFOTEXT", SsReflectionType,
		&VfaApplicationStateInfo::VfaApplicationStateInfoProperties::GetText),
	new TVistaPropertyGet<std::list<std::string>, VfaApplicationStateInfo::VfaApplicationStateInfoProperties, VistaProperty::PROPT_LIST>
	    ("DISPLAY_ON_NODES", SsReflectionType,
		&VfaApplicationStateInfo::VfaApplicationStateInfoProperties::GetNodeNamesForDisplay),
    new TVistaProperty3RefGet<float, VfaApplicationStateInfo::VfaApplicationStateInfoProperties> // implicitely PROPT_LIST
		("COLOR", SsReflectionType,
		&VfaApplicationStateInfo::VfaApplicationStateInfoProperties::GetTextColor),
    new TVistaProperty2RefGet<float, VfaApplicationStateInfo::VfaApplicationStateInfoProperties> // implicitely PROPT_LIST
		("POSITION", SsReflectionType,
		&VfaApplicationStateInfo::VfaApplicationStateInfoProperties::GetTextPosition),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool, bool,
	    VfaApplicationStateInfo::VfaApplicationStateInfoProperties>
		("DISPLAYONCURRENTNODE", SsReflectionType,
		&VfaApplicationStateInfo::VfaApplicationStateInfoProperties::SetDisplayOnCurrentNode),
	new TVistaPropertySet<const std::string &, std::string ,
	    VfaApplicationStateInfo::VfaApplicationStateInfoProperties>
		("INFOTEXT", SsReflectionType,
		&VfaApplicationStateInfo::VfaApplicationStateInfoProperties::SetText),
	new TVistaPropertySet<const std::list<std::string>&, 
		std::list<std::string>, VfaApplicationStateInfo::VfaApplicationStateInfoProperties>
	    ("DISPLAY_ON_NODES", SsReflectionType,
		&VfaApplicationStateInfo::VfaApplicationStateInfoProperties::SetNodeNamesForDisplay),
	new TVistaProperty3ValSet<float, VfaApplicationStateInfo::VfaApplicationStateInfoProperties>
		( "COLOR", SsReflectionType,
		&VfaApplicationStateInfo::VfaApplicationStateInfoProperties::SetTextColor),
	new TVistaProperty2ValSet<float, VfaApplicationStateInfo::VfaApplicationStateInfoProperties>
		( "POSITION", SsReflectionType,
		&VfaApplicationStateInfo::VfaApplicationStateInfoProperties::SetTextPosition),
	NULL
};

VfaApplicationStateInfo::VfaApplicationStateInfoProperties::VfaApplicationStateInfoProperties()
: IVflRenderable::VflRenderableProperties(),
  m_pText(NULL),
  m_bDisplayOnCurrentNode(true)
{	
	m_pText = IVflSystemAbstraction::GetSystemAbs()->AddOverlayText();
	
}

VfaApplicationStateInfo::VfaApplicationStateInfoProperties::~VfaApplicationStateInfoProperties()
{
	if(m_pText)
	{		
		IVflSystemAbstraction::GetSystemAbs()->DeleteOverlayText(m_pText);
	}   
}

bool VfaApplicationStateInfo::VfaApplicationStateInfoProperties::GetTextPosition(float &fX, float &fY) const
{
	if(m_pText)
	{
		m_pText->GetPosition(fX, fY);
		return true;
	}
	return false;
}

bool VfaApplicationStateInfo::VfaApplicationStateInfoProperties::SetTextPosition(float fX, float fY)
{
	if(m_pText)
	{
		m_pText->SetPosition(fX, fY);
		Notify(MSG_POSITIONCHANGE);
		return true;
	}
	return false;
}


bool VfaApplicationStateInfo::VfaApplicationStateInfoProperties::GetTextColor(float &fRed, float &fGreen, float &fBlue) const
{
	if(m_pText)
	{
		return m_pText->GetColor(fRed, fGreen, fBlue);
	}
	return false;
}

bool VfaApplicationStateInfo::VfaApplicationStateInfoProperties::SetTextColor(float fRed, float fGreen, float fBlue)
{
	if(m_pText && m_pText->SetColor(fRed, fGreen, fBlue))
	{
		Notify(MSG_COLORCHANGE);
		return true;
	}
	return true;
}

bool VfaApplicationStateInfo::VfaApplicationStateInfoProperties::SetText(const std::string &sText)
{
	if(m_pText)
	{
		std::string sTmp;
		m_pText->GetText(sTmp);
		if(sTmp != sText)
		{
			m_pText->SetText(sText);
			Notify(MSG_TEXTCHANGE);
			return true;
		}
	}
	return false;
}

string VfaApplicationStateInfo::VfaApplicationStateInfoProperties::GetText() const
{
	if(m_pText)
	{
		std::string sTmp;
		m_pText->GetText(sTmp);
		return sTmp;
	}
	return "<null>";
}


std::string VfaApplicationStateInfo::VfaApplicationStateInfoProperties::GetReflectionableType() const
{
	return SsReflectionType;
}

int VfaApplicationStateInfo::VfaApplicationStateInfoProperties::AddToBaseTypeList(list<string> &rBtList) const
{
	int nSize = IVflRenderable::VflRenderableProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}

bool VfaApplicationStateInfo::VfaApplicationStateInfoProperties::GetDisplayOnCurrentNode() const
{
	return m_bDisplayOnCurrentNode;
}

bool VfaApplicationStateInfo::VfaApplicationStateInfoProperties::SetDisplayOnCurrentNode(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bDisplayOnCurrentNode))
	{
		if(m_pText)
			m_pText->SetEnabled(m_bDisplayOnCurrentNode);

		Notify(MSG_DISPLAYONCURRENTNODE);
		return true;
	}
	return false;
}

std::list<std::string> VfaApplicationStateInfo::VfaApplicationStateInfoProperties::
                                       GetNodeNamesForDisplay() const
									   
{
	return m_liNodeNamesForDisplay;
}


bool VfaApplicationStateInfo::VfaApplicationStateInfoProperties::SetNodeNamesForDisplay(
	                                    const std::list<std::string> &liNodeNames)
{
	// IAR @todo we could check for equality
	m_liNodeNamesForDisplay = liNodeNames;

	if (m_liNodeNamesForDisplay.empty())
	{
		SetDisplayOnCurrentNode(true);
	}
	else
	{
		if (IVflSystemAbstraction::GetSystemAbs()->IsLeader() )
		{
			SetDisplayOnCurrentNode(true);
		}
		else
		{
			// cluster - client/slave
			string strNodeName = IVflSystemAbstraction::GetSystemAbs()->GetClientName();
			if(std::find(m_liNodeNamesForDisplay.begin(), m_liNodeNamesForDisplay.end(), strNodeName)
				== m_liNodeNamesForDisplay.end())
			{
				SetDisplayOnCurrentNode(false);
			}
			else
				SetDisplayOnCurrentNode(true);
		}
	}

	Notify(MSG_NODEDISPLAYCHANGE);
	return true;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaApplicationStateInfo.cpp"                                 */
/*============================================================================*/
