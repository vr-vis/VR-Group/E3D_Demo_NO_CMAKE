/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFACROSSHAIRPOINTER_H
#define _VFACROSSHAIRPOINTER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaPointerManager.h"

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaCrosshairVis;
class VfaCrosshairModel;
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A renderable tool to visualize the selection cone from a IntentionSelect strategy.
 *
 *
 */
class VISTAFLOWLIBAUXAPI VfaCrosshairPointer : public IVfaPointer
{
public: 
	VfaCrosshairPointer();
	virtual ~VfaCrosshairPointer();

	virtual void DrawOpaque();

	virtual unsigned int GetRegistrationMode() const;

	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);

	void SetColor(const VistaColor& color);
	VistaColor GetColor() const;
	void SetColor(float fColor[4]);
	void GetColor(float fColor[4]) const;

	float GetLineWidth() const;
	void SetLineWidth(const float fWidth);

	float GetSize() const;
	void SetSize(float fSize);

	/* 
	 * This model was only(!) imeplemted for observer/observable purpose,
	 * otherwise you NEVER need to use this method
	 */
	VfaCrosshairModel *GetModel() const;
	
protected:
	virtual VflRenderableProperties* CreateProperties() const;

private:
	/** 
	 * just use this to forward to a crosshair vis
	 */
	VfaCrosshairVis	*m_pVis;
	VfaCrosshairModel	*m_pCrosshairModel;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASELECTCONEVIS_H
