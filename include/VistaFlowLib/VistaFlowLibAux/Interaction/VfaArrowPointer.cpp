/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <GL/glew.h>

#include "VfaArrowPointer.h"
#ifdef WIN32
#include <Windows.h>
#endif

#if defined(DARWIN)
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
  #include <GL/glut.h>
#endif

void VfaArrowPointer::DrawOpaque()
{
	VistaVector3D vecPosition;
	VistaQuaternion qOri;
	this->GetPosition(vecPosition);
	this->GetOrientation(qOri);

	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_LIGHTING_BIT|GL_POLYGON_BIT);
	glPushMatrix();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);


	GLfloat Ambient[] = {0.2f,0.2f,0.2f,1.0f};
	GLfloat Specular[] = {1,1,1,1.0};

	float fColor[4];
	this->GetColor(fColor);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,Ambient);
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,fColor);
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,Specular);
	glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0);

	glColor3f(fColor[0], fColor[1], fColor[2]);

//	VistaQuaternion q(0,0.707,0,0.707);
//	q = q * qOri;

	glTranslatef(vecPosition[0], vecPosition[1], vecPosition[2]);
	glRotatef(Vista::RadToDeg(acos(qOri[3])*2), 
		qOri[0], qOri[1], qOri[2]);
	glRotatef(180,0,1,0);
	gluCylinder(m_quadric, this->GetWidth()*0.5f, this->GetWidth()*0.5f, this->GetLength()*0.7f, 16, 16);
	glTranslatef(0.0f, 0.0f, this->GetLength() * 0.7f);
	glutSolidCone(this->GetWidth(), this->GetLength() * 0.3f, 16, 16);


	//glDisable(GL_LIGHTING);

	glPopMatrix();
	glPopAttrib();
}

VfaArrowPointer::VfaArrowPointer()
:VfaBeamPointer()
{
	m_quadric = gluNewQuadric();
	gluQuadricNormals(m_quadric, GLU_SMOOTH);
	gluQuadricTexture(m_quadric, GL_TRUE);
}

VfaArrowPointer::~VfaArrowPointer()
{
	gluDeleteQuadric(m_quadric);
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaArrowPointer.cpp"		         						      */
/*============================================================================*/
