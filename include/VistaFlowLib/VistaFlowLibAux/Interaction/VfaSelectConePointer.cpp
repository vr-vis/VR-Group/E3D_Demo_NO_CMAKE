/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <GL/glew.h>

#include "VfaSelectConePointer.h"
#include <VistaFlowLibAux/Widgets/Sphere/VfaSphereModel.h>
#include <VistaFlowLibAux/Widgets/VfaIntentionSelectFocusStrategy.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaPointerVis                                        */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaSelectConePointer::VfaSelectConePointer(VfaIntentionSelectFocusStrategy* pIntentionSelect)
	:	IVfaPointer(),
		m_pIntentionSelect(pIntentionSelect),
		m_pConeVis(new VfaConeVis),
		m_pSphereVis(new VfaSphereVis(new VfaSphereModel))
{
	m_pConeVis->Init();
	m_pSphereVis->Init();
	
	m_pConeVis->GetProperties()->SetToSolid(false);
	m_pSphereVis->GetProperties()->SetToSolid(true);
	m_pSphereVis->GetModel()->SetRadius(0.01f);
}

VfaSelectConePointer::~VfaSelectConePointer()
{
	delete m_pConeVis;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VfaSelectConePointer::DrawTransparent()
{
	if(!this->GetProperties()->GetVisible())
		return;

	VistaVector3D vecPosition;
	VistaQuaternion qOri;
	this->GetPosition (vecPosition);
	this->GetOrientation (qOri);

	// that too: should be updated on change, not every time:
	// radius and length of intention select search cone
	float fLength = m_pIntentionSelect->GetSelectionConeLength();
	float fRadius = m_pIntentionSelect->GetSelectionConeRadius();

	m_pSphereVis->GetModel()->SetCenter(vecPosition);   // draw a sphere at the cone's spike
	m_pConeVis->SetHeight(fLength);
	m_pConeVis->SetRadius(fRadius);


	// all following matrices are used for rotation
	VistaTransformMatrix mT(VistaQuaternion(),  VistaVector3D(0,0,-m_pConeVis->GetHeight())),
		                  mTm(VistaQuaternion(), VistaVector3D(0,0,m_pConeVis->GetHeight()));

	VistaTransformMatrix mq(qOri), mQ(VistaQuaternion(VistaAxisAndAngle(VistaVector3D(0,1,0), Vista::Pi)));


	// go to the point where you want to rotate around, then rotate, go back to you startpoint and rotate
	// around the y-axis
	VistaTransformMatrix m = mQ * mTm * mq * mT;


	VistaQuaternion q = VistaQuaternion(m);

	// now set the rotation for the cone
	VistaAxisAndAngle aa = q.GetAxisAndAngle();
	m_pConeVis->SetRotate(Vista::RadToDeg(aa.m_fAngle), aa.m_v3Axis[Vista::X], aa.m_v3Axis[Vista::Y], aa.m_v3Axis[Vista::Z]);

	// now set cone's center, therefor you have to add the rotatet(1) height of the cone to the 
	// center of your input - device
	VistaVector3D v3 (0,0,-m_pConeVis->GetHeight());
	v3 = q.Rotate(v3);
	m_pConeVis->SetCenter( vecPosition+v3 );
		

	// now we are ready to draw both visualizations
	m_pConeVis->DrawOpaque();
	m_pSphereVis->DrawOpaque();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaSelectConePointer::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_TRANSPARENT;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaSelectConePointer::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaSelectConePointer::GetBounds(VistaBoundingBox &)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaSelectConePointer::CreateProperties() const
{
	return new IVflRenderable::VflRenderableProperties;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetColor                                                */
/*                                                                            */
/*============================================================================*/
void VfaSelectConePointer::SetColor(const VistaColor& color)
{
	m_pConeVis->GetProperties()->SetColor(color);
	m_pSphereVis->GetProperties()->SetColor(color);
}
void VfaSelectConePointer::SetColor(float fColor[4])
{
	m_pConeVis->GetProperties()->SetColor(fColor);
	m_pSphereVis->GetProperties()->SetColor(fColor);
}

VistaColor VfaSelectConePointer::GetColor() const
{
	VistaColor color = m_pConeVis->GetProperties()->GetColor();
	return color;
}
void VfaSelectConePointer::GetColor(float fColor[4]) const
{
	m_pConeVis->GetProperties()->GetColor(fColor);
}



/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaPointerVis.cpp"		         						      */
/*============================================================================*/
