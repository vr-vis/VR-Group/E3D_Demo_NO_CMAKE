

set( RelativeDir "./Interaction" )
set( RelativeSourceGroup "Source Files\\Interaction" )

set( DirFiles
	VfaApplicationContextObject.cpp
	VfaApplicationContextObject.h
	VfaApplicationStateInfo.cpp
	VfaApplicationStateInfo.h
	VfaApplicationStateManager.cpp
	VfaApplicationStateManager.h
	VfaArrowPointer.cpp
	VfaArrowPointer.h
	VfaBeamPointer.cpp
	VfaBeamPointer.h
	VfaCommandToActionHandler.cpp
	VfaCommandToActionHandler.h
	VfaCrosshairPointer.cpp
	VfaCrosshairPointer.h
	VfaPointerManager.cpp
	VfaPointerManager.h
	VfaSelectConePointer.cpp
	VfaSelectConePointer.h
	VfaTargetPointer.cpp
	VfaTargetPointer.h
	VfaTransformableMediator.cpp
	VfaTransformableMediator.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

