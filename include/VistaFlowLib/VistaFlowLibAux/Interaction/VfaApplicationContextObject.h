/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAAPPLICATIONCONTEXTOBJECT_H
#define _VFAAPPLICATIONCONTEXTOBJECT_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaObserveable.h>
#include <VistaAspects/VistaNameable.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <map>
#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*
* The application context describes the common interaction context of a FlowLib 
* application. This describtion is necessary to define a context for DataFlowNet-
* networks.
*
* Necessary components of an application context are a viewer and a pointer "sensor".
* Additional "sensors" can be added and named on demand.
* In addition, boolean command's can be registered, which correspond to certain basic actions(e.g.
* pressing a button).
* Last but not least, a time slot is used to get frequently updates(e.g., once per frame).
*
*/
class VISTAFLOWLIBAUXAPI VfaApplicationContextObject : public IVistaObserveable,
									public IVistaNameable
{
public:
	enum eMsg{ 
		MSG_SENSORCHANGE = 0, 
		MSG_COMMANDCHANGE, 
		MSG_TIMECHANGE 
	};

	/*!
	 *
	 */
	enum eSensorSlots {
		SLOT_VIEWER_WORLD = 0, 
		SLOT_POINTER_WORLD, 
		SLOT_CURSOR_WORLD,
		SLOT_VIEWER_VIS,
		SLOT_POINTER_VIS,
		SLOT_CURSOR_VIS,
		SLOT_LAST
	};

	VfaApplicationContextObject();
	virtual ~VfaApplicationContextObject();

	struct sSensorFrame {
		VistaVector3D v3Position;
		VistaQuaternion qOrientation;
	};

	/**
	*
	*
	*/
	int RegisterCommand(const std::string& sCommandName);

	/**
	 * Register a new sensor.
	 * The name you give here will serve as template for the 
	 * port names created in the dfn, i.e., the ports you want to
	 * connect your input devices to. For each registered sensor 
	 * "mySensor", there will be two ports in the dfn named
 	 * "mySensor_pos" and "mySensor_ori". Moreover you'll automatically 
	 * get two new slots, one measuring the sensor in world space and
	 * one measuring the sensor in vis space. Each slot aggregates the position
	 * and orientation for one sensor.
	 */
	int RegisterSensor(const std::string& sSensorName);

	/*!
	 * TODO_HIGH: Document these two functions and their relation to the global/local
	 *			  versions (which are currently protected).
	 */
	bool GetSensorFrame(int iSlot, VflRenderNode *pRenderNode,
		sSensorFrame &oFrame);
	sSensorFrame GetSensorFrame(int iSlot, VflRenderNode *pRenderNode);

	/*
	*
	*
	*/
	bool GetCommandState(int iID) const;

/*
	*
	*
	*/
	double GetTime() const;

	/**
	 *
	 */
	std::string GetNameForNameable() const;
	void   SetNameForNameable(const std::string &sNewName);

	int GetChangedSensorSlot() const;
	int GetChangedCommandSlot() const;
	
	/*!
	 * this is for the ApplicationContextNode
	 */
	bool GetSensors(std::vector<std::string> &vecSensors) const;
	bool GetCommands(std::vector<std::string> &vecCommands) const;

	bool SetSensorFrame(int iSlot, 
						 const VistaVector3D & pPos,
						 const VistaQuaternion &qOri);
	bool UpdateLocalFramesTick(int iGlobalSlot);

	bool SetCommandState(int iID, const bool & bValue);

	bool SetTime(double dTime);

protected:
	/*
	*
	*
	*/
	bool GetGlobalSensorFrame(int iSlot, sSensorFrame &oFrame) const;

	/*!
	 *
	 */
	bool GetLocalSensorFrame(int iSlot, VflRenderNode *pRenderNode,
		sSensorFrame &oFrame);

private:
	

	std::vector<VfaApplicationContextObject::sSensorFrame> m_vecSensorFrames;
	std::vector<std::string> m_vecRegisteredSensors;

	std::vector<bool>		 m_vecCommandIds;
	std::vector<std::string> m_vecRegisteredCommands;

	std::string				 m_sName;
	int						 m_iChangedSensorSlot;
	int						 m_iChangedCommandSlot;

	double					 m_dTime;

	struct sFrameBuffer {
		sSensorFrame	oSensorFrame;
		bool			bIsUpToDate;
	};
	
	std::map<int, std::map<VflRenderNode*, sFrameBuffer> >
		m_mapLocalFrameBuffers;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_RAYTRANSFORM_H
