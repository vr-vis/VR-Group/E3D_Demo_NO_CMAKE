/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAPOINTERMANAGER_H
#define _VFAPOINTERMANAGER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaApplicationContextObject.h"
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaBase/VistaVectorMath.h>
#include <map>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaApplicationContextObject;
class VfaIntentionSelectFocusStrategy;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI IVfaPointer : public IVflRenderable
{
public: 
	IVfaPointer();
	virtual ~IVfaPointer();

	void SetPosition(const VistaVector3D &v3Center);
	void SetOrientation(const VistaQuaternion &qQuat);

	void GetPosition(VistaVector3D &v3Center) const;
	void GetOrientation(VistaQuaternion &qQuat) const;

private:
	VistaVector3D m_v3Position;
	VistaQuaternion m_qOrientation;
};
/**
 * A renderable tool to visualize the selection cone from a IntentionSelect strategy.
 *
 *
 */
class VISTAFLOWLIBAUXAPI VfaPointerManager : public VflObserver
{
public: 
	VfaPointerManager(VfaApplicationContextObject* pContext);
	virtual ~VfaPointerManager();

	/**
	 * 
	 */
	bool ActivatePointer(const std::string& sModeName);
	/**
	 * 
	 */
	bool DeactivatePointer();

	/**
	 * 
	 */
	void SetPointerVisible(bool bVisible);
	bool GetPointerVisible() const;

	/**
	 * 
	 */
	void RegisterPointer(	const std::string& sModeName, 
							IVfaPointer *pPointer,
							int iFrame =VfaApplicationContextObject::SLOT_POINTER_VIS);
	
	/**
	 * 
	 */
	void DeregisterPointer(const std::string& sModeName);

	bool GetListRegisteredModes(std::list<std::string> & sListModes);

	// Observer interface
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);	

protected:
	virtual void UpdateActivePointer();
	
	struct SPointerInfo{
		SPointerInfo();
		SPointerInfo(IVfaPointer*, int iFrame);

		IVfaPointer *pPointer;
		int iFrame;
	};

	SPointerInfo m_oActivePointer;
	VfaApplicationContextObject* m_pAppContext;

private:
	std::map<std::string, SPointerInfo> m_mapPointerModes;


	//VistaVector3D m_v3Position;
	//VistaQuaternion m_qOrientation;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAPOINTERMANAGER_H
