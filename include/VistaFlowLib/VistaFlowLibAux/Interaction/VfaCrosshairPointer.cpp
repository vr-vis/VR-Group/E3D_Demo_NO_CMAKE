/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <GL/glew.h>

#include "VfaCrosshairPointer.h"

#include "../Widgets/Crosshair/VfaCrosshairVis.h"
#include "../Widgets/Crosshair/VfaCrosshairModel.h"

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaPointerVis                                        */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaCrosshairPointer::VfaCrosshairPointer()
	:	m_pCrosshairModel(new VfaCrosshairModel())
	
{
	m_pVis = new VfaCrosshairVis(m_pCrosshairModel);
	m_pVis->Init();
	//do not register with any vis controller here -> we act as proxy
}

VfaCrosshairPointer::~VfaCrosshairPointer()
{
	delete m_pVis;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairPointer::DrawOpaque()
{
	if(!this->GetProperties()->GetVisible())
		return;

	// update crosshair model
	VistaVector3D v3Center;
	this->GetPosition(v3Center);
	m_pCrosshairModel->SetCenter (v3Center);

	// draw crosshair	
	m_pVis->DrawOpaque();

	
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaCrosshairPointer::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCrosshairPointer::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return m_pVis->GetBounds(minBounds, maxBounds);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaCrosshairPointer::GetBounds(VistaBoundingBox & bb)
{
	return m_pVis->GetBounds(bb);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetModel                                                    */
/*                                                                            */
/*============================================================================*/
VfaCrosshairModel* VfaCrosshairPointer::GetModel() const
{
	return m_pCrosshairModel;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaCrosshairPointer::CreateProperties() const
{
	return new IVflRenderable::VflRenderableProperties;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetColor                                                */
/*                                                                            */
/*============================================================================*/
void VfaCrosshairPointer::SetColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	this->SetColor(fColor);
}
void VfaCrosshairPointer::SetColor(float fColor[4])
{
	m_pVis->GetProperties()->SetColorForAxis(0, fColor);
	m_pVis->GetProperties()->SetColorForAxis(1, fColor);
	m_pVis->GetProperties()->SetColorForAxis(2, fColor);
}
VistaColor VfaCrosshairPointer::GetColor() const
{
	float fColor[4];
	fColor[3] = 1.0f;
	this->GetColor(fColor);
	VistaColor color(fColor);
	return color;
}
void VfaCrosshairPointer::GetColor(float fColor[4]) const
{
	fColor[3] = 1.0f;
	m_pVis->GetProperties()->GetColorForAxis(0, fColor);
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetWidth/SetWidth                                          */
/*                                                                            */
/*============================================================================*/
float VfaCrosshairPointer::GetLineWidth() const
{
	return m_pVis->GetProperties()->GetLineWidth();
}

void VfaCrosshairPointer::SetLineWidth(const float fWidth) 
{
	m_pVis->GetProperties()->SetLineWidth(fWidth);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSize/SetSize                                             */
/*                                                                            */
/*============================================================================*/
float VfaCrosshairPointer::GetSize() const
{
	return 2.0f*m_pVis->GetProperties()->GetWingLength();
}

void VfaCrosshairPointer::SetSize(float fSize)
{
	m_pVis->GetProperties()->SetWingLength(0.5f*fSize);
}
	

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaPointerVis.cpp"		         						      */
/*============================================================================*/
