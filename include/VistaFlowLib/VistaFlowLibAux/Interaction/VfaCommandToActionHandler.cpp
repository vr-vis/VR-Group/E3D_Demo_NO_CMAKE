/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VfaCommandToActionHandler.h" 
#include "VfaApplicationContextObject.h" 

#include <VistaAspects/VistaExplicitCallbackInterface.h>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/


/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaCommandToActionHandler::VfaCommandToActionHandler(VfaApplicationContextObject *pAppContext)
: VflObserver(),
  m_pAppContext(pAppContext)
{
	this->Observe (m_pAppContext);
}

VfaCommandToActionHandler::~VfaCommandToActionHandler()
{
	this->ReleaseObserveable (m_pAppContext);
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/


void VfaCommandToActionHandler::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if (msg != VfaApplicationContextObject::MSG_COMMANDCHANGE)
		return;

	int iChangedCommandId = m_pAppContext->GetChangedCommandSlot();
	
	// if the command's state is true
	if (m_pAppContext->GetCommandState(iChangedCommandId))
	{
		// do we have a callback for this command?
		std::map<int, IVistaExplicitCallbackInterface*>::const_iterator it = m_mpActions.find (iChangedCommandId);
		if(it != m_mpActions.end())
		{
			// execute it!
			IVistaExplicitCallbackInterface *pI = (*it).second;
			pI->Do();
		}
	}
	
}


bool VfaCommandToActionHandler::RegisterAction(int iCommandID, IVistaExplicitCallbackInterface *pI)
{
	std::map<int, IVistaExplicitCallbackInterface*>::const_iterator it = m_mpActions.find(iCommandID);
	
	if (it == m_mpActions.end())
	{
		m_mpActions[iCommandID] = pI;
		return true;
	}

	return false;
}


bool VfaCommandToActionHandler::GetIsRegistered(IVistaExplicitCallbackInterface *pI) const
{

	for(std::map<int, IVistaExplicitCallbackInterface*>::const_iterator it = m_mpActions.begin();
		it != m_mpActions.end(); ++it)
	{
		if((*it).second == pI)
			return true;
	}

	return false;
}

bool VfaCommandToActionHandler::UnregisterAction(int iCommandID, bool bDelete)
{
	std::map<int, IVistaExplicitCallbackInterface*>::iterator it = m_mpActions.find(iCommandID);
	if(it != m_mpActions.end())
	{
		m_mpActions.erase(it);
		return true;
	}
	return false;
}

IVistaExplicitCallbackInterface *VfaCommandToActionHandler::GetCallback(int iCommandID) const
{
	std::map<int, IVistaExplicitCallbackInterface*>::const_iterator it = m_mpActions.find(iCommandID);
	if(it != m_mpActions.end())
	{
		return (*it).second;
	}
	return NULL;
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VfaCommandToActionHandler.CPP"                                */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

