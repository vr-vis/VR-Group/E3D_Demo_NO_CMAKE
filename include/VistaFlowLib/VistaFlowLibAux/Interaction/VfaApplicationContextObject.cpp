/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include <iostream>

#include "VfaApplicationContextObject.h" 
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaDataFlowNet/VdfnUtil.h>

#include <VistaBase/VistaVectorMath.h>

#include <cassert>

using namespace std;


/*============================================================================*/
/* CON-/DESTRUCTORS															  */
/*============================================================================*/
VfaApplicationContextObject::VfaApplicationContextObject()
	:	m_iChangedSensorSlot(-1),
		m_iChangedCommandSlot(-1)
{

	const int iViewerWorld	= this->RegisterSensor("viewer");
	const int iPointerWorld = this->RegisterSensor("pointer");
	const int iCursorWorld	= this->RegisterSensor("cursor");

	assert(iPointerWorld == SLOT_POINTER_WORLD);
	assert(iViewerWorld == SLOT_VIEWER_WORLD);
	assert(iCursorWorld == SLOT_CURSOR_WORLD);

	map<VflRenderNode*, sFrameBuffer>	mapViewBuf,
										mapPointerBuf,
										mapCursorBuf;
	
	m_mapLocalFrameBuffers[SLOT_VIEWER_VIS] = mapViewBuf;
	m_mapLocalFrameBuffers[SLOT_POINTER_VIS] = mapPointerBuf;
	m_mapLocalFrameBuffers[SLOT_CURSOR_VIS] = mapCursorBuf;
}

VfaApplicationContextObject::~VfaApplicationContextObject()
{
}


/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
std::string VfaApplicationContextObject::GetNameForNameable() const
{
	return m_sName;
}
void   VfaApplicationContextObject::SetNameForNameable(const std::string &sNewName)
{
	m_sName = sNewName;
}

int VfaApplicationContextObject::RegisterCommand(const std::string &sCommandName)
{
	// resize vectors +1 new
	size_t iIdx = m_vecCommandIds.size();
	m_vecCommandIds.resize(iIdx+1);
	m_vecRegisteredCommands.resize(iIdx+1);

	// set values of last element
	m_vecCommandIds[iIdx] = false;
	m_vecRegisteredCommands[iIdx] = sCommandName;

	// return index of cmd in vector
	return static_cast<int>(iIdx);
}

int VfaApplicationContextObject::RegisterSensor(const std::string &sSensorName)
{
	// resize frame vector +1
	size_t iIdx = m_vecSensorFrames.size();
	m_vecSensorFrames.resize(iIdx+1);
	
	// resize sensors vector +1 new
	m_vecRegisteredSensors.resize(m_vecRegisteredSensors.size()+1);
	m_vecRegisteredSensors[iIdx] = sSensorName;

	return static_cast<int>(iIdx);
}

bool VfaApplicationContextObject::GetSensorFrame(int iSlot,
	VflRenderNode *pRenderNode,
	VfaApplicationContextObject::sSensorFrame &oFrame)
{
	if(iSlot >= SLOT_VIEWER_WORLD && iSlot <= SLOT_CURSOR_WORLD)
		return GetGlobalSensorFrame(iSlot, oFrame);
	else if(iSlot >= SLOT_VIEWER_VIS && iSlot <= SLOT_CURSOR_VIS)
		return GetLocalSensorFrame(iSlot, pRenderNode, oFrame);

	return false;
}

VfaApplicationContextObject::sSensorFrame
VfaApplicationContextObject::GetSensorFrame(int iSlot,
											 VflRenderNode *pRenderNode)
{
	sSensorFrame oBuffer;

	bool bResult = GetSensorFrame(iSlot, pRenderNode, oBuffer);

	// Went everything just fine?
	assert(bResult);

	return oBuffer;
}


bool VfaApplicationContextObject::GetGlobalSensorFrame(int iSlot, 
	VfaApplicationContextObject::sSensorFrame & oFrame) const
{
	if((iSlot>=0)&&((unsigned int)iSlot< m_vecSensorFrames.size()))
	{
		oFrame = m_vecSensorFrames[iSlot];
		return true;
	}

	return false;
}

bool VfaApplicationContextObject::GetLocalSensorFrame(int iSlot,
	VflRenderNode *pRenderNode,
	VfaApplicationContextObject::sSensorFrame &oFrame)
{
	// Check if the port is right.
	if(iSlot < SLOT_VIEWER_VIS || iSlot > SLOT_CURSOR_VIS || !pRenderNode)
		return false;

	// Try to retrieve the frame from the corresponding buffer.
	map<VflRenderNode*, sFrameBuffer>::iterator it =
		m_mapLocalFrameBuffers[iSlot].find(pRenderNode);

	// If the frame could be retrieved from the buffer AND is up-to-date
	// it can be directly returned.
	if(it != m_mapLocalFrameBuffers[iSlot].end()
		&& (*it).second.bIsUpToDate)
	{
		oFrame = (*it).second.oSensorFrame;
	}
	// In case there was no buffer entry or the buffer entry was dirty,
	// a new buffer will have to be created or an existing buffer entry
	// needs to be updated.
	else
	{
		// Retrieve inverse transform matrix of the specified RenderNode
		// as it will be used to transform to the RenderNode's local
		// coordinate frame.
		VistaTransformMatrix mInvTrans;
		pRenderNode->GetWorldInverseTransform(mInvTrans);

		// Create a new buffer entry.
		sFrameBuffer oNewBuffer;

		// Transform the global position & orientation to the local frame of
		// the RenderNode.
		oNewBuffer.oSensorFrame.v3Position =
			mInvTrans.Transform(m_vecSensorFrames[iSlot-3].v3Position);
				
		// Do the same for the global orientation/rotation.
		VistaVector3D vTranslation, vScale;
		VistaQuaternion qRotation;
		// Get rid of the scaling factor .
		if(!mInvTrans.Decompose(vTranslation, qRotation, vScale))
		{
			// If there is a shearing component ..
			VistaTransformMatrix mRotTrans;
			for(int i = 0; i < 3; ++i)
			{
				VistaVector3D vCol = mInvTrans.GetColumn(i);
				VistaVector3D vNorm = vCol.GetNormalized();
				VistaTransformMatrix mRotTrans;
				mRotTrans.SetColumn(i, vNorm);
			}
			VistaQuaternion qRotation = VistaQuaternion(mRotTrans);
		}
		oNewBuffer.oSensorFrame.qOrientation =
			qRotation.GetNormalized() * m_vecSensorFrames[iSlot-3].qOrientation;

		// Set up-to-date flag.
		oNewBuffer.bIsUpToDate = true;

		// Save to buffer and set the out variable.
		m_mapLocalFrameBuffers[iSlot][pRenderNode] = oNewBuffer;
		oFrame = oNewBuffer.oSensorFrame;
	}
	
	return true;
}


bool VfaApplicationContextObject::GetCommandState(int iID) const
{
	if((iID>=0) &&((unsigned int)iID < m_vecCommandIds.size()))
		return m_vecCommandIds[iID];

	return false;
}


int VfaApplicationContextObject::GetChangedSensorSlot() const
{
	return m_iChangedSensorSlot;
}

int VfaApplicationContextObject::GetChangedCommandSlot() const
{
	return m_iChangedCommandSlot;
}


bool VfaApplicationContextObject::SetSensorFrame(int iSlot, 
												   const VistaVector3D & pPos,
												   const VistaQuaternion &qOri)
{
	if((iSlot>=0)&&((unsigned int)iSlot< m_vecSensorFrames.size()))
	{
		m_vecSensorFrames[iSlot].v3Position = pPos;
		m_vecSensorFrames[iSlot].qOrientation = qOri;
		m_iChangedSensorSlot = iSlot;
		this->Notify(MSG_SENSORCHANGE);

		// Don't forget to mark the corresponding local buffers as dirty.		
		map<VflRenderNode*, sFrameBuffer>::iterator it
			= m_mapLocalFrameBuffers[iSlot+3].begin();

		while(it != m_mapLocalFrameBuffers[iSlot+3].end())
		{
			(*it).second.bIsUpToDate = false;

			++it;
		}

		// Repeat the notify to signal that the local frames also changed!
		m_iChangedSensorSlot = iSlot + 3; 
		this->Notify(MSG_SENSORCHANGE);
		
		return true;
	}

	return false;
}

bool VfaApplicationContextObject::UpdateLocalFramesTick(int iGlobalSlot)
{
	// TODO_HIGH: Document this function.
	m_iChangedSensorSlot = iGlobalSlot + 3;

	// Don't forget to mark the corresponding local buffers as dirty.		
	map<VflRenderNode*, sFrameBuffer>::iterator it
		= m_mapLocalFrameBuffers[m_iChangedSensorSlot].begin();

	while(it != m_mapLocalFrameBuffers[m_iChangedSensorSlot].end())
	{
		(*it).second.bIsUpToDate = false;
		++it;
	}

	this->Notify(MSG_SENSORCHANGE);

	return true;
}


bool VfaApplicationContextObject::SetCommandState(int iID, const bool & bValue)
{
	if((iID>=0) &&((unsigned int)iID < m_vecCommandIds.size()))
	{
		if(m_vecCommandIds[iID] == bValue)
			return false;

		m_vecCommandIds[iID] = bValue;
		m_iChangedCommandSlot = iID;
		this->Notify(MSG_COMMANDCHANGE);
		return true;
	}

	return false;
}


bool VfaApplicationContextObject::GetSensors(std::vector<std::string> & vecSensors) const
{
	vecSensors = m_vecRegisteredSensors;
	return true;
}

bool VfaApplicationContextObject::GetCommands(std::vector<std::string> & vecCommands) const
{
	vecCommands = m_vecRegisteredCommands;
	return true;
}

double VfaApplicationContextObject::GetTime() const
{
	return m_dTime;
}

bool VfaApplicationContextObject::SetTime(double dTime)
{
	m_dTime = dTime;
	this->Notify(MSG_TIMECHANGE);

	return true;
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "MYDEMO.CPP"                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

