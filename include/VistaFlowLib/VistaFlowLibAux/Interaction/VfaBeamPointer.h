/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFABEAMPOINTER_H
#define _VFABEAMPOINTER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VfaPointerManager.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaObserver.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A renderable tool to visualize the selection cone from a IntentionSelect strategy.
 *
 *
 */
class VISTAFLOWLIBAUXAPI VfaBeamPointer : public IVfaPointer
{
public: 
	//lighting was previously disabled so the standard behaviour will stay the same
	VfaBeamPointer(bool bEnableLighting = false); 
	virtual ~VfaBeamPointer();

	virtual void DrawOpaque();

	virtual unsigned int GetRegistrationMode() const;


	/**
	 * Get the beam's AABB in ViSTA style format
	 */
	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);

	void SetColor(const VistaColor& color);
	VistaColor GetColor() const;
	/**
	 * Set the beam's color as rgba value i.e.
	 * the input float pointer is assumed to point to
	 * an array of length 4.
	 */
	void SetColor(float fColor[4]);
	/**
	 * Get the beam's color as rgba value.
	 */
	void GetColor(float fColor[4]) const;

	VistaVector3D GetHeadPosition();

	/**
	 * Returns length of target beam
	 * 
	 * @param   
	 * @return  --
	 */    
	float	GetLength(void) const;
	/**
	 * Sets lenght of target beam
	 * 
	 * @param   float fLength
	 * @return  --
	 */    
	void	SetLength(const float fLength);
	/**
	 * Returns width of target beam
	 * 
	 * @param   
	 * @return  --
	 */    
	float	GetWidth(void) const;
	/**
	 * Sets width of target beam
	 * 
	 * @param   float fWidth
	 * @return  --
	 */    
	void	SetWidth(const float fWidth);

	/**
	 * Toggles lighting of beam ray
	 * 
	 * @param   bool bEnableLighting
	 * @return  --
	 */    

	void SetEnableLighting(bool bEnableLighting);

	/**
	 * Returns whether lighting state is enabled
	 * 
	 * @param   float fWidth
	 * @return  --
	 */    
	bool GetEnableLighting() const;

protected:
	virtual VflRenderableProperties* CreateProperties() const;

private:
	float				m_fLength;
	float				m_fWidth;
	bool                m_bEnableLighting;

	float m_fColor[4];

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASELECTCONEVIS_H
