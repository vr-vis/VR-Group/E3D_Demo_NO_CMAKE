/*==============================qqq==============================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <GL/glew.h>

#include "VfaBeamPointer.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#ifdef WIN32
#include <Windows.h>
#endif

#if defined(DARWIN)
  #include <OpenGL/gl.h>
#else
  #include <GL/gl.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaPointerVis                                        */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaBeamPointer::VfaBeamPointer(bool bEnableLighting)
:IVfaPointer(),
m_fLength(1.0f),
m_fWidth(0.1f),
m_bEnableLighting(bEnableLighting)
{
	//blue
	m_fColor[0] = 0.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 1.0f;
	m_fColor[3] = 1.0f;
}

VfaBeamPointer::~VfaBeamPointer()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaBeamPointer::DrawOpaque()
{
	if(!this->GetProperties()->GetVisible())
		return;

	VistaVector3D vecPosition;
	VistaQuaternion qOri;
	this->GetPosition(vecPosition);
	this->GetOrientation(qOri);

	glPushAttrib(GL_ENABLE_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	if(!m_bEnableLighting)
		glDisable(GL_LIGHTING);
	else
		glEnable(GL_LIGHTING);

	glEnable(GL_NORMALIZE);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	// draw a sphere at the cone's spike
	glTranslatef(vecPosition[0], vecPosition[1], vecPosition[2]);
	
	// rotate cone accoridng to pointer device's orientation
	float fDenom = sqrt(1.0f - qOri[3] * qOri[3]);
	if(fDenom > numeric_limits<float>::epsilon())
	{
		glRotatef(
			Vista::RadToDeg(2.0f * acos(qOri[3])), 
			qOri[0] / fDenom,
			qOri[1] / fDenom,
			qOri[2] / fDenom
			);
	}

	float v3Min[3] = {-m_fWidth / 2.0f, -m_fWidth / 2.0f, -m_fLength};
	float v3Max[3] = { m_fWidth / 2.0f,  m_fWidth / 2.0f,       0.0f};
	// draw box
	glColor3f(m_fColor[0], m_fColor[1], m_fColor[2]);
	// back
	glBegin(GL_QUADS);
		glNormal3f(0,0,-1);
		glVertex3f(v3Min[0], v3Min[1], v3Min[2]);
		glVertex3f(v3Max[0], v3Min[1], v3Min[2]);
		glVertex3f(v3Max[0], v3Max[1], v3Min[2]);
		glVertex3f(v3Min[0], v3Max[1], v3Min[2]);
	glEnd();
	// front
	glBegin(GL_QUADS);
		glNormal3f(0,0,1);
		glVertex3f(v3Min[0], v3Min[1], v3Max[2]);
		glVertex3f(v3Max[0], v3Min[1], v3Max[2]);
		glVertex3f(v3Max[0], v3Max[1], v3Max[2]);
		glVertex3f(v3Min[0], v3Max[1], v3Max[2]);
	glEnd();
	// upper
	glBegin(GL_QUADS);
		glNormal3f(0,1,0);
		glVertex3f(v3Min[0], v3Max[1], v3Min[2]);
		glVertex3f(v3Max[0], v3Max[1], v3Min[2]);
		glVertex3f(v3Max[0], v3Max[1], v3Max[2]);
		glVertex3f(v3Min[0], v3Max[1], v3Max[2]);
	glEnd();
	// lower
	glBegin(GL_QUADS);
		glNormal3f(0,-1,0);
		glVertex3f(v3Min[0], v3Min[1], v3Min[2]);
		glVertex3f(v3Max[0], v3Min[1], v3Min[2]);
		glVertex3f(v3Max[0], v3Min[1], v3Max[2]);
		glVertex3f(v3Min[0], v3Min[1], v3Max[2]);
	glEnd();
	// left
	glBegin(GL_QUADS);
		glNormal3f(-1,0,0);
		glVertex3f(v3Min[0], v3Min[1], v3Min[2]);
		glVertex3f(v3Min[0], v3Min[1], v3Max[2]);
		glVertex3f(v3Min[0], v3Max[1], v3Max[2]);
		glVertex3f(v3Min[0], v3Max[1], v3Min[2]);
	glEnd();
	// right
	glBegin(GL_QUADS);
		glNormal3f(1,0,0);
		glVertex3f(v3Max[0], v3Min[1], v3Min[2]);
		glVertex3f(v3Max[0], v3Min[1], v3Max[2]);
		glVertex3f(v3Max[0], v3Max[1], v3Max[2]);
		glVertex3f(v3Max[0], v3Max[1], v3Min[2]);
	glEnd();

	glPopMatrix();
	glPopAttrib();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaBeamPointer::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaBeamPointer::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VfaBeamPointer::GetBounds(VistaBoundingBox &)
{
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
VistaVector3D VfaBeamPointer::GetHeadPosition()
{
	VistaQuaternion qBeamOri;
	GetOrientation(qBeamOri);

	VistaVector3D vBeamPos;
	GetPosition(vBeamPos);

	VistaVector3D v3(0,0,-m_fLength);
	v3 = qBeamOri.Rotate(v3);

	return vBeamPos+v3;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaBeamPointer::CreateProperties() const
{
	return new IVflRenderable::VflRenderableProperties;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetColor                                                */
/*                                                                            */
/*============================================================================*/
void VfaBeamPointer::SetColor(const VistaColor& color)
{
	color.GetValues(m_fColor);
}
void VfaBeamPointer::SetColor(float fColor[4])
{
	m_fColor[0] = fColor[0];
	m_fColor[1] = fColor[1];
	m_fColor[2] = fColor[2];
	m_fColor[3] = fColor[3];
}
VistaColor VfaBeamPointer::GetColor() const
{
	VistaColor color(m_fColor);
	return color;
}
void VfaBeamPointer::GetColor(float fColor[4]) const
{
	fColor[0] = m_fColor[0];
	fColor[1] = m_fColor[1];
	fColor[2] = m_fColor[2];
	fColor[3] = m_fColor[3];
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLength/SetLength                                         */
/*                                                                            */
/*============================================================================*/
float VfaBeamPointer::GetLength(void) const
{
	return m_fLength;
}

void VfaBeamPointer::SetLength(const float fLength) 
{
	m_fLength = fLength;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetWidth/SetWidth                                          */
/*                                                                            */
/*============================================================================*/
float VfaBeamPointer::GetWidth(void) const
{
	return m_fWidth;
}

void VfaBeamPointer::SetWidth(const float fWidth) 
{
	// set new width
	m_fWidth = fWidth;

}

void VfaBeamPointer::SetEnableLighting(bool bEnableLighting)
{
	m_bEnableLighting = bEnableLighting;
}

bool VfaBeamPointer::GetEnableLighting() const
{
	return m_bEnableLighting;
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaPointerVis.cpp"		         						      */
/*============================================================================*/
