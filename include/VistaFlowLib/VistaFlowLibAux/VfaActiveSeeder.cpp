/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaActiveSeeder.h"
#include "VfaSeeder.h"
#include "Interaction/VfaApplicationContextObject.h"

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME : IVfaActiveSeeder	                                                  */
/*                                                                            */
/*============================================================================*/
IVfaActiveSeeder::IVfaActiveSeeder ()
:m_pSeeder(NULL),
m_bIsActive(false)
{
}

IVfaActiveSeeder::~IVfaActiveSeeder()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME : SetSeeder		                                                  */
/*                                                                            */
/*============================================================================*/
void IVfaActiveSeeder::SetSeeder( IVfaSeeder * pSeeder )
{
	m_pSeeder = pSeeder;
}

/*============================================================================*/
/*                                                                            */
/*  NAME : GetSeeder		                                                  */
/*                                                                            */
/*============================================================================*/
IVfaSeeder * IVfaActiveSeeder::GetSeeder()
{
	return m_pSeeder;
}

/*============================================================================*/
/*                                                                            */
/*  NAME : SetIsActive		                                                  */
/*                                                                            */
/*============================================================================*/
void IVfaActiveSeeder::SetIsActive (bool bActive)
{
	m_bIsActive = bActive;
}


/*============================================================================*/
/*                                                                            */
/*  NAME : GetIsActive		                                                  */
/*                                                                            */
/*============================================================================*/
bool IVfaActiveSeeder::GetIsActive () const
{
	return m_bIsActive;
}

/*============================================================================*/
/*                                                                            */
/*  NAME : Activate  		                                                  */
/*                                                                            */
/*============================================================================*/
void IVfaActiveSeeder::Activate ()
{
	m_bIsActive = true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME : Deactivate		                                                  */
/*                                                                            */
/*============================================================================*/
void IVfaActiveSeeder::Deactivate()
{
	m_bIsActive = false;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME : VfaActiveRealTimeSeeder                                           */
/*                                                                            */
/*============================================================================*/
VfaActiveRealTimeSeeder::VfaActiveRealTimeSeeder (VfaApplicationContextObject *pAppContext)
:IVfaActiveSeeder(), VflObserver(),
m_pAppContext(pAppContext),
m_dSeedingInterval(0.0),
m_dLastTimeSeeding(0.0)
{
	this->Observe (m_pAppContext);
}

VfaActiveRealTimeSeeder::~VfaActiveRealTimeSeeder()
{
	this->ReleaseObserveable (m_pAppContext);
}

void VfaActiveRealTimeSeeder::SetSeedingInterval (double dInterval)
{
	m_dSeedingInterval = dInterval;
}

double VfaActiveRealTimeSeeder::GetSeedingInterval () const
{
	return m_dSeedingInterval;
}

void VfaActiveRealTimeSeeder::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if (msg != VfaApplicationContextObject::MSG_TIMECHANGE)
		return;

	if (GetSeeder()==NULL)
		return;

	// skip if the active seeder is not active
	if (!GetIsActive())
		return;

	double dTime = m_pAppContext->GetTime();
	
	// check interval since last seeding
	if (dTime-m_dLastTimeSeeding > m_dSeedingInterval)
	{
		GetSeeder()->Seed();
		m_dLastTimeSeeding = dTime;
	}
	
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
