/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFASYSTEMUPDATEHANDLER_H
#define _VFASYSTEMUPDATEHANDLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Data/VflObserver.h>
#include <vector>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaApplicationContextObject;
class VflVisController;
class IVistaExplicitCallbackInterface;
class IVistaTransformable;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaSystemByContextUpdater : public VflObserver
{
public:
	VfaSystemByContextUpdater(VfaApplicationContextObject *pApplContext,
		VflVisController *pVisCtrl);
	virtual ~VfaSystemByContextUpdater();

	int RegisterAction(IVistaExplicitCallbackInterface *);
	bool GetIsRegistered(IVistaExplicitCallbackInterface *) const;
	bool UnregisterAction(int i, bool bDelete);
	bool UnregisterAction(IVistaExplicitCallbackInterface *, bool bDelete);
	IVistaExplicitCallbackInterface *GetCallback(int i) const;

	void SetLightTransform(IVistaTransformable *);
	IVistaTransformable *GetLightTransform() const;

	// Observer interface
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

protected:
private:
	VflVisController  *m_pVisCtrl;
	VfaApplicationContextObject *m_pApplContext;
	std::vector<IVistaExplicitCallbackInterface*> m_vecCallbacks;

	IVistaTransformable *m_pLightTransform;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_CLVSYSTEMUPDATEHANDLER_H
