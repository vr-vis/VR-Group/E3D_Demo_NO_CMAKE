/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _IVFAPOINTGENERATOR_H
#define _IVFAPOINTGENERATOR_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaVector3D;

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI IVfaPointGenerator
{
public:
	
	/**
	 * This interface provides a vector of float values as pointer.
	 * Those Vector contains 4-tupels. The first three values are
	 * 3D coordinates in world space. The fourth value is the timing
	 * information in sim time. (x,y,z,t)
	 * Use this interface in example for GPU particletracing.
	 * Example at VistaFlowLibDemos/WidgetSeeder
	 * 
	 * @return Number of points
	 * @see VistaFlowLibAux/VfaSeeder.h
	 */
	virtual int GetPoints( std::vector<float>& vecPoints4D ) = 0;

protected:
	
	IVfaPointGenerator(){};

	virtual ~IVfaPointGenerator(){};
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _IVFAPOINTGENERATOR_H */
