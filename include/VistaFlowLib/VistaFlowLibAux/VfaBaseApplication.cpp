/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// Includes
#include <GL/glew.h>

#include "VfaBaseApplication.h"
#include "VfaVistaSystemAbstraction.h"
#include "VfaSystemByContextUpdater.h"
#include "VfaDisplayEventObserver.h"
#include "VfaBasicActions.h"

// sub directory interaction
#include "Interaction/VfaApplicationContextObject.h"
//#include "Interaction/VistaLightTransformableAdapter.h"

// dfn nodes
#include "DfnNodes/VfaVisControllerTransformerNode.h"
#include "DfnNodes/VfaApplicationContextNode.h"
#include "DfnNodes/VfaIndirectXFormNode.h"
#include "DfnNodes/VdfnRotateOriNode.h"
#include "DfnNodes/VdfnRotateVectorNode.h"
#include "DfnNodes/VdfnValueToTriggerNode.h"
#include "DfnNodes/VdfnMultiplicationNode.h"
#include "DfnNodes/VdfnAdditionNode.h"
#include "DfnNodes/VdfnTransformReferenceFrameNode.h"
#include "DfnNodes/VdfnNonlinearFunctionNode.h"
#include "DfnNodes/VfaTimeNavigationNode.h"

// ViSTA
#include <VistaBase/VistaVersion.h>
#include <VistaDataFlowNet/VdfnObjectRegistry.h>
#include <VistaDataFlowNet/VdfnNodeFactory.h>
#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/DisplayManager/VistaDisplayManager.h>
#include <VistaKernel/DisplayManager/VistaDisplaySystem.h>
#include <VistaKernel/GraphicsManager/VistaSceneGraph.h>
#include <VistaKernel/GraphicsManager/VistaGroupNode.h>
#include <VistaKernel/GraphicsManager/VistaTransformNode.h>
#include <VistaKernel/GraphicsManager/VistaLightNode.h>
#include <VistaKernel/GraphicsManager/VistaOpenGLDraw.h>
#include <VistaKernel/GraphicsManager/VistaNodeBridge.h>
#include <VistaKernel/InteractionManager/VistaUserPlatform.h>
#include <VistaKernel/InteractionManager/VistaKeyboardSystemControl.h>
#include <VistaKernel/InteractionManager/VistaVirtualPlatformAdapter.h>

// FlowLib
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaFlowLib/Visualization/VflVisController.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/VflVtkLookupTable.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Tools/VflAxisInfo.h>
#include <VistaFlowLib/Tools/VflBoundsInfo.h>

#include <VistaBase/VistaExceptionBase.h>
#include <VistaAspects/VistaExplicitCallbackInterface.h>
#include <VistaAspects/VistaTransformable.h>
#include <VistaAspects/VistaObserver.h>

#include <cstdio>
#include <limits>


/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
class VfaCallbackAdapter : public IVistaOpenGLDraw
{
public:
	VfaCallbackAdapter(IVistaExplicitCallbackInterface *pTarget)
		: IVistaOpenGLDraw(), m_pTarget(pTarget)
	{}

	virtual ~VfaCallbackAdapter()
	{}

	virtual bool Do()
	{
		m_pTarget->Do();
		return true;
	}

	virtual bool GetBoundingBox(VistaBoundingBox &bb) const
	{
		// TODO: find an efficient way of communicating bounding box
		// and its updates
		return false;
	}

private:
	IVistaExplicitCallbackInterface *m_pTarget;
};

VfaBaseApplication::VfaBaseApplication(VistaSystem *pSystem,
											 bool bCreateBoundsInfo,
											 bool bCreateAxisInfo,
											 VistaLightNode *pLight)
: m_pSystemAbstraction(new VfaVistaSystemAbstraction(pSystem)),
  m_pController(NULL),
  m_pRenderNode(NULL),
  m_pFlowLibRoot(NULL),
  m_pUpdateHandler(NULL),
  m_pDisplayUpdate(NULL),
  m_pCtrlScene(NULL),
  m_pAxisInfo(NULL),
  m_pBoundsInfo(NULL),
  m_pSystem(pSystem),
  m_pLight(pLight),
  m_pLightTransform(NULL),
  m_pApplicationContext(NULL),
  m_pVisRefPoint(NULL),
  m_pCamAdapter(NULL)
{
	// First things first - init GLEW.
	if(GLEW_OK != glewInit())
		vstr::warnp() << "[VflRenderNode] GLEW could not be init'ed!" << std::endl;

	// create a new vis controller...this is the "heart" of each flowlib application
	m_pController = new VflVisController(pSystem);

	VistaGraphicsManager	*pGM = pSystem->GetGraphicsManager();
	VistaSceneGraph				*pSG = pGM->GetSceneGraph();

	// create "FlowLib root node" for this scene
	m_pFlowLibRoot = pGM->GetNodeBridge()->NewTransformNode(pSG->GetRoot(), pGM->GetNodeBridge()->NewTransformNodeData(), "FlowLib Scene Root");
	pSG->GetRoot()->AddChild(m_pFlowLibRoot);

	m_pRenderNode = new VflRenderNode(m_pFlowLibRoot, pGM->GetNodeBridge());
	m_pRenderNode->SetNameForNameable("BASIC_SCENE");
	m_pController->AddRenderNode(m_pRenderNode);

	VistaDisplaySystem *pDisp = pSystem->GetDisplayManager()->GetDisplaySystem();
	if(!pDisp)
		VISTA_THROW("VfaBaseApplication::VfaBaseApplication -- Exception(no DisplaySystem present)", 0x00000001);

	m_pCamAdapter = new VistaVirtualPlatformAdapter;
	m_pCamAdapter->SetVirtualPlatform(pSystem->GetPlatformFor( pDisp )->GetPlatform());
	// now register the pCamAdapt with the Dfn-ObjectRegistry
	VdfnObjectRegistry *pReg = pSystem->GetDfnObjectRegistry();
	pReg->SetObject( "camera", NULL, m_pCamAdapter );

	// register all kind of Dfn nodes, found in subdirectory Interaction
	VdfnNodeFactory *pFac = VdfnNodeFactory::GetSingleton();
	pFac->SetNodeCreator( "application_context", new VfaApplicationContextNodeCreate(pReg) );
	pFac->SetNodeCreator( "indirectxnode", new VfaIndirectXFormNodeCreate(pReg) );
	pFac->SetNodeCreator( "viscontrollerindirectxnode", new VfaVisControllerTransformerNodeCreate(pReg) );
	pFac->SetNodeCreator( "rotate_orientation", new VdfnRotateOriNodeCreate() );
	pFac->SetNodeCreator( "rotate_vector", new VdfnRotateVectorNodeCreate() );
	pFac->SetNodeCreator( "referenceframe", new VdfnTransformReferenceFrameNodeCreate() );
	pFac->SetNodeCreator( "value_to_trigger[int]", new VdfnValueToTriggerNodeCreate<int>(VistaAspectsConversionStuff::ConvertToInt) );
	pFac->SetNodeCreator( "multiply[VistaVector3D,float]", new VdfnMultiplicationNodeCreate<VistaVector3D,float> );
	pFac->SetNodeCreator( "multiply[VistaQuaternion,VistaQuaternion]", new VdfnMultiplicationNodeCreate<VistaQuaternion,VistaQuaternion> );
	pFac->SetNodeCreator( "add[VistaVector3D,VistaVector3D]", new VdfnAdditionNodeCreate<VistaVector3D,VistaVector3D> );
	pFac->SetNodeCreator( "add[double,double]", new VdfnAdditionNodeCreate<double,double> );
	pFac->SetNodeCreator( "nonlinearfunction", new VdfnNonlinearFunctionNodeCreate );
	pFac->SetNodeCreator( "timenavigation", new VfaTimeNavigationNodeCreate (m_pRenderNode->GetVisTiming()) );


	// register one "button"(i.e. commands), which is called "select"
	m_pApplicationContext = new VfaApplicationContextObject();
	m_pApplicationContext->RegisterCommand("select");

	// register the application context in Dfn
	pReg->SetObject( "application_context_object", m_pApplicationContext, NULL );

	m_pUpdateHandler = new VfaSystemByContextUpdater(m_pApplicationContext,
													m_pController);

	if(m_pLight)
	{
		//VistaGroupNode* pOldLightParent = m_pLight->GetParent();
		//int iLightIndex = pOldLightParent->DisconnectChild(m_pLight);

		//// insert transform node at light's old position
		//VistaTransformNode* pNewLightParent = pSystem->GetGraphicsManager()->GetSceneGraph()->NewTransformNode(NULL);
		//pOldLightParent->InsertChild(pNewLightParent, iLightIndex);
		////add light to new node
		//pNewLightParent->AddChild(m_pLight);

		//m_pLightTransform = new VistaLightTransformableAdapter(m_pLight);
		//m_pUpdateHandler->SetLightTransform( m_pLightTransform );
		//pReg->SetObject( "light", NULL, m_pLightTransform );
	}

	RegisterAction(' ',
		new VfaToggleAnimation(m_pRenderNode->GetVisTiming()),
		"Toggle animation running state");
	RegisterAction(VISTA_KEY_RIGHTARROW,
		new VfaAnimationSpeedChange(m_pRenderNode->GetVisTiming()),
		"Increase animation speed factor by 0.05 points");
	RegisterAction(VISTA_KEY_LEFTARROW,
		new VfaAnimationSpeedChange(m_pRenderNode->GetVisTiming(), -0.05f),
		"Decrease animation speed factor by 0.05 points");
	RegisterAction(VISTA_KEY_HOME,
		new VfaJumpToVisTimeAction(m_pRenderNode->GetVisTiming(), 0.0),
		"Jump to first frame");
	RegisterAction(VISTA_KEY_END,
		new VfaJumpToVisTimeAction(m_pRenderNode->GetVisTiming(), 1.0),
		"Jump to last frame");

	// For RenderNode updates, the first view port is being observed.
	// @todo Check if this is a valid default. On the other hand, no chance to handle it
	//		 any other way?!
	std::map<std::string, VistaViewport*> mapViewports = pSystem->GetDisplayManager()->GetViewports();
	m_pDisplayUpdate = new VfaDisplayEventObserver( mapViewports.begin()->second, m_pRenderNode );

	// Register the vis controller transformeable with the ObjectRegistry.
	pReg->SetObject( "viscontroller_transformable", NULL, m_pRenderNode->GetTransformable() );
	pReg->SetObject( "visrefpoint_transformable", NULL, m_pRenderNode->GetVisRefPointTransformable() );
	pReg->SetObject( "viscontroller", m_pController, NULL );
	
	// create axis info objects?
	if(bCreateAxisInfo)
	{
		m_pAxisInfo = new VflAxisInfo;
		if(!m_pAxisInfo || !m_pAxisInfo->Init())
			VISTA_THROW("VfaBaseApplication::VfaBaseApplication() -- COULD NOT CREATE AXIS-INFO", 0x0000003);
		m_pRenderNode->AddRenderable(m_pAxisInfo);
	}

	// create bounds info?
	if(bCreateBoundsInfo)
	{
		m_pBoundsInfo = new VflBoundsInfo;
		if(!m_pBoundsInfo || !m_pBoundsInfo->Init())
			VISTA_THROW("VfaBaseApplication::VfaBaseApplication() -- COULD NOT CREATE BOUNDS-INFO", 0x0000004);

		m_pRenderNode->AddRenderable(m_pBoundsInfo);
	}

	// Get viewerposition
	VistaVector3D   vPos(pDisp->GetDisplaySystemProperties()->GetViewerPosition());
	VistaQuaternion qOri(pDisp->GetDisplaySystemProperties()->GetViewerOrientation());

	// when we try to use billboard rendering, so it it necessary to
	// set the viewer position(and orientation) to the VisController
	// in order to allow it to update its internal transformation.
	// when doing this in a VE, call the following methods upon
	// every change of the users's head(e.g., HEAD MOVEMENTS)
	m_pController->TellGlobalViewPosition(vPos);
	m_pController->TellGlobalViewOrientation(qOri);
}


VfaBaseApplication::~VfaBaseApplication()
{
		delete m_pBoundsInfo;
		delete m_pAxisInfo;

		delete m_pDisplayUpdate;
		delete m_pUpdateHandler;

		// is this handled by DFN?
		delete m_pApplicationContext;
		delete m_pCamAdapter;

		for(std::vector<IVistaExplicitCallbackInterface*>::iterator it = m_vecKeyboardActions.begin();
			it != m_vecKeyboardActions.end(); ++it)
		{
			delete *it;
		}

		delete m_pRenderNode;
		delete m_pController;
		delete m_pFlowLibRoot;
		m_pFlowLibRoot = NULL;
}

VflVisController* VfaBaseApplication::GetVisController() const
{
	return m_pController;
}
VflRenderNode* VfaBaseApplication::GetRenderNode() const
{
	return m_pRenderNode;
}
VistaTransformNode* VfaBaseApplication::GetFlowLibRoot() const
{
	return m_pFlowLibRoot;
}
VfaApplicationContextObject* VfaBaseApplication::GetApplicationContext() const
{
	return m_pApplicationContext;
}

IVistaTransformable* VfaBaseApplication::GetLightTransform() const
{
	return m_pLightTransform;
}

IVistaTransformable* VfaBaseApplication::GetVisRefPointTransform() const
{
	return m_pVisRefPoint;
}

VistaVirtualPlatformAdapter* VfaBaseApplication::GetCameraAdapter() const
{
	return m_pCamAdapter;
}

VfaVistaSystemAbstraction* VfaBaseApplication::GetSystemAbstraction()const
{
	return m_pSystemAbstraction;
}

VfaSystemByContextUpdater* VfaBaseApplication::GetUpdateHandler() const
{
	return m_pUpdateHandler;
}

bool VfaBaseApplication::RegisterAction(int nKey,
	                IVistaExplicitCallbackInterface *pAction,
					const std::string &strExplain)
{
	VistaKeyboardSystemControl *pSysCtrl = m_pSystem->GetKeyboardSystemControl();
	if(pSysCtrl)
		return pSysCtrl->BindAction(nKey, pAction, strExplain, false );
	else
		return false;
}

IVistaExplicitCallbackInterface *VfaBaseApplication::UnRegisterAction(int nKey)
{
	VistaKeyboardSystemControl *pSysCtrl = m_pSystem->GetKeyboardSystemControl();
	if(!pSysCtrl)
		return NULL;

	IVistaExplicitCallbackInterface *pCb = pSysCtrl->GetActionForToken(nKey);
	if(pCb)
		pSysCtrl->UnbindAction(nKey);

	return pCb;
}

bool VfaBaseApplication::ResetTransformation()
{
	VistaVector3D v3DeltaTargetBounds = VistaVector3D(m_v3TargetBoundsMax - m_v3TargetBoundsMin);
	VistaVector3D v3BoundsMax, v3BoundsMin;

	m_pFlowLibRoot->GetBoundingBox(v3BoundsMin, v3BoundsMax);
	VistaVector3D v3DeltaBounds = VistaVector3D(v3BoundsMax - v3BoundsMin);	

	// calc smallest necessary scale factor
	float fHelper, fScale = std::numeric_limits<float>::max();

	for(int i=0; i<3; ++i)
	{
		fHelper = v3DeltaTargetBounds[i] / v3DeltaBounds[i];
		fScale = std::min(fHelper, fScale);
	}

	m_pFlowLibRoot->SetScale(fScale,fScale,fScale);

	// Reset rotation.
	m_pFlowLibRoot->SetRotation(VistaQuaternion(0.0f, 0.0f, 0.0f, 1.0f));

	// Calc translation to center
	VistaVector3D v3TargetBoundsCenter = VistaVector3D(0.5f * (m_v3TargetBoundsMin + m_v3TargetBoundsMax));
	VistaVector3D v3BoundsCenter= VistaVector3D(0.5f * (v3BoundsMin + v3BoundsMax));

	VistaTransformMatrix oTransM;
	m_pFlowLibRoot->GetTransform(oTransM);
	v3BoundsCenter = oTransM.Transform(v3BoundsCenter);
	v3TargetBoundsCenter -= v3BoundsCenter;

	m_pFlowLibRoot->Translate(v3TargetBoundsCenter);

	return true;
}
bool VfaBaseApplication::SetTargetBounds(const VistaVector3D &v3Min,
					 const VistaVector3D &v3Max)
{
	m_v3TargetBoundsMin = v3Min;
	m_v3TargetBoundsMax = v3Max;

	return true;
}
bool VfaBaseApplication::GetTargetBounds(VistaVector3D &v3Min,
					 VistaVector3D &v3Max) const
{
	v3Min = m_v3TargetBoundsMin;
	v3Max = m_v3TargetBoundsMax;

	return true;
}

VistaSystem* VfaBaseApplication::GetVistaSystem() const
{
	return m_pSystem;
}
