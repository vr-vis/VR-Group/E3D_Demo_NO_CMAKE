/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFAVIRTUALTOUR_H
#define _VFAVIRTUALTOUR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

// ViSTA stuff
#include <VistaAspects/VistaObserveable.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaPropertyList.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLib/Data/VflObserver.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>




//Vitual Tour
#include "VfaPath.h"
#include "Segments/VfaSegment.h"
#include "Segments/VfaLinearSegment.h"
#include "Segments/VfaPointSegment.h"
#include "Segments/VfaCircularSegment.h"
#include "Segments/VfaSegment.h"
#include "Segments/VfaCatmullRomSegment.h"


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VistaVirtualPlatform;
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Class to create a virtual tour, which contains a PositionPath and an 
 * OrientationPath, where the camera moves along the PositionPath and the view
 * is always directed to the related position on the OrientationPath
 *
 * To get an example and more information about the usage, look at the 
 * CameraNavigation Prototype at VflTutorials/VirtualTourTutorial
 * 
 * Additionally the VirtualTour implements an Observable interface, so a creator
 * could observe the state of the tour and react. for possible notification flags,
 * see the enum below
 *
 * There are 4 Modes (flags) that might be interessting, to look at: CAVE-Mode,
 * Sync-Mode, Gaps-Filling-Mode and No-Rotation-Mode more information about these can 
 * be found below at there getter/setter methods.
 */
class VISTAFLOWLIBAUXAPI VfaVirtualTour :	public IVistaObserveable, 
											public IVflRenderable
{

public:

	enum
	{
		MSG_TOUR_STARTED = IVistaObserveable::MSG_LAST,
		MSG_TOUR_ENDED,
		MSG_TOUR_NEWSTEP,
		MSG_TOUR_END_PREPARATION,
		MSG_TOUR_INIT_FAILURE,
		MSG_TOUR_PAUSED,
		MSG_TOUR_PAUSED_TILL_CONTINUE,
		MSG_LAST
	};

	//creates a new empty Tour
	VfaVirtualTour(VflRenderNode *pRenderNode, 
					VistaVirtualPlatform *pPlatform);

	//creates a new Tour as described in pPropList
	VfaVirtualTour(VflRenderNode *pRenderNode, 
					VistaVirtualPlatform *pPlatform,
					const VistaPropertyList* pPropList);
	virtual ~VfaVirtualTour();

	/**
	* This function prepares and checks the tour parameters.
	* It is called once the start function is called, But it can be also called explicitly.
	*/
	bool Init(float fStartParamtere=0.0);



	/**
	* Start:	Start the tour from the intial position, if the tour was already started, another call for this function will be ignored.
	* Restart:	Same as Start Function, but it restarts the tour from the initial position no matter if it was already started or not.
	* Pause:	If the tour was already started this function will pause it and keep the same start. (Calling start or restart after this 
	*			will inialize from the begining) the proper call should be continue
	* Continue: If the tour was started and paused this call can continue the tour from the paused state, or simply starts the tour 
	*			if it hasn't been started yet
	*/
	virtual void Start();
	virtual void Pause();
	virtual void Continue(float fDirection=1.0f);//if you want the Tour to run backward just go for fDirection=-1.0
	virtual void Restart();

	//this methods takes the tour directly to a station on the position path (number between 0 and numberof stations-1)
	//with a bringingBackTour and continues from there on
	//the stations can be reached if you call GetPositionPath()->GetAllStations()
	//returns whether the station number was valid
	bool GoToStation(int iNumber);

	void SetPositionPath(VfaPath* pPositionPath);
	VfaPath* GetPositionPath() const;

	void SetOrientationPath(VfaPath* pOrientationPath);
	VfaPath* GetOrientationPath() const;

	void SetVelocity(float fVelocity);
	float GetVelocity();

	/**
	* This function gets called regularly automtically, because it implements part of the Renderable interface
	* It sets computes and sets the new position and orientation.
	*/
	virtual void Update();

	/**
	 * \see{VflRenderable.h} for explanations
	 */
	virtual unsigned int GetRegistrationMode() const;

	/**
	 * Adds a new Segment at the end of the position path
	 */
	void AddPositionSegment(VfaSegment* pSegment); 

	/**
	* Adds a new Segment at the end of the orientation path
	*/
	void AddOrientationSegment(VfaSegment* pSegment); 

	/**
	* Get the parameter that shows the progress of the movement
	* parameter in [0;1]
	* 0 --> beginn; 1 --> end
	*/
	float GetParameter() const; 

	/**
	 * Informs about whether a camera interpolation is currently running.
	 */
	bool GetIsInFlight() const; 

	/**
	 *	Get / Set synchronous Mode,
	 * That means if you have as many stations in the orientation path
	 * as in the position path, all segments will be computed synchronous.
	 * That means if you are in the 3rd segment of the position path you will
	 * use the third segment of the orientation path, no matter if they differ in length.
	 * But you need the same number of segments in both paths
	 */
	void SetSynchronousMode(bool bActive);
	bool GetSynchronousMode() const;

	/**
	 * Get / Set Gap filling
	 * So all gaps within your path will be filled by linear interpolation between the end station
	 * and the start station of the following segment if they aren't the same.
	 * This should normally always be active because than jumps are avoided in every case!
	 */
	void SetGapFilling(bool bActive);
	bool GetGapFilling() const;

	/**
	 * Get / Set CAVE Mode
	 * that means there will just be rotation about the y-axis and the floor
	 * stays oriented as it is supposed to be, should be deactivated for desktop applications
	 * and rollercoaster-like applications for CAVE
	 */
	void SetCAVEMode(bool bActive);
	bool GetCAVEMode() const;

	/**
	 * Get / Set LookAhead Mode
	 * that means that the looking At point is always set to the position where the position 
	 * path will be (by default) one second later, this time however can be changed via the
	 * Getter/Setter for the LookAheadTime, here every value fTime>0.0 is valid
	 *
	 * For the LookAhead Mode to work, NoRotation and Synchronous Mode have to be deactivated!
	 */
	void SetLookAheadMode(bool bActive);
	bool GetLookAheadMode() const;
	void SetLookAheadTime(float fTime);
	float GetLookAheadTime() const;


	/*
	* The no Rotation Mode is even stronger than the CAVE Mode here just the position path
	* is interpolated and the platform isn't rotated at all! Therefor the orienation path could
	* be empty or not, it will be discarded anyway. So the Sync-Mode and CAVE-Mode flags will
	* be ignored.
	*/
	void SetNoRotationMode(bool bNoRotation);
	bool GetNoRotationMode();

	/**
	 * Get / Set The default Looking direction
	 */
	void SetDefaultLookingDirection(VistaVector3D v3LookingVector);
	VistaVector3D GetDefaultLookingDirection();

	/**
	 * Get / Set The default Up direction
	 * This is normally (0,1,0) so there will be no rotations along the z-axis.
	 */
	void SetDefaultUpDirection(VistaVector3D v3UpVector);
	VistaVector3D GetDefaultUpDirection();

	/** 
	* IVflRenderable Interface 
	* not to mistaken for CreatePropertyList!!!
	*/
	virtual VflRenderableProperties* CreateProperties() const;

	/**
	* Set the position and the orientation of the user
	*/
	void SetPosition(VistaVector3D v3Pos, VistaVector3D v3Direction);

	/** Sets the user offset from the position path */
	void SetUserOffset(VistaVector3D v3UserOffset);

	/** Gets the user offset from the position path */
	VistaVector3D GetUserOffset() const;

	void SetPausedFor(float seconds);

	/**VflObserver Stuff, for bringing you back on the trail
	 *  
	 * This message is sent to the observer when the state of pObserveable changed. Note that
	 * this is the only information transmitted, the observer can query the new state of pObserveable
	 * using the public interface of pObserveable. Note that an observer usually knows the real type
	 * or at least the least-known-type of the current IVistaObserveable, so a downcast should be used
	 * to query the current state of pObserveable. An Observer can have more than one observeable that
	 * is observed.
	 * @param pObserveable the observeable that changed its state.
	 */
	void ObserverUpdate( IVistaObserveable* pObserveable, int nMsg, int nTicket );

	//this method should be used with great care because it resets the parameter which represents the position
	//in the path, normally fParam is internally updated in the Update-method!
	void SetParameter(float fNewParam);

	//this method changes the Pause position of the Tour, this is needed if you want to restart a Tour from another 
	//parameter than the beginning, if you for example set the Pause Point with this method to 0.5 and call continue
	//the virtual platform is flewn to the asociated position and orientation and continues from there on
	virtual void AdjustPause(float fParam);

	bool IsPaused();

	float GetDirectionFactor();

	//deletes all segments from the paths and resets the whole tour.
	void Clear();
	
	VflRenderNode* GetTourRenderNode();
	VistaVirtualPlatform* GetTourVirtualPlatform();

	//creates aVistaPropertyList containing all information to describe the Tour, which then can be stored or transmitted
	VistaPropertyList* CreatePropertyList();

	//this method updates an existing tour such that it will be the tour described in pPropList afterwards. For example
	//the paths get shirinked or extended and the single segments get updated with the new parameters.
	//returns true if the format of the PropertyList is alright, otherwise false
	bool UpdateWithPropertyList(VistaPropertyList* pPropList);

	
protected:
	virtual void CreateBringingBackTour(VistaVector3D v3EndPosition, VistaVector3D v3EndOrientation);

	void UpdateParameter(double dTimeStep);

	//we need this to see wether two Vectors are equal accounting mashine inaccuracy
	bool Vector3DEquals(VistaVector3D v3One, VistaVector3D v3Two);

	//this helper creates a Segemtn as described in the PropList
	VfaSegment* CreateSegment(VistaPropertyList PropList);


	/**
	 *	Render Node for timing informations
	 */
	VflRenderNode			*m_pRenderNode;

	/**
	* pointer to store the props of this instance of the RenderNode
	*/
	VflRenderableProperties *m_pProperties;
	/**
	 * Platform which will be manipulated
	 */
	VistaVirtualPlatform	*m_pPlatform;
	
	VfaPath*				m_pPositionPath;
	VfaPath*				m_pOrientationPath;

	float					m_fParm;
	//float					m_fdParm;

	//velocity for the position path
	float					m_fVelocity;

	bool					m_bSynchronousMode;
	bool					m_bGapFilling;
	bool					m_bCAVEMode;
	bool					m_bNoRotationMode;
	bool					m_bLookAheadMode;

	float					m_fLookAheadTime;

	double					m_dLastTimeStep;
	VistaVector3D			m_v3UserOffset; //Not used yet

	bool					m_bInitialized;
	bool					m_bIsInFlight;
	bool					m_bIsPaused;
	float					m_fRestPauseTime;

	VfaVirtualTour*			m_pBringingBackTour;
	bool					m_bGettingBroughtBack;

	VistaVector3D			m_v3PausedPosition;
	VistaVector3D			m_v3PausedOrientation;

	VistaVector3D			m_v3DefaultLookingDirection;
	VistaVector3D			m_v3DefaultUpDirection;

	//should be 1 or -1 to let the tour run forward or backward, but speed factors could be faked in her as well
	float					m_fDirectionFactor;

};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _VFAVIRTUALTOUR_H */
