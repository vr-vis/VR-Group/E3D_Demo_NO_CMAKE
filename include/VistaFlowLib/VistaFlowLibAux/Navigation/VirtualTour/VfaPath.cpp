/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
// ViSTA stuff
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include "Segments/VfaSegment.h"
#include "Segments/VfaLinearSegment.h"

// FlowLibAux stuff
#include "VfaPath.h"

#include <cstdio>

using namespace std;

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
VfaPath::VfaPath()
{
	m_vecSegments.clear();
	m_iLastSegmentNumber=0;
	m_iCurrentSegment=0;
	m_fCurrentSegmentStartParm=0.0;
}

VfaPath::~VfaPath()
{
	while(m_vecSegments.size()>0) {
		delete m_vecSegments.back();
		m_vecSegments.pop_back();
	}
	m_vecSegments.clear();
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
void VfaPath::AddSegment(VfaSegment* pSegment)
{
	if (pSegment)
		m_vecSegments.push_back(pSegment);
}

float VfaPath::GetPathLength(){

	float fLength = 0.0f;
	
	for (int i = 0; i < GetNumberOfSegments(); i++)
		fLength += (m_vecSegments.at(i))->GetSegmentLength();

	return fLength;

}

int VfaPath::GetNumberOfSegments(){

	return static_cast<int>(m_vecSegments.size());

}

std::vector<VfaSegment::VfaVTStation*> VfaPath::GetAllStations() {
	std::vector<VfaSegment::VfaVTStation*> vecStations;
	vecStations.clear();

	if(GetPathLength() == 0.0f){
		return vecStations;
	}
	
	VfaSegment::VfaVTStation* pStation;
	float fLengthSoFar=0.0f,
		fPathLength=GetPathLength();

	for(int i=0; i<m_vecSegments.size(); i++) {

		std::vector<VfaSegment::VfaVTStation*> vecSegmentStations = m_vecSegments.at(i)->GetStations();
		for(int j=0;j<vecSegmentStations.size(); j++) {
			if(i!=0 && vecSegmentStations[j]->v3Pos==pStation->v3Pos) {
				continue;
			}
			pStation = vecSegmentStations[j];
			pStation->iSegmentNr = i;
			pStation->fParameter = (fLengthSoFar + pStation->fParameter * m_vecSegments.at(i)->GetSegmentLength()) /fPathLength;
			vecStations.push_back(pStation);
		}
		fLengthSoFar+=m_vecSegments.at(i)->GetSegmentLength();
	}

	return vecStations;
}

bool VfaPath::CheckAndPrepare(bool bForcePrepare){

	m_iLastSegmentNumber=0;
	m_iCurrentSegment=0;
	m_fCurrentSegmentStartParm=0.0;

	if (GetNumberOfSegments() > 0){
		
		bool bCheck = true;
		for (int i = 0; i < GetNumberOfSegments() && bCheck; i++){
			if (bForcePrepare)
				m_vecSegments.at(i)->ResetPreparedFlag();
			bCheck = (m_vecSegments.at(i))->Preparation();
		}
		if(bCheck && GetPathLength()<0.005f && m_vecSegments[0]->GetTypeID()!=VfaSegment::VT_SEG_POINT){
			vstr::warnp()<<"[VfaPath::CheckAndPrepare] whole path is shorter than 0.005, not valid."<<std::endl;
			bCheck = false;
		}
	
		return bCheck;

	}

	return false;

}

VistaVector3D VfaPath::Interpolate(float fParm) {
	float fNotUsed;
	return Interpolate(fParm, fNotUsed);
}

VistaVector3D VfaPath::Interpolate(float fParm, float &fPauseTime){
	
	AdjustParm(fParm);

	//Avoid unexpected calls and prevent crashes
	if (GetNumberOfSegments() == 0)
		return VistaVector3D(0,0,0);

	float fLength = GetPathLength();
	float fCurrentPoint = fParm * fLength;
	float fCheckedLength = 0;
	
	VfaSegment* pCurrSegment;

	int iIndex = 0;
	for (; iIndex < GetNumberOfSegments(); iIndex++){
		pCurrSegment = m_vecSegments.at(iIndex);
		fCheckedLength += pCurrSegment->GetSegmentLength();
		if (fCurrentPoint <= fCheckedLength)
			break;
	}

	
	//We have to take care about the Pause at the end of the segment before this one
	if(iIndex!=m_iLastSegmentNumber) {
		if(iIndex==m_iLastSegmentNumber+1 || iIndex == m_iLastSegmentNumber-1) {
			if(m_iLastSegmentNumber<m_vecSegments.size() && (m_vecSegments.at(m_iLastSegmentNumber)->GetPauseAtEndTime()!=0.0 || m_vecSegments.at(m_iLastSegmentNumber)->GetPauseAtEnd())){
				fPauseTime=m_vecSegments.at(m_iLastSegmentNumber)->GetPauseAtEndTime();
				if(m_vecSegments.at(m_iLastSegmentNumber)->GetPauseAtEnd())
					fPauseTime=-1.0f;
			}
		}
		m_iLastSegmentNumber=iIndex;
		float fLengthSoFar=0.0f;
		for(int i= 0; i< iIndex; i++) {
			fLengthSoFar+=m_vecSegments.at(i)->GetSegmentLength();
		}
		m_fCurrentSegmentStartParm = fLengthSoFar/fLength;
	}

	if(iIndex<m_vecSegments.size())
		m_iCurrentSegment=iIndex;
	

	//[------][------][--*-----][----]
	float fStationPauseTime = 0.0;
	float fSegmentLocalParm = 1 - ((fCheckedLength - fCurrentPoint) /  pCurrSegment->GetSegmentLength());
	VistaVector3D v3Pos = pCurrSegment->InterpolateLocaly(fSegmentLocalParm, fStationPauseTime);

	//maybe the segment was catmullrom segment and there was a pause in one of the stations of this segment
	if (fPauseTime == 0.0f)
		fPauseTime = fStationPauseTime;


	return v3Pos;

}
VistaVector3D VfaPath::InterpolateSynchronous(int iSegNr, float fLocalParm, float &fPauseTime){


	//Avoid unexpected calles and prevent crashes
	if (GetNumberOfSegments() == 0)
		return VistaVector3D(0,0,0);
	
	VfaSegment* pCurrSegment=GetSegment(iSegNr);

	//We have to take care about the Pause at the end of the last segment
	if(iSegNr!=m_iLastSegmentNumber) {

		

		if(iSegNr==m_iLastSegmentNumber+1) {
			if(m_iLastSegmentNumber<m_vecSegments.size() && (m_vecSegments.at(m_iLastSegmentNumber)->GetPauseAtEndTime()!=0.0 || m_vecSegments.at(m_iLastSegmentNumber)->GetPauseAtEnd())){
				fPauseTime=m_vecSegments.at(m_iLastSegmentNumber)->GetPauseAtEndTime();
				if(m_vecSegments.at(m_iLastSegmentNumber)->GetPauseAtEnd())
					fPauseTime=-1.0f;
			}
			m_iLastSegmentNumber=iSegNr;
		}
		else{ //this has to be done if we jumped around in the tour!
			m_iLastSegmentNumber=iSegNr;
		}
	}

	if(iSegNr<m_vecSegments.size())
		m_iCurrentSegment=iSegNr;
	
	VistaVector3D v3Pos = pCurrSegment->InterpolateLocaly(fLocalParm, fPauseTime);

	return v3Pos;


}

VfaSegment* VfaPath::GetSegment(int iIndex){

	if(static_cast<int>(m_vecSegments.size()) <= iIndex)
		return NULL;

	return m_vecSegments.at(iIndex);

}


bool VfaPath::RemoveSegment(int iIndex){

	if(static_cast<int>(m_vecSegments.size()) <= iIndex)
		return false;

	m_vecSegments.erase(m_vecSegments.begin()+iIndex);

	return true;

}
void VfaPath::InsertSegment(VfaSegment* pSegment, int index){
	if(!pSegment)
		return;
	if(index > static_cast<int>(m_vecSegments.size())){ 
		//set index to last position
		index = static_cast<int>(m_vecSegments.size());
	}
	std::vector<VfaSegment*>::iterator it = m_vecSegments.begin()+index;

	m_vecSegments.insert(it ,pSegment);
}

void VfaPath::FillTheGaps(){
	
	if (m_vecSegments.size() < 2)
		return;

	//we have to prepare here, in case a circular segment with forceCircle is used,
	//because then the stations could get altered while preparing.
	CheckAndPrepare(true);
	
	std::vector<VfaSegment*>	vecNewSegments;
	VfaSegment* pCurrSegment;
	VfaSegment* pNextSegment;

	for (int i = 0; i < GetNumberOfSegments() - 1; i++){
		
		pCurrSegment = m_vecSegments.at(i);
		pNextSegment = m_vecSegments.at(i+1);

		vecNewSegments.push_back(pCurrSegment);

		VistaVector3D v3Station1 = pCurrSegment->GetLastRealStation();
		VistaVector3D v3Station2 = pNextSegment->GetFirstRealStation();
		
		if (v3Station1 != v3Station2){

			// Add a connecting segment
			VfaLinearSegment* pNewConnectingSegment = new VfaLinearSegment(v3Station1, v3Station2);
			vecNewSegments.push_back(pNewConnectingSegment);

			vstr::outi()<<"[VfaPath] Filled a gap between segment "<<i<<" and segment "<<i+1<<", because"
						<<"("<<v3Station1[0]<<", "<<v3Station1[1]<<", "<<v3Station1[2]<<") != "<<
						  "("<<v3Station2[0]<<", "<<v3Station2[1]<<", "<<v3Station2[2]<<")"<<endl;
		}

		
	}
	//and add the last segments because all other segments will be the current segment in the next itteration
	vecNewSegments.push_back(pNextSegment);

	m_vecSegments = vecNewSegments;
}

void VfaPath::AdjustParm (float &fParm){
	if (fParm < 0.0)
		fParm = 0.0f;

	if (fParm > 1.0)
		fParm = 1.0f;
}

int VfaPath::GetCurrentSegmentNumber() {
	return m_iCurrentSegment;
}

float VfaPath::GetParmCurrentSegStartedWith() {
	return m_fCurrentSegmentStartParm;
}

void VfaPath::ClearPath() {
	m_vecSegments.clear();
	m_iLastSegmentNumber=0;
	m_iCurrentSegment=0;
	m_fCurrentSegmentStartParm=0.0;
}

// ========================================================================== //
// === End of File
// ========================================================================== //
