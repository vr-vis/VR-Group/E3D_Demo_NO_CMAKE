/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
#include "VfaVirtualTour.h"

// ViSTA stuff
#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>
#include <VistaKernel/InteractionManager/VistaUserPlatform.h>
#include <VistaKernel/GraphicsManager/VistaTransformNode.h>
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>
#include <VistaKernel/DisplayManager/VistaDisplayManager.h>
#include <VistaKernel/DisplayManager/VistaDisplaySystem.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>

#include "Views/VfaVirtualTourViewer.h"
#include "Configurator/VfaVirtualTourAngularVelocityConfig.h"


#include <cstdio>

using namespace std;

const float DEFAULT_VELOCITY = 5;
const float DEFAULT_ANGULAR_VELOCITY = 0.5f;

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //

VfaVirtualTour::VfaVirtualTour(VflRenderNode *pRenderNode, VistaVirtualPlatform *pPlatform)
	: m_pRenderNode ( pRenderNode )
	, m_pPlatform ( pPlatform )
	, m_pPositionPath ( NULL )
	, m_pOrientationPath ( NULL )
	, m_fParm ( 0.0 )
	, m_fVelocity ( DEFAULT_VELOCITY )
	, m_bSynchronousMode ( false )
	, m_bGapFilling ( true )
	, m_bCAVEMode ( true )
	, m_dLastTimeStep ( 0.0 )
	, m_bInitialized ( false )
	, m_bIsInFlight ( false )
	, m_bIsPaused(false)
	, m_bGettingBroughtBack(false)
	, m_pBringingBackTour(NULL)
	, m_v3DefaultLookingDirection(VistaVector3D(0.0,0.0,-1.0))
	, m_v3DefaultUpDirection(VistaVector3D(0.0,1.0,0.0))
	, m_bNoRotationMode(false)
	, m_fDirectionFactor(1.0f)
	,m_fLookAheadTime(3.0f)
	,m_bLookAheadMode(false)
{
	// Get some extrenal parms such as RenderNode and Platform
	// Initialize a default Paths for postion and orientation
	m_pProperties = CreateProperties();

	m_pPositionPath = new VfaPath();		
	m_pOrientationPath = new VfaPath();		
	m_v3PausedPosition= VistaVector3D(-1.0,-1.0,-1.0);

	IVflRenderable::Init();

	if (m_pRenderNode)
		m_pRenderNode->AddRenderable(this);

}

VfaVirtualTour::VfaVirtualTour(VflRenderNode *pRenderNode, VistaVirtualPlatform *pPlatform, const VistaPropertyList* pPropList)
	: m_pRenderNode ( pRenderNode )
	, m_pPlatform ( pPlatform )
	, m_fParm ( 0.0 )
	, m_dLastTimeStep ( 0.0 )
	, m_bInitialized ( false )
	, m_bIsInFlight ( false )
	, m_bIsPaused(false)
	, m_bGettingBroughtBack(false)
	, m_pBringingBackTour(NULL)
	, m_v3DefaultLookingDirection(VistaVector3D(0.0,0.0,-1.0))
	, m_v3DefaultUpDirection(VistaVector3D(0.0,1.0,0.0))
{
	// Get some extrenal parms such as RenderNode and Platform
	// Initialize a default Paths for postion and orientation
	m_pProperties = CreateProperties();

		
	m_v3PausedPosition= VistaVector3D(-1.0,-1.0,-1.0);
	IVflRenderable::Init();
	if (m_pRenderNode)
		m_pRenderNode->AddRenderable(this);

	if(!pPropList){
		vstr::errp()<<"[VfaVirtualTour] Created with NULL pPropList!, default values chosen."<<std::endl;
		pPropList = new VistaPropertyList();
	}


	std::vector<std::string> vecPositionPath;
	pPropList->GetValueInSubList("PositionSegments", "MAIN", vecPositionPath);

	m_pPositionPath = new VfaPath();

	if(vecPositionPath.size()<=0)
		vstr::warnp()<<"[VfaVirtualTour] Create new VirtualTour with empty PropertyList(not Position segments), empty Tour created!"<<std::endl;

	for(int i=0; i<vecPositionPath.size(); i++) {
		VistaPropertyList segList;
		pPropList->GetValue(vecPositionPath[i], segList);
		VfaSegment* pSeg = CreateSegment(segList);
		if( pSeg!=NULL)
			m_pPositionPath->AddSegment(pSeg);
		else
			vstr::errp()<<"Can't add default segments! Broken PropertyList! Segment "<<i<<"in the position path"<<std::endl;			
	}



	std::vector<std::string> vecOrientationPath;
	pPropList->GetValueInSubList("OrientationSegments", "MAIN", vecOrientationPath);
	m_pOrientationPath = new VfaPath();
	for(int i=0; i<vecOrientationPath.size(); i++) {
		VistaPropertyList segList;
		pPropList->GetValue(vecOrientationPath[i], segList);
		VfaSegment* pSeg = CreateSegment(segList);
		if( pSeg!=NULL)
			m_pOrientationPath->AddSegment(pSeg);
		else
			vstr::errp()<<"Can't add default segments! Broken PropertyList! Segment "<<i<<"in the orientation path"<<std::endl;
	}

	m_fVelocity = pPropList->GetValueInSubListOrDefault("Velocity", "MAIN", DEFAULT_VELOCITY);
	m_bCAVEMode  = pPropList->GetValueInSubListOrDefault("CaveMode", "MAIN", true);
	m_bGapFilling  = pPropList->GetValueInSubListOrDefault("GapFilling", "MAIN", true);
	m_bNoRotationMode = pPropList->GetValueInSubListOrDefault("NoRotationMode", "MAIN", true);
	m_bSynchronousMode = pPropList->GetValueInSubListOrDefault("SynchronousMode", "MAIN", true);
	m_fDirectionFactor = pPropList->GetValueInSubListOrDefault("DirectionFactor", "MAIN", 1.0f);
	m_fLookAheadTime = pPropList->GetValueInSubListOrDefault("LookAheadTime", "MAIN", 1.0f);
	m_bLookAheadMode = pPropList->GetValueInSubListOrDefault("LookAheadMode", "MAIN", false);

}



VfaVirtualTour::~VfaVirtualTour()
{
	m_pRenderNode->RemoveRenderable(this);

	if (m_pPositionPath)
		delete m_pPositionPath;

	if (m_pOrientationPath)
		delete m_pOrientationPath;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/* ==========================================================================*/
/* NAME: AnyFunction()					 									 */
/* ==========================================================================*/

bool VfaVirtualTour::Init(float fStartParameter)//fStartParameter=0.0
{
	if (!m_bInitialized){

		//Check the paths we have and check the number of segments
		if (!m_pPositionPath){
			m_pPositionPath = new VfaPath();		
		}

		if (!m_pOrientationPath){
			m_pOrientationPath = new VfaPath();		
		}

		//In case of filling gaps do it
		if (m_bGapFilling){

			vstr::debugi()<<"[VfaVirtualTour] Fill Position Path Gaps"<<endl;
			m_pPositionPath->FillTheGaps();
			vstr::debugi()<<"[VfaVirtualTour] Fill Orientation Path Gaps"<<endl;
			m_pOrientationPath->FillTheGaps();
			vstr::debugi()<<"[VfaVirtualTour] Gaps filled, if there were any (if there is nothing above, there weren't any)"<<endl;

		}
	
		//Check the sync mode if active and if possible
		if (m_bSynchronousMode)
		{
			if (m_pPositionPath->GetNumberOfSegments() != m_pOrientationPath->GetNumberOfSegments()){
				m_bSynchronousMode = false;
				vstr::warnp() << "[VfaVirtualTour] The synchronious mode is not possible, the number of segments in the position path ("<<m_pPositionPath->GetNumberOfSegments()<<
					" segments) and orientation path ("<<m_pOrientationPath->GetNumberOfSegments()<<" segments) are not equal" << endl;
			}
		}
		
		m_fParm = fStartParameter;
		m_dLastTimeStep = 0.0;
		m_bIsPaused = false;

		m_bInitialized = true;
		
		return true;

	}


	return true;

}

/**
* Start: Start the tour from the initial position, if the tour was already started, another call for this function will be ignored.
* Restart: Same as Start Function, but it restarts the tour from the initial position no matter if it was already started or not.
* Pause: If the tour was already started this function will pause it and keep the same start. (Calling start or restart after this will inialize from the begining)
*		the proper call should be continue
* Continue: If the tour was started and pause this call can continue the tour from the paused state
*/
void VfaVirtualTour::Start()
{

	if (m_bIsInFlight || m_bGettingBroughtBack)
		return;

	if (!m_pRenderNode)
		return;

	if (!m_bInitialized)
		Init();

	if(m_pPositionPath->GetNumberOfSegments() ==0 || (m_pOrientationPath->GetNumberOfSegments()==0 && !m_bNoRotationMode && !m_bLookAheadMode)){
		vstr::warnp()<<"[VfaVirtualTour] Not enough segments in the paths! Can't start."<<std::endl;
		return;
	}

	bool bCheckAndPrepare = true;
	//Call Check and Prepare for each path to check the segments inside and prepare for interpolation
	if (!m_pPositionPath->CheckAndPrepare(true) || (!m_pOrientationPath->CheckAndPrepare(true) && !m_bNoRotationMode && !m_bLookAheadMode)){

		bCheckAndPrepare = false;

		vstr::errp() << "[VfaVirtualTour] Failure in preparing and checking the paths." << endl;

		//vstr::outi()<<"PositionPath check: ";
		//if(m_pPositionPath->CheckAndPrepare(true))
		//	vstr::outi()<<"sucess"<<std::endl;
		//else
		//	vstr::erri()<<"failed"<<std::endl;

		//vstr::outi()<<"OrientationPath check: ";
		//if(m_pOrientationPath->CheckAndPrepare(true))
		//	vstr::outi()<<"sucess"<<std::endl;
		//else
		//	vstr::erri()<<"failed"<<std::endl;

		
		Notify(MSG_TOUR_INIT_FAILURE);

		vstr::errp() << "[VfaVirtualTour] This Tour will not start." << endl;

		return;

	}


	VistaVector3D v3Ori = m_v3DefaultLookingDirection; //(0.0, 0.0, -1.0);
	v3Ori = m_pPlatform->GetRotation().Rotate(v3Ori);
	v3Ori.Normalize();

	VistaVector3D v3StartOri, v3StartPos;
	
	if(m_fDirectionFactor>0.0f) {
		v3StartPos = m_pPositionPath->GetSegment(0)->GetFirstRealStation();
		v3StartPos += m_v3UserOffset;
		if(!m_bNoRotationMode) {
			if(m_bLookAheadMode){
				float fOriParam = (m_fVelocity*m_fLookAheadTime)/m_pPositionPath->GetPathLength();
				if(fOriParam>1.0f)
					fOriParam = 1.0f;
				v3StartOri = m_pPositionPath->Interpolate(fOriParam) - v3StartPos;
				v3StartOri += m_v3UserOffset;
			}
			else
				v3StartOri = m_pOrientationPath->GetSegment(0)->GetFirstRealStation() - v3StartPos;
		}
		else 
			v3StartOri = VistaVector3D(0.0, 0.0, -1.0);
	}
	else {
		v3StartPos = m_pPositionPath->Interpolate(1.0f);
		v3StartPos += m_v3UserOffset;
		if(!m_bNoRotationMode)
			if(m_bLookAheadMode){
				float fOriParam = (m_fVelocity*m_fLookAheadTime)/m_pPositionPath->GetPathLength();
				if(fOriParam>1.0f)
					fOriParam = 1.0f;
				v3StartOri = m_pPositionPath->Interpolate(1.0f - fOriParam) - v3StartPos;
				v3StartOri += m_v3UserOffset;
			}
			else
				v3StartOri = m_pOrientationPath->Interpolate(1.0f) - v3StartPos;
		else 
			v3StartOri = VistaVector3D(0.0, 0.0, -1.0);
	}
	
	v3StartOri.Normalize();

	//if we are not at the startposition, we have to get there:
	if((m_pPlatform->GetTranslation() - v3StartPos).GetLength()>0.001f || (!m_bNoRotationMode && !Vector3DEquals(v3Ori, v3StartOri)))
	{
		if((m_pPlatform->GetTranslation() - v3StartPos).GetLength()>0.001f)
		{
			vstr::debugi()<<"[VfaVirtualTour] Positions do not match! "
						  <<m_pPlatform->GetTranslation()<<" != "<<v3StartPos<<std::endl;
		}
		if(!Vector3DEquals(v3Ori, v3StartOri))
		{
			vstr::debugi()<<"[VfaVirtualTour] Orientations do not match! "
						  <<v3Ori<<" != "<<v3StartOri<<std::endl;
		}
		vstr::outi()<<"[VfaVirtualTour] Creating new bringing back Tour for Start()"<<std::endl;
		CreateBringingBackTour(v3StartPos, v3StartOri);
		return;
	}
	
	//Check whether our segments were prepared correctly
	if (bCheckAndPrepare){

		Notify(MSG_TOUR_END_PREPARATION);

		//Init parm and get the proper velocity
		if(m_fDirectionFactor>0.0f)
			m_fParm = 0.0f;
		else
			m_fParm=1.0f;
		

		vstr::outi() << "[VfaVirtualTour] Starting Virtual Tour ... " << endl;

		//Start actual movement
		Notify(MSG_TOUR_STARTED);

		m_dLastTimeStep = m_pRenderNode->GetVisTiming()->GetCurrentClock();
		m_bIsInFlight = true;

	}

}

void VfaVirtualTour::Update()
{

	if (!m_bIsInFlight)
		return;

	double dNewTS = GetRenderNode()->GetVisTiming()->GetCurrentClock();
	double deltaT = dNewTS - m_dLastTimeStep;
	m_dLastTimeStep = m_pRenderNode->GetVisTiming()->GetCurrentClock();

	//vstr::outi()<<"Platform-Pos: "<<m_pPlatform->GetTranslation()<<std::endl;

	//So the Tour is running, but we reached a "Pause at end" point of a segment
	if(m_bIsPaused){
		m_fRestPauseTime-=(float)deltaT;
		if(m_fRestPauseTime<=0.0f)
			m_bIsPaused=false;
		Notify(MSG_TOUR_PAUSED);
		return;
	}

	UpdateParameter(deltaT);

	//because our tour can run in both directions we check for >1.0 and <0.0
	if (m_fParm > 1.0f){

		m_fParm=1.0f;
		m_bIsInFlight = false;
		m_dLastTimeStep = 0.0;
		m_bInitialized=false;	
	}

	if(m_fParm<0.0f) {
		m_fParm = 0.0f;
		m_bIsInFlight = false;
		m_dLastTimeStep = 0.0;
		m_bInitialized=false;
	}

	float fPauseTimePos=0.0, fPauseTimeOri=0.0;
	VistaVector3D v3lookingAt;
	VistaVector3D v3Position;

	if(m_bNoRotationMode) {
		v3Position = m_pPositionPath->Interpolate(m_fParm, fPauseTimePos);
		v3lookingAt = VistaVector3D(0.0,0.0,0.0); //this should not be used! just to avoid crashes
	}
	else if(m_bSynchronousMode)
	{
		v3Position = m_pPositionPath->Interpolate(m_fParm, fPauseTimePos);
		int iSegNr=m_pPositionPath->GetCurrentSegmentNumber();
		float fLocalParm = (m_fParm - m_pPositionPath->GetParmCurrentSegStartedWith()) / m_pPositionPath->GetSegment(iSegNr)->GetSegmentLength() * m_pPositionPath->GetPathLength();
		//this is computed incorrectly if m_fParm==1.0f, therefor
		if(m_fParm==1.0) {
			fLocalParm=1.0;
		}
		v3lookingAt = m_pOrientationPath->InterpolateSynchronous(iSegNr, fLocalParm, fPauseTimeOri);
	}
	else if(m_bLookAheadMode) {
		v3Position = m_pPositionPath->Interpolate(m_fParm, fPauseTimePos);
		float fOriParam = (m_fVelocity*m_fLookAheadTime)/m_pPositionPath->GetPathLength();
		if(m_fDirectionFactor>0.0f){
			fOriParam = m_fParm + fOriParam;
			if(fOriParam > 1.0f){
				//we have to move the looking at position a little bit in direction of the before movement, so looking at and position don't end up in the same place!
				v3lookingAt = m_pPositionPath->Interpolate(1.0f) + (m_pPositionPath->Interpolate(1.0f)-m_pPositionPath->Interpolate(1.0f-(m_fVelocity*0.5f)/m_pPositionPath->GetPathLength()));
			}
			else
				v3lookingAt = m_pPositionPath->Interpolate(fOriParam);
		}
		else if(m_fDirectionFactor<0.0f){
			fOriParam = m_fParm - fOriParam;
			if(fOriParam <0.0f){
				//we have to move the looking at position a little bit in direction of the before movement, so looking at and position don't end up in the same place!
				v3lookingAt = m_pPositionPath->Interpolate(0.0f) + (m_pPositionPath->Interpolate(0.0f)-m_pPositionPath->Interpolate((m_fVelocity*0.5f)/m_pPositionPath->GetPathLength()));
			}
			else
				v3lookingAt = m_pPositionPath->Interpolate(fOriParam);
		}
		//we don't want to look at the floor but at the head position!
		v3lookingAt += m_v3UserOffset;
	}
	else
	{
		v3lookingAt = m_pOrientationPath->Interpolate(m_fParm, fPauseTimePos);
		v3Position = m_pPositionPath->Interpolate(m_fParm, fPauseTimeOri);
	}

	if(fPauseTimePos!=0.0f || fPauseTimeOri!=0.0f){
		if(fPauseTimePos==-1.0f || fPauseTimeOri==-1.0f) {
			Pause();
		}
		else{
			if(fPauseTimePos!=0.0f)
				SetPausedFor(fPauseTimePos);
			else
				SetPausedFor(fPauseTimeOri);
		}
	}

	v3Position += m_v3UserOffset;

	SetPosition(v3Position, v3lookingAt);

//#ifdef DEBUG
	//VistaVector3D v3Ori = GetDefaultLookingDirection();
	//v3Ori = m_pPlatform->GetRotation().Rotate(v3Ori);
	//v3Ori.Normalize();
//	vstr::debugi() << "Tour: Param = " << m_fParm << "  time: "<<deltaT<< endl<<				
//				  "Position =    " << v3Position<< endl<<
//				  "Orientation = "<<v3Ori<<endl<<endl<<
//					  "Looking at = " << v3lookingAt<< endl;
//					//  "PlatfromOri = "<<m_pPlatform->GetRotation().GetViewDir()<< endl<<
//					//  "Pos SegmentNr: "<<m_pPositionPath->GetCurrentSegmentNumber()<<"  Ori SegmentNr: "<<m_pOrientationPath->GetCurrentSegmentNumber()<<endl;
	//vstr::debugi()<<"Ori: "<<v3Ori<<"   pos: "<<m_pPlatform->GetTranslation()<<std::endl;
//#endif

	//vstr::debugi() << "Speed: "<<(v3Position-m_v3PausedPosition).GetLength()/deltaT<<endl;
	//m_v3PausedPosition=v3Position;

	if(m_fParm==1.0f || m_fParm==0.0f){
		Notify(MSG_TOUR_ENDED);
		vstr::debugi()<< "[VfaVirtualTour] Tour ended." << endl;
	}
	else
		Notify(MSG_TOUR_NEWSTEP);

}

void VfaVirtualTour::Pause()
{
	m_bIsInFlight = false;

	if(m_bGettingBroughtBack) {
		m_pBringingBackTour->Pause();
		m_bGettingBroughtBack=false;
		return;
	}


	//Save the current state, to see whether we moved away if we continue
	m_v3PausedPosition=m_pPlatform->GetTranslation();
	m_v3PausedOrientation = m_v3DefaultLookingDirection; //VistaVector3D(0.0, 0.0, -1.0);
	VistaQuaternion qOri= m_pPlatform->GetRotation();
	m_v3PausedOrientation = qOri.Rotate(m_v3PausedOrientation);
	m_v3PausedOrientation.Normalize();
}

void VfaVirtualTour::Continue(float fDirection)
{
	m_fDirectionFactor = fDirection;

	if(m_pPositionPath->GetNumberOfSegments() ==0 || (m_pOrientationPath->GetNumberOfSegments()==0 && !m_bNoRotationMode && !m_bLookAheadMode)){
		vstr::warnp()<<"[VfaVirtualTour] Not enough segments in the paths! Can't continue."<<std::endl;
		return;
	}

	if (!m_bIsInFlight && !m_bGettingBroughtBack)
	{
		if (m_dLastTimeStep != 0.0){ //this checked if the tour was started already.
			VistaVector3D v3Ori = GetDefaultLookingDirection();//(0.0, 0.0, -1.0);
			v3Ori = m_pPlatform->GetRotation().Rotate(v3Ori);
			v3Ori.Normalize();

			//if we are still where we paused we can just continue:
			if(m_pPlatform->GetTranslation() == m_v3PausedPosition 
				&& (Vector3DEquals(v3Ori, m_v3PausedOrientation) || m_bNoRotationMode))
			{
				m_dLastTimeStep = m_pRenderNode->GetVisTiming()->GetCurrentClock();
				m_bIsInFlight = true;
			}

			//otherwise we have to create a new VfaVirtualTour to bring us back, that we observe
			else
			{
				vstr::debugi()<<"[VfaVirtualTour] Creating new bringing back Tour for Continue(), because..."<<std::endl;
				if(m_pPlatform->GetTranslation() != m_v3PausedPosition)
					vstr::debugi()<<"                     ... Position are different: "<<m_pPlatform->GetTranslation()<<" != "<<m_v3PausedPosition<<std::endl;
				if(!Vector3DEquals(v3Ori, m_v3PausedOrientation))
					vstr::debugi()<<"                     ... Orientations are different: "<<v3Ori<<" != "<<m_v3PausedOrientation<<std::endl;

				CreateBringingBackTour(m_v3PausedPosition, m_v3PausedOrientation);
			}
		}
		else
			Start();
	}
}

void VfaVirtualTour::Restart()
{

	if(m_bGettingBroughtBack) {
		m_pBringingBackTour->Pause();
		m_bGettingBroughtBack=false;
	}

	m_bIsInFlight = false;
	m_fParm = 0.0f;
	AdjustPause(0.0f);

	Start();
}

void VfaVirtualTour::SetPositionPath(VfaPath* pPositionPath)
{
	if (pPositionPath){
		m_pPositionPath = pPositionPath;
		m_bInitialized = false;
	}
}

VfaPath* VfaVirtualTour::GetPositionPath() const
{
	return m_pPositionPath;
}

void VfaVirtualTour::SetOrientationPath(VfaPath* pOrientationPath)
{
	if (pOrientationPath){
		m_pOrientationPath = pOrientationPath;
		m_bInitialized = false;
	}
}

VfaPath* VfaVirtualTour::GetOrientationPath() const
{
	return m_pOrientationPath;
}

void VfaVirtualTour::SetVelocity(float fVelocity)
{
	if (fVelocity > 0)
		m_fVelocity = fVelocity;
}

float VfaVirtualTour::GetVelocity(){
	return m_fVelocity;
}

void VfaVirtualTour::AddPositionSegment(VfaSegment* pSegment)
{
	if (pSegment){
		m_pPositionPath->AddSegment(pSegment);
		m_bInitialized = false;
	}

}

void VfaVirtualTour::AddOrientationSegment(VfaSegment* pSegment)
{
	if (pSegment){
		m_pOrientationPath->AddSegment(pSegment);
		m_bInitialized = false;
	}
}

float VfaVirtualTour::GetParameter() const
{
	return m_fParm;
}

void VfaVirtualTour::SetParameter(float fParam) {
	if(fParam>=0.0f && fParam<=1.0f)
		m_fParm=fParam;
}

bool VfaVirtualTour::GetIsInFlight() const
{
	return m_bIsInFlight;
}

void VfaVirtualTour::SetSynchronousMode(bool bActive)
{
	m_bSynchronousMode = bActive;
}

bool VfaVirtualTour::GetSynchronousMode() const
{
	return m_bSynchronousMode;
}

void VfaVirtualTour::SetGapFilling(bool bActive)
{
	m_bGapFilling = bActive;
}

bool VfaVirtualTour::GetGapFilling() const
{
	return m_bGapFilling;
}

void VfaVirtualTour::SetCAVEMode(bool bActive)
{
	m_bCAVEMode = bActive;
}

bool VfaVirtualTour::GetCAVEMode() const
{
	return m_bCAVEMode;
}

void VfaVirtualTour::SetNoRotationMode(bool bNoRotation){
	m_bNoRotationMode = bNoRotation;
}

bool VfaVirtualTour::GetNoRotationMode(){
	return m_bNoRotationMode;
}

void VfaVirtualTour::SetLookAheadMode(bool bActive){
	m_bLookAheadMode = bActive;
}

bool VfaVirtualTour::GetLookAheadMode() const{
	return m_bLookAheadMode;
}

void VfaVirtualTour::SetLookAheadTime(float fTime){
	if(fTime>0.0f)
		m_fLookAheadTime = fTime;
}

float VfaVirtualTour::GetLookAheadTime() const{
	return m_fLookAheadTime;
}

void VfaVirtualTour::UpdateParameter(double dTimeStep)
{

	if (dTimeStep == 0.0)
		return;

	float fPositionDifferance = ((float)dTimeStep * m_fVelocity);
	float fPsitionPathLength = m_pPositionPath->GetPathLength();
	
	if (fPsitionPathLength != 0)
		m_fParm += m_fDirectionFactor * (fPositionDifferance/fPsitionPathLength);


}

unsigned int VfaVirtualTour::GetRegistrationMode() const
{
	return IVflRenderable::OLI_UPDATE | IVflRenderable::OLI_DRAW_OPAQUE;
}

IVflRenderable::VflRenderableProperties* VfaVirtualTour::CreateProperties() const
{
	return new VflRenderableProperties;
}

void VfaVirtualTour::SetPosition(VistaVector3D v3Pos, VistaVector3D v3LookingPoint)
{

	if (m_pPlatform){

		m_pPlatform->SetTranslation(v3Pos);

		if(m_bNoRotationMode){
			//we have set the translation and don't need no rotation so we're done here already.
			return;
		}

		if(m_bCAVEMode)
		{
			v3Pos[1]=0.0;
			v3LookingPoint[1]=0.0;

		}

		VistaVector3D v3InverseViewDir = v3Pos-v3LookingPoint,
					  v3Up = GetDefaultUpDirection(), //(0.0, 1.0, 0.0)
					  v3Right;

		v3Right = v3Up.Cross(v3InverseViewDir);
		v3Up = v3InverseViewDir.Cross(v3Right);

		v3InverseViewDir.Normalize();
		v3Right.Normalize();
		v3Up.Normalize();

		float aMat[16] = {
			v3Right[0], v3Up[0], v3InverseViewDir[0], 0.0f,
			v3Right[1], v3Up[1], v3InverseViewDir[1], 0.0f,
			v3Right[2], v3Up[2], v3InverseViewDir[2], 0.0f,
				0.0f,  0.0f,			    0.0f, 1.0f
		};

		VistaTransformMatrix vtmMatrix(aMat);
		VistaQuaternion qOri(vtmMatrix);

		m_pPlatform->SetRotation(qOri);
		
	}

}

void VfaVirtualTour::SetUserOffset(VistaVector3D v3UserOffset){
	m_v3UserOffset = v3UserOffset;
}

VistaVector3D VfaVirtualTour::GetUserOffset() const
{
	return m_v3UserOffset;
}

void VfaVirtualTour::SetPausedFor(float seconds) {
	m_fRestPauseTime=seconds;
	m_bIsPaused=true;
}

void VfaVirtualTour::ObserverUpdate( IVistaObserveable* pObserveable, int nMsg, int nTicket )
{
	//If the bringing back Tour(the tour consisting just of a linear segment, to the next position of both paths)
	//ended we want our tour to continue or start
	if(static_cast<VfaVirtualTour*>(pObserveable) == m_pBringingBackTour)
	{
		if(nMsg==MSG_TOUR_ENDED)
		{
			m_bGettingBroughtBack=false;

			VistaVector3D v3CurrentOri = GetDefaultLookingDirection();//VistaVector3D(0.0, 0.0, -1.0);
			v3CurrentOri = m_pPlatform->GetRotation().Rotate(v3CurrentOri);
			v3CurrentOri.Normalize();

			if(m_dLastTimeStep==0.0) {

				//we have to set the right position, so Start will be working even if we didn't 100% reach it!

				VistaVector3D v3StartLookingAt, v3StartPos;

				if(m_fDirectionFactor>0.0f) {
					v3StartPos = m_pPositionPath->GetSegment(0)->GetFirstRealStation();
					v3StartPos += m_v3UserOffset;
					if(!m_bNoRotationMode) {
						if(m_bLookAheadMode){
							float fOriParam = (m_fVelocity*m_fLookAheadTime)/m_pPositionPath->GetPathLength();
							if(fOriParam>1.0f)
								fOriParam = 1.0f;
							v3StartLookingAt = m_pPositionPath->Interpolate(fOriParam);
							v3StartLookingAt += m_v3UserOffset;
						}
						else
							v3StartLookingAt = m_pOrientationPath->GetSegment(0)->GetFirstRealStation();
					}
					else 
						v3StartLookingAt = VistaVector3D(0.0, 0.0, -1.0);
				}
				else {
					v3StartPos = m_pPositionPath->Interpolate(1.0f);
					v3StartPos += m_v3UserOffset;
					if(!m_bNoRotationMode)
						if(m_bLookAheadMode){
							float fOriParam = (m_fVelocity*m_fLookAheadTime)/m_pPositionPath->GetPathLength();
							if(fOriParam>1.0f)
								fOriParam = 1.0f;
							v3StartLookingAt = m_pPositionPath->Interpolate(1.0f - fOriParam);
							v3StartLookingAt += m_v3UserOffset;
						}
						else
							v3StartLookingAt = m_pOrientationPath->Interpolate(1.0f);
					else 
						v3StartLookingAt = VistaVector3D(0.0, 0.0, -1.0);
				}

				//we have to check whether we just got brought back to the position but not rotated yet
				if(Vector3DEquals((v3StartLookingAt-v3StartPos).GetNormalized(),v3CurrentOri))
					SetPosition(v3StartPos,v3StartLookingAt);
				else
					SetPosition(v3StartPos, v3StartPos+v3CurrentOri);

			}
			else {
				//This way even if we didn't 100% reach the position, Continue() is satisfied
				m_v3PausedPosition=m_pPlatform->GetTranslation();
				if(Vector3DEquals(m_v3PausedOrientation,v3CurrentOri))
					m_v3PausedOrientation=v3CurrentOri;
			}

			Continue(m_fDirectionFactor);
		}
	}
}

void VfaVirtualTour::CreateBringingBackTour(VistaVector3D v3EndPosition, VistaVector3D v3EndOrientation)
{
	//our bringin back Tour should consist of up to two parts, which are actually 2 tours
	//first we fly the platform to the right position
	//then we handle the rotation

	VistaVector3D v3Ori = m_v3DefaultLookingDirection;//(0.0, 0.0, -1.0);
	v3Ori = m_pPlatform->GetRotation().Rotate(v3Ori);
	v3Ori.Normalize();

	VistaVector3D v3Pos = m_pPlatform->GetTranslation();

	v3EndOrientation.Normalize();

	//we create a new Tour or "recycle" the old one
	if(!m_pBringingBackTour) {
		m_pBringingBackTour = new VfaVirtualTour(m_pRenderNode, m_pPlatform);
	}
	else{
		m_pBringingBackTour->Clear();
	}


	if ((v3EndPosition-v3Pos).GetLength()<0.001f) {
		VfaPointSegment* pPointSeg = new VfaPointSegment( acos(v3Ori.Dot(v3EndOrientation))/DEFAULT_ANGULAR_VELOCITY*GetVelocity()*1.6f, v3Pos);
		m_pBringingBackTour->AddPositionSegment(pPointSeg);

		VfaCircularSegment *pCircSeg = new VfaCircularSegment(v3Pos +v3Ori, v3Pos + v3EndOrientation, v3Pos);
		pCircSeg->SetFadeInLength(0.5f);
		pCircSeg->SetFadeOutLength(0.6f);
		m_pBringingBackTour->AddOrientationSegment(pCircSeg);

		m_pBringingBackTour->SetNoRotationMode(false);

	}
	else{
		//if we are in no rotation mode we just need a position path!
		VfaLinearSegment* pLinSeg = new VfaLinearSegment(v3Pos, v3EndPosition);
		pLinSeg->SetFadeInLength(0.4f);
		pLinSeg->SetFadeOutLength(0.6f);
		m_pBringingBackTour->AddPositionSegment(pLinSeg);
		m_pBringingBackTour->SetNoRotationMode(true);

		m_pBringingBackTour->SetSynchronousMode(false);
	}
	


	m_pBringingBackTour->SetCAVEMode(m_bCAVEMode);
	m_pBringingBackTour->SetVelocity(GetVelocity());
	m_pBringingBackTour->SetGapFilling(false);


	Observe(m_pBringingBackTour, IVistaObserveable::TICKET_NONE);
	m_bGettingBroughtBack=true;

	m_pBringingBackTour->Start();

}

bool VfaVirtualTour::Vector3DEquals(VistaVector3D v3Left, VistaVector3D v3Right)
{
	float eps=0.01f;
		return( abs(v3Left[0] - v3Right[0])<=eps
			&& abs(v3Left[1] - v3Right[1])<=eps
			&& abs(v3Left[2] - v3Right[2])<=eps);
}

void VfaVirtualTour::SetDefaultLookingDirection(VistaVector3D v3LookingVector){
	m_v3DefaultLookingDirection = v3LookingVector;
}

VistaVector3D VfaVirtualTour::GetDefaultLookingDirection(){
	return m_v3DefaultLookingDirection;
}

void VfaVirtualTour::SetDefaultUpDirection(VistaVector3D v3UpVector){
	m_v3DefaultUpDirection = v3UpVector;
}

VistaVector3D VfaVirtualTour::GetDefaultUpDirection(){
	return m_v3DefaultUpDirection;
}

void VfaVirtualTour::AdjustPause(float fParam){

	if(m_bIsInFlight)
		m_bIsInFlight=false;

	m_bIsPaused=true;
	m_fParm = fParam;
	m_dLastTimeStep = m_pRenderNode->GetVisTiming()->GetCurrentClock(); //we have to set this so the tour knows we don't want to start from the beginning if it wasn't started yet

	float fPauseTime=0.0;
	VistaVector3D v3PauselookingAt;
	VistaVector3D v3PausePosition;

	if(m_bNoRotationMode){
		v3PausePosition = m_pPositionPath->Interpolate(m_fParm, fPauseTime);
		v3PauselookingAt = VistaVector3D(0.0,0.0,0.0);
	}
	else if(m_bSynchronousMode)
	{
		v3PausePosition = m_pPositionPath->Interpolate(fParam, fPauseTime);
		int iSegNr=m_pPositionPath->GetCurrentSegmentNumber();
		float fLocalParm = (m_fParm - m_pPositionPath->GetParmCurrentSegStartedWith()) / m_pPositionPath->GetSegment(iSegNr)->GetSegmentLength() * m_pPositionPath->GetPathLength();
		//this is computed incorrectly if m_fParm==1.0f, therefor
		if(m_fParm==1.0) {
			fLocalParm=1.0;
		}
		v3PauselookingAt = m_pOrientationPath->InterpolateSynchronous(iSegNr, fLocalParm, fPauseTime);
	}
	else
	{
		v3PauselookingAt = m_pOrientationPath->Interpolate(m_fParm, fPauseTime);
		v3PausePosition = m_pPositionPath->Interpolate(m_fParm, fPauseTime);
	}

	m_v3PausedPosition = v3PausePosition;

	if(m_bCAVEMode)
	{
		v3PausePosition[1]=0.0;
		v3PauselookingAt[1]=0.0;

	}

	m_v3PausedOrientation = v3PauselookingAt - v3PausePosition;
	m_v3PausedOrientation.Normalize();
}

bool VfaVirtualTour::IsPaused() {
	return (!m_bIsInFlight);
}

float VfaVirtualTour::GetDirectionFactor() {
	return m_fDirectionFactor;
}

bool VfaVirtualTour::GoToStation(int iNumber) {
	std::vector<VfaSegment::VfaVTStation*> vecStations = m_pPositionPath->GetAllStations();
	if(iNumber<0 || iNumber >= vecStations.size()){
		return false;
	}
	AdjustPause(vecStations[iNumber]->fParameter);
	Continue(GetDirectionFactor());
	return true;
}

void VfaVirtualTour::Clear() {

	m_pPositionPath->ClearPath();
	m_pOrientationPath->ClearPath();
	m_fParm=0.0f;
	m_dLastTimeStep=0.0;
	m_bInitialized=false;
	m_bIsInFlight=false;
	m_bIsPaused=false;
	m_bGettingBroughtBack=false;
	m_fDirectionFactor=1.0;

}

VflRenderNode* VfaVirtualTour::GetTourRenderNode() {
	return m_pRenderNode;
}

VistaVirtualPlatform* VfaVirtualTour::GetTourVirtualPlatform() {
	return m_pPlatform;
}

string convertInt(int number)
{
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}

VistaPropertyList* VfaVirtualTour::CreatePropertyList(){
	VistaPropertyList* pPropList = new VistaPropertyList();
	
	std::string sName;

	std::vector<std::string> vecPositionSegmentNames;
	for(int i=0; i<m_pPositionPath->GetNumberOfSegments(); i++) {
		sName = "PositionSegment"+convertInt(i);
		vecPositionSegmentNames.push_back(sName);
		pPropList->SetPropertyListValue(sName ,*(m_pPositionPath->GetSegment(i)->CreatePropertyList()));
	}
	pPropList->SetValueInSubList("PositionSegments", "MAIN", vecPositionSegmentNames);

	std::vector<std::string> vecOrientationSegmentNames;
	for(int i=0; i<m_pPositionPath->GetNumberOfSegments(); i++) {
		sName = "OrientationSegment"+convertInt(i);
		vecOrientationSegmentNames.push_back(sName);
		pPropList->SetPropertyListValue(sName ,*(m_pOrientationPath->GetSegment(i)->CreatePropertyList()));
	}
	pPropList->SetValueInSubList("OrientationSegments", "MAIN", vecOrientationSegmentNames);

	pPropList->SetValueInSubList("Velocity", "MAIN", m_fVelocity);
	pPropList->SetValueInSubList("CaveMode", "MAIN", m_bCAVEMode);
	pPropList->SetValueInSubList("GapFilling", "MAIN", m_bGapFilling);
	pPropList->SetValueInSubList("NoRotationMode", "MAIN", m_bNoRotationMode);
	pPropList->SetValueInSubList("SynchronousMode", "MAIN", m_bSynchronousMode);
	pPropList->SetValueInSubList("DirectionFactor", "MAIN", m_fDirectionFactor);
	pPropList->SetValueInSubList("LookAheadTime", "MAIN", m_fLookAheadTime);
	pPropList->SetValueInSubList("LookAheadMode", "MAIN", m_bLookAheadMode);

#ifdef DEBUG
	pPropList->Print();
#endif

	return pPropList;
}

bool VfaVirtualTour::UpdateWithPropertyList(VistaPropertyList* pPropList) {


	if(!pPropList){
		vstr::errp()<<"[VfaVirtualTour] UpdateWithPropertyList called with NULL! Nothing changed!"<<std::endl;
		return false;
	}

	m_v3PausedPosition= VistaVector3D(-1.0,-1.0,-1.0);
	m_bInitialized = false;


	std::vector<std::string> vecPositionPath;
	pPropList->GetValueInSubList("PositionSegments", "MAIN", vecPositionPath);
	if(vecPositionPath.size()<=0)
		return false;

	if(!m_pPositionPath)
		m_pPositionPath = new VfaPath();

	for(int i=0; i<vecPositionPath.size(); i++) {
		VistaPropertyList segList;
		pPropList->GetValue(vecPositionPath[i], segList);
		if(i<m_pPositionPath->GetNumberOfSegments()) {
			if(m_pPositionPath->GetSegment(i)->GetTypeID()==segList.GetValueOrDefault("Type",(int)VfaSegment::VT_SEG_DEFAULT))
				m_pPositionPath->GetSegment(i)->UpdateWithPropertyList(&segList);
			else{
				m_pPositionPath->RemoveSegment(i);
				VfaSegment* pSeg = CreateSegment(segList);
				if(pSeg)
					m_pPositionPath->InsertSegment(pSeg,i);
				else {
					vstr::errp()<<"Can't add default segments! Broken PropertyList! Segment "<<i<<"in the position path"<<std::endl;
					return false;
				}
			}

		}
		else {
			VfaSegment* pSeg = CreateSegment(segList);
			if(pSeg)
				m_pPositionPath->AddSegment(pSeg);
			else {
				vstr::errp()<<"Can't add default segments! Broken PropertyList! Segment "<<i<<"in the position path"<<std::endl;
				return false;
			}
		}
	}
	while(m_pPositionPath->GetNumberOfSegments() > vecPositionPath.size()) {
		//we have to delete some old stations, because the path is now smaller
		m_pPositionPath->RemoveSegment(m_pPositionPath->GetNumberOfSegments()-1);
	}

	std::vector<std::string> vecOrientationPath;
	pPropList->GetValueInSubList("OrientationSegments", "MAIN", vecOrientationPath);
	if(!m_pOrientationPath)
		m_pOrientationPath = new VfaPath();

	for(int i=0; i<vecOrientationPath.size(); i++) {
		VistaPropertyList segList;
		pPropList->GetValue(vecOrientationPath[i], segList);
		if(i<m_pOrientationPath->GetNumberOfSegments()) {
			if(m_pOrientationPath->GetSegment(i)->GetTypeID()==segList.GetValueOrDefault("Type",(int)VfaSegment::VT_SEG_DEFAULT))
				m_pOrientationPath->GetSegment(i)->UpdateWithPropertyList(&segList);
			else{
				m_pOrientationPath->RemoveSegment(i);
				VfaSegment* pSeg = CreateSegment(segList);
				if(pSeg)
					m_pOrientationPath->InsertSegment(pSeg,i);
				else {
					vstr::errp()<<"Can't add default segments! Broken PropertyList! Segment "<<i<<"in the orientation path"<<std::endl;
					return false;
				}
			}

		}
		else {
			VfaSegment* pSeg = CreateSegment(segList);
			if(pSeg)
				m_pOrientationPath->AddSegment(pSeg);
			else {
				vstr::errp()<<"Can't add default segments! Broken PropertyList! Segment "<<i<<"in the orientation path"<<std::endl;
				return false;
			}
		}
	}
	while(m_pOrientationPath->GetNumberOfSegments() > vecOrientationPath.size()) {
		//we have to delete some old stations, because the path is now smaller
		m_pOrientationPath->RemoveSegment(m_pOrientationPath->GetNumberOfSegments()-1);
	}

	m_fVelocity = pPropList->GetValueInSubListOrDefault("Velocity", "MAIN", DEFAULT_VELOCITY);
	m_bCAVEMode  = pPropList->GetValueInSubListOrDefault("CaveMode", "MAIN", true);
	m_bGapFilling  = pPropList->GetValueInSubListOrDefault("GapFilling", "MAIN", true);
	m_bNoRotationMode = pPropList->GetValueInSubListOrDefault("NoRotationMode", "MAIN", true);
	m_bSynchronousMode = pPropList->GetValueInSubListOrDefault("SynchronousMode", "MAIN", true);
	m_fDirectionFactor = pPropList->GetValueInSubListOrDefault("DirectionFactor", "MAIN", 1.0f);
	m_fLookAheadTime = pPropList->GetValueInSubListOrDefault("LookAheadTime", "MAIN", 1.0f);
	m_bLookAheadMode = pPropList->GetValueInSubListOrDefault("LookAheadMode", "MAIN", false);

	return true;
}

VfaSegment* VfaVirtualTour::CreateSegment(VistaPropertyList PropList) {
	switch(PropList.GetValueOrDefault("Type",(int)VfaSegment::VT_SEG_DEFAULT)) {
		case VfaSegment::VT_SEG_CATMULLROM:
			return new VfaCatmullRomSegment(&PropList);
			break;
		case VfaSegment::VT_SEG_CIRCULAR:
			return new VfaCircularSegment(&PropList);
			break;
		case VfaSegment::VT_SEG_LINEAR:
			return new VfaLinearSegment(&PropList);
			break;
		case VfaSegment::VT_SEG_POINT:
			return new VfaPointSegment(&PropList);
			break;
		case VfaSegment::VT_SEG_DEFAULT:
			break;
	}
	return NULL;

}


// ========================================================================== //
// === End of File
// ========================================================================== //


