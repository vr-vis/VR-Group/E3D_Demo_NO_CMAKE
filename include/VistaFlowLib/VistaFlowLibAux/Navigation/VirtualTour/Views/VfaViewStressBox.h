/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#ifndef _VFAVIEWSTRESSBOX_H
#define _VFAVIEWSTRESSBOX_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaFlowLibAux/Interaction/VfaApplicationContextObject.h>

#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

// this stressbox is used by the RiverTour to visualize that you are about to leave the radius and pause the tour

class VISTAFLOWLIBAUXAPI VfaViewStressBox : public IVflRenderable
{

	public: 
		VfaViewStressBox(VistaVirtualPlatform *pPlatform);
		virtual ~VfaViewStressBox();

		virtual void DrawTransparent();
		virtual unsigned int GetRegistrationMode() const;
		virtual VflRenderableProperties* CreateProperties() const;

		// ---------a---------
		// |                 |
		// b				 b
		// |				 |
		// ---------a--------- depth c
		void SetSize(float a, float b, float c);

		//value between 0.0 and 1.0 to whoch amount the box is stressed
		void SetPercentage(float fPercentage);

		void SetPlatform(VistaVirtualPlatform *pPlatform);

		void SetApplicationContext(VfaApplicationContextObject* pAppContext, VflRenderNode* pRenderNode);


	protected:
		float						m_fa,m_fb,m_fc;
		float						m_faAbsolute, m_fbAbsolute, m_fcAbsolute;
		float						m_fPercentage;
		float						m_fColorCalm[4];
		float						m_fColorStressed[4];

		bool						m_bUseLighting;
		bool						m_bAtHand;

		VistaVector3D				m_v3Position;
		VistaAxisAndAngle			m_aaaRotation;
		VistaVirtualPlatform*		m_pPlatform;
		VfaApplicationContextObject* m_pAppContext;
		VflRenderNode*				m_pRenderNode;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAVIEWSTRESSBOX_H
