/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaViewArrows.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaOGLExt/VistaGLLine.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VatssSliderVis                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaViewArrows::VfaViewArrows()
{

}

VfaViewArrows::~VfaViewArrows()
{
}

void VfaViewArrows::DrawTransparent()
{


	if (m_vecSamples.size() < 1)
		return;
	if(m_vecSamples.size() < 2){
		//we probably just have one point station, so we just double this point so we get a sphere displayed
		m_vecSamples.push_back(m_vecSamples[0]);
	}

	DrawCurrentPositionArrow();

	glColor4f(m_fColor[0],m_fColor[1],m_fColor[2],m_fTransparency);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



	VistaVector3D v3FromPoint;
	VistaVector3D v3ToPoint;
	VistaVector3D v3MiddlePoint;


	VistaGLLine::SetLineWidth(m_fWidth);
	VistaGLLine::SetArrowheadRadius(m_fWidth*2);
	

	for (int i = 0; i <= m_vecSamples.size() - 2; i++){

		v3FromPoint = GetScaledPoint(m_vecSamples[i]);
		v3ToPoint = GetScaledPoint(m_vecSamples[i+1]);

		if(v3FromPoint != v3ToPoint ) {
			
			//we draw one arrow that is half the length, between the two sample points
			v3MiddlePoint = v3FromPoint + (v3ToPoint-v3FromPoint)*0.5f;

			VistaGLLine::SetArrowheadLength((v3ToPoint-v3FromPoint).GetLength()/6);

			//we have to enable and begin and then afterwards end and disable VistaGLLine for every
			//arrow, because otherwise we weren't allowed to change the arrowHeadLength dynamically
			VistaGLLine::Enable(VistaGLLine::SHADER_ARROWS);
			VistaGLLine::Begin(VistaGLLine::VISTA_GL_LINES);

			glVertex3f(v3FromPoint[0]  , v3FromPoint[1]  , v3FromPoint[2]  ); 
			glVertex3f(v3MiddlePoint[0]  , v3MiddlePoint[1]  , v3MiddlePoint[2]  ); 

			VistaGLLine::End();
			VistaGLLine::Disable();

			
		}
		else {

			if(i>0 && m_vecSamples[i] == m_vecSamples[i-1]) {
				//we have already drawn this PointSegment point
				continue;
			}

			

			//so we draw a sphere because it is a point segment
			glPushAttrib(GL_LIGHTING_BIT);
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();

			//glDisable(GL_LIGHTING);

			glTranslatef(v3FromPoint[0], v3FromPoint[1], v3FromPoint[2]);	


			glutSolidSphere( m_fWidth*2, 100, 100); 

			glPopMatrix();
			glPopAttrib();
		}

	}


	

}
