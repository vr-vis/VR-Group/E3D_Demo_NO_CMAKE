/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaViewSlider.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaOGLExt/VistaGLLine.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;


/*============================================================================*/
/*  IMPLEMENTATION      VfaViewSlider                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaViewSlider::VfaViewSlider()
{

}

VfaViewSlider::~VfaViewSlider()
{
}

void VfaViewSlider::DrawTransparent()
{

	if (m_vecSamples.size() < 2)
		return;

	DrawCurrentPositionArrow();

	
	glMatrixMode(GL_MODELVIEW);

	//int depth;
	//glGetIntegerv (GL_MODELVIEW_STACK_DEPTH, &depth);
	//vstr::outi()<<"stack size= "<<depth<<std::endl;

	//float fParams[16];
	//glGetFloatv(GL_MODELVIEW_MATRIX, fParams);
	//vstr::outi()<<fParams[0]<<",  "<<fParams[1]<<",  "<<fParams[2]<<",  "<<fParams[3]<<std::endl
	//			<<fParams[4]<<",  "<<fParams[5]<<",  "<<fParams[6]<<",  "<<fParams[7]<<std::endl
	//			<<fParams[8]<<",  "<<fParams[9]<<",  "<<fParams[10]<<",  "<<fParams[11]<<std::endl
	//			<<fParams[12]<<",  "<<fParams[13]<<",  "<<fParams[14]<<",  "<<fParams[15]<<std::endl
	//			<<"===========================trans: "<<m_fTransparency<<" ============================"<<std::endl;


	glPushAttrib(GL_LIGHTING_BIT|GL_POLYGON_BIT|GL_TRANSFORM_BIT);
	glPushMatrix();

	glDisable(GL_CULL_FACE);
	glColor4f(m_fColor[0],m_fColor[1],m_fColor[2],m_fTransparency);
	glDisable(GL_LIGHTING);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Jonathan: new quadrangular tube slider, for old polygon strip version, see below!

	VistaVector3D v3OldLeft;
	VistaVector3D v3OldRight;

	GetRightAndLeft(GetScaledPoint(m_vecUntransformedSamples[0]), GetScaledPoint(m_vecUntransformedSamples[1]), v3OldRight, v3OldLeft);
	v3OldLeft = m_mTransform.Transform(v3OldLeft);
	v3OldRight = m_mTransform.Transform(v3OldRight);

	//at first we create front cover of our long quadrangular tube representing the path
	glBegin(GL_QUADS);


	glVertex3f(v3OldLeft[0]  , v3OldLeft[1]  , v3OldLeft[2]  );
	glVertex3f(v3OldLeft[0] - 0.1f*m_fWidth , v3OldLeft[1] - 0.1f*m_fWidth , v3OldLeft[2] - 0.1f*m_fWidth ); 
	glVertex3f(v3OldRight[0] - 0.1f*m_fWidth, v3OldRight[1] - 0.1f*m_fWidth, v3OldRight[2]- 0.1f*m_fWidth );
 	glVertex3f(v3OldRight[0] , v3OldRight[1] , v3OldRight[2] );
 	
	

	glEnd();


	for (int i = 1; i <= m_vecSamples.size() - 1; i++){
		
		if(m_vecSamples[i]==m_vecSamples[i-1]) {
			//we haven't move, so we don't have to draw anything
			continue;
		}

		//now we create one segment of our quadrangular tube

		glBegin(GL_QUAD_STRIP);

		VistaVector3D v3TopPoint = GetScaledPoint(m_vecUntransformedSamples[i-1]);
		VistaVector3D v3NextDirectionPoint = GetScaledPoint(m_vecUntransformedSamples[i]);

		VistaVector3D v3TopRight;
		VistaVector3D v3TopLeft;

		GetRightAndLeft(v3TopPoint, v3NextDirectionPoint, v3TopRight, v3TopLeft);
		v3TopRight = m_mTransform.Transform(v3TopRight);
		v3TopLeft = m_mTransform.Transform(v3TopLeft);

		glVertex3f(v3OldLeft[0]  , v3OldLeft[1]  , v3OldLeft[2]  );
		glVertex3f(v3TopLeft[0]  , v3TopLeft[1]  , v3TopLeft[2]  ); 

		glVertex3f(v3OldRight[0] , v3OldRight[1] , v3OldRight[2] );
 		glVertex3f(v3TopRight[0] , v3TopRight[1] , v3TopRight[2] );

		glVertex3f(v3OldRight[0]- 0.1f*m_fWidth , v3OldRight[1]- 0.1f*m_fWidth , v3OldRight[2]- 0.1f*m_fWidth );
 		glVertex3f(v3TopRight[0]- 0.1f*m_fWidth , v3TopRight[1]- 0.1f*m_fWidth , v3TopRight[2]- 0.1f*m_fWidth );

		glVertex3f(v3OldLeft[0]- 0.1f*m_fWidth  , v3OldLeft[1]- 0.1f*m_fWidth  , v3OldLeft[2]- 0.1f*m_fWidth  );
		glVertex3f(v3TopLeft[0]- 0.1f*m_fWidth  , v3TopLeft[1]- 0.1f*m_fWidth  , v3TopLeft[2]- 0.1f*m_fWidth  ); 

		//and again our fist Vertices to close the tube!
		glVertex3f(v3OldLeft[0]  , v3OldLeft[1]  , v3OldLeft[2]  );
		glVertex3f(v3TopLeft[0]  , v3TopLeft[1]  , v3TopLeft[2]  ); 
		


		glEnd();

		v3OldLeft = v3TopLeft;
		v3OldRight = v3TopRight;

	}

	//and finally we close the tube
	glBegin(GL_QUADS);

	glVertex3f(v3OldLeft[0]  , v3OldLeft[1]  , v3OldLeft[2]  );
	glVertex3f(v3OldLeft[0] - 0.1f*m_fWidth , v3OldLeft[1] - 0.1f*m_fWidth , v3OldLeft[2] - 0.1f*m_fWidth ); 
	glVertex3f(v3OldRight[0] - 0.1f*m_fWidth, v3OldRight[1] - 0.1f*m_fWidth, v3OldRight[2]- 0.1f*m_fWidth );
 	glVertex3f(v3OldRight[0] , v3OldRight[1] , v3OldRight[2] ); 

	glEnd();



	//Jonathan: this is the old version of the slider, just a single polygon strip. I replaced this with the
	//quadrangular tube above

	//glBegin(GL_QUAD_STRIP);

	//for (int i = 0; i < m_vecSamples.size() - 1; i++){
	//	
	//	if(m_vecSamples[i]==m_vecSamples[i+1]) {
	//		continue;
	//	}

	//	VistaVector3D v3TopPoint = GetScaledPoint(m_vecSamples[i]);
	//	VistaVector3D v3BasePoint = GetScaledPoint(m_vecSamples[i+1]);

	//	VistaVector3D v3TopRight;
	//	VistaVector3D v3Topleft;
	//	GetRightAndLeft(v3TopPoint, v3BasePoint, v3TopRight, v3Topleft);

	//	glVertex3f(v3Topleft[0]  , v3Topleft[1]  , v3Topleft[2]  ); 
 //		glVertex3f(v3TopRight[0] , v3TopRight[1] , v3TopRight[2] );

	//}

	//glEnd();

	glPopAttrib();
	glPopMatrix();



	

}

float VfaViewSlider::GetSampleDensityFactor() {
	return 1.0f;
}


