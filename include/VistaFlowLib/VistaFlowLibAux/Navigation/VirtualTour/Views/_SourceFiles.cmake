

set( RelativeDir "./Navigation/VirtualTour/Views" )
set( RelativeSourceGroup "Source Files\\Navigation\\VirtualTour\\Views" )
set( SubDirs )

set( DirFiles
	VfaVirtualTourViewer.h
	VfaVirtualTourViewer.cpp
	VfaViewNode.h
	VfaViewNode.cpp
	VfaViewLine.h
	VfaViewLine.cpp
	VfaViewSlider.h
	VfaViewSlider.cpp
	VfaViewArrows.cpp
	VfaViewArrows.h
	VfaViewDirArrows.cpp
	VfaViewDirArrows.h
	VfaViewStressBox.cpp
	VfaViewStressBox.h
	VfaViewStressSphere.cpp
	VfaViewStressSphere.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()

