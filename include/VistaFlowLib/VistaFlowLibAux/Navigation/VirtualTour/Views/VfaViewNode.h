/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#ifndef _VfaViewNode_H
#define _VfaViewNode_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

// This class is used internally by VfaVirtualTourViewer to draw,
// to draw actually the two inheritted classes VfaViewLine and VfaViewSlider
// VfaViewArrows are used.

class VISTAFLOWLIBAUXAPI VfaViewNode : public IVflRenderable
{
public: 
	VfaViewNode();
	virtual ~VfaViewNode();

	//this function draws path of this render node,
	// it is implemented by ViewSlider and ViewLine
	virtual void DrawTransparent() = 0;

	virtual unsigned int GetRegistrationMode() const;

	//adds a new point /sample to our path that should be drawn
	void AddSample(VistaVector3D point);
	std::vector<VistaVector3D> GetSamplesVector();

	//adds a new point of the orientation path to the visualizer, just used by VfaViewDirArrows
	void AddOriSample(VistaVector3D point);

	//sets /gets a scalefactor for every direction x,y anz in a vector
	void SetXYZScale(VistaVector3D v3xyzSamplesScale);
	VistaVector3D GetXYZScale();

	//empty methods just to fill the IVflRenderable interface
	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);

	//if this a valid number 0<=fParam<=1 an arrow is drawn at the given position, this is usefull
	//if the user should predefine for example a starting point, to disable the arrow you just have to 
	//set fParam to an invalid number e.g. -1.0
	void SetCurrentPosition(float fParam);
	//this method is called in the DrawTransparent methods of each View and draws the arrow if the saved parameter is valid
	void DrawCurrentPositionArrow();

	void SetColor(const VistaColor& color);
	VistaColor GetColor() const;
	
	void SetColor(float fColor[4]);
	void GetColor(float fColor[4]) const;

	bool SetUseLighting(bool b);
	bool GetUseLighting() const;

	void SetNodeWidth(float w);
	float GetNodeWidth();

	void SetOpacity(float t);
	float GetOpacity();

	void SetTranformMatrix(VistaTransformMatrix mTransform);
	VistaTransformMatrix GetTranformMatrix();

	//this factor is used for the TourViewer to determine how many samples are needed, for example Arrows need far less samples than a slider
	virtual float GetSampleDensityFactor();

protected:

	virtual VflRenderableProperties* CreateProperties() const;

	//Scales the point by the scale factor of every dimension
	VistaVector3D GetScaledPoint(VistaVector3D v3Point);

	/* This funtion creats two position vectors v3TopLeft and v3TopRight which are 


						TopPoint
			 TopLeft <------*------>TopRight

			         <_____________>
						width

		
	
	 
						    * BasePoint

	*/
	void GetRightAndLeft(VistaVector3D v3TopPoint, VistaVector3D v3BasePoint, VistaVector3D& v3TopRight, VistaVector3D& v3Topleft);

	std::vector<VistaVector3D>		m_vecSamples;
	std::vector<VistaVector3D>		m_vecUntransformedSamples;
	std::vector<VistaVector3D>		m_vecOriSamples; //is just used by VfaViewDirArrows
	VistaVector3D					m_v3xyzSceneSamplesScale;
	float							m_fWidth;

	float							m_fColor[4];
	bool							m_bUseLighting;
	float							m_fTransparency; //this actually is opacity

	float							m_fCurrentParam;
	VistaTransformMatrix			m_mTransform;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaViewNode_H
