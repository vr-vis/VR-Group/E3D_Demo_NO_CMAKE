/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#ifndef _VFAVIEWDIRARROWS_H
#define _VFAVIEWDIRARROWS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include "VfaViewNode.h"

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

// a representation of a path as arrwos, the arrwos always point towards the orientation
// path that means they always represent the orientation of the user at this position. The
// velocity is represented by the density of the arrows.
// to change the density  (number of arrows) use SetNumberOfStepsForPathSampling(int iSteps)
// of the VirtualTourViewer, where iSteps represents the number of arrows (less if
// we use PointSegments.
// If this is used no, Orientation Path is shown, because all the information is represented
// in this single path.

class VISTAFLOWLIBAUXAPI VfaViewDirArrows : public VfaViewNode
{

	public: 
		VfaViewDirArrows();
		virtual ~VfaViewDirArrows();

		virtual void DrawTransparent();

		
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAVIEWDIRARROWS_H
