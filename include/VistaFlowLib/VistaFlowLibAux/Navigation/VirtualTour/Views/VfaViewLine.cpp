/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaViewLine.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaOGLExt/VistaGLLine.h>


#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VatssSliderVis                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaViewLine::VfaViewLine()
{

	
}

VfaViewLine::~VfaViewLine()
{
}

void VfaViewLine::DrawTransparent()
{

	if (m_vecSamples.size() < 2)
		return;

	DrawCurrentPositionArrow();

	glColor4f(m_fColor[0],m_fColor[1],m_fColor[2],m_fTransparency);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	VistaGLLine::SetLineWidth(m_fWidth);
	VistaGLLine::SetHaloSize(50.0f);

	VistaGLLine::Enable(VistaGLLine::SHADER_CYLINDER);
	VistaGLLine::Begin(VistaGLLine::VISTA_GL_LINE_STRIP);


	for (int i = 0; i <= m_vecSamples.size() - 1; i++){

		if(i>0 && m_vecSamples[i]==m_vecSamples[i-1]) {
			continue;
		}

		VistaVector3D v3TopPoint = GetScaledPoint(m_vecSamples[i]);
		glVertex3f(v3TopPoint[0]  , v3TopPoint[1]  , v3TopPoint[2]  ); 

	}

	VistaGLLine::End();
	VistaGLLine::Disable();

}

float VfaViewLine::GetSampleDensityFactor() {
	return 1.0f;
}

