/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  CameraNavigationApp.cpp	     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA FlowLib Prototype      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                        Rheinisch-Westfaelische      */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VfaVirtualTourViewer.h"
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaPath.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLibAux/Widgets/Slider/VfaSliderVis.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include "VfaViewSlider.h"
#include "VfaViewLine.h"
#include "VfaViewArrows.h"
#include "VfaViewDirArrows.h"

float CONST_DEFAULT_COLOR[4] = {0,1,0,0.5};
float CONST_DEFAULT_WIDTH = 0.5f;
const float DEAFAULT_HEAD_HEIGHT =1.8f;

VfaVirtualTourViewer::VfaVirtualTourViewer(VfaVirtualTour* pVirtualTour, 
											VflRenderNode *pRenderNode, 
											EVisualizationType iPosType /*= VFA_VIEW_SLIDER*/,
											EVisualizationType iOriType /*= VFA_VIEW_LINE*/)
	: m_pRenderNode (pRenderNode)
	, m_pVirtualTour (pVirtualTour)
	, m_fOrientationWidth(CONST_DEFAULT_WIDTH)
	, m_fPositionWidth(CONST_DEFAULT_WIDTH)
	, m_fPosPathInterpolateSteps(0.01f)
	, m_fOriPathInterpolateSteps(0.01f)
	, m_iOriType(iOriType)
	, m_iPosType(iPosType)
	, m_bVisible(false)
	, m_fHeadHeight(DEAFAULT_HEAD_HEIGHT)
	, m_pPositionViewNode(NULL)
	, m_pOrientationViewNode(NULL)
{
	
	SetOrientationPathViewerColor(CONST_DEFAULT_COLOR);
	SetPositionPathViewerColor(CONST_DEFAULT_COLOR);

}
void VfaVirtualTourViewer::Init() {

	if (!m_pRenderNode || !m_pVirtualTour)
		return;

	if(m_bVisible) {
		//we have already displayed it so we first have to delete the old representation
		if(m_pOrientationViewNode){
			m_pRenderNode->RemoveRenderable(m_pOrientationViewNode);
			delete m_pOrientationViewNode;
		}
		if(m_pPositionViewNode){
			m_pRenderNode->RemoveRenderable(m_pPositionViewNode);
			delete m_pPositionViewNode;
		}
	}


	switch(m_iPosType) {
		case VFA_VIEW_DIRECTION_ARROWS:
		case VFA_VIEW_NONE:
			m_pPositionViewNode = NULL;
			break;
		case VFA_VIEW_ARROWS:
			m_pPositionViewNode = new VfaViewArrows();
			break;
		case VFA_VIEW_LINE:
			m_pPositionViewNode = new VfaViewLine();
			break;
		case VFA_VIEW_SLIDER:
		default:	
			m_pPositionViewNode = new VfaViewSlider();
			break;
	}

	switch(m_iOriType) {
		case VFA_VIEW_DIRECTION_ARROWS:
		case VFA_VIEW_NONE:
			m_pOrientationViewNode = NULL;
			break;
		case VFA_VIEW_SLIDER:
			m_pOrientationViewNode = new VfaViewSlider();
			break;
		case VFA_VIEW_LINE:
			m_pOrientationViewNode = new VfaViewLine();
			break;
		case VFA_VIEW_ARROWS:
		default:	
			m_pOrientationViewNode = new VfaViewArrows();
			break;
	}

	if(m_pPositionViewNode){
		m_pPositionViewNode->SetNodeWidth(m_fPositionWidth);
		m_pPositionViewNode->SetColor(m_cPositionColor);
	}
	if(m_pOrientationViewNode){
		m_pOrientationViewNode->SetNodeWidth(m_fOrientationWidth);
		m_pOrientationViewNode->SetColor(m_cOrientationColor);
	}

	VfaPath* pOrienationPath = m_pVirtualTour->GetOrientationPath();
	VfaPath* pPositionPath = m_pVirtualTour->GetPositionPath();

	VistaTransformMatrix mTrans;
	//we need to call update here first, otherwise the matrix provided by the rendernode might not be yet up to date
	m_pRenderNode->Update(m_pRenderNode->GetVisTiming()->GetCurrentClock());
	m_pRenderNode->GetWorldInverseTransform(mTrans);

	if(m_iOriType==VFA_VIEW_DIRECTION_ARROWS || m_iPosType==VFA_VIEW_DIRECTION_ARROWS) {
		m_pPositionViewNode = new VfaViewDirArrows();
		m_pOrientationViewNode=NULL;
		m_pPositionViewNode->SetTranformMatrix(mTrans);
		m_pPositionViewNode->SetNodeWidth(m_fPositionWidth);
		m_pPositionViewNode->SetColor(m_cPositionColor);

		if(pPositionPath->CheckAndPrepare() && pOrienationPath->CheckAndPrepare() ) {
			m_fPosPathInterpolateSteps = 1.0f / pPositionPath->GetPathLength() * m_pPositionViewNode->GetSampleDensityFactor() * m_fPositionWidth;//so we get a nice spacing of the arrows
			m_fOriPathInterpolateSteps = m_fPosPathInterpolateSteps;
			for (float fParm = 0.0; fParm < 1.0; fParm += m_fPosPathInterpolateSteps){
				VistaVector3D v3Sample = pPositionPath->Interpolate(fParm);
				v3Sample[1] -= m_fHeadHeight;
				m_pPositionViewNode->AddSample(v3Sample);
				v3Sample = pOrienationPath->Interpolate(fParm);
				m_pPositionViewNode->AddOriSample(v3Sample);
			}

		}
		return;
	}
	


	if (m_pPositionViewNode && pPositionPath->CheckAndPrepare()){
		
		m_fPosPathInterpolateSteps = 1.0f / pPositionPath->GetPathLength()* m_pPositionViewNode->GetSampleDensityFactor() * m_fPositionWidth;//so we get a nice spacing
		if(m_fPosPathInterpolateSteps>1.0f)
			m_fPosPathInterpolateSteps=1.0f;

		m_pPositionViewNode->SetTranformMatrix(mTrans);

		for (float fParm = 0.0; fParm < 1.0; fParm += m_fPosPathInterpolateSteps){
			VistaVector3D v3Sample = pPositionPath->Interpolate(fParm);
			v3Sample[1] -= m_fHeadHeight;
			if(m_pPositionViewNode)//maybe we used VFA_VIEW_NONE, so this pointer would pe NULL
				m_pPositionViewNode->AddSample(v3Sample);
		}

	}

	

	if (m_pOrientationViewNode && pOrienationPath->CheckAndPrepare()){

		m_fOriPathInterpolateSteps = 1.0f / pOrienationPath->GetPathLength() * m_pOrientationViewNode->GetSampleDensityFactor() * m_fOrientationWidth;
		if(m_fOriPathInterpolateSteps>1.0f)
			m_fOriPathInterpolateSteps=1.0f;

		m_pOrientationViewNode->SetTranformMatrix(mTrans);

		for (float fParm = 0.0; fParm < 1.0; fParm += m_fOriPathInterpolateSteps){
			VistaVector3D v3Sample = pOrienationPath->Interpolate(fParm);
			if(m_pOrientationViewNode)//maybe we used VFA_VIEW_NONE, so this pointer would be NULL
				m_pOrientationViewNode->AddSample(v3Sample);
		}

	}
}


VfaVirtualTourViewer::~VfaVirtualTourViewer()
{
	if(m_pOrientationViewNode)
		m_pRenderNode->RemoveRenderable(m_pOrientationViewNode);
	if(m_pPositionViewNode)
		m_pRenderNode->RemoveRenderable(m_pPositionViewNode);
	delete m_pOrientationViewNode;
	delete m_pPositionViewNode;
}

void VfaVirtualTourViewer::Show()
{
	Init();

	if (m_pPositionViewNode){
		m_pPositionViewNode->Init();
		m_pPositionViewNode->SetVisible(true);
		m_pRenderNode->AddRenderable(m_pPositionViewNode);
	}

	if (m_pOrientationViewNode){
		m_pOrientationViewNode->Init();
		m_pOrientationViewNode->SetVisible(true);
		m_pRenderNode->AddRenderable(m_pOrientationViewNode);
	}
	m_bVisible=true;
}

void VfaVirtualTourViewer::Hide() {
		

	if (m_pPositionViewNode){
		m_pPositionViewNode->SetVisible(false);
	}

	if (m_pOrientationViewNode){
		m_pOrientationViewNode->SetVisible(false);
	}
	m_bVisible=false;
}

bool VfaVirtualTourViewer::IsVisible() {
	return m_bVisible;
}

void VfaVirtualTourViewer::SetPositionPathViewerColor(float color[4]){
	m_cPositionColor[0] = color[0];
	m_cPositionColor[1] = color[1];
	m_cPositionColor[2] = color[2];
	m_cPositionColor[3] = color[3];

	if(m_pPositionViewNode) {
		m_pPositionViewNode->SetColor(m_cPositionColor);
	}

}
void VfaVirtualTourViewer::SetOrientationPathViewerColor(float color[4]){
	m_cOrientationColor[0] = color[0];
	m_cOrientationColor[1] = color[1];
	m_cOrientationColor[2] = color[2];
	m_cOrientationColor[3] = color[3];

	if(m_pOrientationViewNode) {
		m_pOrientationViewNode->SetColor(m_cOrientationColor);
	}

}

void VfaVirtualTourViewer::GetPositionPathViewerColor(float color[4]){

	if(m_pPositionViewNode) {
		m_pPositionViewNode->GetColor(color);
		return;
	}

	color[0] = m_cPositionColor[0];
	color[1] = m_cPositionColor[1];
	color[2] = m_cPositionColor[2];
	color[3] = m_cPositionColor[3];
}
void VfaVirtualTourViewer::GetOrientationPathViewerColor(float color[4]){

	if(m_pOrientationViewNode) {
		m_pOrientationViewNode->GetColor(color);
		return;
	}

	color[0] = m_cOrientationColor[0];
	color[1] = m_cOrientationColor[1];
	color[2] = m_cOrientationColor[2];
	color[3] = m_cOrientationColor[3];
}

void VfaVirtualTourViewer::SetPositionPathViewerWidth(float fWidth){
	m_fPositionWidth = fWidth > 0 ? fWidth : CONST_DEFAULT_WIDTH;
	if(m_pPositionViewNode){
		m_pPositionViewNode->SetNodeWidth(m_fPositionWidth);
	}
}
void VfaVirtualTourViewer::SetOrientationPathViewerWidth(float fWidth){
	m_fOrientationWidth = fWidth > 0 ? fWidth : CONST_DEFAULT_WIDTH;
	if(m_pOrientationViewNode){
		m_pOrientationViewNode->SetNodeWidth(m_fOrientationWidth);
	}
}

float VfaVirtualTourViewer::GetPositionPathViewerWidth(){
	return m_fPositionWidth;
}
float VfaVirtualTourViewer::GetOrientationPathViewerWidth(){
	return m_fOrientationWidth;
}

void VfaVirtualTourViewer::SetNumberOfStepsForPosPathSampling(int iSteps) {
	if(iSteps > 1)
		m_fPosPathInterpolateSteps = 1.0f/iSteps;
}

int VfaVirtualTourViewer::GetNumberOfStepsForPosPathSampling() {
	return (int)(1.0f/m_fPosPathInterpolateSteps);
}

void VfaVirtualTourViewer::SetNumberOfStepsForOriPathSampling(int iSteps) {
	if(iSteps > 1)
		m_fOriPathInterpolateSteps = 1.0f/iSteps;
}

int VfaVirtualTourViewer::GetNumberOfStepsForOriPathSampling() {
	return (int)(1.0f/m_fOriPathInterpolateSteps);
}

void VfaVirtualTourViewer::SetCurrentPosition(float fParam){
	if(m_pPositionViewNode)
		m_pPositionViewNode->SetCurrentPosition(fParam);
}

VfaVirtualTourViewer::EVisualizationType VfaVirtualTourViewer::GetPositionPathVisType(){
	return m_iPosType;
}
void VfaVirtualTourViewer::SetPositionPathVisType(VfaVirtualTourViewer::EVisualizationType iType){
	m_iPosType = iType;
	if(m_bVisible)
		Show();
}

VfaVirtualTourViewer::EVisualizationType VfaVirtualTourViewer::GetOrientationPathVisType(){
	return m_iOriType;
}
void VfaVirtualTourViewer::SetOrientationPathVisType(VfaVirtualTourViewer::EVisualizationType iType){
	m_iOriType = iType;
	if(m_bVisible)
		Show();
}
float VfaVirtualTourViewer::GetHeadHeight() {
	return m_fHeadHeight;
}
void VfaVirtualTourViewer::SetHeadHeight(float fHeight){
	m_fHeadHeight=fHeight;
	if(m_bVisible)
		Show();
}

/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/


