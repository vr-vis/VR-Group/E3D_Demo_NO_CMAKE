/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaViewDirArrows.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaOGLExt/VistaGLLine.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VatssSliderVis                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaViewDirArrows::VfaViewDirArrows()
{

}

VfaViewDirArrows::~VfaViewDirArrows()
{
}

void VfaViewDirArrows::DrawTransparent()
{


	if (m_vecSamples.size() < 2)
		return;

	if(m_vecSamples.size() != m_vecOriSamples.size()) {
		vstr::errp() << "[VfaViewDirArrows] Different number of samples in orientation ("<<m_vecOriSamples.size()<<") and position ("<<m_vecSamples.size()<<") vector!"<<endl;
		return;
	}

	DrawCurrentPositionArrow();

	glColor4f(m_fColor[0],m_fColor[1],m_fColor[2],m_fTransparency);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	VistaVector3D v3FromPoint;
	VistaVector3D v3ToPoint;
	VistaVector3D v3MiddlePoint;


	VistaGLLine::SetLineWidth(m_fWidth/3);
	VistaGLLine::SetArrowheadRadius(m_fWidth);
	VistaGLLine::SetArrowheadLength(m_fWidth);
	
	VistaGLLine::Enable(VistaGLLine::SHADER_ARROWS);
	VistaGLLine::Begin(VistaGLLine::VISTA_GL_LINES);

	for (int i = 0; i < m_vecSamples.size(); i++){

		v3FromPoint = GetScaledPoint(m_vecSamples[i]);
		v3ToPoint = GetScaledPoint(m_vecOriSamples[i]);
			
		//we draw an arrwo from our fromPoint (on the position Path) towards our toPoint
		//on the orientation path with the length of m_fWidth
		v3MiddlePoint = v3ToPoint-v3FromPoint;
		v3MiddlePoint.Normalize();
		v3MiddlePoint = v3FromPoint + v3MiddlePoint*m_fWidth*3;

		glVertex3f(v3FromPoint[0]  , v3FromPoint[1]  , v3FromPoint[2]  ); 
		glVertex3f(v3MiddlePoint[0]  , v3MiddlePoint[1]  , v3MiddlePoint[2]  ); 

	}

	VistaGLLine::End();
	VistaGLLine::Disable();
	

}


