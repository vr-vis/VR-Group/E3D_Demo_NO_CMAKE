/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaViewStressBox.h"
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaOGLExt/VistaGLLine.h>


#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VatssSliderVis                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaViewStressBox::VfaViewStressBox(VistaVirtualPlatform *pPlatform)
	:m_faAbsolute(0.4f),
	m_fbAbsolute(0.2f),
	m_fcAbsolute(0.2f),
	m_bUseLighting(false),
	m_fPercentage(0.0f),
	m_pPlatform(pPlatform),
	m_bAtHand(false)
{
	m_fColorCalm[0] = 1.0f;
	m_fColorCalm[1] = 1.0f;
	m_fColorCalm[2] = 0.0f;
	m_fColorCalm[3] = 1.0f;

	m_fColorStressed[0]=1.0f;
	m_fColorStressed[1]=0.2f;
	m_fColorStressed[2]=0.0f;
	m_fColorStressed[3]=1.0f;

	Init();

	SetVisible(false);

	
}

VfaViewStressBox::~VfaViewStressBox()
{
}

unsigned int VfaViewStressBox::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_TRANSPARENT;
}
IVflRenderable::VflRenderableProperties* VfaViewStressBox::CreateProperties() const
{
	return new IVflRenderable::VflRenderableProperties;
}

void VfaViewStressBox::DrawTransparent()
{

	float fColor[4];

	//compute interpolation between our colours
	fColor[0] = m_fColorCalm[0] + (m_fColorStressed[0] - m_fColorCalm[0])*m_fPercentage;
	fColor[1] = m_fColorCalm[1] + (m_fColorStressed[1] - m_fColorCalm[1])*m_fPercentage;
	fColor[2] = m_fColorCalm[2] + (m_fColorStressed[2] - m_fColorCalm[2])*m_fPercentage;
	fColor[3] = m_fColorCalm[3] + (m_fColorStressed[3] - m_fColorCalm[3])*m_fPercentage;


	glPushAttrib(GL_LIGHTING_BIT|GL_POLYGON_BIT|GL_TRANSFORM_BIT);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	if(m_bAtHand) {

		VfaApplicationContextObject::sSensorFrame oFrame;
		m_pAppContext->GetSensorFrame(VfaApplicationContextObject::SLOT_POINTER_VIS,m_pRenderNode, oFrame);
	
		m_v3Position = oFrame.v3Position;
		//m_v3Position = m_pPlatform->GetTranslation();
		m_aaaRotation = m_pPlatform->GetRotation().GetAxisAndAngle();
		//m_aaaRotation = oFrame.qOrientation.GetAxisAndAngle();
	}
	else{
		m_v3Position = m_pPlatform->GetTranslation();
		m_aaaRotation = m_pPlatform->GetRotation().GetAxisAndAngle();
	}


	glTranslatef(m_v3Position[0], m_v3Position[1], m_v3Position[2]);
	glRotatef(m_aaaRotation.m_fAngle/3.1415f*180, m_aaaRotation.m_v3Axis[0], m_aaaRotation.m_v3Axis[1], m_aaaRotation.m_v3Axis[2]);
	glTranslatef(0.0,0.0,-0.5);

	m_fa = m_faAbsolute*(1+m_fPercentage*4);
	m_fb = m_fbAbsolute * (1-m_fPercentage/2);
	m_fc = m_fcAbsolute * (1-m_fPercentage/2);

	glDisable(GL_CULL_FACE);
	glColor4f(fColor[0],fColor[1],fColor[2],fColor[3]);

	if(!m_bUseLighting)
		glDisable(GL_LIGHTING);
	else
		glEnable(GL_LIGHTING);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_QUADS);

	//front cover
	glVertex3f(-m_fa/2  , +m_fb/2  , +m_fc/2  );
	glVertex3f(-m_fa/2  , -m_fb/2  , +m_fc/2  );
	glVertex3f(+m_fa/2  , -m_fb/2  , +m_fc/2  );
	glVertex3f(+m_fa/2  , +m_fb/2  , +m_fc/2  );

	//left side
	glVertex3f(-m_fa/2  , +m_fb/2  , -m_fc/2  );
	glVertex3f(-m_fa/2  , -m_fb/2  , -m_fc/2  );
	glVertex3f(-m_fa/2  , -m_fb/2  , +m_fc/2  );
	glVertex3f(-m_fa/2  , +m_fb/2  , +m_fc/2  );

	//top
	glVertex3f(-m_fa/2  , +m_fb/2  , -m_fc/2  );
	glVertex3f(-m_fa/2  , +m_fb/2  , +m_fc/2  );
	glVertex3f(+m_fa/2  , +m_fb/2  ,+m_fc/2  );
	glVertex3f(m_fa/2  , m_fb/2  , -m_fc/2  );

	//right side
	glVertex3f(m_fa/2  ,m_fb/2  , m_fc/2  );
	glVertex3f(m_fa/2  , -m_fb/2  , m_fc/2  );
	glVertex3f(m_fa/2  , -m_fb/2  , -m_fc/2  );
	glVertex3f(m_fa/2  , m_fb/2  , -m_fc/2  );

	//bottom
	glVertex3f(-m_fa/2  , -m_fb/2  , m_fc/2  );
	glVertex3f(-m_fa/2  , -m_fb/2  , -m_fc/2  );
	glVertex3f(m_fa/2  , -m_fb/2  , -m_fc/2  );
	glVertex3f(m_fa/2  , -m_fb/2  , m_fc/2  );

	glEnd();

	glPopMatrix();
	glPopAttrib();

	//vstr::warn()<<"Position: "<<m_v3Position<<std::endl;

}

		// ---------a---------
		// |                 |
		// b				 b
		// |				 |
		// ---------a--------- depth c
void VfaViewStressBox::SetSize(float a, float b, float c){
	m_faAbsolute = a;
	m_fbAbsolute = b;
	m_fcAbsolute = c;
}

		//value between 0.0 and 1.0 to whoch amount the box is stressed
void VfaViewStressBox::SetPercentage(float fPercentage){
	m_fPercentage = fPercentage;
}

void VfaViewStressBox::SetPlatform(VistaVirtualPlatform *pPlatform) {
	m_pPlatform = pPlatform;
}

void VfaViewStressBox::SetApplicationContext(VfaApplicationContextObject* pAppContext, VflRenderNode* pRenderNode){
	if(pAppContext==NULL){
		m_bAtHand=false;
		m_faAbsolute*=10;
		m_fbAbsolute*=10;
		m_fcAbsolute*=10;
	}
	else {
		m_pAppContext = pAppContext;
		m_pRenderNode = pRenderNode;
		m_bAtHand=true;
		m_faAbsolute*=0.1f;
		m_fbAbsolute*=0.1f;
		m_fcAbsolute*=0.1f;
	}
}







