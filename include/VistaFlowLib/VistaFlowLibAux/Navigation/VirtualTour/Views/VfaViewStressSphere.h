/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#ifndef _VFAVIEWSTRESSSPHERE_H
#define _VFAVIEWSTRESSSPHERE_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaFlowLibAux/Interaction/VfaApplicationContextObject.h>

#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>

#include "VfaViewStressBox.h"



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

// this stressbox is used by the RiverTour to visualize that you are about to leave the radius and pause the tour

class VISTAFLOWLIBAUXAPI VfaViewStressSphere : public VfaViewStressBox
{

	public: 
		VfaViewStressSphere(VistaVirtualPlatform *pPlatform);
		virtual ~VfaViewStressSphere();

		virtual void DrawTransparent();




	private:
		int m_iSubdivisions;
		
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFAVIEWSTRESSSPHERE_H
