/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaViewStressSphere.h"
#include <VistaKernel/GraphicsManager/VistaGeometry.h>



#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VatssSliderVis                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaViewStressSphere::VfaViewStressSphere(VistaVirtualPlatform *pPlatform)
	:VfaViewStressBox(pPlatform)
	, m_iSubdivisions(100)
{
	SetSize(0.2f,0.2f,0.2f);
	
}

VfaViewStressSphere::~VfaViewStressSphere()
{
}



void VfaViewStressSphere::DrawTransparent()
{

	float fColor[4];

	//compute interpolation between our colours
	fColor[0] = m_fColorCalm[0] + (m_fColorStressed[0] - m_fColorCalm[0])*m_fPercentage;
	fColor[1] = m_fColorCalm[1] + (m_fColorStressed[1] - m_fColorCalm[1])*m_fPercentage;
	fColor[2] = m_fColorCalm[2] + (m_fColorStressed[2] - m_fColorCalm[2])*m_fPercentage;
	fColor[3] = m_fColorCalm[3] + (m_fColorStressed[3] - m_fColorCalm[3])*m_fPercentage;


	glPushAttrib(GL_LIGHTING_BIT|GL_POLYGON_BIT|GL_TRANSFORM_BIT);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	if(m_bAtHand) {

		VfaApplicationContextObject::sSensorFrame oFrame;
		m_pAppContext->GetSensorFrame(VfaApplicationContextObject::SLOT_POINTER_VIS,m_pRenderNode, oFrame);
	
		m_v3Position = oFrame.v3Position;
		//m_v3Position = m_pPlatform->GetTranslation();
		m_aaaRotation = m_pPlatform->GetRotation().GetAxisAndAngle();
		//m_aaaRotation = oFrame.qOrientation.GetAxisAndAngle();
	}
	else{
		m_v3Position = m_pPlatform->GetTranslation();
		m_aaaRotation = m_pPlatform->GetRotation().GetAxisAndAngle();
	}



	glTranslatef(m_v3Position[0], m_v3Position[1], m_v3Position[2]);
	glRotatef(m_aaaRotation.m_fAngle/3.1415f*180, m_aaaRotation.m_v3Axis[0], m_aaaRotation.m_v3Axis[1], m_aaaRotation.m_v3Axis[2]);
	glTranslatef(0.0,0.0,-0.5);
	

	m_fa = m_faAbsolute*(1+m_fPercentage*4);
	m_fb = m_fbAbsolute * (1-m_fPercentage/2);
	m_fc = m_fcAbsolute * (1-m_fPercentage/2);

	glScalef(1.0f, m_fb/m_fa, m_fc/m_fa);

	glDisable(GL_CULL_FACE);
	glColor4f(fColor[0],fColor[1],fColor[2],fColor[3]);

	if(!m_bUseLighting)
		glDisable(GL_LIGHTING);
	else
		glEnable(GL_LIGHTING);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutSolidSphere( m_fa, m_iSubdivisions, m_iSubdivisions); 

	glPopMatrix();
	glPopAttrib();

	//vstr::warn()<<"Position: "<<m_v3Position<<std::endl;

}









