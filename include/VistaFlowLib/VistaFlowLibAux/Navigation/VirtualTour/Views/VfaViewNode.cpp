/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaViewNode.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>
#include <VistaOGLExt/VistaGLLine.h>

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

float CONST_TRANSPARENCY = 1.0;
float CONST_WIDTH = 0.5;

/*============================================================================*/
/*  IMPLEMENTATION      VfaViewNode                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaViewNode::VfaViewNode()
{

	m_fTransparency = CONST_TRANSPARENCY;
	m_fWidth = CONST_WIDTH;

	m_fColor[0] = 1.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 0.0f;
	m_fColor[3] = m_fTransparency;

	m_v3xyzSceneSamplesScale = VistaVector3D(1.0, 1.0, 1.0);
	m_fCurrentParam=-1.0f;

	m_vecSamples.clear();

}

VfaViewNode::~VfaViewNode()
{
}

unsigned int VfaViewNode::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_TRANSPARENT;
}

bool VfaViewNode::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}

bool VfaViewNode::GetBounds(VistaBoundingBox &)
{
	return false;
}

IVflRenderable::VflRenderableProperties* VfaViewNode::CreateProperties() const
{
	return new IVflRenderable::VflRenderableProperties;
}

void VfaViewNode::SetColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);

	m_fColor[0] = fColor[0];
	m_fColor[1] = fColor[1];
	m_fColor[2] = fColor[2];
	m_fColor[3] = fColor[3];

	m_fTransparency = fColor[3];
}

VistaColor VfaViewNode::GetColor() const
{
	VistaColor color(m_fColor);
	return color;
}

void VfaViewNode::SetColor(float fColor[4])
{
	m_fColor[0] = fColor[0];
	m_fColor[1] = fColor[1];
	m_fColor[2] = fColor[2];
	m_fColor[3] = fColor[3];
	m_fTransparency = fColor[3];

	//vstr::outi()<<"Set Colour, with opacity: "<<m_fTransparency<<" and colour ("<<m_fColor[0]<<", "<<m_fColor[1]<<", "<<m_fColor[2]<<")"<<std::endl;
}

void VfaViewNode::GetColor(float fColor[4]) const
{
	fColor[0] = m_fColor[0];
	fColor[1] = m_fColor[1];
	fColor[2] = m_fColor[2];
	fColor[3] = m_fTransparency;
}

bool VfaViewNode::SetUseLighting(bool b)
{
	if(m_bUseLighting == b)
		return false;

	m_bUseLighting = b;
	return true;
}

bool VfaViewNode::GetUseLighting() const
{
	return m_bUseLighting;
}

void VfaViewNode::AddSample(VistaVector3D point){
	m_vecUntransformedSamples.push_back(point);
	point = m_mTransform.Transform(point);
	m_vecSamples.push_back(point);
}
void VfaViewNode::AddOriSample(VistaVector3D point){
	point = m_mTransform.Transform(point);
	m_vecOriSamples.push_back(point);
}

std::vector<VistaVector3D> VfaViewNode::GetSamplesVector(){
	return m_vecSamples;
}

void VfaViewNode::SetXYZScale(VistaVector3D v3xyzSamplesScale){
	m_v3xyzSceneSamplesScale= v3xyzSamplesScale;
}

VistaVector3D VfaViewNode::GetXYZScale(){
	return m_v3xyzSceneSamplesScale;
}

VistaVector3D VfaViewNode::GetScaledPoint(VistaVector3D v3Point){
	VistaVector3D v3ScaledPoint = v3Point;
	v3ScaledPoint[0] /= m_v3xyzSceneSamplesScale[0];
	v3ScaledPoint[1] /= m_v3xyzSceneSamplesScale[1];
	v3ScaledPoint[2] /= m_v3xyzSceneSamplesScale[2];
	return v3ScaledPoint;
}

void VfaViewNode::SetNodeWidth(float w){
	m_fWidth = w > 0 ? w : CONST_WIDTH;

	//vstr::outi()<<"Set width to "<<m_fWidth<<std::endl;
}


float VfaViewNode::GetNodeWidth(){
	return m_fWidth;
}

void VfaViewNode::SetOpacity(float t){
	m_fTransparency = t > 0 ? t : CONST_TRANSPARENCY;
	m_fColor[3] = m_fTransparency;
}

float VfaViewNode::GetOpacity(){
	return m_fTransparency;
}

void VfaViewNode::GetRightAndLeft(VistaVector3D v3TopPoint, VistaVector3D v3BasePoint, VistaVector3D& v3TopRight, VistaVector3D& v3Topleft){

			VistaVector3D v3Dir = (v3BasePoint - v3TopPoint);
			v3Dir[1] = 0;
			v3Dir.Normalize();

			v3TopRight = v3Dir;
			v3TopRight[0] = v3Dir[2];
			v3TopRight[2] = -v3Dir[0];
			v3TopRight.Normalize();

			v3TopRight *= m_fWidth/2;
			v3TopRight += v3TopPoint;

			v3Topleft = v3Dir;
			v3Topleft[0] = -v3Dir[2];
			v3Topleft[2] = v3Dir[0];
			v3Topleft.Normalize();

			v3Topleft *= m_fWidth/2;
			v3Topleft += v3TopPoint;

}

void VfaViewNode::SetCurrentPosition(float fParam){
	if(fParam<0.0f || fParam>1.0f)
		m_fCurrentParam=-1.0f;
	else
		m_fCurrentParam = fParam;
}

void VfaViewNode::DrawCurrentPositionArrow(){

	if(m_fCurrentParam==-1.0)
		return;

	//the position arrow is red!
	glColor4f(1.0,0.0,0.0,m_fTransparency);

	VistaVector3D v3FromPoint;
	VistaVector3D v3ToPoint;

	VistaGLLine::SetLineWidth(m_fWidth/4);
	VistaGLLine::SetArrowheadRadius(m_fWidth);
	VistaGLLine::SetArrowheadLength(m_fWidth);
	
	VistaGLLine::Enable(VistaGLLine::SHADER_ARROWS);
	VistaGLLine::Begin(VistaGLLine::VISTA_GL_LINES);

	int iSampleNr = (int)(m_fCurrentParam * m_vecSamples.size());
	if(iSampleNr>=m_vecSamples.size()) {
		iSampleNr = (int)(m_vecSamples.size()-1);
	}
	v3FromPoint = GetScaledPoint(m_vecSamples[iSampleNr])+VistaVector3D(0.0f, 4*m_fWidth, 0.0f);
	v3ToPoint = GetScaledPoint(m_vecSamples[iSampleNr])+VistaVector3D(0.0f, m_fWidth, 0.0f);
			
	glVertex3f(v3FromPoint[0]  , v3FromPoint[1]  , v3FromPoint[2]  ); 
	glVertex3f(v3ToPoint[0]  , v3ToPoint[1]  , v3ToPoint[2]  ); 

	VistaGLLine::End();
	VistaGLLine::Disable();
}

float VfaViewNode::GetSampleDensityFactor() {
	return 7.0f;
}

void VfaViewNode::SetTranformMatrix(VistaTransformMatrix mTransform){
	//vstr::warni()<<"New Transform matrix: "<<mTransform<<std::endl;
	m_mTransform = mTransform;
}
VistaTransformMatrix VfaViewNode::GetTranformMatrix(){
	return m_mTransform;
}


