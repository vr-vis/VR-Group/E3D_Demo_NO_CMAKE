/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/* 
 * This class visualizes the orientation and position path of the virtualTour,
 * therefor it adds to renderables to the render node, m_positionViewNode, which is a
 * VfaViewSlider instance and m_orientationViewNode, which is of the type VfaViewLine
 * Once all settings are set, Show() must be called.
 *
 * You should construct the Viewer after your Tour is set up, because the constructor already reads
 * the data from the Tour.
 *
 */

#ifndef _VfaVIRTUALTOURVIEWER_H
#define _VfaVIRTUALTOURVIEWER_H

#include "../../../VistaFlowLibAuxConfig.h"

#include <VistaFlowLibAux/Navigation/VirtualTour/VfaVirtualTour.h>
#include "VfaViewNode.h"
#include <VistaFlowLib/Visualization/VflRenderable.h>

class VISTAFLOWLIBAUXAPI VfaVirtualTourViewer
{

public:

	 enum EVisualizationType{
		VFA_VIEW_SLIDER = 0,
		VFA_VIEW_LINE,
		VFA_VIEW_ARROWS,
		VFA_VIEW_DIRECTION_ARROWS,
		VFA_VIEW_NONE
	};

	//This creats a new View, which visualizes the Pathes, you can chose with iPosType and iOriType how
	//the pathes should be displayed for further information see VfaViewArrwos.h, VfaViewLine.h, VfaViewSlider.h and VfaViewDirArrows.h
	VfaVirtualTourViewer(VfaVirtualTour* pVirtualTour, VflRenderNode *pRenderNode, EVisualizationType iPosType = VFA_VIEW_SLIDER,  EVisualizationType iOriType = VFA_VIEW_ARROWS);

	~VfaVirtualTourViewer();

	//show the pathes
	void Show();

	//hides the path
	void Hide();

	//Sets / gets the colors for the line representing the positon / orientation path
	//as {red, green, blue, alpha}
	void SetPositionPathViewerColor(float color[4]);
	void SetOrientationPathViewerColor(float color[4]);

	void GetPositionPathViewerColor(float color[4]);
	void GetOrientationPathViewerColor(float color[4]);

	//Sets / gets the width of the line representing the positon / orientation path
	void SetPositionPathViewerWidth(float fWidth);
	void SetOrientationPathViewerWidth(float fWidth);

	float GetPositionPathViewerWidth();
	float GetOrientationPathViewerWidth();


	//Gets/Sets the number of steps made while interpolating along the Path, this is even more important when using
	//VFA_VIEW_ARROWS because this as well sets the density of the arrows. However, normaly a density of one sample
	//step per unit is used automatically
	void SetNumberOfStepsForPosPathSampling(int iSteps);
	int GetNumberOfStepsForPosPathSampling();
	void SetNumberOfStepsForOriPathSampling(int iSteps);
	int GetNumberOfStepsForOriPathSampling();

	//if this a valid number 0<=fParam<=1 an arrow is drawn at the given position, this is usefull
	//if the user should predefine for example a starting point, to disable the arrow you just have to 
	//set fParam to an invalid number e.g. -1.0
	void SetCurrentPosition(float fParam);

	EVisualizationType GetPositionPathVisType();
	void SetPositionPathVisType(EVisualizationType iType);

	EVisualizationType GetOrientationPathVisType();
	void SetOrientationPathVisType(EVisualizationType iType);

	//is set to 1.8 per default, so the path will appear under the user not at the actual position of the camera
	float GetHeadHeight();
	void SetHeadHeight(float fHeight);

	bool IsVisible();

private:

	//is called by show 
	void Init();

	VfaVirtualTour*		m_pVirtualTour;
	VfaViewNode*		m_pPositionViewNode;
	VfaViewNode*		m_pOrientationViewNode;
	VflRenderNode*		m_pRenderNode;

	EVisualizationType	m_iOriType;
	EVisualizationType	m_iPosType;

	float				m_fPositionWidth;
	float				m_fOrientationWidth;

	float				m_cPositionColor[4];
	float				m_cOrientationColor[4];

	float				m_fPosPathInterpolateSteps;
	float				m_fOriPathInterpolateSteps;
	bool				m_bVisible;

	float				m_fHeadHeight;

};

#endif /*_VfaVIRTUALTOURVIEWER_H*/
