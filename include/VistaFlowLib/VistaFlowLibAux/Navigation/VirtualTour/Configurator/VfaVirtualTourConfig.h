/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#ifndef _VFAVIRTUALTOURCONFIG_H
#define _VFAVIRTUALTOURCONFIG_H

#include "../../../VistaFlowLibAuxConfig.h"
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaVirtualTour.h>

/* This class basically just sets the properties like velocity, sync-mode,
 * CAVE-mode, filling-gaps-mode for the given Tour when you call Config().
 * On its own, this class is not of much use so, we use its subclasses,
 * like VfaVirtualTourAngularVelocityConfig.
 */

class VISTAFLOWLIBAUXAPI VfaVirtualTourConfig
{

public:

	VfaVirtualTourConfig(VfaVirtualTour* pVirtualTour);

	~VfaVirtualTourConfig();

	void Config();

	void SetDesiredPositionVelocity(float fVelocity);
	void SetSyncMode(bool bSyncMode);
	void SetCaveMode(bool bCaveMode);
	void SetFillingTheGapsMode(bool bFillGaps);

	//Set the positional velocity for the tour (m/s)
	float GetDesiredPositionVelocity();
	bool GetSyncMode();
	bool GetCaveMode();
	bool GetFillingTheGapsMode();
	float GetFinalVelocity();

protected:

	VfaVirtualTour*		m_pVirtualTour;

	float				m_fDesiredPositionVelocity;
	float				m_fFinalVelocity;
	bool				m_bSyncMode;
	bool				m_bCaveMode;
	bool				m_bFillTheGaps;

};

#endif /*_VFAVIRTUALTOURCONFIG_H*/
