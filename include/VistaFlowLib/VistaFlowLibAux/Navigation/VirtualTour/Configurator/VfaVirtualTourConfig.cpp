/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  CameraNavigationApp.cpp	     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA FlowLib Prototype      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                        Rheinisch-Westfaelische      */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VfaVirtualTourConfig.h"
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaPath.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

const bool SYNC_MODE = false;
const bool CAVE_MODE = true;
const bool FILL_THE_GAPS = true;

VfaVirtualTourConfig::VfaVirtualTourConfig(VfaVirtualTour* pVirtualTour)
	: m_pVirtualTour (pVirtualTour)
	, m_fDesiredPositionVelocity (pVirtualTour->GetVelocity())
	, m_bSyncMode (SYNC_MODE)
	, m_bCaveMode (CAVE_MODE)
	, m_bFillTheGaps (FILL_THE_GAPS)
{
	m_fFinalVelocity = m_fDesiredPositionVelocity;
}


VfaVirtualTourConfig::~VfaVirtualTourConfig()
{	
}

void VfaVirtualTourConfig::Config()
{

	if (!m_pVirtualTour)
		return;

	//set basic variables
	m_pVirtualTour->SetSynchronousMode(m_bSyncMode);
	m_pVirtualTour->SetCAVEMode(m_bCaveMode);
	m_pVirtualTour->SetGapFilling(m_bFillTheGaps);
	m_fFinalVelocity = m_fDesiredPositionVelocity;

	m_pVirtualTour->SetVelocity(m_fFinalVelocity);

}

void VfaVirtualTourConfig::SetDesiredPositionVelocity(float fVelocity){
	m_fDesiredPositionVelocity = fVelocity > 0 ? fVelocity : m_pVirtualTour->GetVelocity();
	
}

void VfaVirtualTourConfig::SetSyncMode(bool bSyncMode){
	m_bSyncMode = bSyncMode;
}

void VfaVirtualTourConfig::SetCaveMode(bool bCaveMode){
	m_bCaveMode = bCaveMode;
}

void VfaVirtualTourConfig::SetFillingTheGapsMode(bool bFillGaps){
	m_bFillTheGaps = bFillGaps;
}

float VfaVirtualTourConfig::GetDesiredPositionVelocity(){
	return m_fDesiredPositionVelocity;
}

bool VfaVirtualTourConfig::GetSyncMode(){
	return m_bSyncMode;
}

bool VfaVirtualTourConfig::GetCaveMode(){
	return m_bCaveMode;
}

bool VfaVirtualTourConfig::GetFillingTheGapsMode(){
	return m_bFillTheGaps;
}

float VfaVirtualTourConfig::GetFinalVelocity(){
	return m_fFinalVelocity;
}

/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

