/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#ifndef _VFAVIRTUALTOURANGULARVELOCITYCONFIG_H
#define _VFAVIRTUALTOURANGULARVELOCITYCONFIG_H

#include "../../../VistaFlowLibAuxConfig.h"
#include "VfaVirtualTourConfig.h"
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaVirtualTour.h>

/*
 * This class adjust by calling Config the velocity (of the position path) in a way
 * that the angular velocity of the movement never get higher than maxAngularVelocity
 */

class VISTAFLOWLIBAUXAPI VfaVirtualTourAngularVelocityConfig : public VfaVirtualTourConfig
{

public:

	VfaVirtualTourAngularVelocityConfig(VfaVirtualTour* pVirtualTour);

	~VfaVirtualTourAngularVelocityConfig();

	//adjusts the velocity of the tour and as well sets the different modes, like cave, sync, .... (see VfaVirtualTourConfig)
	void Config();

	//sets/ gets the max angular velocity to which the position velocity is adjusted
	void SetMaxAngularVelocity(float fAngularVelocity);
	float GetMaxAngularVelocity();

	//sets / gets the step size, tjhis should be a value >0 and <0.5, which is the sample step size in the parameter-space [0,1]
	void SetSamplingStepSize(float fSamplingStepSize);
	float GetSamplingStepSize();

private:


	float				m_fMaxAngularVelocity;
	float				m_fStepSize; //(0.0, 0.5)

	std::vector<VistaVector3D>		m_vecPositonSamples;
	std::vector<VistaVector3D>		m_vecOrientationSamples;

};

#endif /*_VFAVIRTUALTOURANGULARVELOCITYCONFIG_H*/
