/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  CameraNavigationApp.cpp	     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA FlowLib Prototype      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                        Rheinisch-Westfaelische      */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VfaVirtualTourAngularVelocityConfig.h"
#include "VfaVirtualTourConfig.h"
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaPath.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

const float MAX_ANGULAR_VELOCITY = 1000000;
const float	STEP_SIZE = 0.001f;

VfaVirtualTourAngularVelocityConfig::VfaVirtualTourAngularVelocityConfig(VfaVirtualTour* pVirtualTour)
	: VfaVirtualTourConfig (pVirtualTour)
	, m_fMaxAngularVelocity (MAX_ANGULAR_VELOCITY)
	, m_fStepSize (STEP_SIZE)
{
}


VfaVirtualTourAngularVelocityConfig::~VfaVirtualTourAngularVelocityConfig()
{	
}

void VfaVirtualTourAngularVelocityConfig::Config()
{
	
	float fNotUsedPauseTime=0.0;

	if (!m_pVirtualTour)
		return;

	VfaVirtualTourConfig::Config();

	m_fFinalVelocity = m_fDesiredPositionVelocity;

	//Sample the path to decide the proper velocity
	VfaPath* pPositionPath = m_pVirtualTour->GetPositionPath();
	VfaPath* pOrienationPath = m_pVirtualTour->GetOrientationPath();

	if (pPositionPath->CheckAndPrepare() && pOrienationPath->CheckAndPrepare()){

		float fPathSampleLength = pPositionPath->GetPathLength() * m_fStepSize;
		float fPathSampleTime = fPathSampleLength / m_fFinalVelocity;

		float fParm = 0.0;
		VistaVector3D v3PreOrientationSample;
		VistaVector3D v3PrePositionSample;
		for (; fParm <= 1.0; fParm += m_fStepSize){

			VistaVector3D v3PositionSample = pPositionPath->Interpolate(fParm, fNotUsedPauseTime);
			m_vecPositonSamples.push_back(v3PositionSample);

			VistaVector3D v3OrientationSample = pOrienationPath->Interpolate(fParm, fNotUsedPauseTime);
			m_vecOrientationSamples.push_back(v3OrientationSample);

			if(fParm > 2*m_fStepSize){ //Skip the first sample

				VistaVector3D v3Vec1, v3Vec2;

				if (v3OrientationSample == v3PreOrientationSample){
					//Movement felt on the Position Path
					v3Vec1 =v3PrePositionSample - v3OrientationSample;
					v3Vec2 = v3PositionSample - v3OrientationSample;

				}else{
					//Movement felt on the Orientation Path
					v3Vec1 =v3PreOrientationSample - v3PositionSample;
					v3Vec2 = v3OrientationSample - v3PositionSample;
				}

				v3Vec1.Normalize();
				v3Vec2.Normalize();

				float fAngel = acos((v3Vec1[0] * v3Vec2[0] + v3Vec1[1] * v3Vec2[1] + v3Vec1[2] * v3Vec2[2]) /
					(v3Vec1.GetLength() * v3Vec2.GetLength()));

				float fAngularVelocity = fAngel/fPathSampleTime;

				if (fAngularVelocity > m_fMaxAngularVelocity){ //Adjust Position Velocity
					fPathSampleTime = fAngel/m_fMaxAngularVelocity;
					//vstr::debugi() << "Virtual Tour Configurator has changed the Velocity from " << m_fFinalVelocity;
					m_fFinalVelocity = fPathSampleLength/fPathSampleTime;
					//vstr::debugi() << " to " << m_fFinalVelocity << ", since angular velocity was " << fAngularVelocity << std::endl;
				}

			}

			v3PrePositionSample = v3PositionSample;
			v3PreOrientationSample = v3OrientationSample;
		}

	}

	m_pVirtualTour->SetVelocity(m_fFinalVelocity);

	vstr::outi() << "[VirtualTourAngularVelocityConfig] Virtual Tour has been configured! New velocity: " << m_fFinalVelocity<<" Desired was: "<<m_fDesiredPositionVelocity<< std::endl;

}

void VfaVirtualTourAngularVelocityConfig::SetMaxAngularVelocity(float fAngularVelocity){
	m_fMaxAngularVelocity = fAngularVelocity > 0 ? fAngularVelocity : MAX_ANGULAR_VELOCITY;
}

void VfaVirtualTourAngularVelocityConfig::SetSamplingStepSize(float fSamplingStepSize){
	if (fSamplingStepSize > 0 && fSamplingStepSize <= 0.5)
		m_fStepSize = fSamplingStepSize;
}

float VfaVirtualTourAngularVelocityConfig::GetMaxAngularVelocity(){
	return m_fMaxAngularVelocity;
}

float VfaVirtualTourAngularVelocityConfig::GetSamplingStepSize(){
	return m_fStepSize;
}

/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

