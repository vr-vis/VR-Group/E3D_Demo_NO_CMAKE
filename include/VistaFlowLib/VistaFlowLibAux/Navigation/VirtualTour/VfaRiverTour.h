/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFARiverTour_H
#define _VFARiverTour_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>
#include <map>

// ViSTA stuff
#include <VistaAspects/VistaObserveable.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaMath/VistaEaseCurve.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLib/Data/VflObserver.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaFlowLibAux/Interaction/VfaApplicationContextObject.h>


//Vitual Tour
#include "VfaVirtualTour.h"
#include "VfaPath.h"
#include "Segments/VfaSegment.h"
#include "Views/VfaViewStressBox.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Class to create a river tour, this is basically a VirtualTour so for more information 
 * about the usage with pathes and the different flags see VfaVirtualTour.h
 *
 * The difference between a regular virtual tour is, that the camera does not
 * follow the exact position path, so the user is able to move away from the path
 * (inside the Radius which you can set static or dynamical for the tour) If the
 * user moves away from the Tour the speed decreases, how strong this effect is
 * could be controlled by SetVelocityFactorAtBorder() and SetRadialEaseCurveType()
 * As well if the user stays for a given time at the border he will break free and the 
 * tour will pause. While the user is as the border a stress box/sphere will be displayed.
 *
 * To get an example and more information about the usage, look at the 
 * CameraNavigation Prototype at VflTutorials/VirtualTourTutorial
 * 
 */
class VISTAFLOWLIBAUXAPI VfaRiverTour :	public VfaVirtualTour
{

public:

	enum EStressVisType{
		VFA_STRESS_BOX = 0,
		VFA_STRESS_SPHERE,
		VFA_VIEW_NONE
	};

	VfaRiverTour(VflRenderNode *pRenderNode, 
					VistaVirtualPlatform *pPlatform);
	//this constructor takes an existing VfaVirtualTour and makes a RiverTour out of it.
	//CAUTION: You mustn't delete the original Tour, because the paths are still the same objects! So changes to one will be to the other as well,
	//that might be a nice feature you just have to be aware of it!!!!
	VfaRiverTour(VfaVirtualTour *pTour);
	virtual ~VfaRiverTour();



	/**
	* Start:	Start the tour from the intial position, if the tour was already started, another call for this function will be ignored.
	* Restart:	Same as Start Function, but it restarts the tour from the initial position no matter if it was already started or not.
	* Pause:	If the tour was already started this function will pause it and keep the same start. (Calling start or restart after this 
	*			will inialize from the begining) the proper call should be continue
	* Continue: If the tour was started and paused this call can continue the tour from the paused state, or simply starts the tour 
	*			if it hasn't been started yet
	* Stop:		stops the Tour completely for example the inertia factor won't be concidered anymore (which is done in pause)
	*/
	virtual void Start();
	virtual void Pause();
	virtual void Continue(float fDirection=1.0f);//if you want the Tour to run backward just go for fDirection=-1.0
	virtual void Restart();
	virtual void Stop();





	void ObserverUpdate( IVistaObserveable* pObserveable, int nMsg, int nTicket );

	//returns the last used velocity of this tour to smoothly interpolate(for internal use!)
	VistaVector3D GetLastVelocity();

	//this time defines when the tour is stopped if the user moves for fTime seconds to 95% or more of the radius.
	void SetTimeAtBorderBeforeFree(float fTime);
	float GetTimeAtBorderBeforeFree();

	//this sets the radius in which the user is allowed to move along the tour either per station, parameter or for the whole tour.
	//in between the radius is interpolated, if you just want to set one constant radius use  SetRadius(float fRadius), this radius is
	//treated as the radius for parameter 0.0, so don't be confused if you want to extend this later on
	//if the radius for that station/parameter aready exists it's overwritten!!
	void SetRadiusByParameter(float fParameter, float fRadius);
	void SetRadiusByStation(int iStationNr, float fRadius);
	void SetRadius(float fRadius);

	//this methods simply deletes the radius for that station/parameter, if this was the last one, the default radius is used.
	//but be careful if you delete by parameter, because floatingpoint accuracy isn't yet taken into account
	void EraseRadiusByStation(int iStationNr);
	void EraseRadiusByParameter(float fParameter);

	// this inertia factor means how many seconds it will take to adapt to a new velocity
	float GetInertiaFactor();
	void SetInertiaFactor(float fInertia);

	//this method changes the Pause position of the Tour, this is needed if you want to restart a Tour from another 
	//parameter than the beginning, if you for example set the Pause Point with this method to 0.5 and call continue
	//the virtual platform is flewn to the asociated position and orientation and continues from there on
	void AdjustPause(float fParam);

	// if you set this, the stress box is displayed at the hand target, otherwise the stress box ist just oriented at the virtual
	// platform what is the default configuration (TODO: test!!!)
	void SetStressBoxAtHand(VfaApplicationContextObject* pAppContext);
	// this mehtod is just needed if you had set an applicationcontextobject to have the stress box at the hand and now want to undo
	// that, because otherwise this is already set as default.
	void SetStressBoxAtPlatform();

	//this can either set the visualization if you are at the border of the possible radius as Box (VFA_STRESS_BOX)
	//or as sphere (VFA_STRESS_SPHERE)
	void SetSressVisualizationType(EStressVisType iStressVisType);

	//this methods set/get the factor of the velocity if you are at the border of our radius. If for example it is set to 0.5 this means, that you're half
	//as fast if you're at the border. however, you can't get any slower until you break free of the Tour
	//@param fFactor only values between 0 and 1 are allowed, otherwise they are set to 0 or 1.
	void SetVelocityFactorAtBorder(float fFactor);
	float GeVelocityFactorAtBorder();

	//with this function you can set the easecurve which determines the reduction of velocity if you move outside of the path/river
	//per default this is set to linear, that means the velocity is linearly reduced by the radius if you move outside. Other possibilities are
	//for example VistaEaseCurve::QUADRATIC_IN_OUT or VistaEaseCurve::CUBIC_IN_OUT
	bool SetRadialEaseCurveType(VistaEaseCurve::eEaseCurveType eType);

	

	
protected:

	/**
	* This function gets called regularly automtically, because it implements part of the Renderable interface
	* It computes and sets the new position and orientation.
	*/
	void Update();

	//we can't give our method a oreintation as in the VirtualTour, because the bringingBackTour is just bringing us back in the radius so the corresponding point on the orientation path is needed
	void CreateBringingBackTour(VistaVector3D v3EndPosition, VistaVector3D v3LookingAtPoint);

	VistaVector3D		ComputeInertialVelocity(VistaVector3D v3TargetVelocity, float deltaT);


	float				GetCurrentMaxSpringDistance();

	VistaVector3D		VectorProjection(VistaVector3D ofVector, VistaVector3D onVector);

	void SetNewStressVisualization(VfaViewStressBox* pVis);

	bool				m_bStopped;

	float				m_fVeloInertia; // that means how many seconds it will take to adapt to the new velocity

	VistaVector3D		m_v3PausedLookingAt;

	VistaVector3D		m_v3LastPosition;
	VistaVector3D		m_v3LastAnchorPosition;
	VistaVector3D		m_v3LastVelocity;
	std::map<float, float>	m_mapRadiusValues;//saved as pair <parameter, radius>
	float				m_fDampingConstant;

	float				m_fTimeAtBorder;//this stores the time that we have been right next to the radius
	float				m_fTimeBeforeFree;//if we're staying at the border of the radius for this time, we get freed(the tour pauses)

	VistaEaseCurve		m_RadialVelocityEasecurve;
	float				m_fMinVelocityFactor; // if this is 0.5 that means that at the borders of the river it is 0.5 times the velocity as in the middle

	VfaViewStressBox*	m_pStressVis;

	EStressVisType		m_iStressVisType;





};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _VFARiverTour_H */
