/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //

#include "VfaVTPathConstructor.h"
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaVirtualTour.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaPath.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/Segments/VfaCatmullRomSegment.h>
#include <VistaBase/VistaVectorMath.h>

#include <cstdio>
using namespace std;

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
VfaVTPathConstructor::VfaVTPathConstructor(VfaVirtualTour* pVT)
	:m_pVirtualTour(pVT)
{
	m_vecPostionStation.clear();
	m_vecLookingPoints.clear();
	m_vecStationPauseTime.clear();
}

VfaVTPathConstructor::~VfaVTPathConstructor()
{

}

int VfaVTPathConstructor::AddStation(VistaVector3D v3Position, VistaVector3D v3LookingPoint, float fPauseTime){
	m_vecPostionStation.push_back(v3Position);
	m_vecLookingPoints.push_back(v3LookingPoint);
	m_vecStationPauseTime.push_back(fPauseTime);

	return ((int)m_vecPostionStation.size()) - 1;

}

int VfaVTPathConstructor::GetNumberOfStations(){
	return (int)m_vecPostionStation.size();
}

bool  VfaVTPathConstructor::RemoveStation(int iIndex){
	if (iIndex < GetNumberOfStations()){
		m_vecPostionStation.erase(m_vecPostionStation.begin()+iIndex);
		m_vecLookingPoints.erase(m_vecLookingPoints.begin()+iIndex);
		m_vecStationPauseTime.erase(m_vecStationPauseTime.begin()+iIndex);
		return true;
	}
	else
		return false;
}

VistaVector3D  VfaVTPathConstructor::GetPostionForStation(int iIndex){
	if (iIndex < GetNumberOfStations())
		return m_vecPostionStation[iIndex];
	else
		return VistaVector3D(0,0,0);
}

VistaVector3D  VfaVTPathConstructor::GetlookingPointForStation(int iIndex){
	if (iIndex < GetNumberOfStations())
		return m_vecLookingPoints[iIndex];
	else
		return VistaVector3D(0,0,0);
}

float VfaVTPathConstructor::GetPauseTimeForStation(int iIndex){
	if (iIndex < GetNumberOfStations())
		return m_vecStationPauseTime[iIndex];
	else
		return 0.0f;
}
// ========================================================================== //
// === End of File
// ========================================================================== //
