/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //

#include "VfaVTCatmullRomPathConstructor.h"
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaVirtualTour.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaPath.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/Segments/VfaCatmullRomSegment.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaBase/VistaStreamUtils.h>

#include <cstdio>
using namespace std;

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
VfaVTCatmullRomPathConstructor::VfaVTCatmullRomPathConstructor(VfaVirtualTour* pVT)
	:VfaVTPathConstructor(pVT)
{
}

VfaVTCatmullRomPathConstructor::~VfaVTCatmullRomPathConstructor()
{

}

bool VfaVTCatmullRomPathConstructor::ConstructPath(){

	int iStationCount = GetNumberOfStations();

	if (iStationCount < 2){
		//vstr::
		vstr::errp() << "[VfaVTCatmullRomPathConstructor]: can only construct a path with at least 2 stations!";
		return false;
	}

	VfaCatmullRomSegment* pPositionCatmullRomSegment = new VfaCatmullRomSegment();
	VfaCatmullRomSegment* pOrientationCatmullRomSegment = new VfaCatmullRomSegment();

	VistaVector3D v3PointO = (m_vecPostionStation[0] - m_vecPostionStation[1]) * 2 + m_vecPostionStation[0];
	VistaVector3D v3PointP = (m_vecLookingPoints[0] - m_vecLookingPoints[1]) * 2 + m_vecLookingPoints[0];

	pPositionCatmullRomSegment->AddStation(v3PointO);
	pOrientationCatmullRomSegment->AddStation(v3PointP);
	
	for (int i = 0; i < m_vecPostionStation.size(); i++){
		pPositionCatmullRomSegment->AddStation(m_vecPostionStation[i], m_vecStationPauseTime[i]);
		pOrientationCatmullRomSegment->AddStation(m_vecLookingPoints[i]);
	}


	v3PointO = (m_vecPostionStation[iStationCount-1] - m_vecPostionStation[iStationCount-2]) * 2 + m_vecPostionStation[iStationCount-1];
	v3PointP = (m_vecLookingPoints[iStationCount-1] - m_vecLookingPoints[iStationCount-2]) * 2 + m_vecLookingPoints[iStationCount-1];

	pPositionCatmullRomSegment->AddStation(v3PointO);
	pOrientationCatmullRomSegment->AddStation(v3PointP);

	VfaPath* pVTPostionPath = new VfaPath();
	pVTPostionPath->AddSegment(pPositionCatmullRomSegment);
	VfaPath* pVTOrientatrionPath = new VfaPath();
	pVTOrientatrionPath->AddSegment(pOrientationCatmullRomSegment);

	m_pVirtualTour->SetPositionPath(pVTPostionPath);
	m_pVirtualTour->SetOrientationPath(pVTOrientatrionPath);

	return true;

}

// ========================================================================== //
// === End of File
// ========================================================================== //
