/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFAVTPATHCONSTRUCTOR_H
#define _VFAVTPATHCONSTRUCTOR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../../VistaFlowLibAuxConfig.h"
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaVirtualTour.h>
#include <VistaBase/VistaVectorMath.h>

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*
* The path constructor is used to create a tour as a whole and not single paths
* for the oreintation and the position, this way you just have to give for every station
* a position and a looking direction and everything else is created automatically
 */
class VISTAFLOWLIBAUXAPI  VfaVTPathConstructor
{
public:

	VfaVTPathConstructor(VfaVirtualTour* pVT);
	~VfaVTPathConstructor();

	//Adds a station to the end of the constructed path with a mposition, a looking direction and 
	//optional a pause time at that point. It returns the index of the added station
	int AddStation(VistaVector3D v3Position, VistaVector3D v3LookingPoint, float fPauseTime = 0.0f);

	//removes a station with a give index, indices start from 0. But you have to be careful, because
	//removing a station changes the indices of all following stations by -1.
	bool RemoveStation(int iIndex);

	//these functions get information for a single station, if the index is out of bounds 0.0 or {0.0,0.0,0.0} is returned.
	VistaVector3D GetPostionForStation(int iIndex);
	VistaVector3D GetlookingPointForStation(int iIndex);
	float GetPauseTimeForStation(int iIndex);

	//Constructs the final path, this method has to be implemented by the inherited class
	virtual bool ConstructPath() = 0;

	int GetNumberOfStations();

protected:

	VfaVirtualTour*					m_pVirtualTour;
	std::vector<VistaVector3D>		m_vecPostionStation;
	std::vector<VistaVector3D>		m_vecLookingPoints;
	std::vector<float>				m_vecStationPauseTime;

};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _VFAVTPATHCONSTRUCTOR_H */
