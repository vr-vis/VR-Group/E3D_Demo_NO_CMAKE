/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFAVTCATMULLROMPATHCONSTRUCTOR_H
#define _VFAVTCATMULLROMPATHCONSTRUCTOR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../../VistaFlowLibAuxConfig.h"
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaVirtualTour.h>
#include "VfaVTPathConstructor.h"
#include <VistaBase/VistaVectorMath.h>

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*
* This constructs a catmullrom path from the added stations, when ConstructPath() is called.
* See VfaVTPathConstructor for more information on how to add stations. THere are two more 
* pseudo station automatically added, so the segment starts from the first point completely in the
* direction of the second point.
 */
class VISTAFLOWLIBAUXAPI  VfaVTCatmullRomPathConstructor: public VfaVTPathConstructor
{
public:

	VfaVTCatmullRomPathConstructor(VfaVirtualTour* pVT);
	~VfaVTCatmullRomPathConstructor();

	bool ConstructPath();

};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _VFAVTCATMULLROMPATHCONSTRUCTOR_H */
