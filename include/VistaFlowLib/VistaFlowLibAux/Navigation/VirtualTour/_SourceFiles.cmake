

set( RelativeDir "./Navigation/VirtualTour" )
set( RelativeSourceGroup "Source Files\\Navigation\\VirtualTour" )
set( SubDirs Views Segments Configurator PathConstructors)

set( DirFiles
	VfaVirtualTour.h
	VfaVirtualTour.cpp
	VfaVirtualTourCreator.h
	VfaVirtualTourCreator.cpp
	VfaPath.h
	VfaPath.cpp
	VfaRiverTour.h
	VfaRiverTour.cpp
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()

