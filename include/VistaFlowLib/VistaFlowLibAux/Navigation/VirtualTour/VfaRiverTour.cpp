/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //


// ViSTA stuff
#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>
#include <VistaKernel/InteractionManager/VistaUserPlatform.h>
#include <VistaKernel/GraphicsManager/VistaTransformNode.h>
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>
#include <VistaKernel/DisplayManager/VistaDisplayManager.h>
#include <VistaKernel/DisplayManager/VistaDisplaySystem.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>
// FlowLibAux stuff
#include "VfaRiverTour.h"
#include "VfaVirtualTour.h"
#include "Segments/VfaLinearSegment.h"
#include "Segments/VfaPointSegment.h"
#include "Segments/VfaCircularSegment.h"
#include "Views/VfaVirtualTourViewer.h"
#include "Views/VfaViewStressSphere.h"
#include "Configurator/VfaVirtualTourAngularVelocityConfig.h"

#include <cstdio>

using namespace std;

const float DEFAULT_VELOCITY = 5;
const float DEFAULT_ANGULAR_VELOCITY = 0.5f;
const int FLOATINGTYPE = 1;
const float DEFAULT_RADIUS = 4.0f;

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //

VfaRiverTour::VfaRiverTour(VflRenderNode *pRenderNode, VistaVirtualPlatform *pPlatform)
	: VfaVirtualTour(pRenderNode, pPlatform)
	, m_bStopped(true)
	, m_fMinVelocityFactor(0.5f)
	, m_fVeloInertia(1.0f)
	, m_fTimeAtBorder(0.0f)
	, m_fTimeBeforeFree(4.0f)
	, m_iStressVisType(VFA_STRESS_SPHERE)
{
	m_v3LastPosition = pPlatform->GetTranslation();
	m_RadialVelocityEasecurve = VistaEaseCurve();
	m_RadialVelocityEasecurve.SetType(VistaEaseCurve::LINEAR);
	m_v3LastVelocity = VistaVector3D(0,0,0);
	m_mapRadiusValues.clear();


	m_pStressVis = new VfaViewStressSphere(m_pPlatform);
	m_pRenderNode->AddRenderable(m_pStressVis);


}


VfaRiverTour::VfaRiverTour(VfaVirtualTour *pTour)
	: VfaVirtualTour(pTour->GetTourRenderNode(), pTour->GetTourVirtualPlatform() )
	, m_bStopped(true)	
	, m_fMinVelocityFactor(0.5f)
	, m_fVeloInertia(0.5f)
	, m_fTimeAtBorder(0.0f)
	, m_fTimeBeforeFree(4.0f)
	, m_iStressVisType(VFA_STRESS_SPHERE)
{
	m_v3LastPosition = m_pPlatform->GetTranslation();
	m_RadialVelocityEasecurve = VistaEaseCurve();
	m_RadialVelocityEasecurve.SetType(VistaEaseCurve::LINEAR);
	m_v3LastVelocity = VistaVector3D(0,0,0);

	//copy the important values
	m_pPositionPath = pTour->GetPositionPath();
	m_pOrientationPath = pTour->GetOrientationPath();
	m_bCAVEMode = pTour->GetCAVEMode();
	m_bGapFilling = pTour->GetGapFilling();
	m_bNoRotationMode = pTour->GetNoRotationMode();
	m_bSynchronousMode = pTour->GetSynchronousMode();
	m_fDirectionFactor = pTour->GetDirectionFactor();
	m_mapRadiusValues.clear();

	//otherwise the update method of pTour might be called!
	m_pRenderNode->RemoveRenderable(pTour);

	m_pStressVis = new VfaViewStressSphere(m_pPlatform);
	m_pRenderNode->AddRenderable(m_pStressVis);

	

}

VfaRiverTour::~VfaRiverTour()
{
	
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/* ==========================================================================*/
/* NAME: AnyFunction()					 									 */
/* ==========================================================================*/


/**
* Start: Start the tour from the initial position, if the tour was already started, another call for this function will be ignored.
* Restart: Same as Start Function, but it restarts the tour from the initial position no matter if it was already started or not.
* Pause: If the tour was already started this function will pause it and keep the same start. (Calling start or restart after this will inialize from the begining)
*		the proper call should be continue
* Continue: If the tour was started and pause this call can continue the tour from the paused state
* Stop: The tour is stopped so is any influence the tour may have on the observer
*/
void VfaRiverTour::Start()
{

	if (m_bIsInFlight)
		return;

	if (!m_pRenderNode)
		return;

	if (!m_bInitialized)
		Init();

	if(m_pPositionPath->GetNumberOfSegments() ==0 || (m_pOrientationPath->GetNumberOfSegments()==0 && !m_bNoRotationMode)){
		vstr::warnp()<<"[VfaRiverTour] Not enough segments in the paths! Can't start."<<std::endl;
		vstr::warni()<<"               Number of position segments: "<<m_pPositionPath->GetNumberOfSegments()<<std::endl;
		vstr::warni()<<"               Number of orientation segments: "<<m_pOrientationPath->GetNumberOfSegments()<<" with NoRotationMode: "<<m_bNoRotationMode<<std::endl;
		return;
	}

	
	VistaVector3D v3Ori = m_v3DefaultLookingDirection; //(0.0, 0.0, -1.0);
	v3Ori = m_pPlatform->GetRotation().Rotate(v3Ori);
	v3Ori.Normalize();

	//first time we start the Tour?
	if(m_dLastTimeStep==0.0){
		if(m_fDirectionFactor>0.0f)
			m_fParm = 0.0f;
		else
			m_fParm=1.0f;
	}

	VistaVector3D v3StartOri, v3StartPos;
	

	v3StartPos = m_pPositionPath->Interpolate(m_fParm);
	if(!m_bNoRotationMode)
		v3StartOri = m_pOrientationPath->Interpolate(m_fParm) - v3StartPos;
	else 
		v3StartOri = VistaVector3D(0.0, 0.0, -1.0);
	
	v3StartOri.Normalize();

	//if we are not at the startposition, we have to get there:
	if((m_pPlatform->GetTranslation()-v3StartPos).GetLength()>GetCurrentMaxSpringDistance() || (!m_bNoRotationMode && !Vector3DEquals(v3Ori, v3StartOri)))
	{
		if((m_pPlatform->GetTranslation()-v3StartPos).GetLength()>GetCurrentMaxSpringDistance())
		{
			vstr::debugi()<<"[VfaRiverTour] Positions is too far away! "
						  <<m_pPlatform->GetTranslation()<<" != "<<v3StartPos<<"  Distance: "<<(m_pPlatform->GetTranslation()-v3StartPos).GetLength()<<std::endl;
		}
		if(!Vector3DEquals(v3Ori, v3StartOri))
		{
			vstr::debugi()<<"[VfaRiverTour] Orientations do not match! "
						  <<v3Ori<<" != "<<v3StartOri<<std::endl;
		}
		vstr::outi()<<"[VfaRiverTour] Creating new bringing back Tour for Start()"<<std::endl;
		
		CreateBringingBackTour(v3StartPos, m_pOrientationPath->Interpolate(m_fParm));
		return;
	}
	

	//Call Check and Prepare for each path to check the segments inside and prepare for interpolation
	if (!m_pPositionPath->CheckAndPrepare(true) || (!m_pOrientationPath->CheckAndPrepare(true) && (!m_bNoRotationMode ))){

		vstr::errp() << "[VfaRiverTour] Failure in preparing and checking the paths" << endl;
		
		Notify(MSG_TOUR_INIT_FAILURE);

		return;
	}
	else{

		Notify(MSG_TOUR_END_PREPARATION);

		//Init parm and get the proper velocity
		m_dLastTimeStep = m_pRenderNode->GetVisTiming()->GetCurrentClock();

		m_v3LastAnchorPosition = m_pPositionPath->Interpolate(m_fParm);
		m_v3LastPosition = m_pPlatform->GetTranslation();

		vstr::outi() << "[VfaRiverTour] Starting River Tour ... " << endl;

		//Start actual movement
		Notify(MSG_TOUR_STARTED);

		m_bIsInFlight = true;
		m_bStopped=false;

	}

}

void VfaRiverTour::Update()
{

	if(m_bStopped)
		return;

	double dNewTS = GetRenderNode()->GetVisTiming()->GetCurrentClock();
	const float deltaT = (const float)(dNewTS - m_dLastTimeStep);
	m_dLastTimeStep = dNewTS;

	

	//So the Tour is running, but we reached a "Pause at end" point of a segment
	if(m_bIsPaused){
		m_fRestPauseTime-=(float)deltaT;
		if(m_fRestPauseTime<=0.0f)
			m_bIsPaused=false;
		Notify(MSG_TOUR_PAUSED);
	}
	else if(m_bIsInFlight){
		VfaVirtualTour::UpdateParameter(deltaT);
	}

	//because our tour can run in both directions
	if (m_fParm > 1.0f){

		m_fParm=1.0f;
		m_bIsInFlight = false;
		m_dLastTimeStep = 0.0;
		m_bInitialized=false;
		m_bStopped=true;

	}

	if(m_fParm<0.0f) {
		m_fParm = 0.0f;
		m_bIsInFlight = false;
		m_dLastTimeStep = 0.0;
		m_bInitialized=false;
		m_bStopped=true;

	}

	float fPauseTimePos=0.0, fPauseTimeOri=0.0;
	VistaVector3D v3lookingAt;

	VistaVector3D v3AnchorPosition = m_pPositionPath->Interpolate(m_fParm);
	VistaVector3D v3AnchorSpeed = (v3AnchorPosition -m_v3LastAnchorPosition) / deltaT;
	VistaVector3D v3Position = m_pPlatform->GetTranslation();
	VistaVector3D v3Velocity = (v3Position - m_v3LastPosition) / deltaT;
	VistaVector3D v3Distance = m_v3LastAnchorPosition - m_v3LastPosition;
	VistaVector3D v3TargetVelocity;

	VistaVector3D v3NewPosition;

	if(!m_bIsInFlight){
		v3NewPosition = m_v3LastAnchorPosition - v3Distance + ComputeInertialVelocity(VistaVector3D(0,0,0)+v3Velocity, deltaT)*deltaT;
		//vstr::debug()<<"speed: "<<m_v3LastVelocity.GetLength()<<std::endl;
	}
	else {


	switch(FLOATINGTYPE){
	case 1: {//floating
		if(!m_bIsInFlight)
			return;
		if(v3Distance.GetLength() > GetCurrentMaxSpringDistance()){
			//vstr::errp()<<"VfaRiverTour::Update(), not inside my maxDistance bubble!"<<std::endl;
			v3Distance = v3Distance *GetCurrentMaxSpringDistance()/v3Distance.GetLength();
		}

		float fBringingBackFactor = 1.0f; // this is the strength with which the user gets sucked back in the middle of the stream (onto the path).
		VistaVector3D v3UserSpeedFront = VectorProjection(v3Velocity, v3AnchorSpeed);
		VistaVector3D v3UserSpeedSide = v3Velocity - v3UserSpeedFront;
		VistaVector3D v3DistanceFront = VectorProjection(v3Distance, v3AnchorSpeed);
		VistaVector3D v3DistanceSide = v3Distance - v3DistanceFront;
		v3UserSpeedSide = v3UserSpeedSide*(GetCurrentMaxSpringDistance()-v3DistanceSide.GetLength())/GetCurrentMaxSpringDistance();// the speed out of the river gets smaller the farer outside you are
		if(v3UserSpeedSide.GetLength()==0.0){ // now we pull the user back in the middle of the stream
			v3UserSpeedSide= v3Distance*(1-(GetCurrentMaxSpringDistance()-v3DistanceSide.GetLength())/GetCurrentMaxSpringDistance())*fBringingBackFactor;
			m_fTimeAtBorder=0.0f;
			m_pStressVis->SetVisible(false);
		}
		else if(v3Distance.GetLength()>=0.95*GetCurrentMaxSpringDistance() && v3Velocity.GetLength()!=0.0f){

			m_fTimeAtBorder+=deltaT;

			m_pStressVis->SetPercentage(m_fTimeAtBorder/m_fTimeBeforeFree);
			m_pStressVis->SetVisible(true);
		
			//vstr::debugi()<<"At border for: "<<m_fTimeAtBorder<<std::endl;
			if(m_fTimeAtBorder>m_fTimeBeforeFree) {
				Stop();
				m_fTimeAtBorder=0.0f;
				m_pStressVis->SetVisible(false);
				m_pStressVis->SetPercentage(0.0f);
			}
		}




		//so we compute how fast the river is that far outside the middle
		float fSpeedratio =(1.0f-m_fMinVelocityFactor)* (float)m_RadialVelocityEasecurve.GetValue((GetCurrentMaxSpringDistance()-v3DistanceSide.GetLength())/GetCurrentMaxSpringDistance()) + m_fMinVelocityFactor;
		//this part changes the SpeedRation if the users velocity is in the directionj of the anchor. The user swims with or against the stream.
		if(v3Velocity.GetLength()>0.0)
			fSpeedratio += v3AnchorSpeed.Dot(v3UserSpeedFront)/v3AnchorSpeed.GetLength()/v3Velocity.GetLength() *m_fMinVelocityFactor/2;
		UpdateParameter(-deltaT*(1-fSpeedratio));// current anchor position with new parameter
					
		//We build our new position of the old position...
		v3NewPosition = m_v3LastAnchorPosition;
		v3NewPosition-=  v3Distance;
		//... and the velocity to the front(anchor speed) and side, that we have computed
		v3NewPosition+= ComputeInertialVelocity(v3UserSpeedSide+v3AnchorSpeed*fSpeedratio+v3DistanceFront, deltaT)*deltaT;

		//vstr::debug()<<"speed: "<<m_v3LastVelocity.GetLength()<<std::endl;

		break;
	}
	case 2: {//spring

		float fBringingBackFactor = 0.1f;
		v3Velocity = v3Velocity*(GetCurrentMaxSpringDistance()-v3Distance.GetLength())/GetCurrentMaxSpringDistance();//the user speed decreases the farther he is outside of the middle
		if(v3Velocity.GetLength()==0.0){ // now we pull the user back in the middle of the stream
			v3Velocity= v3Distance*(1-(GetCurrentMaxSpringDistance()-v3Distance.GetLength())/GetCurrentMaxSpringDistance())*fBringingBackFactor;
		}

		v3NewPosition = m_v3LastAnchorPosition;
		v3NewPosition-=  v3Distance;
		//v3NewPosition+= 0.5 *( fSpringConstant *v3DistanceFront) * deltaT*deltaT;  
		//v3NewPosition+= (v3UserSpeedFront+v3UserSpeedSide)*deltaT;
		v3NewPosition+= (v3Velocity+v3AnchorSpeed)*deltaT;
		break;
	}
	}
	}

	
	v3NewPosition += m_v3UserOffset;
	m_pPlatform->SetTranslation(v3NewPosition);
	m_v3LastPosition = v3NewPosition;
	m_v3LastAnchorPosition =  m_pPositionPath->Interpolate(m_fParm, fPauseTimePos);


	if( !m_bNoRotationMode) {
		if(m_bSynchronousMode )
		{
			int iSegNr=m_pPositionPath->GetCurrentSegmentNumber();
			float fLocalParm = (m_fParm - m_pPositionPath->GetParmCurrentSegStartedWith()) / m_pPositionPath->GetSegment(iSegNr)->GetSegmentLength() * m_pPositionPath->GetPathLength();
			//this is computed incorrectly if m_fParm==1.0f, therefor
			if(m_fParm==1.0) {
				fLocalParm=1.0;
			}
			v3lookingAt = m_pOrientationPath->InterpolateSynchronous(iSegNr, fLocalParm, fPauseTimeOri);
		}
		else
		{
			v3lookingAt = m_pOrientationPath->Interpolate(m_fParm, fPauseTimePos);
		}

		if(fPauseTimePos!=0.0f || fPauseTimeOri!=0.0f){
			if(fPauseTimePos==-1.0f || fPauseTimeOri==-1.0f) {
				Pause();
			}
			else{
				if(fPauseTimePos!=0.0f)
					SetPausedFor(fPauseTimePos);
				else
					SetPausedFor(fPauseTimeOri);
			}
		}
		SetPosition(v3NewPosition, v3lookingAt);
	}
	VistaVector3D v3Ori = m_v3DefaultLookingDirection;//(0.0, 0.0, -1.0);
	v3Ori = m_pPlatform->GetRotation().Rotate(v3Ori);

	//vstr::debugi()<<"Pos: "<<v3NewPosition<<"  Ori: "<<v3Ori<<std::endl;
	//vstr::debugi()<<"Pos: "<<v3NewPosition<<"  Distance: "<<v3Distance.GetLength()<<std::endl;
	//vstr::debugi()<<"Pos: "<<v3NewPosition<<"  max Distance: "<<GetCurrentMaxSpringDistance()<<std::endl;
	//vstr::debugi()<<"Pos: "<<v3NewPosition<<" looking at: "<<v3lookingAt<<std::endl;
	//vstr::debugi()<<"UserOffset: "<<m_v3UserOffset<<std::endl;

	if(m_fParm==1.0f || m_fParm==0.0f)
		Notify(MSG_TOUR_ENDED);
	else
		Notify(MSG_TOUR_NEWSTEP);

}

void VfaRiverTour::Pause()
{

	vstr::debugi()<<"[VfaRiverTour] Tour paused."<<std::endl;
	m_bIsInFlight = false;

	if(m_bGettingBroughtBack) {
		m_pBringingBackTour->Pause();
		m_bGettingBroughtBack=false;
		return;
	}


	//Save the current state, to see whether we moved away if we continue
	m_v3PausedPosition=m_pPositionPath->Interpolate(m_fParm);
	if(m_bSynchronousMode)
	{
		int iSegNr=m_pPositionPath->GetCurrentSegmentNumber();
		float fLocalParm = (m_fParm - m_pPositionPath->GetParmCurrentSegStartedWith()) / m_pPositionPath->GetSegment(iSegNr)->GetSegmentLength() * m_pPositionPath->GetPathLength();
		//this is computed incorrectly if m_fParm==1.0f, therefor
		if(m_fParm==1.0) {
			fLocalParm=1.0;
		}
		float fNotUsedVariable;
		m_v3PausedLookingAt = m_pOrientationPath->InterpolateSynchronous(iSegNr, fLocalParm, fNotUsedVariable);
	}
	else {
		m_v3PausedLookingAt = m_pOrientationPath->Interpolate(m_fParm);
	}

}

void VfaRiverTour::Continue(float fDirection)
{
	m_fDirectionFactor = fDirection;

	if(m_pPositionPath->GetNumberOfSegments() ==0 || (m_pOrientationPath->GetNumberOfSegments()==0 && !m_bNoRotationMode)){
		vstr::warnp()<<"[VfaRiverTour] Not enough segments in the paths! Can't continue."<<std::endl;
		return;
	}

	if (!m_bIsInFlight && !m_bGettingBroughtBack)
	{
		if (m_dLastTimeStep!=0.0){ //this checked if the tour was started already.
			VistaVector3D v3Ori = m_v3DefaultLookingDirection;//(0.0, 0.0, -1.0);
			v3Ori = m_pPlatform->GetRotation().Rotate(v3Ori);

			//what orientation should we have at this point
			VistaVector3D v3NeededOri = (m_v3PausedLookingAt - m_pPlatform->GetTranslation());
			v3NeededOri.Normalize();
			


			//if we are still where we paused we can just continue:
			if((m_pPlatform->GetTranslation()- m_v3PausedPosition).GetLength() < GetCurrentMaxSpringDistance()
				&& (Vector3DEquals(v3Ori, v3NeededOri) || m_bNoRotationMode))
			{
				m_dLastTimeStep = m_pRenderNode->GetVisTiming()->GetCurrentClock();
				m_v3LastPosition = m_pPlatform->GetTranslation();
				m_v3LastAnchorPosition = m_pPositionPath->Interpolate(m_fParm);
				m_bIsInFlight = true;
				m_bStopped=false;
			}

			//otherwise we have to create a new VfaRiverTour to bring us back, that we observe
			else
			{
				vstr::debugi()<<"[VfaRiverTour] Creating new bringing back Tour for Continue(), because..."<<std::endl;
				if((m_pPlatform->GetTranslation()- m_v3PausedPosition).GetLength() > GetCurrentMaxSpringDistance())
					vstr::debugi()<<"                     ... Position are too far away: "<<m_pPlatform->GetTranslation()<<" != "<<m_v3PausedPosition<<" Distance: "<<(m_pPlatform->GetTranslation()- m_v3PausedPosition).GetLength()<<std::endl;
				else if(!Vector3DEquals(v3Ori, v3NeededOri))
					vstr::debugi()<<"                     ... Orientations are different: "<<v3Ori<<" != "<<v3NeededOri<<std::endl;

				CreateBringingBackTour(m_v3PausedPosition, m_v3PausedLookingAt);
			}
		}
		else
			Start();
	}
}

void VfaRiverTour::Restart()
{

	if(m_bGettingBroughtBack) {
		m_pBringingBackTour->Pause();
		m_bGettingBroughtBack=false;
	}

	m_bIsInFlight = false;
	if(m_fDirectionFactor>0.0f)
		m_fParm = 0.0f;
	else
		m_fParm=1.0f;
	AdjustPause(m_fParm);

	Start();
}

void VfaRiverTour::Stop()
{
	Pause();
	m_bStopped=true;
	vstr::debugi()<<"[VfaRiverTour] River Tour stopped!"<<std::endl;
}

void VfaRiverTour::ObserverUpdate( IVistaObserveable* pObserveable, int nMsg, int nTicket )
{
	//If the bringing back Tour (or tour bringing us in the radius of our Tour and the correct orientation)
	//ended we want our tour to continue or start
	VfaRiverTour* pBringingBackTour = static_cast<VfaRiverTour*>(pObserveable);
	if(pBringingBackTour == m_pBringingBackTour)
	{
		if(nMsg==MSG_TOUR_ENDED)
		{

			vstr::debugi()<<"[VfaRiverTour] BringingBackTour  ended."<<std::endl;

			m_bGettingBroughtBack=false;

			VistaVector3D v3CurrentOri = GetDefaultLookingDirection();//VistaVector3D(0.0, 0.0, -1.0);
			v3CurrentOri = m_pPlatform->GetRotation().Rotate(v3CurrentOri);
			v3CurrentOri.Normalize();

			//TODO: maybe we have to adjust the savedPausedPosition etc, so this will be satisfied
			m_v3LastPosition = m_pPlatform->GetTranslation();
			m_v3LastVelocity = pBringingBackTour->GetLastVelocity();

			Continue(m_fDirectionFactor);
		}
	}
}


void VfaRiverTour::CreateBringingBackTour(VistaVector3D v3EndPosition, VistaVector3D v3EndLookingAt)
{
	//our bringin back Tour should consist of up to two parts, which are actually 2 tours
	//first move the platform inside the radius from where the river can smoothly carry our platform on
	//then rotate to the right orientation

	VistaVector3D v3Ori = m_v3DefaultLookingDirection;//(0.0, 0.0, -1.0);
	v3Ori = m_pPlatform->GetRotation().Rotate(v3Ori);
	v3Ori.Normalize();

	VistaVector3D v3CurrentPos = m_pPlatform->GetTranslation();


	VistaVector3D v3EndOrientation= v3EndLookingAt - v3CurrentPos;
	v3EndOrientation.Normalize();

	//we create a new Tour or "recycle" the old one
	if(!m_pBringingBackTour) {
		m_pBringingBackTour = new VfaRiverTour(m_pRenderNode, m_pPlatform);
	}
	else{
		m_pBringingBackTour->Clear();
	}


	if ((v3EndPosition-v3CurrentPos).GetLength() < GetCurrentMaxSpringDistance()) {
		// we are already in our radius so we just have to rotate

		//we need the virtual length of this segment
		float fRotationTime = acos(v3Ori.Dot(v3EndOrientation))/DEFAULT_ANGULAR_VELOCITY;
		//we don't want to turn to fast!
		if(fRotationTime<3.0f) {
			fRotationTime+= (3.0f-fRotationTime)/4;
		}


		VfaPointSegment* pPointSeg = new VfaPointSegment( fRotationTime*GetVelocity(), v3CurrentPos); 
		m_pBringingBackTour->AddPositionSegment(pPointSeg);

		//we take factor 100 here, because we have a velocity inertia, so the translation might still have some momentum
		VfaCircularSegment *pCircSeg = new VfaCircularSegment(v3CurrentPos +100*v3Ori, v3EndLookingAt, v3CurrentPos);
		pCircSeg->SetFadeInLength(0.5f);
		pCircSeg->SetFadeOutLength(0.6f);
		m_pBringingBackTour->AddOrientationSegment(pCircSeg);

		m_pBringingBackTour->SetNoRotationMode(false);

		vstr::debugi()<<"BringingBack Tour created:  Rotation from "<<v3Ori<<" to "<<v3EndOrientation<<" at pos: "<<v3CurrentPos<<std::endl;
		Observe(m_pBringingBackTour, 1);

	}
	else{
		// we have to bring the viewer inside the radius of our Tour
		// as we choose noRotationMode we just need a position path

		VfaLinearSegment* pLinSeg = new VfaLinearSegment(v3CurrentPos, v3EndPosition);
		pLinSeg->SetFadeInLength(0.4f);
		m_pBringingBackTour->AddPositionSegment(pLinSeg);
		m_pBringingBackTour->SetNoRotationMode(true);

		m_pBringingBackTour->SetSynchronousMode(false);

		vstr::debugi()<<"BringingBack Tour created: from "<<v3CurrentPos<<" to "<<v3EndPosition<<std::endl;
		Observe(m_pBringingBackTour, 2);
	}
	


	m_pBringingBackTour->SetCAVEMode(m_bCAVEMode);
	m_pBringingBackTour->SetVelocity(GetVelocity());
	m_pBringingBackTour->SetGapFilling(false);
	dynamic_cast<VfaRiverTour*>(m_pBringingBackTour)->SetRadius(GetCurrentMaxSpringDistance());

	m_bGettingBroughtBack=true;
	m_bStopped=true;

	m_pBringingBackTour->Start();

}


float VfaRiverTour::GetCurrentMaxSpringDistance() {

	if(m_mapRadiusValues.empty())
		return DEFAULT_RADIUS;

	std::map<float, float>::iterator it = m_mapRadiusValues.begin();

	if(m_mapRadiusValues.size()==1)
		return (*it).second;

	while(it != m_mapRadiusValues.end()) {
		if((*it).first>=m_fParm)
			break;
		it++;
	}
	if(it!=m_mapRadiusValues.begin()) {
		float biggerParam = (*it).first;
		float biggerRadius = (*it).second;
		it--;
		float smallerParam = (*it).first;
		float smallerRadius = (*it).second;

		return (smallerRadius + (biggerRadius-smallerRadius)*(m_fParm-smallerParam)/(biggerParam-smallerParam));
	}
	else{
		return (*it).second;
	}

}
VistaVector3D VfaRiverTour::VectorProjection(VistaVector3D ofVector, VistaVector3D onVector){
	if(onVector.GetLength()==0.0)
		return VistaVector3D(0,0,0);
	VistaVector3D v3res = ofVector.Dot(onVector) / onVector.GetLengthSquared() * onVector;
	return v3res;
}

VistaVector3D VfaRiverTour::ComputeInertialVelocity(VistaVector3D v3TargetVelocity, float deltaT) {
//#define USE_NOINERTIA

#ifndef USE_NOINERTIA

	if(m_fVeloInertia<deltaT)
		m_v3LastVelocity =  v3TargetVelocity;
	else
		m_v3LastVelocity =  m_v3LastVelocity + (v3TargetVelocity-m_v3LastVelocity)/m_fVeloInertia*deltaT;
#else
	m_v3LastVelocity = v3TargetVelocity;
#endif

	return m_v3LastVelocity;
}

VistaVector3D VfaRiverTour::GetLastVelocity() {
	return m_v3LastVelocity;
}

void VfaRiverTour::SetTimeAtBorderBeforeFree(float fTime){
	m_fTimeBeforeFree = fTime;
}
float VfaRiverTour::GetTimeAtBorderBeforeFree(){
	return m_fTimeBeforeFree;
}
void VfaRiverTour::SetRadiusByParameter(float fParameter, float fRadius){
	m_mapRadiusValues[fParameter] = fRadius;
}
void VfaRiverTour::SetRadiusByStation(int iStationNr, float fRadius){
	m_mapRadiusValues[m_pPositionPath->GetAllStations().at(iStationNr)->fParameter] = fRadius;
}
void VfaRiverTour::SetRadius(float fRadius){
	m_mapRadiusValues[0.0] = fRadius;
}

void VfaRiverTour::EraseRadiusByStation(int iStationNr){
	EraseRadiusByParameter(m_pPositionPath->GetAllStations().at(iStationNr)->fParameter);
}
void VfaRiverTour::EraseRadiusByParameter(float fParameter){
	m_mapRadiusValues.erase(fParameter);
}

float VfaRiverTour::GetInertiaFactor() {
	return m_fVeloInertia;
}

void VfaRiverTour::SetInertiaFactor(float fInertia) {
	if(fInertia>=0)
		m_fVeloInertia = fInertia;
}

void VfaRiverTour::AdjustPause(float fParam){

	if(m_bIsInFlight)
		m_bIsInFlight=false;

	m_bIsPaused=true;
	m_bStopped = true;
	m_fParm = fParam;
	m_dLastTimeStep = m_pRenderNode->GetVisTiming()->GetCurrentClock(); //we have to set this so the tour knows we don't want to start from the beginning if it wasn't started yet

	float fPauseTime=0.0;
	VistaVector3D v3PauselookingAt;
	VistaVector3D v3PausePosition;

	if(m_bNoRotationMode){
		v3PausePosition = m_pPositionPath->Interpolate(m_fParm, fPauseTime);
		v3PauselookingAt = VistaVector3D(0.0,0.0,0.0);
	}
	else if(m_bSynchronousMode)
	{
		v3PausePosition = m_pPositionPath->Interpolate(fParam, fPauseTime);
		int iSegNr=m_pPositionPath->GetCurrentSegmentNumber();
		float fLocalParm = (m_fParm - m_pPositionPath->GetParmCurrentSegStartedWith()) / m_pPositionPath->GetSegment(iSegNr)->GetSegmentLength() * m_pPositionPath->GetPathLength();
		//this is computed incorrectly if m_fParm==1.0f, therefor
		if(m_fParm==1.0) {
			fLocalParm=1.0;
		}
		v3PauselookingAt = m_pOrientationPath->InterpolateSynchronous(iSegNr, fLocalParm, fPauseTime);
	}
	else
	{
		v3PauselookingAt = m_pOrientationPath->Interpolate(m_fParm, fPauseTime);
		v3PausePosition = m_pPositionPath->Interpolate(m_fParm, fPauseTime);
	}

	m_v3PausedPosition = v3PausePosition;


	m_v3PausedLookingAt = v3PauselookingAt;

}

	// if you set this, the stress box is displayed at the hand target, otherwise the stress box ist just oriented at the virtual
	// platform what is the default configuration
void VfaRiverTour::SetStressBoxAtHand(VfaApplicationContextObject* pAppContext){
	m_pStressVis->SetApplicationContext(pAppContext, m_pRenderNode);
}
	// this mehtod is just needed if you had set an applicationcontextobject to have the stress box at the hand and now want to undo
	// that, because otherwise this is already set as default.
void VfaRiverTour::SetStressBoxAtPlatform(){
	m_pStressVis->SetApplicationContext(NULL, m_pRenderNode);
}

void VfaRiverTour::SetSressVisualizationType(EStressVisType iStressVisType){
	m_iStressVisType = iStressVisType;

	switch(m_iStressVisType){
	case VFA_STRESS_BOX:
		SetNewStressVisualization( new VfaViewStressBox(m_pPlatform));
		break;
	case VFA_STRESS_SPHERE:
	default:
		SetNewStressVisualization( new VfaViewStressSphere(m_pPlatform));
		break;
	}
}
void VfaRiverTour::SetNewStressVisualization(VfaViewStressBox* pVis){
	if(m_pStressVis!=NULL)
	{
		m_pRenderNode->RemoveRenderable(m_pStressVis);
		delete m_pStressVis;
	}
	m_pStressVis = pVis;
	m_pRenderNode->AddRenderable(m_pStressVis);
}

void VfaRiverTour::SetVelocityFactorAtBorder(float fFactor){
	if(fFactor>1.0f)
		fFactor=1.0f;
	if(fFactor<0.0f)
		fFactor=0.0f;

	m_fMinVelocityFactor = fFactor;
}
float VfaRiverTour::GeVelocityFactorAtBorder(){
	return m_fMinVelocityFactor;
}

bool VfaRiverTour::SetRadialEaseCurveType(VistaEaseCurve::eEaseCurveType eType){
	if(eType<0 || eType> VistaEaseCurve::CIRC_OUT_IN)
		return false;

	m_RadialVelocityEasecurve.SetType(eType);
	return true;
}




// ========================================================================== //
// === End of File
// ========================================================================== //


