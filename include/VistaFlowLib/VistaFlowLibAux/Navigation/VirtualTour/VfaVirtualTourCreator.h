/*============================================================================*/
/*                              ViSTA VR toolkit                              */
/*               Copyright (c) 1997-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAVIRTUALTOURCREATOR_H
#define _VFAVIRTUALTOURCREATOR_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/


#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>

#include <VistaFlowLibAux/Navigation/VirtualTour/VfaVirtualTour.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/Views/VfaVirtualTourViewer.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/Segments/VfaCatmullRomSegment.h>

#include <VistaFlowLibAux/VfaBaseApplication.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>





/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/*
* This class helps to easily add or delete stations to your Tour 
*
* You can simply add stations to a new Tour or your existing Tour by using AddSegmentToTour and AddCurrentPositionToTour
* If you use an already existing Tour and want to add Station to it you can simply use AddExistingVirtualTour and start adding or removing
* stations at the end of it
*/


class VISTAFLOWLIBAUXAPI VfaVirtualTourCreator
{
public:
	enum ESegmentType{
		VT_CIRC,
		VT_CIRC_CENTER,
		VT_LINEAR,
		VT_CATMULLROM,
		VT_POINT
	};

	VfaVirtualTourCreator(VistaVirtualPlatform* pPlatform, VflRenderNode* pRenderNode);
	~VfaVirtualTourCreator();

	VfaVirtualTour* GetVirtualTour();
	VistaVirtualPlatform* GetVirtualPlatform();
	VfaVirtualTourViewer* GetVTViewer();

	//creates an empty Tour
	bool CreateEmptyTour();

	//This adds an virtualTour to to the creator so, it will use this Tour to build on from it, the two last stations are computed automatically
	bool AddExistingVirtualTour(VfaVirtualTour *pTour);

	//this adds a segemnt to the Tour, the new segment is between the last position and the current Position, unless we just set the circulation center
	bool AddSegmentToTour(VistaVector3D v3CurrentPosition, ESegmentType iType, bool bForPositionPath); //if not bForPositionPath it's for the orientation Path
	bool SetAsStartForNewPath(VistaVector3D v3CurrentPosition, bool bForPositionPath);

	//and the same functions, just taking the current position from the virtual Platform
	bool AddCurrentPositionAsSegmentToTour(ESegmentType iType, bool bForPositionPath); //if not bForPositionPath it's for the orientation Path
	bool SetCurrentPositionAsStartForNewPath(bool bForPositionPath);

	//deletes the last station, whether it is in a catmullrom segement or any other segment, if bForPositionPath is true the station is delted
	//from the position path and from the orientation path otherwise
	bool DeleteLastStation(bool bForPositionPath);


	bool IsSynchronousModePossible();
	void EnableSynchronousMode();

private:
	//this method cleans up before we create a new tour
	void DeleteTour();


	VfaVirtualTour			*m_pVirtualTour;
	VfaVirtualTourViewer	*m_pVTViewer;
	VistaVirtualPlatform	*m_pVirtualPlatform;
	VflRenderNode			*m_pRenderNode;

	bool					m_bTourCreated;

	VistaVector3D			m_v3LastPosition;
	VistaVector3D			m_v3BeforeLastPosition; //this one is just needed to smoothly add catmullrom segments
	VistaVector3D			m_v3LastOriStation;
	VistaVector3D			m_v3BeforeLastOriStation; //this one is just needed to smoothly add catmullrom segments
	VistaVector3D			m_v3CircCenter;

	VfaCatmullRomSegment	*m_pLastOriCatmullSegment;
	VfaCatmullRomSegment	*m_pLastPosCatmullSegment;

	float					m_fDefaultPointStayTime;


};

#endif //_VFAVIRTUALTOURCREATOR_H
