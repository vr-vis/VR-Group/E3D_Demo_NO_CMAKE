/*============================================================================*/
/*                              ViSTA VR toolkit                              */
/*               Copyright (c) 1997-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VfaVirtualTourCreator.h"

#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/GraphicsManager/VistaGraphicsManager.h>
#include <VistaKernel/GraphicsManager/VistaSceneGraph.h>
#include <VistaKernel/GraphicsManager/VistaTransformNode.h>
#include <VistaKernel/DisplayManager/VistaDisplayManager.h>
#include <VistaKernel/DisplayManager/VistaDisplaySystem.h>
#include <VistaKernel/InteractionManager/VistaKeyboardSystemControl.h>
#include <VistaKernel/InteractionManager/VistaUserPlatform.h>

// Virtual Tour stuff
#include <VistaFlowLibAux/Navigation/VirtualTour/VfaPath.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/Segments/VfaPointSegment.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/Segments/VfaLinearSegment.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/Views/VfaVirtualTourViewer.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/Segments/VfaCircularSegment.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/Configurator/VfaVirtualTourAngularVelocityConfig.h>
#include <VistaFlowLibAux/Navigation/VirtualTour/PathConstructors/VfaVTCatmullRomPathConstructor.h>

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaVirtualTourCreator::VfaVirtualTourCreator(VistaVirtualPlatform* pPlatform, VflRenderNode* pRenderNode):
	m_pVirtualPlatform(pPlatform),
	m_pRenderNode(pRenderNode),
	m_pVirtualTour(NULL),
	m_pVTViewer(NULL),
	m_pLastOriCatmullSegment(NULL),
	m_pLastPosCatmullSegment(NULL)
{
	m_bTourCreated = false;

	CreateEmptyTour();

	m_v3CircCenter= VistaVector3D(0,0,0);

	m_fDefaultPointStayTime = 3.0f;
}

VfaVirtualTourCreator::~VfaVirtualTourCreator() {
	DeleteTour();
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/



VistaVirtualPlatform* VfaVirtualTourCreator::GetVirtualPlatform() {
	return m_pVirtualPlatform;
}

VfaVirtualTour* VfaVirtualTourCreator::GetVirtualTour() {
	return m_pVirtualTour;
}

VfaVirtualTourViewer* VfaVirtualTourCreator::GetVTViewer() {
	return m_pVTViewer;
}


void VfaVirtualTourCreator::DeleteTour() {
	if(!m_bTourCreated)
		return;

	if(m_pVTViewer!=NULL)
		delete m_pVTViewer;

	delete m_pVirtualTour;

	m_bTourCreated=false;
}

bool VfaVirtualTourCreator::CreateEmptyTour()
{
	DeleteTour();

	m_pVirtualTour = new VfaVirtualTour(m_pRenderNode, m_pVirtualPlatform);
	m_bTourCreated=true;

	float fVelocity=10.0;
	m_pVirtualTour->SetVelocity(fVelocity);

	


	m_pVTViewer = new VfaVirtualTourViewer(m_pVirtualTour, m_pRenderNode);

	// we can set different colors for the paths as RGB and alpha (opacity), so if we don't want to see a path, just set {0,0,0,0} or use VFA_VIEW_NONE
	float color[4] = {0,0,1,1};
	m_pVTViewer->SetPositionPathViewerColor(color);
	float color2[4] = {1,0,0,0.7f};
	m_pVTViewer->SetOrientationPathViewerColor(color2);
	//at last we set the width of the paths...
	m_pVTViewer->SetPositionPathViewerWidth(1.0f);
	m_pVTViewer->SetOrientationPathViewerWidth(0.5f);
	//and show everything
	m_pVTViewer->Show();

	return true;
}

//This adds an virtualTour to to the creator so, it will use this Tour to build on from it
bool VfaVirtualTourCreator::AddExistingVirtualTour(VfaVirtualTour *pTour){

	DeleteTour();

	if(pTour==NULL)
		return false;
	m_pVirtualTour=pTour;

	VistaVector3D v3Pos;
	std::vector<VfaSegment::VfaVTStation*> vecPosStations = m_pVirtualTour->GetPositionPath()->GetAllStations();
	if(vecPosStations.size() >= 2) {
		m_v3LastPosition = vecPosStations[vecPosStations.size()-1]->v3Pos;
		m_v3BeforeLastPosition= vecPosStations[vecPosStations.size()-2]->v3Pos;
	}
	else {
		if(vecPosStations.size() == 1) {
			v3Pos = vecPosStations[0]->v3Pos;
		}
		else {
			v3Pos = VistaVector3D(0.0,0.0,0.0);
		}
		m_v3LastPosition= v3Pos;
		m_v3BeforeLastPosition= v3Pos;
	}
	m_pLastPosCatmullSegment=NULL;

	VistaVector3D v3Ori;
	std::vector<VfaSegment::VfaVTStation*> vecOriStations = m_pVirtualTour->GetOrientationPath()->GetAllStations();
	if(vecOriStations.size() >= 2) {
		m_v3LastOriStation = vecOriStations[vecOriStations.size()-1]->v3Pos;
		m_v3BeforeLastOriStation= vecOriStations[vecOriStations.size()-2]->v3Pos;
	}
	else {
		if(vecOriStations.size() == 1) {
			v3Ori = vecOriStations[0]->v3Pos;
		}
		else {
			v3Ori = VistaVector3D(0.0,0.0,0.0);
		}
		m_v3LastOriStation= v3Ori;
		m_v3BeforeLastOriStation= v3Ori;
	}
	m_pLastOriCatmullSegment=NULL;

	m_bTourCreated=true;


	m_pVTViewer = new VfaVirtualTourViewer(m_pVirtualTour, m_pRenderNode);
	m_pVTViewer->Show();

	return true;
}


bool VfaVirtualTourCreator::AddSegmentToTour(VistaVector3D v3CurrentPosition, ESegmentType iType, bool bForPositionPath) {


	if(iType == VT_CIRC_CENTER) {
		//we just change the rotation center
		m_v3CircCenter = v3CurrentPosition;
		return true;
	}
	//if we add a segment we have to deactivate the synchronous mode because it won't be possible anymore if it was before!
	m_pVirtualTour->SetSynchronousMode(false);

	VfaSegment *pSeg;

	//if we have last added catmullrom segments and add other segments now, we add this station as well to the catmullrom segment to get a smooth tour
	if(iType != VT_CATMULLROM) {
		if(bForPositionPath && m_pLastPosCatmullSegment){
			m_pLastPosCatmullSegment->DeleteLastStation();
			m_pLastPosCatmullSegment->AddStation(v3CurrentPosition);
			m_pLastPosCatmullSegment=NULL;
		}
		if(!bForPositionPath && m_pLastOriCatmullSegment){
			m_pLastOriCatmullSegment->DeleteLastStation();
			m_pLastOriCatmullSegment->AddStation(v3CurrentPosition);
			m_pLastOriCatmullSegment=NULL;
		}

	}


	if(iType == VT_POINT) {
		pSeg = new VfaPointSegment(m_fDefaultPointStayTime, v3CurrentPosition);
	}
	else if(iType == VT_LINEAR) {
		if(bForPositionPath)
			pSeg = new VfaLinearSegment(m_v3LastPosition, v3CurrentPosition);
		else
			pSeg = new VfaLinearSegment(m_v3LastOriStation, v3CurrentPosition);
	}
	else if(iType == VT_CIRC) {
		if(bForPositionPath)
			pSeg = new VfaCircularSegment(m_v3LastPosition, v3CurrentPosition, m_v3CircCenter);
		else
			pSeg = new VfaCircularSegment(m_v3LastOriStation, v3CurrentPosition, m_v3CircCenter);
	}
	else if(iType == VT_CATMULLROM) {
		if(bForPositionPath){
			if(m_pLastPosCatmullSegment){
				//the segment is already added, we just have to add a new Station, and delete the last doubled one
				m_pLastPosCatmullSegment->DeleteLastStation();
				m_pLastPosCatmullSegment->AddStation(v3CurrentPosition);
				m_pLastPosCatmullSegment->AddStation(v3CurrentPosition);
				m_v3LastPosition=v3CurrentPosition;

				if(m_pVirtualTour->IsPaused()){
					m_pVirtualTour->AdjustPause(m_pVirtualTour->GetParameter());
				}


				if(m_pVTViewer)
					m_pVTViewer->Show();
				return true;
			}
			else {
				pSeg = new VfaCatmullRomSegment();
				pSeg->AddStation(m_v3BeforeLastPosition);
				pSeg->AddStation(m_v3LastPosition);
				pSeg->AddStation(v3CurrentPosition);
				// we have to add this station twice, so that we always have 4 stations, the next time we have to delete the doubled last station first
				pSeg->AddStation(v3CurrentPosition);
				m_pLastPosCatmullSegment = static_cast<VfaCatmullRomSegment*>( pSeg);
			}
		}
		else{
			if(m_pLastOriCatmullSegment){
				//the segment is already added, we just have to add a new Station
				m_pLastOriCatmullSegment->DeleteLastStation();
				m_pLastOriCatmullSegment->AddStation(v3CurrentPosition);
				m_pLastOriCatmullSegment->AddStation(v3CurrentPosition);
				m_v3LastOriStation=v3CurrentPosition;

				if(m_pVirtualTour->IsPaused()){
					m_pVirtualTour->AdjustPause(m_pVirtualTour->GetParameter());
				}


				if(m_pVTViewer)
					m_pVTViewer->Show();
				return true;
			}
			else {
				pSeg = new VfaCatmullRomSegment();
				pSeg->AddStation(m_v3BeforeLastOriStation);
				pSeg->AddStation(m_v3LastOriStation);
				pSeg->AddStation(v3CurrentPosition);
				// we have to add this station twice, so that we always have 4 stations, the next time we have to delete the doubled last station first
				pSeg->AddStation(v3CurrentPosition);
				m_pLastOriCatmullSegment = static_cast<VfaCatmullRomSegment*>( pSeg);
			}
		}
		
	}




	if(bForPositionPath){
		m_pVirtualTour->AddPositionSegment(pSeg);
		m_v3BeforeLastPosition= m_v3LastPosition;
		m_v3LastPosition=v3CurrentPosition;

	}
	else {
		m_pVirtualTour->AddOrientationSegment(pSeg);
		m_v3BeforeLastOriStation= m_v3LastOriStation;
		m_v3LastOriStation=v3CurrentPosition;
	}

	
	//if we created a point segment we might have produced a gap in our Tour!
	if(iType==VT_POINT && m_pVirtualTour->GetGapFilling()){
		m_pVirtualTour->GetPositionPath()->FillTheGaps();
		m_pVirtualTour->GetOrientationPath()->FillTheGaps();
	}

	if(bForPositionPath){
		//we have to change the parameter if we are paused so we start from the same point if we continue
		float fNewLength = m_pVirtualTour->GetPositionPath()->GetPathLength();
		m_pVirtualTour->SetParameter(m_pVirtualTour->GetParameter() * (fNewLength - pSeg->GetSegmentLength()) /fNewLength);
	}

	
	if(m_pVirtualTour->IsPaused()){
		//this is for example needed if we changed the orientationpath because now the pausedOrientation has to be different
		m_pVirtualTour->AdjustPause(m_pVirtualTour->GetParameter());
	}




	if(m_pVTViewer)
		m_pVTViewer->Show();

	return true;
}
bool VfaVirtualTourCreator::SetAsStartForNewPath(VistaVector3D v3CurrentPosition, bool bForPositionPath){
	if(bForPositionPath) {
		VfaPath *pPath = new VfaPath();
		m_pVirtualTour->SetPositionPath(pPath);
		m_v3LastPosition= v3CurrentPosition;
		m_v3BeforeLastPosition= v3CurrentPosition;
		m_pLastPosCatmullSegment=NULL;

	}
	else {
		VfaPath *pPath = new VfaPath();
		m_pVirtualTour->SetOrientationPath(pPath);
		m_v3LastOriStation= v3CurrentPosition;
		m_v3BeforeLastOriStation= v3CurrentPosition;
		m_pLastOriCatmullSegment=NULL;

	}
	if(m_pVTViewer)
		m_pVTViewer->Show();

	return true;
}

bool VfaVirtualTourCreator::AddCurrentPositionAsSegmentToTour(ESegmentType iType, bool bForPositionPath){
	VistaVector3D v3currPos;
	m_pVirtualPlatform->GetTranslation(v3currPos);

	return AddSegmentToTour(v3currPos, iType, bForPositionPath);
}

bool VfaVirtualTourCreator::SetCurrentPositionAsStartForNewPath(bool bForPositionPath){
	VistaVector3D v3currPos;
	m_pVirtualPlatform->GetTranslation(v3currPos);

	return SetAsStartForNewPath(v3currPos, bForPositionPath);
}


bool VfaVirtualTourCreator::IsSynchronousModePossible(){
	return (m_pVirtualTour->GetOrientationPath()->GetNumberOfSegments() == m_pVirtualTour->GetPositionPath()->GetNumberOfSegments());
}
void VfaVirtualTourCreator::EnableSynchronousMode(){
	if(IsSynchronousModePossible())
		m_pVirtualTour->SetSynchronousMode(true);
}

bool VfaVirtualTourCreator::DeleteLastStation(bool bForPositionPath) {

	VfaPath *pPath;
	if(bForPositionPath)
		pPath = m_pVirtualTour->GetPositionPath();
	else
		pPath = m_pVirtualTour->GetOrientationPath();
	std::vector<VfaSegment::VfaVTStation*> vecStations = pPath->GetAllStations();

	if(vecStations.size()<1)
		return false;

	int iTypeOfLastSegment = pPath->GetSegment(vecStations.back()->iSegmentNr)->GetTypeID();

	if(iTypeOfLastSegment == VfaSegment::VT_SEG_CATMULLROM) {
		//we have to check whether we just delete the last station in this segment or the whole segment
		VfaCatmullRomSegment *pSeg = static_cast<VfaCatmullRomSegment*>( pPath->GetSegment(pPath->GetNumberOfSegments()-1));
		if(pSeg->GetNumberOfStations()>4) {
			//we can just delete the station
			pSeg->DeleteLastStation();
			pSeg->DeleteLastStation();
			pSeg->AddStation(pSeg->GetStation(pSeg->GetNumberOfStations()-1));
		}
		else {
			pPath->RemoveSegment(vecStations.back()->iSegmentNr);
			if(bForPositionPath)
				m_pLastPosCatmullSegment=NULL;
			else
				m_pLastOriCatmullSegment=NULL;
		}
	}
	else {
		//we can just delte the last segment, bu have to take care, that we handle it right if the segment before this one was a catmullrom
		pPath->RemoveSegment(vecStations.back()->iSegmentNr);

		if(pPath->GetNumberOfSegments()>=1 && pPath->GetSegment(pPath->GetNumberOfSegments()-1)->GetTypeID() == VfaSegment::VT_SEG_CATMULLROM) {
			VfaCatmullRomSegment *pSeg = static_cast<VfaCatmullRomSegment*>( pPath->GetSegment(pPath->GetNumberOfSegments()-1));
			pSeg->DeleteLastStation();
			pSeg->AddStation(pSeg->GetStation(pSeg->GetNumberOfStations()-1));
			if(bForPositionPath)
				m_pLastPosCatmullSegment=pSeg;
			else
				m_pLastOriCatmullSegment=pSeg;
		}
	}
	if( bForPositionPath && vecStations.size()>=2 ) {
		m_v3LastPosition = vecStations[vecStations.size()-2]->v3Pos;
		m_v3BeforeLastPosition = m_v3LastPosition;
		if(vecStations.size()>=3)
			m_v3BeforeLastPosition = vecStations[vecStations.size()-3]->v3Pos;
	}
	else if( !bForPositionPath && vecStations.size()>=2 ) {
		m_v3LastOriStation = vecStations[vecStations.size()-2]->v3Pos;
		m_v3BeforeLastOriStation = m_v3LastOriStation;
		if(vecStations.size()>=3)
			m_v3BeforeLastOriStation = vecStations[vecStations.size()-3]->v3Pos;
	}

	if(m_pVTViewer)
		m_pVTViewer->Show();

	//we just set the Tour to stoped so, we don't get a problem with bad parameter-values
	m_pVirtualTour->SetParameter(0.0f);
	if(m_pVirtualTour->IsPaused())
		m_pVirtualTour->AdjustPause(0.0f);

	return true;
}

