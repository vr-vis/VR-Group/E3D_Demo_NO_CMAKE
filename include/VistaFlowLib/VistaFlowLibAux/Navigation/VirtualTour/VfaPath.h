/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFAPATH_H
#define _VFAPATH_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

// ViSTA stuff
#include <VistaAspects/VistaObserveable.h>
#include <VistaBase/VistaVectorMath.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderable.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include "Segments/VfaSegment.h"
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VistaVirtualPlatform;
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*
* This represents a path and holds all the segments. So the two Interpolate methods
* return a position for a given parameter
 */
class  VISTAFLOWLIBAUXAPI VfaPath
{
public:


	VfaPath();
	~VfaPath();

	float GetPathLength();

	int GetNumberOfSegments();

	//This returns the an ordered vector of all stations
	std::vector<VfaSegment::VfaVTStation*> GetAllStations();

	/**
	* prepares the path parameters and prepare every segment of the path
	*/
	bool CheckAndPrepare(bool bForcePrepare = false);

   /**
    * Returns the position connected to the parameter by re-parameterizing
	* and passing the new param on to the current segment
	* So it calculates a parameter value in [0,1] the index of the segment interval
	* that would be used to interpolate a position for the parameter value.
	*
	* So here fParam is the global parameter value [0,1] of the path.
	*
	* the PauseTime could normally be ignored, because it is merely for internal 
	* function calls.
	*
	* If the whole tour should pause until continue is called by the user PauseTime=-1.0 is returned
	*/
	VistaVector3D Interpolate(float fParm, float &PauseTime);
	VistaVector3D Interpolate(float fParm);

	VistaVector3D InterpolateSynchronous(int SegNr, float fLocalParm, float &PauseTime);

	/**
    * Returns a pointer to the segment, by the interger value of the index
	*/
	VfaSegment * GetSegment(int iIndex);

	/**
	* Remove a Segment at a specific index
	*/
	bool RemoveSegment(int iIndex);

	/**
	* This method should be called if the Path should automatically fill the gaps with linear
	* segments
	*/
	void FillTheGaps();

	/**
	* Add a segment at the end of the path
	*/
	void AddSegment(VfaSegment* pSegment);
	
	//this will insert a segment at position index (starting with 0), so the segment at position index and all folowing will be shifted one back
	void InsertSegment(VfaSegment* pSegment, int index);

	int GetCurrentSegmentNumber();
	float GetParmCurrentSegStartedWith();

	//deletes all segments of the path and resets the path to unprepared
	void ClearPath();


private:
	
	/** 
	* adjust the value of the parm passed to be in the range of [0,1]
	*/
	void AdjustParm(float &fParm);

	std::vector<VfaSegment*>	m_vecSegments;
	int							m_iLastSegmentNumber;
	int							m_iCurrentSegment;
	float						m_fCurrentSegmentStartParm;

};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _VFAPATH_H */
