/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFAPOINTSEGMENT_H
#define _VFAPOINTSEGMENT_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

// ViSTA stuff
#include <VistaBase/VistaVectorMath.h>
#include <VistaMath/VistaEaseCurve.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

//VfaSegment
#include "VfaSegment.h"
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*
* As the name says a PointSegment is just a single point, where the path stops,
* the diffrence between a pointSegment and a pause a the end of an other segment is
* that not the whole tour stops. That means you can create a pointSegment in the
* positionPath, so the viewer stops there for a while and looks around, because
* the orientation path is still moving.
 */
class VISTAFLOWLIBAUXAPI VfaPointSegment :	public VfaSegment

{
public:

	VfaPointSegment();

	VfaPointSegment(VistaVector3D v3Position);

	/**
	* For creating a pointSegment, where the path stays for fTime seconds, here the 
	* global velocity is needed as well, for computation
	*/
	VfaPointSegment(float fTime, float fVelocity, VistaVector3D v3Position);

	/*
	* For creating a pointSegment just by giving it the equivalent length of a
	* normal segment
	*/
	VfaPointSegment(float fLength, VistaVector3D v3Position);

	VfaPointSegment(VistaPropertyList* pPropList);

	virtual ~VfaPointSegment(void);

	void UpdateVelocity(float fVelocity);
	void UpdateStayTime(float fTime);
	void updateVirtualLength(float fLength);

	float GetSegmentLength();

	/**
	 * Returns classname as string
	 */
	std::string GetClassName();

	int GetTypeID();

	/**
	 * make all preparations (will be called automatically if not m_bPrepared)
	 */
	bool Preparation();

	/**
	 *	Adds a new station, which will always be appended at the end, returns the index of the new added station (starting with 0)
	 *  this Segment could just have one single station.
	 */
	int AddStation(VistaVector3D v3Station);


	VistaVector3D GetFirstRealStation();
	VistaVector3D GetLastRealStation();
	VistaPropertyList* CreatePropertyList();
	bool UpdateWithPropertyList(VistaPropertyList* pList);



private:

	VistaVector3D GetPosition(float fParm, float &fPauseTime);

	float			m_fVelocity;
	float			m_fStayTime;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _VFAPOINTSEGMENT_H */

