/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
#include "VfaPointSegment.h"

#include <VistaBase/VistaStreamUtils.h>


// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //

VfaPointSegment::VfaPointSegment() 
	:VfaSegment(0.0, 1.0),
	m_fVelocity(0.0),
	m_fStayTime(-1.0)
{
	m_fLength=-1.0f;
}

VfaPointSegment::VfaPointSegment(VistaVector3D v3Position) 
	:VfaSegment(0.0, 1.0),
	m_fVelocity(0.0),
	m_fStayTime(-1.0)
{
	m_fLength=-1.0f;
	AddStation(v3Position);
}

VfaPointSegment::VfaPointSegment(float fTime, float fVelocity, VistaVector3D v3Position) 
	:VfaSegment(0.0, 1.0),
	m_fVelocity(fVelocity),
	m_fStayTime(fTime)
{
	AddStation(v3Position);
	m_fLength=fVelocity*fTime;
}

VfaPointSegment::VfaPointSegment(float fLength, VistaVector3D v3Position) 
	:VfaSegment(0.0, 1.0),
	m_fVelocity(0.0),
	m_fStayTime(-1.0)
{
	m_fLength = fLength;
	AddStation(v3Position);
}
VfaPointSegment::VfaPointSegment(VistaPropertyList *pPropList)
	:VfaSegment(0.0,1.0)
{
	SetPropertyListValues(pPropList);
	if(pPropList->GetValueOrDefault("Type",(int)VT_SEG_DEFAULT)!=(int)VT_SEG_POINT){
		vstr::warnp()<<"[VfaPointSegment] Created with wrong type of propList. Everything set to default!"<<std::endl;
	}
	m_fLength = pPropList->GetValueOrDefault("VirtualLength", -1.0f);
	m_fStayTime = pPropList->GetValueOrDefault("StayTime", -1.0f);
	m_fVelocity = pPropList->GetValueOrDefault("Velocity", 0.0f);
}

VfaPointSegment::~VfaPointSegment(void)
{}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/* ==========================================================================*/
/* NAME: UpdateVelocity(float fVelocity) 									 */
/* ==========================================================================*/
void VfaPointSegment::UpdateVelocity(float fVelocity){
	m_fVelocity=fVelocity;
	m_bPrepared=false;
}

/* ==========================================================================*/
/* NAME: UpdateStayTime(float fTime) 										 */
/* ==========================================================================*/
void VfaPointSegment::UpdateStayTime(float fTime){
	m_fStayTime=fTime;
	m_bPrepared=false;
}

/* ==========================================================================*/
/* NAME: updateVirtualLength(float fLength) 								 */
/* ==========================================================================*/
void VfaPointSegment::updateVirtualLength(float fLength){
	m_fLength=fLength;
	m_bPrepared=false;
}

/* ==========================================================================*/
/* NAME: GetSegmentLength() 												 */
/* ==========================================================================*/
float VfaPointSegment::GetSegmentLength(){
	if(Preparation())
		return m_fLength;
	else
		return -1.0;

}

/* ==========================================================================*/
/* NAME: GetClassName()														 */
/* ==========================================================================*/
std::string VfaPointSegment::GetClassName(){
	return "VfaPointSegment";
}

int VfaPointSegment::GetTypeID() {
	return VT_SEG_POINT;
}

/* ==========================================================================*/
/* NAME: Preparation()														 */
/* ==========================================================================*/
bool VfaPointSegment::Preparation()
{
	if(m_bPrepared)
		return true;

	if(m_fStayTime==-1.0) {
		
		if(m_fLength==-1.0) {
			vstr::errp()<<"[VfaPointSegment] Can't calculate Point Stay length, because no virtual length given!"<<std::endl;
			m_bPrepared=false; //just to make it clear ;-)
		}
		else {
			if(m_fVelocity!=0.0)
				m_fStayTime=m_fLength/m_fVelocity;
			m_bPrepared=true;
		}
	}
	else {
		if(m_fVelocity!=0.0){
			m_fLength=m_fVelocity*m_fStayTime;
		}
		m_bPrepared=(m_fLength!=-1.0);
	}
	if(GetNumberOfStations()==0)
		m_bPrepared=false;
	return m_bPrepared;
}

/* ==========================================================================*/
/* NAME: AddStation(VistaVector3D &v3Station)				 				 */
/* ==========================================================================*/
int VfaPointSegment::AddStation(VistaVector3D v3Station){

	m_bPrepared=false;

	if(GetNumberOfStations()==1) {
		vstr::warnp()<<"[VfaPointSegment] You can just have one station in a point segment, use reset first."<<std::endl;
		return -1;
	}
	m_vecStations.push_back(v3Station);
	return ((int)m_vecStations.size()-1);
}

/* ==========================================================================*/
/* NAME: GetPosition(float fParam)							 				 */
/* ==========================================================================*/
VistaVector3D VfaPointSegment::GetPosition(float fParm, float &fPauseTime){

	if(!Preparation())
		vstr::errp()<<"[VfaPointSegment] Preparation of this pointSegment failed!"<<std::endl;

	fPauseTime = 0.0f;

	return GetStation(0);
}

VistaVector3D VfaPointSegment::GetFirstRealStation(){
	if (m_vecStations.size() == 1) 
		return GetStation(0);
	else
		return VistaVector3D(0,0,0);
}

VistaVector3D VfaPointSegment::GetLastRealStation(){
	if (m_vecStations.size() == 1) 
		return GetStation(0);
	else
		return VistaVector3D(0,0,0);
}

VistaPropertyList* VfaPointSegment::CreatePropertyList() {
	VistaPropertyList* pList = new VistaPropertyList();
	pList->SetValue("Type",(int)VT_SEG_POINT);
	pList->SetValue("VirtualLength",m_fLength);
	pList->SetValue("StayTime",m_fStayTime);
	pList->SetValue("Velocity",m_fVelocity);
	pList->MergeWith(*CreateSegmentPropertyList());
	return pList;
}

bool VfaPointSegment::UpdateWithPropertyList(VistaPropertyList* pList) {

	if(pList->GetValueOrDefault("Type",(int)VT_SEG_DEFAULT)!=(int)VT_SEG_POINT){
		vstr::warnp()<<"[VfaPointSegment] Update with wrong type of propList. Nothing changed!"<<std::endl;
		return false;
	}
	SetPropertyListValues(pList);
	
	m_fLength = pList->GetValueOrDefault("VirtualLength", -1.0f);
	m_fStayTime = pList->GetValueOrDefault("StayTime", -1.0f);
	m_fVelocity = pList->GetValueOrDefault("Velocity", 0.0f);

	return true;
}

// ========================================================================== //
// === End of File
// ========================================================================== //
