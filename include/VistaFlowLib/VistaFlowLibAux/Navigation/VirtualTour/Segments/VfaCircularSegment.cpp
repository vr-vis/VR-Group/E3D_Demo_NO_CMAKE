/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
#include "VfaCircularSegment.h"
#include <VistaBase/VistaStreamUtils.h>

#include <cmath>

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //

VfaCircularSegment::VfaCircularSegment(bool bForceCircle) //bForceCircle=false
	:VfaSegment(0.0, 1.0),
	m_v3RotationCenter(0.0, 0.0, 0.0),
	m_bForceCircle(bForceCircle),
	m_bCircleBetweenIdenticalPoints(false),
	m_bForceCorrectRotationAxis(false)
{
}
VfaCircularSegment::VfaCircularSegment(VistaVector3D v3Start, VistaVector3D v3End, VistaVector3D v3Center, bool bForceCircle) //bForceCircle=false
	:VfaSegment(0.0, 1.0),
	m_bForceCircle(bForceCircle),
	m_bCircleBetweenIdenticalPoints(false),
	m_bForceCorrectRotationAxis(false)
{
	SetRotationCenter(v3Center);
	AddStation(v3Start);
	AddStation(v3End);
}
VfaCircularSegment::VfaCircularSegment(VistaPropertyList *pPropList)
	:VfaSegment(0.0,1.0),
	m_bCircleBetweenIdenticalPoints(false)
{
	SetPropertyListValues(pPropList);
	if(pPropList->GetValueOrDefault("Type",(int)VT_SEG_DEFAULT)!=(int)VT_SEG_CIRCULAR){
		vstr::warnp()<<"[VfaCircularSegment] Created with wrong type of propList. Everything set to default!"<<std::endl;
	}
	m_v3RotationCenter = pPropList->GetValueOrDefault("RotationCenter", VistaVector3D(0.0,0.0,0.0));
	m_bForceCircle = pPropList->GetValueOrDefault("ForceCircle", false);
	m_bForceCorrectRotationAxis = pPropList->GetValueOrDefault("ForceCorrectRotationAxis", false);

}

VfaCircularSegment::~VfaCircularSegment(void)
{}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/* ==========================================================================*/
/* NAME: GetSegmentLengthWithoutEaseCurve() 								 */
/* ==========================================================================*/
float VfaCircularSegment::GetSegmentLength(){
	if(Preparation())
		return m_fLength;
	else
		return -1.0;

}

/* ==========================================================================*/
/* NAME: GetClassName()														 */
/* ==========================================================================*/
std::string VfaCircularSegment::GetClassName(){
	return "VfaCircularSegment";
}

int VfaCircularSegment::GetTypeID() {
	return VT_SEG_CIRCULAR;
}

/* ==========================================================================*/
/* NAME: Preparation()														 */
/* ==========================================================================*/
bool VfaCircularSegment::Preparation(){
	if(m_bPrepared || m_bCircleBetweenIdenticalPoints)
		return true;

	if(GetNumberOfStations()!=2) {
		vstr::errp()<<"[VfaCircularSegment] Exactly 2 Stations are needed for a circular segment!"<<std::endl;
		m_bPrepared=false;
		return false;
	}

	if((GetStation(0)-GetStation(1)).GetLength()<0.0001){
		//we don't have to circle
		vstr::debugi()<<"[VfaCircularSegment] Circle between 2 identical points!"<<std::endl;
		m_bCircleBetweenIdenticalPoints = true;
		return true;
	}
	m_bCircleBetweenIdenticalPoints = false;

	/* *
	 * The two points don't have to be on a circle around the rotation center
	 * so what we do is, calculate a point that is on the same circle as the
	 * start point and on the axis between the center and our endpoint
	 * and interpolate between these two points. And in each step we
	 * do a linear interpolation between this virtual point and our endpoint
	 * because we need to exactly reach the endpoint.
	 */


	VistaVector3D vec1=GetStation(0)-m_v3RotationCenter;
	VistaVector3D vec2=GetStation(1)- m_v3RotationCenter;
	float eps =0.0001f; // a little epsilon for our length comparision, to handle machine inaccuracy


	if(m_bForceCircle &&   std::abs(vec1.GetLength() - vec2.GetLength())>eps) {
		//we have to move our Station to get a perfect circle with constant radius
		float fRadius = (vec1.GetLength()+ vec2.GetLength())/2;
		vstr::debugi() <<std::endl<<std::endl<< "[VfaCircularSegment] Moved Stations to force a perfect circle with radius "<<fRadius<<std::endl;

		//vstr::debugi()<<"Moved Start from "<<m_vecStations[0]<<" with distance "<<vec1.GetLength();
		m_vecStations[0] += vec1.GetNormalized() * (fRadius - vec1.GetLength());
		//vstr::debugi()<<" to "<<m_vecStations[0]<<std::endl;

		//vstr::debugi()<<"Moved End from "<<m_vecStations[1]<<" with distance "<<vec2.GetLength();
		m_vecStations[1] += vec2.GetNormalized() * (fRadius - vec2.GetLength());
		//vstr::debugi()<<" to "<<m_vecStations[1]<<std::endl;
		
	}

	if(!m_bForceCorrectRotationAxis){
		// we take our normal rotationaxis, because this way we get far less problems with the CAVE-mode and nearly horizontal rotation axes

		//the y-component of the first station is the one we start our circle with
		m_fStartHeight = GetStation(0)[1];
		//and the difference is the amount our circle is rising, so it is more part of a spyral than a correct circle!
		m_fHeightDifference = GetStation(1)[1] - GetStation(0)[1];


		//some help vectors, because we have to delete the y-components.
		VistaVector3D v3HelpSt0, v3HelpSt1, v3HelpRotC;

		v3HelpSt0 = GetStation(0);
		v3HelpSt0[1]=0.0f;
		v3HelpRotC = m_v3RotationCenter;
		v3HelpRotC[1]=0.0f;

		m_fRadius = (v3HelpSt0 -v3HelpRotC).GetLength();

		v3HelpSt1 = GetStation(1);
		v3HelpSt1[1]=0.0f;

		vec1=v3HelpSt0 - v3HelpRotC;
		vec2=v3HelpSt1 - v3HelpRotC;

		while(vec2.Dot(vec1)/(vec2.GetLength()*vec1.GetLength())<=-1.0f || vec2.Dot(vec1)/(vec2.GetLength()*vec1.GetLength())>=1.0f){
			//in this case our calculation would crash and the result would be NaN, so we use a dirty trick here and simply move our point a tiny bit away!
			//but we have to take care that we don't move it along the axis between the two point otherwise we get an endless loop
			vstr::warni()<<"[VfaCircularSegment] Bad points for rotation, altered one Point by 0.001!"<<std::endl;
			VistaVector3D vecDir = VistaVector3D(1.0f,0.0f,0.0f);
			vecDir = vecDir.Cross(vec2-vec1);
			if(vecDir.GetIsZeroVector()) {
				vecDir = VistaVector3D(0.0f,1.0f,0.0f);
				vecDir = vecDir.Cross(vec2-vec1);
			}
			vecDir = 0.001f*vecDir.GetNormalized();
			vec2-=vecDir;
		}
		vec1.Normalize();
		vec2.Normalize();


		m_fAngle = acos(vec2.Dot(vec1));

		//but still we have to calculate the rotation axis, because it could either be 0,1,0 or 0,-1,0
		m_v3RotationAxis = vec1.Cross(vec2);
		m_v3RotationAxis.Normalize();

		m_v3VirtualPoint = (v3HelpSt1- v3HelpRotC) / (v3HelpSt1- v3HelpRotC).GetLength() * m_fRadius + v3HelpRotC;
		m_v3LinearDistance = v3HelpSt1 - m_v3VirtualPoint;

		m_bPrepared=true;

		m_v3YLessStation0 = v3HelpSt0;
		m_v3YLessRotCenter = v3HelpRotC;


	}
	else { //if correct rotation axis is forced!

		vec1.Normalize();
		vec2.Normalize();

		m_v3RotationAxis = vec1.Cross(vec2);
		m_v3RotationAxis.Normalize();

		//see if the rotationaxis is not (0,0,0) otherwise we just take the y-axis
		if(m_v3RotationAxis.GetLength()==0 || vec1.Dot(vec2)==1 || vec1.Dot(vec2)==-1) {
			m_v3RotationAxis = VistaVector3D(0.0, 1.0, 0.0);
			vstr::warnp()<<"[VfaCircularSegment] Bad Rotation-Axis. Use default RotationAxis (0,1,0)."<<std::endl;
		}

		//vstr::debugi()<<"[VfaCircularSegment] Rotation-Axis"<<m_v3RotationAxis<<std::endl;

		m_fAngle = acos(vec1.Dot(vec2));

		m_fRadius = (GetStation(0)-m_v3RotationCenter).GetLength();

		m_v3VirtualPoint = (GetStation(1)- m_v3RotationCenter) / (GetStation(1)- m_v3RotationCenter).GetLength() * m_fRadius + m_v3RotationCenter;
		m_v3LinearDistance = GetStation(1) - m_v3VirtualPoint;


		m_bPrepared=true;


	}
	m_fLength=SimulateSegmentForLength(SIMULATIONSTEPS);
	AlterLengthForEaseCurve();		

	return m_bPrepared;
}

/* ==========================================================================*/
/* NAME: AddStation(VistaVector3D &v3Station)				 				 */
/* ==========================================================================*/
int VfaCircularSegment::AddStation(VistaVector3D v3Station){

	m_bPrepared=false;

	if(GetNumberOfStations()==2) {
		vstr::warnp()<<"[VfaCircularSegment]: You can just have two stations for a circular segment, use reset first."<<std::endl;
		return -1;
	}
	m_vecStations.push_back(v3Station);
	return ((int)m_vecStations.size()-1);
}
/* ==========================================================================*/
/* NAME: SetRotationCenter(const VistaVector3D &v3Pos)		 				 */
/* ==========================================================================*/
void VfaCircularSegment::SetRotationCenter(const VistaVector3D v3Pos){

	m_bPrepared=false;

	m_v3RotationCenter=v3Pos;
}
/* ==========================================================================*/
/* NAME: GetRotationCenter(VistaVector3D &v3Pos)				 				 */
/* ==========================================================================*/
void VfaCircularSegment::GetRotationCenter(VistaVector3D &v3Pos){

	v3Pos=m_v3RotationCenter;
}

/* ==========================================================================*/
/* NAME: GetPosition(float fParam)							 				 */
/* ==========================================================================*/
VistaVector3D VfaCircularSegment::GetPosition(float fParam, float &fPauseTime){

	if(!Preparation())
		vstr::errp()<<"[VfaCircularSegment] Preparation of this circularSegment failed!"<<std::endl;
	if(m_bCircleBetweenIdenticalPoints)
		return GetStation(0);

	fPauseTime = 0.0f;
	VistaVector3D v3Pos;

	if(m_bForceCorrectRotationAxis) {
		//Rotate the startvector around the rotationaxis angle*param:
		VistaQuaternion qRot(VistaAxisAndAngle(m_v3RotationAxis, m_fAngle*fParam));
		v3Pos = qRot.Rotate(GetStation(0) - m_v3RotationCenter) + m_v3RotationCenter;
		//add the linear interpolation between the virtualEndPoint and the real EndPoint
		v3Pos = v3Pos + m_v3LinearDistance*fParam;
	}
	else {
		VistaQuaternion qRot(VistaAxisAndAngle(m_v3RotationAxis, m_fAngle*fParam));
		v3Pos = qRot.Rotate(m_v3YLessStation0 - m_v3YLessRotCenter) + m_v3YLessRotCenter;
		v3Pos = v3Pos + m_v3LinearDistance*fParam;
		v3Pos [1]= m_fStartHeight + fParam*m_fHeightDifference;
	}
	
	return (v3Pos);
}


/* ==========================================================================*/
/* NAME: SimulateSegmentForLength(int steps)				 				 */
/* ==========================================================================*/
float VfaCircularSegment::SimulateSegmentForLength(int steps){

	//we have to return a "really small" length, because otherwise the system crashes!
	if(m_bCircleBetweenIdenticalPoints)
		return 0.0001f;

	float fLength=0.0f;
	VistaVector3D v3LastPos = GetStation(0);
	VistaVector3D v3CurrentPos;
	
	float fNotUsedPauseTimeVariable = 0.0;

	for(float i=1; i<=steps; i++) {
		v3CurrentPos = GetPosition(i/steps, fNotUsedPauseTimeVariable);
		
		fLength += (v3CurrentPos-v3LastPos).GetLength();
		v3LastPos=v3CurrentPos;
	}
	return fLength;
}

VistaVector3D VfaCircularSegment::GetFirstRealStation(){
	if (m_vecStations.size() > 0) 
		return GetStation(0);
	else
		return VistaVector3D(0,0,0);
}

VistaVector3D VfaCircularSegment::GetLastRealStation(){
	int iSize = (int)m_vecStations.size();
	if (iSize == 2) 
		return GetStation(1);
	else
		return VistaVector3D(0,0,0);
}

void VfaCircularSegment::ForceCorrectRotationAxis(bool bForce){
	m_bForceCorrectRotationAxis=bForce;
	m_bPrepared=false;
}
bool VfaCircularSegment::IsCorrectRotationAxisForced(){
	return m_bForceCorrectRotationAxis;
}

VistaPropertyList* VfaCircularSegment::CreatePropertyList() {
	VistaPropertyList* pList = new VistaPropertyList();
	pList->SetValue("Type",(int)VT_SEG_CIRCULAR);

	pList->SetValue("RotationCenter",m_v3RotationCenter);
	pList->SetValue("ForceCircle",m_bForceCircle);
	pList->SetValue("ForceCorrectRotationAxis",m_bForceCorrectRotationAxis);

	pList->MergeWith(*CreateSegmentPropertyList());
	return pList;
}

bool VfaCircularSegment::UpdateWithPropertyList(VistaPropertyList* pList) {

	if(pList->GetValueOrDefault("Type",(int)VT_SEG_DEFAULT)!=(int)VT_SEG_CIRCULAR){
		vstr::warnp()<<"[VfaCircularSegment] Update with wrong type of propList. Nothing changed!"<<std::endl;
		return false;
	}
	SetPropertyListValues(pList);
	
	m_v3RotationCenter = pList->GetValueOrDefault("RotationCenter", VistaVector3D(0.0,0.0,0.0));
	m_bForceCircle = pList->GetValueOrDefault("ForceCircle", false);
	m_bForceCorrectRotationAxis = pList->GetValueOrDefault("ForceCorrectRotationAxis", false);

	return true;
}

// ========================================================================== //
// === End of File
// ========================================================================== //