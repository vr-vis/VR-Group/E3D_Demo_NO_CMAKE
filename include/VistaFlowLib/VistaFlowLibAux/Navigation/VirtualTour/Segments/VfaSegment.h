/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFASEGMENT_H
#define _VFASEGMENT_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

// ViSTA stuff
#include <VistaBase/VistaVectorMath.h>
#include <VistaAspects/VistaPropertyList.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*
*A Segment is just a data-holder so it gets a parameter in his local param-space and
*returns the position of this segment.
*You can't use this class, because it's abstract, so use one of the derived classes
*instead:
*
*-VfaCatmullRomSegment
*-VfaCircularSegment
*-VfaPointSegment
*-VfaLinearSegment
 */


class VISTAFLOWLIBAUXAPI VfaSegment
{
public:

	//this class holds the information about one Station of our path
	class VfaVTStation {

	public:
		VfaVTStation() {
			v3Pos = VistaVector3D(0.0,0.0,0.0);
			fParameter=-1.0f;
			iSegmentNr=-1;
		}

		VistaVector3D	v3Pos;//where it is
		float			fParameter; //at which parameter of the whole path
		int				iSegmentNr; //to which segment it belongs
	};

	enum ESegType {
		VT_SEG_CATMULLROM,
		VT_SEG_CIRCULAR,
		VT_SEG_LINEAR,
		VT_SEG_POINT,
		VT_SEG_DEFAULT};



	VfaSegment(float fFadeInLength, float fFadeOutLength);
	virtual ~VfaSegment();

	/**
	 *	Get / Set the length of the fade-in in the locally param-space, vlaue between 0 and 0.5, default 0
	 *	The fade-in will start at the beginning and last until the param is reached
	 */
	void SetFadeInLength(float length);
	float GetFadeInLength() const;
	/**
	 *	Get / Set the length of the fade-out in the locally param-space, vlaue between 0.5 and 1.0, default 1.0
	 *	The fade-out will start when the param is reached and last until 1.0
	 */
	void SetFadeOutLength(float length);
	float GetFadeOutLength() const;
	/**
	 *	Get / Set the time the whole virtual tour will rest at the end station of this segment in seconds, default 0
	 *  If you just want e.g the position path of the tour to rest at a position, but still look around, you should
	 *  use a PointSegment!
	 *
	 *As well you could let the whole tour pause until Continue is called, e.g. manually by the user
	 */
	void SetPauseAtEndTime(float length);
	float GetPauseAtEndTime() const;
	void SetPauseAtEnd(bool bPause);
	bool GetPauseAtEnd();


	/**
	 *	Adds a new station, which will always be appended at the end, returns the index of the new added station (starting with 0)
	 */
	virtual int AddStation(VistaVector3D v3Station) = 0;
	/**
	 *	Returns the position of the station at iIndex (starting with 0)
	 */
	VistaVector3D GetStation(int iIndex) const;
	/**
	 *	returns the Position for the Parameter, which is in the local space of this segment [0 ; 1]
	 *  by using the transformed param of the easeCurve and calling GetPosition
	 *
	 * fPauseTime is set to -1 if the tour should pause until continue is called
	 */
	VistaVector3D InterpolateLocaly(float fParm, float &fPauseTime);
	/**
	 *	Get the arc length of this segment.
	 *  But careful this length is altered if easeing-in and -out is used so that the max velocity
	 *  isn't higher than defined!
	 */
	virtual float GetSegmentLength() = 0;
	/**
	 *	Remove a station, defined by the index iIndex
	 */
	void Remove(int iIndex);
	const int GetNumberOfStations() const;
	/**
	 *  Delete all stations
	 */
	void Reset();
	/**
	 * Returns class name as string
	 */
	virtual std::string GetClassName() = 0;
	//or as int for faster comparison
	virtual int GetTypeID()=0;


	/**
	 * make all preparations (will be called automatically if not m_bPrepared)
	 * every version should call AlterLengthForEaseCurve()
	 */
	virtual bool Preparation() = 0;
	
	// These return the fist and last station of the path
	//the real means that they are not just helper stations as in the catmullrom segment
	virtual VistaVector3D GetFirstRealStation() = 0;
	virtual VistaVector3D GetLastRealStation() = 0;

	virtual void ResetPreparedFlag();

	virtual std::vector<VfaSegment::VfaVTStation*> GetStations();

	virtual VistaPropertyList* CreatePropertyList();

	virtual bool UpdateWithPropertyList(VistaPropertyList* pList)=0;

	static std::string convertInt(int number);


protected:
	//this function changes the length of a segment so that the max velocity is
	//the velocity of the tour and not faster just because we added an ease curve and went slower
	//in the beginning and end
	void AlterLengthForEaseCurve();

	//we use a cubic bezier urve, to ease the curve in a way that we get an acceleration and a decceleration phase
	//the curve uses the points
	//X0: (0,0)
	//X1: (m_fEaseIn, 0)
	//X2: (m_fEaseOut, 1)
	//X3: (1,1)
	// see this homepage to get an idea what the movement will look like http://cubic-bezier.com/#.42,0,.77,1
	float GetBezierParameter( float fParam);

	virtual VistaVector3D GetPosition(float fParm);

	virtual VistaVector3D GetPosition(float fParm, float &fPauseTime) = 0;

	//this function stores (or sets) all relevant information of a segment in a (from a) property list and should be called from every inherited CreatePropertyList method!
	VistaPropertyList* CreateSegmentPropertyList();
	void SetPropertyListValues(VistaPropertyList* pPropList);

	bool							m_bPrepared;
	std::vector<VistaVector3D>		m_vecStations;
	float							m_fLength;
	//Pause At the end of the segment, the whole tour pauses so if you just want one path to pause, use a point segment
	float							m_fPauseAtEndTime; 
	bool							m_bPauseAtEnd;
	float							m_fEaseIn;
	float							m_fEaseOut;

};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _VFASEGMENT_H */
