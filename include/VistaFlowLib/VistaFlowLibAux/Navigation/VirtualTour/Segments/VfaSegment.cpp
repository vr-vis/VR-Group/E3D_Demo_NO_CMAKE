/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //

#include "VfaSegment.h"

#include <cstdio>
#include <math.h>

#include <VistaBase/VistaStreamUtils.h>


// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
VfaSegment::VfaSegment(float fFadeInLength, float fFadeOutLength)
	:m_bPrepared(false),
	m_fPauseAtEndTime(0.0f),
	m_bPauseAtEnd(false),
	m_fLength(-1.0f) //just initialize it with -1, but as long as it's not prepared, it can't be used
{
	SetFadeInLength(fFadeInLength);
	SetFadeOutLength(fFadeOutLength);

	m_vecStations.clear();

}

VfaSegment::~VfaSegment()
{

}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/* ==========================================================================*/
/* NAME: Get/SetFadeInLength()					 							 */
/* ==========================================================================*/
	/**
	 *	Get / Set the length of the fade-in in the locally param-space, value between 0 and 0.5, default 0
	 */
void VfaSegment::SetFadeInLength(float length) {
		if(length>=0 && length<=0.5)
			m_fEaseIn=length;
		else {
			m_fEaseIn=0.0f;
			vstr::warnp()<<"[VfaSegment] Didn't change FadeInLength, because "<<length<<" is not between 0.0 and 0.5!"<<std::endl;
		}

}
float VfaSegment::GetFadeInLength() const{
	return m_fEaseIn;
}

	/**
	 *	Get / Set the length of the fade-out in the locally param-space, vlaue between 0 and 0.5, default 0
	 */
void VfaSegment::SetFadeOutLength(float length) {
		if(length>=0.5 && length<=1.0)
			m_fEaseOut=length;
		else {
			m_fEaseOut=1.0f;
			vstr::warnp()<<"[VfaSegment] Didn't change FadeOutLength, because "<<length<<" is not between 0.5 and 1.0!"<<std::endl;
		}

}
float VfaSegment::GetFadeOutLength() const{
	return m_fEaseOut;
}

/* ==========================================================================*/
/* NAME: Get/SetPauseAtEndTime()				 							 */
/* ==========================================================================*/

	/**
	 *	Get / Set the time the whole virtual tour will rest at the end station of this segment in seconds, default 0
	 */
void VfaSegment::SetPauseAtEndTime(float length){
	m_fPauseAtEndTime=length;
}
float VfaSegment::GetPauseAtEndTime() const{
	return m_fPauseAtEndTime;
}

void VfaSegment::SetPauseAtEnd(bool bPause) {
	m_bPauseAtEnd = bPause;
}
bool VfaSegment::GetPauseAtEnd() {
	return m_bPauseAtEnd;
}


/* ==========================================================================*/
/* NAME: GetStation(int iIndex)				 								 */
/* ==========================================================================*/

	/**
	 *	Returns the position of the station at iIndex (starting with 0)
	 */
VistaVector3D VfaSegment::GetStation(int iIndex) const{

	return m_vecStations[iIndex];
}

/* ==========================================================================*/
/* NAME: InterpolateLocaly(float fParm)		 								 */
/* ==========================================================================*/

	/**
	 *	returns the Position for the Parameter, which is in the local space of this segment [0 ; 1]
	 *  by using the transformed param of the easeCurve and calling GetPosition
	 */
VistaVector3D VfaSegment::InterpolateLocaly(float fParm, float &fPauseTime){

	
	// for this to work correctly we have to stretch the time the segment gets as well, otherwise
	// we would go faster than with the regular velocity in the middle of the segment. However,
	// this is done in AlterLengthForEaseCurve()

	double dStrechedParam =GetBezierParameter(fParm);
	VistaVector3D v3Pos = GetPosition((float)dStrechedParam, fPauseTime);


	return v3Pos;

}

/* ==========================================================================*/
/* NAME: Remove(int iIndex)					 								 */
/* ==========================================================================*/
	/**
	 *	Remove a station, defined by the index iIndex
	 */
void VfaSegment::Remove(int iIndex){
	if(iIndex<GetNumberOfStations()){
		m_vecStations.erase(m_vecStations.begin()+iIndex);
		m_bPrepared=false;
	}
	else
		vstr::warnp()<<"[VfaSegment::Remove] No Station to delete"<<std::endl;
}
/* ==========================================================================*/
/* NAME: GetNumberOfStations()					 							 */
/* ==========================================================================*/
const int VfaSegment::GetNumberOfStations() const{

	return ((int)m_vecStations.size());
}

/* ==========================================================================*/
/* NAME: GetNumberOfStations()					 							 */
/* ==========================================================================*/
	/**
	 *  Delete all stations
	 */
void VfaSegment::Reset(){

	//start from the end, because vectors is stored as array, so the other elements don't have to be moved
	for(int i=GetNumberOfStations(); i>0; i--) {
		Remove(i-1);
	}
}

VistaVector3D VfaSegment::GetPosition(float fParm){
	float fNotUsedPauseTimeVariable = 0.0;
	return GetPosition(fParm, fNotUsedPauseTimeVariable);
}

void VfaSegment::ResetPreparedFlag(){
	m_bPrepared = false;
}

void VfaSegment::AlterLengthForEaseCurve() {

	float fAlterFactor=1.0;
	float fNotUsedPauseTime=0.0;

	//nothing to do here
	if((m_fEaseIn==0.0f && m_fEaseOut==1.0f) || m_fLength==0.0f) {
		//vstr::debugi()<<"No Length adaptation to do!"<<std::endl;
		return;
	}
	if(!(m_fLength==m_fLength)) {
		vstr::errp()<<"[VfaSegment]Trying to alter length, but length==NaN"<<std::endl;
	}

	//we simulate a run and check how much faster it went, than it is supposed to,
	//the we alter the length so it seams to the rest of the tour as if this element was longer
	//so we get more time and the velocity is smaller
	int iNumberSimulationSteps=100;
	VistaVector3D v3LastPos=InterpolateLocaly(0,fNotUsedPauseTime);
	VistaVector3D v3CurrentPos;
	float fSupposedDistancePerStep = m_fLength / iNumberSimulationSteps;

	for(int i = 1; i<=iNumberSimulationSteps; i++) {
		v3CurrentPos = InterpolateLocaly((float)i/iNumberSimulationSteps,fNotUsedPauseTime);
		if((v3CurrentPos-v3LastPos).GetLength()>fSupposedDistancePerStep) {
			if((v3CurrentPos-v3LastPos).GetLength()/fSupposedDistancePerStep > fAlterFactor) {
				fAlterFactor = (v3CurrentPos-v3LastPos).GetLength()/fSupposedDistancePerStep;
			}
		}
		v3LastPos = v3CurrentPos;
	}
	//vstr::debugi()<<"[AlterLengthForEaseCurve:] length altered by: "<<fAlterFactor<<"  from "<<m_fLength<<" to "<<(m_fLength*fAlterFactor)<<std::endl;
	m_fLength*=fAlterFactor;
}

float VfaSegment::GetBezierParameter(float fParam) {


	if(m_fEaseIn==0.0 && m_fEaseOut==1.0) {
		//it's a straight line anyway
		return fParam;
	}

	if(fParam <=0.0f || fParam>=1.0f) {
		return fParam;
	}

	//a calculation of the point on a bezier curve between the points
	//X0: (0,0)
	//X1: (m_fEaseIn, 0)
	//X2: (m_fEaseOut, 1)
	//X3: (1,1)

	//to find the corresponding t that sets 
	// fXBezierPoint = 3 * t * (1-t)* (1-t) *m_fEaseIn + 3*t*t *(1-t)*m_fEaseOut + t*t*t == fParam
	// we have to solve 0 = 3 * t * (1-t)* (1-t) *m_fEaseIn + 3*t*t *(1-t)*m_fEaseOut + t*t*t -fParam best with a Newton method
	double t=0.5f;
	double epsilon = 0.00001f;

	while( fabs((3 * t * (1-t)* (1-t) *m_fEaseIn + 3*t*t *(1-t)*m_fEaseOut + t*t*t -fParam)-0)>epsilon) {
		//tn+1 = tn - f(tn)/f'(tn)
		t = t - (3 * t * (1-t)* (1-t) *m_fEaseIn + 3*t*t *(1-t)*m_fEaseOut + t*t*t -fParam)/(3*t*t*(3*m_fEaseIn-3*m_fEaseOut+1)+2*t*(3*m_fEaseOut-5*m_fEaseIn)+3*m_fEaseIn);
	}

	
	float fYBezierPoint = (float)(3*t*t *(1-t) + t*t*t);

	return fYBezierPoint;
}

std::vector<VfaSegment::VfaVTStation*> VfaSegment::GetStations() {
	std::vector<VfaSegment::VfaVTStation*> vecStations;
	vecStations.clear();

	if(!m_bPrepared ){
		vstr::errp()<<"[VfaSegment] Not yet prepared"<<std::endl;
		return vecStations;
	}
	
	VfaVTStation* pStation = new VfaVTStation();

	if(m_vecStations.size()>=1) {
		pStation->fParameter=0.0f;
		pStation->v3Pos = m_vecStations.at(0);
		vecStations.push_back(pStation);
	}
	if (m_vecStations.size()>=2) {
		pStation = new VfaVTStation();
		pStation->fParameter=1.0f;
		pStation->v3Pos = m_vecStations.at(1);
		vecStations.push_back(pStation);
	}
	if (m_vecStations.size()>=3) {
		vstr::errp()<<"[VfaSegment] This GetStations method can't be used for this Segment type you have to implement your own!!!"<<std::endl;
	}
		
	return vecStations;
}

VistaPropertyList* VfaSegment::CreatePropertyList() {
	VistaPropertyList* pList = new VistaPropertyList();
	pList->SetValue("Type",(int)VT_SEG_DEFAULT);
	pList->MergeWith(*CreateSegmentPropertyList());
	return pList;
}

VistaPropertyList* VfaSegment::CreateSegmentPropertyList() {
	VistaPropertyList* pList = new VistaPropertyList();
	pList->SetValue("Stations",m_vecStations.size());
	pList->SetValue("EaseIn",m_fEaseIn);
	pList->SetValue("EaseOut",m_fEaseOut);
	pList->SetValue("PauseAtEnd",m_bPauseAtEnd);
	pList->SetValue("PauseAtEndTime",m_fPauseAtEndTime);
	for(int i=0; i<m_vecStations.size(); i++) {
		pList->SetValue("Station"+convertInt(i),m_vecStations[i]);
	}
	return pList;
}

void VfaSegment::SetPropertyListValues(VistaPropertyList* pPropList){

	if(!pPropList) {
		vstr::errp()<<"[VfaSegment] SetPropertyListValues (called from some Segment constructor) called with invalid PropertyList! Default values are used."<<std::endl;
		pPropList = new VistaPropertyList();
	}


	SetFadeInLength(pPropList->GetValueOrDefault("EaseIn", 0.0f));
	SetFadeOutLength(pPropList->GetValueOrDefault("EaseOut", 1.0f));

	m_vecStations.clear();
	for(int i=0; i<pPropList->GetValueOrDefault("Stations", 0); i++){
		AddStation(pPropList->GetValueOrDefault("Station"+convertInt(i), VistaVector3D(0.0,0.0,0.0)));		
	}

	m_bPauseAtEnd = pPropList->GetValueOrDefault("PauseAtEnd", false);
	m_fPauseAtEndTime = pPropList->GetValueOrDefault("PauseAtEndTime", 0.0f);

	ResetPreparedFlag();
}


std::string VfaSegment::convertInt(int number)
{
   std::stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}
// ========================================================================== //
// === End of File
// ========================================================================== //
