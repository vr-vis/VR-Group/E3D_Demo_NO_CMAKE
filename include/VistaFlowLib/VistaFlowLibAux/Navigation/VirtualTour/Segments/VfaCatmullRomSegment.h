/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFACATMULLROMSEGMENT_H
#define _VFACATMULLROMSEGMENT_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

// ViSTA stuff
#include <VistaBase/VistaVectorMath.h>
#include <VistaMath/VistaEaseCurve.h>
#include <VistaMath/VistaMatrix.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

//Virtual Tour
#include "VfaSegment.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* This class creates a catmullrom segment, which is a spline that passes through all our control points
* and is C1 continuous, meaning that there are no discontinuities in the tangent direction and magnitude
*
* However, the first and last added Stations are not part of the path, they are just used to determine the
* the direction and magnitude of the tangente at the first real (in fact 2nd) or last real station.
* With help of these pseudo stations it is possible to create a smooth path, because you just set the station
* where the path was coming from before (the segment before this segment) and as well, after.
*
* Therefor, to work it would need at least 4 control points, if you have less or want all your added stations
* actually to appear in your path, you should use the VfaVTCatmullRomPathContsructor which simply adds two pseudo
* stations (one at the beginning, one a the end) simply by taking the direction between the 2nd and 3rd station
*
 */
class VISTAFLOWLIBAUXAPI VfaCatmullRomSegment :	public VfaSegment
{
public:

	VfaCatmullRomSegment(void);
	VfaCatmullRomSegment(VistaPropertyList* pPropList);
	~VfaCatmullRomSegment(void);

	float GetSegmentLength();

	/**
	 * Returns classname as string
	 */
	std::string GetClassName();

	/**
	 * make all preparations (will be called automatically if not m_bPrepared)
	 */
	bool Preparation();

	/**
	 *	Adds a new station, which will always be appended at the end, returns the index of the new added station (starting with 0)
	 *  
	 */
	int AddStation(VistaVector3D v3Station);
	int AddStation(VistaVector3D v3Station, float fPauseTime);

	void DeleteLastStation();

	VistaVector3D GetFirstRealStation();
	VistaVector3D GetLastRealStation();

	std::vector<VfaSegment::VfaVTStation*> GetStations();

	int GetTypeID();

	VistaPropertyList* CreatePropertyList();
	bool UpdateWithPropertyList(VistaPropertyList* pList);

private:

	VistaVector3D GetPosition(float fParm, float &fPauseTime);

	/**
	 * choose four point to compute the spline
	 */
	virtual bool BuildCurvePtMatrix();

	/**
	 * get the approximated length of the current segment
	 */
	virtual float GetArcLengthBetween2Pt();

	/**
	 * Define number of curve points in one section
	 * used to approximate length of the segment
	 * default: 10.000
	 */
	void SetInnerCount(int i);

	/**
	 * Get number of curve points in a segment
	 */
	int GetInnerCount() const;

	/**
	 * compute duration and timedeltas of one segment corresponding to the 
	 * whole spline
	 */
	virtual void ComputeParameterSteps();

	int GetIntervalForParameter(float fParam);

	std::vector<VistaMatrix<float> >	m_vecPtMatrices;
	std::vector<float>				m_vecLengthOfOneArc;
	int								m_iInnerCount;
	
	/** potencies of the current parameter (f^3, f^2, f, 1) */
	VistaMatrix<float>				m_matU;
	VistaMatrix<float>				m_matM;
	std::vector<float>				m_vecParam;

	std::vector<float>				m_vecStationsPauseTime;
	int								m_iLastPauseSeg;
	std::vector<std::map<float,float> > m_vecMapLengthToParameter; 

};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _VFACATMULLROMSEGMENT_H */
