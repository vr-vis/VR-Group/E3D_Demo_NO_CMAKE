/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFACIRCULARSEGMENT_H
#define _VFACIRCULARSEGMENT_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

// ViSTA stuff
#include <VistaBase/VistaVectorMath.h>
#include <VistaMath/VistaEaseCurve.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

//Virtual Tour
#include "VfaSegment.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*
* This creates a circular segment from start to end around a given Rotation Center
* Normally the segment would start from start and reach end but rather on ellipse than
* on a real circle because the distance between start and center and end and the center
* (the radius) don't have to be the same.
*
* However, if you set the ForceCircle flag, the start and end points are moved along the
* direction line to the center, so that the distances are the same. The Virtual Tour will, if
* the FillTheGaps flag is set, fill the resulting gaps automatically
*
* As well we normally use 0,1,0 as rotation axis instead of the correct axis definded by the 
* points an the rotation center. However if you want it to be calculated correctly, you can set that
* flag, also for the CAVE mode it is more suitable to do it that way.
 */
class VISTAFLOWLIBAUXAPI VfaCircularSegment :	public VfaSegment
{
public:

	VfaCircularSegment(bool bForceCircle=false);
	VfaCircularSegment(VistaVector3D v3Start, VistaVector3D v3End, VistaVector3D v3Center, bool bForceCircle=false);
	VfaCircularSegment(VistaPropertyList* pPropList);
	~VfaCircularSegment(void);

	float GetSegmentLength();

	/**
	 * Returns classname as string
	 */
	std::string GetClassName();

	int GetTypeID();

	/**
	 * make all preparations (will be called automatically if not m_bPrepared)
	 */
	bool Preparation();

	/**
	 *	Adds a new station, which will always be appended at the end, returns the index of the new added station (starting with 0)
	 *  only two stations are allowed
	 */

	/* *
	 * The two points don't have to be on a circle around the rotation center
	 * so what we do is, calculate a point that is on the same circle as the
	 * start point and on the axis between the center and our endpoint
	 * and interpolate between these two points. And in each step we
	 * do a linear interpolation between this virtual point and our endpoint
	 * because we need to exactly reach the endpoint.
	 */
	int AddStation(VistaVector3D v3Station);

	/**
	 * Set the center of the rotation
	 */
	void SetRotationCenter(const VistaVector3D v3Pos);

	/**
	 * Get the center of the rotation
	 */
	void GetRotationCenter(VistaVector3D &v3Pos);


	// we would normally use 0,1,0 as rotation-axis if you want to force the axis to be computed correctly use this function.
	//this is usefull if you for example want the position path to be a bow as a bridge. However 0,1,0 is the better solution
	//in most of the cases, especially if the CAVE-mode is active!
	void ForceCorrectRotationAxis(bool bForce);
	bool IsCorrectRotationAxisForced();

	VistaVector3D GetFirstRealStation();
	VistaVector3D GetLastRealStation();

	VistaPropertyList* CreatePropertyList();
	bool UpdateWithPropertyList(VistaPropertyList* pList);

private:

	float SimulateSegmentForLength(int steps);

	VistaVector3D GetPosition(float fParm, float &fPauseTime);

	static const int		SIMULATIONSTEPS = 100;

	VistaVector3D	m_v3RotationCenter;
	float			m_fAngle;
	float			m_fRadius;
	VistaVector3D	m_v3VirtualPoint;
	VistaVector3D	m_v3LinearDistance;
	VistaVector3D	m_v3RotationAxis;

	float			m_fStartHeight;
	float			m_fHeightDifference;

	bool			m_bForceCircle;
	bool			m_bCircleBetweenIdenticalPoints;
	bool			m_bForceCorrectRotationAxis;

	//this vectors are Station 0 and the rotation center, but with y-componente=0.0
	VistaVector3D	m_v3YLessStation0;
	VistaVector3D	m_v3YLessRotCenter;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _VFACIRCULARSEGMENT_H */
