

set( RelativeDir "./Navigation/VirtualTour/Segments" )
set( RelativeSourceGroup "Source Files\\Navigation\\VirtualTour\\Segments" )
set( SubDirs )

set( DirFiles
	VfaCatmullRomSegment.h
	VfaCatmullRomSegment.cpp
	VfaCircularSegment.h
	VfaCircularSegment.cpp
	VfaLinearSegment.h
	VfaLinearSegment.cpp
	VfaPointSegment.h
	VfaPointSegment.cpp
	VfaSegment.h
	VfaSegment.cpp
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()

