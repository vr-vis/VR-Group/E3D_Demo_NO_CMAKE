/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFALINEARSEGMENT_H
#define _VFALINEARSEGMENT_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

// ViSTA stuff
#include <VistaBase/VistaVectorMath.h>
#include <VistaMath/VistaEaseCurve.h>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

//Virtual Tour
#include "VfaSegment.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*
*A segment that just creates a linear interpolation between to points, so only two 
*points can be set!
 */

class VISTAFLOWLIBAUXAPI VfaLinearSegment : public VfaSegment
{
public:
	VfaLinearSegment(void);
	VfaLinearSegment(VistaVector3D v3StartPoint, VistaVector3D v3EndPoint);
	VfaLinearSegment(VistaPropertyList* pPropList);
	virtual ~VfaLinearSegment(void);

	float GetSegmentLength();

	/**
	 * Returns classname as string
	 */
	std::string GetClassName();

	int GetTypeID();

	/**
	 * make all preparations (will be called automatically if not m_bPrepared)
	 */
	bool Preparation();

	/**
	 *	Adds a new station, which will always be appended at the end, returns the index of the new added station (starting with 0)
	 *	only two stations are allowed
	 */
	int AddStation(VistaVector3D v3Station);

	VistaVector3D GetFirstRealStation();
	VistaVector3D GetLastRealStation();

	VistaPropertyList* CreatePropertyList();
	bool UpdateWithPropertyList(VistaPropertyList* pList);

private:


	VistaVector3D GetPosition(float fParm, float &fPauseTime);

	VistaVector3D m_v3DistanceVector;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _VFALINEARSEGMENT_H */
