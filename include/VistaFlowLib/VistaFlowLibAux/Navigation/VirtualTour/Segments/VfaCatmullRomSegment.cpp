/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
#include "VfaCatmullRomSegment.h"
#include <VistaMath/VistaMatrix.h>

#include <cassert>

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //

VfaCatmullRomSegment::VfaCatmullRomSegment(void) 
	:VfaSegment(0.0, 1.0)
	,m_matU(1,4)
	,m_matM(4,4)
	,m_iInnerCount(100)
	,m_iLastPauseSeg(-1)
{
	
	m_matM.SetVal( 0, 0, -1.0f ); m_matM.SetVal( 0, 1,  3.0f ); m_matM.SetVal( 0, 2, -3.0f ); m_matM.SetVal( 0, 3,  1.0f );	 
	m_matM.SetVal( 1, 0,  2.0f ); m_matM.SetVal( 1, 1, -5.0f ); m_matM.SetVal( 1, 2,  4.0f ); m_matM.SetVal( 1, 3, -1.0f );	 
	m_matM.SetVal( 2, 0, -1.0f ); m_matM.SetVal( 2, 1,  0.0f ); m_matM.SetVal( 2, 2,  1.0f ); m_matM.SetVal( 2, 3,  0.0f );	 
	m_matM.SetVal( 3, 0,  0.0f ); m_matM.SetVal( 3, 1,  2.0f ); m_matM.SetVal( 3, 2,  0.0f ); m_matM.SetVal( 3, 3,  0.0f );
	 

	m_matU.SetNull();
	m_vecLengthOfOneArc.clear();
	m_vecParam.clear();
	m_vecPtMatrices.clear();
	m_vecStationsPauseTime.clear();
}
VfaCatmullRomSegment::VfaCatmullRomSegment(VistaPropertyList *pPropList)
	:VfaSegment(0.0, 1.0)
	,m_matU(1,4)
	,m_matM(4,4)
	,m_iInnerCount(100)
	,m_iLastPauseSeg(-1)
{
	SetPropertyListValues(pPropList);
	if(pPropList->GetValueOrDefault("Type",(int)VT_SEG_DEFAULT)!=(int)VT_SEG_CATMULLROM){
		vstr::warnp()<<"[VfaCatmullRomSegment] Created with wrong type of propList. Everything set to default!"<<std::endl;
	}


	m_matM.SetVal( 0, 0, -1.0f ); m_matM.SetVal( 0, 1,  3.0f ); 
	m_matM.SetVal( 0, 2, -3.0f ); m_matM.SetVal( 0, 3,  1.0f ); 
	m_matM.SetVal( 1, 0,  2.0f ); m_matM.SetVal( 1, 1, -5.0f ); 
	m_matM.SetVal( 1, 2,  4.0f ); m_matM.SetVal( 1, 3, -1.0f ); 
	m_matM.SetVal( 2, 0, -1.0f ); m_matM.SetVal( 2, 1,  0.0f ); 
	m_matM.SetVal( 2, 2,  1.0f ); m_matM.SetVal( 2, 3,  0.0f ); 
	m_matM.SetVal( 3, 0,  0.0f ); m_matM.SetVal( 3, 1,  2.0f ); 
	m_matM.SetVal( 3, 2,  0.0f ); m_matM.SetVal( 3, 3,  0.0f ); 

	m_matU.SetNull();
	m_vecLengthOfOneArc.clear();
	m_vecParam.clear();
	m_vecPtMatrices.clear();
	m_vecStationsPauseTime.clear();

	m_vecStationsPauseTime.clear();
	for(int i=0; i<pPropList->GetValueOrDefault("Stations",0); i++) {
		m_vecStationsPauseTime.push_back( pPropList->GetValueOrDefault("StationPauseTime"+convertInt(i), 0.0f));
	}
}

VfaCatmullRomSegment::~VfaCatmullRomSegment(void)
{}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/* ==========================================================================*/
/* NAME: GetSegmentLength() 												 */
/* ==========================================================================*/
float VfaCatmullRomSegment::GetSegmentLength(){
	if(Preparation())
		return m_fLength;
	else
		return -1.0;

}

/* ==========================================================================*/
/* NAME: GetClassName()														 */
/* ==========================================================================*/
std::string VfaCatmullRomSegment::GetClassName(){
	return "VfaCatmullRomSegment";
}

/* ==========================================================================*/
/* NAME: Preparation()														 */
/* ==========================================================================*/
bool VfaCatmullRomSegment::Preparation(){
	if(m_bPrepared)
		return true;

	// first of all clear matrices
	m_matU.SetNull();
	m_vecPtMatrices.clear();
	m_vecParam.clear();
	m_vecLengthOfOneArc.clear();
	m_fLength = 0.0f;
	m_iLastPauseSeg = -1;
	m_vecMapLengthToParameter.clear();

	// check for correct number of control points
	size_t iSize = m_vecStations.size();
	if( iSize < 4 )
	{
		vstr::errp() << "[VfaCatmullRomSegment] You need at least 2 control points for"
			" CATMULL_ROM interpolation!" << std::endl;
		return false;
	}

	if (m_fPauseAtEndTime != 0.0){
		//Segment Pause time at the end has a higher priority than the station pause time.
		int iSize = (int)m_vecStationsPauseTime.size();
		m_vecStationsPauseTime[iSize-2] = 0.0f;
	}

	BuildCurvePtMatrix();
	ComputeParameterSteps();

	m_bPrepared = true;

	AlterLengthForEaseCurve();


	//this could help finding the problme with the catmullrom velocity because these lengths aren't the same
	//vstr::warni()<<"Intern length: "<<m_fLength<<std::endl;

	//VistaVector3D v3vec1, v3vec2;
	//float fPauseTime, fLength;
	//v3vec1 = GetPosition(0.0f, fPauseTime);
	//fLength=0.0f;

	//for(float i=0.01f; i<=1.0f; i+=0.01f) {
	//	v3vec2 = GetPosition(i, fPauseTime);
	//	fLength += (v3vec2 -v3vec1).GetLength();
	//	v3vec1 = v3vec2;
	//}
	//vstr::warni()<<"Simulated length: "<<fLength<<std::endl;


	return m_bPrepared;
}

/* ==========================================================================*/
/* NAME: AddStation(VistaVector3D &v3Station)				 				 */
/* ==========================================================================*/
int VfaCatmullRomSegment::AddStation(VistaVector3D v3Station){

	//vstr::outi()<<"Catmullrom station added at: "<<v3Station<<std::endl;

	return AddStation(v3Station, 0.0f);
	
}

int VfaCatmullRomSegment::AddStation(VistaVector3D v3Station, float fStationPauseTime){

	m_bPrepared=false;

	m_vecStationsPauseTime.push_back(fStationPauseTime);
	m_vecStations.push_back(v3Station);
	//vstr::debugi()<<"Added station number: "<<m_vecStations.size()-1<<" to a catmullrom segment."<<std::endl;

	return ((int)m_vecStations.size()-1);

}

void VfaCatmullRomSegment::DeleteLastStation() {
	if((int)m_vecStations.size()>=1) {
		m_vecStations.pop_back();
		m_vecStationsPauseTime.pop_back();
	}
}

/* ==========================================================================*/
/* NAME: GetPosition(float fParam)							 				 */
/* ==========================================================================*/
VistaVector3D VfaCatmullRomSegment::GetPosition(float fParam, float &fPauseTime){

	if(!Preparation())
		vstr::warnp()<<"[VfaCatmullRomSegment] Preparation of this CatmullRomSegment failed!"<<std::endl;	

	//check the pause on the station;
	fPauseTime = 0.0f;

	// first of all find out which segment is done in the moment;
	int iSegment = this->GetIntervalForParameter(fParam);

	

	// re-parameterize parameter to range [0,1] for interval
	// [iSegment,iSegment+1]
	float fTempParm = fParam;
	fParam = (fParam - m_vecParam[iSegment])/(m_vecParam[iSegment+1]-m_vecParam[iSegment]);	

	
	if (m_iLastPauseSeg != iSegment){
		if(m_iLastPauseSeg== iSegment-1 || m_iLastPauseSeg== iSegment+1) {
			fPauseTime = m_vecStationsPauseTime[iSegment];
			m_iLastPauseSeg = iSegment;
		}
		else
			m_iLastPauseSeg=iSegment;
	}

	//the problem is that the parameter isn't equivalent to the arc distance, because with different tangents magnitudes at both
	//sides of this segment the velocity might differ a lot within the segment.
	if(fParam<1.0f) {
		float fDesiredLength = fParam * m_vecLengthOfOneArc[iSegment];
		std::map<float,float>::iterator it = m_vecMapLengthToParameter[iSegment].begin();
		while(it->first<=fDesiredLength) {
			it++;
		}
		float fBiggerLength = it->first;
		float fBiggerParam = it->second;
		it--;

		fParam = it->second + (fBiggerParam - it->second)*((fDesiredLength - it->first)/(fBiggerLength - it->first));
	}
	

	
	m_matU.SetVal( 0, 0, pow(fParam,3) );
	m_matU.SetVal( 0, 1, pow(fParam,2) );
	m_matU.SetVal( 0, 2, fParam );
	m_matU.SetVal( 0, 3, 1 );

	VistaMatrix<float> mat_point( 1, 3 );
	mat_point = (m_matU * m_matM) * m_vecPtMatrices[iSegment];

	VistaVector3D vec_point;
	vec_point[0] = mat_point.GetVal( 0, 0 ) * 0.5f;
	vec_point[1] = mat_point.GetVal( 0, 1 ) * 0.5f;
	vec_point[2] = mat_point.GetVal( 0, 2 ) * 0.5f;

	assert(vec_point[0] == vec_point[0]);
	assert(vec_point[1] == vec_point[1]);
	assert(vec_point[2] == vec_point[2]);

	return vec_point;
}

/* ==========================================================================*/
/* NAME: BuildCurvePtMatrix				 									 */
/* ==========================================================================*/
bool VfaCatmullRomSegment::BuildCurvePtMatrix()
{
	std::vector<VistaVector3D>::iterator itEnd = m_vecStations.end();
	itEnd--; itEnd--; itEnd--;

	for (std::vector<VistaVector3D>::iterator it = m_vecStations.begin() ;
		it != itEnd; )
	{

		VistaVector3D p0, p1, p2, p3;

		std::vector<VistaVector3D>::iterator it_local(it);
		it++;
		p0 = (*it_local);
		++it_local;
		p1 = (*it_local);
		++it_local;
		p2 = (*it_local);
		++it_local;
		p3 = (*it_local);

		VistaMatrix<float> matB (4,3);
		matB.SetVal(0,0,p0[0]); matB.SetVal(0,1,p0[1]); 
		matB.SetVal(0,2,p0[2]);		
		matB.SetVal(1,0,p1[0]); matB.SetVal(1,1,p1[1]);
		matB.SetVal(1,2,p1[2]);
		matB.SetVal(2,0,p2[0]); matB.SetVal(2,1,p2[1]);
		matB.SetVal(2,2,p2[2]);
		matB.SetVal(3,0,p3[0]); matB.SetVal(3,1,p3[1]);
		matB.SetVal(3,2,p3[2]);

		m_vecPtMatrices.push_back(matB);

		float fArcLength = GetArcLengthBetween2Pt();
		m_vecLengthOfOneArc.push_back(fArcLength);
		m_fLength += fArcLength;
	}
	return true;
}

float VfaCatmullRomSegment::GetArcLengthBetween2Pt()
{
	float fArcLength = 0.0f;
	std::map<float,float> mapLengthParam;

	std::vector<VistaVector3D> vecCurvePoints;
	float du = 1.0f/(m_iInnerCount);

	int upper_index = m_iInnerCount;

	for( int i = 0 ; i <= upper_index ; i++ )
	{
		VistaVector3D vec_point;
		float u = i*du;

		m_matU.SetVal( 0, 0, pow(u,3) );
		m_matU.SetVal( 0, 1, pow(u,2) );
		m_matU.SetVal( 0, 2, u );
		m_matU.SetVal( 0, 3, 1 );

		VistaMatrix<float> mat_point( 1, 3 );
		mat_point = (m_matU*m_matM)*m_vecPtMatrices[m_vecPtMatrices.size()-1];

		vec_point[0] = mat_point.GetVal( 0, 0 ) * 0.5f;
		vec_point[1] = mat_point.GetVal( 0, 1 ) * 0.5f;
		vec_point[2] = mat_point.GetVal( 0, 2 ) * 0.5f;

		 

		if( i!=0 )
		{
			fArcLength+= (vecCurvePoints.at(i-1) - vec_point).GetLength();
		}



		mapLengthParam[fArcLength]=u;
		vecCurvePoints.push_back( vec_point );
	}        
	vecCurvePoints.clear();
	m_vecMapLengthToParameter.push_back(mapLengthParam);

//#ifdef DEBUG
//	vstr::debugi() << "[VfaCatmullRomSegment::GetArcLengthBetween2Pt] current length: " << fArcLength << std::endl;
//#endif

	return fArcLength;
}

void VfaCatmullRomSegment::SetInnerCount(int i)
{
	m_iInnerCount = i;
}

int VfaCatmullRomSegment::GetInnerCount() const
{
	return m_iInnerCount;
}

void VfaCatmullRomSegment::ComputeParameterSteps()
{
	// first of all compute time steps for next segment
	m_vecParam.push_back(0.0f);

	size_t iSize = m_vecLengthOfOneArc.size();
	for (size_t i = 0; i < iSize; ++i)
	{
		float fDelta = m_vecLengthOfOneArc[i]/m_fLength;
		if(i == iSize-1)
			m_vecParam.push_back(1.0f);
		else
			m_vecParam.push_back(fDelta + m_vecParam[i]);
	}
}

int VfaCatmullRomSegment::GetIntervalForParameter(float fParam)
{
	if(fParam < 0.0f || fParam > 1.0f)
		return -1;

	const int nSize = static_cast<int>(m_vecParam.size());
	int iSegment = 1;
	for (; iSegment < nSize; ++iSegment)
	{
		if (m_vecParam[iSegment] >= fParam) //find end index
			break;
	}

	return --iSegment;
}

VistaVector3D VfaCatmullRomSegment::GetFirstRealStation(){
	if (m_vecStations.size() > 2) 
		return GetStation(1);
	else
		return VistaVector3D(0,0,0);
}

VistaVector3D VfaCatmullRomSegment::GetLastRealStation(){
	int iSize = (int)m_vecStations.size();
	if (iSize > 2) 
		return GetStation(iSize - 2);
	else
		return VistaVector3D(0,0,0);
}

std::vector<VfaSegment::VfaVTStation*> VfaCatmullRomSegment::GetStations() {
	std::vector<VfaSegment::VfaVTStation*> vecStations;
	vecStations.clear();

	if(!m_bPrepared){
		vstr::errp()<<"[VfaSegment] Not yet prepared"<<std::endl;
		return vecStations;
	}
	
	for(int i=1; i<m_vecStations.size()-1; i++) {
		VfaVTStation* pStation = new VfaVTStation();
		pStation->v3Pos=GetStation(i);
		pStation->fParameter = m_vecParam[i-1];
		vecStations.push_back(pStation);
	}
	return vecStations;
}

int VfaCatmullRomSegment::GetTypeID() {
	return VT_SEG_CATMULLROM;
}

VistaPropertyList* VfaCatmullRomSegment::CreatePropertyList() {
	VistaPropertyList* pList = new VistaPropertyList();
	pList->SetValue("Type",(int)VT_SEG_CATMULLROM);

	for(int i=0; i<m_vecStationsPauseTime.size(); i++) {
		pList->SetValue("StationPauseTime"+convertInt(i),m_vecStationsPauseTime[i]);
	}

	pList->MergeWith(*CreateSegmentPropertyList());
	return pList;
}

bool VfaCatmullRomSegment::UpdateWithPropertyList(VistaPropertyList* pList) {

	if(pList->GetValueOrDefault("Type",(int)VT_SEG_DEFAULT)!=(int)VT_SEG_CATMULLROM){
		vstr::warnp()<<"[VfaCatmullRomSegment] Update with wrong type of propList. Nothing changed!"<<std::endl;
		return false;
	}
	SetPropertyListValues(pList);

	for(int i=0; i<pList->GetValueOrDefault("Stations",0); i++) {
		m_vecStationsPauseTime[i] = pList->GetValueOrDefault("StationPauseTime"+convertInt(i), 0.0f);
	}


	return true;
}


// ========================================================================== //
// === End of File
// ========================================================================== //
