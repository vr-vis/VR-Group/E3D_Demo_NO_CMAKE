/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
#include "VfaLinearSegment.h"

#include <VistaBase/VistaStreamUtils.h>


// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //

VfaLinearSegment::VfaLinearSegment(void) 
	:VfaSegment(0.0, 1.0)
{
}
VfaLinearSegment::VfaLinearSegment(VistaVector3D v3StartPoint, VistaVector3D v3EndPoint) 
	:VfaSegment(0.0, 1.0)
{
	AddStation(v3StartPoint);
	AddStation(v3EndPoint);
}
VfaLinearSegment::VfaLinearSegment(VistaPropertyList* pPropList) 
	:VfaSegment(0.0,1.0)
{
	SetPropertyListValues(pPropList);
	if(pPropList->GetValueOrDefault("Type",(int)VT_SEG_DEFAULT)!=(int)VT_SEG_LINEAR){
		vstr::warnp()<<"[VfaLinearSegment] Created with wrong type of propList. Everything set to default!"<<std::endl;
	}
	//nothing else to update
}


VfaLinearSegment::~VfaLinearSegment(void)
{}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

/* ==========================================================================*/
/* NAME: GetSegmentLength() 												 */
/* ==========================================================================*/
float VfaLinearSegment::GetSegmentLength(){
	if(Preparation())
		return m_fLength;
	else
		return -1.0;

}

/* ==========================================================================*/
/* NAME: GetClassName()														 */
/* ==========================================================================*/
std::string VfaLinearSegment::GetClassName(){
	return "VfaLinearSegment";
}

int VfaLinearSegment::GetTypeID() {
	return VT_SEG_LINEAR;
}

/* ==========================================================================*/
/* NAME: Preparation()														 */
/* ==========================================================================*/
bool VfaLinearSegment::Preparation(){
	if(m_bPrepared)
		return true;

	if(GetNumberOfStations()!=2) {
		vstr::errp()<<"[VfaLinearSegment] Exactly 2 stations are needed for a linear segment!"<<std::endl;
		m_bPrepared=false;
	}
	else {
		m_v3DistanceVector= GetStation(1)- GetStation(0);
		m_fLength=m_v3DistanceVector.GetLength();

		m_bPrepared=true;
	}
	AlterLengthForEaseCurve();
	return m_bPrepared;
}

/* ==========================================================================*/
/* NAME: AddStation(VistaVector3D &v3Station)				 				 */
/* ==========================================================================*/
int VfaLinearSegment::AddStation(VistaVector3D v3Station){

	m_bPrepared=false;

	if(GetNumberOfStations()==2) {
		vstr::warnp()<<"[VfaLinearSegment] You can just have two stations for a linear segment, use reset first."<<std::endl;
		return -1;
	}
	m_vecStations.push_back(v3Station);
	return ((int)m_vecStations.size()-1);
}

/* ==========================================================================*/
/* NAME: GetPosition(float fParam)							 				 */
/* ==========================================================================*/
VistaVector3D VfaLinearSegment::GetPosition(float fParam, float &fPauseTime){

	if(!Preparation())
		vstr::errp()<<"[VfaLinearSegment] Preparation of this linearSegment failed!"<<std::endl;

	fPauseTime = 0.0f;

	return (GetStation(0)+fParam*m_v3DistanceVector);
}

VistaVector3D VfaLinearSegment::GetFirstRealStation(){
	if (m_vecStations.size() > 0) 
		return GetStation(0);
	else
		return VistaVector3D(0,0,0);
}

VistaVector3D VfaLinearSegment::GetLastRealStation(){
	if (m_vecStations.size() > 0) 
		return GetStation((int)m_vecStations.size() - 1);
	else
		return VistaVector3D(0,0,0);
}

VistaPropertyList* VfaLinearSegment::CreatePropertyList() {
	VistaPropertyList* pList = new VistaPropertyList();
	pList->SetValue("Type",(int)VT_SEG_LINEAR);
	pList->MergeWith(*CreateSegmentPropertyList());
	return pList;
}

bool VfaLinearSegment::UpdateWithPropertyList(VistaPropertyList* pList) {

	if((int)pList->GetValueOrDefault("Type",(int)VT_SEG_DEFAULT)!=VT_SEG_LINEAR){
		vstr::warnp()<<"[VfaLinearSegment] Update with wrong type of propList. Nothing changed!"<<std::endl;
		return false;
	}
	SetPropertyListValues(pList);

	return true;
}

// ========================================================================== //
// === End of File
// ========================================================================== //
