/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFASTRUCTUREDHIERARCHYCONTROLLER_H
#define _VFASTRUCTUREDHIERARCHYCONTROLLER_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// VistaBase stuff
#include <VistaBase/VistaVectorMath.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <map>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaStructuredHierarchyModel;
class VfaStructureNode;
class VistaTexture;
class IVfaStructuredDrawStrategy;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * @todo Add doc.
 */
class VISTAFLOWLIBAUXAPI VfaStructuredHierarchyController
{
public:
	explicit VfaStructuredHierarchyController
		(VfaStructuredHierarchyModel *pModel);
	virtual ~VfaStructuredHierarchyController();

	VfaStructureNode* CreateNode(VfaStructureNode *pParent);	
	VfaStructureNode* GetRootNode() const;
	VfaStructureNode* GetNodeForId(int iId) const;

	void SetDrawCenter(const VistaVector3D &v3Center);
	void GetDrawCenter(VistaVector3D &v3Center) const;

	void SetDrawOrientation(const VistaQuaternion &qOri);
	void GetDrawOrientation(VistaQuaternion &qOri) const;

	void SetDrawExtents(float fWidth, float fHeight);
	void GetDrawExtents(float &fWidth, float &fHeight) const;

	void GetNodeCenters(std::map<int, VistaVector3D> &mapCenters) const;

	// 
	void LoadStructureFromIni(const std::string &strFilename, const std::string &strSectionName);
	VistaTexture* CreateTexture(const std::string &strFilename);
	
	// just call it, when you fill the structure per hand!
	void DefineDataStructureAsFinal(bool bFinal);
	bool StructureDefinedAsFinal() const;

	void SetDrawStrategy(IVfaStructuredDrawStrategy* pDrawStrat);
	IVfaStructuredDrawStrategy* GetDrawStrategy() const;

	VfaStructureNode* FindNodeForId(int iId, VfaStructureNode *pNode) const;

protected:

private:
	VfaStructuredHierarchyModel *m_pModel;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASTRUCTUREDHIERARCHYCONTROLLER_H
