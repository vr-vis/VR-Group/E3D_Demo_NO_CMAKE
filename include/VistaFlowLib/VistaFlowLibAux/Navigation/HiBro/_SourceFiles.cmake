

set( RelativeDir "./Navigation/HiBro" )
set( RelativeSourceGroup "Source Files\\Navigation\\HiBro" )
set( SubDirs TimerFeedback )

set( DirFiles
	VfaHiBroController.cpp
	VfaHiBroController.h
	VfaHiBroModel.cpp
	VfaHiBroModel.h
	VfaHiBroVis.cpp
	VfaHiBroVis.h
	VfaHiBroWidget.cpp
	VfaHiBroWidget.h
	VfaStructuredDrawStrategy.cpp
	VfaStructuredDrawStrategy.h
	VfaStructuredHierarchy.cpp
	VfaStructuredHierarchy.h
	VfaStructuredHierarchyController.cpp
	VfaStructuredHierarchyController.h
	VfaStructuredHierarchyModel.cpp
	VfaStructuredHierarchyModel.h
	VfaStructuredHierarchyView.cpp
	VfaStructuredHierarchyView.h
	VfaStructureNode.cpp
	VfaStructureNode.h
	VfaTreeDrawStrategy.cpp
	VfaTreeDrawStrategy.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()

