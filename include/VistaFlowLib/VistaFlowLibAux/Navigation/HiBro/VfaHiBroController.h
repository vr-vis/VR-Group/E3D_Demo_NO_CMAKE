/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

 

#ifndef _VFAHIBROCONTROLLER_H
#define _VFAHIBROCONTROLLER_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaFlowLibAux/Widgets/VfaWidgetController.h>
#include <VistaFlowLibAux/Widgets/VfaControlHandle.h>

#include <VistaFlowLib/Visualization/VflRenderToTexture.h>

#include <map>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaHiBroWidget;
class VfaHiBroVis;
class VfaHiBroModel;
struct VfaHiBroText;
class VfaStructureNode;

class VfaTimerFeedbackConfig;

class VflVisTiming;

class VfaTexturedBoxVis;
class Vfl3DTextLabel;
class VfaQuadVis;
class VflRenderToTexture::C2DViewAdapter;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * @todo Add doc!!
 * Controller to interact with the HiBro
 * Here, two kind of handles are generated and controlled:
 * 1) tree handles: 
 *		those handles are small colored handles, positioned in the top right 
 *		of the map, building the complete hierarchy
 * 2) navi handles
 *		those handles are the sized tree nodes with textured,
 *		shown in the middle column and the bottom row
 *
 *
 */

//	schematic representation of the map
//  _________________________________________
//  |	            |	2	|				| row 1
//  |		3	    |_______|		1		|	
//  |			    |	2   |				| row 2
//  |_______________|_______|_______________|
//  |	2	|	2	|	2	|	2	|	2	| row 3
//  |_______|_______|_______|_______|_______|
//  |___4___|___4___|___4___|__4____|__4____| row 4 ( smaller hight as 1-3)
//
// part 1: Overview: shoe whole hierarchy e.g. as tree
// part 2: Zoom-In: one small subtree if shown enlarged with textures
// part 3: Close-Up: one single node is shown with texture and additional note
// part 4: Addition: number of succs of the nodes in row 3 of part 3 are shown
//					as numbers or color-code

class VISTAFLOWLIBAUXAPI VfaHiBroController : public VflObserver,
												public IVfaWidgetController
{
public:

	explicit VfaHiBroController(VfaHiBroModel *pModel, 
								  VflRenderNode* pRenderNode);
	virtual ~VfaHiBroController();
	

	/**
	 * These are called on update of all registered(sensor & command) slots
	 */
	void OnSensorSlotUpdate(int iSlot,
		const VfaApplicationContextObject::sSensorFrame & oFrameData);
	void OnCommandSlotUpdate(int iSlot, const bool bSet);
	void OnTimeSlotUpdate(const double dTime);
	
	/** 
	 * This is called on transition change into(out of) state FOCUS
	 */
	void OnFocus(IVfaControlHandle* pHandle);
	void OnUnfocus();
	
	/**
	 * return an(or'ed) bitmask defining which sensor slots/command slots
	 * are used/handled by this controller
	 */
	int GetSensorMask() const;
	int GetCommandMask() const;
	bool GetTimeUpdate() const;

	
	/** ToDo */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	/**
	 * Define whether node handles are always updates by all interactions 
	 * with the tree handles
	 */
	void SetUpdateNaviHndsWithTreeHnds(bool bDo);
	bool GetUpdateNaviHndsWithTreeHnds() const;


	/** 
	* set Button Id's
	*/
	void SetButtonIDResize(int iID);
	int GetButtonIDResize() const;

	void SetButtonIDTimer(int iD);
	int GetButtonIDTimer() const;

	void SetTimerButtonAction1(int iAction);
	int GetTimerButtonAction1() const;
	
	void SetTimerButtonLongClickTime(double fTime);
	double GetTimerButtonLongClickTime() const;


	/**
	 * At which time of the long click time (in per cent!!) should the 
	 * timer be visible?
	 */
	void SetTimerButtonFeedbackStartTime(float fPerCent);
	float GetTimerButtonFeedbackStartTime() const;
	
	/**
	* time used for the animation during
	* opening and closing
	*/ 
	void SetOpenAnimationTime(double dTime);
	double GetOpenAnimationTime() const;

	/**
	* duration to wait
	* decision for map closing or translating
	*/ 
	void SetCloseWaitTime(double dTime);
	double GetCloseWaitTime() const;
	
	/**
	* time used for the linear animation of handles
	* between old and new pos in map
	*/ 
	void SetLinearAnimationTime(double dTime);
	double GetLinearAnimationTime() const;


	/** 
	 * set the offset vector
	 */
	void SetOffsetVector(const VistaVector3D& v3C);
	void GetOffsetVector(VistaVector3D &v3C) const;

	/**
	* set a scale factor for handles sizes
	*/
	void SetHandleScaleFactor(float fScale);
	float GetHandleScaleFactor() const;

	/**
	* Is the map in state active? --> true
	*/
	bool GetIsMapOpen();

	//
	// Setter and Getter for all handle colors
	//
	void SetUnselectedColor(float aColorRgba[4]);
	void GetUnselectedColor(float aColorRgba[4]) const;

	void SetSelectedColor(float aColorRgba[4]);
	void GetSelectedColor(float aColorRgba[4]) const;

	void SetCenterColorForTree(float aColorRgba[4]);
	void GetCenterColorForTree(float aColorRgba[4]) const;

	void SetHighlightColor(float aColorRgba[4]);
	void GetHighlightColor(float aColorRgba[4]) const;

	void SetColorForSuccQuads(float aColorRgba[4]);
	void GetColorForSuccQuads(float aColorRgba[4]) const;

	void SetColorForNoSuccQuads(float aColorRgba[4]);
	void GetColorForNoSuccQuads(float aColorRgba[4]) const;


	void GetMapOrientation(VistaQuaternion &q) const;


	void SetVariationToShowNodeSuccs(unsigned int iDesc);
	unsigned int GetVariationToShowNodeSuccs() const;
	

	/**
	 * create all handles used in the map
	 */
	bool CreateHandles();

	
	// IVistaObserver
	void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/**
	 * HIBRO_MAPMANIPULATION triggers changes in the map
	 * HIBRO_SZENEMANIPULATION triggers the action in the scene e.g. camMoves
	 */
	enum ETimerActions
	{
		HIBRO_MAP_MANIPULATION = 0,
		HIBRO_SCENE_MANIPULATION,
	};


	enum EVaritionsOfShowingNfOfSuccs
	{
		HIBRO_SUCCS_COLOR_QUADS = 0,
		HIBRO_SUCCS_NUMBER_WITH_ZERO,
		HIBRO_SUCCS_NUMBER_WITHOUT_ZERO,
	};

protected:

	/**************************************************************************/
	/* EVERYTHING FOR HANDLES
	/**************************************************************************/

	/**************************************************************************/
	/* GENERAL
	/**************************************************************************/
	
	void SetCorrespondingHandleHighlighted(int iID,
							std::map<IVfaCenterControlHandle*, 
							VfaStructureNode*> mapNodes, bool bHighlighted);

	void GetCurHandlePos(VistaVector3D &v3C) const;

	/**	 */
	void UpdateNaviHndsWithTreeHnds();


	/**************************************************************************/
	/* CONCERNING ZOOM-IN-REPRESENTATION
	/**************************************************************************/
	
	/**
	* Recursion to initialize all navi handles (textured quads) used in the map
	* @see CreateHandles
	*/
	void CreateNaviHnds(VfaStructureNode *pNode);
	
	/**
	* Find ids of navi nodes, which will be shown in the Zoom-In
	*/
	void GetIdsOfPresentedNaviHnds(std::vector<int> &iNodeHndIdsForMap);

	/**
	* Update the map containing all pairs <id,positions> of current 
	* node handles, which are visible in the map
	*/
	void UpdateNaviHandlePosInMap(float fTime = 1.0f);

	/** 
	 * get the row number for navi node with iId
	 * return -1 if node will be not visible 
	 */
	int GetRowForNaviNode(const std::vector<int> &vector, int iId );

	/** */
	int GetThirdRowColumnForNode(const std::vector<int> &vector, 
				int iId, int iStartC, int iEndC, bool m_bMiddleFree);

	/** Get the current position if the navi handle with the specified id */
	VistaVector3D GetHandlePositionForId(int iD);

	/** Update position of navi handles during the linear animation */
	void UpdateLinearAnimation(double dDeltaT);


	/** Set position of all the navi handles, currently visible in the map */
	void SetNaviHndsPos();
	/** Set the new Size of navi handles */
	void SetNaviHandleSize(float fhw);

	/** Set all handles invisible and disable them */
	void SetNaviHandlesAllInvisible();

		

	/**************************************************************************/
	/* CONCERNING TREE-REPRESENTATION
	/**************************************************************************/

	/**
	 * Rekursion to initialize all tree handles (quads) used in the map
	 * @see CreateHandles
	 */
	void CreateTreeHandles(VfaStructureNode *pNode,
						const std::map<int, VistaVector3D> &map);

	/**  set tree handles visible and selectable*/
	void SetTreeHndsActive(bool bVisible);
	/** set the new size of tree handles */
	void SetTreeHndsSize(float fhw);

	void UpdateTreeEdgeColors();
	void UpdateTreeHndsColor();

	void ResetSubtree(VfaStructureNode *pNode);
	void Reset(VfaStructureNode *pNode, std::list<int> &li);

	void SelectPrevious(VfaStructureNode *pNode);
	void PreviousRecursiv(VfaStructureNode *pNode, std::list<int> &li);

	void MarkSurrounding(VfaStructureNode *pNode);

	/** set new size, orientation and position of the whole tree, also the hnds*/
	void UpdateTreeModel();
	/** @see UpdateTreeModel*/
	void UpdateTreeHndPosAndSize();

	
	
	/**************************************************************************/
	/* MAP 
	/**************************************************************************/
	/**
	* Compute new handels size (for opening/closing process) 
	* @see SetNaviHandleSize
	*/
	void UpdateSize(double dDeltaT);

	void DefineColors();

	void InitTimerActionMapMani();
	void InitTimerActionSzeneMani();

	VistaVector3D GetVectorWithZOffset(const VistaVector3D &v3);


	/**************************************************************************/
	/* LOUPE
	/**************************************************************************/
	/** show enlarged texture of focused handle*/
	void InitLoupe();
	void ActivateLoupe();
	void DeactivateLoupe();
	bool CreateLoupeText();

	/** show description of focused handle, see ActivateLoupe*/
	void ActivateLoupeText();
	/** set description of focused handle invisible, see ActivateLoupe*/
	void DeactivateLoupeText();


	/**************************************************************************/
	/* SHOW SUCCS
	/**************************************************************************/
	void CreateSignForShowingNrOfSuccs();
	void ActivateSignForSuccs(float fScale = 1.0f);
	void DeactivateSignForSuccs();

	bool BuildTextForNodeSuccs();
	void ActivateTextNodeSuccs(float fTimeScale = 1.0f);
	void DeactivateTextNodeSuccs();

	bool BuildQuadsForNodeSuccs();
	void ActivateQuadsNodeSuccs(float fTimeScale = 1.0f);
	void DeactivateQuadsNodeSuccs();
	void DeleteQuadsNodeSuccs();

	/**************************************************************************/
	/* TXT
	/**************************************************************************/
	VfaHiBroText *CreateTextEntry();
	void DeleteTextEntries(int iStartIdx);
	/** */
	bool PositionAndOrientText(int iIdx, float fx = 0.0f, float fy = 0.0f, 
																float fz =0.0f);

	/**************************************************************************/
	/* STATE WORKING
	/**************************************************************************/
	void SetHiBrotoStateLinearAnimation();
	void SetHiBrotoStateAnimation();
	void SetHiBrotoStateActive();
	void SetHiBrotoStateInactive();
	void SetHiBrotoStateDisabled();

	void SetHiBroState(int iNewState);
	void PrintState(int iOldState, int iCurrentState) const;
	

private:
	enum EInternalStates
	{
		HIBRO_INACTIVE = 0,
		HIBRO_ACTIVE,
		HIBRO_ANIMATION,
		HIBRO_LINEAR_ANIMATION,
		HIBRO_DISABLED
	};

	enum EInternalEnumForActiveHnd
	{
		NAVI_HANDLE = 0,
		TREE_HANDLE,
		NONE_HANDLE
	};


	/**************************************************************************/
	/* generel
	/**************************************************************************/
	VfaHiBroModel	*m_pModel;

	/** visible feedback for timer interface */
	VfaTimerFeedbackConfig *m_pTimerFeedbackConfig;

	VfaApplicationContextObject::sSensorFrame m_oLastSensorFrame;

	/** save the last state **/
	unsigned int m_iLastState;
	/** save the current state **/
	unsigned int m_iCurrentState;

	/** distinguish between quads and numbers to represent nr of succs */
	unsigned int m_iVaritionsOfShowingNfOfSuccs;

	/** selection in the Overview leads to an update of the Zoom-In*/
	bool m_bUpdateZoomInWithOverview;

	// color coding
	float m_aUnselColorRgba[4];
	float m_aSelColorRgba[4];
	float m_aColorCenterRgba[4];
	float m_aHighlightColorRgba[4];
	float m_aSuccsColor[4];
	float m_aNoSuccsColor[4];


	/** simple offset for map center*/
	VistaVector3D m_v3MapOffset;


	/**************************************************************************/
	/* BUTTONS and TIMERS ETC.
	/**************************************************************************/
	unsigned int	m_iIDResizeButton;
	unsigned int	m_iIDTimerButton;

	bool			m_bMapMovingInSzene;
	bool			m_bTimerBTActive;
	double			m_dTimerBTLongClick;
	double			m_dTimerBtPressTime;
	float			m_fFeedbackStartTimePerCent;
	unsigned int	m_iTimerAction1;


	/** time to wait before decision closing or translate map */
	double			m_dWaitTimeOnResizeBtClick;
	/** time stemp on Resize button press*/
	double			m_dTimeOnResizeBtPress;

	/** scale factor for navi handles*/ 
	float			m_fScaleFactorForNaviHnds;

	/** vis timing used for everything dealing with time measurement*/
	VflVisTiming *m_pVisTime;
	double m_dOpenAnimationTime;
	double m_dLinearAnimationTime;
	double m_dStartTime;
	bool   m_bLastStepChange;

	bool m_bObserveWhetherMoveMap;


	/**************************************************************************/
	/* all around handles
	/**************************************************************************/
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>	m_mapNaviHnds;
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>	m_mapTreeHnds;

	/** iterator pointing to the handle, which is in focus */
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator m_itActiveHnd;

	/** storing which kind of handle is in focus */
	unsigned int m_iWidgetHndActive;

	/** id of node handle located in the center of the map*/
	unsigned int m_iIdMiddleNaviHnd;
	/** id of node handle located last in the center of the map*/
	unsigned int m_iIdLastMiddleNaviHnd;


	/** map containing the pair <id, nrSuccs> for all handles */
	std::map<int, int> m_mapLastRowNaviSuccsNrs;


	/** 
	 * map containing the pair <id, pos> for all navi handles, 
	 * which are currently visible in the map
	 */
	std::map<int, VistaVector3D> m_mapVisibleNaviHnds;

	/** 
	* store id of those navi handles, which are included in the current
	* and the new version of the Zoom-In
	*/
	std::vector<int> m_iVecTransferedNaviHndsIds;

	/**
	* store positions of nodes in m_iVecTransferedNaviHndsIds in the following order
	* currentPos m_iVecTransferedNaviHndsIds[0]
	* currentPos m_iVecTransferedNaviHndsIds[1]
	* endPos	 m_iVecTransferedNaviHndsIds[0]
	* endPos	 m_iVecTransferedNaviHndsIds[1]
	* this vector is used for linear animation
	*/
	std::vector<VistaVector3D> m_v3VecTransferedNodeHndsPos;

	



	/**************************************************************************/
	/* OVERVIEW and ADDITION (show nr of succs)
	/**************************************************************************/
	/** vis for enlarged texture of focused handle */
	VfaQuadVis *m_pLoupeVis;
	
	/**
	 * vector to store all relevant text 
	 * entry 0: text for loupe in Overview
	 * entry 1-.: optional, nr of succs if text mode is chosen
	 */
	std::vector<VfaHiBroText*> m_vecTextDescription;
	/** store quads to show nr of succs per color coding */
	std::vector<VfaQuadVis*> m_vecColQuads;


	/** integer storing in which row we have to start filling the map with node hnds*/
	unsigned int m_iMapStartInRow;
	
	
	


};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // Include guard.
