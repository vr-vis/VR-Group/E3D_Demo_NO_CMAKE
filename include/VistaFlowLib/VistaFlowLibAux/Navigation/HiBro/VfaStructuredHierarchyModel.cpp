/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#include "VfaStructuredHierarchyModel.h"
#include "VfaStructureNode.h"
#include "VfaStructuredDrawStrategy.h"

// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaStructuredHierarchyModel::VfaStructuredHierarchyModel()
:	m_pRootNode(new VfaStructureNode(0)),
	m_pDrawStrat(0),
	m_bDataFinal(false),
	m_bDrawStratFinal(false)
{
	
}

VfaStructuredHierarchyModel::~VfaStructuredHierarchyModel()
{

}

/*============================================================================*/
/*  NAME:   GetRootNode			                                              */
/*============================================================================*/
VfaStructureNode* VfaStructuredHierarchyModel::GetRootNode() const
{
	return m_pRootNode;
}

/*============================================================================*/
/*  NAME     GetDrawStrategy	                                              */
/*============================================================================*/
void VfaStructuredHierarchyModel::SetDrawStrategy(
	IVfaStructuredDrawStrategy* pDrawStart)
{
	m_pDrawStrat = pDrawStart;
	m_bDrawStratFinal = true;
}

IVfaStructuredDrawStrategy* VfaStructuredHierarchyModel::GetDrawStrategy() const
{
	return m_pDrawStrat;
}

/*============================================================================*/
/*  NAME     Structure final	                                              */
/*============================================================================*/
void VfaStructuredHierarchyModel::DefineDataStructureAsFinal(bool bFinal) 
{
	m_bDataFinal = bFinal;
}

bool VfaStructuredHierarchyModel::StructureDefinedAsFinal() const
{
	return (m_bDataFinal && m_bDrawStratFinal);
}

/*============================================================================*/
/*  END OF FILE						 										  */
/*============================================================================*/


