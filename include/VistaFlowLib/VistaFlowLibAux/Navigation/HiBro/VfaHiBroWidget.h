/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAHIBROWIDGET_H
#define _VFAHIBROWIDGET_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaHiBroVis.h"
#include "VfaHiBroController.h"
#include "VfaHiBroModel.h"

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaFlowLibAux/Widgets/VfaWidget.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaStructuredHierarchy;
class IVfaStructuredDrawStrategy;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * @todo Add doc!
 */
class VISTAFLOWLIBAUXAPI VfaHiBroWidget : public IVfaWidget
{
public:
	
	explicit VfaHiBroWidget(VflRenderNode *pRN,
							  IVfaStructuredDrawStrategy *pStrat,
							  const std::string &strFilename,
							  const std::string &strSectionName);
	virtual ~VfaHiBroWidget();
	
	/**
	 * enable/disable widget
	 * a disabled widget will not handle events AND will not be visible!
	 */
	void SetIsEnabled(bool b);
	bool GetIsEnabled() const;

	void SetIsVisible(bool b);
	bool GetIsVisible() const;

	/**
	 * retrieve the actual visual representation
	 */
	VfaHiBroModel* GetModel() const;
	VfaHiBroVis* GetView() const;
	VfaHiBroController* GetController() const;

	VfaStructuredHierarchy* GetHierarchy() const;

protected:


private:
	VfaHiBroModel			*m_pModel;
	VfaHiBroController	*m_pWidgetCtl;
	VfaHiBroVis			*m_pVis;
	bool					m_bEnabled;
	bool					m_bVisible;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // Include guard.
