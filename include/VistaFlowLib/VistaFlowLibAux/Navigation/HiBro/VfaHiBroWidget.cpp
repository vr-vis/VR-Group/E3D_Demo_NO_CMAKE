/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaHiBroWidget.h"
#include "VfaHiBroVis.h"
#include "VfaHiBroModel.h"
#include "VfaHiBroController.h"
#include "VfaStructuredHierarchy.h"
#include "VfaStructuredHierarchyController.h"

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaHiBroWidget::VfaHiBroWidget(VflRenderNode *pRN,
								   IVfaStructuredDrawStrategy *pStrat,
								   const std::string &strFilename,
								   const std::string &strSectionName)
:	IVfaWidget(pRN),
	m_pModel(new VfaHiBroModel),
	m_bEnabled(true),
	m_bVisible(true)
{
	// View.
	m_pVis = new VfaHiBroVis(m_pModel);
	if(GetRenderNode())
	{
		GetRenderNode()->AddRenderable(m_pVis);
		m_pVis->Init();
	}
	
	// Controller.
	m_pWidgetCtl = new VfaHiBroController(m_pModel, pRN);
	m_pWidgetCtl->Observe(m_pModel);
	m_pModel->Notify();

	// SetUp.
	this->GetHierarchy()->GetController()->SetDrawStrategy(pStrat);
	this->GetHierarchy()->GetController()->LoadStructureFromIni(
												strFilename, strSectionName);
	this->GetController()->CreateHandles();

}
VfaHiBroWidget::~VfaHiBroWidget()
{
	// Delete in inverse creation order (clean dependency rollback!).
	if(m_pWidgetCtl)
	{
		m_pWidgetCtl->ReleaseObserveable(m_pModel);
	}
	delete m_pWidgetCtl;

	if(m_pVis && GetRenderNode())
	{
		GetRenderNode()->RemoveRenderable(m_pVis);
	}
	delete m_pVis;

	delete m_pModel;
}	

/*============================================================================*/
/*  NAME: Set/GetIsEnabled                                                    */
/*============================================================================*/
void VfaHiBroWidget::SetIsEnabled(bool b)
{
	m_bEnabled = b;
	m_pVis->SetVisible(m_bEnabled);
	m_pWidgetCtl->SetIsEnabled(m_bEnabled);
}
bool VfaHiBroWidget::GetIsEnabled() const
{
	return m_bEnabled;
}

/*============================================================================*/
/*  NAME: Set/GetIsVisible                                                    */
/*============================================================================*/
void VfaHiBroWidget::SetIsVisible(bool b)
{
	m_bVisible = b;
	m_pVis->SetVisible(m_bVisible);

	// @todo What about the widget ctl? Shouldn't its handles become (in)visible
	//		 as well? Re-check the enable/visible dependencies!
}
bool VfaHiBroWidget::GetIsVisible() const
{
	return m_bVisible;
}

/*============================================================================*/
/*  NAME: GetModel		                                                      */
/*============================================================================*/
VfaHiBroModel* VfaHiBroWidget::GetModel() const
{
	return m_pModel;
}
/*============================================================================*/
/*  NAME: GetView		                                                      */
/*============================================================================*/
VfaHiBroVis* VfaHiBroWidget::GetView() const
{
	return m_pVis;
}
/*============================================================================*/
/*  NAME: GetController                                                       */
/*============================================================================*/
VfaHiBroController* VfaHiBroWidget::GetController() const
{
	return m_pWidgetCtl;
}

/*============================================================================*/
/*  NAME: GetHierarchy                                                        */
/*============================================================================*/
VfaStructuredHierarchy* VfaHiBroWidget::GetHierarchy() const
{
	return m_pModel->GetHierarchy();
}
/*============================================================================*/
/*  END OF FILE															      */
/*============================================================================*/


