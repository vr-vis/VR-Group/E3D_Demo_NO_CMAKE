/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFASTRUCTURENODE_H
#define _VFASTRUCTURENODE_H

#if defined(WIN32)
#pragma warning(disable: 4786)
#endif

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// VistaAspects stuff
#include <VistaAspects/VistaObserveable.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

//OGLExt stuff
#include <VistaOGLExt/VistaTexture.h>

#include <string>
#include <vector>

/*============================================================================*/
/* FORWARD DECLARATION                                                        */
/*============================================================================*/
class VistaTexture;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaStructureNode : public IVistaObserveable
{
public:
	VfaStructureNode(VfaStructureNode *pPred);
	virtual ~VfaStructureNode();

	int GetId();
	bool IsLeaf();

	// @todo Think about checking for loops when adding a new successor.
	bool AddSucc(VfaStructureNode *pNode);
	int GetNrSuccs();
	VfaStructureNode* GetSuccForIdx(int iIdx);
	VfaStructureNode* GetPred();
	
	bool SetDescriptionText(const std::string &strText);
	std::string GetDescriptionText() const;

	bool SetTexture(VistaTexture *pTex);
	VistaTexture* GetTexture();

	void Execute();

private:
	static int							s_NextId;
	int									m_iId;

	VfaStructureNode					*m_pPred;
	std::vector<VfaStructureNode *>	m_vecSucc;
	
	std::string							m_strDescrText;
	VistaTexture						*m_pTex;	
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_MoveCam_H

