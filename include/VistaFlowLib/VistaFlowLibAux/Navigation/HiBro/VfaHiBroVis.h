/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAHIBROVIS_H
#define _VFAHIBROVIS_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaHiBroModel.h"

#include <VistaBase/VistaBaseTypes.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderable.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This class visualizes the HiBroModel. It is to be refered to if you want to
 * change any visualization-related parameter, e.g. colors etc..
 * NOTE: This class' OpenGL code is not OpenGL 3.x+ compliant and relies on
 *		 the availability of an OpenGL 2.1 compatibility profile!
 */
class VISTAFLOWLIBAUXAPI VfaHiBroVis : public IVflRenderable
{
public: 
	VfaHiBroVis(VfaHiBroModel *pModel);
	virtual ~VfaHiBroVis();
	
	VfaHiBroModel *GetHiBroModel() const;

	virtual bool Init();
	virtual void DrawOpaque();

	/**
	 * the HiBro only registers for opaque drawing since it does not have
	 * transparent components
	 */
	virtual unsigned int GetRegistrationMode() const;
	void ObserverUpdate(IVistaObserveable *pObs, int msg, int ticket);

	class VISTAFLOWLIBAUXAPI VfaHiBroVisProps
	:	public VflRenderableProperties
	{
	public:
		enum eMessages
		{
			MSG_DISPLAY_TILE_FRAME_COLOR_CHANGED = VflRenderableProperties::MSG_LAST,
			MSG_DISPLAY_TILE_BACK_COLOR_CHANGED,
			MSG_DISPLAY_TILE_FRAME_WIDTH_CHANGED,
			MSG_NAVI_TILE_FRAME_COLOR_CHANGED,
			MSG_NAVI_TILE_BACK_COLOR_CHANGED,
			MSG_NAVI_TILE_FRAME_WIDTH_CHANGED,
			MSG_SUCCS_TILE_FRAME_COLOR_CHANGED,
			MSG_SUCCS_TILE_BACK_COLOR_CHANGED,
			MSG_SUCCS_TILE_FRAME_WIDTH_CHANGED,
			MSG_LAST
		};

		VfaHiBroVisProps();
		virtual ~VfaHiBroVisProps();

		bool SetDisplayTileFrameColor(float aRGBA[4]);
		void GetDisplayTileFrameColor(float aRGBA[4]) const;

		bool SetDisplayTileBackColor(float aRGBA[4]);
		void GetDisplayTileBackColor(float aRGBA[4]) const;

		bool SetDisplayTileFrameWidth(float fWidth);
		float GetDisplayTileFrameWidth() const;

		bool SetNaviTileFrameColor(float aRGBA[4]);
		void GetNaviTileFrameColor(float aRGBA[4]) const;

		bool SetNaviTileBackColor(float aRGBA[4]);
		void GetNaviTileBackColor(float aRGBA[4]) const;

		bool SetNaviTileFrameWidth(float fWidth);
		float GetNaviTileFrameWidth() const;

		bool SetSuccsTileFrameColor(float aRGBA[4]);
		void GetSuccsTileFrameColor(float aRGBA[4]) const;

		bool SetSuccsTileBackColor(float aRGBA[4]);
		void GetSuccsTileBackColor(float aRGBA[4]) const;

		bool SetSuccsTileFrameWidth(float fWidth);
		float GetSuccsTileFrameWidth() const;

	private:
		bool		CheckColor(float aRGBA[4]) const;
			
		float		m_aDispTileFrameCol[4],
					m_aDispTileBackCol[4],
					m_fDispTileFrameWidth,
					m_aNaviTileFrameCol[4],
					m_aNaviTileBackCol[4],
					m_fNaviTileFrameWidth,
					m_aSuccsTileFrameCol[4],
					m_aSuccsTileBackCol[4],
					m_fSuccsTileFrameWidth;
	};

	VfaHiBroVisProps *GetProperties() const;

protected:
	virtual VflRenderableProperties* CreateProperties() const;

	void RebuildVbo();
	void RebuildTextures();
	void ConvertColor(float aFloat[4], VistaType::byte aByte[4]) const;
	
	enum eLocalObserverIds
	{
		LOI_OWN_PROPS = 0,
		LOI_MODEL,
		LOI_LAST
	};

private:
	VfaHiBroModel		*m_pModel;
	bool				m_bInitSuccess,
						m_bRebuildVbo,		//!< Flag indicates Vbo rebuild.
						m_bRebuildTexture;	//!< Flag inidcates texture rebuild.
	VistaType::uint32				m_uiTilesVbo;		//!< Vbo holding the tile vertices.
	VistaTexture		*m_pSmallTileTex,	//!< Texture for navigation tiles.
						*m_pBigTileTex, 	//!< Texture for display tiles.
						*m_pSuccsTileTex;	//!< Texture for node succs tiles.
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // Include guard.
