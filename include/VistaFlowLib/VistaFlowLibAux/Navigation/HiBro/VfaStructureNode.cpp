/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VfaStructureNode.h"

//VistaAspects stuff
#include "VistaAspects/VistaAspectsUtils.h"

//OGLExt stuff
#include <VistaOGLExt/VistaTexture.h>


/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
int VfaStructureNode::s_NextId = 0;

VfaStructureNode::VfaStructureNode(VfaStructureNode *pPred)
:	m_pPred(pPred),
	m_strDescrText(""),
	m_pTex(NULL)
{
	m_iId = s_NextId++;
}

VfaStructureNode::~VfaStructureNode()
{
	delete m_pTex;
	m_pTex = NULL;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/* NAME : GetId                                                               */
/*============================================================================*/
int VfaStructureNode::GetId()
{
	return m_iId;
}
/*============================================================================*/
/* NAME : IsLeaf                                                             */
/*============================================================================*/
bool VfaStructureNode::IsLeaf()
{
	if (m_vecSucc.size() == 0)
		return true;
	
	return false;
}

/*============================================================================*/
/* NAME : AddSucc                                                             */
/*============================================================================*/
bool VfaStructureNode::AddSucc(VfaStructureNode *pNode)
{
	m_vecSucc.push_back(pNode);
	return true;
}
/*============================================================================*/
/* NAME : GetNrSuccs                                                          */
/*============================================================================*/
int VfaStructureNode::GetNrSuccs()
{
	return static_cast<int>(m_vecSucc.size());
}
/*============================================================================*/
/* NAME : GetSuccForIdx                                                        */
/*============================================================================*/
VfaStructureNode* VfaStructureNode::GetSuccForIdx(int idx)
{
	if ((int)m_vecSucc.size() <= idx)
		return NULL;

	return m_vecSucc[idx];
}
/*============================================================================*/
/* NAME : GetPred	                                                          */
/*============================================================================*/
VfaStructureNode* VfaStructureNode::GetPred()
{
	return m_pPred;
}

/*============================================================================*/
/* NAME : Set/GetDescriptionText                                              */
/*============================================================================*/
bool VfaStructureNode::SetDescriptionText(const std::string &strText)
{
	if (VistaAspectsComparisonStuff::StringEquals(strText, ""))
		return false;

	m_strDescrText = strText;
	return true;
}
std::string VfaStructureNode::GetDescriptionText() const
{
	return m_strDescrText;
}

/*============================================================================*/
/* NAME : Set/GetTexture                                                      */
/*============================================================================*/
bool VfaStructureNode::SetTexture(VistaTexture *pTex)
{
	if (pTex == NULL)
		return false;

	m_pTex = pTex;
	return true;
}
VistaTexture* VfaStructureNode::GetTexture()
{
	return m_pTex;
}

/*============================================================================*/
/* NAME : Execute															  */
/*============================================================================*/
void VfaStructureNode::Execute()
{
	Notify(0);
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VfaStructureNode.CPP"								          */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/
