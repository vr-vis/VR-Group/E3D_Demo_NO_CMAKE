/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

 
#ifndef _VFAHIBROMODEL_H
#define _VFAHIBROMODEL_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaFlowLibAux/Widgets/VfaTransformableWidgetModel.h>

#include <VistaBase/VistaBaseTypes.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaStructuredHierarchy;
class VfaStructureNode;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* The model of the HiBro contains all information of the structure
*/
class VISTAFLOWLIBAUXAPI VfaHiBroModel : public VfaTransformableWidgetModel
{
public:
	enum eMessages
	{
		MSG_SELECTED_NODE_CHANGED = VfaTransformableWidgetModel::MSG_LAST,
		MSG_TILE_SIZE_CHANGED,
		MSG_NUMBER_OF_TILES_CHANGED,
		MSG_HEIGHT_LAST_ROW_CHANGED,
		MSG_LAST
	};
	
	VfaHiBroModel();
	virtual ~VfaHiBroModel();

	VfaStructuredHierarchy* GetHierarchy() const;	
	void SetHierarchy(VfaStructuredHierarchy* pHierarchy);	

	/**
	 * Set the number of tiles in one row.
	 *
	 * @param uiNrTiles Number of tiles to be used.
	 * @return Returns 'true' iff the number of tiles was set successfully.
	 *		   Minimum number of tiles is 5 and must be 2n + 1 for some n in N.
	 */
	bool SetNumberOfTilesWidth(VistaType::uint32 uiNrTiles); 
	VistaType::uint32 GetNumberOfTilesWidth() const;
	VistaType::uint32 GetNumberOfTilesHeight() const;

	bool SetTileEdgeLength(float fEdgeLength);
	float GetTileEdgeLength() const;
	float GetCurrentTileEdgeLength() const;

	bool SetWidgetWidth(float fWidth);
	//! Returns GetTileEdgeLength() * GetNumberOfTilesWidth().
	float GetWidgetWidth() const;
	float GetWidgetHeight() const;
	float GetCurrentWidgetWidth() const;
	float GetCurrentWidgetHeight() const;

	void SetScaleFactorLastRowHeight(float fScale);
	float GetScaleFactorLastRowHeight() const;

	void SetSelectedNode(VfaStructureNode *pNode);
	VfaStructureNode* GetSelectedNode() const;

	virtual std::string GetReflectionableType() const;

protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	
private:
	//@todo Where to get the rootnode from?
	VfaStructuredHierarchy		*m_pHierarchy;

	VistaType::uint32						m_uiNrTilesWidth;
	float						m_fTileEdgeLength;
	VfaStructureNode			*m_pSelectedNode;
	float						m_fScaleLastRowHeight;
}; 

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // Include guard.
