/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#include <GL/glew.h>

#include "VfaHiBroVis.h"
#include "VfaHiBroModel.h"

#include <VistaBase/VistaMathBasics.h>
#include <VistaOGLExt/VistaTexture.h>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
const VistaType::uint32 TILE_TEX_BASE_SIZE	= 128;

/*============================================================================*/
/*  IMPLEMENTATION      VfaHiBroVis										  */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaHiBroVis::VfaHiBroVis(VfaHiBroModel *pModel)
	:	m_pModel(pModel),
		m_bInitSuccess(false),
		m_bRebuildVbo(true),
		m_bRebuildTexture(true),
		m_uiTilesVbo(0),
		m_pSmallTileTex(0),
		m_pBigTileTex(0),
		m_pSuccsTileTex(0)
{
	if(m_pModel)
		Observe(m_pModel, LOI_MODEL);
}

VfaHiBroVis::~VfaHiBroVis()
{
	if(GetProperties())
		ReleaseObserveable(GetProperties());
	
	if(m_pModel)
		ReleaseObserveable(m_pModel);

	if(m_pBigTileTex)
		delete m_pBigTileTex;

	if(m_pSmallTileTex)
		delete m_pSmallTileTex;

	if(m_pSuccsTileTex)
		delete m_pSuccsTileTex;

	if(m_uiTilesVbo)
		glDeleteBuffers(1, &m_uiTilesVbo);
}

/*============================================================================*/
/*  NAME:	GetHiBroModel	                                                  */
/*============================================================================*/
VfaHiBroModel *VfaHiBroVis::GetHiBroModel() const
{
	return m_pModel;
}

/*============================================================================*/
/*  NAME:	Init			                                                  */
/*============================================================================*/
bool VfaHiBroVis::Init()
{
	if(m_bInitSuccess)
		return true;

	// Call base class Init() to create property object.
	if(!GetProperties())
	{
		if(!IVflRenderable::Init())
			return false;
	}

	// Observe own property object.
	Observe(GetProperties(), LOI_OWN_PROPS);

	// Now, setup all the OpenGL resource.
	// Create a buffer object for tile vertices & bind it.
	if(!m_uiTilesVbo)
	{
		glGenBuffers(1, &m_uiTilesVbo);
	}

	if(m_uiTilesVbo)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_uiTilesVbo);
		// We'll allocate enough vbo memory for 8 vertices + texture coords
		// (2 + 2 components) but don't specify the data yet. Also, data will
		// be specified very rarely, so we'll go static draw for maximum
		// rendering performance.
		const VistaType::uint32 uiVertDataByteSize = 12 * 4 * sizeof(float);
		glBufferData(GL_ARRAY_BUFFER, uiVertDataByteSize, 0, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Signal vbo rebuild (to generate actual vertex pos data).
	}
	else
	{
		return false;
	}
	m_bRebuildVbo = true;

	// Create textures.
	// Generate managed VistaTexture.
	if(!m_pSmallTileTex)
	{
		m_pSmallTileTex = new VistaTexture(GL_TEXTURE_2D);
	}

	if(m_pSmallTileTex)
	{
		m_pSmallTileTex->Bind();
		// Set texture parameters.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Allocate some texture memory but don't fill it yet.
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TILE_TEX_BASE_SIZE,
			TILE_TEX_BASE_SIZE, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		m_pSmallTileTex->Unbind();
	}
	else
	{
		return false;
	}

	// Do the same for the big tile texture.
	// Generate managed VistaTexture.
	if(!m_pBigTileTex)
	{
		m_pBigTileTex = new VistaTexture(GL_TEXTURE_2D);
	}

	if(m_pBigTileTex)
	{
		m_pBigTileTex->Bind();
		// Set texture parameters.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Allocate some texture memory but don't fill it yet.
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TILE_TEX_BASE_SIZE,
			TILE_TEX_BASE_SIZE, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		m_pBigTileTex->Unbind();
	}
	else
	{
		return false;
	}	

	// And agin the same for the succs tile texture.
	// Generate managed VistaTexture.
	if(!m_pSuccsTileTex)
	{
		m_pSuccsTileTex = new VistaTexture(GL_TEXTURE_2D);
	}

	if(m_pSuccsTileTex)
	{
		m_pSuccsTileTex->Bind();
		// Set texture parameters.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Allocate some texture memory but don't fill it yet.
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TILE_TEX_BASE_SIZE,
			TILE_TEX_BASE_SIZE, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		m_pSuccsTileTex->Unbind();
	}
	else
	{
		return false;
	}	


	// Signal texture rebuild to fill them with color data.
	m_bRebuildTexture = true;

	m_bInitSuccess = true;

	return true;
}

/*============================================================================*/
/*  NAME:	DrawOpaque		                                                  */
/*============================================================================*/
void VfaHiBroVis::DrawOpaque()
{
	
	if (!this->GetProperties()->GetVisible())
		return;

	// Rebuild vis state. Both function will determine whether it is really
	// necessary to rebuild each state.
	RebuildVbo();
	RebuildTextures();

	const VistaType::uint32 uiNumTilesWidth	= m_pModel->GetNumberOfTilesWidth(),
				 uiNumTilesHeight	= m_pModel->GetNumberOfTilesHeight();
	const float	fTileWidth		= m_pModel->GetTileEdgeLength(),
				fTileOffsetX	= static_cast<float>(uiNumTilesWidth) / 2,
				fTileOffsetY	= static_cast<float>(uiNumTilesHeight) / 2;
	float aPos[3];
	m_pModel->GetPosition(aPos);
	float aOri[4];
	m_pModel->GetOrientation(aOri);

	glPushAttrib(GL_ENABLE_BIT);

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	
	// Vbo & texture.
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glBindBuffer(GL_ARRAY_BUFFER, m_uiTilesVbo);
	glVertexPointer(2, GL_FLOAT, 4 * sizeof(float), (void*) 8);
	glTexCoordPointer(2, GL_FLOAT, 4 * sizeof(float), (void*) 0);

	glPushMatrix();
	glTranslatef(aPos[0], aPos[1], aPos[2]);

	const float fDenom = sqrt(1.0f - aOri[3] * aOri[3]);
	if(fDenom > numeric_limits<float>::epsilon())
	{
		const float fAngleRad2Deg = 2.0f * acos(aOri[3]) * 180.f / Vista::Pi;
		glRotatef(fAngleRad2Deg, aOri[0] / fDenom, aOri[1] / fDenom, aOri[2] / fDenom);
	}

	// @todo Scale for zoom here.
	float fScale = m_pModel->GetScale();
	glScalef(fScale, fScale, fScale);

	// Draw the navi tiles.
	m_pSmallTileTex->Bind();
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	for(unsigned int h=0; h<uiNumTilesHeight+1; ++h)
	{
		// Lowest level, full row.
		if(h == 0)
		{
			glPushMatrix();
			glTranslatef(-fTileOffsetX * fTileWidth,
				-fTileOffsetY * fTileWidth, 0.0f);
			
			for(unsigned int w=0; w<uiNumTilesWidth; ++w)
			{
				glDrawArrays(GL_QUADS, 0, 4);
				glTranslatef(fTileWidth, 0.0f, 0.0f);
			}
			glPopMatrix();
		}
		// Other rows, only one in the middle.
		else
		{
			glPushMatrix();
			glTranslatef(-0.5f * fTileWidth,
				(static_cast<float>(h-1) - 1.5f) * fTileWidth, 0.0f);
			glDrawArrays(GL_QUADS, 0, 4);
			glPopMatrix();
		}
	}

	// Draw the display tiles.
	m_pBigTileTex->Bind();

	glPushMatrix();
	glTranslatef(-fTileOffsetX * fTileWidth,
				-(fTileOffsetY - 1.0f) * fTileWidth, 0.0f);
	glDrawArrays(GL_QUADS, 4, 4);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.5f * fTileWidth,
				-(fTileOffsetY - 1.0f) * fTileWidth, 0.0f);
	glDrawArrays(GL_QUADS, 4, 4);
	glPopMatrix();

	m_pBigTileTex->Unbind();

	// draw the succs tiles
	m_pSuccsTileTex->Bind();
	glPushMatrix();
	glTranslatef(-fTileOffsetX * fTileWidth,
			fTileWidth*(-fTileOffsetY - m_pModel->GetScaleFactorLastRowHeight()),
			0.0f);

	for(unsigned int w=0; w<uiNumTilesWidth; ++w)
	{
		glDrawArrays(GL_QUADS, 8, 4);
		glTranslatef(fTileWidth, 0.0f, 0.0f);
	}
	glPopMatrix();
	m_pSuccsTileTex->Unbind();

	glPopMatrix();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	
	glPopAttrib();
}

/*============================================================================*/
/*  NAME:	GetRegistrationMode                                               */
/*============================================================================*/
unsigned int VfaHiBroVis::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*  NAME:	ObserverUpdate	                                                  */
/*============================================================================*/
void VfaHiBroVis::ObserverUpdate(IVistaObserveable *pObs, int msg, int ticket)
{
	if(ticket == LOI_OWN_PROPS)
	{
		switch(msg)
		{
		case VfaHiBroVisProps::MSG_DISPLAY_TILE_FRAME_COLOR_CHANGED:
		case VfaHiBroVisProps::MSG_DISPLAY_TILE_BACK_COLOR_CHANGED:
		case VfaHiBroVisProps::MSG_DISPLAY_TILE_FRAME_WIDTH_CHANGED:
		case VfaHiBroVisProps::MSG_NAVI_TILE_FRAME_COLOR_CHANGED:
		case VfaHiBroVisProps::MSG_NAVI_TILE_BACK_COLOR_CHANGED:
		case VfaHiBroVisProps::MSG_NAVI_TILE_FRAME_WIDTH_CHANGED:
		case VfaHiBroVisProps::MSG_SUCCS_TILE_FRAME_COLOR_CHANGED:
		case VfaHiBroVisProps::MSG_SUCCS_TILE_BACK_COLOR_CHANGED:
		case VfaHiBroVisProps::MSG_SUCCS_TILE_FRAME_WIDTH_CHANGED:
			m_bRebuildTexture = true;
			break;
		default:
			break;
		};
	}
	else if(ticket == LOI_MODEL)
	{
		switch(msg)
		{
		case VfaHiBroModel::MSG_NUMBER_OF_TILES_CHANGED:
			m_bRebuildVbo		= true;
			m_bRebuildTexture	= true;
			break;
		case VfaHiBroModel::MSG_TILE_SIZE_CHANGED:
			m_bRebuildVbo		= true;
			break;
		case VfaHiBroModel::MSG_HEIGHT_LAST_ROW_CHANGED:
			m_bRebuildVbo		= true;
			m_bRebuildTexture	= true;
			break;

		default:
			break;
		}; // switch(msg)
	} // if(ticket)
}

/*============================================================================*/
/*  NAME:	GetProperties	                                                  */
/*============================================================================*/
VfaHiBroVis::VfaHiBroVisProps* VfaHiBroVis::GetProperties() const
{
	return static_cast<VfaHiBroVis::VfaHiBroVisProps *>(IVflRenderable::GetProperties());
}

/*============================================================================*/
/*  NAME:	CreateProperties                                                  */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaHiBroVis::CreateProperties() const
{
	return new VfaHiBroVis::VfaHiBroVisProps();
}

/*============================================================================*/
/*  NAME:	Rebuild*()		                                                  */
/*============================================================================*/
void VfaHiBroVis::RebuildVbo()
{
	if(!m_bRebuildVbo)
		return;

	// We need 4 vertices + tex coords per tile, with 2 + 2 components each,
	// using float as type.
	const VistaType::uint32 uiDataSize = 12 * 4;
	const VistaType::uint32 uiDataByteSize = uiDataSize * sizeof(float);
	float pVertData[uiDataSize];

	const float fScale = this->GetHiBroModel()->GetScaleFactorLastRowHeight();
	
	const float fBigTileWidthFac	= static_cast<float>(
		(m_pModel->GetNumberOfTilesWidth() - 1) / 2);
	const float fBigTileHeightFac	= static_cast<float>(
		m_pModel->GetNumberOfTilesHeight() - 1);
	const float fSmallTileWidth		= m_pModel->GetTileEdgeLength();

	//--------------------------------------------------------------------------
	// Define navi (small) tile vertices.
	// Lower, left, texture.
	pVertData[0] = 0.0f;
	pVertData[1] = 0.0f;
	// Lower, left, vertex.
	pVertData[2] = 0.0f;
	pVertData[3] = 0.0f;

	// Lower, right, texture.
	pVertData[4] = 1.0f;
	pVertData[5] = 0.0f;
	// Lower, right, vertex.
	pVertData[6] = fSmallTileWidth;
	pVertData[7] = 0.0f;

	// Upper, right, texture.
	pVertData[8] = 1.0f;
	pVertData[9] = 1.0f;
	// Upper, right, vertex.
	pVertData[10] = fSmallTileWidth;
	pVertData[11] = fSmallTileWidth;

	// Upper, left, texture.
	pVertData[12] = 0.0f;
	pVertData[13] = 1.0f;
	// Upper, left, vertex.
	pVertData[14] = 0.0f;
	pVertData[15] = fSmallTileWidth;

	//--------------------------------------------------------------------------
	// Define display (big) tile vertices.
	// Lower, left, texture.
	pVertData[16] = 0.0f;
	pVertData[17] = 0.0f;
	// Lower, left, vertex.
	pVertData[18] = 0.0f;
	pVertData[19] = 0.0f;

	// Lower, right, texture.
	pVertData[20] = 1.0f;
	pVertData[21] = 0.0f;
	// Lower, right, vertex.
	pVertData[22] = fBigTileWidthFac * fSmallTileWidth;
	pVertData[23] = 0.0f;

	// Upper, right, texture.
	pVertData[24] = 1.0f;
	pVertData[25] = 1.0f;
	// Upper, right, vertex.
	pVertData[26] = fBigTileWidthFac * fSmallTileWidth;
	pVertData[27] = fBigTileHeightFac * fSmallTileWidth;

	// Upper, left, texture.
	pVertData[28] = 0.0f;
	pVertData[29] = 1.0f;
	// Upper, left, vertex.
	pVertData[30] = 0.0f;
	pVertData[31] = fBigTileHeightFac * fSmallTileWidth;

	//--------------------------------------------------------------------------
	// Define succs tile vertices.
	// Lower, left, texture.
	pVertData[32] = 0.0f;
	pVertData[33] = 0.0f;
	// Lower, left, vertex.
	pVertData[34] = 0.0f;
	pVertData[35] = 0.0f;

	// Lower, right, texture.
	pVertData[36] = 1.0f;
	pVertData[37] = 0.0f;
	// Lower, right, vertex.
	pVertData[38] = fSmallTileWidth;
	pVertData[39] = 0.0f;

	// Upper, right, texture.
	pVertData[40] = 1.0f;
	pVertData[41] = 1.0f;
	// Upper, right, vertex.
	pVertData[42] = fSmallTileWidth;
	pVertData[43] = fSmallTileWidth*fScale;

	// Upper, left, texture.
	pVertData[44] = 0.0f;
	pVertData[45] = 1.0f;
	// Upper, left, vertex.
	pVertData[46] = 0.0f;
	pVertData[47] = fSmallTileWidth*fScale;

	glBindBuffer(GL_ARRAY_BUFFER, m_uiTilesVbo);
	//float *pVbo = (float*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	//memcpy(pVbo, pVertData, uiDataByteSize);
	//glUnmapBuffer(GL_ARRAY_BUFFER);
	glBufferData(GL_ARRAY_BUFFER, uiDataByteSize, pVertData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	m_bRebuildVbo = false;
}

void VfaHiBroVis::RebuildTextures()
{
	// @todo If texture rebuilds are triggered rather frequently, it would be
	//		 advisable to cache texture sizes and only resize them if really
	//		 necessary.
	if(!m_bRebuildTexture || !GetProperties())
		return;

	VistaType::uint32	uiGeneralFrameWidth, uiDataIdx;
	float	aColorBuffer[4];
	VistaType::byte	aFrameColor[4],
			aBackColor[4],
			*pColData;


	// we prefer a gerneal frame width, so all tiles have the same one
	uiGeneralFrameWidth = static_cast<VistaType::uint32>(GetProperties()->GetNaviTileFrameWidth()
		* static_cast<float>(TILE_TEX_BASE_SIZE));

	// Rebuild navi tile texture.
	GetProperties()->GetNaviTileFrameColor(aColorBuffer);
	ConvertColor(aColorBuffer, aFrameColor);

	GetProperties()->GetNaviTileBackColor(aColorBuffer);
	ConvertColor(aColorBuffer, aBackColor);

	const VistaType::uint32 uiNaviTileTexSize =
		4 * TILE_TEX_BASE_SIZE * TILE_TEX_BASE_SIZE;
	pColData = new VistaType::byte[uiNaviTileTexSize];

	for(unsigned int y=0; y<TILE_TEX_BASE_SIZE; ++y)
	{
		for(unsigned int x=0; x<TILE_TEX_BASE_SIZE; ++x)
		{
			// idx = x * num_comp + y * max_x_width * num_comp
			uiDataIdx = x * 4 + y * TILE_TEX_BASE_SIZE * 4;
			
			// If close enough to the frame, use frame color.
			if(x <= uiGeneralFrameWidth || y <= uiGeneralFrameWidth
				|| x >= TILE_TEX_BASE_SIZE - uiGeneralFrameWidth
				|| y >= TILE_TEX_BASE_SIZE - uiGeneralFrameWidth)
			{
				memcpy(&pColData[uiDataIdx], aFrameColor,
					4*sizeof(pColData[0]));
			}
			// Else, use background color.
			else
			{
				memcpy(&pColData[uiDataIdx], aBackColor,
					4*sizeof(pColData[0]));
			}
		}
	}

	m_pSmallTileTex->Bind();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TILE_TEX_BASE_SIZE,
		TILE_TEX_BASE_SIZE, 0, GL_RGBA, GL_UNSIGNED_BYTE, pColData);
	glGenerateMipmap(GL_TEXTURE_2D);
	m_pSmallTileTex->Unbind();

	delete [] pColData;

	// Rebuild display tile texture.
	GetProperties()->GetDisplayTileFrameColor(aColorBuffer);
	ConvertColor(aColorBuffer, aFrameColor);

	GetProperties()->GetDisplayTileBackColor(aColorBuffer);
	ConvertColor(aColorBuffer, aBackColor);

	VistaType::uint32 uiResWidth	= TILE_TEX_BASE_SIZE * (m_pModel->GetNumberOfTilesWidth() - 1) / 2;
	VistaType::uint32 uiResHeight	= TILE_TEX_BASE_SIZE * (m_pModel->GetNumberOfTilesHeight() - 1);

	const VistaType::uint32 uiDisplayTileTexSize =	4 * uiResWidth * uiResHeight;
	pColData = new VistaType::byte[uiDisplayTileTexSize];

	for(unsigned int y=0; y<uiResHeight; ++y)
	{
		for(unsigned int x=0; x<uiResWidth; ++x)
		{
			// idx = x * num_comp + y * max_x_width * num_comp
			uiDataIdx = x * 4 + y * uiResWidth * 4;

			// If close enough to the frame, use frame color.
			if(x < uiGeneralFrameWidth || y < uiGeneralFrameWidth
				|| x >= uiResWidth - uiGeneralFrameWidth || y >= uiResHeight - uiGeneralFrameWidth)
			{
				memcpy(&pColData[uiDataIdx], aFrameColor,
					4*sizeof(pColData[0]));
			}
			// Else, use background color.
			else
			{
				memcpy(&pColData[uiDataIdx], aBackColor,
					4*sizeof(pColData[0]));
			}
		}
	}

	m_pBigTileTex->Bind();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, uiResWidth,
		uiResHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pColData);
	glGenerateMipmap(GL_TEXTURE_2D);
	m_pBigTileTex->Unbind();
	
	delete [] pColData;


	// Rebuild Succs Texture
	GetProperties()->GetSuccsTileFrameColor(aColorBuffer);
	ConvertColor(aColorBuffer, aFrameColor);

	GetProperties()->GetSuccsTileBackColor(aColorBuffer);
	ConvertColor(aColorBuffer, aBackColor);

	// we have another height, but the same width as the navi tiles
	uiResHeight	= (unsigned int) (TILE_TEX_BASE_SIZE * m_pModel->GetScaleFactorLastRowHeight());

	const VistaType::uint32 uiSuccsTileTexSize = 4 * TILE_TEX_BASE_SIZE * uiResHeight;
	pColData = new VistaType::byte[uiSuccsTileTexSize];

	for(unsigned int y = 0; y < uiResHeight; ++y)
	{
		for(unsigned int x = 0; x < TILE_TEX_BASE_SIZE; ++x)
		{
			// idx = x * num_comp + y * max_x_width * num_comp

			uiDataIdx = x * 4 + y * TILE_TEX_BASE_SIZE * 4;
			// If close enough to the frame, use frame color.
			if(x <= uiGeneralFrameWidth || y <= uiGeneralFrameWidth
				|| x >= TILE_TEX_BASE_SIZE - uiGeneralFrameWidth 
				|| y >= uiResHeight - uiGeneralFrameWidth)
			{
				memcpy(&pColData[uiDataIdx], aFrameColor,
					4*sizeof(pColData[0]));
			}
			// Else, use background color.
			else
			{
				memcpy(&pColData[uiDataIdx], aBackColor,
					4*sizeof(pColData[0]));
			}
		}
	}

	m_pSuccsTileTex->Bind();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TILE_TEX_BASE_SIZE,
		uiResHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pColData);
	glGenerateMipmap(GL_TEXTURE_2D);
	m_pSuccsTileTex->Unbind();

	delete [] pColData;

	m_bRebuildTexture = false;
}

void VfaHiBroVis::ConvertColor(float aFloat[4], VistaType::byte aByte[4]) const
{
	for(int i=0; i<4; ++i)
	{
		// Convert norm'ed float color component from [0,1] to norm'ed ubyte
		// color component in [0,255].
		aByte[i] = static_cast<VistaType::byte>(255.0f * aFloat[i]);
	}
}

/*============================================================================*/
/*  NAME:	VfaHiBroVisProps                                                */
/*============================================================================*/
VfaHiBroVis::VfaHiBroVisProps::VfaHiBroVisProps()
{
	// Display tile def vals.
	m_aDispTileFrameCol[0] = 0.4f;
	m_aDispTileFrameCol[1] = 0.4f;
	m_aDispTileFrameCol[2] = 0.5f;
	m_aDispTileFrameCol[3] = 1.0f;

	m_aDispTileBackCol[0] = 0.8f;
	m_aDispTileBackCol[1] = 0.8f;
	m_aDispTileBackCol[2] = 1.0f;
	m_aDispTileBackCol[3] = 1.0f;

	m_fDispTileFrameWidth = 0.05f;

	// Navi tile def vals.
	m_aNaviTileFrameCol[0] = 0.4f;
	m_aNaviTileFrameCol[1] = 0.4f;
	m_aNaviTileFrameCol[2] = 0.5f;
	m_aNaviTileFrameCol[3] = 1.0f;

	m_aNaviTileBackCol[0] = 0.8f;
	m_aNaviTileBackCol[1] = 0.8f;
	m_aNaviTileBackCol[2] = 1.0f;
	m_aNaviTileBackCol[3] = 1.0f;

	m_fNaviTileFrameWidth = 0.1f;

	// Succs tiles def val.
	m_aSuccsTileFrameCol[0] = 0.4f;
	m_aSuccsTileFrameCol[1] = 0.4f;
	m_aSuccsTileFrameCol[2] = 0.5f;
	m_aSuccsTileFrameCol[3] = 1.0f;

	m_aSuccsTileBackCol[0] = 0.8f;
	m_aSuccsTileBackCol[1] = 0.8f;
	m_aSuccsTileBackCol[2] = 1.0f;
	m_aSuccsTileBackCol[3] = 1.0f;

	m_fSuccsTileFrameWidth = 0.1f;


}

VfaHiBroVis::VfaHiBroVisProps::~VfaHiBroVisProps()
{
}

bool VfaHiBroVis::
VfaHiBroVisProps::SetDisplayTileFrameColor(float aRGBA[4])
{
	if(!CheckColor(aRGBA))
		return false;

	memcpy(m_aDispTileFrameCol, aRGBA, 4*sizeof(m_aDispTileFrameCol[0]));

	Notify(MSG_DISPLAY_TILE_FRAME_COLOR_CHANGED);

	return true;
}

void VfaHiBroVis::
VfaHiBroVisProps::GetDisplayTileFrameColor(float aRGBA[4]) const
{
	memcpy(aRGBA, m_aDispTileFrameCol, 4*sizeof(aRGBA[0]));
}

bool VfaHiBroVis::
VfaHiBroVisProps::SetDisplayTileBackColor(float aRGBA[4])
{
	if(!CheckColor(aRGBA))
		return false;

	memcpy(m_aDispTileBackCol, aRGBA, 4*sizeof(m_aDispTileBackCol[0]));

	Notify(MSG_DISPLAY_TILE_BACK_COLOR_CHANGED);

	return true;
}

void VfaHiBroVis::
VfaHiBroVisProps::GetDisplayTileBackColor(float aRGBA[4]) const
{
	memcpy(aRGBA, m_aDispTileBackCol, 4*sizeof(aRGBA[0]));
}

bool VfaHiBroVis::
VfaHiBroVisProps::SetDisplayTileFrameWidth(float fWidth)
{
	if(fWidth < 0.0f)
		return false;

	m_fDispTileFrameWidth = fWidth;

	Notify(MSG_DISPLAY_TILE_FRAME_WIDTH_CHANGED);

	return true;
}

float VfaHiBroVis::
VfaHiBroVisProps::GetDisplayTileFrameWidth() const
{
	return m_fDispTileFrameWidth;
}


bool VfaHiBroVis::
VfaHiBroVisProps::SetNaviTileFrameColor(float aRGBA[4])
{
	if(!CheckColor(aRGBA))
		return false;

	memcpy(m_aNaviTileFrameCol, aRGBA, 4*sizeof(m_aNaviTileFrameCol[0]));

	Notify(MSG_NAVI_TILE_FRAME_COLOR_CHANGED);

	return true;
}

void VfaHiBroVis::
VfaHiBroVisProps::GetNaviTileFrameColor(float aRGBA[4]) const
{
	memcpy(aRGBA, m_aNaviTileFrameCol, 4*sizeof(aRGBA[0]));
}

bool VfaHiBroVis::
VfaHiBroVisProps::SetNaviTileBackColor(float aRGBA[4])
{
	if(!CheckColor(aRGBA))
		return false;

	memcpy(m_aNaviTileBackCol, aRGBA, 4*sizeof(m_aNaviTileBackCol[0]));

	Notify(MSG_NAVI_TILE_BACK_COLOR_CHANGED);

	return true;
}

void VfaHiBroVis::
VfaHiBroVisProps::GetNaviTileBackColor(float aRGBA[4]) const
{
	memcpy(aRGBA, m_aNaviTileBackCol, 4*sizeof(aRGBA[0]));
}

bool VfaHiBroVis::
VfaHiBroVisProps::SetNaviTileFrameWidth(float fWidth)
{
	if(fWidth < 0.0f)
		return false;

	m_fNaviTileFrameWidth = fWidth;

	Notify(MSG_DISPLAY_TILE_FRAME_WIDTH_CHANGED);

	return true;
}

float VfaHiBroVis::
VfaHiBroVisProps::GetNaviTileFrameWidth() const
{
	return m_fNaviTileFrameWidth;
}


bool VfaHiBroVis::
VfaHiBroVisProps::SetSuccsTileFrameColor(float aRGBA[4])
{
	if(!CheckColor(aRGBA))
		return false;

	memcpy(m_aSuccsTileFrameCol, aRGBA, 4*sizeof(m_aSuccsTileFrameCol[0]));

	Notify(MSG_SUCCS_TILE_FRAME_COLOR_CHANGED);

	return true;
}

void VfaHiBroVis::
VfaHiBroVisProps::GetSuccsTileFrameColor(float aRGBA[4]) const
{
	memcpy(aRGBA, m_aSuccsTileFrameCol, 4*sizeof(aRGBA[0]));
}

bool VfaHiBroVis::
VfaHiBroVisProps::SetSuccsTileBackColor(float aRGBA[4])
{
	if(!CheckColor(aRGBA))
		return false;

	memcpy(m_aSuccsTileBackCol, aRGBA, 4*sizeof(m_aSuccsTileBackCol[0]));

	Notify(MSG_SUCCS_TILE_BACK_COLOR_CHANGED);

	return true;
}

void VfaHiBroVis::
VfaHiBroVisProps::GetSuccsTileBackColor(float aRGBA[4]) const
{
	memcpy(aRGBA, m_aSuccsTileBackCol, 4*sizeof(aRGBA[0]));
}

bool VfaHiBroVis::
VfaHiBroVisProps::SetSuccsTileFrameWidth(float fWidth)
{
	if(fWidth < 0.0f)
		return false;

	m_fSuccsTileFrameWidth = fWidth;

	Notify(MSG_DISPLAY_TILE_FRAME_WIDTH_CHANGED);

	return true;
}

float VfaHiBroVis::
VfaHiBroVisProps::GetSuccsTileFrameWidth() const
{
	return m_fSuccsTileFrameWidth;
}








bool VfaHiBroVis::
VfaHiBroVisProps::CheckColor(float aRGBA[4]) const
{
	for(int i=0; i<4; ++i)
	{
		if(aRGBA[i] < 0.0f || aRGBA[i] > 1.0f)
			return false;
	}

	return true;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE						                                          */
/*============================================================================*/




