/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#include "VfaStructuredHierarchy.h"
#include "VfaStructuredHierarchyModel.h"
#include "VfaStructuredHierarchyView.h"
#include "VfaStructuredHierarchyController.h"

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>

/*============================================================================*/
/*  CONSTRUCTORS/DESTUCTORS                                                   */
/*============================================================================*/
VfaStructuredHierarchy::VfaStructuredHierarchy(VflRenderNode* pRenderNode)
	:	m_pRenderNode(pRenderNode)
{
	m_pModel		= new VfaStructuredHierarchyModel;
	m_pController	= new VfaStructuredHierarchyController(m_pModel);
	m_pView			= new VfaStructuredHierarchyView(m_pModel);

	if (m_pView->Init())
		m_pRenderNode->AddRenderable(m_pView);
}
VfaStructuredHierarchy::~VfaStructuredHierarchy()
{
	delete m_pController;

	if(m_pView && m_pRenderNode)
	{
		m_pRenderNode->RemoveRenderable(m_pView);
	}
	delete m_pView;
	
	delete m_pModel;
}

/*============================================================================*/
/*  NAME:   GetController   			                                      */
/*============================================================================*/	
VfaStructuredHierarchyController* VfaStructuredHierarchy::GetController()
{
	return m_pController;
}
/*============================================================================*/
/*  NAME:   GetView			   			                                      */
/*============================================================================*/	
VfaStructuredHierarchyView* VfaStructuredHierarchy::GetView()
{
	return m_pView;
}

/*============================================================================*/
/*  END OF FILE "VfaStructuredHierarchyController.cpp" 					  */
/*============================================================================*/
