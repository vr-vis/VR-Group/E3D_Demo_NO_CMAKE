/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#include "VfaStructuredHierarchyController.h"
#include "VfaStructuredHierarchyModel.h"
#include "VfaStructuredDrawStrategy.h"
#include "VfaStructureNode.h"


//VistaTools stuff
#include <VistaTools/VistaProfiler.h>

//VistaAspects stuff
#include "VistaAspects/VistaAspectsUtils.h"

//OGLExt stuff
#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaOGLUtils.h>

#include <VistaBase/VistaStreamUtils.h>


// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

#define MESSAGE

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaStructuredHierarchyController::VfaStructuredHierarchyController(
	VfaStructuredHierarchyModel *pModel)
:	m_pModel(pModel)
{
	m_pModel->DefineDataStructureAsFinal(false);
}

VfaStructuredHierarchyController::~VfaStructuredHierarchyController()
{

}

/*============================================================================*/
/*  NAME:	CreateNode		                                                  */
/*============================================================================*/
VfaStructureNode* VfaStructuredHierarchyController::CreateNode(
	VfaStructureNode *pParent)
{
	VfaStructureNode *pNode = new VfaStructureNode(pParent);
	pParent->AddSucc(pNode);
	return pNode;
}

/*============================================================================*/
/*  NAME:	GetSepecialNode                                                   */
/*============================================================================*/
VfaStructureNode* VfaStructuredHierarchyController::GetRootNode() const
{
	return m_pModel->GetRootNode();
}
VfaStructureNode* VfaStructuredHierarchyController::GetNodeForId(int iId) const
{
	VfaStructureNode* pNode = this->FindNodeForId(iId, this->GetRootNode());
	return pNode;
}

/*============================================================================*/
/*  NAME:	Set/GetDrawCenter                                                 */
/*============================================================================*/
void VfaStructuredHierarchyController::SetDrawCenter(const VistaVector3D &v3Center)
{
	m_pModel->GetDrawStrategy()->SetDrawCenter(v3Center);
}
void VfaStructuredHierarchyController::GetDrawCenter(VistaVector3D &v3Center) const
{
	m_pModel->GetDrawStrategy()->GetDrawCenter(v3Center);
}

/*============================================================================*/
/*  NAME:	Set/GetDrawOrientation                                            */
/*============================================================================*/
void VfaStructuredHierarchyController::SetDrawOrientation(
												const VistaQuaternion &qOri)
{
	m_pModel->GetDrawStrategy()->SetDrawOrientation(qOri);
}
void VfaStructuredHierarchyController::GetDrawOrientation(
												VistaQuaternion &qOri) const
{
	m_pModel->GetDrawStrategy()->GetDrawOrientation(qOri);
}

/*============================================================================*/
/*  NAME:	Set/GetDrawExtents                                                */
/*============================================================================*/
void VfaStructuredHierarchyController::SetDrawExtents(float fWidth, float fHeight)
{
	m_pModel->GetDrawStrategy()->SetDrawExtents(fWidth, fHeight);
}
void VfaStructuredHierarchyController::GetDrawExtents(float &fWidth, float &fHeight) const
{
	m_pModel->GetDrawStrategy()->GetDrawExtents(fWidth, fHeight);
}

/*============================================================================*/
/*  NAME:	GetNodeCenters                                                    */
/*============================================================================*/
void VfaStructuredHierarchyController::GetNodeCenters(
	std::map<int, VistaVector3D> &mapCenters) const
{
	//@todo
	m_pModel->GetDrawStrategy()->GetNodeCenters(mapCenters);
}


/*============================================================================*/
/*  NAME:	LoadStructureFromIni                                              */
/*============================================================================*/
void VfaStructuredHierarchyController::LoadStructureFromIni
(const std::string &strFilename, const std::string &strSectionName)
{
	vstr::outi()<<"            [VfaStructuredHierarchyController]"<<endl;
	vstr::outi()<<"_________________________________________________________" << endl;
	std::list<std::string> liNodeNames;

	VistaProfiler oProf;
	oProf.GetTheProfileList(strSectionName, "NODES", liNodeNames, strFilename);

	std::list<string>::iterator itStrNodes;
	std::string strParentName = "";
	std::string strTexture = "";
	std::string strDesc = "";
	std::map<string, VfaStructureNode*> mapNodes;

	// run through the lists of nodes:
	for (itStrNodes = liNodeNames.begin(); itStrNodes != liNodeNames.end();
																++itStrNodes)
	{
		strParentName = oProf.GetTheProfileString(*itStrNodes, "PARENT",
																 "", strFilename);
		strTexture = oProf.GetTheProfileString(*itStrNodes, "TEXTURE",
																 "", strFilename);
		strDesc = oProf.GetTheProfileString(*itStrNodes, "DESC", "", strFilename);

		if(false == strDesc.empty())
		{
			const size_t uiNewLinePos = strDesc.find("\\n");

			if(uiNewLinePos != string::npos)
			{
				strDesc.replace(uiNewLinePos, 2, "\n");
			}
		}

		VfaStructureNode *pNode = NULL;
		
		if (VistaAspectsComparisonStuff::StringEquals(strParentName, "NULL"))
		{
			pNode = this->GetRootNode();
		}
		else
		{
			std::map<string, VfaStructureNode*>::iterator it = 
							mapNodes.find(strParentName);

			if(it != mapNodes.end())
			{
				pNode = this->CreateNode(it->second);		
			}
			else
			{
#ifdef MESSAGE
				vstr::outi() << "******************************************" << endl;
				vstr::outi() << "ATTENTION:\n Node \"" << *itStrNodes << "\" with "<< strDesc << "is ignored, because node \"" <<
					strParentName << "\" is not available yet.\n So, please correct your ini file!" << endl;
				vstr::outi() << "******************************************" << endl;
#endif
			continue;
			}
		}
		
		pNode->SetTexture(this->CreateTexture(strTexture));
		pNode->SetDescriptionText(strDesc);
		vstr::outi() << pNode->GetId() << ": " << strDesc << endl;
		pair<string, VfaStructureNode*> pN(*itStrNodes,pNode);
		mapNodes.insert(pN);		
	}
	
	// the structure is ready
	m_pModel->DefineDataStructureAsFinal(true);
	vstr::outi() << "_________________________________________________________" << endl;

	// Set the initial node of the draw strategy to our root node.
	if(this->GetDrawStrategy())
	{
		this->GetDrawStrategy()->SetInitialNode(this->GetRootNode());
	}
}

/*============================================================================*/
/*  NAME:	LoadStructureFromIni                                              */
/*============================================================================*/
VistaTexture* VfaStructuredHierarchyController::CreateTexture
												(const std::string &strFilename)
{
	int iWidth;
	int iHeight;
	int iChannels;
	unsigned char* pTex = NULL;
	pTex = VistaOGLUtils::LoadImageFromFile(strFilename,iWidth,iHeight,iChannels);
	VistaTexture *pTexture = new VistaTexture(GL_TEXTURE_2D);
		
	glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	if(iChannels==3)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, iWidth, iHeight, 0, GL_RGB, 
													GL_UNSIGNED_BYTE, pTex);
	if(iChannels==4)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, iWidth, iHeight, 0, 
										GL_RGBA, GL_UNSIGNED_BYTE, pTex);

	return pTexture;
}


/*============================================================================*/
/*  NAME:	DefineDataStructureAsFinal                                            */
/*============================================================================*/
void VfaStructuredHierarchyController::DefineDataStructureAsFinal(bool bFinal)
{
	m_pModel->DefineDataStructureAsFinal(bFinal);
}

bool VfaStructuredHierarchyController::StructureDefinedAsFinal() const
{
	return m_pModel->StructureDefinedAsFinal();
}

/*============================================================================*/
/*  NAME:	Set/GetDrawStrategy                                               */
/*============================================================================*/
void VfaStructuredHierarchyController::SetDrawStrategy(
	IVfaStructuredDrawStrategy* pDrawStrat)
{
	if(m_pModel)
	{
		m_pModel->SetDrawStrategy(pDrawStrat);

		if(pDrawStrat)
		{
			pDrawStrat->SetInitialNode(this->GetRootNode());
			pDrawStrat->SetVisible(false);
		}		
	}
}

IVfaStructuredDrawStrategy* VfaStructuredHierarchyController::GetDrawStrategy()
	const
{
	IVfaStructuredDrawStrategy* pDrawStrat = 0;

	if(m_pModel)
	{
		pDrawStrat = m_pModel->GetDrawStrategy();
	}

	return pDrawStrat;
}

/*============================================================================*/
/*  NAME:	GetNodeForId                                                      */
/*============================================================================*/
VfaStructureNode* VfaStructuredHierarchyController::FindNodeForId(
	int iId, VfaStructureNode *pNode) const
{
	// found corrent node
	if (pNode->GetId() == iId)
		return pNode;

	// look at each succ carefully
	int iNrSuccs = pNode->GetNrSuccs();
	for (int i = 0; i < iNrSuccs; ++i)
	{
		// found the correct node?
		VfaStructureNode* pN = this->FindNodeForId(iId, pNode->GetSuccForIdx(i));
		if (pN != NULL)
			return pN;
	}
	// no matching node was found
	return NULL;
}

/*============================================================================*/
/*  END OF FILE																  */
/*============================================================================*/


