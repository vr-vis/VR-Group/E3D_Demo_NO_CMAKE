/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFASTRUCTEDHIERARCHYVIEW_H
#define _VFASTRUCTEDHIERARCHYVIEW_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <vector>
/*============================================================================*/
/* FORWARD DECLARATION                                                        */
/*============================================================================*/
class VfaStructuredHierarchyModel;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * @todo Add doc.
 */
class VISTAFLOWLIBAUXAPI VfaStructuredHierarchyView : public IVflRenderable
{
public:
	explicit VfaStructuredHierarchyView(VfaStructuredHierarchyModel *pModel);
	virtual ~VfaStructuredHierarchyView();

	//@todo add: drawSelected.....
	void DrawResetSubtree(const std::list<int> &liId);
	void DrawSelected(const std::list<int> &liId);
	void DrawMarkedSourrounding(const std::list<int> &liId);


	virtual void DrawOpaque();
	unsigned int GetRegistrationMode() const;

	class VISTAFLOWLIBAUXAPI VflStructHierarchyProps
	:	public VflRenderableProperties
	{
	public:
		VflStructHierarchyProps();
		virtual ~VflStructHierarchyProps();

		void SetLineWidth(float fWidth);
		float GetLineWidth() const;
		
		void SetUnselectedColor(float aColorRgba[4]);
		void GetUnselectedColor(float aColorRgba[4]) const;

		void SetSelectedColor(float aColorRgba[4]);
		void GetSelectedColor(float aColorRgba[4]) const;

		void SetMarkedColor(float aColorRgba[4]);
		void GetMarkedColor(float aColorRgba[4]) const;

	private:
		float	m_fLineWidth,
				m_aUnselColorRgba[4],
				m_aSelColorRgba[4],
				m_aMarkedColorRgba[4];
	};

	virtual VflStructHierarchyProps* GetProperties() const;

protected:
	VflRenderableProperties* CreateProperties() const;
	bool Find(std::list<int> &vec, int id);

private:
	VfaStructuredHierarchyModel	*m_pModel;
	std::list<int> m_vecIdUnsel;
	std::list<int> m_vecIdSel;
	std::list<int> m_vecIdMarked;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASTRUCTEDHIERARCHYVIEW_H
