/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFASTRUCTUREDHIERARCHYMODEL_H
#define _VFASTRUCTUREDHIERARCHYMODEL_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
//VistaBase stuff
#include <VistaBase/VistaVector3D.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

#include <vector>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaStructureNode;
class IVfaStructuredDrawStrategy;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * @todo Add doc.
 */
class VISTAFLOWLIBAUXAPI VfaStructuredHierarchyModel
{
public:
	VfaStructuredHierarchyModel();
	virtual ~VfaStructuredHierarchyModel();
	
	VfaStructureNode* GetRootNode() const;
	
	void SetDrawStrategy(IVfaStructuredDrawStrategy* pDrawStart);
	IVfaStructuredDrawStrategy* GetDrawStrategy() const;

	void DefineDataStructureAsFinal(bool bFinal);
	bool StructureDefinedAsFinal() const;

private:
	VfaStructureNode			*m_pRootNode;
	IVfaStructuredDrawStrategy	*m_pDrawStrat;

	bool						m_bDataFinal;
	bool						m_bDrawStratFinal;
}; 

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFASTRUCTUREDHIERARCHYMODEL_H
