/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES														              */
/*============================================================================*/
#include "VfaStructuredDrawStrategy.h"
#include "VfaStructureNode.h"

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
IVfaStructuredDrawStrategy::IVfaStructuredDrawStrategy()
:	m_pInitialNode(0),
	m_bVisible(false)
{
	m_v3Center.SetToZeroVector();
	m_qOri.SetToNeutralQuaternion();

	m_aExtents[0] = 1.0f;
	m_aExtents[1] = 1.0f;
}

IVfaStructuredDrawStrategy::~IVfaStructuredDrawStrategy()
{

}

/*============================================================================*/
/* NAME:	Set/GetInitialNode                                                */
/*============================================================================*/
bool IVfaStructuredDrawStrategy::SetInitialNode(VfaStructureNode *pNode)
{
	if(pNode == 0)
		return false;

	m_pInitialNode = pNode;
	this->Update();

	return true;
}

VfaStructureNode* IVfaStructuredDrawStrategy::GetInitialNode() const
{
	return m_pInitialNode;
}

/*============================================================================*/
/* NAME:	Set/GetDrawCenter                                                 */
/*============================================================================*/
void IVfaStructuredDrawStrategy::SetDrawCenter(const VistaVector3D &v3Center)
{
	m_v3Center = v3Center;
}
void IVfaStructuredDrawStrategy::GetDrawCenter(VistaVector3D &v3Center) const
{
	v3Center = m_v3Center;
}

/*============================================================================*/
/* NAME:	Set/GetDrawOrientation	                                          */
/*============================================================================*/
void IVfaStructuredDrawStrategy::SetDrawOrientation(const VistaQuaternion &qOri)
{
	m_qOri = qOri;
}
void IVfaStructuredDrawStrategy::GetDrawOrientation(VistaQuaternion &qOri) const
{
	qOri = m_qOri;
}

/*============================================================================*/
/* NAME:	Set/GetDrawExtents                                                */
/*============================================================================*/
void IVfaStructuredDrawStrategy::SetDrawExtents(float fWidth, float fHeight)
{
	m_aExtents[0] = fWidth;
	m_aExtents[1] = fHeight;
}

void IVfaStructuredDrawStrategy::GetDrawExtents(
	float &fWidth, float &fHeight) const
{
	fWidth = m_aExtents[0];
	fHeight = m_aExtents[1];
}
/*============================================================================*/
/* NAME:	Set/GetVisible		                                              */
/*============================================================================*/
void IVfaStructuredDrawStrategy::SetVisible(bool bVisible)
{
	m_bVisible = bVisible;
}
bool IVfaStructuredDrawStrategy::GetVisible() const
{
	return m_bVisible;
}



/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VfaStructuredDrawStrategy.CPP"   					          */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/
