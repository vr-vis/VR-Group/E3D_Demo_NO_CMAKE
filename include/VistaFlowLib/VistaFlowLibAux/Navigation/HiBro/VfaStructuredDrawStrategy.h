/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFASTRUCTUREDDRAWSTRATEGY_H
#define _VFASTRUCTUREDDRAWSTRATEGY_H

#if defined(WIN32)
#pragma warning(disable: 4786)
#endif



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// VistaBase stuff
#include <VistaBase/VistaVectorMath.h>

// FlowLibAux stuff
#include "../../VistaFlowLibAuxConfig.h"

#include <map>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VfaStructureNode;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * @todo Add doc!
 */
class VISTAFLOWLIBAUXAPI IVfaStructuredDrawStrategy
{
public:
	struct SEdge
	{
		VistaVector3D m_v3Start;
		VistaVector3D m_v3End;
	};

	IVfaStructuredDrawStrategy();
	virtual ~IVfaStructuredDrawStrategy();

	virtual bool SetInitialNode(VfaStructureNode *pNode);
	virtual VfaStructureNode* GetInitialNode() const;

	virtual void SetDrawCenter(const VistaVector3D &v3Center);
	virtual void GetDrawCenter(VistaVector3D &v3Center) const;

	virtual void SetDrawOrientation(const VistaQuaternion &qOri);
	virtual void GetDrawOrientation(VistaQuaternion &qOri) const;

	virtual void SetDrawExtents(float fWidth, float fHeight);
	virtual void GetDrawExtents(float &fWidth, float &fHeight) const;

	virtual void GetEdges(std::map<int, SEdge>& mapEdges) = 0;
	virtual void GetNodeCenters(std::map<int, VistaVector3D>& mapNodes) = 0;

	virtual void Update() = 0;

	void SetVisible(bool bVisible);
	bool GetVisible() const;

	//@ToDo: ADD: method to get extends for one(!) node

private:
	VfaStructureNode		*m_pInitialNode;	
	VistaVector3D			m_v3Center;
	VistaQuaternion			m_qOri;
	float					m_aExtents[2];
	bool					m_bVisible;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFASTRUCTUREDDRAWSTRATEGY_H
