/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFATREEDRAWSTRATEGY_H
#define _VFATREEDRAWSTRATEGY_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../VistaFlowLibAuxConfig.h"
#include "VfaStructuredDrawStrategy.h"

#include <map>
#include <vector>


/*============================================================================*/
/* FORWARD DECLARATION                                                        */
/*============================================================================*/
class VfaStructureNode;
struct SNodeInfo;


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI VfaTreeDrawStrategy
:	public IVfaStructuredDrawStrategy
{
public:
	VfaTreeDrawStrategy();
	virtual ~VfaTreeDrawStrategy();

	virtual void SetDrawCenter(const VistaVector3D &v3Center);
	virtual void SetDrawOrientation(const VistaQuaternion &qOri);
	virtual void SetDrawExtents(float fWidth, float fHeight);

	// int --> child; endpoint of edge
	virtual void GetEdges(
		std::map<int, IVfaStructuredDrawStrategy::SEdge>& mapEdges);
	virtual void GetNodeCenters(std::map<int, VistaVector3D> &mapNodes);

	virtual void Update();

protected:
	/**
	 * 
	 */
	void BuildNodeInfo(VfaStructureNode* pInitialNode,
		std::vector<VfaStructureNode*>& vecTraversal,
		std::map<int, SNodeInfo>& mapNodeInfo) const;
	/**
	 *
	 */
	void GenerateLayout(const std::vector<VfaStructureNode*>& vecTraversal,
		const std::map<int, SNodeInfo>& mapNodeInfo);
	/**
	 * Performs a base change, transforming the layout from the initial
	 * unit square to the supplied target rectangle.
	 */
	void ChangeLayoutRefFrame();

private:
	bool		m_bTransformChanged;
	
	//! Edges of the tree graph. Two successive vv3d constitute one edge.
	std::map<int, IVfaStructuredDrawStrategy::SEdge> m_mapEdges;
	std::map<int, VistaVector3D> m_mapNodes;
	//! Transformed, final layout is held here.
	std::map<int, IVfaStructuredDrawStrategy::SEdge> m_mapFinalEdges;
	std::map<int, VistaVector3D> m_mapFinalNodes;
};

#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
