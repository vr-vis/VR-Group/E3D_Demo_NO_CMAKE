/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// same dir
#include "VfaHiBroController.h"
#include "VfaHiBroModel.h"
#include "VfaStructureNode.h"
#include "VfaStructuredHierarchy.h"
#include "VfaStructuredHierarchyController.h"
#include "VfaStructuredHierarchyView.h"
#include "VfaStructuredDrawStrategy.h"
#include "TimerFeedback/VfaTimerFeedbackConfig.h"

// VistaFlowLibAux stuff
#include <VistaFlowLibAux/Widgets/VfaQuadHandle.h>
#include <VistaFlowLibAux/Widgets/VfaTexturedQuadHandle.h>
#include <VistaFlowLibAux/Widgets/VfaQuadVis.h>
#include <VistaFlowLibAux/Widgets/Plane/VfaPlaneModel.h>
#include <VistaFlowLibAux/Widgets/VfaTexturedBoxVis.h>
#include <VistaFlowLibAux/Widgets/Box/VfaTexturedBoxModel.h>

// VistaFlowLib stuff
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflRenderToTexture.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>

// VistaMath stuff
#include <VistaMath/VistaIndirectXform.h>

#include <cassert>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

struct VfaHiBroText
{
	Vfl3DTextLabel *m_pTextLabel;
	VfaTexturedBoxVis *m_pTexBoxVis;
	VflRenderToTexture *m_pR2T;
	VflRenderToTexture::C2DViewAdapter *m_pViewAdapter;
};

/*============================================================================*/
/*  CONSTRUCTORS/DESTUCTORS                                                   */
/*============================================================================*/
VfaHiBroController::VfaHiBroController(VfaHiBroModel *pModel,
										   VflRenderNode* pRenderNode)
:	IVfaWidgetController(pRenderNode),
	m_pModel(pModel),
	m_iIDResizeButton(0),
	m_bMapMovingInSzene(false),
	m_iIDTimerButton(1),
	m_iTimerAction1(HIBRO_MAP_MANIPULATION),
	m_bTimerBTActive(false),
	m_dTimerBTLongClick(0.30f), // 300ms
	m_fFeedbackStartTimePerCent(0.5f),
	m_dTimerBtPressTime(0.0f),
	m_iLastState(HIBRO_INACTIVE),
	m_iCurrentState(HIBRO_INACTIVE),
	m_iIdMiddleNaviHnd(0),
	m_iIdLastMiddleNaviHnd(0),
	m_fScaleFactorForNaviHnds(0.85f),
	m_itActiveHnd(m_mapNaviHnds.end()),
	m_iWidgetHndActive(NONE_HANDLE),
	m_iMapStartInRow(0),
	m_pVisTime(pRenderNode->GetVisTiming()),
	m_dOpenAnimationTime(0.5f),
	m_dLinearAnimationTime(0.5f),
	m_dStartTime(0.0),
	m_bLastStepChange(false),
	m_bObserveWhetherMoveMap(false),
	m_bUpdateZoomInWithOverview(true),
	m_dTimeOnResizeBtPress(0.0f),
	m_dWaitTimeOnResizeBtClick(0.2f),
	m_iVaritionsOfShowingNfOfSuccs(HIBRO_SUCCS_COLOR_QUADS)
{
	m_pModel->SetHierarchy(new VfaStructuredHierarchy(pRenderNode));
	this->Observe(m_pModel);

	m_pModel->SetScale(0.0f);
	m_v3MapOffset = VistaVector3D(0.0f, 0.0f, -3.0f);

	m_pTimerFeedbackConfig = new VfaTimerFeedbackConfig(pRenderNode,
									VfaTimerFeedbackConfig::MSG_PARTIALDISK);

	// define all needed colors
	DefineColors();

	// define loupe and text (shown as Close-Up)
	InitLoupe();
	CreateLoupeText();
}
VfaHiBroController::~VfaHiBroController()
{
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
				m_mapNaviHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
				m_mapNaviHnds.end();
	
	for (itWid; itWid != itWidEnd; ++itWid)
	{
		//delete itWid->second;
		this->RemoveControlHandle(itWid->first);
		delete itWid->first;
	}
	m_mapNaviHnds.clear();


	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidTree = 
				m_mapTreeHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEndTree = 
				m_mapTreeHnds.end();
	
	for (itWidTree; itWidTree != itWidEndTree; ++itWidTree)
	{
		//delete itWid->second;
		this->RemoveControlHandle(itWidTree->first);
		delete itWidTree->first;
	}
	m_mapTreeHnds.clear();

	DeleteTextEntries(0);
	DeleteQuadsNodeSuccs();
}

/*============================================================================*/
/*  NAME:	OnSensorSlotUpdate                                                */
/*============================================================================*/
void VfaHiBroController::OnSensorSlotUpdate(int iSlot,
		const VfaApplicationContextObject::sSensorFrame & oFrameData)
{
	// just react on our slot
	if (iSlot != VfaApplicationContextObject::SLOT_CURSOR_VIS)
		return;

	if(!m_bMapMovingInSzene)
		return; 

	// save sensor data
	m_oLastSensorFrame = oFrameData;

#ifndef NDEBUG
	vstr::debugi() <<"WIDGET = 0, NODE = 1, NONE = 2 : " << m_iWidgetHndActive << endl;
	vstr::debugi() << "update frame data" << endl;
#endif

	// use it immediately if we are allowed to move the map
	if(m_iCurrentState != HIBRO_INACTIVE)
	{
		const VistaQuaternion qOri = m_oLastSensorFrame.qOrientation;
		m_pModel->SetOrientation(qOri);
		
		VistaVector3D v3Center = m_oLastSensorFrame.v3Position
											+ qOri.Rotate(m_v3MapOffset);
		m_pModel->SetPosition(v3Center);

		this->UpdateNaviHandlePosInMap();
		this->SetNaviHndsPos();
		this->UpdateTreeModel();
		this->ActivateSignForSuccs();
	}
}

/*============================================================================*/
/*  NAME:	OnCommandSlotUpdate                                               */
/*============================================================================*/
void VfaHiBroController::OnCommandSlotUpdate(int iSlot, const bool bSet)
{
	// one of our buttons was released
	if (!bSet)
	{
		if (iSlot == m_iIDTimerButton)
		{
			if (m_dTimerBtPressTime == 0.0f)
			{
				// timer was reseted already, so action 2 took or takes place
				// we needn't do more here
				return;
			}

			// we first reset the important parameters
			m_bTimerBTActive = false;
			m_pTimerFeedbackConfig->ResetFeedback();
			m_dTimerBtPressTime = 0.0f;

			// no action took place yet, so start action 1,
			if (m_iTimerAction1 == HIBRO_MAP_MANIPULATION)
				this->InitTimerActionMapMani();
			else if (m_iTimerAction1 == HIBRO_SCENE_MANIPULATION)
				this->InitTimerActionSzeneMani();

			return;
		}
		else if (iSlot == m_iIDResizeButton)
		{
			// stop sensor updates
			m_bMapMovingInSzene = false;
			
			// close the map
			if (m_pVisTime->GetCurrentClock() - m_dTimeOnResizeBtPress < m_dWaitTimeOnResizeBtClick)
			{
				m_bObserveWhetherMoveMap = false;
				// reset timer
				m_dTimeOnResizeBtPress = 0.0f;
				// initialize closing process
				SetHiBrotoStateAnimation();
				m_iIdLastMiddleNaviHnd = m_iIdMiddleNaviHnd;
				m_iIdMiddleNaviHnd = m_pModel->GetHierarchy()->GetController()
													   ->GetRootNode()->GetId();
				m_dStartTime = m_pVisTime->GetCurrentClock();
			}
		}
		return;
	}

	
	// now analyze which button was pressed and trigger the action for it
	if (iSlot == m_iIDTimerButton && m_iCurrentState == HIBRO_ACTIVE )
	{
		// in the case of the timer button we don't know on button press
		// which action has to be triggered, so just start a timer and
		// store we are pressed and lets find out later 
		// (on button release or on timer notification) what has to be done
		m_dTimerBtPressTime = m_pVisTime->GetCurrentClock();
		m_bTimerBTActive = true;

		// and orient and pos timer in a good way
		VistaVector3D v3Pos;
		this->GetCurHandlePos(v3Pos);
		VistaQuaternion qOri;
		this->GetMapOrientation(qOri);
		qOri.Normalize();
		m_pTimerFeedbackConfig->SetPosAndOri(v3Pos, qOri);
	}
	else if(iSlot == m_iIDResizeButton)
	{
		// for the resize button the actions are clearly defined:
		// open/close or move the map

		if (m_iCurrentState == HIBRO_INACTIVE)
		{
			// initialize opening process
			SetHiBrotoStateAnimation();
			m_dStartTime = m_pVisTime->GetCurrentClock();
			
			// allow movement meaning enable sensor updates
			m_bMapMovingInSzene = true;

			m_pModel->GetHierarchy()->GetController()->GetDrawStrategy()
															->SetVisible(true);
			this->SetTreeHndsActive(true);
			this->UpdateTreeEdgeColors();
		}
		else
		{
			m_dTimeOnResizeBtPress = m_pVisTime->GetCurrentClock();
			m_bObserveWhetherMoveMap = true;
			OnUnfocus();
		}
	}
}

/*============================================================================*/
/*  NAME:	OnTimeSlotUpdate                                                  */
/*============================================================================*/
void VfaHiBroController::OnTimeSlotUpdate(const double dTime)
{
	// first of all control whether timer of our timer button
	// indicates we have a long press here
	if(m_bTimerBTActive)
	{
		double dDelta = dTime - m_dTimerBtPressTime;

		dDelta/=m_dTimerBTLongClick;
		// ATTENTION: delta is mapped from 0 to 1 now

		// first show feedback after a defined portion of time
		if(dDelta >= m_fFeedbackStartTimePerCent)
		{
			double dDeltaFeedback = (dDelta-m_fFeedbackStartTimePerCent)/
				(1.0f-m_fFeedbackStartTimePerCent);
			m_pTimerFeedbackConfig->UpdateParams(dDeltaFeedback);
		}

		if (dDelta >= 1.1f)
		{
			// then reset the timer and store we aren't active anymore
			m_dTimerBtPressTime = 0.0f;
			m_bTimerBTActive = false;
			m_pTimerFeedbackConfig->ResetFeedback();

			// and then trigger action 2
			if(m_iTimerAction1 == HIBRO_MAP_MANIPULATION)
				this->InitTimerActionSzeneMani();
			else if(m_iTimerAction1 == HIBRO_SCENE_MANIPULATION)
				this->InitTimerActionSzeneMani();
		}
	}


	UpdateSize(dTime-m_dStartTime);
	UpdateLinearAnimation(dTime-m_dStartTime);

	// control whether we want to close our map OR want to translate it
	if (m_bObserveWhetherMoveMap)
	{
		if (m_pVisTime->GetCurrentClock() - m_dTimeOnResizeBtPress > 
													m_dWaitTimeOnResizeBtClick)
		{
			m_bMapMovingInSzene = true;
			m_bObserveWhetherMoveMap = false;
		}
	}
}


/*============================================================================*/
/*  NAME:   On(Un)Focus					                                      */
/*============================================================================*/
void VfaHiBroController::OnFocus(IVfaControlHandle* pHandle)
{
	// we could only have a focused handle, if the map is active and
	// the map should not be moved!
	if(m_iCurrentState != HIBRO_ACTIVE)
		return;

	if(m_bMapMovingInSzene)
		return;


	IVfaCenterControlHandle *pHdle = 
		dynamic_cast<IVfaCenterControlHandle*>(pHandle);


	// search focused handle in maps	
	// first of all in the widget handle map
	m_itActiveHnd =	m_mapNaviHnds.find(pHdle);
	if (m_itActiveHnd != m_mapNaviHnds.end())
	{
		m_pModel->SetSelectedNode(m_itActiveHnd->second);
		m_iWidgetHndActive = NAVI_HANDLE;
		m_itActiveHnd->first->SetIsHighlighted(true);

		SetCorrespondingHandleHighlighted(m_itActiveHnd->second->GetId(),
			m_mapTreeHnds, true );

		this->ActivateLoupe();
		return;
	}

	// if we do not find the handle in the m_mapNaviHnds, let's have a look 
	// in the map of the node handles
	m_itActiveHnd = m_mapTreeHnds.find(pHdle);
	if (m_itActiveHnd != m_mapTreeHnds.end())
	{
		m_pModel->SetSelectedNode(m_itActiveHnd->second);
		m_iWidgetHndActive = TREE_HANDLE;
		m_itActiveHnd->first->SetIsHighlighted(true);

		SetCorrespondingHandleHighlighted(m_itActiveHnd->second->GetId(),
			m_mapNaviHnds, true );

		this->ActivateLoupe();
		return;
	}
}
void VfaHiBroController::OnUnfocus()
{
	// do we have an active widget or node handle?
	if (m_iWidgetHndActive == NONE_HANDLE)
		return;

	// if so, make this handle inactive
	m_itActiveHnd->first->SetIsHighlighted(false);	

	if(m_iWidgetHndActive == TREE_HANDLE)
		SetCorrespondingHandleHighlighted(
		m_itActiveHnd->second->GetId(),	m_mapNaviHnds, false );
	else
		SetCorrespondingHandleHighlighted(
		m_itActiveHnd->second->GetId(),	m_mapTreeHnds, false );

	m_iWidgetHndActive = NONE_HANDLE;
	this->DeactivateLoupe();
}

/*============================================================================*/
/*  NAME:	Get...Mask  	                                                  */
/*============================================================================*/
int VfaHiBroController::GetSensorMask() const
{
	return(1 << VfaApplicationContextObject::SLOT_CURSOR_VIS);
}
int VfaHiBroController::GetCommandMask() const
{
	return(1 << m_iIDResizeButton)|(1 << m_iIDTimerButton);
}
bool VfaHiBroController::GetTimeUpdate() const
{
	return true;
}

/*============================================================================*/
/*  NAME:	Set/GetIsEnabled				                                  */
/*============================================================================*/
void VfaHiBroController::SetIsEnabled(bool b)
{
	//@todo
}
bool VfaHiBroController::GetIsEnabled() const
{
	//@todo
	return false;
}

/*============================================================================*/
/*  NAME:	Set/GetUpdateNaviHndsWithTreeHnds                                 */
/*============================================================================*/
void VfaHiBroController::SetUpdateNaviHndsWithTreeHnds(bool bDo)
{
	m_bUpdateZoomInWithOverview = bDo;
}
bool VfaHiBroController::GetUpdateNaviHndsWithTreeHnds() const
{
	return m_bUpdateZoomInWithOverview;
}

/*============================================================================*/
/*  NAME:	SetButtonID____				                                      */
/*============================================================================*/
void VfaHiBroController::SetButtonIDResize(int iID)
{
	m_iIDResizeButton = iID;
}
int VfaHiBroController::GetButtonIDResize() const
{
	return m_iIDResizeButton;
}
void VfaHiBroController::SetButtonIDTimer(int iID)
{
	m_iIDTimerButton = iID;
}
int VfaHiBroController::GetButtonIDTimer() const
{
	return m_iIDTimerButton;
}
void VfaHiBroController::SetTimerButtonAction1(int iAction)
{
	m_iTimerAction1 = iAction;
}
int VfaHiBroController::GetTimerButtonAction1() const
{
	return m_iTimerAction1;
}
void VfaHiBroController::SetTimerButtonLongClickTime(double fTime)
{
	m_dTimerBTLongClick = fTime;
}
double VfaHiBroController::GetTimerButtonLongClickTime() const
{
	return m_dTimerBTLongClick;
}

void VfaHiBroController::SetTimerButtonFeedbackStartTime(float fPerCent)
{
	m_fFeedbackStartTimePerCent = fPerCent;
}
float VfaHiBroController::GetTimerButtonFeedbackStartTime() const
{
	return m_fFeedbackStartTimePerCent;
}
/*============================================================================*/
/*  NAME:	Set/GetOpenAnimationTime			                              */
/*============================================================================*/
void VfaHiBroController::SetOpenAnimationTime(double dTime)
{
	m_dOpenAnimationTime = dTime;
}
double VfaHiBroController::GetOpenAnimationTime() const
{
	return m_dOpenAnimationTime;
}
/*============================================================================*/
/*  NAME:	Set/GetCloseWaitTime				                              */
/*============================================================================*/
void VfaHiBroController::SetCloseWaitTime(double dTime)
{
	m_dWaitTimeOnResizeBtClick = dTime;
}
double VfaHiBroController::GetCloseWaitTime() const
{
	return m_dWaitTimeOnResizeBtClick;
}
/*============================================================================*/
/*  NAME:	Set/GetLinearAnimationTime				                          */
/*============================================================================*/
void VfaHiBroController::SetLinearAnimationTime(double dTime)
{
	m_dLinearAnimationTime = dTime;
}
double VfaHiBroController::GetLinearAnimationTime() const
{
	return m_dLinearAnimationTime;
}
/*============================================================================*/
/*  NAME:	Set/GetOffsetVector						                          */
/*============================================================================*/
void VfaHiBroController::SetOffsetVector(const VistaVector3D& v3C)
{
	m_v3MapOffset = v3C;
}
void VfaHiBroController::GetOffsetVector(VistaVector3D &v3C) const
{
	v3C = m_v3MapOffset;
}
/*============================================================================*/
/*  NAME:	Set/GetHandleScaleFactor		                                  */
/*============================================================================*/
void VfaHiBroController::SetHandleScaleFactor(float fScale)
{
	m_fScaleFactorForNaviHnds	= fScale;
}
float VfaHiBroController::GetHandleScaleFactor() const
{
	return m_fScaleFactorForNaviHnds;
}
/*============================================================================*/
/*  NAME:	GetIsMapOpen						                              */
/*============================================================================*/
bool VfaHiBroController::GetIsMapOpen()
{
	if(m_iCurrentState == HIBRO_INACTIVE || m_iCurrentState == HIBRO_DISABLED)
		return false;

	return true;
}
/*============================================================================*/
/*  NAME:	Set/GetUnselectedColor		                                      */
/*============================================================================*/
void VfaHiBroController::SetUnselectedColor(float aColorRgba[4])
{
	m_aUnselColorRgba[0] = aColorRgba[0];
	m_aUnselColorRgba[1] = aColorRgba[1];
	m_aUnselColorRgba[2] = aColorRgba[2];
	m_aUnselColorRgba[3] = 1.0f;
}
void VfaHiBroController::GetUnselectedColor(float aColorRgba[4]) const
{
	aColorRgba[0] = m_aUnselColorRgba[0];
	aColorRgba[1] = m_aUnselColorRgba[1];
	aColorRgba[2] = m_aUnselColorRgba[2];
	aColorRgba[3] = m_aUnselColorRgba[3];
}
/*============================================================================*/
/*  NAME:	Set/GetSelectedColor		                                      */
/*============================================================================*/
void VfaHiBroController::SetSelectedColor(float aColorRgba[4])
{
	m_aSelColorRgba[0] = aColorRgba[0];
	m_aSelColorRgba[1] = aColorRgba[1];
	m_aSelColorRgba[2] = aColorRgba[2];
	m_aSelColorRgba[3] = 1.0f;
}
void VfaHiBroController::GetSelectedColor(float aColorRgba[4]) const
{
	aColorRgba[0] = m_aSelColorRgba[0];
	aColorRgba[1] = m_aSelColorRgba[1];
	aColorRgba[2] = m_aSelColorRgba[2];
	aColorRgba[3] = m_aSelColorRgba[3];
}
/*============================================================================*/
/*  NAME:	Set/GetCenterColorForTree	                                      */
/*============================================================================*/
void VfaHiBroController::SetCenterColorForTree(float aColorRgba[4])
{
	m_aColorCenterRgba[0] = aColorRgba[0];
	m_aColorCenterRgba[1] = aColorRgba[1];
	m_aColorCenterRgba[2] = aColorRgba[2];
	m_aColorCenterRgba[3] = 1.0f;
}
void VfaHiBroController::GetCenterColorForTree(float aColorRgba[4]) const
{
	aColorRgba[0] = m_aColorCenterRgba[0];
	aColorRgba[1] = m_aColorCenterRgba[1];
	aColorRgba[2] = m_aColorCenterRgba[2];
	aColorRgba[3] = m_aColorCenterRgba[3];
}
/*============================================================================*/
/*  NAME:	Set/GetHighlightColor		                                      */
/*============================================================================*/
void VfaHiBroController::SetHighlightColor(float aColorRgba[4])
{
	m_aHighlightColorRgba[0] = aColorRgba[0];
	m_aHighlightColorRgba[1] = aColorRgba[1];
	m_aHighlightColorRgba[2] = aColorRgba[2];
	m_aHighlightColorRgba[3] = 1.0f;
}
void VfaHiBroController::GetHighlightColor(float aColorRgba[4]) const
{
	aColorRgba[0] = m_aHighlightColorRgba[0];
	aColorRgba[1] = m_aHighlightColorRgba[1];
	aColorRgba[2] = m_aHighlightColorRgba[2];
	aColorRgba[3] = m_aHighlightColorRgba[3];
}
/*============================================================================*/
/*  NAME:	Set/GetColorForSuccQuads	                                      */
/*============================================================================*/
void VfaHiBroController::SetColorForSuccQuads(float aColorRgba[4])
{
	m_aSuccsColor[0] = aColorRgba[0];
	m_aSuccsColor[1] = aColorRgba[1];
	m_aSuccsColor[2] = aColorRgba[2];
	m_aSuccsColor[3] = aColorRgba[3];
}
void VfaHiBroController::GetColorForSuccQuads(float aColorRgba[4]) const
{
	aColorRgba[0] = m_aSuccsColor[0];
	aColorRgba[1] = m_aSuccsColor[1];
	aColorRgba[2] = m_aSuccsColor[2];
	aColorRgba[3] = m_aSuccsColor[3];
}
/*============================================================================*/
/*  NAME:	Set/GetColorForNoSuccQuads	                                      */
/*============================================================================*/
void VfaHiBroController::SetColorForNoSuccQuads(float aColorRgba[4])
{
	m_aNoSuccsColor[0] = aColorRgba[0];
	m_aNoSuccsColor[1] = aColorRgba[1];
	m_aNoSuccsColor[2] = aColorRgba[2];
	m_aNoSuccsColor[3] = aColorRgba[3];
}
void VfaHiBroController::GetColorForNoSuccQuads(float aColorRgba[4]) const
{
	aColorRgba[0] = m_aNoSuccsColor[0];
	aColorRgba[1] = m_aNoSuccsColor[1];
	aColorRgba[2] = m_aNoSuccsColor[2];
	aColorRgba[3] = m_aNoSuccsColor[3];
}
/*============================================================================*/
/*  NAME:	GetMapOrientation			                                      */
/*============================================================================*/
void VfaHiBroController::GetMapOrientation(VistaQuaternion &q) const
{
	m_pModel->GetOrientation(q);
}
/*============================================================================*/
/*  NAME:	Set/GetVariationToShowNodeSuccs			                          */
/*============================================================================*/
void VfaHiBroController::SetVariationToShowNodeSuccs(unsigned int iDesc)
{
	if (m_iVaritionsOfShowingNfOfSuccs == iDesc)
		return;

	if (iDesc == HIBRO_SUCCS_COLOR_QUADS)
	{
		this->DeactivateSignForSuccs();
		this->DeleteTextEntries(1);

		m_iVaritionsOfShowingNfOfSuccs = iDesc;
		this->CreateSignForShowingNrOfSuccs();
		this->ActivateSignForSuccs();
	}
	else if (m_iVaritionsOfShowingNfOfSuccs == HIBRO_SUCCS_COLOR_QUADS)
	{
		this->DeactivateSignForSuccs();
		this->DeleteQuadsNodeSuccs();
		
		m_iVaritionsOfShowingNfOfSuccs = iDesc;
		this->CreateSignForShowingNrOfSuccs();
		this->ActivateSignForSuccs();
	}
	else
	{
		m_iVaritionsOfShowingNfOfSuccs = iDesc;
		this->ActivateSignForSuccs();
	}
}
unsigned int VfaHiBroController::GetVariationToShowNodeSuccs() const
{
	return m_iVaritionsOfShowingNfOfSuccs;
}
/*============================================================================*/
/*  NAME:   CreateHandles				                                      */
/*============================================================================*/
bool VfaHiBroController::CreateHandles()
{
	if(!m_pModel->GetHierarchy()->GetController()->StructureDefinedAsFinal())
		return false;

	// get root node
	VfaStructureNode *pNode = m_pModel->GetHierarchy()->
		GetController()->GetRootNode();
	m_iIdLastMiddleNaviHnd = m_iIdMiddleNaviHnd;
	m_iIdMiddleNaviHnd = pNode->GetId();

	// build node handles
	CreateNaviHnds(pNode);	

	// build tree handles
	std::map<int, VistaVector3D> tempMap;
	m_pModel->GetHierarchy()->GetController()->GetNodeCenters(tempMap);
	CreateTreeHandles(pNode,tempMap);

	CreateSignForShowingNrOfSuccs();

	return true;
}

/*============================================================================*/
/*  NAME:	ObserverUpdate		                                              */
/*============================================================================*/
void VfaHiBroController::ObserverUpdate(IVistaObserveable *pObserveable, 
										  int msg, int ticket)
{
// nothing to observe
}



/*============================================================================*/
/*								                                              */
/*  PROTECTED METHODS			                                              */
/*								                                              */
/*============================================================================*/

/*============================================================================*/
/*  NAME:	SetCorrespondingHandleHighlighted						          */
/*============================================================================*/
void VfaHiBroController::SetCorrespondingHandleHighlighted (int iID,
									 std::map<IVfaCenterControlHandle*, 
									 VfaStructureNode*> mapNodes,
									 bool bHighlighted)
{

	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itBegin = 
		mapNodes.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itEnd = 
		mapNodes.end();

	for (;itBegin != itEnd ; ++itBegin)
	{
		if (itBegin->second->GetId() == iID)
			itBegin->first->SetIsHighlighted(bHighlighted);
	}
}

/*============================================================================*/
/*  NAME:	GetCurHandlePos													  */
/*============================================================================*/
void VfaHiBroController::GetCurHandlePos(VistaVector3D &v3C) const
{
	VistaQuaternion q;
	m_pModel->GetOrientation(q);

	m_itActiveHnd->first->GetCenter(v3C);
	v3C = v3C + q.Rotate(VistaVector3D(0.0f, 0.0f, 0.05f));
}

/*============================================================================*/
/*  NAME:	UpdateNaviHndsWithTreeHnds		                                  */
/*============================================================================*/
void VfaHiBroController::UpdateNaviHndsWithTreeHnds()
{

	// get node which should be located in the middle
	if(!m_itActiveHnd->second->IsLeaf())
	{
		m_iIdLastMiddleNaviHnd = m_iIdMiddleNaviHnd;
		m_iIdMiddleNaviHnd = m_itActiveHnd->second->GetId();
	}
	else
	{
		m_iIdLastMiddleNaviHnd = m_iIdMiddleNaviHnd;
		m_iIdMiddleNaviHnd = m_itActiveHnd->second->
			GetPred()->GetId();
	}

	// update vis of node handles to current selection with tree nodes
	this->SetNaviHandlesAllInvisible();
	this->UpdateNaviHandlePosInMap();
	this->SetNaviHndsPos();	
}

/*============================================================================*/
/*  NAME:	CreateNaviHnds													  */
/*============================================================================*/
void VfaHiBroController::CreateNaviHnds(VfaStructureNode *pNode)
{
	if (pNode == NULL)
		return;

	VflRenderNode *pRNode = this->GetRenderNode();

	VfaTexturedQuadHandle *pTexBox = new VfaTexturedQuadHandle(pRNode);
	pTexBox->SetEnable(false);
	pTexBox->SetIsSelectionEnabled(false);
	pTexBox->SetIsVisible(false);
	pTexBox->SetSize(0.0f, 0.0);
	pTexBox->SetCenter(VistaVector3D(0.0f, 0.0f, 0.001f));
	pTexBox->SetTexture(pNode->GetTexture());
	pTexBox->GetQuadVis()->GetProperties()->SetLineWidth(5.0f);

	float fColorN[4] = {0.4f, 0.4f, 0.5f, 1.0f};
	pTexBox->SetNormalColor(fColorN);

	float fColorH[4] = {1.0f, 0.0f, 0.0f, 1.0f};
	pTexBox->SetHighlightedColor(fColorH);

	pTexBox->SetIsHighlighted(false);
	pair<IVfaCenterControlHandle*, VfaStructureNode*> pPH(pTexBox, pNode);
	m_mapNaviHnds.insert(pPH);
	this->AddControlHandle(pTexBox);

	// store number of succs for showing 
	// this additional information in the Zoom-In
	int iNrOfSuccs = pNode->GetNrSuccs();
	pair<int, int> pNRS(pNode->GetId(), iNrOfSuccs);
	m_mapLastRowNaviSuccsNrs.insert(pNRS);

	for (int i = 0; i < iNrOfSuccs; ++i)
		CreateNaviHnds(pNode->GetSuccForIdx(i));
}


/*============================================================================*/
/*  NAME:	GetIdsOfPresentedNaviHnds                                         */
/*============================================================================*/
void VfaHiBroController::GetIdsOfPresentedNaviHnds(
										std::vector<int> &iNodeHndIdsForMap)
{
	// index for row of map (middle row as default)
	m_iMapStartInRow = 1;

	// get node located in the middle of the map
	VfaStructureNode *pNode =
		m_pModel->GetHierarchy()->GetController()->GetNodeForId(m_iIdMiddleNaviHnd);

	// do we have a node for the first row, so do we have a pred (index: 0)?
	if (pNode->GetPred() != NULL)
	{
		iNodeHndIdsForMap.push_back(pNode->GetPred()->GetId());
		m_iMapStartInRow = 0;
	}

	// store current middle button
	iNodeHndIdsForMap.push_back(m_iIdMiddleNaviHnd);


	// now look for succs and their ids
	int iNrSuccs = pNode->GetNrSuccs();
	for (int i = 0; i < iNrSuccs; ++i)
	{
		iNodeHndIdsForMap.push_back(pNode->GetSuccForIdx(i)->GetId());
	}
}

/*============================================================================*/
/*  NAME:	UpdateNaviHandlePosInMap                                            */
/*============================================================================*/
void VfaHiBroController::UpdateNaviHandlePosInMap(float fTime /*= 1.0f*/)
{
	// fTime can be used in animation mode, to scale the position of the 
	// nodes, otherwise it is one!

	// get ids for current map situation in correct order
	std::vector<int> iNodeHndIdsForMap;
	this->GetIdsOfPresentedNaviHnds(iNodeHndIdsForMap);

	// delete old positions
	m_mapVisibleNaviHnds.clear();

	// iterator for node handles
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
				m_mapNaviHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
				m_mapNaviHnds.end();

	// get current map parameters
	VistaVector3D v3;
	m_pModel->GetPosition(v3);
	VistaQuaternion quat;
	m_pModel->GetOrientation(quat);

	// get node located in the middle of the map
	VfaStructureNode *pNode =
		m_pModel->GetHierarchy()->GetController()->GetNodeForId(m_iIdMiddleNaviHnd);
	
	// get number of succs
	int iNrSuccs = pNode->GetNrSuccs();
	
	// in which column of the third row should we place the first succ node
	// under the condition of a symmetric ordering?
	int iColumnStart = (m_pModel->GetNumberOfTilesWidth()/2)-iNrSuccs/2;
	int iColumnEnd = iColumnStart+iNrSuccs;
	bool bMiddleEmpty = false;
	if(iNrSuccs%2 == 0)
	{
		++iColumnEnd;
		bMiddleEmpty = true;
	}


	// go through all nodes (node handles)
	for (itWid; itWid != itWidEnd; ++itWid)
	{
		// get id ...
		int iId = itWid->second->GetId();
		
		// get row, where node should be positioned
		// see @GetRowForNaviNode for more comments
		int iRow = this->GetRowForNaviNode(iNodeHndIdsForMap, iId);
		
		// let's proof, whether the row exists
		if(iRow != -1 )
		{
			// ... we compute its new position and store it as a pair with the id
			
			if (iRow == 0)
			{
				// first row, middle column
				pair<int, VistaVector3D> pP(iId, 
					v3+quat.Rotate(
					VistaVector3D(0.0f,fTime*m_pModel->GetTileEdgeLength(),0.0f)));
				m_mapVisibleNaviHnds.insert(pP);
			}
			else if (iRow == 1)
			{
				// second row, middle column
				pair<int, VistaVector3D> pP(iId, v3);
				m_mapVisibleNaviHnds.insert(pP);
			}
			else if (iRow == 2)
			{
				// third row

/*				// update column number for symmetric
				if(m_pModel->GetNumberOfTilesWidth()/2	== iColumn && iNrSuccs%2 == 0 &&
					iNrSuccs != 1)
					++iColumn;
					*/
				int iColumn = this->GetThirdRowColumnForNode(iNodeHndIdsForMap, iId, iColumnStart, iColumnEnd, bMiddleEmpty);

				// get index for column, at which the node should be placed
				// please pay attention: 
				// the column indexes are  ... -2 -1 0 1 2 ...,
				// where zero is the column in the middle of the map
				float fXFactor = (iColumn-(int)(m_pModel->GetNumberOfTilesWidth()/2))*
							fTime*m_pModel->GetTileEdgeLength();

				pair<int, VistaVector3D> pP(iId, v3+
					quat.Rotate(VistaVector3D(fXFactor,
					-fTime*m_pModel->GetTileEdgeLength(), 0.0f)));
				m_mapVisibleNaviHnds.insert(pP);
				++iColumn;
			}
			
		}
	}
	
	this->UpdateTreeHndsColor();
}

/*============================================================================*/
/*  NAME:	GetRowForNaviNode												  */
/*============================================================================*/
int VfaHiBroController::GetRowForNaviNode(const std::vector<int> &vector, 
											int iId )
{
	// Ok, here some easy stuff is done.

	// The vector contains all ids of nodes (sorted by depth-first search), 
	// which are visible in the map after the current interactions and the 
	// iID value shows the id of the node which we want to position now.
	
	// get size of vector --> get number of nodes in the next map state
	const size_t nSize = vector.size();

	for (size_t i = 0; i < nSize; ++i)
	{
		// search whether current iID is in the map
		if (vector[i] == iId)
		{
			if (m_iMapStartInRow == 0)
			{
				// We was informed before, that we have to fill all three rows of our map.
				// So, if the current iId is in position zero in the vector, it has to be positioned
				// in the first row, if it is in position one, it has to be put in the middle row,
				// otherwise in the last row
				return ( i>=1 ? (i>=2 ? 2 : 1) : 0);
			}
			else if (m_iMapStartInRow == 1)
			{
				// Here, we was informed before, that we have to fill the last two rows of our map.
				// So, if the current iId is in position zero in the vector, it has to be positioned
				// in the middle row, otherwise in the last row
				return ( i>=1 ? 2:1);
			}
		}
	}

	// our node is not visible, so return -1
	return -1;
}

/*============================================================================*/
/*  NAME:	GetThirdRowColumnForNode										  */
/*============================================================================*/
int VfaHiBroController::GetThirdRowColumnForNode(const std::vector<int> &vector, 
						int iId, int iStartC, int iEndC, bool m_bMiddleFree)
{
	// The vector contains all ids of nodes (sorted by depth-first search), which are visible in the map
	// after the current interactions 
	// iId				id of the node, which we want to position now.
	// [iStartC, iEndC] intervall of allowed column numbers 

	const int iSize = static_cast<int>(vector.size());
	int iReturn = -1;

	// how may nodes in the vector should NOT be located in the third row
	// they are in the beginn of the vector
	int iDiff = 1;
	if (m_iMapStartInRow == 0)
		++iDiff;		

	for (int i = 0; i < iSize; ++i)
	{
		// search whether current iID is in the map
		if (vector[i] == iId)
		{			
			//we found our node in the vector
			
			// compute column number for current node depending on his place in the vector and
			// the column number to start with
			int iNr = i-iDiff;
			iReturn = iNr+iStartC;
			
			// correct the column number if our new position is in or after the the middle of the 
			// allowed column numbers
			// IF the middle column should be left empty with due to symmetry
			if(iReturn >= (iStartC+iEndC)/2 && m_bMiddleFree)
				++iReturn;

			break;
		}
	}

	// our node is not in the third row, so return -1
	return iReturn;
}

/*============================================================================*/
/*  NAME:	GetHandlePositionForId                                            */
/*============================================================================*/
VistaVector3D VfaHiBroController::GetHandlePositionForId(int iD) 
{
	// iterator for widget handles
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
		m_mapNaviHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
		m_mapNaviHnds.end();

	VistaVector3D v3;
	v3.SetToZeroVector();

	// go through all nodes (widget handles)
	for (itWid; itWid != itWidEnd; ++itWid)
	{
		if (itWid->second->GetId() == iD)
		{
			(dynamic_cast<VfaTexturedQuadHandle*>(itWid->first))->GetCenter(v3);
			return v3;
		}
	}
	return v3;
}

/*============================================================================*/
/*  NAME:	UpdateLinearAnimation                                             */
/*============================================================================*/
void VfaHiBroController::UpdateLinearAnimation(double dDeltaT)
{
	if (m_iCurrentState != HIBRO_LINEAR_ANIMATION)
		return;

	VfaStructureNode *pNode1 = 
		m_pModel->GetHierarchy()->GetController()->
		GetNodeForId(m_iVecTransferedNaviHndsIds[0]);
	VfaStructureNode *pNode2 = 
		m_pModel->GetHierarchy()->GetController()->
		GetNodeForId(m_iVecTransferedNaviHndsIds[1]);

	// what's the current delta?
	float dCoefficient = (float)(dDeltaT/m_dLinearAnimationTime);
	if(dCoefficient >= 0 && dCoefficient <= 1)
	{
		// lineare transformation
		std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
			m_mapNaviHnds.begin();
		std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
			m_mapNaviHnds.end();

		bool b1 = false;
		bool b2 = false;

		for (itWid; itWid != itWidEnd; ++itWid)
		{
			if (itWid->second->GetId() == pNode1->GetId())
			{
				VistaVector3D v3Dir =	m_v3VecTransferedNodeHndsPos[2] - 
										m_v3VecTransferedNodeHndsPos[0];
				float fLength = v3Dir.GetLength();
				v3Dir.Normalize();
				VfaTexturedQuadHandle *pTexBox = 
					dynamic_cast<VfaTexturedQuadHandle *>(itWid->first);
				VistaVector3D v3 = m_v3VecTransferedNodeHndsPos[0] +
									dCoefficient*fLength*v3Dir;
				pTexBox->SetCenter(GetVectorWithZOffset(v3));
				pTexBox->SetIsVisible(true);
				b1 = true;
			}
			else if(itWid->second->GetId() == pNode2->GetId())
			{
				VistaVector3D v3Dir =	m_v3VecTransferedNodeHndsPos[3] - 
										m_v3VecTransferedNodeHndsPos[1];
				float fLength = v3Dir.GetLength();
				v3Dir.Normalize();
				v3Dir[3] = 0.0001f;
				VfaTexturedQuadHandle *pTexBox = 
					dynamic_cast<VfaTexturedQuadHandle *>(itWid->first);
				VistaVector3D v3 = m_v3VecTransferedNodeHndsPos[1] +
									dCoefficient*fLength*v3Dir;
				pTexBox->SetCenter(GetVectorWithZOffset(v3));
				pTexBox->SetIsVisible(true);
				b2 = true;
			}

			if(b1 && b2)
				break;
		}
	}
	else
	{
		// set rest handles
		this->SetNaviHndsPos();
		SetHiBrotoStateActive();
	}
}

/*============================================================================*/
/*  NAME:	SetNaviHndsPos													  */
/*============================================================================*/
void VfaHiBroController::SetNaviHndsPos()
{
	// iterator for widget handles
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
		m_mapNaviHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
		m_mapNaviHnds.end();

	// get map orientation	
	VistaQuaternion quat;
	m_pModel->GetOrientation(quat);

	// go through all nodes (widget handles)
	for (itWid; itWid != itWidEnd; ++itWid)
	{
		// find the one we are interested in
		std::map<int, VistaVector3D>::iterator it = 
			m_mapVisibleNaviHnds.find(itWid->second->GetId());
		if(it != m_mapVisibleNaviHnds.end())
		{
			// get their handle representation
			VfaTexturedQuadHandle *pTexBox = 
				dynamic_cast<VfaTexturedQuadHandle *>(itWid->first);

			pTexBox->SetIsVisible(true);
			pTexBox->SetEnable(true);
			pTexBox->SetIsSelectionEnabled(true);

			VistaVector3D v3 = it->second;
			pTexBox->SetCenter(GetVectorWithZOffset(v3));
			pTexBox->SetNormal(quat.Rotate(VistaVector3D(0.0f, 0.0f, 1.0f)));
		}
		else
		{
			// get their handle representation
			VfaTexturedQuadHandle *pTexBox = 
				dynamic_cast<VfaTexturedQuadHandle *>(itWid->first);
			pTexBox->SetIsVisible(false);
			pTexBox->SetEnable(false);
			pTexBox->SetIsSelectionEnabled(false);
		}
	}
}

/*============================================================================*/
/*  NAME:	SetNode/TreeHandleSize                                            */
/*============================================================================*/
void VfaHiBroController::SetNaviHandleSize(float fhw)
{
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
		m_mapNaviHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
		m_mapNaviHnds.end();

	for (itWid; itWid != itWidEnd; ++itWid)
	{
		VfaTexturedQuadHandle *pTexBox = 
			dynamic_cast<VfaTexturedQuadHandle *>(itWid->first);

		// m_fScaleFactorForNaviHnds is used to scale the handle size (reason:
		// handles should be a bit smaller than the matrix fields)
		pTexBox->SetSize(m_fScaleFactorForNaviHnds*fhw, 
						 m_fScaleFactorForNaviHnds*fhw);
	}
}

/*============================================================================*/
/*  NAME:	SetNaviHandlesAllInvisible			                              */
/*============================================================================*/
void VfaHiBroController::SetNaviHandlesAllInvisible()
{
	// iterator for widget handles
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
		m_mapNaviHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
		m_mapNaviHnds.end();

	// go through all nodes (widget handles)
	for (itWid; itWid != itWidEnd; ++itWid)
	{
		VfaTexturedQuadHandle *pTexBox = 
			dynamic_cast<VfaTexturedQuadHandle *>(itWid->first);

		pTexBox->SetIsVisible(false);
		pTexBox->SetEnable(false);
		pTexBox->SetIsSelectionEnabled(false);
	}	
}

/*============================================================================*/
/*  NAME:	CreateTreeHandles												  */
/*============================================================================*/
void VfaHiBroController::CreateTreeHandles(VfaStructureNode *pNode,
											 const std::map<int, 
											 VistaVector3D> &map)
{
	if (pNode == NULL)
		return;

	VflRenderNode *pRNode = this->GetRenderNode();

	VfaQuadHandle *pBox = new VfaQuadHandle(pRNode);
	pBox->SetEnable(false);
	pBox->SetVisible(false);
	pBox->SetSize(0.0f, 0.0f);

	pBox->SetNormalColor(m_aUnselColorRgba);
	pBox->SetHighlightColor(m_aHighlightColorRgba);
	pBox->SetCenter(GetVectorWithZOffset(map.find(pNode->GetId())->second));

	pBox->SetIsHighlighted(false);
	pair<IVfaCenterControlHandle*, VfaStructureNode*> pPN(pBox, pNode);
	m_mapTreeHnds.insert(pPN);
	this->AddControlHandle(pBox);

	int iNrOfSuccs = pNode->GetNrSuccs();
	for (int i = 0; i < iNrOfSuccs; ++i)
		CreateTreeHandles(pNode->GetSuccForIdx(i), map);
}

/*============================================================================*/
/*  NAME:	SetTreeHndsActive				                                  */
/*============================================================================*/
void VfaHiBroController::SetTreeHndsActive(bool bVisible)
{
	// iterator for widget handles
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
		m_mapTreeHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
		m_mapTreeHnds.end();

	// go through all nodes (widget handles)
	for (itWid; itWid != itWidEnd; ++itWid)
	{
		VfaQuadHandle *pTexBox = 
			dynamic_cast<VfaQuadHandle *>(itWid->first);

		pTexBox->SetVisible(bVisible);
		pTexBox->SetEnable(bVisible);
		pTexBox->SetIsSelectionEnabled(bVisible);
	}	
}

/*============================================================================*/
/*  NAME:	SetTreeHndsSize					                                  */
/*============================================================================*/
void VfaHiBroController::SetTreeHndsSize(float fhw)
{
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
		m_mapTreeHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
		m_mapTreeHnds.end();

	for (itWid; itWid != itWidEnd; ++itWid)
	{
		VfaQuadHandle *pBox = 
			dynamic_cast<VfaQuadHandle *>(itWid->first);

		pBox->SetSize(fhw, fhw);
	}
}

/*============================================================================*/
/*  NAME: UpdateTreeEdgeColors							                      */
/*============================================================================*/
void VfaHiBroController::UpdateTreeEdgeColors()
{
	// be careful: we just need the id of the end point of one edge!

	VfaStructureNode *pRoot = 
					m_pModel->GetHierarchy()->GetController()->GetRootNode();

	VfaStructureNode *pNode = m_pModel->GetHierarchy()->GetController()
		->FindNodeForId(m_iIdMiddleNaviHnd, pRoot);
	assert(pNode);

	// we go down in hierarchy
	if (	pNode->GetPred()!= NULL && 
			pNode->GetPred()->GetId() == m_iIdLastMiddleNaviHnd)
	{
		this->ResetSubtree(pNode->GetPred());
		this->SelectPrevious(pNode->GetPred());
		this->MarkSurrounding(pNode);
		return;
	}

	// we go up in hierarchy
	int uiSize = pNode->GetNrSuccs();
	for (int i = 0; i < uiSize; ++i)
	{
		if (pNode->GetSuccForIdx(i)->GetId() == m_iIdLastMiddleNaviHnd)
		{
			this->ResetSubtree(pNode->GetSuccForIdx(i));
			this->MarkSurrounding(pNode);
			return;
		}
	}

	// if we are still in this method, we choose a tree handle which is not
	// in the directly neighborhood of our last middle button, so we have to
	// do the following things:

	this->ResetSubtree(pRoot);
	this->SelectPrevious(pNode->GetPred());
	this->MarkSurrounding(pNode);
}


/*============================================================================*/
/*  NAME:	UpdateTreeHndsColor					                              */
/*============================================================================*/
void VfaHiBroController::UpdateTreeHndsColor()
{
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
		m_mapTreeHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
		m_mapTreeHnds.end();

	std::map<int, VistaVector3D>::iterator itExistEnd = 
													m_mapVisibleNaviHnds.end();

	for (itWid; itWid != itWidEnd; ++itWid)
	{
		std::map<int, VistaVector3D>::iterator itExists = 
			m_mapVisibleNaviHnds.find(itWid->second->GetId());

		VfaQuadHandle *pBox = 
			dynamic_cast<VfaQuadHandle *>(itWid->first);

		if (itExists != itExistEnd)
		{
			if(itWid->second->GetId() == m_iIdMiddleNaviHnd)
				pBox->SetNormalColor(m_aColorCenterRgba);
			else
				pBox->SetNormalColor(m_aSelColorRgba);
		}
		else
			pBox->SetNormalColor(m_aUnselColorRgba);


		if (m_iWidgetHndActive == TREE_HANDLE && m_itActiveHnd == itWid)
			continue;

		// just to be sure, that the new normal color is used 
		pBox->SetIsHighlighted(false);
	}
}

/*============================================================================*/
/*  NAME: ResetSubtree									                      */
/*============================================================================*/
void VfaHiBroController::ResetSubtree(VfaStructureNode *pNode)
{
	std::list<int> mTemp;
	this->Reset(pNode, mTemp);
	// delete first value because we only need the end points of the edges
	mTemp.remove(pNode->GetId());

	m_pModel->GetHierarchy()->GetView()->DrawResetSubtree(mTemp);
}

/*============================================================================*/
/*  NAME: Reset											                      */
/*============================================================================*/
void VfaHiBroController::Reset(VfaStructureNode *pNode, std::list<int> &li)
{
	if(pNode == NULL)
		return;

	li.push_back(pNode->GetId());

	int iNrOfSucc = pNode->GetNrSuccs();
	for (int i = 0; i < iNrOfSucc; ++i)
		this->Reset(pNode->GetSuccForIdx(i), li);
}

/*============================================================================*/
/*  NAME: SelectPrevious											          */
/*============================================================================*/
void VfaHiBroController::SelectPrevious(VfaStructureNode *pNode)
{
	if (pNode == NULL)
		return;

	std::list<int> mTemp;
	this->PreviousRecursiv(pNode, mTemp);

	m_pModel->GetHierarchy()->GetView()->DrawSelected(mTemp);
}

/*============================================================================*/
/*  NAME: PreviousRecursiv											          */
/*============================================================================*/
void VfaHiBroController::PreviousRecursiv(VfaStructureNode *pNode, std::list<int> &li)
{
	if(pNode == m_pModel->GetHierarchy()->GetController()->GetRootNode())
		return;

	li.push_back(pNode->GetId());
	this->PreviousRecursiv(pNode->GetPred(), li);
}

/*============================================================================*/
/*  NAME: MarkSurrounding											          */
/*============================================================================*/
void VfaHiBroController::MarkSurrounding(VfaStructureNode *pNode)
{
	std::list<int> mTemp;

	// edge from father to child
	mTemp.push_back(pNode->GetId());

	int iNrOfSucc = pNode->GetNrSuccs();
	for (int i = 0; i < iNrOfSucc; ++i)
		mTemp.push_back(pNode->GetSuccForIdx(i)->GetId());

	m_pModel->GetHierarchy()->GetView()->DrawMarkedSourrounding(mTemp);
}

/*============================================================================*/
/*  NAME:	UpdateTreeModel													  */
/*============================================================================*/
void VfaHiBroController::UpdateTreeModel()
{
	VistaVector3D v3Center;
	VistaQuaternion qOri;
	m_pModel->GetPosition(v3Center);
	m_pModel->GetOrientation(qOri);

	// Retrieve the draw strategy.
	IVfaStructuredDrawStrategy* pDrawStrat = 
		m_pModel->GetHierarchy()->GetController()->GetDrawStrategy();

	// Determine the size of one (small) tile.
	const float fTileSize = m_pModel->GetCurrentTileEdgeLength();
	// How many tiles constitute one big tile?
	const float fDrawSize = static_cast<float>(
		(m_pModel->GetNumberOfTilesWidth() - 1) / 2);

	/**************************************************/
	// first of all size the tree
	// @todo Magic value 0.9f.
	pDrawStrat->SetDrawExtents(fDrawSize * fTileSize * 0.8f,
		2.0f * fTileSize);


	/**************************************************/
	// compute the position and orientation

	// Calc the coordinate axes of the widget.
	const VistaVector3D v3Right = qOri.Rotate(
		VistaVector3D(1.0f, 0.0f, 0.0f, 0.0f));
	const VistaVector3D v3Up = qOri.Rotate(
		VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f));
	const VistaVector3D v3Normal = qOri.Rotate(
		VistaVector3D(0.0f, 0.0f, 1.0f, 0.0f));

	// The origin for the draw strategy is _always_ one half tile to the
	// left and one half to the bottom.
	// @todo 0.1f * fDrawSize is magic value.
	v3Center += (0.5f + 0.1f * fDrawSize) * fTileSize * v3Right;
	v3Center -= 0.5f * fTileSize * v3Up;
	// Additionally, we add a little offset in normal direction to counter
	// z-fighting.
	v3Center += 0.01f * v3Normal;

	// Set draw transformation.
	pDrawStrat->SetDrawCenter(v3Center);
	pDrawStrat->SetDrawOrientation(qOri);

	this->UpdateTreeHndPosAndSize();
}

/*============================================================================*/
/*  NAME:	UpdateTreeHndPosAndSize			                                  */
/*============================================================================*/
void VfaHiBroController::UpdateTreeHndPosAndSize()
{

	std::map<int, VistaVector3D> map;
	m_pModel->GetHierarchy()->GetController()->GetNodeCenters(map);

	// iterator for widget handles
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWid = 
		m_mapTreeHnds.begin();
	std::map<IVfaCenterControlHandle*, VfaStructureNode*>::iterator itWidEnd = 
		m_mapTreeHnds.end();

	// go through all nodes (widget handles)
	for (itWid; itWid != itWidEnd; ++itWid)
	{
		VfaQuadHandle *pBox = 
			dynamic_cast<VfaQuadHandle *>(itWid->first);

		std::map<int, VistaVector3D>::iterator it = 
											map.find(itWid->second->GetId());
		assert(it != map.end());

		pBox->SetCenter(GetVectorWithZOffset(it->second));
	}	

	float ftWidth, ftHeight, fQuadSize;
	m_pModel->GetHierarchy()->GetController()->GetDrawExtents(ftWidth, ftHeight);
	fQuadSize = ftWidth/map.size();;
	this->SetTreeHndsSize(fQuadSize);
}

/*============================================================================*/
/*  NAME:	UpdateSize		                                                  */
/*============================================================================*/
void VfaHiBroController::UpdateSize(double dDeltaT)
{
	// we only want to update if we are in animation mode
	if(m_iCurrentState != HIBRO_ANIMATION)
		return;

	// what's the current delta?
	double dCoefficient = dDeltaT/m_dOpenAnimationTime;

	// what was the last state
	bool bLastStateClosed = true;
	if(m_iLastState == HIBRO_ACTIVE)
		bLastStateClosed = false;

	// to store scale factor temporary
	float fModelScale = 0.0f;

	// navi hnds a are adjusted directly
	// tree hnds at the end of the method
	if(dCoefficient >= 0 && dCoefficient <= 1)
	{
		m_bLastStepChange = true;

		if(bLastStateClosed)
		{
			this->SetNaviHandleSize((float)dCoefficient*m_pModel->GetTileEdgeLength());
			fModelScale = (float)dCoefficient;
		}
		else
		{
			float fSize = m_pModel->GetTileEdgeLength();
			this->SetNaviHandleSize( fSize - ((float)dCoefficient*fSize) );
			fModelScale = 1-(float)dCoefficient;

		}
	}
	else if (m_bLastStepChange)
	{
		if(bLastStateClosed)
		{
			fModelScale = 1.0f;
			this->SetNaviHandleSize(m_pModel->GetTileEdgeLength());
			SetHiBrotoStateActive();
		}
		else
		{
			fModelScale = 0.0f;
			this->SetNaviHandleSize(0.0f);
			m_pModel->GetHierarchy()->GetController()->GetDrawStrategy()
															->SetVisible(false);
			this->SetTreeHndsActive(false);
			SetHiBrotoStateInactive();
		}
		m_bLastStepChange = false;
	}

	m_pModel->SetScale(fModelScale);
	this->UpdateNaviHandlePosInMap(fModelScale);
	this->SetNaviHndsPos();


	this->UpdateTreeModel();

	if(fModelScale != 0.0f)
		this->ActivateSignForSuccs(fModelScale);
	else
		this->DeactivateSignForSuccs();

}

/*============================================================================*/
/*  NAME:   DefineColors				                                      */
/*============================================================================*/
void VfaHiBroController::DefineColors()
{
	m_aUnselColorRgba[0] = 0.0f;
	m_aUnselColorRgba[1] = 0.0f;
	m_aUnselColorRgba[2] = 1.0f;
	m_aUnselColorRgba[3] = 1.0f;

	m_aSelColorRgba[0] = 1.0f;
	m_aSelColorRgba[1] = 0.0f;
	m_aSelColorRgba[2] = 0.0f;
	m_aSelColorRgba[3] = 1.0f;

	m_aColorCenterRgba[0] = 0.4f;
	m_aColorCenterRgba[1] = 0.0f;
	m_aColorCenterRgba[2] = 0.25f;
	m_aColorCenterRgba[3] = 1.0f;

	m_aHighlightColorRgba[0] = 0.0f;
	m_aHighlightColorRgba[1] = 1.0f;
	m_aHighlightColorRgba[2] = 0.0f;
	m_aHighlightColorRgba[3] = 1.0f;

	m_aSuccsColor[0] = 0.0f;
	m_aSuccsColor[1] = 0.7f;
	m_aSuccsColor[2] = 0.0f;
	m_aSuccsColor[3] = 1.0f;

	m_aNoSuccsColor[0] = 0.5f;
	m_aNoSuccsColor[1] = 0.5f;
	m_aNoSuccsColor[2] = 0.5f;
	m_aNoSuccsColor[3] = 1.0f;
}

/*============================================================================*/
/*  NAME:	InitTimerAction??Mani                                             */
/*============================================================================*/
void VfaHiBroController::InitTimerActionMapMani()
{
	if(m_iWidgetHndActive == TREE_HANDLE)
	{
		if(m_bUpdateZoomInWithOverview)
			this->UpdateNaviHndsWithTreeHnds();

		this->UpdateTreeEdgeColors();
		this->ActivateSignForSuccs();
	}
	else if(m_iWidgetHndActive == NAVI_HANDLE)
	{
		// to store nodes on which the linear movement will be performed
		m_iVecTransferedNaviHndsIds.clear();
		// store start (SN1,SN2) and end-position (EN1, EN2) of the movement here
		// in form SN1, SN2, EN1, EN2
		m_v3VecTransferedNodeHndsPos.clear();

		// handle is NO leaf
		if(!m_itActiveHnd->second->IsLeaf())
		{
			// insert handle, currently(!) positioned in the middle of the map
			m_iVecTransferedNaviHndsIds.push_back(m_iIdMiddleNaviHnd);
			m_v3VecTransferedNodeHndsPos.push_back(
				this->GetHandlePositionForId(m_iIdMiddleNaviHnd));

			// user pressed middle button, update middle button id?
			if (m_iIdMiddleNaviHnd == m_itActiveHnd->second->GetId())
			{
				// pred will be next middle button,
				// otherwise NO movement will take place
				if(m_itActiveHnd->second->GetPred() != NULL)
				{
					m_iIdLastMiddleNaviHnd = m_iIdMiddleNaviHnd;
					m_iIdMiddleNaviHnd = m_itActiveHnd->second->GetPred()->GetId();
				}
				else
					return;
			}
			else
			{
				m_iIdLastMiddleNaviHnd = m_iIdMiddleNaviHnd;
				m_iIdMiddleNaviHnd = m_itActiveHnd->second->GetId();
			}

			// now insert handle, which will be located in the middle of the map
			m_iVecTransferedNaviHndsIds.push_back(m_iIdMiddleNaviHnd);
			m_v3VecTransferedNodeHndsPos.push_back(
				this->GetHandlePositionForId(m_iIdMiddleNaviHnd));

			this->SetNaviHandlesAllInvisible();

			// compute new positions and save the two interesting ones
			this->UpdateNaviHandlePosInMap();
			m_v3VecTransferedNodeHndsPos.push_back(
				m_mapVisibleNaviHnds.find(m_iVecTransferedNaviHndsIds[0])->second);
			m_v3VecTransferedNodeHndsPos.push_back(
				m_mapVisibleNaviHnds.find(m_iVecTransferedNaviHndsIds[1])->second);


			// let's start the linear animation
			m_dStartTime = m_pVisTime->GetCurrentClock();
			SetHiBrotoStateLinearAnimation();

			this->UpdateTreeEdgeColors();
		}
	}
}
void VfaHiBroController::InitTimerActionSzeneMani()
{
	if(m_iWidgetHndActive == TREE_HANDLE)
	{
		m_itActiveHnd->second->Execute();
		if(m_bUpdateZoomInWithOverview)
		{
			this->UpdateNaviHndsWithTreeHnds();
			this->UpdateTreeEdgeColors();
		}
	}
	else if(m_iWidgetHndActive == NAVI_HANDLE)
		m_itActiveHnd->second->Execute();
}

/*============================================================================*/
/*  NAME:	GetVectorWithZOffset                                              */
/*============================================================================*/
VistaVector3D VfaHiBroController::GetVectorWithZOffset(const VistaVector3D &v3)
{
	VistaQuaternion q4;
	this->GetMapOrientation(q4);
	return (v3 + q4.Rotate(VistaVector3D(0.0f, 0.0f, 0.001f)));
}


/*============================================================================*/
/*  NAME:	InitLoupe		                                                  */
/*============================================================================*/
void VfaHiBroController::InitLoupe()
{
	m_pLoupeVis = new VfaQuadVis(new VfaPlaneModel());
	if(m_pLoupeVis->Init())
	{
		this->GetRenderNode()->AddRenderable(m_pLoupeVis);
		m_pLoupeVis->SetVisible(false);
		// set line color
		float fColorN[4] = {0.4f, 0.4f, 0.5f, 1.0f};
		m_pLoupeVis->GetProperties()->SetLineColor(fColorN);

		// need to be set, to see the texture
		m_pLoupeVis->GetProperties()->SetFillPlane(true);
		m_pLoupeVis->GetProperties()->SetDrawBorder(true);
	}
}

/*============================================================================*/
/*  NAME:	De/ActivateLoupe                                                  */
/*============================================================================*/
void VfaHiBroController::ActivateLoupe()
{
	// set current texture
	m_pLoupeVis->SetTexture(m_pModel->GetSelectedNode()->GetTexture());
	m_pLoupeVis->Update();


	// get map width 
	const unsigned int iWidth = m_pModel->GetNumberOfTilesWidth();
	// get half of the half
	const float fHalf = ((int)(iWidth/2))/2.0f + 0.5f;

	const float fUnit = m_pModel->GetTileEdgeLength();

	// set the orientation
	VistaQuaternion qOri;
	m_pModel->GetOrientation(qOri);
	m_pLoupeVis->GetModel()->SetNormal(
				qOri.Rotate(VistaVector3D(0.0f, 0.0f, 1.0f)));

	// set center of loupe
	VistaVector3D v3Pos;
	m_pModel->GetPosition(v3Pos);
	m_pLoupeVis->GetModel()->SetTranslation(v3Pos+
		qOri.Rotate(VistaVector3D(-fHalf*fUnit ,0.75f*fUnit , 0.01f)));

	// set the size of textured box
	m_pLoupeVis->GetModel()->SetExtents(1.25f*fUnit, 1.25f*fUnit);

	// and at the end, set box visible
	m_pLoupeVis->SetVisible(true);
	this->ActivateLoupeText();
}
void VfaHiBroController::DeactivateLoupe()
{
	m_pLoupeVis->SetVisible(false);
	this->DeactivateLoupeText();
}

/*============================================================================*/
/*  NAME:   CreateLoupeText												      */
/*============================================================================*/
bool VfaHiBroController::CreateLoupeText()
{
	if (m_vecTextDescription.size() != 0)
	{
		vstr::errp() << "[VfaHiBroController] No text should have been build yet."<< endl;
		return false;
	}

	// store the text as first entry in the vector!
	m_vecTextDescription.push_back(this->CreateTextEntry());
	return true;
}

/*============================================================================*/
/*  NAME:	De/ActivateLoupeText	                                          */
/*============================================================================*/
void VfaHiBroController::ActivateLoupeText()
{
	// now update m_pToolTipText (with default orientation)
	m_vecTextDescription[0]->m_pTextLabel->SetText
		(m_pModel->GetSelectedNode()->GetDescriptionText());
	m_vecTextDescription[0]->m_pTextLabel->Update();

	const float fUnit = m_pModel->GetTileEdgeLength();
	// get half of the half
	const float fHalf = 
		static_cast<float>(m_pModel->GetNumberOfTilesWidth() / 2) / 2.0f + 0.5f;

	this->PositionAndOrientText(0, -fHalf*fUnit, -0.15f*fUnit, 0.05f);
	m_vecTextDescription[0]->m_pTexBoxVis->SetVisible(true);
}
void VfaHiBroController::DeactivateLoupeText()
{
	m_vecTextDescription[0]->m_pTexBoxVis->SetVisible(false);	
}

/*============================================================================*/
/*  NAME:   CreateSignForShowingNrOfSuccs                                     */
/*============================================================================*/
void VfaHiBroController::CreateSignForShowingNrOfSuccs()
{
	if(m_iVaritionsOfShowingNfOfSuccs == HIBRO_SUCCS_COLOR_QUADS)
		BuildQuadsForNodeSuccs();
	else
		BuildTextForNodeSuccs();
}
/*============================================================================*/
/*  NAME:   De/ActivateSignForSuccs											  */
/*============================================================================*/
void VfaHiBroController::ActivateSignForSuccs(float fScale /* = 1.0f*/)
{
	if(m_iVaritionsOfShowingNfOfSuccs == HIBRO_SUCCS_COLOR_QUADS)
		this->ActivateQuadsNodeSuccs(fScale);
	else
		this->ActivateTextNodeSuccs(fScale);
}
void VfaHiBroController::DeactivateSignForSuccs()
{
	if(m_iVaritionsOfShowingNfOfSuccs == HIBRO_SUCCS_COLOR_QUADS)
		this->DeactivateQuadsNodeSuccs();
	else
		this->DeactivateTextNodeSuccs();
}

/*============================================================================*/
/*  NAME:   BuildTextForNodeSuccs		                                      */
/*============================================================================*/
bool VfaHiBroController::BuildTextForNodeSuccs()
{
	const size_t nVecSize = m_vecTextDescription.size();
	if (nVecSize == 0)
	{
		vstr::errp() << "[VfaHiBroController] " 
			<< "Text for Zoom-In should have been build already." << endl;
		return false;
	}
	else if (nVecSize > 1)
		this->DeleteTextEntries(1);

	m_pModel->SetScaleFactorLastRowHeight(0.5f);

	const unsigned iNrOfTiles = m_pModel->GetNumberOfTilesWidth();
	for (unsigned i = 0; i < iNrOfTiles; ++i)
		m_vecTextDescription.push_back(this->CreateTextEntry());

	return true;
}

/*============================================================================*/
/*  NAME:   De/ActivateTextNodeSuccs		                                  */
/*============================================================================*/
void VfaHiBroController::ActivateTextNodeSuccs(float fTimeScale /*= 1.0f*/)
{
	DeactivateTextNodeSuccs();

	// get ids for current map situation in correct order
	std::vector<int> iNodeHndIdsForMap;
	this->GetIdsOfPresentedNaviHnds(iNodeHndIdsForMap);

	int iNrOfNodesVisible = static_cast<int>(iNodeHndIdsForMap.size());
	int iNrOfTilesWidth = m_pModel->GetNumberOfTilesWidth();
	int iIdxFirstCurNodes = -1;
	for (int i = 1; i < iNrOfNodesVisible+1; ++i)
	{
		// find the nodes of the last row and store first occurrence
		if (GetRowForNaviNode(iNodeHndIdsForMap, iNodeHndIdsForMap[i]) == 2)
		{
			iIdxFirstCurNodes = i;
			break;
		}
	}

	// find out whether last column contain an even or an odd nr of tiles
	bool bMiddleEmpty = false;
	int iNrSuccs = iNrOfNodesVisible-iIdxFirstCurNodes;
	if(iNrSuccs%2 == 0)
		bMiddleEmpty = true;

	int iColumnStart = (iNrOfTilesWidth/2)-iNrSuccs/2;
	int iColumnEnd = iColumnStart+iNrSuccs;

	// now position the text lables for all nodes in the last row
	for (int i = iIdxFirstCurNodes; i < iNrOfNodesVisible; ++i)
	{
		int iColumn = this->GetThirdRowColumnForNode(iNodeHndIdsForMap, iNodeHndIdsForMap[i], iColumnStart, iColumnEnd, bMiddleEmpty);
		++iColumn;

		int iNrSuccsForTextLabel =  m_mapLastRowNaviSuccsNrs.find(iNodeHndIdsForMap[i])->second;

		if (iNrSuccsForTextLabel == 0 && (m_iVaritionsOfShowingNfOfSuccs == HIBRO_SUCCS_NUMBER_WITHOUT_ZERO))
			continue;
		else
			m_vecTextDescription[iColumn]->m_pTextLabel->SetText(VistaAspectsConversionStuff::ConvertToString(iNrSuccsForTextLabel));


		m_vecTextDescription[iColumn]->m_pTextLabel->Update();

		const float fUnit = m_pModel->GetTileEdgeLength();
		const int iWidth = m_pModel->GetNumberOfTilesWidth();
		const float fScale = m_pModel->GetScaleFactorLastRowHeight();
		this->PositionAndOrientText(iColumn, fTimeScale*((-iWidth/2*fUnit)+ ((iColumn-1)*fUnit)) ,
			fTimeScale*((-1.5f-0.5f*fScale)*fUnit), 0.001f);	
		m_vecTextDescription[iColumn]->m_pTexBoxVis->SetVisible(true);
	}
}
void VfaHiBroController::DeactivateTextNodeSuccs()
{
	// pay attention, just set unvisible from idx 1
	// because at idx 0 we have the node description
	for (unsigned int i = 1; i < m_vecTextDescription.size(); ++i)
		m_vecTextDescription[i]->m_pTexBoxVis->SetVisible(false);
}

/*============================================================================*/
/*  NAME:   BuildQuadsForNodeSuccs											  */
/*============================================================================*/
bool VfaHiBroController::BuildQuadsForNodeSuccs()
{
	if(m_vecColQuads.size() != 0)
		this->DeleteQuadsNodeSuccs();

	int iNrOfTiles = m_pModel->GetNumberOfTilesWidth();
	m_pModel->SetScaleFactorLastRowHeight(0.2f);

	for (int i = 0; i < iNrOfTiles; ++i)
	{
		VfaQuadVis *pVis = new VfaQuadVis(new VfaPlaneModel());
		pVis->Init();
		this->GetRenderNode()->AddRenderable(pVis);
		pVis->GetProperties()->SetFillPlane(true);
		pVis->GetProperties()->SetDrawBorder(false);
		pVis->GetProperties()->SetFillColor(m_aUnselColorRgba);
		pVis->SetVisible(false);

		m_vecColQuads.push_back(pVis);
	}

	return true;
}

/*============================================================================*/
/*  NAME:	De/ActivateQuadsNodeSuccs                                         */
/*============================================================================*/
void VfaHiBroController::ActivateQuadsNodeSuccs(float fTimeScale /* = 1.0f*/)
{
	DeactivateQuadsNodeSuccs();

	// get ids for current map situation in correct order
	std::vector<int> iNodeHndIdsForMap;
	this->GetIdsOfPresentedNaviHnds(iNodeHndIdsForMap);

	int iNrOfNodesVisible = static_cast<int>(iNodeHndIdsForMap.size());
	int iNrOfTilesWidth = m_pModel->GetNumberOfTilesWidth();
	int iIdxFirstCurNodes = -1;
	for (int i = 1; i < iNrOfNodesVisible+1; ++i)
	{
		// find the nodes of the last row and store first occurrence
		if (GetRowForNaviNode(iNodeHndIdsForMap, iNodeHndIdsForMap[i]) == 2)
		{
			iIdxFirstCurNodes = i;
			break;
		}
	}

	// find out whether last column contain an even or an odd nr of tiles
	bool bMiddleEmpty = false;
	int iNrSuccs = iNrOfNodesVisible-iIdxFirstCurNodes;
	if(iNrSuccs%2 == 0)
		bMiddleEmpty = true;

	int iColumnStart = (iNrOfTilesWidth/2)-iNrSuccs/2;
	int iColumnEnd = iColumnStart+iNrSuccs;

	// now position the text labels for all nodes in the last row
	for (int i = iIdxFirstCurNodes; i < iNrOfNodesVisible; ++i)
	{
		int iColumn = this->GetThirdRowColumnForNode(iNodeHndIdsForMap, iNodeHndIdsForMap[i], iColumnStart, iColumnEnd, bMiddleEmpty);

		int iNrSuccsForTextLabel =  m_mapLastRowNaviSuccsNrs.find(iNodeHndIdsForMap[i])->second;

		if (iNrSuccsForTextLabel == 0)
			m_vecColQuads[iColumn]->GetProperties()->SetFillColor(m_aNoSuccsColor);
		else
			m_vecColQuads[iColumn]->GetProperties()->SetFillColor(m_aSuccsColor);

		VistaQuaternion qOri;
		m_pModel->GetOrientation(qOri);
		qOri.Normalize();

		VistaVector3D v3Normal (0.0f, 0.0f, 1.0f);
		v3Normal =  qOri.Rotate(v3Normal);

		VistaVector3D v3;
		m_pModel->GetPosition(v3);

		const float fUnit = m_pModel->GetTileEdgeLength();
		const int iWidth = m_pModel->GetNumberOfTilesWidth();
		const float fScale = m_pModel->GetScaleFactorLastRowHeight();

		float fx = (-iWidth/2*fUnit)+((iColumn)*fUnit);
		float fy = -fUnit - 0.5f*fUnit - fScale*fUnit*0.5f;

		m_vecColQuads[iColumn]->GetModel()->SetNormal(v3Normal);
		m_vecColQuads[iColumn]->GetModel()->SetTranslation(v3
			+ qOri.Rotate(VistaVector3D(fTimeScale*fx, fTimeScale*fy, 0.001f)));

		m_vecColQuads[iColumn]->GetModel()->SetExtents(fTimeScale*0.95f*fUnit, 
			0.55f*(fTimeScale*fScale*fUnit));
		m_vecColQuads[iColumn]->SetVisible(true);
	}
}
void VfaHiBroController::DeactivateQuadsNodeSuccs()
{
	const size_t nSize = m_vecColQuads.size();
	for (size_t i = 0; i < nSize; ++i)
	{
		m_vecColQuads[i]->GetProperties()->SetFillColor(m_aNoSuccsColor);
		m_vecColQuads[i]->SetVisible(false);
	}
}
/*============================================================================*/
/*  NAME:   DeleteQuadsNodeSuccs		                                      */
/*============================================================================*/
void VfaHiBroController::DeleteQuadsNodeSuccs()
{

	unsigned int iNrOfQuads = m_pModel->GetNumberOfTilesWidth();
	
	if(iNrOfQuads == 0)
		return;

	for (int i = iNrOfQuads-1; i >= 0; --i)
	{
		VfaQuadVis *pVis = m_vecColQuads[i];
		this->GetRenderNode()->RemoveRenderable(pVis);
		delete pVis;
		m_vecColQuads.pop_back();
	}
	m_vecColQuads.clear();
}

/*============================================================================*/
/*  NAME:   CreateTextEntry				                                      */
/*============================================================================*/
VfaHiBroText *VfaHiBroController::CreateTextEntry()
{
	VfaHiBroText *pTxt = new VfaHiBroText();

	pTxt->m_pTextLabel = new Vfl3DTextLabel();
	if(pTxt->m_pTextLabel->Init())
	{
		pTxt->m_pTextLabel->SetVisible(true);
		pTxt->m_pTextLabel->SetTextSize(0.03f);
		float fColor[4] = {1.0f, 0.0f, 0.0f, 1.0f};
		pTxt->m_pTextLabel->SetColor(fColor);
		pTxt->m_pTextLabel->SetTextFollowViewDir(false);
	}

	pTxt->m_pTexBoxVis = new VfaTexturedBoxVis(new VfaTexturedBoxModel());
	if(pTxt->m_pTexBoxVis->Init())
	{
		this->GetRenderNode()->AddRenderable(pTxt->m_pTexBoxVis);
		pTxt->m_pTexBoxVis->SetVisible(false);
		// set line color
		float fColorN[4] = {0.8f, 0.8f, 1.0f, 1.0f};
		pTxt->m_pTexBoxVis->GetProperties()->SetLineColor(fColorN);
	}

	pTxt->m_pR2T = new VflRenderToTexture(512,512, false);
	pTxt->m_pR2T->SetRenderNode(this->GetRenderNode());
	pTxt->m_pR2T->Init();
	float fC [] = {0.8f, 0.8f, 1.0f, 1.0f};
	pTxt->m_pR2T->SetBackgroundColor(fC);

	pTxt->m_pViewAdapter = new VflRenderToTexture::C2DViewAdapter;
	pTxt->m_pViewAdapter->Init();
	pTxt->m_pViewAdapter->RegisterTarget(pTxt->m_pTextLabel);
	pTxt->m_pR2T->SetTarget(pTxt->m_pViewAdapter);	

	return pTxt;
}

/*============================================================================*/
/*  NAME:   DeleteTextEntries			                                      */
/*============================================================================*/
void VfaHiBroController::DeleteTextEntries(int iStartIdx)
{
	const int nVecSize = static_cast<int>(m_vecTextDescription.size());	

	// already empty
	if (nVecSize == 0)
		return;

	for (int i = nVecSize-1; i >= iStartIdx; --i)
	{
		VfaHiBroText *pTxt = m_vecTextDescription[i];
		
		this->GetRenderNode()->RemoveRenderable(pTxt->m_pTextLabel);
		this->GetRenderNode()->RemoveRenderable(pTxt->m_pTexBoxVis);
		this->GetRenderNode()->RemoveRenderable(pTxt->m_pViewAdapter);
		this->GetRenderNode()->RemoveRenderable(pTxt->m_pR2T);


		pTxt->m_pViewAdapter->UnregisterTarget(pTxt->m_pTextLabel);
		delete pTxt->m_pViewAdapter;
		delete pTxt->m_pR2T;

		delete pTxt->m_pTextLabel;
		delete pTxt->m_pTexBoxVis;

		m_vecTextDescription.pop_back();
	}
	if(iStartIdx == 0)
		m_vecTextDescription.clear();
}

/*============================================================================*/
/*  NAME:	PositionAndOrientText	                                          */
/*============================================================================*/
bool VfaHiBroController::PositionAndOrientText(int iIdx, float fx /*=0.0f*/, float fy /*=0.0f*/, float fz /*=0.0f*/)
{
	// idx of vec
	int iSize = (int)m_vecTextDescription.size();
	if (iIdx < 0 || iIdx > iSize)
		return false;

	VfaHiBroText *pTxt = m_vecTextDescription[iIdx];

	pTxt->m_pTextLabel->SetPosition(VistaVector3D(0.0f, 0.0f, 0.0f));
	pTxt->m_pTextLabel->SetOrientation(VistaQuaternion());

	VistaVector3D v3Min, v3Max;
	pTxt->m_pTextLabel->GetBounds(v3Min, v3Max);

	const float fExtendX = v3Max[0] - v3Min[0];
	const float fExtendY = v3Max[1] - v3Min[1];
	
	const float fLabelAspect = fExtendX / fExtendY;
	
	const float aLabelExtents[2] = {fExtendX, fExtendY};
	pTxt->m_pViewAdapter->SetOrthoParams(
		v3Min[0] - 0.01f * aLabelExtents[0],
		v3Min[0] + 1.01f * aLabelExtents[0],
		v3Min[1] - 0.01f * aLabelExtents[1],
		v3Min[1] + 1.01f * aLabelExtents[1],
		-1.0f, 1.0f);

	pTxt->m_pR2T->Resize(512, 512);
	pTxt->m_pTextLabel->Draw2D();	

	pTxt->m_pTexBoxVis->GetTexturedBoxModel()->SetTexture(pTxt->m_pR2T->GetTexture());
	
	// extends:
	VistaQuaternion qOri;
	m_pModel->GetOrientation(qOri);
	VistaVector3D v3;
	m_pModel->GetPosition(v3);
	
	const float fUnit = m_pModel->GetTileEdgeLength();
	const int iWidth = m_pModel->GetNumberOfTilesWidth();
	// get half of the half
	const float fHalf = static_cast<float>(iWidth / 2) / 2.0f + 0.5f;

	const float fFieldExtendX = static_cast<float>(iWidth / 2) * fUnit - 0.25f * fUnit;
	const float fFieldExtendY = 0.4f * fUnit;
	const float fFieldAspect = fFieldExtendX / fFieldExtendY;
	const float fFieldDepth = 0.001f;

	string strText = pTxt->m_pTextLabel->GetText();

	// @todo It is assumed that we have either 1 or 2 lines, but not more.
	const bool bIsMultiLine = strText.find('\n') != string::npos;
	const float fMaxSizeY = bIsMultiLine ? fFieldExtendY : 0.5f * fFieldExtendY;

	// First case:
	// Label has the same aspect as the target field, so set it to the same absolute size.
	if(fLabelAspect == fFieldAspect)
	{
		const float fHeight = fMaxSizeY;
		const float fWidth	= fHeight * fLabelAspect; 
		pTxt->m_pTexBoxVis->GetTexturedBoxModel()->SetExtents(
			fWidth, fHeight, fFieldDepth);
	}
	// Second case:
	// Label's aspect is larger than the one of the target field. So we have to determine
	// the size by the width. Remember, aspect was defined as width per size!
	else if(fLabelAspect > fFieldAspect)
	{
		const float fHeight = min(fFieldExtendX / fLabelAspect, fMaxSizeY);
		const float fWidth	= fHeight * fLabelAspect; 

		pTxt->m_pTexBoxVis->GetTexturedBoxModel()->SetExtents(
			fWidth, fHeight, fFieldDepth);

	}
	// Third case:
	// Inverse to second case. Determine size by height.
	else
	{
		const float fHeight = min(fFieldExtendY, fMaxSizeY);
		const float fWidth	= fHeight * fLabelAspect; 

		pTxt->m_pTexBoxVis->GetTexturedBoxModel()->SetExtents(
			fWidth, fHeight, fFieldDepth);

	}
	
	pTxt->m_pTexBoxVis->GetTexturedBoxModel()->SetRotation(qOri);
	pTxt->m_pTexBoxVis->GetTexturedBoxModel()->SetTranslation(v3+
		qOri.Rotate(VistaVector3D(fx, fy, fz)));

	return true;
}

/*============================================================================*/
/*  NAME:	State working	                                                  */
/*============================================================================*/
void VfaHiBroController::SetHiBrotoStateLinearAnimation()
{
	this->SetHiBroState(HIBRO_LINEAR_ANIMATION);
	this->DeactivateSignForSuccs();
}
void VfaHiBroController::SetHiBrotoStateAnimation()
{
	this->SetHiBroState(HIBRO_ANIMATION);
	
	this->DeactivateSignForSuccs();
	this->DeactivateLoupe();
}
void VfaHiBroController::SetHiBrotoStateActive()
{
	this->SetHiBroState(HIBRO_ACTIVE);
	ActivateSignForSuccs();
}
void VfaHiBroController::SetHiBrotoStateInactive()
{
	this->SetHiBroState(HIBRO_INACTIVE);

	this->DeactivateSignForSuccs();
	this->DeactivateLoupe();
}
void VfaHiBroController::SetHiBrotoStateDisabled()
{
	this->SetHiBroState(HIBRO_DISABLED);
}
void VfaHiBroController::SetHiBroState(int iNewState)
{
	// set new state and store old one
	m_iLastState = m_iCurrentState;
	m_iCurrentState = iNewState;
	this->PrintState(m_iLastState, m_iCurrentState);
}
void VfaHiBroController::PrintState(int iOldState, int iCurrentState) const
{
	std::string strOldState = "UNKNOWN";
	std::string strNewState = "UNKNOWN";
	switch(iOldState)
	{
		case HIBRO_INACTIVE: strOldState = "INACTIVE"; break;
		case HIBRO_ACTIVE: strOldState = "ACTIVE"; break;
		case HIBRO_ANIMATION: strOldState = "ANIMATION"; break;
		case HIBRO_LINEAR_ANIMATION: strOldState = "LINEAR_ANIMATION"; break;
		case HIBRO_DISABLED: strOldState = "DISABLED"; break;
	}
	switch(iCurrentState)	
	{
		case HIBRO_INACTIVE: strNewState = "INACTIVE"; break;
		case HIBRO_ACTIVE: strNewState = "ACTIVE"; break;
		case HIBRO_ANIMATION: strNewState = "ANIMATION"; break;
		case HIBRO_LINEAR_ANIMATION: strNewState = "LINEAR_ANIMATION"; break;
		case HIBRO_DISABLED: strNewState = "DISABLED"; break;
	}
	vstr::outi()<<"HiBroState change : "<<strOldState.c_str()<<" -> "<<strNewState.c_str()<<endl;
}


/*============================================================================*/
/*  END OF FILE "VfaHiBroController.cpp"									  */
/*============================================================================*/


