/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifdef WIN32
#include <Windows.h>
#endif

#include <GL/glut.h>
#include "VfaPieTimer.h"

#include <VistaFlowLibAux/Widgets/Sphere/VfaSphereWidget.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <cassert>
#include <cstring>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaPieTimer									      */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaPieTimer::VfaPieTimer()
	:	m_pModel(NULL)
{}

VfaPieTimer::~VfaPieTimer()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VfaPieTimer::DrawOpaque()
{
	VfaPieTimerProps *pProps = this->GetProperties();
	glMatrixMode(GL_MODELVIEW);

	glPushAttrib(GL_ENABLE_BIT|GL_LIGHTING_BIT|GL_LINE_BIT);
	glPushMatrix();

	glEnable(GL_NORMALIZE);
	glEnable(GL_DEPTH_TEST);

	float fColor[4] = {1.0f,1.0f,1.0f,1.0f};
	pProps->GetColor(fColor);

	if(pProps->GetUseLighting())
	{
		glEnable(GL_LIGHTING);
		glDisable(GL_COLOR_MATERIAL);
		GLfloat fAmbient[] = {0.1f, 0.1f, 0.1f, 1.0f};
		GLfloat fSpecular[] = {1.0f,1.0f,1.0f,1.0f};
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,fAmbient);
		glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,fColor);
		glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,fSpecular);
		glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,64.0f);	
	}
	else
	{
		glDisable(GL_LIGHTING);
		glColor4fv(fColor);
	}
	
	float fSlice = pProps->GetSlices();
	float fLoops = pProps->GetLoop();
	float fStart = pProps->GetStartAngle();
	float fSweep = pProps->GetSweepAngle();
	float fInnerRadius = pProps->GetInnerRadius();
	float fOuterRadius = pProps->GetOuterRadius();

	float fCenter[3];
	pProps->GetCenter(fCenter);
	float fRotate[4];
	pProps->GetRotate(fRotate);

	glTranslatef(fCenter[0], fCenter[1], fCenter[2]);
	glRotatef(fRotate[0], fRotate[1], fRotate[2], fRotate[3]);

	GLUquadric *q = gluNewQuadric();

	if(pProps->GetCounterClockwiseRotation())
		gluQuadricOrientation(q, GLU_INSIDE);
	
	gluPartialDisk(q, static_cast<double>(fInnerRadius),
		static_cast<double>(fOuterRadius), static_cast<int>(fSlice),
		static_cast<int>(fLoops), static_cast<double>(fStart),
		static_cast<double>(fSweep));

	glPopMatrix();
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VfaPieTimer::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VfaPieTimer::VfaPieTimerProps *VfaPieTimer::GetProperties() const
{
	return 
		static_cast<VfaPieTimer::VfaPieTimerProps *>
											(IVflRenderable::GetProperties());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VfaPieTimer::CreateProperties() const
{
	return new VfaPieTimer::VfaPieTimerProps();
}


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaPieTimer::VfaPieTimerProps::VfaPieTimerProps()
:	m_bUseLighting(false),
	m_fSweepAngle(90.0f),
	m_fStartAngle(180.0f),
	m_fLoops(20.0f),
	m_fSlices(20.0f),
	m_fInnerRadius(0.1f),
	m_fOuterRadius(0.5f),
	m_bCounterClockwise(true)
{
	m_fColor[0] = 0.7f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 0.0f;
	m_fColor[3] = 1.0f;

	m_v3Center.SetToZeroVector();

	m_fRotate[1] = m_fRotate[2] = m_fRotate[3] = 0.0f;
	m_fRotate[0] = 1.0f;

	VistaVector3D v3Dir (1.0f, 0.0f, 0.0f);
	m_qRot180 = VistaQuaternion(
							VistaAxisAndAngle(v3Dir, Vista::DegToRad(180.0f)));
}

VfaPieTimer::VfaPieTimerProps::~VfaPieTimerProps()
{}


/*============================================================================*/
/*  NAME      :   Set/GetColor                                                */
/*============================================================================*/
bool VfaPieTimer::VfaPieTimerProps::SetColor(const VistaColor& color)
{
	float fColor[4];
	color.GetValues(fColor);
	return this->SetColor(fColor);
}

VistaColor VfaPieTimer::VfaPieTimerProps::GetColor() const
{
	return VistaColor(m_fColor);
}

bool VfaPieTimer::VfaPieTimerProps::SetColor(float fColor[4])
{
	if(	fColor[0] == m_fColor[0] &&
		fColor[1] == m_fColor[1] &&
		fColor[2] == m_fColor[2] &&
		fColor[3] == m_fColor[3])
	{
		return false;
	}

	memcpy(m_fColor, fColor, 4*sizeof(float));
	this->Notify(MSG_COLOR_CHG);
	return true;
}

void VfaPieTimer::VfaPieTimerProps::GetColor(float fColor[4]) const
{
	memcpy(fColor, m_fColor, 4*sizeof(float));
}

/*============================================================================*/
/*  NAME      :   Set/GetProperties                                           */
/*============================================================================*/
bool VfaPieTimer::VfaPieTimerProps::SetUseLighting(bool b)
{
	if(m_bUseLighting == b)
		return false;

	m_bUseLighting = b;
	return true;
}

bool VfaPieTimer::VfaPieTimerProps::GetUseLighting() const
{
	return m_bUseLighting;
}

void VfaPieTimer::VfaPieTimerProps::SetSweepAngle(float fAngle)
{
	m_fSweepAngle = fAngle;
}
float VfaPieTimer::VfaPieTimerProps::GetSweepAngle() const
{
	return m_fSweepAngle;
}

void VfaPieTimer::VfaPieTimerProps::SetStartAngle(float fAngle)
{
	m_fStartAngle = fAngle;
}
float VfaPieTimer::VfaPieTimerProps::GetStartAngle() const
{
	return m_fStartAngle;
}

void VfaPieTimer::VfaPieTimerProps::SetLoop(float fLoops)
{
	m_fLoops = fLoops;
}
float VfaPieTimer::VfaPieTimerProps::GetLoop() const
{
	return m_fLoops;
}

void VfaPieTimer::VfaPieTimerProps::SetSlices(float fSlices)
{
	m_fSlices = fSlices;
}
float VfaPieTimer::VfaPieTimerProps::GetSlices() const
{
	return m_fSlices;
}

void VfaPieTimer::VfaPieTimerProps::SetInnerRadius(float fRadius)
{
	m_fInnerRadius = fRadius;
}
float VfaPieTimer::VfaPieTimerProps::GetInnerRadius() const
{
	return m_fInnerRadius;
}

void VfaPieTimer::VfaPieTimerProps::SetOuterRadius(float fRadius)
{
	m_fOuterRadius = fRadius;
}
float VfaPieTimer::VfaPieTimerProps::GetOuterRadius() const
{
	return m_fOuterRadius;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter			                                          */
/*                                                                            */
/*============================================================================*/
bool VfaPieTimer::VfaPieTimerProps::SetCenter(float fC[])
{
	VistaVector3D v3(fC);
	return this->SetCenter(v3);
}
bool VfaPieTimer::VfaPieTimerProps::SetCenter(double dC[])
{
	VistaVector3D v3(dC);
	return this->SetCenter(v3);
}
bool VfaPieTimer::VfaPieTimerProps::SetCenter(const VistaVector3D &v3C)
{
	if(m_v3Center == v3C)
		return false;

	m_v3Center = v3C;
	
	this->Notify(MSG_CENTER_CHANGE);
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter											          */
/*                                                                            */
/*============================================================================*/
void VfaPieTimer::VfaPieTimerProps::GetCenter(float fC[3]) const
{
	m_v3Center.GetValues(fC);
}

void VfaPieTimer::VfaPieTimerProps::GetCenter(double dC[3]) const
{
	m_v3Center.GetValues(dC);
}

void VfaPieTimer::VfaPieTimerProps::GetCenter(VistaVector3D &v3C) const
{
	v3C = m_v3Center;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetOrientation/GetRotation						          */
/*                                                                            */
/*============================================================================*/
void VfaPieTimer::VfaPieTimerProps::SetOrientation
												   (const VistaQuaternion &qOri)
{
	VistaQuaternion qTemp;
	if (m_bCounterClockwise)
		qTemp = qOri*m_qRot180;
	else
		qTemp = qOri;

	//second possibility to compute angle: Vista::RadToDeg(acos(qOri[3])*2);
	m_fRotate[0] = qTemp.GetAxisAndAngle().m_fAngle*180.0f/Vista::Pi; 
 	
	m_fRotate[1] = qTemp[0];
	m_fRotate[2] = qTemp[1];
	m_fRotate[3] = qTemp[2];


	this->Notify(MSG_ORIENTATION_CHANGE);
}
void VfaPieTimer::VfaPieTimerProps::GetRotate(float fRotate[4]) const
{
	fRotate[0] = m_fRotate[0];
	fRotate[1] = m_fRotate[1];
	fRotate[2] = m_fRotate[2];
	fRotate[3] = m_fRotate[3];
}

void VfaPieTimer::VfaPieTimerProps::SetCounterClockwiseRotation(bool bCCW)
{
	m_bCounterClockwise = bCCW;
	this->SetStartAngle(bCCW ? 180.0f : 0.0f);
}
bool VfaPieTimer::VfaPieTimerProps::GetCounterClockwiseRotation() const
{
	return m_bCounterClockwise;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaPieTimer.cpp"										      */
/*============================================================================*/

