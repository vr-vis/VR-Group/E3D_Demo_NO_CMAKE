

set( RelativeDir "./Navigation/HiBro/TimerFeedback" )
set( RelativeSourceGroup "Source Files\\Navigation\\HiBro\\TimerFeedback" )

set( DirFiles
	VfaPieTimer.cpp
	VfaPieTimer.h
	VfaTimerFeedbackConfig.cpp
	VfaTimerFeedbackConfig.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

