/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1999-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFAPIETIMER_H
#define _VFAPIETIMER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VfaSphereModel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VfaPieTimer : public IVflRenderable
{
public: 
	VfaPieTimer();
	virtual ~VfaPieTimer();


	/**
	* here the rendering is done
	*/
	virtual void DrawOpaque();

	virtual unsigned int GetRegistrationMode() const;

	
	//void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/**
	*
	*/
	class VfaPieTimerProps : public IVflRenderable::VflRenderableProperties
	{
	public:
		enum{
			MSG_COLOR_CHG = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_SOLID_CHG,
			MSG_LIGHTING_CHG,
			MSG_SWEEP_ANGLE,
			MSG_START_ANGLE,
			MSG_LOOPS,
			MSG_SLICES,
			MSG_CENTER_CHANGE,
			MSG_ORIENTATION_CHANGE,
			MSG_LAST
		};

		VfaPieTimerProps();
		virtual ~VfaPieTimerProps();

		bool SetColor(const VistaColor& color);
		VistaColor GetColor() const;
		bool SetColor(float fColor[4]);
		void GetColor(float fColor[4]) const;

		bool SetUseLighting(bool b);
		bool GetUseLighting() const;

		void SetSweepAngle(float fAngle);
		float GetSweepAngle() const;

		void SetStartAngle(float fAngle);
		float GetStartAngle() const;

		void SetLoop(float fLoops);
		float GetLoop() const;

		void SetSlices(float fSlices);
		float GetSlices() const;

		void SetInnerRadius(float fRadius);
		float GetInnerRadius() const;

		void SetOuterRadius(float fRadius);
		float GetOuterRadius() const;

		bool SetCenter(float fC[3]);
		bool SetCenter(double dC[3]);
		bool SetCenter(const VistaVector3D &v3C);

		void GetCenter(float fC[3]) const;
		void GetCenter(double dC[3]) const;
		void GetCenter(VistaVector3D &v3C) const;

		void SetOrientation(const VistaQuaternion &qOri);
		void GetRotate(float fRotate[4]) const;

		void SetCounterClockwiseRotation(bool bCCW);
		bool GetCounterClockwiseRotation() const;

	private:
		bool m_bUseLighting;

		float m_fColor[4];

		float m_fSweepAngle;
		float m_fStartAngle;
		float m_fLoops;
		float m_fSlices;

		float m_fInnerRadius;
		float m_fOuterRadius;
		VistaVector3D m_v3Center;
		VistaQuaternion m_qRot180;
		float m_fRotate[4];

		bool m_bCounterClockwise;
	};

	virtual VfaPieTimerProps *GetProperties() const;

protected:

	virtual VflRenderableProperties* CreateProperties() const;
	
private:
	VfaSphereModel *m_pModel;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VfaPieTimer_H
