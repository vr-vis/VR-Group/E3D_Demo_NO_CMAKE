/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFATIMERFEEDBACKCONFIG_H
#define _VFATIMERFEEDBACKCONFIG_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaAspects/VistaObserver.h>
#include <VistaBase/VistaQuaternion.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Data/VveVtkData.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaPieTimer;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*
* 
*/
class VfaTimerFeedbackConfig
{
public:
	enum
	{
		MSG_PARTIALDISK
	};

	VfaTimerFeedbackConfig(VflRenderNode *pRenderNode, 
							unsigned int iMode);
	virtual ~VfaTimerFeedbackConfig();

	void SetPosAndOri(const VistaVector3D &v3, VistaQuaternion &qOri);	
	void UpdateParams(double m_fTime);

	void ResetFeedback();

	IVflRenderable* GetFeedback() const;

protected:

	void InitFeedback();
	void InitPartialDisk();

	void UpdatePartialDiskAccordingTime(float fParam);

private:

	VflRenderNode			*m_pRNHiBro;

	unsigned int			m_iMode;
	VfaPieTimer			*m_pPie;

	float					m_fColor[4];
	
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VfaTimerFeedbackConfig_H
