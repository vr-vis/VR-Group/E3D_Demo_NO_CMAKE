/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VfaTimerFeedbackConfig.h" 
#include "VfaPieTimer.h"

// FlowLibAux stuff
#include <VistaFlowLibAux/Interaction/VfaApplicationContextObject.h>
#include <VistaFlowLibAux/Navigation/HiBro/VfaHiBroController.h>
#include <VistaFlowLibAux/Navigation/HiBro/VfaHiBroModel.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>

// ViSTA stuff
#include <VistaAspects/VistaPropertyAwareable.h>
#include <VistaBase/VistaMathBasics.h>

#include <iostream>
#include <cassert>

using namespace std;


/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
VfaTimerFeedbackConfig::VfaTimerFeedbackConfig( VflRenderNode *pRenderNode, 
												  unsigned int iMode)
:	m_pRNHiBro(pRenderNode),
	m_iMode(iMode)
{
	this->InitFeedback();
}

VfaTimerFeedbackConfig::~VfaTimerFeedbackConfig()
{}

/*============================================================================*/
/* NAME: InitFeedback				                                          */
/*============================================================================*/
IVflRenderable* VfaTimerFeedbackConfig::GetFeedback() const
{
	switch(m_iMode)	
	{
	case MSG_PARTIALDISK:
		return m_pPie;
	default:
		return NULL;
	}
}

/*============================================================================*/
/* NAME: InitFeedback				                                          */
/*============================================================================*/
void VfaTimerFeedbackConfig::InitFeedback()
{	
	switch(m_iMode)	
	{
	case MSG_PARTIALDISK:
		this->InitPartialDisk();
		break;
	default:
		break;
	}
}
/*============================================================================*/
/* NAME: InitPartialDisk		                                              */
/*============================================================================*/
void VfaTimerFeedbackConfig::InitPartialDisk()
{
	m_fColor[0] = 0.0f;
	m_fColor[1] = 1.0f;
	m_fColor[2] = 0.0f;
	m_fColor[3] = 1.0f;

	m_pPie = new VfaPieTimer();
	if(m_pPie->Init())
	{
		m_pRNHiBro->AddRenderable(m_pPie);
		m_pPie->GetProperties()->SetInnerRadius(0.0f);
		m_pPie->GetProperties()->SetOuterRadius(0.025f);
		m_pPie->GetProperties()->SetCounterClockwiseRotation(true);
	}
	m_pPie->SetVisible(false);
}

/*============================================================================*/
/* NAME: SetCenterToVisualFeedback                                            */
/*============================================================================*/
void VfaTimerFeedbackConfig::SetPosAndOri(const VistaVector3D &v3,
										   VistaQuaternion &qOri)
{
	qOri.Normalize();

	switch(m_iMode)
	{
	case MSG_PARTIALDISK:
		m_pPie->GetProperties()->SetCenter(v3);
		m_pPie->GetProperties()->SetOrientation(qOri);
		break;
	default:
		break;
	}
}

/*============================================================================*/
/* NAME: Update					                                              */
/*============================================================================*/
void VfaTimerFeedbackConfig::UpdateParams(double m_fTime)
{
	switch(m_iMode)
	{
	case MSG_PARTIALDISK:
		this->UpdatePartialDiskAccordingTime(static_cast<float>(m_fTime));
		break;
	default:
		break;
	}
}
/*============================================================================*/
/* NAME: UpdateTimerFeedbackAccording Time									  */
/*============================================================================*/
void VfaTimerFeedbackConfig::UpdatePartialDiskAccordingTime(float fParam)
{
	if (fParam >= 0.0f && fParam < 1.0f)
	{
		if (!m_pPie->GetVisible())
			m_pPie->SetVisible(true);

		m_pPie->GetProperties()->SetSweepAngle(fParam*360);
	}

	if (fParam >= 10.0f)
	{
		if (m_pPie->GetVisible())
			ResetFeedback();
	}
	else if (fParam >= 1.0f)
	{
		m_pPie->GetProperties()->SetSweepAngle(360.0f);
		m_pPie->GetProperties()->SetOuterRadius(0.025f*1.25f);
	}
}


/*============================================================================*/
/* NAME: DisableFeedback		                                              */
/*============================================================================*/
void VfaTimerFeedbackConfig::ResetFeedback()
{
	switch(m_iMode)
	{
	case MSG_PARTIALDISK:
		if (m_pPie->GetVisible())
		{
			m_pPie->SetVisible(false);
			m_pPie->GetProperties()->SetSweepAngle(0.0f);
			m_pPie->GetProperties()->SetOuterRadius(0.025f);
		}
		break;
	default:
		break;
	}
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VfaTimerFeedbackConfig.CPP"					              				          */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

