/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaTreeDrawStrategy.h"
#include "VfaStructureNode.h"

#include <VistaBase/VistaVectorMath.h>

#include <vector>
#include <cassert>
#include <algorithm>

using namespace std;


/*============================================================================*/
/* LOCAL DEFINITIONS                                                          */
/*============================================================================*/
struct SNodeInfo
{
	SNodeInfo()
	:	m_uiInverseDepth(0),
		m_uiLeafSupport(0)		
	{ }

	size_t m_uiInverseDepth;
	size_t m_uiLeafSupport;
};

struct SRange
{
	SRange()
	:	m_fLower(0.0f),
		m_fUpper(1.0f)
	{ }

	float m_fLower;
	float m_fUpper;
};


/*============================================================================*/
/* IMPLEMENTAION                                                              */
/*============================================================================*/
VfaTreeDrawStrategy::VfaTreeDrawStrategy()
:	m_bTransformChanged(true)
{

}
VfaTreeDrawStrategy::~VfaTreeDrawStrategy()
{

}

/*============================================================================*/
/* NAME: SetDrawCenter                                                        */
/*============================================================================*/
void VfaTreeDrawStrategy::SetDrawCenter(const VistaVector3D &v3Center)
{
	VistaVector3D v3Old;
	GetDrawCenter(v3Old);

	if(v3Old != v3Center)
	{
		m_bTransformChanged = true;
	}

	IVfaStructuredDrawStrategy::SetDrawCenter(v3Center);
}
/*============================================================================*/
/* NAME: SetDrawOrientation                                                   */
/*============================================================================*/
void VfaTreeDrawStrategy::SetDrawOrientation(const VistaQuaternion &qOri)
{
	VistaQuaternion qOld;
	GetDrawOrientation(qOld);

	if(qOld != qOri)
	{
		m_bTransformChanged = true;
	}

	IVfaStructuredDrawStrategy::SetDrawOrientation(qOri);
}
/*============================================================================*/
/* NAME:	SetDrawExtents                                                    */
/*============================================================================*/
void VfaTreeDrawStrategy::SetDrawExtents(float fWidth, float fHeight)
{
	float fOldW, fOldH;
	GetDrawExtents(fOldW, fOldH);

	if(fOldW != fWidth || fOldH != fHeight)
	{
		m_bTransformChanged = true;
	}

	IVfaStructuredDrawStrategy::SetDrawExtents(fWidth, fHeight);
}

/*============================================================================*/
/* NAME:	GetEdges                                                          */
/*============================================================================*/
void VfaTreeDrawStrategy::GetEdges(
	std::map<int, IVfaStructuredDrawStrategy::SEdge>& mapEdges)
{
	if(m_bTransformChanged)
	{
		ChangeLayoutRefFrame();
		m_bTransformChanged = false;
	}

	mapEdges = m_mapFinalEdges;
}
/*============================================================================*/
/* NAME:	GetNodeCenters                                                    */
/*============================================================================*/
void VfaTreeDrawStrategy::GetNodeCenters(std::map<int, VistaVector3D> &mapNodes)
{
	if(m_bTransformChanged)
	{
		ChangeLayoutRefFrame();
		m_bTransformChanged = false;
	}

	mapNodes = m_mapFinalNodes;
}

/*============================================================================*/
/* NAME: Update	                                                              */
/*============================================================================*/
void VfaTreeDrawStrategy::Update()
{
	if(GetInitialNode() == 0)
		return;

	m_mapEdges.clear();
	m_mapNodes.clear();
	
	vector<VfaStructureNode*>	vecTraversal;
	map<int, SNodeInfo>			mapNodeInfo;

	// Determine the tree support for that tree.
	BuildNodeInfo(GetInitialNode(), vecTraversal, mapNodeInfo);
	// Generate the actual node layout.
	GenerateLayout(vecTraversal, mapNodeInfo);
	// Transform the layout into the correct reference frame.
	ChangeLayoutRefFrame();
}

/*============================================================================*/
/* NAME:	BuildNodeInfo                                                     */
/*============================================================================*/
void VfaTreeDrawStrategy::BuildNodeInfo(
	VfaStructureNode* pInitialNode,
	std::vector<VfaStructureNode*>& vecTraversal,
	std::map<int, SNodeInfo>& mapNodeInfo) const
{
	//--------------------------------------------------------------------------
	// Create structure used for bottom-up breadth first tree traversal.
	// Holds the node that is currently being visited.
	VfaStructureNode* pCurNode = 0;
	// Convenience variable pointing to children of the node currently being
	// visited.
	VfaStructureNode* pChildNode	= 0;
	// Used for later bottom-up breadth-first traversal.
	vecTraversal.push_back(pInitialNode);
	// Init depth.
	mapNodeInfo[pInitialNode->GetId()].m_uiInverseDepth = 1;
	// Max depth of the tree.
	size_t uiMaxDepth = 1;
	
	for(size_t i=0; i<vecTraversal.size(); ++i)
	{
		pCurNode = vecTraversal[i];

		for(int j=0; j<pCurNode->GetNrSuccs(); ++j)
		{		
			// Retrieve neext successor.
			pChildNode = pCurNode->GetSuccForIdx(j);			
			vecTraversal.push_back(pChildNode);

			mapNodeInfo[pChildNode->GetId()].m_uiInverseDepth = 
				mapNodeInfo[pCurNode->GetId()].m_uiInverseDepth + 1;

			uiMaxDepth = std::max(
					uiMaxDepth,
					mapNodeInfo[pChildNode->GetId()].m_uiInverseDepth
				);
		}
	}

	//--------------------------------------------------------------------------
	// Determine leaf support.
	// Collect the leaf node support by performing a bottom-to-top breadth-first
	// traversal collecting node weights.
	for(int i = static_cast<int>(vecTraversal.size()-1); i >= 0; --i)
	{
		pCurNode = vecTraversal[i];
		
		// If there are no successors, we do not have any children and hence
		// are a leaf node. Leaf nodes have a weight of 1.
		if(pCurNode->GetNrSuccs() == 0)
		{
			mapNodeInfo[pCurNode->GetId()].m_uiLeafSupport	= 1;
		}
		// The weight for non-leaves is the sum of all childrens' weights.
		else
		{
			mapNodeInfo[pCurNode->GetId()].m_uiLeafSupport = 0;
			for(int j=0; j<pCurNode->GetNrSuccs(); ++j)
			{
				pChildNode = pCurNode->GetSuccForIdx(j);
				// Set node weight which is the number of leaf nodes supported
				// by each of the current nodes successors.
				mapNodeInfo[pCurNode->GetId()].m_uiLeafSupport += 
					mapNodeInfo[pChildNode->GetId()].m_uiLeafSupport;
			}		
		}

		// Set the inverse depth which is the maximal depth + 1 minus
		// the node's actual depth.
		mapNodeInfo[pCurNode->GetId()].m_uiInverseDepth =
			uiMaxDepth + 1
			- mapNodeInfo[pCurNode->GetId()].m_uiInverseDepth;
	}
}

/*============================================================================*/
/* NAME:	GenerateLayout                                                    */
/*============================================================================*/
void VfaTreeDrawStrategy::GenerateLayout(
	const std::vector<VfaStructureNode*>& vecTraversal,
	const std::map<int, SNodeInfo>& mapNodeInfo)
{
	map<int, SNodeInfo>::const_iterator cit =
		mapNodeInfo.find(vecTraversal.front()->GetId());
	
	// This should never ever happen or a previous step succeed.
	assert(cit != mapNodeInfo.end());

	//--------------------------------------------------------------------------
	// Layout variables.
	// Max depth is required for vertical layout (height).
	// It is defined via the inverse depth of the initial node.
	const size_t uiMaxDepth = cit->second.m_uiInverseDepth;
	// Max width is required for horizontal layout (width).
	// It is defined as the leaf support of the initial node.
	const size_t uiMaxWidth = cit->second.m_uiLeafSupport;
	//
	const float fVertStep = 1.0f / static_cast<float>(uiMaxDepth);

	//--------------------------------------------------------------------------
	// Layout process variables.
	// Range structure. Holds for each node the available horizontal range that
	// can be used to layout supported leaf nodes.
	map<int, SRange> mapHorRanges;
	// Init initial node's range (which is essential the full available width).
	mapHorRanges[vecTraversal.front()->GetId()].m_fLower = 0.0f;
	mapHorRanges[vecTraversal.front()->GetId()].m_fUpper = 1.0f;
	// Helper, holding the currently visited node.
	VfaStructureNode* pCurNode = 0;
	// Helper, prevents calling the size function over and over again.
	const int iVecSize = static_cast<int>(vecTraversal.size());

	//--------------------------------------------------------------------------
	// Loop to push down ranges and layout leaves.
	for(int i=0; i<iVecSize; ++i) // Top-down traversal!
	{
		// Retrieve next node to visit.
		pCurNode = vecTraversal[i];

		// If there are successors, we can not yet lay out the node but only
		// push down its range to the successors.
		if(pCurNode->GetNrSuccs() != 0)
		{
			map<int, SNodeInfo>::const_iterator itCurNode = 
				mapNodeInfo.find(pCurNode->GetId());
			assert(itCurNode != mapNodeInfo.end());
			size_t uiCurLeafSupp = itCurNode->second.m_uiLeafSupport;
				
			float fLower			= mapHorRanges[pCurNode->GetId()].m_fLower;
			float fUpper			= mapHorRanges[pCurNode->GetId()].m_fUpper;
			float fRangeWidth		= fUpper - fLower;
			float fCurChildLower	= fLower;

			for(int j=0; j<pCurNode->GetNrSuccs(); ++j)
			{
				// Retrieve next child node ...
				VfaStructureNode* pChildNode = pCurNode->GetSuccForIdx(j);
				// ... as well as its support.
				map<int, SNodeInfo>::const_iterator itChildNode = 
					mapNodeInfo.find(pChildNode->GetId());
				assert(itChildNode != mapNodeInfo.end());
				size_t uiChildLeafSupp = itChildNode->second.m_uiLeafSupport;
				
				// How much of the parent's range is the child node entitled to
				// receive? It is based on its leaf support!
				float fChildLeafSuppRatio = static_cast<float>(uiChildLeafSupp)
					/ static_cast<float>(uiCurLeafSupp);

				// Calculate the range of the current child.
				mapHorRanges[pChildNode->GetId()].m_fLower = fCurChildLower;
				fCurChildLower += fChildLeafSuppRatio * fRangeWidth;
				mapHorRanges[pChildNode->GetId()].m_fUpper = fCurChildLower;				
			}
		}
		// No successors? Then we can lay out a leaf node.
		else
		{
			map<int, SNodeInfo>::const_iterator itCurNode = 
				mapNodeInfo.find(pCurNode->GetId());
			assert(itCurNode != mapNodeInfo.end());
			size_t uiCurDepth = itCurNode->second.m_uiInverseDepth;

			float fLower			= mapHorRanges[pCurNode->GetId()].m_fLower;
			float fUpper			= mapHorRanges[pCurNode->GetId()].m_fUpper;

			m_mapNodes[pCurNode->GetId()] = VistaVector3D
				(
					0.5f * (fLower + fUpper),
					fVertStep * (0.5f + static_cast<float>(uiCurDepth - 1)),
					0.0f
				);
		}
	}

	//--------------------------------------------------------------------------
	// Loop to pull up leaf positions and layout non-leaves.
	// Traversal: bottom-up.
	for(int i=iVecSize-1; i>=0; --i) // Bottom-up traversl!
	{
		// Retrieve next node to visit.
		pCurNode = vecTraversal[i];

		// We only need to do something in case we found a non-leaf.
		if(pCurNode->GetNrSuccs() > 0)
		{
			// Set the position of the current node.
			map<int, SNodeInfo>::const_iterator itCurNode = 
				mapNodeInfo.find(pCurNode->GetId());
			assert(itCurNode != mapNodeInfo.end());
			size_t uiCurDepth = itCurNode->second.m_uiInverseDepth;

			
			VistaVector3D v3CurNodePos
				(
					0.0f,
					fVertStep * (0.5f + static_cast<float>(uiCurDepth - 1)),
					0.0f
				);
			
			// Add left-most node.
			VfaStructureNode* pChildNode = pCurNode->GetSuccForIdx(0);
			v3CurNodePos[0] += m_mapNodes[pChildNode->GetId()][0];
			// Add right-most node.
			pChildNode = pCurNode->GetSuccForIdx(pCurNode->GetNrSuccs()-1);
			v3CurNodePos[0] += m_mapNodes[pChildNode->GetId()][0];
			// Average x-pos and add pos to map.
			v3CurNodePos[0] /= 2.0f;
			m_mapNodes[pCurNode->GetId()] = v3CurNodePos;

			// Add an edge from the current node to each child.
			for(int j=0; j<pCurNode->GetNrSuccs(); ++j)
			{
				pChildNode = pCurNode->GetSuccForIdx(j);
				
				m_mapEdges[pChildNode->GetId()].m_v3Start = v3CurNodePos;
				m_mapEdges[pChildNode->GetId()].m_v3End =
					m_mapNodes[pChildNode->GetId()];
			}
		}
	}
}

/*============================================================================*/
/* NAME:		ChangeLayoutRefFrame                                          */
/*============================================================================*/
void VfaTreeDrawStrategy::ChangeLayoutRefFrame()
{
	// Reset transformed vertex containers.
	m_mapFinalEdges.clear();
	m_mapFinalNodes.clear();

	// Variables holding some size and position defining values.
	float	fWidth	= 0.0f,
			fHeight	= 0.0f;
	VistaQuaternion qOri;
	VistaVector3D v3Pos, v3Up, v3Right;

	// Retrieve defining values.
	GetDrawExtents(fWidth, fHeight);
	GetDrawOrientation(qOri);
	GetDrawCenter(v3Pos);

	v3Right	= qOri.Rotate(VistaVector3D(1.0f, 0.0f, 0.0f, 0.0f));
	v3Up	= qOri.Rotate(VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f));

	VistaTransformMatrix oScale;
	oScale.SetToIdentity();
	oScale.SetValue(0, 0, fWidth);
	oScale.SetValue(1, 1, fHeight);

	VistaTransformMatrix oRotation(qOri);
	VistaTransformMatrix oTranslation(v3Pos);
	VistaTransformMatrix oFullTrans = oTranslation * oRotation * oScale;

	map<int, IVfaStructuredDrawStrategy::SEdge>::const_iterator citEdge =
		m_mapEdges.begin();

	while(citEdge != m_mapEdges.end())
	{
		pair<int, IVfaStructuredDrawStrategy::SEdge> oNewEdge;
		
		// Set id.
		oNewEdge.first = citEdge->first;

		// Transform edge.
		oNewEdge.second.m_v3Start =
			oFullTrans.TransformPoint(citEdge->second.m_v3Start);
		oNewEdge.second.m_v3End =
			oFullTrans.TransformPoint(citEdge->second.m_v3End);

		// Add edge to transformed map.
		m_mapFinalEdges.insert(oNewEdge);

		++citEdge;
	}

	map<int, VistaVector3D>::const_iterator citNode = m_mapNodes.begin();

	while(citNode != m_mapNodes.end())
	{
		pair<int, VistaVector3D> oNewNode;
		
		// Set id.
		oNewNode.first = citNode->first;

		// Transform node.
		oNewNode.second = oFullTrans.TransformPoint(citNode->second);

		// Add node to transformed map.
		m_mapFinalNodes.insert(oNewNode);

		++citNode;
	}
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
