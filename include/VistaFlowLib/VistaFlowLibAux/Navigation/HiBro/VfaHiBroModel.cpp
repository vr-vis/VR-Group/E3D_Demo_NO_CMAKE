/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




/*============================================================================*/
/*  INCLUDES		                                                          */
/*============================================================================*/
#include "VfaHiBroModel.h"
#include "VfaStructureNode.h"

#include "VfaStructuredHierarchy.h"

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static const string STR_REF_TYPENAME("VfaHiBroModel");

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaHiBroModel::VfaHiBroModel()
:   m_pHierarchy(0),
	m_uiNrTilesWidth(5),
	m_fTileEdgeLength(1.0f),
	m_pSelectedNode(0),
	m_fScaleLastRowHeight(0.3f)
{
}

VfaHiBroModel::~VfaHiBroModel()
{
	delete m_pHierarchy;
}

/*============================================================================*/
/* NAME:    Get/SetHierarchy		                                          */
/*============================================================================*/
VfaStructuredHierarchy* VfaHiBroModel::GetHierarchy() const
{
	return m_pHierarchy;
}

void VfaHiBroModel::SetHierarchy(VfaStructuredHierarchy* pHierarchy)
{
	m_pHierarchy = pHierarchy;
}

/*============================================================================*/
/* NAME:   Set/GetNumberOfTilesWidth/Height                                   */
/*============================================================================*/
bool VfaHiBroModel::SetNumberOfTilesWidth(VistaType::uint32 uiNrTiles)
{
	if (uiNrTiles < 5 || uiNrTiles % 2 == 0)
		return false;

	m_uiNrTilesWidth = uiNrTiles;

	Notify(MSG_NUMBER_OF_TILES_CHANGED);

	return true;
}
VistaType::uint32 VfaHiBroModel::GetNumberOfTilesWidth() const
{
	return m_uiNrTilesWidth;
}
VistaType::uint32 VfaHiBroModel::GetNumberOfTilesHeight() const
{
	return 3;
}

/*============================================================================*/
/* NAME:    Set/GetTileEdgeLength                                             */
/*============================================================================*/
bool VfaHiBroModel::SetTileEdgeLength(float fEdgeLength)
{
	if (fEdgeLength <= 0.0f)
		return false;

	m_fTileEdgeLength = fEdgeLength;

	Notify(MSG_TILE_SIZE_CHANGED);

	return true;

}
float VfaHiBroModel::GetTileEdgeLength() const
{
	return m_fTileEdgeLength;
}
float VfaHiBroModel::GetCurrentTileEdgeLength() const
{
	return this->GetScale()*GetTileEdgeLength();
}
/*============================================================================*/
/* NAME:    Set/GetWidgetWidth/Height                                         */
/*============================================================================*/
bool VfaHiBroModel::SetWidgetWidth(float fWidth)
{
	if (fWidth <= 0.0f)
		return false;

	SetTileEdgeLength(fWidth / static_cast<float>(m_uiNrTilesWidth));
	
	return true;
}
float VfaHiBroModel::GetWidgetWidth() const
{
	return m_fTileEdgeLength * static_cast<float>(m_uiNrTilesWidth);
}
float VfaHiBroModel::GetWidgetHeight() const
{
	return m_fTileEdgeLength * 3.0f;
}
float VfaHiBroModel::GetCurrentWidgetWidth() const
{
	return this->GetScale()*GetWidgetWidth();
}
float VfaHiBroModel::GetCurrentWidgetHeight() const
{
	return this->GetScale()*GetWidgetHeight();
}
/*============================================================================*/
/* NAME:    Set/SetScaleFactorLastRowHeight                                   */
/*============================================================================*/
void VfaHiBroModel::SetScaleFactorLastRowHeight(float fScale)
{
	m_fScaleLastRowHeight = fScale;
	this->Notify(MSG_HEIGHT_LAST_ROW_CHANGED);
}
float VfaHiBroModel::GetScaleFactorLastRowHeight() const
{
	return m_fScaleLastRowHeight;
}

/*============================================================================*/
/* NAME:    Set/GetSelectedNode                                               */
/*============================================================================*/
void VfaHiBroModel::SetSelectedNode(VfaStructureNode *pNode)
{
	m_pSelectedNode = pNode;
}
VfaStructureNode* VfaHiBroModel::GetSelectedNode() const
{
	return m_pSelectedNode;
}

/*============================================================================*/
/* NAME:   GetReflectionableType                                              */
/*============================================================================*/
std::string VfaHiBroModel::GetReflectionableType() const
{
	return STR_REF_TYPENAME;
}

/*============================================================================*/
/* NAME:   AddToBaseTypeList	                                              */
/*============================================================================*/
int VfaHiBroModel::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(this->GetReflectionableType());
	return static_cast<int>(rBtList.size());
}
/*============================================================================*/
/*  END OF FILE																  */
/*============================================================================*/


