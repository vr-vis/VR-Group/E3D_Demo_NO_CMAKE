/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#include <GL/glew.h>

#include "VfaStructuredHierarchyView.h"
#include "VfaStructuredHierarchyModel.h"

#include "VfaStructuredDrawStrategy.h"

#include <map>
#include <vector>

using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION      VfaHiBroVis										  */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaStructuredHierarchyView::VfaStructuredHierarchyView(
	VfaStructuredHierarchyModel *pModel)
:	m_pModel(pModel)
{

}

VfaStructuredHierarchyView::~VfaStructuredHierarchyView()
{

}
/*============================================================================*/
/*  NAME:    DrawOpaque			                                              */
/*============================================================================*/

void VfaStructuredHierarchyView::DrawResetSubtree(const std::list<int> &liId)
{
	m_vecIdUnsel = liId;

	std::list<int>::iterator it = m_vecIdUnsel.begin();
	for (it; it != m_vecIdUnsel.end(); ++it)
	{
		m_vecIdSel.remove(*it);
		m_vecIdMarked.remove(*it);
	}
	
	
}
void VfaStructuredHierarchyView::DrawSelected(const std::list<int> &liId)
{
	m_vecIdSel = liId;

	std::list<int>::iterator it = m_vecIdSel.begin();
	for (it; it != m_vecIdSel.end(); ++it)
	{
		m_vecIdUnsel.remove(*it);
		m_vecIdMarked.remove(*it);
	}
	
}
void VfaStructuredHierarchyView::DrawMarkedSourrounding(const std::list<int> &liId)
{
	m_vecIdMarked = liId;

	std::list<int>::iterator it = m_vecIdMarked.begin();
	for (it; it != m_vecIdMarked.end(); ++it)
	{
		m_vecIdUnsel.remove(*it);
		m_vecIdSel.remove(*it);
	}
}

/*============================================================================*/
/*  NAME:    DrawOpaque			                                              */
/*============================================================================*/
void VfaStructuredHierarchyView::DrawOpaque()
{
	if(!m_pModel || !m_pModel->GetDrawStrategy() || !m_pModel->GetDrawStrategy()->GetVisible())
		return;

	map<int, VistaVector3D> mapNodes;
	map<int, IVfaStructuredDrawStrategy::SEdge> mapEdges;

	m_pModel->GetDrawStrategy()->GetNodeCenters(mapNodes);
	m_pModel->GetDrawStrategy()->GetEdges(mapEdges);

	glPushMatrix();

	glPushAttrib(GL_ENABLE_BIT | GL_POINT_BIT | GL_LINE_BIT | GL_CURRENT_BIT);

	glDisable(GL_LIGHTING);

	glLineWidth(2.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_LINES);
		map<int, IVfaStructuredDrawStrategy::SEdge>::iterator itEdges =
			mapEdges.begin();
		while(itEdges != mapEdges.end())
		{
			if(Find(m_vecIdMarked,itEdges->first))
			{
				float f[4];
				this->GetProperties()->GetMarkedColor(f);
				glColor3f(f[0], f[1], f[2]);
			}
			else if (Find(m_vecIdSel,itEdges->first))
			{
				float f[4];
				this->GetProperties()->GetSelectedColor(f);
				glColor3f(f[0], f[1], f[2]);
			}
			else //if(Find(m_vecIdUnsel,itEdges->first))
			{
				float f[4];
				this->GetProperties()->GetUnselectedColor(f);
				glColor3f(f[0], f[1], f[2]);
			}

			glVertex3fv(&itEdges->second.m_v3Start[0]);
			glVertex3fv(&itEdges->second.m_v3End[0]);
			++itEdges;
		}		
	glEnd();

/*
	glPointSize(8.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_POINTS);
		map<int, VistaVector3D>::iterator itNodes = mapNodes.begin();	
		while(itNodes != mapNodes.end())
		{
			glVertex3fv(&itNodes->second[0]);
			++itNodes;
		}
	glEnd();
*/

	glPopAttrib();

	glPopMatrix();
}

/*============================================================================*/
/*  NAME:     Find															  */
/*============================================================================*/
bool VfaStructuredHierarchyView::Find(std::list<int> &vec, int id)
{
	std::list<int>::iterator it = vec.begin();
	const size_t nSize = vec.size();
	for (size_t i = 0; i< nSize; ++i)
	{
		if (*it == id)
			return true;
		++it;
	}

	return false;
}

/*============================================================================*/
/*  NAME:     GetRegistrationMode                                             */
/*============================================================================*/
unsigned int VfaStructuredHierarchyView::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE; 
}

/*============================================================================*/
/*  NAME:    GetProperties		                                              */
/*============================================================================*/
VfaStructuredHierarchyView::VflStructHierarchyProps*
	VfaStructuredHierarchyView::GetProperties() const
{
	return static_cast<VfaStructuredHierarchyView::VflStructHierarchyProps *>
		(IVflRenderable::GetProperties());
}
/*============================================================================*/
/*  NAME:      CreateProperties	                                              */
/*============================================================================*/
IVflRenderable::VflRenderableProperties*
	VfaStructuredHierarchyView::CreateProperties() const
{
		return new VfaStructuredHierarchyView::VflStructHierarchyProps();
}

/*============================================================================*/
/*  NAME:   VflStructHierarchyProps                                          */
/*============================================================================*/
VfaStructuredHierarchyView::VflStructHierarchyProps::VflStructHierarchyProps()
: m_fLineWidth(1.0f)
{
	m_aUnselColorRgba[0] = 0.0f;
	m_aUnselColorRgba[1] = 0.0f;
	m_aUnselColorRgba[2] = 0.7f;
	m_aUnselColorRgba[3] = 1.0f;

	m_aSelColorRgba[0] = 0.0f;
	m_aSelColorRgba[1] = 0.7f;
	m_aSelColorRgba[2] = 0.0f;
	m_aSelColorRgba[3] = 1.0f;

	m_aMarkedColorRgba[0] = 0.7f;
	m_aMarkedColorRgba[1] = 0.0f;
	m_aMarkedColorRgba[2] = 0.0f;
	m_aMarkedColorRgba[3] = 1.0f;
}
VfaStructuredHierarchyView::VflStructHierarchyProps::~VflStructHierarchyProps()
{}

/*============================================================================*/
/*  NAME:   Set/GetLineWidth	                                              */
/*============================================================================*/
void VfaStructuredHierarchyView::VflStructHierarchyProps::SetLineWidth(
	float fWidth)
{
	m_fLineWidth = fWidth;
}
float VfaStructuredHierarchyView::VflStructHierarchyProps::GetLineWidth() const
{
	return m_fLineWidth;
}
		
/*============================================================================*/
/*  NAME:  Set/GetColor			                                              */
/*============================================================================*/
void VfaStructuredHierarchyView::VflStructHierarchyProps::SetUnselectedColor(
	float aColorRgba[4])
{
	m_aUnselColorRgba[0] = aColorRgba[0];
	m_aUnselColorRgba[1] = aColorRgba[1];
	m_aUnselColorRgba[2] = aColorRgba[2];
	m_aUnselColorRgba[3] = 1.0f;
}
void VfaStructuredHierarchyView::VflStructHierarchyProps::GetUnselectedColor(
	float aColorRgba[4]) const
{
	aColorRgba[0] = m_aUnselColorRgba[0];
	aColorRgba[1] = m_aUnselColorRgba[1];
	aColorRgba[2] = m_aUnselColorRgba[2];
	aColorRgba[3] = m_aUnselColorRgba[3];
}

void VfaStructuredHierarchyView::VflStructHierarchyProps::SetSelectedColor(
	float aColorRgba[4])
{
	m_aSelColorRgba[0] = aColorRgba[0];
	m_aSelColorRgba[1] = aColorRgba[1];
	m_aSelColorRgba[2] = aColorRgba[2];
	m_aSelColorRgba[3] = 1.0f;
}
void VfaStructuredHierarchyView::VflStructHierarchyProps::GetSelectedColor(
	float aColorRgba[4]) const
{
	aColorRgba[0] = m_aSelColorRgba[0];
	aColorRgba[1] = m_aSelColorRgba[1];
	aColorRgba[2] = m_aSelColorRgba[2];
	aColorRgba[3] = m_aSelColorRgba[3];
}

void VfaStructuredHierarchyView::VflStructHierarchyProps::SetMarkedColor(
	float aColorRgba[4])
{
	m_aMarkedColorRgba[0] = aColorRgba[0];
	m_aMarkedColorRgba[1] = aColorRgba[1];
	m_aMarkedColorRgba[2] = aColorRgba[2];
	m_aMarkedColorRgba[3] = 1.0f;
}
void VfaStructuredHierarchyView::VflStructHierarchyProps::GetMarkedColor(
	float aColorRgba[4]) const
{
	aColorRgba[0] = m_aMarkedColorRgba[0];
	aColorRgba[1] = m_aMarkedColorRgba[1];
	aColorRgba[2] = m_aMarkedColorRgba[2];
	aColorRgba[3] = m_aMarkedColorRgba[3];
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VfaStructuredHierarchyView.cpp"                              */
/*============================================================================*/




