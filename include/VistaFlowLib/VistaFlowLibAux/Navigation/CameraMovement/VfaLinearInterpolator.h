/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _IVFALINEARINTERPOLATOR_H
#define _IVFALINEARINTERPOLATOR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// ViSTA stuff
#include <VistaBase/VistaVectorMath.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include "VfaCameraInterpolator.h"
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaCameraInterpolator;
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Class to enable a linear camera movement between two points
 */
class VISTAFLOWLIBAUXAPI VfaLinearInterpolator : public IVfaCameraInterpolator
{
public:
	/**
	 * Constructor
	 */
	VfaLinearInterpolator(VflRenderNode *pRenderNode, 
								VistaVirtualPlatform *pPlatform);
	/**
	 * Destructor
	 */
	virtual ~VfaLinearInterpolator();

	/**
	 * Interpolating a linear movement
	 */
	virtual void Interpolate(float fParam, VistaVector3D &v3Pos,
										   VistaQuaternion &qOri);

	/**
	 * @overright
	 * User is only allowed to set two points 
	 */
	virtual bool AddStation(const VistaVector3D &v3Pos, 
										const VistaQuaternion &qOri);

	/**
	 *	Start computations for the animation AND
	 *  the animation itself
	 */
	virtual void Start();

	/**
	 * Define the velocity for one unit
	 */
	void SetVelocity(float fVel);
	/**
	 * Get the velocity for one unit
	 */
	float GetVelocity() const;

	/**
	 * Get the current platform orientation
	 */
	void GetPlatformOrientation(VistaQuaternion &qOri) const;
	/**
	 * Get the current platform position
	 */
	void GetPlatformPosition(VistaVector3D &v3Pos) const;

	/**
	 * @See IVfaCameraInterpolator
	 */
	virtual std::string GetClassName();

	/**
	 * Get to know the acr length between the specified stations
	 */
	virtual float GetArcLength();

	/**
	 * @See IVfaCameraInterpolator::Preparation
	 */
	virtual bool Preparation();

protected:
	
private:

	//########//
	// PARAMS //
	//########//

	VistaVector3D m_v3NewPlatformPos;
	VistaQuaternion m_qNewPlatformOri;
	VistaVector3D m_v3DiffVector;

	float m_fVelocity;

};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _IVFALINEARINTERPOLATOR_H */
