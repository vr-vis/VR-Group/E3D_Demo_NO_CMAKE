/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _IVFACIRCULARINTERPOLATOR_H
#define _IVFACIRCULARINTERPOLATOR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// ViSTA stuff
#include <VistaBase/VistaVectorMath.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include "VfaCameraInterpolator.h"
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaCameraInterpolator;
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Class to enable a circular camera movement between two point and around a
 * specified rotation centre
 * --> three points are used
 */
class VISTAFLOWLIBAUXAPI VfaCircularInterpolator : public IVfaCameraInterpolator
{
public:
	VfaCircularInterpolator(VflRenderNode *pRenderNode, 
								VistaVirtualPlatform *pPlatform);
	virtual ~VfaCircularInterpolator();

	/**
	 * Interpolating a circular movement
	 */
	virtual void Interpolate(float fParam, VistaVector3D &v3Pos,
										   VistaQuaternion &qOri);

	/**
	 * @overright
	 * User is only allowed to set two points 
	 */
	virtual bool AddStation(const VistaVector3D &v3Pos, 
										const VistaQuaternion &qOri);

	/**
	 *	Start computations for the animation AND
	 *  the animation itself
	 */
	virtual void Start();

	/**
	 * Set the center of the rotation
	 */
	void SetRotationCenter(const VistaVector3D &v3Pos);

	/**
	 * Get the center of the rotation
	 */
	void GetRotationCenter(VistaVector3D &v3Pos) const;

	/**
	 * Define the angular velocity
	 */
	void SetAngularVelocity(float fAngVel);
	/**
	 * Get the angular velocity
	 */
	float GetAngularVelocity() const;

	/**
	 * Get the current platform orientation
	 */
	void GetPlatformOrientation(VistaQuaternion &qOri) const;
	/**
	 * Get the current platform position
	 */
	void GetPlatformPosition(VistaVector3D &v3Pos) const;

	/**
	 * Get to know the acr length between the specified stations
	 */
	virtual float GetArcLength();

	/**
	 * @See IVfaCameraInterpolator
	 */
	virtual std::string GetClassName();

	/**
	* @See IVfaCameraInterpolator::Preparation
	 */
	virtual bool Preparation();


protected:

private:
	//##############//
	//   PARAMS     //
	//##############//

	VistaTransformMatrix m_mRenderNodeTransMatrix;
	VistaVector3D m_v3RotationCenter;

	VistaVector3D m_v3HorOffset;

	VistaVector3D m_v3NewPlatformPos;
	VistaQuaternion m_qNewPlatformOri;

	
	VistaVector3D m_v3DirStartVec;
	VistaVector3D m_v3DirEndVec;
	VistaVector3D m_v3Normal;

	float m_fAngleAtCenterInRad;
	float m_fAngularVelocity;
	float m_fRadius;

};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _IVFACIRCULARINTERPOLATOR_H */
