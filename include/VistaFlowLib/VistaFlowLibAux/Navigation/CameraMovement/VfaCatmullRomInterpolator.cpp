/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
#include "VfaCatmullRomInterpolator.h"
#include "STrailStation.h"

// ViSTA stuff
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

// C/C++ stuff
#include <limits>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <cassert>

using namespace std;

// ========================================================================== //
// GLOBAL FUNCTIONS
// ========================================================================== //
inline float intro_func(const float &fA, const float &fC, const float &fX)
{
	return fC * (Vista::Pi * fX - fA * sinf(Vista::Pi * fX / fA)) / (2.0f * Vista::Pi);
}

inline float const_func(const float &fA, const float &fC, const float &fX)
{
	return fC * fX;
}

inline float outro_func(const float &fB, const float &fC, const float &fX)
{
	float fRes = (fB - 1.0f) * sinf(Vista::Pi * (fX - 1.0f) / (fB - 1.0f)) / Vista::Pi;
	return 0.5f * fC * (-1.0f * fRes + fX - 1.0f);
}

inline float integrate_param(const float &fA, const float &fX)
{
	// On erroneous input value, simply return the supplied parameter.
	if(fA < 0.0f || fA > 0.5f || fX < 0.0f || fX > 1.0f)
		return fX;

	// Declare (and init) some necessary variables.
	float fResult	= 0.0f;
	const float fB	= 1.0f - fA;
	const float fC	= 1.0f / fB;

	// If we are in the first sub-interval ...
	if(fX >= 0.0f && fX < fA)
	{
		// ... integrate from 0 to fX.
		fResult += intro_func(fA, fC, fX) - intro_func(fA, fC, 0.0f);
	}
	// If we are in the second sub-interval ...
	else if(fX >= fA && fX <= fB)
	{
		// ... integrate over the full first sub-interval from 0 to fA ...
		fResult += intro_func(fA, fC, fA) - intro_func(fA, fC, 0.0f);
		// ... and then integrate the second sub-interval from fA to fX.
		fResult += const_func(fA, fC, fX) - const_func(fA, fC, fA);
	}
	// If we are in the third sub-interval ...
	else if(fX > fB && fX <= 1.0f)
	{
		// ... integrate over the full first sub-interval from 0 to fA, ...
		fResult += intro_func(fA, fC, fA) - intro_func(fA, fC, 0.0f);
		// ... then integrate the full second sub-interval from fA to fB, ...
		fResult += const_func(fA, fC, fB) - const_func(fA, fC, fA);
		// ... and finally integrate the third sub-interval from fB to fX.
		fResult += outro_func(fB, fC, fX) - outro_func(fB, fC, fB);
	}

	// Just in the (impossible case) we get here...
	return fResult;
}

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
VfaCatmullRomInterpolator::VfaCatmullRomInterpolator(VflRenderNode *pRenderNode, 
											 VistaVirtualPlatform *pPlatform)
 :	IVfaCameraInterpolator(pRenderNode, pPlatform),
	m_v3NewPlatformPos(0.0f, 0.0f, 0.0f, 1.0f),
	m_qNewPlatformOri(),
	m_fVelocity(0.9f),
	m_matU(1,4),
	m_matM(4,4),
	m_iInnerCount(100),
	m_fInterWaitDuration(3.0f),
	m_iLastStation(0),
	m_fAccelPhaseLength(0.1f)
{
	Init();
	m_pRenderNode->AddRenderable(this);

   	m_matM.SetVal( 0, 0, -1.0f ); m_matM.SetVal( 0, 1,  3.0f ); 
	m_matM.SetVal( 0, 2, -3.0f ); m_matM.SetVal( 0, 3,  1.0f ); 
 	m_matM.SetVal( 1, 0,  2.0f ); m_matM.SetVal( 1, 1, -5.0f ); 
	m_matM.SetVal( 1, 2,  4.0f ); m_matM.SetVal( 1, 3, -1.0f ); 
 	m_matM.SetVal( 2, 0, -1.0f ); m_matM.SetVal( 2, 1,  0.0f ); 
	m_matM.SetVal( 2, 2,  1.0f ); m_matM.SetVal( 2, 3,  0.0f ); 
 	m_matM.SetVal( 3, 0,  0.0f ); m_matM.SetVal( 3, 1,  2.0f ); 
	m_matM.SetVal( 3, 2,  0.0f ); m_matM.SetVal( 3, 3,  0.0f ); 

	m_matU.SetNull();
	m_vecLengthOfOneArc.clear();
	m_vecParam.clear();
	m_vecPtMatrices.clear();
}

VfaCatmullRomInterpolator::~VfaCatmullRomInterpolator(){;}

bool VfaCatmullRomInterpolator::AddStation(const VistaVector3D &v3Pos, 
						const VistaQuaternion &qOri)
{
	STrailStation newStation(v3Pos, qOri);
	//as long as we do not have a complete journey duplicate stations
	if(m_vecTrailStations.size() < 4)
	{
		m_vecTrailStations.push_back(newStation);
		m_vecTrailStations.push_back(newStation);
	}
	else
	{
		m_vecTrailStations[m_vecTrailStations.size()-1] = newStation;//remove duplicate
		m_vecTrailStations.push_back(newStation);//push back new duplicate
	}
	return true;
}

/*===========================================================================*/
/* IMPLEMENTATION                                                            */
/*===========================================================================*/
/* ==========================================================================*/
/* NAME: Start()					 									     */
/* ==========================================================================*/
void VfaCatmullRomInterpolator::Start()
{
	// do some calculations before starting the camera movement itself
	if(this->Preparation())
		IVfaCameraInterpolator::Start();
	else
		vstr::debugi() << "[VfaCatmullRomInterpolator] No Cam move has to be done." << endl;
}


/* ==========================================================================*/
/* NAME: Preparation()					 									 */
/* ==========================================================================*/
bool VfaCatmullRomInterpolator::Preparation()
{
	// @todo This conflicts the design of the interpolator combiner. Segment
	//		 interpolators won't have a render node if they are added to a
	//		 combiner, but as this function shows they might still need info
	//		 from the render node. Think about how to give them that info w/o
	//		 having to add them to a render node.
	if( GetRenderNode() )
	{
		m_v3UserOffset = GetRenderNode()->GetLocalViewPosition();
		m_v3UserOffset[3] = 1.0f;

		VistaTransformMatrix mTransBuf;

		// Calculate global view position.
		GetRenderNode()->GetTransform(mTransBuf);
		m_v3UserOffset = mTransBuf.Transform(m_v3UserOffset);

		// Transform viewer pos into platform coord frame.
		m_pPlatform->GetMatrixInverse(mTransBuf);
		m_v3UserOffset = mTransBuf.Transform(m_v3UserOffset);
	}

#ifdef _DEBUG
	vstr::debugi() << "[VfaCatmullRomInterpolator] User Offset to platform"
		<<" origin: "<<m_v3UserOffset<<". This (static) offset will be compensated for!"<<endl;
#endif

	// first of all clear matrices
	m_matU.SetNull();
	m_vecPtMatrices.clear();
	m_vecParam.clear();
	m_vecLengthOfOneArc.clear();
	m_fArcLength = 0.0f;

	// check for correct number of control points
    size_t iSize = m_vecTrailStations.size();
    if( iSize < 4 )
    {
        vstr::errp() << "[VfaCatmullRomInterpolator] you need at least 2 control points for"
			" CATMULL_ROM interpolation!" << std::endl;
        return false;
    }

	// startpos & ori;
	m_qNewPlatformOri = m_vecTrailStations[1].m_qOrientation;
	m_v3NewPlatformPos = m_vecTrailStations[1].m_v3Position;

	this->BuildCurvePtMatrix();
	this->ComputeParameterSteps();

	this->Notify(MSG_END_PREPARATION);
	return true;
}

/* ==========================================================================*/
/* NAME: GetArcLength()					 									 */
/* ==========================================================================*/
float VfaCatmullRomInterpolator::GetArcLength()
{
	BuildCurvePtMatrix();
	return m_fArcLength;
}

/* ==========================================================================*/
/* NAME: Interpolate()					 									 */
/* ==========================================================================*/
void VfaCatmullRomInterpolator::Interpolate(float fParam, VistaVector3D &v3Pos,
										   VistaQuaternion &qOri)
{
	// let's compute the platform pos and ori for parameter fParam
	
	// first of all find out which segment is done in the moment;
	int iSegment = this->GetIntervalForParameter(fParam);

	// re-parameterize parameter to range [0,1] for interval
	// [iSegment,iSegment+1]
	fParam = (fParam - m_vecParam[iSegment])/(m_vecParam[iSegment+1]-m_vecParam[iSegment]);
	fParam = integrate_param(m_fAccelPhaseLength, fParam);
	


	m_matU.SetVal( 0, 0, pow(fParam,3) );
	m_matU.SetVal( 0, 1, pow(fParam,2) );
	m_matU.SetVal( 0, 2, fParam );
	m_matU.SetVal( 0, 3, 1 );

	VistaMatrix<float> mat_point( 1, 3 );
	mat_point = (m_matU * m_matM) * m_vecPtMatrices[iSegment];
	
	VistaVector3D vec_point;
	vec_point[0] = mat_point.GetVal( 0, 0 ) * 0.5f;
	vec_point[1] = mat_point.GetVal( 0, 1 ) * 0.5f;
	vec_point[2] = mat_point.GetVal( 0, 2 ) * 0.5f;

	assert(vec_point[0] == vec_point[0]);
	assert(vec_point[1] == vec_point[1]);
	assert(vec_point[2] == vec_point[2]);

	m_v3NewPlatformPos = vec_point;
	// for the interpolation four points are used, but we only make a connection between points p1 and p2
	// that's why we need to transform our indeces here to get the correct orientations to splerp between
	m_qNewPlatformOri = m_vecTrailStations[iSegment+1].m_qOrientation.Slerp(m_vecTrailStations[iSegment+2].m_qOrientation, fParam);

	v3Pos = m_v3NewPlatformPos - GetUserOffset();
	qOri = m_qNewPlatformOri;
}

/* ==========================================================================*/
/* NAME: GetIntervalForParameter()					     */
/* ==========================================================================*/
int VfaCatmullRomInterpolator::GetIntervalForParameter(float fParam)
{
	if(fParam < 0.0f || fParam > 1.0f)
		return -1;

	const int nSize = static_cast<int>(m_vecParam.size());
	int iSegment = 1;
	for (; iSegment < nSize; ++iSegment)
	{
		if (m_vecParam[iSegment] >= fParam) //find end index
			break;
	}

	return --iSegment;
}


/* ==========================================================================*/
/* NAME: GetUserOffset()					     	     */
/* ==========================================================================*/
VistaVector3D VfaCatmullRomInterpolator::GetUserOffset()
{
	VistaTransformMatrix m;
	m_pPlatform->GetMatrix(m);
	return m.TransformVector(m_v3UserOffset);
}

/* ==========================================================================*/
/* NAME: BuildCurvePtMatrix				 									 */
/* ==========================================================================*/
bool VfaCatmullRomInterpolator::BuildCurvePtMatrix()
{
	std::vector<STrailStation>::iterator itEnd = m_vecTrailStations.end();
	itEnd--; itEnd--; itEnd--;

	for (std::vector<STrailStation>::iterator it = m_vecTrailStations.begin() ;
         it != itEnd; )
	{

		VistaVector3D p0, p1, p2, p3;
		
		std::vector<STrailStation>::iterator it_local(it);
		it++;
		p0 = (*it_local).m_v3Position;
		++it_local;
		p1 = (*it_local).m_v3Position;
		++it_local;
		p2 = (*it_local).m_v3Position;
		++it_local;
		p3 = (*it_local).m_v3Position;

		VistaMatrix<float> matB (4,3);
		matB.SetVal(0,0,p0[0]); matB.SetVal(0,1,p0[1]); 
			matB.SetVal(0,2,p0[2]);		
		matB.SetVal(1,0,p1[0]); matB.SetVal(1,1,p1[1]);
			matB.SetVal(1,2,p1[2]);
		matB.SetVal(2,0,p2[0]); matB.SetVal(2,1,p2[1]);
			matB.SetVal(2,2,p2[2]);
		matB.SetVal(3,0,p3[0]); matB.SetVal(3,1,p3[1]);
			matB.SetVal(3,2,p3[2]);

		m_vecPtMatrices.push_back(matB);

		float fArcLength = GetArcLengthBetween2Pt();
		m_vecLengthOfOneArc.push_back(fArcLength);
		m_fArcLength += fArcLength;
	}
	return true;
}

/* ==========================================================================*/
/* NAME: ComputeParameterSteps	 											 */
/* ==========================================================================*/
void VfaCatmullRomInterpolator::ComputeParameterSteps()
{
	// first of all compute time steps for next segment
	m_vecParam.push_back(0.0f);
	
	size_t iSize = m_vecLengthOfOneArc.size();
	for (size_t i = 0; i < iSize; ++i)
	{
		float fDelta = m_vecLengthOfOneArc[i]/m_fArcLength;
		if(i == iSize-1)
			m_vecParam.push_back(1.0f);
		else
			m_vecParam.push_back(fDelta + m_vecParam[i]);
	}
	
	// compute total length
	m_fDuration = m_fArcLength / m_fVelocity;
}

/* ==========================================================================*/
/* NAME: Set/GetVelocity()		 											 */
/* ==========================================================================*/
void VfaCatmullRomInterpolator::SetVelocity(float fVel)
{
	m_fVelocity = fVel;
}
float VfaCatmullRomInterpolator::GetVelocity() const
{
	return m_fVelocity;
}

/* ==========================================================================*/
/* NAME: Set/GetAccelPhaseLength()											 */
/* ==========================================================================*/
bool VfaCatmullRomInterpolator::SetAccelPhaseLength(float fLength)
{
	if(fLength < 0.0f || fLength > 0.5f)
		return false;

	m_fAccelPhaseLength = fLength;

	return true;
}
float VfaCatmullRomInterpolator::GetAccelPhaseLength() const
{
	return m_fAccelPhaseLength;
}

/* ==========================================================================*/
/* NAME: GetPlatformOrientation/Position									 */
/* ==========================================================================*/
void VfaCatmullRomInterpolator::GetPlatformOrientation(
												VistaQuaternion &qOri) const
{
	qOri = m_qNewPlatformOri;
}

void VfaCatmullRomInterpolator::GetPlatformPosition(VistaVector3D &v3Pos) const
{
	v3Pos = m_v3NewPlatformPos;
}


/* ==========================================================================*/
/* NAME: GetArcLengthBetween2Pt												 */
/* ==========================================================================*/
float VfaCatmullRomInterpolator::GetArcLengthBetween2Pt()
{
	float fArcLength = 0.0f;

	std::vector<VistaVector3D> vecCurvePoints;
	float du = 1.0f/(m_iInnerCount+1);
	
	int upper_index = m_iInnerCount;
	
    for( int i = 0 ; i < upper_index ; i++ )
    {
        VistaVector3D vec_point;
        float u = i*du;

        m_matU.SetVal( 0, 0, pow(u,3) );
        m_matU.SetVal( 0, 1, pow(u,2) );
        m_matU.SetVal( 0, 2, u );
        m_matU.SetVal( 0, 3, 1 );

		VistaMatrix<float> mat_point( 1, 3 );
		mat_point = (m_matU*m_matM)*m_vecPtMatrices[m_vecPtMatrices.size()-1];

        vec_point[0] = mat_point.GetVal( 0, 0 ) * 0.5f;
        vec_point[1] = mat_point.GetVal( 0, 1 ) * 0.5f;
        vec_point[2] = mat_point.GetVal( 0, 2 ) * 0.5f;
            
       
		if( i!=0 )
		{
			fArcLength+= (vecCurvePoints.at(i-1) - vec_point).GetLength();
		}
		 vecCurvePoints.push_back( vec_point );
      }        
	vecCurvePoints.clear();

#ifdef DEBUG
	vstr::debugi() << "[VfaCatmullRomInterpolator] current length: " << fArcLength << endl;
#endif

	return fArcLength;
}


/* ==========================================================================*/
/* NAME: m_iInnerCount														 */
/* ==========================================================================*/
void VfaCatmullRomInterpolator::SetInnerCount(int i)
{
	m_iInnerCount = i;
}
int VfaCatmullRomInterpolator::GetInnerCount() const
{
	return m_iInnerCount;
}

/* ==========================================================================*/
/* NAME: GetClassName				 										 */
/* ==========================================================================*/
std::string VfaCatmullRomInterpolator::GetClassName()
{
	return "VfaCatmullRomInterpolator";
}

// ========================================================================== //
// === End of File
// ========================================================================== //
