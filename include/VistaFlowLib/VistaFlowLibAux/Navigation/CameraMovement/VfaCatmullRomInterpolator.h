/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _IVFACATMULLROMINTERPOLATOR_H
#define _IVFACATMULLROMINTERPOLATOR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// ViSTA stuff
#include <VistaMath/VistaMatrix.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include "VfaCameraInterpolator.h"
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaCameraInterpolator;
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Class to enable a camera movement; path computed as CatmullRom spline
 * You have to insert at least four points [p0, p1, p2, p3],
 * the spline is then computed between p1 and p2.
 */
class VISTAFLOWLIBAUXAPI VfaCatmullRomInterpolator : public IVfaCameraInterpolator
{
public:
	/**
	 * Constructor
	 */
	VfaCatmullRomInterpolator(VflRenderNode *pRenderNode, 
								VistaVirtualPlatform *pPlatform);
	/**
	 * Destructor
	 */
	virtual ~VfaCatmullRomInterpolator();

	/**
	 * Interpolating a CatmullRom movement
	 */
	virtual void Interpolate(float fParam, VistaVector3D &v3Pos,
										   VistaQuaternion &qOri);


	virtual int GetIntervalForParameter(float fParam);
	virtual VistaVector3D GetUserOffset(); 

	/**
	 *	Start computations for the animation AND
	 *  the animation itself
	 */
	virtual void Start();


	/**
	 * Define the velocity
	 */
	void SetVelocity(float fVel);
	/**
	 * Get the velocity
	 */
	float GetVelocity() const;

	/**
	 * Defines the (relative) length of the acceleration and deceleration phase.
	 * Hence, the supplied value must be within the range [0, 0.5].
	 */
	bool SetAccelPhaseLength(float fLength);
	/**
	 * Returns the value currently set for accel/decel phase length.
	 */
	float GetAccelPhaseLength() const;

	/**
	 * Get the current platform orientation
	 */
	void GetPlatformOrientation(VistaQuaternion &qOri) const;
	/**
	 * Get the current platform position
	 */
	void GetPlatformPosition(VistaVector3D &v3Pos) const;


	/**
	 * Define number of curce points in one section
	 * used to approximate length of the segment
	 * default: 10.000
	 */
	void SetInnerCount(int i);

	/**
	 * Get number of curve points in a segment
	 */
	int GetInnerCount() const;

	/**
	 * Get to know the acr length between the specified stations
	 */
	virtual float GetArcLength();

	/**
	 * @See IVfaCameraInterpolator
	 */
	virtual std::string GetClassName();

	/**
	* @See VfaCameraInterpolator::Preparation
	 */
	virtual bool Preparation();

	virtual bool AddStation(const VistaVector3D &v3Pos, const VistaQuaternion &qOri);

protected:

	/**
	 * choose four point to compute the spline
	 */
	virtual bool BuildCurvePtMatrix();

	/**
	 * get the approximated length of the current segment
	 */
	virtual float GetArcLengthBetween2Pt();

	/**
	 * compute duration and timedeltas of one segment corresponding to the 
	 * whole spline
	 */
	virtual void ComputeParameterSteps();

private:

	//########//
	// PARAMS //
	//########//

	VistaVector3D m_v3NewPlatformPos;
	VistaQuaternion m_qNewPlatformOri;

	float m_fVelocity;

	/** potences of the current parameter (f^3, f^2, f, 1) */
	VistaMatrix<float> m_matU;

	/** defined in mathematics :) */
	VistaMatrix<float> m_matM;
	
	
	/**
	 * save matrices containing four points
	 */
	std::vector<VistaMatrix<float> > m_vecPtMatrices;

	std::vector<float> m_vecParam;
	std::vector<float> m_vecLengthOfOneArc;
	

	/** number of curve points in a segment*/
	int m_iInnerCount;

	/**
	 * The offset from the user's head to the platform origin at the BEGINNING
	 * of the camera movement. The offset will be calculated once when starting
	 * the camera movement and then subtracted from the calculated platform
	 * position every interpolation step. The idea is to bring the user's head
	 * position to the first interpolation position but afterwards letting the
	 * user move around freely without compensating for the changing offset as
	 * this would feel un-natural/akward in immersive VEs.
	 */
	VistaVector3D m_v3UserOffset;

	float			m_fInterWaitDuration;
	int				m_iLastStation;
	float			m_fAccelPhaseLength;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _IVFACATMULLROMINTERPOLATOR_H */
