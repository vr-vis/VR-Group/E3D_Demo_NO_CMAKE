/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
// ViSTA stuff
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

// FlowLibAux stuff
#include "VfaCircularInterpolator.h"
#include "STrailStation.h"

// C/C++ stuff
#include <limits>

using namespace std;

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
VfaCircularInterpolator::VfaCircularInterpolator(VflRenderNode *pRenderNode, 
											 VistaVirtualPlatform *pPlatform)
 :	IVfaCameraInterpolator(pRenderNode, pPlatform),
	m_v3RotationCenter(0.0f, 0.0f, 0.0f, 1.0f),
	m_v3HorOffset(0.0f, 0.0f, 0.0f, 1.0f),
	m_v3NewPlatformPos(0.0f, 0.0f, 0.0f, 1.0f),
	m_qNewPlatformOri(),
	m_v3DirStartVec(0.0f, 0.0f, 0.0f, 1.0f),
	m_v3DirEndVec(0.0f, 0.0f, 0.0f, 1.0f),
	m_v3Normal(0.0f, 0.0f, 0.0f, 1.0f),
	m_fAngleAtCenterInRad(0.0f),
	m_fAngularVelocity(0.0f)
{
	Init();
	m_pRenderNode->AddRenderable(this);
	m_pRenderNode->GetTransform(m_mRenderNodeTransMatrix);
}

VfaCircularInterpolator::~VfaCircularInterpolator(){}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/* ==========================================================================*/
/* NAME: Start()					 									     */
/* ==========================================================================*/
void VfaCircularInterpolator::Start()
{
	// do some calculations before starting the cmaera movement itself
	if(this->Preparation())
		IVfaCameraInterpolator::Start();
	else
		vstr::debugi() << "[VfaCircularInterpolator] No Cam move has to be done." << endl;
}

/* ==========================================================================*/
/* NAME: Preparation()					 									 */
/* ==========================================================================*/
bool VfaCircularInterpolator::Preparation()
{
	// maybe you have an offset here,
	// (e.g. if you store the user's head as platform position)
	// so assure to compute the offset between head and platform
	// if you took the platform pos, this offset will be zero and won't
	// change the later computations
	// so this computation just lower risks for false results
	VistaVector3D v3TempTrans = m_pPlatform->GetTranslation();

	m_v3HorOffset = v3TempTrans - m_vecTrailStations[0].m_v3Position;
	m_v3HorOffset[3] = 0.0f;

	VistaTransformMatrix mStartTransfrorm(m_vecTrailStations[0].m_qOrientation);
	mStartTransfrorm.SetTranslation(v3TempTrans);
	mStartTransfrorm = mStartTransfrorm.GetInverted();

	// offset in camera space
	m_v3HorOffset = mStartTransfrorm.Transform(m_v3HorOffset);
	m_v3HorOffset[3] = 0.0f;

	// store the startposition and orientation of the camera
	m_v3NewPlatformPos = v3TempTrans;
	m_qNewPlatformOri = m_vecTrailStations[0].m_qOrientation;

	// pretent starting a camMove when are already positioned and oriented
	// in the right way;
	// get dot product first (take normalized vectors, IMPORTANT)
	float fDotProduct = m_vecTrailStations[0].m_v3Position.GetNormalized().Dot
		(m_vecTrailStations[1].m_v3Position.GetNormalized());
	
	// now test if dot product is 1 --> means vectors are parallel
	// because <a,b> = |a|*|b| if a == b
	//               =  1 * 1 = 1 (length of a normalized vector is 1)
	if( fDotProduct &&
		m_vecTrailStations[0].m_qOrientation ==
			m_vecTrailStations[1].m_qOrientation)
		return false;

	// compute startvector
	m_v3DirStartVec = m_vecTrailStations[0].m_v3Position - 
		m_mRenderNodeTransMatrix.Transform(m_v3RotationCenter);

	m_fRadius = m_v3DirStartVec.GetLength();

	m_v3DirStartVec.Normalize();
	m_v3DirStartVec[3] = 0.0f;


	// compute endvector
	m_v3DirEndVec = m_vecTrailStations[1].m_v3Position - 
		m_mRenderNodeTransMatrix.Transform(m_v3RotationCenter);
	m_v3DirEndVec.Normalize();
	m_v3DirEndVec[3] = 0.0f;


	// compute angle between both direction vectors
	float fAngle = m_v3DirStartVec.Dot(m_v3DirEndVec);
	m_fAngleAtCenterInRad = acosf(fAngle);

	// compute duration of the animation
	m_fDuration = m_fAngleAtCenterInRad * m_fAngularVelocity;
	if(m_fDuration < 0)
		m_fDuration*=(-1);

	// compute normal of the plane, the circle lies in
	if(abs(m_v3DirStartVec.Dot(m_v3DirEndVec)) <
								1.0f - numeric_limits<float>::epsilon())
		m_v3Normal = m_v3DirStartVec.Cross(m_v3DirEndVec);
	else if(abs(m_v3DirStartVec.Dot(VistaVector3D(1.0f, 0.0f, 0.0f, 0.0f))) > 
								1.0f - numeric_limits<float>::epsilon())		
		m_v3Normal = VistaVector3D(0.0f, 0.0f, 1.0f, 0.0f);
	else if(abs(m_v3DirStartVec.Dot(VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f))) > 
								1.0f - numeric_limits<float>::epsilon())		
		m_v3Normal = VistaVector3D(0.0f, 0.0f, 1.0f, 0.0f);
	else if(abs(m_v3DirStartVec.Dot(VistaVector3D(0.0f, 0.0f, 1.0f, 0.0f))) > 
								1.0f - numeric_limits<float>::epsilon())		
		m_v3Normal = VistaVector3D(0.0f, 1.0f, 0.0f, 0.0f);

	m_v3Normal.Normalize();
	m_v3Normal[3] = 0.0f;

	this->Notify(MSG_END_PREPARATION);
	return true;
}

/* ==========================================================================*/
/* NAME: GetArcLength()					 									 */
/* ==========================================================================*/
float VfaCircularInterpolator::GetArcLength()
{
	Preparation();
	m_fArcLength =  m_fRadius*m_fAngleAtCenterInRad;
	return m_fArcLength;
}

/* ==========================================================================*/
/* NAME: Interpolate()					 									 */
/* ==========================================================================*/
void VfaCircularInterpolator::Interpolate(float fParam, VistaVector3D &v3Pos,
										   VistaQuaternion &qOri)
{
#ifdef DEBUG
	vstr::debugi() <<"[VfaCircularInterpolator] fParam: "<< fParam << endl;
#endif

	// let's compute the platform pos and ori for parameter fParam

	VistaVector3D v3Trans;
	VistaQuaternion qRotOri = VistaQuaternion(VistaAxisAndAngle(m_v3Normal, 
								fParam*m_fAngleAtCenterInRad));
	qRotOri.Normalize(); 


	// Introduced an if clause to check whether the quaternion qOri is
	// valid or not. It might be invalid due to m_v3Normal being a
	// zero-vector. This on the other hand can occur if m_v3DirStartVec
	// and m_v3DirEndVec are (anti-)parallel
	if(m_v3Normal.GetLength() > numeric_limits<float>::epsilon())
	{
		v3Trans = m_mRenderNodeTransMatrix.Transform(m_v3RotationCenter)
							+ m_fRadius*qRotOri.Rotate(m_v3DirStartVec);
	}
	else
	{
		v3Trans = m_mRenderNodeTransMatrix.Transform(m_v3RotationCenter)
							+ m_fRadius*m_v3DirStartVec;
		vstr::debugi() << "[VfaCircularInterpolator] smaller numeric_limits<float>::epsilon()" << endl;
	}


	// compute quaternion describing the rotation
	VistaQuaternion qSlerpOri = m_vecTrailStations[0].m_qOrientation.Slerp(
							m_vecTrailStations[1].m_qOrientation, fParam);


	// compute coordinate system
	VistaVector3D v3Dir = v3Trans - m_mRenderNodeTransMatrix.Transform(m_v3RotationCenter);
	v3Dir.Normalize();
	v3Dir[3] = 0.0f;

	VistaVector3D v3Up (0.0f, 1.0f, 0.0f, 0.0f);
	v3Up = qSlerpOri.Rotate(v3Up);
	v3Up.Normalize();
	v3Up[3] = 0.0f;
	
	VistaVector3D v3Right = v3Up.Cross(v3Dir);
	v3Right.Normalize();
	v3Right[3] = 0.0f;

	// now get the orientation
	float fMat[]={	
			v3Right[0], v3Up[0], v3Dir[0], 0.0f,
			v3Right[1], v3Up[1], v3Dir[1], 0.0f,
			v3Right[2], v3Up[2], v3Dir[2], 0.0f,
				  0.0f,	   0.0f,	 0.0f, 1.0f};

	VistaTransformMatrix mTransMat(fMat);
	m_qNewPlatformOri = VistaQuaternion(mTransMat);
	m_qNewPlatformOri.Normalize();


	// last, but not least, get the new platform pos
	VistaTransformMatrix mTrans(m_qNewPlatformOri);
	mTrans.SetTranslation(v3Trans);
	m_v3NewPlatformPos = v3Trans + mTrans.Transform(m_v3HorOffset);	

	v3Pos = m_v3NewPlatformPos;
	qOri = m_qNewPlatformOri;
}

/* ==========================================================================*/
/* NAME: Set/GetRotationCenter()		 									 */
/* ==========================================================================*/
void VfaCircularInterpolator::SetRotationCenter(const VistaVector3D &v3Pos)
{
	m_v3RotationCenter = v3Pos;
}
void VfaCircularInterpolator::GetRotationCenter(VistaVector3D &v3Pos) const
{
	v3Pos = m_v3RotationCenter;
}

/* ==========================================================================*/
/* NAME: Set/GetAngularVelocity()		 									 */
/* ==========================================================================*/
void VfaCircularInterpolator::SetAngularVelocity(float fAngVel)
{
	m_fAngularVelocity = fAngVel;
}
float VfaCircularInterpolator::GetAngularVelocity() const
{
	return m_fAngularVelocity;
}

/* ==========================================================================*/
/* NAME: GetPlatformOrientation/Position									 */
/* ==========================================================================*/
void VfaCircularInterpolator::GetPlatformOrientation(
	VistaQuaternion &qOri) const
{
	qOri = m_qNewPlatformOri;
}

void VfaCircularInterpolator::GetPlatformPosition(VistaVector3D &v3Pos) const
{
	v3Pos = m_v3NewPlatformPos;
}

/* ==========================================================================*/
/* NAME: AddStation															 */
/* ==========================================================================*/
bool VfaCircularInterpolator::AddStation(const VistaVector3D &v3Pos, 
										  const VistaQuaternion &qOri)
{
	if(m_vecTrailStations.size() < 2)
		return IVfaCameraInterpolator::AddStation(v3Pos, qOri);

	vstr::warnp() << "[VfaCircularInterpolator::AddStation]"
				  << " Two stations are already given, more can't be used! " << endl;
	return false;
}

/* ==========================================================================*/
/* NAME: GetClassName				 										 */
/* ==========================================================================*/
std::string VfaCircularInterpolator::GetClassName()
{
	return "VfaCircularInterpolator";
}

// ========================================================================== //
// === End of File
// ========================================================================== //
