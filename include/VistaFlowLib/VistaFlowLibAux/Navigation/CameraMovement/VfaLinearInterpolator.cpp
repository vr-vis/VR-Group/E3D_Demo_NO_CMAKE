/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
#include "VfaLinearInterpolator.h"
#include "STrailStation.h"

// ViSTA stuff
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

// C/C++ stuff
#include <limits>

using namespace std;

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
VfaLinearInterpolator::VfaLinearInterpolator(VflRenderNode *pRenderNode, 
											 VistaVirtualPlatform *pPlatform)
 :	IVfaCameraInterpolator(pRenderNode, pPlatform),
	m_v3NewPlatformPos(0.0f, 0.0f, 0.0f, 1.0f),
	m_qNewPlatformOri(),
	m_fVelocity(0.0f)
{
	Init();
	m_pRenderNode->AddRenderable(this);
}

VfaLinearInterpolator::~VfaLinearInterpolator(){}

/*===========================================================================*/
/* IMPLEMENTATION                                                            */
/*===========================================================================*/
/* ==========================================================================*/
/* NAME: Start()					 									     */
/* ==========================================================================*/
void VfaLinearInterpolator::Start()
{
	// do some calculations before starting the cmaera movement itself
	if(this->Preparation())
		IVfaCameraInterpolator::Start();
	else
		vstr::debugi() << "[VfaLinearInterpolator] No Cam move has to be done." << endl;
}

/* ==========================================================================*/
/* NAME: Preparation()					 									 */
/* ==========================================================================*/
bool VfaLinearInterpolator::Preparation()
{
	m_qNewPlatformOri = m_vecTrailStations[0].m_qOrientation;
	m_v3NewPlatformPos = m_vecTrailStations[0].m_v3Position;

	// duration of the movement
	m_fDuration = this->GetArcLength() / m_fVelocity;
	if (m_fDuration < 0)
		m_fDuration*=(-1.0f);

	this->Notify(MSG_END_PREPARATION);
	return true;
}

/* ==========================================================================*/
/* NAME: GetArcLength					 									 */
/* ==========================================================================*/
float VfaLinearInterpolator::GetArcLength()
{
	// vector between start and end, this one will be interpolated
	m_v3DiffVector = m_vecTrailStations[1].m_v3Position - m_vecTrailStations[0].m_v3Position;
	m_fArcLength   = m_v3DiffVector.GetLength();
	return m_fArcLength;
}

/* ==========================================================================*/
/* NAME: Interpolate()					 									 */
/* ==========================================================================*/
void VfaLinearInterpolator::Interpolate(float fParam, VistaVector3D &v3Pos,
										   VistaQuaternion &qOri)
{
#ifdef DEBUG
	vstr::debugi() <<"[VfaLinearInterpolator] fParam: "<< fParam << endl;
#endif

	// let's compute the platform pos and ori for parameter fParam

	// compute quaternion describing the new orientation
	m_qNewPlatformOri = m_vecTrailStations[0].m_qOrientation.Slerp(
							m_vecTrailStations[1].m_qOrientation, fParam);
	// compute new position depending on the parameter
	m_v3NewPlatformPos = m_vecTrailStations[0].m_v3Position + fParam*m_v3DiffVector;

	v3Pos = m_v3NewPlatformPos;
	qOri = m_qNewPlatformOri;
}

/* ==========================================================================*/
/* NAME: Set/GetVelocity()		 											 */
/* ==========================================================================*/
void VfaLinearInterpolator::SetVelocity(float fVel)
{
	m_fVelocity = fVel;
}
float VfaLinearInterpolator::GetVelocity() const
{
	return m_fVelocity;
}

/* ==========================================================================*/
/* NAME: GetPlatformOrientation/Position									 */
/* ==========================================================================*/
void VfaLinearInterpolator::GetPlatformOrientation(
	VistaQuaternion &qOri) const
{
	qOri = m_qNewPlatformOri;
}

void VfaLinearInterpolator::GetPlatformPosition(VistaVector3D &v3Pos) const
{
	v3Pos = m_v3NewPlatformPos;
}

/* ==========================================================================*/
/* NAME: AddStation															 */
/* ==========================================================================*/
bool VfaLinearInterpolator::AddStation(const VistaVector3D &v3Pos, 
										  const VistaQuaternion &qOri)
{
	if(m_vecTrailStations.size() < 2)
		return IVfaCameraInterpolator::AddStation(v3Pos, qOri);

	vstr::warnp() << "[VfaLinearInterpolator::AddStation]" 
				  << "Two stations are already given, more can't be used! " << endl;
	return false;
}

/* ==========================================================================*/
/* NAME: GetClassName				 										 */
/* ==========================================================================*/
std::string VfaLinearInterpolator::GetClassName()
{
	return "VfaLinearInterpolator";
}

// ========================================================================== //
// === End of File
// ========================================================================== //
