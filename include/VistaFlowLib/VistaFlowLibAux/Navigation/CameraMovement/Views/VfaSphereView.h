/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFASPHEREVIEW_H
#define _VFASPHEREVIEW_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaFlowLibAux/Navigation/CameraMovement/Views/VfaInterpolatorView.h>

// C/C++ stuff
#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VfaSphereVis;

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * inheritance of InterpolatorView
 * this class enables the user to show all computation steps of the
 * camera movement itself (in form of spheres)
 */
class VISTAFLOWLIBAUXAPI VfaSphereView : public IVfaInterpolatorView
{
public:
	/**
	 * constructor
	 * define the used interpolator, the renderNode to add spheres
	 * and a prototyp of a spheres, which will be duplicated during session
	 */
	VfaSphereView( IVfaCameraInterpolator *pCamInterpolator, 
					VflRenderNode *pRenderNode,
					VfaSphereVis *pPrototypSphere);

	/**
	 * destructor
	 */
	virtual ~VfaSphereView();

	/**
	 * \see{VfrlRenderable.h}
	 */
	void ObserverUpdate(IVistaObserveable *pObserveable, 
													int msg, int ticket);

	/**
	 * Define: just draw a sphere for every nth notification
	 * default: zero --> for all notifications
	 */
	bool SetSample(int iNth);

	/**
	 * get state: which sphere rate will be drawn
	 * 0 --> one sphere for every notification
	 * n --> one sphere for every nth notification
	 */
	int GetSample() const;

	/**
	 * Set index of color-array, which is multiplied by the current parameter
	 * by doing this, you can also code the progress by color
	 */
	bool SetIndex(int iInd);
	/**
	 * get index of color array which will be manipulated
	 */
	int GetIndex() const;

	/**
	 * define, that all spheres look exactly like the prototyp
	 * (all have exactly the same color)
	 */
	bool SetUniqueColor(bool bUnique);
	/**
	 * Get to know whether all spheres are colored same
	 */
	bool GetUniqueColor() const;

	/**
	 * @see VfaInterpolatorView::TidyUp
	 */
	virtual void TidyUp();


	
protected:
	/**
	 * internal method to add a new sphere
	 */
	void AddSphere();

private:

	//########//
	// PARAMS //
	//########//

	VflRenderNode *m_pRenderNode;
	int m_iSample;
	int m_iIndex;
	int m_iNrOfNotifications;
	bool m_bUnique;
	float m_fCurveSphereRadius;
	VfaSphereVis *m_pPrototypSphere;
	std::vector<VfaSphereVis*> m_vecSphereVis;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
