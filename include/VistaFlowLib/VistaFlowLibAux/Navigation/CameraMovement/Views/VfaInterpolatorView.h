/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _IVFAINTERPOLATORVIEW_H
#define _IVFAINTERPOLATORVIEW_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLib/Data/VflObserver.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaCameraInterpolator;

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Interface for all views, used to show different aspects of the 
 * camera interpolation
 */
class VISTAFLOWLIBAUXAPI IVfaInterpolatorView : public IVflRenderable
{
public:
	virtual ~IVfaInterpolatorView();

	/**
	 * set a new interpoltor
	 */
	virtual void SetInterpolator(IVfaCameraInterpolator *pCamInterpolator);
	
	/**
	 *	get current interpolator
	 */
	virtual IVfaCameraInterpolator* GetInterpolator() const;

	/**
	 * this should be do in the inheritance classes,
	 * because we have to react on different msg from the interpolator
	 */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, 
													int msg, int ticket) = 0;

	/**
	 * @see VflRenderable.h for explanations
	 */
	virtual unsigned int GetRegistrationMode() const;

	/**
	 * clear vector with views
	 */
	virtual void TidyUp() = 0;
	

protected:
	/**
	 * protected constructor, because this is an interface
	 */
	explicit IVfaInterpolatorView(IVfaCameraInterpolator *pCamInterpolator);

	/**
	 * camera interpolator from which we get all data
	 */
	IVfaCameraInterpolator *m_pCamInterpolator;

	/** 
	 * IVflRenderable Interface 
	 */
	virtual VflRenderableProperties* CreateProperties() const;

private:
	
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _IVFAINTERPOLATORVIEW_H */
