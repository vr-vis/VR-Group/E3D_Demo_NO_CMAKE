/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// ========================================================================== //
// INCLUDES
// ========================================================================== //

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/Navigation/CameraMovement/VfaCameraInterpolator.h>
#include <VistaFlowLibAux/Navigation/CameraMovement/Views/VfaSphereView.h>
#include <VistaFlowLibAux/Navigation/CameraMovement/VfaCatmullRomInterpolator.h>
#include <VistaFlowLibAux/Widgets/Sphere/VfaSphereModel.h>
#include <VistaFlowLibAux/Widgets/VfaSphereVis.h>

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
VfaSphereView::VfaSphereView (IVfaCameraInterpolator *pCamInterpolator,
							    VflRenderNode *pRenderNode,
								VfaSphereVis *pPrototypSphere)
	:	IVfaInterpolatorView(pCamInterpolator),
		m_pRenderNode(pRenderNode),
		m_pPrototypSphere(pPrototypSphere),
		m_iSample(0),
		m_fCurveSphereRadius(0.0f),
		m_iIndex(0),
		m_bUnique(true),
		m_iNrOfNotifications(0)
{}

VfaSphereView::~VfaSphereView()
{
	this->TidyUp();
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/* ==========================================================================*/
/* NAME: Set/GetSample					 									 */
/* ==========================================================================*/
bool VfaSphereView::SetSample(int iNth)
{
	if (m_iSample == iNth)
		return false;

	m_iSample = iNth;
	return true;
}
int VfaSphereView::GetSample() const
{
	return m_iSample;
}

/* ==========================================================================*/
/* NAME: ObserverUpdate					 									 */
/* ==========================================================================*/
void VfaSphereView::ObserverUpdate(IVistaObserveable *pObserveable, 
															int msg, int ticket)
{
	if(msg == IVfaCameraInterpolator::MSG_CAM_MOVE_NEWSTEP ||
		msg == IVfaCameraInterpolator::MSG_CAM_MOVE_ENDED)
	{
		// just draw a new sphere if it fits to the sampling rate
		++m_iNrOfNotifications;
		if (m_iSample == 0 || m_iNrOfNotifications % m_iSample == 0)
			this->AddSphere();
	}

	//if(msg == IVfaCameraInterpolator::MSG_END_PREPARATION)
	//{
	//	VfaCatmullRomInterpolator *pCatmullRom = dynamic_cast<VfaCatmullRomInterpolator*>(m_pCamInterpolator);
	//	int iSize = pCatmullRom->GetNumberCurvePt();
	//	vstr::debugi() << "[VfaSphereView] Number CurvePt: " << iSize << std::endl;
	//	for (int i = 0; i < iSize; ++i)
	//	{
	//		this->AddSphere();
	//		++m_iNrOfNotifications;
	//	}
	//}

}

/* ==========================================================================*/
/* NAME: Set/GetUniqueColor						 							 */
/* ==========================================================================*/
bool VfaSphereView::SetUniqueColor(bool bUnique)
{
	if (m_bUnique == bUnique)
		return false;

	m_bUnique = bUnique;
	return true;
	
}
bool VfaSphereView::GetUniqueColor() const
{
	return m_bUnique;
}
/* ==========================================================================*/
/* NAME: Set/GetIndex						 								 */
/* ==========================================================================*/
bool VfaSphereView::SetIndex(int iInd)
{
	if (iInd < 0 || iInd > 2)
		return false;

	if(m_iIndex == iInd)
		return false;

	m_iIndex = iInd;
	// now we have an index, this means the color is not unique :)
	m_bUnique = false;

	return true;
}
int VfaSphereView::GetIndex() const
{
	return m_iIndex;
}

/* ==========================================================================*/
/* NAME: AddSphere						 									 */
/* ==========================================================================*/
void VfaSphereView::AddSphere()
{
	// first of all get the current param
	float fParam = m_pCamInterpolator->GetParameter();

#ifdef DEBUG
	vstr::debugi() << "[VfaSphereView] Sphere for Update: " << fParam << std::endl;
#endif

	// now interpolate to get all available information
	// about pos and ori
	VistaVector3D v3Pos;
	VistaQuaternion qOri;
	//m_pCamInterpolator->Interpolate(m_iNrOfNotifications, v3Pos, qOri);
	m_pCamInterpolator->Interpolate(fParam, v3Pos, qOri);
	
	// this one will ne used to compute the center in vis space
	VistaTransformMatrix matRenderNode;
	m_pRenderNode->GetTransform(matRenderNode);
	
	
	// duplicate prototyp sphere and set oit to the current pos
	VfaSphereVis *pSphereVis = new VfaSphereVis(new VfaSphereModel);
	pSphereVis->GetModel()->SetRadius(m_pPrototypSphere->GetModel()->GetRadius());
	pSphereVis->GetModel()->SetCenter(matRenderNode.GetInverted().Transform(v3Pos));


	if(!pSphereVis->Init())
		return;

	m_pRenderNode->AddRenderable(pSphereVis);
	pSphereVis->GetProperties()->SetToSolid(m_pPrototypSphere->GetProperties()->GetIsSolid());
	pSphereVis->GetProperties()->SetUseLighting(m_pPrototypSphere->GetProperties()->GetUseLighting());
	pSphereVis->GetProperties()->SetVisible(m_pPrototypSphere->GetProperties()->GetVisible());

	// define color
	float fColor[4];
	m_pPrototypSphere->GetProperties()->GetColor(fColor);
	
	if(!m_bUnique)
		fColor[m_iIndex] = fColor[m_iIndex]*(1-fParam);
	
	pSphereVis->GetProperties()->SetColor(fColor);

	m_vecSphereVis.push_back(pSphereVis);

}

/* ==========================================================================*/
/* NAME: TidyUp							 									 */
/* ==========================================================================*/
void VfaSphereView::TidyUp()
{
	const size_t iSize = m_vecSphereVis.size();
	for (size_t i = 0; i < iSize; ++i)
	{
		m_pRenderNode->RemoveRenderable(m_vecSphereVis[i]);
		delete m_vecSphereVis[i]->GetModel();
		delete m_vecSphereVis[i];
	}

	m_vecSphereVis.clear();
}


// ========================================================================== //
// === End of File
// ========================================================================== //
