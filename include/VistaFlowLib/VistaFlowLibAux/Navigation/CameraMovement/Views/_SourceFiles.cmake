

set( RelativeDir "./Navigation/CameraMovement/Views" )
set( RelativeSourceGroup "Source Files\\Navigation\\CameraMovement\\Views" )

set( DirFiles
	VfaArrowView.cpp
	VfaArrowView.h
	VfaInterpolatorView.cpp
	VfaInterpolatorView.h
	VfaSphereView.cpp
	VfaSphereView.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

