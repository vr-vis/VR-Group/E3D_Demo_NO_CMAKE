/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// ========================================================================== //
// INCLUDES
// ========================================================================== //

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/Navigation/CameraMovement/VfaCameraInterpolator.h>
#include <VistaFlowLibAux/Navigation/CameraMovement/Views/VfaArrowView.h>
#include <VistaFlowLibAux/Widgets/VfaArrowVis.h>

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
VfaArrowView::VfaArrowView (IVfaCameraInterpolator *pCamInterpolator,
							    VflRenderNode *pRenderNode,
								VfaArrowVis *pPrototypArrow)
	:	IVfaInterpolatorView(pCamInterpolator),
		m_pRenderNode(pRenderNode),
		m_pPrototypArrow(pPrototypArrow),
		m_iSample(0),
		m_fCurveArrowRadius(0.0f),
		m_iIndex(0),
		m_bUnique(true),
		m_iNrOfNotifications(0)
{}

VfaArrowView::~VfaArrowView()
{
	this->TidyUp();
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/* ==========================================================================*/
/* NAME: Set/GetSample					 									 */
/* ==========================================================================*/
bool VfaArrowView::SetSample(int iNth)
{
	if (m_iSample == iNth)
		return false;

	m_iSample = iNth;
	return true;
}
int VfaArrowView::GetSample() const
{
	return m_iSample;
}

/* ==========================================================================*/
/* NAME: ObserverUpdate					 									 */
/* ==========================================================================*/
void VfaArrowView::ObserverUpdate(IVistaObserveable *pObserveable, 
															int msg, int ticket)
{
#ifdef DEBUG
	vstr::debugi() << "[VfaArrowView] ObserverUpdate" << std::endl;
#endif

	if(msg == IVfaCameraInterpolator::MSG_CAM_MOVE_NEWSTEP ||
		msg == IVfaCameraInterpolator::MSG_CAM_MOVE_ENDED)
	{
		// just draw a new Arrow if it fits to the sampling rate
		++m_iNrOfNotifications;
		if (m_iSample == 0 || m_iNrOfNotifications % m_iSample == 0)
			this->AddArrow();

	}
}

/* ==========================================================================*/
/* NAME: Set/GetUniqueColor						 							 */
/* ==========================================================================*/
bool VfaArrowView::SetUniqueColor(bool bUnique)
{
	if (m_bUnique == bUnique)
		return false;

	m_bUnique = bUnique;
	return true;
	
}
bool VfaArrowView::GetUniqueColor() const
{
	return m_bUnique;
}
/* ==========================================================================*/
/* NAME: Set/GetIndex						 								 */
/* ==========================================================================*/
bool VfaArrowView::SetIndex(int iInd)
{
	if (iInd < 0 || iInd > 2)
		return false;

	if(m_iIndex == iInd)
		return false;

	m_iIndex = iInd;
	// now we have an index, this means the color is not unique :)
	m_bUnique = false;

	return true;
}
int VfaArrowView::GetIndex() const
{
	return m_iIndex;
}

/* ==========================================================================*/
/* NAME: AddArrow						 									 */
/* ==========================================================================*/
void VfaArrowView::AddArrow()
{
	// first of all get the current param
	float fParam = m_pCamInterpolator->GetParameter();

#ifdef DEBUG
	vstr::debugi() << "[VfaArrowView] Arrow for Update: " << fParam << std::endl;
#endif

	// now interpolate to get all available information
	// about pos and ori
	VistaVector3D v3Pos;
	VistaQuaternion qOri;
	m_pCamInterpolator->Interpolate(fParam, v3Pos, qOri);
	
	// this one will ne used to compute the center in vis space
	VistaTransformMatrix matRenderNode;
	m_pRenderNode->GetTransform(matRenderNode);
	
	
	// duplicate prototyp Arrow and set oit to the current pos
	VfaArrowVis *pArrowVis = new VfaArrowVis();
	pArrowVis->SetConeHeight(m_pPrototypArrow->GetConeHeight());
	pArrowVis->SetConeRadius(m_pPrototypArrow->GetConeRadius());
	pArrowVis->SetCylinderHeight(m_pPrototypArrow->GetCylinderHeight());
	pArrowVis->SetCylinderRadius(m_pPrototypArrow->GetCylinderRadius());

	VistaVector3D v3MP = matRenderNode.GetInverted().Transform(v3Pos);
	VistaVector3D v3Dir = qOri.Rotate(VistaVector3D(0,0,-1)); 
	v3Pos = v3MP + m_pPrototypArrow->GetCylinderHeight()*(v3MP - v3Dir);

	pArrowVis->SetCenter(v3MP);
	pArrowVis->SetRotate(v3Dir);


	if(!pArrowVis->Init())
		return;

	m_pRenderNode->AddRenderable(pArrowVis);
	
	// define color
	float fColor[4];
	m_pPrototypArrow->GetColor(fColor);
	
	if(!m_bUnique)
		fColor[m_iIndex] = fColor[m_iIndex]*(1-fParam);
	
	pArrowVis->SetColor(fColor);

	m_vecArrowVis.push_back(pArrowVis);
}

/* ==========================================================================*/
/* NAME: TidyUp							 									 */
/* ==========================================================================*/
void VfaArrowView::TidyUp()
{
	const size_t iSize = m_vecArrowVis.size();
	for (size_t i = 0;  i < iSize; ++i)
	{
		m_pRenderNode->RemoveRenderable(m_vecArrowVis[i]);
		delete m_vecArrowVis[i];
	}

	m_vecArrowVis.clear();
}

// ========================================================================== //
// === End of File
// ========================================================================== //
