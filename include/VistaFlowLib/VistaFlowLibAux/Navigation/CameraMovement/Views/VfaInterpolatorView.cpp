/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// ========================================================================== //
// INCLUDES
// ========================================================================== //
//FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderable.h>

// FlowLibAux stuff
#include "VfaInterpolatorView.h"
#include <VistaFlowLibAux/Navigation/CameraMovement/VfaCameraInterpolator.h>

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
IVfaInterpolatorView::IVfaInterpolatorView
								  	  (IVfaCameraInterpolator *pCamInterpolator)
	:	m_pCamInterpolator(pCamInterpolator)
{
	this->Observe(m_pCamInterpolator);
}

IVfaInterpolatorView::~IVfaInterpolatorView()
{}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/* ==========================================================================*/
/* NAME: Set/GetInterpolator			 									 */
/* ==========================================================================*/
void IVfaInterpolatorView::SetInterpolator
									(IVfaCameraInterpolator *pCamInterpolator)
{
	this->ObserveableDelete(m_pCamInterpolator);
	m_pCamInterpolator = pCamInterpolator;
	this->Observe(m_pCamInterpolator);
}
IVfaCameraInterpolator* IVfaInterpolatorView::GetInterpolator() const
{
	return m_pCamInterpolator;
}

/* ==========================================================================*/
/* NAME: ObserverUpdate					 									 */
/* ==========================================================================*/
void IVfaInterpolatorView::ObserverUpdate(IVistaObserveable *pObserveable, 
															int msg, int ticket)
{

}

/* ==========================================================================*/
/* NAME: GetRegistrationMode			 									 */
/* ==========================================================================*/
unsigned int IVfaInterpolatorView::GetRegistrationMode() const
{
	return IVflRenderable::OLI_UPDATE | IVflRenderable::OLI_DRAW_OPAQUE;
}

/* ==========================================================================*/
/* NAME: CreateProperties			 										 */
/* ==========================================================================*/
IVflRenderable::VflRenderableProperties* 
						IVfaInterpolatorView::CreateProperties() const
{
		return new VflRenderableProperties;
}

// ========================================================================== //
// === End of File
// ========================================================================== //
