/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
// ViSTA stuff
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

// FlowLibAux stuff
#include "VfaInterpolatorCombiner.h"
#include "STrailStation.h"

using namespace std;

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
VfaInterpolatorCombiner::VfaInterpolatorCombiner(VflRenderNode *pRenderNode,
											   VistaVirtualPlatform *pPlatform)
	:	IVfaCameraInterpolator(pRenderNode, pPlatform),
		m_iCurrentIndex(0)
{

}

VfaInterpolatorCombiner::~VfaInterpolatorCombiner()
{}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/* ==========================================================================*/
/* NAME: Start()					 									     */
/* ==========================================================================*/
void VfaInterpolatorCombiner::Start()
{
	// do some calculations before starting the camera movement itself
	if(this->Preparation())
		IVfaCameraInterpolator::Start();
	else
		vstr::debugi() << "[VfaInterpolatorCombiner] No Cam move has to be done." << endl;
}

/* ==========================================================================*/
/* NAME: Reset()					 									     */
/* ==========================================================================*/
void VfaInterpolatorCombiner::Reset()
{
	m_vecArcSegmentLength.clear();
	m_vecParams.clear();
	
	const size_t iSize = m_vecInterpolators.size();
	for (size_t i = 0; i < iSize; ++i)
		delete m_vecInterpolators[i];
	
	m_vecInterpolators.clear();

	m_iCurrentIndex = 0;

	// last but not least remove all stations
	IVfaCameraInterpolator::Reset();

}

/* ==========================================================================*/
/* NAME: Preparation()				 									     */
/* ==========================================================================*/
bool VfaInterpolatorCombiner::Preparation()
{
	float fLength = this->GetArcLength();
	m_fDuration = fLength / m_fVelocity;
	
	// compute time deltas
	const size_t iSize = m_vecArcSegmentLength.size();
	m_vecParams.push_back(0.0f);
	for (size_t i = 0; i< iSize; ++i)
	{
		if (i == iSize-1)
			m_vecParams.push_back(1);
		else
			m_vecParams.push_back(m_vecArcSegmentLength[i]/fLength+m_vecParams[i]);
	}

	return true;
}

/* ==========================================================================*/
/* NAME: Interpolate				 									     */
/* ==========================================================================*/
void VfaInterpolatorCombiner::Interpolate(float fParam, VistaVector3D &v3Pos,
		VistaQuaternion &qOri)
{
	const size_t iSize = m_vecInterpolators.size();
	size_t iSegment = 0;
	for (iSegment = 0; iSegment < iSize; ++iSegment)
	{
		if (m_vecParams[iSegment] > fParam)	
			break;
	}
	--iSegment;

	// recompute parameter
	fParam = (fParam - m_vecParams[iSegment])/(m_vecParams[iSegment+1] - m_vecParams[iSegment]);
	// get values for pos and ori
	m_vecInterpolators[iSegment]->Interpolate(fParam, v3Pos, qOri);
}

/* ==========================================================================*/
/* NAME: AddNewInterpolator			 									     */
/* ==========================================================================*/
bool VfaInterpolatorCombiner::AddNewInterpolator (IVfaCameraInterpolator* pInterpol,
									 int iStartIndex, int iEndIndex)
{
	// start index has to be smaller than end index
	if(iStartIndex >= iEndIndex)
		return false;

	// check for wrong indexes
	if (iStartIndex < 0 || iEndIndex >= static_cast<int>(m_vecTrailStations.size()))
		return false;

	// add specified points for special interpolation
	for (int i = iStartIndex; i <= iEndIndex; ++i)
		pInterpol->AddStation(m_vecTrailStations[i].m_v3Position, m_vecTrailStations[i].m_qOrientation);

	// Test whether points are correct
	if(!pInterpol->Preparation())
	{
		pInterpol->Reset(); // delete all stations
		return false;
	}

	// now we know everything is ok, so store the interpolator
	m_vecInterpolators.push_back(pInterpol);

	// last test
	// check whether the correct station is startstation,
	// else delete interpolator
	const size_t iInterPolSize = m_vecInterpolators.size();
	if (iStartIndex != m_iCurrentIndex)
	{
		m_vecInterpolators[iInterPolSize-1]->Reset();
		m_vecInterpolators.pop_back();
		vstr::errp() << "[VfaInterpolatorCombiner] Please add an interpolator starting at index "
			 << m_iCurrentIndex << ". Thanks." << endl;
		return false;
	}
	m_iCurrentIndex += iEndIndex;

	return true;
}

/* ==========================================================================*/
/* NAME: GetArcLength				 									     */
/* ==========================================================================*/
float VfaInterpolatorCombiner::GetArcLength()
{
	m_fArcLength = 0.0f;

	const size_t iSize = m_vecInterpolators.size();
	for (size_t i = 0; i < iSize; ++i)
	{
		float f = m_vecInterpolators[i]->GetArcLength();
		m_vecArcSegmentLength.push_back(f);
		m_fArcLength += f;
	}
	
	return m_fArcLength;
}

/* ==========================================================================*/
/* NAME: Set/GetVelocity				 									 */
/* ==========================================================================*/
bool VfaInterpolatorCombiner::SetVelocity(float fVelo)
{
	if (fVelo<0.0f)
		return false;

	m_fVelocity	= fVelo;
	return true;
}
float VfaInterpolatorCombiner::GetVelocity() const
{
	return m_fVelocity;
}


/* ==========================================================================*/
/* NAME: GetClassName				 									     */
/* ==========================================================================*/
std::string VfaInterpolatorCombiner::GetClassName()
{ 
	return "VfaInterpolatorCombiner";
}



// ========================================================================== //
// === End of File
// ========================================================================== //
