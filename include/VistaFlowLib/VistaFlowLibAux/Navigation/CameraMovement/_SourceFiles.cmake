

set( RelativeDir "./Navigation/CameraMovement" )
set( RelativeSourceGroup "Source Files\\Navigation\\CameraMovement" )
set( SubDirs Views )

set( DirFiles
	STrailStation.h
	VfaCameraInterpolator.cpp
	VfaCameraInterpolator.h
	VfaCatmullRomInterpolator.cpp
	VfaCatmullRomInterpolator.h
	VfaCircularInterpolator.cpp
	VfaCircularInterpolator.h
	VfaInterpolatorCombiner.cpp
	VfaInterpolatorCombiner.h
	VfaLinearInterpolator.cpp
	VfaLinearInterpolator.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()

