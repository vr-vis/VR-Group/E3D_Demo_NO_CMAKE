/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// ========================================================================== //
// INCLUDES
// ========================================================================== //
// ViSTA stuff
#include <VistaBase/VistaStreamUtils.h>
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

// FlowLibAux stuff
#include "VfaCameraInterpolator.h"
#include "STrailStation.h"

#include <cstdio>

using namespace std;

// ========================================================================== //
// CON-/DESTRUCTOR
// ========================================================================== //
IVfaCameraInterpolator::IVfaCameraInterpolator(VflRenderNode *pRenderNode,
											   VistaVirtualPlatform *pPlatform)
:	m_pRenderNode(pRenderNode),
	m_pPlatform(pPlatform),
	m_fDuration(5.0f),
	m_fArcLength(0.0f),
	m_dLastTimeStep(0.0),
	m_fCurrentTime(0.0f),
	m_fCurrentParam(0.0f),
	m_bDoRestore(false),
	m_bMovement(true),
	m_bWaitAtStation(false),
	m_fWaitAtStationDur(1.0f),
	m_dWaitStartTS(0.0),
	m_iWaitAtStation(0)
{}

IVfaCameraInterpolator::~IVfaCameraInterpolator()
{
	m_vecTrailStations.clear();
}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/* ==========================================================================*/
/* NAME: Start()					 									     */
/* ==========================================================================*/
void IVfaCameraInterpolator::Start()
{
	// If we do not have enough stations for camera movement or if we are
	// still in-flight, stop it here.
	if(m_vecTrailStations.size() < 2 || m_bDoRestore)
		return;

	// reset time values
	m_fCurrentTime = 0.0f;
	m_dLastTimeStep = m_pRenderNode->GetVisTiming()->GetCurrentClock();
	m_fCurrentParam = 0.0f;

#ifdef DEBUG
	vstr::debugi() << "starting camera movement" << endl;
#endif

	m_bDoRestore = true;

	// throw notification, that the movement starts now
	this->Notify(MSG_CAM_MOVE_STARTED);
}

/* ==========================================================================*/
/* NAME: Stop()						 									     */
/* ==========================================================================*/
void IVfaCameraInterpolator::Stop()
{
#ifdef DEBUG
	vstr::debugi() << "stopping camera movement" << endl;
#endif

	m_fCurrentTime = 0.0f;
	m_bDoRestore = false;
	
	// also reset parameters used for defining what's the current interval
	// and what's the movement state
	m_iWaitAtStation = 0;
	m_bWaitAtStation = false;

	// ATTENTION:
	// DON'T throw a notification here, because after calling this method
	// the platform will be set one more time.
	// That's why the updat-routine informs about the end of the movement
	// by having a look on the current parameter
}

/* ==========================================================================*/
/* NAME: Pause()					 									     */
/* ==========================================================================*/
void IVfaCameraInterpolator::Pause()
{
	m_bDoRestore = false;
}

/* ==========================================================================*/
/* NAME: Continue()					 									     */
/* ==========================================================================*/
void IVfaCameraInterpolator::Continue()
{
	m_dLastTimeStep = m_pRenderNode->GetVisTiming()->GetCurrentClock();
	m_bDoRestore = true;
}


/* ==========================================================================*/
/* NAME: Update()					 									     */
/* ==========================================================================*/
void IVfaCameraInterpolator::Update()
{
	if(m_pRenderNode == NULL)
		return;

	if(!m_bDoRestore)
		return;

	// Helper variables.
	VistaVector3D v3Pos;
	VistaQuaternion qOri;
	
	if(!m_bWaitAtStation)
	{
		// Determine the interval of the last interpolation step.
		int iOldInterval		= GetIntervalForParameter(m_fCurrentParam);

		// We'll only do a simulation here because we only want to check whether
		// the new interpolation interval differs from the last one (without
		// generating any side effects).
		float fNewParam			= this->UpdateParameter(true);
		int iNewInterval		= GetIntervalForParameter(fNewParam);

		if(iOldInterval != iNewInterval)
		{
			m_bWaitAtStation	= true;
			m_dWaitStartTS		= GetRenderNode()->GetVisTiming()->GetCurrentClock();
			m_iWaitAtStation	= iNewInterval;

			vstr::outi()<<"[VfaCameraInterpolator] Waiting at station: "<<iNewInterval<<" (Old station: "<<iOldInterval<<")..."<<endl;
		}

		// FIX (maybe): explicitly update the m_fCurrentParam in case of "Stop"-event
		if(fNewParam == 1.0f)
			m_fCurrentParam = fNewParam;
	}
	else
	{
		double dDelta = GetRenderNode()->GetVisTiming()->GetCurrentClock()
				- m_dWaitStartTS;

		if(dDelta > double(m_fWaitAtStationDur))
		{
			m_bWaitAtStation = false;
			vstr::outi()<<"[VfaCameraInterpolator] Done waiting."<<endl;
		}
	}

	// If the interpolation intervals are the same, we'll do a _normal_
	// interpolation step.
	if(!m_bWaitAtStation)
	{
		m_fCurrentParam = this->UpdateParameter(false);
		this->Interpolate(m_fCurrentParam, v3Pos, qOri);

#ifdef DEBUG
		vstr::debugi() << "camera interpolation alpha: " << m_fCurrentParam << endl;
#endif

	}
	else
	{
		// @todo This crashes if only two stations are present as this tries
		//		 to access a third one. Fix for now: deactivate wait at station.
		//		 It should not be possible for two stations anyways!
		v3Pos	= m_vecTrailStations[m_iWaitAtStation+1].m_v3Position;
		v3Pos 	-= GetUserOffset();
		qOri	= m_vecTrailStations[m_iWaitAtStation+1].m_qOrientation;
	}
	
	if (m_bMovement)
		this->SetPosition(v3Pos, qOri);

	m_dLastTimeStep = m_pRenderNode->GetVisTiming()->GetCurrentClock();

	if(m_fCurrentParam == 1.0f)  
		this->Notify(MSG_CAM_MOVE_ENDED);
	else
		this->Notify(MSG_CAM_MOVE_NEWSTEP);
}

/* ==========================================================================*/
/* NAME: UpdateParameter()					 								 */
/* ==========================================================================*/
float IVfaCameraInterpolator::UpdateParameter(bool bSimulateUpdate)
{
	const float fCurTimeBackup = m_fCurrentTime;

	// compu the current parameter depending of time delta
	float fTimeDelta = float(m_pRenderNode->GetVisTiming()->GetCurrentClock()
							- m_dLastTimeStep);
	m_fCurrentTime+=fTimeDelta;

	float fParam =  m_fCurrentTime/m_fDuration;

	// Are we still in time?
	bool bDurationHit = m_fCurrentTime > m_fDuration;

	// If we only simulate the update, we'll reset the member variable to
	// the value it had when entering this function.
	if(bSimulateUpdate)
		m_fCurrentTime = fCurTimeBackup;
	
	if(!bDurationHit)
		return fParam;
		
	// ... Otherwise set the conditions to set the exact pos and ori
	if(!bSimulateUpdate)
		Stop();
	return 1.0f;
}

/* ==========================================================================*/
/* NAME: SetPosition()					 									 */
/* ==========================================================================*/
void IVfaCameraInterpolator::SetPosition(const VistaVector3D &v3Pos, 
										 const VistaQuaternion &qOri)
{
	m_pPlatform->SetTranslation(v3Pos);
	m_pPlatform->SetRotation(qOri);	
}


/* ==========================================================================*/
/* NAME: GetParameter()					 									 */
/* ==========================================================================*/
float IVfaCameraInterpolator::GetParameter()
{
	return m_fCurrentParam;
}


/* ==========================================================================*/
/* NAME: GetIntervalForParameter()											 */
/* ==========================================================================*/
int IVfaCameraInterpolator::GetIntervalForParameter(float fParam)
{
	return -1;
}


/* ==========================================================================*/
/* NAME: GetUserOffset()  						     */
/* ==========================================================================*/
VistaVector3D IVfaCameraInterpolator::GetUserOffset()
{
	return VistaVector3D(0.0f, 0.0f, 0.0f, 0.0f);
}


/* ==========================================================================*/
/* NAME: AddStation()					 									 */
/* ==========================================================================*/
bool IVfaCameraInterpolator::AddStation(const VistaVector3D &v3Pos,
										const VistaQuaternion &qOri)
{
#ifdef DEBUG
	vstr::debugi() << "adding new camera interpolator station" << std::endl;
#endif

	// no new stations during a camera movement
	if (m_bDoRestore)
		return false;

	STrailStation sStation;
	sStation.m_v3Position = v3Pos;
	sStation.m_qOrientation = qOri;
	m_vecTrailStations.push_back(sStation);
	this->Notify(MSG_ADD_CONTROLPT);
	return true;
}
/* ==========================================================================*/
/* NAME: GetStation()					 									 */
/* ==========================================================================*/
bool IVfaCameraInterpolator::GetStation(int iIndex, 
							VistaVector3D &v3Pos, VistaQuaternion &qOri) const
{
	if(static_cast<int>(m_vecTrailStations.size()) <= iIndex)
		return false;

	STrailStation sStation = m_vecTrailStations.at(iIndex);
	v3Pos = sStation.m_v3Position;
	qOri = sStation.m_qOrientation;
	return true;
}
/* ==========================================================================*/
/* NAME: Remove()					 										 */
/* ==========================================================================*/
bool IVfaCameraInterpolator::Remove(int iIndex)
{
	if(static_cast<int>(m_vecTrailStations.size()) <= iIndex)
		return false;

	m_vecTrailStations.erase(m_vecTrailStations.begin()+iIndex);
	this->Notify(MSG_REM_CONTROLPT);
	return true;
}

/* ==========================================================================*/
/* NAME: Reset()					 										 */
/* ==========================================================================*/
void IVfaCameraInterpolator::Reset()
{
	if(m_bDoRestore)
		return;

	m_vecTrailStations.clear();
}

/* ==========================================================================*/
/* NAME: SetEnableMovement			 										 */
/* ==========================================================================*/
void IVfaCameraInterpolator::SetEnableMovement(bool bMovement)
{
	m_bMovement = bMovement;
}
bool IVfaCameraInterpolator::GetEnableMovement()
{
	return m_bMovement;
}

/* ==========================================================================*/
/* NAME: GetIsInFlight				 										 */
/* ==========================================================================*/
bool IVfaCameraInterpolator::GetIsInFlight() const
{
	return m_bDoRestore;
}

/* ==========================================================================*/
/* NAME: GetRegistrationMode()		 									     */
/* ==========================================================================*/
unsigned int IVfaCameraInterpolator::GetRegistrationMode() const
{
	return IVflRenderable::OLI_UPDATE | IVflRenderable::OLI_DRAW_OPAQUE;
}

/* ==========================================================================*/
/* NAME: CreateProperties													 */
/* ==========================================================================*/
IVflRenderable::VflRenderableProperties* 
							IVfaCameraInterpolator::CreateProperties() const
{
	return new VflRenderableProperties;
}

const size_t IVfaCameraInterpolator::GetNumberOfStations() const
{
	return m_vecTrailStations.size();
}

// ========================================================================== //
// === End of File
// ========================================================================== //
