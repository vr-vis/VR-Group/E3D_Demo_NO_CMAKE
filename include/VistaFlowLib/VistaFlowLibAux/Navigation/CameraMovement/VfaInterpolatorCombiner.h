/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _VFAINTERPOLATIONCOMBINER_H
#define _VFAINTERPOLATIONCOMBINER_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// ViSTA stuff
#include <VistaAspects/VistaObserveable.h>
#include <VistaBase/VistaVectorMath.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderable.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include "STrailStation.h"
#include "VfaCameraInterpolator.h"
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VistaVirtualPlatform;
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * What's this?
 * This class is just a little dummy to be able to control several camera 
 * movements with different camera interpolators
 *
 * class usage:
 * 1) first of all insert all stations in correct order 
 *    (order you want to visit during the walk)
 * 2) afterwards define which stations should be connected by which camera interpolation
 *    (attention: insert interpolators in the correct order)
 * 3) then start everything
 *
 * If you want to add new stations to the walk, you have to reset everything
 * and start totally new.
 */
class VISTAFLOWLIBAUXAPI VfaInterpolatorCombiner :	public IVfaCameraInterpolator
{
public:

	/**
	 * Constructor
	 */
	VfaInterpolatorCombiner(VflRenderNode *pRenderNode, 
		VistaVirtualPlatform *pPlatform);

	/**
	 * Destructor
	 */
	virtual ~VfaInterpolatorCombiner();


	/**
	 *  After inheritance, you have to insert the whished behaviour here!
	 */
	virtual void Interpolate(float fParam, VistaVector3D &v3Pos,
										   VistaQuaternion &qOri);


	/**
	 * Add an interpolator and define
	 * which stations he should connect
	 */
	virtual bool AddNewInterpolator (IVfaCameraInterpolator* pInterpol,
									 int iStartIndex, int iEndIndex);

	/**
	 * Start the animation (camera movement)
	 */
	virtual void Start();

	/**
	* @See IVfaCameraInterpolator::Reset
	 */
	virtual void Reset();

	/**
	 * Get to know the acr length between the specified stations
	 */
	virtual float GetArcLength();

	/**
	 * Returns classname as string
	 */
	virtual std::string GetClassName();

	/**
	* @See IVfaCameraInterpolator::Preparation()
	 */
	virtual bool Preparation();

	/**
	 * Define velocity factor
	 */
	virtual bool SetVelocity(float fVelo);

	/**
	 * Get velocity factor
	 */
	virtual float GetVelocity() const;

protected:


private:
	/** store given Interpolators */
	std::vector<IVfaCameraInterpolator *> m_vecInterpolators;

	/** used for interpolate method*/
	std::vector<float> m_vecParams;

	/** store the length of each arc segment */
	std::vector<float> m_vecArcSegmentLength;

	/** factor for velocity; is multiplied by the arc length */
	float m_fVelocity;

	/**
	 * Store the current index
	 */
	int m_iCurrentIndex;
	
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // Include guard.
