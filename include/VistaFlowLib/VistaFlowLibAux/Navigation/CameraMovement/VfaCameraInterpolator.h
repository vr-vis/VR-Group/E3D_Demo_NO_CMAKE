/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _IVFACAMERAINTERPOLATOR_H
#define _IVFACAMERAINTERPOLATOR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

// ViSTA stuff
#include <VistaAspects/VistaObserveable.h>
#include <VistaBase/VistaVectorMath.h>

// FlowLib stuff
#include <VistaFlowLib/Visualization/VflRenderable.h>

// FlowLibAux stuff
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include "STrailStation.h"
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VistaVirtualPlatform;
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Interface which defines routines and algorithms all camera interpolation have
 * to use.
 * 
 * If you want to define an own camera movement, inherit from
 * IVfaCameraInterpolator and fill the Interpolate-routine.
 *
 *
 * Attention: 
 * To use this classes you have to configurate the display_desktop.ini
 * VIEWER_POSITION has to be 0,0,0
 * PROJ_PLANE_MIDPOINT has to be 0,0,-1
 *
 * @todo Base the interpolator on template policies. Should worth the while.
 */
class VISTAFLOWLIBAUXAPI IVfaCameraInterpolator :	public IVflRenderable,
													public IVistaObserveable
{
public:

	/** 
	 * enumeration of notifications about important acts
	 */
	enum
	{
		MSG_CAM_MOVE_STARTED = IVistaObserveable::MSG_LAST,
		MSG_CAM_MOVE_ENDED,
		MSG_CAM_MOVE_NEWSTEP,
		MSG_ADD_CONTROLPT,
		MSG_REM_CONTROLPT,
		MSG_END_PREPARATION,
		MSG_LAST
	};

	/**
	 * Destructor
	 */
	virtual ~IVfaCameraInterpolator();

	/**
	 * is called regulary
	 */
	virtual void Update();

	/**
	 * \see{VflRenderable.h} for explanations
	 */
	virtual unsigned int GetRegistrationMode() const;


	/**
	 *	Add a new station consisting of a position and an orientation
	 */
	virtual bool AddStation(const VistaVector3D &v3Pos, 
							const VistaQuaternion &qOri);
	
	/**
	 * Get a station, defined by the index iIndex
	 */
	virtual bool GetStation(int iIndex, VistaVector3D &v3Pos,
										VistaQuaternion &qOri) const;
	
	/**
	 *	Remove a station, defined by the index iIndex
	 */
	virtual bool Remove(int iIndex);
	virtual const size_t GetNumberOfStations() const;

	/**
	 *  Delete all stations
	 */
	virtual void Reset();

	/**
	 *  After inheritance, you have to insert the whished behaviour here!
	 */
	virtual void Interpolate(float fParam, VistaVector3D &v3Pos,
										   VistaQuaternion &qOri) = 0;


	/**
	 * Get the parameter that shows the progress of the movement
	 * parameter in [0;1]
	 * 0 --> beginn; 1 --> end
	 */
	virtual float GetParameter();


	/**
	 * Returns for a parameter value in [0,1] the index of the station interval
	 * that would be used to interpolate a position for the parameter value.
	 *
	 * For example, assume index 1 is returned by the function. This means that
	 * the specified parameter would be used to interpolate between station 1
	 * and 2. Note, that the specified parameter is a _global_ parameter value
	 * and that it might actually be re-parameterized internally by the
	 * interpolator before using it to interpolate between the two stations.
	 */
	virtual int GetIntervalForParameter(float fParam);

	virtual VistaVector3D GetUserOffset();

	/**
	 * Start the animation (camera movement)
	 */
	virtual void Start();

	/**
	 * Stop the animation (camera movement), because we reached the aim.
	 * After calling this method, the platform will be set
	 * one more time, to reach exactly the wished pos and ori.
	 */
	virtual void Stop();

	/**
	 * Stop the animation at a current state AND
	 * be able to go on from this point by calling
	 * Continue().
	 */
	virtual void Pause();
	
	/**
	 * Go on in the animation after calling Pause();
	 */
	virtual void Continue();

	/**
	 * Define whether a movement should be done or not 
	 * not means just calculating the path
	 */
	virtual void SetEnableMovement(bool bMovement);

	/**
	 *	get to know whether a movement will be done or not
	 */
	virtual bool GetEnableMovement();

	/**
	 * Get to know the acr length between the specified stations
	 */
	virtual float GetArcLength() = 0;

	/**
	 * Returns classname as string
	 */
	virtual std::string GetClassName() = 0;

	/**
	 * make all preparations (just call this method if you won't use the start-routine)
	 */
	virtual bool Preparation() = 0;

	/**
	 * Informs about whether a camera interpolation is currently running.
	 */
	bool GetIsInFlight() const;

protected:
	/**
	 * protected constructor
	 * because this is only an interfce
	 */
	IVfaCameraInterpolator(VflRenderNode *pRenderNode, 
						   VistaVirtualPlatform *pPlatform);

	/** 
	* IVflRenderable Interface 
	*/
	virtual VflRenderableProperties* CreateProperties() const;

	/**
	 * Compute current parameter
	 * If the bSimulateUpdate flag is 'true' the update will be performed
	 * but the internal state of the interpolator will _not_ change.
	 */
	virtual float UpdateParameter(bool bSimulateUpdate);

	/**
	 * Set new Position and orientation to the platform
	 */
	virtual void SetPosition(const VistaVector3D &v3Pos, 
							const VistaQuaternion &qOri);

	//########################
	// params
	//########################

	/**
	 *	Render Node for timing informations
	 */
	VflRenderNode				*m_pRenderNode;
	
	/**
	 * Platform which will be manipulated
	 */
	VistaVirtualPlatform		*m_pPlatform;
	
	/**
	 * Duration of the whole movement
	 */
	float m_fDuration;

	/**
	 * length of the arc between the specified points
	 */
	float m_fArcLength;

	/**
	 * vector used to save all stations on the trail
	 */
	std::vector<STrailStation> m_vecTrailStations;


private:
	double m_dLastTimeStep;
	float m_fCurrentTime;

	float m_fCurrentParam;

	bool m_bDoRestore;
	bool m_bMovement;

	bool m_bWaitAtStation;
	float m_fWaitAtStationDur;
	double m_dWaitStartTS;
	int m_iWaitAtStation;
	VistaVector3D m_v3UserOffset;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _IVFACAMERAINTERPOLATOR_H */
