/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VfaSystemUpdateHandler.h" 

#include <VistaKernel/DisplayManager/VistaDisplayManager.h>
#include <VistaKernel/DisplayManager/VistaDisplaySystem.h>
#include <VistaKernel/EventManager/VistaEventManager.h>
#include <VistaKernel/EventManager/VistaSystemEvent.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaAspects/VistaExplicitCallbackInterface.h>
#include <VistaAspects/VistaTransformable.h>


#include <VistaBase/VistaVersion.h>


/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaSystemUpdateHandler::VfaSystemUpdateHandler(
							VistaEventManager *pEvMgr,
							VistaDisplayManager *pDispMgr,
							VflRenderNode *pRenderNode)
: VistaEventHandler(),
  m_pRenderNode(pRenderNode),
  m_pEvMgr(pEvMgr),
  m_pDispMgr(pDispMgr),
  m_pLightTransform(NULL)
{
	m_pEvMgr->AddEventHandler(this, VistaSystemEvent::GetTypeId(),
		VistaSystemEvent::VSE_POSTAPPLICATIONLOOP);
}

VfaSystemUpdateHandler::~VfaSystemUpdateHandler()
{
	m_pEvMgr->RemEventHandler(this, VistaEventManager::NVET_ALL,
								  VistaEventManager::NVET_ALL);
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   HandleEvent                                                 */
/*                                                                            */
/*============================================================================*/
void VfaSystemUpdateHandler::HandleEvent(VistaEvent *pEvent)
{
	// we pass the return of update internals as
	// state to the handle flag of the event
	if(m_pRenderNode)
	{
		
//#if defined(__VISTA_HEAD)
		m_pRenderNode->TellGlobalViewPosition(
			m_pDispMgr->GetDisplaySystem()->GetDisplaySystemProperties()
				->GetViewerPosition());
		m_pRenderNode->TellGlobalViewOrientation(
			m_pDispMgr->GetDisplaySystem()->GetDisplaySystemProperties()
				->GetViewerOrientation());
//#else
//		m_pController->SetViewPosition(
//			m_pDispMgr->GetDisplaySystem()->GetViewerPosition());
//        m_pController->SetViewOrientation(
//			m_pDispMgr->GetDisplaySystem()->GetViewerOrientation());
//#endif

		if(m_pLightTransform)
		{
			float mt[16];

			m_pLightTransform->GetWorldTransform(mt);

			VistaTransformMatrix m(mt);
			VistaQuaternion q(m);

			// hack: this is a fix for a neg-z headlight
			// in a real setup, we should transform the light direction
			// with the transform m and get that direction...
			// but for that, we'd have to change the interface
			// of the update handler a bit
			// maybe it would be better to register a callback that
			// is executed matching the application's needs.
			m_pRenderNode->TellGlobalLightDirection(-q.Rotate(VistaVector3D(0,0,-1)));
		}

		m_pRenderNode->Update(pEvent->GetTime());
		pEvent->SetHandled(false);
	}
	for(unsigned int i=0; i<m_vecCallbacks.size(); ++i)
		if(m_vecCallbacks[i])
			(m_vecCallbacks[i])->Do();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   RegisterAction                                              */
/*                                                                            */
/*============================================================================*/
int VfaSystemUpdateHandler::RegisterAction(IVistaExplicitCallbackInterface *pAction)
{
	m_vecCallbacks.push_back(pAction);
	return static_cast<int>(m_vecCallbacks.size()-1);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIsRegistered                                             */
/*                                                                            */
/*============================================================================*/
bool VfaSystemUpdateHandler::GetIsRegistered(IVistaExplicitCallbackInterface *pAction) const
{
	for(size_t i=0; i<m_vecCallbacks.size(); ++i)
		if(m_vecCallbacks[i] == pAction)
			return true;
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   UnregisterAction                                            */
/*                                                                            */
/*============================================================================*/
bool VfaSystemUpdateHandler::UnregisterAction(int iPos, bool bDelete)
{
	if(iPos < 0 || static_cast<size_t>(iPos) >= m_vecCallbacks.size())
		return false;
	if(bDelete)
		delete m_vecCallbacks[iPos];
	m_vecCallbacks[iPos]= NULL;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   UnregisterAction                                            */
/*                                                                            */
/*============================================================================*/
bool VfaSystemUpdateHandler::UnregisterAction(
	IVistaExplicitCallbackInterface *pAction, bool bDelete)
{
	for(size_t i=0; i<m_vecCallbacks.size(); ++i)
	{
		if(m_vecCallbacks[i] == pAction)
		{
			if(bDelete)
				delete m_vecCallbacks[i];
			m_vecCallbacks[i] = NULL;
			return true;
		}
	}
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCallback                                                 */
/*                                                                            */
/*============================================================================*/
IVistaExplicitCallbackInterface *VfaSystemUpdateHandler::GetCallback(int i) const
{
	if(i < 0 || static_cast<size_t>(i) >= m_vecCallbacks.size())
		return NULL;
	return m_vecCallbacks[i];
}


void VfaSystemUpdateHandler::RegisterLightTransform(IVistaTransformable *pLightTransform)
{
	m_pLightTransform = pLightTransform;
}

IVistaTransformable *VfaSystemUpdateHandler::GetLightTransform() const
{
	return m_pLightTransform;
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VfaSystemUpdateHandler.cpp"                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

