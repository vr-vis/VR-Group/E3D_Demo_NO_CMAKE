/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFASYSTEMUPDATEHANDLER_H
#define _VFASYSTEMUPDATEHANDLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaKernel/EventManager/VistaEventHandler.h>
#include <vector>

#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaEventManager;
class VistaDisplayManager;
class VflRenderNode;
class IVistaExplicitCallbackInterface;
class IVistaTransformable;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAUXAPI VfaSystemUpdateHandler : public VistaEventHandler
{
public:
	VfaSystemUpdateHandler(VistaEventManager *pEvMgr,
							VistaDisplayManager *pDispMgr,
							VflRenderNode *pRenderNode);
	virtual ~VfaSystemUpdateHandler();

    virtual void HandleEvent(VistaEvent *pEvent);

	int RegisterAction(IVistaExplicitCallbackInterface *);
	bool GetIsRegistered(IVistaExplicitCallbackInterface *) const;
	bool UnregisterAction(int i, bool bDelete);
	bool UnregisterAction(IVistaExplicitCallbackInterface *, bool bDelete);
	IVistaExplicitCallbackInterface *GetCallback(int i) const;

	void RegisterLightTransform(IVistaTransformable *);
	IVistaTransformable *GetLightTransform() const;
protected:
private:
	VflRenderNode  *m_pRenderNode;
	VistaEventManager *m_pEvMgr;
	VistaDisplayManager *m_pDispMgr;
	std::vector<IVistaExplicitCallbackInterface*> m_vecCallbacks;

	IVistaTransformable *m_pLightTransform;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_CLVSYSTEMUPDATEHANDLER_H
