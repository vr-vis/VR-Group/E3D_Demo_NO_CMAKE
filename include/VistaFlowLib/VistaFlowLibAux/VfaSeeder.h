/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifndef _IVFASEEDER_H
#define _IVFASEEDER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVfaPointGenerator;

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAUXAPI IVfaSeeder
{
public:
	
	/**
	 * The Seed function is called by the application and triggers the seeding.
	 * The seeding points are given by the IVfaPointGenerator.
	 * A concrete implementation can be found at
	 *  VflModules/VflGpuParticles/VflGpuParticleSeeder.cpp
	 *
	 * @see VistaFlowLibAux/VfaPointGenerator.h
	 */
	virtual void Seed( void ) = 0;

	void SetPointGenerator( IVfaPointGenerator * pPointGenerator );
	IVfaPointGenerator * GetPointGenerator();

	virtual ~IVfaSeeder(){};

protected:
	
	IVfaSeeder(){};

private:
	IVfaPointGenerator *m_pPointGenerator;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif /* _IVFASEEDER_H */
