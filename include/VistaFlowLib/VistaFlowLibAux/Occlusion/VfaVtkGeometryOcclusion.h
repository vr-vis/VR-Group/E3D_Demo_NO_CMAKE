/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVTKGEOMETRYOCCLUSION_H
#define _VFLVTKGEOMETRYOCCLUSION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaOcclusionManager.h"
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <vector>
#include <map>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVflRenderable;
class VflVisVtkGeometry;
class VfaUnsteadyDataFilterExtract;
class vtkOBBTree;
class vtkPolyData;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Implementation of a  IVflOcclusionTestStrategy for VTK geometry using a simple 
 * bounding box test. The view ray is tested with the geometries bounding box for collision.
 * Works for all renderables with a valid bounding box.
 */
class VISTAFLOWLIBAUXAPI VfaVtkGeometryOcclusionBoxTest : public IVfaOcclusionTestStrategy
{
public:
	VfaVtkGeometryOcclusionBoxTest();
	virtual ~VfaVtkGeometryOcclusionBoxTest();

	bool TestDirection(IVflRenderable*,float vViewPosition [3], float vViewDirection [3], float fVisTime);

};

/**
 * Implementation of a IVflOcclusionTestStrategy for VTK geometry using OBBs(oriented bounding boxes).
 * For the VTK geometry, a hierarchy of OBBs is computed and this collision tree is checked
 * against the view ray for collision. Much more accurate than the bounding box test.
 * Works for VflVisVtkGeometry only.
 */
class VISTAFLOWLIBAUXAPI VfaVtkGeometryOcclusionCollisionTest : public IVfaOcclusionTestStrategy
{
public:
	VfaVtkGeometryOcclusionCollisionTest();
	virtual ~VfaVtkGeometryOcclusionCollisionTest();

	bool TestDirection(IVflRenderable*,float vViewPosition [3], float vViewDirection [3], float fVisTime);

private:
	std::vector<vtkPolyData*> m_vecGeometries;
	std::vector<vtkOBBTree*> m_vecOBBTrees;
};
/**
 * Implementation of a IVflOcclusionManagementStrategy for VTK geometry, which just
 * increases transparency of the geometry on occlusion.
 * Works for VflVisVtkGeometry only.
 */
class VISTAFLOWLIBAUXAPI VfaVtkGeometryOcclusionTransparency : public IVfaOcclusionManagementStrategy
{
public:
	VfaVtkGeometryOcclusionTransparency();
	virtual ~VfaVtkGeometryOcclusionTransparency();

	bool OnOcclude(IVflRenderable*, float vViewPosition [3], float vViewDirection [3], float fVisTime);
	bool OnDeOcclude(IVflRenderable* pRenderable);
private:
	std::vector<VflVisVtkGeometry*> m_vecGeometries;
	std::vector<float> m_vecOpacities;
};

/**
 * Implementation of a IVflOcclusionManagementStrategy for VTK geometry, which uses
 * VflUnsteadyDataFilterExtract filters to cut out occluding parts.
 * Works for VflVisVtkGeometry only.
 */
class VISTAFLOWLIBAUXAPI VfaVtkGeometryOcclusionCutout : public IVfaOcclusionManagementStrategy
{
public:
	VfaVtkGeometryOcclusionCutout();
	virtual ~VfaVtkGeometryOcclusionCutout();

	bool OnOcclude(IVflRenderable*, float vViewPosition [3], float vViewDirection [3], float fVisTime);
	bool OnDeOcclude(IVflRenderable* pRenderable);
private:
	std::vector<VflVisVtkGeometry*> m_vecGeometries;
	std::vector<VfaUnsteadyDataFilterExtract*> m_vecExtractFilters;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFLVTKGEOMETRYOCCLUSION_H



