/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here
#include "VfaVtkGeometryOcclusion.h" 
#include "VfaUnsteadyDataFilterExtract.h" 

#include <VistaFlowLib/Visualization/VflVisVtkGeometry.h>
#include <VistaFlowLib/Visualization/IVflTransformer.h>

#include <VistaMath/VistaGeometries.h>
#include <VistaMath/VistaBoundingBox.h>

#include <VistaBase/VistaTimer.h>

#include <vtkProperty.h>
#include <vtkOBBTree.h>
#include <vtkPolyData.h>

#include <algorithm>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaVtkGeometryOcclusionBoxTest::VfaVtkGeometryOcclusionBoxTest()
:IVfaOcclusionTestStrategy()
{
}

VfaVtkGeometryOcclusionBoxTest::~VfaVtkGeometryOcclusionBoxTest()
{
}

/*============================================================================*/
/*																			  */
/*		TestDirection														  */
/*																			  */
/*============================================================================*/

bool VfaVtkGeometryOcclusionBoxTest::TestDirection(IVflRenderable* pRenderable,float vViewPosition [3], float vViewDirection [3], float fVisTime)
{
	
	VistaBoundingBox oBBox;
	
	pRenderable->GetBounds(oBBox);

	// naive test with boundig box... better would be a "real" geometry test with vtkPointPicker
	if(oBBox.Intersects(vViewPosition, vViewDirection, true))
	{
			return true;
	}
	else
	{
		return false;
	}
}

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaVtkGeometryOcclusionCollisionTest::VfaVtkGeometryOcclusionCollisionTest()
:IVfaOcclusionTestStrategy()
{
}

VfaVtkGeometryOcclusionCollisionTest::~VfaVtkGeometryOcclusionCollisionTest()
{
}

/*============================================================================*/
/*																			  */
/*		TestDirection														  */
/*																			  */
/*============================================================================*/

bool VfaVtkGeometryOcclusionCollisionTest::TestDirection(IVflRenderable* pRenderable,float vViewPosition [3], float vViewDirection [3], float fVisTime)
{
	VflVisVtkGeometry* pGeom = dynamic_cast<VflVisVtkGeometry*>(pRenderable);
	if(!pGeom)
		return false;

	int iLevel = pGeom->GetData()->GetTimeMapper()->GetLevelIndex(fVisTime);
	vtkPolyData* pPolyGeometry = pGeom->GetData()->GetTypedLevelDataByLevelIndex(iLevel)->GetData();

	// check if we have handled this geometry before
	std::vector<vtkPolyData*>::iterator itGeom = std::find(m_vecGeometries.begin(), m_vecGeometries.end(), pPolyGeometry);

	size_t iIdx = -1;
	if(itGeom == m_vecGeometries.end())
	{
		m_vecGeometries.push_back(pPolyGeometry);
		vtkOBBTree* pTree = vtkOBBTree::New();	
		
		VistaTimer oTimer;
		double dStart = oTimer.GetSystemTime();
		// which time level of geoemtry? where to get time information here?
		
		pTree->Initialize();
		pTree->SetDataSet(pPolyGeometry);
		pTree->AutomaticOn();
		pTree->BuildLocator();

		double dEnd = oTimer.GetSystemTime();
		vstr::outi() << "[VflVtkGeometryOcclusionCutout] Time to build OBB tree for occlusion detection "<<dEnd-dStart<<" sec.\n";
		m_vecOBBTrees.push_back(pTree);
		iIdx = m_vecGeometries.size()-1;
	}
	else
	 iIdx = itGeom - m_vecGeometries.begin();
	
	// if the geometry has a transformer, transform viewer into this coordinate system
	if(pGeom->GetTransformer())
	{
		// the transformer is unsteady, so get transformation for current time instant
		VistaTransformMatrix m;
		double dSimTime = pGeom->GetData()->GetTimeMapper()->GetSimulationTime(fVisTime);
		if(pGeom->GetTransformer()->GetUnsteadyTransform(dSimTime, m))
		{
			m = m.GetInverted();
			VistaVector3D vViewTransformed = m.Transform(VistaVector3D(vViewPosition));
			vViewTransformed.GetValues(vViewPosition);
			VistaVector3D vDirTransformed = m.Transform(VistaVector3D(vViewDirection));
			vDirTransformed.GetValues(vViewDirection);
		}
	}

	double   	tol = 0.01;
	double   	t;
	double  	a0[3] = {vViewPosition[0], vViewPosition[1], vViewPosition[2]};
	double  	a1[3] = {vViewPosition[0]+vViewDirection[0], vViewPosition[1]+vViewDirection[1], vViewPosition[2]+vViewDirection[2]};
	double  	x[3];
	double  	pcoords[3];
	int   		subId;
	vtkIdType  	cellId;
	
	int iReturn = m_vecOBBTrees[iIdx]->IntersectWithLine(a0, a1, tol, t, x, pcoords, subId, cellId);

	if(iReturn==0)
		return false;
	else
		// -1 inside, +1 outside
		return true;
}

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaVtkGeometryOcclusionTransparency::VfaVtkGeometryOcclusionTransparency()
:IVfaOcclusionManagementStrategy()
{
}

VfaVtkGeometryOcclusionTransparency::~VfaVtkGeometryOcclusionTransparency()
{
}

/*============================================================================*/
/*																			  */
/*		TestDirection														  */
/*																			  */
/*============================================================================*/
bool VfaVtkGeometryOcclusionTransparency::OnOcclude(IVflRenderable* pRenderable, float vViewPosition [3], float vViewDirection [3], float fVisTime)
{
	VflVisVtkGeometry* pGeom = dynamic_cast<VflVisVtkGeometry*>(pRenderable);
	if(!pGeom)
		return false;

	// check if we have handled this geometry before
	std::vector<VflVisVtkGeometry*>::iterator itGeom = std::find(m_vecGeometries.begin(), m_vecGeometries.end(), pGeom);
	
	vtkProperty *pGeomProps = pGeom->GetProperty();

	if(itGeom == m_vecGeometries.end())
	{
		m_vecGeometries.push_back(pGeom);

		float fOpacity = 1.0f;
		
		if(pGeomProps)
			fOpacity = pGeomProps->GetOpacity();

		m_vecOpacities.push_back(fOpacity);
	}

	//handle occlusion

	// changing opacity costs time when using display lists... options are using immediate mode
	// or using two geometries, one for occlusion, one no occlusion
	if(pGeomProps)
		pGeomProps->SetOpacity(0.25);
	return true;

}

bool VfaVtkGeometryOcclusionTransparency::OnDeOcclude(IVflRenderable* pRenderable)
{
	VflVisVtkGeometry* pGeom = dynamic_cast<VflVisVtkGeometry*>(pRenderable);
	if(!pGeom)
		return false;

	// check if we have handled this geometry before
	std::vector<VflVisVtkGeometry*>::iterator itGeom = std::find(m_vecGeometries.begin(), m_vecGeometries.end(), pGeom);

	vtkProperty *pGeomProps = pGeom->GetProperty();
	if(itGeom != m_vecGeometries.end())
	{
		int iPos = itGeom - m_vecGeometries.begin();

		if(pGeomProps)
			pGeomProps->SetOpacity(m_vecOpacities[iPos]);
	}

	return true;
}

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaVtkGeometryOcclusionCutout::VfaVtkGeometryOcclusionCutout()
:IVfaOcclusionManagementStrategy()
{
}

VfaVtkGeometryOcclusionCutout::~VfaVtkGeometryOcclusionCutout()
{
}

/*============================================================================*/
/*																			  */
/*		TestDirection														  */
/*																			  */
/*============================================================================*/
bool VfaVtkGeometryOcclusionCutout::OnOcclude(IVflRenderable* pRenderable, float vViewPosition [3], float vViewDirection [3], float fVisTime)
{
	VflVisVtkGeometry* pGeom = dynamic_cast<VflVisVtkGeometry*>(pRenderable);
	if(!pGeom)
		return false;

	// check if we have handled this geometry before
	std::vector<VflVisVtkGeometry*>::iterator itGeom = std::find(m_vecGeometries.begin(), m_vecGeometries.end(), pGeom);
	
	size_t iIdx = -1;
	if(itGeom == m_vecGeometries.end())
	{
		size_t iExtractFilterIdx = -1;
		for(size_t j=0; j < pGeom->GetFilterCount(); ++j)
		{
			if(dynamic_cast<VfaUnsteadyDataFilterExtract*>(pGeom->GetFilter(j)) != NULL)
			{
				iExtractFilterIdx = j;
			}
		}

		if(iExtractFilterIdx < 0)
		{
			vstr::errp() << "[VflVtkGeometryOcclusionCutout] Vis Object has no VflUnsteadyDataFilterExtract. Will not add this renderable \n";
			return false;
		}

		m_vecGeometries.push_back(pGeom);
		m_vecExtractFilters.push_back(dynamic_cast<VfaUnsteadyDataFilterExtract*>(pGeom->GetFilter(iExtractFilterIdx)));
		iIdx = m_vecGeometries.size()-1;
	}
	else
		iIdx = itGeom - m_vecGeometries.begin();


	//handle occlusion
	m_vecExtractFilters[iIdx]->SetActive(true);

	// if the geometry has a transformer, transform viewer into this coordinate system
	VistaTransformMatrix mConeTransform(VistaQuaternion(VistaVector3D(0,1,0), VistaVector3D(vViewDirection)), VistaVector3D(vViewPosition));
	mConeTransform = mConeTransform.GetInverted();

	float fMatrix[16];
	mConeTransform.GetValues(fMatrix);
	double dMatrix[16];
	for(int j=0; j < 16; ++j)
		dMatrix[j] = fMatrix[j];

	double dOrigin[3] = {vViewPosition[0]+vViewDirection[0], vViewPosition[1]+vViewDirection[1], vViewPosition[2]+vViewDirection[2]};
	double dNormal[3] = {vViewDirection[0], vViewDirection[1], vViewDirection[2]} ;
	m_vecExtractFilters[iIdx]->SetTransform(dOrigin, dNormal, dMatrix);
	
	return false;

}

bool VfaVtkGeometryOcclusionCutout::OnDeOcclude(IVflRenderable* pRenderable)
{
	VflVisVtkGeometry* pGeom = dynamic_cast<VflVisVtkGeometry*>(pRenderable);
	if(!pGeom)
		return false;

	// check if we have handled this geometry before
	std::vector<VflVisVtkGeometry*>::iterator itGeom = std::find(m_vecGeometries.begin(), m_vecGeometries.end(), pGeom);
	
	if(itGeom != m_vecGeometries.end())
	{
		// get index of geometry and deactivate corresponding extract filter
		int iIdx = itGeom - m_vecGeometries.begin();
		m_vecExtractFilters[iIdx]->SetActive(false);

	}


	return true;
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "          "                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

