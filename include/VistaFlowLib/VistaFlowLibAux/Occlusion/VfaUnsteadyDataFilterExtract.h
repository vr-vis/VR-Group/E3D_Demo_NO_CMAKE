/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAUNSTEADYDATAFILTEREXTRACT_H
#define _VFAUNSTEADYDATAFILTEREXTRACT_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflUnsteadyDataFilter.h>
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkExtractPolyDataGeometry;
class vtkCone;
class vtkCylinder;
class vtkPlane;
class vtkImplicitBoolean;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/*
* This unsteady data filter for VTK geometry cuts parts out of the geometry.
* This may be used, e.g., for occlusion management
*
*/
class VISTAFLOWLIBAUXAPI VfaUnsteadyDataFilterExtract : public VflUnsteadyDataFilter
{
public:
	VfaUnsteadyDataFilterExtract(vtkExtractPolyDataGeometry *pPrototype = NULL);
	virtual ~VfaUnsteadyDataFilterExtract();

	// filter and pipeline management
	virtual void CreateFilters(int iCount);
	virtual void SetInput(int iIndex, vtkPolyData *pData);
	virtual vtkPolyData *GetInput(int iIndex) const;
	virtual vtkPolyData *GetOutput(int iIndex);
	virtual int GetFilterCount() const;
	virtual void Synchronize();

	// dealing with the prototype
	vtkExtractPolyDataGeometry *GetPrototype() const;
	static void CopyProperties(vtkExtractPolyDataGeometry *pTarget,
		vtkExtractPolyDataGeometry *pSource);

	/*
	* The VTK geometry, is cutted with an implicit function, which is
	* defined by the union of a cylinder and a plane to restrict the length
	* of the cylinder.
	* Radius is of the cylinder, mTrans describes the transformation of the cylinder,
	* dOrigin the origin of plane, and dNormal its normal vector.
	*/
	void SetRadius(double dRadius);
	void SetTransform(double dOrigin[3], double dNormal[3], double mTrans [16]);

	struct sImplicitFunctions {
		vtkCylinder * pCylinder;
		vtkPlane*	  pPlane;
		vtkImplicitBoolean* pUnion;
	};

	REFL_INLINEIMP(VfaUnsteadyDataFilterExtract,VflUnsteadyDataFilter)

private:


	std::vector<vtkExtractPolyDataGeometry *>	m_vecFilters;
	//std::vector<vtkCone *>			m_vecImplicitFunctions;

	std::vector<sImplicitFunctions>			m_vecImplicitFunctions;
	vtkExtractPolyDataGeometry  				*m_pPrototype;
};

#endif // _VFAUNSTEADYDATAFILTEREXTRACT_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

