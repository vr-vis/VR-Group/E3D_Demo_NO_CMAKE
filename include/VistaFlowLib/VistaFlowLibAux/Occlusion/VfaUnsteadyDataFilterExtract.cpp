/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VfaUnsteadyDataFilterExtract.h"

#include <VistaAspects/VistaAspectsUtils.h>

#include <vtkExtractPolyDataGeometry.h>
#include <vtkCone.h>
#include <vtkCylinder.h>
#include <vtkPlane.h>
#include <vtkImplicitBoolean.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

static void DestroyFilters(std::vector<vtkExtractPolyDataGeometry *> &refFilters);
static void DestroyImplicitFunctions(std::vector<VfaUnsteadyDataFilterExtract::sImplicitFunctions> &refFunctions);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VfaUnsteadyDataFilterExtract::VfaUnsteadyDataFilterExtract(vtkExtractPolyDataGeometry *pPrototype)
: VflUnsteadyDataFilter(),
  m_pPrototype(NULL)
{
	m_pPrototype = vtkExtractPolyDataGeometry::New();
	m_pPrototype->GlobalWarningDisplayOff();

	if(pPrototype)
		CopyProperties(m_pPrototype, pPrototype);
}

VfaUnsteadyDataFilterExtract::~VfaUnsteadyDataFilterExtract()
{
	DestroyFilters(m_vecFilters);
	DestroyImplicitFunctions(m_vecImplicitFunctions);
	m_pPrototype->Delete();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

// quick and dirty
void VfaUnsteadyDataFilterExtract::SetRadius(double dRadius)
{
	for(unsigned int i=0; i<m_vecImplicitFunctions.size(); ++i)
	{
		m_vecImplicitFunctions[i].pCylinder->SetRadius( dRadius);
	}
}

void VfaUnsteadyDataFilterExtract::SetTransform(double dOrigin[3], double dNormal[3], double mTrans [16])
{
	// set transformations
	for(unsigned int i=0; i<m_vecImplicitFunctions.size(); ++i)
	{
		m_vecImplicitFunctions[i].pCylinder->SetTransform(mTrans);
		m_vecImplicitFunctions[i].pPlane->SetOrigin(dOrigin);
		m_vecImplicitFunctions[i].pPlane->SetNormal(dNormal);
	}
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateFilters                                               */
/*                                                                            */
/*============================================================================*/
void VfaUnsteadyDataFilterExtract::CreateFilters(int iCount)
{
	DestroyFilters(m_vecFilters);

	m_vecFilters.resize(iCount);
	m_vecImplicitFunctions.resize(iCount);
	for(int i=0; i<iCount; ++i)
	{
		// create a union of plane and cylinder
		m_vecFilters[i] = vtkExtractPolyDataGeometry::New();
		m_vecImplicitFunctions[i].pCylinder = vtkCylinder::New();
		m_vecImplicitFunctions[i].pPlane = vtkPlane::New();
		m_vecImplicitFunctions[i].pUnion = vtkImplicitBoolean::New();
		m_vecImplicitFunctions[i].pUnion->AddFunction(m_vecImplicitFunctions[i].pCylinder);
		m_vecImplicitFunctions[i].pUnion->AddFunction(m_vecImplicitFunctions[i].pPlane);
		m_vecImplicitFunctions[i].pUnion->SetOperationTypeToIntersection();

		// give this union as input into ExtractVtkPolyData for cutting
		m_vecFilters[i]->SetImplicitFunction(m_vecImplicitFunctions[i].pUnion);
		m_vecFilters[i]->ExtractInsideOff();
	}

	Synchronize();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetInput                                                    */
/*                                                                            */
/*============================================================================*/
void VfaUnsteadyDataFilterExtract::SetInput(int iIndex, vtkPolyData *pData)
{
	if(0<=iIndex && iIndex<int(m_vecFilters.size()))
	{
#if VTK_MAJOR_VERSION > 5
		m_vecFilters[iIndex]->SetInputData(pData);
#else
		m_vecFilters[iIndex]->SetInput(pData);
#endif
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetInput                                                    */
/*                                                                            */
/*============================================================================*/
vtkPolyData *VfaUnsteadyDataFilterExtract::GetInput(int iIndex) const
{
	if(iIndex<0 || iIndex>=int(m_vecFilters.size()))
		return NULL;

	return m_vecFilters[iIndex]->GetPolyDataInput(0);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetOutput                                                   */
/*                                                                            */
/*============================================================================*/
vtkPolyData *VfaUnsteadyDataFilterExtract::GetOutput(int iIndex)
{
	if(iIndex<0 || iIndex>=int(m_vecFilters.size()))
		return NULL;
	
	if(m_bActive)
		return m_vecFilters[iIndex]->GetOutput();
	else
		return m_vecFilters[iIndex]->GetPolyDataInput(0);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilterCount                                              */
/*                                                                            */
/*============================================================================*/
int VfaUnsteadyDataFilterExtract::GetFilterCount() const
{
	return static_cast<int>(m_vecFilters.size());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Synchronize                                                 */
/*                                                                            */
/*============================================================================*/
void VfaUnsteadyDataFilterExtract::Synchronize()
{
	for(unsigned int i=0; i<m_vecFilters.size(); ++i)
	{
		CopyProperties(m_vecFilters[i], m_pPrototype);
	}
	Notify(MSG_PARAMETER_CHANGE);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPrototype                                                */
/*                                                                            */
/*============================================================================*/
vtkExtractPolyDataGeometry *VfaUnsteadyDataFilterExtract::GetPrototype() const
{
	return m_pPrototype;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CopyProperties                                              */
/*                                                                            */
/*============================================================================*/
void VfaUnsteadyDataFilterExtract::CopyProperties(vtkExtractPolyDataGeometry *pTarget,
												   vtkExtractPolyDataGeometry *pSource)
{
	//pTarget->SetFeatureAngle( pSource->GetFeatureAngle() );
	//pTarget->SetSplitting( pSource->GetSplitting() );
	//pTarget->SetConsistency( pSource->GetConsistency() );
	//pTarget->SetFlipNormals( pSource->GetFlipNormals() );
	//pTarget->SetNonManifoldTraversal( pSource->GetNonManifoldTraversal() );
	//pTarget->SetComputePointNormals( pSource->GetComputePointNormals() );
	//pTarget->SetComputeCellNormals( pSource->GetComputeCellNormals() );
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DestroyFilters                                              */
/*                                                                            */
/*============================================================================*/

void DestroyFilters(vector<vtkExtractPolyDataGeometry *> &vecFilters)
{
	for(unsigned int i=0; i<vecFilters.size(); ++i)
	{
		vecFilters[i]->Delete();
		vecFilters[i] = NULL;
	}
	vecFilters.clear();
}
void DestroyImplicitFunctions(std::vector<VfaUnsteadyDataFilterExtract::sImplicitFunctions> &refFunctions)
{
	for(unsigned int i=0; i<refFunctions.size(); ++i)
	{
		refFunctions[i].pCylinder->Delete();
		refFunctions[i].pPlane->Delete();
		refFunctions[i].pUnion->Delete();
		
	}
	refFunctions.clear();
}

/*============================================================================*/
/*  END OF FILE "VflUnsteadyDataFilter.cpp"                                   */
/*============================================================================*/
