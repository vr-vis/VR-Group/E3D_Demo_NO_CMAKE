/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFAOCCLUSIONMANAGER_H
#define _VFAOCCLUSIONMANAGER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLibAux/VistaFlowLibAuxConfig.h>
#include <VistaBase/VistaTimer.h>
#include <vector>
#include <map>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVflRenderable;
class VflRenderNode;
class VfaApplicationContextObject;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * base class for occlusion tests, i.e. test if IVflRenderable occludes the ray given by
 * vViewPosition into direction vViewDirection(and length given by the direction vector)
 * at a specific point in time(for moving geometry)
 */
class VISTAFLOWLIBAUXAPI IVfaOcclusionTestStrategy
{
public:
	virtual ~IVfaOcclusionTestStrategy();

	virtual bool TestDirection(IVflRenderable*, float vViewPosition [3], float vViewDirection [3], float fVisTime) = 0;

protected:
	IVfaOcclusionTestStrategy();
};

/**
 * base class for anti-occlusion strategies, i.e. how to react on occlusion with IVflRenderable as occluder.
 */
class VISTAFLOWLIBAUXAPI IVfaOcclusionManagementStrategy
{
public:
	virtual ~IVfaOcclusionManagementStrategy();

	virtual bool OnOcclude(IVflRenderable*,float vViewPosition [3], float vViewDirection [3], float fVisTime) = 0;
	virtual bool OnDeOcclude(IVflRenderable*) = 0;
protected:
	IVfaOcclusionManagementStrategy();
};

/**
 * VflOcclusionManager handles occlusion tests and application of occlusion strategies for registered objects.
 *
 *
 */
class VISTAFLOWLIBAUXAPI VfaOcclusionManager
{
public:
	VfaOcclusionManager(VfaApplicationContextObject* pAppContext,
		VflRenderNode* pRenderNode);

	virtual ~VfaOcclusionManager();

	// a "think" step, tests for occlusion and applies strategies
	void Update();

	// Register a renderable object for occlusion testing.
	// Each object is associated with a test and occlusion strategy suitable for this kind of
	// object(e.g., VtkGeometry).
	bool RegisterObject(IVflRenderable*, IVfaOcclusionTestStrategy*, IVfaOcclusionManagementStrategy* );
	bool UnregisterObject(IVflRenderable* );

	// Set an active focus point for which occlusion should be avoided
	bool SetFocusPoint(float*v3Point);

	// Unset the active focus point, i.e., we currently have no focus
	bool SetNoFocusPoint();

	bool GetHasFocus() const;

	// Set minimum time between two occlusion tests.
	// If Update is called within an interval smaller than dMin, it returns without testing(for efficiency).
	void SetMinUpdateDistance(double dMin);

	// Set UseActivePointer, if you would like to use the application context's pointer position and orientation
	// instead of the viewer position and orientation(using the pointer as some kind of "occlusion flashlight").
	void SetUseActivePointer(bool bUsePointer);
	bool GetUseActivePointer() const;

private:
	VflRenderNode*			m_pRenderNode;
	VfaApplicationContextObject* m_pAppContext;

	bool					m_bUseActivePointer;

	float					m_v3FocusPoint[3];
	bool					m_bNoFocus;
	bool					m_bFocusHasChanged;

	float					m_v3Viewer[3];

	VistaTimer				m_oTimer;
	double					m_dLastUpdate;
	double					m_dMinUpdateDifference;

	struct sObjectData{
		IVflRenderable* pObject;
		IVfaOcclusionTestStrategy* pTestStrategy;
		IVfaOcclusionManagementStrategy* pManagementStrategy;
		bool			bWasOccluding;
	};

	std::map<int, sObjectData> m_mapObjectData;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFLOOCLUSIONMANAGER_H



