/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here
#include "VfaOcclusionManager.h" 

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLib/Visualization/VflVisVtkGeometry.h>

#include <VistaFlowLibAux/Interaction/VfaApplicationContextObject.h>

#include <VistaMath/VistaGeometries.h>
#include <VistaMath/VistaBoundingBox.h>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

IVfaOcclusionTestStrategy::IVfaOcclusionTestStrategy()
{
}
IVfaOcclusionTestStrategy::~IVfaOcclusionTestStrategy()
{
}

IVfaOcclusionManagementStrategy::IVfaOcclusionManagementStrategy()
{
}

IVfaOcclusionManagementStrategy::~IVfaOcclusionManagementStrategy()
{
}

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

static const float DISTANCE_EPSILON = 0.1f;

VfaOcclusionManager::VfaOcclusionManager(
	VfaApplicationContextObject* pAppContext, VflRenderNode* pRenderNode)
:	m_pRenderNode(pRenderNode),
	m_pAppContext(pAppContext),
	m_bNoFocus(true),
	m_bFocusHasChanged(true),
	m_dLastUpdate(0.0),
	m_dMinUpdateDifference(0.0),
	m_bUseActivePointer(false)
{
 
}

VfaOcclusionManager::~VfaOcclusionManager()
{
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*																			  */
/*		Update																  */
/*																			  */
/*============================================================================*/

void VfaOcclusionManager::Update()
{
	VistaVector3D v3Viewer;

	if(m_bUseActivePointer)
	{
		v3Viewer = m_pAppContext->GetSensorFrame(
			VfaApplicationContextObject::SLOT_POINTER_VIS,
			m_pRenderNode).v3Position;
	}
	else
		m_pRenderNode->GetLocalViewPosition(v3Viewer);


	if(!m_bUseActivePointer && !m_bFocusHasChanged &&((v3Viewer-VistaVector3D(m_v3Viewer)).GetLength()<DISTANCE_EPSILON))
		return;

	// check update frequency
	double dCurrentTime = m_oTimer.GetSystemTime();
	if(dCurrentTime-m_dLastUpdate <= m_dMinUpdateDifference)
		return;
	
	m_dLastUpdate = dCurrentTime;

	// set new values
	v3Viewer.GetValues(m_v3Viewer);
	float v3Direction [3];
	if(m_bUseActivePointer)
	{
		VistaQuaternion qPointerOri = m_pAppContext->GetSensorFrame(
			VfaApplicationContextObject::SLOT_POINTER_VIS, m_pRenderNode).qOrientation;
		VistaVector3D vecDir = qPointerOri.Rotate(VistaVector3D(0,0,-1));
		vecDir.Normalize();
		vecDir *= 10.0f;
		//qPointerOri.Rotate(VistaVector3D(0,0,-1)).GetValues(v3Direction);
		vecDir.GetValues(v3Direction);
	}
	else
	{
		v3Direction[0] = m_v3FocusPoint[0]-m_v3Viewer[0];
		v3Direction[1] = m_v3FocusPoint[1]-m_v3Viewer[1];
		v3Direction[2] = m_v3FocusPoint[2]-m_v3Viewer[2];
	}

	// test for each object
	std::map<int, sObjectData>::iterator itObject = m_mapObjectData.begin();
	while(itObject != m_mapObjectData.end())
	{
		bool bOccludes = false;
		
		float fVisTime = static_cast<float>(
			m_pRenderNode->GetVisTiming()->GetVisualizationTime());
		// if we have a valid focus or we use the pointer instead of viewer, test
		if(m_bUseActivePointer ||(!m_bNoFocus))
		{
			//double dTestStart = m_oTimer.GetSystemTime();
			bOccludes = itObject->second.pTestStrategy->TestDirection(itObject->second.pObject, m_v3Viewer, v3Direction, fVisTime);
			//double dTestEnd = m_oTimer.GetSystemTime();
			//vstr::outi() << "Testing time "<<dTestEnd-dTestStart<<" sec\n";
		}

		if(bOccludes)
		{
			if(!itObject->second.bWasOccluding)
			{
				//vstr::outi() << "Object occludes\n";
				itObject->second.bWasOccluding = true;
			}
			//double dTestStart = m_oTimer.GetSystemTime();
			itObject->second.pManagementStrategy->OnOcclude(itObject->second.pObject, m_v3Viewer, v3Direction, fVisTime);
			//double dTestEnd = m_oTimer.GetSystemTime();
			//vstr::outi() << "Occlude time "<<dTestEnd-dTestStart<<" sec\n";
		}

		if((!bOccludes) && itObject->second.bWasOccluding)
		{
			//vstr::outi() << "Object does not occlude any more\n";
			itObject->second.pManagementStrategy->OnDeOcclude(itObject->second.pObject);
			itObject->second.bWasOccluding = false;
		}

		itObject++;
	}

	// reset focus flag
	m_bFocusHasChanged = false;

}

/*============================================================================*/
/*																			  */
/*		SetFocusPoint														  */
/*																			  */
/*============================================================================*/
bool VfaOcclusionManager::SetFocusPoint(float*v3Point)
{
	memcpy(m_v3FocusPoint, v3Point, 3*sizeof(float));
	m_bNoFocus = false;
	m_bFocusHasChanged = true;
	return true;
}

bool VfaOcclusionManager::SetNoFocusPoint()
{
	m_bNoFocus = true;
	m_bFocusHasChanged = true;
	return true;
}

bool  VfaOcclusionManager::GetHasFocus() const
{
	return !m_bNoFocus;
}

/*============================================================================*/
/*																			  */
/*		SetMaxUpdateDistance												  */
/*																			  */
/*============================================================================*/
void VfaOcclusionManager::SetMinUpdateDistance(double dMax)
{
	m_dMinUpdateDifference = dMax;
}

/*============================================================================*/
/*																			  */
/*		SetUseActivePointer													  */
/*																			  */
/*============================================================================*/

void VfaOcclusionManager::SetUseActivePointer(bool bUsePointer)
{
	m_bUseActivePointer = bUsePointer;
}

bool VfaOcclusionManager::GetUseActivePointer() const
{
	return m_bUseActivePointer;
}


/*============================================================================*/
/*																			  */
/*		RegisterObject														  */
/*																			  */
/*============================================================================*/
bool VfaOcclusionManager::RegisterObject(IVflRenderable* pObject, IVfaOcclusionTestStrategy* pTest, IVfaOcclusionManagementStrategy* pManagement)
{
	// check for existance
	
	sObjectData oData;
	oData.pObject = pObject;
	oData.pTestStrategy = pTest;
	oData.pManagementStrategy = pManagement;
	oData.bWasOccluding = false;

	const int iID = static_cast<int>(m_mapObjectData.size());
	m_mapObjectData.insert(std::map<int, sObjectData>::value_type(iID, oData));

	// do a test once, as some tests have to build up a search structure once 
	float fTmp[3] = {0.0f,0.0f,0.0f};
	pTest->TestDirection(pObject, fTmp, fTmp, 0.0);

	return true;
}
/*============================================================================*/
/*																			  */
/*		UnregisterObject													  */
/*																			  */
/*============================================================================*/
bool VfaOcclusionManager::UnregisterObject(IVflRenderable* pObject)
{
	// TODO: delete in map

	return true;
}


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "          "                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

