/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VfaSystemByContextUpdater.h" 
#include "Interaction/VfaApplicationContextObject.h" 

#include <VistaFlowLib/Visualization/VflVisController.h>
#include <VistaAspects/VistaExplicitCallbackInterface.h>

#include <VistaAspects/VistaTransformable.h>


/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaSystemByContextUpdater::VfaSystemByContextUpdater(
	VfaApplicationContextObject *pApplContext, VflVisController *pVisCtrl)
: VflObserver(),
  m_pVisCtrl(pVisCtrl),
  m_pApplContext(pApplContext),
  m_pLightTransform(NULL)
{
	this->Observe(m_pApplContext);
}

VfaSystemByContextUpdater::~VfaSystemByContextUpdater()
{
	this->ReleaseObserveable (m_pApplContext);
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VfaSystemByContextUpdater::ObserverUpdate(
	IVistaObserveable *pObserveable, int msg, int ticket)
{
	if(pObserveable!=m_pApplContext)
		return;

	if(msg == VfaApplicationContextObject::MSG_TIMECHANGE)
	{
		// update light transform
		if(m_pLightTransform)
		{
			float mt[16];

			m_pLightTransform->GetWorldTransform(mt);

			VistaTransformMatrix m(mt);
			VistaQuaternion q(m);

			// MW: check this!
			float qxyz[4];
			m_pLightTransform->GetRotation(qxyz);
			VistaQuaternion qLocalLightDirection(qxyz);

			//rotate local light direction around global transform
			q = qLocalLightDirection*q;
			q.Normalize();
			m_pVisCtrl->TellGlobalLightDirection(q.Rotate(VistaVector3D(0,0,-1)));
		}

		// NOTE: VisCtrl directly determines the timestamp itself.
		m_pVisCtrl->Update(m_pApplContext->GetTime());
		for(unsigned int i=0; i<m_vecCallbacks.size(); ++i)
			if(m_vecCallbacks[i])
				(m_vecCallbacks[i])->Do();

	}

	if(msg == VfaApplicationContextObject::MSG_SENSORCHANGE)
	{
		if(m_pApplContext->GetChangedSensorSlot() 
			== VfaApplicationContextObject::SLOT_VIEWER_WORLD)
		{
			// set pos and ori of viewer in world coordinates
			VfaApplicationContextObject::sSensorFrame oFrame; 
			m_pApplContext->GetSensorFrame(
				VfaApplicationContextObject::SLOT_VIEWER_WORLD, 0, oFrame);

			m_pVisCtrl->TellGlobalViewPosition(oFrame.v3Position);
			m_pVisCtrl->TellGlobalViewOrientation(oFrame.qOrientation);
		}
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   RegisterAction                                              */
/*                                                                            */
/*============================================================================*/
int VfaSystemByContextUpdater::RegisterAction(IVistaExplicitCallbackInterface *pAction)
{
	m_vecCallbacks.push_back(pAction);
	return static_cast<int>(m_vecCallbacks.size()-1);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIsRegistered                                             */
/*                                                                            */
/*============================================================================*/
bool VfaSystemByContextUpdater::GetIsRegistered(
	IVistaExplicitCallbackInterface *pAction) const
{
	for(size_t i=0; i<m_vecCallbacks.size(); ++i)
		if(m_vecCallbacks[i] == pAction)
			return true;
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   UnregisterAction                                            */
/*                                                                            */
/*============================================================================*/
bool VfaSystemByContextUpdater::UnregisterAction(int iPos, bool bDelete)
{
	if(iPos < 0 || static_cast<size_t>(iPos) >= m_vecCallbacks.size())
		return false;
	if(bDelete)
		delete m_vecCallbacks[iPos];
	m_vecCallbacks[iPos] = NULL;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   UnregisterAction                                            */
/*                                                                            */
/*============================================================================*/
bool VfaSystemByContextUpdater::UnregisterAction(
	IVistaExplicitCallbackInterface *pAction, bool bDelete)
{
	for(size_t i=0; i<m_vecCallbacks.size(); ++i)
	{
		if(m_vecCallbacks[i] == pAction)
		{
			if(bDelete)
				delete m_vecCallbacks[i];
			m_vecCallbacks[i] = NULL;
			return true;
		}
	}
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCallback                                                 */
/*                                                                            */
/*============================================================================*/
IVistaExplicitCallbackInterface *VfaSystemByContextUpdater::GetCallback(int i) const
{
	if(i < 0 || static_cast<size_t>(i) >= m_vecCallbacks.size())
		return NULL;
	return m_vecCallbacks[i];
}


void VfaSystemByContextUpdater::SetLightTransform(IVistaTransformable *pLightTransform)
{
	m_pLightTransform = pLightTransform;
}

IVistaTransformable *VfaSystemByContextUpdater::GetLightTransform() const
{
	return m_pLightTransform;
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

