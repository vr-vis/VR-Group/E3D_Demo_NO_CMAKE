/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VfaVistaSystemAbstraction.h" 

#include <cassert>

#include <VistaBase/VistaVersion.h>

#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/Cluster/VistaClusterMode.h>

#include <VistaKernel/GraphicsManager/VistaGraphicsManager.h>
#include <VistaKernel/DisplayManager/Vista2DDrawingObjects.h>
#include <VistaKernel/GraphicsManager/VistaSceneGraph.h>
#include <VistaKernel/GraphicsManager/VistaOpenGLNode.h>
#include <VistaKernel/DisplayManager/VistaDisplayManager.h>


#include <VistaAspects/VistaExplicitCallbackInterface.h>

#include <VistaFlowLib/Visualization/VflVisController.h>

#include <VistaKernel/GraphicsManager/VistaTransformNode.h>
#include <VistaKernel/GraphicsManager/VistaOpenGLNode.h>


/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VfaVistaSystemAbstraction::VfaVistaSystemAbstraction(VistaSystem *pSystem)
: IVflSystemAbstraction(),
  m_pSystem(pSystem)
{
	assert(m_pSystem != NULL);

	this->SetSystemAbs(this);
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

 double VfaVistaSystemAbstraction::GetFrameClock() const
 {
	 return m_pSystem->GetFrameClock();
 }

 float  VfaVistaSystemAbstraction::GenerateRandomNumber(int nLowerBound, int nUpperBound) const
 {
	 return float(m_pSystem->GenerateRandomNumber(nLowerBound, nUpperBound));
 }


 IVflSystemAbstraction::IVflOverlayText *VfaVistaSystemAbstraction::AddOverlayText(const std::string &strFontHint, 
																					const std::string strViewportName)
 {
	 
	 VistaDisplayManager *dm = m_pSystem->GetDisplayManager();
	 Vista2DText *pText = dm->New2DText(strViewportName);
	 if(!pText)
		 return NULL;

	 Vista2DText::Vista2DTextFontFamily fm = Vista2DText::TYPEWRITER;
	 if(strFontHint == "SANS")
		 fm = Vista2DText::SANS;
	 else if(strFontHint == "SERIF")
		 fm = Vista2DText::SERIF;


	 pText->Init("", 0.0f, 0.0f, 255,255,0, 20, fm);
	 
	 return new VfaVista2DTextAdapter(pText, dm);
 }

 bool VfaVistaSystemAbstraction::DeleteOverlayText(IVflOverlayText *pText)
 {
	 delete pText;
	 return true;
 }

 bool VfaVistaSystemAbstraction::IsLeader() const
 {
	 return m_pSystem->GetIsClusterLeader();
 }

  bool VfaVistaSystemAbstraction::IsFollower() const
 {
	 return m_pSystem->GetIsClusterFollower();
 }

 //bool VfaVistaSystemAbstraction::IsServer() const
 //{
	// return m_pSystem->IsServer();
 //}

 //bool VfaVistaSystemAbstraction::IsClient() const
 //{
	// return m_pSystem->IsClient();
 //}

 //bool VfaVistaSystemAbstraction::IsStandalone() const
 //{
	// return m_pSystem->IsStandalone();
 //}

 //bool VfaVistaSystemAbstraction::IsSlave() const
 //{
	// return m_pSystem->GetIsSlave();
 //}

 //bool VfaVistaSystemAbstraction::IsMaster() const
 //{
	// return m_pSystem->GetIsMaster();
 //}

 std::string VfaVistaSystemAbstraction::GetClientName() const
 {
	 return m_pSystem->GetClusterMode()->GetNodeName();
 }


VfaVistaSystemAbstraction::VfaVista2DTextAdapter::~VfaVista2DTextAdapter()
{
	// @todo
	// get rid of 2d overlay!
}

 bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::GetPosition(float &x, float &y) const
 {
	 return m_pVistaText->GetPosition(x,y);
 }

 bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::SetPosition(float x, float y)
 {
	 return m_pVistaText->SetPosition(x,y);
 }

 bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::GetText(std::string &sText) const
 {
	 sText = m_pVistaText->GetText();
	 return true;
 }

 bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::SetText(const std::string &sText)
 {
	 return m_pVistaText->SetText(sText);
 }

 bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::GetSize(float &fTextSize) const
 {
	 m_pVistaText->GetSize(fTextSize);
	 return true;
 }

 bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::SetSize(float fTextSize)
 {
	 return m_pVistaText->SetSize(fTextSize);
 }


 bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::SetEnabled(bool bEnableState)
 {
	 m_pVistaText->SetEnabled(bEnableState);
	 return(m_pVistaText->GetEnabled() == bEnableState);
 }

 bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::GetEnabled() const
 {
	 return m_pVistaText->GetEnabled();
 }

bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::GetColor(float &r, float &g, float &b) const
{
	unsigned char cr, cg, cb;
	if(m_pVistaText->GetColor(cr,cg,cb))
	{
		r = float(cr)/255.0f;
		g = float(cg)/255.0f;
		b = float(cb)/255.0f;
		return true;
	}
	return false;
}

bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::SetColor(float r, float g, float b)
{
	return m_pVistaText->SetColor(char(r*255.0f), char(g*255.0f), char(b*255.0f));
}

bool VfaVistaSystemAbstraction::VfaVista2DTextAdapter::Update()
{
	return true;
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "MYDEMO.CPP"                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/


