/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflAsyncNotificationQueue.h"

#include <VistaInterProcComm/Concurrency/VistaSemaphore.h>
#include <VistaAspects/VistaObserveable.h>
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflAsyncNotificationQueue::VflAsyncNotificationQueue()
: m_pLock(new VistaSemaphore), m_bHasData(false)
{
}

VflAsyncNotificationQueue::~VflAsyncNotificationQueue()
{
	delete m_pLock;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   PushElement                                                 */
/*                                                                            */
/*============================================================================*/
void VflAsyncNotificationQueue::PushNotification(
	IVistaObserveable *pTarget, int iMsg)
{
	VistaSemaphoreLock oLock(*m_pLock);
	m_quData.push(SQueueEntry(pTarget, iMsg));
	m_bHasData = true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ForwardResults                                              */
/*                                                                            */
/*============================================================================*/
void VflAsyncNotificationQueue::ForwardNotifications()
{
	VistaSemaphoreLock oLock(*m_pLock);

	while(!m_quData.empty())
	{
		SQueueEntry oEntry(m_quData.front());
		m_quData.pop();

		oEntry.m_pTarget->Notify(oEntry.m_iMsg);
	}
	m_bHasData = false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetHasData                                                  */
/*                                                                            */
/*============================================================================*/
bool VflAsyncNotificationQueue::GetHasNotifications()
{
	return m_bHasData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SQueueEntry::SQueueEntry                                    */
/*                                                                            */
/*============================================================================*/
VflAsyncNotificationQueue::SQueueEntry::SQueueEntry(
	IVistaObserveable *pTarget, int iMsg)
	: m_pTarget(pTarget), m_iMsg(iMsg)
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLookAheadCount                                       */
/*                                                                            */
/*============================================================================*/
VflAsyncNotificationQueue::SQueueEntry::~SQueueEntry()
{
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
