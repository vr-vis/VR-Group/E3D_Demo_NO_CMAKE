/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLUNSTEADYCARTESIANGRIDPUSHER_H
#define _VFLUNSTEADYCARTESIANGRIDPUSHER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <vector>
#include <list>
#include <string>

#include "VflUnsteadyTexturePusher.h"

#include <VistaFlowLib/VistaFlowLibConfig.h>
#include <VistaVisExt/Data/VveCartesianGrid.h>

/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 * This class provides data synchronization between texture data and 
 * flow data on a Cartesian grid in main memory.
 */
class VISTAFLOWLIBAPI VflUnsteadyCartesianGridPusher : public VflUnsteadyTexturePusher
{
public:
	VflUnsteadyCartesianGridPusher(VveUnsteadyCartesianGrid *pUCGrid,int iTexturePrecision=TP_FLOAT);
	virtual ~VflUnsteadyCartesianGridPusher();

	/**
	 * Data retrieval.
	 */
	VveUnsteadyCartesianGrid *GetUnsteadyCartesianGrid() const;

	/**
	 * Retrieve the texture storing the current time step.
	 * Use this one, if the animation is *not* playing!
	 */
	VistaTexture *GetTexture(int iLookAhead = 0) const;

	/**
	 * Retrieve data for interpolating vector data over time, starting
	 * at the *last* visualization time with offsets given in 
	 * visualization time as well.
	 * Use this one, if the animation *is* playing.
	 * NOTE: All given vectors are supposed to be empty!
	 * NOTE: As soon as an offset is encountered, which cannot be
	 *       served by the currently stored texture data, 
	 *       this method returns.
	 *
	 * @param[in]  vecOffsets   : time offsets
	 * @param[out] vecTextures  : the textures containing the data,
	 *                        2 for every valid time offset
	 * @param[out] vecWeights   : interpolation weights alpha(i)
	 *                        for every valid time offset
	 * @param[out] return value : number n of valid time offsets,
	 *                        n <= vecOffsets.size().
	 *
	 * So, for vecOffsets=[0, t/2, t, 200*t, t] this method returns the 
	 * following information:
	 * return value r = 3 (200*t cannot be served as the number of 
	 *                     look-aheads is too small, the remaining offsets
	 *                     are ignored)
	 * vecTextures = [texture(0), texture(1), .., texture(2*(r-1))]
	 * vecWeights = [alpha(0), alpha(1), .., alpha(r-1)],
	 * which means that for interpolating at time 
	 *   fTime = m_fLastVisTime+vecOffsets[i] 
	 * textures
	 *   texture(2*i) and texture(2*i+1)
	 * with the interpolation weights
	 *   (1-alpha(i)) and alpha(i)
	 * are to be used, i.e.
	 *   data(fTime) = (1-alpha(i)) * texture(2*i)
	 *                  + alpha(i) * texture(2*i+1);
	 *
	 * Note, that vecOffsets[4]==t is not considered anymore!
	 *
	 * *phew* Now, try saying that three times with eyes closed 
	 * and standing on your left leg...
	 */
	int GetTexturesAndWeights(const std::vector<double> &vecOffsets,
		std::vector<VistaTexture *> &vecTextures,
		std::vector<float> &vecWeights);

	/**
	 * Retrieve the maximum texture dimensions.
	 */
	void GetMaxDimensions(int *aDims);

	/**
	 * Update the pusher, i.e. synchronize texture and flow data.
	 */
	virtual void Update(VflRenderNode *pRN);

	/**
	 * Handle messages concerning notifications from the data object.
	 */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	enum EInterpolMode{
		IM_NOT_SET = -1,
		IM_NEAREST,
		IM_LINEAR
	};
	/**
	 *
	 */
	void SetInterpolationMode(int iMode);
	int GetInterpolationMode() const;

	/**
	 * Manage internal texture data format.
	 */
	void SetTexturePrecision(int iPrecision);
	void SetTexturePrecision(const std::string &strPrecision);
	int GetTexturePrecision() const;
	std::string GetTexturePrecisionAsString() const;
	std::string GetTexturePrecisionString(int iPrecision) const;

    /**
	 * Property management via VistaPropertyLists
	 */
	virtual int SetProperty(const VistaProperty &refProp);
	virtual int GetProperty(VistaProperty &refProp);
	virtual int GetPropertySymbolList(std::list<std::string> &rStorageList);

	enum TEXTURE_PRECISION
	{
		TP_USE_METADATA = -1,
		TP_HALF = 0,
		TP_FLOAT,
		TP_UNSIGNED_BYTE,
		TP_UNSIGNED_SHORT,
		TP_LAST
	};

protected:
	VflUnsteadyCartesianGridPusher();

	void AllocateTextures();
	bool DetermineMaxDataDims();
	void SetTextureFiltering();
	void PushTexture(int iTimeIndex, int iTarget);

	VveUnsteadyCartesianGrid		*m_pData;
	std::vector<VistaTexture *>	m_vecTextures;
	std::vector<int>				m_vecLevelIndices;
	int								m_iCurrentTexture;
	int								m_iTextureForCurrentTime;
	int								m_aMaxDataDims[3];
	std::vector<bool>				m_vecDataChanged;
	bool							m_bNeedTextureDimCheck;
	bool							m_bNeedReallocation;
	bool							m_bSetFiltering;
	int								m_iInterpolMode;

	int								m_iTexturePrecision;
	GLuint							m_iPBO;
	int								m_iBufferSize;

	// go for unsteady data
	double m_fLastVisTime;
	double m_fCurrentVisTime;
};


#endif // _VFLUNSTEADYCARTESIANGRIDPUSHER_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

