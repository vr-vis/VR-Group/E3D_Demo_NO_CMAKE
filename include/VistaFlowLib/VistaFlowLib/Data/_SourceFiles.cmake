

set( RelativeDir "./Data" )
set( RelativeSourceGroup "Source Files\\Data" )

set( DirFiles
	VflAsyncDataQueue.h
	VflAsyncNotificationQueue.cpp
	VflAsyncNotificationQueue.h
	VflDataConverter.cpp
	VflDataConverter.h
	VflObserver.cpp
	VflObserver.h
	VflUnsteadyCartesianGridPusher.cpp
	VflUnsteadyCartesianGridPusher.h
	VflUnsteadyTetGridPusher.cpp
	VflUnsteadyTetGridPusher.h
	VflUnsteadyTexturePusher.cpp
	VflUnsteadyTexturePusher.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

