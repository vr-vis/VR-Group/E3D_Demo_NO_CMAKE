/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLASYNCDATAQUEUE_H
#define _VFLASYNCDATAQUEUE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <queue>
#include <cassert>

#include <VistaInterProcComm/Concurrency/VistaMutex.h>
#include <VistaVisExt/Data/VveDiscreteDataTyped.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 * An asynch data queue is used to synchronize data writes between different 
 * threads. Its most common use in in extraction commands, which asynchronously
 * receive extraction results and have to forward them to the visualization.
 * Basically this implements a two-phase protocol: A writer pushes data to the 
 * queue specifying the target time level index. A potential reader subsequently
 * calls the ForwardResutlts method in order to place the results into their
 * respective slots.
 *
 * @todo Implement this using a lock-free queue!
 *
 */
template<class TRawData> class VflAsyncDataQueue
{
public:
	VflAsyncDataQueue();
	virtual ~VflAsyncDataQueue();

	/**
	 * enqueue an element with the given data target
	 */
	void PushElement(VveDiscreteDataTyped<TRawData> *pTarget,
					 int iLevelIdx,
					 TRawData *pData);
	/** 
	 * forward all data currently in the queue to their 
	 * respective targets
	 * returns a (potentially empty) list of data objects which 
	 * have been replaced in the target data object (e.g. for proper
	 * deletion).
	 */
	std::list<TRawData*> ForwardData();

	/**
	 * check if the queue has something to forward
	 */
	bool GetHasData() const;

	/**
	 * 
	 */
	unsigned int GetNumElements() const;

protected:
	struct SQueueEntry{
		SQueueEntry(VveDiscreteDataTyped<TRawData> *pTarget,
					int iLevelIdx,
					TRawData *pData);
		virtual ~SQueueEntry();

		VveDiscreteDataTyped<TRawData> *m_pTarget;
		int m_iLevelIdx;
		TRawData *m_pData;
	};
private:
	std::queue<SQueueEntry> m_quData;
	VistaMutex *m_pMutex;
	bool m_bHasData;
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
template<class TRawData>
VflAsyncDataQueue<TRawData>::VflAsyncDataQueue()
	: m_pMutex(new VistaMutex), m_bHasData(false)
{
}

template<class TRawData>
VflAsyncDataQueue<TRawData>::~VflAsyncDataQueue()
{
	delete m_pMutex;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   PushElement                                                 */
/*                                                                            */
/*============================================================================*/
template<class TRawData>
inline void VflAsyncDataQueue<TRawData>::PushElement(
						VveDiscreteDataTyped<TRawData> *pTarget,
						int iLevelIdx,
						TRawData *pData)
{
	VistaMutexLock oLock(*m_pMutex);
	m_quData.push(SQueueEntry(pTarget, iLevelIdx, pData));
	m_bHasData = true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ForwardResults                                              */
/*                                                                            */
/*============================================================================*/
template<class TRawData>
inline std::list<TRawData*> VflAsyncDataQueue<TRawData>::ForwardData()
{
	VistaMutexLock oLock(*m_pMutex);
	std::list<TRawData*> liForwards;

	//copy all entries from the queue to their respective locations in
	//the data set
	while(!m_quData.empty())
	{
		SQueueEntry oEntry(m_quData.front());
		m_quData.pop();

		VveDataItem<TRawData> *pItem =
			oEntry.m_pTarget->GetTypedLevelDataByLevelIndex(oEntry.m_iLevelIdx);

		assert(pItem != NULL);
		//make sure to store the old entry for outside use
		TRawData *pOldItem = pItem->SetData(oEntry.m_pData);
		liForwards.push_back(pOldItem);
	}
	m_bHasData = false;
	return liForwards;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetHasData                                                  */
/*                                                                            */
/*============================================================================*/
template<class TRawData>
inline bool VflAsyncDataQueue<TRawData>::GetHasData() const
{
	return m_bHasData;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumElements                                              */
/*                                                                            */
/*============================================================================*/
template<class TRawData>
inline unsigned int VflAsyncDataQueue<TRawData>::GetNumElements() const
{
	VistaMutexLock oLock(*m_pMutex);
	return (unsigned int) m_quData.size();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SQueueEntry::SQueueEntry                                    */
/*                                                                            */
/*============================================================================*/
template<class TRawData>
VflAsyncDataQueue<TRawData>::SQueueEntry::SQueueEntry(
									VveDiscreteDataTyped<TRawData> *pTarget,
									int iLevelIdx,
									TRawData *pData)
	: m_pTarget(pTarget), m_iLevelIdx(iLevelIdx), m_pData(pData)
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLookAheadCount                                       */
/*                                                                            */
/*============================================================================*/
template<class TRawData>
VflAsyncDataQueue<TRawData>::SQueueEntry::~SQueueEntry()
{
}


#endif // _VflAsyncDataQueue_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

