/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// VistaFlowLib includes
#include "VflUnsteadyTexturePusher.h"
#include <VistaAspects/VistaAspectsUtils.h>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflUnsteadyTexturePusher::VflUnsteadyTexturePusher()
: m_iLookAheadCount(0),
  m_bWrapAround(false),
  m_bStreamingUpdate(false)
{
}

VflUnsteadyTexturePusher::~VflUnsteadyTexturePusher()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLookAheadCount                                       */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyTexturePusher::SetLookAheadCount(int iCount)
{
	m_iLookAheadCount = iCount;
}

int VflUnsteadyTexturePusher::GetLookAheadCount() const
{
	return m_iLookAheadCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetWrapAround                                           */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyTexturePusher::SetWrapAround(bool bWrap)
{
	m_bWrapAround = bWrap;
}

bool VflUnsteadyTexturePusher::GetWrapAround() const
{
	return m_bWrapAround;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetStreamingUpdate                                      */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyTexturePusher::SetStreamingUpdate(bool bStreamingUpdate)
{
	m_bStreamingUpdate = bStreamingUpdate;
}

bool VflUnsteadyTexturePusher::GetStreamingUpdate() const
{
	return m_bStreamingUpdate;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetProperty*                                            */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyTexturePusher::SetProperty(const VistaProperty &refProp)
{
	string strKey 
		= VistaAspectsConversionStuff::ConvertToLower(refProp.GetNameForNameable());

    if (strKey == "look_ahead")
    {
		SetLookAheadCount(VistaAspectsConversionStuff::ConvertToInt(refProp.GetValue()));
    }
	else if (strKey == "wrap_around")
	{
		SetWrapAround(VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue()));
	}
	else if (strKey == "streaming_update")
	{
		SetStreamingUpdate(VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue()));
	}
   
    Notify();

	return IVistaPropertyAwareable::PROP_OK;
}

int VflUnsteadyTexturePusher::GetProperty(VistaProperty &refProp)
{
   	string strKey 
		= VistaAspectsConversionStuff::ConvertToLower(refProp.GetNameForNameable());

    if (strKey == "look_ahead")
    {
		refProp.SetValue(VistaAspectsConversionStuff::ConvertToString(GetLookAheadCount()));
    }
	else if (strKey == "wrap_around")
	{
		refProp.SetValue(VistaAspectsConversionStuff::ConvertToString(GetWrapAround()));
	}
	else if (strKey == "streaming_update")
	{
		refProp.SetValue(VistaAspectsConversionStuff::ConvertToString(GetStreamingUpdate()));
	}
   
    return PROP_OK;
}

int VflUnsteadyTexturePusher::GetPropertySymbolList(std::list<std::string> &rStorageList)
{
	IVistaReflectionable::GetPropertySymbolList(rStorageList);
    rStorageList.push_back("LOOK_AHEAD");
    rStorageList.push_back("WRAP_AROUND");
    rStorageList.push_back("STREAMING_UPDATE");

    return int(rStorageList.size());
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
