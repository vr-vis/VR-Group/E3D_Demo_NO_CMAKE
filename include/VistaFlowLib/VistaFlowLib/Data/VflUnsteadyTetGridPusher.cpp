/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <cstring>

// VistaFlowLib includes
#include "VflUnsteadyTetGridPusher.h"
#include <VistaVisExt/Data/VveTetGrid.h>
#include <VistaVisExt/Data/VveUnsteadyTetGrid.h>

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaOGLUtils.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaAspects/VistaAspectsUtils.h>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflUnsteadyTetGridPusher::VflUnsteadyTetGridPusher(VveUnsteadyTetGrid *pUTetGrid)
: m_pData(pUTetGrid),
  m_iCurrentTexture(0),
  m_bTopologyGridChanged(true),
  m_bUseTopologyGrid(false),
  m_bNeedTextureDimCheck(true),
  m_fLastVisTime(0),
  m_fCurrentVisTime(0),
  m_pBuffer(NULL)
{
	m_aVertexTextureDims[0] = 1;
	m_aVertexTextureDims[1] = 1;
	m_aCellTextureDims[0] = 1;
	m_aCellTextureDims[1] = 1;

	DetermineMaxTextureDims();
	AllocateTextures();

	// observe our data
	if (m_pData)
	{
		Observe(m_pData, -2);
		if(m_pData->GetTopologyGrid())
		{
			Observe(m_pData->GetTopologyGrid(), -1);
		}
		m_vecDataChanged.resize(m_pData->GetNumberOfLevels());
		for (int i=0; i<m_pData->GetNumberOfLevels(); ++i)
		{
			Observe(m_pData->GetTypedLevelDataByLevelIndex(i), i);
			m_vecDataChanged[i] = true;
		}
	}
}

VflUnsteadyTetGridPusher::~VflUnsteadyTetGridPusher()
{
	if (m_pData)
	{
		for (int i=0; i<m_pData->GetTimeMapper()->GetNumberOfTimeLevels(); ++i)
		{
			ReleaseObserveable(m_pData->GetTypedLevelDataByLevelIndex(i));
		}
		if(m_pData->GetTopologyGrid())
		{
			ReleaseObserveable(m_pData->GetTopologyGrid());
		}
		ReleaseObserveable(m_pData);
	}

	for (unsigned int i=0; i<m_vecVertexTextures.size(); ++i)
	{
		delete m_vecVertexTextures[i];
		m_vecVertexTextures[i] = NULL;
		delete m_vecPointDataTextures[i];
		m_vecPointDataTextures[i] = NULL;
		delete m_vecCellTextures[i];
		m_vecCellTextures[i] = NULL;
		delete m_vecCellNeighborTextures[i];
		m_vecCellNeighborTextures[i] = NULL;
	}
	m_vecVertexTextures.clear();
	m_vecPointDataTextures.clear();
	m_vecCellTextures.clear();
	m_vecCellNeighborTextures.clear();

	delete [] m_pBuffer;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetUnsteadyTetGrid                                          */
/*                                                                            */
/*============================================================================*/
VveUnsteadyTetGrid *VflUnsteadyTetGridPusher::GetUnsteadyTetGrid() const
{
	return m_pData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get*Texture                                                 */
/*                                                                            */
/*============================================================================*/
VistaTexture *VflUnsteadyTetGridPusher::GetVertexTexture(int iLookAhead) const
{
	if (m_bUseTopologyGrid)
		return m_vecVertexTextures[0];

	int iIndex = (m_iCurrentTexture + iLookAhead) % (m_iLookAheadCount+1);
	return m_vecVertexTextures[iIndex];

}

VistaTexture *VflUnsteadyTetGridPusher::GetPointDataTexture(int iLookAhead) const
{
	int iIndex = (m_iCurrentTexture + iLookAhead) % (m_iLookAheadCount+1);
	return m_vecPointDataTextures[iIndex];
}

VistaTexture *VflUnsteadyTetGridPusher::GetCellTexture(int iLookAhead) const
{
	if (m_bUseTopologyGrid)
		return m_vecCellTextures[0];

	int iIndex = (m_iCurrentTexture + iLookAhead) % (m_iLookAheadCount+1);
	return m_vecCellTextures[iIndex];
}

VistaTexture *VflUnsteadyTetGridPusher::GetCellNeighborTexture(int iLookAhead) const
{
	if (m_bUseTopologyGrid)
		return m_vecCellNeighborTextures[0];

	int iIndex = (m_iCurrentTexture + iLookAhead) % (m_iLookAheadCount+1);
	return m_vecCellNeighborTextures[iIndex];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetUseTopologyGrid                                          */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyTetGridPusher::GetUseTopologyGrid() const
{
	return m_bUseTopologyGrid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTexturesAndWeights                                       */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyTetGridPusher::GetTexturesAndWeights(const std::vector<float> &vecOffsets,
														   std::vector<VistaTexture *> &vecTextures,
														   std::vector<float> &vecWeights)
{
	VveTimeMapper *pMapper = m_pData->GetTimeMapper();
	int aLevelIndices[2];
	VistaTexture *aTextures[2];

	for (unsigned int i=0; i<vecOffsets.size(); ++i)
	{
		double fTime = m_fLastVisTime + vecOffsets[i];
		while (fTime > 1.0)
			fTime -= 1.0;
		float fAlpha = static_cast<float>(
			pMapper->GetWeightedLevelIndices(fTime, aLevelIndices[0], aLevelIndices[1]));

		if (fAlpha < 0)
			return int(vecWeights.size());

		// now, try to find the level indices within the stored texture indices
		for (int j=0; j<2; ++j)
		{
			int iTextureIndex = m_iCurrentTexture;

			do
			{
				if (m_vecLevelIndices[iTextureIndex] == aLevelIndices[j])
					break;

				iTextureIndex = (++iTextureIndex) % (m_iLookAheadCount+1);
			}
			while (iTextureIndex != m_iCurrentTexture);

			if (m_vecLevelIndices[iTextureIndex] != aLevelIndices[j])
				return int(vecWeights.size());

			aTextures[j] = m_vecPointDataTextures[iTextureIndex];
		}

		// store the appropriate values
		vecWeights.push_back(fAlpha);
		vecTextures.push_back(aTextures[0]);
		vecTextures.push_back(aTextures[1]);
	}

	return int(vecWeights.size());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyTetGridPusher::Update(VflRenderNode *pRenderNode)
{
	// well, if we're not streaming, but the animation is playing, we
	// don't even have to think about staying here, right?!
	if ((!m_bStreamingUpdate || m_iLookAheadCount<2)
		&& pRenderNode->GetVisTiming()->GetAnimationPlaying())
		return;

	// Did anyone change the number of lookaheads? Anyone? Bueller?
	if (m_iLookAheadCount+1 != m_vecVertexTextures.size())
	{
		DetermineMaxTextureDims();	// well, just to be sure...
		AllocateTextures();
	}
	else if (m_bNeedTextureDimCheck)
	{
		if (DetermineMaxTextureDims())
		{
			AllocateTextures();
		}
	}
	else if (pRenderNode->GetVisTiming()->GetVisualizationTime() == m_fCurrentVisTime)
		return;	// been there, seen that, pushed the data...

	m_fLastVisTime = m_fCurrentVisTime;

	VveTimeMapper *pTimeMapper = m_pData->GetTimeMapper();
	m_fCurrentVisTime = pRenderNode->GetVisTiming()->GetVisualizationTime();
	int iTimeIndex;

	if (pRenderNode->GetVisTiming()->GetAnimationPlaying())
	{
		int iTimeIndex2;
		pTimeMapper->GetWeightedTimeIndices(m_fLastVisTime, iTimeIndex, iTimeIndex2);
	}
	else
	{
		// determine current time index
		iTimeIndex = pTimeMapper->GetTimeIndex(m_fCurrentVisTime);
	}

	int iLevelIndex = pTimeMapper->GetLevelIndex(iTimeIndex);
	int iIndexCount = pTimeMapper->GetNumberOfTimeIndices();

	// match current level index with indices stored in textures
	for (int i=0; i<=m_iLookAheadCount; ++i)
	{
		if (m_vecLevelIndices[m_iCurrentTexture] == iLevelIndex)
			break;

		m_iCurrentTexture = (m_iCurrentTexture+1) % (m_iLookAheadCount+1);
	}

	// iterate through textures and make sure, that every texture contains
	// correct data
	int iTextureIndex = m_iCurrentTexture;
	for (int i=0; i<=m_iLookAheadCount; ++i)
	{
		if (m_vecLevelIndices[iTextureIndex] != iLevelIndex
			|| m_vecDataChanged[iLevelIndex]
			|| (m_bUseTopologyGrid && m_bTopologyGridChanged))
		{
			PushTextures(iLevelIndex, iTextureIndex);
		}

		iTextureIndex = (iTextureIndex+1) % (m_iLookAheadCount+1);
		iTimeIndex = (iTimeIndex+1) % iIndexCount;
		iLevelIndex = pTimeMapper->GetLevelIndex(iTimeIndex);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyTetGridPusher::ObserverUpdate(IVistaObserveable *pObserveable,
															int msg,
															int ticket)
{
	if (ticket >= 0 && ticket < int(m_vecDataChanged.size()))
	{
		m_vecDataChanged[ticket] = true;
		m_bNeedTextureDimCheck = true;
	}
	else if (ticket == -1)
	{
		m_bTopologyGridChanged = true;
		m_bNeedTextureDimCheck = true;
	}
	else
	{
		// anything else..?!
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AllocateTextures                                            */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyTetGridPusher::AllocateTextures()
{
	if (!m_pData->GetTypedLevelDataByLevelIndex(0))
	{
		vstr::errp() << " [VflTetGridTexPusher] unable to find data container..." << endl;
		return;
	}

	VveTetGrid *pGrid = m_pData->GetTypedLevelDataByLevelIndex(0)->GetData();
	if (!pGrid)
	{
		vstr::errp() << " [VflTetGridTexPusher] unable to find tetrahedral grid..." << endl;
		return;
	}

	// sanity check
	if (m_iLookAheadCount < 0)
		m_iLookAheadCount = 0;

	if (m_vecVertexTextures.size() != m_iLookAheadCount+1)
	{
		// destroy old textures
		for (unsigned int i=0; i<m_vecVertexTextures.size(); ++i)
		{
			delete m_vecVertexTextures[i];
			m_vecVertexTextures[i] = NULL;
			delete m_vecPointDataTextures[i];
			m_vecPointDataTextures[i] = NULL;
			delete m_vecCellTextures[i];
			m_vecCellTextures[i] = NULL;
			delete m_vecCellNeighborTextures[i];
			m_vecCellNeighborTextures[i] = NULL;
		}

		// allocate space for new textures
		m_vecVertexTextures.resize(m_iLookAheadCount+1);
		m_vecPointDataTextures.resize(m_iLookAheadCount+1);
		m_vecCellTextures.resize(m_iLookAheadCount+1);
		m_vecCellNeighborTextures.resize(m_iLookAheadCount+1);
		m_vecLevelIndices.resize(m_iLookAheadCount+1);

		for (int i=0; i<=m_iLookAheadCount; ++i)
		{
			VistaTexture *pTexture = new VistaTexture(GL_TEXTURE_RECTANGLE_ARB);
			pTexture->Bind();
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER,GL_NEAREST);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER,GL_NEAREST);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexImage2D(pTexture->GetTarget(), 0, GL_RGB32F_ARB, m_aVertexTextureDims[0],
				m_aVertexTextureDims[1], 0, GL_RGBA, GL_FLOAT, NULL);
			pTexture->SetMaxS(float(m_aVertexTextureDims[0]));
			pTexture->SetMaxT(float(m_aVertexTextureDims[1]));
			m_vecVertexTextures[i] = pTexture;

			pTexture = new VistaTexture(GL_TEXTURE_RECTANGLE_ARB);
			pTexture->Bind();
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER,GL_NEAREST);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER,GL_NEAREST);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexImage2D(pTexture->GetTarget(), 0, GL_RGBA32F_ARB, m_aVertexTextureDims[0],
				m_aVertexTextureDims[1], 0, GL_RGBA, GL_FLOAT, NULL);
			pTexture->SetMaxS(float(m_aVertexTextureDims[0]));
			pTexture->SetMaxT(float(m_aVertexTextureDims[1]));
			m_vecPointDataTextures[i] = pTexture;

			pTexture = new VistaTexture(GL_TEXTURE_RECTANGLE_ARB);
			pTexture->Bind();
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER,GL_NEAREST);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER,GL_NEAREST);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexImage2D(pTexture->GetTarget(), 0, GL_RGBA32F_ARB, m_aCellTextureDims[0],
				m_aCellTextureDims[1], 0, GL_RGBA, GL_FLOAT, NULL);
			pTexture->SetMaxS(float(m_aCellTextureDims[0]));
			pTexture->SetMaxT(float(m_aCellTextureDims[1]));
			m_vecCellTextures[i] = pTexture;

			pTexture = new VistaTexture(GL_TEXTURE_RECTANGLE_ARB);
			pTexture->Bind();
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER,GL_NEAREST);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER,GL_NEAREST);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexImage2D(pTexture->GetTarget(), 0, GL_RGBA32F_ARB, m_aCellTextureDims[0],
				m_aCellTextureDims[1], 0, GL_RGBA, GL_FLOAT, NULL);
			pTexture->SetMaxS(float(m_aCellTextureDims[0]));
			pTexture->SetMaxT(float(m_aCellTextureDims[1]));
			m_vecCellNeighborTextures[i] = pTexture;

			m_vecLevelIndices[i] = -1;
		}
	}
	else
	{
		VistaTexture *pTexture = NULL;
		for (int i=0; i<=m_iLookAheadCount; ++i)
		{
			pTexture = m_vecVertexTextures[i];
			pTexture->Bind();
			glTexImage2D(pTexture->GetTarget(), 0, GL_RGB32F_ARB, m_aVertexTextureDims[0],
				m_aVertexTextureDims[1], 0, GL_RGBA, GL_FLOAT, NULL);
			pTexture->SetMaxS(float(m_aVertexTextureDims[0]));
			pTexture->SetMaxT(float(m_aVertexTextureDims[1]));

			pTexture = m_vecPointDataTextures[i];
			pTexture->Bind();
			glTexImage2D(pTexture->GetTarget(), 0, GL_RGBA32F_ARB, m_aVertexTextureDims[0],
				m_aVertexTextureDims[1], 0, GL_RGBA, GL_FLOAT, NULL);
			pTexture->SetMaxS(float(m_aVertexTextureDims[0]));
			pTexture->SetMaxT(float(m_aVertexTextureDims[1]));

			pTexture = m_vecCellTextures[i];
			pTexture->Bind();
			glTexImage2D(pTexture->GetTarget(), 0, GL_RGBA32F_ARB, m_aCellTextureDims[0],
				m_aCellTextureDims[1], 0, GL_RGBA, GL_FLOAT, NULL);
			pTexture->SetMaxS(float(m_aCellTextureDims[0]));
			pTexture->SetMaxT(float(m_aCellTextureDims[1]));

			pTexture = m_vecCellNeighborTextures[i];
			pTexture->Bind();
			glTexImage2D(pTexture->GetTarget(), 0, GL_RGBA32F_ARB, m_aCellTextureDims[0],
				m_aCellTextureDims[1], 0, GL_RGBA, GL_FLOAT, NULL);
			pTexture->SetMaxS(float(m_aCellTextureDims[0]));
			pTexture->SetMaxT(float(m_aCellTextureDims[1]));

			m_vecLevelIndices[i] = -1;	
		}
	}
	m_iCurrentTexture = 0;

	int iMaxWidth = (m_aVertexTextureDims[0]>m_aCellTextureDims[0] ? m_aVertexTextureDims[0] : m_aCellTextureDims[0]);
	int iMaxHeight = (m_aVertexTextureDims[1]>m_aCellTextureDims[1] ? m_aVertexTextureDims[1] : m_aCellTextureDims[1]);
	delete [] m_pBuffer;
	m_pBuffer = new float[iMaxWidth * iMaxHeight * 4];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DetermineMaxTextureDims                                     */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyTetGridPusher::DetermineMaxTextureDims()
{
	int aOldVertexDims[2];
	memcpy(aOldVertexDims, m_aVertexTextureDims, 2*sizeof(int));

	int aOldCellDims[2];
	memcpy(aOldCellDims, m_aCellTextureDims, 2*sizeof(int));

	m_aVertexTextureDims[0] = 1;
	m_aVertexTextureDims[1] = 1;
	m_aCellTextureDims[0] = 1;
	m_aCellTextureDims[1] = 1;

	int iCount = m_pData->GetNumberOfLevels();
	bool bFirst = true;
	int iTemp;
	int iWidth, iHeight;
	m_bUseTopologyGrid = false;
	for (int i=0; i<iCount; ++i)
	{
		VveTetGridItem *pContainer = m_pData->GetTypedLevelDataByLevelIndex(i);
		VveTetGrid *pGrid = pContainer->GetData();

		if (pGrid && pGrid->GetValid())
		{
			if (bFirst)
			{
				iTemp = pGrid->GetVertexCount();
				VistaOGLUtils::ComputeTextureDimensions(iTemp, m_aVertexTextureDims[0], m_aVertexTextureDims[1]);
				
				iTemp = pGrid->GetCellCount();
				VistaOGLUtils::ComputeTextureDimensions(iTemp, m_aCellTextureDims[0], m_aCellTextureDims[1]);

				bFirst = false;
			}
			else
			{
				iTemp = pGrid->GetVertexCount();
				VistaOGLUtils::ComputeTextureDimensions(iTemp, iWidth, iHeight);
				if (iWidth > m_aVertexTextureDims[0])
					m_aVertexTextureDims[0] = iWidth;
				if (iHeight > m_aVertexTextureDims[1])
					m_aVertexTextureDims[1] = iHeight;

				iTemp = pGrid->GetCellCount();
				VistaOGLUtils::ComputeTextureDimensions(iTemp, iWidth, iHeight);
				if (iWidth > m_aCellTextureDims[0])
					m_aCellTextureDims[0] = iWidth;
				if (iHeight > m_aCellTextureDims[1])
					m_aCellTextureDims[1] = iHeight;
			}

			// do we have to deal with shared topology?
			m_bUseTopologyGrid |= pGrid->GetTopologyGrid()!=NULL;
		}
	}

	m_bNeedTextureDimCheck = false;

	return (aOldVertexDims[0] != m_aVertexTextureDims[0]
		|| aOldVertexDims[1] != m_aVertexTextureDims[1]
		|| aOldCellDims[0] != m_aCellTextureDims[0]
		|| aOldCellDims[1] != m_aCellTextureDims[1]);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   PushTextures                                                */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyTetGridPusher::PushTextures(int iLevelIndex, int iTarget)
{
	// TODO - think about using PBOs as we have to convert the ints to floats
	// anyway...
	VveTetGridItem *pContainer = m_pData->GetTypedLevelDataByLevelIndex(iLevelIndex);
	pContainer->LockData();
	VveTetGrid *pGrid = pContainer->GetData();

	if (pGrid && pGrid->GetValid() && !m_bNeedTextureDimCheck)
	{
		float *pData = NULL;
		bool bPad = false;
		int iCount, w, h;

		iCount = pGrid->GetPaddedVertexCount();
		VistaOGLUtils::ComputeTextureDimensions(iCount, w, h);
		if (iCount != w*h)
		{
			// pad data... *sigh*
			bPad = true;

#if 0
			m_pBuffer = new float[4*w*h];
			float *pPointData = pGrid->GetPointData();

			for (int i=0; i<iCount; ++i)
				memcpy(&m_pBuffer[4*i], &pPointData[4*i], 4*sizeof(float));

			delete m_pBuffer;
			m_pBuffer = NULL;
#else
			pData = m_pBuffer;
			memcpy(pData, pGrid->GetPointData(), iCount*4*sizeof(float));
			memset(&pData[iCount*4], 0, (w*h-iCount)*4*sizeof(float));
#endif
		}
		else
			pData = pGrid->GetPointData();

		m_vecPointDataTextures[iTarget]->Bind();
		glTexSubImage2D(m_vecPointDataTextures[iTarget]->GetTarget(), 0, 0, 0, w, h, GL_RGBA, GL_FLOAT, pData);
		m_vecPointDataTextures[iTarget]->SetMaxS(float(w));
		m_vecPointDataTextures[iTarget]->SetMaxT(float(h));

		if (m_bUseTopologyGrid)
		{
			if (m_bTopologyGridChanged)
			{
				m_pData->GetTopologyGrid()->LockData();

				if (bPad)
				{
					memcpy(pData, pGrid->GetVertices(), iCount*3*sizeof(float));
					memset(&pData[iCount*3], 0, (w*h-iCount)*3*sizeof(float));
				}
				else
					pData = pGrid->GetVertices();

				m_vecVertexTextures[0]->Bind();
				glTexSubImage2D(m_vecVertexTextures[0]->GetTarget(), 0, 0, 0, w, h, GL_RGB, GL_FLOAT, pData);
				m_vecVertexTextures[0]->SetMaxS(float(w));
				m_vecVertexTextures[0]->SetMaxT(float(h));

				iCount = pGrid->GetCellCount();
				VistaOGLUtils::ComputeTextureDimensions(iCount, w, h);
				int *pCellData = pGrid->GetCells();
				float *pPos = m_pBuffer;
				for (int i=0; i<4*iCount; ++i)
					*(pPos++) = float(pCellData[i]);
				memset(&m_pBuffer[iCount*4], 0, (w*h-iCount)*4*sizeof(float));
				m_vecCellTextures[0]->Bind();
				glTexSubImage2D(m_vecCellTextures[0]->GetTarget(), 0, 0, 0, w, h, GL_RGBA, GL_FLOAT, m_pBuffer);
				m_vecCellTextures[0]->SetMaxS(float(w));
				m_vecCellTextures[0]->SetMaxT(float(h));

				pCellData = pGrid->GetCellNeighbors();
				pPos = m_pBuffer;
				for (int i=0; i<4*iCount; ++i)
					*(pPos++) = float(pCellData[i]);
				m_vecCellNeighborTextures[0]->Bind();
				glTexSubImage2D(m_vecCellNeighborTextures[0]->GetTarget(), 0, 0, 0, w, h, GL_RGBA, GL_FLOAT, m_pBuffer);
				m_vecCellNeighborTextures[0]->SetMaxS(float(w));
				m_vecCellNeighborTextures[0]->SetMaxT(float(h));

				m_pData->GetTopologyGrid()->UnlockData();

				m_bTopologyGridChanged = false;
			}
		}
		else
		{
			if (bPad)
			{
				memcpy(pData, pGrid->GetVertices(), iCount*3*sizeof(float));
				memset(&pData[iCount*3], 0, (w*h-iCount)*3*sizeof(float));
			}
			else
				pData = pGrid->GetVertices();

			m_vecVertexTextures[iTarget]->Bind();
			glTexSubImage2D(m_vecVertexTextures[iTarget]->GetTarget(), 0, 0, 0, w, h, GL_RGB, GL_FLOAT, pData);
			m_vecVertexTextures[iTarget]->SetMaxS(float(w));
			m_vecVertexTextures[iTarget]->SetMaxT(float(h));

			iCount = pGrid->GetCellCount();
			VistaOGLUtils::ComputeTextureDimensions(iCount, w, h);
			int *pCellData = pGrid->GetCells();
			float *pPos = m_pBuffer;
			for (int i=0; i<4*iCount; ++i)
				*(pPos++) = float(pCellData[i]);
				
#if 0
			vstr::debugi() << "comparing contents..." << endl;
			for (int i=0; i<4*iCount; ++i)
			{
				if (pCellData[i] != int (m_pBuffer[i]))
					vstr::debugi() << "cell mismatch : " << i << ": " << pCellData[i] << "!=" 
					<< int(m_pBuffer[i]) << endl;
			}
#endif				
			memset(&m_pBuffer[iCount*4], 0, (w*h-iCount)*4*sizeof(float));
			m_vecCellTextures[iTarget]->Bind();
			glTexSubImage2D(m_vecCellTextures[iTarget]->GetTarget(), 0, 0, 0, w, h, GL_RGBA, GL_FLOAT, m_pBuffer);
			m_vecCellTextures[iTarget]->SetMaxS(float(w));
			m_vecCellTextures[iTarget]->SetMaxT(float(h));

			pCellData = pGrid->GetCellNeighbors();
			pPos = m_pBuffer;
			for (int i=0; i<4*iCount; ++i)
				*(pPos++) = float(pCellData[i]);
#if 0
			for (int i=0; i<4*iCount; ++i)
			{
				if (pCellData[i] != int (m_pBuffer[i]))
					vstr::debugi() << "neighbor mismatch : " << i << ": " << pCellData[i] << "!=" 
					<< int(m_pBuffer[i]) << endl;
			}
#endif				
			m_vecCellNeighborTextures[iTarget]->Bind();
			glTexSubImage2D(m_vecCellNeighborTextures[iTarget]->GetTarget(), 0, 0, 0, w, h, GL_RGBA, GL_FLOAT, m_pBuffer);
			m_vecCellNeighborTextures[iTarget]->SetMaxS(float(w));
			m_vecCellNeighborTextures[iTarget]->SetMaxT(float(h));
		}

		m_vecLevelIndices[iTarget] = iLevelIndex;
	}
	m_vecDataChanged[iLevelIndex] = false;
	pContainer->UnlockData();
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
