/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLUNSTEADYTETGRIDPUSHER_H
#define _VFLUNSTEADYTETGRIDPUSHER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflUnsteadyTexturePusher.h"

#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;
class VveTetGrid;
class VveUnsteadyTetGrid;

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 * This class provides data synchronization between texture data and flow data 
 * on a tetrahedral grid in main memory.
 */
class VISTAFLOWLIBAPI VflUnsteadyTetGridPusher : public VflUnsteadyTexturePusher
{
public:
	VflUnsteadyTetGridPusher(VveUnsteadyTetGrid *pUTetGrid);
	virtual ~VflUnsteadyTetGridPusher();

	/**
	 * Data retrieval.
	 */
	VveUnsteadyTetGrid *GetUnsteadyTetGrid() const;

	/**
	 * Retrieve the textures storing the current time step.
	 * Use this one, if the animation is *not* playing!
	 */
	VistaTexture *GetVertexTexture(int iLookAhead = 0) const;
	VistaTexture *GetPointDataTexture(int iLookAhead = 0) const;
	VistaTexture *GetCellTexture(int iLookAhead = 0) const;
	VistaTexture *GetCellNeighborTexture(int iLookAhead = 0) const;

	/** 
	 * Check, whether topology is shared between time steps
	 * of the given grid.
	 */
	bool GetUseTopologyGrid() const;

	/**
	 * Retrieve data for interpolating vector data over time, starting
	 * at the *last* visualization time with offsets given in 
	 * visualization time as well.
	 * Use this one, if the animation *is* playing.
	 * NOTE: All given vectors are supposed to be empty!
	 * NOTE: As soon as an offset is encountered, which cannot be
	 *       served by the currently stored texture data, 
	 *       this method returns.
	 * NOTE: This method assumes that the grid is static and topology 
	 *       information is shared, i.e. it returns point data textures only. 
	 *
	 * @param[in]  vecOffsets   : time offsets
	 * @param[out] vecPointDataTextures  : the textures containing the point data,
	 *                                 2 for every valid time offset
	 * @param[out] vecWeights   : interpolation weights alpha(i)
	 *                        for every valid time offset
	 * @param[out] return value : number n of valid time offsets,
	 *                        n <= vecOffsets.size().
	 *
	 * So, for vecOffsets=[0, t/2, t, 200*t, t] this method returns the 
	 * following information:
	 * return value r = 3 (200*t cannot be served as the number of 
	 *                     look-aheads is too small, the remaining offsets
	 *                     are ignored)
	 * vecPointDataTextures = [texture(0), texture(1), .., texture(2*(r-1))]
	 * vecWeights = [alpha(0), alpha(1), .., alpha(r-1)],
	 * which means that for interpolating at time 
	 *   fTime = m_fLastVisTime+vecOffsets[i] 
	 * textures
	 *   texture(2*i) and texture(2*i+1)
	 * with the interpolation weights
	 *   (1-alpha(i)) and alpha(i)
	 * are to be used, i.e.
	 *   data(fTime) = (1-alpha(i)) * texture(2*i)
	 *                  + alpha(i) * texture(2*i+1);
	 *
	 * Note, that vecOffsets[4]==t is not considered anymore!
	 *
	 * *phew* Now, try saying that three times with eyes closed 
	 * and standing on your left leg...
	 */
	int GetTexturesAndWeights(const std::vector<float> &vecOffsets,
		std::vector<VistaTexture *> &vecPointDataTextures,
		std::vector<float> &vecWeights);

	/**
	 * Update the pusher, i.e. synchronize texture and flow data.
	 */
	virtual void Update(VflRenderNode *pRenderNode);

	/**
	 * Handle messages concerning notifications from the data object.
	 */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

protected:
	VflUnsteadyTetGridPusher();

	void AllocateTextures();
	bool DetermineMaxTextureDims();
	void PushTextures(int iTimeIndex, int iTarget);

	VveUnsteadyTetGrid				*m_pData;
	std::vector<VistaTexture *>	m_vecVertexTextures;
	std::vector<VistaTexture *>	m_vecPointDataTextures;
	std::vector<VistaTexture *>	m_vecCellTextures;
	std::vector<VistaTexture *>	m_vecCellNeighborTextures;
	std::vector<int>				m_vecLevelIndices;
	int								m_iCurrentTexture;
	int								m_aVertexTextureDims[2];
	int								m_aCellTextureDims[2];
	std::vector<bool>				m_vecDataChanged;
	bool							m_bTopologyGridChanged;
	bool							m_bUseTopologyGrid;
	bool							m_bNeedTextureDimCheck;
	float							*m_pBuffer;

	// go for unsteady data
	double m_fLastVisTime;
	double m_fCurrentVisTime;
};

#endif // _VFLUNSTEADYTETGRIDPUSHER_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

