/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <cstring>

// VistaFlowLib includes
#include "VflUnsteadyCartesianGridPusher.h"

#include <VistaOGLExt/VistaTexture.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaBase/Half/VistaHalf.h>
#include <VistaAspects/VistaReflectionable.h>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#define USE_PBO


/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflUnsteadyCartesianGridPusher::VflUnsteadyCartesianGridPusher(VveUnsteadyCartesianGrid *pUCGrid,int iTexturePrecision)
: m_pData(pUCGrid),
  m_iCurrentTexture(0),
  m_iTextureForCurrentTime(0),
  m_bNeedTextureDimCheck(true),
  m_bNeedReallocation(true),
  m_iInterpolMode(IM_NOT_SET),
  m_bSetFiltering(false),
  m_iTexturePrecision(iTexturePrecision),
  m_fLastVisTime(0),
  m_fCurrentVisTime(0),
  m_iPBO(0),
  m_iBufferSize(0)
{
		if (m_iTexturePrecision == TP_USE_METADATA)
	{		
		if (!m_pData->GetPropertyByName("TEXTURE_PRECISION").GetValue().empty())
		{
			this->SetProperty(m_pData->GetPropertyByName("TEXTURE_PRECISION").GetValue());
		}
	}

	m_aMaxDataDims[0] = 1;
	m_aMaxDataDims[1] = 1;
	m_aMaxDataDims[2] = 1;

#ifdef USE_PBO
	glGenBuffersARB(1, &m_iPBO);
	m_iBufferSize = sizeof(float);
	glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_iPBO);
	glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_iBufferSize, NULL, GL_STREAM_DRAW);
	glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
#endif

	DetermineMaxDataDims();
	AllocateTextures();

	// observe our data
	if (m_pData)
	{
		Observe(m_pData, -1);
		m_vecDataChanged.resize(m_pData->GetNumberOfLevels());
		for (int i=0; i<m_pData->GetNumberOfLevels(); ++i)
		{
			Observe(m_pData->GetTypedLevelDataByLevelIndex(i), i);
			m_vecDataChanged[i] = true;
		}
	}
}

VflUnsteadyCartesianGridPusher::~VflUnsteadyCartesianGridPusher()
{
#ifdef USE_PBO
	glDeleteBuffersARB(1, &m_iPBO);
	m_iPBO = 0;
#endif

	if (m_pData)
	{
		for (int i=0; i<m_pData->GetTimeMapper()->GetNumberOfTimeLevels(); ++i)
		{
			ReleaseObserveable(m_pData->GetTypedLevelDataByLevelIndex(i));
		}
		ReleaseObserveable(m_pData);
	}

	for (unsigned int i=0; i<m_vecTextures.size(); ++i)
	{
		delete m_vecTextures[i];
		m_vecTextures[i] = NULL;
	}
	m_vecTextures.clear();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetUnsteadyCartesianGrid                                    */
/*                                                                            */
/*============================================================================*/
VveUnsteadyCartesianGrid *VflUnsteadyCartesianGridPusher::GetUnsteadyCartesianGrid() const
{
	return m_pData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTexture                                                  */
/*                                                                            */
/*============================================================================*/
VistaTexture *VflUnsteadyCartesianGridPusher::GetTexture(int iLookAhead) const
{
	int iIndex = (m_iTextureForCurrentTime + iLookAhead) % (m_iLookAheadCount+1);
	return m_vecTextures[iIndex];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTexturesAndWeights                                       */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyCartesianGridPusher::GetTexturesAndWeights(const std::vector<double> &vecOffsets,
														   std::vector<VistaTexture *> &vecTextures,
														   std::vector<float> &vecWeights)
{
	VveTimeMapper *pMapper = m_pData->GetTimeMapper();
	int aLevelIndices[2];
	VistaTexture *aTextures[2];

	for (unsigned int i=0; i<vecOffsets.size(); ++i)
	{
		double fTime = m_fLastVisTime + vecOffsets[i];
		while (fTime > 1.0)
			fTime -= 1.0;
		float fAlpha = static_cast<float>(
			pMapper->GetWeightedLevelIndices(fTime, aLevelIndices[0], aLevelIndices[1]));

		if (fAlpha < 0)
			return int(vecWeights.size());

		// now, try to find the level indices within the stored texture indices
		for (int j=0; j<2; ++j)
		{
			int iTextureIndex = m_iCurrentTexture;

			do
			{
				if (m_vecLevelIndices[iTextureIndex] == aLevelIndices[j])
					break;

				iTextureIndex = (++iTextureIndex) % (m_iLookAheadCount+1);
			}
			while (iTextureIndex != m_iCurrentTexture);

			if (m_vecLevelIndices[iTextureIndex] != aLevelIndices[j])
				return int(vecWeights.size());

			aTextures[j] = m_vecTextures[iTextureIndex];
		}

		// store the appropriate values
		vecWeights.push_back(fAlpha);
		vecTextures.push_back(aTextures[0]);
		vecTextures.push_back(aTextures[1]);
	}

	return int(vecWeights.size());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetMaxDimensions                                            */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyCartesianGridPusher::GetMaxDimensions(int *aDims)
{
	memcpy(aDims, m_aMaxDataDims, 3*sizeof(int));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyCartesianGridPusher::Update(VflRenderNode *pRenderNode)
{
	// well, if we're not streaming, but the animation is playing, we
	// don't even have to think about staying here, right?!
	if (!m_bStreamingUpdate && pRenderNode->GetVisTiming()->GetAnimationPlaying())
		return;

	// Did anyone change the number of lookaheads? Anyone? Bueller?
	if (m_iLookAheadCount+1 != m_vecTextures.size())
	{
		DetermineMaxDataDims(); // well, just to be sure...
		m_bNeedReallocation = true;
	}
	else if (m_bNeedTextureDimCheck)
	{
		m_bNeedReallocation |= DetermineMaxDataDims();
	}
	else if (pRenderNode->GetVisTiming()->GetVisualizationTime() == m_fCurrentVisTime)
		return;	// been there, seen that, pushed the data...

	if (m_bNeedReallocation)
		AllocateTextures();

	if (m_bSetFiltering)
		SetTextureFiltering();

	m_fLastVisTime = m_fCurrentVisTime;

	VveTimeMapper *pTimeMapper = m_pData->GetTimeMapper();
	m_fCurrentVisTime = pRenderNode->GetVisTiming()->GetVisualizationTime();
	int iTimeIndex;
	int iCurrentTimeIndex;

	if (pRenderNode->GetVisTiming()->GetAnimationPlaying()
		&& m_iLookAheadCount>1)
	{
		// if we have a look-ahead count of at least 2, we are supposed
		// to store at least the two levels which are relevant for the 
		// current visualization/simulation time
		int iTimeIndex2;
		pTimeMapper->GetWeightedTimeIndices(m_fLastVisTime, iTimeIndex, iTimeIndex2);
		iCurrentTimeIndex = pTimeMapper->GetTimeIndex(m_fCurrentVisTime);
	}
	else
	{
		// determine current time index
		iCurrentTimeIndex = iTimeIndex = pTimeMapper->GetTimeIndex(m_fCurrentVisTime);
	}

	int iLevelIndex = pTimeMapper->GetLevelIndex(iTimeIndex);
	int iIndexCount = pTimeMapper->GetNumberOfTimeIndices();
	int iCurrentLevelIndex = pTimeMapper->GetLevelIndex(iCurrentTimeIndex);

	// match current level index with indices stored in textures
	for (int i=0; i<=m_iLookAheadCount; ++i)
	{
		if (m_vecLevelIndices[m_iCurrentTexture] == iLevelIndex)
			break;

		m_iCurrentTexture = (m_iCurrentTexture+1) % (m_iLookAheadCount+1);
	}

	// iterate through textures and make sure, that every texture contains
	// correct data
	int iTextureIndex = m_iCurrentTexture;
	int iTimeIndexTemp = iTimeIndex;
	for (int i=0; i<=m_iLookAheadCount; ++i)
	{
		if (m_vecLevelIndices[iTextureIndex] != iLevelIndex
			|| m_vecDataChanged[iLevelIndex])
		{
			PushTexture(iLevelIndex, iTextureIndex);
		}

		iTextureIndex = (iTextureIndex+1) % (m_iLookAheadCount+1);
		iTimeIndex = (iTimeIndex+1) % iIndexCount;
		iLevelIndex = pTimeMapper->GetLevelIndex(iTimeIndex);
	}

	// now, reset their changed flags
	iTextureIndex = m_iCurrentTexture;
	iTimeIndex = iTimeIndexTemp;
	iLevelIndex = pTimeMapper->GetLevelIndex(iTimeIndex);
	for (int i=0; i<=m_iLookAheadCount; ++i)
	{
		m_vecDataChanged[iLevelIndex] = false;

		iTimeIndex = (iTimeIndex+1) % iIndexCount;
		iLevelIndex = pTimeMapper->GetLevelIndex(iTimeIndex);
	}

	m_iTextureForCurrentTime = m_iCurrentTexture;
	for (int i=0; i<=m_iLookAheadCount; ++i)
	{
		if (m_vecLevelIndices[m_iTextureForCurrentTime] == iCurrentLevelIndex)
			break;
		m_iTextureForCurrentTime = (m_iTextureForCurrentTime+1) % (m_iLookAheadCount+1);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyCartesianGridPusher::ObserverUpdate(IVistaObserveable *pObserveable,
															int msg,
															int ticket)
{
	if (ticket >= 0 && ticket < int(m_vecDataChanged.size()))
	{
		m_vecDataChanged[ticket] = true;
		m_bNeedTextureDimCheck = true;
	}
	else
	{
		// anything else..?!
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetForceLinearInterpolation                             */
/*                                                                            */
/*============================================================================*/
/*
void VflUnsteadyCartesianGridPusher::SetForceLinearInterpolation(bool bForce)
{
	if (bForce != m_bForceLinearInterpolation)
	{
		m_bForceLinearInterpolation = bForce;
		m_bSetFiltering = true;
	}
}

bool VflUnsteadyCartesianGridPusher::GetForceLinearInterpolation() const
{
	return m_bForceLinearInterpolation;
}
*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetInterpolationMode					                  */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyCartesianGridPusher::SetInterpolationMode(int iMode)
{
	if (iMode != m_iInterpolMode)
	{
		m_iInterpolMode = iMode;
		m_bSetFiltering = true;
	}
}

int VflUnsteadyCartesianGridPusher::GetInterpolationMode() const
{
	return m_iInterpolMode;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTexturePrecision*                                    */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyCartesianGridPusher::SetTexturePrecision(int iPrecision)
{
	if (TP_HALF<=iPrecision && iPrecision<TP_LAST && m_iTexturePrecision!=iPrecision)
	{
		m_iTexturePrecision = iPrecision;
		m_bNeedReallocation = true;
	}
}

void VflUnsteadyCartesianGridPusher::SetTexturePrecision(const std::string &strPrecision)
{
	for (int i=TP_USE_METADATA; i<TP_LAST; ++i)
	{
		if (GetTexturePrecisionString(i) == strPrecision)
		{
			SetTexturePrecision(i);
			break;
		}
	}
}

int VflUnsteadyCartesianGridPusher::GetTexturePrecision() const
{
	return m_iTexturePrecision;
}

std::string VflUnsteadyCartesianGridPusher::GetTexturePrecisionAsString() const
{
	return GetTexturePrecisionString(m_iTexturePrecision);
}

std::string VflUnsteadyCartesianGridPusher::GetTexturePrecisionString(int iPrecision) const
{
	switch (iPrecision)
	{
	case TP_USE_METADATA:
		return string("USE_METADATA");
	case TP_HALF:
		return string("HALF");
	case TP_FLOAT:
		return string("FLOAT");
	case TP_UNSIGNED_BYTE:
		return string("UNSIGNED_BYTE");
  case TP_UNSIGNED_SHORT:
		return string("UNSIGNED_SHORT");
	}
	return string("INVALID");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetProperty*                                            */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyCartesianGridPusher::SetProperty(const VistaProperty &refProp)
{
	string strKey 
		= VistaAspectsConversionStuff::ConvertToLower(refProp.GetNameForNameable());

    if (strKey == "texture_precision")
    {
		SetTexturePrecision(refProp.GetValue());
    }
    else
    {
		return VflUnsteadyTexturePusher::SetProperty(refProp);
    }

    Notify();

    return PROP_OK;
}

int VflUnsteadyCartesianGridPusher::GetProperty(VistaProperty &refProp)
{
   	string strKey 
		= VistaAspectsConversionStuff::ConvertToLower(refProp.GetNameForNameable());

    if (strKey == "texture_precision")
    {
		refProp.SetValue(GetTexturePrecisionAsString());
    }
    else
    {
        return VflUnsteadyTexturePusher::GetProperty(refProp);
    }

    return PROP_OK;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPropertySymbolList                                       */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyCartesianGridPusher::GetPropertySymbolList(std::list<string> &rStorageList)
{
	VflUnsteadyTexturePusher::GetPropertySymbolList(rStorageList);
	rStorageList.push_back("TEXTURE_PRECISION");
	return int(rStorageList.size());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AllocateTextures                                            */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyCartesianGridPusher::AllocateTextures()
{
	if (!m_vecTextures.empty())
	{
		// destroy old textures
		for (unsigned int i=0; i<m_vecTextures.size(); ++i)
		{
			delete m_vecTextures[i];
			m_vecTextures[i] = NULL;
		}
	}

	if (!m_pData->GetTypedLevelDataByLevelIndex(0))
	{
		vstr::errp() << " [VflUnCartGridTexPusher] unable to find data container..." << endl;
		return;
	}

	VveCartesianGrid *pGrid = m_pData->GetTypedLevelDataByLevelIndex(0)->GetData();
	if (!pGrid)
	{
		vstr::errp() << " [VflUnCartGridTexPusher] unable to find cartesian grid..." << endl;
		return;
	}

	// sanity check
	if (m_iLookAheadCount < 0)
		m_iLookAheadCount = 0;

	// allocate new textures
	m_vecTextures.resize(m_iLookAheadCount+1);
	m_vecLevelIndices.resize(m_iLookAheadCount+1);

	int iFormat = GL_RGBA;
	int iType = GL_FLOAT;
	int iInternalFormat = GL_RGBA;
	int iByteCountPerEntry;
	int iComponents = pGrid->GetComponents();
	if (m_iTexturePrecision == TP_HALF)
	{
		switch (iComponents)
		{
		case 1:
			iInternalFormat = GL_ALPHA16F_ARB;
			break;
		case 2:
			iInternalFormat = GL_LUMINANCE_ALPHA16F_ARB;
			break;
		case 3:
			iInternalFormat = GL_RGB16F_ARB;
			break;
		case 4:
		default:
			iInternalFormat = GL_RGBA16F_ARB;
			break;
		}
		iByteCountPerEntry = sizeof(VistaHalf);
	}
	else if (m_iTexturePrecision == TP_FLOAT)
	{
		switch (iComponents)
		{
		case 1:
			iInternalFormat = GL_ALPHA32F_ARB;
			break;
		case 2:
			iInternalFormat = GL_LUMINANCE_ALPHA32F_ARB;
			break;
		case 3:
			iInternalFormat = GL_RGB32F_ARB;
			break;
		case 4:
		default:
			iInternalFormat = GL_RGBA32F_ARB;
			break;
		}
		iByteCountPerEntry = sizeof(float);
	}
	else 
	{
		switch (iComponents)
		{
		case 1:
			iInternalFormat = GL_ALPHA;
			break;
		case 2:
			iInternalFormat = GL_LUMINANCE_ALPHA;
			break;
		case 3:
			iInternalFormat = GL_RGB;
			break;
		case 4:
		default:
			iInternalFormat = GL_RGBA;
			break;
		}
    if (m_iTexturePrecision == TP_UNSIGNED_SHORT)
    {
      iByteCountPerEntry = sizeof(unsigned short);
	    iType = GL_UNSIGNED_SHORT;
    }
    else
      iByteCountPerEntry = sizeof(unsigned char);
	}

#ifdef USE_PBO
	m_iBufferSize = m_aMaxDataDims[0]*m_aMaxDataDims[1]*m_aMaxDataDims[2]*iComponents*iByteCountPerEntry;
	glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_iPBO);
	glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_iBufferSize,
		NULL, GL_STREAM_DRAW);
	glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
#endif

	for (int i=0; i<=m_iLookAheadCount; ++i)
	{
		VistaTexture *pTexture = new VistaTexture(GL_TEXTURE_3D);
		pTexture->Bind();

		glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexImage3D(pTexture->GetTarget(), 0, iInternalFormat, m_aMaxDataDims[0],
			m_aMaxDataDims[1], m_aMaxDataDims[2], 0, iFormat,iType, NULL);

		m_vecTextures[i] = pTexture;
		m_vecLevelIndices[i] = -1;
	}
	SetTextureFiltering();

	m_iCurrentTexture = 0;
	m_iTextureForCurrentTime = 0;
	m_bNeedReallocation = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DetermineMaxDataDims                                        */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyCartesianGridPusher::DetermineMaxDataDims()
{
	int aOldDims[3];
	memcpy(aOldDims, m_aMaxDataDims, 3*sizeof(int));

	m_aMaxDataDims[0] = 1;
	m_aMaxDataDims[1] = 1;
	m_aMaxDataDims[2] = 1;

	int iCount = m_pData->GetNumberOfLevels();
	bool bFirst = true;
	for (int i=0; i<iCount; ++i)
	{
		VveCartesianGridItem *pContainer = m_pData->GetTypedLevelDataByLevelIndex(i);
		VveCartesianGrid *pGrid = pContainer->GetData();

		if (pGrid && pGrid->GetValid())
		{
			if (bFirst)
			{
				pGrid->GetDimensions(m_aMaxDataDims);
				bFirst = false;
			}
			else
			{
				int aTemp[3];
				pGrid->GetDimensions(aTemp);

				if (aTemp[0] > m_aMaxDataDims[0])
					m_aMaxDataDims[0] = aTemp[0];
				if (aTemp[1] > m_aMaxDataDims[1])
					m_aMaxDataDims[1] = aTemp[1];
				if (aTemp[2] > m_aMaxDataDims[2])
					m_aMaxDataDims[2] = aTemp[2];
			}
		}
	}

	m_bNeedTextureDimCheck = false;

	return (aOldDims[0] != m_aMaxDataDims[0]
		|| aOldDims[1] != m_aMaxDataDims[1]
		|| aOldDims[2] != m_aMaxDataDims[2]);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTextureFiltering                                         */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyCartesianGridPusher::SetTextureFiltering()
{
	for (int i=0; i<=m_iLookAheadCount; ++i)
	{
		VistaTexture *pTexture = m_vecTextures[i];
		pTexture->Bind();
		switch(m_iInterpolMode)
		{
		case IM_NOT_SET:
			if (m_iTexturePrecision == TP_FLOAT)
			{
				glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER,GL_NEAREST);
				glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER,GL_NEAREST);
			}
			else
			{
				glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER,GL_LINEAR);
				glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			}
			break;
		case IM_NEAREST:
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER,GL_NEAREST);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER,GL_NEAREST);
			break;
		case IM_LINEAR:
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER,GL_LINEAR);
			glTexParameteri(pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			break;
		}
		m_vecTextures[i] = pTexture;
	}
	m_bSetFiltering = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   PushTexture                                                 */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyCartesianGridPusher::PushTexture(int iLevelIndex, int iTarget)
{
	VveCartesianGridItem *pContainer = m_pData->GetTypedLevelDataByLevelIndex(iLevelIndex);
	VveCartesianGrid *pGrid = pContainer->GetData();

	if (pGrid && pGrid->GetValid())
	{
		int iFormat = GL_RGBA;
		int iType = GL_FLOAT;
		void *pData = pGrid->GetData();
		bool bFormatMatch = false;
		if (pGrid->GetDataType() == VveCartesianGrid::DT_FLOAT)
		{
			iType = GL_FLOAT;
			if (m_iTexturePrecision==TP_FLOAT)
				bFormatMatch = true;
		}
		else if (pGrid->GetDataType() == VveCartesianGrid::DT_HALF)
		{
			iType = GL_HALF_FLOAT_ARB;
			if (m_iTexturePrecision==TP_HALF)
				bFormatMatch = true;
		}
		else if (pGrid->GetDataType() == VveCartesianGrid::DT_UNSIGNED_CHAR)
		{
			iType = GL_UNSIGNED_BYTE;
			if (m_iTexturePrecision==TP_UNSIGNED_BYTE)
				bFormatMatch = true;
		}
    else if (pGrid->GetDataType() == VveCartesianGrid::DT_UNSIGNED_SHORT)
		{
			iType = GL_UNSIGNED_SHORT;
			if (m_iTexturePrecision==TP_UNSIGNED_SHORT)
				bFormatMatch = true;
		}

		switch (pGrid->GetComponents())
		{
		case 1:
			iFormat = GL_ALPHA;
			break;
		case 2:
			iFormat = GL_LUMINANCE_ALPHA;
			break;
		case 3:
			iFormat = GL_RGB;
			break;
		case 4:
			iFormat = GL_RGBA;
			break;
		}

		m_vecTextures[iTarget]->Bind();
		int x, y, z;
		pGrid->GetDimensions(x, y, z);

#ifdef USE_PBO
		if (bFormatMatch)
		{
			glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_iPBO);
#if 1
			glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_iBufferSize, pData, GL_STREAM_DRAW);
			//glBufferSubData(GL_PIXEL_UNPACK_BUFFER_ARB, 0, m_iBufferSize, pData, GL_STREAM_DRAW);
#else
			glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_iBufferSize, NULL, GL_STREAM_DRAW);
			void *pMem = glMapBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, GL_WRITE_ONLY);
			memcpy(pMem, pData, m_iBufferSize);
			glUnmapBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB);
#endif
			glTexSubImage3D(m_vecTextures[iTarget]->GetTarget(), 0, 0, 0, 0,
				x, y, z, iFormat, iType, 0);
			glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
		}
		else
		{
			glTexSubImage3D(m_vecTextures[iTarget]->GetTarget(), 0, 0, 0, 0,
				x, y, z, iFormat, iType, pData);
		}
#else
		glTexSubImage3D(m_vecTextures[iTarget]->GetTarget(), 0, 0, 0, 0,
			x, y, z, iFormat, iType, pData);
#endif

		m_vecLevelIndices[iTarget] = iLevelIndex;
	}
//	m_vecDataChanged[iLevelIndex] = false;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
