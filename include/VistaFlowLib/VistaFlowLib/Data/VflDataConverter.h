/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLDATACONVERTER_H
#define _VFLDATACONVERTER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflVisObject.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * VflDataConverter is the parent class for data conversion objects.
 * It is derived from VflVisObject to receive CPU time for data updates.
 * NOTE: Don't forget to call Init(VflVisController *pController, int iRegistration),
 *       otherwise this thing won't be able to work...
 */    
class VISTAFLOWLIBAPI VflDataConverter : public IVflVisObject
{
public:
    VflDataConverter();

    virtual std::string GetType() const;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLDATACONVERTER_H
