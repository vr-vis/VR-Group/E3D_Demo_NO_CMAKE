/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLUNSTEADYTEXTUREPUSHER_H
#define _VFLUNSTEADYTEXTUREPUSHER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>
#include <VistaAspects/VistaReflectionable.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VistaProperty;
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 * This class provides a common interface for objects, which perform data 
 * synchronization between texture data and flow data in main memory.
 */
class VISTAFLOWLIBAPI VflUnsteadyTexturePusher : public VflObserver ,public IVistaReflectionable
{
public:
	virtual ~VflUnsteadyTexturePusher();

	/**
	 * Update the pusher, i.e. synchronize texture and flow data.
	 * Use the RenderNode to determine animation status and 
	 * current vis time.
	 */
	virtual void Update(VflRenderNode *pRenderNode) = 0;

	/**
	 * Set/get number of additional time steps to be transfered
	 * onto the graphics system, i.e. a look-ahead value of 3
	 * means, that the current time step t(i) and the step t(i+1)
	 * and t(i+2) are on the graphics card, whereas t(i+3) is
	 * potentially being uploaded.
	 */
	void SetLookAheadCount(int iCount);
	int GetLookAheadCount() const;

	/**
	 * Toggle wrap-around of time steps, i.e when using the look
	 * ahead, let the time be clamped at the last time step or
	 * make it start at the first time level. So, with wrap-around
	 * you would have time steps t(n-2), t(n-1), t(0), t(1) and
	 * without you'd have t(n-2), t(n-1), t(n-1), t(n-1) for n
	 * time steps and a look-ahead of 3.
	 */
	void SetWrapAround(bool bWrap);
	bool GetWrapAround() const;

	/**
	 * Toggle texture updates during running animation.
	 */
	void SetStreamingUpdate(bool bStreamingUpdate);
	bool GetStreamingUpdate() const;

    /**
	 * Property management via VistaPropertyLists
	 */
	virtual int SetProperty(const VistaProperty &refProp);
	virtual int GetProperty(VistaProperty &refProp);
	virtual int GetPropertySymbolList(std::list<std::string> &rStorageList);
	REFL_INLINEIMP(VflUnsteadyTexturePusher,IVistaReflectionable);
protected:
	VflUnsteadyTexturePusher();

	int		m_iLookAheadCount;
	bool	m_bWrapAround;
	bool	m_bStreamingUpdate;
};

#endif // _VFLUNSTEADYTEXTUREPUSHER_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

