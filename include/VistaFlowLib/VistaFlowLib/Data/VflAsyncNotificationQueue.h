/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLASYNCNOTIFICATIONQUEUE_H
#define _VFLASYNCNOTIFICATIONQUEUE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <queue>
#include <cassert>


#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVistaObserveable;
class VistaSemaphore;
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 * An asynch notification queue is used to synchronize observeable/observer
 * interactions between two threads.
 * One thread (the writer) may issue notifications while the target thread
 * (the reader) has to call the Forward method somewhere in its control
 * flow. Enqueued notifications will be forwarded to their respective targets.
 *
 * @todo Implement this using a lock-free queue!
 *
 */
class VISTAFLOWLIBAPI VflAsyncNotificationQueue
{
public:
	VflAsyncNotificationQueue();
	virtual ~VflAsyncNotificationQueue();

	/**
	 * enqueue an element with the given observeable as target
	 */
	void PushNotification(IVistaObserveable *pObserveable, int iMessage);
	/** 
	 * forward all notifications currently in the queue to their 
	 * respective targets
	 */
	void ForwardNotifications();

	/**
	 * check if the queue has something to forward
	 */
	bool GetHasNotifications();

protected:
	struct SQueueEntry{
		SQueueEntry(IVistaObserveable *pTarget, int iMsg);
		virtual ~SQueueEntry();

		IVistaObserveable *m_pTarget;
		int m_iMsg;
	};
private:
	std::queue<SQueueEntry> m_quData;
	VistaSemaphore *m_pLock;
	bool m_bHasData;
};


#endif // __VFLASYNCNOTIFICATIONQUEUE_H
/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

