

set( RelativeDir "./Visualization/Volume" )
set( RelativeSourceGroup "Source Files\\Visualization\\Volume" )
set( SubDirs shaders)

set( DirFiles
	VflObliqueSlicer.cpp
	VflObliqueSlicer.h
	VflRaycastSlicer.cpp
	VflRaycastSlicer.h
	VflSlice.cpp
	VflSlice.h
	VflTriplanarSlicer.cpp
	VflTriplanarSlicer.h
	VflVisVolume.cpp
	VflVisVolume.h
	VflVisVolumeMagicLensProperties.cpp
	VflVisVolumeMagicLensProperties.h
	VflVisVolumeMultiLensProperties.cpp
	VflVisVolumeMultiLensProperties.h
	VflVisVolumeMultiProperties.cpp
	VflVisVolumeMultiProperties.h
	VflVolumeDataConverter.cpp
	VflVolumeDataConverter.h
	VflVolumeRaycaster.cpp
	VflVolumeRaycaster.h
	VflVolumeRenderer3DTexture.cpp
	VflVolumeRenderer3DTexture.h
	VflVolumeRendererInterface.cpp
	VflVolumeRendererInterface.h
	VflVolumeRendererMagicLens.cpp
	VflVolumeRendererMagicLens.h
	VflVolumeRendererMulti.cpp
	VflVolumeRendererMulti.h
	VflVolumeRendererMultiLens.cpp
	VflVolumeRendererMultiLens.h
	VflVolumeRendererRaycast.cpp
	VflVolumeRendererRaycast.h
	VflVolumeRendererRaycastProxy.cpp
	VflVolumeRendererRaycastProxy.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()
