/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// VistaFlowLib includes
#include "VflVisVolumeMultiLensProperties.h"
#include "../VflVtkLookupTable.h"
#include "../VflLookupTexture.h"
#include <VistaVisExt/Tools/VveUtils.h>

#include <VistaAspects/VistaAspectsUtils.h>
#include <string>
#include <list>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strVflVisVolumeMultiLensPropertiesReflType("VflVisVolumeMultiLensProperties");

static IVistaPropertyGetFunctor *s_aVflVisVolumeMultiLensPropertiesGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aVflVisVolumeMultiLensPropertiesSetFunctors[] =
{
	NULL
};
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/

VflVisVolumeMultiLensPropertiesBase::VflVisVolumeMultiLensPropertiesBase()
{
	for(int i=0; i < 3; ++i)
	{
		VflVtkLookupTable* pTable = new VflVtkLookupTable();
		m_vecMultiLensLookupTable.push_back(pTable);
		m_vecManageMultiLensLut.push_back(true);    
		m_vecMultiLensLookupTexture.push_back(new VflLookupTexture(pTable));
		m_vecManageMultiLensTexture.push_back(true);
	}
}


VflVisVolumeMultiLensPropertiesBase::~VflVisVolumeMultiLensPropertiesBase()
{
	for(int i=0; i < 3; ++i)
	{
		if (m_vecManageMultiLensTexture[i])
			delete m_vecMultiLensLookupTexture[i];

		if (m_vecManageMultiLensLut[i])
			delete m_vecMultiLensLookupTable[i];
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTable                                              */
/*                                                                            */
/*============================================================================*/
VflVtkLookupTable *VflVisVolumeMultiLensPropertiesBase::GetMultiLensLookupTable(const int iChannel) const
{
	if(iChannel >=0 && iChannel < 3)
		return m_vecMultiLensLookupTable[iChannel];
	return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTexture                                            */
/*                                                                            */
/*============================================================================*/
VflLookupTexture *VflVisVolumeMultiLensPropertiesBase::GetMultiLensLookupTexture(const int iChannel) const
{
	if(iChannel >=0 && iChannel < 3)
		return m_vecMultiLensLookupTexture[iChannel];
	return NULL;
}

VflVisVolumeMultiLensProperties::VflVisVolumeMultiLensProperties(VflVtkLookupTable *pLut)
	: VflVisVolume::VflVisVolumeProperties(pLut)
{
}

VflVisVolumeMultiLensProperties::~VflVisVolumeMultiLensProperties()
{}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflVisVolumeMultiLensProperties::GetReflectionableType() const
{
	return s_strVflVisVolumeMultiLensPropertiesReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflVisVolumeMultiLensProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strVflVisVolumeMultiLensPropertiesReflType);
	return i+1;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
