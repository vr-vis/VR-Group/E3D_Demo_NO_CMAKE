/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflVolumeRaycaster.h"

/*============================================================================*/
/*  PROPERTY FUNCTORS AND REFLECTIONABLETYPE                                  */
/*============================================================================*/

static const std::string SsReflectionType("VflVolumeRaycasterProperties");

static IVistaPropertyGetFunctor *aCgFunctors[] = 
{
	new TVistaPropertyGet<float, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("STEP_SIZE", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::GetStepSize),
	new TVistaPropertyGet<int, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("TEXTURE_INTERPOLATION_MODE", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::GetTextureInterpolationMode),
	new TVistaPropertyGet<float, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("ALPHA_SATURATION", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::GetAlphaSaturationLimit),
	new TVistaPropertyGet<float, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("ALPHA_COMPENSATION", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::GetAlphaCompensationFactor),
	new TVistaPropertyGet<bool, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("LIGHTING", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::GetIsLightingActive),
	new TVistaPropertyGet<bool, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("DEPTH_TEST", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::GetPerformDepthTest),
	new TVistaPropertyGet<bool, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("RAY_JITTERING", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::GetPerformRayJittering),
	NULL //<* terminating array
};


static IVistaPropertySetFunctor *aCsFunctors[] =
{   
	new TVistaPropertySet<float, bool, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("STEP_SIZE", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::SetStepSize),
	new TVistaPropertySet<int, bool, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("TEXTURE_INTERPOLATION_MODE", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::SetTextureInterpolationMode),
	new TVistaPropertySet<float, bool, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("ALPHA_SATURATION", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::SetAlphaSaturationLimit),
	new TVistaPropertySet<float, bool, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("ALPHA_COMPENSATION", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::SetAlphaCompensationFactor),
	new TVistaPropertySet<bool , bool, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("LIGHTING", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::SetIsLightingActive),
	new TVistaPropertySet<bool , bool, IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("DEPTH_TEST", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::SetPerformDepthTest),
	new TVistaPropertySet<bool , bool,IVflVolumeRaycaster::VflVolumeRaycasterProperties>
		("RAY_JITTERING", SsReflectionType,
		&IVflVolumeRaycaster::VflVolumeRaycasterProperties::SetPerformRayJittering),
	NULL //<* terminating array
};




IVflVolumeRaycaster::IVflVolumeRaycaster()
	:	m_pRaycasterCore( new VistaVolumeRaycasterCore() )	
{
	

}


IVflVolumeRaycaster::~IVflVolumeRaycaster()
{
	delete m_pRaycasterCore;
}


/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/


IVflVolumeRaycaster::VflVolumeRaycasterProperties* IVflVolumeRaycaster::GetProperties() const
{
		return static_cast<IVflVolumeRaycaster::VflVolumeRaycasterProperties*>(
			IVflRenderable::GetProperties());
}


IVflVisObject::VflVisObjProperties *IVflVolumeRaycaster::CreateProperties() const
{
	return new IVflVolumeRaycaster::VflVolumeRaycasterProperties();
	
}

/*============================================================================*/
/*  PROPERTY INLINE MEMBERS                                                   */
/*============================================================================*/

IVflVolumeRaycaster::VflVolumeRaycasterProperties::VflVolumeRaycasterProperties()
	: m_iTextureInterpolationMode( GL_LINEAR )
{

}


IVflVolumeRaycaster::VflVolumeRaycasterProperties::~VflVolumeRaycasterProperties()
{

}

int IVflVolumeRaycaster::VflVolumeRaycasterProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = VflVisObjProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}

std::string IVflVolumeRaycaster::VflVolumeRaycasterProperties::GetReflectionableType() const
{
	return SsReflectionType;
}


bool IVflVolumeRaycaster::VflVolumeRaycasterProperties::SetTextureInterpolationMode( const int iMode)
{
	if( iMode == GL_NEAREST || iMode == GL_LINEAR )
	{
		m_iTextureInterpolationMode = iMode;
		return true;
	}
	return false;
}

/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

