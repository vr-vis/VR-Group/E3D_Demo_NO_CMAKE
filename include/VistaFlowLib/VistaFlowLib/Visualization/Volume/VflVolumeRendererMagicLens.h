/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVOLUMERENDERERMAGIC_LENS_H
#define _VFLVOLUMERENDERERMAGIC_LENS_H

#ifdef WIN32
	#pragma warning (disable: 4786)
#endif

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflVolumeRendererInterface.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>
#include "VflVolumeRendererRaycast.h"


#include <list>
#include <string>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaGLSLShader;
class VflLookupTexture;
class VflVtkLookupTable; 

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 */

class VISTAFLOWLIBAPI VflVolumeRendererMagicLens : public VflVolumeRendererRaycast
{
public:
	VflVolumeRendererMagicLens(VflVisVolume *pParent,const int width, const int height, const float near_plane);
	virtual ~VflVolumeRendererMagicLens();

	virtual void Update();
	virtual void DrawOpaque();
	virtual void DrawTransparent();
	virtual void Draw2D();

	virtual bool IsValid() const;
	virtual std::string GetReflectionableType() const;

protected:
	VflVolumeRendererMagicLens();
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	// validity information
	bool m_bValid;
	bool m_bValidData;
  
  // updates offscreen buffers
  virtual void UpdateBuffer(const int iVolId, const int iTfId, const float aRange[2], const float aRenderParams[2]);

  // draw content of offscreen buffer 
  virtual void DrawBuffer(const unsigned int iWhich);

  VistaGLSLShader* m_pMagicLensShader;
};

extern const char* cMagiclensFragmentSourceLens;
extern const char* cMagiclensFragmentSourceHeader;
extern const char* cMagiclensFragmentSourceComposite;

#endif //_VFLVOLUMERENDERERMAGIC_LENS_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

