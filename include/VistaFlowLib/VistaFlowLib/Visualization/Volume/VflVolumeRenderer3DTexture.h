/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVOLUMERENDERER3DTEXTURE_H
#define _VFLVOLUMERENDERER3DTEXTURE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflVolumeRendererInterface.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <vector>
#include <string>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaGLSLShader;

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 */
class VISTAFLOWLIBAPI VflVolumeRenderer3DTexture : public IVflVolumeRenderer
{
public:
	VflVolumeRenderer3DTexture(VflVisVolume *pParent);
	virtual ~VflVolumeRenderer3DTexture();

	virtual void Update();
	virtual void DrawOpaque();
	virtual void DrawTransparent();
	virtual void Draw2D();

	virtual bool IsValid() const;
	virtual std::string GetReflectionableType() const;

protected:
	VflVolumeRenderer3DTexture();
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	// bounding volume corners
	float m_aVertices[8][3];

	// distance information
	int m_iMaxDistIdx;
	float m_fMinDist, m_fMaxDist;

	// tex coord scale and bias
	float m_aTCScale[3];
	float m_aTCBias[3];

	// viewing information
	VistaVector3D	m_v3ViewPos;
	VistaVector3D	m_v3ViewDir;


	// validity information
	bool m_bValid;
	bool m_bValidData;

	// helper structures
	static const int m_aEdgeList[8][12];
	static const int m_aEdges[12][2];

	VistaGLSLShader *m_pShader;
	VistaGLSLShader *m_pPreIntShader;
};


#endif // _VFLVOLUMERENDERER3DTEXTURE_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

