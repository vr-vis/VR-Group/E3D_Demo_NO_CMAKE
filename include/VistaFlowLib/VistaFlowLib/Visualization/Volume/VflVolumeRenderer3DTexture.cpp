/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflVolumeRenderer3DTexture.h"
#include "VflVisVolume.h"

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include "../VflVisTiming.h"
#include <VistaVisExt/Data/VveCartesianGrid.h>
#include "../../Data/VflUnsteadyCartesianGridPusher.h"
#include "../VflVtkLookupTable.h"
#include "../VflLookupTexture.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaShaderRegistry.h>
#include <vtkLookupTable.h>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
inline static float SquaredDist(float *v1, float *v2);
inline static float ScalarMult(float *v1, float *v2);
inline static void MultiplyAdd(float *pResult, float fScalar, float *v1, float *v2);
inline static void MultiplyAdd(float *pResult, float *v1, float *v2, float *v3);

static std::string s_strCVflVolumeRenderer3DTextureReflType("VflVolumeRenderer3DTexture");

const int VflVolumeRenderer3DTexture::m_aEdgeList[8][12] = {
	{ 0,1,5,6,   4,8,11,9,  3,7,2,10 }, // v0 is front
	{ 0,4,3,11,  1,2,6,7,   5,9,8,10 }, // v1 is front
	{ 1,5,0,8,   2,3,7,4,   6,10,9,11}, // v2 is front
	{ 7,11,10,8, 2,6,1,9,   3,0,4,5  }, // v3 is front
	{ 8,5,9,1,   11,10,7,6, 4,3,0,2  }, // v4 is front
	{ 9,6,10,2,  8,11,4,7,  5,0,1,3  }, // v5 is front
	{ 9,8,5,4,   6,1,2,0,   10,7,11,3}, // v6 is front
	{ 10,9,6,5,  7,2,3,1,   11,4,8,0 }  // v7 is front
};

const int VflVolumeRenderer3DTexture::m_aEdges[12][2] =
{
	{ 0, 1 },
	{ 1, 2 },
	{ 2, 3 },
	{ 3, 0 },
	{ 0, 4 },
	{ 1, 5 },
	{ 2, 6 },
	{ 3, 7 },
	{ 4, 5 },
	{ 5, 6 },
	{ 6, 7 },
	{ 7, 4 }
};

static IVistaPropertyGetFunctor *s_aCVflVolumeRenderer3DTextureGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aCVflVolumeRenderer3DTextureSetFunctors[] =
{
	NULL
};

#define SHADER_VOLUME_VS	"VflVolumeRenderer3DTexture_vert.glsl"
#define SHADER_VOLUME_FS	"VflVolumeRenderer3DTexture_frag.glsl"

#define SHADER_VOLUME_PREINT_VS	"VflVolumeRenderer3DTexture_PreInt_vert.glsl"
#define SHADER_VOLUME_PREINT_FS	"VflVolumeRenderer3DTexture_PreInt_frag.glsl"

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflVolumeRenderer3DTexture::VflVolumeRenderer3DTexture(VflVisVolume *pParent)
: IVflVolumeRenderer(pParent),
m_bValid(false),
m_bValidData(false)
{
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();

	m_pShader = new VistaGLSLShader;

	std::string strVertexShader    = rShaderReg.RetrieveShader( SHADER_VOLUME_VS );
	std::string strFragmentShader  = rShaderReg.RetrieveShader( SHADER_VOLUME_FS );

	if ( strVertexShader.empty() || strFragmentShader.empty() )
	{
		vstr::errp() << "[VflVolumeRenderer3DTex] shaders not available..." << std::endl;
		return;
	}
	if (!m_pShader->InitFromStrings(strVertexShader, strFragmentShader))
	{
		vstr::errp() << "[VflVolumeRenderer3DTex] unable to init shader..." << std::endl;
		delete m_pShader;
		m_pShader = NULL;
		return;
	}
	if (!m_pShader->Link())
	{
		vstr::errp() << "[VflVolumeRenderer3DTex] unable to link shader..." << std::endl;
		delete m_pShader;
		m_pShader = NULL;
		return;
	}
	m_pShader->Bind();
	m_pShader->SetUniform(m_pShader->GetUniformLocation("texUnitVolume"), 0);
	m_pShader->SetUniform(m_pShader->GetUniformLocation("texUnitLookupTable"), 1);
	m_pShader->SetUniform(m_pShader->GetUniformLocation("texUnitOpacityCorrection"), 2);
	m_pShader->Release();


	/// load shader for preintegration
	m_pPreIntShader = new VistaGLSLShader;
	//string strVertexShader, strFragmentShader;
	//VistaProgramPool *pPP = VistaProgramPool::GetProgramPool();
	strVertexShader    = rShaderReg.RetrieveShader( SHADER_VOLUME_PREINT_VS );
	strFragmentShader  = rShaderReg.RetrieveShader( SHADER_VOLUME_PREINT_FS );
	if ( strVertexShader.empty() )
	{
		vstr::errp() << " [VflVolumeRenderer3DTex] preint VS shaders not available..." << std::endl;
		return;
	}
	if(	strFragmentShader.empty())
	{
		vstr::errp() << " [VflVolumeRenderer3DTex] preint FS shaders not available..." << std::endl;
		return;
	}

	if (!m_pPreIntShader->InitFromStrings(strVertexShader, strFragmentShader))
	{
		vstr::errp() << " [VflVolumeRenderer3DTex] unable to init shader..." << endl;
		delete m_pPreIntShader;
		m_pPreIntShader = NULL;
		return;
	}
	if (!m_pPreIntShader->Link())
	{
		vstr::errp() << " [VflVolumeRenderer3DTex] unable to link shader..." << std::endl;
		delete m_pPreIntShader;
		m_pPreIntShader = NULL;
		return;
	}
	m_pPreIntShader->Bind();
	m_pPreIntShader->SetUniform(m_pPreIntShader->GetUniformLocation("texUnitVolume"), 0);
	m_pPreIntShader->SetUniform(m_pPreIntShader->GetUniformLocation("texUnitLookupTable"), 1);
	m_pPreIntShader->SetUniform(m_pPreIntShader->GetUniformLocation("texUnitOpacityCorrection"), 2);
	m_pPreIntShader->SetUniform(m_pPreIntShader->GetUniformLocation("texUnitPreIntegration"), 3);
	m_pPreIntShader->Release();


	m_bValid = true;
}

VflVolumeRenderer3DTexture::~VflVolumeRenderer3DTexture()
{
	delete m_pShader;
	m_pShader = NULL;

	if(!m_pPreIntShader)
	{
		delete m_pPreIntShader;
		m_pPreIntShader = NULL;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflVolumeRenderer3DTexture::Update()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;

	// determine dataset boundaries for current time step
	float fVisTime = m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	VveUnsteadyCartesianGrid *pUGrid = m_pParent->GetData();
	VveTimeMapper *pTimeMapper = pUGrid->GetTimeMapper();
	int iIndex = pTimeMapper->GetLevelIndex(fVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveCartesianGrid *pGrid = pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		m_bValidData = false;
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return;
	}
	m_bValidData = true;
	VistaVector3D v3BoundsMin, v3BoundsMax;
	pGrid->GetBounds(v3BoundsMin, v3BoundsMax);

	// find out about axis-dependent scaling factors (due to data padding)
	int aDims[3];
	int aDataDims[3];

	pGrid->GetDimensions(aDims);
	pGrid->GetDataDimensions(aDataDims);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	// compute tex coord scale and bias
	float aTemp1[3];
	aTemp1[0] = aDataDims[0]*aDataDims[0] / (v3BoundsMax[0]-v3BoundsMin[0]) / aDims[0];
	aTemp1[1] = aDataDims[1]*aDataDims[1] / (v3BoundsMax[1]-v3BoundsMin[1]) / aDims[1];
	aTemp1[2] = aDataDims[2]*aDataDims[2] / (v3BoundsMax[2]-v3BoundsMin[2]) / aDims[2];

	float aTemp2[3];
	aTemp2[0] = 1.0f / (aDataDims[0]-1.0f);
	aTemp2[1] = 1.0f / (aDataDims[1]-1.0f);
	aTemp2[2] = 1.0f / (aDataDims[2]-1.0f);

	m_aTCScale[0] = aTemp1[0] * aTemp2[0];
	m_aTCScale[1] = aTemp1[1] * aTemp2[1];
	m_aTCScale[2] = aTemp1[2] * aTemp2[2];

	m_aTCBias[0] = -(v3BoundsMin[0]*aTemp1[0] + 0.5f) * aTemp2[0];
	m_aTCBias[1] = -(v3BoundsMin[1]*aTemp1[1] + 0.5f) * aTemp2[1];
	m_aTCBias[2] = -(v3BoundsMin[2]*aTemp1[2] + 0.5f) * aTemp2[2];

	// compute corner vertices
	//                     e2       
	//		   (v3)o----------------o(v2)    
	//            /|               /|
	//           / |              / |
	//       e7 /  |          e6 /  |e1
	//         /   |   e10      /   |
	//	  (v7)o----------------o(v6)|
	//        |    |           |    |
	//        |    |e3         |    |
	//     e11|    |           |e9  |
	//        |    |    e0     |    |
	//        |    o-----------|----o(v1) 
	//        |   / (v0)       |   /
	//        |  /             |  /
	//        | /e4            | /e5
	//        |/               |/
	//   y    o----------------o
	//   |   (v4)    e8       (v5)
	//   |
	//   +---x
	//  /
	// z
	m_aVertices[0][0] = m_aVertices[3][0] = m_aVertices[4][0] = m_aVertices[7][0] = v3BoundsMin[0];
	m_aVertices[0][1] = m_aVertices[1][1] = m_aVertices[4][1] = m_aVertices[5][1] = v3BoundsMin[1];
	m_aVertices[0][2] = m_aVertices[1][2] = m_aVertices[2][2] = m_aVertices[3][2] = v3BoundsMin[2];

	m_aVertices[1][0] = m_aVertices[2][0] = m_aVertices[5][0] = m_aVertices[6][0] = v3BoundsMax[0];
	m_aVertices[2][1] = m_aVertices[3][1] = m_aVertices[6][1] = m_aVertices[7][1] = v3BoundsMax[1];
	m_aVertices[4][2] = m_aVertices[5][2] = m_aVertices[6][2] = m_aVertices[7][2] = v3BoundsMax[2];

	m_v3ViewPos = m_pParent->GetRenderNode()->GetLocalViewPosition();
	//VistaVector3D v3Center(0.5f*(v3BoundsMin+v3BoundsMax));
	//m_v3ViewDir = v3Center-m_v3ViewPos;
	//m_v3ViewDir.Normalize();
	m_pParent->GetRenderNode()->GetLocalViewDirection(m_v3ViewDir);


	//	float fViewerDistance = m_v3ViewPos * m_v3ViewDir;
	m_fMinDist = m_fMaxDist = ScalarMult(&m_v3ViewDir[0], m_aVertices[0]);
	m_iMaxDistIdx = 0;
	float fDist;

	for (int i=1; i<8; ++i)
	{
		fDist = ScalarMult(&m_v3ViewDir[0], m_aVertices[i]);
		if (fDist>m_fMaxDist)
		{
			m_fMaxDist = fDist;
			m_iMaxDistIdx = i;
		}
		else if (fDist<m_fMinDist)
			m_fMinDist = fDist;
	}

	float fOffset = 0.5f * pProperties->GetSamplingRate();
	m_fMinDist += fOffset;
	m_fMaxDist -= fOffset;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVolumeRenderer3DTexture::DrawOpaque()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;

	if (!m_bValidData)
		return;

	if (!pProperties->GetDrawBounds())
		return;

	// draw bounding box
	glPushAttrib(GL_ENABLE_BIT | GL_POINT_BIT | GL_LINE_BIT | GL_POLYGON_BIT);
	glDisable(GL_LIGHTING);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_CULL_FACE);

	glLineWidth(2.0f);
	float fR, fG, fB;
	pProperties->GetBoundColor(fR, fG, fB);
	glColor4f(fR, fG, fB, 1.0f);

	glBegin(GL_LINES);
	for (int i=0; i<12; ++i)
	{
		glVertex3fv(m_aVertices[m_aEdges[i][0]]);
		glVertex3fv(m_aVertices[m_aEdges[i][1]]);
	}
	glEnd();

	if (!pProperties->GetDrawSlices())
	{
		glPopAttrib();
		return;
	}

	// draw slices...
	float fPlaneDist = m_fMinDist;
	float fSamplingRate = m_pParent->GetProperties()->GetSamplingRate();
	int iPlaneCount = static_cast<int>(floor((m_fMaxDist-m_fMinDist) / fSamplingRate));
	float aVertices[12*3];
	float aEdges[12*3];
	float aLambda[12];
	float aLambdaInc[12];
	float *aStart[12];
	float aDir[12][3];
	float fDenom;


	for (int i=0; i<12; ++i)
	{
		aStart[i] = m_aVertices[m_aEdges[m_aEdgeList[m_iMaxDistIdx][i]][0]];
		float *pEnd = m_aVertices[m_aEdges[m_aEdgeList[m_iMaxDistIdx][i]][1]];
		aDir[i][0] = pEnd[0]-aStart[i][0];
		aDir[i][1] = pEnd[1]-aStart[i][1];
		aDir[i][2] = pEnd[2]-aStart[i][2];

		fDenom = ScalarMult(aDir[i], &m_v3ViewDir[0]);
		if (fDenom + 1.0f != fDenom)
		{
			aLambda[i] = (fPlaneDist-ScalarMult(&m_v3ViewDir[0], aStart[i])) / fDenom;
			aLambdaInc[i] = fSamplingRate / fDenom;
		}
		else
		{
			aLambda[i] = -1.0f;
			aLambdaInc[i] = 0;
		}

		memcpy(&aVertices[3*i], aStart[i], 3*sizeof(float));
		memcpy(&aEdges[3*i], aDir[i], 3*sizeof(float));
	}

	float aIntersection[6][3];
	float aCurrentLambda[12];

	for (int i=iPlaneCount-1; i>=0; --i)
	{
		for (int j=0; j<12; ++j)
			aCurrentLambda[j] = aLambda[j] + i*aLambdaInc[j];

		if (aCurrentLambda[0]>=0 && aCurrentLambda[0]<1.0f)
			MultiplyAdd(aIntersection[0], aCurrentLambda[0], aDir[0], aStart[0]);
		else if (aCurrentLambda[1]>=0 && aCurrentLambda[1]<1.0f)
			MultiplyAdd(aIntersection[0], aCurrentLambda[1], aDir[1], aStart[1]);
		else if (aCurrentLambda[3]>=0 && aCurrentLambda[3]<1.0f)
			MultiplyAdd(aIntersection[0], aCurrentLambda[3], aDir[3], aStart[3]);
		else continue;

		if (aCurrentLambda[2]>=0 && aCurrentLambda[2]<1.0f)
			MultiplyAdd(aIntersection[1], aCurrentLambda[2], aDir[2], aStart[2]);
		else if (aCurrentLambda[0]>=0 && aCurrentLambda[0]<1.0f)
			MultiplyAdd(aIntersection[1], aCurrentLambda[0], aDir[0], aStart[0]);
		else if (aCurrentLambda[1]>=0 && aCurrentLambda[1]<1.0f)
			MultiplyAdd(aIntersection[1], aCurrentLambda[1], aDir[1], aStart[1]);
		else MultiplyAdd(aIntersection[1], aCurrentLambda[3], aDir[3], aStart[3]);

		if (aCurrentLambda[4]>=0 && aCurrentLambda[4]<1.0f)
			MultiplyAdd(aIntersection[2], aCurrentLambda[4], aDir[4], aStart[4]);
		else if (aCurrentLambda[5]>=0 && aCurrentLambda[5]<1.0f)
			MultiplyAdd(aIntersection[2], aCurrentLambda[5], aDir[5], aStart[5]);
		else MultiplyAdd(aIntersection[2], aCurrentLambda[7], aDir[7], aStart[7]);

		if (aCurrentLambda[6]>=0 && aCurrentLambda[6]<1.0f)
			MultiplyAdd(aIntersection[3], aCurrentLambda[6], aDir[6], aStart[6]);
		else if (aCurrentLambda[4]>=0 && aCurrentLambda[4]<1.0f)
			MultiplyAdd(aIntersection[3], aCurrentLambda[4], aDir[4], aStart[4]);
		else if (aCurrentLambda[5]>=0 && aCurrentLambda[5]<1.0f)
			MultiplyAdd(aIntersection[3], aCurrentLambda[5], aDir[5], aStart[5]);
		else MultiplyAdd(aIntersection[3], aCurrentLambda[7], aDir[7], aStart[7]);

		if (aCurrentLambda[8]>=0 && aCurrentLambda[8]<1.0f)
			MultiplyAdd(aIntersection[4], aCurrentLambda[8], aDir[8], aStart[8]);
		else if (aCurrentLambda[9]>=0 && aCurrentLambda[9]<1.0f)
			MultiplyAdd(aIntersection[4], aCurrentLambda[9], aDir[9], aStart[9]);
		else MultiplyAdd(aIntersection[4], aCurrentLambda[11], aDir[11], aStart[11]);

		if (aCurrentLambda[10]>=0 && aCurrentLambda[10]<1.0f)
			MultiplyAdd(aIntersection[5], aCurrentLambda[10], aDir[10], aStart[10]);
		else if (aCurrentLambda[8]>=0 && aCurrentLambda[8]<1.0f)
			MultiplyAdd(aIntersection[5], aCurrentLambda[8], aDir[8], aStart[8]);
		else if (aCurrentLambda[9]>=0 && aCurrentLambda[9]<1.0f)
			MultiplyAdd(aIntersection[5], aCurrentLambda[9], aDir[9], aStart[9]);
		else MultiplyAdd(aIntersection[5], aCurrentLambda[11], aDir[11], aStart[11]);


		float aTemp[3];
		glBegin(GL_LINE_LOOP);
		for (int j=0; j<6; ++j)
		{
#if 1
			MultiplyAdd(aTemp, m_aTCScale, aIntersection[j], m_aTCBias);
			glColor3fv(aTemp);
#else
			glColor3f(1.0f-(j/7.0f), 1.0f-(j/7.0f), 1.0f-(j/7.0f));
#endif
			glVertex3fv(aIntersection[j]);
		}
		glEnd();
	}

	VistaVector3D v3LookAt(m_v3ViewPos + 2*m_v3ViewDir);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0, 0);
	glVertex3fv(&m_v3ViewPos[0]);
	glVertex3fv(&v3LookAt[0]);
	glEnd();

	glPointSize(10.0f);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 0);
	glVertex3fv(m_aVertices[m_iMaxDistIdx]);
	glEnd();

	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVolumeRenderer3DTexture::DrawTransparent()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;

	if (!m_bValidData)
		return;

	if (!pProperties->GetVisible() || pProperties->GetDrawSlices())
		return;

	// check for preintegration, switch shader accordingly
	VistaGLSLShader * pCurrentShader = m_pShader;

	if(pProperties->GetPreIntegration())
		pCurrentShader = m_pPreIntShader;

	// save OpenGL state
	glPushAttrib(GL_ENABLE_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_LIGHTING);
	glEnable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_CULL_FACE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(false);


	// activate shader
	pCurrentShader->Bind();

	// set scale and bias for texture coordinates
	pCurrentShader->SetUniform(pCurrentShader->GetUniformLocation("v3TCScale"), 3, 1, m_aTCScale);
	pCurrentShader->SetUniform(pCurrentShader->GetUniformLocation("v3TCBias"), 3, 1, m_aTCBias);

	if(pProperties->GetPreIntegration())
	{
		// setup preintegration specific stuff

		// bind preint texture
		VistaTexture *pPreInt = m_pParent->GetProperties()->GetLookupTexture()->GetPreIntegrationTexture();
		pPreInt->Bind(GL_TEXTURE3);

		// step size
		const float slice_distance = pProperties->GetSamplingRate();
		m_pPreIntShader->SetUniform(m_pPreIntShader->GetUniformLocation("slice_distance"), slice_distance);

	}

	// bind lookup texture
	VistaTexture *pLut = m_pParent->GetProperties()->GetLookupTexture()->GetLookupTexture();
	pLut->Bind(GL_TEXTURE1);
	double aRange[2];
	m_pParent->GetProperties()->GetLookupTable()->GetLookupTable()->GetTableRange(aRange);
	pCurrentShader->SetUniform(pCurrentShader->GetUniformLocation("v2RangeParams"), 
		1.0f/(aRange[1]-aRange[0]), -aRange[0]/(aRange[1]-aRange[0]));

	// add opacity correction
	m_pParent->GetOpacityCorrectionTexture()->Bind(GL_TEXTURE2);
	pCurrentShader->SetUniform(pCurrentShader->GetUniformLocation("v2BlendingCorrection"),
		pProperties->GetBlendingFactor(), pProperties->GetSamplingRate());

	// finally, bind data texture
	m_pParent->GetPusher()->GetTexture()->Bind(GL_TEXTURE0);

	// draw slices...
	float fPlaneDist = m_fMinDist;
	float fSamplingRate = m_pParent->GetProperties()->GetSamplingRate();
	int iPlaneCount = static_cast<int>(floor((m_fMaxDist-m_fMinDist) / fSamplingRate));
	float aLambda[12];
	float aLambdaInc[12];
	float *aStart[12];
	float aDir[12][3];
	float fDenom;


	for (int i=0; i<12; ++i)
	{
		aStart[i] = m_aVertices[m_aEdges[m_aEdgeList[m_iMaxDistIdx][i]][0]];
		float *pEnd = m_aVertices[m_aEdges[m_aEdgeList[m_iMaxDistIdx][i]][1]];
		aDir[i][0] = pEnd[0]-aStart[i][0];
		aDir[i][1] = pEnd[1]-aStart[i][1];
		aDir[i][2] = pEnd[2]-aStart[i][2];

		fDenom = ScalarMult(aDir[i], &m_v3ViewDir[0]);
		if (fDenom + 1.0f != fDenom)
		{
			aLambda[i] = (fPlaneDist-ScalarMult(&m_v3ViewDir[0], aStart[i])) / fDenom;
			aLambdaInc[i] = fSamplingRate / fDenom;
		}
		else
		{
			aLambda[i] = -1.0f;
			aLambdaInc[i] = 0;
		}
	}

	float aIntersection[6][3];
	float aCurrentLambda[12];

	for (int i=iPlaneCount-1; i>=0; --i)
	{
		for (int j=0; j<12; ++j)
			aCurrentLambda[j] = aLambda[j] + i*aLambdaInc[j];

		if (aCurrentLambda[0]>=0 && aCurrentLambda[0]<1.0f)
			MultiplyAdd(aIntersection[0], aCurrentLambda[0], aDir[0], aStart[0]);
		else if (aCurrentLambda[1]>=0 && aCurrentLambda[1]<1.0f)
			MultiplyAdd(aIntersection[0], aCurrentLambda[1], aDir[1], aStart[1]);
		else if (aCurrentLambda[3]>=0 && aCurrentLambda[3]<1.0f)
			MultiplyAdd(aIntersection[0], aCurrentLambda[3], aDir[3], aStart[3]);
		else continue;

		if (aCurrentLambda[2]>=0 && aCurrentLambda[2]<1.0f)
			MultiplyAdd(aIntersection[1], aCurrentLambda[2], aDir[2], aStart[2]);
		else if (aCurrentLambda[0]>=0 && aCurrentLambda[0]<1.0f)
			MultiplyAdd(aIntersection[1], aCurrentLambda[0], aDir[0], aStart[0]);
		else if (aCurrentLambda[1]>=0 && aCurrentLambda[1]<1.0f)
			MultiplyAdd(aIntersection[1], aCurrentLambda[1], aDir[1], aStart[1]);
		else MultiplyAdd(aIntersection[1], aCurrentLambda[3], aDir[3], aStart[3]);

		if (aCurrentLambda[4]>=0 && aCurrentLambda[4]<1.0f)
			MultiplyAdd(aIntersection[2], aCurrentLambda[4], aDir[4], aStart[4]);
		else if (aCurrentLambda[5]>=0 && aCurrentLambda[5]<1.0f)
			MultiplyAdd(aIntersection[2], aCurrentLambda[5], aDir[5], aStart[5]);
		else MultiplyAdd(aIntersection[2], aCurrentLambda[7], aDir[7], aStart[7]);

		if (aCurrentLambda[6]>=0 && aCurrentLambda[6]<1.0f)
			MultiplyAdd(aIntersection[3], aCurrentLambda[6], aDir[6], aStart[6]);
		else if (aCurrentLambda[4]>=0 && aCurrentLambda[4]<1.0f)
			MultiplyAdd(aIntersection[3], aCurrentLambda[4], aDir[4], aStart[4]);
		else if (aCurrentLambda[5]>=0 && aCurrentLambda[5]<1.0f)
			MultiplyAdd(aIntersection[3], aCurrentLambda[5], aDir[5], aStart[5]);
		else MultiplyAdd(aIntersection[3], aCurrentLambda[7], aDir[7], aStart[7]);

		if (aCurrentLambda[8]>=0 && aCurrentLambda[8]<1.0f)
			MultiplyAdd(aIntersection[4], aCurrentLambda[8], aDir[8], aStart[8]);
		else if (aCurrentLambda[9]>=0 && aCurrentLambda[9]<1.0f)
			MultiplyAdd(aIntersection[4], aCurrentLambda[9], aDir[9], aStart[9]);
		else MultiplyAdd(aIntersection[4], aCurrentLambda[11], aDir[11], aStart[11]);

		if (aCurrentLambda[10]>=0 && aCurrentLambda[10]<1.0f)
			MultiplyAdd(aIntersection[5], aCurrentLambda[10], aDir[10], aStart[10]);
		else if (aCurrentLambda[8]>=0 && aCurrentLambda[8]<1.0f)
			MultiplyAdd(aIntersection[5], aCurrentLambda[8], aDir[8], aStart[8]);
		else if (aCurrentLambda[9]>=0 && aCurrentLambda[9]<1.0f)
			MultiplyAdd(aIntersection[5], aCurrentLambda[9], aDir[9], aStart[9]);
		else MultiplyAdd(aIntersection[5], aCurrentLambda[11], aDir[11], aStart[11]);

		glBegin(GL_TRIANGLE_FAN);
		for (int j=0; j<6; ++j)
		{
			glVertex3fv(aIntersection[j]);
		}
		glEnd();
	}

	// release shader
	pCurrentShader->Release();

	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void VflVolumeRenderer3DTexture::Draw2D()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflVolumeRenderer3DTexture::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflVolumeRenderer3DTexture::GetReflectionableType() const
{
	return s_strCVflVolumeRenderer3DTextureReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflVolumeRenderer3DTexture::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCVflVolumeRenderer3DTextureReflType);
	return i+1;
}

/*============================================================================*/
/*  LOCAL FUNCTIONS                                                           */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SquaredDist                                                 */
/*                                                                            */
/*============================================================================*/
inline static float SquaredDist(float *v1, float *v2)
{
	float fDiff[3];
	fDiff[0] = v1[0]-v2[0];
	fDiff[1] = v1[1]-v2[1];
	fDiff[2] = v1[2]-v2[2];

	return fDiff[0]*fDiff[0] + fDiff[1]*fDiff[1] + fDiff[2]*fDiff[2];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ScalarMult                                                  */
/*                                                                            */
/*============================================================================*/
inline static float ScalarMult(float *v1, float *v2)
{
	return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   MultiplyAdd                                                 */
/*                                                                            */
/*============================================================================*/
inline static void MultiplyAdd(float *pResult, float fScalar, float *v1, float *v2)
{
	pResult[0] = fScalar * v1[0] + v2[0];
	pResult[1] = fScalar * v1[1] + v2[1];
	pResult[2] = fScalar * v1[2] + v2[2];
}

inline static void MultiplyAdd(float *pResult, float *v1, float *v2, float *v3)
{
	pResult[0] = v1[0] * v2[0] + v3[0];
	pResult[1] = v1[1] * v2[1] + v3[1];
	pResult[2] = v1[2] * v2[2] + v3[2];
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
