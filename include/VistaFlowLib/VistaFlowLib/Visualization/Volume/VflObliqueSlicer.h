/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLOBLIQUESLICER_H
#define _VFLOBLIQUESLICER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflSlice.h"
#include "../VflVisObject.h"
#include "../VflRenderNode.h"

#include <VistaVisExt/Data/VveVtkData.h>
#include <VistaVisExt/Data/VveCartesianGrid.h>

#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <vector>
#include <list>
#include <string>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;
class VistaGLSLShader;
class VistaColor;
class VflLookupTexture;
class VflVtkLookupTable;
class IVflLookupTable;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflObliqueSlicer : public IVflVisObject
{
public:
	VflObliqueSlicer();
	virtual ~VflObliqueSlicer();

	/**
	 *  IVflRenderable interface
	 * SetData first !!
	 */
	virtual bool Init();

    /**
	 * IVflRenderable interface
	 */
	virtual void DrawOpaque();

	/**
	 * Observer interface
	 */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, 
								int msg, int ticket);


	/**
	 * Set/Get data (todo: use cartesian grid)
	 */
	bool SetData(VveUnsteadyCartesianGrid *pData);
	VveUnsteadyCartesianGrid* GetData();

	/**
	 * Set/Get center of the slicer plane
	 */
	void SetCenter(const VistaVector3D &v3dPos);
	void SetCenter(const float fCenter[3]);
	void GetCenter(float (&fCenter)[3]);
	VistaVector3D GetCenter();

	/**
	 * Set/Get normal of the cutting plane
	 */
	void SetNormal(const VistaVector3D &v3dNormal);
	void SetNormal(const float fNormal[3]);
	VistaVector3D GetNormal();
	void GetNormal(float (&fNormal)[3]);

	/**
	 *
	 */
	void SetDimensions(const float fW, const float fH);
	void GetDimensions(float &fW, float &fH);

	/**
	 *
	 */
	void SetRotation(const float fR);
	float GetRotation();

	/**
	 *
	 */
	void SetPoints(const VistaVector3D v3dPoints[4]);
	void SetPoints(const VistaVector3D v3dPoint1, const VistaVector3D v3dPoint2,
				   const VistaVector3D v3dPoint3, const VistaVector3D v3dPoint4);
	void SetPoints(const float fPoints[12]);
	void SetPoints(const float fPoint1[3], const float fPoint2[3],
				   const float fPoint3[3], const float fPoint4[3]);

	/**
	 *
	 */
	void GetPoints(VistaVector3D (&v3dPoints)[4]);
	void GetPoints(VistaVector3D v3dPoint1, VistaVector3D v3dPoint2,
				   VistaVector3D v3dPoint3, VistaVector3D v3dPoint4);
	void GetPoints(float (&fPoints)[12]);
	void GetPoints(float (&fPoint1)[3], float (&fPoint2)[3],
				   float (&fPoint3)[3], float (&fPoint4)[3]);

	///**
	// * color of the border
	// */
	//void SetBorderColor(VistaColor *pColor);
	//VistaColor* GetBorderColor();

	virtual std::string GetType() const;
	virtual unsigned int GetRegistrationMode() const;

	IVflLookupTable* GetLookupTable() const;	
	void SetLookupTable(VflVtkLookupTable* pLut);

	VflLookupTexture* GetLookupTexture() const;

	void SetNoiseTexture(VistaTexture* pNoiseTexture);
	VistaTexture* GetNoiseTexture();

	/**
	 * Renderable Properties API
	 */
	class VISTAFLOWLIBAPI VflObliqueSlicerProperties : public IVflVisObject::VflVisObjProperties
	{
		friend class VflObliqueSlicer;
	public:
		VflObliqueSlicerProperties();
		virtual ~VflObliqueSlicerProperties();

		virtual std::string GetReflectionableType() const;

		bool SetShowBorder(bool bShowBorder);
		bool GetShowBorder() const;

		bool SetBorderWidth(float fWidth);
		float GetBorderWidth() const;

		bool SetBorderColor(float fRed, float fGreen, float fBlue);
		bool GetBorderColor(float &fRed, float &fGreen, float &fBlue) const;

		bool SetDrawToExpPlane(bool bDraw);
		bool GetDrawToExpPlane() const;


	protected:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		bool			m_bShowBorder;
		float			m_fBorderWidth;
		float			m_aBorderColor[3]; 
	};

	virtual VflVisObjProperties    *CreateProperties() const;

	// API for the case of drawing the resulting slice to an explicitly given plane

	void SetDrawToExpRenderPlane(bool bDraw);
	bool GetDrawToExpRenderPlane() const;

	/**
	 *  Defining an explicit render plane where the resulting slice should be rendered
	 *  (instead of rendering to the real position of the slice)
	 *  CAUTION: this setter also set the boolean variable m_bDrawToExpRenderPlane to activate  
	 *           this special rendering mode to true. I find it better if the user don't have 
	 *           to explicitly think about activating the mode after he defined an explicit rendering plane.
	 *           (Change it if u don't like it, and write down the reason why.)
	 */
	void SetExpRenderPlane(	VistaVector3D &v3Normal, VistaVector3D &v3Center,
							float fWidth, float fHeight, float fRot);
	bool GetExpRenderPlane(	VistaVector3D &v3Normal, VistaVector3D &v3Center,
							float &fWidth, float &fHeight, float &fRot);

	void SetExpRenderPlane( VflSlice *pRenderPlane );
	VflSlice *GetExpRenderPlane() const;
	/**
	 * Set renderer to a static mode, i.e. it only considers one single (defined) level data.
	 * This is setter for member variable m_bStaticMode.
	 */
	void SetStaticMode(bool b);

	/**
	 * 
	 */
	bool GetStaticMode() const;

	/**
	 * Define the single level index which is to be considered in the static mode.
	 * This is setter for member variable m_iStaticLevelIdx.
	 */
	void SetStaticLevelIdx(int i);

	/**
	 * Get the single level index which is to be considered in the static mode, returns 
	 * -1 if no valid level index has been specified, i.e. if the static mode is not
	 * activated.
	 * This is getter for member variable m_iStaticLevelIdx.
	 */
	int GetStaticLevelIdx() const;


protected:
	virtual bool InitTextures();
	void UpdatePlaneParameters();
	void SetBoundsToData();

	/**
	 * RayTracer-like intersection test between the plane represented by m_oObliqueSlice
	 * and the supplied edge (ray) v3dOrigin, v3dDir.
	 * @return Returns 'true' if a intersection was found. The parameter value is delivered through the reference fI.
	 */
	bool IntersectionTest(VistaVector3D v3dOrigin, VistaVector3D v3dDir, float &fI);
	
	VistaTexture			*m_pTransferFunction;
	VistaGLSLShader		*m_pShader;
	VistaTransformMatrix	m_oScaleToRender,
							m_oScaleToTexture;
	VflSlice				m_oObliqueSlice;

	VistaVector3D			m_v3dOrigins[12],
							m_v3dDirs[12];

	VistaVector3D			m_v3dBoundsCOG;

	float					m_fLengths[12];
	
	std::vector<VistaVector3D> m_vIntersects,
								m_vTexCoords;


	VveUnsteadyCartesianGrid		*m_pData;
	std::vector<VistaTexture*>		m_vecUnsteadyDataTextures;
	
	float m_fMinBound[3], m_fMaxBound[3];
	float m_fCenter[3];

	// to allow a static mode
	bool  m_bStaticMode;
	int   m_iStaticLevelIdx;

	// to draw the resulting slice to an explicit plane (not the slicing plane)
	bool			m_bDrawToExpRenderPlane;
	VflSlice       *m_pExpRenderPlane;

	/**
	 *  Transform the render intersection points m_vIntersects such that 
	 *  they fit the given plane as defined in m_pExpPlane.
	 *  This protected method is to be called from: 
	 *  1) UpdatePlaneParameters after the computation of current intersection points is finished
	 *  or
	 *  2) SetExpRenderPlane, because the definition of target plane is changed
	 *
	 *  m_bDrawToExpPlane must be true and a target plane must be defined
	 */
	void TransformIntersections();
	std::vector<VistaVector3D> m_vExpIntersects;   // intersection points transformed to a given explicit render plane

	VflLookupTexture *m_pLookupTexture;

	VistaTexture*	m_pNoiseTexture;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLOBLIQUESLICER_H
