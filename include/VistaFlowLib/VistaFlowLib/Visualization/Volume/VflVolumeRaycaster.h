/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef __VFLVOLUMERAYCASTER_H
#define __VFLVOLUMERAYCASTER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflVolumeDataConverter.h"

#include "../VflVisObject.h"
#include "../VflRenderable.h"
#include "../VflRenderNode.h"
#include "../VflVisTiming.h"
#include "../IVflTransformer.h"

#include <VistaOGLExt/Rendering/VistaVolumeRaycasterCore.h>
#include <VistaVisExt/Data/VveDiscreteDataTyped.h>

#include <vector>
#include <cassert>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This class represents a volume raycaster for unsteady data sets. For a list
 * of features see the documentation of the underlying VistaVolumeRaycasterCore.
 */
class VISTAFLOWLIBAPI IVflVolumeRaycaster : public IVflVisObject
{
	friend class VflVolumeRaycasterProperties;
public:

	class VISTAFLOWLIBAPI VflVolumeRaycasterProperties : public IVflVisObject::VflVisObjProperties
	{
	public:

		VflVolumeRaycasterProperties();
		virtual ~VflVolumeRaycasterProperties();

		virtual std::string GetReflectionableType() const;

		/**
		 * Allowed are GL_LINEAR and GL_NEAREST, everything else is ignored. Changes
		 * are only applied when a valid value is supplied. Note, that the
		 * interpolation mode for cached time steps will not change unless a call
		 * to ClearCache(int) of the appropriate channel is made. Therefore, it is
		 * recommended to switch to the desired interpolation mode right after
		 * initialization of the raycaster. Default is GL_LINEAR.
		 */
		bool SetTextureInterpolationMode( const int iMode );

		int GetTextureInterpolationMode() const
		{
			return m_iTextureInterpolationMode;
		}

		bool SetStepSize( const float fSize )
		{
			return dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->SetStepSize(fSize);
		}
		float GetStepSize() const
		{
			return dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->GetStepSize();
		}

		bool SetAlphaSaturationLimit( const float fLimit )
		{
			return dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->SetAlphaSaturationLimit( fLimit );
		}
		float GetAlphaSaturationLimit() const
		{
			return dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->GetAlphaSaturationLimit();
		}

		bool SetAlphaCompensationFactor( const float fFactor )
		{
			return dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->SetAlphaCompensationFactor( fFactor );
		}	
		float GetAlphaCompensationFactor() const
		{
			return dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->GetAlphaCompensationFactor();
		}

		bool SetIsLightingActive( const bool bIsActive )
		{
			dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->SetIsLightingActive( bIsActive );
			return true;
		}
		bool GetIsLightingActive() const
		{
			return dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->GetIsLightingActive();
		}

		bool SetPerformDepthTest( const bool bIsActive )
		{
			dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->SetPerformDepthTest( bIsActive );
			return true;
		}
		bool GetPerformDepthTest() const
		{
			return dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->GetPerformDepthTest();
		}

		bool SetPerformRayJittering( const bool bIsActive )
		{
			dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->SetPerformRayJittering( bIsActive );
			return true;
		}
		bool GetPerformRayJittering() const
		{
			return dynamic_cast<IVflVolumeRaycaster*>(m_pParentObject)->m_pRaycasterCore->GetPerformRayJittering();
		}
	private:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

		int m_iTextureInterpolationMode;

	};

	IVflVolumeRaycaster();
	virtual ~IVflVolumeRaycaster();

	VflVolumeRaycasterProperties* GetProperties() const;
	
	// *** VistaVolumeRaycasterCore wrapper ***
#pragma region VistaVolumeRaycasterCore wrapper
	
	int DeclareLookupTexture( const std::string& strSamplerName )
	{
		return m_pRaycasterCore->DeclareLookupTexture( strSamplerName );
	}

	int DeclareLookupRange( const std::string& strUniformName )
	{
		return m_pRaycasterCore->DeclareLookupRange( strUniformName );
	}


	void SetUseAlphaClassifiedGradient( const bool bUse )
	{
		m_pRaycasterCore->SetUseAlphaClassifiedGradient( bUse );
	}

	bool GetUseAlphaClassifiedGradient() const
	{
		return m_pRaycasterCore->GetUseAlphaClassifiedGradient();
	}


	bool SetGradientVolume( const int iDataChannelId, const int iScalarIndex )
	{
		return m_pRaycasterCore->SetGradientVolume( iDataChannelId, iScalarIndex );
	}

	void GetGradientVolume( int& iDataChannelId, int& iScalarIndex ) const
	{
		m_pRaycasterCore->GetGradientVolume( iDataChannelId, iScalarIndex );
	}


	void SetLookupTexture( VistaTexture* pLookupTexture, int iLookupId = 0 )
	{
		m_pRaycasterCore->SetLookupTexture( pLookupTexture, iLookupId );
	}
	
	VistaTexture* GetLookupTexture( int iLookupId = 0 ) const
	{
		return m_pRaycasterCore->GetLookupTexture( iLookupId );
	}


	bool SetLookupTableRange( float* pLookupRange, int iRangeId = 0,
			unsigned int uiNumComponents = 2, unsigned int uiNumVectors = 1 )
	{
		return m_pRaycasterCore->SetLookupRange( pLookupRange, iRangeId,
				uiNumComponents, uiNumVectors );
	}

	void GetLookupTableRange( float* pLookupRange, int iRangeId = 0,
		unsigned int uiNumComponents = 2, unsigned int uiNumVectors = 1 ) const
	{
		m_pRaycasterCore->GetLookupRange( pLookupRange, iRangeId,
				uiNumComponents, uiNumVectors );
	}
#pragma endregion VistaVolumeRaycasterCore wrapper

protected:
	virtual IVflVisObject::VflVisObjProperties *CreateProperties() const;
	VistaVolumeRaycasterCore* m_pRaycasterCore;

private:
	

	

};

template <class DataType>
class VflVolumeRaycaster : public IVflVolumeRaycaster
{
public : 
	VflVolumeRaycaster();
	virtual ~VflVolumeRaycaster();

	bool SetPerformStreamUpdate( const bool bEnable, const int iDataChannelId = -1 );

	// Return value is only an error indicator
	bool GetPerformStreamUpdate( bool& bIsEnabled, const int iDataChannelId = 0 );

	void ClearCache( const int iDataChannelId = -1 );


	// Maybe only template this function. Conversion then can happen inside.
	bool SetData( VveDiscreteDataTyped<DataType>* pData, const int iDataChannelId = 0 );

	VveDiscreteDataTyped<DataType>* GetData( const int iDataChannelId = 0 ) const;

	bool SetSamplerShaderFilename( const std::string& strFilename );

	std::string GetSamplerShaderFilename() const
	{
		return m_pRaycasterCore->GetSamplerShaderFilename();
	}

	int DeclareDataChannel( const std::string& strSamplerName );

		// *** IVflRenderable interface ***
	virtual void Update();

	virtual void DrawTransparent();
	
	virtual unsigned int GetRegistrationMode() const
	{
		return OLI_UPDATE | OLI_DRAW_TRANSPARENT;
	}

protected:

	void PerformClearCache( const int iDataChannelId );

private:
	struct TimeStep
	{
		TimeStep()
		:	m_pVolumeData( NULL )
		{ }

		void CalculateGeometry();

		float m_aBounds[6];
		VistaTexture* m_pVolumeData;

		// Don't set these but call CalculateGeometry()
		float m_aExtents[3];
		float m_aOffset[3];
	};
    struct DataChannel
	{
		DataChannel()
		:	m_pUnsteadyVolumeData( NULL )
		,	m_pActiveTimeStep( NULL )
		,	m_bStreamUpdate( false )
		{ }

		VveDiscreteDataTyped<DataType>* m_pUnsteadyVolumeData;
		std::vector<TimeStep> m_vecConvertedVolumeData;
		TimeStep* m_pActiveTimeStep;
		bool m_bStreamUpdate; //< Update in every frame
	};

	std::vector<DataChannel> m_vecDataChannels;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/


/*============================================================================*/
/*  INLINE MEMBERS   TEMPLATE                                                 */
/*============================================================================*/
template< class DataType >
VflVolumeRaycaster< DataType >::VflVolumeRaycaster()
{
	SetSamplerShaderFilename( "VistaVanillaRaycasterSample_frag.glsl" );

	int iRes = DeclareDataChannel( "u_volume_sampler" );	assert( iRes != -1 );
	iRes = DeclareLookupTexture( "u_lookup_sampler" );		assert( iRes != -1 );
	iRes = DeclareLookupRange( "u_lut_range" );				assert( iRes != -1 );

	for( size_t i=0; i<m_vecDataChannels.size(); ++i )
	{
		DataChannel& rDC = m_vecDataChannels[i];
		for( size_t j=0; j<rDC.m_vecConvertedVolumeData.size(); ++j )
		{
			delete rDC.m_vecConvertedVolumeData[j].m_pVolumeData;
		}
	}
}

template< class DataType >
VflVolumeRaycaster< DataType >::~VflVolumeRaycaster() = default;



template <class DataType>
bool VflVolumeRaycaster<DataType>::SetData( VveDiscreteDataTyped<DataType>* pData,
													     const int iDataChannelId)
{
	if(    iDataChannelId < 0
		|| iDataChannelId >= static_cast<int>( m_vecDataChannels.size() ) )
	{
		return false;
	}
		
	if( m_vecDataChannels[iDataChannelId].m_pUnsteadyVolumeData != pData )
	{
		m_vecDataChannels[iDataChannelId].m_pUnsteadyVolumeData = pData;
		ClearCache( iDataChannelId );
	}
	return true;
}

template <class DataType>
VveDiscreteDataTyped<DataType>* VflVolumeRaycaster<DataType>::GetData(const int iDataChannelId) const
{
	if(    iDataChannelId < 0
		|| iDataChannelId >= static_cast<int>( m_vecDataChannels.size() ) )
	{
		return NULL;
	}
	return m_vecDataChannels[iDataChannelId].m_pUnsteadyVolumeData;
}

template <class DataType>
bool VflVolumeRaycaster<DataType>::SetPerformStreamUpdate( const bool bEnable,
												     const int iDataChannelId)
{
	if( iDataChannelId < -1 || iDataChannelId >= static_cast<int>( m_vecDataChannels.size() ) )
		return false;
	if( iDataChannelId == -1 )
	{
		for( std::size_t i=0; i<m_vecDataChannels.size(); ++i )
		{
			m_vecDataChannels[ i ].m_bStreamUpdate = bEnable;
		}
	}
	else
	{
		m_vecDataChannels[ iDataChannelId ].m_bStreamUpdate = bEnable;
	}

	return true;
}

template <class DataType>
bool VflVolumeRaycaster<DataType>::GetPerformStreamUpdate(bool& bIsEnabled,
												  const int iDataChannelId)
{
	if( iDataChannelId < 0 || iDataChannelId >= static_cast<int>( m_vecDataChannels.size() ) )
		return false;
	bIsEnabled = m_vecDataChannels[iDataChannelId].m_bStreamUpdate;
	return true;
}

template <class DataType>
void VflVolumeRaycaster<DataType>::ClearCache( const int iDataChannelId )
{
	// Assuming, that iDataChannelId is valid
	assert(    iDataChannelId >= -1
			&& iDataChannelId < static_cast< int >( m_vecDataChannels.size() ) );

	if( iDataChannelId == -1 )
	{
		for( std::size_t i=0; i<m_vecDataChannels.size(); ++i )
			PerformClearCache( static_cast<int>(i) );
	}
	else
	{
		PerformClearCache( iDataChannelId );
	}
}

template <class DataType>
void VflVolumeRaycaster<DataType>::PerformClearCache(const int iDataChannelId)
{
	// Assuming, that iDataChannelId is valid
	assert(    iDataChannelId >= 0
		&& iDataChannelId < static_cast<int>( m_vecDataChannels.size() ) );

	m_pRaycasterCore->SetVolumeTexture( NULL, static_cast<int>( iDataChannelId ) );

	DataChannel& rDC = m_vecDataChannels[iDataChannelId];
	for( size_t i=0; i<rDC.m_vecConvertedVolumeData.size(); ++i )
	{
		delete rDC.m_vecConvertedVolumeData[i].m_pVolumeData;
		rDC.m_vecConvertedVolumeData[i].m_pVolumeData = NULL;
	}

	if( rDC.m_pUnsteadyVolumeData )
	{
		rDC.m_vecConvertedVolumeData.resize(
			rDC.m_pUnsteadyVolumeData->GetNumberOfLevels() );
	}
}


// *** VistaVolumeRaycasterCore wrapper   ****
template <class DataType>
bool VflVolumeRaycaster<DataType>::SetSamplerShaderFilename(const std::string& strFilename)
{
	const bool bRes = m_pRaycasterCore->SetSamplerShaderFilename( strFilename );
	if( bRes )
	{
		for( size_t i=0; i<m_vecDataChannels.size(); ++i )
			ClearCache( static_cast<int>( i ) );
		m_vecDataChannels.clear();
	}
	return bRes;
}

template <class DataType>
int VflVolumeRaycaster<DataType>::DeclareDataChannel(const std::string& strSamplerName)
{
	const int iRes = m_pRaycasterCore->DeclareVolumeTexture( strSamplerName );
	if( iRes != -1 )
	{
		// The index of the new data channel (volume texture) must be the
		// last entry of the data channel vector after inserting it, or we
		// have a mismatch. However, this should not be possible to occur,
		// hence the assertion.
		assert( iRes == static_cast<int>( m_vecDataChannels.size() ) );
		m_vecDataChannels.resize( m_vecDataChannels.size() + 1 );
	}
	return iRes;
}

// *** IVflRenderable Interface ***

template <class DataType>
void VflVolumeRaycaster<DataType>::Update()
{
	// Determine currently active data time step
	const double dVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
		
	for( size_t i=0; i<m_vecDataChannels.size(); ++i )
	{
		DataChannel& rDC = m_vecDataChannels[i];

		if( !rDC.m_pUnsteadyVolumeData )
			continue;

		const int iLevelIndex = 
				rDC.m_pUnsteadyVolumeData->GetTimeMapper()->GetLevelIndex( dVisTime );
		DataType* pData =
				rDC.m_pUnsteadyVolumeData->GetTypedLevelDataByLevelIndex( iLevelIndex )->GetData();
		
		// Check if volume texture needs to be created. This is also the
		// case when stream updates are enabled.
		// @TODO Stream updates are overly expensive as the 3D texture is
		//		 always re-build from scratch (glTexImage3D) even though
		//		 straight updates should most often suffice (glTexSubImage3D).
		if(    !rDC.m_vecConvertedVolumeData[iLevelIndex].m_pVolumeData
			|| rDC.m_bStreamUpdate )
		{
			// Delete old data in case stream updates are used
			if( rDC.m_vecConvertedVolumeData[iLevelIndex].m_pVolumeData )
				delete rDC.m_vecConvertedVolumeData[iLevelIndex].m_pVolumeData;

			TimeStep oNewTimeStep;
			oNewTimeStep.m_pVolumeData = VflVolumeDataConverter::ConvertTo3DTexture(
				pData, oNewTimeStep.m_aBounds, GetProperties()->GetTextureInterpolationMode());
			oNewTimeStep.CalculateGeometry();
			rDC.m_vecConvertedVolumeData[iLevelIndex] = oNewTimeStep;
		}

		// Set active data time step so it can be rendered in the Draw*() method.
		rDC.m_pActiveTimeStep = &rDC.m_vecConvertedVolumeData[iLevelIndex];
		assert( rDC.m_pActiveTimeStep && rDC.m_pActiveTimeStep->m_pVolumeData );
	}
}

template <class DataType>
void VflVolumeRaycaster<DataType>::DrawTransparent()
{
	for( int i=0; i<static_cast<int>( m_vecDataChannels.size() ); ++i )
	{
		DataChannel& rDC = m_vecDataChannels[i];
		if( m_pRaycasterCore->GetVolumeTexture( i ) != rDC.m_pActiveTimeStep->m_pVolumeData )
		{
			m_pRaycasterCore->SetVolumeTexture( rDC.m_pActiveTimeStep->m_pVolumeData, i );
			// @TODO Do this only once as the extents are expected to be
			//		 the same for all data channels. For now, do it only
			//		 for the first data channel.
			if( i == 0 )
				m_pRaycasterCore->SetExtents( rDC.m_pActiveTimeStep->m_aExtents );
		}
	}
		
	glPushMatrix();

	// Apply transformer
	if( m_vecDataChannels.size() > 0 )
	{
		// @TODO Find consistent way of applying the transformer transformation
		//		 only once for all data channels. For now, the first data
		//		 channel will be used to retrieve the required information.
		DataChannel& rDC = m_vecDataChannels[0];
						
		const double dVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
		const double dSimTime = rDC.m_pUnsteadyVolumeData->GetTimeMapper()->GetSimulationTime( dVisTime );

		VistaTransformMatrix oTransformerMat;
		if( GetTransformer() && GetTransformer()->GetUnsteadyTransform( dSimTime, oTransformerMat ) )
		{
			float aMatValues[16];
			oTransformerMat.GetTransposedValues( aMatValues );
			glMultMatrixf( aMatValues );
		}

		// The raycaster core centers the volume around the coordinate origin.
		// To align it to its actual extents, the appropriate offset has to be
		// applied here.
		glTranslatef( 
			rDC.m_pActiveTimeStep->m_aOffset[0],
			rDC.m_pActiveTimeStep->m_aOffset[1],
			rDC.m_pActiveTimeStep->m_aOffset[2]
		);
	}
	m_pRaycasterCore->Draw();
	glPopMatrix();
}

// Struct TimeStep

template <class DataType>
void VflVolumeRaycaster<DataType>::TimeStep::CalculateGeometry()
{
	m_aExtents[0] = m_aBounds[1] - m_aBounds[0];
	m_aExtents[1] = m_aBounds[3] - m_aBounds[2];
	m_aExtents[2] = m_aBounds[5] - m_aBounds[4];

	m_aOffset[0] = 0.5f * ( m_aBounds[1] + m_aBounds[0] );
	m_aOffset[1] = 0.5f * ( m_aBounds[3] + m_aBounds[2] );
	m_aOffset[2] = 0.5f * ( m_aBounds[5] + m_aBounds[4] );
}


#endif // Include guard.
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
