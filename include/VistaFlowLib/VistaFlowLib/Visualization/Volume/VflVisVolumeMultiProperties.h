/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVISVOLUMEMULTIPROPERTIES_H
#define _VFLVISVOLUMEMULTIPROPERTIES_H

#ifdef WIN32
	#pragma warning (disable: 4786)
#endif

#include "VflVisVolume.h"
#include <vector>


class VISTAFLOWLIBAPI VflVisVolumeMultiPropertiesBase 
{
public:
  VflVisVolumeMultiPropertiesBase();
  virtual ~VflVisVolumeMultiPropertiesBase();
  
  virtual VflVtkLookupTable *GetMultiLookupTable(const int iChannel) const;
  virtual VflLookupTexture	*GetMultiLookupTexture(const int iChannel) const;

  /** controls color blending in alpha channel for segmented data
   *  if frac(col * fScale) > fThreshold 
   *    color modifier is col.a = col.a *fAlphaWeight
   *   e.g. disable color bleeding in 8bit resolution segmentation: 
   *   fScale = 255, fThreshold = 0.001, fAlphaWeight = 0.0
   */
  virtual void SetAlphaFade(const float fScale, const float fThreshold, const float fAlphaWeight);
  virtual const float* GetAlphaFade()const;
protected:

  std::vector<VflVtkLookupTable*> m_vecMultiLookupTable;
  std::vector<VflLookupTexture*>	 m_vecMultiLookupTexture;
  std::vector<bool>	m_vecManageMultiLut;
  std::vector<bool>	m_vecManageMultiTexture;
  float m_aAlphaFade[3];
};

class VISTAFLOWLIBAPI VflVisVolumeMultiProperties: public VflVisVolume::VflVisVolumeProperties, public VflVisVolumeMultiPropertiesBase
{
public:
  VflVisVolumeMultiProperties(VflVtkLookupTable *pLut = NULL);
  virtual ~VflVisVolumeMultiProperties();
	
  
  virtual std::string GetReflectionableType() const;
protected:
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

};


#endif // _VFLVISVOLUMEMULTIPROPERTIES_H

