/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// VistaFlowLib includes
#include "VflVisVolumeMagicLensProperties.h"
#include "../VflVtkLookupTable.h"
#include "../VflLookupTexture.h"
#include <VistaVisExt/Tools/VveUtils.h>

#include <VistaAspects/VistaAspectsUtils.h>
#include <string>
#include <list>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strVflVisVolumeMagicLensPropertiesReflType("VflVisVolumeMagicLensProperties");

static IVistaPropertyGetFunctor *s_aVflVisVolumeMagicLensPropertiesGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aVflVisVolumeMagicLensPropertiesSetFunctors[] =
{
	NULL
};
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/

VflVisVolumeMagicLensPropertiesBase::VflVisVolumeMagicLensPropertiesBase()
	:	m_pMagicLensLookupTable(NULL)
	,	m_pMagicLensLookupTexture(NULL)
	,	m_bManageMagicLensLut(false)
	,	m_bManageMagicLensTexture(false)
	,	m_v3Center(VistaVector3D(0.75f,0.75f,0.75f))
	,	m_fRadius(0.5f)
{
	if (!m_pMagicLensLookupTable)
	{
		m_pMagicLensLookupTable = new VflVtkLookupTable;
		// create dummy table with low alpha range
		m_pMagicLensLookupTable->SetAlphaRange(0.0, 0.05f);
		m_bManageMagicLensLut = true;
	}

	if (!m_pMagicLensLookupTexture)
	{
		m_pMagicLensLookupTexture = new VflLookupTexture(m_pMagicLensLookupTable);
		m_bManageMagicLensTexture = true;
	}
}

VflVisVolumeMagicLensPropertiesBase::~VflVisVolumeMagicLensPropertiesBase()
{
	if (m_bManageMagicLensTexture)
		delete m_pMagicLensLookupTexture;

	if (m_bManageMagicLensLut)
		delete m_pMagicLensLookupTable;
}

bool VflVisVolumeMagicLensPropertiesBase::SetMagicLensCenter(const VistaVector3D v3Center)
{
	if(m_v3Center != v3Center)
	{
		m_v3Center = v3Center;
		return true;
	}
	return false;
}

const VistaVector3D VflVisVolumeMagicLensPropertiesBase::GetMagicLensCenter() const
{
	return m_v3Center;
}

bool VflVisVolumeMagicLensPropertiesBase::SetMagicLensRadius(const float fRadius)
{
	if(m_fRadius != fRadius)
	{
		m_fRadius = fRadius;
		return true;
	}
	return false;
}

const float VflVisVolumeMagicLensPropertiesBase::GetMagicLensRadius() const
{
	return m_fRadius;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTable                                              */
/*                                                                            */
/*============================================================================*/
VflVtkLookupTable *VflVisVolumeMagicLensPropertiesBase::GetMagicLensLookupTable() const
{
	return m_pMagicLensLookupTable;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTexture                                            */
/*                                                                            */
/*============================================================================*/
VflLookupTexture *VflVisVolumeMagicLensPropertiesBase::GetMagicLensLookupTexture() const
{
	return m_pMagicLensLookupTexture;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   VflVisVolumeMagicLensProperties                             */
/*                                                                            */
/*============================================================================*/
VflVisVolumeMagicLensProperties::VflVisVolumeMagicLensProperties(VflVtkLookupTable *pLut)
	:	VflVisVolume::VflVisVolumeProperties(pLut)
{
}

VflVisVolumeMagicLensProperties::~VflVisVolumeMagicLensProperties()
{}

bool VflVisVolumeMagicLensProperties::SetMagicLensCenter(const VistaVector3D v3Center)
{
	if(m_v3Center != v3Center)
	{
		VflVisVolumeMagicLensPropertiesBase::SetMagicLensCenter(v3Center);
		Notify();
		return true;
	}
	return false;
}

bool VflVisVolumeMagicLensProperties::SetMagicLensRadius(const float fRadius)
{
	if(m_fRadius != fRadius)
	{
		VflVisVolumeMagicLensPropertiesBase::SetMagicLensRadius(fRadius);   
		Notify();
		return true;
	}
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflVisVolumeMagicLensProperties::GetReflectionableType() const
{
	return s_strVflVisVolumeMagicLensPropertiesReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflVisVolumeMagicLensProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strVflVisVolumeMagicLensPropertiesReflType);
	return i+1;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
