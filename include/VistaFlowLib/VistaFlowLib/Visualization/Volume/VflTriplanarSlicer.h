/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFLTRIPLANARSLICER_H
#define _VFLTRIPLANARSLICER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflVisObject.h>
#include <VistaFlowLib/Visualization/VflVisController.h>

#include <VistaFlowLib/Visualization/Volume/VflSlice.h>

#include <VistaVisExt/Data/VveVtkData.h>

#include <VistaBase/VistaVectorMath.h>

#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;
class VistaGLSLShader;
class VflLookupTexture;
class IVflLookupTable;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * 
 */
class VISTAFLOWLIBAPI VflTriplanarSlicer : public IVflVisObject
{
public:
	/**
	 * determine where the orthogonal slices are drawn 
	 * AT_CENTER centers the slices at the current center location whereas
	 * AT_BACKPLANE projects the slices to the data's bounding box.
	 */
	enum{
		DM_AT_CENTER,
		DM_AT_BACKPLANE
	};
	
	VflTriplanarSlicer();
	virtual ~VflTriplanarSlicer();

	/**
	 *  IVflRenderable interface
	 */
	virtual bool Init();

    /**
	 *
	 */
	virtual void DrawTransparent();

	/**
	 *
	 */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, 
								int msg, int ticket);

	
	/**
	 *
	 */
	bool SetUnsteadyData(VveUnsteadyData *pData);
	/**
	 *
	 */
	bool SetData(VveUnsteadyVtkStructuredPoints *pData);
	/**
	 *
	 */
	VveUnsteadyVtkStructuredPoints* GetData();
	/**
	 *
	 */
	void SetBounds(	const VistaVector3D& v3minBound, 
					const VistaVector3D& v3maxBound);
	/**
	 *
	 */
	virtual bool GetBounds(VistaVector3D &minBounds, 
		                   VistaVector3D &maxBounds);

	void SetBoundsToData();
	/**
	 *
	 */
	void SetCenter( const VistaVector3D &v3Pos);
	/**
	 *
	 */
	void SetCenter( const float fCenter[3]);
	/**
	 *
	 */
	VistaVector3D GetCenter() const;
	/**
	 *
	 */
	void GetCenter(float fCenter[3]) const;
	/**
	 *
	 */
	virtual std::string GetType() const;
	/**
	 *
	 */
	virtual unsigned int GetRegistrationMode() const;
	/**
	 *
	 */
	IVflLookupTable* GetLookupTable();		
	/**
	 *
	 */
	void SetPadDataToPowerOf2(bool b);
	bool GetPadDataToPowerOf2() const;

	/**
	 *
	 */
	class VISTAFLOWLIBAPI VflTriplanarSlicerProperties : public VflVisObjProperties
	{
	public:
		VflTriplanarSlicerProperties();
		virtual ~VflTriplanarSlicerProperties();
		
		std::string GetReflectionableType() const;


		/**
		*
		*/
		bool SetDrawMode(int dm);
		/**
		*
		*/
		int GetDrawMode() const;

		enum
		{
			MSG_DRAWMODE_CHANGE = VflVisObjProperties::MSG_LAST,

			MSG_LAST
		};

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	private:
		VflTriplanarSlicerProperties( const VflTriplanarSlicerProperties &) {}
		VflTriplanarSlicerProperties &operator=(const VflTriplanarSlicerProperties &) { return *this; }

		int m_iDrawMode;

		friend class VflTriplanarSlicer;
	};


protected:
	bool InitShaders();
	bool InitTextures();
	void UpdatePlaneParameters();
	
	VistaTexture* m_pTransferFunction;
	VistaGLSLShader *m_pShader;

	VveUnsteadyVtkStructuredPoints *m_pData;
	std::vector<VistaTexture*> m_vecUnsteadyDataTextures;
	
	float m_fMinBound[3], m_fMaxBound[3];
	float m_fCenter[3];

	VflSlice		m_oXZPlane, m_oXYPlane, m_oYZPlane;

	std::vector<VistaVector3D> m_vBBPlaneNormals;

	bool m_bDisplayMode;

	bool m_bPadData;

private :
	virtual VflVisObjProperties    *CreateProperties() const;
	VflLookupTexture *m_pLookupTexture;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLTRIPLANARSLICER_H

