/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include <GL/glew.h>

#include "VflRaycastSlicer.h"

#include "VflVisVolume.h"
#include "../VflVisTiming.h"
#include "../VflLookupTexture.h"
#include "../VflVtkLookupTable.h"
#include "../../Data/VflUnsteadyCartesianGridPusher.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include "VflObliqueSlicer.h"

static const int s_iTransferFunctionTextureUnit = 0;
static const int s_iDataTextureUnit = 1;

const std::string RAYCAST_SLICE_NEAREST = "VflRaycastSlicer_Nearest_frag.glsl";
const std::string RAYCAST_SLICE_LINEAR  = "VflRaycastSlicer_Linear_frag.glsl";

using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflRaycastSlicer::VflRaycastSlicer(VflVisVolume *pTargetData)
	:	VflObliqueSlicer()
	,	m_pTexShader(new VistaGLSLShader)
{	
	m_pTargetData	= pTargetData;
	m_pData			= pTargetData->GetData();
}


VflRaycastSlicer::~VflRaycastSlicer()
{
	delete m_pTexShader;	
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflRaycastSlicer::Init()
{
	bool bSuccess = true;

	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();

	std::string strRaycastNearest = rShaderReg.RetrieveShader( RAYCAST_SLICE_NEAREST );
	std::string strRaycastLinear  = rShaderReg.RetrieveShader( RAYCAST_SLICE_LINEAR );

	if( strRaycastNearest.empty() ||
		strRaycastLinear.empty()  )
	{
		return false;
	}

	if(m_pTargetData->GetProperties()->GetSlicerSegmentation())
		m_pTexShader->InitFragmentShaderFromString(strRaycastNearest);
	else
		m_pTexShader->InitFragmentShaderFromString(strRaycastLinear);

	bSuccess &= m_pTexShader->Link();

	SetBoundsToData();
	bSuccess &= InitTextures();
	bSuccess &= VflObliqueSlicer::Init();

	return bSuccess;
}

void VflRaycastSlicer::DrawCustom()
{
	if(!GetVisible())
		return ;

	VflObliqueSlicerProperties *pProps = dynamic_cast<VflObliqueSlicerProperties*>(GetProperties());

	double dCurrentVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	int iCurrentLevel;
	
	if(!m_bStaticMode)
		iCurrentLevel = m_pData->GetTimeMapper()->GetLevelIndex(dCurrentVisTime);
	else
		iCurrentLevel = m_iStaticLevelIdx;

	if(iCurrentLevel < 0 || iCurrentLevel >= m_pData->GetNumberOfLevels())
		return;

	VistaVector3D v3ViewDir(GetRenderNode()->GetLocalViewDirection());

	glPushAttrib( GL_ENABLE_BIT | GL_CURRENT_BIT | GL_TEXTURE_BIT 
				| GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
 
	glDisable(GL_DITHER);
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);

	if(!m_bDrawToExpRenderPlane)
	{
		glBegin(GL_POLYGON);
		for(unsigned int i=0; i<m_vIntersects.size(); ++i) {
			glTexCoord3f(m_vTexCoords[i][0], m_vTexCoords[i][1], m_vTexCoords[i][2]);
			glVertex3f(m_vIntersects[i][0], m_vIntersects[i][1], m_vIntersects[i][2]);
		}
		glEnd();
	}
	else
	{
		glBegin(GL_POLYGON);
		for(unsigned int i=0; i<m_vIntersects.size(); ++i) {
			glTexCoord3f(m_vTexCoords[i][0], m_vTexCoords[i][1], m_vTexCoords[i][2]);
			glVertex3f(m_vExpIntersects[i][0], m_vExpIntersects[i][1], m_vExpIntersects[i][2]);
		}
		glEnd();
	}

	if(pProps->GetShowBorder())
	{
		glLineWidth(pProps->GetBorderWidth());
		float fR, fG, fB;
		pProps->GetBorderColor(fR, fG, fB);
		glColor3f((GLfloat)fR, (GLfloat)fG, (GLfloat)fB);
		if(!m_bDrawToExpRenderPlane)
		{
			glBegin(GL_LINE_LOOP);
			for(unsigned int i=0; i<m_vIntersects.size(); ++i)
				glVertex3f(m_vIntersects[i][0], m_vIntersects[i][1], m_vIntersects[i][2]);
			glEnd();
		}
		else
		{
			glBegin(GL_LINE_LOOP);
			for(unsigned int i=0; i<m_vIntersects.size(); ++i)
				glVertex3f(m_vExpIntersects[i][0], m_vExpIntersects[i][1], m_vExpIntersects[i][2]);
			glEnd();

		}
	}
	glPopAttrib();

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflRaycastSlicer::DrawOpaque()
{
	if(!GetVisible())
		return ;

	glPushAttrib( GL_TEXTURE_BIT | GL_CURRENT_BIT | GL_COLOR_BUFFER_BIT );


	glDisable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);

	// 3D volume texture
	const int iTexVolId = m_pTargetData->GetPusher()->GetTexture()->GetId();
	const int iTexLutId = m_pLookupTexture->GetLookupTexture()->GetId();
	float aRange[2] = {0.0,1.0};
	m_pLookupTexture->GetLookupTable()->GetTableRange(aRange[0],aRange[1]);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D,iTexVolId);
	if(m_pTargetData->GetProperties()->GetSlicerSegmentation())
	{
		glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	}

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_1D,iTexLutId);

	m_pTexShader->Bind();
	m_pTexShader->SetUniform(m_pTexShader->GetUniformLocation("tex_vol"),0);
	m_pTexShader->SetUniform(m_pTexShader->GetUniformLocation("tex_tf"),1);
	const float aTfRange[2] = {1.0f/(aRange[1]-aRange[0]),-aRange[0]/(aRange[1]-aRange[0])};
	m_pTexShader->SetUniform(m_pTexShader->GetUniformLocation("tf_aRange"),2,1,aTfRange);
	int aDims[3];
	m_pTargetData->GetPusher()->GetMaxDimensions(aDims);
	const float aSize[3] = {static_cast<float>(aDims[0]),static_cast<float>(aDims[1]),static_cast<float>(aDims[2])};
	m_pTexShader->SetUniform(m_pTexShader->GetUniformLocation("size"),3,1,aSize);

	DrawCustom();

	if(m_pTargetData->GetProperties()->GetSlicerSegmentation())
	{
		glActiveTexture(GL_TEXTURE0);
		glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	}      
	m_pTexShader->Release();

	glPopAttrib();
}
	
bool VflRaycastSlicer::InitTextures()
{
	if(!m_pLookupTexture)
	{
		m_pLookupTexture = new VflLookupTexture(new VflVtkLookupTable, GL_LINEAR, true);
	} 
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflRaycastSlicer::GetType() const
{
	return string("VflRaycastSlicer");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflRaycastSlicer::GetRegistrationMode() const
{
	return OLI_DRAW_OPAQUE;
}
/*============================================================================*/
/* END OF FILE       VflRaycastSlicer.cpp                                     */
/*============================================================================*/
