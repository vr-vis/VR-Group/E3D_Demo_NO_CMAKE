/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflVolumeRendererMagicLens.h"
#include "VflVisVolume.h"
#include "VflVisVolumeMagicLensProperties.h"
#include <VistaOGLExt/VistaFramebufferObj.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include "../VflVisTiming.h"
#include <VistaVisExt/Data/VveCartesianGrid.h>
#include "../../Data/VflUnsteadyCartesianGridPusher.h"
#include "../VflVtkLookupTable.h"
#include "../VflLookupTexture.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strVflVolumeRendererMagicLensReflType("VflVolumeRendererMagicLens");
static std::string s_strVflVolumeRendererShaderName("VflVolumeRendererRaycast_frag.glsl");

static IVistaPropertyGetFunctor *s_aVflVolumeRendererMagicLensGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aVflVolumeRendererMagicLensSetFunctors[] =
{
	NULL
};


/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflVolumeRendererMagicLens::VflVolumeRendererMagicLens(	VflVisVolume *pParent,
														const int iWidth, 
														const int iHeight, 
														const float fNearPlane )
	:	VflVolumeRendererRaycast(pParent, iWidth, iHeight, fNearPlane)
	,	m_bValid(false)
	,	m_bValidData(false)
	,	m_pMagicLensShader(new VistaGLSLShader())
{
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();

	string strFragmetShader = rShaderReg.RetrieveShader( s_strVflVolumeRendererShaderName );
	if( strFragmetShader.empty() )
	{
		vstr::errp() << "VflVolumeRendererMagicLens: can't find shader file \"" 
			<< s_strVflVolumeRendererShaderName << "\"" << endl;
		return;
	}

	m_pMagicLensShader->InitFragmentShaderFromString(
			"#define _MagicLens\n" + strFragmetShader );

	m_pMagicLensShader->Link();

	if(m_pRaycastShader)
		delete m_pRaycastShader;
	m_pRaycastShader = m_pMagicLensShader;
  
	m_bValid = true;
}

VflVolumeRendererMagicLens::~VflVolumeRendererMagicLens() 
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMagicLens::Update()
{
	VflVolumeRendererRaycast::Update();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMagicLens::DrawOpaque()
{
	VflVolumeRendererRaycast::DrawOpaque();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMagicLens::DrawTransparent()
{
	VflVolumeRendererRaycast::DrawTransparent();
}

void VflVolumeRendererMagicLens::UpdateBuffer(	const int iVolId, 
												const int iTfId, 
												const float aRange[2], 
												const float aRenderParams[2] ) 
{
	PrepareBuffer();
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D,iVolId);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_1D,iTfId);

	// fall back to standard tf
	int iTexMagicLensLutId = iTfId;
	float aMagicLensParams[4] = {0.0f, 0.0f, 0.0f, 0.0f}; //< disable by default
	glActiveTexture(GL_TEXTURE2);
	VflVisVolumeMagicLensPropertiesBase* pMagicLensProperties =  
			dynamic_cast<VflVisVolumeMagicLensPropertiesBase*>(pProperties);
	if(pMagicLensProperties)
	{
		iTexMagicLensLutId = pMagicLensProperties->GetMagicLensLookupTexture() 
				->GetLookupTexture()->GetId();
		const VistaVector3D v3Pos = pMagicLensProperties->GetMagicLensCenter();
		const float fRadius = pMagicLensProperties->GetMagicLensRadius();
		aMagicLensParams[0] = v3Pos[0];
		aMagicLensParams[1] = v3Pos[1];
		aMagicLensParams[2] = v3Pos[2];
		aMagicLensParams[3] = fRadius;
	}
	glBindTexture(GL_TEXTURE_1D,iTexMagicLensLutId );

	glActiveTexture(GL_TEXTURE3);
	m_pProxy->BindTexture(0);
	glActiveTexture(GL_TEXTURE4);
	m_pProxy->BindTexture(1);

	m_pRaycastShader->Bind();

	int iLoc;
	iLoc = m_pRaycastShader->GetUniformLocation("g_texVol");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 0);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texTF");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 1);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texMagicLens");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 2);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texEntry");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 3);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texExit");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 4);

	iLoc = m_pRaycastShader->GetUniformLocation("g_v4MagicLensParams");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 4, 1, aMagicLensParams);

	const VistaVector3D v3Scale = m_v3BoundsMax - m_v3BoundsMin;
	float aProxyScaling[3] = {v3Scale[0], v3Scale[1], v3Scale[2]};

	iLoc = m_pRaycastShader->GetUniformLocation("g_v3ProxyScaling");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 3, 1, aProxyScaling);

	SetupUniforms(m_pRaycastShader,aRange,aRenderParams);

	FinalizeBuffer();
}  

void VflVolumeRendererMagicLens::DrawBuffer(const unsigned int iWhich)
{
	VflVolumeRendererRaycast::DrawBuffer(iWhich);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMagicLens::Draw2D()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflVolumeRendererMagicLens::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflVolumeRendererMagicLens::GetReflectionableType() const
{
	return s_strVflVolumeRendererMagicLensReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflVolumeRendererMagicLens::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strVflVolumeRendererMagicLensReflType);
	return i+1;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
