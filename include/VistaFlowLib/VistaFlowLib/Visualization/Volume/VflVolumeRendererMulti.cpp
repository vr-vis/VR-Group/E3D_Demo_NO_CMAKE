/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflVolumeRendererMulti.h"
#include "VflVisVolume.h"
#include "VflVisVolumeMultiProperties.h"
#include <VistaOGLExt/VistaFramebufferObj.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include "../VflVisTiming.h"
#include <VistaVisExt/Data/VveCartesianGrid.h>
#include "../../Data/VflUnsteadyCartesianGridPusher.h"
#include "../VflVtkLookupTable.h"
#include "../VflLookupTexture.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strVflVolumeRendererMultiReflType("VflVolumeRendererMulti");
static std::string s_strVflVolumeRendererShaderName("VflVolumeRendererRaycast_frag.glsl");

static IVistaPropertyGetFunctor *s_aVflVolumeRendererMultiGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aVflVolumeRendererMultiSetFunctors[] =
{
	NULL
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflVolumeRendererMulti::VflVolumeRendererMulti( VflVisVolume *pParent,
												const int iWidth, 
												const int iHeight, 
												const float fNearPlane)
	:	VflVolumeRendererRaycast(pParent, iWidth, iHeight, fNearPlane)
	,	m_bValid(false)
	,	m_bValidData(false)
	,	m_pMultiShader(new VistaGLSLShader())
{
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();

	string strFragmetShader = rShaderReg.RetrieveShader( s_strVflVolumeRendererShaderName );
	if( strFragmetShader.empty() )
	{
		vstr::errp() << "VflVolumeRendererMulti: can't find shader file \"" 
			<< s_strVflVolumeRendererShaderName << "\"" << endl;
		return;
	}

	m_pMultiShader->InitFragmentShaderFromString(
			"#define _Multi\n" + strFragmetShader );
	m_pMultiShader->Link();

	if(m_pRaycastShader)
		delete m_pRaycastShader;
	m_pRaycastShader = m_pMultiShader;
  
	m_bValid = true;
}

VflVolumeRendererMulti::~VflVolumeRendererMulti() 
{
  
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMulti::Update()
{
	VflVolumeRendererRaycast::Update();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMulti::DrawOpaque()
{
	VflVolumeRendererRaycast::DrawOpaque();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMulti::DrawTransparent()
{
	VflVolumeRendererRaycast::DrawTransparent();
}

void VflVolumeRendererMulti::UpdateBufferCore(	const int iVolId, 
												const int iTfId, 
												const float aRange[2], 
												const float aRenderParams[2])
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D,iVolId);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_1D,iTfId);

	// fall back to standard tf
	int iTexTFRed   = iTfId;
	int iTexTFGreen = iTfId;
	int iTexTFBlue  = iTfId;
	float aAlphaFade[3] = {0.0f, 0.0f, 1.0f};
	if(	VflVisVolumeMultiPropertiesBase* pMultiProperties = 
			dynamic_cast<VflVisVolumeMultiPropertiesBase*>(pProperties) )
	{
		iTexTFRed    = pMultiProperties->GetMultiLookupTexture(0)
			->GetLookupTexture()->GetId();
		iTexTFGreen  = pMultiProperties->GetMultiLookupTexture(1)
			->GetLookupTexture()->GetId();
		iTexTFBlue   = pMultiProperties->GetMultiLookupTexture(2)
			->GetLookupTexture()->GetId();
		const float* aTmp = pMultiProperties->GetAlphaFade();
		aAlphaFade[0] = aTmp[0];
		aAlphaFade[1] = aTmp[1];
		aAlphaFade[2] = aTmp[2];
	}
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_1D,iTexTFRed );
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_1D,iTexTFGreen );
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_1D,iTexTFBlue);

	glActiveTexture(GL_TEXTURE5);
	m_pProxy->BindTexture(0);
	glActiveTexture(GL_TEXTURE6);
	m_pProxy->BindTexture(1);

	m_pRaycastShader->Bind();

	int iLoc;
	iLoc = m_pRaycastShader->GetUniformLocation("g_texVol");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 0);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texTF");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 1);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texTFRed");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 2);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texTFGreen");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 3);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texTFBlue");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 4);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texEntry");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 5);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texExit");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 6);

	iLoc = m_pRaycastShader->GetUniformLocation("g_AlphaFade");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 3, 1, aAlphaFade);
}

void VflVolumeRendererMulti::UpdateBuffer(	const int iVolId, const int iTfId,
											const float aRange[2], 
											const float aRenderParams[2]) 
{
	PrepareBuffer();
	UpdateBufferCore(iVolId,iTfId,aRange,aRenderParams);

	SetupUniforms(m_pRaycastShader,aRange,aRenderParams);
	FinalizeBuffer();
}  

void VflVolumeRendererMulti::DrawBuffer(const unsigned int iWhich)
{
	VflVolumeRendererRaycast::DrawBuffer(iWhich);
}

      

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMulti::Draw2D()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflVolumeRendererMulti::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflVolumeRendererMulti::GetReflectionableType() const
{
	return s_strVflVolumeRendererMultiReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflVolumeRendererMulti::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strVflVolumeRendererMultiReflType);
	return i+1;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
