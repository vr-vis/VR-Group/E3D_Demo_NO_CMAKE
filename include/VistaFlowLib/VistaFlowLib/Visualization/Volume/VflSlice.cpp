/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include <GL/glew.h>

#include <VistaFlowLib/Visualization/Volume/VflSlice.h>

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflSlice::VflSlice()
:	m_bIsReadyToRender(false)
,	m_bUseImpModeRenderPlane(true)
,	m_bUseImpModeTexturePlane(true)
,	m_bIsActive(false)
{
	// Render plane
	SetNormal		(PT_RENDER_PLANE, VistaVector3D(0.0f, 0.0f, 1.0f));
	SetCenter		(PT_RENDER_PLANE, VistaVector3D(0.0f, 0.0f, 0.0f));
	SetDimensions	(PT_RENDER_PLANE, 1.0f, 1.0f);
	SetRotation		(PT_RENDER_PLANE, 0.0f);

	// Texture Plane
	SetNormal		(PT_TEXTURE_PLANE, VistaVector3D(0.0f, 0.0f, 1.0f));
	SetCenter		(PT_TEXTURE_PLANE, VistaVector3D(0.0f, 0.0f, 0.0f));
	SetDimensions	(PT_TEXTURE_PLANE, 1.0f, 1.0f);
	SetRotation		(PT_TEXTURE_PLANE, 0.0f);

	// Generate Vertex Data
	FromImplicitRep();
}

VflSlice::VflSlice(	const VistaVector3D &n1, const VistaVector3D &c1,
					 float w1, float h1, float r1,
					 const VistaVector3D &n2, const VistaVector3D &c2, 
					 float w2, float h2, float r2)
:	m_bIsReadyToRender(false)
,	m_bUseImpModeRenderPlane(true)
,	m_bUseImpModeTexturePlane(true)
,	m_bIsActive(true)
{
	VistaVector3D v3dFallBackVec(0.0f, 0.0f, 1.0f);

	// Render plane
	if(!SetNormal	(PT_RENDER_PLANE, n1)) m_v3dNormal1 = v3dFallBackVec;
	SetCenter		(PT_RENDER_PLANE, c1);
	SetDimensions	(PT_RENDER_PLANE, w1, h1);
	SetRotation		(PT_RENDER_PLANE, r1);

	// Texture plane
	if(!SetNormal	(PT_TEXTURE_PLANE, n2)) m_v3dNormal2 = v3dFallBackVec;
	SetCenter		(PT_TEXTURE_PLANE, c2);
	SetDimensions	(PT_TEXTURE_PLANE, w2, h2);
	SetRotation		(PT_TEXTURE_PLANE, r2);
	
	// Generate vertex data
	FromImplicitRep();
}

VflSlice::VflSlice(const VistaVector3D &p1, const VistaVector3D &p2,
					 const VistaVector3D &p3, const VistaVector3D &p4, 
					 const VistaVector3D &t1, const VistaVector3D &t2,
					 const VistaVector3D &t3, const VistaVector3D &t4)
	:	m_bIsReadyToRender(true)
	,	m_bUseImpModeRenderPlane(false)
	,	m_bUseImpModeTexturePlane(false)
	,	m_bIsActive(true)
{
	// Set points for render plane and texture plane
	m_v3dPoint1 = p1;
	m_v3dPoint2 = p2;
	m_v3dPoint3 = p3;
	m_v3dPoint4 = p4;

	m_v3dTexture1 = t1;
	m_v3dTexture2 = t2;
	m_v3dTexture3 = t3;
	m_v3dTexture4 = t4;	
}

VflSlice::~VflSlice()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
bool VflSlice::Draw()
{
	if(!m_bIsActive)
		return false;

	if(!m_bIsReadyToRender)
		if(!FromImplicitRep())
			return false;

	glBegin(GL_QUADS);
		
		// Point1
		glTexCoord3f(m_v3dTexture1[0], m_v3dTexture1[1], m_v3dTexture1[2]	);
		glVertex3f	(m_v3dPoint1[0]	 , m_v3dPoint1[1]  , m_v3dPoint1[2]		);
	
		// Point2
		glTexCoord3f(m_v3dTexture2[0], m_v3dTexture2[1], m_v3dTexture2[2]	);
		glVertex3f	(m_v3dPoint2[0]	 , m_v3dPoint2[1]  , m_v3dPoint2[2]		);

		// Point3
		glTexCoord3f(m_v3dTexture3[0], m_v3dTexture3[1], m_v3dTexture3[2]	);
		glVertex3f	(m_v3dPoint3[0]	 , m_v3dPoint3[1]  , m_v3dPoint3[2]		);

		// Point4
		glTexCoord3f(m_v3dTexture4[0], m_v3dTexture4[1], m_v3dTexture4[2]	);
		glVertex3f	(m_v3dPoint4[0]	 , m_v3dPoint4[1]  , m_v3dPoint4[2]		);

	glEnd();

	return true;
}

bool VflSlice::FromImplicitRep()
{
	// Default plane 
	// (will be mapped onto the plane described by the supplied normal, center, dims, and rotation)
	VistaVector3D v3dXAxis(1.0f, 0.0f, 0.0f);
	VistaVector3D v3dYAxis(0.0f, 1.0f, 0.0f);
	VistaVector3D v3dZAxis(0.0f, 0.0f, 1.0f);

	// Local variables
	VistaQuaternion	oMappedNormal;
	VistaQuaternion	oNormalRotation;
	VistaVector3D	v3dNewXAxis;
	VistaVector3D	v3dNewYAxis;

	// Render plane computations
	if(m_bUseImpModeRenderPlane)
	{
		// Map the supplied normal onto the z-axis
		oMappedNormal = VistaQuaternion(v3dZAxis, m_v3dNormal1);
		
		// Rotate the other axis to "match" the new z-axis
		v3dNewXAxis = oMappedNormal.Rotate(v3dXAxis);
		v3dNewXAxis.Normalize();
		v3dNewYAxis = oMappedNormal.Rotate(v3dYAxis);
		v3dNewYAxis.Normalize();

		// Rotate m_fRotation1 degrees around the normal (user-supplied value)
		if(m_fRotation1 != 0.0f)
		{
			oNormalRotation = VistaQuaternion(VistaAxisAndAngle(m_v3dNormal1,
				float(m_fRotation1 * Vista::Pi / 180.0f)));

			v3dNewXAxis = oNormalRotation.Rotate(v3dNewXAxis);
			v3dNewXAxis.Normalize();
			v3dNewYAxis = oNormalRotation.Rotate(v3dNewYAxis);
			v3dNewYAxis.Normalize();
		}		

		m_v3dPoint1 = m_v3dCenter1 - .5f * m_fWidth1 * v3dNewXAxis
								   + .5f * m_fHeight1 * v3dNewYAxis;
		m_v3dPoint2 = m_v3dCenter1 - .5f * m_fWidth1 * v3dNewXAxis
								   - .5f * m_fHeight1 * v3dNewYAxis;
		m_v3dPoint3 = m_v3dCenter1 + .5f * m_fWidth1 * v3dNewXAxis
								   - .5f * m_fHeight1 * v3dNewYAxis;
		m_v3dPoint4 = m_v3dCenter1 + .5f * m_fWidth1 * v3dNewXAxis
								   + .5f * m_fHeight1 * v3dNewYAxis;
	}

	// Texture plane computations
	if(m_bUseImpModeTexturePlane)
	{
		// Map the supplied normal onto the z-axis
		oMappedNormal = VistaQuaternion(v3dZAxis, m_v3dNormal2);
		
		// Rotate the other axis to "match" the new z-axis
		v3dNewXAxis = oMappedNormal.Rotate(v3dXAxis);
		v3dNewXAxis.Normalize();
		v3dNewYAxis = oMappedNormal.Rotate(v3dYAxis);
		v3dNewYAxis.Normalize();

		// Rotate m_fRotation2 degrees around the normal (user-supplied value)
		if(m_fRotation2 != 0.0f)
		{
			oNormalRotation = VistaQuaternion(VistaAxisAndAngle(m_v3dNormal2,
				float(m_fRotation2 * Vista::Pi / 180.0f)));
			
			v3dNewXAxis = oNormalRotation.Rotate(v3dNewXAxis);
			v3dNewXAxis.Normalize();
			v3dNewYAxis = oNormalRotation.Rotate(v3dNewYAxis);
			v3dNewYAxis.Normalize();
		}

		m_v3dTexture1 = m_v3dCenter2 - .5f * m_fWidth2 * v3dNewXAxis
									 + .5f * m_fHeight2 * v3dNewYAxis;
		m_v3dTexture2 = m_v3dCenter2 - .5f * m_fWidth2 * v3dNewXAxis
									 - .5f * m_fHeight2 * v3dNewYAxis;
		m_v3dTexture3 = m_v3dCenter2 + .5f * m_fWidth2 * v3dNewXAxis
									 - .5f * m_fHeight2 * v3dNewYAxis;
		m_v3dTexture4 = m_v3dCenter2 + .5f * m_fWidth2 * v3dNewXAxis
									 + .5f * m_fHeight2 * v3dNewYAxis;
	}

	m_bIsReadyToRender = true;

	return m_bIsReadyToRender;
}

/*============================================================================*/
/*  SELECTORS	                                                              */
/*============================================================================*/
bool VflSlice::SetNormal(EPlaneTarget target, const VistaVector3D &n)
{
	if(n == VistaVector3D(0,0,0))
		return false;

	switch(target)
	{
		case PT_RENDER_PLANE:
			m_v3dNormal1 = n;
			m_v3dNormal1.Normalize();
			m_bUseImpModeRenderPlane = true;
			break;
		case PT_TEXTURE_PLANE:
			m_v3dNormal2 = n;
			m_v3dNormal2.Normalize();
			m_bUseImpModeTexturePlane = true;
			break;
		default:
			return false;
			break;
	}

	m_bIsReadyToRender = false;
		
	return FromImplicitRep();
}

bool VflSlice::GetNormal(VflSlice::EPlaneTarget target, VistaVector3D &v3dNormal)
{
	switch(target)
	{
		case PT_RENDER_PLANE:
			if(!m_bUseImpModeRenderPlane) return false;
			v3dNormal = m_v3dNormal1;
			return true;
			break;
		case PT_TEXTURE_PLANE:
			if(!m_bUseImpModeTexturePlane) return false;
			v3dNormal = m_v3dNormal2;
			return true;
			break;
		default:
			return false;
			break;
	}
}


bool VflSlice::SetCenter(EPlaneTarget target, const VistaVector3D &c)
{
	switch(target)
	{
		case PT_RENDER_PLANE:
			m_v3dCenter1 = c;
			m_bUseImpModeRenderPlane = true;
			break;
		case PT_TEXTURE_PLANE:
			m_v3dCenter2 = c;
			m_bUseImpModeTexturePlane = true;
			break;
		default:
			return false;
			break;
	}

	m_bIsReadyToRender = false;
	
	return FromImplicitRep();
}

bool VflSlice::GetCenter(VflSlice::EPlaneTarget target, VistaVector3D &v3dCenter)
{
	switch(target)
	{
		case PT_RENDER_PLANE:
			if(!m_bUseImpModeRenderPlane) return false;
			v3dCenter = m_v3dCenter1;
			break;
		case PT_TEXTURE_PLANE:
			if(!m_bUseImpModeTexturePlane) return false;
			v3dCenter = m_v3dCenter2;
			break;
		default:
			return false;
			break;
	}

	return true;
}


bool VflSlice::SetDimensions(EPlaneTarget target, float w, float h)
{
	bool bSuccess = true;
	
	if(w <= 0.0f) {
		w = 0.01f;
		bSuccess = false;
	}

	if(h <= 0.0f) {
		h = 0.01f;
		bSuccess = false;
	}

	switch(target)
	{
		case PT_RENDER_PLANE:	
			m_fWidth1	= w;
			m_fHeight1	= h;
			m_bUseImpModeRenderPlane = true;
			break;
		case PT_TEXTURE_PLANE:
			m_fWidth2	= w;
			m_fHeight2	= h;
			m_bUseImpModeTexturePlane = true;
			break;
		default:
			return false;
			break;
	}

	m_bIsReadyToRender = false;
	
	bSuccess &= FromImplicitRep();

	return bSuccess;
}

bool VflSlice::GetDimensions(EPlaneTarget target, float &fWidth, float &fHeight)
{
	switch(target)
	{
		case PT_RENDER_PLANE:
			if(!m_bUseImpModeRenderPlane) return false;
			fWidth  = m_fWidth1;
			fHeight = m_fHeight1;
			break;
		case PT_TEXTURE_PLANE:
			if(!m_bUseImpModeTexturePlane) return false;
			fWidth  = m_fWidth2;
			fHeight = m_fHeight2;
			break;
		default:
			return false;
			break;
	}

	return true;
}


bool VflSlice::SetRotation(VflSlice::EPlaneTarget target, float fDegrees)
{
	while(fDegrees <= -360.0f || fDegrees >= 360.0f) {
		if(fDegrees >= 360.0f) fDegrees -= 360.0f;
		else if(fDegrees <= -360.0f) fDegrees += 360.0f;
	}
	
	switch(target)
	{
		case PT_RENDER_PLANE:
			m_fRotation1 = fDegrees;
			m_bUseImpModeRenderPlane = true;
			break;
		case PT_TEXTURE_PLANE:
			m_fRotation2 = fDegrees;
			m_bUseImpModeTexturePlane = true;
			break;
		default:
			return false;
			break;
	}

	m_bIsReadyToRender = false;
	
	return FromImplicitRep();
}

bool VflSlice::GetRotation(VflSlice::EPlaneTarget target, float &fDegrees)
{
	switch(target)
	{
		case PT_RENDER_PLANE:
			if(!m_bUseImpModeRenderPlane) return false;
			fDegrees = m_fRotation1;
			break;
		case PT_TEXTURE_PLANE:
			if(!m_bUseImpModeTexturePlane) return false;
			fDegrees = m_fRotation2;
			break;
		default:
			return false;
			break;
	}

	return true;
}

bool VflSlice::SetPoints(EPlaneTarget target, const VistaVector3D &p1, const VistaVector3D &p2, 
											   const VistaVector3D &p3, const VistaVector3D &p4)
{
	switch(target)
	{
		case PT_RENDER_PLANE:
			m_v3dPoint1 = p1;
			m_v3dPoint2 = p2;
			m_v3dPoint3 = p3;
			m_v3dPoint4 = p4;
			m_bUseImpModeRenderPlane = false;
			break;
		case PT_TEXTURE_PLANE:
			m_v3dTexture1 = p1;
			m_v3dTexture2 = p2;
			m_v3dTexture3 = p3;
			m_v3dTexture4 = p4;
			m_bUseImpModeTexturePlane = false;
			break;
		default:
			return false;
			break;
	}

	return true;
}

bool VflSlice::SetPoints(VflSlice::EPlaneTarget target, const VistaVector3D (&v3dPoints)[4])
{
	return this->SetPoints(target, v3dPoints[0], v3dPoints[1], v3dPoints[2], v3dPoints[3]);
}

bool VflSlice::GetPoints(VflSlice::EPlaneTarget target, VistaVector3D (&v3dPoints)[4])
{
	switch(target)
	{
		case PT_RENDER_PLANE:
			// Update points if necessary
			if(!m_bIsReadyToRender && m_bUseImpModeRenderPlane && !FromImplicitRep()) return false;
			v3dPoints[0] = m_v3dPoint1;
			v3dPoints[1] = m_v3dPoint2;
			v3dPoints[2] = m_v3dPoint3;
			v3dPoints[3] = m_v3dPoint4;
			break;
		case PT_TEXTURE_PLANE:
			// Update points if necessary
			if(!m_bIsReadyToRender && m_bUseImpModeTexturePlane && !FromImplicitRep()) return false;
			v3dPoints[0] = m_v3dTexture1;
			v3dPoints[1] = m_v3dTexture2;
			v3dPoints[2] = m_v3dTexture3;
			v3dPoints[3] = m_v3dTexture4;
			break;
		default:
			return false;
			break;
	}

	return true;
}

bool VflSlice::GetPoint(VflSlice::EPlaneTarget target, int iPointIndex, VistaVector3D &v3dPoint)
{
	switch(target)
	{
		case PT_RENDER_PLANE:
			// Update points if necessary
			if(!m_bIsReadyToRender && m_bUseImpModeRenderPlane && !FromImplicitRep()) return false;
			switch(iPointIndex)
			{
				case 1: v3dPoint = m_v3dPoint1; break;
				case 2: v3dPoint = m_v3dPoint2; break;
				case 3: v3dPoint = m_v3dPoint3; break;
				case 4: v3dPoint = m_v3dPoint4; break;
				default: return false; break;				
			}
			break;
		case PT_TEXTURE_PLANE:
			// Update points if necessary
			if(!m_bIsReadyToRender && m_bUseImpModeTexturePlane && !FromImplicitRep()) return false;
			switch(iPointIndex)
			{
				case 1: v3dPoint = m_v3dTexture1; break;
				case 2: v3dPoint = m_v3dTexture2; break;
				case 3: v3dPoint = m_v3dTexture3; break;
				case 4: v3dPoint = m_v3dTexture4; break;
				default: return false; break;				
			}
			break;
		default:
			return false;
			break;
	}

	return true;
}


bool VflSlice::GetIsImpModeActive(EPlaneTarget target)
{
	switch(target)
	{
		case PT_RENDER_PLANE:
			return m_bUseImpModeRenderPlane;
			break;
		case PT_TEXTURE_PLANE:
			return m_bUseImpModeTexturePlane;
			break;
		default:
			return false;
			break;
	}
}


void VflSlice::Activate() {
	m_bIsActive = true;
}

void VflSlice::Deactivate() {
	m_bIsActive = false;
}

bool VflSlice::GetIsActive() {
	return m_bIsActive;
}

/*============================================================================*/
/* END OF FILE       VflSlice.cpp											  */
/*============================================================================*/
