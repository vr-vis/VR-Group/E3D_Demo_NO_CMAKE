/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflVisVolume.h"
#include "VflVolumeRendererInterface.h"

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strIVflVolumeRendererReflType("IVflVolumeRenderer");

static IVistaPropertyGetFunctor *s_aIVflVolumeRendererGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aIVflVolumeRendererSetFunctors[] =
{
	NULL
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
IVflVolumeRenderer::IVflVolumeRenderer(VflVisVolume *pParent)
	:	m_pParent(pParent)
{
	m_pParent->AddRenderer(this);
}

IVflVolumeRenderer::~IVflVolumeRenderer()
{
	// remove self from parent's renderer set
	for (int i=0; i<m_pParent->GetRendererCount(); ++i)
	{
		IVflVolumeRenderer *pRenderer = m_pParent->GetRenderer(i);
		if (pRenderer == this)
		{
			m_pParent->RemoveRenderer(i);
			break;
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void IVflVolumeRenderer::Update()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void IVflVolumeRenderer::DrawOpaque()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void IVflVolumeRenderer::DrawTransparent()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void IVflVolumeRenderer::Draw2D()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string IVflVolumeRenderer::GetReflectionableType() const
{
	return s_strIVflVolumeRendererReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int IVflVolumeRenderer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strIVflVolumeRendererReflType);
	return i+1;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
