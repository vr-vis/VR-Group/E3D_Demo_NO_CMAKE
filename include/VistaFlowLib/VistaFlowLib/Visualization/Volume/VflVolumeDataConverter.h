/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef __VflVolumeDataConverter_h
#define __VflVolumeDataConverter_h

#include "../../VistaFlowLibConfig.h"

#include <VistaVisExt/Data/VveCartesianGrid.h>
#include <VistaOGLExt/VistaTexture.h>

#include <cassert>


namespace VflVolumeDataConverter
{
	template<class DataType>
	VistaTexture* ConvertTo3DTexture( DataType* const pDataIn, float aBoundsOut[6],
		int iTexutreInterpolationMode );

	template<> VISTAFLOWLIBAPI VistaTexture* ConvertTo3DTexture<VveCartesianGrid>(
		VveCartesianGrid* const pData, float aBoundsOut[6],
		int iTexutreInterpolationMode );
}


#endif // Include guard.
