/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVOLUMERENDERERRAYCAST_H
#define _VFLVOLUMERENDERERRAYCAST_H

#ifdef WIN32
	#pragma warning (disable: 4786)
#endif

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflVolumeRendererInterface.h"
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>
#include "VflVolumeRendererRaycastProxy.h"
#include <VistaFlowLib/Data/VflObserver.h>

#include <list>
#include <string>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaGLSLShader;
class VfaApplicationContextObject;

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 */
class VISTAFLOWLIBAPI VflVolumeRendererRaycast : public IVflVolumeRenderer, public VflObserver
{
public:
	VflVolumeRendererRaycast(VflVisVolume *pParent,const int iWidth, const int iHeight, const float fNearPlane);
	virtual ~VflVolumeRendererRaycast();

	virtual void Update();
	virtual void DrawOpaque();
	virtual void DrawTransparent();
	virtual void Draw2D();

	virtual bool IsValid() const;
	virtual std::string GetReflectionableType() const;

  	virtual void ObserverUpdate(IVistaObserveable *pObs, int msg, int ticket);
	
protected:
	VflVolumeRendererRaycast();
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	void Resize(const int iWidth, const int iHeight, const bool bForce = false);
	void PrepareBuffer();
	void FinalizeBuffer();
	void SetupUniforms(VistaGLSLShader* pShader,const float aRange[2], const float aRenderParams[2]);
	
	// updates offscreen buffers
	virtual void UpdateBuffer(const int iVolId, const int iTfId, const float aRange[2], const float aRenderParams[2]);
	// draw content of offscreen buffer 
	virtual void DrawBuffer(const unsigned int iWhich);

protected:
	// validity information
	bool m_bValid;
	bool m_bValidData;

	VistaVector3D m_v3BoundsMin;
	VistaVector3D m_v3BoundsMax;
	int m_aDataDims[3];

	VflVolumeRaycastProxy*	m_pProxy;			//< proxy geometry
	VistaGLSLShader*		m_pRaycastShader;	//< shader for raycasting
	VistaFramebufferObj*	m_pTexBuffer;		//< offscreen buffer
	VistaTexture*			m_pTexture;			//< offscreen texture

};

#endif // _VFLVOLUMERENDERERRAYCAST_H
/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

