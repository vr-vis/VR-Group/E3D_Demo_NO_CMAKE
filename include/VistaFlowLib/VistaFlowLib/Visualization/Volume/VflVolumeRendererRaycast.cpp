/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

#include <VistaBase/VistaStreamUtils.h>

#include "VflVolumeRendererRaycast.h"
#include "VflVisVolume.h"

#include <VistaOGLExt/VistaFramebufferObj.h>
#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/DisplayManager/VistaViewport.h>
#include <VistaKernel/DisplayManager/VistaDisplaySystem.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include "../VflVisTiming.h"
#include <VistaVisExt/Data/VveCartesianGrid.h>
#include "../../Data/VflUnsteadyCartesianGridPusher.h"
#include "../VflVtkLookupTable.h"
#include "../VflLookupTexture.h"
#include <VistaAspects/VistaTransformable.h>

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaShaderRegistry.h>
#include <VistaKernel/DisplayManager/VistaDisplayManager.h>
#include <VistaKernel/DisplayManager/VistaWindow.h>

#include <map>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strVflVolumeRendererRaycastReflType("VflVolumeRendererRaycast");
static std::string s_strVflVolumeRendererShaderName("VflVolumeRendererRaycast_frag.glsl");

static IVistaPropertyGetFunctor *s_aVflVolumeRendererRaycastGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aVflVolumeRendererRaycastSetFunctors[] =
{
	NULL
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflVolumeRendererRaycast::VflVolumeRendererRaycast( VflVisVolume*	pParent,
													const int		iWidth, 
													const int		iHeight, 
													const float		fNearPlane )
	:	IVflVolumeRenderer( pParent )
	,	m_bValid( false )
	,	m_bValidData( false )
	,	m_pProxy( new VflVolumeRaycastProxy( iWidth, iHeight, fNearPlane ) )
	,	m_pTexBuffer( 0 )
	,	m_pRaycastShader( new VistaGLSLShader() )
{
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();

	string strFragmetShader = rShaderReg.RetrieveShader(s_strVflVolumeRendererShaderName);
	if(strFragmetShader.empty())
	{
		vstr::errp() << "VflVolumeRendererRaycast: can't find shader file \"" 
					 << s_strVflVolumeRendererShaderName << "\"" << endl;
		return;
	}

	m_pRaycastShader->InitFragmentShaderFromString(strFragmetShader);
	m_pRaycastShader->Link();
        
	// create offscreen buffer 
	m_pTexBuffer = new VistaFramebufferObj();

	m_pTexture = new VistaTexture(GL_TEXTURE_RECTANGLE_ARB);
	Resize(iWidth, iHeight, true);
	m_bValid = true;

	VistaSystem* pVistaSystem = GetVistaSystem();

	std::map<std::string, VistaWindow*> mapWindows =   
			pVistaSystem->GetDisplayManager()->GetWindows();
	std::map<std::string, VistaWindow*>::iterator it;
	for(it = mapWindows.begin(); it != mapWindows.end(); ++it)
	{
		VistaWindow*   pWindow   = (*it).second;
		VistaViewport* pViewport = pWindow->GetViewport(0);
		VistaViewport::VistaViewportProperties* pProps =
				pViewport->GetViewportProperties(); 

		Observe(pProps, VistaViewport::VistaViewportProperties::MSG_SIZE_CHANGE);
	}
}

VflVolumeRendererRaycast::~VflVolumeRendererRaycast()
{
	VistaSystem* pVistaSystem = GetVistaSystem ();

	std::map<std::string, VistaWindow*> mapWindows =
			pVistaSystem->GetDisplayManager()->GetWindows();
	std::map<std::string, VistaWindow*>::iterator it;
	for(it = mapWindows.begin(); it != mapWindows.end(); ++it)
	{
		VistaWindow*   pWindow   = (*it).second;
		VistaViewport* pViewport = pWindow->GetViewport(0);
		VistaViewport::VistaViewportProperties* pProps = 
				pViewport->GetViewportProperties(); 

		pProps->DetachObserver( this );
	}

	delete m_pRaycastShader;
	delete m_pProxy;
	delete m_pTexBuffer;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererRaycast::Update()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if ( !m_bValid || !pProperties->GetVisible() )
		return; // not valid or not visible

	// determine dataset boundaries for current time step
	double dVisTime = 
			m_pParent->GetRenderNode()->GetVisTiming()->GetVisualizationTime();

	VveUnsteadyCartesianGrid* pUGrid = m_pParent->GetData();
	VveTimeMapper* pTimeMapper = pUGrid->GetTimeMapper();
	int iIndex = pTimeMapper->GetLevelIndex(dVisTime);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
	VveCartesianGrid* pGrid = 
			pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if (!pGrid->GetValid())
	{
		m_bValidData = false;
		pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
		return;
	}
	m_bValidData = true;
	pGrid->GetBounds(m_v3BoundsMin, m_v3BoundsMax);

	pGrid->GetDataDimensions(m_aDataDims);
	pUGrid->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

	VflRenderNode* pNode = m_pParent->GetRenderNode();
	m_pProxy->SetRenderNode(pNode);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererRaycast::DrawOpaque()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if (	!m_bValid              || !m_bValidData 
		||	!pProperties->GetVisible() || !pProperties->GetDrawBounds() )
	{
		return;
	}

	// draw bounding box
	glPushAttrib( GL_CURRENT_BIT | GL_ENABLE_BIT  | GL_POINT_BIT
				| GL_LINE_BIT    | GL_POLYGON_BIT | GL_DEPTH_BUFFER_BIT );
	glDisable(GL_LIGHTING);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_CULL_FACE);

	glLineWidth(2.0f);
	float fR, fG, fB;
	pProperties->GetBoundColor(fR, fG, fB);
	glColor4f(fR, fG, fB, 1.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	const VistaVector3D v3Scale = m_v3BoundsMax - m_v3BoundsMin;
	glPushMatrix();
	glScalef(v3Scale[0], v3Scale[1], v3Scale[2]);
	m_pProxy->DrawProxy(false);
	glPopMatrix();
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererRaycast::DrawTransparent()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if(!m_bValid || !m_bValidData || !pProperties->GetVisible())
		return; // not valid or not visible

	VflRenderNode* pNode = m_pParent->GetRenderNode();
	VistaVector3D v3Pos  = pNode->GetLocalViewPosition();
	VistaQuaternion q    = pNode->GetLocalViewOrientation();
	VistaTransformMatrix matQ(q);

	VistaTransformMatrix matTransform;
	matTransform.SetTranslation(-v3Pos);
  
	VistaTransformMatrix matModelView = matQ.GetTranspose() * matTransform;

	glPushAttrib( GL_ENABLE_BIT | GL_POINT_BIT | GL_LINE_BIT | GL_POLYGON_BIT );
	glDisable( GL_LIGHTING   );
	glDisable( GL_ALPHA_TEST );
	glDisable( GL_CULL_FACE  );

	// 3D volume texture
	const int iTexVolId = m_pParent->GetPusher()->GetTexture()->GetId();

	// lookup texture
	const int iTexLutId = m_pParent->GetProperties()->GetLookupTexture()->GetLookupTexture()->GetId();
	float aRange[2] = {0.0, 1.0};
	m_pParent->GetProperties()->GetLookupTable()->GetTableRange(aRange[0], aRange[1]);

	const VistaVector3D v3Scale = m_v3BoundsMax - m_v3BoundsMin;
	glPushMatrix();
	// glTranslatef(m_v3BoundsMin[0],m_v3BoundsMin[1],m_v3BoundsMin[2]);
 
	VistaTransformMatrix matBound;
	matBound.SetTranslation(m_v3BoundsMin);
	VistaTransformMatrix matScale;
	matScale.SetToScaleMatrix(v3Scale[0], v3Scale[1], v3Scale[2]);

	m_pProxy->UpdateBuffer(matModelView, matScale);
	glPopMatrix();
  
	glPushAttrib( GL_ENABLE_BIT | GL_CURRENT_BIT | GL_TEXTURE_BIT
				| GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  
	float aRenderParms[2] = { pProperties->GetOpacityFactor(), 1.0f };
	UpdateBuffer( iTexVolId, iTexLutId, aRange, aRenderParms );
 
	glPopAttrib();

	DrawBuffer(0);

	glPopAttrib();
}


void VflVolumeRendererRaycast::PrepareBuffer()
{
	glPushAttrib( GL_ENABLE_BIT | GL_CURRENT_BIT | GL_TEXTURE_BIT 
				| GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glDisable(GL_DITHER);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	// get clear color
	float clear_color[4];
	glGetFloatv(GL_COLOR_CLEAR_VALUE, clear_color);

	glClearColor(clear_color[0], clear_color[1], clear_color[2], 0.0);

	glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
	m_pTexBuffer->Bind();
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);                   

	m_pProxy->SetOrthoView();
	glClear(GL_COLOR_BUFFER_BIT);
}

void VflVolumeRendererRaycast::UpdateBuffer( const int iVolId, const int iTfId, 
											 const float aRange[2], 
											 const float aRenderParams[2]) 
{
	PrepareBuffer();
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
  
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D,iVolId);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_1D,iTfId);
	glActiveTexture(GL_TEXTURE3);
	m_pProxy->BindTexture(0);
	glActiveTexture(GL_TEXTURE4);
	m_pProxy->BindTexture(1);

	m_pRaycastShader->Bind();

	int iLoc;
	iLoc = m_pRaycastShader->GetUniformLocation("g_texVol");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 0);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texTF");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 1);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texEntry");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 3);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texExit");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 4);

	SetupUniforms(m_pRaycastShader, aRange, aRenderParams);
  
	FinalizeBuffer();
}  

void VflVolumeRendererRaycast::SetupUniforms( VistaGLSLShader* pShader, 
											  const float aRange[2], 
											  const float aRenderParams[2] )
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	// adjust sampling rate to data set size
	const float fSize = 
		static_cast<float>( std::max(m_aDataDims[0],
							std::max(m_aDataDims[1], m_aDataDims[2])));

	const float fDensity  = pProperties->GetSamplingDensity() ;
	const float fStepSize = 1.0f/( fDensity*1.4f*fSize); // sqrt(2) * size -> slices in diagonal cross section
	const float fExtra    = 0.75f; 
	const int   iNumSteps = 1 + int(1.0f/(fStepSize*fExtra) + 0.5f); 
	const float aTfRange[2] = {   1.0f / (aRange[1]-aRange[0]),
							-aRange[0] / (aRange[1]-aRange[0]) };

	int iLoc;
	iLoc = pShader->GetUniformLocation("g_fStepSize");
	if(iLoc >= 0) pShader->SetUniform(iLoc,fStepSize);
	iLoc = pShader->GetUniformLocation("g_iNumSteps");
	if(iLoc >= 0) pShader->SetUniform(iLoc, iNumSteps ); 
	iLoc = pShader->GetUniformLocation("g_v2RenderParams");
	if(iLoc >= 0) pShader->SetUniform(iLoc, 2, 1, aRenderParams);
	iLoc = pShader->GetUniformLocation("g_v2TFRange");
	if(iLoc >= 0) pShader->SetUniform(iLoc, 2, 1, aTfRange);
}

void VflVolumeRendererRaycast::FinalizeBuffer()
{
	m_pProxy->DrawQuad();
	m_pRaycastShader->Release();

	m_pProxy->RestoreView();
	m_pTexBuffer->Release();
	glPopAttrib();
}

void VflVolumeRendererRaycast::DrawBuffer(const unsigned int iWhich) 
{
	if(iWhich >= 1)
		return; // no valid buffer

	glPushAttrib( GL_ENABLE_BIT | GL_CURRENT_BIT | GL_TEXTURE_BIT 
				| GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );

	m_pProxy->SetOrthoView();
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
 // glDisable(GL_ALPHA_TEST);
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	m_pTexture->Bind();
	m_pTexture->Enable();
	m_pProxy->DrawQuad();
	m_pProxy->RestoreView();
	glPopAttrib();
}
      
void VflVolumeRendererRaycast::Resize(	const int iWidth, 
										const int iHeight, 
										const bool bForce )
{
	if(bForce || m_pProxy->Width() != iWidth || m_pProxy->Height() != iHeight)
	{
		m_pProxy->Resize(iWidth,iHeight,bForce);

		m_pTexBuffer->Bind();
		m_pTexture->Bind();
		m_pTexture->SetWrapS(GL_CLAMP);
		m_pTexture->SetWrapT(GL_CLAMP);
		m_pTexture->SetMinFilter(GL_LINEAR);
		m_pTexture->SetMagFilter(GL_LINEAR);
		glTexImage2D(	GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, 
						iWidth, iHeight, 0, GL_RGBA, GL_FLOAT, 0);
		glFramebufferTexture2DEXT( GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, 
							GL_TEXTURE_RECTANGLE_ARB, m_pTexture->GetId(), 0);
		m_pTexBuffer->Release();
		m_pTexture->Unbind();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererRaycast::Draw2D()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflVolumeRendererRaycast::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflVolumeRendererRaycast::GetReflectionableType() const
{
	return s_strVflVolumeRendererRaycastReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflVolumeRendererRaycast::AddToBaseTypeList( list<string> &rBtList ) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strVflVolumeRendererRaycastReflType);
	return i+1;
}

void VflVolumeRendererRaycast::ObserverUpdate(	IVistaObserveable *pObs,
												int msg, int ticket)
{
	if( msg == VistaViewport::VistaViewportProperties::MSG_SIZE_CHANGE)
	{
		int iWidth, iHeight;
		GetVistaSystem()->GetDisplayManager()->GetViewports().begin()->second
				->GetViewportProperties()->GetSize( iWidth, iHeight);
		Resize( iWidth, iHeight );
	}
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
