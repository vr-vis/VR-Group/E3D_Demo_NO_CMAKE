/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <vector>
#include <limits>
#include <algorithm>

#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaRenderbuffer.h>
#include <VistaOGLExt/VistaBufferObject.h>
#include <VistaOGLExt/VistaFramebufferObj.h>
#include <VistaOGLExt/VistaTexture.h>
#include <VistaBase/VistaTransformMatrix.h>
#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/DisplayManager/VistaDisplayManager.h>
#include <VistaKernel/DisplayManager/VistaDisplaySystem.h>
#include "../../Visualization/VflRenderNode.h"
#include "../../Visualization/VflRenderable.h"

#include "VflVolumeRendererRaycastProxy.h"
#include "VflRaycastSlicer.h"

#if defined(DARWIN)
  #include <GLUT/glut.h>
#else
  #if defined(USE_NATIVE_GLUT)
    #include <GL/glut.h>
  #else
    #include <GL/freeglut.h>
  #endif
#endif

VflVolumeRaycastProxy::VflVolumeRaycastProxy(	const int iWidth, 
												const int iHeight,
												const float fNearPlane )
	:	m_pRenderNode(0) 
	,	m_iWidth(iWidth)
	,	m_iHeight(iHeight)
	,	m_fNearPlane(fNearPlane)
	,	m_pVboProxy(0)
	,	m_pTexBuffer(0)
	,	m_pEntryShader( new VistaGLSLShader )
	,	m_pGeomShader(  new VistaGLSLShader )
	,	m_pDepthBuffer( new VistaRenderbuffer() )
{
	CreateShader();      
	CreateProxy(); 
	CreateBuffers();
}
    
VflVolumeRaycastProxy::~VflVolumeRaycastProxy()
{
	delete m_pVboProxy;
	delete m_pTexBuffer;
	delete m_pEntryShader;
	delete m_pGeomShader;
	delete m_pDepthBuffer;
	delete m_vecTextures[0];
	delete m_vecTextures[1];
}

void VflVolumeRaycastProxy::SetRenderNode(VflRenderNode* pNode)
{
	m_pRenderNode = pNode;
}
    
void VflVolumeRaycastProxy::DrawGeometry( const VistaTransformMatrix& matModelview, 
										  const VistaTransformMatrix& matLocal ) 
{
	if(!m_pRenderNode)
	{
		return;
	}
	const int iNumRenderables = m_pRenderNode->GetNumberOfRenderables();

	float aMV[16];
	glGetFloatv( GL_MODELVIEW_MATRIX, aMV );
	VistaTransformMatrix matFinal( aMV ); 
	matFinal = matFinal.GetInverted() * matLocal.GetInverted();
 
	float aInverse[16];
	matFinal.GetValues(aInverse);

	m_pGeomShader->Bind();
	glUniformMatrix4fv( m_pGeomShader->GetUniformLocation("mv_inv"), 
						1, false, aInverse);

	for(int i=0; i < iNumRenderables; ++i)
	{
		IVflRenderable* pRen = m_pRenderNode->GetRenderable(i);
		int iMode = pRen->GetRegistrationMode();
		if(pRen->GetVisible() && (iMode & IVflRenderable::OLI_DRAW_OPAQUE) ) 
		{
			// test for oblique slicers...
			if(VflRaycastSlicer* pSlicer = dynamic_cast<VflRaycastSlicer*>(pRen))
			{
				pSlicer->DrawCustom();
			}
			else
				pRen->DrawOpaque();
		}
	}
	m_pGeomShader->Release();
}

void VflVolumeRaycastProxy::UpdateBuffer( 
								const VistaTransformMatrix& matModelview, 
								const VistaTransformMatrix& matLocalTransform )
{
	VistaTransformMatrix matLocal = matLocalTransform.GetTranspose();
	float aLocal[16];
	matLocal.GetValues(aLocal);  
 
	glPushAttrib( GL_ENABLE_BIT | GL_CURRENT_BIT | GL_TEXTURE_BIT 
				| GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glDisable(GL_DITHER);
	glDisable(GL_LIGHTING);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
	m_pTexBuffer->Bind();
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + 0);                   

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// draw near plane with depth buffer turned off
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
  
	DrawViewerBox(matLocalTransform);
 
	// draw exit points only into depth buffer
	glColorMask(0, 0, 0, 0);
	glPushMatrix();
	glMultMatrixf(aLocal);
	DrawExit();
	glPopMatrix();
	// draw possible geometry intersections
	DrawGeometry(matModelview, matLocalTransform); 
	glColorMask(1, 1, 1, 1);

	// draw entry points
	glEnable(GL_DEPTH_TEST);
	glPushMatrix();
	glMultMatrixf(aLocal);
	DrawEntry();
	glPopMatrix();
  
	glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
	m_pTexBuffer->Bind();
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + 1);                   
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
	glMultMatrixf(aLocal);
	DrawExit();
	glPopMatrix();

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	DrawGeometry(matModelview, matLocalTransform); 

	m_pTexBuffer->Release();
	glPopAttrib();
}


void VflVolumeRaycastProxy::BindTexture(const unsigned int iWhich)
{
	if(iWhich >= 2)
		return; // no valid buffer
	m_vecTextures[iWhich]->Bind();
	m_vecTextures[iWhich]->Enable();
}

void VflVolumeRaycastProxy::DrawBuffer(const unsigned int iWhich)
{
	if(iWhich >= 2)
		return; // no valid buffer

	glPushAttrib( GL_ENABLE_BIT | GL_CURRENT_BIT | GL_TEXTURE_BIT 
				| GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	SetOrthoView();
	m_vecTextures[iWhich]->Bind();
	m_vecTextures[iWhich]->Enable();
	DrawQuad();
	RestoreView();

	glPopAttrib();

}

void VflVolumeRaycastProxy::DrawFaces(GLenum eCullMode)
{
	glPushAttrib( GL_ENABLE_BIT );
	glEnable( GL_CULL_FACE );
	glCullFace( eCullMode );
	DrawProxy();
	glPopAttrib();
}

void VflVolumeRaycastProxy::DrawEntry()
{
	DrawFaces(GL_BACK);
}

void VflVolumeRaycastProxy::DrawExit()
{
	DrawFaces(GL_FRONT);
}


void VflVolumeRaycastProxy::DrawProxy(const bool bAsProxy)
{
	// draw proxy:
	m_pVboProxy->BindAsVertexDataBuffer();
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,GL_FLOAT,0,0);
	// use shader to setup volume entry and exit points for rays
	if(bAsProxy) m_pEntryShader->Bind();
	glDrawArrays(GL_QUADS,0,m_nNumElements);
	if(bAsProxy) m_pEntryShader->Release();
	m_pVboProxy->Release();
	glDisableClientState(GL_VERTEX_ARRAY);
}

void VflVolumeRaycastProxy::CreateShader()
{
	// map vertex coordinates to color
	const std::string vert_source = "										\n\
		void main(void)														\n\
		{																	\n\
			gl_TexCoord[0] = gl_Vertex;										\n\
			gl_ClipVertex = gl_ModelViewMatrix*gl_Vertex;					\n\
			gl_Position = ftransform();										\n\
		}																	\n";

	const std::string frag_source =  "										\n\
		void main(void)														\n\
		{																	\n\
			gl_FragColor = gl_TexCoord[0];									\n\
		}																	\n";

	const std::string vert_geo_source =  "									\n\
		uniform mat4 mv_inv;												\n\
		void main(void)														\n\
		{																	\n\
			gl_TexCoord[0] = mv_inv * gl_ModelViewMatrix*gl_Vertex;			\n\
			gl_ClipVertex = gl_ModelViewMatrix*gl_Vertex; 					\n\
			gl_Position = ftransform();										\n\
		}																	\n";

	m_pEntryShader->InitFromStrings(vert_source, frag_source);
	m_pEntryShader->Link();

	// map vertex coordinates to color for geometries
	m_pGeomShader->InitFromStrings(vert_geo_source, frag_source);
	m_pGeomShader->Link();
}

void VflVolumeRaycastProxy::CreateProxy()
{
	// create vbo from proxy box 

	VistaVector3D aCubeCorners[8];
	//  setup cube
	aCubeCorners[0] = VistaVector3D( 0, 0, 0 ); // left  ground back
	aCubeCorners[1] = VistaVector3D( 1, 0, 0 ); // right ground back
	aCubeCorners[2] = VistaVector3D( 1, 1, 0 ); // right top    back
	aCubeCorners[3] = VistaVector3D( 0, 1, 0 ); // left  top    back
	aCubeCorners[4] = VistaVector3D( 0, 0, 1 ); // left  ground front
	aCubeCorners[5] = VistaVector3D( 1, 0, 1 ); // right ground front
	aCubeCorners[6] = VistaVector3D( 1, 1, 1 ); // right top    front
	aCubeCorners[7] = VistaVector3D( 0, 1, 1 ); // left  top    front

	
	const int indices[] =	{ 6, 7, 4, 5	// front
							, 0, 3, 2, 1	// back
							, 6, 5, 1, 2	// right
							, 4, 7, 3, 0	// left
							, 3, 7, 6, 2	// top
							, 1, 5, 4, 0 };	// ground

	std::vector<VistaVector3D> vecBox(24);
	for(int i=0; i < 24; ++i)
	{
		vecBox[i] = aCubeCorners[indices[i]];
	}

	m_nNumElements = static_cast<int>(vecBox.size());
	m_pVboProxy = new VistaBufferObject();
	m_pVboProxy->BindAsVertexDataBuffer();
  
	std::vector<float> vecTmp(3 * vecBox.size());
	for(size_t i = 0; i < vecBox.size(); ++i)
	{
		vecTmp[3*i+0] = vecBox[i][0];
		vecTmp[3*i+1] = vecBox[i][1];
		vecTmp[3*i+2] = vecBox[i][2];
	}
	
	const int iSize = sizeof(float) * 3 * static_cast<int>(vecBox.size());
	m_pVboProxy->BufferData(iSize,&vecTmp[0],GL_STATIC_DRAW_ARB);
	m_pVboProxy->Release();

	// store corners and  indices:
	for(int i = 0; i < 8; ++i)
		m_vecCorners.push_back(aCubeCorners[i]);
	// store indices for box outline
	int box_ind[] = {	6, 7,	7, 4,	4, 5,	5, 6,
						0, 3,	3, 2,	2, 1,	0, 1, 
						7, 3,	4, 0,	1, 5,	6, 2 };
	for(int i=0; i < 12; ++i)
	{
		int aTemp[2] = {box_ind[2*i],box_ind[(2*i)+1]};
		m_vecCornerIndices.push_back(VistaVector<int,2>(aTemp));
	}

}

void VflVolumeRaycastProxy::DrawQuad() 
{
	glBegin(GL_QUADS);
	glTexCoord2i(       0,        0 ); glVertex2i(       0,        0 );
	glTexCoord2i( Width(),        0 ); glVertex2i( Width(),        0 );
	glTexCoord2i( Width(), Height() ); glVertex2i( Width(), Height() );
	glTexCoord2i(       0, Height() ); glVertex2i(       0, Height() );
	glEnd();
}

void VflVolumeRaycastProxy::RestoreView()
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	glPopMatrix();
	glPopAttrib();
}


void VflVolumeRaycastProxy::SetOrthoView()
{
	glPushAttrib(GL_VIEWPORT_BIT);
	glViewport( 0, 0, Width(), Height()) ;
	glMatrixMode(GL_PROJECTION);

	glPushMatrix();

	glLoadIdentity();
	glOrtho(0.0, Width(), 0.0, Height(), -1.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
}

void VflVolumeRaycastProxy::CreateBuffers()
{
	m_pTexBuffer = new VistaFramebufferObj();
	VistaTexture* pTex0 = new VistaTexture( GL_TEXTURE_RECTANGLE_ARB );
	VistaTexture* pTex1 = new VistaTexture( GL_TEXTURE_RECTANGLE_ARB );
	m_vecTextures.push_back(pTex0);
	m_vecTextures.push_back(pTex1);
	m_pTexBuffer->Bind();
	for(int i=0; i < 2; ++i)
	{
		m_vecTextures[i]->Bind();
		glTexImage2D(   GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA32F_ARB,
						m_iWidth, m_iHeight, 0, GL_RGBA, GL_FLOAT, 0 );
		m_vecTextures[i]->SetWrapS(GL_CLAMP);
		m_vecTextures[i]->SetWrapT(GL_CLAMP);
		m_vecTextures[i]->SetMinFilter(GL_LINEAR);
		m_vecTextures[i]->SetMagFilter(GL_LINEAR);
		glFramebufferTexture2DEXT( 
				GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT+i, 
				GL_TEXTURE_RECTANGLE_ARB, m_vecTextures[i]->GetId(), 0);
	}
	m_pTexBuffer->Release();

	// create offscreen buffer width depth buffer
	m_pDepthBuffer->Init(GL_DEPTH_COMPONENT24, m_iWidth, m_iHeight); 

	AttachDepthBuffer();
}

void VflVolumeRaycastProxy::Resize( const int iWidth, 
									const int iHeight, 
									const bool bForce )
{
	if(iWidth == m_iWidth && iHeight == m_iHeight)
		return;

	m_pTexBuffer->Bind();

	m_iWidth = iWidth;
	m_iHeight = iHeight;
	for(int i=0; i < 2; ++i)
	{
		m_vecTextures[i]->Bind();
		glTexImage2D(   GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA32F_ARB, 
						m_iWidth, m_iHeight, 0, GL_RGBA, GL_FLOAT, 0 );
	}
	m_pTexBuffer->Release();
	delete m_pDepthBuffer;
	m_pDepthBuffer = new VistaRenderbuffer();
	m_pDepthBuffer->Init( GL_DEPTH_COMPONENT24, m_iWidth, m_iHeight ); 
	AttachDepthBuffer();
}

void VflVolumeRaycastProxy::AttachDepthBuffer()
{
	// attach depth buffer to target nr. 0 and 1
	glBindTexture( GL_TEXTURE_RECTANGLE_ARB, 0 );
	m_pTexBuffer->Bind();
	glDrawBuffer( GL_COLOR_ATTACHMENT0_EXT + 0 );                   
	m_pTexBuffer->Attach( m_pDepthBuffer, GL_DEPTH_ATTACHMENT_EXT );
	glBindTexture( GL_TEXTURE_RECTANGLE_ARB, 0 );
	m_pTexBuffer->Bind();
	glDrawBuffer( GL_COLOR_ATTACHMENT0_EXT + 1 );                   
	m_pTexBuffer->Attach( m_pDepthBuffer, GL_DEPTH_ATTACHMENT_EXT );
	m_pTexBuffer->Release(); 
}

const bool Intersect( const VistaVector3D& v3P0, const VistaVector3D& v3P1,
						float fZ, VistaVector3D& v3IP)
{
	if (v3P0[2] < fZ && v3P1[2] < fZ)    return false;
	if (v3P0[2] > fZ && v3P1[2] > fZ)    return false;
	if (fabs(v3P0[2] - v3P1[2]) < 1e-10) return false;

	float t = (fZ - v3P0[2]) / (v3P1[2] - v3P0[2]);
	v3IP = v3P0*(1.0f-t)  + v3P1*t;

	return true;
}

// This functor can be used to sort points in ccw order around a given center point 
struct CmpIPs
{
	CmpIPs(const VistaVector3D& v3Center) : m_v3Center(v3Center) {}

	bool operator()(const VistaVector3D& v3P0, const VistaVector3D& v3P1) const
	{
		VistaVector3D d0 = v3P0; d0 -= m_v3Center; d0.Normalize();
		VistaVector3D d1 = v3P1; d1 -= m_v3Center; d1.Normalize();

		float a0 = acos(d0[0]); 
		if (d0[1] < 0) a0 = 2.0f*Vista::Pi-a0;
		float a1 = acos(d1[0]); 
		if (d1[1] < 0) a1 = 2.0f*Vista::Pi-a1;

		return a0 < a1;
	}

	const VistaVector3D m_v3Center;
};

void VflVolumeRaycastProxy::DrawViewerBox( const VistaTransformMatrix &matLocal )
{
	VistaDisplaySystem *pDSys = 
			GetVistaSystem ()->GetDisplayManager()->GetDisplaySystem(0);
  
	VistaQuaternion qOri = 
			pDSys->GetDisplaySystemProperties()->GetViewerOrientation();
	qOri.Normalize();
	VistaTransformMatrix matQ(qOri);

	float aMat[16];
	matQ.GetValues(aMat);

	float aMV[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, aMV);
	VistaTransformMatrix matFinal(aMV); 
	matFinal = matFinal.GetInverted() * matLocal.GetInverted();
 
	float aInverse[16];
	matFinal.GetValues(aInverse);

	m_pGeomShader->Bind();
	int iloc = m_pGeomShader->GetUniformLocation("mv_inv");
	if(iloc != -1) glUniformMatrix4fv( iloc, 1, false, aInverse);

	glPushMatrix();
	glLoadIdentity();

	glMultMatrixf(aMat);
	glutSolidSphere( 0.25f, 8, 4 );

	m_pGeomShader->Release();

	glPopMatrix();
}

void VflVolumeRaycastProxy::DrawNearPlane(const float fNearPlane, const float* pMatrix)
{
	VistaTransformMatrix matModelview((float*)pMatrix,true);
	VistaTransformMatrix matModelviewInverse= matModelview.GetInverted();

	VistaVector3D aTransformedCorners[8];

	// transform cube aTransformedCorners using current modelview matrix
	unsigned int i;
	for (i=0; i<8; ++i)
		aTransformedCorners[i] = matModelview.Transform(m_vecCorners[i]);

	std::vector<VistaVector3D>  aPoints;  aPoints.reserve(12);
	VistaVector3D               ip, bc;

	// near clipping plane + offset
	const float z = -fNearPlane -0.001f; 
  
	// intersect all 12 edges with near plane
	aPoints.clear();
	for (unsigned int j=0; j<12; ++j)
	{
		const VistaVector3D p0 = aTransformedCorners[ m_vecCornerIndices[j][0] ];
		const VistaVector3D p1 = aTransformedCorners[ m_vecCornerIndices[j][1] ];
		if (Intersect(p0,p1, z,ip)) 
			aPoints.push_back(ip);
	}


	// need at least three intersection aPoints
	if (aPoints.size() < 3) return;

	// compute barycenter
	VistaVector3D v3Barycenter = VistaVector3D(0,0,0);
	for (unsigned int j=0; j<aPoints.size(); ++j)
		v3Barycenter += aPoints[j];
	v3Barycenter /= float(aPoints.size());

	// sort aPoints ccw around barycenter
	std::sort(aPoints.begin(), aPoints.end(), CmpIPs(v3Barycenter));

	// draw near plane
	glPushMatrix();
	glLoadIdentity();
	glBegin(GL_POLYGON);
	for (unsigned int j=0; j<aPoints.size(); ++j)
	{
		VistaVector3D v3Color = matModelviewInverse.Transform(aPoints[j]);
		// clamp to [0,1]
		for(int i=0; i < 3; ++i)
		{
			v3Color[i] = std::max(0.0f,v3Color[i]);
			v3Color[i] = std::min(1.0f,v3Color[i]);
		}
		glColor3f(v3Color[0],v3Color[1],v3Color[2]);
		glVertex3f(aPoints[j][0],aPoints[j][1],aPoints[j][2]);
	}
	glEnd();
	glPopMatrix();
}




