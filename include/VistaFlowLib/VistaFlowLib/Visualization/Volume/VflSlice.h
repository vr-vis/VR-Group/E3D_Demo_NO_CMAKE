/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLSLICE_H
#define _VFLSLICE_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaBase/VistaVectorMath.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAPI VflSlice
{
public:
	/**
	 * This enum is used to specify which plane will be modified.
	 * PT_RENDER_PLANE is the plane which represents the GL_QUAD
	 * PT_TEXTURE_PLANE is the plane within the texture object 
	 * which will be used to texture the PT_RENDER_PLANE
	 */
	enum EPlaneTarget
	{
		PT_RENDER_PLANE = 0,
		PT_TEXTURE_PLANE = 1
	};

	/**
	 *
	 */
	VflSlice();

	/**
	 * Initializes the slice via normal, center, dimensions, and rotation.
	 */
	VflSlice(const VistaVector3D &n1, const VistaVector3D &c1,
			  float w1, float h1, float r1,
			  const VistaVector3D &n2, const VistaVector3D &c2, 
			  float w2, float h2, float r2);

	/**
	 * Initializes the slice via (co-planar) points.
	 */
	VflSlice(const VistaVector3D &p1, const VistaVector3D &p2,
			  const VistaVector3D &p3, const VistaVector3D &p4, 
			  const VistaVector3D &t1, const VistaVector3D &t2,
			  const VistaVector3D &t3, const VistaVector3D &t4);

	virtual ~VflSlice();
	
	/**
	 * Renders the slice.
	 * @return Returns 'true' iff the OpenGL calls could be made.
	 *
	 * NOTE: The Draw() routine only specifies the vertex and texture coordinates but
	 *		 doesn't perform texture/shader binding or matrix preparations of any kind.
	 *		 Hence the current OpenGL state has to be set correctly before calling this
	 *		 function!
	 */
	bool Draw();
	
	// Selectors
	bool SetNormal(EPlaneTarget target, const VistaVector3D &n);
	/**
	 * NOTE: Will return false (invalid normal) if the target plane is not in implicit mode.
	 */
	bool GetNormal(EPlaneTarget target, VistaVector3D &v3dNormal);


	bool SetCenter(EPlaneTarget target, const VistaVector3D &c);
	/**
	 * NOTE: Will return false (invalid center) if the target plane is not in implicit mode.
	 */
	bool GetCenter(EPlaneTarget target, VistaVector3D &v3dCenter);


	bool SetDimensions(EPlaneTarget target, float w, float h);
	/**
	 * NOTE: Will return false (invalid dims) if the target plane is not in implicit mode.
	 */
	bool GetDimensions(EPlaneTarget target, float &fWidth, float &fHeight);


	/**
	 * Sets the rotation around the normal (normal == rotation axis).
	 * @return Returns 'true' iff the operation succeeded.
	 */
	bool SetRotation(EPlaneTarget target, float fDegrees);
	/**
	 * NOTE: Will return false (invalid rotation) if the target plane is not in implicit mode.
	 */
	bool GetRotation(EPlaneTarget target, float &fDegrees);
	
	bool SetPoints(EPlaneTarget target, const VistaVector3D &p1, const VistaVector3D &p2, 
		 const VistaVector3D &p3, const VistaVector3D &p4);
	bool SetPoints(EPlaneTarget target, const VistaVector3D (&v3dPoints)[4] );
	bool GetPoints(EPlaneTarget target, VistaVector3D (&v3dPoints)[4]);
	/**
	 * Returns the specified point.
	 *
	 * NOTE: The indices range from 1 to 4!
	 */
	bool GetPoint(EPlaneTarget target, int iPointIndex, VistaVector3D &v3dPoint);


	/**
	 * This function is to determine whether the plane specified by target is constructed
	 * from normals, center, and dimension data (implicit) or not (explicit).
	 * This is improtant for some Get-selectors because they only work in implict mode.
	 * @return 'true' is returned in case the plane is constructed implicitly.
	 */
	bool GetIsImpModeActive(EPlaneTarget target);


	/**
	 * Planes won't be drawn if deactivated.
	 */
	void Activate();
	void Deactivate();
	bool GetIsActive();

protected:
	VistaVector3D		m_v3dPoint1,
						m_v3dPoint2,
						m_v3dPoint3,
						m_v3dPoint4,
						m_v3dTexture1,
						m_v3dTexture2,
						m_v3dTexture3,
						m_v3dTexture4;

	VistaVector3D		m_v3dNormal1,
						m_v3dCenter1,
						m_v3dNormal2,
						m_v3dCenter2;

	float				m_fWidth1,
						m_fHeight1,
						m_fWidth2,
						m_fHeight2;

	float				m_fRotation1,
						m_fRotation2;

	bool				m_bIsReadyToRender,
						m_bUseImpModeRenderPlane,
						m_bUseImpModeTexturePlane,
						m_bIsActive;

private:
	/**
	 * Converts the supplied normal, center, and dimensions into point representation.
	 * @return Returns 'true' iff the conversion was successful.
	 */
	bool	FromImplicitRep();
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLSLICE_H


