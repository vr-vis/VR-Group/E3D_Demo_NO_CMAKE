

set( RelativeDir "./Visualization/Volume/shaders" )
set( RelativeSourceGroup "Source Files\\Visualization\\Volume\\Shaders" )

set( DirFiles
	VflRaycastSlicer_Linear_frag.glsl
	VflRaycastSlicer_Nearest_frag.glsl
	VflTriplanarSlicer_frag.glsl
	VflTriplanarSlicer_vert.glsl
	VflVolumeRenderer3DTexture_frag.glsl
	VflVolumeRenderer3DTexture_PreInt_frag.glsl
	VflVolumeRenderer3DTexture_PreInt_vert.glsl
	VflVolumeRenderer3DTexture_vert.glsl
	VflVolumeRendererRaycast_frag.glsl
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

