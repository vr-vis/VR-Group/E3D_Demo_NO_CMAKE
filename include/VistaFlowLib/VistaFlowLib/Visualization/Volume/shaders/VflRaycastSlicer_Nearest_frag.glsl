uniform sampler3D tex_vol;  //< 3D volume 
uniform sampler1D tex_tf;  //< transfer function 
uniform vec2 tf_aRange;	//< 1/(max-min) ; -min/(max-min) for transfer function lookups
uniform vec3 size; //< texture size

void main(void)
{
	vec3 c = vec3(gl_TexCoord[0].x*size.x,gl_TexCoord[0].y*size.y,gl_TexCoord[0].z*size.z);
	vec3 cf = floor(c);
	vec3 cc = ceil(c);
	vec3 cd = c - cf;
    
	cc = vec3( cc.x/size.x,cc.y/size.y,cc.z/size.z);
	cf = vec3( cf.x/size.x,cf.y/size.y,cf.z/size.z);
    
	vec4 i10 = texture1D(tex_tf,tf_aRange.y + tf_aRange.x * texture3D(tex_vol, vec3(cf.x,cf.y,cf.z   ) ).a);
	vec4 i11 = texture1D(tex_tf,tf_aRange.y + tf_aRange.x * texture3D(tex_vol, vec3(cf.x,cf.y,cc.z   ) ).a);
	vec4 i20 = texture1D(tex_tf,tf_aRange.y + tf_aRange.x * texture3D(tex_vol, vec3(cf.x,cc.y,cf.z   ) ).a);
	vec4 i21 = texture1D(tex_tf,tf_aRange.y + tf_aRange.x * texture3D(tex_vol, vec3(cf.x,cc.y,cc.z   ) ).a);
	vec4 j10 = texture1D(tex_tf,tf_aRange.y + tf_aRange.x * texture3D(tex_vol, vec3(cc.x,cf.y,cf.z   ) ).a);
	vec4 j11 = texture1D(tex_tf,tf_aRange.y + tf_aRange.x * texture3D(tex_vol, vec3(cc.x,cf.y,cc.z   ) ).a);
	vec4 j20 = texture1D(tex_tf,tf_aRange.y + tf_aRange.x * texture3D(tex_vol, vec3(cc.x,cc.y,cf.z  ) ).a);
	vec4 j21 = texture1D(tex_tf,tf_aRange.y + tf_aRange.x * texture3D(tex_vol, vec3(cc.x,cc.y,cc.z  ) ).a);
	vec4 i1 = mix(i10,i11,cd.z);
	vec4 i2 = mix(i20,i21,cd.z);
	vec4 j1 = mix(j10,j11,cd.z);
	vec4 j2 = mix(j20,j21,cd.z);
	vec4 w1 = mix(i1,i2,  cd.y);
	vec4 w2 = mix(j1,j2,  cd.y);
	
	gl_FragColor = mix(w1,w2, cd.x);
}