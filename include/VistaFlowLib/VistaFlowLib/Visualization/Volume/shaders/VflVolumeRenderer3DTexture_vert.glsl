uniform vec3 v3TCScale;	// scale for converting vertex positions to texture coordinates
uniform vec3 v3TCBias;	// bias for converting vertex positions to texture coordinates

// inputs:
// gl_Vertex			: vertex position

// outputs:
// gl_Position			: transformed vertex position
// gl_ClipVertex    : transformed clip vertex position, required if GL_CLIP_PLANE is used
// gl_TexCoord[0].xyz	: texture coordinates for 3D texture

void main(void)
{
	gl_Position = ftransform();
  gl_ClipVertex = gl_ModelViewMatrix*gl_Vertex;
	gl_TexCoord[0].xyz = gl_Vertex.xyz * v3TCScale + v3TCBias;
}
