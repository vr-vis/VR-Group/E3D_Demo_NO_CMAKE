/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/

#include <GL/glew.h>

#include "VflObliqueSlicer.h"

#include "../VflVisTiming.h"
#include "../VflLookupTexture.h"
#include "../VflVtkLookupTable.h"
#include "../../Data/VflUnsteadyCartesianGridPusher.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

#include <VistaBase/VistaStreamUtils.h>
#include <VistaKernel/GraphicsManager/VistaGeometry.h>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static const int s_iTransferFunctionTextureUnit = 0;
static const int s_iDataTextureUnit = 1;
static const int s_iNoiseTextureUnit = 2;

static const string s_strVertexShaderFile = "VflTriplanarSlicer_vert.glsl";
static const string s_strFragmentShaderFile = "VflTriplanarSlicer_frag.glsl";

struct S2DCoord
{
	float u;
	float v;
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflObliqueSlicer::VflObliqueSlicer()
	:	IVflVisObject()
	,	m_pShader(NULL)
	// 	,	m_bShowBorder(false)
	// 	,	m_fBorderWidth(1.0f)
	// 	,	m_pBorderColor(new VistaColor(VistaColor::WHITE))
	,	m_pData(NULL)
	,	m_pLookupTexture(NULL)
	,	m_pExpRenderPlane(NULL)
	,	m_bDrawToExpRenderPlane(false)
	,	m_bStaticMode(false)
	,	m_iStaticLevelIdx(-1)
	, m_pNoiseTexture(NULL)
{
	m_pShader = new VistaGLSLShader();

	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();

	string strVertexShader = rShaderReg.RetrieveShader(s_strVertexShaderFile);
	string strFragmentShader= rShaderReg.RetrieveShader(s_strFragmentShaderFile);

	if(	strVertexShader.empty()	|| strFragmentShader.empty() )
	{
		vstr::errp() << "[VflObliqueSlicer] - shaders not available..." << endl;
		return;
	}

	if(!m_pShader->InitFromStrings(strVertexShader, strFragmentShader))
	{
		vstr::errp() << "[VflObliqueSlicer] - unable to init shader..." << endl;
		delete m_pShader;
		m_pShader = NULL;
		return;
	}

	if(!m_pShader->Link())
	{
		vstr::errp() << "[VflObliqueSlicer]- unable to link shader..." << endl;
		return;
	}
	if(!m_pShader->IsReadyForUse())
		return;

	m_pShader->Bind();
	int data_loc = m_pShader->GetUniformLocation("Data");
	int noise_loc = m_pShader->GetUniformLocation("Noise");
	int tf_loc = m_pShader->GetUniformLocation("transfer_function");
	m_pShader->SetUniform(tf_loc, s_iTransferFunctionTextureUnit);
	m_pShader->SetUniform(data_loc, s_iDataTextureUnit);
	m_pShader->SetUniform(noise_loc, s_iNoiseTextureUnit);
	m_pShader->Release();

	m_pExpRenderPlane = NULL;
}

VflObliqueSlicer::~VflObliqueSlicer()
{
	for(size_t i=0; i<m_vecUnsteadyDataTextures.size(); ++i)
	{
		delete m_vecUnsteadyDataTextures[i];
	}
	m_vecUnsteadyDataTextures.clear();

	if(m_pLookupTexture)
		 delete m_pLookupTexture;
		
	if(m_pShader)
		delete m_pShader;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::Init()
{
	if(!m_pData)
	{
		vstr::errp() << "[VflObliqueSlicer::Init] - Set data before init." << endl;
		return false;
	}

	if(IVflVisObject::Init())
	{
		if(!InitTextures())
			return false;

		this->SetBoundsToData();

		return true;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   InitTextures                                                */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::InitTextures()
{
	if(!m_pData)
	{
		vstr::errp() << "[VflObliqueSlicer::InitTextures] - No data" << endl;
		return false;
	}

	int iLevels = m_pData->GetNumberOfLevels();

	if(m_vecUnsteadyDataTextures.size() != 0)
	{
		for(size_t i=0; i<m_vecUnsteadyDataTextures.size(); ++i)
			delete m_vecUnsteadyDataTextures[i];
	}

	m_vecUnsteadyDataTextures.resize(iLevels);

	vstr::debugi() << "[VflObliqueSlicer] creating textures .." << endl;

	GLenum glerr;

	// create data textures 
	int iDims[3];
	float fRange[2]; 
	float fNorm_range;
	int iNumOfTuples;	
	vector<float> vecData;
	VveCartesianGrid *pRawData;

	for(int i=0; i<iLevels; ++i)
	{
		if(	!m_pData->GetTypedLevelDataByLevelIndex(i) || 
			!m_pData->GetTypedLevelDataByLevelIndex(i)->GetData())
			continue;
		
		pRawData = m_pData->GetTypedLevelDataByLevelIndex(i)->GetData();
		pRawData->GetDataDimensions(iDims);
		pRawData->GetScalarRange(fRange);
		fNorm_range = 1.0f / (fRange[1]-fRange[0]);

		iNumOfTuples = iDims[0] * iDims[1] * iDims[2];
		if(int(vecData.size()) < iNumOfTuples)
			vecData.resize(iNumOfTuples);

		int iId = 0;

		if(pRawData->GetComponents() == 1)   // the cartesian grid contains only scalar data
		{
			float fValue[1];
			for(int z=0; z<iDims[2]; ++z)
			{
				for(int y=0; y<iDims[1]; ++y)
				{
					for(int x=0; x<iDims[0]; ++x)
					{
						pRawData->GetData(x, y, z, fValue);
						vecData[iId] = (fValue[0]-fRange[0]) * fNorm_range;
						++iId;
					}
				}
			}
		}

		if(pRawData->GetComponents() == 3)		// the cartesian grid contains only vector data       
		{
			for(int z=0; z<iDims[2]; ++z)
			{
				for(int y=0; y<iDims[1]; ++y)
				{
					for(int x=0; x<iDims[0]; ++x)
					{
						vecData[iId] = 0.0f;  // set default scalar value
						++iId;
					}
				}
			}
			vstr::warnp() << "[VflObliqueSlicer] data item contains no scalar field" << std::endl;
		}


		bool bUpload4Components = false;


		if(pRawData->GetComponents() == 4)	   // the cartesian grid contains vector and scalar data
		{
			if (!bUpload4Components)
			{
				float fValue[4];
				for (int z = 0; z < iDims[2]; ++z)
				{
					for (int y = 0; y < iDims[1]; ++y)
					{
						for (int x = 0; x < iDims[0]; ++x)
						{
							pRawData->GetData(x, y, z, fValue);
							vecData[iId] = (fValue[3] - fRange[0]) * fNorm_range;
							++iId;
						}
					}
				}
			}
			else
			{
				vecData.resize(iNumOfTuples * 4);
				float fValue[4];
				for (int z = 0; z < iDims[2]; ++z)
				{
					for (int y = 0; y < iDims[1]; ++y)
					{
						for (int x = 0; x < iDims[0]; ++x)
						{
							pRawData->GetData(x, y, z, fValue);
							/*vecData[iId] = (fValue[0] - fRange[0]) * fNorm_range;
							++iId; 
							vecData[iId] = (fValue[1] - fRange[0]) * fNorm_range;
							++iId; 
							vecData[iId] = (fValue[2] - fRange[0]) * fNorm_range;
							++iId; 
							vecData[iId] = (fValue[3] - fRange[0]) * fNorm_range;
							++iId;*/


							vecData[iId] = (fValue[0] * 0.5f) + 0.5f;
							++iId; 
							vecData[iId] = (fValue[1] * 0.5f) + 0.5f;
							++iId; 
							vecData[iId] = (fValue[2] * 0.5f) + 0.5f;
							++iId; 
							vecData[iId] = (fValue[3] - fRange[0]) * fNorm_range;
							++iId;
						}
					}
				}
			}
		}

		m_vecUnsteadyDataTextures[i] = new VistaTexture(GL_TEXTURE_3D);

		glTexParameteri(m_vecUnsteadyDataTextures[i]->GetTarget(),
			GL_TEXTURE_WRAP_S, GL_CLAMP);  
		glTexParameteri(m_vecUnsteadyDataTextures[i]->GetTarget(),
			GL_TEXTURE_WRAP_T, GL_CLAMP);  
		glTexParameteri(m_vecUnsteadyDataTextures[i]->GetTarget(),
			GL_TEXTURE_WRAP_R, GL_CLAMP);  
		glTexParameteri(m_vecUnsteadyDataTextures[i]->GetTarget(),
			GL_TEXTURE_MIN_FILTER, GL_LINEAR);  
		glTexParameteri(m_vecUnsteadyDataTextures[i]->GetTarget(),
			GL_TEXTURE_MAG_FILTER, GL_LINEAR);  

		if (!bUpload4Components)
		{
			glTexImage3D(m_vecUnsteadyDataTextures[i]->GetTarget(), 0, GL_LUMINANCE,
				iDims[0], iDims[1], iDims[2], 0, GL_LUMINANCE, GL_FLOAT,
				&vecData[0]);
		}
		else
		{
			glTexImage3D(m_vecUnsteadyDataTextures[i]->GetTarget(), 0, GL_RGBA8,
				iDims[0], iDims[1], iDims[2], 0, GL_RGBA, GL_FLOAT,
				&vecData[0]);
		}

		glerr = glGetError();
		if(glerr != GL_NO_ERROR)
		{
			vstr::errp() << "[VflObliqueSlicer] - Texture for Level <" << i
				<< "> could not be created" << endl;
		}
	}

	if(!m_pLookupTexture)
	{
		m_pLookupTexture = new VflLookupTexture(new VflVtkLookupTable, GL_LINEAR, true);
	}

	glerr = glGetError();
	if(glerr != GL_NO_ERROR){
		vstr::errp() << "[VflObliqueSlicer] - transfer function could not be created"
			<< endl;
	}

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTable                                              */
/*                                                                            */
/*============================================================================*/
IVflLookupTable* VflObliqueSlicer::GetLookupTable() const
{
	return m_pLookupTexture->GetLookupTable();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetLookupTable                                              */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetLookupTable(VflVtkLookupTable* pLut)
{
	if(m_pLookupTexture)
	{
		m_pLookupTexture->SetLookupTable(pLut);
	}
	else
	{
		m_pLookupTexture = new VflLookupTexture(pLut, GL_LINEAR);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTexture                                            */
/*                                                                            */
/*============================================================================*/
VflLookupTexture* VflObliqueSlicer::GetLookupTexture() const
{
	return m_pLookupTexture;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetData                                                     */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::SetData(VveUnsteadyCartesianGrid *pData)
{
	if(pData == NULL)
		return false;

	m_pData = pData;
	this->SetBoundsToData();
	InitTextures();

	return true;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData                                                     */
/*                                                                            */
/*============================================================================*/
VveUnsteadyCartesianGrid* VflObliqueSlicer::GetData()
{
	return m_pData;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdatePlaneParameters                                       */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::UpdatePlaneParameters()
{
	m_vIntersects.clear();
	m_vTexCoords.clear();

	// Calcuate the intersection with the 12 edges of the bounding box
	for(int i=0; i<12; ++i)
	{
		float fParam;
		if(IntersectionTest(m_v3dOrigins[i], m_v3dDirs[i], fParam))
			if(fParam >= 0.0f && fParam <= m_fLengths[i]) {
				VistaVector3D v3dTmp = m_v3dOrigins[i]
							+ fParam * m_v3dDirs[i]; // Dirs were normalized!
				bool bAlreadyAdded = false;
				for(unsigned int j=0; j<m_vIntersects.size(); ++j)
					if(m_vIntersects[j] == v3dTmp)
						bAlreadyAdded = true;

				if(!bAlreadyAdded)
					m_vIntersects.push_back(v3dTmp);
			}
	}

	// Get the text coords right
	for(unsigned int i=0; i<m_vIntersects.size(); ++i) {
		VistaVector3D v3dTmpTextCoord = m_oScaleToTexture.Transform(m_vIntersects[i] - m_v3dBoundsCOG) 
										 + VistaVector3D(0.5f, 0.5f, 0.5f);
		m_vTexCoords.push_back(v3dTmpTextCoord);
	}

	//vstr::outi() << "before:" << m_vIntersects.size() << endl;

	// Sort the std::vectors so the saved vertices can be easily drawn in ccw order
	if(m_vIntersects.size() > 2) 
	{
		// Get the normal of the plane
		VistaVector3D v3dPN;
		m_oObliqueSlice.GetNormal(VflSlice::PT_RENDER_PLANE, v3dPN);

		// Calculate the center of gravity of all vertices (will be origin for vectors COG -> vert_i)
		VistaVector3D v3dCOG(0.0f, 0.0f, 0.0f);
		for(unsigned int i=0; i<m_vIntersects.size(); ++i)
			v3dCOG += m_vIntersects[i];
		v3dCOG /= (float) m_vIntersects.size();

		std::vector<VistaVector3D> vSortedIntersects;
		std::vector<VistaVector3D> vSortedTextCoords;

		// We'll always start with the first vertex (first found intersection)
		vSortedIntersects.push_back(m_vIntersects[0]);
		vSortedTextCoords.push_back(m_vTexCoords[0]);
		bool bIntersectUsed[12];
		memset(bIntersectUsed, 0, 12 * sizeof(bool));
		bIntersectUsed[0] = true;

		int iNextVertIndex = -1;
		float fMinAngle = -1.0f; 

		// Actuall sort happens here
		for(unsigned int i=1; i<m_vIntersects.size(); ++i) {
			fMinAngle = -1.0f;
			int iNextVertIndex = -1;

			for(unsigned int j=1; j<m_vIntersects.size(); ++j)
			{
				// In case the vertex has already been written to the sorted list
				if(bIntersectUsed[j])
					continue;

				// COG -> Vertex_j vector
				VistaVector3D v3dNewVec = m_vIntersects[j] - v3dCOG;
				v3dNewVec.Normalize();
				// COG -> Last_saved_vertex vector
				VistaVector3D v3dLastVec = vSortedIntersects[i-1] - v3dCOG;
				v3dLastVec.Normalize();
				
				// If the cross product is negative we are moving into cw direction
				// (but we want to move ccw, so we ignore the vertex)
				if(v3dNewVec.Cross(v3dLastVec).Dot(v3dPN) < 0.0f)
					continue;

				// Calculate the angle between the two vectors
				// (The vector which has the smallest angle with the last written vertex's vector
				// contains the "nearest" (next) vertex on the route around the normal)
				float dotti = v3dLastVec.Dot(v3dNewVec);
				if(dotti > fMinAngle || iNextVertIndex == -1)
				{
					fMinAngle = v3dLastVec.Dot(v3dNewVec);
					iNextVertIndex = j;
				}
			}
			bIntersectUsed[iNextVertIndex] = true;
			vSortedIntersects.push_back(m_vIntersects[iNextVertIndex]);
			vSortedTextCoords.push_back(m_vTexCoords[iNextVertIndex]);
		}

		m_vIntersects = vSortedIntersects;
		m_vTexCoords = vSortedTextCoords;
	}

	m_oObliqueSlice.Activate();

	if(m_bDrawToExpRenderPlane && m_pExpRenderPlane)
	{
		// transform m_vIntersects to fit in the defined plane 
		TransformIntersections();
	}
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   TranformIntersections                                       */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::TransformIntersections()
{
	const size_t nNumOfIntersects = m_vIntersects.size();
	if(m_vExpIntersects.size() != nNumOfIntersects)
		m_vExpIntersects.resize(nNumOfIntersects);
	VistaVector3D v3IntersectsMiddle, v3Dist, v3Tmp;

	// map the slicing plane normal to the render plane normal
	VistaVector3D v3RenderPlaneNormal;
	VistaVector3D v3SlicingPlaneNormal;
	m_pExpRenderPlane->GetNormal(VflSlice::PT_RENDER_PLANE, v3RenderPlaneNormal);
	v3RenderPlaneNormal[3] = 0.0f;
	v3RenderPlaneNormal.Normalize();
	m_oObliqueSlice.GetNormal(VflSlice::PT_RENDER_PLANE, v3SlicingPlaneNormal);
	v3SlicingPlaneNormal[3] = 0.0f;
	v3SlicingPlaneNormal.Normalize();
	VistaQuaternion qMapNormal(v3SlicingPlaneNormal, v3RenderPlaneNormal);
	qMapNormal.Normalize();

	// rotate the intersects points
	for(size_t i=0; i<nNumOfIntersects; ++i)
		m_vExpIntersects[i] = qMapNormal.Rotate(m_vIntersects[i]);   
	
	// get the middle position of current intersection
	for(size_t i=0; i<nNumOfIntersects; ++i)
		v3IntersectsMiddle += m_vExpIntersects[i];
	v3IntersectsMiddle = v3IntersectsMiddle / static_cast<float>(nNumOfIntersects);

	// transform the intersection points such that v3Middle = render plane's middle point
	VistaVector3D v3RenderPlaneMiddle;
	m_pExpRenderPlane->GetCenter(VflSlice::PT_RENDER_PLANE, v3RenderPlaneMiddle);
	VistaVector3D v3Move = v3RenderPlaneMiddle - v3IntersectsMiddle;
	for(size_t i=0; i<nNumOfIntersects; ++i)
		m_vExpIntersects[i] = m_vExpIntersects[i] + v3Move;

	// --------------------------------------------------------
	// find the farthest U and V coordinates to scale the result
	// if every intersection points are in the rendering quad, find a maximizing scale factor
	//                    -> scale_max = max(Uscale,Vscale) > 1  
	// if one of the intersection points is outside of the rendering quad, find a minimizing scale factor
	//                    -> scale_min = min(Uscale,Vscale) < 1 
	// --------------------------------------------------------
	float fScale = 1.0f;

	// == compute the (u,v) coordinates of the intersection points to determine the scale factor ==

	// compute the 2D-coord defining vectors u and v
	// 2D coord (x,y) of a 3D point p lying on the plane P(x,y) = m + x*u + y*v
	// x := uT . (p-m), y := vT . (p-m)	

	// assumption of the order of plane's corner points (of type VflSlice)
    //   0 o----------------o 3
    //     |                |
	//   1 o----------------o 2
	//  that's why, u:=(v2-v1) and v:=(v0-v1)
	VistaVector3D v3CornerPts[4];
	m_pExpRenderPlane->GetPoints(VflSlice::PT_RENDER_PLANE, v3CornerPts);
	VistaVector3D v3U = v3CornerPts[2] - v3CornerPts[1];
	v3U.Normalize();
	VistaVector3D v3V = v3CornerPts[0] - v3CornerPts[1];
	v3V.Normalize();

	// compute plane border's u and v coord
	// ->  aPlaneBorders[4] := (u_min, u_max, v_min, v_max)
	// p1 = (u_min, v_min), p3 = (u_max, v_max)
	float aPlaneBorders[4];
	v3Tmp = v3CornerPts[1] - v3RenderPlaneMiddle;
	aPlaneBorders[0] = v3U * v3Tmp;
	aPlaneBorders[2] = v3V * v3Tmp;
	v3Tmp = v3CornerPts[3] - v3RenderPlaneMiddle;
	aPlaneBorders[1] = v3U * v3Tmp;
	aPlaneBorders[3] = v3V * v3Tmp;

	// compute intersection points' 2D-coords and find farthest u and v
	float fFarthestU=0.0f, fFarthestV=0.0f;
	vector<S2DCoord> vec2DCoords;
	for(unsigned int i=0; i<nNumOfIntersects; ++i)
	{
		S2DCoord oCoord;
		v3Tmp = (m_vExpIntersects[i]-v3RenderPlaneMiddle);
		oCoord.u = v3U * v3Tmp;
		oCoord.v = v3V * v3Tmp;
		vec2DCoords.push_back(oCoord);

		if(fabs(oCoord.u) > fabs(fFarthestU))
			fFarthestU = oCoord.u;
		if(fabs(oCoord.v) > fabs(fFarthestV))
			fFarthestV = oCoord.v;
	}

	// determine scales in u and v direction
	float fRenderPlaneWidth, fRenderPlaneHeight; 
	m_pExpRenderPlane->GetDimensions(VflSlice::PT_RENDER_PLANE, fRenderPlaneWidth, fRenderPlaneHeight);
	float fUScale = fabs(fRenderPlaneWidth/(fFarthestU*2.0f));
	float fVScale = fabs(fRenderPlaneHeight/(fFarthestV*2.0f));

	// minimize or maximize?
	bool bMinimize = false;
	// if the farthest u and v coordinates is outside of render plane's boundary, then minimize
	if(fFarthestU < aPlaneBorders[0] || fFarthestU > aPlaneBorders[1])
		bMinimize = true;
	if(fFarthestV < aPlaneBorders[2] || fFarthestV > aPlaneBorders[3])
		bMinimize = true;

	if(bMinimize)  // scale_min < 1
	{
		if(fUScale < 1.0f && fVScale < 1.0f)      // if both are minimizing
			fScale = min(fUScale, fVScale);
		else if(fUScale < 1.0f)                   // if only u_scale is minimizing 
			fScale = fUScale;
		else									  // if only v_scale is minimizing
			fScale = fVScale;
	}
	else          // scale_max > 1
	{
		if(fUScale > 1.0f && fVScale > 1.0f)      // if both are maximizing
			fScale = max(fUScale, fVScale);
		else if(fUScale > 1.0f)					  // if only u_scale is maximizing
			fScale = fUScale;
		else									  // if only v_scale is maximizing
			fScale = fVScale;
	}

	// scale the intersection points (relatively according to the middle point)
	for(unsigned int i=0; i<nNumOfIntersects; ++i)
	{
		v3Dist = m_vExpIntersects[i] - v3RenderPlaneMiddle;
		v3Dist = v3Dist*fScale;
		m_vExpIntersects[i] = v3Dist + v3RenderPlaneMiddle;
	}
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::DrawOpaque()
{
	if(!GetVisible())
		return ;

	VflObliqueSlicerProperties *pProps = dynamic_cast<VflObliqueSlicerProperties*>(GetProperties());

	double dCurrentVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	int iCurrentLevel;
	
	if(!m_bStaticMode)
		iCurrentLevel = m_pData->GetTimeMapper()->GetLevelIndex(dCurrentVisTime);
	else
		iCurrentLevel = m_iStaticLevelIdx;

	if(iCurrentLevel < 0 || iCurrentLevel >= m_pData->GetNumberOfLevels())
		return;

	VistaVector3D v3ViewDir(GetRenderNode()->GetLocalViewDirection());

	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//m_pTransferFunction->Bind(TEX_UNIT(TRANSFERFUNCTION_TEXTURE_UNIT));
	m_pLookupTexture->GetLookupTexture()->Bind(GL_TEXTURE0 + s_iTransferFunctionTextureUnit);
	m_vecUnsteadyDataTextures[iCurrentLevel]->Bind(GL_TEXTURE0 + s_iDataTextureUnit);
	if (m_pNoiseTexture != NULL)
		m_pNoiseTexture->Bind(GL_TEXTURE0 + s_iNoiseTextureUnit);
	//m_pTargetData->GetPusher()->GetTexture()->Bind(GL_TEXTURE0 + s_iDataTextureUnit);  // TODO: use texture pusher!

	//glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_CULL_FACE);
	// glDisable(GL_DEPTH_TEST); // TODO: Might not be necessary after all.

	m_pShader->Bind();

	if(!m_bDrawToExpRenderPlane)
	{
		glBegin(GL_POLYGON);
		for(unsigned int i=0; i<m_vIntersects.size(); ++i) {
			glTexCoord3f(m_vTexCoords[i][0], m_vTexCoords[i][1], m_vTexCoords[i][2]);
			glVertex3f(m_vIntersects[i][0], m_vIntersects[i][1], m_vIntersects[i][2]);
		}
		glEnd();
	}
	else
	{
		glBegin(GL_POLYGON);
		for(unsigned int i=0; i<m_vIntersects.size(); ++i) {
			glTexCoord3f(m_vTexCoords[i][0], m_vTexCoords[i][1], m_vTexCoords[i][2]);
			glVertex3f(m_vExpIntersects[i][0], m_vExpIntersects[i][1], m_vExpIntersects[i][2]);
		}
		glEnd();
	}

	m_pShader->Release();	

	if(pProps->GetShowBorder())
	{
		glLineWidth(pProps->GetBorderWidth());
		float fR, fG, fB;
		pProps->GetBorderColor(fR, fG, fB);
		glColor3f((GLfloat)fR, (GLfloat)fG, (GLfloat)fB);
		if(!m_bDrawToExpRenderPlane)
		{
			glBegin(GL_LINE_LOOP);
			for(unsigned int i=0; i<m_vIntersects.size(); ++i)
				glVertex3f(m_vIntersects[i][0], m_vIntersects[i][1], m_vIntersects[i][2]);
			glEnd();
		}
		else
		{
			glBegin(GL_LINE_LOOP);
			for(unsigned int i=0; i<m_vIntersects.size(); ++i)
				glVertex3f(m_vExpIntersects[i][0], m_vExpIntersects[i][1], m_vExpIntersects[i][2]);
			glEnd();

		}
	}

	glPopAttrib();

	//m_vecUnsteadyDataTextures[iCurrentLevel]->Disable(TEX_UNIT(DATA_TEXTURE_UNIT));
	//m_pLookupTexture->GetLookupTexture()->Disable(TEX_UNIT(TRANSFERFUNCTION_TEXTURE_UNIT));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBoundsToData                                             */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetBoundsToData()
{
	if(!m_pData)
		return;
	
	m_fMinBound[0] = m_fMinBound[1] = m_fMinBound[2] = std::numeric_limits<float>::max();
	m_fMaxBound[0] = m_fMaxBound[1] = m_fMaxBound[2] = -std::numeric_limits<float>::max();

	for(int i=0; i<m_pData->GetNumberOfLevels(); ++i)
	{
		if( !m_pData->GetTypedLevelDataByLevelIndex(i) ||
			!m_pData->GetTypedLevelDataByLevelIndex(i)->GetData())
			continue;

		VistaVector3D v3Min, v3Max;
		m_pData->GetTypedLevelDataByLevelIndex(i)->GetData()->GetBounds(v3Min, v3Max);
		m_fMinBound[0] = std::min<float>(v3Min[0], m_fMinBound[0]);
		m_fMinBound[1] = std::min<float>(v3Min[1], m_fMinBound[1]);
		m_fMinBound[2] = std::min<float>(v3Min[2], m_fMinBound[2]);
		m_fMaxBound[0] = std::max<float>(v3Max[0], m_fMaxBound[0]);
		m_fMaxBound[1] = std::max<float>(v3Max[1], m_fMaxBound[1]);
		m_fMaxBound[2] = std::max<float>(v3Max[2], m_fMaxBound[2]);
	}

	float fXScale = m_fMaxBound[0] - m_fMinBound[0],
		  fYScale = m_fMaxBound[1] - m_fMinBound[1],
		  fZScale = m_fMaxBound[2] - m_fMinBound[2];

	m_oScaleToRender.SetToIdentity();
	m_oScaleToRender.SetToScaleMatrix(fXScale, fYScale, fZScale);
	
	m_oScaleToTexture.SetToIdentity();
	m_oScaleToTexture.SetToScaleMatrix(1.0f / fXScale, 1.0f / fYScale, 1.0f / fZScale);

	m_v3dBoundsCOG = VistaVector3D(0.5f*(m_fMinBound[0]+m_fMaxBound[0]),
								   0.5f*(m_fMinBound[1]+m_fMaxBound[1]),
								   0.5f*(m_fMinBound[2]+m_fMaxBound[2]));

	VistaVector3D v3dCorners[8];

	v3dCorners[0][0] = m_fMinBound[0]; v3dCorners[0][1] = m_fMinBound[1]; v3dCorners[0][2] = m_fMinBound[2]; // lower, front, left
	v3dCorners[1][0] = m_fMaxBound[0]; v3dCorners[1][1] = m_fMinBound[1]; v3dCorners[1][2] = m_fMinBound[2]; // lower, front, right
	v3dCorners[2][0] = m_fMinBound[0]; v3dCorners[2][1] = m_fMinBound[1]; v3dCorners[2][2] = m_fMaxBound[2]; // lower, back, left
	v3dCorners[3][0] = m_fMaxBound[0]; v3dCorners[3][1] = m_fMinBound[1]; v3dCorners[3][2] = m_fMaxBound[2]; // lower, back, right
	
	v3dCorners[4][0] = m_fMinBound[0]; v3dCorners[4][1] = m_fMaxBound[1]; v3dCorners[4][2] = m_fMinBound[2]; // upper, front, left
	v3dCorners[5][0] = m_fMaxBound[0]; v3dCorners[5][1] = m_fMaxBound[1]; v3dCorners[5][2] = m_fMinBound[2]; // upper, front, right
	v3dCorners[6][0] = m_fMinBound[0]; v3dCorners[6][1] = m_fMaxBound[1]; v3dCorners[6][2] = m_fMaxBound[2]; // upper, back, left
	v3dCorners[7][0] = m_fMaxBound[0]; v3dCorners[7][1] = m_fMaxBound[1]; v3dCorners[7][2] = m_fMaxBound[2]; // upper, back, right	

	m_v3dOrigins[0] = v3dCorners[0];
	m_v3dOrigins[1] = v3dCorners[1];
	m_v3dOrigins[2] = v3dCorners[2];
	m_v3dOrigins[3] = v3dCorners[3];

	m_v3dOrigins[4] = v3dCorners[0];
	m_v3dOrigins[5] = v3dCorners[2];
	m_v3dOrigins[6] = v3dCorners[4];
	m_v3dOrigins[7] = v3dCorners[6];

	m_v3dOrigins[8] = v3dCorners[0];
	m_v3dOrigins[9] = v3dCorners[1];
	m_v3dOrigins[10] = v3dCorners[4];
	m_v3dOrigins[11] = v3dCorners[5];	

	m_v3dDirs[0] = v3dCorners[4] - v3dCorners[0]; // 0 -> 4
	m_v3dDirs[1] = v3dCorners[5] - v3dCorners[1]; // 1 -> 5
	m_v3dDirs[2] = v3dCorners[6] - v3dCorners[2]; // 2 -> 6
	m_v3dDirs[3] = v3dCorners[7] - v3dCorners[3]; // 3 -> 7
	
	m_v3dDirs[4] = v3dCorners[1] - v3dCorners[0]; // 0 -> 1
	m_v3dDirs[5] = v3dCorners[3] - v3dCorners[2]; // 2 -> 3
	m_v3dDirs[6] = v3dCorners[5] - v3dCorners[4]; // 4 -> 5
	m_v3dDirs[7] = v3dCorners[7] - v3dCorners[6]; // 6 -> 7

	m_v3dDirs[8] = v3dCorners[2] - v3dCorners[0]; // 0 -> 2
	m_v3dDirs[9] = v3dCorners[3] - v3dCorners[1]; // 1 -> 3
	m_v3dDirs[10] = v3dCorners[6] - v3dCorners[4]; // 4 -> 6
	m_v3dDirs[11] = v3dCorners[7] - v3dCorners[5]; // 5 -> 7

	// Save length of each edge and normalize the direction vectors
	for(int i=0; i<12; ++i) {
		m_fLengths[i] = m_v3dDirs[i].GetLength();
		m_v3dDirs[i].Normalize();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IntersectionTest                                            */
/*                                                                            */
/*============================================================================*/

bool VflObliqueSlicer::IntersectionTest(VistaVector3D v3dOrigin, VistaVector3D v3dDir, float &fI)
{
	v3dDir.Normalize();

	// Retrieve the plane's center
	VistaVector3D v3dPO;
	m_oObliqueSlice.GetCenter(VflSlice::PT_RENDER_PLANE, v3dPO);

	// Retrieve the plane's normal
	VistaVector3D v3dPN;
	m_oObliqueSlice.GetNormal(VflSlice::PT_RENDER_PLANE, v3dPN);
	
	// Calculate the distance of the global origin from the plane
	v3dPN.Normalize();
	float fPD = v3dPO.Dot(v3dPN);

	// Calculate the intersection
	float fDenom = v3dPN.Dot(v3dDir);

	if(fDenom == 0)
		return false;

	fI = (-v3dPN.Dot(v3dOrigin) + fPD) / fDenom;

	return true;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetCenter(const VistaVector3D &v3dPos)
{
	m_fCenter[0] = std::min<float>(
		std::max<float>(v3dPos[0], m_fMinBound[0]), m_fMaxBound[0]);
	m_fCenter[1] = std::min<float>(
		std::max<float>(v3dPos[1], m_fMinBound[1]), m_fMaxBound[1]);
	m_fCenter[2] = std::min<float>(
		std::max<float>(v3dPos[2], m_fMinBound[2]), m_fMaxBound[2]);

	VistaVector3D v3dCenter (m_fCenter[0], m_fCenter[1], m_fCenter[2]);
/*
	m_fCenter[0] = v3dPos[0];
	m_fCenter[1] = v3dPos[1];
	m_fCenter[2] = v3dPos[2];
	VistaVector3D v3dCenter = v3dPos; */

	m_oObliqueSlice.SetCenter(VflSlice::PT_RENDER_PLANE, v3dCenter);
	
	this->UpdatePlaneParameters();
}

void VflObliqueSlicer::SetCenter(const float fCenter[3])
{
	this->SetCenter(VistaVector3D(fCenter[0], fCenter[1], fCenter[2]));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter                                                   */
/*                                                                            */
/*============================================================================*/
VistaVector3D VflObliqueSlicer::GetCenter()
{
	VistaVector3D v3dCenter;
	m_oObliqueSlice.GetCenter(VflSlice::PT_RENDER_PLANE, v3dCenter);

	return v3dCenter;
}

void VflObliqueSlicer::GetCenter(float (&fCenter)[3]) 
{
	this->GetCenter().GetValues(fCenter);
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetNormal                                                   */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetNormal(const VistaVector3D &v3dNormal)
{
	m_oObliqueSlice.SetNormal(VflSlice::PT_RENDER_PLANE, v3dNormal);
	
	this->UpdatePlaneParameters();
}

void VflObliqueSlicer::SetNormal(const float fNormal[3]) 
{
	this->SetNormal(VistaVector3D(fNormal[0], fNormal[1], fNormal[2]));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNormal                                                   */
/*                                                                            */
/*============================================================================*/
VistaVector3D VflObliqueSlicer::GetNormal() 
{
	VistaVector3D v3dNormal;
	m_oObliqueSlice.GetNormal(VflSlice::PT_RENDER_PLANE, v3dNormal);

	return v3dNormal;
}

void VflObliqueSlicer::GetNormal(float (&fNormal)[3])
{
	this->GetNormal().GetValues(fNormal);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDimensions                                               */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetDimensions(const float fW, const float fH)
{
	m_oObliqueSlice.SetDimensions(VflSlice::PT_RENDER_PLANE, fW, fH);

	this->UpdatePlaneParameters();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDimensions                                               */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::GetDimensions(float &fW, float &fH)
{
	m_oObliqueSlice.GetDimensions(VflSlice::PT_TEXTURE_PLANE, fW, fH);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRotation                                                 */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetRotation(const float fR)
{
	m_oObliqueSlice.SetRotation(VflSlice::PT_RENDER_PLANE, fR);
	
	this->UpdatePlaneParameters();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRotation                                                 */
/*                                                                            */
/*============================================================================*/
float VflObliqueSlicer::GetRotation()
{
	float fRotation;
	m_oObliqueSlice.GetRotation(VflSlice::PT_RENDER_PLANE, fRotation);

	return fRotation;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPoints                                                   */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetPoints(const VistaVector3D v3dPoints[4])
{
	m_oObliqueSlice.SetPoints(VflSlice::PT_RENDER_PLANE, v3dPoints[0], 
		v3dPoints[1], v3dPoints[2], v3dPoints[3]);

	this->UpdatePlaneParameters();
}

void VflObliqueSlicer::SetPoints(const VistaVector3D v3dPoint1, 
	const VistaVector3D v3dPoint2, const VistaVector3D v3dPoint3, 
	const VistaVector3D v3dPoint4)
{
	m_oObliqueSlice.SetPoints(VflSlice::PT_RENDER_PLANE, v3dPoint1,
		v3dPoint2, v3dPoint3, v3dPoint4);

	this->UpdatePlaneParameters();
}

void VflObliqueSlicer::SetPoints(const float fPoints[12])
{
	m_oObliqueSlice.SetPoints(VflSlice::PT_RENDER_PLANE,
		VistaVector3D(fPoints[0], fPoints[1], fPoints[2]),
		VistaVector3D(fPoints[3], fPoints[4], fPoints[5]),
		VistaVector3D(fPoints[6], fPoints[7], fPoints[8]),
		VistaVector3D(fPoints[9], fPoints[10], fPoints[11]));

	this->UpdatePlaneParameters();
}

void VflObliqueSlicer::SetPoints(const float fPoint1[], const float fPoint2[],
								  const float fPoint3[], const float fPoint4[])
{
	m_oObliqueSlice.SetPoints(VflSlice::PT_RENDER_PLANE,
		VistaVector3D(fPoint1[0], fPoint1[1], fPoint1[2]),
		VistaVector3D(fPoint2[0], fPoint2[1], fPoint2[2]),
		VistaVector3D(fPoint3[0], fPoint3[1], fPoint3[2]),
		VistaVector3D(fPoint4[0], fPoint4[1], fPoint4[2]));

	this->UpdatePlaneParameters();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPoints                                                   */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::GetPoints(VistaVector3D (&v3dPoints)[4])
{
	m_oObliqueSlice.GetPoints(VflSlice::PT_RENDER_PLANE, v3dPoints);
}

void VflObliqueSlicer::GetPoints(VistaVector3D v3dPoint1, VistaVector3D v3dPoint2, 
								  VistaVector3D v3dPoint3, VistaVector3D v3dPoint4)
{
	m_oObliqueSlice.GetPoint(VflSlice::PT_RENDER_PLANE, 1, v3dPoint1);
	m_oObliqueSlice.GetPoint(VflSlice::PT_RENDER_PLANE, 2, v3dPoint2);
	m_oObliqueSlice.GetPoint(VflSlice::PT_RENDER_PLANE, 3, v3dPoint3);
	m_oObliqueSlice.GetPoint(VflSlice::PT_RENDER_PLANE, 4, v3dPoint4);
}

void VflObliqueSlicer::GetPoints(float (&fPoints)[12])
{
	VistaVector3D vec1, vec2, vec3, vec4;
	this->GetPoints(vec1, vec2, vec3, vec4);

	fPoints[0] = vec1[0]; fPoints[1] = vec1[1]; fPoints[2] = vec1[2];
	fPoints[3] = vec2[0]; fPoints[4] = vec2[1]; fPoints[5] = vec2[2];
	fPoints[6] = vec3[0]; fPoints[7] = vec3[1]; fPoints[8] = vec3[2];
	fPoints[9] = vec4[0]; fPoints[10] = vec4[1]; fPoints[11] = vec4[2];	
}

void VflObliqueSlicer::GetPoints(float (&fPoint1)[3], float (&fPoint2)[3],
								  float (&fPoint3)[3], float (&fPoint4)[3])
{
	VistaVector3D vec1, vec2, vec3, vec4;
	this->GetPoints(vec1, vec2, vec3, vec4);

	vec1.GetValues(fPoint1);
	vec2.GetValues(fPoint2);
	vec3.GetValues(fPoint3);
	vec4.GetValues(fPoint4);
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::ObserverUpdate(IVistaObserveable *pObserveable, 
										int msg, int ticket)
{
	/*switch(msg){
		default :
			break;
	}*/

	IVflVisObject::ObserverUpdate(pObserveable, msg, ticket);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflObliqueSlicer::GetType() const
{
	return string("VflTriplanarSlicer");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflObliqueSlicer::GetRegistrationMode() const
{
	return OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  SetDrawToExpPlane                                            */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetDrawToExpRenderPlane(bool bDraw)
{
	m_bDrawToExpRenderPlane = bDraw;
		
	if(bDraw && !m_pExpRenderPlane)
		vstr::warnp() << "[VflObliqueSlicer] draw to exp plane is requested, "
					  << "but no target plane is defined" << endl;

	//return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetDrawToExpPlane                                            */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::GetDrawToExpRenderPlane() const
{
	return m_bDrawToExpRenderPlane;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  SetStaticMode                                                */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetStaticMode(bool b)
{
	m_bStaticMode = b;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetStaticMode                                                */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::GetStaticMode() const
{
	return m_bStaticMode;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  SetStaticLevelIdx                                            */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetStaticLevelIdx(int i)
{
	m_iStaticLevelIdx = i;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetStaticLevelIdx                                            */
/*                                                                            */
/*============================================================================*/
int VflObliqueSlicer::GetStaticLevelIdx() const
{
	return m_iStaticLevelIdx;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  SetExpRenderPlane                                            */
/*                                                                            */
/*============================================================================*/
void VflObliqueSlicer::SetExpRenderPlane(
						VistaVector3D &v3Normal, VistaVector3D &v3Center,
						float fWidth, float fHeight, float fRot)
{
	if(!m_pExpRenderPlane)
	{
		m_pExpRenderPlane = new VflSlice(v3Normal, v3Center, fWidth, fHeight, fRot,
									v3Normal, v3Center, fWidth, fHeight, fRot);
	}
	else
	{
		m_pExpRenderPlane->SetNormal(VflSlice::PT_RENDER_PLANE, v3Normal);
		m_pExpRenderPlane->SetCenter(VflSlice::PT_RENDER_PLANE, v3Center);
		m_pExpRenderPlane->SetDimensions(VflSlice::PT_RENDER_PLANE, fWidth, fHeight);
		m_pExpRenderPlane->SetRotation(VflSlice::PT_RENDER_PLANE, fRot);
	}

	this->SetDrawToExpRenderPlane(true);
	this->TransformIntersections();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetExpRenderPlane                                            */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::GetExpRenderPlane(
						VistaVector3D &v3Normal, VistaVector3D &v3Center,
						float &fWidth, float &fHeight, float &fRot)
{
	if(!m_pExpRenderPlane)
		return false;

	m_pExpRenderPlane->GetNormal(VflSlice::PT_RENDER_PLANE, v3Normal);
	m_pExpRenderPlane->GetCenter(VflSlice::PT_RENDER_PLANE, v3Center);
	m_pExpRenderPlane->GetDimensions(VflSlice::PT_RENDER_PLANE, fWidth, fHeight);
	m_pExpRenderPlane->GetRotation(VflSlice::PT_RENDER_PLANE, fRot);
		
	return true;
}

void VflObliqueSlicer::SetExpRenderPlane( VflSlice *pRenderPlane )
{
	m_pExpRenderPlane = pRenderPlane;
}

VflSlice *VflObliqueSlicer::GetExpRenderPlane() const
{
	return m_pExpRenderPlane;
}

/*============================================================================*/
/*======================== Reflectionable Properties =========================*/
/*============================================================================*/
static const string SsReflectionType("VflObliqueSlicer");

IVflVisObject::VflVisObjProperties  *VflObliqueSlicer::CreateProperties() const
{
	return new VflObliqueSlicerProperties;
}

void VflObliqueSlicer::SetNoiseTexture(VistaTexture* pNoiseTexture)
{
	m_pNoiseTexture = pNoiseTexture;
}

VistaTexture* VflObliqueSlicer::GetNoiseTexture()
{
	return m_pNoiseTexture;
}



int VflObliqueSlicer::VflObliqueSlicerProperties::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	int nSize = IVflVisObject::VflVisObjProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize;
}

static IVistaPropertyGetFunctor *getFunctors[] = 
{
	new TVistaPropertyGet<bool, VflObliqueSlicer::VflObliqueSlicerProperties>
		("SHOWBORDER", SsReflectionType, 
		&VflObliqueSlicer::VflObliqueSlicerProperties::GetShowBorder),
	new TVistaPropertyGet<float, VflObliqueSlicer::VflObliqueSlicerProperties>
		("BORDERWIDTH", SsReflectionType,
		&VflObliqueSlicer::VflObliqueSlicerProperties::GetBorderWidth),
	new TVistaProperty3RefGet<float, VflObliqueSlicer::VflObliqueSlicerProperties>
		("BORDERCOLOR", SsReflectionType,
		&VflObliqueSlicer::VflObliqueSlicerProperties::GetBorderColor),
	NULL
};

static IVistaPropertySetFunctor *setFunctors[] = 
{
	new TVistaPropertySet<bool, bool, VflObliqueSlicer::VflObliqueSlicerProperties>
		("SHOWBORDER", SsReflectionType,
		&VflObliqueSlicer::VflObliqueSlicerProperties::SetShowBorder),
	new TVistaPropertySet<float, float, VflObliqueSlicer:: VflObliqueSlicerProperties>
		("BORDERWIDTH", SsReflectionType,
		&VflObliqueSlicer::VflObliqueSlicerProperties::SetBorderWidth),
	new TVistaProperty3ValSet<float, VflObliqueSlicer:: VflObliqueSlicerProperties>
		("BORDERCOLOR", SsReflectionType,
		&VflObliqueSlicer::VflObliqueSlicerProperties::SetBorderColor),
	NULL 
};

VflObliqueSlicer::VflObliqueSlicerProperties::VflObliqueSlicerProperties()
: IVflVisObject::VflVisObjProperties(),
m_bShowBorder(true), m_fBorderWidth(1.0f)
{
	m_aBorderColor[0] = 1.0f;
	m_aBorderColor[1] = 1.0f;
	m_aBorderColor[2] = 1.0f;	
}

VflObliqueSlicer::VflObliqueSlicerProperties::~VflObliqueSlicerProperties()
{
}

string VflObliqueSlicer::VflObliqueSlicerProperties::GetReflectionableType() const
{
	return SsReflectionType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetShowBorder                                               */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::VflObliqueSlicerProperties::SetShowBorder(bool bShowBorder)
{
	m_bShowBorder = bShowBorder;
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetShowBorder                                               */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::VflObliqueSlicerProperties::GetShowBorder() const
{
	return m_bShowBorder;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBorderWidth                                              */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::VflObliqueSlicerProperties::SetBorderWidth(float fWidth)
{
	m_fBorderWidth = fWidth;
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBorderWidth                                              */
/*                                                                            */
/*============================================================================*/
float VflObliqueSlicer::VflObliqueSlicerProperties::GetBorderWidth() const
{
	return m_fBorderWidth;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBorderColor                                              */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::VflObliqueSlicerProperties::
SetBorderColor(float fRed, float fGreen, float fBlue)
{
	m_aBorderColor[0] = fRed;
	m_aBorderColor[1] = fGreen;
	m_aBorderColor[2] = fBlue;
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBorderColor                                              */
/*                                                                            */
/*============================================================================*/
bool VflObliqueSlicer::VflObliqueSlicerProperties::
GetBorderColor(float &fRed, float &fGreen, float &fBlue) const
{
	fRed = m_aBorderColor[0];
	fGreen = m_aBorderColor[1];
	fBlue = m_aBorderColor[2];
	return true;
}



/*============================================================================*/
/* END OF FILE       VflObliqueSlicer.cpp                                     */
/*============================================================================*/
