/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VflVolumeRendererMagicLens.h"

#include "VflVolumeRendererMultiLens.h"
#include "VflVisVolume.h"
#include "VflVisVolumeMultiLensProperties.h"
#include <VistaOGLExt/VistaFramebufferObj.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include "../VflVisTiming.h"
#include <VistaVisExt/Data/VveCartesianGrid.h>
#include "../../Data/VflUnsteadyCartesianGridPusher.h"
#include "../VflVtkLookupTable.h"
#include "../VflLookupTexture.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaShaderRegistry.h>
#include "VflVolumeRendererMulti.h"

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strVflVolumeRendererMultiLensReflType("VflVolumeRendererMultiLens");
static std::string s_strVflVolumeRendererShaderName("VflVolumeRendererRaycast_frag.glsl");

static IVistaPropertyGetFunctor *s_aVflVolumeRendererMultiLensGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aVflVolumeRendererMultiLensSetFunctors[] =
{
	NULL
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VflVolumeRendererMultiLens::VflVolumeRendererMultiLens(	VflVisVolume *pParent,
														const int iWidth, 
														const int iHeight, 
														const float fNearPlane)
	:	VflVolumeRendererMulti( pParent, iWidth, iHeight, fNearPlane )
	,	m_bValid( false )
	,	m_bValidData( false )
	,	m_pMultiLensShader( new VistaGLSLShader() )
{
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();

	string strFragmetShader = rShaderReg.RetrieveShader( s_strVflVolumeRendererShaderName );
	if( strFragmetShader.empty() )
	{
		vstr::errp() << "VflVolumeRendererMultiLens: can't find shader file \"" 
			<< s_strVflVolumeRendererShaderName << "\"" << endl;
		return;
	}

	m_pMultiLensShader->InitFragmentShaderFromString(
			"#define _MagicLens\n#define _Multi\n" + strFragmetShader);
	m_pMultiLensShader->Link();

	if(m_pRaycastShader)
		delete m_pRaycastShader;
	m_pRaycastShader = m_pMultiLensShader;
  
	m_bValid = true;
}

VflVolumeRendererMultiLens::~VflVolumeRendererMultiLens() 
{
  
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMultiLens::Update()
{
	VflVolumeRendererMulti::Update();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMultiLens::DrawOpaque()
{
	VflVolumeRendererMulti::DrawOpaque();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMultiLens::DrawTransparent()
{
	VflVolumeRendererMulti::DrawTransparent();
}

void VflVolumeRendererMultiLens::UpdateBufferCore(	const int iVolId,
													const int iTfId, 
													const float aRange[2], 
													const float aRenderParams[2]) 
{
	VflVolumeRendererMulti::UpdateBufferCore(iVolId, iTfId, aRange, aRenderParams);

	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();

	// fall back to standard tf
	int iTexMagicLensLutId = iTfId;
	int iTexTFRed   = iTfId;
	int iTexTFGreen = iTfId;
	int iTexTFBlue  = iTfId;
	if( VflVisVolumeMultiLensPropertiesBase* pMultiLensProperties = 
			dynamic_cast<VflVisVolumeMultiLensPropertiesBase*>(pProperties) )
	{
		iTexTFRed    = pMultiLensProperties->GetMultiLensLookupTexture(0)->GetLookupTexture()->GetId();
		iTexTFGreen  = pMultiLensProperties->GetMultiLensLookupTexture(1)->GetLookupTexture()->GetId();
		iTexTFBlue   = pMultiLensProperties->GetMultiLensLookupTexture(2)->GetLookupTexture()->GetId();
	}
	glActiveTexture(GL_TEXTURE8);
	glBindTexture(GL_TEXTURE_1D,iTexTFRed );
	glActiveTexture(GL_TEXTURE9);
	glBindTexture(GL_TEXTURE_1D,iTexTFGreen );
	glActiveTexture(GL_TEXTURE10);
	glBindTexture(GL_TEXTURE_1D,iTexTFBlue);

	int iLoc;
	iLoc = m_pRaycastShader->GetUniformLocation("g_texTFRedLens");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 8);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texTFGreenLens");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 9);
	iLoc = m_pRaycastShader->GetUniformLocation("g_texTFBlueLens");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 10);

	float aMagicLensParams[4] = {0.0f,0.0f,0.0f,0.0f}; //< disable by default
	if(VflVisVolumeMagicLensPropertiesBase* pMagicLensProperties = 
			dynamic_cast<VflVisVolumeMagicLensPropertiesBase*>(pProperties))
	{
		iTexMagicLensLutId = pMagicLensProperties->GetMagicLensLookupTexture()
				->GetLookupTexture()->GetId();
		const VistaVector3D v3Pos = pMagicLensProperties->GetMagicLensCenter();
		const float fRadius = pMagicLensProperties->GetMagicLensRadius();
		aMagicLensParams[0] = v3Pos[0];
		aMagicLensParams[1] = v3Pos[1];
		aMagicLensParams[2] = v3Pos[2];
		aMagicLensParams[3] = fRadius;
	}
	glActiveTexture(GL_TEXTURE11);
	glBindTexture(GL_TEXTURE_1D,iTexMagicLensLutId);

	iLoc = m_pRaycastShader->GetUniformLocation("g_texMagicLens");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 11);
	iLoc = m_pRaycastShader->GetUniformLocation("g_v4MagicLensParams");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 4, 1, aMagicLensParams);

	const VistaVector3D v3Scale = m_v3BoundsMax - m_v3BoundsMin;
	float aProxyScaling[3] = {v3Scale[0],v3Scale[1],v3Scale[2]};

	iLoc = m_pRaycastShader->GetUniformLocation("g_v3ProxyScaling");
	if(iLoc >= 0) m_pRaycastShader->SetUniform(iLoc, 3, 1, aProxyScaling);
}  

void VflVolumeRendererMultiLens::DrawBuffer(const unsigned int iWhich)
{
	VflVolumeRendererMulti::DrawBuffer(iWhich);
}

      

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void VflVolumeRendererMultiLens::Draw2D()
{
	VflVisVolume::VflVisVolumeProperties *pProperties = m_pParent->GetProperties();
	if (!pProperties->GetVisible())
		return;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsValid                                                     */
/*                                                                            */
/*============================================================================*/
bool VflVolumeRendererMultiLens::IsValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflVolumeRendererMultiLens::GetReflectionableType() const
{
	return s_strVflVolumeRendererMultiLensReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflVolumeRendererMultiLens::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strVflVolumeRendererMultiLensReflType);
	return i+1;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
