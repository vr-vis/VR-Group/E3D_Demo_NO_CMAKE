/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include <GL/glew.h>

#include <VistaBase/VistaStreamUtils.h>

#include <VistaFlowLib/Visualization/Volume/VflTriplanarSlicer.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/VflLookupTexture.h>
#include <VistaFlowLib/Visualization/VflVtkLookupTable.h>

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

#include <vtkStructuredPoints.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>

#include <VistaMath/VistaBoundingBox.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static const std::string SsReflectionType("VflTriplanarSlicerProperties");
static const int TRANSFERFUNCTION_TEXTURE_UNIT = 0;
static const int DATA_TEXTURE_UNIT = 1;
inline GLenum TEX_UNIT(int TEXTURE_UNIT)
{
	return GL_TEXTURE0 + TEXTURE_UNIT;
}

const std::string TRIPLANARSLICER_FRAGMENT_SHADER = "VflTriplanarSlicer_frag.glsl";
const std::string TRIPLANARSLICER_VERTEX_SHADER   = "VflTriplanarSlicer_vert.glsl";
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflTriplanarSlicer::VflTriplanarSlicer()
	:	m_pShader(NULL)
	,	m_pData(NULL)
	,	m_bDisplayMode(false)
	,	m_pLookupTexture(NULL)
	,	m_bPadData(false)
{ }

VflTriplanarSlicer::~VflTriplanarSlicer()
{
	const size_t nSize = m_vecUnsteadyDataTextures.size();
	for(size_t i=0; i<nSize; ++i)
		delete m_vecUnsteadyDataTextures[i];

	delete m_pLookupTexture;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflTriplanarSlicer::Init()
{
	if(IVflVisObject::Init())
	{
		if(!InitShaders())
			return false;
		if(!InitTextures())
			return false;

		this->SetBoundsToData();

		m_vBBPlaneNormals.push_back(VistaVector3D( 1, 0, 0));// right
		m_vBBPlaneNormals.push_back(VistaVector3D(-1, 0, 0));// left
		m_vBBPlaneNormals.push_back(VistaVector3D( 0, 1, 0));// top
		m_vBBPlaneNormals.push_back(VistaVector3D( 0,-1, 0));// bottom
		m_vBBPlaneNormals.push_back(VistaVector3D( 0, 0, 1));// front
		m_vBBPlaneNormals.push_back(VistaVector3D( 0, 0,-1));// back

		return true;
	}
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   InitShaders                                                 */
/*                                                                            */
/*============================================================================*/
bool VflTriplanarSlicer::InitShaders()
{
	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();

	std::string strVert = rShaderReg.RetrieveShader(TRIPLANARSLICER_VERTEX_SHADER);
	std::string strFrag = rShaderReg.RetrieveShader(TRIPLANARSLICER_FRAGMENT_SHADER);

	m_pShader = new VistaGLSLShader();
	m_pShader->InitVertexShaderFromString(strVert);
	m_pShader->InitFragmentShaderFromString(strFrag);

	if(!m_pShader->Link())
		return false;
	if(!m_pShader->IsReadyForUse())
		return false;

	m_pShader->Bind();
	int data_loc = m_pShader->GetUniformLocation("Data");
	int tf_loc = m_pShader->GetUniformLocation("transfer_function");
	m_pShader->SetUniform(data_loc,DATA_TEXTURE_UNIT);
	m_pShader->SetUniform(tf_loc,TRANSFERFUNCTION_TEXTURE_UNIT);
	m_pShader->Release();

	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   InitTextures                                                */
/*                                                                            */
/*============================================================================*/
bool VflTriplanarSlicer::InitTextures()
{
	int iLevels = m_pData->GetNumberOfLevels();
	m_vecUnsteadyDataTextures.resize(iLevels);

	vstr::debugi() << "[VflTriplanarSlicer] creating textures .." << endl;

	GLenum glerr;

	// create data textures 
	int iDataDims[3], iDims[3];
	double dRange[2];
	double dNorm_range;
	std::vector<float> vecData;	
	vtkStructuredPoints *pRawData;
	vtkDataArray *pArray;

	for(int i=0; i<iLevels; ++i)
	{
		if( !m_pData->GetTypedLevelDataByLevelIndex(i)->GetData() )
			continue;

		pRawData = m_pData->GetTypedLevelDataByLevelIndex(i)->GetData();
		pArray = pRawData->GetPointData()->GetScalars();
		if(!pArray)
		{
			m_vecUnsteadyDataTextures[i] = NULL;
			vstr::errp() << "[VflTriplanarSlicer::InitTextures] - "
						 << "Texture for Level <" << i 
						 << "> could not be created" << endl;
			continue;
		}
		pArray->GetRange(dRange);
		dNorm_range = 1.0 / (dRange[1]-dRange[0]);

		if(m_bPadData)
		{
			pRawData->GetDimensions(iDataDims);
			for(int i=0; i<3; ++i)
			{
				iDims[i] = 1;
				while(iDims[i] < iDataDims[i])
					iDims[i] *= 2;
			}
			vecData.resize(iDims[0] * iDims[1] * iDims[2], 0);

			//pad the data to a 2^n size...
			int iTexIdx, iDataIdx;
			for(int k=0; k<iDataDims[2]; ++k)
			{
				for(int j=0; j<iDataDims[1]; ++j)
				{
					for(int i=0; i<iDataDims[0]; ++i)
					{
						//note: the texture's bounds are given by iDims, not iDataDims
						//but we do have data only for idx within iDataDims.
						iTexIdx = i+iDims[0] * (j + iDims[1] * k);
						iDataIdx = i+iDataDims[0] * (j + iDataDims[1] * k);
						vecData[iTexIdx] = ( pArray->GetTuple1(iDataIdx) - dRange[0])* dNorm_range;
					}
				}
			}
		} 
		else// no padding -> direct scale and copy
		{
			pRawData->GetDimensions(iDims);
			vecData.resize(iDims[0] * iDims[1] * iDims[2]);
			for(int i=0; i<pArray->GetNumberOfTuples(); ++i)
				vecData[i] = ( pArray->GetTuple1(i) - dRange[0])* dNorm_range;
		}

		m_vecUnsteadyDataTextures[i] = new VistaTexture(GL_TEXTURE_3D);

		glTexParameteri(m_vecUnsteadyDataTextures[i]->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP);  
		glTexParameteri(m_vecUnsteadyDataTextures[i]->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP);  
		glTexParameteri(m_vecUnsteadyDataTextures[i]->GetTarget(), GL_TEXTURE_WRAP_R, GL_CLAMP);  
		glTexParameteri(m_vecUnsteadyDataTextures[i]->GetTarget(), GL_TEXTURE_MIN_FILTER, GL_LINEAR);  
		glTexParameteri(m_vecUnsteadyDataTextures[i]->GetTarget(), GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexImage3D(m_vecUnsteadyDataTextures[i]->GetTarget(), 0, GL_LUMINANCE, iDims[0], iDims[1], iDims[2], 0, GL_LUMINANCE, GL_FLOAT, &vecData[0]);

		glerr = glGetError();
		if(glerr != GL_NO_ERROR)
		{
			vstr::errp() << "  [VflTriplanarSlicer] Texture for Level <" 
						 << i << "> could not be created" << endl;
		}
	}

	m_pLookupTexture = new VflLookupTexture(new VflVtkLookupTable(), GL_LINEAR);
	IVflLookupTable *table = m_pLookupTexture->GetLookupTable();
	table->SetHueRange(0.6,0.3);
	table->SetAlphaRange(0.3,0.6);

	glerr = glGetError();
	if(glerr != GL_NO_ERROR){
		vstr::errp() << "  [VflTriplanarSlicer] transfer function could not be created" << endl;
	}

	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTable                                              */
/*                                                                            */
/*============================================================================*/
IVflLookupTable* VflTriplanarSlicer::GetLookupTable()
{
	return m_pLookupTexture->GetLookupTable();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPadDataToPowerOf2                                        */
/*                                                                            */
/*============================================================================*/

void VflTriplanarSlicer::SetPadDataToPowerOf2(bool b)
{
	m_bPadData = b;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPadDataToPowerOf2                                        */
/*                                                                            */
/*============================================================================*/

bool VflTriplanarSlicer::GetPadDataToPowerOf2() const
{
	return m_bPadData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetUnsteadyData                                             */
/*                                                                            */
/*============================================================================*/
bool VflTriplanarSlicer::SetUnsteadyData(VveUnsteadyData *pData)
{
	IVflVisObject::SetUnsteadyData(pData);

	SetData(dynamic_cast<VveUnsteadyVtkStructuredPoints*>(pData));
	return (m_pData != NULL);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetData                                                     */
/*                                                                            */
/*============================================================================*/
bool VflTriplanarSlicer::SetData(VveUnsteadyVtkStructuredPoints *pData)
{
	if(pData == NULL)
		return false;

	m_pData = pData;
	this->SetBoundsToData();
	this->InitTextures();

	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData                                                     */
/*                                                                            */
/*============================================================================*/
VveUnsteadyVtkStructuredPoints* VflTriplanarSlicer::GetData()
{
	return m_pData;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflVisObject::VflVisObjProperties*
VflTriplanarSlicer::CreateProperties() const
{
	return new VflTriplanarSlicerProperties;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdatePlaneParameters                                       */
/*                                                                            */
/*============================================================================*/
void VflTriplanarSlicer::UpdatePlaneParameters()
{
	float tex_x, tex_y, tex_z;
	tex_x = (m_fCenter[0]-m_fMinBound[0])/(m_fMaxBound[0]-m_fMinBound[0]);
	tex_y = (m_fCenter[1]-m_fMinBound[1])/(m_fMaxBound[1]-m_fMinBound[1]);
	tex_z = (m_fCenter[2]-m_fMinBound[2])/(m_fMaxBound[2]-m_fMinBound[2]);

	// clamp values to [0,1]
	tex_x = tex_x > 1.0f ? 1.0f : (tex_x < 0.0f ? 0.0 : tex_x);
	tex_y = tex_y > 1.0f ? 1.0f : (tex_y < 0.0f ? 0.0 : tex_y);
	tex_z = tex_z > 1.0f ? 1.0f : (tex_z < 0.0f ? 0.0 : tex_z);

	float _varying = 0.0f;

	// varying y component
	_varying = (1-tex_y)*m_fMinBound[1] + tex_y*m_fMaxBound[1];
	m_oXZPlane.SetPoints(VflSlice::PT_RENDER_PLANE, 
		VistaVector3D(m_fMinBound[0],_varying, m_fMinBound[2]),
		VistaVector3D(m_fMinBound[0],_varying, m_fMaxBound[2]),
		VistaVector3D(m_fMaxBound[0],_varying, m_fMaxBound[2]),
		VistaVector3D(m_fMaxBound[0],_varying, m_fMinBound[2]));

	m_oXZPlane.SetPoints(VflSlice::PT_TEXTURE_PLANE,
		VistaVector3D(0,tex_y,0),
		VistaVector3D(0,tex_y,1),
		VistaVector3D(1,tex_y,1),
		VistaVector3D(1,tex_y,0));
	
	m_oXZPlane.Activate();


	// varying z component
	_varying = (1-tex_z)*m_fMinBound[2] + tex_z*m_fMaxBound[2];
	m_oXYPlane.SetPoints(VflSlice::PT_RENDER_PLANE,
		VistaVector3D(m_fMaxBound[0], m_fMinBound[1],_varying),
		VistaVector3D(m_fMaxBound[0], m_fMaxBound[1],_varying),
		VistaVector3D(m_fMinBound[0], m_fMaxBound[1],_varying),
		VistaVector3D(m_fMinBound[0], m_fMinBound[1],_varying));

	m_oXYPlane.SetPoints(VflSlice::PT_TEXTURE_PLANE,
		VistaVector3D(1,0,tex_z),
		VistaVector3D(1,1,tex_z),
		VistaVector3D(0,1,tex_z),
		VistaVector3D(0,0,tex_z));

	m_oXYPlane.Activate();


	// varying x component
	_varying = (1-tex_x)*m_fMinBound[0] + tex_x*m_fMaxBound[0];
	m_oYZPlane.SetPoints(VflSlice::PT_RENDER_PLANE,
		VistaVector3D(_varying, m_fMinBound[1], m_fMinBound[2]),
		VistaVector3D(_varying, m_fMaxBound[1], m_fMinBound[2]),
		VistaVector3D(_varying, m_fMaxBound[1], m_fMaxBound[2]),
		VistaVector3D(_varying, m_fMinBound[1], m_fMaxBound[2]));

	m_oYZPlane.SetPoints(VflSlice::PT_TEXTURE_PLANE,
		VistaVector3D(tex_x,0,0),
		VistaVector3D(tex_x,1,0),
		VistaVector3D(tex_x,1,1),
		VistaVector3D(tex_x,0,1));

	m_oYZPlane.Activate();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflTriplanarSlicer::DrawTransparent()
{
	if(!GetVisible())
		return ;

	float fCurrentVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	int iCurrentLevel      = m_pData->GetTimeMapper()->GetLevelIndex(fCurrentVisTime);

	if(iCurrentLevel < 0 || iCurrentLevel >= m_pData->GetNumberOfLevels())
		return;

	VistaVector3D v3ViewDir(GetRenderNode()->GetLocalViewDirection());

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//m_pTransferFunction->Bind(TEX_UNIT(TRANSFERFUNCTION_TEXTURE_UNIT));
	m_pLookupTexture->GetLookupTexture()->Bind(TEX_UNIT(TRANSFERFUNCTION_TEXTURE_UNIT));
	m_vecUnsteadyDataTextures[iCurrentLevel]->Bind(TEX_UNIT(DATA_TEXTURE_UNIT));
	
	float fVarX, fVarY, fVarZ, perCentX, perCentY, perCentZ;
	int iDrawmode = 
		dynamic_cast<VflTriplanarSlicer::VflTriplanarSlicerProperties*>(
		GetProperties())->GetDrawMode();

	if(iDrawmode == DM_AT_BACKPLANE)
	{
		//determine which faces of the bounding box shall be drawn
		// x
		if(v3ViewDir.Dot(m_vBBPlaneNormals[0]) >= 0.0){ // right
			fVarX = m_fMaxBound[0];
			perCentX = 0.999;
		}
		else if(v3ViewDir.Dot(m_vBBPlaneNormals[1]) >= 0.0){ // left
			fVarX = m_fMinBound[0];
			perCentX = 0.999;
		}
		// y
		if(v3ViewDir.Dot(m_vBBPlaneNormals[2]) >= 0.0){ // top
			fVarY = m_fMaxBound[1];
			perCentY = 1.001;
		}
		else if(v3ViewDir.Dot(m_vBBPlaneNormals[3]) >= 0.0){ // bottom
			fVarY = m_fMinBound[1];
			perCentY = 0.999;
		}
		// z
		if(v3ViewDir.Dot(m_vBBPlaneNormals[4]) >= 0.0){ // front
			fVarZ = m_fMaxBound[2];
			perCentZ = 0.999;
		}
		else if(v3ViewDir.Dot(m_vBBPlaneNormals[5]) >= 0.0){ // back
			fVarZ = m_fMinBound[2];
			perCentZ = 1.001;
		}

		//set the planes to be drawn at the data set bounding box
		VistaVector3D v3PlanePts[4];
		m_oXYPlane.GetPoints(VflSlice::PT_RENDER_PLANE, v3PlanePts);
		for(int i=0; i<4; ++i)
		{
			v3PlanePts[i][2] = fVarZ;
		}
		m_oXYPlane.SetPoints(VflSlice::PT_RENDER_PLANE, v3PlanePts);

		m_oXZPlane.GetPoints(VflSlice::PT_RENDER_PLANE, v3PlanePts);
		for(int i=0; i<4; ++i)
		{
			v3PlanePts[i][1] = fVarY;
		}
		m_oXZPlane.SetPoints(VflSlice::PT_RENDER_PLANE, v3PlanePts);

		m_oYZPlane.GetPoints(VflSlice::PT_RENDER_PLANE, v3PlanePts);
		for(int i=0; i<4; ++i)
		{
			v3PlanePts[i][0] = fVarX;
		}
		m_oYZPlane.SetPoints(VflSlice::PT_RENDER_PLANE, v3PlanePts);

		//draw red cursor lines
		VistaVector3D center = VflTriplanarSlicer::GetCenter();
		glColor3f(1.0f,0.0f,0.0f);
		glDisable(GL_LIGHTING);
		
		//x-y plane
		float hor1XY [3] = {m_fMinBound[0],center[1],perCentZ*fVarZ};
		float hor2XY [3] = {m_fMaxBound[0],center[1],perCentZ*fVarZ};
			
		glBegin(GL_LINES);
			glVertex3fv(hor1XY);
			glVertex3fv(hor2XY);
		glEnd();
		
		float ver1XY [3] = {center[0],m_fMinBound[1],perCentZ*fVarZ};
		float ver2XY [3] = {center[0],m_fMaxBound[1],perCentZ*fVarZ};
			
		glBegin(GL_LINES);
			glVertex3fv(ver1XY);
			glVertex3fv(ver2XY);
		glEnd();

		
		//x-z plane
		float ver1XZ [3] = {m_fMinBound[0],perCentY*fVarY,center[2]};
		float ver2XZ [3] = {m_fMaxBound[0],perCentY*fVarY,center[2]};
			
		glBegin(GL_LINES);
			glVertex3fv(ver1XZ);
			glVertex3fv(ver2XZ);
		glEnd();
		
		float hor1XZ [3] = {center[0],perCentY*fVarY, m_fMinBound[2]};
		float hor2XZ [3] = {center[0],perCentY*fVarY, m_fMaxBound[2]};
			
		glBegin(GL_LINES);
			glVertex3fv(hor1XZ);
			glVertex3fv(hor2XZ);
		glEnd();

		
		//y-z plane
		float hor1YZ [3] = {perCentX*fVarX, m_fMinBound[1],center[2]};
		float hor2YZ [3] = {perCentX*fVarX, m_fMaxBound[1],center[2]};
			
		glBegin(GL_LINES);
			glVertex3fv(hor1YZ);
			glVertex3fv(hor2YZ);
		glEnd();
		
		float ver1YZ [3] = {perCentX*fVarX, center[1], m_fMinBound[2]};
		float ver2YZ [3] = {perCentX*fVarX, center[1], m_fMaxBound[2]};
			
		glBegin(GL_LINES);
			glVertex3fv(ver1YZ);
			glVertex3fv(ver2YZ);
		glEnd();
	}
	/*
	else if(iDrawmode == DM_AT_CENTER)
	{
		VistaVector3D v3dX, v3dY, v3dZ;

		m_oYZPlane.GetPoint(VflSlice::PT_RENDER_PLANE, 1, v3dX);
		m_oXZPlane.GetPoint(VflSlice::PT_RENDER_PLANE, 1, v3dY);
		m_oXYPlane.GetPoint(VflSlice::PT_RENDER_PLANE, 1, v3dZ);

		fVarX = v3dX[0];
		fVarY = v3dY[1];
		fVarZ = v3dZ[2];
	}
	*/

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_CULL_FACE);
	
	m_pShader->Bind();
	m_oXZPlane.Draw();
	m_oXYPlane.Draw();
	m_oYZPlane.Draw();
	m_pShader->Release();

	glPopAttrib();

	m_vecUnsteadyDataTextures[iCurrentLevel]->Disable(TEX_UNIT(DATA_TEXTURE_UNIT));
	m_pLookupTexture->GetLookupTexture()->Disable(TEX_UNIT(TRANSFERFUNCTION_TEXTURE_UNIT));

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBounds                                                   */
/*                                                                            */
/*============================================================================*/
void VflTriplanarSlicer::SetBounds(const VistaVector3D& v3minBound, 
									const VistaVector3D& v3maxBound)
{
	v3minBound.GetValues(m_fMinBound);
	v3maxBound.GetValues(m_fMaxBound);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VflTriplanarSlicer::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{

	minBounds[0]= m_fMinBound[0];
	minBounds[1]= m_fMinBound[1];
	minBounds[2]= m_fMinBound[2];
	maxBounds[0]= m_fMaxBound[0];
	maxBounds[1]= m_fMaxBound[1];
	maxBounds[2]= m_fMaxBound[2];

	return true;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBoundsToData                                             */
/*                                                                            */
/*============================================================================*/
void VflTriplanarSlicer::SetBoundsToData()
{
	if(!m_pData)
		return;

	m_fMinBound[0] = m_fMinBound[1] = m_fMinBound[2] = std::numeric_limits<float>::max();
	m_fMaxBound[0] = m_fMaxBound[1] = m_fMaxBound[2] = -std::numeric_limits<float>::max();

	double *pBounds;
	for(int i=0; i<m_pData->GetNumberOfLevels(); ++i)
	{
		if( !m_pData->GetLevelDataByLevelIndex(i) ||
			!m_pData->GetTypedLevelDataByLevelIndex(i)->GetData())
			continue;
		pBounds = m_pData->GetTypedLevelDataByLevelIndex(i)->GetData()->GetBounds();
		m_fMinBound[0] = std::min<float>(pBounds[0], m_fMinBound[0]);
		m_fMinBound[1] = std::min<float>(pBounds[2], m_fMinBound[1]);
		m_fMinBound[2] = std::min<float>(pBounds[4], m_fMinBound[2]);
		m_fMaxBound[0] = std::max<float>(pBounds[1], m_fMaxBound[0]);
		m_fMaxBound[1] = std::max<float>(pBounds[3], m_fMaxBound[1]);
		m_fMaxBound[2] = std::max<float>(pBounds[5], m_fMaxBound[2]);
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void VflTriplanarSlicer::SetCenter( const VistaVector3D &v3Pos)
{
	float fVals[3];
	v3Pos.GetValues(fVals);
	this->SetCenter(fVals);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void VflTriplanarSlicer::SetCenter( const float fCenter[3])
{
	//clamp center to bounding box!
	m_fCenter[0] = std::min<float>(
		std::max<float>(fCenter[0], m_fMinBound[0]), m_fMaxBound[0]);
	m_fCenter[1] = std::min<float>(
		std::max<float>(fCenter[1], m_fMinBound[1]), m_fMaxBound[1]);
	m_fCenter[2] = std::min<float>(
		std::max<float>(fCenter[2], m_fMinBound[2]), m_fMaxBound[2]);
	this->UpdatePlaneParameters();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter                                                   */
/*                                                                            */
/*============================================================================*/
VistaVector3D VflTriplanarSlicer::GetCenter() const
{
	return VistaVector3D(m_fCenter);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCenter                                                   */
/*                                                                            */
/*============================================================================*/
void VflTriplanarSlicer::GetCenter(float fCenter[3]) const
{
	fCenter[0] = m_fCenter[0];
	fCenter[1] = m_fCenter[1];
	fCenter[2] = m_fCenter[2];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflTriplanarSlicer::ObserverUpdate(IVistaObserveable *pObserveable, 
										 int msg, int ticket)
{
	switch(msg){
		case VflTriplanarSlicerProperties::MSG_DRAWMODE_CHANGE:
			{
				this->UpdatePlaneParameters();
			}
			break;
		default :
			break;
	}

	IVflVisObject::ObserverUpdate(pObserveable, msg, ticket);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflTriplanarSlicer::GetType() const
{
	return std::string("VflTriplanarSlicer");
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflTriplanarSlicer::GetRegistrationMode() const
{
	return OLI_DRAW_TRANSPARENT;
}


//###########################################################################################

/*============================================================================*/
/*                                                                            */
/*  NAME      :   VflTriplanarSlicerProperties                               */
/*                                                                            */
/*============================================================================*/
VflTriplanarSlicer::VflTriplanarSlicerProperties::VflTriplanarSlicerProperties()
: IVflVisObject::VflVisObjProperties(),
	m_iDrawMode(DM_AT_CENTER)
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ~VflTriplanarSlicerProperties                              */
/*                                                                            */
/*============================================================================*/
VflTriplanarSlicer::VflTriplanarSlicerProperties::~VflTriplanarSlicerProperties()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDrawMode                                                 */
/*                                                                            */
/*============================================================================*/
int VflTriplanarSlicer::VflTriplanarSlicerProperties::GetDrawMode() const
{
	return m_iDrawMode;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDrawMode                                                 */
/*                                                                            */
/*============================================================================*/
bool VflTriplanarSlicer::VflTriplanarSlicerProperties::SetDrawMode(
	int nDrawMode)
{
	if(compAndAssignFunc<int>(nDrawMode, m_iDrawMode) == 1)
	{
		Notify(MSG_DRAWMODE_CHANGE);
		return true;
	}
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string 
VflTriplanarSlicer::VflTriplanarSlicerProperties::GetReflectionableType() const
{
	return SsReflectionType;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflTriplanarSlicer::VflTriplanarSlicerProperties::AddToBaseTypeList(
	std::list<std::string> &rBtList) const
{
	int nSize = VflVisObjProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}
/*============================================================================*/
/* END OF FILE       VflTriplanarSlicer.cpp                                   */
/*============================================================================*/


