/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VflVolumeDataConverter.h"

#include <cassert>


namespace VflVolumeDataConverter
{
	template<> VistaTexture* ConvertTo3DTexture<VveCartesianGrid>(
		VveCartesianGrid* const pData,
		float aBoundsOut[6],
		int iTexutreInterpolationMode )
	{
		VistaTexture* pVolumeTexture = new VistaTexture( GL_TEXTURE_3D );
		pVolumeTexture->Bind();

		// Determine bounds of data
		pData->GetBounds( aBoundsOut );

		int iStride = pData->GetComponents();
		
		// So far, only one and four component textures are supported. In case
		// of four components, the first three are assumed to hold a vector
		// leaving the fourth to hold the scalar data.
		assert( iStride == 4 || iStride == 1 );

		// Retrieve data and move up to first fourth component entry
		float* pScalarField = pData->GetDataAsFloat();

		// Determine how many cells there are
		int iXDim, iYDim, iZDim;
		pData->GetDimensions( iXDim, iYDim, iZDim );
		const int iSize = iXDim * iYDim * iZDim;

		// Prepare some target memory that will be copied to the texture
		float* pTextureMemory = new float[iSize];

		// For each cell read only the fourth, i.e. the scalar component
		if( iStride == 1 )
		{
			for( size_t i=0; i<iSize; ++i )
				pTextureMemory[i] = pScalarField[i];
		}
		else if( iStride == 4 )
		{
			for( size_t i=0; i<iSize; ++i )
				pTextureMemory[i] = pScalarField[4*i+3];
		}
		else
		{
			assert( "Number of components must be one or four" && false );
		}

		glTexImage3D( GL_TEXTURE_3D, 0, GL_R32F, iXDim, iYDim, iZDim, 0,
			GL_RED, GL_FLOAT, pTextureMemory );
		
		glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, iTexutreInterpolationMode );
		glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, iTexutreInterpolationMode );
		glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );
		glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT );
		glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_MIRRORED_REPEAT );

		delete [] pTextureMemory;
		pVolumeTexture->Unbind();

		return pVolumeTexture;
	}
}
