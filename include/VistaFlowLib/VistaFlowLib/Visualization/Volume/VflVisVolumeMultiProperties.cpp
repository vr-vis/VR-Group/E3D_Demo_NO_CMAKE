/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


// VistaFlowLib includes
#include "VflVisVolumeMultiProperties.h"
#include "../VflVtkLookupTable.h"
#include "../VflLookupTexture.h"
#include <VistaVisExt/Tools/VveUtils.h>

#include <VistaAspects/VistaAspectsUtils.h>
#include <string>
#include <list>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string s_strVflVisVolumeMultiPropertiesReflType("VflVisVolumeMultiProperties");

static IVistaPropertyGetFunctor *s_aVflVisVolumeMultiPropertiesGetFunctors[] =
{
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *s_aVflVisVolumeMultiPropertiesSetFunctors[] =
{
	NULL
};
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/

VflVisVolumeMultiPropertiesBase::VflVisVolumeMultiPropertiesBase()
{
	// disabled  by default
	m_aAlphaFade[0] = 0.0f; 
	m_aAlphaFade[1] = 0.0f;
	m_aAlphaFade[2] = 1.0f;

	for(int i=0; i < 3; ++i)
	{
		VflVtkLookupTable* pTable = new VflVtkLookupTable();
		m_vecMultiLookupTable.push_back(pTable);
		m_vecManageMultiLut.push_back(true);    
		m_vecMultiLookupTexture.push_back(new VflLookupTexture(pTable));
		m_vecManageMultiTexture.push_back(true);
	}
}

void VflVisVolumeMultiPropertiesBase::SetAlphaFade(const float fScale, const float fThreshold, const float fAlphaWeight)
{
	m_aAlphaFade[0] = fScale;
	m_aAlphaFade[1] = fThreshold;
	m_aAlphaFade[2] = fAlphaWeight;
}

const float* VflVisVolumeMultiPropertiesBase::GetAlphaFade() const
{
	return m_aAlphaFade;
}


VflVisVolumeMultiPropertiesBase::~VflVisVolumeMultiPropertiesBase()
{
	for(int i=0; i < 3; ++i)
	{
		if (m_vecManageMultiTexture[i])
			delete m_vecMultiLookupTexture[i];

		if (m_vecManageMultiLut[i])
			delete m_vecMultiLookupTable[i];
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTable                                              */
/*                                                                            */
/*============================================================================*/
VflVtkLookupTable *VflVisVolumeMultiPropertiesBase::GetMultiLookupTable(const int iChannel) const
{
	if(iChannel >=0 && iChannel < 3)
		return m_vecMultiLookupTable[iChannel];
	return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTexture                                            */
/*                                                                            */
/*============================================================================*/
VflLookupTexture *VflVisVolumeMultiPropertiesBase::GetMultiLookupTexture(const int iChannel) const
{
	if(iChannel >=0 && iChannel < 3)
		return m_vecMultiLookupTexture[iChannel];
	return NULL;
}

VflVisVolumeMultiProperties::VflVisVolumeMultiProperties(VflVtkLookupTable *pLut)
	:	VflVisVolume::VflVisVolumeProperties(pLut)
{ }


VflVisVolumeMultiProperties::~VflVisVolumeMultiProperties()
{ }

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflVisVolumeMultiProperties::GetReflectionableType() const
{
	return s_strVflVisVolumeMultiPropertiesReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflVisVolumeMultiProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strVflVisVolumeMultiPropertiesReflType);
	return i+1;
}



/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
