/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include <cstring>

// VistaFlowLib includes
#include "VflVisVolume.h"

#include "VflVolumeRendererInterface.h"

#include "../VflRenderNode.h"
#include "../VflVisTiming.h"
#include "../IVflTransformer.h"
#include "../VflLookupTexture.h"
#include "../VflVtkLookupTable.h"

#include <VistaOGLExt/VistaTexture.h>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/

VflVisVolume::VflVisVolume(VveUnsteadyCartesianGrid *pData,
				  VflVisVolume::VflVisVolumeProperties *pProperties,
				  VflVtkLookupTable* pTable,
			      VflUnsteadyCartesianGridPusher::TEXTURE_PRECISION eTexturePrecision
               )
: m_pData(pData),
  m_pPusher(NULL),
  m_iCurrentRenderer(-1),
  m_pOpacityCorrection(NULL),
  m_fLastSamplingRate(-1.0f),
  m_fLastReferenceRate(-1.0f),
  m_bNeedOpacityCorrectionUpdate(true)
{
	m_pPusher = new VflUnsteadyCartesianGridPusher(m_pData,eTexturePrecision);

	memset(m_aViewportState, 0, 4*sizeof(float));
	
	if(pProperties != NULL){
		m_pProperties = pProperties;
		Observe(m_pProperties, E_PROP_TICKET);
		m_pProperties->SetParentRenderable(this);
	}
	else
	{
		if(pTable != NULL)
		{
			m_pProperties = CreateProperties(pTable);
			m_pProperties->SetParentRenderable(this);
			Observe(m_pProperties, E_PROP_TICKET);
		}
		else
		{
			IVflRenderable::Init();
		}
	}

	m_pOpacityCorrection = new VistaTexture(GL_TEXTURE_1D);
	m_pOpacityCorrection->Bind();
	glTexParameteri(m_pOpacityCorrection->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP);  
	glTexParameteri(m_pOpacityCorrection->GetTarget(), GL_TEXTURE_MIN_FILTER, GL_NEAREST);  
	glTexParameteri(m_pOpacityCorrection->GetTarget(), GL_TEXTURE_MAG_FILTER, GL_NEAREST);  
	glTexImage1D(m_pOpacityCorrection->GetTarget(), 0, GL_ALPHA16F_ARB, 256, 0, GL_ALPHA, GL_FLOAT, NULL);
}


VflVisVolume::~VflVisVolume()
{
	delete m_pOpacityCorrection;
	m_pOpacityCorrection = NULL;

	// we don't destroy the renderers here, because we haven't been responsible
	// for creating them, and thus we are not responsible for destroying them ;-)
	// UPDATE: well, we DO destroy them - unless they have explicitly been
	// removed beforehand...
	for (std::vector<IVflVolumeRenderer*>::size_type i=0;
		i<m_vecRenderers.size(); ++i)
	{
		delete m_vecRenderers[i];
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/

IVflVisObject::VflVisObjProperties *VflVisVolume::CreateProperties() const
{
	return new VflVisVolumeProperties;
}

IVflVisObject::VflVisObjProperties *VflVisVolume::CreateProperties(VflVtkLookupTable *pTable) const
{
	return new VflVisVolumeProperties(pTable);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/

VflVisVolume::VflVisVolumeProperties *VflVisVolume::GetProperties() const
{ 

	return static_cast<VflVisVolumeProperties*>(
		IVflRenderable::GetProperties());

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflVisVolume::Init()
{
	// TODO: check for OpenGL extensions!!!

	if (!m_pData)
	{
		vstr::errp() << " [VflVisVolume] no data given..." << endl;
		return false;
	}

	if (!m_pPusher)
	{
		vstr::errp() << " [VflVisVolume] unable to retrieve texture pusher..." << endl;
		return false;
	}

	// configure texture pusher
	m_pPusher->SetLookAheadCount(GetProperties()->GetLookAheadCount());
	m_pPusher->SetStreamingUpdate(GetProperties()->GetStreamingUpdate());

	// check renderers
	if (m_vecRenderers.empty())
		vstr::warnp() << " [VflVisVolume] no renderers given..." << endl;
	else 
	{
		m_iCurrentRenderer = 0;
	}

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflVisVolume::Update()
{
	// an update consists of the following steps:
	// - let the texture pusher send data to the GPU
	// - update opacity correction texture (if necessary)
  // - update transformation according to vis time
	// - let the currently active renderer prepare data

	double dVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	//bool bAnimationPlaying = m_pController->GetVisTiming()->IsAnimationPlaying();

	//m_pPusher->Update(fVisTime, bAnimationPlaying);
	m_pPusher->Update(GetRenderNode());

	if (m_bNeedOpacityCorrectionUpdate)
		UpdateOpacityCorrection();
  
  UpdateTransform(dVisTime);

	if (m_iCurrentRenderer >= 0)
		m_vecRenderers[m_iCurrentRenderer]->Update();

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVisVolume::DrawOpaque()
{
	if (!GetVisible())
		return;

  // make sure current matrix mode is the modelview matrix
  glPushAttrib(GL_TRANSFORM_BIT);
  glMatrixMode(GL_MODELVIEW);
  // store current modelview matrix and apply custom transformation
  glPushMatrix();
  ApplyTransform();

	if (m_iCurrentRenderer >= 0)
		m_vecRenderers[m_iCurrentRenderer]->DrawOpaque();

  // restore previous modelview matrix, i.e., undo custom transformation
  glPopMatrix();
  // restore matrix mode state 
  glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVisVolume::DrawTransparent()
{
	if (!GetVisible())
		return;

  // make sure current matrix mode is the modelview matrix
  glPushAttrib(GL_TRANSFORM_BIT);
  glMatrixMode(GL_MODELVIEW);
  // store current modelview matrix and apply custom transformation
  glPushMatrix();
  ApplyTransform();

	if (m_iCurrentRenderer >= 0)
		m_vecRenderers[m_iCurrentRenderer]->DrawTransparent();

  // restore previous modelview matrix, i.e., undo custom transformation
  glPopMatrix();
  // restore matrix mode state 
  glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void VflVisVolume::Draw2D()
{
	if (!GetVisible())
		return;

	if (m_iCurrentRenderer >= 0)
		m_vecRenderers[m_iCurrentRenderer]->Draw2D();

	if (GetProperties()->GetDrawOverlay())
	{
		// draw texture
		
		VflLookupTexture *pTexture = GetProperties()->GetLookupTexture();
		if (pTexture)
		{

			glPushAttrib(GL_ALL_ATTRIB_BITS);
			glDisable(GL_LIGHTING);
			glDisable(GL_ALPHA_TEST);
			glDisable(GL_DEPTH_TEST);
			glDisable(GL_BLEND);
			glDisable(GL_CULL_FACE);
			glColor4f(1, 1, 1, 1);

			glPushMatrix();
			glLoadIdentity();
			glMatrixMode(GL_PROJECTION);
			glPushMatrix();
			glLoadIdentity();
			gluOrtho2D(0, 1, 0, 1);

			pTexture->GetLookupTexture()->Enable();
			pTexture->GetLookupTexture()->Bind();

			glBegin(GL_QUADS);
			glTexCoord1f(0);
			glVertex2f(0.95f, 0.8f);
			glTexCoord1f(0);
			glVertex2f(1.0f, 0.8f);
			glTexCoord1f(1.0f);
			glVertex2f(1.0f, 1.0f);
			glTexCoord1f(1.0f);
			glVertex2f(0.95f, 1.0f);
			glEnd();

			glPopMatrix();
			glMatrixMode(GL_MODELVIEW);
			glPopMatrix();
			glPopAttrib();		
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflVisVolume::GetRegistrationMode() const
{
	return OLI_ALL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VflVisVolume::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	// get the bounds information from the first level data,
	// so we assume that the bounds stay constant all the time
	// todo: do we need better methods to determine the bounds?

	VveUnsteadyCartesianGrid *pData = this->GetData();
	VveCartesianGrid *pFirstLevelData = pData->GetTypedLevelDataByLevelIndex(0)->GetData();
	pFirstLevelData->GetBounds(minBounds, maxBounds);

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVisVolume::GetType() const
{
	return IVflVisObject::GetType() + string("::VflVisVolume");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflVisVolume::Debug(std::ostream &out) const
{
	IVflVisObject::Debug(out);
	out << " [VflVisVolume] - grid data:        " << m_pData << endl;
	out << " [VflVisVolume] - properties object: " << GetProperties() << endl;
	out << " [VflVisVolume] - texture pusher:   " << m_pPusher << endl;
	out << " [VflVisVolume] - renderer count:   " << m_vecRenderers.size() << endl;
	out << " [VflVisVolume] - current renderer: " << m_iCurrentRenderer << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflVisVolume::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	// we expect notifications from the parameter object, only...
	if (pObserveable == static_cast<IVistaObserveable *>(GetProperties()))
	{
		m_pPusher->SetLookAheadCount(GetProperties()->GetLookAheadCount());
		m_pPusher->SetStreamingUpdate(GetProperties()->GetStreamingUpdate());

		if (GetProperties()->GetSamplingRate() != m_fLastSamplingRate
			|| GetProperties()->GetReferenceRate() != m_fLastReferenceRate)
		{
			m_bNeedOpacityCorrectionUpdate = true;
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData                                                     */
/*                                                                            */
/*============================================================================*/
VveUnsteadyCartesianGrid *VflVisVolume::GetData() const
{
	return m_pData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPusher                                                   */
/*                                                                            */
/*============================================================================*/
VflUnsteadyCartesianGridPusher *VflVisVolume::GetPusher() const
{
	return m_pPusher;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Add/Remove/GetRenderer                                      */
/*                                                                            */
/*============================================================================*/
int VflVisVolume::AddRenderer(IVflVolumeRenderer *pRenderer)
{
	if (!pRenderer)
		return -1;

	const int iIndex = static_cast<int>(m_vecRenderers.size());
	m_vecRenderers.resize(iIndex+1);
	m_vecRenderers[iIndex] = pRenderer;

	if (m_iCurrentRenderer < 0)
		SetCurrentRenderer(iIndex);

	return iIndex;
}

bool VflVisVolume::RemoveRenderer(int iIndex)
{
	if (iIndex<0 || iIndex>=int(m_vecRenderers.size()))
		return false;

	for (int i=iIndex; i<int(m_vecRenderers.size())-1; ++i)
		m_vecRenderers[i] = m_vecRenderers[i+1];

	m_vecRenderers.resize(m_vecRenderers.size()-1);

	if (m_vecRenderers.empty())
		m_iCurrentRenderer = -1;
	else
		SetCurrentRenderer(0);

	return true;
}

IVflVolumeRenderer *VflVisVolume::GetRenderer(int iIndex)
{
	if (iIndex<0 || iIndex>=int(m_vecRenderers.size()))
		return NULL;

	return m_vecRenderers[iIndex];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetCurrentRenderer                                      */
/*                                                                            */
/*============================================================================*/
bool VflVisVolume::SetCurrentRenderer(int iIndex)
{
	if (iIndex<0 || iIndex>=int(m_vecRenderers.size()))
		return false;

	m_iCurrentRenderer = iIndex;

	return true;
}

int VflVisVolume::GetCurrentRenderer() const
{
	return m_iCurrentRenderer;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRendererCount                                            */
/*                                                                            */
/*============================================================================*/
int VflVisVolume::GetRendererCount() const
{
	return static_cast<int>(m_vecRenderers.size());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetOpacityCorrectionTexture                                 */
/*                                                                            */
/*============================================================================*/
VistaTexture *VflVisVolume::GetOpacityCorrectionTexture() const
{
	return m_pOpacityCorrection;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SaveOGLState                                                */
/*                                                                            */
/*============================================================================*/
void VflVisVolume::SaveOGLState()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	// save old viewport state
	glGetIntegerv(GL_VIEWPORT, m_aViewportState);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RestoreOGLState                                             */
/*                                                                            */
/*============================================================================*/
void VflVisVolume::RestoreOGLState()
{
	// restore old projection and modelview matrices
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	// restore old viewport
	glViewport(m_aViewportState[0], m_aViewportState[1], m_aViewportState[2], m_aViewportState[3]);

	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateOpacityCorrection                                     */
/*                                                                            */
/*============================================================================*/
void VflVisVolume::UpdateOpacityCorrection()
{
	float aTemp[256];
	m_fLastSamplingRate = GetProperties()->GetSamplingRate();
	m_fLastReferenceRate = GetProperties()->GetReferenceRate();
	float fPower(m_fLastSamplingRate / m_fLastReferenceRate);
	float fInc = 1.0f / 255;
	float fAlpha = 0; // 0.5f * fInc;
	for (int i=0; i<256; ++i, fAlpha += fInc)
	{
		aTemp[i] = 1.0f - (pow(1.0f-fAlpha, fPower));
	}

	m_pOpacityCorrection->Bind();
	glTexSubImage1D(m_pOpacityCorrection->GetTarget(), 0, 0, 256, 
		GL_ALPHA, GL_FLOAT, aTemp);

	m_bNeedOpacityCorrectionUpdate = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateTransform                                             */
/*                                                                            */
/*============================================================================*/
const bool VflVisVolume::UpdateTransform(const double fVisTime)
{
	IVflTransformer * pTransformer = GetTransformer();
	if(pTransformer)
	{
		const double dSimTime = GetData()->GetTimeMapper()->GetSimulationTime(fVisTime);
		VistaTransformMatrix m4Matrix;
		const bool bTransformOK = pTransformer->GetUnsteadyTransform(dSimTime,m4Matrix); 
		if(bTransformOK)
			m_m4Transform = m4Matrix;
		return bTransformOK;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ApplyTransform                                              */
/*                                                                            */
/*============================================================================*/
void VflVisVolume::ApplyTransform()
{
	float fMatrix[16] = {
			m_m4Transform[0][0], m_m4Transform[1][0], m_m4Transform[2][0], m_m4Transform[3][0],
			m_m4Transform[0][1], m_m4Transform[1][1], m_m4Transform[2][1], m_m4Transform[3][1],
			m_m4Transform[0][2], m_m4Transform[1][2], m_m4Transform[2][2], m_m4Transform[3][2],
			m_m4Transform[0][3], m_m4Transform[1][3], m_m4Transform[2][3], m_m4Transform[3][3]};
	glMultMatrixf(fMatrix);
}


// #############################################################################
// ################### VflVisVolumeProperties ##################################
// #############################################################################

/* ==========================================================================*/
/* MAKROS AND DEFINES														 */
/* ==========================================================================*/

static const string SsReflectionType("VflVisVolumeProperties");

static IVistaPropertyGetFunctor *aCgFunctors[] = 
{
	new TVistaPropertyGet<bool, VflVisVolume::VflVisVolumeProperties>
			("VISIBLE", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::GetVisible),
	new TVistaPropertyGet<bool, VflVisVolume::VflVisVolumeProperties>
			("DRAW_OVERLAY", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::GetDrawOverlay),
	new TVistaPropertyGet<float, VflVisVolume::VflVisVolumeProperties>
			("BLENDING_FACTOR", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::GetBlendingFactor),
	new TVistaPropertyGet<float, VflVisVolume::VflVisVolumeProperties>
			("REFERENCE_RATE", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::GetReferenceRate),
	new TVistaPropertyGet<float, VflVisVolume::VflVisVolumeProperties>
			("SAMPLING_RATE", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::GetSamplingRate),
	new TVistaPropertyGet<bool, VflVisVolume::VflVisVolumeProperties>
			("DRAW_SLICES", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::GetDrawSlices),
	new TVistaPropertyGet<bool, VflVisVolume::VflVisVolumeProperties>
			("DRAW_BOUNDS", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::GetDrawBounds),
	new TVistaProperty3RefGet<float, VflVisVolume::VflVisVolumeProperties>
			("BOUND_COLOR", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::GetBoundColor),
	new TVistaPropertyGet<int, VflVisVolume::VflVisVolumeProperties>
			("LOOK_AHEAD_COUNT", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::GetLookAheadCount),
	new TVistaPropertyGet<bool, VflVisVolume::VflVisVolumeProperties>
			("STREAMING_UPDATE", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::GetStreamingUpdate),
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool, bool, VflVisVolume::VflVisVolumeProperties>
			("VISIBLE", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::SetVisible),
	new TVistaPropertySet<bool, bool, VflVisVolume::VflVisVolumeProperties>
			("DRAW_OVERLAY", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::SetDrawOverlay),
	new TVistaPropertySet<float, float, VflVisVolume::VflVisVolumeProperties>
			("BLENDING_FACTOR", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::SetBlendingFactor),
	new TVistaPropertySet<float, float, VflVisVolume::VflVisVolumeProperties>
			("REFERENCE_RATE", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::SetReferenceRate),
	new TVistaPropertySet<float, float, VflVisVolume::VflVisVolumeProperties>
			("SAMPLING_RATE", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::SetSamplingRate),
	new TVistaPropertySet<bool, bool, VflVisVolume::VflVisVolumeProperties>
			("DRAW_SLICES", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::SetDrawSlices),
	new TVistaPropertySet<bool, bool, VflVisVolume::VflVisVolumeProperties>
			("DRAW_BOUNDS", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::SetDrawBounds),
	new TVistaProperty3ValSet<float, VflVisVolume::VflVisVolumeProperties>
			("BOUND_COLOR", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::SetBoundColor),
	new TVistaPropertySet<int, int, VflVisVolume::VflVisVolumeProperties>
			("LOOK_AHEAD_COUNT", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::SetLookAheadCount),
	new TVistaPropertySet<bool, bool, VflVisVolume::VflVisVolumeProperties>
			("STREAMING_UPDATE", SsReflectionType,
			 &VflVisVolume::VflVisVolumeProperties::SetStreamingUpdate),
	NULL
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/

VflVisVolume::VflVisVolumeProperties::VflVisVolumeProperties()
	: m_pLookupTable(NULL)
	, m_pLookupTexture(NULL)
	, m_bManageLut(false)
	, m_bManageTexture(false)
	, m_bVisible(true)
	, m_bDrawOverlay(false)
	, m_fBlendingFactor(1.0f)
	, m_fReferenceRate(1.0f)
	, m_fSamplingRate(1.0f)
	, m_bDrawSlices(false)
	, m_iLookAheadCount(0)
	, m_bStreamingUpdate(false)
	, m_bUsePreIntegration(false)
	, m_bSlicerSegmentation(false)
	, m_fOpacityFactor(1.0f)
	, m_fSamplingDensity(1.0f)
{
	m_fBoundColor[0] = 0.5f;
	m_fBoundColor[1] = 1.0f;
	m_fBoundColor[2] = 1.0f;

	if(!m_pLookupTable)
	{
		m_pLookupTable = new VflVtkLookupTable;
		m_bManageLut = true;
	}

	if(!m_pLookupTexture)
	{
		m_pLookupTexture = new VflLookupTexture(m_pLookupTable);
		m_bManageTexture = true;
	}
}

VflVisVolume::VflVisVolumeProperties::VflVisVolumeProperties(VflVtkLookupTable *pLut)
	: m_pLookupTable(pLut)
	, m_pLookupTexture(NULL)
	, m_bManageLut(false)
	, m_bManageTexture(false)
	, m_bVisible(true)
	, m_bDrawOverlay(false)
	, m_fBlendingFactor(1.0f)
	, m_fReferenceRate(1.0f)
	, m_fSamplingRate(1.0f)
	, m_bDrawSlices(false)
	, m_iLookAheadCount(0)
	, m_bStreamingUpdate(false)
	, m_bUsePreIntegration(false)
	, m_bSlicerSegmentation(false)
	, m_fOpacityFactor(1.0f)
	, m_fSamplingDensity(1.0f)
{
	m_fBoundColor[0] = 0.5f;
	m_fBoundColor[1] = 1.0f;
	m_fBoundColor[2] = 1.0f;

	if(!m_pLookupTable)
	{
		m_pLookupTable = new VflVtkLookupTable;
		m_bManageLut = true;
	}

	if(!m_pLookupTexture)
	{
		m_pLookupTexture = new VflLookupTexture(m_pLookupTable);
		m_bManageTexture = true;
	}

}

VflVisVolume::VflVisVolumeProperties::~VflVisVolumeProperties()
{
}

int VflVisVolume::VflVisVolumeProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = VflVisObjProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}

void VflVisVolume::VflVisVolumeProperties::Debug(std::ostream &out) const
{
	out << " [VflVisVolumeProperties] - visible:          " << (m_bVisible ? "true" : "false") << endl;
	out << " [VflVisVolumeProperties] - draw overlay:     " << (m_bDrawOverlay ? "true" : "false") << endl;
	out << " [VflVisVolumeProperties] - blending factor:  " << m_fBlendingFactor << endl;
	out << " [VflVisVolumeProperties] - reference rate:   " << m_fReferenceRate << endl;
	out << " [VflVisVolumeProperties] - sampling rate:    " << m_fSamplingRate << endl;
	out << " [VflVisVolumeProperties] - draw slices:      " << (m_bDrawSlices ? "true" : "false") << endl;
	out << " [VflVisVolumeProperties] - look ahead count: " << m_iLookAheadCount << endl;
	out << " [VflVisVolumeProperties] - streaming update: " << (m_bStreamingUpdate ? "true" : "false") << endl;
	out << " [VflVisVolumeProperties] - preintegration: " << (m_bUsePreIntegration? "true" : "false") << endl;
}

std::string VflVisVolume::VflVisVolumeProperties::GetReflectionableType() const
{
	return SsReflectionType;
}

// rendering parameters ---------------

/**
* Set visibility of the volume visualisation.
*/
bool VflVisVolume::VflVisVolumeProperties::SetVisible(bool bVisible)
{
	if (bVisible != m_bVisible)
	{
		m_bVisible = bVisible;
		Notify();
		return true;
	}
	return false;
}

bool VflVisVolume::VflVisVolumeProperties::GetVisible() const
{
	return m_bVisible;
}

/**
 * Draw debugging information as overlay.
 */
bool VflVisVolume::VflVisVolumeProperties::SetDrawOverlay(bool bOverlay)
{
	if (bOverlay != m_bDrawOverlay)
	{
		m_bDrawOverlay = bOverlay;
		Notify();
		return true;
	}
	return false;
}
bool VflVisVolume::VflVisVolumeProperties::GetDrawOverlay() const
{
	return m_bDrawOverlay;
}

/**
* Set blending facton (used for opacity correction based on samle rate).
*/
bool VflVisVolume::VflVisVolumeProperties::SetBlendingFactor(float fFactor)
{
	if (fFactor != m_fBlendingFactor)
	{
		m_fBlendingFactor = fFactor;
		Notify(); 
		return true;
	}
	return false;
}
float VflVisVolume::VflVisVolumeProperties::GetBlendingFactor() const
{
	return m_fBlendingFactor;
}

// sampling parameters ------------------------------------

/**
* Reference value for the sampling rate (used for opacity correction
* based on sample rate).
*/
bool VflVisVolume::VflVisVolumeProperties::SetReferenceRate(float fRate)
{
	if (fRate != m_fReferenceRate)
	{
		m_fReferenceRate = fRate;
		Notify(); 
		return true;
	}
	return false;
}
float VflVisVolume::VflVisVolumeProperties::GetReferenceRate() const
{
	return m_fReferenceRate;
}

/**
* Sampling rate in visualization system coordinates.
*/
bool VflVisVolume::VflVisVolumeProperties::SetSamplingRate(float fRate)
{
	if (fRate != m_fSamplingRate)
	{
		m_fSamplingRate = fRate;
		Notify();
		return true;
	}
	return false;
}
float VflVisVolume::VflVisVolumeProperties::GetSamplingRate() const
{
	return m_fSamplingRate;
}

// debugging help -----------------------------------------
/**
* Draw simple slice outlines instead of actual volume data.
*/
bool VflVisVolume::VflVisVolumeProperties::SetDrawSlices(bool bDraw)
{
	if (bDraw != m_bDrawSlices)
	{
		m_bDrawSlices = bDraw;
		Notify();
		return true;
	}
	return false;
}
bool VflVisVolume::VflVisVolumeProperties::GetDrawSlices() const
{
	return m_bDrawSlices;
}

/**
* Draw data boundaries.
*/
bool VflVisVolume::VflVisVolumeProperties::SetDrawBounds(bool bDraw)
{
	if (bDraw != m_bDrawBounds)
	{
		m_bDrawBounds = bDraw;
		Notify();
		return true;
	}
	return false;
}
bool VflVisVolume::VflVisVolumeProperties::GetDrawBounds() const
{
	return m_bDrawBounds;
}

/**
* Color for data boundaries
*/
bool VflVisVolume::VflVisVolumeProperties::SetBoundColor(float fR, float fG, float fB)
{
	if(m_fBoundColor[0]!=fR || m_fBoundColor[1]!=fG || m_fBoundColor[2]!=fB)
	{
		m_fBoundColor[0] = fR;
		m_fBoundColor[1] = fG;
		m_fBoundColor[2] = fB;
		Notify();
		return true;
	}
	return false;
}
bool VflVisVolume::VflVisVolumeProperties::GetBoundColor(float &fR, float &fG, float &fB) const
{
	fR = m_fBoundColor[0];
	fG = m_fBoundColor[1];
	fB = m_fBoundColor[2];
	return true;
}

// texture upload behavior --------------------------------
/**
* Specify number of textures to be loaded in advance
* (this information is forwarded to the VflUnsteadyTexturePusher, 
* see VflUnsteadyTexturePusher.h for more information).
* This can typically be set to 0 for standard volume rendering.
*/
bool VflVisVolume::VflVisVolumeProperties::SetLookAheadCount(int iCount)
{
	if (iCount != m_iLookAheadCount)
	{
		m_iLookAheadCount = iCount;
		Notify();
		return true;
	}
	return false;
}
int VflVisVolume::VflVisVolumeProperties::GetLookAheadCount() const
{
	return m_iLookAheadCount;
}

/**
* Upload new volume data while the animation is playing.
*/
bool VflVisVolume::VflVisVolumeProperties::SetStreamingUpdate(bool bStream)
{
	if (bStream != m_bStreamingUpdate)
	{
		m_bStreamingUpdate = bStream;
		Notify();
		return true;
	}
	return false;
}
bool VflVisVolume::VflVisVolumeProperties::GetStreamingUpdate() const
{
	return m_bStreamingUpdate;
}

/**
* Set wether to use pre integration or not.
*/
bool VflVisVolume::VflVisVolumeProperties::SetPreIntegration(bool bState)
{
	if (bState != m_bUsePreIntegration)
	{
    m_bUsePreIntegration= bState;
    m_pLookupTexture->SetUpdatePreIntegration(bState);
    if(m_bUsePreIntegration)
		m_pLookupTexture->ObserverUpdate(0,0,0);
		Notify();
		return true;
	}
	return false;
}
bool VflVisVolume::VflVisVolumeProperties::GetPreIntegration() const
{
	return m_bUsePreIntegration;
}

/**
* Set overall opacity (only used in Raycaster). Values larger than 1 increase
* opacity, values smaller then 1 decrease opacity
*/
bool VflVisVolume::VflVisVolumeProperties::SetOpacityFactor(float fFactor)
{
	if (fFactor != m_fOpacityFactor)
	{
		m_fOpacityFactor = fFactor;
		Notify(); 
		return true;
	}
	return false;
}
float VflVisVolume::VflVisVolumeProperties::GetOpacityFactor() const
{
	return m_fOpacityFactor;
}

/**
* Set sampling density (only used in Raycaster). 
* The sampling rate is automatically adjusted to the size of the dataset.
* Default value is 1.
* Values larger than 1 increase
* sampling density, values smaller then 1 decrease sampling density.
*/	
bool VflVisVolume::VflVisVolumeProperties::SetSamplingDensity(float fDensity)
{
	if (fDensity != m_fSamplingDensity)
	{
		m_fSamplingDensity = fDensity;
		Notify(); 
		return true;
	}
	return false;
}
float VflVisVolume::VflVisVolumeProperties::GetSamplingDensity() const
{
	return m_fSamplingDensity;
}


/**
* Set wether to use rendering for segmentation in Slicer
*/
bool VflVisVolume::VflVisVolumeProperties::SetSlicerSegmentation(bool bState)
{
	if (bState != m_bSlicerSegmentation)
	{
    m_bSlicerSegmentation= bState;
		Notify();
		return true;
	}
	return false;
}
bool VflVisVolume::VflVisVolumeProperties::GetSlicerSegmentation() const
{
	return m_bSlicerSegmentation;
}

VflVtkLookupTable	*VflVisVolume::VflVisVolumeProperties::GetLookupTable() const
{
	return m_pLookupTable;
}

VflLookupTexture	*VflVisVolume::VflVisVolumeProperties::GetLookupTexture() const
{
	return m_pLookupTexture;
}

// #############################################################################
// ################## End of VlfVisVolumeProperties ############################
// #############################################################################


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
