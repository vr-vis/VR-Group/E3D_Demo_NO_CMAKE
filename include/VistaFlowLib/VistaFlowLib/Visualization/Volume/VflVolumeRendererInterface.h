/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVOLUMERENDERERINTERFACE_H
#define _VFLVOLUMERENDERERINTERFACE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <string>
#include <list>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisVolume;

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 */
class VISTAFLOWLIBAPI IVflVolumeRenderer : public IVistaReflectionable
{
public:
	IVflVolumeRenderer(VflVisVolume *pParent);
	virtual ~IVflVolumeRenderer();

	virtual void Update();
	virtual void DrawOpaque();
	virtual void DrawTransparent();
	virtual void Draw2D();

	virtual bool IsValid() const = 0;
	virtual std::string GetReflectionableType() const;

protected:
	IVflVolumeRenderer();
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	VflVisVolume *m_pParent;
};


#endif // _VFLVOLUMERENDERERINTERFACE_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

