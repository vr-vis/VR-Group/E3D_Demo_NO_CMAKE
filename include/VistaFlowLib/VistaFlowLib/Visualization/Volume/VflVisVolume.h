/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVISVOLUME_H
#define _VFLVISVOLUME_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VflVisObject.h"
#include "../../Data/VflUnsteadyCartesianGridPusher.h"
#include <VistaVisExt/Data/VveCartesianGrid.h>

#include <VistaAspects/VistaPropertyAwareable.h>

#include <vector>






/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVflVolumeRenderer;
class VistaTexture;
class VflLookupTexture;
class VflVtkLookupTable;

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 */

//VflSharedLookupTable *pLut;

class VISTAFLOWLIBAPI VflVisVolume : public IVflVisObject
{


public:

	// #### NESTED CLASS #### REST FOLLOWS ###################
	class VISTAFLOWLIBAPI VflVisVolumeProperties : public IVflVisObject::VflVisObjProperties
	{
		friend class VflVisVolume;
	public:
		VflVisVolumeProperties();

		// Do not pass NULL as lookupTable, instead use constructor without parameters
		VflVisVolumeProperties(VflVtkLookupTable *pLut);
		virtual ~VflVisVolumeProperties();

		virtual void Debug(std::ostream &out) const;
		virtual std::string GetReflectionableType() const;

		// rendering parameters ---------------

		/**
		 * Set visibility of the volume visualisation.
		 */
		bool SetVisible(bool bVisible);
		bool GetVisible() const;

		/**
		 * Draw debugging information as overlay.
		 */
		bool SetDrawOverlay(bool bOverlay);
		bool GetDrawOverlay() const;

		/**
		 * Set blending facton (used for opacity correction based on samle rate).
		 */
		bool SetBlendingFactor(float fFactor);
		float GetBlendingFactor() const;

		// sampling parameters ------------------------------------

		/**
		 * Reference value for the sampling rate (used for opacity correction
		 * based on sample rate).
		 */
		bool SetReferenceRate(float fRate);
		float GetReferenceRate() const;

		/**
		 * Sampling rate in visualization system coordinates.
		 */
		bool SetSamplingRate(float fRate);
		float GetSamplingRate() const;

		// debugging help -----------------------------------------
		/**
		 * Draw simple slice outlines instead of actual volume data.
		 */
		bool SetDrawSlices(bool bDraw);
		bool GetDrawSlices() const;

		/**
		 * Draw data boundaries.
		 */
		bool SetDrawBounds(bool bDraw);
		bool GetDrawBounds() const;

		/**
		 * Color for data boundaries
		 */
		bool SetBoundColor(float fR, float fG, float fB);
		bool GetBoundColor(float &fR, float &fG, float &fB) const;

		// texture upload behavior --------------------------------
		/**
		 * Specify number of textures to be loaded in advance
		 * (this information is forwarded to the VflUnsteadyTexturePusher, 
		 * see VflUnsteadyTexturePusher.h for more information).
		 * This can typically be set to 0 for standard volume rendering.
		 */
		bool SetLookAheadCount(int iCount);
		int GetLookAheadCount() const;

		/**
		 * Upload new volume data while the animation is playing.
		 */
		bool SetStreamingUpdate(bool bStream);
		bool GetStreamingUpdate() const;

		/**
		 * Set wether to use pre integration or not.
		 */
		bool SetPreIntegration(bool bState);
		bool GetPreIntegration() const;

		/**
		 * Set overall opacity (only used in Raycaster). Values larger than 1 increase
		 * opacity, values smaller then 1 decrease opacity
		 */
		bool SetOpacityFactor(float fFactor);
		float GetOpacityFactor() const;

		/**
		 * Set sampling density (only used in Raycaster). 
		 * The sampling rate is automatically adjusted to the size of the dataset.
		 * Default value is 1.
		 * Values larger than 1 increase
		 * sampling density, values smaller then 1 decrease sampling density.
		 */	
		bool SetSamplingDensity(float fDensity);
		float GetSamplingDensity() const;


		/**
		 * Set wether to use rendering for segmentation in Slicer
		 */
		bool SetSlicerSegmentation(bool bState);
		bool GetSlicerSegmentation() const;

		/**
		 * Get the lookup information via table and texture
		 */
		VflVtkLookupTable	*GetLookupTable() const;
		VflLookupTexture	*GetLookupTexture() const;

	protected:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	
		VflVtkLookupTable	   *m_pLookupTable;
		VflLookupTexture     *m_pLookupTexture;
		bool						m_bManageLut; 
		bool						m_bManageTexture; 
		bool						m_bVisible; 
		bool						m_bDrawOverlay; 
		float						m_fBlendingFactor; 
		float						m_fReferenceRate; 
		float						m_fSamplingRate; 
		bool						m_bDrawSlices; 
		bool						m_bDrawBounds; 
		float						m_fBoundColor[3];
		int							m_iLookAheadCount;
		bool						m_bStreamingUpdate;
		bool						m_bUsePreIntegration;
		bool						m_bSlicerSegmentation;
		float						m_fOpacityFactor; //< adjust overall opacity (raycaster only)
		float						m_fSamplingDensity; //< sampling density is controlled by size of dataset (raycaster only)
	};
	 // ######## END NESTED CLASS ##########

	/**
	 * Construct a new VflVisVolume object.
	 */
	VflVisVolume(VveUnsteadyCartesianGrid *pData,
			VflVisVolumeProperties *pProperties = NULL,
			VflVtkLookupTable* pTable = NULL,
			VflUnsteadyCartesianGridPusher::TEXTURE_PRECISION eTexturePrecision = VflUnsteadyCartesianGridPusher::TP_FLOAT);

	virtual ~VflVisVolume();

    // *** IVflRenderable interface. ***
    virtual bool Init();
	virtual void Update();
	virtual void DrawOpaque();
	virtual void DrawTransparent();
	virtual void Draw2D();
	virtual unsigned int GetRegistrationMode() const;

    /**
     * Returns the boundaries of the visualization object.
	 * Note: This method is not declared const, as it might change the
	 *       cached boundaries inside the object.
     * 
     * @param[out]  VistaVector3D minBounds    minimum of the object's AABB
	 * @param[out]  VistaVector3D maxBounds    maximum of the object's AABB
     * @return  bool	is the bounding box valid?
     */    
	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);

    /**
     * Process a given command event.
     */    
	//virtual void ProcessCommand(VflVOCEvent *pEvent);

	/**
     * Returns the name of the visualization object.
     * 
     * @param[in]   --
     * @return  std::string
     */    
	virtual std::string GetType() const;

    /**
     * Prints out some debug information to the given output stream.
     * 
     * @param[in]   std::ostream & out
     * @return  --
     */    
    virtual void Debug(std::ostream & out) const;
	
    VflVisVolumeProperties *GetProperties() const;
    /**
     * Notify this object of any changes in the tracing parameters.
     */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/** 
	 * Retrieve some information.
	 */
	int GetDataType() const;
	VveUnsteadyCartesianGrid *GetData() const;
	VflUnsteadyCartesianGridPusher *GetPusher() const;
	float GetVisTime() const;

	/**
	 * Manage volume renderers.
	 */
	int AddRenderer(IVflVolumeRenderer *pRenderer);
	bool RemoveRenderer(int iIndex);
	IVflVolumeRenderer *GetRenderer(int iIndex);
	bool SetCurrentRenderer(int iIndex);
	int GetCurrentRenderer() const;
	int GetRendererCount() const;
	VistaTexture *GetOpacityCorrectionTexture() const;

	/**
	 * Helpers for managing OpenGL state for GPGPU.
	 */
	void SaveOGLState();
	void RestoreOGLState();

	enum NOTIFICATION_ID
	{
		NI_PARAMETERS_CHANGE
	};

protected:
	virtual VflVisObjProperties *CreateProperties() const;
	virtual VflVisObjProperties *CreateProperties(VflVtkLookupTable *pTable) const;

	VflVisVolume();
	void UpdateOpacityCorrection();

  /** Updates transformation matrix according to vis time. 
   * Transformation matrix is used in ApplyTransform().
   * @param fVisTime current visualization time
   * @return true if transformation matrix has been updated, false otherwise
   */
  const bool UpdateTransform(const double fVisTime);

  /** Applies current transformation matrix to the OpenGL matrix stack.
   * ApplyTransform() is called in DrawOpaque() and DrawTransparent().
   */
  void ApplyTransform();

    VveUnsteadyCartesianGrid		*m_pData;
	VflUnsteadyCartesianGridPusher	*m_pPusher;

	std::vector<IVflVolumeRenderer *>	m_vecRenderers;
	int		m_iCurrentRenderer;

	int	m_aViewportState[4];

	// opacity correction features
	VistaTexture *m_pOpacityCorrection;
	float m_fLastSamplingRate;
	float m_fLastReferenceRate;
	bool m_bNeedOpacityCorrectionUpdate;
   
  // transformation matrix, is updated by UpdateTransform() and applied by ApplyTransform()
  VistaTransformMatrix m_m4Transform;

  private:
	  friend class VflVisVolumeProperties;
};

#endif // _VFLVISVOLUME_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

