/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFL_RAYCAST_PROXY_H
#define VFL_RAYCAST_PROXY_H


#include <GL/glew.h>

#include "../../VistaFlowLibConfig.h"

#include <vector>
#include <VistaMath/VistaVector.h>
#include <VistaBase/VistaVector3D.h>

class VistaGLSLShader;
class VistaRenderbuffer;
class VistaFramebufferObj;
class VistaTexture;
class VistaBufferObject;
class VflVolumeRendererRaycast;
class VflRenderNode;

/// proxy geometry for entry points of volume raycaster
class VISTAFLOWLIBAPI VflVolumeRaycastProxy 
{
  public:
    /// c'tor requires valid OpenGL context
    /// \param width width of raycast resolution
    /// \param height height of raycast resolution
    VflVolumeRaycastProxy(const int iWidth, const int iHeight, const float fNearPlane);
    virtual ~VflVolumeRaycastProxy();

    /// updates offscreen buffers
    /// \param _m modelview matrix
    void UpdateBuffer(const VistaTransformMatrix &matModelview, const VistaTransformMatrix & matLocalTransform);

    // draw content of offscreen buffer (for debug only)
    void DrawBuffer(const unsigned int iWhich);
    // binds content of offscreen buffer to texture
    void BindTexture(const unsigned int iWhich);

    const int Width() const {return m_iWidth;}
    const int Height() const {return m_iHeight;}

    void SetOrthoView();
    void RestoreView();
    void DrawQuad();

    void Resize(const int iWidth, const int iHeight, const bool bForce = false);
    void SetRenderNode(VflRenderNode* pNode);

    // draw proxy as vbo call
    void DrawProxy(const bool bAsProxy=true);
  protected:
    // init vbo for proxy 
    void CreateProxy();
    // init shader
    void CreateShader();
    // draw back/front faces
    void DrawFaces(GLenum eCullMode);
    // create buffers with entry and exit points
    void CreateBuffers();
        // draw entry points
    void DrawEntry();
    // draw exit points
    void DrawExit();
    // draw near clipping plane
    void DrawNearPlane(const float fNearPlane, const float* pMatrix);
    void AttachDepthBuffer();
    void DrawGeometry(const VistaTransformMatrix &matModelview, const VistaTransformMatrix &matLocal); 
    void DrawViewerBox(const VistaTransformMatrix &matLocal);
   
    VflRenderNode* m_pRenderNode; //< store render node for geometry intersection
    int m_iWidth, m_iHeight; //< width and height of raycast resolution (e.g. screensize)
    float m_fNearPlane; //< near clipping plane
    VistaBufferObject* m_pVboProxy; //< vertex buffer of proxy
    int m_nNumElements; //< number of points in proxy
    VistaGLSLShader* m_pEntryShader;//< shader to setup entry/exit points of rays
    VistaGLSLShader* m_pGeomShader;//< shader to setup entry/exit points of geometry
    VistaFramebufferObj* m_pTexBuffer; //< offscreen buffer
    std::vector<VistaTexture*> m_vecTextures; //< offscreen textures
    VistaRenderbuffer* m_pDepthBuffer;//< offscreen depth buffer
    std::vector<VistaVector3D> m_vecCorners; //< cube corners
    std::vector<VistaVector<int,2> > m_vecCornerIndices; //< indices of cube lines

};

// VFL_RAYCAST_PROXY_H
#endif
