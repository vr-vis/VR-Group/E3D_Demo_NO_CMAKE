/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflUnsteadyDataFilter.h"

#include <VistaAspects/VistaAspectsUtils.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflUnsteadyDataFilter::VflUnsteadyDataFilter()
: m_bActive(true)
{
}

VflUnsteadyDataFilter::~VflUnsteadyDataFilter()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

static const std::string SsReflectionableType("VflUnsteadyDataFilter");

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetActive                                               */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyDataFilter::SetActive(bool bActive)
{
	if (compAndAssignFunc<bool>(bActive,m_bActive))
	{
		// this one is important!
		bool bNotify = GetNotificationFlag();
		SetNotificationFlag(true);
		Notify(MSG_NEED_REWIRE);
		SetNotificationFlag(bNotify);
		return true;
	}
	return false;
}

bool VflUnsteadyDataFilter::GetActive() const
{
	return m_bActive;
}

static string SsReflectionType("VflUnsteadyDataFilter");
static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, VflUnsteadyDataFilter, VistaProperty::PROPT_BOOL>
	("ACTIVE", SsReflectionType,
	&VflUnsteadyDataFilter::GetActive),
	NULL
};
static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool, bool,
	VflUnsteadyDataFilter>
	("ACTIVE", SsReflectionType,
	&VflUnsteadyDataFilter::SetActive),
	NULL
};

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflUnsteadyDataFilter.cpp"                                           */
/*============================================================================*/
