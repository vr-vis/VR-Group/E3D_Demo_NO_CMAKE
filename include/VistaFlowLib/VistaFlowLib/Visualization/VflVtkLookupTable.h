/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  BaseAppTutorial.h	         RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA FlowLib Tutorials      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                        Rheinisch-Westfaelische      */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/


#ifndef _VflVtkLookupTable_h
#define _VflVtkLookupTable_h

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/VistaFlowLibConfig.h>

#include "VflLookupTable.h"

#include <string>
#include <list>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkLookupTable;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflVtkLookupTable : public IVflLookupTable
{
public:
	VflVtkLookupTable();
	virtual ~VflVtkLookupTable();

	enum
	{
		MSG_SCALE_CHANGE = IVflLookupTable::MSG_LAST,
		MSG_LAST
	};

    /**
     * Returns a pointer to the shared vtk lookup table.
     */
    vtkLookupTable* GetLookupTable() const;

	/**
	 * Configure the lookup table as a piecewise linear function per
	 * color channel. The value list is supposed to be built as follows:
	 * "NUM_COLORS_IN_TABLE, INTERPOLATION_MODE[PLF_RGBA==0|PLF_HSVA==1],
	 *  VALUE_POS, R, VALUE_POS, R, ..., VALUE_POS, R, -1,
	 *  VALUE_POS, G, VALUE_POS, G, ..., VALUE_POS, G, -1,
	 *  VALUE_POS, B, VALUE_POS, B, ..., VALUE_POS, B, -1,
	 *  VALUE_POS, A, VALUE_POS, A, ..., VALUE_POS, A"
	 * Thus, every channel can have its own piecewise linear function, 
	 * terminated by -1.
	 * NOTE: For every channel, at least one value has to be given, which 
	 *       is then "smeared" across the whole table.
	 * NOTE: If the control point list (i.e. VALUE_POSs) does not include
	 *       values for 0 or 1 (i.e. the start and end of the function
	 *       interval), the value of the first or last control point is
	 *       "extended" to the start or the end, respectively.
	 * NOTE: For PLF_HSVA interpolation mode, RGBA values are interpreted,
	 *       as HSVA values, of course.
	 *
	 * EXAMPLE: The list "128, 0, 
	 *                    0.5, 0, 1.0, 1, -1,
	 *                    0.1, 0, 0.5, 1, 1.0, 0, -1,
	 *                    0.1, 1, 0.5, 0, -1,
	 *                    0.0, 0, 0.8, 1"
	 * results in a lookup table with 128 values with a constant blue
	 * for the first few values (0 to 0.1), green at the middle of the 
	 * table, and red at its end with values in between being 
	 * linearly interpolated in RGB color space, being completely
	 * transparent at the start of the table and reaching full opacity
	 * at 80% of the table range.
	 */
	enum EPLFState
	{
		PLF_NONE = -1,
		PLF_RGBA = 0,
		PLF_HSVA
	};
	bool SetPiecewiseLinearFunction( const std::list<float>& refPLFDescription );
	std::list<float> GetPiecewiseLinearFunction() const;
	int GetPiecewiseLinearFunction( std::list<float>& refPLFDescription ) const;
	
	virtual bool GetTableRange( float& fMin, float& fMax ) const;
	virtual bool SetTableRange( float  fMin, float  fMax );

	virtual bool GetHueRange( float& fMin, float& fMax ) const;
	virtual bool SetHueRange( float  fMin, float  fMax );

	virtual bool GetSaturationRange( float& fMin, float& fMax ) const;
	virtual bool SetSaturationRange( float  fMin, float  fMax );

	virtual bool GetValueRange( float& fMin, float& fMax ) const;
	virtual bool SetValueRange( float  fMin, float  fMax );

	virtual bool GetAlphaRange( float& fMin, float& fMax ) const;
	virtual bool SetAlphaRange( float  fMin, float  fMax );

	std::string GetScale() const;
	bool SetScale( const std::string &sScaleName );

	int GetScaleId() const;
	bool SetScaleId( int nId );

	virtual bool SetValueCount( unsigned int iNumColors );
	virtual unsigned int GetValueCount() const;

	virtual bool SetTableValues( const std::vector<VistaColor>& vecValues );
	virtual bool GetTableValues( std::vector<VistaColor>& vecValues ) const;

	virtual bool SetTableValues( const std::vector<float>& vecValues );
	virtual std::vector<float> GetTableValues() const;

	virtual bool SetTableValue( unsigned int nIndex, const VistaColor& rColor );
	virtual bool GetTableValue( unsigned int nIndex, VistaColor& rColor ) const;

	virtual VistaColor GetColor( float fScalar ) const;

	virtual void UpdateTableValues();

	REFL_DECLARE

private:
	vtkLookupTable* m_pLookupTable;

	// piecewise linear function management
	std::list<float> m_liPLFDescription;
};

#endif // Include guard
/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
