/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VflVtkProperty.h"
#include <vtkProperty.h>
#include "../Tools/VflPropertyLoader.h"

#include <VistaAspects/VistaAspectsUtils.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

static const std::string SsReflectionType("VFL_SHARED_VTK_PROPERTY");



/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflVtkProperty::VflVtkProperty()
{
    m_pProperty = vtkProperty::New();
}

VflVtkProperty::~VflVtkProperty()
{
    m_pProperty->Delete();
    m_pProperty = NULL;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/


/*============================================================================*/
/*                                                                            */
/*  access methods                                                            */
/*                                                                            */
/*============================================================================*/
vtkProperty *VflVtkProperty::GetVtkProperty() const
{
    return m_pProperty;
}


std::string VflVtkProperty::GetReflectionableType() const
{
	return SsReflectionType;
}

int VflVtkProperty::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}




static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<double, 
	                      VflVtkProperty, 
						  VistaProperty::PROPT_DOUBLE>
	      ("AMBIENT", SsReflectionType,
		  &VflVtkProperty::GetAmbient),

	new TVistaPropertyGet<double, 
	                      VflVtkProperty, 
						  VistaProperty::PROPT_DOUBLE>
	      ("DIFFUSE", SsReflectionType,
		  &VflVtkProperty::GetDiffuse),

	new TVistaPropertyGet<double, 
	                      VflVtkProperty, 
						  VistaProperty::PROPT_DOUBLE>
	      ("SPECULAR", SsReflectionType,
		  &VflVtkProperty::GetSpecular),

	new TVistaPropertyGet<double, 
	                      VflVtkProperty, 
						  VistaProperty::PROPT_DOUBLE>
	      ("SPECULAR_POWER", SsReflectionType,
		  &VflVtkProperty::GetSpecularPower),

	new TVistaPropertyGet<double, 
	                      VflVtkProperty, 
						  VistaProperty::PROPT_DOUBLE>
	      ("OPACITY", SsReflectionType,
		  &VflVtkProperty::GetOpacity),

	new TVistaPropertyGet<string, 
	                      VflVtkProperty, 
						  VistaProperty::PROPT_STRING>
	      ("INTERPOLATION", SsReflectionType,
		  &VflVtkProperty::GetInterpolation),

	new TVistaPropertyGet<string, 
	                      VflVtkProperty, 
						  VistaProperty::PROPT_STRING>
	      ("REPRESENTATION", SsReflectionType,
		  &VflVtkProperty::GetRepresentation),

	new TVistaPropertyGet<string, 
	                      VflVtkProperty, 
						  VistaProperty::PROPT_STRING>
	      ("CULLING", SsReflectionType,
		  &VflVtkProperty::GetCulling),

	new TVistaPropertyGet<bool, 
	                      VflVtkProperty, 
						  VistaProperty::PROPT_BOOL>
	      ("EDGE_VISIBILITY", SsReflectionType,
		  &VflVtkProperty::GetEdgeVisibility),

    new TVistaProperty3RefGet<float, VflVtkProperty> // implicitely PROPT_LIST
		("COLOR", SsReflectionType,
		&VflVtkProperty::GetColor),

    new TVistaProperty3RefGet<float, VflVtkProperty> // implicitely PROPT_LIST
		("AMBIENT_COLOR", SsReflectionType,
		&VflVtkProperty::GetAmbientColor),

    new TVistaProperty3RefGet<float, VflVtkProperty> // implicitely PROPT_LIST
		("DIFFUSE_COLOR", SsReflectionType,
		&VflVtkProperty::GetDiffuseColor),

    new TVistaProperty3RefGet<float, VflVtkProperty> // implicitely PROPT_LIST
		("SPECULAR_COLOR", SsReflectionType,
		&VflVtkProperty::GetSpecularColor),

    new TVistaProperty3RefGet<float, VflVtkProperty> // implicitely PROPT_LIST
		("EDGE_COLOR", SsReflectionType,
		&VflVtkProperty::GetEdgeColor),

		  NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<double, double,
	                    VflVtkProperty>
		("AMBIENT", SsReflectionType,
		&VflVtkProperty::SetAmbient),

	new TVistaPropertySet<double, double,
	                    VflVtkProperty>
		("DIFFUSE", SsReflectionType,
		&VflVtkProperty::SetDiffuse),

	new TVistaPropertySet<double, double,
	                    VflVtkProperty>
		("SPECULAR", SsReflectionType,
		&VflVtkProperty::SetSpecular),

	new TVistaPropertySet<double, double,
	                    VflVtkProperty>
		("SPECULAR_POWER", SsReflectionType,
		&VflVtkProperty::SetSpecularPower),

	new TVistaPropertySet<double, double,
	                    VflVtkProperty>
		("OPACITY", SsReflectionType,
		&VflVtkProperty::SetOpacity),

		new TVistaPropertySet<const std::string &, string,
	                    VflVtkProperty>
		("INTERPOLATION", SsReflectionType,
		&VflVtkProperty::SetInterpolation),

		new TVistaPropertySet<const std::string &, string,
	                    VflVtkProperty>
		("REPRESENTATION", SsReflectionType,
		&VflVtkProperty::SetRepresentation),

		new TVistaPropertySet<const std::string &, string,
	                    VflVtkProperty>
		("CULLING", SsReflectionType,
		&VflVtkProperty::SetCulling),

	new TVistaPropertySet<bool, bool,
	                    VflVtkProperty>
		("EDGE_VISIBILITY", SsReflectionType,
		&VflVtkProperty::SetEdgeVisibility),

	new TVistaProperty3ValSet<float, VflVtkProperty>
		( "COLOR", SsReflectionType,
		&VflVtkProperty::SetColor),

	new TVistaProperty3ValSet<float, VflVtkProperty>
		( "AMBIENT_COLOR", SsReflectionType,
		&VflVtkProperty::SetAmbientColor),

	new TVistaProperty3ValSet<float, VflVtkProperty>
		( "DIFFUSE_COLOR", SsReflectionType,
		&VflVtkProperty::SetDiffuseColor),

	new TVistaProperty3ValSet<float, VflVtkProperty>
		( "SPECUALR_COLOR", SsReflectionType,
		&VflVtkProperty::SetSpecularColor),

	new TVistaProperty3ValSet<float, VflVtkProperty>
		( "EDGE_COLOR", SsReflectionType,
		&VflVtkProperty::SetEdgeColor),
		NULL
};


bool VflVtkProperty::GetColor(float &r, float &g, float &b) const
{
    double aTemp[3];
    m_pProperty->GetColor(aTemp);

	r = float(aTemp[0]);
	g = float(aTemp[1]);
	b = float(aTemp[2]);

	return true;
}

bool VflVtkProperty::SetColor(float r, float g, float b)
{
	float mr, mg, mb;
	if(!GetColor(mr,mg,mb))
		return false;

	if(mr != r || mg != g || mb != b)
	{
		m_pProperty->SetColor(r,g,b);
		Notify(MSG_COLOR_CHANGE);
		return true;
	}
	return false;
}

bool VflVtkProperty::GetAmbientColor(float &r, float &g, float &b) const
{
    double aTemp[3];
    m_pProperty->GetAmbientColor(aTemp);

	r = float(aTemp[0]);
	g = float(aTemp[1]);
	b = float(aTemp[2]);
	return true;
}
bool VflVtkProperty::SetAmbientColor(float r, float g, float b)
{
	float mr, mg, mb;
	if(!GetColor(mr,mg,mb))
		return false;

	if(mr != r || mg != g || mb != b)
	{
		m_pProperty->SetAmbientColor(r,g,b);
		Notify(MSG_AMBIENT_COLOR_CHANGE);
		return true;
	}
	return false;}

bool VflVtkProperty::GetDiffuseColor(float &r, float &g, float &b) const
{
    double aTemp[3];
    m_pProperty->GetDiffuseColor(aTemp);

	r = float(aTemp[0]);
	g = float(aTemp[1]);
	b = float(aTemp[2]);
	return true;
}
bool VflVtkProperty::SetDiffuseColor(float r, float g, float b)
{
	float mr, mg, mb;
	if(!GetColor(mr,mg,mb))
		return false;

	if(mr != r || mg != g || mb != b)
	{
		m_pProperty->SetDiffuseColor(r,g,b);
		Notify(MSG_DIFFUSE_COLOR_CHANGE);
		return true;
	}
	return false;}

bool VflVtkProperty::GetSpecularColor(float &r, float &g, float &b) const
{
    double aTemp[3];
    m_pProperty->GetSpecularColor(aTemp);

	r = float(aTemp[0]);
	g = float(aTemp[1]);
	b = float(aTemp[2]);
	return true;
}
bool VflVtkProperty::SetSpecularColor(float r, float g, float b)
{
	float mr, mg, mb;
	if(!GetColor(mr,mg,mb))
		return false;

	if(mr != r || mg != g || mb != b)
	{
		m_pProperty->SetSpecularColor(r,g,b);
		Notify(MSG_SPECULAR_COLOR_CHANGE);
		return true;
	}
	return false;
}

bool VflVtkProperty::GetEdgeColor(float &r, float &g, float &b) const
{
    double aTemp[3];
    m_pProperty->GetEdgeColor(aTemp);

	r = float(aTemp[0]);
	g = float(aTemp[1]);
	b = float(aTemp[2]);
	return true;
}
bool VflVtkProperty::SetEdgeColor(float r, float g, float b)
{
	float mr, mg, mb;
	if(!GetColor(mr,mg,mb))
		return false;

	if(mr != r || mg != g || mb != b)
	{
		m_pProperty->SetEdgeColor(r,g,b);
		Notify(MSG_EDGE_COLOR_CHANGE);
		return true;
	}
	return false;
}

double VflVtkProperty::GetAmbient() const
{
	return m_pProperty->GetAmbient();
}

bool   VflVtkProperty::SetAmbient(double dAmbient)
{
	if(GetAmbient() != dAmbient)
	{
		m_pProperty->SetAmbient(dAmbient);
		Notify(MSG_AMBIENT_CHANGE);
		return true;
	}
	return false;
}

double VflVtkProperty::GetDiffuse() const
{
	return m_pProperty->GetDiffuse();
}

bool   VflVtkProperty::SetDiffuse(double dDiffuse)
{
	if(GetDiffuse() != dDiffuse)
	{
		m_pProperty->SetDiffuse(dDiffuse);
		Notify(MSG_DIFFUSE_CHANGE);
		return true;
	}
	return false;
}

double VflVtkProperty::GetSpecular() const
{
	return m_pProperty->GetSpecular();
}

bool   VflVtkProperty::SetSpecular( double dSpecular )
{
	if(GetSpecular() != dSpecular)
	{
		m_pProperty->SetSpecular(dSpecular);
		Notify(MSG_SPECULAR_CHANGE);
		return true;
	}
	return false;
}


double VflVtkProperty::GetSpecularPower() const
{
	return m_pProperty->GetSpecularPower();
}

bool   VflVtkProperty::SetSpecularPower(double dSpecP )
{
	if(GetSpecularPower() != dSpecP)
	{
		m_pProperty->SetSpecularPower(dSpecP);
		Notify(MSG_SPECULAR_POWER_CHANGE);
		return true;
	}
	return false;
}

double VflVtkProperty::GetOpacity() const
{
	return m_pProperty->GetOpacity();
}

bool   VflVtkProperty::SetOpacity(double dOpacity)
{
	if(GetOpacity() != dOpacity)
	{
		m_pProperty->SetOpacity(dOpacity);
		Notify(MSG_OPACITY_CHANGE);
		return true;
	}
	return false;
}


std::string VflVtkProperty::GetInterpolation() const
{
	switch(GetInterpolationId())
	{
	case VTK_FLAT:
		return "FLAT";
	case VTK_GOURAUD:
		return "GOURAUD";
	case VTK_PHONG:
		return "PHONG";
	default:
		return "<none>";
	}
}
bool VflVtkProperty::SetInterpolation( const std::string &sInterpolation)
{
	if(sInterpolation == "FLAT")
		return SetInterpolationId(VTK_FLAT);
	else if(sInterpolation == "GOURAUD")
		return SetInterpolationId(VTK_GOURAUD);
	else if(sInterpolation == "PHONG")
		return SetInterpolationId(VTK_PHONG);
	
	return false;
}

int VflVtkProperty::GetInterpolationId() const
{
	return m_pProperty->GetInterpolation();
}

bool VflVtkProperty::SetInterpolationId( int id )
{
	if(GetInterpolationId() != id)
	{
		m_pProperty->SetInterpolation(id);
		Notify(MSG_INTERPOLATION_CHANGE);
		return true;
	}
	return false;
}

std::string VflVtkProperty::GetRepresentation() const
{
	switch(GetRepresentationId())
	{
	case VTK_SURFACE:
		return "SURFACE";
	case VTK_WIREFRAME:
		return "WIREFRAME";
	case VTK_POINTS:
		return "POINTS";
	default:
		return "<none>";
	}
}

bool VflVtkProperty::SetRepresentation(const std::string &sRepresentation)
{
	if(sRepresentation == "SURFACE")
		return SetRepresentationId(VTK_SURFACE);
	else if(sRepresentation == "WIREFRAME")
		return SetRepresentationId(VTK_WIREFRAME);
	else if(sRepresentation == "POINTS")
		return SetRepresentationId(VTK_POINTS);
	return false;
}

int VflVtkProperty::GetRepresentationId() const
{
	return m_pProperty->GetRepresentation();
}

bool VflVtkProperty::SetRepresentationId( int nRep )
{
	if(GetRepresentationId() != nRep)
	{
		m_pProperty->SetRepresentation(nRep);
		Notify(MSG_REPRESENTATION_CHANGE);
		return true;
	}
	return false;
}

std::string VflVtkProperty::GetCulling() const
{
	switch(GetCullingId())
	{
	case CULL_NONE:
		return "NONE";
	case CULL_BOTH:
		return "BOTH";
	case CULL_FRONT:
		return "FRONTFACE";
	case CULL_BACK:
		return "BACKFACE";
	default:
		return "<none>";
	}
}

bool VflVtkProperty::SetCulling(const std::string &sCulling)
{
	if(sCulling == "BACKFACE")
		return SetCullingId(CULL_BACK);
	else if(sCulling == "FRONTFACE")
		return SetCullingId(CULL_FRONT);
	else if(sCulling == "BOTH")
		return SetCullingId(CULL_BOTH);
	else if(sCulling == "NONE")
		return SetCullingId(CULL_NONE);

	return false;
}

VflVtkProperty::eCullId VflVtkProperty::GetCullingId() const
{
	eCullId nFront = m_pProperty->GetFrontfaceCulling() ? CULL_FRONT : CULL_NONE;
	eCullId nBack  = m_pProperty->GetBackfaceCulling()  ? CULL_BACK  : CULL_NONE;

	return eCullId (nFront + nBack);
}

bool VflVtkProperty::SetCullingId(eCullId nId)
{
	if(GetCullingId() != nId)
	{
		m_pProperty->SetFrontfaceCulling( nId & CULL_FRONT);
		m_pProperty->SetBackfaceCulling(  nId & CULL_BACK );
		Notify(MSG_CULLING_CHANGE);
		return true;
	}
	return false;
}

bool VflVtkProperty::GetEdgeVisibility() const
{
	return m_pProperty->GetEdgeVisibility() == 1 ? true : false;
}

bool VflVtkProperty::SetEdgeVisibility(bool bEdgeV)
{
	if(GetEdgeVisibility() != bEdgeV)
	{
		m_pProperty->SetEdgeVisibility(bEdgeV ? 1 : 0);
		Notify(MSG_EDGE_VISIBILITY_CHANGE);
		return true;
	}
	return false;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflVtkProperty.cpp"                                    */
/*============================================================================*/
