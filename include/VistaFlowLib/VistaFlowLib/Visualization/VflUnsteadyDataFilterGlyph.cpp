/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflUnsteadyDataFilterGlyph.h"

#include "../Tools/VflPropertyLoader.h"

#include <VistaAspects/VistaAspectsUtils.h>

#include <vtkGlyph3D.h>
#include <vtkArrowSource.h>
#include <vtkConeSource.h>
#include <vtkCubeSource.h>
#include <vtkCylinderSource.h>
#include <vtkDiskSource.h>
#include <vtkGlyphSource2D.h>
#include <vtkLineSource.h>
#include <vtkSphereSource.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

static void DestroyFilters(std::vector<vtkGlyph3D *> &refFilters);
static vtkPolyDataAlgorithm *CreateSource(const VistaPropertyList &refProps);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflUnsteadyDataFilterGlyph::VflUnsteadyDataFilterGlyph(vtkGlyph3D *pPrototype)
: VflUnsteadyDataFilter(),
  m_pPrototype(NULL)
{
	m_pPrototype = vtkGlyph3D::New();
	m_pPrototype->GlobalWarningDisplayOff();

	if (pPrototype)
		CopyProperties(m_pPrototype, pPrototype);
}

VflUnsteadyDataFilterGlyph::~VflUnsteadyDataFilterGlyph()
{
	DestroyFilters(m_vecFilters);
	m_oSourceProps.clear();

	m_pPrototype->Delete();
	m_pPrototype = NULL;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateFilters                                               */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyDataFilterGlyph::CreateFilters(int iCount)
{
	DestroyFilters(m_vecFilters);
	m_vecFilters.clear();

	m_vecFilters.resize(iCount);
	for (int i=0; i<iCount; ++i)
	{
		m_vecFilters[i] = vtkGlyph3D::New();
	}

	Synchronize();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetInput                                                    */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyDataFilterGlyph::SetInput(int iIndex, vtkPolyData *pData)
{
	if (0<=iIndex && iIndex<int(m_vecFilters.size()))
	{
#if VTK_MAJOR_VERSION > 5
		m_vecFilters[iIndex]->SetInputData(pData);
#else
		m_vecFilters[iIndex]->SetInput(pData);
#endif
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetInput                                                    */
/*                                                                            */
/*============================================================================*/
vtkPolyData *VflUnsteadyDataFilterGlyph::GetInput(int iIndex) const
{
	if (0<=iIndex && iIndex<int(m_vecFilters.size()))
	{
		return dynamic_cast<vtkPolyData*>(m_vecFilters[iIndex]->GetInput());
	}
	return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetOutput                                                   */
/*                                                                            */
/*============================================================================*/
vtkPolyData *VflUnsteadyDataFilterGlyph::GetOutput(int iIndex)
{
	if (0<=iIndex && iIndex<int(m_vecFilters.size()))
	{
		if (m_bActive)
			return m_vecFilters[iIndex]->GetOutput();
		else
			return dynamic_cast<vtkPolyData*>(m_vecFilters[iIndex]->GetInput());
	}
	return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilterCount                                              */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyDataFilterGlyph::GetFilterCount() const
{
	return static_cast<int>(m_vecFilters.size());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Synchronize                                                 */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyDataFilterGlyph::Synchronize()
{
	for (unsigned int i=0; i<m_vecFilters.size(); ++i)
	{
		CopyProperties(m_vecFilters[i], m_pPrototype);
	}
	Notify(MSG_PARAMETER_CHANGE);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPrototype                                                */
/*                                                                            */
/*============================================================================*/
vtkGlyph3D *VflUnsteadyDataFilterGlyph::GetPrototype() const
{
	return m_pPrototype;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CopyProperties                                              */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyDataFilterGlyph::CopyProperties(vtkGlyph3D *pTarget,
												   vtkGlyph3D *pSource)
{
	pTarget->SetScaling( pSource->GetScaling() );
	pTarget->SetScaleMode( pSource->GetScaleMode() );
	pTarget->SetColorMode( pSource->GetColorMode() );
	pTarget->SetScaleFactor( pSource->GetScaleFactor() );
	pTarget->SetOrient( pSource->GetOrient() );
	pTarget->SetVectorMode( pSource->GetVectorMode() );
	pTarget->SetClamping( pSource->GetClamping() );
	pTarget->SetIndexMode( pSource->GetIndexMode() );
	pTarget->SetGeneratePointIds( pSource->GetGeneratePointIds() );
	pTarget->SetRange( pSource->GetRange() );
	
	//NOTE: This may be kind of a problem, since input management has changed in VTK5
	int nNumberOfSources = pSource->GetNumberOfInputConnections(1);
	for( int i=0; i < nNumberOfSources; ++i )
	{
		//in VTK5 this will add a new source at the end of the current sources array, if needed
#if VTK_MAJOR_VERSION > 5
		pTarget->SetSourceData(i, pSource->GetSource(i) );
#else
		pTarget->SetSource(i, pSource->GetSource(i) );
#endif
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateSources                                               */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyDataFilterGlyph::CreateSources(const VistaPropertyList &refSources)
{
	if (refSources == m_oSourceProps)
	{
#ifdef DEBUG
		vstr::debugi() << " [VflUnsteadyDataFilterGlyph] - no need to modify sources..." << endl;
#endif
		return;		// nothing to do...
	}

#ifdef DEBUG 
	vstr::debugi() << " [VflUnsteadyDataFilterGlyph] - creating "
		<< refSources.size() << " source(s)..." << endl;
#endif

	//Port 1 should contain the glyph sources 
	int iCount = m_pPrototype->GetNumberOfInputConnections(1);
	list<vtkPolyData *> liData;

	// gather and destroy old sources
	for (int i=0; i<iCount; ++i)
	{
		liData.push_back(m_pPrototype->GetSource(i));
#if VTK_MAJOR_VERSION > 5
		m_pPrototype->SetSourceData(i, NULL);
#else
		m_pPrototype->SetSource(i, NULL);
#endif
	}

	list<vtkPolyData *>::iterator dit;
	for (dit=liData.begin(); dit!=liData.end(); ++dit)
	{
		if (*dit)
			(*dit)->Delete();
	}
	liData.clear();

	// create and register new sources
	VistaPropertyList::const_iterator cit;
	list<vtkPolyDataAlgorithm *> liSources;
/*	for (pit=refPropLists.begin(); pit!=refPropLists.end(); ++pit)
	{
		vtkPolyDataAlgorithm *pNew = CreateSource(*pit);
		if (pNew)
		{
			liSources.push_back(pNew);
			m_liSourceProps.push_back(*pit);
		}
	}
*/
	for (cit=refSources.begin(); cit!=refSources.end(); ++cit)
	{
		const VistaPropertyList &refSource = (*cit).second.GetPropertyListConstRef();
		vtkPolyDataAlgorithm *pNew = CreateSource(refSource);
		if (pNew)
		{
			liSources.push_back(pNew);
			m_oSourceProps.SetPropertyListValue((*cit).first, refSource);
		}
	}

	//m_pPrototype->SetNumberOfInputConnections (0, liSources.size());
	list<vtkPolyDataAlgorithm *>::iterator sit;

	int iId=0;
	for (sit=liSources.begin(); sit!=liSources.end(); ++sit, ++iId)
	{
#if VTK_MAJOR_VERSION > 5
		m_pPrototype->SetSourceData(iId, (*sit)->GetOutput());
#else
		m_pPrototype->SetSource(iId, (*sit)->GetOutput());
#endif
//		(*sit)->Delete();	// decrease reference count ;-)
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetProperty                                                 */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyDataFilterGlyph::SetProperty(const VistaProperty &refProp)
{
	string strKey =	VistaConversion::StringToLower( refProp.GetNameForNameable() );

	if (strKey == "active")
	{
		SetActive( VistaConversion::FromString<bool>( refProp.GetValue() ) );
	}
	else if (strKey == "ini_file")
	{
		m_strIniFile = refProp.GetValue();
#ifdef DEBUG
		vstr::debugi() << " [VflUnsteadyDataFilterGlyph] - ini file name set to '"
			<< m_strIniFile << "'..." << endl;
#endif
	}
	else if (strKey == "sources")
	{
		VistaPropertyList oSources;
		// do we have a list of proplists or do we have a simple string?
		if (refProp.GetPropertyType() == VistaProperty::PROPT_PROPERTYLIST)
		{
/*			// yep, we have everything we need...
			//liPropLists = refProp.GetVistaPropertyListLISTValue();
			for(VistaPropertyList::const_iterator cit = refProp.GetPropertyListConstRef().begin();
				cit != refProp.GetPropertyListConstRef().end(); ++cit)
			{
				if((*cit).second.GetPropertyType() == VistaProperty::PROPT_PROPERTYLIST)
					liPropLists.push_back((*cit).second.GetPropertyListConstRef()); // make a copy
				else
				{
					vstr::errp() << "Prop [" << (*cit).second.GetNameForNameable() << "] is not VistaPropertyList, but "
						<< (*cit).second.GetPropertyType() << endl;
				}
			}
*/
			oSources = refProp.GetPropertyListConstRef();
		}
		else
		{
			// well, we don't have a list of proplists, so interpret our contents
			// as a list of strings (i.e. ini sections) and try to retrieve the 
			// necessary parameters from file...
			if (!m_strIniFile.empty())
			{
				vstr::outi() << " [VflUnsteadyDataFilterGlyph] - retrieving source info from file..." << endl;
				list<string> liSections;
				VistaAspectsConversionStuff::ConvertToList(refProp.GetValue(), liSections);
				list<string>::iterator itSection;
				int i=0;
				for (itSection=liSections.begin(); itSection!=liSections.end(); ++itSection)
				{
					VistaPropertyList oProps;
					VflPropertyLoader::FillPropertyList(oProps, *itSection, 
						m_strIniFile, VflPropertyLoader::CONVERSION_KEY_TO_UPPER);

					char buf[256];
					sprintf(buf, "%03d", i);
					oSources.SetPropertyListValue(buf, oProps);
				}
			}
			else
			{
#ifdef DEBUG
				vstr::warnp() << " [VflUnsteadyDataFilterGlyph] unable to retrieve source data..." << endl
							  << "                                          no ini file name given..." << endl;
#endif
			}
		}
		CreateSources(oSources);
	}
	else if (strKey == "scaling")
	{
		bool bValue = VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue());
		m_pPrototype->SetScaling(bValue);
	}
	else if (strKey == "scale_mode")
	{
		string strMode = VistaAspectsConversionStuff::ConvertToLower(refProp.GetValue());

		if (strMode == "scalar")
		{
			m_pPrototype->SetScaleModeToScaleByScalar();
		}
		else if (strMode == "vector")
		{
			m_pPrototype->SetScaleModeToScaleByVector();
		}
		else if (strMode == "vector_components")
		{
			m_pPrototype->SetScaleModeToScaleByVectorComponents();
		}
		else if (strMode == "off")
		{
			m_pPrototype->SetScaleModeToDataScalingOff();
		}
		else 
		{
			return PROP_INVALID_VALUE;
		}
	}
	else if (strKey == "color_mode")
	{
		string strMode = VistaConversion::StringToLower(refProp.GetValue());

		if (strMode == "scale")
		{
			m_pPrototype->SetColorModeToColorByScale();
		}
		else if (strMode == "scalar")
		{
			m_pPrototype->SetColorModeToColorByScalar();
		}
		else if (strMode == "vector")
		{
			m_pPrototype->SetColorModeToColorByVector();
		}
		else 
		{
			return PROP_INVALID_VALUE;
		}
	}
	else if (strKey == "scale_factor")
	{
		float fValue = VistaAspectsConversionStuff::ConvertToDouble(refProp.GetValue());
		m_pPrototype->SetScaleFactor(fValue);
	}
	else if (strKey == "range")
	{
		float min, max;
		if (sscanf(refProp.GetValue().c_str(), "%f, %f", &min, &max) == 2)
		{
			m_pPrototype->SetRange(min, max);
		}
	}
	else if (strKey == "orient")
	{
		bool bValue = VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue());
		m_pPrototype->SetOrient(bValue);
	}
	else if (strKey == "clamping")
	{
		bool bValue = VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue());
		m_pPrototype->SetClamping(bValue);
	}
	else if (strKey == "vector_mode")
	{
		string strMode = VistaAspectsConversionStuff::ConvertToLower(refProp.GetValue());

		if (strMode == "vector")
		{
			m_pPrototype->SetVectorModeToUseVector();
		}
		else if (strMode == "normal")
		{
			m_pPrototype->SetVectorModeToUseNormal();
		}
		else if (strMode == "off")
		{
			m_pPrototype->SetVectorModeToVectorRotationOff();
		}
		else 
		{
			return PROP_INVALID_VALUE;
		}
	}
	else if (strKey == "generate_point_ids")
	{
		bool bValue = VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue());
		m_pPrototype->SetGeneratePointIds(bValue);
	}
	else if (strKey == "point_ids_name")
	{
		m_pPrototype->SetPointIdsName(refProp.GetValue().c_str());
	}
	else
	{
		return VflUnsteadyDataFilter::SetProperty(refProp);
	}

	Synchronize();
	
	return PROP_OK;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperty                                                 */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyDataFilterGlyph::GetProperty(VistaProperty &refProp)
{
	string strKey =
		VistaAspectsConversionStuff::ConvertToLower(refProp.GetNameForNameable());

	if (strKey == "active")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(GetActive()));
	}
	else if (strKey == "sources")
	{
		refProp.SetPropertyListValue(m_oSourceProps);
/*		VistaPropertyList rProp;
		int i=0;
		for(list<VistaPropertyList>::const_iterator cit = m_liSourceProps.begin();
			cit != m_liSourceProps.end(); ++cit)
		{
			rProp.SetPropertyListValue(VistaAspectsConversionStuff::ConvertToString(i++), *cit);
		}
		refProp.SetPropertyListValue(rProp);
*/
	}
	else if (strKey == "scaling")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetScaling()));
	}
	else if (strKey == "scale_mode")
	{
		int iMode = m_pPrototype->GetScaleMode();
		string strMode;
		switch (iMode)
		{
		case VTK_SCALE_BY_SCALAR:
			strMode = "SCALAR";
			break;
		case VTK_SCALE_BY_VECTOR:
			strMode = "VECTOR";
			break;
		case VTK_SCALE_BY_VECTORCOMPONENTS:
			strMode = "VECTOR_COMPONENTS";
			break;
		case VTK_DATA_SCALING_OFF:
			strMode = "OFF";
			break;
		default:
			strMode = "UNKNOWN";
		}
		refProp.SetValue(strMode);
	}
	else if (strKey == "color_mode")
	{
		int iMode = m_pPrototype->GetColorMode();
		string strMode;
		switch (iMode)
		{
		case VTK_COLOR_BY_SCALE:
			strMode = "SCALE";
			break;
		case VTK_COLOR_BY_SCALAR:
			strMode = "SCALAR";
			break;
		case VTK_COLOR_BY_VECTOR:
			strMode = "VECTOR";
			break;
		default:
			strMode = "UNKNOWN";
		}
		refProp.SetValue(strMode);
	}
	else if (strKey == "scale_factor")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetScaleFactor()));
	}
	else if (strKey == "range")
	{
		char buf[256];
        double fRange[2];
		m_pPrototype->GetRange(fRange);

		sprintf(buf, "%f, %f", fRange[0], fRange[1]);
		refProp.SetValue(buf);
	}
	else if (strKey == "orient")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetOrient()));
	}
	else if (strKey == "clamping")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetClamping()));
	}
	else if (strKey == "vector_mode")
	{
		int iMode = m_pPrototype->GetVectorMode();
		string strMode;
		switch (iMode)
		{
		case VTK_USE_VECTOR:
			strMode = "VECTOR";
			break;
		case VTK_USE_NORMAL:
			strMode = "NORMAL";
			break;
		case VTK_VECTOR_ROTATION_OFF:
			strMode = "OFF";
			break;
		default:
			strMode = "UNKNOWN";
		}
		refProp.SetValue(strMode);
	}
	else if (strKey == "generate_point_ids")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetGeneratePointIds()));
	}
	else if (strKey == "point_ids_name")
	{
		refProp.SetValue(m_pPrototype->GetPointIdsName());
	}
	else if (strKey == "type")
	{
		refProp.SetValue("VFL_UNSTEADY_DATA_FILTER_GLYPH");
	}
	else
	{
		return VflUnsteadyDataFilter::GetProperty(refProp);
	}

	return PROP_OK;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPropertySymbolList                                       */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyDataFilterGlyph::GetPropertySymbolList(std::list<std::string> &rStorageList)
{
  

	rStorageList.push_back("ACTIVE");
	rStorageList.push_back("SOURCES");
	rStorageList.push_back("NUMBER_OF_SOURCES");
	rStorageList.push_back("SCALING");
	rStorageList.push_back("SCALE_MODE");
	rStorageList.push_back("COLOR_MODE");
	rStorageList.push_back("SCALE_FACTOR");
	rStorageList.push_back("RANGE");
	rStorageList.push_back("ORIENT");
	rStorageList.push_back("CLAMPING");
	rStorageList.push_back("GENERATE_POINT_IDS");
	rStorageList.push_back("POINT_IDS_NAME");
	rStorageList.push_back("TYPE");

    return static_cast<int>(rStorageList.size());
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DestroyFilters                                              */
/*                                                                            */
/*============================================================================*/

void DestroyFilters(vector<vtkGlyph3D *> &vecFilters)
{
	for (unsigned int i=0; i<vecFilters.size(); ++i)
	{
		vecFilters[i]->Delete();
		vecFilters[i] = NULL;
	}
	vecFilters.clear();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateSource                                                */
/*                                                                            */
/*============================================================================*/
static vtkPolyDataAlgorithm *CreateSource(const VistaPropertyList &refProps)
{
	vtkPolyDataAlgorithm *pResult = NULL;

	string strType;
	if( refProps.GetValue( "TYPE", strType ) == false )
		return NULL; // @DRCLUSTER@ warning?

	strType = VistaConversion::StringToLower( strType );

	if (strType == "2d_glyph_source")
	{
		vtkGlyphSource2D *pSource = vtkGlyphSource2D::New();
		pSource->SetFilled(false);
		pSource->SetGlyphTypeToArrow();

		double a3dValues[3];
		double dValue;
		bool bValue;
		std::string strValue;

		if( refProps.GetValueAsArray<3>( "CENTER", a3dValues ) )
		{
			pSource->SetCenter( a3dValues );
		}
		
		if( refProps.GetValue("SCALE", dValue ) )
			pSource->SetScale( dValue );
		if( refProps.GetValue("SCALE2", dValue ) )
			pSource->SetScale2( dValue );
		if( refProps.GetValueAsArray<3>( "COLOR", a3dValues ) )
			pSource->SetColor( a3dValues );
		if( refProps.GetValue( "FILLED", bValue ) )
			pSource->SetFilled( bValue );
		if( refProps.GetValue( "DASH", bValue ) )
			pSource->SetDash( bValue );
		if( refProps.GetValue( "CROSS", bValue ) )
			pSource->SetCross( bValue );
		if( refProps.GetValue( "ROTATION_ANGLE", dValue ) )
			pSource->SetRotationAngle( dValue );
		if( refProps.GetValue( "GLYPH_TYPE", strValue ) )
		{
			string strGlyph = VistaConversion::StringToLower( strValue );
			if (strGlyph=="none")
				pSource->SetGlyphTypeToNone();
			else if (strGlyph=="vertex")
				pSource->SetGlyphTypeToVertex();
			else if (strGlyph=="dash")
				pSource->SetGlyphTypeToDash();
			else if (strGlyph=="cross")
				pSource->SetGlyphTypeToCross();
			else if (strGlyph=="thick_cross")
				pSource->SetGlyphTypeToThickCross();
			else if (strGlyph=="triangle")
				pSource->SetGlyphTypeToTriangle();
			else if (strGlyph=="square")
				pSource->SetGlyphTypeToSquare();
			else if (strGlyph=="circle")
				pSource->SetGlyphTypeToCircle();
			else if (strGlyph=="diamond")
				pSource->SetGlyphTypeToDiamond();
			else if (strGlyph=="arrow")
				pSource->SetGlyphTypeToArrow();
			else if (strGlyph=="thick_arrow")
				pSource->SetGlyphTypeToThickArrow();
			else if (strGlyph=="hooked_arrow")
				pSource->SetGlyphTypeToHookedArrow();
		}

		pResult = pSource;
	}
	else if (strType == "arrow_source")
	{
		vtkArrowSource *pSource = vtkArrowSource::New();
		pSource->SetTipResolution(3);
		pSource->SetShaftResolution(3);

		double nDouble;
		int nInt;

		if (refProps.GetValue("TIP_LENGTH", nDouble))
			pSource->SetTipLength(nDouble);
		if (refProps.GetValue("TIP_RADIUS", nDouble))
			pSource->SetTipRadius(nDouble);
		if (refProps.GetValue("TIP_RESOLUTION", nInt))
			pSource->SetTipResolution(nInt);
		if (refProps.GetValue("SHAFT_RADIUS", nDouble))
			pSource->SetShaftRadius(nDouble);
		if (refProps.GetValue("SHAFT_RESOLUTION", nInt))
			pSource->SetShaftResolution(nInt);

		pResult = pSource;
	}
	else if (strType == "cone_source")
	{
		vtkConeSource *pSource = vtkConeSource::New();
		pSource->SetResolution(3);

		double nDouble;
		int nInt;
		bool bBool;
		double a3dBuffer[3];

		if (refProps.GetValue("HEIGHT",nDouble))
			pSource->SetHeight(nDouble);
		if (refProps.GetValue("RADIUS", nDouble))
			pSource->SetRadius(nDouble);
		if (refProps.GetValue("RESOLUTION", nInt ))
			pSource->SetResolution(nInt);
		if (refProps.GetValueAsArray<3>("CENTER", a3dBuffer) )
			pSource->SetCenter(a3dBuffer);
		if (refProps.GetValueAsArray<3>("DIRECTION", a3dBuffer))
			pSource->SetDirection(a3dBuffer);
		if (refProps.GetValue("ANGLE", nDouble))
			pSource->SetAngle(nDouble);
		if (refProps.GetValue("CAPPING", bBool))
			pSource->SetCapping(bBool);

		pResult = pSource;
	}
	else if (strType == "cube_source")
	{
		vtkCubeSource *pSource = vtkCubeSource::New();

		double nDouble;
		double a3dBuffer[3];

		if (refProps.GetValue("X_LENGTH", nDouble))
			pSource->SetXLength(nDouble);
		if (refProps.GetValue("Y_LENGTH", nDouble))
			pSource->SetYLength(nDouble);
		if (refProps.GetValue("Z_LENGTH", nDouble))
			pSource->SetZLength(nDouble);
		if (refProps.GetValueAsArray<3>("CENTER", a3dBuffer))
			pSource->SetCenter(a3dBuffer);
		if (refProps.GetValueAsArray<3>("SIZE", a3dBuffer))
		{
			pSource->SetXLength(a3dBuffer[0]);
			pSource->SetYLength(a3dBuffer[1]);
			pSource->SetZLength(a3dBuffer[2]);
		}
 
		pResult = pSource;
	}
	else if (strType == "cylinder_source")
	{
		vtkCylinderSource *pSource = vtkCylinderSource::New();
		pSource->SetResolution(3);

		double nDouble;
		int nInt;
		double a3dBuffer[3];
		bool bBool;

		if (refProps.GetValue("HEIGHT", nDouble))
			pSource->SetHeight(nDouble);
		if (refProps.GetValue("RADIUS", nDouble))
			pSource->SetRadius(nDouble);
		if (refProps.GetValue("RESOLUTION", nInt))
			pSource->SetResolution(nInt);
		if (refProps.GetValueAsArray<3>("CENTER", a3dBuffer))
		{
			pSource->SetCenter(a3dBuffer);
		}
		if (refProps.GetValue("CAPPING", bBool))
			pSource->SetCapping(bBool);

		pResult = pSource;
	}
	else if (strType == "disk_source")
	{
		vtkDiskSource *pSource = vtkDiskSource::New();
		pSource->SetCircumferentialResolution(6);
		pSource->SetInnerRadius(0);

		double nDouble;
		int nInt;

		if (refProps.GetValue("INNER_RADIUS", nDouble))
			pSource->SetInnerRadius(nDouble);
		if (refProps.GetValue("OUTER_RADIUS", nDouble))
			pSource->SetOuterRadius(nDouble);
		if (refProps.GetValue("RADIAL_RESOLUTION", nInt))
			pSource->SetRadialResolution(nInt);
		if (refProps.GetValue("CIRCUMFERENTIAL_RESOLUTION", nInt))
			pSource->SetCircumferentialResolution(nInt);

		pResult = pSource;
	}
	else if (strType == "line_source")
	{
		vtkLineSource *pSource = vtkLineSource::New();

		int nInt;
		double a3dBuffer[3];

		if (refProps.GetValue("RESOLUTION", nInt))
			pSource->SetResolution(nInt);
		if (refProps.GetValueAsArray<3>("POINT_1", a3dBuffer))
		{
			pSource->SetPoint1(a3dBuffer);
		}
		if (refProps.GetValueAsArray<3>("POINT_2",a3dBuffer))
		{
			pSource->SetPoint2(a3dBuffer);
		}

		pResult = pSource;
	}
	else if (strType == "sphere_source")
	{
		vtkSphereSource *pSource = vtkSphereSource::New();
		pSource->SetThetaResolution(3);
		pSource->SetPhiResolution(3);

		double nDouble;
		int nInt;
		bool bBool;
		double a3dBuffer[3];

		if (refProps.GetValue("RADIUS", nInt))
			pSource->SetRadius(nInt);
		if (refProps.GetValueAsArray<3>("CENTER", a3dBuffer))
			pSource->SetCenter(a3dBuffer);
		if (refProps.GetValue("THETA_RESOLUTION", nInt))
			pSource->SetThetaResolution(nInt);
		if (refProps.GetValue("PHI_RESOLUTION", nInt))
			pSource->SetPhiResolution(nInt);
		if (refProps.GetValue("START_THETA", nDouble))
			pSource->SetStartTheta(nDouble);
		if (refProps.GetValue("END_THETA", nDouble))
			pSource->SetEndTheta(nDouble);
		if (refProps.GetValue("START_PHI", nDouble))
			pSource->SetStartPhi(nDouble);
		if (refProps.GetValue("END_PHI", nDouble))
			pSource->SetEndPhi(nDouble);
		if (refProps.GetValue("LAT_LONG_TESSELATION", bBool))
			pSource->SetLatLongTessellation(bBool);

		pResult = pSource;
	}

	return pResult;
}

/*============================================================================*/
/*  END OF FILE "VflUnsteadyDataFilter.cpp"                                   */
/*============================================================================*/
