/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLUNSTEADYDATAFILTER_H
#define _VFLUNSTEADYDATAFILTER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaReflectionable.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkPolyData;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Parent class for unsteady VTK filters to be used in conjunction
 * with VflVisVtkGeometry.
 */
class VISTAFLOWLIBAPI VflUnsteadyDataFilter : public IVistaReflectionable
{
public:
	enum EFilterChangeMsg
	{
		MSG_PARAMETER_CHANGE = IVistaObserveable::MSG_LAST,	// just tell everyone, that (some of) our parameters changed...
		MSG_NEED_REWIRE,									// make sure, we get re-wired (i.e. we've been (de-)activated...
		MSG_LAST
	};

	virtual ~VflUnsteadyDataFilter();

	// filter state management (to be extended by child classes)
	virtual bool SetActive(bool bActive);
	virtual bool GetActive() const;

	// filter and pipeline management
	virtual void CreateFilters(int iCount) = 0;
	virtual void SetInput(int iIndex, vtkPolyData *pData) = 0;
	virtual vtkPolyData *GetInput(int iIndex) const = 0;
	virtual vtkPolyData *GetOutput(int iIndex) = 0;
	virtual int GetFilterCount() const = 0;
	virtual void Synchronize() = 0;

	REFL_INLINEIMP(VflUnsteadyDataFilter,IVistaReflectionable)
protected:
	VflUnsteadyDataFilter();

	bool	m_bActive;
};

#endif // _VFLUNSTEADYDATAFILTER_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/
