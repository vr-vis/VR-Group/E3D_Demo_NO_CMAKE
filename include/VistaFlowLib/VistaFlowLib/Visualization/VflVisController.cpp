/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflVisController.h"

#include "VflRenderNode.h"
#include <VistaInterProcComm/Concurrency/VistaMutex.h>
#include <VistaBase/VistaVectorMath.h>

#include <limits>
#include <algorithm>


/*============================================================================*/
/* CON-/DESTRUCTORS                                                           */
/*============================================================================*/
VflVisController::VflVisController(VistaSystem *pVistaSystem)
:	m_pVistaSystem(pVistaSystem)
,   m_pListMutex(new VistaMutex())
{ }

VflVisController::~VflVisController()
{
	delete m_pListMutex;
}


/*============================================================================*/
/* IVISTANAMEABLE INTERFACE                                                   */
/*============================================================================*/
void VflVisController::SetNameForNameable(const std::string &strName)
{
	m_strName = strName;
}

std::string VflVisController::GetNameForNameable() const
{
	return m_strName;
}


/*============================================================================*/
/* PUBLIC INTERFACE                                                           */
/*============================================================================*/
void VflVisController::Update(double dTimeStamp)
{
	VistaMutexLock oAutoLock(*m_pListMutex);

	std::list<VflRenderNode*>::iterator it	= m_liRenderNodes.begin();
	for(; it != m_liRenderNodes.end(); ++it)
		(*it)->Update(dTimeStamp);
}


bool VflVisController::AddRenderNode(VflRenderNode *pRenderNode)
{
	VistaMutexLock oAutoLock(*m_pListMutex);

	std::list<VflRenderNode*>::iterator it = m_liRenderNodes.begin();
	for( ; it != m_liRenderNodes.end(); ++it)
	{
		if((*it) == pRenderNode)
			return false;
	}

	if(pRenderNode->GetVisController())
		pRenderNode->GetVisController()->RemoveRenderNode(pRenderNode);

	pRenderNode->SetVisController(this);
	pRenderNode->Init();
	m_liRenderNodes.push_back(pRenderNode);
	return true;
}

bool VflVisController::RemoveRenderNode(VflRenderNode *pRenderNode)
{
	VistaMutexLock oAutoLock(*m_pListMutex);

	std::list<VflRenderNode*>::iterator it = m_liRenderNodes.begin();
	for(; it != m_liRenderNodes.end(); ++it)
	{
		if((*it) == pRenderNode)
		{
			pRenderNode->SetVisController(NULL);
			m_liRenderNodes.erase(it);
			return true;
		}
	}

	return false;
}

VflRenderNode* const VflVisController::GetRenderNodeByName(const std::string &strName) const
{
	if(strName.empty())
		return NULL;

	VistaMutexLock oAutoLock(*m_pListMutex);

	for(std::list<VflRenderNode*>::const_iterator itNodes = m_liRenderNodes.begin();
			itNodes != m_liRenderNodes.end(); ++itNodes)
	{
		VflRenderNode* pNode = *itNodes;
		if(pNode && pNode->GetNameForNameable() == strName)
		{
			return pNode;
		}
	}
	return NULL;
}

void VflVisController::TellGlobalViewPosition(
	const VistaVector3D &v3ViewPosGlobal)
{
	VistaMutexLock oAutoLock(*m_pListMutex);
	std::list<VflRenderNode*>::iterator it = m_liRenderNodes.begin();

	while(it != m_liRenderNodes.end())
	{
		(*it)->TellGlobalViewPosition(v3ViewPosGlobal);

		++it;
	}
}

void  VflVisController::TellGlobalViewOrientation(
	const VistaQuaternion &qViewOriGlobal)
{
	VistaMutexLock oAutoLock(*m_pListMutex);
	std::list<VflRenderNode*>::iterator it = m_liRenderNodes.begin();

	while(it != m_liRenderNodes.end())
	{
		(*it)->TellGlobalViewOrientation(qViewOriGlobal);

		++it;
	}
}

void VflVisController::TellGlobalLightDirection(
	const VistaVector3D &v3LightDirGlobal)
{
	VistaMutexLock oAutoLock(*m_pListMutex);
	std::list<VflRenderNode*>::iterator it = m_liRenderNodes.begin();

	while(it != m_liRenderNodes.end())
	{
		(*it)->TellGlobalLightDirection(v3LightDirGlobal);

		++it;
	}
}


bool VflVisController::GetVisBounds(VistaVector3D &v3Min,
									 VistaVector3D &v3Max)
{
	VistaMutexLock oAutoLock(*m_pListMutex);
	std::list<VflRenderNode*>::iterator it = m_liRenderNodes.begin();

	v3Min[0] = v3Min[1] = v3Min[2] = std::numeric_limits<float>::max();
	v3Max[0] = v3Max[1] = v3Max[2] = -std::numeric_limits<float>::max();

	VistaVector3D v3MinNew, v3MaxNew;

	while(it != m_liRenderNodes.end())
	{
		(*it)->GetVisBounds(v3MinNew, v3MaxNew);

		for(int i=0; i<3; ++i)
		{
			v3Min[i] = std::min(v3Min[i], v3MinNew[i]);
			v3Max[i] = std::max(v3Max[i], v3MaxNew[i]);
		}

		++it;
	}

	return true;
}


VistaSystem* VflVisController::GetVistaSystem() const
{
	return m_pVistaSystem;
}
