/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  BaseAppTutorial.cpp	         RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA FlowLib Tutorials      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                        Rheinisch-Westfaelische      */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "VflLookupTable.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaBase/VistaMathBasics.h>

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VflLookupTable::VflLookupTable()
{
	m_aTableRange[0] = 0.0f;
	m_aTableRange[1] = 1.0f;

	m_aHueRange[0] = 0.6667f;
	m_aHueRange[1] = 0.0000f;

	m_aSatRange[0] = 1.0f;
	m_aSatRange[1] = 1.0f;

	m_aValRange[0] = 1.0f;
	m_aValRange[1] = 1.0f;

	m_aAlphaRange[0] = 1.0f;
	m_aAlphaRange[1] = 1.0f;

	m_vecValues.resize( 256 );
	UpdateTableValues();
}

VflLookupTable::~VflLookupTable()
{}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
#pragma region Getter

bool VflLookupTable::GetTableRange( float& fMin, float& fMax ) const
{
	fMin = m_aTableRange[0];
	fMax = m_aTableRange[1];
	return true;
}

bool VflLookupTable::GetHueRange( float& fMin, float& fMax ) const
{
	fMin = m_aHueRange[0];
	fMax = m_aHueRange[1];
	return true;
}
bool VflLookupTable::GetSaturationRange( float& fMin, float& fMax ) const
{
	fMin = m_aSatRange[0];
	fMax = m_aSatRange[1];
	return true;
}
bool VflLookupTable::GetValueRange(float& fMin, float& fMax) const
{
	fMin = m_aValRange[0];
	fMax = m_aValRange[1];
	return true;
}
bool VflLookupTable::GetAlphaRange(float& fMin, float& fMax) const
{
	fMin = m_aAlphaRange[0];
	fMax = m_aAlphaRange[1];
	return true;
}

unsigned int VflLookupTable::GetValueCount() const
{
	return static_cast<unsigned int>( m_vecValues.size() );
}

bool VflLookupTable::GetTableValues( std::vector<VistaColor>& vecValues ) const
{
	vecValues = m_vecValues;
	return true;
}

std::vector<float> VflLookupTable::GetTableValues() const
{
	std::vector<float> vecValues;
	vecValues.resize( m_vecValues.size()*4 );

	for( size_t n=0; n <m_vecValues.size(); ++n)
	{
		m_vecValues[n].GetValues( &vecValues[4*n], VistaColor::RGBA );
	}

	return vecValues;
}

bool VflLookupTable::GetTableValue( unsigned int nIndex, VistaColor& rColor ) const
{
	if( nIndex >= GetValueCount() )
		return false;

	rColor = m_vecValues[nIndex];
	return true;
}

VistaColor VflLookupTable::GetColor( float fScalar ) const
{
	float fTexCoord = ( fScalar          - m_aTableRange[0] )
	                / ( m_aTableRange[1] - m_aTableRange[0] );

	fTexCoord = Vista::Clamp( fTexCoord, 0.0f, 1.0f );

	float dIndex  = fTexCoord * ( GetValueCount() - 1 );

	int   iFloor  = static_cast<int>( floor( dIndex ) );
	int   iCeil   = static_cast<int>( ceil(  dIndex ) );
	float iFract  = dIndex-iFloor;

	const VistaColor& rFirst  = m_vecValues[ iFloor ];
	const VistaColor& rSecond = m_vecValues[ iCeil  ];

	return rFirst.Mix( rSecond, iFract );
}

#pragma endregion Getter
/******************************************************************************/
#pragma region Setter

bool VflLookupTable::SetTableRange( float fMin, float fMax )
{
	if( fMin==m_aTableRange[0] && fMax == m_aTableRange[1] )
		return false;// Noting changed

	m_aTableRange[0] = fMin;
	m_aTableRange[1] = fMax;

	Notify( MSG_TABLE_RANGE_CHANGED );

	return true;
}

bool VflLookupTable::SetHueRange( float fMin, float fMax )
{
	if( fMin==m_aHueRange[0] && fMax==m_aHueRange[1] )
		return false;

	m_aHueRange[0] = fMin;
	m_aHueRange[1] = fMax;

	Notify( MSG_HUE_RANGE_CHANGED );

	UpdateTableValues();
	return true;
}

bool VflLookupTable::SetSaturationRange( float fMin, float fMax )
{
	if( fMin==m_aSatRange[0] && fMax==m_aSatRange[1] )
		return false;

	m_aSatRange[0] = fMin;
	m_aSatRange[1] = fMax;

	Notify( MSG_SATURATION_RANGE_CHANGED );

	UpdateTableValues();
	return true;
}

bool VflLookupTable::SetValueRange( float fMin, float fMax )
{
	if( fMin==m_aValRange[0] && fMax==m_aValRange[1] )
		return false;

	m_aValRange[0] = fMin;
	m_aValRange[1] = fMax;

	Notify( MSG_VALUE_RANGE_CHANGED );

	UpdateTableValues();
	return true;
}

bool VflLookupTable::SetAlphaRange( float fMin, float fMax )
{
	if( fMin==m_aAlphaRange[0] && fMax==m_aAlphaRange[1] )
		return false;

	m_aAlphaRange[0] = fMin;
	m_aAlphaRange[1] = fMax;

	Notify( MSG_ALPHA_RANGE_CHANGED );

	UpdateTableValues();
	return true;
}
bool VflLookupTable::SetValueCount( unsigned int iNumColors )
{

	if( iNumColors == m_vecValues.size() )
		return false; // Noting changed

	if ( iNumColors < 1 )
		return false; 

	m_vecValues.resize( iNumColors );

	Notify( MSG_NUMBER_OF_COLORS_CHANGED );

	UpdateTableValues();
	return true;
}

bool VflLookupTable::SetTableValues( const std::vector<VistaColor> &vecValues )
{
	if( vecValues.size() < 1 )
		return false; // we need at least one Color

	size_t nOldSize = m_vecValues.size();

	m_vecValues = vecValues;

	if( nOldSize != m_vecValues.size() )
		Notify( MSG_NUMBER_OF_COLORS_CHANGED );

	Notify( MSG_VALUES_CHANGED );
	return true;
}

bool VflLookupTable::SetTableValues( const std::vector<float>& vecValues )
{
	if( vecValues.size() < 4 ||   // we need at least one Color
		vecValues.size()%4 != 0 ) // all Colors consist of four float values
		return false; 

	size_t nNumValues = vecValues.size()/4;

	if( m_vecValues.size() != nNumValues )
	{
		m_vecValues.resize(nNumValues);
		Notify( MSG_NUMBER_OF_COLORS_CHANGED );
	}

	for( size_t n=0; n <nNumValues; ++n)
		m_vecValues[n].SetValues( &vecValues[4*n], VistaColor::RGBA );

	Notify( MSG_VALUES_CHANGED );
	return true;
}

bool VflLookupTable::SetTableValue( unsigned int nIndex, const VistaColor& rColor )
{
	if( nIndex >= GetValueCount() )
		return false;

	if( m_vecValues[nIndex] == rColor )
		return false; // Noting changed

	m_vecValues[nIndex] = rColor;
	
	Notify( MSG_VALUES_CHANGED );
	return true;
}

#pragma endregion Setter
/******************************************************************************/
#pragma region Reflectionable API
REFL_IMPLEMENT_FULL( IVflLookupTable, IVistaReflectionable );

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaProperty2RefGet< float, IVflLookupTable >(
			"TABLE_RANGE", SsReflectionName,
			&IVflLookupTable::GetTableRange
		),
	new TVistaProperty2RefGet< float, IVflLookupTable >(
			"HUE_RANGE", SsReflectionName,
			&IVflLookupTable::GetHueRange
		),
	new TVistaProperty2RefGet< float, IVflLookupTable >(
			"SATURATION_RANGE", SsReflectionName,
			&IVflLookupTable::GetSaturationRange
		),
	new TVistaProperty2RefGet< float, IVflLookupTable >(
			"VALUE_RANGE", SsReflectionName,
			&IVflLookupTable::GetValueRange
		),
	new TVistaProperty2RefGet< float, IVflLookupTable >(
			"ALPHA_RANGE", SsReflectionName,
			&IVflLookupTable::GetAlphaRange
		),
	new TVistaPropertyGet< unsigned int, IVflLookupTable, VistaProperty::PROPT_DOUBLE >(
			"VALUE_COUNT", SsReflectionName, 
			&IVflLookupTable::GetValueCount
		),
	new TVistaPropertyGet< std::vector<float>, IVflLookupTable, VistaProperty::PROPT_LIST>(
			"TABLE_VALUES", SsReflectionName,
			&IVflLookupTable::GetTableValues
		),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaProperty2ValSet< float, IVflLookupTable >( 
			"TABLE_RANGE", SsReflectionName,
			&IVflLookupTable::SetTableRange
		),
	new TVistaProperty2ValSet< float, IVflLookupTable >( 
			"HUE_RANGE", SsReflectionName,
			&IVflLookupTable::SetHueRange
		),
	new TVistaProperty2ValSet< float, IVflLookupTable >( 
			"SATURATION_RANGE", SsReflectionName,
			&IVflLookupTable::SetSaturationRange
		),
	new TVistaProperty2ValSet< float, IVflLookupTable >( 
			"VALUE_RANGE", SsReflectionName,
			&IVflLookupTable::SetValueRange
		),
	new TVistaProperty2ValSet< float, IVflLookupTable >( 
			"ALPHA_RANGE", SsReflectionName,
			&IVflLookupTable::SetAlphaRange
		),
	new TVistaPropertySet< unsigned int, unsigned int, IVflLookupTable >(
			"VALUE_COUNT", SsReflectionName,
			&IVflLookupTable::SetValueCount
		),
	new TVistaPropertySet< 
			const std::vector<float> &,
			std::vector<float>, 
			IVflLookupTable 
		>(
			"TABLE_VALUES", SsReflectionName,
			&IVflLookupTable::SetTableValues
		),	
	NULL
};

#pragma endregion Reflectionable API

/******************************************************************************/
void VflLookupTable::UpdateTableValues()
{
	size_t nNumberColors = m_vecValues.size();

	for( size_t n=0; n<nNumberColors; ++n )
	{ 
		float f = float( n )/float( nNumberColors-1 );

		float H = f*m_aHueRange[1]   + (1.0f-f)*m_aHueRange[0];
		float S = f*m_aSatRange[1]   + (1.0f-f)*m_aSatRange[0];
		float V = f*m_aValRange[1]   + (1.0f-f)*m_aValRange[0];
		float A = f*m_aAlphaRange[1] + (1.0f-f)*m_aAlphaRange[0];

		m_vecValues[n] = VistaColor( H, S, V, A, VistaColor::HSVA );
	}
	
	Notify( MSG_VALUES_CHANGED );
}
/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

