/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVISVTKGEOMETRY_H
#define _VFLVISVTKGEOMETRY_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <iostream>
#include <string>
#include "VflVisObject.h"
#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaVisExt/Data/VveVtkData.h>
#include <VistaVisExt/Tools/VtkObjectStack.h>

#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkProperty;
class vtkLookupTable;
class vtkPolyData;
class vtkActor;
class vtkTexture;
class VflUnsteadyDataFilter;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * PLEASE DISREGARD THE FOLLOWING INFORMATION, AS IT IS NOT BROUGHT UP-TO-DATE
 * YET AFTER DOING SOME CONSIDERABLE CHANGES REGARDING DATA STRUCTURES!!!
 *
 * VflVisVtkGeometry is supposed to be the parent class of everything that 
 * draws vtk geometry. This class can easily be used for the visualization
 * of the data inside a VflUnsteadyData object as follows. For some notes regarding 
 * the derivation of new classes from this one, see below.
 * 1. Create and fill a VveUnsteadyVtkPolyData object d, including a 
 *     valid time mapper, and tell VflVisVtkGeometry about it through SetData(). 
 *     "Valid" means, that for any index i out of [0, d->GetNumberOfLevels()-1]
 *     d->GetLevelDataByLevelIndex(i) *must* return a valid VflVisData<TRawVisData> object,
 *     not a NULL pointer!!! 
 *     Tell VflVisVtkGeometry about it via SetData().
 * 2. (OPTIONAL) Create a vtkPropery object to be used for rendering and
 *     set it via SetProperty(). If you don't provide your own one, a
 *     default object will be created automatically, which can be retrieved
 *     through GetProperty().
 * 3. (OPTIONAL) Create a vtkLookupTable object to be used for rendering and
 *     set it via SetLookupTable(). If you don't provide your own one, a
 *     default object will be created automatically, which can be retrieved
 *     through GetLookupTable().
 * 4. Call Init() with a sensible value for the iRegistration parameter.
 *     This will register the VflVisVtkGeometry object with the visualization
 *     controller and set up all necessary objects and pipelines to render
 *     the given data.
 *
 * If you derive your own classes from this class, you better fill in the 
 * corresponding data according to the following rules:
 *
 * 1. Data management is done through a VflUnsteadyData object. This one 
 *     *has* to have an appropriate time mapper and a data vector of the 
 *     correct length. VflVisVtkGeometry relies on the information given 
 *     by VflUnsteadyObject and does *not* check any returned values
 *     for validity.
 * 2. If the data inside the VflUnsteadyData object changes, VflVisVtkGeometry
 *     will find out through the given interface and will only update its
 *     corresponding actor, i.e. it will neither delete any old data (this 
 *     should be done by whoever fills VflUnsteadyData object), nor will it
 *     execute a threaded update or the like on the new vtkPolyData object.
 *     To avoid a stalling of the rendering process, the new/updated object
 *     should have its Update() methode executed, already (possibly in a
 *     seperate thread).
 * 3. If the data inside VflUnsteadyData is being replaced (e.g. by using
 *     VflDBVisData), you can safely delete the old data as long as you
 *     make sure VflVisData::HasChanged() returns true. In this case, the old
 *     data won't be used anymore, but the fresh data will be used instead, i.e.
 *     the corresponding actors will have the current vtkPolyData objects
 *     assigned. (Note: There are problems with this mechanism, if more than
 *     one object share the same VflUnsteadyData object (or VflVisData object, 
 *     for that matter) - this is hopefully going to be resolved in the future,
 *     possibly through the observer or model-view-controller design pattern...)
 * 4. This object assumes, that the time mapping (especially the number of time
 *     levels) doesn't change during the lifetime of this object. So make sure, 
 *     that the time mapper of the VflUnsteadyData object *always* returns the
 *     same number of time levels.
 *
 * To make sure everything works as planned, it might be a good idea to take the 
 * following steps:
 * 1. Create and fill the VflUnsteadyData object and set m_pData accordingly.
 * 2. (OPTIONAL) Create a property object, which should be used by this object
 *     and set m_pProperty accordingly. If no such object is given, it is
 *     created automatically later on.
 * 3. (OPTIONAL) Create a lookup table, which should be used by this object
 *     and set m_pLookupTable accordingly. If no such object is given, it is
 *     created automatically later on.
 * 4. (OPTIONAL) Set m_bScalarVisibility (or accept the default value "true").
 * 5. If you don't overwrite Init(), just call it. This will set m_pController
 *     appropriately and will call InitPipeline(), which will in turn create any
 *     actors and polydata mappers needed. In addition, m_pLookupTable
 *     and m_pProperty are associated with the respective data objects. If you
 *     overwrite Init(), make sure, that you set m_pController correctly (e.g. 
 *     by calling CVisObject->Init()), before you call InitPipeline().
 *     Of course, you have to provide a sensible value for the parameter 
 *     iRegistration, when calling Init().
 *
 * Additional notes:
 * Notifications concerning changed data are received through the IVflObserver
 * interface. The VflVisObject registers itself with the sub-objects of
 * the VflUnsteadyData object, setting their respective time index as the
 * identifier, which is to be sent upon notification.
 * 
 */    
class VISTAFLOWLIBAPI VflVisVtkGeometry : public IVflVisObject
{
public:

    // CONSTRUCTORS / DESTRUCTOR
    VflVisVtkGeometry();
    virtual ~VflVisVtkGeometry();

	// public typedefs
	typedef std::vector<VflUnsteadyDataFilter *>::size_type FLTIDX;


    /**
     * Initialize object (and register with visualization controller).
     * 
     * @param[in]   VflVisController *pController		the visualization controller
	 * @param[in]	int iRegistration				callbacks to be registered for (default: NONE)
	 *                                          see VflVisController::OBJECT_LIST_MODE for more
     * @return  bool							success of the operation
     *
     */    
    virtual bool Init();
	virtual void Update();
    virtual void DrawOpaque();
	virtual void DrawTransparent();
	virtual unsigned int GetRegistrationMode() const;

    /**
     * Returns the boundaries of the visualization object.
	 * Note: This method is not declared const, as it might change the
	 *       cached boundaries inside the object.
     * 
     * @param[out]  VistaVector3D minBounds    minimum of the object's AABB
	 * @param[out]  VistaVector3D maxBounds    maximum of the object's AABB
     * @return  bool	is the bounding box valid?
     */    
	virtual bool GetBounds(VistaVector3D &minBounds, 
		                   VistaVector3D &maxBounds);

    /**
     * Returns the name of the visualization object.
     * 
     * @param[in]   --
     * @return  std::string
     */    
	virtual std::string GetType() const;

    /**
     * Prints out some debug information to the given output stream.
     * 
     * @param[in]   std::ostream & out
     * @return  --
     */    
    virtual void Debug(std::ostream & out) const;

    /**
     * Retrieve the vtkProperty object, which is shared by all actors inside
	 * this object.
     * 
     * @param[in]   ---
     * @return  vtkProperty *     the corresponding property object
     */    
	vtkProperty *GetProperty() const;

    /**
     * Set the vtkProperty object, which is to be shared by all actors inside
	 * this object. Note, that this is only possible as long as Init() has not
	 * been called.
     * 
     * @param[in]   vtkProperty * pProperty		the property object to be used for drawing
     * @return  bool						Has the operation been successful?
     */    
	bool SetProperty(vtkProperty *);

    /**
     * Retrieve the vtkProperty object, which is shared by all actors inside
	 * this object, and which is used for backface rendering.
     * 
     * @param[in]   ---
     * @return  vtkProperty *     the corresponding property object
     */    
	vtkProperty *GetBackfaceProperty() const;

    /**
     * Set the vtkProperty object, which is to be shared by all actors inside
	 * this object for rendering backfaces. Note, that this is only possible as long 
	 * as Init() has not been called.
     * 
     * @param[in]   vtkProperty * pProperty		the property object to be used for backface drawing
     * @return  bool						Has the operation been successful?
     */    
	bool SetBackfaceProperty(vtkProperty *);

	 /**
     * Retrieve the vtkLookupTable object, which is to be used by this
	 * visualization object.
     *
     * @param[in]   ---
     * @return  vtkLookupTable *     the corresponding lookup table
     */    
	vtkLookupTable *GetLookupTable() const;

    /**
     * Set the vtkLookupTable object, which is to be shared by all actors inside
	 * this object. 
     * 
     * @param[in]   vtkLookupTable *pLookupTable	the lookup table to be used for drawing
     * @return  bool							Has the operation been successful?
     */    
	bool SetLookupTable(vtkLookupTable *);


    /**
     * Get a pointer to the unsteady data object to be visualized.
     * 
     * @param[in]   ---
     * @return  VveUnsteadyVtkPolyData*     the corresponding data
     */    
	VveUnsteadyVtkPolyData *GetData() const;

    /**
     * Set the unsteady data object to be visualized. Note, that this is only
	 * possible as long as Init() has not been called!
     * 
     * @param[in]   VveUnsteadyVtkPolyData *pData  the data to be visualized
     * @return  bool                                  Operation successful?
     */    
	bool SetData(VveUnsteadyVtkPolyData *pData);

	bool      SetUnsteadyData(VveUnsteadyData *);

    /**
     * The callback method from the IVistaObserver class, which is to be
	 * used for notificatoins concerning data changes.
     */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	void AddFilter(VflUnsteadyDataFilter *pFilter);
	void RemoveFilter(FLTIDX iIndex);

	VflUnsteadyDataFilter *GetFilter(FLTIDX  iIndex) const;
	FLTIDX                  GetFilterCount() const;
	FLTIDX                  GetFilterIndex(VflUnsteadyDataFilter *pFilter) const;	

	enum ETransferMode
    {
		TM_NONE                 = -1,
        TM_IMMEDIATE_MODE       =  0,
        TM_DISPLAY_LIST         =  1,
        TM_VERTEX_ARRAY         =  2,
        TM_VERTEX_BUFFER_OBJECT =  3,
        TM_LAST_MODE            =  4
    };

	/**
	 * @param pTexture a pointer to a valid vtkTexture that will be set to every vtkActor for this instance.
	 * @return true iff the texture was sucessfully applied to all vtkActors
	 *
	 */
	bool SetTexture(vtkTexture *pTexture);

	/**
	 * This methode retrieves the texture from the *first* vtkActor and
	 * returns it. This is valid, as SetTexture simple multiplex the texture
	 * on every element of the UnsteadyData/vtkActor.
	 * @return the texture that was set with SetTexture(). 
	 */
	vtkTexture *GetTexture() const;

	class VISTAFLOWLIBAPI VflVtkGeometryProperties : public VflVisObjProperties
	{
	public:
		VflVtkGeometryProperties();
		virtual ~VflVtkGeometryProperties();
		
		std::string GetReflectionableType() const;

		bool GetScalarVisibility() const;
		bool SetScalarVisibility(bool bScalarVisibility);

		bool GetImmediateMode() const;
		bool SetImmediateMode(bool bImmediateMode);

		ETransferMode  GetTransferModeId() const;
		bool SetTransferModeId(ETransferMode nTransferMode);

		std::string GetTransferMode() const;
		//! sTransferMode is one of {"DISPLAYLIST", "IMMEDIATE_MODE",
		//! "VERTEX_BUFFER_OBJECT", "VERTEX_ARRAY"}
		bool SetTransferMode(const std::string &sTransferMode);

		enum
		{
			MSG_SCALARVISIBILITY_CHANGE = VflVisObjProperties::MSG_LAST,
			MSG_IMMEDIATEMODE_CHANGE,

			MSG_TRANSFERMODE_CHANGE,

			MSG_LAST
		};

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	private:
		VflVtkGeometryProperties( const VflVtkGeometryProperties &) {}
		VflVtkGeometryProperties &operator=(const VflVtkGeometryProperties &) 
		{
			return *this;
		}
		bool m_bScalarVisibility, 
			 m_bImmediateMode;
		// holds information about the way to transfer geometry to the
		// OpenGL pipeline (Immediate mode, display lists, vertex arrays, 
		// vertex buffer objects)
		ETransferMode m_iTransfermode;


		friend class VflVisVtkGeometry;
	};

    virtual VflVtkGeometryProperties *GetProperties() const;

	static std::string   GetFactoryType();
private:


	enum
	{
		E_FILTER_TICKET = IVflVisObject::E_TICKET_LAST,
		E_LEVELDATA_TICKET,
		E_TICKET_LAST
	};


	virtual VflVisObjProperties    *CreateProperties() const;
	vtkPolyData * GetFilteredPolyData( int iIndex );


	bool HandlePropertyChange(VflVtkGeometryProperties *p, int msg);
	bool HandleLevelDataChange(VveVtkPolyDataItem *pCont, int msg);
	bool HandleFilterChange(VflUnsteadyDataFilter *pFilter, int msg);

	void SetScalarVisibility(bool bScalarVisibility);
	void SetImmediateMode(bool bImmediateMode);


    /**
     * This method creates the required polydata mappers and actors and builds
	 * the corresponding pipeline. In addition, a lookup table and a property
	 * object are created and shared among them.
	 */
	bool InitPipeline();

    /**
     * Compute (or update) the object's boundaries.
     */    
	virtual bool ComputeBounds();

	/**
	 * Connect all filters in a (hopefully) correct way...
	 */
	void RewireFilters();



	typedef std::vector<vtkActor*> ACVEC;
	typedef std::vector<bool>      BOOLVEC;
	typedef std::vector<VflUnsteadyDataFilter *> FILTVEC;

	VveUnsteadyVtkPolyData *m_pData;
	vtkProperty    *m_pProperty, 
		           *m_pBackfaceProperty;
	vtkLookupTable *m_pLookupTable;
	vtkTexture     *m_pTexture;


	bool m_bBoundsValid, 
		 m_bNeedRewire,
		 m_bTransformerDirty;

	double      m_dLastDrawSimTime,
		        m_dLastDrawTransSimTime;



	FILTVEC m_vecFilters;
	ACVEC   m_vecActors;
	BOOLVEC m_vecDataChanged,
		    m_vecBoundsChanged;
	
	VistaVector3D m_v3BoundsMin, 
		           m_v3BoundsMax;
	VtkObjectStack m_oVtkObjectStack;
};

/*============================================================================*/
/* INLINE METHODS                                                             */
/*============================================================================*/
inline vtkProperty *VflVisVtkGeometry::GetProperty() const
{
	return m_pProperty;
}

inline vtkProperty *VflVisVtkGeometry::GetBackfaceProperty() const
{
	return m_pBackfaceProperty;
}

inline vtkLookupTable *VflVisVtkGeometry::GetLookupTable() const
{
	return m_pLookupTable;
}

inline VveUnsteadyVtkPolyData *VflVisVtkGeometry::GetData() const
{
	return m_pData;
}



/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLVISVTKGEOMETRY_H)
