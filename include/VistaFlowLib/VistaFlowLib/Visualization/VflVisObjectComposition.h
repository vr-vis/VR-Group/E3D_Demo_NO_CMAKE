/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVISOBJECTCOMPOSITION_H
#define _VFLVISOBJECTCOMPOSITION_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflRenderable.h"

#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This class just defines and implements the composite part of the composite
 * design pattern for visualization objects, being implemented together with
 * IVflVisObject and VflVisObject.
 *
 * (see VflVisObjectComposition and VflVisObjectCompositionComposition)
 */
class VISTAFLOWLIBAPI VflVisObjectComposition : public IVflRenderable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflVisObjectComposition();
    virtual ~VflVisObjectComposition();

public:
    /**
     * Add the given visualization object (or composite) to this composition.
     * This method returns the number of contained objects.
     */
    int AddVisObject(IVflRenderable *pVisObject);

    /** 
     * Remove the given visualization object (or composite) from this composition.
     * Note, that by issuing this command, the indices for included vis object
     * may (or better: WILL) change.
     * This method returns the number of containerd objects, after removal of 
     * the given one.
     */
    int RemoveVisObject(IVflRenderable *pVisObject);

    /** 
     * Remove the given visualization object (or composite) from this composition.
     * Note, that by issuing this command, the indices for included vis object
     * may (or better: WILL) change.
     * This method returns the number of containerd objects, after removal of 
     * the given one.
     */
    int RemoveVisObject(int iIndex);

    /**
     * Retrieve the number of registered vis objects.
     */
    int GetNumberOfVisObjects() const;

    /**
     * Retrieve the vis object with the given index.
     */
    IVflRenderable *GetVisObject(int iIndex) const;

    /**
     * Retrieve the index of the given vis object.
     * If it cannot be found, return -1.
     */
    int GetVisObjectIndex(IVflRenderable *pVisObject) const;

   /**
     * Process a given command event.
     * 
     * @param[in]   VflVOCEvent *pEvent
     * @return  --
     */    
	//virtual void ProcessCommand(VflVOCEvent *pEvent);

    /**
     * Returns the boundaries of the visualization object.
     * In this case, it's the maximum bounding box from all 
     * sub-object, which have valid bounds.
     * 
     * @author  Marc Schirski  
     *
     * @param[out]  VistaVector3D &minBounds	minimum of the object's AABB
	 * @param[out]  VistaVector3D &maxBounds	maximum of the object's AABB
     * @return  bool	is the bounding box valid?
     */    
	virtual bool GetBounds(VistaVector3D &minBounds, 
		                   VistaVector3D &maxBounds);

    /**
     * Returns the type of the visualization object as a string.
     * 
     * @author  Marc Schirski  
     *
     * @param[in]   --
     * @return  std::string
     */    
	virtual std::string GetType() const;

    /**
     * Prints out some debug information to the given output stream.
     * 
     * @author  Marc Schirski  
     *
     * @param[in]   std::ostream & out
     * @return  --
     */    
    virtual void  Debug(std::ostream & out) const;

	/**
     * Visibility control - this calls the respective methods
     * of the registered sub-objects.
     */
	//virtual void SetVisible(bool bVisible);
	//virtual bool GetVisible() const;

    /**
     * Visibility control (again ;-) - this calls the respective
     * methods of the given sub-object.
     */
    //virtual void SetVisible(int iIndex, bool bVisible);
    //virtual bool GetVisible(int iIndex) const;

protected:
    std::vector<IVflRenderable *> m_vecVisObjects;
    int m_iObjectCount;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLVISOBJECTCOMPOSITION_H)
