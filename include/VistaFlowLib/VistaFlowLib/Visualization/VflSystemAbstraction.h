/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLSYSTEMABSTRACTION_H
#define _VFLSYSTEMABSTRACTION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <string>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class IVistaExplicitCallbackInterface;
class VflVisController;
class VistaVector3D;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAPI IVflSystemAbstraction
{
public:
	virtual ~IVflSystemAbstraction();

	virtual double GetFrameClock() const = 0;
	virtual float  GenerateRandomNumber(int nLowerBound, 
		                                int nUpperBound) const = 0;


	class VISTAFLOWLIBAPI IVflOverlayText
	{
	public:
		virtual ~IVflOverlayText() {}

		virtual bool GetPosition(float &x, float &y) const = 0;
		virtual bool SetPosition(float x, float y) = 0;
		virtual bool GetText(std::string &sText) const = 0;
		virtual bool SetText(const std::string &sText) = 0;

		virtual bool GetSize(float &fTextSize) const = 0;
		virtual bool SetSize(float fTextSize) = 0;

		virtual bool SetEnabled(bool bEnableState) = 0;
		virtual bool GetEnabled() const = 0;

		virtual bool GetColor(float &r, float &g, float &b) const = 0;
		virtual bool SetColor(float r, float g, float b) = 0;

		virtual bool Update() = 0;
	protected:
		IVflOverlayText() {}
	};

	virtual IVflOverlayText *AddOverlayText( const std::string &sFontHint = "TYPEWRITER", 
												const std::string strViewportName = "" ) = 0;
	virtual bool             DeleteOverlayText(IVflOverlayText *) = 0;

	virtual bool IsLeader() const = 0;
	virtual bool IsFollower() const = 0;

	virtual std::string GetClientName() const = 0;

	//! Returns a pointer to the app's system abstraction instance.
	/*!
	 * NOTE: The purpose of this function is to make the system abstraction
	 *		 easily accessible from arbitrary places as some classes need
	 *		 access to the underlying system. Until now this functionality
	 *		 was encapsulated in the VflVisController via the GetSystem()
	 *		 function.
	 *
	 * \return Returns 0 if no system abstraction has been set, yet, or a
	 *		   pointer to the set system abstraction.
	 * \sa The system abstraction is set via SetSystemAbstraction().
	 */
	static IVflSystemAbstraction* GetSystemAbs();

protected:
	IVflSystemAbstraction();

	static IVflSystemAbstraction	*m_pSystemAbs;

	//! Set the FlowLib-wide system abstraction.
	/*!
	 * NOTE: This function should be called by the constructor of a concrete
	 *		 implementation of IVflSystemAbstraction (hence it's protected).
	 *		 It can only be called once (with a non-NULL pointer) so that only
	 *		 one instance of a concrete system abstraction can exist at a
	 *		 given time. That way a pseudo-singleton is created.
	 *
	 * \param pSysAbs A pointer to the system abstraction to be set.
	 * \return Returns 'true' iff the system abstraction could be set.
	 * \sa See GetSystemAbstraction() for more details.
	 */
	bool SetSystemAbs(IVflSystemAbstraction *pSysAbs);

private:
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VISTASYSTEM_H

