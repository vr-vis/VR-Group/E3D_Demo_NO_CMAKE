/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflRenderOrthoProjToTexture.h"

#include "VflRenderNode.h"

#include <iostream>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/


/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
IVflRenderOrthoProjToTexture::IVflRenderOrthoProjToTexture(
	VflRenderNode *pRenderNode,
	int iWidth, int iHeight,
	float fLeft, float fRight, 
	float fBottom, float fTop,
	float fNear, float fFar)
: IVfl2DTextureGenerator(iWidth, iHeight), 
  m_pRenderNode(pRenderNode), m_pRenderToTexture(NULL), m_pViewAdapter(NULL)
{
	m_pViewAdapter = new VflRenderToTexture::C2DViewAdapter;
	m_pViewAdapter->Init();
	m_pViewAdapter->SetOrthoParams(fLeft, fRight, fBottom, fTop, fNear, fFar);

	m_pRenderToTexture = new VflRenderToTexture(iWidth, iHeight);
	m_pRenderToTexture->Init();
	m_pRenderToTexture->SetTarget(m_pViewAdapter);
	m_pRenderNode->AddRenderable(m_pRenderToTexture);
}

IVflRenderOrthoProjToTexture::~IVflRenderOrthoProjToTexture()
{
	delete m_pViewAdapter;
	delete m_pRenderToTexture;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetOutput                                                   */
/*                                                                            */
/*============================================================================*/
VistaTexture* IVflRenderOrthoProjToTexture::GetOutput() const
{
	return m_pRenderToTexture->GetTexture();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RegisterTarget                                              */
/*                                                                            */
/*============================================================================*/
void IVflRenderOrthoProjToTexture::RegisterTarget(IVflRenderable *pTarget)
{
	m_pViewAdapter->RegisterTarget(pTarget);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UnregisterTarget                                            */
/*                                                                            */
/*============================================================================*/
void IVflRenderOrthoProjToTexture::UnregisterTarget(IVflRenderable *pTarget)
{
	m_pViewAdapter->UnregisterTarget(pTarget);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTextureBackgroundColor                                   */
/*                                                                            */
/*============================================================================*/
void IVflRenderOrthoProjToTexture::SetTextureBackgroundColor(float fC[4])
{
	IVfl2DTextureGenerator::SetTextureBackgroundColor(fC);
	m_pRenderToTexture->SetBackgroundColor(fC);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetOrthoParams                                              */
/*                                                                            */
/*============================================================================*/
void IVflRenderOrthoProjToTexture::GetOrthoParams(float &fLeft, float &fRight, 
												  float &fBottom, float &fTop,
												  float &fNear, float &fFar)
{
	m_pViewAdapter->GetOrthoParams(fLeft, fRight, fBottom, fTop, fNear, fFar);
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
