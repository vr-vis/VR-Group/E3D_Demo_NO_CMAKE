/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFLRENDERABLE_H
#define _VFLRENDERABLE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VistaFlowLibConfig.h"
#include "../Data/VflObserver.h"

#include <VistaAspects/VistaNameable.h>
#include <VistaAspects/VistaReflectionable.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaVector3D;
class VistaBoundingBox;
class IVflRenderable;
class VflVisTiming;
class VflRenderNode;

std::ostream & operator<<(std::ostream&, const IVflRenderable&);


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Base class for objects that get computation time provided by a RenderNode
 * to which they are attached to. Basically this is needed in order to render
 * something (hence the name), but this is not a must.
 * When subclassing, be sure to subclass and define the following:
 *   - implement Init() properly, but do NOT forget to call init from any
 *     subclass! I repeat: do NOT forget this!
 *   - implement GetBounds() properly! Otherwise you can end up with severe
 *     visual artifacts due to bad culling or the like
 *   - implement GetType() properly in any subclass
 *   - implement GetRegistrationType()
 * when using properties, you should do as follows
 *   - subclass VflRenderableProperties
 *   - implement CreateProperties()
 *   - implement DeleteProperties() (optional)
 * Properties are defined by the well known reflectionable API.
 * In case you want to listen to update messages, e.g. from your data, a
 * transformer or the properties, override the ObserverUpdate() method, see the
 * IVistaObserver interface for details on this.
 */
class VISTAFLOWLIBAPI IVflRenderable : public VflObserver, public IVistaNameable
{
public:
    virtual ~IVflRenderable();

	/**
	 * It is important for any subclass to call this method, although it
	 * can be overridden for special purpose modifications.
	 * A renderable is not ready to operate when this method is not called
	 * or does return false. The usage of a renderable is thus
	 *   - create via constructor
	 *   - call Init(), check return value
	 *   - register it with a RenderNode
	 * Registering a Renderable with a RenderNode can work without Init()
	 * but you will probably not see anything. 
	 * This method calls CreateProperties(), which in term will call the
	 * specialized method hook from a Renderable. Additionally, it will hook
	 * this Renderable as an observer onto the created properties, so changes
	 * on the props get propagated to this Renderable
	 */
	virtual bool Init();

    /**
     * Returns the boundaries of the visualization object.
     * 
     * @param minBounds Minimum of the object's AABB.
	 * @param maxBounds Maximum of the object's AABB.
     * @return 'false' iff the bounding box could not be determined.
     */
	virtual bool GetBounds(VistaVector3D& v3MinBounds, 
		                   VistaVector3D& v3MaxBounds);

	/**
	 * This implementation calls GetBounds() with two vectors and stores the
	 * result in the VistaBoundingBox() as a side effect. It's an AABB.
	 * @param a bounding box to store the AABB to.
	 * @return 'true' iff the bounds could be determined.
	 * @see GetBounds()	 
	 */
	virtual bool GetBounds(VistaBoundingBox& oBounds);

	/**
	 * Convinience method, calls SetVisible() on the properties member.
	 * @see GetProperties()
	 * @param bVisible true iff the renderable gets computation time from
	 *		  the RenderNode, false else (it will be inactive then)
	 */
	virtual void SetVisible(bool bVisible);

	/**
	 * Convinience method, calls GetVisible() from the properties member.
	 * @return 'true' iff this Renderable gets computation time from the
	 *		   RenderNode, false else (it will be inactive then)
	 * @see GetProperties()
	 */
	virtual bool GetVisible() const;

    /**
     * Prints out some debug information to the given output stream.
	 * You can consider this method deprecated.
     * @param[in] out The stream to output to.
     */    
    virtual void Debug(std::ostream& out) const;


    /**
     * Returns the type of the visualization object as a string.
	 * Constantly "IVflRenderable".
     */    
	virtual std::string GetType() const;
	
	/**
     * Returns the object's id within the scope of its RenderNode.
	 * @see GetRenderNode()
     * @return int	the id (-1 if not registered )
     */    
	int GetId() const;
	/** 
	 * API used by the RenderNode. Normal clients should not use it.
	 */
	void SetId(int nId);


	//==========================================================================
	// Core functionallity API.
	//==========================================================================
	/** 
	 * Called, when this instance is registered by its RenderNode with the
	 * registration flag OLI_UPDATE.
	 * Implement code here that performs updates. Do not assume any kind of call
	 * order in relation to other draw routines, e.g., DrawOpaque().
	 * @see GetRegistrationMode(), DrawOpaque(), DrawTransparent(), Draw2D()
	 */
    virtual void Update();

	/** 
	 * Implement code here that draws opaque, e.g., non-transparent stuff. You
	 * can assume a draw mode in data space here. Renderable that are registered
	 * with draw mode OLI_DRAW_OPAQUE have this method called.
	 * @see Update(), DrawTransparent(), Draw2D()
	 */
    virtual void DrawOpaque();

	/** 
	 * Implement code here that draws with transparency. You can assume a draw
	 * mode in data space here. Renderables that are registered with draw mode
	 * OLI_DRAW_TRANSPARENT have this method called.
	 * @see Update()
	 * @see DrawOpaque()
	 * @see Draw2D()
	 */
    virtual void DrawTransparent();

	/** 
	 * Implement code here to draw scene overlays, e.g., text or bitmaps or the
	 * like here. However, if you do OpenGL stuff manually, you have to setup
	 * your projection on your own, as you might need some special settings for
	 * that. The only thing you can assume is that everything has been drawn for
	 * the current RenderNode context in relation to the scene stuff, e.g,
	 * DrawTransparent(), DrawOpaque and Update() are already history.
	 * @see Update(), DrawOpaque(), DrawTransparent()
	 */
    virtual void Draw2D();


	//==========================================================================
	// Interface used by the VflRenderNode.
	//==========================================================================
	/**
	 * @see GetRegistrationMode()
	 */
	enum RenderableDrawMode
	{
											//< Owning RenderNode calls ...
		OLI_NONE				= 0,		//<	... none of the following.
		OLI_UPDATE				= (1 << 0), //< ... Update()
		OLI_DRAW_OPAQUE			= (1 << 1), //< ... DrawOpaque()
		OLI_DRAW_TRANSPARENT	= (1 << 2), //< ... DrawTransparent()
		OLI_DRAW_2D				= (1 << 3), //< ... Draw2D()
		OLI_ALL					= (~0)		//< ... all of the above.
	};
		
	/**
	 * Has to be defined by every subclass of IVflRenderable. Return a bit-mask
	 * of the combined by elements of the OBJECT_LIST_MODE enum as an unsigned
	 * int here. The value of that bit mask decides on whether DrawOpaque(),
	 * DrawTransparent() or Draw2D() or Update() is called during RenderNode
	 * update.
	 * @return a bit mask to match against the OBJECT_LIST_MODE members
	 */ 
	virtual unsigned int GetRegistrationMode() const = 0;

	/**
	 * This API is used by the RenderNode after a sucessfull registration,
	 * client code should not use it. However, one can subclass it to intercept
	 * the setting of the RenderNode if that is of special interest to
	 * the subclass of this IVflRenderable.
	 */
	virtual void SetRenderNode(VflRenderNode* pRenderNode);
	/**
	 * Return this vis object's RenderNode.
	 */
	VflRenderNode* GetRenderNode() const;


	//==========================================================================
	// Property-related API.
	//==========================================================================
	/**
	 * Base class for renderable props. Defines:
	 * - "VISIBILITY": visibility [bool].
	 *                 an invisible renderable simply does not get
	 *				   any computation time from its RenderNode
	 * - a backling to its parent context (GetParentRenderable())
	 * - the renderable name and its type for reflection (usually
	 *   its human readable class name)
	 */ 
	class VISTAFLOWLIBAPI VflRenderableProperties : public IVistaReflectionable
	{
		REFL_DECLARE

	public:
		enum Messages
		{
			MSG_PARENT_CHANGED = IVistaReflectionable::MSG_LAST,
			MSG_VISIBILITY,
			MSG_LAST
		};
		
		VflRenderableProperties ();
		virtual ~VflRenderableProperties ();

		IVflRenderable* GetParentRenderable() const;
		void SetParentRenderable(IVflRenderable* pParent);

		virtual bool GetVisible() const;
		virtual bool SetVisible(bool bVisible);

		std::string GetRenderableName() const;
		
	protected:
		IVflRenderable* m_pParentObject;
	private:
		bool            m_bVisible;		
	};

	/**
	 * Generic purpose getter for the properties of this renderable.
	 * Clients that need a special type may need a downcast of the return value.
	 * @return 'NULL' iff Init() was not called properly.
	 */
	virtual VflRenderableProperties* GetProperties() const;


	// *** IVistaNameable interface. ***
    virtual std::string GetNameForNameable() const;
	virtual void SetNameForNameable(const std::string &strNewName);


	// *** IVistaObserver interface. ***
    /**
     * Implemented here to avoid forcing implementation in deriving classes, as
	 * this function is a purely virtual function.
	 */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int iMsg,
							    int iTicket);
	
protected:
	IVflRenderable();

	/**
	 * Deriving classes are required to implement this function. This is an
	 * attempt to avoid the creation of non-matching property objects for the
	 * deriving class.
	 */
	virtual VflRenderableProperties* CreateProperties() const = 0;

	/**
	 * This method simply calls delete on the pointer that is passed to it.
	 * It should not be necessary to redefine it, but if you need to, it is
	 * possible.
	 * @param pProps The property object to be deleted.
	 * @return 'true' iff the memory for the props was released.
	 */
	virtual bool DeleteProperties(VflRenderableProperties* pProps) const;

	/**
	 * Ticket symbols for calls to ObserverUpdate().
	 * Changes on the properties are updated with a ticket E_PROP_TICKET.
	 * May be useful for routing.
	 * @see ObserverUpdate()
	 */
	enum LocalObserverTickets
	{
		E_PROP_TICKET = 0,
		E_TICKET_LAST
	};
	
	VflRenderableProperties* m_pProperties; /**< pointer to store the
											  props of this instance */

private:
	int	        m_iId;		//< The object's unique identifier.
	std::string	m_strName;	//< The object's name (don't mix up with type string!)


	VflRenderNode* m_pRenderNode; /**< pointer to the RenderNode that holds
								    this instance. */
};


/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/
inline int IVflRenderable::GetId() const
{
	return m_iId;
}

inline void IVflRenderable::SetId(int nId)
{
	m_iId = nId;
}


inline VflRenderNode* IVflRenderable::GetRenderNode() const
{
	return m_pRenderNode;
}


#endif // Include gurad.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
