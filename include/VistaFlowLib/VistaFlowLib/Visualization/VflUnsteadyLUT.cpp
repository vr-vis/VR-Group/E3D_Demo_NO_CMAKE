/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include <iostream>

#include "VflUnsteadyLUT.h"

#include <VistaVisExt/Tools/VveInfoReader.h>
#include <VistaVisExt/Tools/VveDataSetInfo.h>
#include <VistaVisExt/Tools/VveDataChunkInfo.h>
#include <VistaVisExt/Tools/VveDataFieldInfo.h>

#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>

#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>

#include <VistaAspects/VistaAspectsUtils.h>

#include <VistaBase/VistaVectorMath.h>

#include <vtkLookupTable.h>

/*============================================================================*/
/* Constructor/Destructor                                                     */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :       VflUnsteadyLUT()                                       */
/*                                                                            */
/*============================================================================*/
VflUnsteadyLUT::VflUnsteadyLUT(VflRenderNode *pRenderNode,
								 VveTimeMapper* pTargetTM):
	m_pTimeMapper(pTargetTM),
	m_bIsInited(false),
	m_eRenderMode(VflUnsteadyLUT::RM_PERSPECTIVE),
	m_pDataSetInfo(new VveDataSetInfo),
	m_iActiveField(0),
	m_pLUT(NULL),
	m_iCurTimeIndex(0),
	m_fWidth(0.2f), m_fHeight(1.0f),
	m_fCurPos(new float[12]),
	m_fGlobalPos(new float[12]),
	m_pGlobalMinTxt(new Vfl3DTextLabel),
	m_pGlobalMaxTxt(new Vfl3DTextLabel),
	m_pCurMinTxt(new Vfl3DTextLabel),
	m_pCurMaxTxt(new Vfl3DTextLabel)
{
	m_pLUT = vtkLookupTable::New();

	this->SetRenderNode(pRenderNode);

	if(m_fCurPos)
		for(int i=0; i<16; ++i)
			m_fCurPos[i] = 0.0f;
	
	if(m_fGlobalPos)
		for(int i=0; i<16; ++i)
			m_fGlobalPos[i] = 0.0f;

	if(m_pGlobalMinTxt)
	{
		m_pGlobalMinTxt->Init();
		m_pGlobalMinTxt->SetTextFollowViewDir(false);
		m_pGlobalMinTxt->SetRenderNode(this->GetRenderNode());
		m_pGlobalMinTxt->SetTextSize(0.06f);
	}
	if(m_pGlobalMaxTxt)
	{
		m_pGlobalMaxTxt->Init();
		m_pGlobalMaxTxt->SetTextFollowViewDir(false);
		m_pGlobalMaxTxt->SetRenderNode(this->GetRenderNode());
		m_pGlobalMaxTxt->SetTextSize(0.06f);
	}
	if(m_pCurMinTxt)
	{
		m_pCurMinTxt->Init();
		m_pCurMinTxt->SetTextFollowViewDir(false);
		m_pCurMinTxt->SetRenderNode(this->GetRenderNode());
		m_pCurMinTxt->SetTextSize(0.06f);
	}
	if(m_pCurMaxTxt)
	{
		m_pCurMaxTxt->Init();
		m_pCurMaxTxt->SetTextFollowViewDir(false);
		m_pCurMaxTxt->SetRenderNode(this->GetRenderNode());
		m_pCurMaxTxt->SetTextSize(0.06f);
	}

	// Generate texture for coloring the current LUT vis.
	glEnable(GL_TEXTURE_1D);

	glGenTextures(1, &m_uiLUTColorTxt);
	glBindTexture(GL_TEXTURE_1D, m_uiLUTColorTxt);

	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, 256, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	glBindTexture(GL_TEXTURE_1D, 0);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       ~VflUnsteadyLUT()                                      */
/*                                                                            */
/*============================================================================*/
VflUnsteadyLUT::~VflUnsteadyLUT()
{
	if(m_pDataSetInfo)
		delete m_pDataSetInfo;

	if(m_pLUT)
		m_pLUT->Delete();

	if(m_pGlobalMinTxt)
		delete m_pGlobalMinTxt;
	if(m_pGlobalMaxTxt)
		delete m_pGlobalMaxTxt;
	if(m_pCurMinTxt)
		delete m_pCurMinTxt;
	if(m_pCurMaxTxt)
		delete m_pCurMaxTxt;

	glDeleteTextures(1, &m_uiLUTColorTxt);
}

/*============================================================================*/
/* Interface                                                                  */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetLookupTable(...)                                     */
/*                                                                            */
/*============================================================================*/
vtkLookupTable *VflUnsteadyLUT::GetLookupTable() const
{
	return m_pLUT;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       Init(...)                                               */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyLUT::Init(VveInfoReader *pInfoReader)
{
	// We need a (valid) VveInfoReader to initialize.
	if(!pInfoReader)
		return false;

	pInfoReader->ReadFileInfo(m_pDataSetInfo);

	m_bIsInited = true;

	return IVflRenderable::Init();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       Update()                                             */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyLUT::Update()
{
	if(!m_pTimeMapper || !m_bIsInited || !m_pLUT)
		return;

	float fVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	m_iCurTimeIndex = m_pTimeMapper->GetLevelIndex(fVisTime);

	// --------------------------------------------------------------------- //
	// Variable inits.
	VveDataFieldInfo *pCur = 
		m_pDataSetInfo->GetChildChunkInfo(m_iCurTimeIndex)->GetFieldInfo(m_iActiveField);
	VveDataFieldInfo *pGlobal =
		m_pDataSetInfo->GetFieldInfo(m_iActiveField);

	double dCurMin = pCur->GetMin(0);
	double dCurMax = pCur->GetMax(0);
	double dGlobalMin = pGlobal->GetMin(0);
	double dGlobalMax = pGlobal->GetMax(0);

	// --------------------------------------------------------------------- //
	// LUT update.
	m_pLUT->SetTableRange(dCurMin, dCurMax);

	m_pLUT->ForceBuild();

	// --------------------------------------------------------------------- //
	// Vis update.
	m_pGlobalMinTxt->SetText(VistaAspectsConversionStuff::ConvertToString(dGlobalMin));
	m_pGlobalMaxTxt->SetText(VistaAspectsConversionStuff::ConvertToString(dGlobalMax));
	m_pCurMinTxt->SetText(VistaAspectsConversionStuff::ConvertToString(dCurMin));
	m_pCurMaxTxt->SetText(VistaAspectsConversionStuff::ConvertToString(dCurMax));

	m_pGlobalMinTxt->Update();
	m_pGlobalMaxTxt->Update();
	m_pCurMinTxt->Update();
	m_pCurMaxTxt->Update();

	UpdateTexture();
	UpdateVis();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       DrawOpaque()	                                          */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyLUT::DrawOpaque()
{
	if(!GetVisible())
		return;
	
	glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT | GL_POLYGON_BIT);

	if(m_eRenderMode == RM_ORTHOGONAL)
	{
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();

		gluOrtho2D(0.0f, 1.0f, 0.0f, 1.0f);
	}
	glEnable(GL_TEXTURE_1D);
	glDisable(GL_LIGHTING);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	glDisable(GL_CULL_FACE);

	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
		glVertex3f(m_fGlobalPos[0], m_fGlobalPos[1], m_fGlobalPos[2]);
		glVertex3f(m_fGlobalPos[3], m_fGlobalPos[4], m_fGlobalPos[5]);
		glVertex3f(m_fGlobalPos[6], m_fGlobalPos[7], m_fGlobalPos[8]);
		glVertex3f(m_fGlobalPos[9], m_fGlobalPos[10], m_fGlobalPos[11]);
	glEnd();

	glBindTexture(GL_TEXTURE_1D, m_uiLUTColorTxt);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
		glTexCoord1f(0.0f);
		glVertex3f(m_fCurPos[0], m_fCurPos[1], m_fCurPos[2]);
		glVertex3f(m_fCurPos[3], m_fCurPos[4], m_fCurPos[5]);
		glTexCoord1f(1.0f);
		glVertex3f(m_fCurPos[6], m_fCurPos[7], m_fCurPos[8]);
		glVertex3f(m_fCurPos[9], m_fCurPos[10], m_fCurPos[11]);
	glEnd();
	glBindTexture(GL_TEXTURE_1D, 0);

	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_LINES);
		glVertex3f(m_fGlobalPos[0], m_fGlobalPos[1], m_fGlobalPos[2]);
		glVertex3f(m_fGlobalPos[3], m_fGlobalPos[4], m_fGlobalPos[5]);

		glVertex3f(m_fGlobalPos[3], m_fGlobalPos[4], m_fGlobalPos[5]);
		glVertex3f(m_fGlobalPos[6], m_fGlobalPos[7], m_fGlobalPos[8]);

		glVertex3f(m_fGlobalPos[6], m_fGlobalPos[7], m_fGlobalPos[8]);
		glVertex3f(m_fGlobalPos[9], m_fGlobalPos[10], m_fGlobalPos[11]);

		glVertex3f(m_fGlobalPos[9], m_fGlobalPos[10], m_fGlobalPos[11]);
		glVertex3f(m_fGlobalPos[0], m_fGlobalPos[1], m_fGlobalPos[2]);

		glVertex3f(m_fCurPos[0], m_fCurPos[1], m_fCurPos[2]);
		glVertex3f(m_fCurPos[3], m_fCurPos[4], m_fCurPos[5]);

		glVertex3f(m_fCurPos[3], m_fCurPos[4], m_fCurPos[5]);
		glVertex3f(m_fCurPos[6], m_fCurPos[7], m_fCurPos[8]);

		glVertex3f(m_fCurPos[6], m_fCurPos[7], m_fCurPos[8]);
		glVertex3f(m_fCurPos[9], m_fCurPos[10], m_fCurPos[11]);

		glVertex3f(m_fCurPos[9], m_fCurPos[10], m_fCurPos[11]);
		glVertex3f(m_fCurPos[0], m_fCurPos[1], m_fCurPos[2]);
	glEnd();

	glPopAttrib();

	m_pGlobalMinTxt->DrawOpaque();
	m_pGlobalMaxTxt->DrawOpaque();
	m_pCurMinTxt->DrawOpaque();
	m_pCurMaxTxt->DrawOpaque();

	if(m_eRenderMode == RM_ORTHOGONAL)
	{
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetRegistrationMode()                                   */
/*                                                                            */
/*============================================================================*/
unsigned int VflUnsteadyLUT::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE | IVflRenderable::OLI_UPDATE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       CreateProperties()                                      */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* VflUnsteadyLUT::CreateProperties() const
{
	return new VflRenderableProperties;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetType()			                                      */
/*                                                                            */
/*============================================================================*/
std::string VflUnsteadyLUT::GetType() const
{
	return IVflRenderable::GetType()+std::string("::VflUnsteadyLUT");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetColor(...)                                           */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyLUT::GetColor(double x, double rgb[3]) const
{
	if (!m_pLUT)
		return false;

	m_pLUT->GetColor(x, rgb);

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetOpacity(...)                                         */
/*                                                                            */
/*============================================================================*/
double VflUnsteadyLUT::GetOpacity(double x) const
{
	if (!m_pLUT)
		return -1.0;

	return m_pLUT->GetOpacity(x);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetGlobalMin(...)                                       */
/*                                                                            */
/*============================================================================*/
double VflUnsteadyLUT::GetGlobalMin() const
{
	return m_pDataSetInfo->GetFieldInfo(m_iActiveField)->GetMin(0);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetGlobalMax(...)                                       */
/*                                                                            */
/*============================================================================*/
double VflUnsteadyLUT::GetGlobalMax() const
{
	return m_pDataSetInfo->GetFieldInfo(m_iActiveField)->GetMax(0);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetGlobalMinMax(...)                                    */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyLUT::GetGlobalMinMax(double &dMin, double &dMax) const
{
	dMin = m_pDataSetInfo->GetFieldInfo(m_iActiveField)->GetMin(0);
	dMax = m_pDataSetInfo->GetFieldInfo(m_iActiveField)->GetMax(0);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetGlobalMax(...)                                       */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyLUT::GetGlobalMinMax(double dMinMax[2]) const
{
	this->GetGlobalMinMax(dMinMax[0], dMinMax[1]);
}

/*============================================================================*/
/* Selectors                                                                  */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :       SetRenderMode(...)                                      */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyLUT::SetRenderMode(VflUnsteadyLUT::RENDERMODE eMode)
{
	m_eRenderMode = eMode;

	if(m_eRenderMode == RM_ORTHOGONAL)
	{
		m_pGlobalMinTxt->SetTextSize(0.015f);
		m_pGlobalMaxTxt->SetTextSize(0.015f);
		m_pCurMinTxt->SetTextSize(0.015f);
		m_pCurMaxTxt->SetTextSize(0.015f);

		m_fWidth	= 0.05f;
		m_fHeight	= 0.5f;

		m_v3Position[0]		= 0.01f;
		m_v3Position[1]		= 0.05f;
		m_v3Position[2]		= 0.0f;
	}
	else if(m_eRenderMode == RM_PERSPECTIVE)
	{
		m_pGlobalMinTxt->SetTextSize(0.06f);
		m_pGlobalMaxTxt->SetTextSize(0.06f);
		m_pCurMinTxt->SetTextSize(0.06f);
		m_pCurMaxTxt->SetTextSize(0.06f);

		m_fWidth	= 0.2f;
		m_fHeight	= 1.0f;

		m_v3Position[0]		= 0.0f;
		m_v3Position[1]		= 0.0f;
		m_v3Position[2]		= 0.0f;
	}

	UpdateVis();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetRenderMode()	                                      */
/*                                                                            */
/*============================================================================*/
VflUnsteadyLUT::RENDERMODE VflUnsteadyLUT::GetRenderMode() const
{
	return m_eRenderMode;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       SetActiveField(...)                                     */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyLUT::SetActiveField(int iActiveField)
{
	if(iActiveField >= 0 &&
		iActiveField < static_cast<int>(m_pDataSetInfo->GetNumFieldInfo()) &&
		m_pDataSetInfo->GetFieldInfo(iActiveField)->GetNumberOfComponents() == 1)
	{
		m_iActiveField = iActiveField;
		return true;
	}

	return false;
}

bool VflUnsteadyLUT::SetActiveFieldByName(const std::string & sFieldName)
{
	// search field info for sFieldName
	int iField = 0;
	while ((iField < static_cast<int>(m_pDataSetInfo->GetNumFieldInfo())) &&
		(m_pDataSetInfo->GetFieldInfo(iField)->GetFieldName() != sFieldName))
	{
		iField++;
	}

	// not found, return false
	if (iField >= static_cast<int>(m_pDataSetInfo->GetNumFieldInfo()))
		return false;

	// the second while-condition was met, we have a valid ActiveField
	return this->SetActiveField(iField);

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetActiveField(...)                                     */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyLUT::GetActiveField() const
{
	return m_iActiveField;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       SetLUTVisSize(...)                                      */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyLUT::SetLUTVisSize(float fWidth, float fHeight)
{
	if(fWidth <= 0 || fHeight <= 0 ||
		(m_eRenderMode == RM_ORTHOGONAL && (fWidth > 1.0f || fHeight > 1.0f)))
		return false;

	m_fWidth	= fWidth;
	m_fHeight	= fHeight;
	
	UpdateVis();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       SetLUTVisSize(...)                                      */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyLUT::SetLUTVisSize(float fSize[2])
{
	return this->SetLUTVisSize(fSize[0], fSize[1]);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetLUTVisSize(...)                                      */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyLUT::GetLUTVisSize(float &fWidth, float &fHeight) const
{
	fWidth	= m_fWidth;
	fHeight	= m_fHeight;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetLUTVisSize(...)                                      */
/*                                                                            */
/*============================================================================*/
bool VflUnsteadyLUT::GetLUTVisSize(float fSize[2]) const
{
	return this->GetLUTVisSize(fSize[0], fSize[1]);
}


bool VflUnsteadyLUT::SetLUTVisPos3D( const VistaVector3D& v3Pos )
{
	if(m_eRenderMode == RM_ORTHOGONAL)
		return false;

	m_v3Position = v3Pos;
	UpdateVis();
	return true;
}

bool VflUnsteadyLUT::SetLUTVisPos3D(float fX, float fY, float fZ)
{
	return SetLUTVisPos3D(VistaVector3D(fX, fY, fZ));
}

bool VflUnsteadyLUT::SetLUTVisPos3D(float fPos[3])
{
	return SetLUTVisPos3D(VistaVector3D(fPos));
}


VistaVector3D VflUnsteadyLUT::GetLUTVisPos3D() const
{
	return m_v3Position;
}

bool VflUnsteadyLUT::GetLUTVisPos3D(float &fX, float &fY, float &fZ) const
{
	if(m_eRenderMode == RM_ORTHOGONAL)
		return false;

	fX = m_v3Position[0];
	fY = m_v3Position[1];
	fZ = m_v3Position[2];

	return true;
}

bool VflUnsteadyLUT::GetLUTVisPos3D(float fPos[3]) const
{
	return GetLUTVisPos3D(fPos[0], fPos[1], fPos[2]);
}

bool VflUnsteadyLUT::SetLUTVisOrientation(const VistaQuaternion& qOri)
{
	if(m_eRenderMode == RM_ORTHOGONAL)
		return false;

	m_qOrientation = qOri;
	m_pGlobalMinTxt->SetOrientation(qOri);
	m_pGlobalMaxTxt->SetOrientation(qOri);
	m_pCurMinTxt->SetOrientation(qOri);
	m_pCurMaxTxt->SetOrientation(qOri);
	
	UpdateVis();
	return true;
}

VistaQuaternion VflUnsteadyLUT::GetLUTVisOrientation() const
{
	return m_qOrientation;
}


bool VflUnsteadyLUT::SetLUTVisPos2D(float fX, float fY)
{
	if(m_eRenderMode == RM_PERSPECTIVE ||
		fX < 0.0f || fX > 1.0f ||
		fY < 0.0f || fY > 1.0f)
	{
		return false;
	}

	m_v3Position = VistaVector3D(fX, fY, 0.0f);
	return true;
}

bool VflUnsteadyLUT::SetLUTVisPos2D(float fPos[2])
{
	return this->SetLUTVisPos2D(fPos[0], fPos[1]);
}


bool VflUnsteadyLUT::GetLUTVisPos2D(float &fX, float &fY) const
{
	if(m_eRenderMode == RM_PERSPECTIVE)
		return false;

	fX = m_v3Position[0];
	fY = m_v3Position[1];
	return true;
}

bool VflUnsteadyLUT::GetLUTVisPos2D(float fPos[2]) const
{
	return this->GetLUTVisPos2D(fPos[0], fPos[1]);
}


/*============================================================================*/
/* Helper Functions															  */
/*============================================================================*/

void VflUnsteadyLUT::UpdateVis()
{
	// ------------------------------------------------------------------------ //
	// Read the ranges.
	VveDataFieldInfo *pCur = 
		m_pDataSetInfo->GetChildChunkInfo(m_iCurTimeIndex)->GetFieldInfo(m_iActiveField);
	VveDataFieldInfo *pGlobal =
		m_pDataSetInfo->GetFieldInfo(m_iActiveField);

	double dCurMin = pCur->GetMin(0);
	double dCurMax = pCur->GetMax(0);
	double dGlobalMin = pGlobal->GetMin(0);
	double dGlobalMax = pGlobal->GetMax(0);

	if(m_eRenderMode == RM_ORTHOGONAL)
	{
		// ------------------------------------------------- //
		// The 'global' case.
		// Lower Left Corner.
		m_fGlobalPos[0] = m_v3Position[0];
		m_fGlobalPos[1] = m_v3Position[1];
		m_fGlobalPos[2] = 0.0f;

		// Lower Right Corner.
		m_fGlobalPos[3] = m_v3Position[0] + m_fWidth;
		m_fGlobalPos[4] = m_v3Position[1];
		m_fGlobalPos[5] = 0.0f;

		// Upper Right Corner.
		m_fGlobalPos[6] = m_v3Position[0] + m_fWidth;
		m_fGlobalPos[7] = m_v3Position[1] + m_fHeight;
		m_fGlobalPos[8] = 0.0f;

		// Upper Left Corner.
		m_fGlobalPos[9]  = m_v3Position[0];
		m_fGlobalPos[10] = m_v3Position[1] + m_fHeight;
		m_fGlobalPos[11] = 0.0f;

		// ------------------------------------------------- //
		// The 'current' case.
		float fMinOffset = m_fHeight * float((dCurMin - dGlobalMin) / (dGlobalMax - dGlobalMin));
		float fMinMaxOffset = m_fHeight * float((dCurMax - dCurMin) / (dGlobalMax - dGlobalMin));

		// Lower Left Corner.
		m_fCurPos[0] = m_v3Position[0];
		m_fCurPos[1] = m_v3Position[1] + fMinOffset;
		m_fCurPos[2] = 0.01f;

		// Lower Right Corner.
		m_fCurPos[3] = m_v3Position[0] + m_fWidth;
		m_fCurPos[4] = m_v3Position[1] + fMinOffset;
		m_fCurPos[5] = 0.01f;

		// Upper Right Corner.
		m_fCurPos[6] = m_v3Position[0] + m_fWidth;
		m_fCurPos[7] = m_v3Position[1] + fMinOffset + fMinMaxOffset;
		m_fCurPos[8] = 0.01f;

		// Upper Left Corner.
		m_fCurPos[9]  = m_v3Position[0];
		m_fCurPos[10] = m_v3Position[1] + fMinOffset + fMinMaxOffset;
		m_fCurPos[11] = 0.01f;

		// ------------------------------------------------------------------------ //
		// Update text label position.
		VistaVector3D v3Pos;

		v3Pos = VistaVector3D(m_v3Position[0] + m_fWidth,
							   m_v3Position[1] - 1.2f * m_pGlobalMinTxt->GetTextSize(),
							   0.0f);
	/*	v3Pos = VistaVector3D(m_v3Position[0],
							   m_v3Position[1] - 2.0f * m_pGlobalMinTxt->GetTextSize(),
							   0.0f);*/
		m_pGlobalMinTxt->SetPosition(v3Pos);
		
		v3Pos = VistaVector3D(m_v3Position[0] + m_fWidth,
							   m_v3Position[1] + m_fHeight + 0.2f * m_pGlobalMaxTxt->GetTextSize(),
							   0.0f);
	/*	v3Pos = VistaVector3D(m_v3Position[0],
							   m_v3Position[1] + m_fHeight + 1.2f * m_pGlobalMaxTxt->GetTextSize(),
							   0.0f);*/
		m_pGlobalMaxTxt->SetPosition(v3Pos);
		
		v3Pos = VistaVector3D(m_v3Position[0] + m_fWidth,
							   m_v3Position[1] + fMinOffset,
							   0.0f);
		m_pCurMinTxt->SetPosition(v3Pos);
		
		v3Pos = VistaVector3D(m_v3Position[0] + m_fWidth,
							   m_v3Position[1] + fMinOffset + fMinMaxOffset - 1.2f * m_pCurMaxTxt->GetTextSize(),
							   0.0f);
		m_pCurMaxTxt->SetPosition(v3Pos);
	}
	else if(m_eRenderMode == RM_PERSPECTIVE)
	{
		// ------------------------------------------------------------------------ //
		// Calculate vertex positions.
		VistaVector3D v3Right = m_qOrientation.Rotate(VistaVector3D(1.0f, 0.0f, 0.0f));
		VistaVector3D v3Up = m_qOrientation.Rotate(VistaVector3D(0.0f, 1.0f, 0.0f));
		VistaVector3D v3Normal = m_qOrientation.Rotate(VistaVector3D(0.0f, 0.0f, 1.0f));
		
		// ------------------------------------------------- //
		// The 'global' case.
		// Lower Left Corner.
		m_fGlobalPos[3] = m_v3Position[0];
		m_fGlobalPos[4] = m_v3Position[1];
		m_fGlobalPos[5] = m_v3Position[2];
		
		// Lower Right Corner.
		m_fGlobalPos[3] = m_fGlobalPos[0] + v3Right[0] * m_fWidth;
		m_fGlobalPos[4] = m_fGlobalPos[1] + v3Right[1] * m_fWidth;
		m_fGlobalPos[5] = m_fGlobalPos[2] + v3Right[2] * m_fWidth;

		// Upper Right Corner.
		m_fGlobalPos[6] = m_fGlobalPos[3] + v3Up[0] * m_fHeight;
		m_fGlobalPos[7] = m_fGlobalPos[4] + v3Up[1] * m_fHeight;
		m_fGlobalPos[8] = m_fGlobalPos[5] + v3Up[2] * m_fHeight;

		// Upper Left Corner.
		m_fGlobalPos[9]  = m_fGlobalPos[0] + v3Up[0] * m_fHeight;
		m_fGlobalPos[10] = m_fGlobalPos[1] + v3Up[1] * m_fHeight;
		m_fGlobalPos[11] = m_fGlobalPos[2] + v3Up[2] * m_fHeight;

		// ------------------------------------------------- //
		// The 'current' case.
		float fMinOffset = float((dCurMin - dGlobalMin) / (dGlobalMax - dGlobalMin)) * m_fHeight;
		float fMinMaxOffset = float((dCurMax - dCurMin) / (dGlobalMax - dGlobalMin)) * m_fHeight;

		// Lower Left Corner.
		m_fCurPos[0] = m_fGlobalPos[0] + v3Up[0] * fMinOffset;
		m_fCurPos[1] = m_fGlobalPos[1] + v3Up[1] * fMinOffset;
		m_fCurPos[2] = m_fGlobalPos[2] + v3Up[2] * fMinOffset;

		// Lower Right Corner.
		m_fCurPos[3] = m_fCurPos[0] + v3Right[0] * m_fWidth;
		m_fCurPos[4] = m_fCurPos[1] + v3Right[1] * m_fWidth;
		m_fCurPos[5] = m_fCurPos[2] + v3Right[2] * m_fWidth;

		// Upper Right Corner.
		m_fCurPos[6] = m_fCurPos[3] + v3Up[0] * fMinMaxOffset;
		m_fCurPos[7] = m_fCurPos[4] + v3Up[1] * fMinMaxOffset;
		m_fCurPos[8] = m_fCurPos[5] + v3Up[2] * fMinMaxOffset;

		// Upper Left Corner.
		m_fCurPos[9]  = m_fCurPos[0] + v3Up[0] * fMinMaxOffset;
		m_fCurPos[10] = m_fCurPos[1] + v3Up[1] * fMinMaxOffset;
		m_fCurPos[11] = m_fCurPos[2] + v3Up[2] * fMinMaxOffset;

		// Make the current LUT a little wider than the global one.
		const float fStrFac		= 0.00f; // Stretch factor in v3Right direction (default: 0.05f).
		const float fNormOffset = 0.001f; // Distance of global bar from current bar.

		// Lower Left Corner.
		m_fCurPos[0] = m_fCurPos[0] - v3Right[0] * fStrFac * m_fWidth
									+ fNormOffset * v3Normal[0];
		m_fCurPos[1] = m_fCurPos[1] - v3Right[1] * fStrFac * m_fWidth
									+ fNormOffset * v3Normal[1];
		m_fCurPos[2] = m_fCurPos[2] - v3Right[2] * fStrFac * m_fWidth
									+ fNormOffset * v3Normal[2];

		// Lower Right Corner.
		m_fCurPos[3] = m_fCurPos[3] + v3Right[0] * fStrFac * m_fWidth
									+ fNormOffset * v3Normal[0];
		m_fCurPos[4] = m_fCurPos[4] + v3Right[1] * fStrFac * m_fWidth
									+ fNormOffset * v3Normal[1];
		m_fCurPos[5] = m_fCurPos[5] + v3Right[2] * fStrFac * m_fWidth
									+ fNormOffset * v3Normal[2];

		// Upper Right Corner.
		m_fCurPos[6] = m_fCurPos[6] + v3Right[0] * fStrFac * m_fWidth
									+ fNormOffset * v3Normal[0];
		m_fCurPos[7] = m_fCurPos[7] + v3Right[1] * fStrFac * m_fWidth
									+ fNormOffset * v3Normal[1];
		m_fCurPos[8] = m_fCurPos[8] + v3Right[2] * fStrFac * m_fWidth
									+ fNormOffset * v3Normal[2];

		// Upper Left Corner.
		m_fCurPos[9]  = m_fCurPos[9]  - v3Right[0] * fStrFac * m_fWidth 
									  + fNormOffset * v3Normal[0];
		m_fCurPos[10] = m_fCurPos[10] - v3Right[1] * fStrFac * m_fWidth 
									  + fNormOffset * v3Normal[1];
		m_fCurPos[11] = m_fCurPos[11] - v3Right[2] * fStrFac * m_fWidth 
									  + fNormOffset * v3Normal[2];

		// ------------------------------------------------------------------------ //
		// Update text label position.
		VistaVector3D v3Pos;

		v3Pos = VistaVector3D(m_fGlobalPos[3], m_fGlobalPos[4], m_fGlobalPos[5]) - 
				1.1f * m_pGlobalMinTxt->GetTextSize() * VistaVector3D(v3Up[0], v3Up[1], v3Up[2]);
		m_pGlobalMinTxt->SetPosition(v3Pos);
		
		v3Pos = VistaVector3D(m_fGlobalPos[6], m_fGlobalPos[7], m_fGlobalPos[8]) + 
				0.1f * m_pGlobalMinTxt->GetTextSize() * VistaVector3D(v3Up[0], v3Up[1], v3Up[2]);
		m_pGlobalMaxTxt->SetPosition(v3Pos);
		
		v3Pos = VistaVector3D(m_fCurPos[3], m_fCurPos[4], m_fCurPos[5]) - 
				0.0f * m_pGlobalMinTxt->GetTextSize() * VistaVector3D(v3Up[0], v3Up[1], v3Up[2]);
		m_pCurMinTxt->SetPosition(v3Pos);
		
		v3Pos = VistaVector3D(m_fCurPos[6], m_fCurPos[7], m_fCurPos[8]) - 
				1.0f * m_pGlobalMinTxt->GetTextSize() * VistaVector3D(v3Up[0], v3Up[1], v3Up[2]);
		m_pCurMaxTxt->SetPosition(v3Pos);
	}

	m_pGlobalMinTxt->Update();
	m_pGlobalMaxTxt->Update();
	m_pCurMinTxt->Update();
	m_pCurMaxTxt->Update();
}

void VflUnsteadyLUT::UpdateTexture() const
{
	// ------------------------------------------------------------------------ //
	// Read the ranges.
	VveDataFieldInfo *pCur = 
		m_pDataSetInfo->GetChildChunkInfo(m_iCurTimeIndex)->GetFieldInfo(m_iActiveField);

	double dCurMin = pCur->GetMin(0);
	double dCurMax = pCur->GetMax(0);

	// ------------------------------------------------------------------------ //
	// Rebuild the lookup texture.

	float	fColors[1024];
	double	dColor[3];

	double dStep	= (dCurMax - dCurMin) / 256.0;
	double dPos		= dCurMin + 0.5 * dStep;
	

	for(int i=0; i<256; ++i, dPos += dStep)
	{
		this->GetColor(dPos, dColor);
		fColors[4*i+0] = float(dColor[0]);
		fColors[4*i+1] = float(dColor[1]);
		fColors[4*i+2] = float(dColor[2]);

		fColors[4*i+3] = float(this->GetOpacity(dPos));
	}
	
	
	glBindTexture(GL_TEXTURE_1D, m_uiLUTColorTxt);
	glTexSubImage1D(GL_TEXTURE_1D, 0, 0, 256, GL_RGBA, GL_FLOAT, fColors);
	glBindTexture(GL_TEXTURE_1D, 0);
}


/*============================================================================*/
/* vtkLookupTable Wrapper                                                     */
/*============================================================================*/

void VflUnsteadyLUT::SetRamp(int iRamp) const {
	m_pLUT->SetRamp(iRamp);
}

void VflUnsteadyLUT::SetRampToLinear() const {
	m_pLUT->SetRampToLinear();
}

void VflUnsteadyLUT::SetRampToSCurve() const {
	m_pLUT->SetRampToSCurve();
}

void VflUnsteadyLUT::SetRampToSQRT() const {
	m_pLUT->SetRampToSQRT();
}

int VflUnsteadyLUT::GetRamp() const {
	return m_pLUT->GetRamp();
}


void VflUnsteadyLUT::SetScale(int iScale) const {
	m_pLUT->SetScale(iScale);
}

void VflUnsteadyLUT::SetScaleToLinear() const {
	m_pLUT->SetScaleToLinear();
}

void VflUnsteadyLUT::SetScaleToLog10() const {
	m_pLUT->SetScaleToLog10();
}

int VflUnsteadyLUT::GetScale() const {
	return m_pLUT->GetScale();
}


void VflUnsteadyLUT::SetTableRange(double dMin, double dMax) const {
	m_pLUT->SetTableRange(dMin, dMax);
}

void VflUnsteadyLUT::SetTableRange(double dRange[2]) const {
	m_pLUT->SetTableRange(dRange);
}

void VflUnsteadyLUT::GetTableRange(double dRange[2]) const {
	m_pLUT->GetTableRange(dRange);
}

double* VflUnsteadyLUT::GetTableRange() const {
	return m_pLUT->GetTableRange();
}


void VflUnsteadyLUT::SetHueRange(double dMin, double dMax) const {
	m_pLUT->SetHueRange(dMin, dMax);
}

void VflUnsteadyLUT::SetHueRange(double dRange[2]) const {
	m_pLUT->SetHueRange(dRange);
}

void VflUnsteadyLUT::GetHueRange(double &dMin, double &dMax) const {
	m_pLUT->GetHueRange(dMin, dMax);
}

void VflUnsteadyLUT::GetHueRange(double dRange[2]) const {
	m_pLUT->GetHueRange(dRange);
}

double* VflUnsteadyLUT::GetHueRange() const {
	return m_pLUT->GetHueRange();
}


void VflUnsteadyLUT::SetSaturationRange(double dMin, double dMax) const {
	m_pLUT->SetSaturationRange(dMin, dMax);
}

void VflUnsteadyLUT::SetSaturationRange(double dRange[2]) const {
	m_pLUT->SetSaturationRange(dRange);
}

void VflUnsteadyLUT::GetSaturationRange(double &dMin, double &dMax) const {
	m_pLUT->GetSaturationRange(dMin, dMax);
}

void VflUnsteadyLUT::GetSaturationRange(double dRange[2]) const {
	m_pLUT->GetSaturationRange(dRange);
}

double* VflUnsteadyLUT::GetSaturationRange() const {
	return m_pLUT->GetSaturationRange();
}


void VflUnsteadyLUT::SetValueRange(double dMin, double dMax) const {
	m_pLUT->SetValueRange(dMin, dMax);
}

void VflUnsteadyLUT::SetValueRange(double dRange[2]) const {
	m_pLUT->SetValueRange(dRange);
}

void VflUnsteadyLUT::GetValueRange(double &dMin, double &dMax) const {
	m_pLUT->GetValueRange(dMin, dMax);
}

void VflUnsteadyLUT::GetValueRange(double dRange[2]) const {
	m_pLUT->GetValueRange(dRange);
}

double* VflUnsteadyLUT::GetValueRange() const {
	return m_pLUT->GetValueRange();
}


void VflUnsteadyLUT::SetAlphaRange(double dMin, double dMax) const {
	m_pLUT->SetAlphaRange(dMin, dMax);
}

void VflUnsteadyLUT::SetAlphaRange(double dRange[2]) const {
	m_pLUT->SetAlphaRange(dRange);
}

void VflUnsteadyLUT::GetAlphaRange(double &dMin, double &dMax) const {
	m_pLUT->GetAlphaRange(dMin, dMax);
}

void VflUnsteadyLUT::GetAlphaRange(double dRange[2]) const {
	m_pLUT->GetAlphaRange(dRange);
}

double* VflUnsteadyLUT::GetAlphaRange() const {
	return m_pLUT->GetAlphaRange();
}


int VflUnsteadyLUT::GetIndex(double x) const {
	return m_pLUT->GetIndex(x);
}

void VflUnsteadyLUT::SetNumberOfTableValues(int iNumber) const {
	m_pLUT->SetNumberOfTableValues(iNumber);
}

int VflUnsteadyLUT::GetNumberOfTableValues() const {
	return m_pLUT->GetNumberOfTableValues();
}

void VflUnsteadyLUT::GetTableValue(double x, double rgba[4]) const {
	m_pLUT->GetTableValue(int(x), rgba);
}


unsigned char* VflUnsteadyLUT::WritePointer(int iID, 
													int iNumber) const {
	return m_pLUT->WritePointer(iID, iNumber);
}

unsigned char* VflUnsteadyLUT::GetPointer(int iID) const {
	return m_pLUT->GetPointer(iID);
}


double* VflUnsteadyLUT::GetRange() const {
	return m_pLUT->GetRange();
}

void VflUnsteadyLUT::SetRange(double dMin, double dMax) const {
	m_pLUT->SetRange(dMin, dMax);
}

void VflUnsteadyLUT::SetRange(double dRange[2]) const {
	m_pLUT->SetRange(dRange);
}


void VflUnsteadyLUT::SetNumberOfColors(int iNum) const {
	m_pLUT->SetNumberOfColors(iNum);
}

int VflUnsteadyLUT::GetNumberOfColors() const {
	return m_pLUT->GetNumberOfColors();
}


void VflUnsteadyLUT::SetTable(vtkUnsignedCharArray *table) const {
	m_pLUT->SetTable(table);
}

vtkUnsignedCharArray* VflUnsteadyLUT::GetTable() const {
	return m_pLUT->GetTable();
}


void VflUnsteadyLUT::MapScalarsThroughTable2(void *vInput,
													 unsigned char *ubOutput,
													 int iInputDataType,
													 int iNumberOfValues,
													 int iInputIncrement,
													 int iOutputIncrement)
													 const {
	m_pLUT->MapScalarsThroughTable2(vInput,
		ubOutput, iInputDataType, iNumberOfValues, iInputIncrement,
		iOutputIncrement);
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
