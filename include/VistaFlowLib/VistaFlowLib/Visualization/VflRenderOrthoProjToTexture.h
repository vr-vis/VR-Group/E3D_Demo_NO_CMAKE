/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLRENDERORTHOPROJTOTEXTURE_H
#define _VFLRENDERORTHOPROJTOTEXTURE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "Vfl2DTextureGenerator.h"

#include "VflRenderToTexture.h"

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VistaTexture;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This is an interface for all classes that generates 2D texture by orthogonally
 * projecting given IVflRenderable.
 *
 * OUTPUT: VistaTexture
 */
class VISTAFLOWLIBAPI IVflRenderOrthoProjToTexture : public IVfl2DTextureGenerator
{
public:
	virtual ~IVflRenderOrthoProjToTexture();

	/**
	 * Access for output texture
	 */
	virtual VistaTexture* GetOutput() const;

	void GetOrthoParams(float &fLeft, float &fRight, 
						float &fBottom, float &fTop,
						float &fNear, float &fFar);
	
	void RegisterTarget(IVflRenderable *pTarget);
	void UnregisterTarget(IVflRenderable *pTarget);

	virtual void SetTextureBackgroundColor(float fC[4]);
	
protected:
	IVflRenderOrthoProjToTexture( 
		VflRenderNode *pRenderNode,
		int iWidth    = 32  , int iHeight  = 32,
		float fLeft   =-1.0f, float fRight = 1.0f, 
		float fBottom =-1.0f, float fTop   = 1.0f,
		float fNear   = 1.0f, float fFar   =-1.0f);

//private:
	VflRenderToTexture					*m_pRenderToTexture;
	VflRenderToTexture::C2DViewAdapter	*m_pViewAdapter;
	VflRenderNode						*m_pRenderNode;
};

/*============================================================================*/
/* INLINE METHODS                                                             */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFLRENDERORTHOPROJTOTEXTURE_H


