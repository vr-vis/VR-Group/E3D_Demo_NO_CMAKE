/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#include "IVflTransformer.h"


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string SVflTransformerType = "IVflTransformer";


static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, IVflTransformer>
	      ("DOESTLSNAP", SVflTransformerType,
	      &IVflTransformer::GetDoesTimeLevelSnap),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool, bool,
	                    IVflTransformer>
		("DOESTLSNAP", SVflTransformerType,
		&IVflTransformer::SetDoesTimeLevelSnap),
	NULL
};

/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
IVflTransformer::IVflTransformer()
: m_bDoesTLSnap(true)
{
}

IVflTransformer::~IVflTransformer()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
std::string IVflTransformer::GetReflectionableType() const
{
	return SVflTransformerType;
}
	

int IVflTransformer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nRet = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(SVflTransformerType);
	return nRet+1;
}


bool IVflTransformer::GetDoesTimeLevelSnap() const
{
	return m_bDoesTLSnap;
}

bool IVflTransformer::SetDoesTimeLevelSnap(bool bDoesIt)
{
	if(compAndAssignFunc<bool>(bDoesIt, m_bDoesTLSnap) == 1)
	{
		Notify(MSG_TIMELEVELSNAP_CHANGE);
		return true;
	}
	return false;
}

std::string IVflTransformer::GetFactoryType()
{
	return std::string("<NONE>");
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetName                                                     */
/*                                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTarget                                                   */
/*                                                                            */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
