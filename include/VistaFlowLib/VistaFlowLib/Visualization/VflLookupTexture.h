/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VflLookupTexture_h
#define _VflLookupTexture_h

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include "VflLookupTable.h"
#include "../Data/VflObserver.h"

#include <VistaFlowLib/VistaFlowLibConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflLookupTexture contains a VistaTexture object, which can be shared
 * between several different visualization objects. It automatically
 * synchronizes with the associated VTK lookup table.
 */    
class VISTAFLOWLIBAPI VflLookupTexture : public VflObserver
{
public:
	// Interpolation mode: either GL_NEAREST or GL_LINEAR
	explicit VflLookupTexture(IVflLookupTable *pTable = NULL, 
		int iInterpolationMode = GL_NEAREST, bool bManageLut = false);

    virtual ~VflLookupTexture();


    /**
     * Returns a pointer to the shared 1D lookup texture.
     */
    VistaTexture* GetLookupTexture() const;

	/**
	 * Returns a pointer to the 2D preintegration texture
	 */
	VistaTexture* GetPreIntegrationTexture() const;

	/**
	 * Retrieve the lookup table.
	 */
	IVflLookupTable* GetLookupTable() const;
	void SetLookupTable(IVflLookupTable *pLut, bool bManageLut = false);

	/**
	 * Find out, if the lookup table contains transparent values.
	 */
	bool GetHasTransparencies() const;

	/**
	 * React on changes of the underlying lookup table.
	 */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg,
		int ticket);
	  
	/**
	 *Returns wether updating of preintegration table is enabled
	 */
	const bool GetUpdatePreIntegration() const;
  
	/**
	 * Set wether to update preintegration table on lookup table change or not
	 */
	void SetUpdatePreIntegration(const bool state);

	/**
	 * Call this function to update the lookup texture after the
	 * VflSharedVtkLookupTable's underlying vtkLookupTable has been modified
	 * directly.
	 *
	 * @sa GetLookupTable(), 
	 */
	void ForceUpdate();

protected:
	void AllocateTexture();
	void SynchTexture();
	/** 
	 * Creates table of preintegrated values and updates corresponding texture 
	 * which can be accessed via GetPreIntegrationTexture(). 
	 * Preintegration is disabled by default, call SetUpdatePreIntegration() to 
	 * turn updating of preintegration table on/off.
	 *
	 * @param values Floating point array of RGBA values in the range of
	 *		  [0.0, 1.0].
	 * @param num_values Number of RGBA entries in parameter values, currently
	 *		  only a value of 256 is supported.
	 */
	void CreatePreIntegrationTable( const std::vector<VistaColor>& vecColors );

	VistaTexture* m_pTexture;
	VistaTexture* m_pPreIntegrationTexture;
		
	IVflLookupTable* m_pLut;
	
	bool m_bHasTransparencies;
	bool m_bManageLut;
	int  m_iInterpolationMode;
	bool m_bUpdatePreIntegration;
};

#endif // Include guard
