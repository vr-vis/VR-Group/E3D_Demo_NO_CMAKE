/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <GL/glew.h>


#include "VflRenderNode.h"
#include "VflRenderToTexture.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaFramebufferObj.h>
#include <VistaOGLExt/VistaRenderbuffer.h>

#include <VistaInterProcComm/Concurrency/VistaMutex.h>
#include <VistaBase/VistaVectorMath.h>

#include <cstring>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION FOR VflRenderToTexture                                   */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflRenderToTexture::VflRenderToTexture(int iWidth, int iHeight,
										 bool bDoMipMap /* = false */)
	:	m_iWidth(0),
		m_iHeight(0),
		m_pBuffer(new VistaFramebufferObj),
		m_pTexture(NULL),
		m_pRenBuffer(NULL),
		m_pTarget(NULL),
		m_bInited(false)
{
	if(!this->Resize(iWidth, iHeight, bDoMipMap))
	{
		vstr::warnp() << "[VflRenderToTExture] FBO not initialized!" << endl<<endl;
	}
	//init background color
	m_fBackColor[0] = m_fBackColor[1] = m_fBackColor[2] = 0.0f;
	m_fBackColor[3] = 1.0f;

#ifdef DEBUG
	vstr::debugi() << "[VflRenderToTexture] Init FBO SUCCESS!!" << endl<<endl;
#endif
}

VflRenderToTexture::~VflRenderToTexture()
{
	//disable and dismantle the FBO
	m_pBuffer->Detach(GL_COLOR_ATTACHMENT0_EXT);
	m_pBuffer->Detach(GL_DEPTH_ATTACHMENT_EXT);
	
	//cleanup the parts
	if(m_pTexture)
		delete m_pTexture;
	m_pTexture = 0;

	if(m_pRenBuffer)
		delete m_pRenBuffer;
	m_pRenBuffer = 0;

	if(m_pBuffer)
		delete m_pBuffer;
	m_pBuffer = 0;

	m_pTarget = NULL;

	//
	if(GetRenderNode())
		GetRenderNode()->RemoveRenderable(this);
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Resize                                                  */
/*                                                                            */
/*============================================================================*/
bool VflRenderToTexture::Resize(int iWidth, int iHeight,
								 bool bDoMipMap /* = false */)
{
	if(iWidth < 1 || iHeight < 1 || (iWidth == m_iWidth && iHeight == m_iHeight))
	{
		return false;
	}

	m_bDoMipMap = bDoMipMap;
	m_iWidth	= iWidth;
	m_iHeight	= iHeight;

	//bind the render buffer to the FBO in order to provide it a depth buffer
	if(m_pRenBuffer)
	{
		m_pBuffer->Detach(GL_DEPTH_ATTACHMENT_EXT);
		delete m_pRenBuffer;
	}

	//allocate space for the tex image
	if(!m_pTexture)
	{
		//NOTE: Texture is bound after construction
		m_pTexture = new VistaTexture(GL_TEXTURE_2D);
	}

	m_pTexture->Bind();
	glTexImage2D(m_pTexture->GetTarget(), 0, GL_RGBA8, m_iWidth, m_iHeight, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	
	if(m_bDoMipMap)
	{
		glTexParameterf(m_pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glGenerateMipmapEXT(GL_TEXTURE_2D);
	}
	else
	{
		glTexParameterf(m_pTexture->GetTarget(), GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}

    glTexParameterf(m_pTexture->GetTarget(), GL_TEXTURE_MAG_FILTER, GL_LINEAR);                
    glTexParameterf(m_pTexture->GetTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(m_pTexture->GetTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP);
	glBindTexture(m_pTexture->GetTarget(), 0);	
	
	m_pRenBuffer =
		new VistaRenderbuffer(GL_DEPTH_COMPONENT, m_iWidth, m_iHeight);
	
	//bind texture to FBO in order to have the "canvas" that we want to draw to
	m_pBuffer->Attach(m_pTexture, GL_COLOR_ATTACHMENT0_EXT);
	m_pBuffer->Attach(m_pRenBuffer, GL_DEPTH_ATTACHMENT_EXT);

	//finally check if we are good to go
	GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	m_bInited = status == GL_FRAMEBUFFER_COMPLETE_EXT;

	return m_bInited;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSize	                                                  */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::GetSize(int &iWidth, int &iHeight)
{
	iWidth	= m_iWidth;
	iHeight	= m_iHeight;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDoMipMap                                                 */
/*                                                                            */
/*============================================================================*/
bool VflRenderToTexture::GetDoMipMap() const
{
	return m_bDoMipMap;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTexture                                                  */
/*                                                                            */
/*============================================================================*/
VistaTexture *VflRenderToTexture::GetTexture() const
{
	return m_pTexture;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTarget                                                   */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::SetTarget(IVflRenderable *pRenderable)
{
	VflRenderNode *pRN = this->GetRenderNode();
	//unregister because drawing mode might change
	if(pRN)
		pRN->RemoveRenderable(this);
	
	m_pTarget = pRenderable;

	if(m_pTarget)
		m_pTarget->SetRenderNode(pRN);
	
	//register because drawing mode might have changed
	if(pRN && m_pTarget)
		pRN->AddRenderable(this);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTarget                                                   */
/*                                                                            */
/*============================================================================*/
IVflRenderable *VflRenderToTexture::GetTarget() const
{
	return m_pTarget;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBackgroundColor                                          */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::SetBackgroundColor(float fColor[3],
											 float fAlpha /* = 1.0f */)
{
	memcpy(m_fBackColor, fColor, 3*sizeof(m_fBackColor[0]));
	m_fBackColor[3] = fAlpha;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetbackgroundColor                                          */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::GetBackgroundColor(float fColor[4]) const
{
	memcpy(fColor, m_fBackColor, 4*sizeof(m_fBackColor[0]));
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflRenderToTexture::GetType() const
{
	return string("VflRenderToTexture");
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VflRenderToTexture::GetBounds(VistaVector3D &minBounds, 
	                   VistaVector3D &maxBounds)
{
	//we do not draw into the normal screen space so we do not count for bounds!
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::Update()
{
	if(m_pTarget != NULL)
		m_pTarget->Update();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw*                                                       */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::DrawOpaque()
{
	//make sure the FBO is set up correctly (cf. constructor)
	if(!m_bInited || m_pTarget==NULL || !this->GetProperties()->GetVisible())
		return;

	this->SetupRendering();

	//call the target rendering code
	m_pTarget->DrawOpaque();

	if(m_bDoMipMap)
	{
		m_pTexture->Bind();
		glGenerateMipmapEXT(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	this->CleanupRendering();
}

void VflRenderToTexture::DrawTransparent()
{
	//make sure the FBO is set up correctly (cf. constructor)
	if(!m_bInited || m_pTarget==NULL || !this->GetProperties()->GetVisible())
		return;

	this->SetupRendering();

	//call the target rendering code
	m_pTarget->DrawTransparent();

	if(m_bDoMipMap)
	{
		m_pTexture->Bind();
		glGenerateMipmapEXT(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	this->CleanupRendering();
}

void VflRenderToTexture::Draw2D()
{
	//make sure the FBO is set up correctly (cf. constructor)
	if(!m_bInited || m_pTarget==NULL || !this->GetProperties()->GetVisible())
		return;

	this->SetupRendering();

	//call the target rendering code
	m_pTarget->Draw2D();

	if(m_bDoMipMap)
	{
		m_pTexture->Bind();
		glGenerateMipmapEXT(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	this->CleanupRendering();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflRenderToTexture::GetRegistrationMode() const
{
	if(m_pTarget)
		return m_pTarget->GetRegistrationMode();

	vstr::warnp() << "[VflRenderToTexture] m_pTarget not set yet" << endl;
	return OLI_DRAW_OPAQUE;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRenderNode												  */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::SetRenderNode(VflRenderNode *pRN)
{
	IVflRenderable::SetRenderNode(pRN);
	if(m_pTarget)
		m_pTarget->SetRenderNode(pRN);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* 
VflRenderToTexture::CreateProperties() const
{
	return new VflRenderableProperties;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetupRendering                                              */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::SetupRendering()
{
	glGetIntegerv(GL_DRAW_BUFFER, &m_iOldDrawBuffer);
	//enable and bind the render target
	glBindTexture(GL_TEXTURE_2D, 0); 
	m_pBuffer->Bind();
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);
	//safe viewport setup for later 
	glPushAttrib(GL_VIEWPORT_BIT | GL_ENABLE_BIT);
	glViewport(0,0,m_iWidth, m_iHeight);

	glClearColor(m_fBackColor[0], m_fBackColor[1], m_fBackColor[2],
		m_fBackColor[3]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
	glEnable(GL_TEXTURE_2D);
	//glDisable(GL_DEPTH_TEST);
	//glDisable(GL_CULL_FACE);
	//glDisable(GL_LIGHTING);
	//glDisable(GL_BLEND);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CleanupRendering                                            */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::CleanupRendering()
{
	//cleanup FBO
	m_pBuffer->Release();
	//restore draw buffer
	glDrawBuffer(m_iOldDrawBuffer);
	glPopAttrib();
}

/*============================================================================*/
/*  IMPLEMENTATION FOR VflRenderToTexture                                   */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflRenderToTexture::C2DViewAdapter::C2DViewAdapter()
{
	m_fOrthoParams[0] = 0.0f;
	m_fOrthoParams[1] = 1.0f;
	m_fOrthoParams[2] = 0.0f;
	m_fOrthoParams[3] = 1.0f;
	m_fOrthoParams[4] = 0.0f;
	m_fOrthoParams[5] = 1.0f;

	m_pTargetListLock = new VistaMutex;
}
VflRenderToTexture::C2DViewAdapter::~C2DViewAdapter()
{
	delete m_pTargetListLock;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RegisterTarget                                              */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::C2DViewAdapter::RegisterTarget(IVflRenderable *pRenderable)
{
	if(!pRenderable)
	{
		vstr::errp() << " [VflRenderToTexture::C2DViewAdapter] given renderable" << endl;
		return;
	}

	m_pTargetListLock->Lock();             // CRITICAL SECTION - BEGIN
	m_liTargets.push_back(pRenderable);
	pRenderable->SetRenderNode(this->GetRenderNode());
	m_pTargetListLock->Unlock();           // CRITICAL SECTION - END
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UnregisterTarget                                            */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::C2DViewAdapter::UnregisterTarget(IVflRenderable *pRenderable)
{
	if(!pRenderable)
	{
		vstr::errp() << " [VflRenderToTexture::C2DViewAdapter] given renderable" << endl;
		return;
	}

	m_pTargetListLock->Lock();            // CRITICAL SECTION - BEGIN
	m_liTargets.remove(pRenderable);
	pRenderable->SetRenderNode(NULL);
	m_pTargetListLock->Unlock();          // CRITICAL SECTION - END
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetOrthoParams                                              */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::C2DViewAdapter::SetOrthoParams(
									float fLeft, float fRight, float fBottom, 
									float fTop, float fNear, float fFar)
{
	m_fOrthoParams[0] = fLeft;
	m_fOrthoParams[1] = fRight;
	m_fOrthoParams[2] = fBottom;
	m_fOrthoParams[3] = fTop;
	m_fOrthoParams[4] = fNear;
	m_fOrthoParams[5] = fFar;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetOrthoParams                                              */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::C2DViewAdapter::GetOrthoParams(
									float &fLeft, float &fRight, float &fBottom, 
									float &fTop, float &fNear, float &fFar) const
{
	fLeft   = m_fOrthoParams[0];
	fRight  = m_fOrthoParams[1];
	fBottom = m_fOrthoParams[2];
	fTop    = m_fOrthoParams[3];
	fNear   = m_fOrthoParams[4];
	fFar    = m_fOrthoParams[5];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::C2DViewAdapter::Update()
{
	const size_t nNumOfTargets = m_liTargets.size();
	list <IVflRenderable *>::iterator it = m_liTargets.begin();
	for(size_t i=0; i<nNumOfTargets; ++i)
	{
		if(*it != NULL)
			(*it)->Update();
		++it;
	}

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw*                                                       */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::C2DViewAdapter::Draw2D()
{
	this->SetupProjection();

	//call the targets's rendering code

	m_pTargetListLock->Lock();            // CRITICAL SECTION - BEGIN

	list<IVflRenderable*>::iterator it = m_liTargets.begin();
	list<IVflRenderable*>::iterator itEnd = m_liTargets.end();

	while(it != itEnd)
	{
		if(*it != NULL)
		{
			(*it)->DrawOpaque();
			(*it)->DrawTransparent();
			(*it)->Draw2D();
		}
		++it;
	}
	m_pTargetListLock->Unlock();          // CRITICAL SECTION - END

	this->CleanupProjection();
}/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetupProjection                                             */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::C2DViewAdapter::SetupProjection()
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glOrtho(m_fOrthoParams[0],m_fOrthoParams[1],m_fOrthoParams[2],
		m_fOrthoParams[3],m_fOrthoParams[4],m_fOrthoParams[5]);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CleanupProjection                                           */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::C2DViewAdapter::CleanupProjection()
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflRenderToTexture::C2DViewAdapter::GetRegistrationMode() const
{
	//check if target needs update
	//int iTargetReg = (m_pTarget != NULL ? 
		//m_pTarget->GetRegistrationMode() & OLI_UPDATE : 0);
	
	/*
	unsigned int iTargetReg = 0;
	m_pTargetListLock->Lock();            // CRITICAL SECTION - BEGIN
	int iNumOfTargets = m_liTargets.size();
	list<IVflRenderable*>::const_iterator it = m_liTargets.begin();
	for(int i=0; i<iNumOfTargets; ++i)
	{
		if(*it != NULL)
			iTargetReg = iTargetReg | (*it)->GetRegistrationMode();
		++it;
	}
	m_pTargetListLock->Unlock();          // CRITICAL SECTION - END
	
	iTargetReg = iTargetReg & OLI_UPDATE;

	//register for 2D and update if needed
	return OLI_DRAW_2D | iTargetReg;
	*/

	//we render 2D and need an update anyway
	return OLI_DRAW_2D | OLI_UPDATE;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetVisController                                            */
/*                                                                            */
/*============================================================================*/
void VflRenderToTexture::C2DViewAdapter::SetRenderNode(VflRenderNode *pRN)
{
	IVflRenderable::SetRenderNode(pRN);

	list<IVflRenderable*>::iterator it = m_liTargets.begin();
	list<IVflRenderable*>::iterator itEnd = m_liTargets.end();

	while(it != itEnd)
	{
		if(*it != NULL)
			(*it)->SetRenderNode(pRN);
		++it;
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties* 
VflRenderToTexture::C2DViewAdapter::CreateProperties() const
{
	return new VflRenderableProperties;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflRenderToTexture.cpp"                                      */
/*============================================================================*/
