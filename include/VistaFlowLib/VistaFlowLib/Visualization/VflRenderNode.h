/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLRENDERNODE_H
#define _VFLRENDERNODE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <VistaAspects/VistaNameable.h>
#include <VistaBase/VistaVectorMath.h>

#include <string>
#include <list>

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/
class IVistaNodeBridge;
class VflVisController;
class IVflRenderable;
class IVistaTransformable;
class VistaOpenGLNode;
class VistaGroupNode;
class VistaTransformNode;
class VflVisTiming;
class VistaMutex;

class vtkRenderer;
class vtkRenderWindow;


/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
/*!
 * TODO_HIGH: Find a nice description for this class.
 */
class VISTAFLOWLIBAPI VflRenderNode : public IVistaNameable
{
	friend class VflVisController;

public:
	/*------------------------------------------------------------------------*/
	/* Con-/Destructors														  */
	/*------------------------------------------------------------------------*/
	//! The one and only constructor for a render node.
	/*!
	 * \param pParentNode The scene graph parent node of this render node.
	 * \param pNodeBridge The node bridge from the VistaSystem.
	 */
	VflRenderNode(VistaGroupNode *pParentNode, IVistaNodeBridge *pNodeBridge,
		vtkRenderWindow *pVtkRenderWindow = 0);
	//!
	virtual ~VflRenderNode();


	/*------------------------------------------------------------------------*/
	/* IVistaNameable Interface												  */
	/*------------------------------------------------------------------------*/
	inline virtual void SetNameForNameable(const std::string &sName);
	inline virtual std::string GetNameForNameable() const;


	/*------------------------------------------------------------------------*/
	/* RenderNode & Renderable Management									  */
	/*------------------------------------------------------------------------*/
	VflVisController* GetVisController() const;
	
	//! Adds a renderable to the render node.
	/*!
	 * \param pRen The renderable to be added.
	 * \return Returns 'true' iff the renderable was added, 'false' otherwise.
	 */
	bool AddRenderable(IVflRenderable *pRen);
	//! Removes a renderable from the render node.
	/*!
	 * \param pRen The renderable to be removed.
	 * \return Returns 'true' iff the renderable was actually removed, 'false'
	 *		   otherwise.
	 */
	bool RemoveRenderable(IVflRenderable *pRen);

	int GetNumberOfRenderables() const;

	IVflRenderable * GetRenderable( const unsigned int i );

	//! Updates every Renderable's VisTiming and calls its Update() function.
	bool Update(double dTimeStamp);

	//! Calls the draw functions of all managed renderables.
	bool DrawRenderables();

	VflVisTiming* GetVisTiming() const;

	//! Set the vis timing with an existing vis timing, e.g. to have sync render nodes.
	/*!
	 * \param pVisTiming the existing vis timing
	 * \param bCleanup whether you wish to delete the old vis timing, default: true
	 */
	void SetVisTiming(VflVisTiming *pVisTiming);
	/*------------------------------------------------------------------------*/
	/* Transformation Management											  */
	/*------------------------------------------------------------------------*/
	bool ResetTransformation();
	
	//! Returns a pointer to the transformable used for the RenderNode.
	/*!
	 * NOTE: Any transformation should be applied using the below interface
	 *		 instead of using the transformable directly as additional state
	 *		 management is performed when a transformation is applied.
	 *		 This getter is a convenience function which allow access to
	 *		 the transformable so it can be integrated into a DataFlowNetwork.
	 *		 Any other usage of this function is discouraged!0
	 */
	IVistaTransformable* GetTransformable() const;
	//! Returns a pointer to the transformable holding the 
	IVistaTransformable* GetVisRefPointTransformable() const;

	bool Translate(const VistaVector3D &v3Trans);
	bool SetTranslation(const VistaVector3D &v3Trans);
	VistaVector3D GetTranslation() const;
	bool GetTranslation(VistaVector3D &v3Trans) const;

	bool Rotate(const VistaQuaternion &qRot);
	bool SetRotation(const VistaQuaternion &qRot);
	VistaQuaternion GetRotation() const;
	bool GetRotation(VistaQuaternion &qRot);

	bool ScaleUniformly(float fScale);
	bool SetScaleUniform(float fScale);
	//! TODO_HIGH: Returns 'true' iff the scale is uniform, 'false' otherwise.
	// fScale is the value of the largest scale value.
	bool GetScaleUniform(float &fScale) const;
	float GetScaleUniform() const;
	
	bool Scale(float fXScale, float fYScale, float fZScale);
	bool SetScale(float fXScale, float fYScale, float fZScale);
	bool GetScale(float &fXScale, float &fYScale, float &fZScale) const;

	bool Transform(const VistaTransformMatrix &mTrans);
	bool SetTransform(const VistaTransformMatrix &mTrans);
	VistaTransformMatrix GetTransform() const;
	bool GetTransform(VistaTransformMatrix &mTrans) const;
	VistaTransformMatrix GetInverseTransform() const;
	bool GetInverseTransform(VistaTransformMatrix &mInvTrans) const;
	
	VistaTransformMatrix GetWorldTransform() const;
	bool GetWorldTransform(VistaTransformMatrix &mTrans) const;
	VistaTransformMatrix GetWorldInverseTransform() const;
	bool GetWorldInverseTransform(VistaTransformMatrix &mInvTrans) const;

	bool SetVisRefPoint(const VistaVector3D &v3RotCenter);
	VistaVector3D GetVisRefPoint() const;
	bool GetVisRefPoint(VistaVector3D &v3RotCenter);


	/*------------------------------------------------------------------------*/
	/* View Information														  */
	/*------------------------------------------------------------------------*/
		//! Tells the render node about the global position of the viewer.
	/*!
	 * This function informs the render node about the global viewer position
	 * so it can transform it into its local frame. This routine should usually
	 * not be called by a user.
	 *
	 * \sa The same is done in TellGlobalViewOrientation for the global
	 *	   view orientation.
	 *
	 * NOTE: Only the VisController is supposed to call this function!
	 */
	void TellGlobalViewPosition(const VistaVector3D &v3GlobalViewPos);
	
	//! Tells the render node the global view orientation.
	/*!
	 * \sa See TellGlobalViewPosition for an explanation of how this
	 *	   function works.
	 *
	 * NOTE: Only the VisController is supposed to call this function!
	 */ 
	void TellGlobalViewOrientation(const VistaQuaternion &qGlobalViewOri);

	//! Retrieves the viewer's position in the render node's coordinate frame.
	VistaVector3D GetLocalViewPosition() const;
	void GetLocalViewPosition(VistaVector3D &v3LocalViewPos) const;

	//! Retrieves the view Dir in the render node's coordinate frame.
	VistaVector3D GetLocalViewDirection() const;
	void GetLocalViewDirection(VistaVector3D &v3LocalViewDir) const;
	
	//! Retrieves the view orientation in the render node's coordinate frame.
	VistaQuaternion GetLocalViewOrientation() const;
	void GetLocalViewOrientation(VistaQuaternion &qGlobalViewOri) const;


	/*------------------------------------------------------------------------*/
	/* Light Information													  */
	/*------------------------------------------------------------------------*/
	//! Tells the render node about the global light Dir.
	/*!
	 * The global light Dir is transformed into the render node's local
	 * coordinate frame on each update call.
	 *
	 * NOTE: Only the VisController is supposed to call this function!
	 */
	void TellGlobalLightDirection(const VistaVector3D &vec);

	//! Retrieves the local light dir in the render node's coordinate frame.
	VistaVector3D GetLocalLightDirection() const;
	void GetLocalLightDirection(VistaVector3D &vec) const;


	/*------------------------------------------------------------------------*/
	/* Frame Rate Control													  */
	/*------------------------------------------------------------------------*/
	//! En-/Disables frame rate capping.
	void SetPerformFrameRateCapping(bool bCap);
	bool GetPerformFrameRateCapping() const;

	//! Sets the frame rate cap by specifying a minimum frame time.
	void SetMinFrameTime(double dMinFrameTime);
	double GetMinFrameTime() const;

	//! Sets the frame rate cap by specifying it the maximum fps directly.
	void SetMaxFrameRate(float fFrameCap);
	float GetMaxFrameRate() const;


	/*------------------------------------------------------------------------*/
	/* Visualization Bounds													  */
	/*------------------------------------------------------------------------*/
	//! Retrieves the time stamp of the last bounds update.
	double GetBoundsLastRefreshTimeStamp() const;

	/**
	 * \return Returns 'true' iff the bounds have been recomputed, else 'false'.
	 *
	 * NOTE: The bounds are always supposed to be valid.
	 */
	bool GetVisBounds(VistaVector3D &v3Min, VistaVector3D &v3Max);

	/*!
	 * Recompute the boundaries of all registered renderables and forget about
	 * any bounds set by the user via OverrideVisBounds (if applicable).
	 */
	bool RecomputeVisBounds();

	/*!
	 * Manually set the boundaries of all renderables managed by this render
	 * node. Calling this methods stops the render node from automatically
	 * determining the spatial boundaries of all managed renderables. To
	 * re-enable an automatic computation, call RecomputeVisBounds().
	 *
	 * \sa See RecomputeVisBounds for more details.
	 */
	bool OverrideVisBounds(const VistaVector3D &v3Min,
						   const VistaVector3D &v3Max);

	/**
	 * Inform the vis ctrl of potential changes of spatial boundaries of
	 * registered vis objects. Calling this method leads to a re-compuation
	 * of the vis objects' boundaries, iff they haven't been overridden by
	 * OverrideVisBounds().
	 *
	 * \sa See OverrideVisBounds for more details.
	 */
	void SetBoundsToModified();

	//! Sets the bounds the scene depicted by this RenderNode should lie in.
	bool SetTargetBounds(const VistaVector3D &v3Min,
						 const VistaVector3D &v3Max);
	bool GetTargetBounds(VistaVector3D &v3Min,
						 VistaVector3D &v3Max) const;
	
	/*------------------------------------------------------------------------*/
	/* VTK Window & Renderer Management										  */
	/*------------------------------------------------------------------------*/
	//!
	void UpdateVtkWindowSize(int nWidth, int nHeight);
	//!
	vtkRenderer* GetVtkRenderer() const;


protected:
	/*------------------------------------------------------------------------*/
	/* RenderNode & Renderable Management									  */
	/*------------------------------------------------------------------------*/
	bool Init();
	bool SetVisController(VflVisController *pVisCtrl);


	/*------------------------------------------------------------------------*/
	/* Helper Functions														  */
	/*------------------------------------------------------------------------*/
	void RefreshMatrices();
	void RefreshBounds();


	/*------------------------------------------------------------------------*/
	/* Variables															  */
	/*------------------------------------------------------------------------*/
	bool							m_bIsValid;
	//! The vis controller which manages the render node (and its renderables).
	VflVisController				*m_pVisCtrl;
	//! Transform node which is used to be able to transform the render node.
	VistaTransformNode				*m_pTransformNode;
	//! Transform node to specify a rotation center for render node transforms.
	VistaTransformNode				*m_pVisRefPointNode;
	//! OpenGL node used for receiving renderable rendering calls.
	VistaOpenGLNode					*m_pOGLNode;
	//! List of all added renderables.
	std::list<IVflRenderable*>		m_liRenderables;
	std::list<IVflRenderable*>		m_liUpdateList;
	std::list<IVflRenderable*>		m_liDrawOpaqueList;
	std::list<IVflRenderable*>		m_liDrawTransparentList;
	std::list<IVflRenderable*>		m_liDraw2DList;

	VflVisTiming					*m_pVisTiming;
	bool							m_bHasLocalTiming;

	//! Manages access to the renderable list during add/remove/update/draw.
	VistaMutex						*m_pListMutex;

	std::string						m_sName;

	//! Should frame capping be engaged?!
	bool							m_bIsFrameRateCappingOn;
	//! The time a single frame should minimally last (1 / maxfps).
	double							m_dMinFrameTime;
	
	//! Time stamp of the last bounds update.
	double							m_dBoundsTimeStamp;


	/*------------------------------------------------------------------------*/
	/* Transformation Management											  */
	/*------------------------------------------------------------------------*/
	VistaTransformMatrix			m_mTrans,
									m_mInvTrans;
	
	VistaTransformMatrix			m_mWorldTrans,
									m_mWorldInvTrans;

	/*------------------------------------------------------------------------*/
	/* Bounds Management													  */
	/*------------------------------------------------------------------------*/
	bool							m_bBoundsValid,
									m_bBoundsSetByUser;
	VistaVector3D					m_v3BoundsMin,
									m_v3BoundsMax,
									m_v3TargetBoundsMin,
									m_v3TargetBoundsMax;


	/*------------------------------------------------------------------------*/
	/* Viewer & Light Info													  */
	/*------------------------------------------------------------------------*/
	VistaVector3D					m_v3ViewPosGlobal,
									m_v3ViewPosLocal,
									m_v3ViewDirGlobal,
									m_v3ViewDirLocal,
									m_v3LightDirGlobal,
									m_v3LightDirLocal;
	VistaQuaternion					m_qViewOriGlobal,
									m_qViewOriLocal;

	/*------------------------------------------------------------------------*/
	/* VTK Window & Renderer Management										  */
	/*------------------------------------------------------------------------*/
	vtkRenderer						*m_pVtkRenderer;
	vtkRenderWindow					*m_pVtkRenderWindow;
};

/*============================================================================*/
/* INLINE FUNCTIONS	                                                          */
/*============================================================================*/
/*============================================================================*/
/* IVISTANAMEABLE INTERFACE	                                                  */
/*============================================================================*/
inline void VflRenderNode::SetNameForNameable(const std::string &sName)
{
	m_sName = sName;
}

inline std::string VflRenderNode::GetNameForNameable() const
{
	return m_sName;

}


/*============================================================================*/
/* View Information															  */
/*============================================================================*/
inline void VflRenderNode::TellGlobalViewPosition(const VistaVector3D &v3Pos)
{
	m_v3ViewPosGlobal = v3Pos;
}

inline VistaVector3D VflRenderNode::GetLocalViewPosition() const
{
	return m_v3ViewPosLocal;
}

inline void VflRenderNode::GetLocalViewPosition(VistaVector3D &v3Pos) const
{
	v3Pos = m_v3ViewPosLocal;
}

inline VistaVector3D VflRenderNode::GetLocalViewDirection() const
{
	return m_v3ViewDirLocal;
}

inline void VflRenderNode::GetLocalViewDirection(VistaVector3D &v3Dir) const
{
	v3Dir = m_v3ViewDirLocal;
}

inline void VflRenderNode::TellGlobalViewOrientation(
	const VistaQuaternion & qViewOriGlobal)
{
	// Just in case: Save the normalized supplied quaternion as only normalized
	// quaternions can geometrically be interpreted as rotations.
	m_qViewOriGlobal = qViewOriGlobal.GetNormalized();

	// If an invalid quaternion was given (norm is NAN) fall back to a
	// "neutral" quaternion (0, 0, 0, 1).
	if(!(m_qViewOriGlobal.GetLengthSquared() == m_qViewOriGlobal.GetLengthSquared()))
		m_qViewOriGlobal = VistaQuaternion();
	
	// The default (initial) view Dir along the negative z-axis is
	// transformed by the global viewer orientation. As a result it matches
	// the viewer's actual _global_ view Dir.
	m_v3ViewDirGlobal =
		m_qViewOriGlobal.Rotate(VistaVector3D(0.0f, 0.0f, -1.0f));
	m_v3ViewDirGlobal.Normalize();
	m_v3ViewDirGlobal[3] = 0.0f;
}

inline VistaQuaternion VflRenderNode::GetLocalViewOrientation() const
{
	return m_qViewOriLocal;
}

inline void VflRenderNode::GetLocalViewOrientation(
	VistaQuaternion &qViewOriLocal) const
{
	qViewOriLocal = m_qViewOriLocal;
}

inline void VflRenderNode::TellGlobalLightDirection(
	const VistaVector3D &v3LightDirGlobal)
{
	m_v3LightDirGlobal = v3LightDirGlobal;

	// Normalize the incoming vector as Dirs are usually assumed to have
	// unit length.
	m_v3LightDirGlobal.Normalize();
	// Vectors need a 0 in their last component (points have a 1).
	m_v3LightDirGlobal[3] = 0.0f;

	// If a 0-vector was given, i.e. a vector that cannot be used as Dir,
	// we'll simply fall back to a default view Dir.
	if (m_v3LightDirGlobal.GetLengthSquared() == 0.0f)
	{
		m_v3LightDirGlobal = VistaVector3D(0.0f, 0.0f, -1.0f, 0.0f);
	}
}


/*============================================================================*/
/* Light Information														  */
/*============================================================================*/
inline VistaVector3D VflRenderNode::GetLocalLightDirection() const
{
	return m_v3LightDirLocal;
}

inline void VflRenderNode::GetLocalLightDirection(
	VistaVector3D &v3LightDirLocal) const
{
	v3LightDirLocal = m_v3LightDirLocal;
}


/*============================================================================*/
/* Frame Rate Control														  */
/*============================================================================*/
inline void VflRenderNode::SetPerformFrameRateCapping(bool bEnable)
{
	m_bIsFrameRateCappingOn = bEnable;
}

inline bool VflRenderNode::GetPerformFrameRateCapping() const
{
	return m_bIsFrameRateCappingOn;
}

inline void VflRenderNode::SetMinFrameTime(double dMinFrameTime)
{
	m_dMinFrameTime = dMinFrameTime;
}

inline double VflRenderNode::GetMinFrameTime() const
{
	return m_dMinFrameTime;
}

inline void VflRenderNode::SetMaxFrameRate(float fFrameCap)
{
	SetMinFrameTime(1.0 / double(fFrameCap));
}

inline float VflRenderNode::GetMaxFrameRate() const
{
	return 1.0f / float(GetMinFrameTime());
}

/*============================================================================*/
/* Visualization Bounds														  */
/*============================================================================*/
inline double VflRenderNode::GetBoundsLastRefreshTimeStamp() const
{
	return m_dBoundsTimeStamp;
}


/*============================================================================*/
/* END OF FILE	                                                              */
/*============================================================================*/
#endif // _VFLRENDERNODE
