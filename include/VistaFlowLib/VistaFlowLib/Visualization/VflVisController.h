/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVISCONTROLLER_H
#define _VFLVISCONTROLLER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaNameable.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <list>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class VistaSystem;
class VistaMutex;
class VistaVector3D;
class VistaQuaternion;

/*============================================================================*/
/* CLASS DEFINITION		                                                      */
/*============================================================================*/
class VISTAFLOWLIBAPI VflVisController : public IVistaNameable
{
public:
	VflVisController(VistaSystem *pVistaSystem);
	virtual ~VflVisController();
	
	void Update(double dTimeStamp);
	
	bool AddRenderNode(VflRenderNode *pRenderNode);
	bool RemoveRenderNode(VflRenderNode *pRenderNode);

	/**
	 * @return Registered RenderNode of given name if it exists and a name is
	 * defined, NULL else.
	 */
	VflRenderNode* const GetRenderNodeByName(const std::string &strName) const;

	void TellGlobalViewPosition(const VistaVector3D &v3ViewPosGlobal);
	void TellGlobalViewOrientation(const VistaQuaternion &qViewOriGlobal);
	void TellGlobalLightDirection(const VistaVector3D &v3LightDirGlobal);

	bool GetVisBounds(VistaVector3D &v3Min, VistaVector3D &v3Max);

	VistaSystem* GetVistaSystem() const;

	// *** IVistaNameable interface ***
	void SetNameForNameable(const std::string &sName);
	std::string GetNameForNameable() const;

private:
	VistaSystem *m_pVistaSystem;
	std::string	m_strName;
	std::list<VflRenderNode*> m_liRenderNodes;
	VistaMutex* m_pListMutex;
};

#endif // Incude guard
