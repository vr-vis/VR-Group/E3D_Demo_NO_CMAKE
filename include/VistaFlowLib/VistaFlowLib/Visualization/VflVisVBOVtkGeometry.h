/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VflVisVBOVtkGeometry_H
#define _VflVisVBOVtkGeometry_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <iostream>
#include <string>
#include "VflVisObject.h"
#include <VistaFlowLib/Data/VflObserver.h>
#include <VistaVisExt/Data/VveVtkData.h>
#include <VistaVisExt/Tools/VtkObjectStack.h>

#include <VistaBase/VistaVectorMath.h>
#include <VistaInterProcComm/Concurrency/VistaThreadPool.h>
#include <VistaInterProcComm/Concurrency/VistaThreadEvent.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVtkLookupTable;
class VflVtkProperty;
class vtkPolyData;
class vtkActor;
class vtkTexture;
class VveTimeMapper;
class VflUnsteadyDataFilter;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * CvflVisVBOVtkGeometry is an equivalent to VflVisVtkGeometry in that it 
 * renders simple vtk polydata. In contrast to its sibling it uses vertex buffer
 * objects to speed up rendering and remedy expensive display list generation.
 * NOTE: The geometry to be rendered has to be made up of either triangles or 
 *		 triangle strips in order for this renderer to work properly. Everything
 *		 else should be pretty much the same as VflVisVtkGeometry.
 *
 * @author Sebastian Pick
 * @date August, 2007
 *
 */    
class VISTAFLOWLIBAPI VflVisVBOVtkGeometry : public IVflVisObject
{
	friend class CVBOFiller; // Thread based filling algortihm for VBO rendering

public:

    // CONSTRUCTORS / DESTRUCTOR
    VflVisVBOVtkGeometry(int iNumOfVBOs);
    virtual ~VflVisVBOVtkGeometry();

	// public typedefs
	typedef std::vector<VflUnsteadyDataFilter *>::size_type FLTIDX;


    /**
     * Initialize object (and register with visualization controller).
     * 
     * @param[in]   VflVisController *pController		the visualization controller
	 * @param[in]	int iRegistration				callbacks to be registered for (default: NONE)
	 *                                          see VflVisController::OBJECT_LIST_MODE for more
     * @param[out]  --
     * @GLOBALS --
     * @return  bool							success of the operation
     *
     */    
    virtual bool Init();

	/** 
	 * Update the underlying data object. To be precise, the currently active
	 * time level gets updated.
	 */
	virtual void Update();

    /**
     * Draw opaque stuff. During rendering the underlying data object gets locked.
     * 
     * @param[in]   --
     * @param[out]  --
     * @GLOBALS --
     * @return  --
     *
     */    
    virtual void DrawOpaque();

    /**
     * Draw transparent stuff. During rendering the underlying data object gets locked.
     * 
     * @param[in]   --
     * @param[out]  --
     * @GLOBALS --
     * @return  --
     *
     */    
    virtual void DrawTransparent();

    /**
     * Returns the boundaries of the visualization object.
	 * Note: This method is not declared const, as it might change the
	 *       cached boundaries inside the object.
     * 
     * @param[out]  VistaVector3D minBounds    minimum of the object's AABB
	 * @param[out]  VistaVector3D maxBounds    maximum of the object's AABB
     * @return  bool	is the bounding box valid?
     */    
	virtual bool GetBounds(VistaVector3D &minBounds, 
		                   VistaVector3D &maxBounds);

    /**
     * Returns the name of the visualization object.
     * 
     * @param[in]   --
     * @return  std::string
     */    
	virtual std::string GetType() const;

    /**
     * Prints out some debug information to the given output stream.
     * 
     * @param[in]   std::ostream & out
     * @return  --
     */    
    virtual void Debug(std::ostream & out) const;

    /**
     * Retrieve the vtkProperty object, which is shared by all actors inside
	 * this object.
     * 
     * @param[in]   ---
     * @return  vtkProperty *     the corresponding property object
     */    
	VflVtkProperty *GetProperty() const;

    /**
     * Set the vtkProperty object, which is to be shared by all actors inside
	 * this object. Note, that this is only possible as long as Init() has not
	 * been called.
     * 
     * @param[in]   vtkProperty * pProperty		the property object to be used for drawing
     * @return  bool						Has the operation been successful?
     */    
	bool SetProperty(VflVtkProperty *);

    /**
     * Retrieve the vtkProperty object, which is shared by all actors inside
	 * this object, and which is used for backface rendering.
     * 
     * @param[in]   ---
     * @return  vtkProperty *     the corresponding property object
     */    
	VflVtkProperty *GetBackfaceProperty() const;

    /**
     * Set the vtkProperty object, which is to be shared by all actors inside
	 * this object for rendering backfaces. Note, that this is only possible as long 
	 * as Init() has not been called.
     * 
     * @param[in]   vtkProperty * pProperty		the property object to be used for backface drawing
     * @return  bool						Has the operation been successful?
     */    
	bool SetBackfaceProperty(VflVtkProperty *);

	 /**
     * Retrieve the vtkLookupTable object, which is to be used by this
	 * visualization object.
     *
     * @param[in]   ---
     * @return  vtkLookupTable *     the corresponding lookup table
     */    
	VflVtkLookupTable *GetLookupTable() const;

    /**
     * Set the vtkLookupTable object, which is to be shared by all actors inside
	 * this object. Note, that this is only possible as long as Init() has not
	 * been called.
     * 
     * @param[in]   vtkLookupTable *pLookupTable	the lookup table to be used for drawing
     * @return  bool							Has the operation been successful?
     */    
	bool SetLookupTable(VflVtkLookupTable *);


    /**
     * Get a pointer to the unsteady data object to be visualized.
     * 
     * @param[in]   ---
     * @return  VveUnsteadyVtkPolyData*     the corresponding data
     */    
	VveUnsteadyVtkPolyData *GetData() const;

    /**
     * Set the unsteady data object to be visualized. Note, that this is only
	 * possible as long as Init() has not been called!
     * 
     * @param[in]   VveUnsteadyVtkPolyData *pData  the data to be visualized
     * @return  bool                                  Operation successful?
     */    
	bool SetData(VveUnsteadyVtkPolyData *pData);

	bool      SetUnsteadyData(VveUnsteadyData *);

    /**
     * The callback method from the IVistaObserver class, which is to be
	 * used for notificatoins concerning data changes.
     */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	void AddFilter(VflUnsteadyDataFilter *pFilter);
	void RemoveFilter(FLTIDX iIndex);

	VflUnsteadyDataFilter *GetFilter(FLTIDX  iIndex) const;
	FLTIDX                  GetFilterCount() const;
	FLTIDX                  GetFilterIndex(VflUnsteadyDataFilter *pFilter) const;	

	enum ETransferMode
    {
		TM_NONE                 = -1,
        TM_IMMEDIATE_MODE       =  0,
        TM_DISPLAY_LIST         =  1,
        TM_VERTEX_ARRAY         =  2,
        TM_VERTEX_BUFFER_OBJECT =  3,
        TM_LAST_MODE            =  4
    };

	/**
	 * @param pTexture a pointer to a valid vtkTexture that will be set to every vtkActor for this instance.
	 * @return true iff the texture was sucessfully applied to all vtkActors
	 *
	 */
	bool SetTexture(vtkTexture *pTexture);

	/**
	 * This methode retrieves the texture from the *first* vtkActor and
	 * returns it. This is valid, as SetTexture simple multiplex the texture
	 * on every element of the UnsteadyData/vtkActor.
	 * @return the texture that was set with SetTexture(). 
	 */
	vtkTexture *GetTexture() const;

	class VISTAFLOWLIBAPI VflVtkGeometryProperties : public VflVisObjProperties
	{
	public:
		VflVtkGeometryProperties();
		virtual ~VflVtkGeometryProperties();
		
		std::string GetReflectionableType() const;

		bool GetScalarVisibility() const;
		bool SetScalarVisibility(bool bScalarVisibility);

		bool GetImmediateMode() const;
		bool SetImmediateMode(bool bImmediateMode);

		ETransferMode  GetTransferModeId() const;
		bool SetTransferModeId(ETransferMode nTransferMode);

		std::string GetTransferMode() const;
		bool SetTransferMode(const std::string &sTransferMode);

		enum
		{
			MSG_SCALARVISIBILITY_CHANGE = VflVisObjProperties::MSG_LAST,
			MSG_IMMEDIATEMODE_CHANGE,

			MSG_TRANSFERMODE_CHANGE,

			MSG_LAST
		};

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	private:
		VflVtkGeometryProperties( const VflVtkGeometryProperties &) {}
		VflVtkGeometryProperties &operator=(const VflVtkGeometryProperties &) 
		{
			return *this;
		}
		bool m_bScalarVisibility, 
			 m_bImmediateMode;
		// holds information about the way to transfer geometry to the
		// OpenGL pipeline (Immediate mode, display lists, vertex arrays, 
		// vertex buffer objects)
		ETransferMode m_iTransfermode;


		friend class VflVisVBOVtkGeometry;
	};

	static std::string   GetFactoryType();
private:


	enum
	{

		E_FILTER_TICKET = IVflVisObject::E_TICKET_LAST,
		E_LEVELDATA_TICKET,
		E_TICKET_LAST
	};


	virtual VflVisObjProperties    *CreateProperties() const;
	vtkPolyData * GetFilteredPolyData( int iIndex );


	bool HandlePropertyChange(VflVtkGeometryProperties *p, int msg);
	bool HandleLevelDataChange(VveVtkPolyDataItem *pCont, int msg);
	bool HandleFilterChange(VflUnsteadyDataFilter *pFilter, int msg);

	void SetScalarVisibility(bool bScalarVisibility);
	void SetImmediateMode(bool bImmediateMode);


    /**
     * This method creates the required polydata mappers and actors and builds
	 * the corresponding pipeline. In addition, a lookup table and a property
	 * object are created and shared among them.
	 */
	bool InitPipeline();

    /**
     * Compute (or update) the object's boundaries.
     */    
	virtual bool ComputeBounds();

	/**
	 * Connect all filters in a (hopefully) correct way...
	 */
	void RewireFilters();



	typedef std::vector<vtkActor*> ACVEC;
	typedef std::vector<bool>      BOOLVEC;
	typedef std::vector<VflUnsteadyDataFilter *> FILTVEC;

	VveUnsteadyVtkPolyData	*m_pData;
	
	VflVtkProperty		*m_pProperty,
								*m_pBackfaceProperty;
	VflVtkLookupTable	*m_pLookupTable;
	bool						m_bIsPropSafeKill,
								m_bIsLookupTableSafeKill;

	vtkTexture *m_pTexture;


	bool m_bBoundsValid, 
		 m_bNeedRewire,
		 m_bTransformerDirty;

	double      m_dLastDrawSimTime,
		        m_dLastDrawTransSimTime;

	FILTVEC m_vecFilters;
	ACVEC   m_vecActors;
	BOOLVEC m_vecDataChanged,
		    m_vecBoundsChanged;
	
	VistaVector3D m_v3BoundsMin, 
		           m_v3BoundsMax;
	VtkObjectStack m_oVtkObjectStack;

	// ********************************************************************************************
	// VBO EXTENSION from here
	// ********************************************************************************************
	/**
	 * VBO stuff
	 * ATTENTION: The behaviour of vtkProperties is not fully reflected by this extension of
	 * VflVisVtkGeometry. Thus different behaviour when changing properties, i.e. the diffuse and
	 * ambient terms, may be experienced. (-> DefinedThreadWork() for further info (old: FillVBO(...))
	 */

public:
	// ************************************************************************
	// ENUMS
	
	enum VBOModes { VBO_UNDEFINED = -1, VBO_SINGLE, VBO_SERIAL, VBO_PARALLEL };

	// ************************************************************************
	// FUNCTIONS

	/**
	 * If not initialized into mode VBOModes::VBO_SINGLE this function will toggle between precaching (parallel mode)
	 * and no precaching (serial mode).
	 * @return Returns the new mode or VBOModes::VBO_UNDEFINED if an error occurs.
	 */
	VBOModes ToggleVBOMode();

	/**
	 * Getter for the VBO Mode.
	 * @return Returns the current VBO mode.
	 */
	VBOModes GetVBOMode();

protected:
	// ************************************************************************
	// CLASSES

	class CVBOWorkInstance : public IVistaThreadPoolWorkInstance
	{
	public:
		CVBOWorkInstance(VflVisVBOVtkGeometry *pParent);
		virtual ~CVBOWorkInstance();

		// selectors to modify the conditions
		void			SetVertexPtr(float* pVertexPtr);
		void			SetElementPtr(int* pElementPtr);
		void			SetPolyData(vtkPolyData *pPolyData);
		void			SetWorkUnitID(int iWUID);

	protected:
		// interface
		virtual void	DefinedThreadWork();

		// variables
		VflVisVBOVtkGeometry	*m_pParent;
		float					*m_pVertexPtr;
		int						*m_pElementPtr;
		int						m_iWorkUnitID;
		vtkPolyData				*m_pPolyData;
	};


	// ************************************************************************
	// STRUCTS

	struct VBOWorkUnit {
		int					iIdentifier;
		bool				bDoCleanUp;

		unsigned int		uiVertexBufferHandle;
		unsigned int		uiElementBufferHandle;
		unsigned int		nSizeOfElementBuffer;
		unsigned int		uiBufferOffset;
		int					iIndex;						// will be -1 if not set/not filled
		bool				bScalarVisFlag;
		bool				bIsTriangleStrip;
		VistaThreadEvent	*pIsReadyToMod;

		CVBOWorkInstance	*pWorkInstance;
		
		VveVtkPolyDataItem	*pLevelData;				// used to lock/unlock data
	};


	// ************************************************************************
	// VARIABLES
	int					VBO_BUFFER_SIZE;				// how many VBOs do we want?

	bool				m_bIsVBOInited;					// tells whether the VBO extension is ready to use						
	VBOModes			m_eVBOUsageMode;

	unsigned int		*m_pVertexBuffers,				// pointer to the allocated array buffers
						*m_pElementBuffers;				// pointer to the allocated element array buffers

	int					m_iDrawIndex,					// index of the buffer pair to be drawn next
						m_iFillIndex,					// index of the buffer pair to be filled next
						m_iLookAheadIndex;

	int					m_iLastDrawnIndex;
	float				m_fLastLoopTime;

	int					m_iSimDir;						// "playback" direction: +1 == forward, -1 == backward
	
	VBOWorkUnit			*m_pVBOWorkUnits;

	VistaThreadPool	*m_pThreadPool;					// this guy'll manage the fill work :)


	// ************************************************************************
	// FUNCTIONS

	/**
	 * Initialize all VBO relevant stuff.
	 * @param buffer_size The number of vertex buffer objects to be readied.
	 * @return Returns 'true' if the initialization was a success.
	 */
	bool	InitVBO(int buffer_size);

	/**
	 * Prepares the VBO renderer for destruction.
	 */
	void	KillVBO();

	/**
	 * Draws a specific vertex buffer object.
	 * @param oTarget A reference to the work unit cotaining the VBO to be drawn.
	 * @result Returns true if the draw call was a success.
	 */	
	bool	DrawVBO(VBOWorkUnit &oTarget);

	/**
	 * Updates the VBOs, i.e. removes obsolete precached frames and precaches new ones.
	 * @param iIndex The frame that is requested (i.e. to be drawn next).
	 * @result Returns 'true' if the updated succeeded.
	 */
	bool	UpdateVBO(int iIndex);
	
	/**
	 * Prepares a supplied work unit to be filled with the supplied frame.
	 * @param iIndex Index of the frame that will be precached.
	 * @param oTarget Work unit that will be used for the precaching.
	 * @result Returns 'true' if the preparation was a success and the work unit is ready for the fill operation.
	 */
	bool	PrepareAsyncFill(int iIndex, VBOWorkUnit &oTarget);

	/**
	 * Cleans up a specific work unit.
	 * Attention: This function has to be executed for every work unit that has to be drawn before it is drawn!!!
	 * The function will take care of the synchronisation.
	 * @param oTarget The work unit to be cleaned up.
	 * @return Returns 'true' if the GL_ARRAY_BUFFER and GL_ELEMENT_ARRAY_BUFFER could be unmapped successfully.
	 */
	bool	CleanUpAsyncFill(VBOWorkUnit &oTarget);

	// ********************************************************************************************
	// VBO EXTENSION until here
	// ********************************************************************************************
};

/*============================================================================*/
/* INLINE METHODS                                                             */
/*============================================================================*/
inline VflVtkProperty *VflVisVBOVtkGeometry::GetProperty() const
{
	return m_pProperty;
}

inline VflVtkProperty *VflVisVBOVtkGeometry::GetBackfaceProperty() const
{
	return m_pBackfaceProperty;
}

inline VflVtkLookupTable *VflVisVBOVtkGeometry::GetLookupTable() const
{
	return m_pLookupTable;
}

inline VveUnsteadyVtkPolyData *VflVisVBOVtkGeometry::GetData() const
{
	return m_pData;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VflVisVBOVtkGeometry_H)
