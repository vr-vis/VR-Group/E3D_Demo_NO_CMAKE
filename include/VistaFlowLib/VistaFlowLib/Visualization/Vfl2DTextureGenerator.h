/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFL2DTEXTUREGENERATOR_H
#define _VFL2DTEXTUREGENERATOR_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/VistaFlowLibConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This is an interface for all classes that *somehow* generates 2D texture
 * OUTPUT: VistaTexture
 */
class VISTAFLOWLIBAPI IVfl2DTextureGenerator
{
public:
	virtual ~IVfl2DTextureGenerator();

	/**
	 * This member method *MUST* be implemented by every subclasses.
	 */
	virtual VistaTexture* GetOutput() const = 0;

	virtual void SetTextureBackgroundColor(float fC[4]);
	virtual void GetTextureBackgroundColor(float fC[4]) const;

	void GetTextureResolution(int &iWidth, int &iHeight);

protected:
	IVfl2DTextureGenerator(int iWidth, int iHeight);

private:
	int		m_iTextureWidth;
	int		m_iTextureHeight;
	float	m_fEmptyTexColor[4];
};

/*============================================================================*/
/* INLINE METHODS                                                             */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_2DTEXTUREGENERATOR_H


