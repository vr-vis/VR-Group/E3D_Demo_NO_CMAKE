/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLUNSTEADYDATAFILTERGLYPH_H
#define _VFLUNSTEADYDATAFILTERGLYPH_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflUnsteadyDataFilter.h"
#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkGlyph3D;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflUnsteadyDataFilterGlyph : public VflUnsteadyDataFilter
{
public:
	VflUnsteadyDataFilterGlyph(vtkGlyph3D *pPrototype = NULL);
	virtual ~VflUnsteadyDataFilterGlyph();

	// filter and pipeline management
	virtual void CreateFilters(int iCount);
	virtual void SetInput(int iIndex, vtkPolyData *pData);
	virtual vtkPolyData *GetInput(int iIndex) const;
	virtual vtkPolyData *GetOutput(int iIndex);
	virtual int GetFilterCount() const;
	virtual void Synchronize();

	// dealing with the prototype
	vtkGlyph3D *GetPrototype() const;
	static void CopyProperties(vtkGlyph3D *pTarget,
		vtkGlyph3D *pSource);

	// interface from VflPropertyContainer:
	virtual int SetProperty(const VistaProperty &refProp);
	virtual int GetProperty(VistaProperty &refProp);
	virtual int GetPropertySymbolList(std::list<std::string> &rStorageList);

	REFL_INLINEIMP(VflUnsteadyDataFilterGlyph,VflUnsteadyDataFilter)
protected:
	void CreateSources(const VistaPropertyList &refSources);

	std::vector<vtkGlyph3D *>	m_vecFilters;
	vtkGlyph3D  				*m_pPrototype;
	VistaPropertyList					m_oSourceProps;	// remember our source data

	std::string					m_strIniFile;	// just a hack - please ignore this...
};

#endif // _VFLUNSTEADYDATAFILTERGLYPH_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

