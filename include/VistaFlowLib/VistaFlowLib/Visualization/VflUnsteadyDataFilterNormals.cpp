/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflUnsteadyDataFilterNormals.h"

#include <VistaAspects/VistaAspectsUtils.h>

#include <vtkPolyDataNormals.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

static void DestroyFilters(std::vector<vtkPolyDataNormals *> &refFilters);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflUnsteadyDataFilterNormals::VflUnsteadyDataFilterNormals(vtkPolyDataNormals *pPrototype)
: VflUnsteadyDataFilter(),
  m_pPrototype(NULL)
{
	m_pPrototype = vtkPolyDataNormals::New();
	m_pPrototype->GlobalWarningDisplayOff();

	if (pPrototype)
		CopyProperties(m_pPrototype, pPrototype);
}

VflUnsteadyDataFilterNormals::~VflUnsteadyDataFilterNormals()
{
	DestroyFilters(m_vecFilters);

	m_pPrototype->Delete();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateFilters                                               */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyDataFilterNormals::CreateFilters(int iCount)
{
	DestroyFilters(m_vecFilters);

	m_vecFilters.resize(iCount);
	for (int i=0; i<iCount; ++i)
	{
		m_vecFilters[i] = vtkPolyDataNormals::New();
	}

	Synchronize();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetInput                                                    */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyDataFilterNormals::SetInput(int iIndex, vtkPolyData *pData)
{
	if (0<=iIndex && iIndex<int(m_vecFilters.size()))
	{
#if VTK_MAJOR_VERSION > 5
		m_vecFilters[iIndex]->SetInputData(pData);
#else
		m_vecFilters[iIndex]->SetInput(pData);
#endif
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetInput                                                    */
/*                                                                            */
/*============================================================================*/
vtkPolyData *VflUnsteadyDataFilterNormals::GetInput(int iIndex) const
{
	if (iIndex<0 || iIndex>=int(m_vecFilters.size()))
		return NULL;

	return m_vecFilters[iIndex]->GetPolyDataInput(0);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetOutput                                                   */
/*                                                                            */
/*============================================================================*/
vtkPolyData *VflUnsteadyDataFilterNormals::GetOutput(int iIndex)
{
	if (iIndex<0 || iIndex>=int(m_vecFilters.size()))
		return NULL;
	
	if (m_bActive)
		return m_vecFilters[iIndex]->GetOutput();
	else
		return m_vecFilters[iIndex]->GetPolyDataInput(0);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilterCount                                              */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyDataFilterNormals::GetFilterCount() const
{
	return static_cast<int>(m_vecFilters.size());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Synchronize                                                 */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyDataFilterNormals::Synchronize()
{
	for (unsigned int i=0; i<m_vecFilters.size(); ++i)
	{
		CopyProperties(m_vecFilters[i], m_pPrototype);
	}
	Notify(MSG_PARAMETER_CHANGE);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPrototype                                                */
/*                                                                            */
/*============================================================================*/
vtkPolyDataNormals *VflUnsteadyDataFilterNormals::GetPrototype() const
{
	return m_pPrototype;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CopyProperties                                              */
/*                                                                            */
/*============================================================================*/
void VflUnsteadyDataFilterNormals::CopyProperties(vtkPolyDataNormals *pTarget,
												   vtkPolyDataNormals *pSource)
{
	pTarget->SetFeatureAngle( pSource->GetFeatureAngle() );
	pTarget->SetSplitting( pSource->GetSplitting() );
	pTarget->SetConsistency( pSource->GetConsistency() );
	pTarget->SetFlipNormals( pSource->GetFlipNormals() );
	pTarget->SetNonManifoldTraversal( pSource->GetNonManifoldTraversal() );
	pTarget->SetComputePointNormals( pSource->GetComputePointNormals() );
	pTarget->SetComputeCellNormals( pSource->GetComputeCellNormals() );
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetProperty                                                 */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyDataFilterNormals::SetProperty(const VistaProperty &refProp)
{
	string strKey =
		VistaAspectsConversionStuff::ConvertToLower(refProp.GetNameForNameable());

	if (strKey == "active")
	{
		SetActive(VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue()));
	}
	else if (strKey == "feature_angle")
	{
		float fValue = float(VistaAspectsConversionStuff::ConvertToDouble(refProp.GetValue()));
		m_pPrototype->SetFeatureAngle (fValue);
	}
	else if (strKey == "splitting")
	{
		bool bValue = VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue());
		m_pPrototype->SetSplitting (bValue);
	}
	else if (strKey == "consistency")
	{
		bool bValue = VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue());
		m_pPrototype->SetConsistency (bValue);
	}
	else if (strKey == "compute_point_normals")
	{
		bool bValue = VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue());
		m_pPrototype->SetComputePointNormals (bValue);
	}
	else if (strKey == "compute_cell_normals")
	{
		bool bValue = VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue());
		m_pPrototype->SetComputeCellNormals (bValue);
	}
	else if (strKey == "flip_normals")
	{
		bool bValue = VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue());
		m_pPrototype->SetFlipNormals (bValue);
	}
	else if (strKey == "non_manifold_traversal")
	{
		bool bValue = VistaAspectsConversionStuff::ConvertToBool(refProp.GetValue());
		m_pPrototype->SetNonManifoldTraversal (bValue);
	}
	else
	{
		return VflUnsteadyDataFilter::SetProperty(refProp);
	}

	Synchronize();
	
	return PROP_OK;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperty                                                 */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyDataFilterNormals::GetProperty(VistaProperty &refProp)
{
	string strKey =
		VistaAspectsConversionStuff::ConvertToLower(refProp.GetNameForNameable());

	if (strKey == "active")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(GetActive()));
	}
	else if (strKey == "feature_angle")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetFeatureAngle()));
	}
	else if (strKey == "splitting")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetSplitting()?true:false));
	}
	else if (strKey == "consistency")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetConsistency()?true:false));
	}
	else if (strKey == "compute_point_normals")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetComputePointNormals()?true:false));
	}
	else if (strKey == "compute_cell_normals")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetComputeCellNormals()?true:false));
	}
	else if (strKey == "flip_normals")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetFlipNormals()?true:false));
	}
	else if (strKey == "non_manifold_traversal")
	{
		refProp.SetValue(
			VistaAspectsConversionStuff::ConvertToString(m_pPrototype->GetNonManifoldTraversal()?true:false));
	}
	else if (strKey == "type")
	{
		refProp.SetValue("VFL_UNSTEADY_DATA_FILTER_NORMALS");
	}
	else
	{
		return VflUnsteadyDataFilter::GetProperty(refProp);
	}

	return PROP_OK;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPropertySymbolList                                       */
/*                                                                            */
/*============================================================================*/
int VflUnsteadyDataFilterNormals::GetPropertySymbolList(std::list<std::string> &rStorageList)
{
  	rStorageList.push_back("ACTIVE");
	rStorageList.push_back("FEATURE_ANGLE");
	rStorageList.push_back("SPLITTING");
	rStorageList.push_back("CONSISTENCY");
	rStorageList.push_back("COMPUTE_POINT_NORMALS");
	rStorageList.push_back("COMPUTE_CELL_NORMALS");
	rStorageList.push_back("FLIP_NORMALS");
	rStorageList.push_back("NON_MANIFOLD_TRAVERSAL");
	rStorageList.push_back("TYPE");

    return static_cast<int>(rStorageList.size());
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DestroyFilters                                              */
/*                                                                            */
/*============================================================================*/

void DestroyFilters(vector<vtkPolyDataNormals *> &vecFilters)
{
	for (unsigned int i=0; i<vecFilters.size(); ++i)
	{
		vecFilters[i]->Delete();
		vecFilters[i] = NULL;
	}
	vecFilters.clear();
}

/*============================================================================*/
/*  END OF FILE "VflUnsteadyDataFilter.cpp"                                           */
/*============================================================================*/
