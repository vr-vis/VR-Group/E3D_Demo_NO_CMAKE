/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLUNSTEADYLUT_H
#define _VFLUNSTEADYLUT_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

#include <VistaVisExt/Data/VveVtkData.h>

#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveInfoReader;
class VveDataSetInfo;
class VveDataFieldInfo;
class VveTimeMapper;

class VflRenderNode;

class Vfl3DTextLabel;

class vtkLookupTable;
class vtkUnsignedCharArray;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflUnsteadyLUT : public IVflRenderable
{
public:
	/*---------------------------------------------------*/
	/* Enums											 */
	/*---------------------------------------------------*/
	enum RENDERMODE
	{
		RM_ORTHOGONAL = 0,
		RM_PERSPECTIVE
	};

	/*---------------------------------------------------*/
	/* Con-/Destructor									 */
	/*---------------------------------------------------*/

	//! Constructor.
	VflUnsteadyLUT(VflRenderNode *pRenderNode,
					VveTimeMapper* pTargetTM);

	//! Destructor.
	virtual ~VflUnsteadyLUT();

    /**
     * Returns a pointer to the shared vtk lookup table.
     */
    vtkLookupTable *GetLookupTable() const;

	/*---------------------------------------------------*/
	/* Interface										 */
	/*---------------------------------------------------*/

	//! Initialization (requires a VveInfoReader to be set).
	/*!
		NOTE: This function has to be explicitly called once after
			  instantiation or after setting a new info reader.
			  It is only used for the initial setup.
			  Don't forget to set a valid info reader before calling the init
			  (if that didn't already happen at instantiation).

		\return Returns 'true' iff the init was successful and the
				unsteady LUT is ready to be used.
	 */
	bool Init(VveInfoReader *pIR);

	//! Update the LUT to match the supplied timeindex.
	/*!
		NOTE: Interface of IVflRenderable.
	 */
	virtual void Update();

	//! Visualizes the UnsteadyLUT.
	/*!
		NOTE: Interface of IVflRenderable.
	 */
	virtual void DrawOpaque();

	//! Retrieve the registration mode.
	/*!
		NOTE: Interface of IVflRenderable.
	 */
	virtual unsigned int GetRegistrationMode() const;

	//! Create renderable properties.
	/*!
		NOTE: Interface of IVflRenderable.
	 */
	virtual VflRenderableProperties    *CreateProperties() const;

	//! Retrieve the type.
	/*!
		NOTE: Interface of IVflRenderable.
	 */
	virtual std::string GetType() const;

	//! Getter to retrieve the color from the LUT.
	/*!
		NOTE: Call _after_ setting up the LUT as desired via the
			  vtkLookupTable wrapper.

		\param x The scalar that is to be mapped into the LUT.
		\param rgb The color of the LUT as position x.

		\return Returns 'true' iff the lookup was successful and rgb is
				a valid color.
	 */
	bool GetColor(double x, double rgb[3]) const;

	//! Getter to retrieve the opacitiy from the LUT.
	/*!
		\param x The scalar that is to be mapped into LUT.

		\return Returns a value in the range [0,1] if the lookup was
				successful. -1.0 is returned in case of an error.
	 */
	double GetOpacity(double x) const;

	//! Getter to retrieve the global minimum of the active field.
	double GetGlobalMin() const;
	
	//! Getter to retrieve the global maximum of the active field.
	double GetGlobalMax() const;

	//! Getter to retrieve the global minimum and maximum of the active field.
	void GetGlobalMinMax(double &dMin, double &dMax) const;
	void GetGlobalMinMax(double dMinMax[2]) const;


	/*---------------------------------------------------*/
	/* Selectors										 */
	/*---------------------------------------------------*/

	//! Set the render mode.
	void SetRenderMode(RENDERMODE eMode);

	//! Get the render mode.
	RENDERMODE GetRenderMode() const;

	//! Set the active field for the LUT.
	bool SetActiveField(int iActiveField);

	//! Set the active field for the LUT.
	bool SetActiveFieldByName(const std::string & sFieldName);

	//! Get the field the LUT represents.
	int	GetActiveField() const;

	bool SetLUTVisSize(float fWidth, float fHeight);
	bool SetLUTVisSize(float fSize[2]);
	
	bool GetLUTVisSize(float &fWidth, float &fHeight) const;
	bool GetLUTVisSize(float fSize[2]) const;
	
	bool SetLUTVisPos3D(const VistaVector3D& v3Pos);
	bool SetLUTVisPos3D(float fX, float fY, float fZ);
	bool SetLUTVisPos3D(float fPos[3]);

	VistaVector3D GetLUTVisPos3D() const;
	bool GetLUTVisPos3D(float &fX, float &fY, float &fZ) const;
	bool GetLUTVisPos3D(float fPos[3]) const;

	bool SetLUTVisOrientation(const VistaQuaternion& qOri);
	VistaQuaternion GetLUTVisOrientation() const;

	//! Setter for the position of LUT's vis in orthogonal mode (2D).
	/*!
		\param fX Relative screen pos in [0.0f, 1.0f].
		\param fY Relative screen pos in [0.0f, 1.0f].

		\return Returns 'true' iff the operation was successful.
	 */
	bool SetLUTVisPos2D(float fX, float fY);
	bool SetLUTVisPos2D(float fPos[2]);

	bool GetLUTVisPos2D(float &fX, float &fY) const;
	bool GetLUTVisPos2D(float fPos[2]) const;

	/*---------------------------------------------------*/
	/* vtkLookupTable wrapper							 */
	/*---------------------------------------------------*/
	
	void			SetRamp(int iRamp) const;
	void			SetRampToLinear() const;
	void			SetRampToSCurve() const;
	void			SetRampToSQRT() const;
	int				GetRamp() const;

	void			SetScale(int iScale) const;
	void			SetScaleToLinear() const;
	void			SetScaleToLog10() const;
	int				GetScale() const;

	void			SetTableRange(double dMin, double dMax) const;
	void			SetTableRange(double dRange[2]) const;
	void			GetTableRange(double dRange[2]) const;
	double*			GetTableRange() const;

	void			SetHueRange(double dMin, double dMax) const;
	void			SetHueRange(double dRange[2]) const;
	void			GetHueRange(double &dMin, double &dMax) const;
	void			GetHueRange(double dRange[2]) const;
	double*			GetHueRange() const;

	void			SetSaturationRange(double dMin, double dMax) const;
	void			SetSaturationRange(double dRange[2]) const;
	void			GetSaturationRange(double &dMin, double &dMax) const;
	void			GetSaturationRange(double dRange[2]) const;
	double*			GetSaturationRange() const;

	void			SetValueRange(double dMin, double dMax) const;
	void			SetValueRange(double dRange[2]) const;
	void			GetValueRange(double &dMin, double &dMax) const;
	void			GetValueRange(double dRange[2]) const;
	double*			GetValueRange() const;

	void			SetAlphaRange(double dMin, double dMax) const;
	void			SetAlphaRange(double dRange[2]) const;
	void			GetAlphaRange(double &dMin, double &dMax) const;
	void			GetAlphaRange(double dRange[2]) const;
	double*			GetAlphaRange() const;

	int				GetIndex(double x) const;
	void			SetNumberOfTableValues(int iNumber) const;
	int				GetNumberOfTableValues() const;
	void			GetTableValue(double x, double rgba[4]) const;

	unsigned char*	WritePointer(int iID, 
								 int iNumber) const;
	unsigned char*	GetPointer(int iID) const;

	double*			GetRange() const;
	void			SetRange(double dMin, double dMax) const;
	void			SetRange(double dRange[2]) const;

	void			SetNumberOfColors(int iNum) const;
	int				GetNumberOfColors() const;

	void			SetTable(vtkUnsignedCharArray *table) const;
	vtkUnsignedCharArray* GetTable() const;

	void			MapScalarsThroughTable2(void *vInput,
						unsigned char *ubOutput, int iInputDataType,
						int iNumberOfValues, int iInputIncrement,
						int iOutputIncrement) const;

protected:
	/*---------------------------------------------------*/
	/* General Variables								 */
	/*---------------------------------------------------*/
	// The attached time info.
	VveTimeMapper *m_pTimeMapper;
	
	bool					m_bIsInited;
	// Render mode.
	RENDERMODE				m_eRenderMode;
	VveDataSetInfo			*m_pDataSetInfo;
	int						m_iActiveField;
	// The vtkLookupTabel which holds the LookupTable for the time index
	// that was supplied with the last update.
	vtkLookupTable			*m_pLUT;
	// Time index of the current LUT.
	int						m_iCurTimeIndex;
	// LUT's size.
	float					m_fWidth, m_fHeight;
	// Texture for current LUT's coloring.
	unsigned int			m_uiLUTColorTxt;
	VistaVector3D			m_v3Position;
	VistaQuaternion			m_qOrientation;
	// Current LUT's vertex positions (4 x (x,y,z) entries).
	float					*m_fCurPos;
	// Global LUT's vertex positions (4 x (x,y,z) entries).
	float					*m_fGlobalPos;
	// LUT range labels current and global.
	Vfl3DTextLabel			*m_pGlobalMinTxt, *m_pGlobalMaxTxt,
							*m_pCurMinTxt, *m_pCurMaxTxt;
private:
	/*---------------------------------------------------*/
	/* Helper Functions									 */
	/*---------------------------------------------------*/
	
	//! This function updates the vis on size/pos/normal changes.
	void UpdateVis();

	//! This function updates the lookup texture for the LUT vis.
	void UpdateTexture() const;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VflUnsteadyLUT_H)
