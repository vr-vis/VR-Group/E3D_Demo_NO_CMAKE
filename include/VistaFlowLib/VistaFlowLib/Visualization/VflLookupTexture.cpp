/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES			                                                          */
/*============================================================================*/
#include "VflLookupTexture.h"

#include <VistaBase/VistaMathBasics.h>

#include <VistaOGLExt/VistaTexture.h>

using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflLookupTexture::VflLookupTexture(
						IVflLookupTable* pTable, 
						int iInterpolationMode /* = GL_NEAREST */,
						bool bManageLut  /* = false */ )
	:	VflObserver()
	,	m_pTexture( NULL )
	,	 m_pPreIntegrationTexture( NULL )
	,	m_pLut( pTable )
	,	m_bHasTransparencies( false )
	,	m_bManageLut( bManageLut )
	,	m_bUpdatePreIntegration( false )
{
	if ( !m_pLut )
	{
		m_pLut = new VflLookupTable();
		m_bManageLut = true;
	}

	if( iInterpolationMode == GL_NEAREST || iInterpolationMode == GL_LINEAR )
		m_iInterpolationMode = iInterpolationMode;
	else
		m_iInterpolationMode = GL_NEAREST;

	AllocateTexture();
	SynchTexture();

	//we may only observe the LUT after! we allocated the texture!
	Observe( m_pLut );
}

VflLookupTexture::~VflLookupTexture()
{
	ReleaseObserveable( m_pLut, IVistaObserveable::TICKET_NONE );

	delete m_pTexture;
	m_pTexture = NULL;

	delete m_pPreIntegrationTexture;
	m_pPreIntegrationTexture = NULL;

	if( m_bManageLut )
	{		
		delete m_pLut;
		m_pLut = NULL;
	}
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
const bool VflLookupTexture::GetUpdatePreIntegration() const
{
	return m_bUpdatePreIntegration;
}

void VflLookupTexture::SetUpdatePreIntegration( const bool state )
{
	m_bUpdatePreIntegration = state;
}


void VflLookupTexture::ForceUpdate()
{
	SynchTexture();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLookupTexture                                            */
/*                                                                            */
/*============================================================================*/
VistaTexture *VflLookupTexture::GetLookupTexture() const
{
	return m_pTexture;
}
  
VistaTexture *VflLookupTexture::GetPreIntegrationTexture() const
{
	return m_pPreIntegrationTexture;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get-/SetLookupTable                                         */
/*                                                                            */
/*============================================================================*/
IVflLookupTable* VflLookupTexture::GetLookupTable() const
{
    return m_pLut;
}

void VflLookupTexture::SetLookupTable( IVflLookupTable* pLut, bool bManageLut )
{
	if(m_bManageLut)
	{
		ReleaseObserveable(m_pLut);
		delete m_pLut;
		m_pLut = NULL;
	}

	m_bManageLut = bManageLut;
	m_pLut = pLut;
	SynchTexture();
	Observe(m_pLut);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetHasTransparencies                                        */
/*                                                                            */
/*============================================================================*/
bool VflLookupTexture::GetHasTransparencies() const
{
	return m_bHasTransparencies;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflLookupTexture::ObserverUpdate(
	IVistaObserveable *pObserveable, int msg, int ticket )
{
	if( msg == IVflLookupTable::MSG_VALUES_CHANGED )
		SynchTexture();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AllocateTexture                                             */
/*                                                                            */
/*============================================================================*/
void VflLookupTexture::AllocateTexture()
{
	m_pTexture = new VistaTexture(GL_TEXTURE_1D);
	m_pTexture->Bind();
	m_pTexture->SetWrapS( GL_CLAMP_TO_EDGE );
	m_pTexture->SetMinFilter( m_iInterpolationMode );
	m_pTexture->SetMagFilter( m_iInterpolationMode );
	m_pTexture->UploadTexture( 1, 0, NULL, false, GL_RGBA, GL_FLOAT );
	m_pTexture->Unbind();

	// create preintegration texture
	m_pPreIntegrationTexture = new VistaTexture(GL_TEXTURE_2D);
	m_pPreIntegrationTexture->Bind();
	m_pPreIntegrationTexture->SetWrapS( GL_CLAMP_TO_EDGE );
	m_pPreIntegrationTexture->SetWrapT( GL_CLAMP_TO_EDGE );
	m_pPreIntegrationTexture->SetMinFilter( m_iInterpolationMode );
	m_pPreIntegrationTexture->SetMagFilter( m_iInterpolationMode );
	m_pPreIntegrationTexture->UploadTexture( 256, 256, NULL, false, GL_RGBA, GL_FLOAT );
	m_pPreIntegrationTexture->Unbind();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SynchTexture                                                */
/*                                                                            */
/*============================================================================*/
void VflLookupTexture::SynchTexture()
{
	std::vector<VistaColor> vecColors;
	m_pLut->GetTableValues( vecColors );

	m_bHasTransparencies = false;
	for( size_t n=0; n<vecColors.size(); ++n )
	{
		if( vecColors[n].GetAlpha() <1.0 )
		{
			m_bHasTransparencies = true;
			break;
		}
	}

	m_pTexture->Bind();
	m_pTexture->UploadTexture( 
		static_cast<int>(vecColors.size()), 0, 
		&vecColors[0], false, GL_RGBA, GL_FLOAT );
	m_pTexture->Unbind();
	
	// only update preintegration table when flag set
	if( m_bUpdatePreIntegration )
		CreatePreIntegrationTable( vecColors );
}

void VflLookupTexture::CreatePreIntegrationTable( 
	const std::vector<VistaColor>& vecColors )
{
	if( vecColors.size() != 256 )
		return;
	// only support table size of 256 right now
	

	double rInt[256], gInt[256], bInt[256], aInt[256];

	rInt[0] = 0.0;
	gInt[0] = 0.0;
	bInt[0] = 0.0;
	aInt[0] = 0.0;
	
	double r = 0.0; 
	double g = 0.0;
	double b = 0.0;
	double a = 0.0;
	// compute integral functions
	for( int i=1; i < 256; ++i )
	{
		r += ( vecColors[i-1][0] + vecColors[i][3] )/2.0;
		g += ( vecColors[i-1][1] + vecColors[i][3] )/2.0;
		b += ( vecColors[i-1][2] + vecColors[i][3] )/2.0;
		a += ( vecColors[i-1][3] + vecColors[i][3] )/2.0;

		rInt[i] = r;
		gInt[i] = g;
		bInt[i] = b;
		aInt[i] = a;
	}

	std::vector<float> pre_int_table;
	pre_int_table.resize( 256*256*4 );

	double aColor[4];
	int iMin, iMax, iLookupIndex = 0;
	double dFactor;
	// compute look-up tex from integral functions
	for( int r=0; r < 256; ++r )
	{
		for( int s = 0; s < 256; ++s )
		{
			iMin = ( r<s )? r : s;
			iMax = ( r>s )? r : s;

			if( iMax != iMin )
			{
				dFactor = 1.0 / (double)(iMax-iMin); 
				aColor[0] = dFactor*( rInt[iMax] - rInt[iMin] ); 
				aColor[1] = dFactor*( gInt[iMax] - gInt[iMin] ); 
				aColor[2] = dFactor*( bInt[iMax] - bInt[iMin] ); 
				aColor[3] = 1.0 - dFactor*exp( -( aInt[iMax] - aInt[iMin] ) );
			}
			else
			{
				aColor[0] = vecColors[iMin][0];
				aColor[1] = vecColors[iMin][1];
				aColor[2] = vecColors[iMin][2];
				aColor[3] = 1.0 - exp( -vecColors[iMin][3] );
			}

			pre_int_table[iLookupIndex++] = static_cast<float>( Vista::Clamp( aColor[0], 0.0, 1.0 ) );
			pre_int_table[iLookupIndex++] = static_cast<float>( Vista::Clamp( aColor[1], 0.0, 1.0 ) );
			pre_int_table[iLookupIndex++] = static_cast<float>( Vista::Clamp( aColor[2], 0.0, 1.0 ) );
			pre_int_table[iLookupIndex++] = static_cast<float>( Vista::Clamp( aColor[3], 0.0, 1.0 ) );
		}
	}

	// update texture
	m_pPreIntegrationTexture->Bind();
	m_pPreIntegrationTexture->UploadTexture( 256, 256, &pre_int_table[0], false, GL_RGBA, GL_FLOAT );
	m_pPreIntegrationTexture->Unbind();
}

/*============================================================================*/
/*  END OF FILE "VflLookupTexture.cpp"                                  */
/*============================================================================*/
