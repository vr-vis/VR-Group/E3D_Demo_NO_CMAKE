/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVISTIMING_H
#define _VFLVISTIMING_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaObserveable.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveTimeMapper;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/*
* The VflVisTiming is the time controlling element of each render node.
* For more information on the used notation of time frames, please refer to:
* Marc Wolter, Ingo Assenmacher, Bernd Hentschel, Marc Schirski and
* Torsten Kuhlen (2009). A Time Model for Time-Varying Visualization.
* Computer Graphics Forum 28(6), pp. 1561-1571.
*/
class VISTAFLOWLIBAPI VflVisTiming : public IVistaObserveable
{
public:
	/*
	* messages for observers
	*/
    enum{
        MSG_VISTIME_CHG = IVistaObserveable::MSG_LAST,
        MSG_LOOPTIME_CHG,
        MSG_ANIMSTATUS_CHG,
		MSG_RANGE_CHG,
		MSG_ANIMSPEED_CHG,
        MSG_LAST
    };

	/**
     * Constructor. The creation time is the beginning of the user time frame.
     */    
	VflVisTiming(double dCreationTime);
	virtual ~VflVisTiming();

	/**
     * Get current visualization time, which lies within [0 <= m_dStartVisTime, m_dEndVisTime<=1].
	 * 
     */    
	double GetVisualizationTime() const;

    /**
     * Set current visualization time, which lies within [0 <= m_dStartVisTime, m_dEndVisTime<=1].
	 * A value outside this interval is restricted to the interval bounds.
	 * @author  Marc Wolter
	 * @date    September 2010
	 *
	 * @param[in]   float fVisTime 	 value between 0 and 1, is clamped to value between m_dStartVisTime, m_dEndVisTime
     */    
	bool SetVisualizationTime(double fVisTime);

    /**
     * Get current loop time in seconds. The loop time is how long a
	 * visualization time frame is shown to the user in seconds.
	 *
	 * The actual loop time is computed by taking a base loop time ("how many seconds should the system need
	 * to show the visualization from vis time 0 to 1?") and multiplying this base value with a loop time factor
	 * ("e.g., double speed/half speed").
     * 
     * @author  Marc Wolter
     * @date    September 2010
     *
     * @return  double		actual loop time in seconds
     *
     */    
	double GetLoopTime() const;

	/**
     * Get base loop time in seconds. (Current) loop time and base loop time are distinguished.
     * The base loop time is defined by the user, while the loop time itself is computed by the
     * base loop time and the current animation speed.
	 *
	 * @author  Marc Wolter
	 * @date    September 2010
	 *
	 * @return  double		base loop time in seconds
     */    
	double GetBaseLoopTime() const;

    /**
     * Set base loop time in seconds.
	 *
	 * NOTE: this was SetLoopTime and has been changed due to a consistent naming
	 * for new functionality. See "GetLoopTime" for an explanation.
	 * If you have used SetLoopTime in FlowLib v0.9 or earlier, you can simply
	 * use SetBaseLoopTime instead.
     * 
     * @author  Marc Wolter
     * @date    September 2010
     *
     * @param[in]   double dLoopTime 	desired loop time in seconds
     * @param[out]  --
     * @GLOBALS --
     * @return  bool	success, i.e. 0 < fLoopTime 
     *
     */    
	bool SetBaseLoopTime(double dLoopTime);

	 /**
     * Accelerate/decelerate base loop time by a factor,
	 * fValue > 1 increases speed, fValue < 1 slows down.
     *
	 * Using this speed method instead of adapting the base loop time
	 * relieves you of remembering what the original loop time before
	 * acceleration/deceleration was.
	 *
	 * @author  Marc Wolter
	 * @date    September 2010
	 * @param[in]   double dValue 	<1 slower, >1 faster
	 * @return  bool	success, i.e fValue>0 and SetBaseLoopTime successful
	 *
     */ 
	bool SetAnimationSpeed(double dValue);
	double GetAnimationSpeed() const;

	/*
	* Return animation play state.
	* @return  bool		true if playing, false else
	*/
	bool GetAnimationPlaying() const;

	/*
	* Set animation play state.
	* @param[in]  bool		true if playing, false else
	*/
	void SetAnimationPlaying(bool bSet);
	
	/*
	* Set animation play state with accurate time control.
	* The method SetAnimationPlaying without time control uses the clock value given by SetCurrentClock
	* to stamp the moment the animation is paused/started. 
	* Use this method if you need a finer control. This could be the need if you'd like to stop exactly
	* at some event from an input device.
	*
	* @param[in]  bool		true if playing, false else
	* @param[in]  double	current system time (in the same format as given to VflVisController::Update
	*/
	void SetAnimationPlaying(bool bSet, double dCurrentTime);
	
	/*
	* Toggle animation play state (from playing to pausing and vice versa).
	* The method ToggleAnimation without time control uses the clock value given by SetCurrentClock
	* to stamp the moment the animation is paused/started. 
	* Use this method if you need a finer control. This could be the need if you'd like to stop exactly
	* at some event from an input device.
	*
	* @param[in]  double	current system time (in the same format as given to VflVisController::Update
	*/
	void ToggleAnimation(double dCurrentTime);

	/*
	* Toggle animation play state (from playing to pausing and vice versa).
	*/
	void ToggleAnimation();

	/*
	* Return current clock stamp as updated by SetCurrentClock
	*/
	double GetCurrentClock() const;

	/*
	* Set current clock stamp used for animation purposes.
	* This method is typically not called by any user, but is updated
	* by the VflRenderNode owning this object.
	* Otherwise, strange things may occur.
	*
	* @return  double	returns new clock value
	*/
	double SetCurrentClock(double dCurTime);

	/*
	* This method updates the animation time based on m_dCurrentTime when running, i.e. it increases the current visualization time
	* and does a wrap-around if vis time exceeds 1.0 (or m_dEndVisTime, if set).
	*
	* This method is typically not called by any user, but is updated
	* by the VflRenderNode owning this object, right after a new time stamp is
	* set using SetCurrentClock.
	* Otherwise, strange things may occur.
	*/
	void UpdateAnimationTime();

	/**
	* Returns creation time of this object.
	*
	*/
	double GetStartupTimeStamp() const;


	/**
	* Returns life duration of this object,
	* which is GetCurrentClock() - GetStartupTimeStamp();
	*
	*/
	double GetLifeTime() const;

	/**
     * Change the range of the visualization time (within [0,1])
     * If bKeepUserTime is set, the animation speed is slowed down to match the
	 * new vis range interval size and keep animation speed for the process constant
     * (by changing m_dVisRangeSpeedFactor).
	 *
	 * @author  Marc Wolter
	 * @date    September 2010
     */  
    void SetVisTimeRange (double dStart, double dEnd, bool bKeepUserTime = true);
    bool GetVisTimeRange (double& dStart, double& dEnd) const;

	/**
     * Jump to first or last position in visualization time, i.e. typically vis time 0 (first) or 1 (last).
	 * (or, if set: m_dStartVisTime or m_dEndVisTime).
     * 
	 * @author  Marc Wolter
	 * @date    September 2010
     */  
	bool Last();
	bool First();
protected:
private:
	/**
     * Set/Get FrameTimeStamp.
	 *
	 * The FrameTimeStamp is a system clock time stamp (same time frame as SetCurrentClock) that determines when
	 * the current visualization frame (i.e., vis time 0 to 1) has started in user (system) time.
     * 
     */  
	double GetFrameTimeStamp() const;
	double SetFrameTimeStamp(double dCurrentClock);

	void UpdateTiming();

	// user time variables
	double			m_dStartupTimeStamp;	// time stamp for determining the life time
	double			m_dFrameTimeStamp;		// time stamp for determining time within animation loop

	double			m_dCurrentTime;			// current user time
	double			m_dLoopTime;			// time in seconds for one animation loop

	// visualization time variables
	double			m_dCurrentVisTime;
    double          m_dStartVisTime;
    double          m_dEndVisTime;

	// animation state
	bool			m_bAnimationPlaying;	

	/*
	* variables to determine the actual loop time:
	* loop time = base * animation speed factor * vsi range speed factor
	*/
	double			m_dBaseLoopTime;
	double			m_dAnimationSpeedFactor;
	double			m_dVisRangeSpeedFactor;


};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
inline double VflVisTiming::GetVisualizationTime() const
{ 
	return m_dCurrentVisTime; 
}


inline double VflVisTiming::GetBaseLoopTime() const
{
	return m_dBaseLoopTime;
}

inline double VflVisTiming::GetAnimationSpeed() const
{
	return 1.0 / m_dAnimationSpeedFactor;
}

inline double VflVisTiming::GetLoopTime() const
{ 
	return m_dLoopTime; 
}

inline bool VflVisTiming::GetAnimationPlaying() const
{ 
	return m_bAnimationPlaying; 
}


inline double VflVisTiming::GetCurrentClock() const
{
	return m_dCurrentTime;
}


inline double VflVisTiming::GetFrameTimeStamp() const
{
	return m_dFrameTimeStamp;
}

inline double VflVisTiming::GetStartupTimeStamp() const
{
	return m_dStartupTimeStamp;
}

inline double VflVisTiming::GetLifeTime() const
{
	return m_dCurrentTime - m_dStartupTimeStamp;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFLVISTIMING_H

