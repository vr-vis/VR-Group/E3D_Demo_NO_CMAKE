/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLRENDERTOTEXTURE_H
#define _VFLRENDERTOTEXTURE_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;
class VistaFramebufferObj;
class VistaRenderbuffer;
class VflRenderNode;
class VistaMutex;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflRenderToTexture provides basic render to texture functionality via
 * the IVflRenderable abstraction. 
 * Register a target renderable which you want to render into the texture.
 * 
 * NOTE: This class provides a setup viewport but does neither clear it
 * nor set the projection and model view matrices in any way. You have to
 * care for this in your renderable code!
 * 
 * NOTE: The target renderable should NOT be registered with the 
 * vis controller. It should be called only within the context 
 * of VflRenderToTexture. However, the SetTarget routine will set
 * the target's RenderNode so that it be informed e.g. on the 
 * proper vis timings.
 *
 * NOTE: You either have to call Render* on the RenderToTexture
 * object manually or register this one with the VisController in order
 * for the displayed data to be current.
 *
 * NOTE: The glViewport setup will be from 0,0 to iWidth,iHeight
 *
 * NOTE: Always delete the target AFTER the RenderToTexture object's unregister
 *       call to the vis controller because otherwise a forwarded GetRegMode
 *       call will crash!
 */
class VISTAFLOWLIBAPI VflRenderToTexture : public IVflRenderable
{
public:
	/**
	 * Create a new render texture with the given resolution
	 */
	VflRenderToTexture(int iWidth, int iHeight, bool bDoMipMap = false);
    virtual ~VflRenderToTexture();

	/**
	 * Resize the texture after instanciation.
	 */
	bool Resize(int iWidth, int iHeight, bool bDoMipMap = false);
	void GetSize(int &iWidth, int &iHeight);
	bool GetDoMipMap() const;

	//Just make the texture readable from the outside
	VistaTexture *GetTexture() const;

	/**
	 * Get/Set the target renderable i.e. the thing you want to be rendered
	 * into the texture. 
	 */
	void SetTarget(IVflRenderable *pRenderable);
	IVflRenderable *GetTarget() const;

	/**
	 * get/set the color with which the background will be filled
	 */
	void SetBackgroundColor(float fColor[3], float fAlpha = 1.0f);
	void GetBackgroundColor(float fColor[4]) const;

	//RENDERABLE INTERFACE
	virtual bool GetBounds(VistaVector3D &minBounds, 
		                   VistaVector3D &maxBounds);
	virtual std::string GetType() const;

	virtual void Update();

	virtual void DrawOpaque();

	virtual void DrawTransparent();

	virtual void Draw2D();

	virtual unsigned int GetRegistrationMode() const;

	virtual void SetRenderNode(VflRenderNode *pRN);
	/**
	 * Helper class to setup a default 2D ortho projection
	 * It will forward the drawing calls to its target in turn.
	 */
	class VISTAFLOWLIBAPI C2DViewAdapter : public IVflRenderable
	{
	public:
		C2DViewAdapter();
		virtual ~C2DViewAdapter();

		void RegisterTarget(IVflRenderable *pRenderable);
		void UnregisterTarget(IVflRenderable *pRenderable);

		void SetOrthoParams(float fLeft, float fRight, float fBottom, 
							float fTop, float fNear, float fFar);
		void GetOrthoParams(float &fLeft, float &fRight, float &fBottom, 
							float &fTop, float &fNear, float &fFar) const;
		
		virtual void Update();

		//we have a 2D projection only -> render everything in a 2D pass
		virtual void Draw2D();

		virtual unsigned int GetRegistrationMode() const;

		virtual void SetRenderNode(VflRenderNode *pRN);

	protected:
		virtual VflRenderableProperties* CreateProperties() const;

		void SetupProjection();
		void CleanupProjection();
	private:
		float m_fOrthoParams[6];
		std::list<IVflRenderable *>	m_liTargets;
		VistaMutex* m_pTargetListLock;
	};

	/**
	 * @todo get some perspective projecton default adapter in here.
	 */

protected:
	virtual VflRenderableProperties* CreateProperties() const;

	void SetupRendering();

	void CleanupRendering();
private:
	//Dimensions of the buffer to render to
	int m_iWidth, m_iHeight;
	
	//background color
	float m_fBackColor[4];

	//The framebuffer object itself
	VistaFramebufferObj *m_pBuffer;
	
	//The texture that will be attached to the framebuffer obj
	//This texture will then hold the off-screen rendering.
	VistaTexture *m_pTexture;
	
	//The render buffer that will be attached to the framebuffer object
	//This is needed to have a proper depth buffer in the FBO which
	//is not provided by the texture itself.
	VistaRenderbuffer *m_pRenBuffer;

	//the target renderable
	IVflRenderable *m_pTarget;

	//remember if the FBO is fine
	bool m_bInited;

	//flag for automatic mipmap generation
	bool m_bDoMipMap;

	//
	int m_iOldDrawBuffer;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
