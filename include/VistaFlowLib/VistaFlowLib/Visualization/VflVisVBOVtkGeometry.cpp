/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include <VistaMath/VistaVector.h>
#include <VistaInterProcComm/Concurrency/VistaThreadPool.h>

#include "VflVisVBOVtkGeometry.h"
#include "VflRenderNode.h"
#include "VflUnsteadyDataFilter.h"
#include "IVflTransformer.h"
#include "VflVisTiming.h"
#include <vtkPolyDataMapper.h>
#include <vtkLookupTable.h>
#include "VflVtkLookupTable.h"
#include <vtkProperty.h>
#include "VflVtkProperty.h"
#include <vtkMatrix4x4.h>

#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkOpenGLRenderer.h>

#include <cassert>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflVisVBOVtkGeometry::VflVisVBOVtkGeometry(int iNumOfVBOs)
: IVflVisObject(), m_pProperty(NULL), m_pBackfaceProperty(NULL), 
  m_pLookupTable(NULL), 
  m_pTexture(NULL), 
  m_bBoundsValid(false), 
  m_pData(NULL),
  m_bNeedRewire(true),
  m_dLastDrawSimTime(-1.0),
  m_dLastDrawTransSimTime(-1.0),
  m_bIsPropSafeKill(false),
  m_bIsLookupTableSafeKill(false),
  m_bIsVBOInited(false),
  m_eVBOUsageMode(VBO_UNDEFINED),
  m_iLastDrawnIndex(-1),
  m_fLastLoopTime(0.0f),
  m_iSimDir(1),
  m_pThreadPool(NULL)
{
	m_v3BoundsMin = VistaVector3D(0, 0, 0);
	m_v3BoundsMax = VistaVector3D(0, 0, 0);

	// init vertex buffer ofbjects for rendering
	m_bIsVBOInited = InitVBO(iNumOfVBOs);
}

VflVisVBOVtkGeometry::~VflVisVBOVtkGeometry()
{
	KillVBO();

	m_oVtkObjectStack.DeleteObjects();

	// unregister as observer of level data...
	for (int i=0; i<m_pData->GetNumberOfLevels(); ++i)
	{
		VveVtkPolyDataItem *pLevelData = m_pData->GetTypedLevelDataByLevelIndex(i);
		ReleaseObserveable(pLevelData);
	}

	// delete property and lookup table object in case they haven't been overwritten by the set method
	// (additional delete-operations are happening there in case necessary)
	if(m_bIsPropSafeKill)
		delete m_pProperty;

	if(m_bIsLookupTableSafeKill)
		delete m_pLookupTable;
		
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflVisVBOVtkGeometry::Init()
{
	if(IVflVisObject::Init())
	{
		if (InitPipeline())
		{			
			// IAR
			return true;
		}
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflVisVBOVtkGeometry::Update()
{
		int iIndex = m_pData->GetTimeMapper()->GetLevelIndex(
			GetRenderNode()->GetVisTiming()->GetVisualizationTime());
		double dSimTime = m_pData->GetTimeMapper()->GetSimulationTime(
			GetRenderNode()->GetVisTiming()->GetVisualizationTime());

		if (GetVisible()) {
			// VBO magic happens in this function
			UpdateVBO(iIndex);
		}

		if(!GetTransformer())
			return;

		if(m_bTransformerDirty || (m_dLastDrawSimTime != dSimTime))
		{
			VistaTransformMatrix m;
			if(GetTransformer()->GetUnsteadyTransform(dSimTime, m)==true)
			{
				// APPLY THIS!
				if(m_vecActors[iIndex]->GetUserMatrix() == NULL)
				{
					vtkMatrix4x4 *mt = vtkMatrix4x4::New();
					m_vecActors[iIndex]->SetUserMatrix(mt);
					m_oVtkObjectStack.PushObject(mt);
				}

				vtkMatrix4x4 *pM = m_vecActors[iIndex]->GetUserMatrix();

				for(register int r=0; r < 4; ++r)
				{
					for(register int c=0; c < 4; ++c)
					{
						(*pM)[r][c] = m[r][c];
					}
				}
				(*pM).Modified();
			}
			m_dLastDrawSimTime = dSimTime;
		    m_bTransformerDirty = false;
			// @iar: think about diz...
			//GetVisController()->SetBoundsToModified();
		}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVisVBOVtkGeometry::DrawOpaque()
{
	if (!GetVisible())
		return;

	// find out about time level to be displayed
	// rely on time mapper of unsteady data for that ;-)
	// oh, did I mention, that we rely on the visualization controller as well???
	int iIndex = m_pData->GetTimeMapper()->GetLevelIndex(GetRenderNode()->GetVisTiming()->GetVisualizationTime());
		
	if (iIndex < 0)
		return;

	assert(iIndex<m_pData->GetNumberOfLevels() && "time index too big!");

	//if(m_pTransformer)
	//	iIndex = 0;


	// don't forget to lock the data object
	VveVtkPolyDataItem *pLevelData = m_pData->GetTypedLevelDataByLevelIndex(iIndex);
	// pLevelData->TryLockData();

	// TODO: TryLockData() is supposed to be very slow (see underlying mutex'
	// trylockdata-routine comments for details). But for a quick fix of the
	// multi-lock-under-linux problem it will suffice.
	// pLevelData->TryLockData();

	// do we need to re-wire our pipeline?
	if (m_bNeedRewire)
		RewireFilters();

	// check, whether our data is up-to-date
	if (m_vecDataChanged[iIndex])
	{
		// well, it has changed, so update the corresponding poly data mapper
		//vtkPolyData *pPolyData = pLevelData->GetData();
		vtkPolyData *pPolyData = this->GetFilteredPolyData( iIndex );

		if (!pPolyData)
		{
			// if the polydata is empty, just return (without resetting the changed flag!)
			// (but don't forget to unlock the data object!)
			// pLevelData->UnlockData();
			return;
		}
		
		vtkPolyDataMapper *pMapper = (vtkPolyDataMapper *)(m_vecActors[iIndex]->GetMapper());
#if VTK_MAJOR_VERSION > 5
		pMapper->SetInputData(pPolyData);
#else
		pMapper->SetInput(pPolyData);
#endif

		m_vecDataChanged[iIndex] = false;
		m_vecBoundsChanged[iIndex] = true; // indicate individual bounds change

		// force re-computation of visualization boundaries
		m_bBoundsValid = false;
		GetRenderNode()->SetBoundsToModified();
	}

	// unlock the underlying data object
	// pLevelData->UnlockData();

	// if this actor is transparent, don't draw it here
	if(this->GetProperty()->GetOpacity() != 1.0)
		return;

	//*********************************************************************************************
	// Rendering
	//*********************************************************************************************
	DrawVBO(m_pVBOWorkUnits[m_iDrawIndex]);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVisVBOVtkGeometry::DrawTransparent()
{
	if (!GetVisible())
		return;

	// find out about time level to be displayed
	// rely on time mapper of unsteady data for that ;-)
	// oh, did I mention, that we rely on the visualization controller as well???
	int iIndex = m_pData->GetTimeMapper()->GetLevelIndex(GetRenderNode()->GetVisTiming()->GetVisualizationTime());

	if (iIndex < 0)
		return;

	assert(iIndex<m_pData->GetNumberOfLevels() && "time index too big!");

	// don't forget to lock the data object
	VveVtkPolyDataItem *pLevelData = m_pData->GetTypedLevelDataByLevelIndex(iIndex);
	//pLevelData->TryLockData();

	// TODO: TryLockData() is supposed to be very slow (see underlying mutex'
	// trylockdata-routine comments for details). But for a quick fix of the
	// multi-lock-under-linux problem it will suffice.
	// pLevelData->TryLockData();

	// do we need to re-wire our pipeline?
	if (m_bNeedRewire)
		RewireFilters();

	// check, whether our data is up-to-date
	if (m_vecDataChanged[iIndex])
	{
		// well, it has changed, so update the corresponding poly data mapper
		//vtkPolyData *pPolyData = pLevelData->GetData();
		vtkPolyData *pPolyData = this->GetFilteredPolyData( iIndex );

		if (!pPolyData)
		{
			// if the polydata is empty, just return (without resetting the changed flag!)
			// (but don't forget to unlock the data object!)
			// pLevelData->UnlockData();
			return;
		}

		vtkPolyDataMapper *pMapper = (vtkPolyDataMapper *)(m_vecActors[iIndex]->GetMapper());
#if VTK_MAJOR_VERSION > 5
		pMapper->SetInputData(pPolyData);
#else
		pMapper->SetInput(pPolyData);
#endif

		m_vecDataChanged[iIndex] = false;

		// force recomputation of visualization boundaries
		GetRenderNode()->SetBoundsToModified();
	}

	// unlock the underlying data object
	// pLevelData->UnlockData();

	// if this actor is opaque, don't draw it here
	if(this->GetProperty()->GetOpacity() == 1.0) {
		pLevelData->UnlockData();
		return;
	}

	//*********************************************************************************************
	// Rendering
	//*********************************************************************************************
	DrawVBO(m_pVBOWorkUnits[m_iDrawIndex]);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VflVisVBOVtkGeometry::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	if (!m_bBoundsValid)
	{
		if (!ComputeBounds())
			return false;
	}

	minBounds = m_v3BoundsMin;
	maxBounds = m_v3BoundsMax;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVisVBOVtkGeometry::GetType() const
{
	return IVflVisObject::GetType()+string("::VflVisVBOVtkGeometry");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflVisVBOVtkGeometry::Debug(std::ostream & out) const
{
	IVflVisObject::Debug(out);
	out << " [VflVisVtkGeo] Number of time levels:  " << m_vecActors.size() << endl;
	out << " [VflVisVtkGeo] Bounds: ";
	if (m_bBoundsValid)
		out << m_v3BoundsMin << " - " << m_v3BoundsMax << endl;
	else
		out << "*invalid*" << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetProperty                                                 */
/*                                                                            */
/*============================================================================*/
bool VflVisVBOVtkGeometry::SetProperty(VflVtkProperty *pProperty)
{
	if (m_vecActors.empty())
	{
		if(m_bIsPropSafeKill) {
			delete m_pProperty;
			m_bIsPropSafeKill = false;
		}
		
		m_pProperty = pProperty;
		
		return true;
	}

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBackfaceProperty                                         */
/*                                                                            */
/*============================================================================*/
bool VflVisVBOVtkGeometry::SetBackfaceProperty(VflVtkProperty *pProperty)
{
	if (m_vecActors.empty())
	{
		m_pBackfaceProperty = pProperty;
		return true;
	}

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetLookupTable                                              */
/*                                                                            */
/*============================================================================*/
bool VflVisVBOVtkGeometry::SetLookupTable(VflVtkLookupTable *pLookupTable)
{
	if (m_vecActors.empty())
	{
		if(m_bIsLookupTableSafeKill) {
			delete m_pLookupTable;
			m_bIsLookupTableSafeKill = false;
		}

		m_pLookupTable = pLookupTable;
		return true;
	}

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetData                                                 */
/*                                                                            */
/*============================================================================*/
bool VflVisVBOVtkGeometry::SetData(VveUnsteadyVtkPolyData *pData)
{
	if (m_vecActors.empty())
	{
		m_pData = pData;
		return true;
	}

	return false;
}

bool VflVisVBOVtkGeometry::SetUnsteadyData(VveUnsteadyData *pD)
{
	IVflVisObject::SetUnsteadyData(pD);

	SetData(dynamic_cast<VveUnsteadyVtkPolyData*>(pD));
	return (m_pData != NULL);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetScalarVisibility                                         */
/*                                                                            */
/*============================================================================*/
void VflVisVBOVtkGeometry::SetScalarVisibility(bool bScalarVisibility)
{
	if (m_vecActors.empty())
	{
		return;
	}

	vtkActor *pActor;
	for (ACVEC::size_type i=0; i<m_vecActors.size(); ++i)
	{
		pActor = m_vecActors[i];
		pActor->GetMapper()->SetScalarVisibility(bScalarVisibility);
	}
}

void VflVisVBOVtkGeometry::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	switch(ticket)
	{
	case IVflVisObject::E_PROP_TICKET:
		{
			if(HandlePropertyChange(dynamic_cast<VflVisVBOVtkGeometry::VflVtkGeometryProperties *>
				                    (GetProperties()), msg) == true)
				return;
			break;
		}
	case IVflVisObject::E_TRANSFORMER_TICKET:
		{
			m_bTransformerDirty = true;
			break;
		}
	case E_FILTER_TICKET:
		{
			// find filter
			if(HandleFilterChange(dynamic_cast<VflUnsteadyDataFilter*>(pObserveable), msg) == true)
				return;
			break;
		}
	case E_LEVELDATA_TICKET:
		{
			if(HandleLevelDataChange(dynamic_cast<VveVtkPolyDataItem*>(pObserveable), msg) == true)
				return;
			break;
		}
	default:
		break;
	}

	IVflVisObject::ObserverUpdate(pObserveable, msg, ticket);
}

bool VflVisVBOVtkGeometry::HandlePropertyChange(VflVtkGeometryProperties *p, int msg)
{
	switch(msg)
	{
	case VflVisVBOVtkGeometry::VflVtkGeometryProperties::MSG_SCALARVISIBILITY_CHANGE:
		{
			SetScalarVisibility(p->GetScalarVisibility());
			break;
		}
	case VflVisVBOVtkGeometry::VflVtkGeometryProperties::MSG_IMMEDIATEMODE_CHANGE:
		{
			SetImmediateMode(p->GetImmediateMode());
			break;
		}
	default:
		break;
	}

	return false;
}

bool VflVisVBOVtkGeometry::HandleLevelDataChange(VveVtkPolyDataItem *pCont, int msg)
{
	if(!pCont)
		return false;

	int n = 0;
	for(; n < m_pData->GetNumberOfLevels(); ++n)
	{
		if(m_pData->GetLevelDataByLevelIndex(n) == pCont)
			break;
	}

	if(n < m_pData->GetNumberOfLevels())
	{
			m_vecDataChanged[n] = true;
			m_vecBoundsChanged[n]= true;
			m_bBoundsValid = false;
			return true; // ok to stop here
	}
	return false;
}

bool VflVisVBOVtkGeometry::HandleFilterChange(VflUnsteadyDataFilter *pFilter, int msg)
{
	if(msg == VflUnsteadyDataFilter::MSG_NEED_REWIRE)
	{
		m_bNeedRewire = true;
		return true; // ok to stop here
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetImmediateMode                                            */
/*                                                                            */
/*============================================================================*/
void VflVisVBOVtkGeometry::SetImmediateMode(bool bImmediateMode)
{
	if (m_vecActors.empty())
	{
		return;
	}

	vtkActor *pActor;
	for (ACVEC::size_type i=0; i<m_vecActors.size(); ++i)
	{
		pActor = m_vecActors[i];
		pActor->GetMapper()->SetImmediateModeRendering(bImmediateMode);
	}
}

bool VflVisVBOVtkGeometry::SetTexture(vtkTexture *pTexture)
{
	if(m_vecActors.empty())
	{
		m_pTexture = pTexture;
		return true;
	}

	for (ACVEC::size_type i=0; i<m_vecActors.size(); ++i)
	{
		m_vecActors[i]->SetTexture(pTexture);
	}
	return true;
}

vtkTexture *VflVisVBOVtkGeometry::GetTexture() const
{
	if(m_vecActors.empty())
		return m_pTexture;
	return m_vecActors[0]->GetTexture();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   InitPipeline                                                */
/*                                                                            */
/*============================================================================*/
bool VflVisVBOVtkGeometry::InitPipeline()
{
	if (!m_vecActors.empty())
	{
		vstr::warnp() << " [VflVisVtkGeo] Actors already initialized..." << endl;
		return false;
	}

	if (!m_pData)
	{
		vstr::errp() << " [VflVisVtkGeo] Unable to init pipeline! No VflUnsteadyData object given!" << endl;
		return false;
	}

	if (!m_pData->GetNumberOfLevels())
	{
		vstr::errp() << " [VflVisVtkGeo] Unable to init pipeline! Cannot find data..." << endl;
		return false;
	}

	// what we do here is create a VflSharedVtkProperties object and a VflVtkLookupTable (if necessary)
	// and associate them with every single actor (and its mapper, respectively)
	// we don't create a special backface property object, though. vtk uses the same property
	// for frontface and backface rendering, if the backface property is not set - basically,
	// we do the same thing...
	if (!m_pProperty)
	{
		m_pProperty = new VflVtkProperty();
		m_bIsPropSafeKill = true;
	}

	if (!m_pLookupTable)
	{
		m_pLookupTable = new VflVtkLookupTable();
		m_bIsLookupTableSafeKill = true;
	}

	// create actors and poly data mappers for the data objects 
	// and associate them with the poly data from the data object
	// in addition set the corresponding lookup table and property
	int i;
	vtkPolyDataMapper *pMapper;
	vtkActor *pActor;
	vtkPolyData *pPolyData;
	m_vecActors.resize(m_pData->GetNumberOfLevels());
	m_vecDataChanged.resize(m_pData->GetNumberOfLevels());
    m_vecBoundsChanged.resize(m_pData->GetNumberOfLevels());
	for (i=0; i<m_pData->GetNumberOfLevels(); ++i)
	{
		// create the pipeline
		pMapper = vtkPolyDataMapper::New();
		VflVtkGeometryProperties *p = dynamic_cast<VflVtkGeometryProperties*>(GetProperties());

		m_oVtkObjectStack.PushObject(pMapper);
		pMapper->SetLookupTable(m_pLookupTable->GetLookupTable());
		pMapper->SetScalarVisibility(p->GetScalarVisibility());
		if (p->GetScalarVisibility())
			pMapper->UseLookupTableScalarRangeOn();
		pMapper->SetImmediateModeRendering(p->GetImmediateMode());

		pActor = vtkActor::New();
		m_oVtkObjectStack.PushObject(pActor);
		pActor->SetMapper(pMapper);
		pActor->SetProperty(m_pProperty->GetVtkProperty());
		if (m_pBackfaceProperty)
			pActor->SetBackfaceProperty(m_pBackfaceProperty->GetVtkProperty());

		m_vecActors[i] = pActor;

		// now, retrieve the data and (if present) register it
		VveVtkPolyDataItem *pLevelData = m_pData->GetTypedLevelDataByLevelIndex(i);
		//pPolyData = pLevelData->GetData();
		pPolyData = this->GetFilteredPolyData( i );

		// while we're at it -> register self to be notified of data updates
		Observe(pLevelData, E_LEVELDATA_TICKET);

		if (pPolyData)
		{
#if VTK_MAJOR_VERSION > 5
			pMapper->SetInputData(pPolyData);
#else
			pMapper->SetInput(pPolyData);
#endif
			// well, we're still initializing, so it might be a good
			// idea to do any pending update now...
			pMapper->Update();

			m_vecDataChanged[i] = false;
            m_vecBoundsChanged[i] = true;
		}
		else
		{
			// we set the "changed" flag to true, to allow for checking
			// the presence of valid data
			m_vecDataChanged[i] = true;
            /* maybe the bounds changed but there is currently no inbound for 
            the mapper and this might be fatal if recomputation of bounds is attempted nonetheless */
            m_vecBoundsChanged[i] = false;
		}

		// we now have actors, so we can safely set pre-set textures

	}
	if(m_pTexture)
	{
		SetTexture(m_pTexture);
	}

	// don't forget the objects' boundaries
	ComputeBounds();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
bool VflVisVBOVtkGeometry::ComputeBounds()
{
	bool bBoundsValid = false;
	double aBounds[6], aTemp[6];

	for (ACVEC::size_type i=0; i<m_vecActors.size(); ++i)
	{
		assert(m_vecActors[i] && "ERROR - NULL pointer in actor vector...");

		// @todo: skip empty vtkPolyDatas
		// as GetBounds() does return non-sense
		// on empty vtkPolys
		if (true)
		{
			// TODO_MID: Non-sense lock?!
			// lock data...
			// m_pData->GetLevelDataByLevelIndex(i)->TryLockData();

			// bBoundsValid == true iff we already got
			// some values to aBounds (first-time-switch)
			if (bBoundsValid)
			{
				m_vecActors[i]->GetBounds(aTemp);
				if (aBounds[0] > aTemp[0])
					aBounds[0] = aTemp[0];
				if (aBounds[1] < aTemp[1])
					aBounds[1] = aTemp[1];
				if (aBounds[2] > aTemp[2])
					aBounds[2] = aTemp[2];
				if (aBounds[3] < aTemp[3])
					aBounds[3] = aTemp[3];
				if (aBounds[4] > aTemp[4])
					aBounds[4] = aTemp[4];
				if (aBounds[5] < aTemp[5])
					aBounds[5] = aTemp[5];
			}
			else
			{
				// initialize aBounds with bounds
				// from actor[i]
				m_vecActors[i]->GetBounds(aBounds);

				// switch first-time flag to true
				bBoundsValid = true;
			}

			// TODO_MID: See lock above.
			// unlock data...
			// m_pData->GetLevelDataByLevelIndex(int(i))->UnlockData();

            m_vecBoundsChanged[i] = false;
		}
	}

	if (bBoundsValid)
	{
		m_v3BoundsMin = VistaVector3D(aBounds[0], aBounds[2], aBounds[4]);
		m_v3BoundsMax = VistaVector3D(aBounds[1], aBounds[3], aBounds[5]);
		m_bBoundsValid = true;
		return true;
	}

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Notify                                                      */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddFilter                                                   */
/*                                                                            */
/*============================================================================*/
void VflVisVBOVtkGeometry::AddFilter( VflUnsteadyDataFilter * pFilter )
{
	if( pFilter )
	{
		pFilter->CreateFilters(m_pData->GetNumberOfLevels());
		m_vecFilters.push_back(pFilter);
		m_bNeedRewire = true;

		Observe(pFilter, E_FILTER_TICKET);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemoveFilter                                                */
/*                                                                            */
/*============================================================================*/
void VflVisVBOVtkGeometry::RemoveFilter(FLTIDX iIndex)
{
	if (iIndex<0 || iIndex>=m_vecFilters.size())
		return;

	VflUnsteadyDataFilter *pFilter = m_vecFilters[iIndex];
	ReleaseObserveable(pFilter);

	m_vecFilters.erase(m_vecFilters.begin()+iIndex);

	m_bNeedRewire = true;

}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilterIndex                                              */
/*                                                                            */
/*============================================================================*/
VflVisVBOVtkGeometry::FLTIDX VflVisVBOVtkGeometry::GetFilterIndex(VflUnsteadyDataFilter *pFilter) const
{
	if (!pFilter)
		return -1;

	for (FLTIDX i=0; i < m_vecFilters.size(); ++i)
	{
		if (m_vecFilters[i] == pFilter)
			return i;
	}

	return FLTIDX(-1);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilter                                                   */
/*                                                                            */
/*============================================================================*/
VflUnsteadyDataFilter *VflVisVBOVtkGeometry::GetFilter(FLTIDX iIndex) const
{
	if (0<=iIndex && iIndex<m_vecFilters.size())
		return m_vecFilters[iIndex];

	return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilterCount                                              */
/*                                                                            */
/*============================================================================*/
VflVisVBOVtkGeometry::FLTIDX VflVisVBOVtkGeometry::GetFilterCount() const
{
	return m_vecFilters.size();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RewireFilters                                               */
/*                                                                            */
/*============================================================================*/
void VflVisVBOVtkGeometry::RewireFilters()
{
	if (!m_pData)
		return;

	vector<vtkPolyData *> vecPolyData;
	vecPolyData.resize(m_pData->GetNumberOfLevels());

	for (int i=0; i<m_pData->GetNumberOfLevels(); ++i)
	{
		vecPolyData[i]= m_pData->GetTypedLevelDataByLevelIndex(i)->GetData();
		m_vecDataChanged[i] = true;
	}

	for (FLTIDX j=0; j<m_vecFilters.size(); ++j)
	{
		VflUnsteadyDataFilter *pFilter = m_vecFilters[j];

		const int nNumPolyDatas = static_cast<int>(vecPolyData.size());
		for (int i=0; i<nNumPolyDatas; ++i)
		{
			pFilter->SetInput(i, vecPolyData[i]);
			vecPolyData[i] = pFilter->GetOutput(i);
		}
	}

	m_bNeedRewire = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilteredPolyData                                         */
/*                                                                            */
/*============================================================================*/
vtkPolyData * VflVisVBOVtkGeometry::GetFilteredPolyData( int iIndex )
{
	vtkPolyData * pUnfilteredData = 
					m_pData->GetTypedLevelDataByLevelIndex( iIndex )->GetData();
	if( pUnfilteredData )
	{
		if( m_vecFilters.empty() )
		{
			return pUnfilteredData;
		}
		else
		{
			if (pUnfilteredData != m_vecFilters[0]->GetInput(iIndex))
			{
				// whoops - data input has changed. Double buffered polydata
				// or something like that?
				// anyway - we have to re-wire
				RewireFilters();
			}

			return m_vecFilters[m_vecFilters.size()-1]->GetOutput( iIndex );
		}
	}
	else
		return NULL;
}


std::string   VflVisVBOVtkGeometry::GetFactoryType()
{
	return std::string("VTK_GEOMETRY");
}

IVflVisObject::VflVisObjProperties    *VflVisVBOVtkGeometry::CreateProperties() const
{
	return new VflVtkGeometryProperties;
}

bool VflVisVBOVtkGeometry::InitVBO(int buffersize)
{
	VBO_BUFFER_SIZE = buffersize;
	if(VBO_BUFFER_SIZE < 1)
		return false;
	else if(VBO_BUFFER_SIZE < 2)
		m_eVBOUsageMode = VBO_SINGLE;
	else
		m_eVBOUsageMode = VBO_PARALLEL;

	if(glewIsSupported("GL_VERSION_2_0"))
	{
		// set OpenGL state
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_INDEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		// store all the buffers in class members so they can be (easily) deleted in the destructor
		m_pVertexBuffers  = new unsigned int[VBO_BUFFER_SIZE];
		m_pElementBuffers = new unsigned int[VBO_BUFFER_SIZE];
		
		// create the desired number of vbos ...
		glGenBuffersARB(VBO_BUFFER_SIZE, m_pVertexBuffers);
		glGenBuffersARB(VBO_BUFFER_SIZE, m_pElementBuffers);

		// create all the work units
		m_pVBOWorkUnits = new VBOWorkUnit[VBO_BUFFER_SIZE];

		// ... and initialize the corresponding buffer pairs
		for(int i = 0; i < VBO_BUFFER_SIZE; ++i) {
			m_pVBOWorkUnits[i].iIdentifier				= i;
			m_pVBOWorkUnits[i].bDoCleanUp				= false;
			
			m_pVBOWorkUnits[i].uiVertexBufferHandle		= m_pVertexBuffers[i];
			m_pVBOWorkUnits[i].uiElementBufferHandle	= m_pElementBuffers[i];
			m_pVBOWorkUnits[i].iIndex					= -1;
			m_pVBOWorkUnits[i].nSizeOfElementBuffer		= 0;
			m_pVBOWorkUnits[i].uiBufferOffset			= 0;
			m_pVBOWorkUnits[i].bScalarVisFlag			= false;
			m_pVBOWorkUnits[i].bIsTriangleStrip			= false;
			m_pVBOWorkUnits[i].pIsReadyToMod			= new VistaThreadEvent(VistaThreadEvent::WAITABLE_EVENT);

			m_pVBOWorkUnits[i].pWorkInstance			= new CVBOWorkInstance(this);

			m_pVBOWorkUnits[i].pLevelData				= NULL;
		}

		// set the starting indices
		m_iDrawIndex		= 0;	// vbo buffer pair index
		m_iFillIndex		= 0;	// vbo buffer pair index
		m_iLookAheadIndex	= -1;	// vtk leveldata index

		// create new thread pool (max. 2 threads, max. VBO_BUFFER_SIZE tasks)
		m_pThreadPool = new VistaThreadPool(2, VBO_BUFFER_SIZE);
		m_pThreadPool->StartPoolWork();

		return true;
	}
	else
		vstr::warnp() << "[VflVisVBOVtkGeometry] No OpenGL 2.0 found!" << endl;

	return false;
}

void VflVisVBOVtkGeometry::KillVBO()
{
	// stop the thread pool and kill it
	m_pThreadPool->WaitForAllJobsDone();
	m_pThreadPool->StopPoolWork();
	delete m_pThreadPool;

	// delete all the work instances
	for(int i = 0; i < VBO_BUFFER_SIZE; ++i) {
		delete m_pVBOWorkUnits[i].pWorkInstance;
		delete m_pVBOWorkUnits[i].pIsReadyToMod;
	}

	// free the vertex buffer objects
	glDeleteBuffersARB(VBO_BUFFER_SIZE, m_pVertexBuffers);
	glDeleteBuffersARB(VBO_BUFFER_SIZE, m_pElementBuffers);

	// delete the storage vars
	delete [] m_pVertexBuffers;
	delete [] m_pElementBuffers;
	delete [] m_pVBOWorkUnits;
}

bool VflVisVBOVtkGeometry::DrawVBO(VBOWorkUnit &oTarget)
{
	if(!m_bIsVBOInited || oTarget.nSizeOfElementBuffer == 0)
		return false;

	CleanUpAsyncFill(oTarget);

	// @todo : this eliminates vtk-complains about missing opengl-renderer
	GetProperty()->GetVtkProperty()->Render(NULL, vtkOpenGLRenderer::New());//NULL

	//*********************************************************************************************
	// set up opengl state
	//*********************************************************************************************
	glPushAttrib(GL_LIGHTING_BIT | GL_POLYGON_BIT | GL_ENABLE_BIT);
	
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_BLEND);
	glDisable(GL_CULL_FACE);

	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	

	//*********************************************************************************************
	// set up buffers
	//*********************************************************************************************
	glBindBufferARB(GL_ARRAY_BUFFER, oTarget.uiVertexBufferHandle);
	glVertexPointer(3, GL_FLOAT, 0, 0);
	glNormalPointer(GL_FLOAT, 0, (GLvoid*)(oTarget.uiBufferOffset * sizeof(float)));
	
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, oTarget.uiElementBufferHandle);
	glIndexPointer(GL_INT, 0, 0);

	if(!oTarget.bScalarVisFlag)
		glDisableClientState(GL_COLOR_ARRAY);
	else {
		glEnableClientState(GL_COLOR_ARRAY);
		glColorPointer(4, GL_UNSIGNED_BYTE, 0, (GLvoid*)(oTarget.uiBufferOffset * 2 * sizeof(float)));
	}

	/*********************************************************************************************/
	// actual render call
	/*********************************************************************************************/
	if(oTarget.bIsTriangleStrip)
		glDrawElements(GL_TRIANGLE_STRIP, oTarget.nSizeOfElementBuffer, GL_UNSIGNED_INT, 0);
	else
		glDrawElements(GL_TRIANGLES, oTarget.nSizeOfElementBuffer, GL_UNSIGNED_INT, 0);

	/*********************************************************************************************/
	// unbind buffers + reset opengl state
	/*********************************************************************************************/
	glBindBufferARB(GL_ARRAY_BUFFER, 0);
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);

	glPopAttrib();

	return true;
}

bool VflVisVBOVtkGeometry::UpdateVBO(int iIndex)
{
	if(iIndex == m_iLastDrawnIndex)
		return false;

	// In case the loop time changes the vbo caches will be rebuild.
	if(m_fLastLoopTime != GetRenderNode()->GetVisTiming()->GetLoopTime())
	{
		for(int i = 0; i < VBO_BUFFER_SIZE; ++i) {
			CleanUpAsyncFill(m_pVBOWorkUnits[i]);
			m_pVBOWorkUnits[i].iIndex = -1;
		}

		m_iDrawIndex = m_iFillIndex = 0;
	}

	m_fLastLoopTime = GetRenderNode()->GetVisTiming()->GetLoopTime();
		
	// TODO: This if-clause is supposed to check whether a change in the
	//		 simulations playback direction (forward <-> backward) has occured.
	//		 But it does the check using a couple of magic numbers and assumptions
	//		 which should be eliminated.
	//		 If a changed occured the buffers will be flushed and rebuild.
	if ( (m_iSimDir == 1 && ((iIndex - m_iLastDrawnIndex) > 0.3f * m_pData->GetNumberOfLevels() || 
		(m_iLastDrawnIndex - iIndex) > 0 && (m_iLastDrawnIndex - iIndex) < 0.3f * m_pData->GetNumberOfLevels()))
		||
		(m_iSimDir == -1 && ((iIndex - m_iLastDrawnIndex) < -0.3f * m_pData->GetNumberOfLevels() || 
		(iIndex - m_iLastDrawnIndex) > 0 && (iIndex - m_iLastDrawnIndex) < 0.3f * m_pData->GetNumberOfLevels())))
	{
		m_iSimDir *= -1;		
		
		// if we change the movement dir we'll dump the cache
		for(int i = 0; i < VBO_BUFFER_SIZE; ++i) {
			CleanUpAsyncFill(m_pVBOWorkUnits[i]);
			m_pVBOWorkUnits[i].iIndex = -1;
		}

		m_iDrawIndex = m_iFillIndex = 0;
	}

	m_iLastDrawnIndex = iIndex;

	// search for the requested level data index and count cache misses while doing so
	int iCacheMisses = 0;
	if(m_eVBOUsageMode == VBO_PARALLEL)
	{
		for(int i = 0; i < VBO_BUFFER_SIZE; ++i) {
			if(m_pVBOWorkUnits[m_iDrawIndex].iIndex == iIndex)
				break;

			++m_iDrawIndex;
			m_iDrawIndex %= VBO_BUFFER_SIZE;

			++iCacheMisses;
		}

		if(iCacheMisses == VBO_BUFFER_SIZE)
			m_iLookAheadIndex = iIndex;
	}
	else if(m_eVBOUsageMode == VBO_SINGLE || m_eVBOUsageMode == VBO_SERIAL)
	{
		if(m_pVBOWorkUnits[m_iDrawIndex].iIndex != iIndex)
			iCacheMisses = 1;

		m_iDrawIndex = m_iFillIndex = 0;
		m_iLookAheadIndex = iIndex;
	}

	for(int i = 0; i < iCacheMisses; ++i) {
		// clean up the required work unit ...
		CleanUpAsyncFill(m_pVBOWorkUnits[m_iFillIndex]);
		
		// ... and prepare it for the next run
		PrepareAsyncFill(m_iLookAheadIndex, m_pVBOWorkUnits[m_iFillIndex]);

		// add it to the thread pool for execution
		if(iCacheMisses == VBO_BUFFER_SIZE && i == 0 || m_eVBOUsageMode == VBO_SERIAL)
			m_pVBOWorkUnits[m_iFillIndex].pWorkInstance->ThreadWork();
		else
			m_pThreadPool->AddWork(m_pVBOWorkUnits[m_iFillIndex].pWorkInstance);

		// index updating
		m_iLookAheadIndex += m_iSimDir;
		if(m_iLookAheadIndex < 0) 
			m_iLookAheadIndex += m_pData->GetNumberOfLevels();
		m_iLookAheadIndex %= m_pData->GetNumberOfLevels();

		++m_iFillIndex;
		m_iFillIndex %= VBO_BUFFER_SIZE;
	}

	return true;
}

bool VflVisVBOVtkGeometry::PrepareAsyncFill(int iIndex, VBOWorkUnit &oTarget)
{
	// ********************************************************************************************
	// preparation of the data objects
	// ********************************************************************************************
	
	// set which index pointer to the vbo buffer pair we are gonna use
	oTarget.pWorkInstance->SetWorkUnitID(oTarget.iIdentifier);

	// set the index of the data that will be filled into the vbo
	oTarget.iIndex = iIndex;

	// lock data
	oTarget.pLevelData = m_pData->GetTypedLevelDataByLevelIndex(iIndex);
	if(!oTarget.pLevelData) {
		// set the size to zero (can be used to check for error/empty data later)
		oTarget.nSizeOfElementBuffer = 0;
		return false;
	}
	oTarget.pLevelData->LockData();
	
	// first try to get us some polydata
	vtkPolyData *pPolyData = oTarget.pLevelData->GetData(); // this->GetFilteredPolyData( iIndex );
	if(!pPolyData) {
		// set the size to zero (can be used to check for error/empty data later)
		oTarget.nSizeOfElementBuffer = 0;
		return false;
	}
	oTarget.pWorkInstance->SetPolyData(pPolyData);

	// now check whether we would like to use lookup tables
	VflVtkGeometryProperties *pGeoProps = static_cast<VflVtkGeometryProperties*>(GetProperties());
	if(!pGeoProps) {
		oTarget.bScalarVisFlag = false;
	}
	oTarget.bScalarVisFlag = pGeoProps->GetScalarVisibility();
	
	// then determine whether the polydata consists of triangles XOR triangle strips
	oTarget.bIsTriangleStrip = pPolyData->GetNumberOfStrips() > 0 ? true : false;


	// ********************************************************************************************
	// first big part here: prepare the Vertex Buffer Object
	// ********************************************************************************************

	int iSizeModifier = oTarget.bScalarVisFlag ? 1 : 0;
		
	vtkIdType iNumPoints = pPolyData->GetNumberOfPoints();
	oTarget.uiBufferOffset = 3 * iNumPoints;

	// in case we need to store color data as well iSizeModifier will be 1,
	// and hence additional memory will be allocated
	unsigned int uiVBSize = iNumPoints * 
				   (6 * sizeof(float) +					// vertices and normals, 3 components each
					4 * sizeof(char) * iSizeModifier);	// color (optional), 4 components
	
	glBindBufferARB(GL_ARRAY_BUFFER, oTarget.uiVertexBufferHandle);
	
	glBufferDataARB(GL_ARRAY_BUFFER,				// target == vertex buffer
				uiVBSize,						// size
				NULL,							// no data specified yet
				GL_DYNAMIC_DRAW);				// we'll update our data every frame
		
	oTarget.pWorkInstance->SetVertexPtr((float*)glMapBufferARB(GL_ARRAY_BUFFER, GL_WRITE_ONLY));


	// ********************************************************************************************
	// second big part: prepare the Element Buffer Object
	// ********************************************************************************************

	// will hold size of the element buffer
	unsigned int uiEBSize = 0;

	if(oTarget.bIsTriangleStrip) {
		// # of pts. == # of Con. Entries - # of Strips (if wrong, use above for-loop to determine # of pts.)
		uiEBSize += pPolyData->GetStrips()->GetData()->GetNumberOfTuples() - pPolyData->GetNumberOfStrips();

		// takes into account degenerate triangles
		uiEBSize += 2 * pPolyData->GetNumberOfStrips() - 2;
	}
	else {
		// simply take the + of polygons for none-tri-strip rendering
		uiEBSize = 3 * pPolyData->GetNumberOfPolys();;
	}

	oTarget.nSizeOfElementBuffer = uiEBSize;
	uiEBSize *= sizeof(int);
	
	// building the element buffer
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, oTarget.uiElementBufferHandle);
	glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER,		// target == index buffer
					uiEBSize,					// size
					NULL,						// no data yet (--> later via mapping the buffer)
					GL_DYNAMIC_DRAW);			// update every frame
	
	oTarget.pWorkInstance->SetElementPtr((int*)glMapBufferARB(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY));

	oTarget.bDoCleanUp = true;

	// signal success
	return true;
}

bool VflVisVBOVtkGeometry::CleanUpAsyncFill(VBOWorkUnit &oTarget)
{
	if(!oTarget.bDoCleanUp || oTarget.iIndex == -1)
		return false;

	oTarget.bDoCleanUp = false;

	// wait for filling to finish
	if(m_eVBOUsageMode == VBO_PARALLEL) {
		oTarget.pIsReadyToMod->WaitForEvent(true);
		oTarget.pIsReadyToMod->ResetThisEvent();
	}

	// TODO_MID: Thread work now resetting mutex themselves.
	// 	     Since we wait for a thread to finish (see above) we can
	//	     assume that the data is unlocked here.
	// unlock the vtk data object
	//if(oTarget.pLevelData)
	//	oTarget.pLevelData->UnlockData();

	// bind the buffers we would like to unmap
	glBindBufferARB(GL_ARRAY_BUFFER, oTarget.uiVertexBufferHandle);
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, oTarget.uiElementBufferHandle);

	// unmap those buffers
	GLboolean bIsSuccess = true;
	bIsSuccess &= glUnmapBufferARB(GL_ARRAY_BUFFER);
	bIsSuccess &= glUnmapBufferARB(GL_ELEMENT_ARRAY_BUFFER);

	// opengl clean up (unbind vbos)
	glBindBufferARB(GL_ARRAY_BUFFER, 0);
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);

	return (bIsSuccess == GL_TRUE);
}

VflVisVBOVtkGeometry::VBOModes VflVisVBOVtkGeometry::ToggleVBOMode()
{
	// parallel --> serial
	if(m_eVBOUsageMode == VBO_PARALLEL) {
		for(int i = 0; i < VBO_BUFFER_SIZE; ++i)
			CleanUpAsyncFill(m_pVBOWorkUnits[i]);

		// m_pThreadPool->StopPoolWork();

		m_eVBOUsageMode = VBO_SERIAL;
	}
	// serial --> parallel
	else if(m_eVBOUsageMode == VBO_SERIAL) {
		CleanUpAsyncFill(m_pVBOWorkUnits[0]);
		
		// dumb (re-init) the cache (important to prevent a deadlock in the cleanup func
		// when switching back to parallel mode)
		for(int i = 0; i < VBO_BUFFER_SIZE; ++i)
			m_pVBOWorkUnits[i].iIndex = -1;
		m_iDrawIndex = m_iFillIndex = 0;

		// m_pThreadPool->StartPoolWork();
		m_eVBOUsageMode = VBO_PARALLEL;
	}
	else
		return VBO_UNDEFINED;

	return m_eVBOUsageMode;
}

VflVisVBOVtkGeometry::VBOModes VflVisVBOVtkGeometry::GetVBOMode() {
	return m_eVBOUsageMode;
}

// ############################################################################

VflVisVBOVtkGeometry::CVBOWorkInstance::CVBOWorkInstance(VflVisVBOVtkGeometry *pParent):
m_pParent(pParent),
m_pVertexPtr(NULL), 
m_pElementPtr(NULL),
m_pPolyData(NULL),
m_iWorkUnitID(-1) {
}

VflVisVBOVtkGeometry::CVBOWorkInstance::~CVBOWorkInstance() {
}

void VflVisVBOVtkGeometry::CVBOWorkInstance::DefinedThreadWork()
{
	if(m_iWorkUnitID == -1)
		return;
	
	if(!m_pParent->m_bIsVBOInited || !m_pPolyData || !m_pVertexPtr || !m_pElementPtr) {
		m_pParent->m_pVBOWorkUnits[m_iWorkUnitID].pLevelData->UnlockData();
		m_pParent->m_pVBOWorkUnits[m_iWorkUnitID].pIsReadyToMod->SignalEvent();
		return;
	}

	/*********************************************************************************************/
	// filling the vertex buffer from here
	/*********************************************************************************************/
	
 	float *pVBufferPtr = m_pVertexPtr;
	
	/*********************************************************************************************/
	// adding the vertex coordinates here
	vtkIdType iNumPoints = m_pPolyData->GetNumberOfPoints();

	memcpy(	pVBufferPtr,											// destination (vertex buffer)
			m_pPolyData->GetPoints()->GetData()->GetVoidPointer(0), // source (vtkdataarray 0th element)
			sizeof(float) * iNumPoints * 3);						// size (3 components * iNumPoints points * data type)
	
	// update pVBufferPtr according to copied # of points
	pVBufferPtr += 3 * iNumPoints;

	/*********************************************************************************************/
	// adding the normals here
	vtkDataArray *pData = m_pPolyData->GetPointData()->GetNormals();
	void* pDataPtr = NULL;

	memcpy( pVBufferPtr,
			m_pPolyData->GetPointData()->GetNormals()->GetVoidPointer(0),
			3 * iNumPoints * sizeof(float));
	pVBufferPtr += 3 * iNumPoints;

	/*********************************************************************************************/
	// adding (optional) color data here
	if(m_pParent->m_pVBOWorkUnits[m_iWorkUnitID].bScalarVisFlag)
	{	
		pData						= NULL;							// pointer to the scalars
		double dDataValue;											// retrieved, single scalar
		vtkLookupTable *pLookupTbl	= NULL;							// lookup table to use
		unsigned char *pColor		= NULL;							// color returned by lookup table using scalar pDataValue
		unsigned char *pCBufferPtr	= (unsigned char*) pVBufferPtr;	// re-casted pointer to the mapped vbo memory
		float fDiffuseCoeff = m_pParent->GetProperty()->GetDiffuse();
		float fAmbientCoeff = m_pParent->GetProperty()->GetAmbient();
		
		// retireve the scalars
		pData = m_pPolyData->GetPointData()->GetScalars();

		// retrieve the lookup table
		pLookupTbl = m_pParent->GetLookupTable()->GetLookupTable();
		
		if(pData && pLookupTbl)
		{
			// retrieve the color for every point and fill it into the vertex buffer
			for(int iPID = 0; iPID < iNumPoints; ++iPID)
			{
				dDataValue	= pData->GetTuple1(iPID);
				pColor		= pLookupTbl->MapValue(dDataValue);

				// TODO/FIX?
				// The computation here will behave differently on the Diffuse and Ambient Coefficients
				// of the vtkProperty object of this class. Hence the colors will be slightly different
				// from the vtk-only renderer.
				pCBufferPtr[0] = (unsigned char)(pColor[0] * 0.5f * (fDiffuseCoeff + fAmbientCoeff));
				pCBufferPtr[1] = (unsigned char)(pColor[1] * 0.5f * (fDiffuseCoeff + fAmbientCoeff));
				pCBufferPtr[2] = (unsigned char)(pColor[2] * 0.5f * (fDiffuseCoeff + fAmbientCoeff));
				pCBufferPtr[3] = (unsigned char)(pColor[3]);
				pCBufferPtr += 4;
			}
		} // if(pData && pLookupTbl)
	} // if(!bScalarVisFlag)

	/*********************************************************************************************/
	// filling of the element buffer from here
	/*********************************************************************************************/
	
	// pointer to a set of indices belonging to one cell
	vtkIdType *pPoints = NULL;

	// var for holding # of points/indices of one cell
	vtkIdType iPointCount = 0;

	// number of polygons/strips
	vtkIdType iNumPolys = 0; 
	vtkIdType iNumStrips = 0;

	if(m_pParent->m_pVBOWorkUnits[m_iWorkUnitID].bIsTriangleStrip)
		iNumStrips = m_pPolyData->GetNumberOfStrips();
	else
		iNumPolys = m_pPolyData->GetNumberOfPolys();
	
	int *pEBufferPtr = m_pElementPtr;

	/*********************************************************************************************/
	// filling in the index data from here
	vtkCellArray *pPolysStrips = NULL;
	if(m_pParent->m_pVBOWorkUnits[m_iWorkUnitID].bIsTriangleStrip)
		pPolysStrips = m_pPolyData->GetStrips();
	else
		pPolysStrips = m_pPolyData->GetPolys();

	int iRange = m_pParent->m_pVBOWorkUnits[m_iWorkUnitID].bIsTriangleStrip ? iNumStrips : iNumPolys;


	vtkIdType *pStartPoint = NULL;
	vtkIdType *pEndPoint = NULL;
	for(vtkIdType i = 0, iCID = 0; i < iRange; ++i)
	{
		// get the offset pPoints to the next iPointCount number of indices (these references the vertices of a whole cell)
		pPolysStrips->GetCell(iCID, iPointCount, pPoints);
		
		if(m_pParent->m_pVBOWorkUnits[m_iWorkUnitID].bIsTriangleStrip) {
			if(i == 0) {
				pStartPoint = NULL;
				pEndPoint = pPoints + iPointCount - 1;
			}
			else if(i == iRange-1) {
				pStartPoint = pPoints;
				pEndPoint = NULL;
			}
			else {
				pStartPoint = pPoints;
				pEndPoint = pPoints + iPointCount - 1;
			}

			if(pStartPoint) {
				*pEBufferPtr = *pStartPoint;
				++pEBufferPtr;
			}
		}

		memcpy(pEBufferPtr, pPoints, iPointCount * sizeof(int));		
		pEBufferPtr += iPointCount;
		
		if(m_pParent->m_pVBOWorkUnits[m_iWorkUnitID].bIsTriangleStrip && pEndPoint) {
			*pEBufferPtr = *pEndPoint;
			++pEBufferPtr;
		}

		iCID += iPointCount + 1;
	}

	// Unlock Data.
	m_pParent->m_pVBOWorkUnits[m_iWorkUnitID].pLevelData->UnlockData();

	// Send 'finish' signal.
	m_pParent->m_pVBOWorkUnits[m_iWorkUnitID].pIsReadyToMod->SignalEvent();

	// thread's own clean-up mechanism (invalidation so it can't be simply re-added to the thread pool!)
	m_pVertexPtr	= NULL;
	m_pElementPtr	= NULL;
	m_pPolyData	= NULL;
	m_iWorkUnitID	= -1;
}

void VflVisVBOVtkGeometry::CVBOWorkInstance::SetVertexPtr(float *pVertexPtr) {
	m_pVertexPtr = pVertexPtr;
}

void VflVisVBOVtkGeometry::CVBOWorkInstance::SetElementPtr(int *pElementPtr) {
	m_pElementPtr = pElementPtr;
}

void VflVisVBOVtkGeometry::CVBOWorkInstance::SetPolyData(vtkPolyData *pPolyData) {
	m_pPolyData = pPolyData;
}

void VflVisVBOVtkGeometry::CVBOWorkInstance::SetWorkUnitID(int iWUID) {
	m_iWorkUnitID = iWUID;
}

// ############################################################################


static const string SsReflectionType("VflVisVBOVtkGeometry");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, VflVisVBOVtkGeometry::VflVtkGeometryProperties, VistaProperty::PROPT_BOOL>
	("SCALAR_VISIBILITY", SsReflectionType,
	&VflVisVBOVtkGeometry::VflVtkGeometryProperties::GetScalarVisibility),
	new TVistaPropertyGet<bool, VflVisVBOVtkGeometry::VflVtkGeometryProperties, VistaProperty::PROPT_BOOL>
	("IMMEDIATE_MODE", SsReflectionType,
	&VflVisVBOVtkGeometry::VflVtkGeometryProperties::GetImmediateMode),
	new TVistaPropertyGet<std::string, VflVisVBOVtkGeometry::VflVtkGeometryProperties, VistaProperty::PROPT_STRING>
	("TRANSFERMODE_STRING", SsReflectionType,
	&VflVisVBOVtkGeometry::VflVtkGeometryProperties::GetTransferMode),
	new TVistaPropertyGet<VflVisVBOVtkGeometry::ETransferMode, 
	                      VflVisVBOVtkGeometry::VflVtkGeometryProperties, 
						  VistaProperty::PROPT_DOUBLE>
	("TRANSFERMODE", SsReflectionType,
	&VflVisVBOVtkGeometry::VflVtkGeometryProperties::GetTransferModeId),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool, bool,
	VflVisVBOVtkGeometry::VflVtkGeometryProperties>
	("SCALAR_VISIBILITY", SsReflectionType,
	&VflVisVBOVtkGeometry::VflVtkGeometryProperties::SetScalarVisibility),
	new TVistaPropertySet<bool, bool,
	VflVisVBOVtkGeometry::VflVtkGeometryProperties>
	("IMMEDIATE_MODE", SsReflectionType,
	&VflVisVBOVtkGeometry::VflVtkGeometryProperties::SetImmediateMode),
	new TVistaPropertySet<const std::string &, std::string,
	VflVisVBOVtkGeometry::VflVtkGeometryProperties>
	("TRANSFERMODE_STRING", SsReflectionType,
	&VflVisVBOVtkGeometry::VflVtkGeometryProperties::SetTransferMode),
	NULL
};

VflVisVBOVtkGeometry::VflVtkGeometryProperties::VflVtkGeometryProperties()
: IVflVisObject::VflVisObjProperties(),
  m_bScalarVisibility(true),
  m_bImmediateMode(false),
  m_iTransfermode(TM_DISPLAY_LIST)
{
}

VflVisVBOVtkGeometry::VflVtkGeometryProperties::~VflVtkGeometryProperties()
{
}
		


bool VflVisVBOVtkGeometry::VflVtkGeometryProperties::GetScalarVisibility() const
{
	return m_bScalarVisibility;
}

bool VflVisVBOVtkGeometry::VflVtkGeometryProperties::SetScalarVisibility(bool bScalarVisibility)
{
	if(compAndAssignFunc<bool>(bScalarVisibility, m_bScalarVisibility))
	{
		Notify(MSG_SCALARVISIBILITY_CHANGE);
		return true;
	}
	return false;
}


bool VflVisVBOVtkGeometry::VflVtkGeometryProperties::GetImmediateMode() const
{
	return m_bImmediateMode;
}


bool VflVisVBOVtkGeometry::VflVtkGeometryProperties::SetImmediateMode(bool bImmediateMode)
{
	if(compAndAssignFunc<bool>(bImmediateMode, m_bImmediateMode) == 1)
	{
		Notify(MSG_IMMEDIATEMODE_CHANGE);
		return true;
	}
	return false;
}


VflVisVBOVtkGeometry::ETransferMode  VflVisVBOVtkGeometry::VflVtkGeometryProperties::GetTransferModeId() const
{
	return m_iTransfermode;
}

bool VflVisVBOVtkGeometry::VflVtkGeometryProperties::SetTransferModeId(ETransferMode nTransferMode)
{
	if(compAndAssignFunc<ETransferMode>(nTransferMode, m_iTransfermode) == 1)
	{
		Notify(MSG_TRANSFERMODE_CHANGE);
		return true;
	}
	return false;
}


std::string VflVisVBOVtkGeometry::VflVtkGeometryProperties::GetTransferMode() const
{
	switch(m_iTransfermode)
	{
	case TM_NONE:
		{
			return "NONE";
		}
	case TM_IMMEDIATE_MODE:
		{
			return "IMMEDIATE_MODE";
		}
	case TM_DISPLAY_LIST:
		{
			return "DISPLAY_LIST";
		}
	case TM_VERTEX_ARRAY:
		{
			return "VERTEX_ARRAY";
		}
	case TM_VERTEX_BUFFER_OBJECT:
		{
			return "VERTEX_BUFFER_OBJECT";
		}
	case TM_LAST_MODE:
	default:
		{
			break;
		}
	}
	return "<none>";
}

bool VflVisVBOVtkGeometry::VflVtkGeometryProperties::SetTransferMode(const std::string &sTransferMode)
{
	if(sTransferMode == "DISPLAYLIST")
	{
		return SetTransferModeId(TM_DISPLAY_LIST);
	}
	else if(sTransferMode == "IMMEDIATE_MODE")
	{
		return SetTransferModeId(TM_IMMEDIATE_MODE);
	}
	else if(sTransferMode == "VERTEX_BUFFER_OBJECT")
	{
		return SetTransferModeId(TM_VERTEX_BUFFER_OBJECT);
	}
	else if(sTransferMode == "VERTEX_ARRAY")
	{
		return SetTransferModeId(TM_VERTEX_ARRAY);
	}
	return false;
}


std::string VflVisVBOVtkGeometry::VflVtkGeometryProperties::GetReflectionableType() const
{
	return SsReflectionType;
}



int VflVisVBOVtkGeometry::VflVtkGeometryProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = VflVisObjProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VisObject.cpp"                                               */
/*============================================================================*/
