/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <GL/glew.h>

#include "VflStaticRangePlot.h"

#include <VistaVisExt/Data/VveTimeSeries.h>

#include <cassert>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflStaticRangePlot::VflStaticRangePlot()
: m_dLastDrawSimTime(-1.0),
  m_dLastDrawTransSimTime(-1.0),
  m_bNeedUpdate(true),
  m_pMinFunc(NULL), m_pMaxFunc(NULL),
  m_fPlotCanvasXmin(-0.8f), m_fPlotCanvasXmax(0.8f),
  m_fPlotCanvasYmin(-0.8f), m_fPlotCanvasYmax(0.8f),
  m_fDepth(0.0f),
  m_fXValueMin(0.0f),
  m_fXValueMax(1.0f),
  m_fYValueMin(0.0f),
  m_fYValueMax(0.0f)
{
	m_v3BoundsMin = VistaVector3D(0, 0, 0);
	m_v3BoundsMax = VistaVector3D(0, 0, 0);

}

VflStaticRangePlot::~VflStaticRangePlot()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflStaticRangePlot::Init()
{
	if(!m_pMinFunc || !m_pMaxFunc)
	{
		vstr::errp() << "[VflStaticRangePlot] min or max function is undefined" << endl;
		return false;
	}

	if(!IVflVisObject::Init())
		return false;
		
	UpdatePlotPoints();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetMinMaxFunction                                           */
/*                                                                            */
/*============================================================================*/
bool VflStaticRangePlot::SetMinMaxFunction(VveTimeSeries *pMinFunc, VveTimeSeries *pMaxFunc)
{
	if(pMinFunc==m_pMinFunc && pMaxFunc==m_pMaxFunc)
		return false;

	if(pMinFunc != m_pMinFunc)
	{
		if(m_pMinFunc)
			ReleaseObserveable(m_pMinFunc, E_UNSTEADYDATA_TICKET);
		m_pMinFunc = pMinFunc;
		if(m_pMinFunc)
			Observe(m_pMinFunc, E_UNSTEADYDATA_TICKET);
	}

	if(pMaxFunc != m_pMaxFunc)
	{
		if(m_pMaxFunc)
			ReleaseObserveable(m_pMaxFunc, E_UNSTEADYDATA_TICKET);
		m_pMaxFunc = pMaxFunc;
		if(m_pMaxFunc)
			Observe(m_pMaxFunc, E_UNSTEADYDATA_TICKET);
	}

//	ComputeMinMaxYValues();
	UpdatePlotPoints();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetMinFunction                                              */
/*                                                                            */
/*============================================================================*/
VveTimeSeries* VflStaticRangePlot::GetMinFunction() const
{
	return m_pMinFunc;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetMaxFunction                                              */
/*                                                                            */
/*============================================================================*/
VveTimeSeries* VflStaticRangePlot::GetMaxFunction() const
{
	return m_pMaxFunc;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetUnsteadyData                                             */
/*                                                                            */
/*============================================================================*/
bool VflStaticRangePlot::SetUnsteadyData(VveUnsteadyData *pData)
{
	vstr::errp() << "[VflStaticRangePlot] SetUnsteadyData: this renderable must get two datas" << endl;
	return false; 
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdatePlotPoints                                            */
/*                                                                            */
/*============================================================================*/
void VflStaticRangePlot::UpdatePlotPoints()
{
	if(!m_bNeedUpdate)
		return;

	// assumption: m_pMaxFunc and m_pMinFunc have matched time setup
	VveTimeMapper *pTimeMapper = m_pMaxFunc->GetTimeMapper();   
	int iNumOfTIs = pTimeMapper->GetNumberOfTimeIndices();

	//m_fXValueMin = 0;			
	//m_fXValueMax = iNumOfTIs-1;
	// compute x_step
	//float fXstep = (m_fPlotCanvasXmax - m_fPlotCanvasXmin) / (iNumOfTIs-1);
	float fXValRange = m_fXValueMax - m_fXValueMin;
	float fXCanvasRange = m_fPlotCanvasXmax - m_fPlotCanvasXmin;
	float fDeltaX = fXCanvasRange /	fXValRange;

	// get both function's min/max and compute y-step
	float fMinFuncValue = m_fYValueMin;
	float fMaxFuncValue = m_fYValueMax;
	float fDeltaFuncValue = fMaxFuncValue - fMinFuncValue;
	float fDeltaY = m_fPlotCanvasYmax - m_fPlotCanvasYmin;	
	float fYstep = fDeltaY/fDeltaFuncValue;

	// generate plot points for m_vecMaxPlotPoints and m_vecMinPlotPoints
	float fX = 0.0f, fY = 0.0f, fZ = m_fDepth;
	float fCurrentValue = 0.0f;
	// init
	m_vecMaxPlotPoints.clear();
	m_vecMinPlotPoints.clear();
	int iLevelIdx = 0;
	double dVisTime;
	// iterate for all discrete points in min and max func
	for(int i=0; i<iNumOfTIs; ++i)
	{
		iLevelIdx = pTimeMapper->GetLevelIndex(i);
		dVisTime = pTimeMapper->GetVisualizationTime(i);

		//draw only the stuff in range
		if(dVisTime < m_fXValueMin || dVisTime > m_fXValueMax)
			continue;
		
		//map vis time to canvas x coord
		dVisTime -= m_fXValueMin;
		fX = static_cast<float>(dVisTime) * fDeltaX + m_fPlotCanvasXmin;
		
		// max plot point
		fCurrentValue = m_pMaxFunc->GetValueForLevelIdx(iLevelIdx);
		fY = (fCurrentValue-fMinFuncValue) * fYstep + m_fPlotCanvasYmin;
		//clamp to canvas
		fY = std::min<float>(m_fPlotCanvasYmax, fY);
		fY = std::max<float>(m_fPlotCanvasYmin, fY);
		m_vecMaxPlotPoints.push_back(VistaVector3D(fX,fY,fZ));

		// min plot point
		fCurrentValue = m_pMinFunc->GetValueForLevelIdx(iLevelIdx);
		fY = (fCurrentValue-fMinFuncValue) * fYstep + m_fPlotCanvasYmin;
		//clamp to canvas
		fY = std::min<float>(m_fPlotCanvasYmax, fY);
		fY = std::max<float>(m_fPlotCanvasYmin, fY);
		m_vecMinPlotPoints.push_back(VistaVector3D(fX,fY,fZ));
	}

	// done, no update need until the observed VveTimeSeries changes
	m_bNeedUpdate = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflStaticRangePlot::Update()
{
	//vstr::outi() << "Ta da !" << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflStaticRangePlot::DrawTransparent()
{
	const size_t nNumOfPlotPoints = m_vecMaxPlotPoints.size();
	if(m_vecMinPlotPoints.size() != nNumOfPlotPoints)
	{
		vstr::errp() << "[VflStaticRangePlot] DrawTransparent: invalid plot point set" << endl;
		return;
	}

	if(nNumOfPlotPoints < 2)
		return;

	float fPos[3] = {0.0f, 0.0f, 0.0f};
	VflStaticRangePlotProperties *pProps = dynamic_cast<VflStaticRangePlotProperties*>
		(this->GetProperties());

	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_LIGHTING);

	float fR, fG, fB, fOpacity;
	pProps->GetColor(fR, fG, fB);
	fOpacity = pProps->GetOpacity();
	glColor4f(fR, fG, fB, fOpacity);

	glBegin(GL_QUAD_STRIP);
		for(size_t i=0; i<nNumOfPlotPoints;++i)
		{
			m_vecMaxPlotPoints[i].GetValues(fPos);
			glVertex3f(fPos[0], fPos[1], fPos[2]);
			m_vecMinPlotPoints[i].GetValues(fPos);
			glVertex3f(fPos[0], fPos[1], fPos[2]);
		}
	glEnd();

	glEnable(GL_ALPHA_TEST);
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflStaticRangePlot::GetRegistrationMode() const
{
	//return OLI_UPDATE;
	return OLI_DRAW_TRANSPARENT;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VflStaticRangePlot::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	minBounds = m_v3BoundsMin;
	maxBounds = m_v3BoundsMax;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflStaticRangePlot::GetType() const
{
	return IVflVisObject::GetType()+string("::VflStaticRangePlot");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflStaticRangePlot::Debug(std::ostream & out) const
{
	IVflVisObject::Debug(out);

	if (m_bBoundsValid)
		out << m_v3BoundsMin << " - " << m_v3BoundsMax << endl;
	else
		out << "*invalid*" << endl;
}


void VflStaticRangePlot::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	switch(ticket)
	{
	case IVflVisObject::E_UNSTEADYDATA_TICKET:
		m_bNeedUpdate = true;
		break;
	default:
		break;
	}

	//this->ComputeMinMaxYValues();
	this->UpdatePlotPoints();
	//VflVisObject::ObserverUpdate(pObserveable, msg, ticket);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
void VflStaticRangePlot::SetPlotCanvasCoordinates(float fXmin, float fXmax, 
													 float fYmin, float fYmax)
{
	m_fPlotCanvasXmin = fXmin;
	m_fPlotCanvasXmax = fXmax;
	m_fPlotCanvasYmin = fYmin;
	m_fPlotCanvasYmax = fYmax;
	m_bNeedUpdate = true;
//	ComputeMinMaxYValues();
	UpdatePlotPoints();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
void VflStaticRangePlot::GetPlotCanvasCoordinates(float &fXmin, float &fXmax, 
													 float &fYmin, float &fYmax)
{
	fXmin = m_fPlotCanvasXmin;
	fXmax = m_fPlotCanvasXmax;
	fYmin = m_fPlotCanvasYmin;
	fYmax = m_fPlotCanvasYmax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
bool VflStaticRangePlot::ComputeBounds()
{
	bool bBoundsValid = false;

	// todo: compute bounds

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  ComputeMinMaxYValues                                         */
/*                                                                            */
/*============================================================================*/
void VflStaticRangePlot::ComputeMinMaxYValues()
{
	// assumption: m_pMaxFunc and m_pMinFunc have matched time setup
	VveTimeMapper *pTimeMapper = m_pMaxFunc->GetTimeMapper();
	int iNumOfTIs = pTimeMapper->GetNumberOfTimeIndices();

	float fMin=0.0f, fMax=0.0f;
	float fCurrentMinFuncVal=0.0f, fCurrentMaxFuncVal=0.0f; 
	fMin = m_pMinFunc->GetValueForLevelIdx(0);
	fMax = m_pMaxFunc->GetValueForLevelIdx(0);
	int iLevelIdx = 0;
	for(int i=1; i<iNumOfTIs; ++i)
	{
		iLevelIdx = pTimeMapper->GetLevelIndex(i);
		fCurrentMinFuncVal = m_pMinFunc->GetValueForLevelIdx(iLevelIdx);
		if(fCurrentMinFuncVal < fMin)
			fMin = fCurrentMinFuncVal;
		fCurrentMaxFuncVal = m_pMaxFunc->GetValueForLevelIdx(iLevelIdx);
		if(fCurrentMaxFuncVal > fMax)
			fMax = fCurrentMaxFuncVal;
	}

	m_fYValueMin = fMin;
	m_fYValueMax = fMax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  SetValueRanges                                               */
/*                                                                            */
/*============================================================================*/
void VflStaticRangePlot::SetValueRanges(float fXValueMin, float fXValueMax, 
										   float fYValueMin, float fYValueMax)
{
	m_fXValueMin = fXValueMin;
	m_fXValueMax = fXValueMax;
	m_fYValueMin = fYValueMin;
	m_fYValueMax = fYValueMax;
	m_bNeedUpdate = true;
	this->UpdatePlotPoints();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetValueRanges                                               */
/*                                                                            */
/*============================================================================*/
void VflStaticRangePlot::GetValueRanges(float &fXValueMin, float &fXValueMax, 
										   float &fYValueMin, float &fYValueMax) const
{
	fXValueMin = m_fXValueMin;
	fXValueMax = m_fXValueMax;
	fYValueMin = m_fYValueMin;
	fYValueMax = m_fYValueMax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  SetDepth                                                     */
/*                                                                            */
/*============================================================================*/
void VflStaticRangePlot::SetDepth(float fZ)
{
	m_fDepth = fZ;
	m_bNeedUpdate = true;
	UpdatePlotPoints();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetDepth                                                     */
/*                                                                            */
/*============================================================================*/
float VflStaticRangePlot::GetDepth() const
{
	return m_fDepth;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetProperties                                                */
/*                                                                            */
/*============================================================================*/
VflStaticRangePlot::VflStaticRangePlotProperties*
VflStaticRangePlot::GetProperties() const
{
	return static_cast<VflStaticRangePlotProperties*>(
								IVflVisObject::GetProperties());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetDepth                                                     */
/*                                                                            */
/*============================================================================*/
IVflVisObject::VflVisObjProperties* VflStaticRangePlot::CreateProperties() const
{
	return new VflStaticRangePlotProperties;
}

// ############################################################################


static const string SsReflectionType("VflStaticRangePlot");

//static IVistaPropertyGetFunctor *aCgFunctors[] =
//{
//};

//static IVistaPropertySetFunctor *aCsFunctors[] =
//{
//};

VflStaticRangePlot::VflStaticRangePlotProperties::VflStaticRangePlotProperties()
: IVflVisObject::VflVisObjProperties(),
m_fColorR(1.0f), m_fColorG(1.0f), m_fColorB(1.0f),
m_fOpacity(0.7f)
{
}
VflStaticRangePlot::VflStaticRangePlotProperties::~VflStaticRangePlotProperties()
{
}
void VflStaticRangePlot::VflStaticRangePlotProperties::SetColor(float fR, float fG, float fB)
{
	m_fColorR = fR;
	m_fColorG = fG;
	m_fColorB = fB;
}
void VflStaticRangePlot::VflStaticRangePlotProperties::GetColor(float &fR, float &fG, float &fB) const
{
	fR = m_fColorR;
	fG = m_fColorG;
	fB = m_fColorB;
}
void VflStaticRangePlot::VflStaticRangePlotProperties::SetOpacity(float fOpacity)
{
	m_fOpacity = fOpacity;
}
float VflStaticRangePlot::VflStaticRangePlotProperties::GetOpacity() const
{
	return m_fOpacity;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflStatic1DFuncPlot.cpp"                                     */
/*============================================================================*/
