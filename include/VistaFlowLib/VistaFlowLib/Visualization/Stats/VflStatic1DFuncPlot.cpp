/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <GL/glew.h>

#include "VflStatic1DFuncPlot.h"
#include "VflStaticPlotRendering.h"

#include <VistaVisExt/Data/VveTimeSeries.h>

#include <cassert>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflStatic1DFuncPlot::VflStatic1DFuncPlot()
	:	m_dLastDrawSimTime(-1.0),
		m_dLastDrawTransSimTime(-1.0),
		m_bNeedUpdate(true),
		m_pData(NULL),
		m_fPlotCanvasXmin(-0.8f), m_fPlotCanvasXmax(0.8f),
		m_fPlotCanvasYmin(-0.8f), m_fPlotCanvasYmax(0.8f),
		m_fDepth(0.0f),
		m_fXValueMin(0.0f),
		m_fXValueMax(1.0f),
		m_fYValueMin(0.0f),
		m_fYValueMax(0.0f),
		m_pPlotRendering(NULL)
{
	m_v3BoundsMin = VistaVector3D(0, 0, 0);
	m_v3BoundsMax = VistaVector3D(0, 0, 0);
}

VflStatic1DFuncPlot::VflStatic1DFuncPlot(VflStaticPlotRendering *pPlotRendering)
	:	m_dLastDrawSimTime(-1.0),
		m_dLastDrawTransSimTime(-1.0),
		m_bNeedUpdate(true),
		m_pData(NULL),
		m_fPlotCanvasXmin(-0.8f), m_fPlotCanvasXmax(0.8f),
		m_fPlotCanvasYmin(-0.8f), m_fPlotCanvasYmax(0.8f),
		m_fDepth(0.0f),
		m_fXValueMin(0.0f),
		m_fXValueMax(1.0f),
		m_fYValueMin(0.0f),
		m_fYValueMax(0.0f),
		m_pPlotRendering(pPlotRendering)
{
	m_v3BoundsMin = VistaVector3D(0, 0, 0);
	m_v3BoundsMax = VistaVector3D(0, 0, 0);
}

VflStatic1DFuncPlot::~VflStatic1DFuncPlot()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflStatic1DFuncPlot::Init()
{
	if(!IVflVisObject::Init())
		return false;
	
	if(!m_pData)
	{
		vstr::errp() << "[CStatic2DPlotRenderer] VveTimeSeries is undefined" << endl;
		return false;
	}
		
	UpdatePlotPoints();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetData                                                     */
/*                                                                            */
/*============================================================================*/
bool VflStatic1DFuncPlot::SetData(VveTimeSeries *pData)
{
	if(pData != m_pData)
	{
		if(m_pData)
			ReleaseObserveable(m_pData, E_UNSTEADYDATA_TICKET);

		m_pData = pData;

		if(m_pData)
			Observe(m_pData, E_UNSTEADYDATA_TICKET);

		m_bNeedUpdate = true;
		UpdatePlotPoints();
		return true;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData                                                     */
/*                                                                            */
/*============================================================================*/
VveTimeSeries* VflStatic1DFuncPlot::GetData() const
{
	return m_pData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetUnsteadyData                                             */
/*                                                                            */
/*============================================================================*/
bool VflStatic1DFuncPlot::SetUnsteadyData(VveUnsteadyData *pData)
{
	VveTimeSeries *pTSData = dynamic_cast<VveTimeSeries*>(pData);

	if(!pTSData)
		return false;

	bool b = SetData(pTSData);
	return b;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdatePlotPoints                                            */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::UpdatePlotPoints()
{
	if(!m_bNeedUpdate)
		return;

	VveTimeMapper *pTimeMapper = m_pData->GetTimeMapper();
	int iNumOfTIs = pTimeMapper->GetNumberOfTimeIndices();
	
	//x axis is scaled according to vis time
	//m_fXValueMin = 0.9;	
	//m_fXValueMax = 1.0f;

	float fXValRange = m_fXValueMax - m_fXValueMin;
	float fXCanvasRange = m_fPlotCanvasXmax - m_fPlotCanvasXmin;

	float fDeltaX = fXCanvasRange /	fXValRange;
	// get min/max in f(t) -> get delta function value 
	//// -> get "scale" of function value for its plot point's y-coord -> "y_step"
	float fCurrentFuncValue = 0.0f;

	if(m_pPlotRendering)
		m_pPlotRendering->GetGlobalFuncMinMax(m_fYValueMin, m_fYValueMax);

	float fDeltaFuncValue = m_fYValueMax - m_fYValueMin;
	float fDeltaY = m_fPlotCanvasYmax - m_fPlotCanvasYmin;	
	float fYstep = fDeltaY/fDeltaFuncValue;

	// generate plot points on x,y-plane
	float fX, fY = 0.0f, fZ = m_fDepth;
	double dVisTime;

	// init iteration
	m_vecPlotPoints.clear();
	//start drawing at xOffset
	int iLevelIdx = 0;
	// iterate for all included discrete points in VveTimeSeries
	for(int i=0; i<iNumOfTIs; ++i)
	{
		iLevelIdx = pTimeMapper->GetLevelIndex(i);
		dVisTime = pTimeMapper->GetVisualizationTime(i);

		//draw only the stuff in range
		if(dVisTime < m_fXValueMin || dVisTime > m_fXValueMax)
			continue;
		
		//map vis time to canvas x coord
		dVisTime -= m_fXValueMin;
		fX = static_cast<float>(dVisTime) * fDeltaX + m_fPlotCanvasXmin;

		fCurrentFuncValue = m_pData->GetValueForLevelIdx(iLevelIdx);
		fY = (fCurrentFuncValue-m_fYValueMin)* fYstep + m_fPlotCanvasYmin;
		
		//clamp to canvas
		fY = std::min<float>(m_fPlotCanvasYmax, fY);
		fY = std::max<float>(m_fPlotCanvasYmin, fY);
		
		m_vecPlotPoints.push_back(VistaVector3D(fX,fY,fZ));  // output
	}

	// done, no update need until the obeserved VveTimeSeries changes
	m_bNeedUpdate = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::Update()
{
	//vstr::outi() << "Ta da !" << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::DrawOpaque()
{
	const size_t nNumOfPlotPoints = m_vecPlotPoints.size();
	if(nNumOfPlotPoints<2)
		return;

	glPushAttrib(GL_ENABLE_BIT|GL_LINE_BIT);
	glDisable(GL_LIGHTING);

	float fPos[3] = {0.0f, 0.0f, 0.0f};
	VflStatic1DFuncPlotProperties *pProps = 
		dynamic_cast<VflStatic1DFuncPlotProperties*>(this->GetProperties());

	glLineWidth(pProps->GetPlotLineWidth());  
	float fR, fG, fB;
	pProps->GetColor(fR,fG,fB);
	glColor3f(fR, fG, fB); 
	glBegin(GL_LINE_STRIP);
		for(size_t i=0; i<nNumOfPlotPoints;++i)
		{
			m_vecPlotPoints[i].GetValues(fPos);
			glVertex3f(fPos[0], fPos[1], fPos[2]);
		}
	glEnd();

	if(pProps->GetDrawPlotPoints())
	{
		// draw "X" on the plot points position fPos
		//       p0    p3
		//        \   /          -
		//         \ /           | fD  
		//          X    fPos    -
		//         / \
		//        /   \
		//       p2    p1
		//  
		float fD = pProps->GetPlotPointSize();
		glColor3f(fR,fG,fB);
		glBegin(GL_LINES);
		for(size_t i=0; i<nNumOfPlotPoints;++i)
		{
			m_vecPlotPoints[i].GetValues(fPos);
			glVertex3f( fPos[0]-fD, fPos[1]+fD, fPos[2] );   // p0
			glVertex3f( fPos[0]+fD, fPos[1]-fD, fPos[2] );   // p1
			glVertex3f( fPos[0]-fD, fPos[1]-fD, fPos[2] );   // p2
			glVertex3f( fPos[0]+fD, fPos[1]+fD, fPos[2] );   // p3
		}
		glEnd();
	}
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflStatic1DFuncPlot::GetRegistrationMode() const
{
	//return OLI_UPDATE | OLI_DRAW_OPAQUE;
	return OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VflStatic1DFuncPlot::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	minBounds = m_v3BoundsMin;
	maxBounds = m_v3BoundsMax;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflVisObject::VflVisObjProperties* VflStatic1DFuncPlot::CreateProperties() const
{
	return new VflStatic1DFuncPlotProperties;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflStatic1DFuncPlot::GetType() const
{
	return IVflVisObject::GetType()+string("::VflStatic1DFuncPlot");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::Debug(std::ostream & out) const
{
	IVflVisObject::Debug(out);

	if (m_bBoundsValid)
		out << m_v3BoundsMin << " - " << m_v3BoundsMax << endl;
	else
		out << "*invalid*" << endl;
}


void VflStatic1DFuncPlot::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	switch(ticket)
	{
	case IVflVisObject::E_UNSTEADYDATA_TICKET:
		m_bNeedUpdate = true;
		break;
	default:
		break;
	}

	//this->ComputeMinMaxYValues();

	/*
	if(m_pPlotRendering)
	{
		float fFuncGlobalMin = 0.0f, fFuncGlobalMax = 0.0f;
		m_pPlotRendering->GetGlobalFuncMinMax(fFuncGlobalMin, fFuncGlobalMax);

		// if current global min/max is affected by new min/max values of this function 
		if(m_fYValueMin<fFuncGlobalMin || m_fYValueMax>fFuncGlobalMax)
			this->Notify(VflStatic1DFuncPlot::MSG_MINMAXCHANGE);

		// adjust the y-scale for this function's plot canvas
		//this->SetPlotCanvasCoordinates();
		VistaVector3D v3PlotMin, v3PlotMax;
		m_pPlotRendering->GetPlotDrawRegion(v3PlotMin, v3PlotMax);

		float fCanvasYmin=0.0f, fCanvasYmax=0.0f;
		float fGlobalCanvasYmin = v3PlotMin.GetVal(1);
		float fGlobalCanvasYmax = v3PlotMax.GetVal(1);
		float fScaleFactor  = (fGlobalCanvasYmax-fGlobalCanvasYmin)
							/ (fFuncGlobalMax-fFuncGlobalMin);
		fCanvasYmin = (m_fYValueMin-fFuncGlobalMin)*fScaleFactor + fGlobalCanvasYmin;
		fCanvasYmax = (m_fYValueMax-fFuncGlobalMin)*fScaleFactor + fGlobalCanvasYmin;
		this->SetPlotCanvasCoordinates(m_fPlotCanvasXmin, m_fPlotCanvasXmax, fCanvasYmin, fCanvasYmax);
	}
	*/
	m_bNeedUpdate = true;
	this->UpdatePlotPoints();
	//VflVisObject::ObserverUpdate(pObserveable, msg, ticket);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::SetPlotCanvasCoordinates(float fXmin, float fXmax, 
													 float fYmin, float fYmax)
{
	m_fPlotCanvasXmin = fXmin;
	m_fPlotCanvasXmax = fXmax;
	m_fPlotCanvasYmin = fYmin;
	m_fPlotCanvasYmax = fYmax;
	m_bNeedUpdate = true;
	UpdatePlotPoints();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::GetPlotCanvasCoordinates(float &fXmin, float &fXmax, 
													 float &fYmin, float &fYmax)
{
	fXmin = m_fPlotCanvasXmin;
	fXmax = m_fPlotCanvasXmax;
	fYmin = m_fPlotCanvasYmin;
	fYmax = m_fPlotCanvasYmax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties* 
VflStatic1DFuncPlot::GetProperties() const
{
	return static_cast<VflStatic1DFuncPlotProperties*>(
		IVflVisObject::GetProperties());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
bool VflStatic1DFuncPlot::ComputeBounds()
{
	bool bBoundsValid = false;

	// todo: compute bounds

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  ComputeMinMaxYValues                                         */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::ComputeMinMaxYValues()
{
	VveTimeMapper *pTimeMapper = m_pData->GetTimeMapper();
	int iNumOfTIs = pTimeMapper->GetNumberOfTimeIndices();
	int iLevelIdx = 0;

	// get min/max in f(t), i.e. values for the y-axes
	float fMinFuncValue = 0.0f, fMaxFuncValue = 0.0f;
	float fCurrentFuncValue = 0.0f;

	fMinFuncValue = m_pData->GetValueForLevelIdx(0);
	fMaxFuncValue = m_pData->GetValueForLevelIdx(0);	
	for(int i=1; i<iNumOfTIs; ++i)
	{
		iLevelIdx = pTimeMapper->GetLevelIndex(i);
		fCurrentFuncValue = m_pData->GetValueForLevelIdx(iLevelIdx);
		if(fMinFuncValue > fCurrentFuncValue)
			fMinFuncValue = fCurrentFuncValue;
		if(fMaxFuncValue < fCurrentFuncValue)
			fMaxFuncValue = fCurrentFuncValue;
	}
	m_fYValueMin = fMinFuncValue;    // save down into this class's attributes
	m_fYValueMax = fMaxFuncValue;    // save down into this class's attributes
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  SetValueRanges                                               */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::SetValueRanges(float fXValueMin, float fXValueMax, 
										   float fYValueMin, float fYValueMax)
{
	m_fXValueMin = fXValueMin;
	m_fXValueMax = fXValueMax;
	m_fYValueMin = fYValueMin;
	m_fYValueMax = fYValueMax;
	m_bNeedUpdate = true;
	this->UpdatePlotPoints();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetValueRanges                                               */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::GetValueRanges(float &fXValueMin, float &fXValueMax, 
										   float &fYValueMin, float &fYValueMax) const
{
	fXValueMin = m_fXValueMin;
	fXValueMax = m_fXValueMax;
	fYValueMin = m_fYValueMin;
	fYValueMax = m_fYValueMax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  SetDepth                                                     */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::SetDepth(float fZ)
{
	m_fDepth = fZ;
	m_bNeedUpdate = true;
	UpdatePlotPoints();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetDepth                                                     */
/*                                                                            */
/*============================================================================*/
float VflStatic1DFuncPlot::GetDepth() const
{
	return m_fDepth;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetPlotPoints                                                */
/*                                                                            */
/*============================================================================*/
std::vector<VistaVector3D> *VflStatic1DFuncPlot::GetPlotPoints()
{
	return &m_vecPlotPoints;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  SetPlotRendering                                             */
/*                                                                            */
/*============================================================================*/
void VflStatic1DFuncPlot::SetPlotRendering(VflStaticPlotRendering* pRend)
{
	m_pPlotRendering = pRend;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  getPlotRendering                                             */
/*                                                                            */
/*============================================================================*/
VflStaticPlotRendering* VflStatic1DFuncPlot::GetPlotRendering()
{
	return m_pPlotRendering;
}

// ############################################################################


static const string SsReflectionType("VflStatic1DFuncPlot");

//static IVistaPropertyGetFunctor *aCgFunctors[] =
//{
//};

//static IVistaPropertySetFunctor *aCsFunctors[] =
//{
//};

VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties::VflStatic1DFuncPlotProperties()
: IVflVisObject::VflVisObjProperties(),
m_fColorR(1.0f), m_fColorG(1.0f), m_fColorB(1.0f),
m_fPlotLineWidth(1.0f),
m_bDrawPlotPoints(true), m_fPlotPointSize(0.02f)
{
}
VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties::~VflStatic1DFuncPlotProperties()
{
}
void VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties::SetColor(float fR, float fG, float fB)
{
	m_fColorR = fR;
	m_fColorG = fG;
	m_fColorB = fB;
}
void VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties::GetColor(float &fR, float &fG, float &fB) const
{
	fR = m_fColorR;
	fG = m_fColorG;
	fB = m_fColorB;
}
void VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties::SetPlotLineWidth(float fWidth)
{
	m_fPlotLineWidth = fWidth;
}
float VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties::GetPlotLineWidth() const
{
	return m_fPlotLineWidth;
}
void VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties::SetDrawPlotPoints(bool b)
{
	m_bDrawPlotPoints = b;
}
bool VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties::GetDrawPlotPoints() const
{
	return m_bDrawPlotPoints;
}
void VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties::SetPlotPointSize(float fSize)
{
	m_fPlotPointSize = fSize;
}
float VflStatic1DFuncPlot::VflStatic1DFuncPlotProperties::GetPlotPointSize() const
{
	return m_fPlotPointSize;
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflStatic1DFuncPlot.cpp"                                     */
/*============================================================================*/
