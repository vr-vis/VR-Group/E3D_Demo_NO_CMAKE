

set( RelativeDir "./Visualization/Stats" )
set( RelativeSourceGroup "Source Files\\Visualization\\Stats" )

set( DirFiles
	VflStatic1DFuncPlot.cpp
	VflStatic1DFuncPlot.h
	VflStatic2DHistogramPlot.cpp
	VflStatic2DHistogramPlot.h
	VflStaticPlotRendering.cpp
	VflStaticPlotRendering.h
	VflStaticRangePlot.cpp
	VflStaticRangePlot.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

