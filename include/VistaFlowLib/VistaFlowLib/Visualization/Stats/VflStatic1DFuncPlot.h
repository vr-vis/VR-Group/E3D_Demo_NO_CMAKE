/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLSTATIC1DFUNCPLOT_H
#define _VFLSTATIC1DFUNCPLOT_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflVisObject.h>

#include <VistaBase/VistaVectorMath.h>

#include <iostream>
#include <string>
#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTimeSeries;
class VflStaticPlotRendering;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAPI VflStatic1DFuncPlot : public IVflVisObject, public IVistaObserveable
{
public:

    // CONSTRUCTORS / DESTRUCTOR
    VflStatic1DFuncPlot();
	VflStatic1DFuncPlot(VflStaticPlotRendering *pPlotRendering);
    virtual ~VflStatic1DFuncPlot();
  
    virtual bool Init();
	virtual void Update();
    virtual void DrawOpaque(); 

	void UpdatePlotPoints();

    /**
     * Returns the boundaries of the visualization object.
	 * Note: This method is not declared const, as it might change the
	 *       cached boundaries inside the object.
     * 
     * @param[out]  VistaVector3D minBounds    minimum of the object's AABB
	 * @param[out]  VistaVector3D maxBounds    maximum of the object's AABB
     * @return  bool	is the bounding box valid?
     */    
	virtual bool GetBounds(VistaVector3D &minBounds, 
		                   VistaVector3D &maxBounds);

    /**
     * The callback method from the IVistaObserver class, which is to be
	 * used for notifications concerning data changes.
     */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	virtual unsigned int GetRegistrationMode() const;
	virtual std::string GetType() const;
    virtual void Debug(std::ostream & out) const;

	/**
     * The coordinates intervall for the plot canvas
     */    
	void SetPlotCanvasCoordinates(float fXmin, float fXmax, float fYmin, float fYmax);
	void GetPlotCanvasCoordinates(float &fXmin, float &fXmax, float &fYmin, float &fYmax);

	/**
     * Set the unsteady data object to be visualized. Note, that this is only
	 * possible as long as Init() has not been called!
     * 
     * @param[in]   VveTimeSeries *pData  the data to be visualized
     * @return  bool                                  Operation successful?
     */    
	bool SetData(VveTimeSeries *pData);
	VveTimeSeries* GetData() const;

	/**
	 * Overwrite VflVisObject's method.
	 */
	bool SetUnsteadyData(VveUnsteadyData *pData);

	/**
	 * set the data ranges manually for consistency
	 */
	void SetValueRanges(float fXValueMin, float fXValueMax, 
						float fYValueMin, float fYValueMax);

	/**
	 * get the data ranges
	 */
	void GetValueRanges(float &fXValueMin, float &fXValueMax, 
						float &fYValueMin, float &fYValueMax) const;

	void SetDepth(float fZ);
	float GetDepth() const;

	/**
	 * Get plot points
	 * This function is helpful, e.g. if you want to draw the plot in other kind of view
	 * but you don't want to compute the positions all over again
	 */
	std::vector<VistaVector3D> *GetPlotPoints();

	class VISTAFLOWLIBAPI VflStatic1DFuncPlotProperties : public VflVisObjProperties
	{
		friend class VflStatic1DFuncPlot;
	public:
		VflStatic1DFuncPlotProperties();
		virtual ~VflStatic1DFuncPlotProperties();

		void SetColor(float fR, float fG, float fB);
		void GetColor(float &fR, float &fG, float &fB) const;

		void SetPlotLineWidth(float fWidth);
		float GetPlotLineWidth() const;

		void SetDrawPlotPoints(bool b);
		bool GetDrawPlotPoints() const;

		void SetPlotPointSize(float fSize);
		float GetPlotPointSize() const;

	private:
		float	m_fColorR,
				m_fColorG,
				m_fColorB;
		float	m_fPlotLineWidth;
		bool	m_bDrawPlotPoints;   // draw "X"
		float	m_fPlotPointSize;
	};

	virtual VflStatic1DFuncPlotProperties* GetProperties() const;

	VflStaticPlotRendering *GetPlotRendering();
	void SetPlotRendering(VflStaticPlotRendering* pRend);

	enum
	{
		MSG_MINMAXCHANGE = IVistaObserveable::MSG_LAST,
		MSG_LAST
	};

private:
    /**
     * Compute (or update) the object's boundaries.
     */    
	virtual bool ComputeBounds();

	/** 
	 * Compute minimal and maximal of values for the y axes, i.e. the function min/max-values
	 * of the given VveTimeSeries
	 * Output --> m_fYValueMin, m_fYValueMax
	 */
	void ComputeMinMaxYValues();

	virtual VflVisObjProperties *CreateProperties() const;

	// member attributes

	VveTimeSeries	*m_pData;

	std::vector<VistaVector3D> m_vecPlotPoints;
	bool m_bNeedUpdate;

	float	m_fPlotCanvasXmin, 
			m_fPlotCanvasXmax,
			m_fPlotCanvasYmin,
			m_fPlotCanvasYmax;

	bool m_bBoundsValid; 

	double      m_dLastDrawSimTime,
		        m_dLastDrawTransSimTime;
	
	VistaVector3D m_v3BoundsMin, 
		           m_v3BoundsMax;
	float		m_fDepth;    // the z-coordinates

	float	m_fXValueMin, 
			m_fXValueMax,
			m_fYValueMin,
			m_fYValueMax;

	VflStaticPlotRendering		*m_pPlotRendering;
};

/*============================================================================*/
/* INLINE METHODS                                                             */
/*============================================================================*/



/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLSTATIC1DFUNCPLOT_H)
