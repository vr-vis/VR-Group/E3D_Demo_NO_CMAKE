/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <GL/glew.h>

#include "VflStaticPlotRendering.h"
#include "VflStatic1DFuncPlot.h"
#include "VflStaticRangePlot.h"
#include "VflStatic2DHistogramPlot.h"

#include <VistaFlowLib/Tools/VflLegendInfo.h>
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>
#include <VistaFlowLib/Tools/VflTimeLine.h>
#include <VistaFlowLib/Tools/VflRegGridBackground.h>
#include <VistaVisExt/Data/VveTimeSeries.h>

#include <cassert>
#include <vector>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

bool sort_prio(VflStaticPlotRendering::sRenderOrder sFirst,
			   VflStaticPlotRendering::sRenderOrder sSecond)
{
	if(sFirst.fOrderPrio < sSecond.fOrderPrio)
		return true;
	else 
		return false;
}

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflStaticPlotRendering::VflStaticPlotRendering(
	VflRenderNode *pRenderNode, 
	int iWidth, int iHeight,
	float fLeft, float fRight, 
	float fBottom, float fTop,
	float fNear, float fFar)
: IVflRenderOrthoProjToTexture(pRenderNode, iWidth, iHeight,
							   fLeft, fRight,
							   fBottom, fTop,
							   fNear, fFar),
  m_fGlobalFuncMin(0.0f), m_fGlobalFuncMax(0.0f),
  m_fZcoordBack(fNear), m_fZcoordFront(fFar)
{
	m_oTimeSetup.iNumOfTimeLevels= -1;
	m_oTimeSetup.iNumOfTimeIndices  = -1;
	
	// Plot's composition (depends on the given ortho projection)
	// assumption: 7% left/right/bottom margin of draw region (relative to the whole texture)
	//			   10% top margin to reserve place for plot title
	float fDrawLeft		= fLeft + 0.07f * (fRight-fLeft);
	float fDrawRight	= fRight - 0.07f * (fRight-fLeft);  
	float fDrawBottom	= fBottom + 0.07f * (fTop-fBottom);
	float fDrawTop		= fTop - 0.1f * (fTop-fBottom);   //fTop - 0.07f * (fTop-fBottom);
	m_v3PlotMin = VistaVector3D(	fDrawLeft+0.03f*(fRight-fLeft),     // 3% of width for the legend
									fDrawBottom+0.03f*(fTop-fBottom),   // 3% of height for the legend
									fFar );
	m_v3PlotMax = VistaVector3D(	fDrawRight, fDrawTop, fNear );

	//------------------------------------ Plot's Legend ------------------------------------
	m_pPlotLegend = new VflLegendInfo(pRenderNode);
	m_pPlotLegend->Init();
	// some standard settings for pot legend
	m_pPlotLegend->SetTargetBoundingBox(m_v3PlotMin, m_v3PlotMax);
	VflLegendInfo::VflLegendInfoProperties *pLegendProps = 
		dynamic_cast<VflLegendInfo::VflLegendInfoProperties*>(m_pPlotLegend->GetProperties());
	pLegendProps->SetLineWidth(2.0f);
	pLegendProps->SetLegendColor(1.0f, 1.0f, 1.0f);
	pLegendProps->SetDrawLegendAxesAtRangeZero(false);
	// label position
	float fXLabelOffset=-0.02f, fYLabelOffset=0.0f, fZLabelOffset=0.0f;
	float fXMinMaxOffset=-0.02f, fYMinMaxOffset=-0.09f, fZMinMaxOffset=0.0f;
	pLegendProps->SetLabelOffset(fXLabelOffset, fYLabelOffset, fZLabelOffset);
	pLegendProps->SetMinMaxOffset(fXMinMaxOffset, fYMinMaxOffset, fZMinMaxOffset);
	pLegendProps->SetLegendText(0, "time");
	pLegendProps->SetLegendText(1, "labelY");
	pLegendProps->SetLegendText(2, "labelZ");
	pLegendProps->SetLabelAlignment(0, VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_AUTO);
	pLegendProps->SetLabelAlignment(1, VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_AUTO);
	pLegendProps->SetLabelAlignment(2, VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_AUTO);
	pLegendProps->SetMinMaxAlignment(0, VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_INSIDE);
	pLegendProps->SetMinMaxAlignment(1, VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_INSIDE);
	float fSize = 0.0f;
	fSize = pLegendProps->GetLegendTextSize();
	pLegendProps->SetLegendTextSize(0.5f*fSize);
	pLegendProps->SetOverrideBounds(true);

	RegisterTarget(m_pPlotLegend);
	//---------------------------------------------------------------------------------------

	//------------------------------------ Plot's Title ------------------------------------
	m_pPlotTitle = new Vfl3DTextLabel;
	m_pPlotTitle->SetRenderNode(pRenderNode);
	m_pPlotTitle->Init();
	m_pPlotTitle->SetText("");
	m_pPlotTitle->GetProperties()->SetVisible(true);
	m_pPlotTitle->SetOrientation(VistaQuaternion());
	m_pPlotTitle->SetTextSize(0.9f*fSize);
	m_pPlotTitle->SetTextFollowViewDir(false);
	VistaVector3D v3Pos;
	m_pPlotTitle->GetPosition(v3Pos);
	v3Pos[0] = fLeft + 0.5f*(fRight-fLeft);
	v3Pos[1] = fBottom + 0.95f*(fTop-fBottom);
	m_pPlotTitle->SetPosition(v3Pos);

	RegisterTarget(m_pPlotTitle);
	//---------------------------------------------------------------------------------------

	//------------------------------------- Time line ---------------------------------------
	m_bDrawTimeLine = true;
	m_pTimeLine = new VflTimeLine;
	m_pTimeLine->Init();
	VflTimeLine::VflTimeLineProperties *pTimeLineProps =
		static_cast<VflTimeLine::VflTimeLineProperties*>(m_pTimeLine->GetProperties());
	pTimeLineProps->SetColor(1.0f, 0.0f, 0.0f);
	pTimeLineProps->SetTimeLineWidth(5.0f);
	VistaVector3D v3Start, v3End;
	float fYmiddle = 0.5f * (m_v3PlotMax[1]-m_v3PlotMin[1]) + m_v3PlotMin[1];
	v3Start = VistaVector3D(m_v3PlotMin[0], fYmiddle, m_v3PlotMin[2]);
	v3End   = VistaVector3D(m_v3PlotMax[0], fYmiddle, m_v3PlotMin[2]);
	pTimeLineProps->SetLinePoints(v3Start, v3End);
	pTimeLineProps->SetShowTimeLine(false);
	pTimeLineProps->SetTimeLineWidth(3.0f);
	pTimeLineProps->SetTimePointerSize(fabs(m_v3PlotMax[1]-m_v3PlotMin[1]));
	RegisterTarget(m_pTimeLine);

	//---------------------------------------------------------------------------------------

	//--------------------------------------- grid ------------------------------------------
	m_pGrid = new VflRegGridBackground;
	m_pGrid->Init();
	VflRegGridBackground::VflRegGridBackgroundProperties *pGridProps =
		static_cast<VflRegGridBackground::VflRegGridBackgroundProperties*>
		(m_pGrid->GetProperties());
	pGridProps->SetColor(0.7f, 0.7f, 0.7f);
	pGridProps->SetLineWidth(1.2f);
	pGridProps->SetGridWidth(fabs(m_v3PlotMax[0]-m_v3PlotMin[0]));
	pGridProps->SetGridHeight(fabs(m_v3PlotMax[1]-m_v3PlotMin[1]));
	pGridProps->SetPosition(0.5f*(m_v3PlotMax-m_v3PlotMin)+m_v3PlotMin);
	RegisterTarget(m_pGrid);

	//---------------------------------------------------------------------------------------

	m_p2DHistogramPlot = NULL;

	m_vec1DFuncPlots.clear();
	m_vecRangePlots.clear();
	m_liRenderOrder.clear();
	m_liRenderOrder.push_back(sRenderOrder(m_pPlotLegend,10.0f));
	m_liRenderOrder.push_back(sRenderOrder(m_pTimeLine, 10.0f));
	m_liRenderOrder.push_back(sRenderOrder(m_pPlotTitle, 10.0f));
	m_liRenderOrder.push_back(sRenderOrder(m_pGrid, 10.0f));
}

VflStaticPlotRendering::~VflStaticPlotRendering()
{
	// todo:
	//ReleaseObserveable(FuncPlot)

	//delete m_pPlotLegend;
	//delete m_pPlotTitle;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   InsertTimeSeriesFunction                                    */
/*                                                                            */
/*============================================================================*/
bool VflStaticPlotRendering::InsertTimeSeriesFunction(VveTimeSeries *pFunc, 
													   float fOrderPrio,
													   bool bCheckTimeSetup)
{
	VflStatic1DFuncPlot *pPlot = NULL;
	VflStaticRangePlot *pRangePlot = NULL;

	// whether pFunc match the defined time setup
	if(m_oTimeSetup.iNumOfTimeLevels <= 0 || m_oTimeSetup.iNumOfTimeIndices <= 0)
	{
		vstr::warnp() << "[VflStaticPlotRendering] invalid time setup" << endl;
		return false;
	}
	if(bCheckTimeSetup)
	{
		VveTimeMapper *pTM = pFunc->GetTimeMapper();
		if(    pTM->GetNumberOfTimeLevels()!=m_oTimeSetup.iNumOfTimeLevels 
			|| pTM->GetNumberOfTimeIndices()!=m_oTimeSetup.iNumOfTimeIndices )
		{
			vstr::warnp() << "[VflStaticPlotRendering] inconsistence time setup" << endl;
			return false;
		}
	}

	// compute shared fCanvasXmin and fCanvasXmax for the whole registered functions
	float fCanvasXmin = m_v3PlotMin[0];
	float fCanvasXmax = m_v3PlotMax[0];
	float fCanvasYmin = m_v3PlotMin[1];
	float fCanvasYmax = m_v3PlotMax[1];

	// init a new function plot for pFunc
	m_vec1DFuncPlots.push_back(new VflStatic1DFuncPlot(this));
	pPlot = m_vec1DFuncPlots.back();
	pPlot->SetData(pFunc);
	pPlot->Init();
	
	pPlot->SetPlotCanvasCoordinates(fCanvasXmin, fCanvasXmax, fCanvasYmin, fCanvasYmax);
	
	// --------------------- adjust other function plots ---------------------
	/*
	int iNumOfFuncPlots = m_vec1DFuncPlots.size();

	// get global min/max of f(t)
	
	float fFuncGlobalMin=0.0f, fFuncGlobalMax=0.0f;
	float fXValueMin=0.0f, fXValueMax=0.0f, fYValueMin=0.0f, fYValueMax=0.0f;
	// init
	pPlot = m_vec1DFuncPlots[0];
	pPlot->GetValueRanges(fXValueMin, fXValueMax, fYValueMin, fYValueMax); 
	fFuncGlobalMin = fYValueMin;
	fFuncGlobalMax = fYValueMax;
	// iteration
	for(int i=1; i<iNumOfFuncPlots; ++i)
	{
		pPlot = m_vec1DFuncPlots[i];
		pPlot->GetValueRanges(fXValueMin, fXValueMax, fYValueMin, fYValueMax); 
		if(fFuncGlobalMin > fYValueMin)
			fFuncGlobalMin = fYValueMin;
		if(fFuncGlobalMax < fYValueMax)
			fFuncGlobalMax = fYValueMax;
	}
	m_fGlobalFuncMin = fFuncGlobalMin;
	m_fGlobalFuncMax = fFuncGlobalMax;

	
	// set plot canvas coords for all time series function in the list
	float fCanvasYmin=0.0f, fCanvasYmax=0.0f;
	float fGlobalCanvasYmin = m_v3PlotMin.GetVal(1);
	float fGlobalCanvasYmax = m_v3PlotMax.GetVal(1);
	float fScaleFactor = (fGlobalCanvasYmax-fGlobalCanvasYmin)
						/(fFuncGlobalMax-fFuncGlobalMin);
	for(int i=0; i<iNumOfFuncPlots; ++i)
	{
		pPlot = m_vec1DFuncPlots[i];
		pPlot->GetValueRanges(fXValueMin, fXValueMax, fYValueMin, fYValueMax); 
		fCanvasYmin = (fYValueMin-fFuncGlobalMin)*fScaleFactor + fGlobalCanvasYmin;
		fCanvasYmax = (fYValueMax-fFuncGlobalMin)*fScaleFactor + fGlobalCanvasYmin;
		pPlot->SetPlotCanvasCoordinates(fCanvasXmin, fCanvasXmax, fCanvasYmin, fCanvasYmax);
	}
	//---------------------------------------------------------------------------

	// ----------------------- adjust other range plots -------------------------
	int iNumOfRangePlots = m_vecRangePlots.size();
	// global min/max of f(t) is in m_fGlobalFuncMin, m_fGlobalFuncMax

	// set plot canvas coords for all range plots in the list
	for(int i=0; i<iNumOfRangePlots; ++i)
	{
		pRangePlot = m_vecRangePlots[i];
		pRangePlot->GetValueRanges(fXValueMin, fXValueMax, fYValueMin, fYValueMax);
		fCanvasYmin = (fYValueMin-fFuncGlobalMin)*fScaleFactor + fGlobalCanvasYmin;
		fCanvasYmax = (fYValueMax-fFuncGlobalMin)*fScaleFactor + fGlobalCanvasYmin;
		pRangePlot->SetPlotCanvasCoordinates(fCanvasXmin, fCanvasXmax, fCanvasYmin, fCanvasYmax);
	}
	//---------------------------------------------------------------------------

	// update legend's range
	VflLegendInfo::VflLegendInfoProperties *pLegendProps = 
		dynamic_cast<VflLegendInfo::VflLegendInfoProperties*>(m_pPlotLegend->GetProperties());
	float fRanges[6] = {fXValueMin, fXValueMax, fFuncGlobalMin, fFuncGlobalMax, 0.0f, 0.0f}; 
	pLegendProps->SetOverrideBounds(true);
	pLegendProps->SetLegendRanges(fRanges);

	// observe this plot
	Observe(pPlot, VflStaticPlotRendering::E_FUNCTIONPLOT_TICKET);
	*/
	
	// register the newly inserted function's plot as renderable target
	pPlot = m_vec1DFuncPlots.back();
	RegisterTarget(pPlot);
	m_liRenderOrder.push_back(sRenderOrder(pPlot, fOrderPrio));
	// todo: check whether fOrderPrio is taken --> OrderPrioIsTaken()
	// todo: if yes, automatically assign a next free prio "p" > original fOrderPrio, "p" < legend's prio
	// --> SuggestFreeOrderPrio
	// hint: find the next higher prio n --> fOrderPrio = (n-p)/2
	pPlot->SetPlotRendering(this);

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   InsertRangePlot                                             */
/*                                                                            */
/*============================================================================*/
bool VflStaticPlotRendering::InsertRangePlot(VveTimeSeries *pFuncMin, 
											  VveTimeSeries *pFuncMax, 
											  float fOrderPrio)
{
	// register both functions
	InsertTimeSeriesFunction(pFuncMin, fOrderPrio);
	InsertTimeSeriesFunction(pFuncMax, fOrderPrio);

	m_vecRangePlots.push_back(new VflStaticRangePlot);
	VflStaticRangePlot *pPlot = m_vecRangePlots.back();
	pPlot->SetMinMaxFunction(pFuncMin, pFuncMax);
	pPlot->Init();

	float fCanvasXmin = m_v3PlotMin[0];
	float fCanvasXmax = m_v3PlotMax[0];
	float fCanvasYmin = m_v3PlotMin[1];
	float fCanvasYmax = m_v3PlotMax[1];
	pPlot->SetPlotCanvasCoordinates(fCanvasXmin, fCanvasXmax, fCanvasYmin, fCanvasYmax);

	// well, the ranges are still correct, since the above calls of 
	// InsertTimeSeriesFunction has triggered the update
	//-> m_fGlobalFuncMin, m_fGlobalFuncMax

	// set plot canvas coords
	/*
	float fGlobalCanvasYmin = m_v3PlotMin.GetVal(1);
	float fGlobalCanvasYmax = m_v3PlotMax.GetVal(1);
	float fScaleFactor = (fGlobalCanvasYmax-fGlobalCanvasYmin)
					    /(m_fGlobalFuncMax-m_fGlobalFuncMin);
	float fXValueMin=0.0f, fXValueMax=0.0f, fYValueMin=0.0f, fYValueMax=0.0f;
	pPlot->GetValueRanges(fXValueMin, fXValueMax, fYValueMin, fYValueMax); 
	float fCanvasYmin = (fYValueMin-m_fGlobalFuncMin)*fScaleFactor + fGlobalCanvasYmin;
	float fCanvasYmax = (fYValueMax-m_fGlobalFuncMin)*fScaleFactor + fGlobalCanvasYmin;
	float fCanvasXmin = m_v3PlotMin.GetVal(0);
	float fCanvasXmax = m_v3PlotMax.GetVal(0);
	pPlot->SetPlotCanvasCoordinates(fCanvasXmin, fCanvasXmax, fCanvasYmin, fCanvasYmax);
	*/

	// register the newly inserted function's plot as renderable target
	RegisterTarget(pPlot);
	m_liRenderOrder.push_back(sRenderOrder(pPlot, fOrderPrio));

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   InsertHistogram                                             */
/*                                                                            */
/*============================================================================*/
bool VflStaticPlotRendering::InsertHistogram(VveTimeHistogram *pHist,
											  vtkLookupTable *pLut,
											  float fOrderPrio)
{
	m_p2DHistogramPlot = new VflStatic2DHistogramPlot;
	m_p2DHistogramPlot->SetTimeHistogram(pHist);
	m_p2DHistogramPlot->Init();

	// set lut
	VflStatic2DHistogramPlot::VflStatic2DHistogramPlotProperties *pProps =
		dynamic_cast<VflStatic2DHistogramPlot::VflStatic2DHistogramPlotProperties*>
		(m_p2DHistogramPlot->GetProperties());
	pProps->SetVtkLookupTable(pLut);
	
	m_p2DHistogramPlot->SetPlotCanvasCoordinates(
		m_v3PlotMin[0], m_v3PlotMax[0], m_v3PlotMin[1], m_v3PlotMax[1]);

	RegisterTarget(m_p2DHistogramPlot);
	m_liRenderOrder.push_back(sRenderOrder(m_p2DHistogramPlot, fOrderPrio));

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetStatic1DFuncPlot                                         */
/*                                                                            */
/*============================================================================*/
VflStatic1DFuncPlot* VflStaticPlotRendering::GetStatic1DFuncPlot(
	VveTimeSeries *pFunc) const
{
	VveTimeSeries *pRegFunc;
	const size_t nSize = m_vec1DFuncPlots.size();
	for(size_t i=0; i<nSize; ++i)
	{
		pRegFunc = m_vec1DFuncPlots[i]->GetData();
		if(pRegFunc == pFunc)
			return m_vec1DFuncPlots[i];
	}
	return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetStaticRangePlot                                          */
/*                                                                            */
/*============================================================================*/
VflStaticRangePlot* VflStaticPlotRendering::GetStaticRangePlot(VveTimeSeries *pFuncMin, 
															 VveTimeSeries *pFuncMax) const
{
	const size_t nSize = m_vecRangePlots.size();
	VveTimeSeries *pRegMinFunc, *pRegMaxFunc;
	for(size_t i=0; i<nSize; ++i)
	{
		pRegMinFunc = m_vecRangePlots[i]->GetMinFunction();
		pRegMaxFunc = m_vecRangePlots[i]->GetMaxFunction();
		if(pRegMinFunc==pFuncMin && pRegMaxFunc==pFuncMax)
			return m_vecRangePlots[i];
	}
	return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetStaticRangePlot                                          */
/*                                                                            */
/*============================================================================*/
VflStatic2DHistogramPlot* VflStaticPlotRendering::Get2DHistogramPlot(
	VveTimeHistogram *pHist) const
{
	return m_p2DHistogramPlot;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPlotTitle                                                */
/*                                                                            */
/*============================================================================*/
void VflStaticPlotRendering::SetPlotTitle(string strTitle)
{
	m_pPlotTitle->SetText(strTitle);
	m_pPlotTitle->Update();

	VistaVector3D v3Min, v3Max;
	m_pPlotTitle->GetBounds(v3Min, v3Max);

	// adjust position
	float fLeft, fRight, fTop, fBottom, fNear, fFar;
	m_pViewAdapter->GetOrthoParams(fLeft, fRight, fBottom, fTop, fNear, fFar);

	VistaVector3D v3Pos;
	m_pPlotTitle->GetPosition(v3Pos);
	v3Pos[0] = fLeft + 0.5f*(fRight-fLeft) - 0.5f*(v3Max[0]-v3Min[0]);
	v3Pos[1] = fBottom + 0.95f*(fTop-fBottom) - 0.5f*(v3Max[1]-v3Min[1]);
	m_pPlotTitle->SetPosition(v3Pos);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPlotTitle                                                */
/*                                                                            */
/*============================================================================*/
string VflStaticPlotRendering::GetPlotTitle() const
{
	return m_pPlotTitle->GetText();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPlotTitle3DTextLabel                                     */
/*                                                                            */
/*============================================================================*/
Vfl3DTextLabel* VflStaticPlotRendering::GetPlotTitle3DTextLabel() const
{
	return m_pPlotTitle;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGridBackground                                           */
/*                                                                            */
/*============================================================================*/
VflRegGridBackground* VflStaticPlotRendering::GetGridBackground() const
{
	return m_pGrid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RearrangeElements                                           */
/*                                                                            */
/*============================================================================*/
void VflStaticPlotRendering::RearrangeElements()
{
	// sort m_liRenderOrder     // back to front -> small prio to big prio
	m_liRenderOrder.sort(sort_prio);

	std::list<sRenderOrder>::iterator it;
	int iNumOfLayers = 0;
	float fTmpLayerPrio = 0.0f;
	// init: get prio of the first layer (the most behind)
	it = m_liRenderOrder.begin();
	fTmpLayerPrio = (*it).fOrderPrio;
	++iNumOfLayers;
	++it;
	while(it!=m_liRenderOrder.end())
	{
		if(fTmpLayerPrio < (*it).fOrderPrio)
		{
			++iNumOfLayers;
			fTmpLayerPrio = (*it).fOrderPrio;
		}
		++it;
	}
	float fZstep = fabs(m_fZcoordBack-m_fZcoordFront) 
				 / (float)(iNumOfLayers-1);

	// set renderable properties, such that it gets the correct depth position
	// init
	bool bInit = true;
	size_t iCount = 0;
	const size_t nListSize = m_liRenderOrder.size();
	float fCurrentZ = m_fZcoordBack;
	it=m_liRenderOrder.begin();
	fTmpLayerPrio = (*it).fOrderPrio;
	while(iCount+1 < nListSize)
	{
		if(!bInit)
		{
			++it;
			++iCount;
			if(fTmpLayerPrio < (*it).fOrderPrio)
			{
				fTmpLayerPrio = (*it).fOrderPrio;
				fCurrentZ -= fZstep;
			}
		}
		else
			bInit = false;

		// in case of VflStatic1DFuncPlot
		VflStatic1DFuncPlot *pFuncPlot = dynamic_cast<VflStatic1DFuncPlot*>((*it).pRenderable);
		if(pFuncPlot)
		{
			pFuncPlot->SetDepth(fCurrentZ);
			continue;
		}
		// in case of VflStaticRangePlot
		VflStaticRangePlot *pRangePlot = dynamic_cast<VflStaticRangePlot*>((*it).pRenderable);
		if(pRangePlot)
		{
			pRangePlot->SetDepth(fCurrentZ);
			continue;
		}
		// in case of VflStatic2DHistogramPlot
		VflStatic2DHistogramPlot *pHistPlot = dynamic_cast<VflStatic2DHistogramPlot*>((*it).pRenderable);
		if(pHistPlot)
		{
			pHistPlot->SetDepth(fCurrentZ);
			continue;
		}
		// in case of VflLegendInfo
		VflLegendInfo *pLegendInfo = dynamic_cast<VflLegendInfo*>((*it).pRenderable);
		if(pLegendInfo)
		{
			VistaVector3D v3Min, v3Max;
			pLegendInfo->GetTargetBoundingBox(v3Min, v3Max);
			v3Min[2] = fCurrentZ;
			v3Max[2] = fCurrentZ+1.0f;
			pLegendInfo->SetTargetBoundingBox(v3Min, v3Max);
		}
		// in case of VflTimeLine
		VflTimeLine *pTimeLine = dynamic_cast<VflTimeLine*>((*it).pRenderable);
		if(pTimeLine)
		{
			VistaVector3D v3Start, v3End;
			VflTimeLine::VflTimeLineProperties *pTLProps = 
				static_cast<VflTimeLine::VflTimeLineProperties*>(pTimeLine->GetProperties());
			pTLProps->GetLinePoints(v3Start, v3End);
			v3Start[2] = fCurrentZ;
			v3End[2]   = fCurrentZ;
			pTLProps->SetLinePoints(v3Start, v3End);
		}
		// in case of Vfl3DTextLabel (title)
		Vfl3DTextLabel *pPlotTitle = dynamic_cast<Vfl3DTextLabel*>((*it).pRenderable);
		if(pPlotTitle)
		{
			VistaVector3D v3Pos;
			pPlotTitle->GetPosition(v3Pos);
			v3Pos[2] = fCurrentZ;
			pPlotTitle->SetPosition(v3Pos);
		}
		// in case of VflRegGridBackground
		VflRegGridBackground *pGrid = dynamic_cast<VflRegGridBackground*>((*it).pRenderable);
		if(pGrid)
		{
			VflRegGridBackground::VflRegGridBackgroundProperties *pGProps =
				static_cast<VflRegGridBackground::VflRegGridBackgroundProperties*>
				(pGrid->GetProperties());
			VistaVector3D v3Pos;
			pGProps->GetPosition(v3Pos);
			v3Pos[2] = fCurrentZ;
			pGProps->SetPosition(v3Pos);
		}
	}
	
	// work-around for the order bug.. -> (todo: has somenone a better idea?)
	// unregister everything..
	it=m_liRenderOrder.begin();
	while(it!=m_liRenderOrder.end())
	{
		UnregisterTarget((*it).pRenderable);
		++it;
	}
	it=m_liRenderOrder.begin();
	// re-register everything..
	while(it!=m_liRenderOrder.end())
	{
		RegisterTarget((*it).pRenderable);
		++it;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetGlobalFuncMinMax                                         */
/*                                                                            */
/*============================================================================*/
void VflStaticPlotRendering::GetGlobalFuncMinMax(float &fMin, float &fMax) const
{
	fMin = m_fGlobalFuncMin;
	fMax = m_fGlobalFuncMax;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTimeSetup                                                */
/*                                                                            */
/*============================================================================*/
void VflStaticPlotRendering::SetTimeSetup(int iNumOfTimeIndices, 
										   int iNumOfTimeLevels)
{
	m_oTimeSetup.iNumOfTimeIndices  = iNumOfTimeIndices;
	m_oTimeSetup.iNumOfTimeLevels = iNumOfTimeLevels;


}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeSetup                                                */
/*                                                                            */
/*============================================================================*/
void VflStaticPlotRendering::GetTimeSetup(int &iNumOfTimeIndices, 
										   int &iNumOfTimeLevels)
{
	iNumOfTimeIndices  = m_oTimeSetup.iNumOfTimeIndices;
	iNumOfTimeLevels = m_oTimeSetup.iNumOfTimeLevels; 
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPlotRanges                                               */
/*                                                                            */
/*============================================================================*/
void VflStaticPlotRendering::SetPlotRanges(float fXMin, float fXMax, 
											float fYMin, float fYMax)
{
	m_fGlobalFuncMin = fYMin;
	m_fGlobalFuncMax = fYMax;

	for(size_t i=0; i<m_vec1DFuncPlots.size(); ++i)
	{
		m_vec1DFuncPlots[i]->SetValueRanges(fXMin, fXMax, fYMin, fYMax);
	}
	for(size_t i=0; i<m_vecRangePlots.size(); ++i)
	{
		m_vecRangePlots[i]->SetValueRanges(fXMin, fXMax, fYMin, fYMax);
	}
	
	if(m_p2DHistogramPlot)
		m_p2DHistogramPlot->SetZoomRanges(fXMin, fXMax, fYMin, fYMax);

	//set legend accordingly
	VflLegendInfo::VflLegendInfoProperties *pLegendProps = 
		m_pPlotLegend->GetProperties();
	float fRanges[6] = {fXMin, fXMax, fYMin, fYMax, 0.0f, 0.0f}; 
	pLegendProps->SetLegendRanges(fRanges);

	// adjust time line
	VflTimeLine::VflTimeLineProperties *pTimeLineProps =
		dynamic_cast<VflTimeLine::VflTimeLineProperties*>(m_pTimeLine->GetProperties());
	pTimeLineProps->SetVisTimeRange(fXMin, fXMax);

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPlotLegend                                               */
/*                                                                            */
/*============================================================================*/
VflLegendInfo* VflStaticPlotRendering::GetPlotLegend() const
{
	return m_pPlotLegend;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeLine                                                 */
/*                                                                            */
/*============================================================================*/
VflTimeLine* VflStaticPlotRendering::GetTimeLine() const
{
	return m_pTimeLine;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflStaticPlotRendering::ObserverUpdate(IVistaObserveable *pObserveable, 
											 int msg, int ticket)
{
	if(ticket == VflStaticPlotRendering::E_FUNCTIONPLOT_TICKET)
	{
		// msg: minmax change
		switch(msg)
		{
		case VflStatic1DFuncPlot::MSG_MINMAXCHANGE:
			this->UpdatePlotScales();
			break;

		default:
			break;
		}
	}

	if(ticket == VflStaticPlotRendering::E_RANGEPLOT_TICKET)
	{
		// msg: 
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ForceRescaleRangesToMaxRanges                               */
/*                                                                            */
/*============================================================================*/
void VflStaticPlotRendering::ForceRescaleRangesToMaxRanges()
{
	//@todo IMPLEMENT ME!
	vstr::errp() << "[VflStaticPlotRendering::ForceRescaleRangesToMaxRanges] NOT IMPLEMENTED YET!" << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdatePlotScales                                            */
/*                                                                            */
/*============================================================================*/
void VflStaticPlotRendering::GetPlotDrawRegion(VistaVector3D &v3PlotMin, 
												VistaVector3D &v3PlotMax) const
{
	v3PlotMin = m_v3PlotMin;
	v3PlotMax = m_v3PlotMax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdatePlotScales                                            */
/*                                                                            */
/*============================================================================*/
void VflStaticPlotRendering::UpdatePlotScales()
{
	/*
	float fCanvasXmin=0.0f, fCanvasXmax=0.0f;
	fCanvasXmin = m_v3PlotMin.GetVal(0);
	fCanvasXmax = m_v3PlotMax.GetVal(0);

	VflStatic1DFuncPlot *pPlot = NULL;
	VflStaticRangePlot *pRangePlot = NULL;

	// --------------------- adjust other function plots ---------------------
	int iNumOfFuncPlots = m_vec1DFuncPlots.size();

	// get global min/max of f(t)
	float fFuncGlobalMin=0.0f, fFuncGlobalMax=0.0f;
	float fXValueMin=0.0f, fXValueMax=0.0f, fYValueMin=0.0f, fYValueMax=0.0f;
	// init
	pPlot = m_vec1DFuncPlots[0];
	pPlot->GetValueRanges(fXValueMin, fXValueMax, fYValueMin, fYValueMax); 
	fFuncGlobalMin = fYValueMin;
	fFuncGlobalMax = fYValueMax;
	// iteration
	for(int i=1; i<iNumOfFuncPlots; ++i)
	{
		pPlot = m_vec1DFuncPlots[i];
		pPlot->GetValueRanges(fXValueMin, fXValueMax, fYValueMin, fYValueMax); 
		if(fFuncGlobalMin > fYValueMin)
			fFuncGlobalMin = fYValueMin;
		if(fFuncGlobalMax < fYValueMax)
			fFuncGlobalMax = fYValueMax;
	}
	m_fGlobalFuncMin = fFuncGlobalMin;
	m_fGlobalFuncMax = fFuncGlobalMax;

	// set plot canvas coords for all time series function in the list
	float fCanvasYmin=0.0f, fCanvasYmax=0.0f;
	float fGlobalCanvasYmin = m_v3PlotMin.GetVal(1);
	float fGlobalCanvasYmax = m_v3PlotMax.GetVal(1);
	float fScaleFactor = (fGlobalCanvasYmax-fGlobalCanvasYmin)
						/(fFuncGlobalMax-fFuncGlobalMin);
	for(int i=0; i<iNumOfFuncPlots; ++i)
	{
		pPlot = m_vec1DFuncPlots[i];
		pPlot->GetValueRanges(fXValueMin, fXValueMax, fYValueMin, fYValueMax); 
		fCanvasYmin = (fYValueMin-fFuncGlobalMin)*fScaleFactor + fGlobalCanvasYmin;
		fCanvasYmax = (fYValueMax-fFuncGlobalMin)*fScaleFactor + fGlobalCanvasYmin;
		pPlot->SetPlotCanvasCoordinates(fCanvasXmin, fCanvasXmax, fCanvasYmin, fCanvasYmax);
	}
	//---------------------------------------------------------------------------

	// ----------------------- adjust other range plots -------------------------
	int iNumOfRangePlots = m_vecRangePlots.size();
	// global min/max of f(t) is in m_fGlobalFuncMin, m_fGlobalFuncMax

	// set plot canvas coords for all range plots in the list
	for(int i=0; i<iNumOfRangePlots; ++i)
	{
		pRangePlot = m_vecRangePlots[i];
		pRangePlot->GetValueRanges(fXValueMin, fXValueMax, fYValueMin, fYValueMax);
		fCanvasYmin = (fYValueMin-fFuncGlobalMin)*fScaleFactor + fGlobalCanvasYmin;
		fCanvasYmax = (fYValueMax-fFuncGlobalMin)*fScaleFactor + fGlobalCanvasYmin;
		pRangePlot->SetPlotCanvasCoordinates(fCanvasXmin, fCanvasXmax, fCanvasYmin, fCanvasYmax);
	}
	//---------------------------------------------------------------------------
	*/
	//always draw to the whole canvas.
	float fCanvasXmin = m_v3PlotMin[0];
	float fCanvasXmax = m_v3PlotMax[0];
	float fCanvasYmin = m_v3PlotMin[1];
	float fCanvasYmax = m_v3PlotMax[1];
	
	//set canvas ranges for all function plots
	for(size_t i=0; i<m_vec1DFuncPlots.size(); ++i)
	{
		m_vec1DFuncPlots[i]->SetPlotCanvasCoordinates(fCanvasXmin, fCanvasXmax, fCanvasYmin, fCanvasYmax);
	}
	for(size_t i=0; i<m_vecRangePlots.size(); ++i)
	{
		m_vecRangePlots[i]->SetPlotCanvasCoordinates(fCanvasXmin, fCanvasXmax, fCanvasYmin, fCanvasYmax);
	}
}

// ############################################################################


static const string SsReflectionType("VflStatic2DPlotRendering");

//static IVistaPropertyGetFunctor *aCgFunctors[] =
//{
//};

//static IVistaPropertySetFunctor *aCsFunctors[] =
//{
//};


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflStaticPlotRendering.cpp"                                  */
/*============================================================================*/
