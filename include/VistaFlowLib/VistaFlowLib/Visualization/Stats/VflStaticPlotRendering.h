/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLSTATICPLOTRENDERING_H
#define _VFLSTATICPLOTRENDERING_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "../VflRenderOrthoProjToTexture.h"

#include <VistaFlowLib/Data/VflObserver.h>

#include <VistaBase/VistaVectorMath.h>

#include <iostream>
#include <string>
#include <list>
#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTimeSeries;
class VflLegendInfo;
class VflStatic1DFuncPlot;
class VflStaticRangePlot;
class VflStatic2DHistogramPlot;
class VflTimeLine;
class Vfl3DTextLabel;
class VflRegGridBackground;
class VveTimeHistogram;
class vtkLookupTable;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAPI VflStaticPlotRendering : public IVflRenderOrthoProjToTexture, public VflObserver
{
public:

    // CONSTRUCTORS / DESTRUCTOR
    VflStaticPlotRendering(	
		VflRenderNode *pRenderNode, 
		int iWidth = 32, int iHeight = 32,
		float fLeft   =-1.0f, float fRight = 1.0f, 
		float fBottom =-1.0f, float fTop   = 1.0f,
		float fNear   = 1.0f, float fFar   =-1.0f);
	virtual ~VflStaticPlotRendering();

	VflLegendInfo *GetPlotLegend() const;
	VflTimeLine* GetTimeLine() const;

	/**
	 *	Register TimeSeriesFunctions to be drawn by the 1D function plots
	 *  @param[in]  VveTimeSeries , a 1D function which must match the defined time setup
	 *          float fOrderPrio, order priority. Low value -> background. Value 10 is reserved for the legend.
	 *          bool bCheckTimeSetup, whether the insertion should fail if time series's time mapper doesn't match
	 *          the time setup
	 *  @param[out] bool whether the registration succeeded
	 */
	bool InsertTimeSeriesFunction(VveTimeSeries *pFunc, float fOrderPrio=1.0f, bool bCheckTimeSetup=true);


	/**
	 *  pFuncMin and pFuncMax will be automatically registered as TimeSeries functions
	 *  additonally, a range plot between pFuncMin and pFuncMax will be generated
	 */
	bool InsertRangePlot(VveTimeSeries *pFuncMin, VveTimeSeries *pFuncMax, float fOrderPrio=1.0f);
	
	/**
	 * Insert a TimeHistogram which will be ploted by VflStatic2DHistogramPlot.
	 * 
	 */
	bool InsertHistogram(VveTimeHistogram *pHist, vtkLookupTable *pLut, float fOrderPrio=1.0f);

	 /**
	  * Query function to get an access to VflStatic1DFuncPlot which was created as
	  * VveTimeSeries *pFunc was inserted by the call of InsertTimeSeriesFunction.
	  * If no appropriate VflStatic1DFuncPlot can be found then NULL will be returned.
	  * @param[in]  VveTimeSeries to query the renderable plot
	  * @param[out]	VflStatic1DFuncPlot, the corresponding plot instance  
	  */
	 VflStatic1DFuncPlot* GetStatic1DFuncPlot(VveTimeSeries *pFunc) const;

	 /**
	  * Query function to get an access to VflStaticRangePlot which was created 
	  * by the call of InsertTimeSeriesFunction(VveTimeSeries *pFuncMin, VveTimeSeries *pFuncMax)
	  * If no appropriate VflStatic1DFuncPlot can be found then NULL will be returned.
	  * @param[in]  VveTimeSeries *pFuncMin and *pFuncMax to query the renderable plot
	  * @param[out]	VflStaticRangePlot, the corresponding plot instance  
	  */
	 VflStaticRangePlot* GetStaticRangePlot(VveTimeSeries *pFuncMin, VveTimeSeries *pFuncMax) const;

	/**
	 * Query the time histogram plot.
	 * @param[in]   VveTimeHistogram            the histogram
	 * @param[out]  VflStatic2DHistogramPlot    the corresponding plot
	 */
	 VflStatic2DHistogramPlot* Get2DHistogramPlot(VveTimeHistogram *pHist) const;


	/**
	 * Timing setup, which must fit for every inserted TimeSeries.
	 * This must be called before any VveTimeSeries is registered.
	 * @param[in] iNumOfTimeIndex, iNumOfLevelIndex
	 * @OUTPU -- 
	 */
	void SetTimeSetup(int iNumOfTimeIndices, int iNumOfTimeLevels);
	void GetTimeSetup(int &iNumOfTimeIndices, int &iNumOfTimeLevels);

	/**
	 * Set uniforma value ranges for all plots managed by this object
	 * NOTE: these ranges are given in data units. For the x-axis this usually
	 *       is the sim time (use a subinterval of [0..1] to zoom onto that 
	 *       interval) The YRange heavily depends on the data you plot.
	 */
	void SetPlotRanges(float fXMin, float fXMax, float fYMin, float fYMax);

	/**
	 * draw order
	 * fOrderPrio is always positive, i.e. > 0.0
	 * high prio --> front
	 * low  prio --> back
	 * the prio-value "10" is reserved for plot legend!
	 */
	struct sRenderOrder
	{
		IVflRenderable	*pRenderable;
		float			fOrderPrio;
		sRenderOrder(IVflRenderable *pRend, float fPrio)
			: pRenderable(pRend), fOrderPrio(fPrio)
		{
		}
	};

	void GetGlobalFuncMinMax(float &fMin, float &fMax) const;
	void GetPlotDrawRegion(VistaVector3D &v3PlotMin, VistaVector3D &v3PlotMax) const;

	 // adjust front/back dependencies
	void RearrangeElements();  

	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	void SetPlotTitle(std::string strTitle);
	std::string GetPlotTitle() const;
	Vfl3DTextLabel* GetPlotTitle3DTextLabel() const;

	VflRegGridBackground* GetGridBackground() const;

	//@TODO Implement me!
	void ForceRescaleRangesToMaxRanges();

protected:
	enum
	{
		E_FUNCTIONPLOT_TICKET,
		E_RANGEPLOT_TICKET,
		E_TICKET_LAST
	};

private:

	void UpdatePlotScales();

	VflLegendInfo*					m_pPlotLegend;
	std::vector<VflStatic1DFuncPlot*>	m_vec1DFuncPlots;
	std::vector<VflStaticRangePlot*>	m_vecRangePlots;
	VflStatic2DHistogramPlot*		m_p2DHistogramPlot;
	VflTimeLine* 					m_pTimeLine;
	Vfl3DTextLabel*				m_pPlotTitle;
	VflRegGridBackground*			m_pGrid;		

	std::list<sRenderOrder> m_liRenderOrder;

	// plot's draw region
	VistaVector3D		m_v3PlotMin,
						m_v3PlotMax;

	float m_fGlobalFuncMin;
	float m_fGlobalFuncMax;

	float m_fZcoordFront;
	float m_fZcoordBack;

	bool m_bDrawTimeLine;

	/**
	 * time setup
	 */
	struct
	{
		int iNumOfTimeIndices;
		int iNumOfTimeLevels;
	} m_oTimeSetup;

};

/*============================================================================*/
/* INLINE METHODS                                                             */
/*============================================================================*/



/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_STATICPLOTRENDERING_H)
