/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <GL/glew.h>

#include "VflStatic2DHistogramPlot.h"

#include <VistaVisExt/Data/VveTimeHistogram.h>
#include <VistaVisExt/Tools/Vve2DHistogram.h>

#include <vtkLookupTable.h>

#include <cassert>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflStatic2DHistogramPlot::VflStatic2DHistogramPlot()
: m_dLastDrawSimTime(-1.0),
  m_dLastDrawTransSimTime(-1.0),
  m_bNeedUpdate(true),
  m_pData(NULL),
  m_fPlotCanvasXmin(0.0f), m_fPlotCanvasXmax(1.0f),
  m_fPlotCanvasYmin(0.0f), m_fPlotCanvasYmax(1.0f),
  m_fDepth(0.0f),
  m_fZoomXmin(0.0f), m_fZoomXmax(1.0f)
{
	m_v3BoundsMin = VistaVector3D(0, 0, 0);
	m_v3BoundsMax = VistaVector3D(0, 0, 0);

}

VflStatic2DHistogramPlot::~VflStatic2DHistogramPlot()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflStatic2DHistogramPlot::Init()
{
	if(!m_pData)
	{
		vstr::errp() << "[VflStatic2DHistogramPlot] min or max function is undefined" << endl;
		return false;
	}

	if(!IVflVisObject::Init())
		return false;
		
	this->UpdatePlotPoints();

	return true;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTimeHistogram                                            */
/*                                                                            */
/*============================================================================*/
bool VflStatic2DHistogramPlot::SetTimeHistogram(VveTimeHistogram *pData)
{
	if(pData == m_pData)
		return false;
	
	m_pData = pData;
	m_bNeedUpdate = true;
	this->UpdatePlotPoints();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeHistogram                                            */
/*                                                                            */
/*============================================================================*/
VveTimeHistogram* VflStatic2DHistogramPlot::GetTimeHistogram() const
{
	return m_pData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetUnsteadyData                                             */
/*                                                                            */
/*============================================================================*/
bool VflStatic2DHistogramPlot::SetUnsteadyData(VveUnsteadyData *pData)
{
	VveTimeHistogram *pTimeHistogram = dynamic_cast<VveTimeHistogram*>(pData);
	if(!pTimeHistogram)
		return false; 

	return SetTimeHistogram(pTimeHistogram);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdatePlotPoints                                            */
/*                                                                            */
/*============================================================================*/
void VflStatic2DHistogramPlot::UpdatePlotPoints()
{
	if(!m_bNeedUpdate || m_pData == NULL)
		return;

	VflStatic2DHistogramPlotProperties *pProps = this->GetProperties();
	if(pProps == NULL)
		return;

	vtkLookupTable *pLut = pProps->GetVtkLookupTable();

	if(!pLut)
	{
		vstr::errp() << "[VflStatic2DHistogramPlot] invalid lookup table" << endl;
		return;
	}

	m_vecQuads.clear();
	m_vecColors.clear();

	VveTimeMapper *pTimeMapper = m_pData->GetTimeMapper();
	int iNumOfTIs = pTimeMapper->GetNumberOfTimeIndices();

	int iResX=0, iResY=0;
	Vve2DHistogram *pHist = m_pData->GetHistogram();
	pHist->GetResolution(iResX, iResY);
	iResX -= 2;
	iResY -= 2;   // without the outliners
	
	if(iResX != iNumOfTIs)
	{
		vstr::errp() << "[VflStatic2DHistogramPlot] invalid histogram" << endl;
		return;
	}

	//NOTE: x axis needs offsetting by 1/2 space due to specific
	//notation of the time mapper (first and last time step are valid
	//for 1/2 deltaT only) --> therefore this is -1!
	float fStepX = (m_fPlotCanvasXmax-m_fPlotCanvasXmin)/(float)(iResX-1); 	
	//NOTE: no offsetting on y-axis
	float fStepY = (m_fPlotCanvasYmax-m_fPlotCanvasYmin)/(float)(iResY);

	float fMaxBC = pHist->GetMaxBinCount();
	int iBinCount = -1;
	double dRGB[3] = {0.0, 0.0, 0.0};
	VistaVector3D v0, v1, v2, v3;
	float fCurrentBottom, fCurrentTop;

	// compute tiles' edge point positions
	//     v3        v2
	//       o------o
	//       |      |
	//       o------o
	//     v0        v1
	fCurrentBottom = m_fPlotCanvasYmin;
	fCurrentTop = m_fPlotCanvasYmin + fStepY;
	for(int j=0; j<iResY; ++j)
	{
		/*
		// init first tile of the row
		if(j==0)
		{   // the first row, first column
			fCurrentBottom = m_fPlotCanvasYmin;
			fCurrentTop    = m_fPlotCanvasYmin + 0.5f*fStepY;
		}
		else if(j==1)
		{   // second row, first column
			fCurrentBottom += 0.5f*fStepY;
			fCurrentTop    += fStepY;
		}
		else if(j>1 && j<iResY-1)
		*/
		{
			fCurrentBottom += fStepY;
			fCurrentTop	   += fStepY;
		}
		/*
		else if(j==iResY-1)
		{
			fCurrentBottom += fStepY;
			fCurrentTop    += 0.5f*fStepY;
		}
		*/
		v0 = VistaVector3D(m_fPlotCanvasXmin			 , fCurrentBottom, m_fDepth);
		v1 = VistaVector3D(m_fPlotCanvasXmin+0.5f*fStepX, fCurrentBottom, m_fDepth);
		v2 = VistaVector3D(m_fPlotCanvasXmin+0.5f*fStepX, fCurrentTop	 , m_fDepth);
		v3 = VistaVector3D(m_fPlotCanvasXmin			 , fCurrentTop	 , m_fDepth);

		for(int i=0; i<iResX; ++i)
		{
			iBinCount = pHist->GetBinCount(i+1,j+1);   // "shift" to get the outliners out 
			// determine color
			pLut->GetColor(float(iBinCount)/fMaxBC, dRGB);
			m_vecColors.push_back(sColor((float)dRGB[0],(float)dRGB[1],(float)dRGB[2]));

			// determine current tile
			if(i==1)             
			{// update positions for the second tile in current row
				v0[0] += 0.5f*fStepX;
				v1[0] += fStepX;
				v2[0] += fStepX;
				v3[0] += 0.5f*fStepX;
			}
			else if(i>1 && i<iResX-1)
			{
				v0[0] += fStepX;
				v1[0] += fStepX;
				v2[0] += fStepX;
				v3[0] += fStepX;
			}
			else if(i==iResX-1)
			{
				v0[0] += fStepX;
				v1[0] += 0.5f*fStepX;
				v2[0] += 0.5f*fStepX;
				v3[0] += fStepX;
			}

			m_vecQuads.push_back(sQuadPoints(v0,v1,v2,v3));
		}
	}
	
	// zoom in x and y direction
	float fXScale = 1.0f/ fabs(m_fZoomXmax-m_fZoomXmin);
	float fXShift = m_fZoomXmin * (m_fPlotCanvasXmax-m_fPlotCanvasXmin) * fXScale;

	double dHistXmin, dHistXmax, dHistYmin, dHistYmax;
	pHist->GetRanges(dHistXmin, dHistXmax, dHistYmin, dHistYmax);
	float fYScale = fabs(dHistYmax-dHistYmin)/ fabs(m_fZoomYmax-m_fZoomYmin);
	float fYShift = m_fZoomYmin * (m_fPlotCanvasYmax-m_fPlotCanvasYmin) * fYScale;
	float fTmp;
	// zoom and shift every point of every quads in the list
	const int nNumOfQuads = static_cast<int>(m_vecQuads.size());
	for(int i=0; i<nNumOfQuads; ++i)
	{
		// v0
		fTmp = (m_vecQuads[i].v0[0]-m_fPlotCanvasXmin)*fXScale + m_fPlotCanvasXmin-fXShift;
		ClipXCoord(fTmp);
		m_vecQuads[i].v0[0] = fTmp;
		fTmp = (m_vecQuads[i].v0[1]-m_fPlotCanvasYmin)*fYScale + m_fPlotCanvasYmin-fYShift;
		ClipYCoord(fTmp);
		m_vecQuads[i].v0[1] = fTmp;

		// v1
		fTmp = (m_vecQuads[i].v1[0]-m_fPlotCanvasXmin)*fXScale + m_fPlotCanvasXmin - fXShift;
		ClipXCoord(fTmp);
		m_vecQuads[i].v1[0] = fTmp;
		fTmp = (m_vecQuads[i].v1[1]-m_fPlotCanvasYmin)*fYScale + m_fPlotCanvasYmin-fYShift;
		ClipYCoord(fTmp);
		m_vecQuads[i].v1[1] = fTmp;

		// v2
		fTmp = (m_vecQuads[i].v2[0]-m_fPlotCanvasXmin)*fXScale + m_fPlotCanvasXmin - fXShift;
		ClipXCoord(fTmp);
		m_vecQuads[i].v2[0] = fTmp;
		fTmp = (m_vecQuads[i].v2[1]-m_fPlotCanvasYmin)*fYScale + m_fPlotCanvasYmin-fYShift;
		ClipYCoord(fTmp);
		m_vecQuads[i].v2[1] = fTmp;

		// v3
		fTmp = (m_vecQuads[i].v3[0]-m_fPlotCanvasXmin)*fXScale + m_fPlotCanvasXmin - fXShift;
		ClipXCoord(fTmp);
		m_vecQuads[i].v3[0] = fTmp;
		fTmp = (m_vecQuads[i].v3[1]-m_fPlotCanvasYmin)*fYScale + m_fPlotCanvasYmin-fYShift;
		ClipYCoord(fTmp);
		m_vecQuads[i].v3[1] = fTmp;
	}

	// done, no update need until the observed VveTimeSeries changes
	m_bNeedUpdate = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ClipXCoord                                                  */
/*                                                                            */
/*============================================================================*/
void VflStatic2DHistogramPlot::ClipXCoord(float &fX)
{
	if(fX < m_fPlotCanvasXmin)
		fX = m_fPlotCanvasXmin;
	if(fX > m_fPlotCanvasXmax)
		fX = m_fPlotCanvasXmax;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ClipYCoord                                                  */
/*                                                                            */
/*============================================================================*/
void VflStatic2DHistogramPlot::ClipYCoord(float &fY)
{
	if(fY < m_fPlotCanvasYmin)
		fY = m_fPlotCanvasYmin;
	if(fY > m_fPlotCanvasYmax)
		fY = m_fPlotCanvasYmax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflStatic2DHistogramPlot::Update()
{
	//vstr::outi() << "Ta da !" << endl;
	UpdatePlotPoints();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflStatic2DHistogramPlot::DrawOpaque()
{
	const int nNumOfTiles = static_cast<int>(m_vecQuads.size());

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_LIGHTING);

	glBegin(GL_QUADS);
	for(int i=0; i<nNumOfTiles; ++i)
	{
		glColor3f(m_vecColors[i].fRed, m_vecColors[i].fGreen, m_vecColors[i].fBlue);
		glVertex3f(m_vecQuads[i].v0[0], m_vecQuads[i].v0[1], m_vecQuads[i].v0[2]);
		glVertex3f(m_vecQuads[i].v1[0], m_vecQuads[i].v1[1], m_vecQuads[i].v1[2]);
		glVertex3f(m_vecQuads[i].v2[0], m_vecQuads[i].v2[1], m_vecQuads[i].v2[2]);
		glVertex3f(m_vecQuads[i].v3[0], m_vecQuads[i].v3[1], m_vecQuads[i].v3[2]);
	}
	glEnd();

	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetZoomRanges                                               */
/*                                                                            */
/*============================================================================*/
void VflStatic2DHistogramPlot::SetZoomRanges(float fXMin, float fXMax,
											  float fYMin, float fYMax)
{
	m_fZoomXmin = fXMin;
	m_fZoomXmax = fXMax;
	m_fZoomYmin = fYMin;
	m_fZoomYmax = fYMax;
	m_bNeedUpdate = true;
	UpdatePlotPoints();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflStatic2DHistogramPlot::GetRegistrationMode() const
{
	//return OLI_UPDATE;
	return OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VflStatic2DHistogramPlot::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	minBounds = m_v3BoundsMin;
	maxBounds = m_v3BoundsMax;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflStatic2DHistogramPlot::GetType() const
{
	return IVflVisObject::GetType()+string("::VflStatic2DHistogramPlot");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflStatic2DHistogramPlot::Debug(std::ostream & out) const
{
	IVflVisObject::Debug(out);

	if (m_bBoundsValid)
		out << m_v3BoundsMin << " - " << m_v3BoundsMax << endl;
	else
		out << "*invalid*" << endl;
}


void VflStatic2DHistogramPlot::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	switch(ticket)
	{
	case IVflVisObject::E_UNSTEADYDATA_TICKET:
		m_bNeedUpdate = true;
		break;
	default:
		break;
	}

	//this->ComputeMinMaxYValues();
	this->UpdatePlotPoints();
	//VflVisObject::ObserverUpdate(pObserveable, msg, ticket);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
void VflStatic2DHistogramPlot::SetPlotCanvasCoordinates(float fXmin, float fXmax, 
													 float fYmin, float fYmax)
{
	m_fPlotCanvasXmin = fXmin;
	m_fPlotCanvasXmax = fXmax;
	m_fPlotCanvasYmin = fYmin;
	m_fPlotCanvasYmax = fYmax;
	m_bNeedUpdate = true;
	UpdatePlotPoints();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
void VflStatic2DHistogramPlot::GetPlotCanvasCoordinates(float &fXmin, float &fXmax, 
													 float &fYmin, float &fYmax)
{
	fXmin = m_fPlotCanvasXmin;
	fXmax = m_fPlotCanvasXmax;
	fYmin = m_fPlotCanvasYmin;
	fYmax = m_fPlotCanvasYmax;
}
VflStatic2DHistogramPlot::VflStatic2DHistogramPlotProperties* 
VflStatic2DHistogramPlot::GetProperties() const
{
	return static_cast<VflStatic2DHistogramPlotProperties*>(
		IVflRenderable::GetProperties());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
bool VflStatic2DHistogramPlot::ComputeBounds()
{
	bool bBoundsValid = false;

	// todo: compute bounds

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  SetDepth                                                     */
/*                                                                            */
/*============================================================================*/
void VflStatic2DHistogramPlot::SetDepth(float fZ)
{
	m_fDepth = fZ;
	m_bNeedUpdate = true;
	UpdatePlotPoints();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetDepth                                                     */
/*                                                                            */
/*============================================================================*/
float VflStatic2DHistogramPlot::GetDepth() const
{
	return m_fDepth;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :  GetDepth                                                     */
/*                                                                            */
/*============================================================================*/
IVflVisObject::VflVisObjProperties* VflStatic2DHistogramPlot::CreateProperties() const
{
	return new VflStatic2DHistogramPlotProperties;
}

// ############################################################################


static const string SsReflectionType("VflStatic2DHistogramPlot");

//static IVistaPropertyGetFunctor *aCgFunctors[] =
//{
//};

//static IVistaPropertySetFunctor *aCsFunctors[] =
//{
//};

VflStatic2DHistogramPlot::VflStatic2DHistogramPlotProperties::VflStatic2DHistogramPlotProperties()
: IVflVisObject::VflVisObjProperties(),
m_pLut(NULL)
{
}
VflStatic2DHistogramPlot::VflStatic2DHistogramPlotProperties::~VflStatic2DHistogramPlotProperties()
{
}
void VflStatic2DHistogramPlot::VflStatic2DHistogramPlotProperties::SetVtkLookupTable(vtkLookupTable *pLut)
{
	m_pLut = pLut;
}
vtkLookupTable* VflStatic2DHistogramPlot::VflStatic2DHistogramPlotProperties::GetVtkLookupTable() const
{
	return m_pLut;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflStatic2DHistogramPlot.cpp"                                */
/*============================================================================*/
