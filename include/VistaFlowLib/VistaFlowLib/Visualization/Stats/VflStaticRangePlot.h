/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLSTATICRANGEPLOT_H
#define _VFLSTATICRANGEPLOT_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VflVisObject.h"

#include <VistaBase/VistaVectorMath.h>

#include <iostream>
#include <string>
#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTimeSeries;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 *   This class renders range plot for given maximal function and minimal function.
 *   The max / min functions must be defined as VveTimeSeries.
 *   There is no check, whether max function > min function.
 *   So make sure, that this is the case before using this class.
 */

class VISTAFLOWLIBAPI VflStaticRangePlot : public IVflVisObject
{
public:

    // CONSTRUCTORS / DESTRUCTOR
    VflStaticRangePlot();
    virtual ~VflStaticRangePlot();

    /**
     * Initialize object (and register with visualization controller).
     * 
     * @param[in]   VflVisController *pController		the visualization controller
	 * @param[in]	int iRegistration				callbacks to be registered for (default: NONE)
	 *                                          see VflVisController::OBJECT_LIST_MODE for more
     * @param[out]  --
     * @GLOBALS --
     * @return  bool							success of the operation
     *
     */    
    virtual bool Init();

	/** 
	 * Update the underlying data object. To be precise, the currently active
	 * time level gets updated.
	 */
	virtual void Update();
	void UpdatePlotPoints();

    /**
     * Draw transparent stuff. During rendering the underlying data object gets locked.
     * 
     * @param[in]   --
     * @param[out]  --
     * @GLOBALS --
     * @return  --
     *
     */    
    virtual void DrawTransparent();

    /**
     * Returns the boundaries of the visualization object.
	 * Note: This method is not declared const, as it might change the
	 *       cached boundaries inside the object.
     * 
     * @param[out]  VistaVector3D minBounds    minimum of the object's AABB
	 * @param[out]  VistaVector3D maxBounds    maximum of the object's AABB
     * @return  bool	is the bounding box valid?
     */    
	virtual bool GetBounds(VistaVector3D &minBounds, 
		                   VistaVector3D &maxBounds);

    /**
     * Returns the name of the visualization object.
     * 
     * @param[in]   --
     * @return  std::string
     */    
	virtual std::string GetType() const;

    /**
     * Prints out some debug information to the given output stream.
     * 
     * @param[in]   std::ostream & out
     * @return  --
     */    
    virtual void Debug(std::ostream & out) const;

    /**
     * The callback method from the IVistaObserver class, which is to be
	 * used for notifications concerning data changes.
     */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	virtual unsigned int GetRegistrationMode() const;

	/**
     * Set the coordinates intervall for the plot canvas
	 *
     * @param[in]   float x_min, float x_max, float y_min, float y_max
     * @return  --
     */    
	void SetPlotCanvasCoordinates(float fXmin, float fXmax, float fYmin, float fYmax);

	/**
     * Get the coordinates intervall for the plot canvas
	 *
	 * @param[in]   --
     * @param[out]   float x_min, float x_max, float y_min, float y_max
     * @return  --
     */
	void GetPlotCanvasCoordinates(float &fXmin, float &fXmax, float &fYmin, float &fYmax);


	/**
     * Set the unsteady data object to be visualized. Note, that this is only
	 * possible as long as Init() has not been called!
     * 
     * @param[in]   VveTimeSeries *pMinFunc   the minimal function for the range plot
	 *          VveTimeSeries *pMaxFunc   the maximal function for the range plot
     * @return  bool                                  Operation successful?
     */    
	bool SetMinMaxFunction(VveTimeSeries *pMinFunc, VveTimeSeries *pMaxFunc);

	/**
     * Get a pointer to the min function
     * 
     * @param[in]   ---
     * @return  VveTimeSeries*     the corresponding data
     */  
	//VveTimeSeries* GetData() const;
	VveTimeSeries* GetMinFunction() const;

	/**
     * Get a pointer to the max function
     * 
     * @param[in]   ---
     * @return  VveTimeSeries*     the corresponding data
     */  
	//VveTimeSeries* GetData() const;
	VveTimeSeries* GetMaxFunction() const;

	/**
	 * Overwrite VflVisObject's method. The call is redirect to SetData(VveTimeSeries*)
	 */
	bool SetUnsteadyData(VveUnsteadyData *pData);

	void SetValueRanges(float fXValueMin, float fXValueMax, 
			 		    float fYValueMin, float fYValueMax);

	void GetValueRanges(float &fXValueMin, float &fXValueMax, 
						float &fYValueMin, float &fYValueMax) const;

	void SetDepth(float fZ);
	float GetDepth() const;


	class VISTAFLOWLIBAPI VflStaticRangePlotProperties : public VflVisObjProperties
	{
		friend class VflStatic1DFuncPlot;
	public:
		VflStaticRangePlotProperties();
		virtual ~VflStaticRangePlotProperties();

		void SetColor(float fR, float fG, float fB);
		void GetColor(float &fR, float &fG, float &fB) const;

		void SetOpacity(float fOpacity);
		float GetOpacity() const;

	private:
		float	m_fColorR,
				m_fColorG,
				m_fColorB;
		float	m_fOpacity;
	};

	virtual VflStaticRangePlotProperties *GetProperties() const;

private:
    /**
     * Compute (or update) the object's boundaries.
     */    
	virtual bool ComputeBounds();
	void ComputeMinMaxYValues();

	virtual VflVisObjProperties *CreateProperties() const;

	// member attributes

	VveTimeSeries	*m_pMinFunc;
	VveTimeSeries	*m_pMaxFunc;

	/**
	 * to be rendered using GL_QUAD_STRIP
	 *
	 *    v0       v2      v4         
	 *     x--------x-------x--- ...    m_vecMaxPlotPoints   
	 *     |        |       |
	 *     x--------x-------x--- ...    m_vecMinPlotPoints
	 *    v1       v3       v5
	 */
	std::vector<VistaVector3D> m_vecMaxPlotPoints;
	std::vector<VistaVector3D> m_vecMinPlotPoints;

	bool m_bNeedUpdate;

	float	m_fPlotCanvasXmin, 
			m_fPlotCanvasXmax,
			m_fPlotCanvasYmin,
			m_fPlotCanvasYmax;

	bool m_bBoundsValid; 

	double      m_dLastDrawSimTime,
		        m_dLastDrawTransSimTime;
	
	VistaVector3D m_v3BoundsMin, 
		           m_v3BoundsMax;
	float			m_fDepth;

	float	m_fXValueMin, 
			m_fXValueMax,
			m_fYValueMin,
			m_fYValueMax;
};

/*============================================================================*/
/* INLINE METHODS                                                             */
/*============================================================================*/



/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLSTATICRANGEPLOT_H)
