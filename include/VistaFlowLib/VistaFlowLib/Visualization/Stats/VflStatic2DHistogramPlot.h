/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLSTATIC2DHISTOGRAMPLOT_H
#define _VFLSTATIC2DHISTOGRAMPLOT_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflVisObject.h>

#include <VistaBase/VistaVectorMath.h>

#include <iostream>
#include <string>
#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTimeHistogram;
class vtkLookupTable;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 *   This class renders range plot for given maximal function and minimal function.
 *   The max / min functions must be defined as VveTimeSeries.
 *   There is no check, whether max function > min function.
 *   So make sure, that this is the case before using this class.
 */

class VISTAFLOWLIBAPI VflStatic2DHistogramPlot : public IVflVisObject
{
public:

    // CONSTRUCTORS / DESTRUCTOR
    VflStatic2DHistogramPlot();
    virtual ~VflStatic2DHistogramPlot();
   
    virtual bool Init();
	virtual void Update();
    virtual void DrawOpaque();

	void UpdatePlotPoints();

    /**
     * Returns the boundaries of the visualization object.
	 * Note: This method is not declared const, as it might change the
	 *       cached boundaries inside the object.
     * 
     * @param[out]  VistaVector3D minBounds    minimum of the object's AABB
	 * @param[out]  VistaVector3D maxBounds    maximum of the object's AABB
     * @return  bool	is the bounding box valid?
     */    
	virtual bool GetBounds(VistaVector3D &minBounds, 
		                   VistaVector3D &maxBounds);

	virtual std::string GetType() const;
    virtual void Debug(std::ostream & out) const;
	virtual unsigned int GetRegistrationMode() const;

    /**
     * The callback method from the IVistaObserver class, which is to be
	 * used for notifications concerning data changes.
     */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	/**
     * Set the coordinates intervall for the plot canvas
	 *
     * @param[in]   float x_min, float x_max, float y_min, float y_max
     * @return  --
     */    
	void SetPlotCanvasCoordinates(float fXmin, float fXmax, float fYmin, float fYmax);

	/**
     * Get the coordinates intervall for the plot canvas
	 *
	 * @param[in]   --
     * @param[out]   float x_min, float x_max, float y_min, float y_max
     * @return  --
     */
	void GetPlotCanvasCoordinates(float &fXmin, float &fXmax, float &fYmin, float &fYmax);

	/**
	 * Overwrite VflVisObject's method. The call is redirect to SetData(VveTimeHistogram*)
	 */
	bool SetUnsteadyData(VveUnsteadyData *pData);
	bool SetTimeHistogram(VveTimeHistogram *pData);
	VveTimeHistogram* GetTimeHistogram() const;

	//void GetValueRanges(float &fXValueMin, float &fXValueMax, 
	//					float &fYValueMin, float &fYValueMax);

	void SetDepth(float fZ);
	float GetDepth() const;

	void SetZoomRanges(float fXMin, float fXMax, float fYMin, float fYMax);

	class VISTAFLOWLIBAPI VflStatic2DHistogramPlotProperties : public VflVisObjProperties
	{
		friend class VflStatic1DFuncPlot;
	public:
		VflStatic2DHistogramPlotProperties();
		virtual ~VflStatic2DHistogramPlotProperties();

		void SetVtkLookupTable(vtkLookupTable *pLut);
		vtkLookupTable *GetVtkLookupTable() const;

	private:
		vtkLookupTable *m_pLut;
	};

	virtual VflStatic2DHistogramPlotProperties*
		GetProperties() const;

private:
    /**
     * Compute (or update) the object's boundaries.
     */    
	virtual bool ComputeBounds();
	//void ComputeMinMaxYValues();

	virtual VflVisObjProperties *CreateProperties() const;

	// member attributes
	VveTimeHistogram	*m_pData;

	/**
	 * to be rendered using GL_QUADS
	 *
	 *    v3        v2            
	 *     x--------x   
	 *     |        |    
	 *     x--------x
	 *    v0        v1   
	 */
	struct sQuadPoints
	{
		VistaVector3D v0;
		VistaVector3D v1;
		VistaVector3D v2;
		VistaVector3D v3;
		sQuadPoints(VistaVector3D _v0, VistaVector3D _v1,
					VistaVector3D _v2, VistaVector3D _v3)
					: v0(_v0), v1(_v1), v2(_v2), v3(_v3)
		{}
		~sQuadPoints(){}
	};
	std::vector<sQuadPoints>		m_vecQuads;
	struct sColor
	{
		float fRed;
		float fGreen;
		float fBlue;
		sColor(float fR, float fG, float fB)
			: fRed(fR), fGreen(fG), fBlue(fB)
		{}
		~sColor(){}
	};
	std::vector<sColor>	m_vecColors;

	bool m_bNeedUpdate;

	float	m_fPlotCanvasXmin, 
			m_fPlotCanvasXmax,
			m_fPlotCanvasYmin,
			m_fPlotCanvasYmax;

	bool m_bBoundsValid; 

	double      m_dLastDrawSimTime,
		        m_dLastDrawTransSimTime;
	
	VistaVector3D m_v3BoundsMin, 
		           m_v3BoundsMax;
	float			m_fDepth;

	float	m_fZoomXmin,
			m_fZoomXmax,
			m_fZoomYmin,
			m_fZoomYmax;

	void ClipXCoord(float &fX);
	void ClipYCoord(float &fY);

	//float	m_fXValueMin, 
	//		m_fXValueMax,
	//		m_fYValueMin,
	//		m_fYValueMax;
};

/*============================================================================*/
/* INLINE METHODS                                                             */
/*============================================================================*/



/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLSTATICRANGEPLOT_H)
