/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflVisObjectComposition.h"
//#include "VflVisController.h"

#include <iostream>

#include <VistaBase/VistaVectorMath.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflVisObjectComposition::VflVisObjectComposition()
: IVflRenderable(),
  m_iObjectCount(0)
{
}

VflVisObjectComposition::~VflVisObjectComposition()
{
#ifdef DEBUG
    if (m_iObjectCount)
    {
        vstr::warnp() << " [VisObjComp] composition is not empty during destruction..." << endl;
    }
#endif
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddVisObject                                                */
/*                                                                            */
/*============================================================================*/
int VflVisObjectComposition::AddVisObject(IVflRenderable *pVisObject)
{
    if (pVisObject)
    {
        if (m_iObjectCount>=int(m_vecVisObjects.size()))
            m_vecVisObjects.resize(m_iObjectCount+1);
        m_vecVisObjects[m_iObjectCount] = pVisObject;

        ++m_iObjectCount;
    }

    return m_iObjectCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemoveVisObject                                             */
/*                                                                            */
/*============================================================================*/
int VflVisObjectComposition::RemoveVisObject(IVflRenderable *pVisObject)
{
    int iIndex = GetVisObjectIndex(pVisObject);

    if (iIndex >= 0)
    {
        RemoveVisObject(iIndex);
    }

    return m_iObjectCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemoveVisObject                                             */
/*                                                                            */
/*============================================================================*/
int VflVisObjectComposition::RemoveVisObject(int iIndex)
{
    if (iIndex>=0 && iIndex<m_iObjectCount)
    {
        int i;
        for (i=iIndex; i<m_iObjectCount-1; ++i)
        {
            m_vecVisObjects[i] = m_vecVisObjects[i+1];
        }
        m_vecVisObjects[i] = NULL;
    }

    --m_iObjectCount;

    return m_iObjectCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumberOfVisObjects                                       */
/*                                                                            */
/*============================================================================*/
int VflVisObjectComposition::GetNumberOfVisObjects() const
{
    return m_iObjectCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisObject                                                */
/*                                                                            */
/*============================================================================*/
IVflRenderable *VflVisObjectComposition::GetVisObject(int iIndex) const
{
    if (iIndex>=0 && iIndex<m_iObjectCount)
    {
        return m_vecVisObjects[iIndex];
    }

    return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisObjectIndex                                           */
/*                                                                            */
/*============================================================================*/
int VflVisObjectComposition::GetVisObjectIndex(IVflRenderable *pVisObject) const
{
    int i;
    for (i=0; i<m_iObjectCount; ++i)
    {
        if (m_vecVisObjects[i] == pVisObject)
        {
            return i;
        }
    }

    return -1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ProcessCommand                                              */
/*                                                                            */
/*============================================================================*/
//void VflVisObjectComposition::ProcessCommand(VflVOCEvent *pEvent)
//{
//    // distribute the command to all registered sub-objects
//    int i;
//    for (i=0; i<m_iObjectCount; ++i)
//    {
//        m_vecVisObjects[i]->ProcessCommand(pEvent);
//    }
//}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VflVisObjectComposition::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
    bool bBoundsValid = false;
    int i;
    for (i=0; i<m_iObjectCount; ++i)
    {
        if (bBoundsValid)
        {
            VistaVector3D vecMin, vecMax;
            bool bTemp = m_vecVisObjects[i]->GetBounds(vecMin, vecMax);

            if (bTemp)
            {
                int j;
                for (j=0; j<3; ++j)
                {
                    if (vecMin[j]<minBounds[j])
                        minBounds[j] = vecMin[j];
                    if (vecMax[j]>maxBounds[j])
                        maxBounds[j] = vecMax[j];
                }
            }
        }
        else
        {
            bBoundsValid = m_vecVisObjects[i]->GetBounds(minBounds, maxBounds);
        }
    }

	return bBoundsValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVisObjectComposition::GetType() const
{
    return IVflRenderable::GetType()+"::VflVisObjectComposition";
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflVisObjectComposition::Debug(std::ostream & out) const
{
	out << " [VflVisObjComp] Type:                  " << GetType() << endl;
	out << " [VflVisObjComp] Name:                  " << GetNameForNameable() << endl;
    out << " [VflVisObjComp] Number of sub-objects: " << m_iObjectCount << endl;
    out << " [VflVisObjComp] Sub-object information <start> " << endl;

    int i;
    for (i=0; i<m_iObjectCount; ++i)
    {
        m_vecVisObjects[i]->Debug(out);
    }

    out << " [VflVisObjComp] Sub-object information <finish> " << endl;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflVisObjectComposition.cpp"                                               */
/*============================================================================*/
