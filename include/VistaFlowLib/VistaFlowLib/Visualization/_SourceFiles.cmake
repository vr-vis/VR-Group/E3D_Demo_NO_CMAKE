

set( RelativeDir "./Visualization" )
set( RelativeSourceGroup "Source Files\\Visualization" )
set( SubDirs Particle Stats Volume Geometry )

set( DirFiles
	IVflTransformer.cpp
	IVflTransformer.h
	Vfl2DTextureGenerator.cpp
	Vfl2DTextureGenerator.h
	VflLookupTable.cpp
	VflLookupTable.h
	VflLookupTexture.cpp
	VflLookupTexture.h
	VflRenderNode.cpp
	VflRenderNode.h
	VflRenderOrthoProjToTexture.cpp
	VflRenderOrthoProjToTexture.h
	VflRenderToTexture.cpp
	VflRenderToTexture.h
	VflRenderable.cpp
	VflRenderable.h
	VflSystemAbstraction.cpp
	VflSystemAbstraction.h
	VflUnsteadyDataFilter.cpp
	VflUnsteadyDataFilter.h
	VflUnsteadyDataFilterGlyph.cpp
	VflUnsteadyDataFilterGlyph.h
	VflUnsteadyDataFilterNormals.cpp
	VflUnsteadyDataFilterNormals.h
	VflUnsteadyLUT.cpp
	VflUnsteadyLUT.h
	VflVisController.cpp
	VflVisController.h
	VflVisObject.cpp
	VflVisObject.h
	VflVisObjectComposition.cpp
	VflVisObjectComposition.h
	VflVisTiming.cpp
	VflVisTiming.h
	VflVisVBOVtkGeometry.cpp
	VflVisVBOVtkGeometry.h
	VflVisVtkGeometry.cpp
	VflVisVtkGeometry.h
	VflVtkLookupTable.cpp
	VflVtkLookupTable.h
	VflVtkProperty.cpp
	VflVtkProperty.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()

