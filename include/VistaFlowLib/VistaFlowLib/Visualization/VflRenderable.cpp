/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflRenderable.h"

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaBase/VistaStreamUtils.h>
#include <VistaMath/VistaBoundingBox.h>

#include <iostream>

using namespace std;


/*============================================================================*/
/* GLOBAL FUNCTIONS                                                           */
/*============================================================================*/
ostream& operator<<(ostream& out, const IVflRenderable& object)
{
    object.Debug(out);
    return out;
}


/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
IVflRenderable::IVflRenderable()
:	m_iId(-1)
,   m_pProperties(NULL)
,	m_pRenderNode(NULL)
{ }

IVflRenderable::~IVflRenderable()
{
	vstr::debugi() << "[IVflRenderable] Destroying IVflRenderable: "
		<< m_strName << "("  << GetNameableId() << ")." << endl;
	DeleteProperties(m_pProperties);
}


bool IVflRenderable::Init()
{
	if(m_pProperties)
		return true; // already initialized

	// factory method 
	m_pProperties = CreateProperties();
	if(m_pProperties)
	{
		Observe(m_pProperties, E_PROP_TICKET);
		m_pProperties->SetParentRenderable(this);
		return true;
	}
	return false;
}


bool IVflRenderable::GetBounds(VistaVector3D &v3MinBounds,
	VistaVector3D &v3MaxBounds)
{
	return false;
}

bool IVflRenderable::GetBounds(VistaBoundingBox &oBounds) 
{
	VistaVector3D min, max;
	if(GetBounds(min, max))
	{
		oBounds.SetBounds(min, max);
		//oBounds.Include(&min[0]);
		//oBounds.Include(&max[0]);
		return true;
	}
	return false;
}


void IVflRenderable::SetVisible(bool bVisible)
{
	if(m_pProperties)
		m_pProperties->SetVisible(bVisible);
}

bool IVflRenderable::GetVisible() const         
{ 
	if(m_pProperties)
		return m_pProperties->GetVisible();
	return false;
}


void IVflRenderable::Debug(std::ostream & out) const
{
	out << " [VflVisObject] Type:        " << GetType() << endl;
	out << " [VflVisObject] Name:        " << GetNameForNameable() << endl;
	out << " [VflVisObject] RenderNode:  " << m_pRenderNode << endl;
	out << " [VflVisObject] Id:          " << m_iId << endl;
	//out << " [VflVisObject] Visible:     " << (m_bVisible ? "true" : "false") 
	//	<< endl;

	if(m_pProperties)
	{
		VistaPropertyList p;
		m_pProperties->GetPropertiesByList(p);
		p.Print(out, 0);
	}

}


std::string IVflRenderable::GetType() const
{
	return string("IVflRenderable");
}


void IVflRenderable::Update()
{ }

void IVflRenderable::DrawOpaque()
{ }

void IVflRenderable::DrawTransparent()
{ }

void IVflRenderable::Draw2D()
{ }


void IVflRenderable::SetRenderNode(VflRenderNode *pRenderNode)
{
	m_pRenderNode = pRenderNode;
}

IVflRenderable::VflRenderableProperties *IVflRenderable::GetProperties() const
{
	return m_pProperties;
}


std::string IVflRenderable::GetNameForNameable() const
{
    return m_strName;
}

void IVflRenderable::SetNameForNameable(const std::string &strNewName)
{
    m_strName = strNewName;
}


bool IVflRenderable::DeleteProperties(VflRenderableProperties* pProps) const
{
	delete pProps;
	return true;
}


void IVflRenderable::ObserverUpdate(IVistaObserveable* pObserveable, 
	int iMsg, int iTicket)
{ }


//==============================================================================
REFL_IMPLEMENT_FULL(IVflRenderable::VflRenderableProperties, IVistaReflectionable)

namespace
{

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, IVflRenderable::VflRenderableProperties,
		VistaProperty::PROPT_BOOL>
			("VISIBLE", SsReflectionName,
			&IVflRenderable::VflRenderableProperties::GetVisible),
	new TVistaPropertyGet<string, IVflRenderable::VflRenderableProperties,
		VistaProperty::PROPT_STRING>
			("VISOBJNAME", SsReflectionName,
			&IVflRenderable::VflRenderableProperties::GetRenderableName),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool, bool,
		IVflRenderable::VflRenderableProperties>
			("VISIBLE", SsReflectionName,
			&IVflRenderable::VflRenderableProperties::SetVisible),
	NULL
};

}


IVflRenderable::VflRenderableProperties::VflRenderableProperties()
:	m_pParentObject(NULL)
,	m_bVisible(true)
{ }

IVflRenderable::VflRenderableProperties::~VflRenderableProperties()
{
	if(m_pParentObject)
		m_pParentObject->m_pProperties = NULL;
}


IVflRenderable *IVflRenderable::VflRenderableProperties::
	GetParentRenderable() const
{
	return m_pParentObject;
}

void IVflRenderable::VflRenderableProperties::
	SetParentRenderable(IVflRenderable *pParent)
{
	if(compAndAssignFunc(pParent, m_pParentObject) == 1)
		Notify(MSG_PARENT_CHANGED);
}


bool IVflRenderable::VflRenderableProperties::GetVisible() const
{
	return m_bVisible;
}

bool IVflRenderable::VflRenderableProperties::SetVisible(bool bVisible)
{
	if(compAndAssignFunc(bVisible, m_bVisible) == 1)
	{
		Notify(MSG_VISIBILITY);
		return true;
	}
	return false;
}


string IVflRenderable::VflRenderableProperties::GetRenderableName() const
{
	if(m_pParentObject)
		return m_pParentObject->GetNameForNameable();
	return "<null>";
}


/*============================================================================*/
/*  END OF FILE				                                                  */
/*============================================================================*/
