/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  BaseAppTutorial.h	         RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA FlowLib Tutorials      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                        Rheinisch-Westfaelische      */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/


#ifndef _VflLookupTable_h
#define _VflLookupTable_h

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <VistaBase/VistaColor.h>
#include <VistaAspects/VistaReflectionable.h>

#include <vector>

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI IVflLookupTable : public IVistaReflectionable
{
public:
	IVflLookupTable(){};
	virtual ~IVflLookupTable(){};

	enum
	{
		MSG_TABLE_RANGE_CHANGED = IVistaObserveable::MSG_LAST,
		MSG_NUMBER_OF_COLORS_CHANGED,
		MSG_VALUES_CHANGED,		
		MSG_HUE_RANGE_CHANGED,
		MSG_SATURATION_RANGE_CHANGED,
		MSG_VALUE_RANGE_CHANGED,
		MSG_ALPHA_RANGE_CHANGED,
		MSG_LAST
	};

	/**
	 * Set/Get the minimum/maximum scalar values for scalar mapping.
	 * Scalar values less than minimum range value are clamped to minimum 
	 * range value. Scalar values greater than maximum range value are 
	 * clamped to maximum range value.
	 */
	virtual bool SetTableRange( float  fMin, float  fMax ) = 0;
	virtual bool GetTableRange( float& fMin, float& fMax ) const = 0;

	/**
	 * Set/Get the Hue-Range that is used for automatic generation.
	 *
	 * If the Hue-Range changes, UpdateTableValues() 
	 * will be called to  update the LookupTable based on 
	 * the Hue-, Saturation-, Value-, and Alpha-Range.
	 */
	virtual bool SetHueRange( float  fMin, float  fMax ) = 0;
	virtual bool GetHueRange( float& fMin, float& fMax ) const = 0;
	
	/**
	 * Set/Get the Saturation-Range that is used for automatic generation.
	 *
	 * If the Saturation-Range changes, UpdateTableValues() 
	 * will be called to  update the LookupTable based on 
	 * the Hue-, Saturation-, Value-, and Alpha-Range.
	 */
	virtual bool SetSaturationRange( float  fMin, float  fMax ) = 0;
	virtual bool GetSaturationRange( float& fMin, float& fMax ) const = 0;

	/**
	 * Set/Get the Value-Range that is used for automatic generation.
	 *
	 * If the Value-Range changes, UpdateTableValues() 
	 * will be called to  update the LookupTable based on 
	 * the Hue-, Saturation-, Value-, and Alpha-Range.
	 */
	virtual bool SetValueRange( float  fMin, float  fMax ) = 0;
	virtual bool GetValueRange( float& fMin, float& fMax ) const = 0;

	/**
	 * Set/Get the Alpha-Range that is used for automatic generation.
	 *
	 * If the Alpha-Range changes, UpdateTableValues() 
	 * will be called to  update the LookupTable based on 
	 * the Hue-, Saturation-, Value-, and Alpha-Range.
	 */
	virtual bool SetAlphaRange( float  fMin, float  fMax ) = 0;
	virtual bool GetAlphaRange( float& fMin, float& fMax ) const = 0;

	/**
	 * Specify the number of values (i.e., colors) in the lookup table.
	 *
	 * If the number of values changes, the LookupTable will be updated
	 * based on the Hue-, Saturation-, Value-, and Alpha-Range.
	 */
	virtual bool SetValueCount( unsigned int iNumColors ) = 0;
	virtual unsigned int GetValueCount() const = 0;

	/**
	 * Set/Get all values (i.e., colors) in the lookup table.
	 */
	virtual bool SetTableValues( const std::vector<VistaColor>& vecValues ) = 0;
	virtual bool GetTableValues( std::vector<VistaColor>& vecValues ) const = 0;
	
	/**
	 * Set/Get all values (i.e., colors) in the lookup table.
	 */
	virtual bool SetTableValues( const std::vector<float>& vecValues ) = 0;
	virtual std::vector<float> GetTableValues() const = 0;

	/**
	* Set/Get a single value (i.e., color) in the lookup table.
	 */
	virtual bool SetTableValue( unsigned int nIndex, const VistaColor& rColor ) = 0;
	virtual bool GetTableValue( unsigned int nIndex, VistaColor& rColor ) const = 0;

	/**
	 * Map one value through the lookup table and return the color. 
	 */
	virtual VistaColor GetColor( float dScalar ) const = 0;

	/**
	 * Updates the LookupTable based on the Hue-, Saturation-, Value-, 
	 * and Alpha-Range.
	 */
	virtual void UpdateTableValues() = 0;

	REFL_DECLARE
};

class VISTAFLOWLIBAPI VflLookupTable : public IVflLookupTable
{
public:
	VflLookupTable();
	virtual ~VflLookupTable();

	virtual bool GetTableRange( float& dMin, float& dMax ) const;
	virtual bool SetTableRange( float  dMin, float  dMax );

	virtual bool GetHueRange( float& fMin, float& fMax ) const ;
	virtual bool SetHueRange( float fMin, float fMax );

	virtual bool GetSaturationRange( float& fMin, float& fMax ) const;
	virtual bool SetSaturationRange( float fMin, float fMax );

	virtual bool GetValueRange( float& fMin, float& fMax ) const;
	virtual bool SetValueRange( float fMin, float fMax );

	virtual bool GetAlphaRange( float& fMin, float& fMax ) const;
	virtual bool SetAlphaRange( float fMin, float fMax );

	virtual bool SetValueCount( unsigned int iNumColors );
	virtual unsigned int GetValueCount() const;

	virtual bool SetTableValues( const std::vector<VistaColor>& vecValues );
	virtual bool GetTableValues( std::vector<VistaColor>& vecValues ) const;

	virtual bool SetTableValues( const std::vector<float>& vecValues );
	virtual std::vector<float> GetTableValues() const;

	virtual bool SetTableValue( unsigned int nIndex, const VistaColor& rColor );
	virtual bool GetTableValue( unsigned int nIndex, VistaColor& rColor ) const;

	virtual VistaColor GetColor( float dScalar ) const;

	virtual void UpdateTableValues();

protected:
	float m_aTableRange[2];

	float m_aHueRange[2];
	float m_aSatRange[2];
	float m_aValRange[2];
	float m_aAlphaRange[2];

	std::vector<VistaColor> m_vecValues;
};

#endif
/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
