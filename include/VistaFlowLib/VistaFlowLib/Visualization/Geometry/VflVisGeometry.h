/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFL_VIS_GEOMETRY_H
#define VFL_VIS_GEOMETRY_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>
#include <string>
#include <map>
#include <vector>
#include <set>
#include <VistaVisExt/Data/VveVtkData.h>

#include <VistaFlowLib/Visualization/VflVisObject.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <VistaInterProcComm/Concurrency/VistaThreadPool.h>
#include <VistaInterProcComm/Concurrency/VistaThreadEvent.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
#define GETPOINTEROFFSET(ATTRIB,VBO) (((char*)(0))+(((ATTRIB)->iOffset)*((VBO)->iNumElements)))
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflGeometryRenderer;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflVisGeometry : public IVflVisObject//, public IVistaObserveable
{
public:
	struct VlfVisGeomBufferObject
	{
		GLuint	uiBufferID;
		int		iNumElements;
		int		iSize;
	};

	struct VlfVisGeomAttribute
	{
		enum eType{
			TYPE_FLOAT  = 0,
			TYPE_DOUBLE = 1,
			TYPE_INT    = 2
		};

		std::string strName;
		GLuint		iComponents;
		int			iOffset; // this value times the number of elements of a VBO
							 // represents the offset to the first element of
							 // the very next attribute's array
		eType		eType;
	};

	/**************************************************************************/
	/* CONSTRUCTORS / DESTRUCTOR                                              */
	/**************************************************************************/
	VflVisGeometry(unsigned int iCashSize = 0);
	virtual ~VflVisGeometry();
	
	/**************************************************************************/
	/* GETTER                                                                 */
	/**************************************************************************/
	int GetNumAttribute() const;

	const VlfVisGeomAttribute* GetAttributeByIndex(int iIndex) const;
	const VlfVisGeomAttribute* GetAttributeByName(const std::string& strName) const;
	const VlfVisGeomBufferObject* GetVertexBufferObject() const;
	const VlfVisGeomBufferObject* GetIndexBufferObject() const;

	void AddAttributeToBlackList(const std::string& strAttribName);
	bool RemoveAttributeFromBlackList(const std::string& strAttribName);

	bool HasValidData() const;

	/**************************************************************************/
	/* VISGEOMETRY API                                                        */
	/**************************************************************************/
	VveTimeMapper*			GetTimeMapper() const;

	VveUnsteadyVtkPolyData* GetUnsteadyData();
	virtual bool			SetUnsteadyData(VveUnsteadyData *pD);

	bool AddRenderer(VflGeometryRenderer *pRenderer);
	bool RemoveRenderer(VflGeometryRenderer *pRenderer);

	size_t					GetNumberOfRenderers() const;
	VflGeometryRenderer*	GetRenderer(int iIndex) const;
	int						GetCurrentRenderer() const;
	bool					SetCurrentRenderer(int iIndex);

	void RefreshData();

	/**************************************************************************/
	/* PROPERTES                                                              */
	/**************************************************************************/
	class VISTAFLOWLIBAPI VflVisGeometryProperties 
		: public IVflVisObject::VflVisObjProperties
	{
	public:
		VflVisGeometryProperties();
		virtual ~VflVisGeometryProperties();

		virtual std::string GetReflectionableType() const;

	protected:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		friend class VflVisGeometry;
	};

	/**************************************************************************/
	/* RENDERABLE API                                                         */
	/**************************************************************************/
	virtual bool Init();
	virtual void Update();
	virtual void DrawOpaque();
	virtual void DrawTransparent();
	virtual unsigned int GetRegistrationMode() const;

	virtual std::string GetType() const;
	virtual void		Debug(std::ostream & out) const;

	virtual bool GetBounds(VistaVector3D &v3Min, VistaVector3D &v3Max);

	virtual VflVisGeometryProperties* GetProperties() const;

	virtual VflVisObjProperties*      CreateProperties() const;

	/**************************************************************************/
	/* OBSERVER API                                                           */
	/**************************************************************************/
	virtual void ObserverUpdate(IVistaObserveable *pObs, int msg, int ticket);

protected:

	class VflVisGeomTimeStep
	{
	public: 
		VflVisGeomTimeStep(VflVisGeometry* pParent);
		~VflVisGeomTimeStep();

		bool MapAndResizeVertexBuffer(int iNumVertices);
		bool MapAndResizeIndexBuffer(int iNumIndices);

		bool PushVertices(vtkPolyData *pPolyData);
		bool PushIndices(vtkPolyData *pPolyData);

		void UnmapVertexBuffer();
		void UnmapIndexBuffer();

		const VlfVisGeomBufferObject* GetVBO();
		const VlfVisGeomBufferObject* GetIBO();

	private:
		VlfVisGeomBufferObject	m_oVBO;
		VlfVisGeomBufferObject	m_oIBO;

		char*			m_pVertexBuffer;
		unsigned int*	m_pIndexBuffer;

		VflVisGeometry* m_pParent;
	};

	class VflVisGeomWorkInstance : public IVistaThreadPoolWorkInstance
	{
	public:
		VflVisGeomWorkInstance(VflVisGeometry *pParent);
		virtual ~VflVisGeomWorkInstance();

		// selectors to modify the conditions
		void SetPolyData(VveDataItem<vtkPolyData>* pPolyData);
		void SetTimeStep(VflVisGeomTimeStep*	   pTimeStep);
		void SetLevelIndex(int iLevelIndex);
		void SetDoCleanUp(bool b);

		int	 GetLevelIndex();
		bool GetDoCleanUp();

		VflVisGeomTimeStep* GetTimeStep();
		VistaThreadEvent*	GetThreadEvent();
	
	protected:
		// interface
		virtual void	DefinedThreadWork();
	private:
		// variables
		VistaThreadEvent*			m_pThreadEvent;
		VflVisGeometry*				m_pParent;
		VflVisGeomTimeStep*			m_pTimeStep;
		VveDataItem<vtkPolyData>*	m_pPolyData;
		int							m_iLevelIndex;
		bool						m_bDoCleanUp;
	};

	void UpdateAttributeMap();
	void UpdateBufferObjects();

	bool PrepareAsyncFill(int iTimeLevel, VflVisGeomWorkInstance* pTatget);
	bool CleanUpAsyncFill(VflVisGeomWorkInstance* pTarget);

	static int CountIndices(vtkPolyData* pPolyData);
private:
	typedef std::vector<VlfVisGeomAttribute> ATTRIBUTEVECTOR;

	std::map<std::string, int>	m_mapAttribMap;
	ATTRIBUTEVECTOR				m_vecAttributes;
	std::set<std::string>		m_setAttribBlackList;
	int							m_iVertexSize;

	std::vector<VflVisGeomTimeStep*>		m_vecTimeSteps;
	std::vector<VflVisGeomWorkInstance*>	m_vecWorkInstances;
	int										m_iNumWorkInstances;
	unsigned int							m_uiDrawIndex;
	int										m_iLookAheadIndex;

	VveUnsteadyVtkPolyData*		m_pData;

	std::vector<VflGeometryRenderer*>	m_vecRenderers;
	int									m_iCurrentRenderer;

	static VistaThreadPool*	m_pThreadPool;
	static int				m_iThreadPoolRefCount;

	double	m_dDataTimeStamp;
	double	m_dVisParamsTimeStamp;
	int		m_iCurrentLevelIndex;

	bool    m_bBoundsDirty;
	bool	m_bAttribMapDirty;
	bool	m_bDataUpdated;

	float   m_afMinBounds[3];
	float   m_afMaxBounds[3];

	float	m_aCurTrans[16];

	friend class VflVisGeomTimeStep;
};
#endif // Include guard.
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
