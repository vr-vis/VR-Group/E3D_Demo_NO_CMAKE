/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFL_GEOMETRY_DATA_CONVERTER_H
#define VFL_GEOMETRY_DATA_CONVERTER_H

#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <vector>

class vtkPolyData;
class vtkAbstractArray;
class VistaGeometryData;

class VISTAFLOWLIBAPI VflVtkPolyDataConverter
{
public:
	VflVtkPolyDataConverter();
	virtual ~VflVtkPolyDataConverter();

	/**
	 * Converts a vtkPolyData-Object.
	 * GL_VERTEX_ATTRIB_ARRAY0 will contain Points
	 * GL_VERTEX_ATTRIB_ARRAY1 will contain Normals,  if available
	 * GL_VERTEX_ATTRIB_ARRAY2 will contain TexCoord, if available
	 * GL_VERTEX_ATTRIB_ARRAY3 will contain Scalars,  if available
	 */
	virtual VistaGeometryData* ConvertData( vtkPolyData* pDataIn );

protected:
	virtual void PushVertices();
	virtual void PushIndices();

	void PushAttribute( vtkAbstractArray* pDataIn, unsigned int uiNumVerts, unsigned int uiChannel );

	void PushPolygons( std::vector<unsigned int>& vecIndices );
	void PushStrips( std::vector<unsigned int>& vecIndices );
	void PushLines( std::vector<unsigned int>& vecIndices );

protected:
	vtkPolyData*		m_pCurrentData;
	VistaGeometryData*	m_pConvertedData;
};

#endif // Include guard.
