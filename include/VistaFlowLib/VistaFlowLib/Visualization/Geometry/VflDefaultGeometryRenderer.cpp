/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VflDefaultGeometryRenderer.h"
#include "VflVisGeometry.h"

#include <VistaOGLExt/VistaTexture.h>

#include <VistaBase/VistaStreamUtils.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflDefaultGeometryRenderer::VflDefaultGeometryRenderer(VflVisGeometry *pOwner)
	:	VflGeometryRenderer(pOwner)
	,	m_iDrawMode(DM_SOLID_COLOR)
	,	m_oColor(VistaColor::RED)
	,	m_pTexture(NULL)
	,	m_v3ScalarMin(0.0f, 0.0f, 0.0f)
	,	m_v3ScalarMax(1.0f, 1.0f, 1.0f)
	,	m_strPointesAttribName("Points")
	,	m_strNormalsAttribName("Normals")
	,	m_strScalarAttribName("Scalars")
	,	m_bIsLightingEnabled(false)
	,	m_bIsFaceCullingEnabled(false)
{ }

VflDefaultGeometryRenderer::~VflDefaultGeometryRenderer()
{ }

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
void VflDefaultGeometryRenderer::SetDrawMode(int iMode)
{
	m_iDrawMode = iMode;
}

int VflDefaultGeometryRenderer::GetDrawMode() const
{
	return m_iDrawMode;
}

int VflDefaultGeometryRenderer::GetDrawModeCount() const
{
	return DM_COUNT;
}

/*============================================================================*/
void VflDefaultGeometryRenderer::DrawOpaque()
{
	if(m_iDrawMode < 0 || m_iDrawMode >= GetDrawModeCount())
		return;

	const VflVisGeometry::VlfVisGeomBufferObject* pVBO;
	const VflVisGeometry::VlfVisGeomBufferObject* pIBO;

	const VflVisGeometry::VlfVisGeomAttribute* pPoints;
	const VflVisGeometry::VlfVisGeomAttribute* pNormals;
	const VflVisGeometry::VlfVisGeomAttribute* pScalars;

	pVBO = m_pOwner->GetVertexBufferObject();
	pIBO = m_pOwner->GetIndexBufferObject();

	pPoints  = m_pOwner->GetAttributeByName(m_strPointesAttribName);
	pNormals = m_pOwner->GetAttributeByName(m_strNormalsAttribName);
	pScalars = m_pOwner->GetAttributeByName(m_strScalarAttribName);

	if(!pVBO || !pIBO || !pPoints || !pNormals)
	{
		if (!pVBO)
			vstr::errp() <<"[VflDefaultGeometryRenderer] - No VBO!" << endl; 
		if (!pIBO)
			vstr::errp() <<"[VflDefaultGeometryRenderer] - No IBO!" << endl;
		if (!pPoints)
			vstr::errp() <<"[VflDefaultGeometryRenderer] - No Points given!" << endl;
		if (!pNormals)
			vstr::errp() <<"[VflDefaultGeometryRenderer] - No Normals given!" << endl;
		return;
	}
	if( pPoints->eType != VflVisGeometry::VlfVisGeomAttribute::TYPE_FLOAT ||
		pNormals->eType != VflVisGeometry::VlfVisGeomAttribute::TYPE_FLOAT )
	{
		vstr::errp() << "[VflDefaultGeometryRenderer] Type of Points/Normals must be FLOAT" << endl;
		return;
	}
	if(DM_SCALARS_AS_TEX_COORDS == m_iDrawMode)
	{
		if(!pScalars)
		{
			vstr::errp() << "[VflDefaultGeometryRenderer] Draw mode is"
				<< " DM_SCALARS_AS_TEX_CORDS, but no scalars are given." << endl;
			return;
		}
		if( pScalars->eType != VflVisGeometry::VlfVisGeomAttribute::TYPE_FLOAT )
		{
			vstr::errp() << "[VflDefaultGeometryRenderer] Type of Scalars must be FLOAT" << endl;
			return;
		}
		if(!m_pTexture)
		{
			vstr::errp() << "[VflDefaultGeometryRenderer] Draw mode is"
				<< " DM_SCALARS_AS_TEX_CORDS, but no texture is given." << endl;
			return;
		}
	}

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glBindBuffer(GL_ARRAY_BUFFER,         pVBO->uiBufferID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pIBO->uiBufferID);

	glVertexPointer(pPoints->iComponents, GL_FLOAT, 0, GETPOINTEROFFSET(pPoints, pVBO));
	glNormalPointer(GL_FLOAT, 0, GETPOINTEROFFSET(pNormals, pVBO));

	if(DM_SCALARS_AS_TEX_COORDS == m_iDrawMode)
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(pScalars->iComponents, GL_FLOAT, 0, GETPOINTEROFFSET(pScalars, pVBO));
			
		m_pTexture->Enable();
		m_pTexture->Bind();

		if(!m_bIsFaceCullingEnabled)
			glDisable(GL_CULL_FACE);
		else
			glEnable(GL_CULL_FACE);

		VistaVector3D v3ScalarSpread = (m_v3ScalarMax - m_v3ScalarMin);
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
		glScalef(1.0f/v3ScalarSpread[0], 1.0f/v3ScalarSpread[1], 1.0f/v3ScalarSpread[2]);
		glTranslated(m_v3ScalarMin[0], m_v3ScalarMin[1], m_v3ScalarMin[2]);
	}
	else
	{
		if(!m_bIsFaceCullingEnabled)
			glDisable(GL_CULL_FACE);
		else
			glEnable(GL_CULL_FACE);
		glColor3f(m_oColor[0], m_oColor[1], m_oColor[2]);
	}

	if(m_bIsLightingEnabled)
		glEnable(GL_LIGHTING);
	else
		glDisable(GL_LIGHTING);

	glDrawElements(GL_TRIANGLES, pIBO->iNumElements, GL_UNSIGNED_INT, (void*)(0));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	if(DM_SCALARS_AS_TEX_COORDS == m_iDrawMode)
	{
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);

		m_pTexture->Unbind();
		m_pTexture->Disable();

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

/*============================================================================*/

void VflDefaultGeometryRenderer::SetColor(const VistaColor& rColor)
{
	m_oColor = rColor;
}

const VistaColor& VflDefaultGeometryRenderer::GetColor() const
{
	return m_oColor;
}

void VflDefaultGeometryRenderer::SetTexture(VistaTexture* pTexture)
{
	m_pTexture = pTexture;
}

VistaTexture* VflDefaultGeometryRenderer::GetTexture()
{
	return m_pTexture;
}

void VflDefaultGeometryRenderer::SetScalarInterval(
	const VistaVector3D& v3Min, const VistaVector3D& v3Max)
{
	m_v3ScalarMin = v3Min;
	m_v3ScalarMax = v3Max;
}

void VflDefaultGeometryRenderer::GetScalarInterval(
	VistaVector3D& v3Min, VistaVector3D& v3Max) const
{
	v3Min = m_v3ScalarMin;
	v3Max = m_v3ScalarMax;
}
/*============================================================================*/


void VflDefaultGeometryRenderer::SetScalarName(const string& strScalarName)
{
	m_strScalarAttribName = strScalarName;
}

const string& VflDefaultGeometryRenderer::GetScalarName()const
{
	return m_strScalarAttribName;
}
/*============================================================================*/

const string& VflDefaultGeometryRenderer::GetNormalsAttribName()const
{
	return m_strNormalsAttribName;
}
const string& VflDefaultGeometryRenderer::GetPointesAttribName()const
{
	return m_strPointesAttribName;
}

void VflDefaultGeometryRenderer::SetPointesAttribName(const string& strName)
{
	m_strPointesAttribName = strName;
}

void VflDefaultGeometryRenderer::SetNormalsAttribName(const string& strName)
{
	m_strNormalsAttribName = strName;
}

/*============================================================================*/

bool VflDefaultGeometryRenderer::GetIsLightingEnabled()
{
	return m_bIsLightingEnabled;
}
void VflDefaultGeometryRenderer::SetIsLightingEnabled(bool b)
{
	m_bIsLightingEnabled = b;
}

/*============================================================================*/


bool VflDefaultGeometryRenderer::GetIsFaceCullingEnabled()
{
	return m_bIsFaceCullingEnabled;
}


void VflDefaultGeometryRenderer::SetIsFaceCullingEnabled( bool b )
{
	m_bIsFaceCullingEnabled = b;
}


/*============================================================================*/

std::string VflDefaultGeometryRenderer::GetType() const
{
	return VflGeometryRenderer::GetType() 
		+ std::string("::VflDefaultGeometryRenderer");
}


/*============================================================================*/
/*  END OF FILE "VflParticleRenderer.cpp"                                     */
/*============================================================================*/
