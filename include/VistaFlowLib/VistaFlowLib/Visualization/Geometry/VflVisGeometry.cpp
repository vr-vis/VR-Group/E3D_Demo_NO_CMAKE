/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VflVisGeometry.h"

#include "VflGeometryRenderer.h"

#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>

#include <VistaMath/VistaBoundingBox.h>
#include <VistaAspects/VistaConversion.h>
#include <VistaTools/VistaStreams.h>

#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/IVflTransformer.h>

#include <VistaVisExt/Data/VveVtkData.h>
#include <VistaOGLExt/VistaOGLUtils.h>

#include <cassert>

using namespace std;

VistaThreadPool*	VflVisGeometry::m_pThreadPool			= NULL;
int					VflVisGeometry::m_iThreadPoolRefCount	= 0;
/*============================================================================*/
/* VflVisGeometry                                                             */
/*============================================================================*/
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
VflVisGeometry::VflVisGeometry(unsigned int iCashSize)
	:	IVflVisObject()
	,	m_iVertexSize(0)
	,	m_iNumWorkInstances(iCashSize)
	,	m_uiDrawIndex(0)
	,	m_pData(NULL)
	,	m_iCurrentRenderer(-1)
	,	m_dDataTimeStamp(0.0)
	,	m_dVisParamsTimeStamp(0.0)
	,	m_iCurrentLevelIndex(-1)
	,	m_bBoundsDirty(true)
	,	m_bAttribMapDirty(true)
	,	m_bDataUpdated(true)
{
	m_afMinBounds[0] = m_afMinBounds[1] = m_afMinBounds[2] = 0.0f;
	m_afMaxBounds[0] = m_afMaxBounds[1] = m_afMaxBounds[2] = 0.0f;

	for (unsigned int i=0; i< iCashSize; ++i)
	{
		m_vecWorkInstances.push_back(new VflVisGeomWorkInstance(this));
	}

	if(m_iNumWorkInstances > 0)
	{
		if(m_iThreadPoolRefCount == 0)
		{
			m_pThreadPool = new VistaThreadPool(2, 100, "VisGeomWorker");
			m_pThreadPool->StartPoolWork();
		}
		++m_iThreadPoolRefCount;
	}
}

VflVisGeometry::~VflVisGeometry()
{
	for (size_t n=0; n < m_vecWorkInstances.size(); ++n)
	{
		CleanUpAsyncFill(m_vecWorkInstances[n]);
	}

	if(m_iNumWorkInstances > 0)
	{
		--m_iThreadPoolRefCount;
		if(m_iThreadPoolRefCount == 0)
		{
			m_pThreadPool->WaitForAllJobsDone();
			m_pThreadPool->StopPoolWork();
			delete m_pThreadPool;
			m_pThreadPool = NULL;
		}
	}

	for (size_t n=0; n < m_vecTimeSteps.size(); ++n)
	{
		delete m_vecTimeSteps[n];
	}
	for (size_t n=0; n < m_vecWorkInstances.size(); ++n)
	{
		delete m_vecWorkInstances[n];
	}
	for (size_t n=0; n < m_vecRenderers.size(); ++n)
	{
		delete m_vecRenderers[n];
	}
}

/******************************************************************************/
/* GETTER                                                                     */
/******************************************************************************/
int VflVisGeometry::GetNumAttribute() const
{
	return static_cast<int>(m_mapAttribMap.size());
}

/******************************************************************************/
const VflVisGeometry::VlfVisGeomAttribute* 
	VflVisGeometry::GetAttributeByIndex(int iIndex) const
{
	if (iIndex >= 0 && static_cast<size_t>(iIndex) < m_vecAttributes.size())
	{
		return &(m_vecAttributes[iIndex]);
	}
	else
	{
		return NULL;
	}

}
const VflVisGeometry::VlfVisGeomAttribute* 
	VflVisGeometry::GetAttributeByName(const std::string& strName) const
{
	std::map<std::string, int>::const_iterator it =
		m_mapAttribMap.find(strName);

	if(it != m_mapAttribMap.end())
		return GetAttributeByIndex(it->second);

	return NULL;
}

/******************************************************************************/
const VflVisGeometry::VlfVisGeomBufferObject* 
	VflVisGeometry::GetVertexBufferObject() const
{
	return m_vecTimeSteps[m_uiDrawIndex]->GetVBO();
}

const VflVisGeometry::VlfVisGeomBufferObject* 
	VflVisGeometry::GetIndexBufferObject() const
{
	return m_vecTimeSteps[m_uiDrawIndex]->GetIBO();
}

/******************************************************************************/
void VflVisGeometry::AddAttributeToBlackList(const string& strAttribName)
{
	m_setAttribBlackList.insert(strAttribName);
	m_bAttribMapDirty = true;
}

bool VflVisGeometry::RemoveAttributeFromBlackList(const string& strAttribName)
{
	if(m_setAttribBlackList.erase(strAttribName) == 1)
	{
		m_bAttribMapDirty = true;
		return true;
	}
	return false;
}
/******************************************************************************/
bool VflVisGeometry::HasValidData() const
{
	if(!m_pData)
		return false;

	VveDataItem<vtkPolyData>*  pDataItem =
		m_pData->GetTypedLevelDataByLevelIndex(0);


	if(!pDataItem || !pDataItem->GetData())
		return false;

	return true;
}

/******************************************************************************/
/* VISGEOMETRY API                                                            */
/******************************************************************************/
VveTimeMapper* VflVisGeometry::GetTimeMapper() const
{
	if(m_pData)
		return m_pData->GetTimeMapper();

	return NULL;
}

/******************************************************************************/
VveUnsteadyVtkPolyData* VflVisGeometry::GetUnsteadyData()
{
	return m_pData;
}

bool VflVisGeometry::SetUnsteadyData(VveUnsteadyData *pD)
{
	VveUnsteadyVtkPolyData *pData = 
		dynamic_cast<VveUnsteadyVtkPolyData*>(pD);

	if(!pData)
		return false; // reject, not for me...

	m_bDataUpdated    = true;

	if(!IVflVisObject::SetUnsteadyData(pD))
	{
		return false; // reject, something went wrong
	}

	m_pData = pData;
	m_bBoundsDirty    = true;
	m_bAttribMapDirty = true;

	return true; // ok, everything is fine
}

/******************************************************************************/
bool VflVisGeometry::AddRenderer(VflGeometryRenderer *pRenderer)
{
	if (!pRenderer)
		return false;

	m_vecRenderers.push_back(pRenderer);

	if (m_iCurrentRenderer<0)
		m_iCurrentRenderer = static_cast<int>(GetNumberOfRenderers()) - 1;

	return true;
}

bool VflVisGeometry::RemoveRenderer(VflGeometryRenderer *pRenderer)
{
	std::vector<VflGeometryRenderer*>::iterator it = 
		std::find(m_vecRenderers.begin(), m_vecRenderers.end(), pRenderer);

	if(it != m_vecRenderers.end())
	{
		// remove from vector
		m_vecRenderers.erase(it);

		m_iCurrentRenderer = m_vecRenderers.empty() ? -1 : 0;
		return true;
	}
	return false;
}

/******************************************************************************/
size_t VflVisGeometry::GetNumberOfRenderers() const
{
	return m_vecRenderers.size();
}

VflGeometryRenderer* VflVisGeometry::GetRenderer(int iIndex) const
{
	if (iIndex < 0 || static_cast<size_t>(iIndex) >= GetNumberOfRenderers())
		return NULL;

	return m_vecRenderers[iIndex];
}

/******************************************************************************/
int VflVisGeometry::GetCurrentRenderer() const
{
	return m_iCurrentRenderer;
}

bool VflVisGeometry::SetCurrentRenderer(int iIndex)
{
	if (iIndex<0 || static_cast<size_t>(iIndex)>=GetNumberOfRenderers())
		return false;

	m_iCurrentRenderer = iIndex;

	return true;
}

/******************************************************************************/
void VflVisGeometry::RefreshData()
{
	m_bAttribMapDirty = true;
	m_bBoundsDirty = true;
	m_bDataUpdated    = true;
}

/******************************************************************************/
/* RENDERABLE API                                                             */
/******************************************************************************/
bool VflVisGeometry::Init()
{
	if(IVflVisObject::Init() == false)
		return false;

	m_vecTimeSteps.push_back(new VflVisGeomTimeStep(this));
	for (int i=1; i < m_iNumWorkInstances; ++i)
	{
		m_vecTimeSteps.push_back(new VflVisGeomTimeStep(this));
	}
	for (int i=0; i < m_iNumWorkInstances; ++i)
	{
		m_vecWorkInstances[i]->SetTimeStep(m_vecTimeSteps[i]);
	}

	return true;
}

void VflVisGeometry::Update()
{
	if(m_iCurrentRenderer < 0 || !HasValidData())
		return;

	// no data in data object (empty)
	if (m_pData->GetTypedLevelDataByLevelIndex(0)->GetData() == NULL)
		return;

	if (m_pData->GetTypedLevelDataByLevelIndex(0)->GetData()->GetNumberOfPoints() == 0)
		return;

	if(m_bAttribMapDirty)
		UpdateAttributeMap();

	const double dVisTime =
		GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	const double dSimTime =
		m_pData->GetTimeMapper()->GetSimulationTime(dVisTime);

	int iLevelIndex = m_pData->GetTimeMapper()->GetLevelIndex(dVisTime);
	if(m_bDataUpdated || m_iCurrentLevelIndex!=iLevelIndex)
	{
		m_iCurrentLevelIndex = iLevelIndex;
		UpdateBufferObjects();
	}

	VistaTransformMatrix oCurTrans;
	if(NULL != GetTransformer())
	{
		GetTransformer()->GetUnsteadyTransform(dSimTime, oCurTrans);
	}
	else
	{
		oCurTrans.SetToIdentity();
	}
	oCurTrans.GetTransposedValues(m_aCurTrans);

	if(m_dDataTimeStamp == 0)
		m_dDataTimeStamp = GetRenderNode()->GetVisTiming()->GetLifeTime();

	if(m_dVisParamsTimeStamp == 0)
		m_dVisParamsTimeStamp = GetRenderNode()->GetVisTiming()->GetLifeTime();

	m_vecRenderers[m_iCurrentRenderer]->Update();
}

void VflVisGeometry::DrawOpaque()
{
	if(m_iCurrentRenderer<0 || !HasValidData())
		return;

	glPushMatrix();
	glMultMatrixf(m_aCurTrans);

	m_vecRenderers[m_iCurrentRenderer]->DrawOpaque();

	glPopMatrix();
}

void VflVisGeometry::DrawTransparent()
{
	if(m_iCurrentRenderer<0 || !HasValidData()) 
		return;

	glPushMatrix();
	glMultMatrixf(m_aCurTrans);

	m_vecRenderers[m_iCurrentRenderer]->DrawTransparent();

	glPopMatrix();
}

unsigned int VflVisGeometry::GetRegistrationMode() const
{
	return OLI_UPDATE | OLI_DRAW_OPAQUE | OLI_DRAW_TRANSPARENT;
}

/******************************************************************************/
string VflVisGeometry::GetType() const
{
	return IVflVisObject::GetType()+string("::VflVisGeometry");
}

void VflVisGeometry::Debug(ostream & out) const
{
	IVflVisObject::Debug(out);

	out << " [VflVisGeometry] Data time stamp:          " 
		<< m_dDataTimeStamp << endl;
	out << " [VflVisGeometry] Vis parameter time stamp: " 
		<< m_dVisParamsTimeStamp << endl;
	out << " [VflVisGeometry] NumberOfRenderers:        " 
		<< GetNumberOfRenderers() << endl;

	if (m_iCurrentRenderer >= 0)
	{
		out << " [VflVisGeometry] Active renderer:" << endl;
		m_vecRenderers[m_iCurrentRenderer]->Debug(out);
	}
}

/******************************************************************************/
bool VflVisGeometry::GetBounds(VistaVector3D& v3Min, VistaVector3D& v3Max)
{
	if(!m_bBoundsDirty)
	{
		// copy to target vectors
		v3Min[0] = m_afMinBounds[0];
		v3Min[1] = m_afMinBounds[1];
		v3Min[2] = m_afMinBounds[2];

		v3Max[0] = m_afMaxBounds[0];
		v3Max[1] = m_afMaxBounds[1];
		v3Max[2] = m_afMaxBounds[2];
	}
	else
	{
		// recompute bounds
		if(!m_pData) // do we have data?
		{	// no, empty bounds
			m_afMinBounds[0] = m_afMinBounds[1] = m_afMinBounds[2] = 0.0f;
			m_afMaxBounds[0] = m_afMaxBounds[1] = m_afMaxBounds[2] = 0.0f;
		}
		else
		{
			VistaBoundingBox b;
			for (int i = 0; i < m_pData->GetNumberOfLevels(); ++i)
			{
				VveDataItem<vtkPolyData>*  pPolydata =
					m_pData->GetTypedLevelDataByLevelIndex(i);

				double adBounds[6];
				pPolydata->GetData()->GetBounds(adBounds);

				float afMin[3]={adBounds[0],adBounds[2],adBounds[4]};
				float afMax[3]={adBounds[1],adBounds[3],adBounds[5]};

				if(i!=0)
					b.Include(VistaBoundingBox(afMin,afMax));
				else 
					b.SetBounds(afMin,afMax);
			}

			v3Min[0] = m_afMinBounds[0] = b.m_v3Min[0];
			v3Min[1] = m_afMinBounds[1] = b.m_v3Min[1];
			v3Min[2] = m_afMinBounds[2] = b.m_v3Min[2];

			v3Max[0] = m_afMaxBounds[0] = b.m_v3Max[0];
			v3Max[1] = m_afMaxBounds[1] = b.m_v3Max[1];
			v3Max[2] = m_afMaxBounds[2] = b.m_v3Max[2];
		}
		m_bBoundsDirty = false;
	}

	return true;
}

/******************************************************************************/
VflVisGeometry::VflVisGeometryProperties *VflVisGeometry::GetProperties() const
{
	return static_cast<VflVisGeometryProperties*>(
		IVflRenderable::GetProperties());
}

IVflVisObject::VflVisObjProperties* VflVisGeometry::CreateProperties() const
{
	return new VflVisGeometryProperties;
}
/******************************************************************************/
/* OBSERVER API                                                               */
/******************************************************************************/
void VflVisGeometry::ObserverUpdate(IVistaObserveable *pObs, int msg, int ticket)
{
	IVflVisObject::ObserverUpdate(pObs, msg, ticket);
}

/******************************************************************************/
/* PROTECTED INTERFACE                                                        */
/******************************************************************************/
void VflVisGeometry::UpdateAttributeMap()
{
	m_mapAttribMap.clear();
	m_vecAttributes.clear();
	m_iVertexSize = 0;

	VveDataItem<vtkPolyData>*  pDataItem =
		m_pData->GetTypedLevelDataByLevelIndex(0);

	vtkPolyData*	pPolyData		= pDataItem->GetData();

	std::cout << "UpdateAttributeMap" << std::endl;

	if (pPolyData == NULL)
		return;

	std::cout << "     NOT NULL" << std::endl;

	vtkPointData*	pPointData		= pPolyData->GetPointData();
	int				iNumAttributes	= pPointData->GetNumberOfArrays();
	m_vecAttributes.reserve(iNumAttributes + 1);

	vtkAbstractArray* pDataArray = pPolyData->GetPoints()->GetData();

	VlfVisGeomAttribute oAttrib;

	oAttrib.strName		= pDataArray->GetName();
	oAttrib.iComponents	= pDataArray->GetNumberOfComponents();
	oAttrib.iOffset		= m_iVertexSize;
	oAttrib.eType       = VlfVisGeomAttribute::TYPE_FLOAT;

	m_vecAttributes.push_back(oAttrib);
	m_mapAttribMap[oAttrib.strName] = 0;
	m_iVertexSize+= oAttrib.iComponents*pDataArray->GetDataTypeSize();
	for (int i=0; i<iNumAttributes; ++i)
	{
		pDataArray = pPointData->GetAbstractArray(i);

		oAttrib.strName		= pDataArray->GetName();
		oAttrib.iComponents	= pDataArray->GetNumberOfComponents();
		oAttrib.iOffset		= m_iVertexSize;
		switch( pDataArray->GetDataType() )
		{
		case VTK_FLOAT:  oAttrib.eType = VlfVisGeomAttribute::TYPE_FLOAT;  break;
		case VTK_DOUBLE: oAttrib.eType = VlfVisGeomAttribute::TYPE_DOUBLE; break;
		case VTK_INT:    oAttrib.eType = VlfVisGeomAttribute::TYPE_INT;    break;
		default: 
			vstr::errp() << "[VflVisGeometry] type \""
				<< pDataArray->GetDataTypeAsString()
				<< "\" of Attribute \""
				<< oAttrib.strName
				<< "\" is currently not supported!" << endl;
			vstr::erri() << "Supported types are: VTK_FLOAT, VTK_DOUBLE, VTK_INT" << endl;
			continue;
		}

		std::set<std::string>::iterator it = 
			m_setAttribBlackList.find(oAttrib.strName);

		if(it == m_setAttribBlackList.end())
		{
			m_vecAttributes.push_back(oAttrib);
			m_mapAttribMap[oAttrib.strName] =
				static_cast<int>(m_vecAttributes.size()) - 1;
			m_iVertexSize+= oAttrib.iComponents*pDataArray->GetDataTypeSize();
		}
	}

	m_bAttribMapDirty = false;
}

/******************************************************************************/
void VflVisGeometry::UpdateBufferObjects()
{
	m_bDataUpdated = false;

	if(m_iNumWorkInstances ==0)
	{
		m_uiDrawIndex = 0;

		vtkPolyData *pData = 
			m_pData->GetTypedLevelDataByLevelIndex(m_iCurrentLevelIndex)->GetData();

		if(!pData) return;

		int iNumVertices = pData->GetNumberOfPoints();
		int iNumIndices  = CountIndices(pData);

		if(m_vecTimeSteps[0]->MapAndResizeVertexBuffer(iNumVertices))
		{
			m_vecTimeSteps[0]->PushVertices(pData);
			m_vecTimeSteps[0]->UnmapVertexBuffer();
		}

		if(m_vecTimeSteps[0]->MapAndResizeIndexBuffer(iNumIndices))
		{
			m_vecTimeSteps[0]->PushIndices(pData);
			m_vecTimeSteps[0]->UnmapIndexBuffer();
		}
	}
	else
	{
		int iCacheMisses	= 0;
		int iIndex			= m_uiDrawIndex;

		for(int i=0; i < m_iNumWorkInstances; ++i)
		{
			if(m_vecWorkInstances[m_uiDrawIndex]->GetLevelIndex() == m_iCurrentLevelIndex)
				break;

			++m_uiDrawIndex;
			m_uiDrawIndex %= m_vecWorkInstances.size();

			++iCacheMisses;
		}

		vstr::debugi() << "[VflVisGeometry] CacheMisses: " << iCacheMisses << endl;
		if(iCacheMisses == m_iNumWorkInstances)
			m_iLookAheadIndex = m_iCurrentLevelIndex;

		for(int i=0; i < iCacheMisses; ++i)
		{
			CleanUpAsyncFill(m_vecWorkInstances[iIndex]);
			PrepareAsyncFill(m_iLookAheadIndex, m_vecWorkInstances[iIndex]);

			if(m_iLookAheadIndex == m_iCurrentLevelIndex)
				m_vecWorkInstances[iIndex]->ThreadWork();
			else
				m_pThreadPool->AddWork(m_vecWorkInstances[iIndex]);

			++m_iLookAheadIndex;
			m_iLookAheadIndex %= m_pData->GetNumberOfLevels();


			++iIndex;
			iIndex %= m_iNumWorkInstances;
		}

		CleanUpAsyncFill(m_vecWorkInstances[m_uiDrawIndex]);
	}
}

/******************************************************************************/
bool VflVisGeometry::PrepareAsyncFill(int iTimeLevel,
		VflVisGeometry::VflVisGeomWorkInstance* pTatget)
{
	VveDataItem<vtkPolyData>* pDataItem = 
		m_pData->GetTypedLevelDataByLevelIndex(iTimeLevel);

	int iNumVertices = pDataItem->GetData()->GetNumberOfPoints();
	int iNumIndices  = CountIndices(pDataItem->GetData());

	pTatget->SetPolyData(pDataItem);
	pTatget->SetLevelIndex(iTimeLevel);
	pTatget->SetDoCleanUp(true);

	bool b = true;
	b &= pTatget->GetTimeStep()->MapAndResizeIndexBuffer(iNumIndices);
	b &= pTatget->GetTimeStep()->MapAndResizeVertexBuffer(iNumVertices);

	return b;
}

bool VflVisGeometry::CleanUpAsyncFill(
		VflVisGeometry::VflVisGeomWorkInstance* pTarget)
{
	if(!pTarget->GetDoCleanUp())
		return false;

	pTarget->SetDoCleanUp(false);

	pTarget->GetThreadEvent()->WaitForEvent(true);
	pTarget->GetThreadEvent()->ResetThisEvent();

	pTarget->GetTimeStep()->UnmapIndexBuffer();
	pTarget->GetTimeStep()->UnmapVertexBuffer();
	return true;
}

/******************************************************************************/
int VflVisGeometry::CountIndices(vtkPolyData* pPolyData)
{
	int iNumIndices	 = 0;

	vtkIdType iCellCount = pPolyData->GetNumberOfPolys();
	vtkCellArray*	pCellArray	= pPolyData->GetPolys();
	vtkIdType		iCellId		= 0;
	vtkIdType		iPtCount	= 0;
	vtkIdType*		pPts		= 0;

	for(vtkIdType i=0; i<iCellCount; ++i)
	{
		pCellArray->GetCell(iCellId, iPtCount, pPts);
		iNumIndices += ((iPtCount-2)*3);

		iCellId += iPtCount + 1;
	}

	iCellCount	= pPolyData->GetNumberOfStrips();
	pCellArray	= pPolyData->GetStrips();
	iCellId		= 0;
	iPtCount	= 0;
	pPts		= 0;

	for(vtkIdType i=0; i<iCellCount; ++i)
	{
		pCellArray->GetCell(iCellId, iPtCount, pPts);
		iNumIndices += ((iPtCount-2)*3);

		iCellId += iPtCount + 1;
	}

	return iNumIndices;
}
/*============================================================================*/
/*  VflVisGeomTimeStep                                                        */
/*============================================================================*/
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
VflVisGeometry::VflVisGeomTimeStep::VflVisGeomTimeStep(VflVisGeometry* pParent)
	:	m_pVertexBuffer(NULL)
	,	m_pIndexBuffer(NULL)
	,	m_pParent(pParent)
{
	glGenBuffers(1, &m_oVBO.uiBufferID);
	glGenBuffers(1, &m_oIBO.uiBufferID);

	m_oVBO.iNumElements = 0;
	m_oIBO.iNumElements = 0;

	m_oVBO.iSize = 0;
	m_oIBO.iSize = 0;

}
VflVisGeometry::VflVisGeomTimeStep::~VflVisGeomTimeStep()
{
	glDeleteBuffers(1, &m_oIBO.uiBufferID);
	glDeleteBuffers(1, &m_oVBO.uiBufferID);
}
/******************************************************************************/
/*  IMPLEMENTATION                                                            */
/******************************************************************************/
bool VflVisGeometry::VflVisGeomTimeStep::
		MapAndResizeVertexBuffer(int iNumVertices)
{
	int iVertexBufferSize = iNumVertices*(m_pParent->m_iVertexSize);
	glBindBuffer(GL_ARRAY_BUFFER, m_oVBO.uiBufferID);
	if(iVertexBufferSize > m_oVBO.iSize)
	{
		glBufferData(GL_ARRAY_BUFFER, iVertexBufferSize,
			NULL, GL_DYNAMIC_DRAW);

		m_oVBO.iSize = iVertexBufferSize;
	}

	m_oVBO.iNumElements = iNumVertices;

	m_pVertexBuffer = (char*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

	if(!m_pVertexBuffer)
	{
		vstr::errp() << "Can't map Vertex Buffer." << endl;
		VistaOGLUtils::CheckForOGLError(__FILE__,__LINE__);

		return false;
	}
	return true;
}

/******************************************************************************/
bool VflVisGeometry::VflVisGeomTimeStep::
		MapAndResizeIndexBuffer(int iNumIndices)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_oIBO.uiBufferID);
	if(iNumIndices > m_oIBO.iSize)
	{
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, iNumIndices*sizeof(unsigned int),
			NULL, GL_DYNAMIC_DRAW);

		m_oIBO.iSize = iNumIndices;
	}

	m_oIBO.iNumElements = iNumIndices;

	m_pIndexBuffer = (unsigned int*) glMapBuffer(
		GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

	if(!m_pIndexBuffer)
	{
		vstr::errp() << "Can't map Index Buffer." << endl;
		VistaOGLUtils::CheckForOGLError(__FILE__,__LINE__);

		return false;
	}
	return true;
}

/******************************************************************************/
bool VflVisGeometry::VflVisGeomTimeStep::PushVertices(vtkPolyData *pPolyData)
{
	const unsigned int uiNumVerts = pPolyData->GetNumberOfPoints();

	char* pBuffer = m_pVertexBuffer;

	vtkAbstractArray* pDataArray = pPolyData->GetPoints()->GetData();

	unsigned int nAttribDataSize;
	nAttribDataSize  = pDataArray->GetDataTypeSize();
	nAttribDataSize *= 3*(uiNumVerts);
	memcpy(pBuffer, pDataArray->GetVoidPointer(0), nAttribDataSize);
	pBuffer += nAttribDataSize;

	vtkPointData* pPointData = pPolyData->GetPointData();
	for (size_t n = 1; n < m_pParent->m_vecAttributes.size(); ++n)
	{
		const VlfVisGeomAttribute* pAttrib = &(m_pParent->m_vecAttributes[n]);

		pDataArray = pPointData->GetAbstractArray(pAttrib->strName.c_str());
		if (NULL == pDataArray)
		{
			vstr::errp() <<"[VflVisGeometry] Attribute \"" << pAttrib->strName 
				<< "\" not found" << endl;
			return false;
		}
		nAttribDataSize  = pDataArray->GetDataTypeSize();
		nAttribDataSize *= (pAttrib->iComponents)*(uiNumVerts);
		memcpy(pBuffer, pDataArray->GetVoidPointer(0), nAttribDataSize);
		pBuffer += nAttribDataSize;
	}

	return true;
}

/******************************************************************************/
bool VflVisGeometry::VflVisGeomTimeStep::PushIndices(vtkPolyData *pPolyData)
{
	unsigned int* pElemData = m_pIndexBuffer;

	vtkIdType		iCellCount = pPolyData->GetNumberOfPolys();
	vtkCellArray*	pCellArray	= pPolyData->GetPolys();
	vtkIdType		iCellId		= 0;
	vtkIdType		iPtCount	= 0;
	vtkIdType*		pPts		= 0;


	for(vtkIdType i=0, iCellId = 0; i<iCellCount; ++i)
	{
		pCellArray->GetCell(iCellId, iPtCount, pPts);
		assert(iPtCount >= 3);
		for (int j = 0; j<iPtCount-2; ++j)
		{
			*pElemData = pPts[0];   ++pElemData;
			*pElemData = pPts[j+1]; ++pElemData;
			*pElemData = pPts[j+2]; ++pElemData;
		}
		iCellId += iPtCount + 1;
	}

	iCellCount	= pPolyData->GetNumberOfStrips();
	pCellArray	= pPolyData->GetStrips();
	iCellId		= 0;
	iPtCount	= 0;
	pPts		= 0;

	for(vtkIdType i=0, iCellId = 0; i<iCellCount; ++i)
	{
		pCellArray->GetCell(iCellId, iPtCount, pPts);
		assert(iPtCount >= 3);
		for (int j = 0; j<iPtCount-2; ++j)
		{
			if(j%2==0)
			{
				*pElemData = pPts[j];   ++pElemData;
				*pElemData = pPts[j+1]; ++pElemData;
				*pElemData = pPts[j+2]; ++pElemData;
			}
			else
			{
				*pElemData = pPts[j];   ++pElemData;
				*pElemData = pPts[j+2]; ++pElemData;
				*pElemData = pPts[j+1]; ++pElemData;
			}
		}
		iCellId += iPtCount + 1;
	}
	return true;
}

/******************************************************************************/
void VflVisGeometry::VflVisGeomTimeStep::UnmapVertexBuffer()
{
	glBindBuffer(GL_ARRAY_BUFFER, m_oVBO.uiBufferID);
	glUnmapBuffer(GL_ARRAY_BUFFER);
	m_pVertexBuffer = NULL;
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void VflVisGeometry::VflVisGeomTimeStep::UnmapIndexBuffer()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_oIBO.uiBufferID);
	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	m_pIndexBuffer = NULL;
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
/******************************************************************************/

const VflVisGeometry::VlfVisGeomBufferObject* 
	VflVisGeometry::VflVisGeomTimeStep::GetVBO()
{
	return &m_oVBO;
}
const VflVisGeometry::VlfVisGeomBufferObject* 
	VflVisGeometry::VflVisGeomTimeStep::GetIBO()
{
	return &m_oIBO;
}
/*============================================================================*/
/*  VflVisGeomTimeStep                                                        */
/*============================================================================*/
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
VflVisGeometry::VflVisGeomWorkInstance::VflVisGeomWorkInstance(
		VflVisGeometry* pParent)
:	m_pThreadEvent(new VistaThreadEvent(VistaThreadEvent::WAITABLE_EVENT))
	,	m_pParent(pParent)
	,	m_pTimeStep(NULL)
	,	m_pPolyData(NULL)
	,	m_iLevelIndex(-1)
	,	m_bDoCleanUp(false)
{}
VflVisGeometry::VflVisGeomWorkInstance::~VflVisGeomWorkInstance()
{
	delete m_pThreadEvent;
}
/******************************************************************************/
/*  IMPLEMENTATION                                                            */
/******************************************************************************/
void VflVisGeometry::VflVisGeomWorkInstance::SetPolyData(
		VveDataItem<vtkPolyData>* pPolyData)
{
	m_pPolyData = pPolyData;
}

void VflVisGeometry::VflVisGeomWorkInstance::SetTimeStep(
		VflVisGeomTimeStep* pTimeStep)
{
	m_pTimeStep = pTimeStep;
}

void VflVisGeometry::VflVisGeomWorkInstance::SetLevelIndex(int iLevelIndex)
{
	m_iLevelIndex = iLevelIndex;
}

void VflVisGeometry::VflVisGeomWorkInstance::SetDoCleanUp(bool b)
{
	m_bDoCleanUp = b;
}

int VflVisGeometry::VflVisGeomWorkInstance::GetLevelIndex()
{
	return m_iLevelIndex;
}
bool VflVisGeometry::VflVisGeomWorkInstance::GetDoCleanUp()
{
	return m_bDoCleanUp;
}

VistaThreadEvent* VflVisGeometry::VflVisGeomWorkInstance::GetThreadEvent()
{
	return m_pThreadEvent;
}

VflVisGeometry::VflVisGeomTimeStep* 
		VflVisGeometry::VflVisGeomWorkInstance::GetTimeStep()
{
	return m_pTimeStep;
}
void VflVisGeometry::VflVisGeomWorkInstance::DefinedThreadWork()
{
	if (!m_pPolyData || !m_pTimeStep)
	{
		m_pThreadEvent->SignalEvent();
		return;
	}

	m_pPolyData->LockData();

	m_pTimeStep->PushVertices(m_pPolyData->GetData());
	m_pTimeStep->PushIndices(m_pPolyData->GetData());

	m_pPolyData->UnlockData();

	m_pThreadEvent->SignalEvent();
}
/*============================================================================*/
/*  PROPERTES                                                                 */
/*============================================================================*/
static const string SsReflectionType("VflVisGeomatry");
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
VflVisGeometry::VflVisGeometryProperties::VflVisGeometryProperties()
	:	IVflVisObject::VflVisObjProperties()
{}

VflVisGeometry::VflVisGeometryProperties::~VflVisGeometryProperties()
{}

/******************************************************************************/
/*  IMPLEMENTATION                                                            */
/******************************************************************************/
string VflVisGeometry::VflVisGeometryProperties::GetReflectionableType() const
{
	return SsReflectionType;
}

int VflVisGeometry::VflVisGeometryProperties::
		AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = IVflVisObject::VflVisObjProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize;
}
/*============================================================================*/
/*  END OF FILE					                                              */
/*============================================================================*/
