/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFL_DEFAULT_GEOMETRY_RENDERER_H
#define VFL_DEFAULT_GEOMETRY_RENDERER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <iostream>
#include <string>

#include "VflGeometryRenderer.h"

#include <VistaBase/VistaBaseTypes.h>
#include <VistaBase/VistaVector3D.h>
#include <VistaBase/VistaColor.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisGeometry;
class VistaTexture;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflDefaultGeometryRenderer : public VflGeometryRenderer
{
public:
	enum EDrawMode
	{
		DM_SOLID_COLOR = DM_FIRST,
		DM_SCALARS_AS_TEX_COORDS,
		DM_COUNT
	};

	/**************************************************************************/
	/* CONSTRUCTORS / DESTRUCTOR                                              */
	/**************************************************************************/
	VflDefaultGeometryRenderer(VflVisGeometry *pOwner);
	virtual ~VflDefaultGeometryRenderer();

	/**************************************************************************/
	/* DRAW MODE                                                              */
	/**************************************************************************/
	virtual void SetDrawMode(int iMode);
	virtual int GetDrawMode() const;
	virtual int GetDrawModeCount() const;

	/**************************************************************************/
	/* VISGEOMETRYRENDERER API                                                */
	/**************************************************************************/
	virtual void DrawOpaque();

	virtual std::string GetType() const;

	/**************************************************************************/
	/* Getter/Setter for all for all propertys use by                         */
	/* Draw Mode DM_SOLID_COLOR                                               */
	/**************************************************************************/
	const VistaColor&	GetColor() const;
	void				SetColor(const VistaColor& rColor);

	/**************************************************************************/
	/* Getter/Setter for all for all propertys use by                         */
	/* Draw Mode DM_SCALARS_AS_TEX_COORDS                                     */
	/**************************************************************************/
	VistaTexture*	GetTexture();
	void			SetTexture(VistaTexture* pTexture);

	void GetScalarInterval(VistaVector3D& v3Min, VistaVector3D& v3Max) const;
	void SetScalarInterval(const VistaVector3D& v3Min, const VistaVector3D& v3Max);

	const std::string&	GetScalarName()const;
	void				SetScalarName(const std::string& strName);
		

	/**************************************************************************/
	/* Getter/Setter for all for all propertys use by all Draw Modes          */
	/**************************************************************************/

	const std::string&	GetPointesAttribName()const;
	const std::string&	GetNormalsAttribName()const;
	void				SetPointesAttribName(const std::string& strName);
	void				SetNormalsAttribName(const std::string& strName);

	bool GetIsLightingEnabled();
	void SetIsLightingEnabled(bool b);

	bool GetIsFaceCullingEnabled();
	void SetIsFaceCullingEnabled(bool b);

private:
	int		    m_iDrawMode;

	VistaColor	m_oColor;


	VistaTexture*	m_pTexture;
	VistaVector3D	m_v3ScalarMin;
	VistaVector3D	m_v3ScalarMax;

	std::string m_strPointesAttribName;
	std::string	m_strNormalsAttribName;
	std::string	m_strScalarAttribName;

	bool		m_bIsLightingEnabled;
	bool		m_bIsFaceCullingEnabled;
};

#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

