/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFL_GEOMETRY_RENDERER_H
#define VFL_GEOMETRY_RENDERER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <iostream>
#include <string>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VflGeometryRenderer;
class VflVisGeometry;
std::ostream& operator<< ( std::ostream &, const VflGeometryRenderer & );
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflGeometryRenderer
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflGeometryRenderer(VflVisGeometry *pOwner);
    virtual ~VflGeometryRenderer();

    /**
     * Initialize renderer.
     */    
    virtual bool Init();

	/**
	 * Destroy internal data.
	 */
	virtual void Destroy();

	/**
	 * Activate this renderer, i.e. notify the renderer, that it's
	 * going to be used pretty soon, to allow it to e.g. bring 
	 * internal data structures up-to-date.
	 */
	virtual void Activate();

	/**
	 * Deactivate this renderer, i.e. inform it, that it might destroy /
	 * deallocate internal data structures or stop updating internal data.
	 */
	virtual void Deactivate();

	/**
	 * Update the renderer, i.e. allow it to perform any data updates and such
	 * outside the actual render process.
	 */
	virtual void Update();

	/**
	 * Draw any opaque stuff.
	 */
	virtual void DrawOpaque();

	/**
	 * Draw any transparent stuff.
	 */
	virtual void DrawTransparent();

    /**
     * Returns the type of the particle renderer as a string.
     */    
	virtual std::string GetType() const;

    /**
     * Prints out some debug information to the given output stream.
     */    
    virtual void  Debug(std::ostream & out) const;

	/**
	 * Manage different draw modes.
	 */
	virtual void SetDrawMode(int nMode);
	virtual int GetDrawMode() const;
	virtual int GetDrawModeCount() const;

	enum EDrawMode
	{
		DM_NONE = -1,
		DM_FIRST = 0
	};

	std::string GetName() const;
	void SetName(std::string strName);

protected:
	VflVisGeometry *m_pOwner;
	std::string		m_strName;
	double			m_dDataTimeStamp;
	double			m_dVisParamsTimeStamp;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
