

set( RelativeDir "./Visualization/Geometry" )
set( RelativeSourceGroup "Source Files\\Visualization\\Geometry" )
set( SubDirs shaders)

set( DirFiles
	VflVisGeometry.cpp
	VflVisGeometry.h
	VflGeometryRenderer.cpp
	VflGeometryRenderer.h
	VflDefaultGeometryRenderer.cpp
	VflDefaultGeometryRenderer.h
	VflShaderGeomRenderer.cpp
	VflShaderGeomRenderer.h
	VflGeometryVis.cpp
	VflGeometryVis.h
	VflVtkPolyDataConverter.cpp
	VflVtkPolyDataConverter.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()