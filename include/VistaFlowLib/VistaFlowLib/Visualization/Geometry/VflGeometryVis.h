/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFL_UNSTEADY_GEOMATRY_RENDERER_H
#define VFL_UNSTEADY_GEOMATRY_RENDERER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "VflVtkPolyDataConverter.h"

#include <VistaFlowLib/Visualization/VflVisObject.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/IVflTransformer.h>

#include <VistaVisExt/Data/VveDiscreteDataTyped.h>
#include <VistaVisExt/Data/VveVtkData.h>

#include <VistaOGLExt/Rendering/VistaGeometryData.h>
#include <VistaOGLExt/Rendering/VistaGeometryRenderingCore.h>

#include <vector>
#include <cassert>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;
class VistaBufferObject;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI IVflGeometryVis : public IVflVisObject
{
	friend class VflGeometryVisProperties;

public:
	IVflGeometryVis();
	virtual ~IVflGeometryVis();

	virtual void ClearCache() = 0;

	// *** VistaVolumeRaycasterCore wrapper **
#pragma region GeometryRenderingCore wrapper

	void SetSurfaceShader( VistaGLSLShader* pShader );
	void SetLineShader( VistaGLSLShader* pShader );
	VistaGLSLShader* GetSurfaceShader() const;
	VistaGLSLShader* GetLineShader() const;

	void SetSurfaceColor( const VistaColor& rColor );
	void SetLineColor( const VistaColor& rColor );
	const VistaColor& GetSurfaceColor() const;
	const VistaColor& GetLineColor() const;

	void SetUsedTextures( const std::vector<VistaTexture*>& vecTextures,
						  bool bTransferOwnership = false );
	std::vector<VistaTexture*>& GetUsedTextures();

	void SetUniform( const std::string& strName, float f1);
	void SetUniform( const std::string& strName, float f1, float f2);
	void SetUniform( const std::string& strName, float f1, float f2, float f3);
	void SetUniform( const std::string& strName, float f1, float f2, float f3, float f4);
	void SetUniform( const std::string& strName, int i );

#pragma endregion GeometryRenderingCore wrapper



	// *** IVflRenderable interface ***
	class VISTAFLOWLIBAPI VflGeometryVisProperties  : public IVflVisObject::VflVisObjProperties
	{
	public:
		VflGeometryVisProperties();
		~VflGeometryVisProperties();

		bool SetPerformStreamUpdate( bool bEnable );
		bool GetPerformStreamUpdate() const;

		bool SetEnableCullFace( bool bEnable );
		bool GetEnableCullFace() const;
		
		bool SetPolygonModeByString(const std::string& strName );
		bool SetPolygonMode( int iMode );
		int  GetPolygonMode() const;

		bool SetEnableCullFrontFace( bool bEnable );
		bool GetEnableCullFrontFace() const;

		std::string GetReflectionableType() const;

	protected:
		int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		bool m_bStreamUpdate; //< Update in every frame
		bool m_bEnableCullFace;
		bool m_bEnableCullFrontFace;
		int  m_iPolygonMode; // Must be GL_FILL, GL_LINE, or GL_POINT
	};

	VflGeometryVisProperties* GetProperties() const;

protected:
	virtual VflRenderableProperties* CreateProperties() const;

protected:
	VistaGeometryRenderingCore*			m_pRenderingCore;

	std::vector<VistaGeometryData*>		m_vecConvertedData;
};

template<typename TDataType, typename TConverter>
class TVflGeometryVis : public IVflGeometryVis
{
public:
	TVflGeometryVis(bool bTranscarency = false);
	virtual ~TVflGeometryVis();

	bool SetData( VveDiscreteDataTyped<TDataType>* pData );
	VveDiscreteDataTyped<TDataType>* GetData() const;

	TConverter* GetDataConverter() const;

	void ClearCache();

	// *** IVflRenderable interface ***
	virtual void Update();

	virtual void DrawOpaque();
	virtual void DrawTransparent();

	virtual unsigned int GetRegistrationMode() const;

private:
	VveDiscreteDataTyped<TDataType>*	m_pUnsteadyData;
	TConverter*							m_pDataConverter;
	bool								m_bTransparency;
};

/*============================================================================*/
/* TYPEDEFS                                                                   */
/*============================================================================*/
typedef TVflGeometryVis< vtkPolyData, VflVtkPolyDataConverter > 
	VflVtkPolyDataVis;

/*============================================================================*/
/* Template Implementation                                                    */
/*============================================================================*/
#pragma region TemplateImplementation

template<typename TDataType, typename TConverter>
TVflGeometryVis<TDataType, TConverter>::TVflGeometryVis(bool bTransparency)
	:	IVflGeometryVis( )
	,	m_pDataConverter( new TConverter() )
	,	m_pUnsteadyData(NULL)
	,   m_bTransparency(bTransparency)
{ }

template<typename TDataType, typename TConverter>
TVflGeometryVis<TDataType, TConverter>::~TVflGeometryVis()
{
	delete m_pDataConverter;
}

template<typename TDataType, typename TConverter>
bool TVflGeometryVis<TDataType, TConverter>
	::SetData( VveDiscreteDataTyped<TDataType>* pData )
{
	if( m_pUnsteadyData != pData )
	{
		m_pUnsteadyData = pData;
	}

	ClearCache();
	return true;
}

template<typename TDataType, typename TConverter>
VveDiscreteDataTyped<TDataType>* 
	TVflGeometryVis<TDataType, TConverter>::GetData() const
{
	return m_pUnsteadyData;
}

template<typename TDataType, typename TConverter>
TConverter* TVflGeometryVis<TDataType, TConverter>
	::GetDataConverter() const
{
	return m_pDataConverter;
}

template<typename TDataType, typename TConverter>
void TVflGeometryVis<TDataType, TConverter>::ClearCache()
{
	for( size_t n=0; n<m_vecConvertedData.size(); ++n)
	{
		delete m_vecConvertedData[n];
		m_vecConvertedData[n] = NULL;
	}

	m_vecConvertedData.resize( m_pUnsteadyData->GetNumberOfLevels() );
}

template<typename TDataType, typename TConverter>
void TVflGeometryVis<TDataType, TConverter>::Update()
{
	IVflGeometryVis::VflGeometryVisProperties* pProps = GetProperties();
	if(!pProps || !pProps->GetVisible())
		return;

	// Determine currently active data time step
	const double dVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	const double dSimTime = m_pUnsteadyData->GetTimeMapper()->GetSimulationTime(dVisTime);

	int iLevelIndex = m_pUnsteadyData->GetTimeMapper()->GetLevelIndex(dVisTime);

	if( !m_vecConvertedData[iLevelIndex] || pProps->GetPerformStreamUpdate() )
	{
		if( m_vecConvertedData[iLevelIndex] )
			delete m_vecConvertedData[iLevelIndex];

		m_vecConvertedData[iLevelIndex]  = 
			m_pDataConverter->ConvertData(
				m_pUnsteadyData->GetTypedLevelDataByLevelIndex(iLevelIndex)->GetData()
			);
	}

	m_pRenderingCore->SetData( m_vecConvertedData[iLevelIndex] );
}

template<typename TDataType, typename TConverter>
void TVflGeometryVis<TDataType, TConverter>::DrawOpaque()
{
	IVflGeometryVis::VflGeometryVisProperties* pProps = GetProperties();
	if(!pProps || !pProps->GetVisible())
		return;

	glPushAttrib( GL_ENABLE_BIT | GL_POLYGON_BIT );
	if( pProps->GetEnableCullFace() )
	{
		glCullFace(GL_BACK);
		glEnable( GL_CULL_FACE );
	}
	else
	{
		glDisable( GL_CULL_FACE );
	}
	glPolygonMode( GL_FRONT_AND_BACK, pProps->GetPolygonMode() );

	if( pProps->GetEnableCullFrontFace() )
	{
		glCullFace(GL_FRONT);
		glEnable(GL_CULL_FACE );
	}
	else
	{
		glDisable( GL_CULL_FACE );
	}

	if( pProps->GetEnableCullFrontFace() && pProps->GetEnableCullFace())
	{
		glCullFace(GL_FRONT_AND_BACK);
	}

	glPushMatrix();

	const double dVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	const double dSimTime = m_pUnsteadyData->GetTimeMapper()->GetSimulationTime(dVisTime);

	VistaTransformMatrix oTransformerMat;
	if( m_pTransformer && m_pTransformer->GetUnsteadyTransform( dSimTime, oTransformerMat ) )
	{
		float aMatValues[16];
		oTransformerMat.GetTransposedValues( aMatValues );
		glMultMatrixf( aMatValues );
	}

	m_pRenderingCore->Draw();

	glPopMatrix();

	glPopAttrib();
}

template<typename TDataType, typename TConverter>
void TVflGeometryVis<TDataType, TConverter>::DrawTransparent()
{
	DrawOpaque();
}

template<typename TDataType, typename TConverter>
unsigned int TVflGeometryVis<TDataType, TConverter>
	::GetRegistrationMode() const
{
	if(m_bTransparency)
	{
		return OLI_UPDATE | OLI_DRAW_TRANSPARENT;
	}
	else
	{
		return OLI_UPDATE | OLI_DRAW_OPAQUE;
	}
}

#pragma endregion TemplateImplementation
#endif // Include guard.
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/


