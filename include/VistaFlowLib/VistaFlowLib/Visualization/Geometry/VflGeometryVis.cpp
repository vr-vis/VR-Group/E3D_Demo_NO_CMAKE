/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VflGeometryVis.h"

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
IVflGeometryVis::IVflGeometryVis()
	:	m_pRenderingCore( new VistaGeometryRenderingCore() )
{ }

IVflGeometryVis::~IVflGeometryVis()
{
	delete m_pRenderingCore;
}

// *** VistaGeometryRenderingCore wrapper ***
#pragma region GeometryRenderingCore wrapper

void IVflGeometryVis::SetSurfaceShader( VistaGLSLShader* pShader )
{
	m_pRenderingCore->SetSurfaceShader(pShader);
}
void IVflGeometryVis::SetLineShader( VistaGLSLShader* pShader )
{
	m_pRenderingCore->SetLineShader(pShader);
}
VistaGLSLShader* IVflGeometryVis::GetSurfaceShader() const
{
	return m_pRenderingCore->GetSurfaceShader();
}
VistaGLSLShader* IVflGeometryVis::GetLineShader() const
{
	return m_pRenderingCore->GetLineShader();
}

void IVflGeometryVis::SetSurfaceColor( const VistaColor& rColor )
{
	m_pRenderingCore->SetSurfaceColor( rColor );
}
void IVflGeometryVis::SetLineColor( const VistaColor& rColor )
{
	m_pRenderingCore->SetLineColor( rColor );
}
const VistaColor& IVflGeometryVis::GetSurfaceColor() const
{
	return m_pRenderingCore->GetSurfaceColor();
}
const VistaColor& IVflGeometryVis::GetLineColor() const
{
	return m_pRenderingCore->GetLineColor();
}

void IVflGeometryVis::SetUsedTextures( const std::vector<VistaTexture*>& vecTextures,
									   bool bTransferOwnership )
{
	m_pRenderingCore->SetUsedTextures(vecTextures, bTransferOwnership);
}
std::vector<VistaTexture*>& IVflGeometryVis::GetUsedTextures()
{
	return m_pRenderingCore->GetUsedTextures();
}

void IVflGeometryVis::SetUniform( const std::string& strName, float f1)
{
	m_pRenderingCore->SetUniform( strName, f1 );
}
void IVflGeometryVis::SetUniform( const std::string& strName, float f1, float f2)
{
	m_pRenderingCore->SetUniform( strName, f1, f2 );
}
void IVflGeometryVis::SetUniform( const std::string& strName, float f1, float f2, float f3)
{
	m_pRenderingCore->SetUniform( strName, f1, f2, f3 );
}
void IVflGeometryVis::SetUniform( const std::string& strName, float f1, float f2, float f3, float f4)
{
	m_pRenderingCore->SetUniform( strName, f1, f2, f3, f4 );
}
void IVflGeometryVis::SetUniform( const std::string& strName, int i )
{
	m_pRenderingCore->SetUniform( strName, i );
}

#pragma endregion GeometryRenderingCore wrapper

// *** IVflRenderable interface ***

IVflGeometryVis::VflGeometryVisProperties* IVflGeometryVis::GetProperties() const
{
	return dynamic_cast<VflGeometryVisProperties*>( IVflRenderable::GetProperties() );
}

IVflRenderable::VflRenderableProperties* IVflGeometryVis::CreateProperties() const
{
	return new VflGeometryVisProperties();
}

/*============================================================================*/
/*  PROPERTIES                                                                */
/*============================================================================*/

static const string SsReflectionType("VflGeometryVis");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, IVflGeometryVis::VflGeometryVisProperties>
		("PERFORM_STREAM_UPDATE", SsReflectionType,
		&IVflGeometryVis::VflGeometryVisProperties::GetPerformStreamUpdate),
	new TVistaPropertyGet<bool, IVflGeometryVis::VflGeometryVisProperties>
		("BACKFACE_CULLING", SsReflectionType,
		&IVflGeometryVis::VflGeometryVisProperties::GetEnableCullFace),
	new TVistaPropertyGet<int, IVflGeometryVis::VflGeometryVisProperties>
		("POLYGON_MODE", SsReflectionType,
		&IVflGeometryVis::VflGeometryVisProperties::GetPolygonMode),
	new TVistaPropertyGet<bool, IVflGeometryVis::VflGeometryVisProperties>
		("FRONTFACE_CULLING", SsReflectionType,
		&IVflGeometryVis::VflGeometryVisProperties::GetEnableCullFrontFace),
	NULL //<* terminate array
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool,bool, IVflGeometryVis::VflGeometryVisProperties>
		("PERFORM_STREAM_UPDATE", SsReflectionType,
		&IVflGeometryVis::VflGeometryVisProperties::SetPerformStreamUpdate),
	new TVistaPropertySet<bool,bool, IVflGeometryVis::VflGeometryVisProperties>
		("BACKFACE_CULLING", SsReflectionType,
		&IVflGeometryVis::VflGeometryVisProperties::SetEnableCullFace),
	new TVistaPropertySet<const std::string&,std::string, IVflGeometryVis::VflGeometryVisProperties>
		("POLYGON_MODE", SsReflectionType,
		&IVflGeometryVis::VflGeometryVisProperties::SetPolygonModeByString),
	new TVistaPropertySet<bool,bool, IVflGeometryVis::VflGeometryVisProperties>
		("FRONTFACE_CULLING", SsReflectionType,
		&IVflGeometryVis::VflGeometryVisProperties::SetEnableCullFrontFace),
	NULL //<* terminate array
};

std::string IVflGeometryVis::VflGeometryVisProperties::GetReflectionableType() const
{
	return SsReflectionType;
}

int IVflGeometryVis::VflGeometryVisProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = VflVisObjProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}

IVflGeometryVis::VflGeometryVisProperties::VflGeometryVisProperties()
	:	IVflVisObject::VflVisObjProperties()
	,	m_bStreamUpdate( false )
	,	m_bEnableCullFace( true )
	,   m_bEnableCullFrontFace( false )
	,	m_iPolygonMode( GL_FILL )
{ }
IVflGeometryVis::VflGeometryVisProperties::~VflGeometryVisProperties()
{ }

bool IVflGeometryVis::VflGeometryVisProperties::SetPerformStreamUpdate( bool bEnable )
{
	m_bStreamUpdate = bEnable;
	return true;
}
bool IVflGeometryVis::VflGeometryVisProperties::GetPerformStreamUpdate() const
{
	return m_bStreamUpdate;
}
bool IVflGeometryVis::VflGeometryVisProperties::SetEnableCullFace( bool bEnable )
{
	m_bEnableCullFace = bEnable;
	return true;
}

bool IVflGeometryVis::VflGeometryVisProperties::GetEnableCullFace() const
{
	return m_bEnableCullFace;
}

bool IVflGeometryVis::VflGeometryVisProperties::SetEnableCullFrontFace( bool bEnable )
{
	m_bEnableCullFrontFace = bEnable;
	return true;
}
bool IVflGeometryVis::VflGeometryVisProperties::GetEnableCullFrontFace() const
{
	return m_bEnableCullFrontFace;
}

bool IVflGeometryVis::VflGeometryVisProperties::SetPolygonModeByString(const std::string& strName )
{
	if( strName == "WIREFRAME")
	{
		return SetPolygonMode(GL_LINE);
	}
	else if (strName == "SOLID")
	{
		return SetPolygonMode(GL_FILL);
	}
	else if (strName == "POINTS")
	{
		return SetPolygonMode(GL_POINT);
	}
	else
	{
		return false;
	}
}

bool IVflGeometryVis::VflGeometryVisProperties::SetPolygonMode( int iMode )
{
	if( iMode!=GL_FILL && iMode!=GL_LINE && iMode!=GL_POINT )
		return false;

	m_iPolygonMode = iMode;
	return true;
}
int  IVflGeometryVis::VflGeometryVisProperties::GetPolygonMode() const
{
	return m_iPolygonMode;
}


