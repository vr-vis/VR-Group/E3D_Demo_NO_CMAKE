/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflGeometryRenderer.h"
#include "VflVisGeometry.h"

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflGeometryRenderer::VflGeometryRenderer(VflVisGeometry *pOwner)
: m_pOwner(pOwner), m_dDataTimeStamp(-1), m_dVisParamsTimeStamp(-1)
{
}

VflGeometryRenderer::~VflGeometryRenderer()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/

bool VflGeometryRenderer::Init()
{
	return true;
}

void VflGeometryRenderer::Destroy()
{
}

/*============================================================================*/

void VflGeometryRenderer::Activate()
{
}

void VflGeometryRenderer::Deactivate()
{
}

/*============================================================================*/

void VflGeometryRenderer::Update()
{
}

void VflGeometryRenderer::DrawOpaque()
{
}

void VflGeometryRenderer::DrawTransparent()
{
}

/*============================================================================*/

void VflGeometryRenderer::SetName(string strName)
{
	m_strName = strName;
}

std::string VflGeometryRenderer::GetName() const
{
	return m_strName;
}

/*============================================================================*/

std::string VflGeometryRenderer::GetType() const
{
	return string("VflGeometryRenderer");
}

void VflGeometryRenderer::Debug(std::ostream & out) const
{
	out << " [VflGeometryRenderer] Type: " << GetType() << endl;
}

/*============================================================================*/
void VflGeometryRenderer::SetDrawMode(int nMode)
{
}

int VflGeometryRenderer::GetDrawMode() const
{
	return DM_NONE;
}

int VflGeometryRenderer::GetDrawModeCount() const
{
	return 0;
}

/*============================================================================*/

ostream & operator<< ( ostream & out, const VflGeometryRenderer & object )
{
	object.Debug ( out );
	return out;
}

/*============================================================================*/
/*  END OF FILE "VflParticleRenderer.cpp"                                     */
/*============================================================================*/
