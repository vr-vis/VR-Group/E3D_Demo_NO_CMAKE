/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VflVtkPolyDataConverter.h"

#include <VistaOGLExt/VistaBufferObject.h>
#include <VistaOGLExt/VistaVertexArrayObject.h>
#include <VistaOGLExt/Rendering/VistaGeometryData.h>

#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>

VflVtkPolyDataConverter::VflVtkPolyDataConverter()
	:	m_pCurrentData( NULL ),
		m_pConvertedData( NULL )
{ }

VflVtkPolyDataConverter::~VflVtkPolyDataConverter()
{
	delete m_pConvertedData;
}

VistaGeometryData* VflVtkPolyDataConverter::ConvertData( vtkPolyData* pDataIn )
{
	m_pConvertedData = new VistaGeometryData;

	//check if we have non-existing or empty data and return empty data if so...
	if(pDataIn == NULL || pDataIn->GetNumberOfPoints() == 0 || pDataIn->GetNumberOfCells() == 0)
		return m_pConvertedData;

	// Since Vtk 6.* Update() should be called on the generating algorithm.
	// Therefore, the calling function has to take care of that
	// pDataIn->Update();

	m_pCurrentData   = pDataIn;
	
	PushVertices();
	PushIndices();

	return m_pConvertedData;
}

void VflVtkPolyDataConverter::PushVertices( )
{
	const unsigned int uiNumVerts = m_pCurrentData->GetNumberOfPoints();

	vtkPointData*	  pPointData  = m_pCurrentData->GetPointData();
	vtkAbstractArray* pDataArray  = m_pCurrentData->GetPoints()->GetData();
	unsigned int      uiNumAttrib = pPointData->GetNumberOfArrays();

	PushAttribute( pDataArray, uiNumVerts, 0 );
	
	pDataArray = pPointData->GetNormals();
	if( pDataArray ) 
		PushAttribute( pDataArray, uiNumVerts, 1 );

	pDataArray = pPointData->GetTCoords();
	if( pDataArray ) 
		PushAttribute( pDataArray, uiNumVerts, 2 );

	pDataArray = pPointData->GetScalars();
	if( pDataArray ) 
		PushAttribute( pDataArray, uiNumVerts, 3 );
}

void VflVtkPolyDataConverter::PushIndices()
{
	std::vector<unsigned int> vecIndices;

	PushPolygons( vecIndices );
	PushStrips( vecIndices );
	PushLines( vecIndices );

	VistaBufferObject* pIBO = m_pConvertedData->GetInderBufferObject();
	pIBO->BindAsIndexBuffer();
	pIBO->BufferData( sizeof( unsigned int ) * vecIndices.size(), &vecIndices[0], GL_STATIC_DRAW );
	pIBO->Release();
}

void VflVtkPolyDataConverter::PushAttribute( vtkAbstractArray* pDataIn, unsigned int uiNumVerts, unsigned int uiChannel )
{
	unsigned int uiNumComponents = pDataIn->GetNumberOfComponents();
	unsigned int uiBufferSize    = pDataIn->GetDataTypeSize() * uiNumVerts *uiNumComponents;

	switch( pDataIn->GetDataType() )
	{
	case VTK_FLOAT:
		m_pConvertedData->SpecifyFloatAttribute( uiChannel, uiNumComponents, GL_FLOAT );
		break;
	case VTK_DOUBLE:
		m_pConvertedData->SpecifyDoubleAttribute( uiChannel, uiNumComponents, GL_DOUBLE );
		break;
	case VTK_INT:
		m_pConvertedData->SpecifyIntegerAttribute( uiChannel, uiNumComponents, GL_INT );
		break;
	case VTK_UNSIGNED_INT:
		m_pConvertedData->SpecifyIntegerAttribute( uiChannel, uiNumComponents, GL_UNSIGNED_INT );
		break;
	}

	m_pConvertedData->UpdateAttributeData( uiChannel, uiBufferSize, pDataIn->GetVoidPointer(0) );
}

void VflVtkPolyDataConverter::PushPolygons( std::vector<unsigned int>& vecIndices )
{
	size_t nOffset = vecIndices.size();

	vtkIdType		iCellCount = m_pCurrentData->GetNumberOfPolys();
	vtkCellArray*	pCellArray = m_pCurrentData->GetPolys();
	vtkIdType		iCellId	   = 0;
	vtkIdType		iPtCount   = 0;
	vtkIdType*		pPts       = 0;

	for(vtkIdType i=0, iCellId = 0; i<iCellCount; ++i)
	{
		pCellArray->GetCell(iCellId, iPtCount, pPts);
		for (int j = 0; j<iPtCount-2; ++j)
		{
			vecIndices.push_back( pPts[0] );
			vecIndices.push_back( pPts[j+1] );
			vecIndices.push_back( pPts[j+2] );
			vecIndices.push_back( ~0 );
		}
		iCellId += iPtCount + 1;
	}

	m_pConvertedData->SetPolygonCount( static_cast<unsigned int>( vecIndices.size() - nOffset ) );
	m_pConvertedData->SetPolygonOffset( static_cast<unsigned int>( nOffset*sizeof(unsigned int) ) );
}

void VflVtkPolyDataConverter::PushStrips( std::vector<unsigned int>& vecIndices )
{
	size_t nOffset = vecIndices.size();

	vtkIdType		iCellCount	= m_pCurrentData->GetNumberOfStrips();
	vtkCellArray*	pCellArray	= m_pCurrentData->GetStrips();
	vtkIdType		iCellId		= 0;
	vtkIdType		iPtCount	= 0;
	vtkIdType*		pPts		= 0;

	for(vtkIdType i=0, iCellId = 0; i<iCellCount; ++i)
	{
		pCellArray->GetCell(iCellId, iPtCount, pPts);
		for (int j = 0; j<iPtCount; ++j)
		{
			vecIndices.push_back( pPts[j] );
		}
		vecIndices.push_back( ~0 );
		iCellId += iPtCount + 1;
	}

	m_pConvertedData->SetStripCount( static_cast<unsigned int>( vecIndices.size() - nOffset ) );
	m_pConvertedData->SetStripOffset( static_cast<unsigned int>( nOffset*sizeof(unsigned int) ) );
}

void VflVtkPolyDataConverter::PushLines( std::vector<unsigned int>& vecIndices  )
{
	size_t nOffset = vecIndices.size();

	vtkIdType		iCellCount	= m_pCurrentData->GetNumberOfLines();
	vtkCellArray*	pCellArray	= m_pCurrentData->GetLines();
	vtkIdType		iCellId		= 0;
	vtkIdType		iPtCount	= 0;
	vtkIdType*		pPts		= 0;

	for(vtkIdType i=0, iCellId = 0; i<iCellCount; ++i)
	{
		pCellArray->GetCell(iCellId, iPtCount, pPts);
		for (int j = 0; j<iPtCount; ++j)
		{
			vecIndices.push_back( pPts[j] );
		}
		vecIndices.push_back( ~0 );
		iCellId += iPtCount + 1;
	}

	m_pConvertedData->SetLineCount( static_cast<unsigned int>( vecIndices.size() - nOffset ) );
	m_pConvertedData->SetLineOffset( static_cast<unsigned int>( nOffset*sizeof(unsigned int) ) );
}