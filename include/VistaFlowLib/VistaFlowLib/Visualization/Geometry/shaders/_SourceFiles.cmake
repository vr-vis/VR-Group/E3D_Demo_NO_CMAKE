

set( RelativeDir "./Visualization/Geometry/shaders" )
set( RelativeSourceGroup "Source Files\\Visualization\\Geometry\\Shaders" )

set( DirFiles
	VflShaderGeomRenderer_vert.glsl
	VflShaderGeomRenderer_frag.glsl
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

