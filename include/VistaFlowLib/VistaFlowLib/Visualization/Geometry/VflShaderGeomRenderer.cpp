/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

#include <GL/glew.h>

#include "VflShaderGeomRenderer.h"
#include "VflVisGeometry.h"

#include "../VflRenderNode.h"

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaShaderRegistry.h>

#include <VistaAspects/VistaTransformable.h>

#include <VistaBase/VistaStreamUtils.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/* VflShaderGeomRenderer                                                      */
/*============================================================================*/
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
VflShaderGeomRenderer::VflShaderGeomRenderer(VflVisGeometry *pOwner, 
											 bool bCreateDefaultDrawMode)
:	VflGeometryRenderer(pOwner)
,	m_iDrawMode(-1)
{
	if(!bCreateDefaultDrawMode)
		return;

	VistaShaderRegistry& rShaderReg = VistaShaderRegistry::GetInstance();

	string strVS = rShaderReg.RetrieveShader( "VflShaderGeomRenderer_vert.glsl" );
	string strFS = rShaderReg.RetrieveShader( "VflShaderGeomRenderer_frag.glsl" );;


	if( strVS.empty() || strFS.empty() )
	{
		vstr::errp() << "[VflShaderGeomRenderer] Failed to init Shader" << endl;
		vstr::IndentObject oIndent;
		if( strVS.empty() ) vstr::erri() << "VflGeomRenderer_VS.glsl not found." << endl;
		if( strFS.empty() ) vstr::erri() << "VflGeomRenderer_FS.glsl not found." << endl;
		return;
	}

	VflShaderDrawMode* pDrawMode = new VflShaderDrawMode();

	VistaGLSLShader* pShader = new VistaGLSLShader;

	pShader->InitVertexShaderFromString(strVS);
	pShader->InitFragmentShaderFromString(strFS);
	pShader->Link();

	pDrawMode->SetShader(pShader);

	m_vecDrawModes.push_back(pDrawMode);
	m_iDrawMode = 0;
}

VflShaderGeomRenderer::~VflShaderGeomRenderer()
{
	delete m_vecDrawModes[0];
}

/******************************************************************************/
/* DRAW MODE                                                                  */
/******************************************************************************/                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
int VflShaderGeomRenderer::AddDrawMode(VflShaderDrawMode* pDrawMode)
{
	m_vecDrawModes.push_back(pDrawMode);
	m_iDrawMode = static_cast<int>(m_vecDrawModes.size()) - 1;
	return m_iDrawMode;
}

VflShaderGeomRenderer::VflShaderDrawMode* VflShaderGeomRenderer::GetDrawMode(int iMode)
{
	if(iMode<0 || static_cast<size_t>(iMode)>= m_vecDrawModes.size())
		return NULL;

	return (m_vecDrawModes[iMode]);
}

/******************************************************************************/
void VflShaderGeomRenderer::SetDrawMode(int iMode)
{
	m_iDrawMode = iMode;
}

int VflShaderGeomRenderer::GetDrawMode() const
{
	return m_iDrawMode;
}

int VflShaderGeomRenderer::GetDrawModeCount() const
{
	return static_cast<int>(m_vecDrawModes.size());
}

/******************************************************************************/
/* VISGEOMETRYRENDERER API                                                    */
/******************************************************************************/
void VflShaderGeomRenderer::DrawOpaque()
{
	if(m_iDrawMode < 0 || m_iDrawMode >= GetDrawModeCount())
		return;

	const VflVisGeometry::VlfVisGeomBufferObject* pVBO;
	const VflVisGeometry::VlfVisGeomBufferObject* pIBO;
	const VflVisGeometry::VlfVisGeomAttribute* pAttrib;

	pVBO = m_pOwner->GetVertexBufferObject();
	pIBO = m_pOwner->GetIndexBufferObject();

	if(!pVBO || !pIBO || pIBO->iNumElements == 0)
		return;
 
	glBindBuffer(GL_ARRAY_BUFFER, pVBO->uiBufferID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pIBO->uiBufferID);

	size_t nAttribs = m_vecDrawModes[m_iDrawMode]->GetReqiredArtibuts().size();
	for(size_t n = 0; n <nAttribs; ++n)
	{
		glEnableVertexAttribArray(static_cast<GLuint>(n));
		pAttrib = m_pOwner->GetAttributeByName(
				m_vecDrawModes[m_iDrawMode]->GetReqiredArtibuts()[n]);

		if (pAttrib == NULL)
		{
			vstr::warnp() << "[VflShaderGeomRenderer] Attribute \""
				 << m_vecDrawModes[m_iDrawMode]->GetReqiredArtibuts()[n] 
				 << "\" not found" << endl;
			continue;
		}

		switch(pAttrib->eType)
		{
		case VflVisGeometry::VlfVisGeomAttribute::TYPE_FLOAT:
			glVertexAttribPointer(static_cast<GLuint>(n), pAttrib->iComponents,
				GL_FLOAT, GL_FALSE, 0, GETPOINTEROFFSET(pAttrib, pVBO));
			break;
		case VflVisGeometry::VlfVisGeomAttribute::TYPE_DOUBLE:
			glVertexAttribLPointer(static_cast<GLuint>(n), pAttrib->iComponents,
				GL_DOUBLE, 0, GETPOINTEROFFSET(pAttrib, pVBO));
			break;
		case VflVisGeometry::VlfVisGeomAttribute::TYPE_INT:
			glVertexAttribIPointer(static_cast<GLuint>(n), pAttrib->iComponents,
				GL_INT, 0, GETPOINTEROFFSET(pAttrib, pVBO));
			break;
		}
	}

	glDisable(GL_CULL_FACE);

	m_vecDrawModes[m_iDrawMode]->GetShader()->Bind();
	m_vecDrawModes[m_iDrawMode]->UpdateUniforms(m_pOwner);

	glDrawElements(GL_TRIANGLES, pIBO->iNumElements, GL_UNSIGNED_INT, (void*)(0));

	m_vecDrawModes[m_iDrawMode]->GetShader()->Release();

	for(size_t n = 0; n <nAttribs; ++n)
	{
		glDisableVertexAttribArray(static_cast<GLuint>(n));
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

/******************************************************************************/
std::string VflShaderGeomRenderer::GetType() const
{
	return VflGeometryRenderer::GetType() + std::string("::VflShaderGeomRenderer");
}


/*============================================================================*/
/* VflShaderGeomRenderer::VflShaderDrawMode                                   */
/*============================================================================*/
/******************************************************************************/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/******************************************************************************/
VflShaderGeomRenderer::VflShaderDrawMode::VflShaderDrawMode()
	:	m_pShader(NULL)
{
	m_vecArttibutNames.push_back("Points");
	m_vecArttibutNames.push_back("Normals");
}

VflShaderGeomRenderer::VflShaderDrawMode::~VflShaderDrawMode()
{ 
	delete m_pShader;
}
/******************************************************************************/
/*  IMPLEMENTATION                                                            */
/******************************************************************************/
void VflShaderGeomRenderer::VflShaderDrawMode::UpdateUniforms(VflVisGeometry* pGeom)
{
	float aMv[16], aNormalMv[9], aMp[16];

	float aProjMat[16];
	glGetFloatv(GL_PROJECTION_MATRIX, aProjMat);
	VistaTransformMatrix oProjMat(aProjMat);
	oProjMat = oProjMat.GetTranspose();

	float aMvMat[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, aMvMat);
	VistaTransformMatrix oMv(aMvMat);
	oMv = oMv.GetTranspose();

	IVistaTransformable *pTrans = pGeom->GetRenderNode()->GetTransformable();
	if(pTrans)
	{
		float aModel[16];
		pTrans->GetWorldTransform(aModel);
		VistaTransformMatrix oModel(aModel);

		oMv = oMv * oModel;
	}

	// Get the mv matrix for light calculations.
	oMv.GetValues(aMv);
	oProjMat.GetValues(aMp);

	// Create the inverse, transposed mv matrix for normal transformation.
	aNormalMv[0] = oMv.GetInverted().GetValue(0, 0);
	aNormalMv[1] = oMv.GetInverted().GetValue(0, 1);
	aNormalMv[2] = oMv.GetInverted().GetValue(0, 2);

	aNormalMv[3] = oMv.GetInverted().GetValue(1, 0);
	aNormalMv[4] = oMv.GetInverted().GetValue(1, 1);
	aNormalMv[5] = oMv.GetInverted().GetValue(1, 2);

	aNormalMv[6] = oMv.GetInverted().GetValue(2, 0);
	aNormalMv[7] = oMv.GetInverted().GetValue(2, 1);
	aNormalMv[8] = oMv.GetInverted().GetValue(2, 2);

	// Set shader's vertex mv matrix.
	int iLoc = m_pShader->GetUniformLocation("u_ModleViewMatrix");
	if(-1 != iLoc) glUniformMatrix4fv(iLoc, 1, GL_TRUE, aMv);

	// Set shader's normal mv matrix.
	iLoc = m_pShader->GetUniformLocation("u_NormalMatrix");
	if(-1 != iLoc) glUniformMatrix3fv(iLoc, 1, GL_FALSE, aNormalMv);

	// Set shader's vertex mvp matrix.
	iLoc = m_pShader->GetUniformLocation("u_ProjMatrix");
	if(-1 != iLoc) glUniformMatrix4fv(iLoc, 1, GL_TRUE, aMp);
}

/******************************************************************************/
VistaGLSLShader* VflShaderGeomRenderer::VflShaderDrawMode::GetShader()
{
	return m_pShader;
}
void VflShaderGeomRenderer::VflShaderDrawMode::SetShader(VistaGLSLShader* pShader)
{
	m_pShader = pShader;
}

vector<string>& VflShaderGeomRenderer::VflShaderDrawMode::GetReqiredArtibuts()
{
	return m_vecArttibutNames;
}

void VflShaderGeomRenderer::VflShaderDrawMode::SetReqiredArtibuts(
											const vector<string>& vecArttibuts)
{
	m_vecArttibutNames = vecArttibuts;
}
/*============================================================================*/
/*  END OF FILE					                                              */
/*============================================================================*/


