/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFL_SHADER_GEOMETRY_RENDERER_H
#define VFL_SHADER_GEOMETRY_RENDERER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <iostream>
#include <string>
#include <vector>

#include "VflGeometryRenderer.h"
#include <VistaFlowLib/VistaFlowLibConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisGeometry;
class VistaGLSLShader;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflShaderGeomRenderer : public VflGeometryRenderer
{
public:

	/**************************************************************************/
	/* CONSTRUCTORS / DESTRUCTOR                                              */
	/**************************************************************************/
	VflShaderGeomRenderer(VflVisGeometry *pOwner, bool bCreateDefaultDrawMode = true);
	virtual ~VflShaderGeomRenderer();

	/**
	 * Manage different draw modes.
	 * Every draw mode has is own shader and list of required attributes.
	 * You can write your own draw mode that extents this class if you need
	 * additional data in your shader
	 */
	class VISTAFLOWLIBAPI VflShaderDrawMode 
	{
	public:
		VflShaderDrawMode();
		virtual ~VflShaderDrawMode();

		/**
		 * This method updates the uniform values for u_ModleViewMatrix, 
		 * u_ProjMatrix and u_NormalMatrix.
		 */
		virtual void UpdateUniforms(VflVisGeometry* pGeom);

		VistaGLSLShader* GetShader();
		void			 SetShader(VistaGLSLShader* pShader);

		/**
		 * Getter/Setter for the required attribute vector.
		 * This vector contains the names of all attributes, that are required
		 * by the shader.
		 * The position within the vector determine to which VertexAttribArray
		 * the attribute is bound.
		 */
		std::vector<std::string>& GetReqiredArtibuts();
		void SetReqiredArtibuts(const std::vector<std::string>& vecArttibuts);

	protected:
		std::vector<std::string> m_vecArttibutNames;
		VistaGLSLShader* m_pShader;
	};

	/**************************************************************************/
	/* DRAW MODE                                                              */
	/**************************************************************************/
	int			        AddDrawMode(VflShaderDrawMode* pDrawMode);
	VflShaderDrawMode*	GetDrawMode(int iMode);

	virtual void SetDrawMode(int iMode);
	virtual int GetDrawMode() const;
	virtual int GetDrawModeCount() const;

	/**************************************************************************/
	/* VISGEOMETRYRENDERER API                                                */
	/**************************************************************************/
	virtual void DrawOpaque();
	//virtual void DrawTransparent();

	/**
	 * Returns the type of the geomety renderer as a string.
	 */
	virtual std::string GetType() const;

private:
	int		m_iDrawMode;

	std::vector<VflShaderDrawMode*> m_vecDrawModes;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLPARTICLERENDERER_H)
