/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  BaseAppTutorial.cpp	         RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA FlowLib Tutorials      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                        Rheinisch-Westfaelische      */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflVtkLookupTable.h"

#include <VistaBase/VistaStreamUtils.h>

#include <vtkLookupTable.h>
#include <vtkPiecewiseFunction.h>

using namespace std;

static void ConvertHSVAtoRGBA( float *pColors, int iColorCount )
{
	if (iColorCount < 1)
		return;

	float  aRGB[3];
	float *pCurColor = pColors;

	for (int i=0; i<iColorCount; ++i)
	{
		VistaColor::HSVtoRGB( pCurColor, aRGB );
		memcpy( pCurColor, aRGB, 3*sizeof( float ) );

		pCurColor += 4;
	}
}

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VflVtkLookupTable::VflVtkLookupTable()
{
	m_pLookupTable = vtkLookupTable::New();
	m_pLookupTable->Build();

}

VflVtkLookupTable::~VflVtkLookupTable()
{
	m_pLookupTable->Delete();
	m_pLookupTable = NULL;
}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
#pragma region Getter
std::list<float> VflVtkLookupTable::GetPiecewiseLinearFunction() const
{
	return m_liPLFDescription;
}

int VflVtkLookupTable::GetPiecewiseLinearFunction( std::list<float> &refPLFDescription ) const
{
	refPLFDescription = m_liPLFDescription;
	return static_cast<int>( m_liPLFDescription.size() );
}

vtkLookupTable* VflVtkLookupTable::GetLookupTable() const
{
	return m_pLookupTable;
}

bool VflVtkLookupTable::GetTableRange( float &fMin, float &fMax ) const
{	
	double aTemp[2];
	m_pLookupTable->GetTableRange( aTemp );
	fMin = float( aTemp[0] );
	fMax = float( aTemp[1] );

	return true;
}

bool VflVtkLookupTable::GetHueRange( float &fMin, float &fMax ) const
{
	double aTemp[2];
	m_pLookupTable->GetHueRange( aTemp );
	fMin = float( aTemp[0] );
	fMax = float( aTemp[1] );

	return true;
}

bool VflVtkLookupTable::GetSaturationRange( float &fMin, float &fMax ) const
{
	double aTemp[2];
	m_pLookupTable->GetSaturationRange( aTemp );
	fMin = float( aTemp[0] );
	fMax = float( aTemp[1] );

	return true;
}

bool VflVtkLookupTable::GetValueRange( float &fMin, float &fMax ) const
{
	double aTemp[2];
	m_pLookupTable->GetValueRange( aTemp );
	fMin = float( aTemp[0] );
	fMax = float( aTemp[1] );

	return true;
}

bool VflVtkLookupTable::GetAlphaRange( float &fMin, float &fMax ) const
{
	double aTemp[2];
	m_pLookupTable->GetAlphaRange( aTemp );
	fMin = float( aTemp[0] );
	fMax = float( aTemp[1] );

	return true;
}

std::string VflVtkLookupTable::GetScale() const
{
	int nScaleId = GetScaleId();
	switch(GetScaleId())
	{
	case VTK_SCALE_LINEAR:
		return "LINEAR";
	case VTK_SCALE_LOG10:
		return "LOG10";
	default:
		return "<unknown>";
	}
}

int VflVtkLookupTable::GetScaleId() const
{
	if(!m_pLookupTable)
		return -1;
	return m_pLookupTable->GetScale();
}

unsigned int VflVtkLookupTable::GetValueCount() const
{
	if( !m_pLookupTable )
		return 0;

	return m_pLookupTable->GetNumberOfTableValues();
}

bool VflVtkLookupTable::GetTableValues( std::vector<VistaColor> &vecValues )  const
{
	vtkIdType iCount = m_pLookupTable->GetNumberOfTableValues();

	vecValues.resize( iCount );

	for( vtkIdType i=0; i<iCount; ++i )
	{
		double* pColor = m_pLookupTable->GetTableValue(i);
		if (!pColor)
			continue;

		vecValues[i].SetValues( pColor, VistaColor::RGBA );
	}

	return true;
}

std::vector<float> VflVtkLookupTable::GetTableValues() const
{
	std::vector<float> vecValues;

	vtkIdType iCount = m_pLookupTable->GetNumberOfTableValues();

	vecValues.resize( 4*iCount );

	for( vtkIdType i=0; i<iCount; ++i )
	{
		double* pColor = m_pLookupTable->GetTableValue(i);
		if (!pColor)
			continue;

		vecValues[ 4*i + 0 ] = pColor[0];
		vecValues[ 4*i + 1 ] = pColor[1];
		vecValues[ 4*i + 2 ] = pColor[2];
		vecValues[ 4*i + 3 ] = pColor[3];
	}

	return vecValues;
}

bool VflVtkLookupTable::GetTableValue( unsigned int nIndex, VistaColor& rColor ) const
{
	if( nIndex >= GetValueCount() )
		return false;

	rColor.SetValues( m_pLookupTable->GetTableValue( nIndex ), VistaColor::RGBA );
	return true;
}

VistaColor VflVtkLookupTable::GetColor( float fScalar ) const
{
	double aColor[4];
	m_pLookupTable->GetColor(fScalar, aColor);
	aColor[3] = m_pLookupTable->GetOpacity(fScalar);

	return VistaColor(aColor, VistaColor::RGBA);
}

#pragma endregion Getter
/******************************************************************************/
#pragma region Setter

bool VflVtkLookupTable::SetPiecewiseLinearFunction( const std::list<float>& refPLFDescription )
{
	// sanity checks
	const int nCount = static_cast<int>( refPLFDescription.size() );

	if( nCount == 0 )
	{
		// don't use PLF, i.e. rebuild lookup table for given parameters
		m_pLookupTable->ForceBuild();
		m_liPLFDescription.clear();
		return true;
	}

	if( nCount<14 )
	{
		vstr::errp() << " [SharedVtkLUT::SetPLF] invalid value list size " 
			<< refPLFDescription.size() << endl;
		return false;
	}

	// parse the value list and put 'em into piecewise functions
	list<float>::const_iterator cit = refPLFDescription.begin();
	int iColorCount = (int) *(cit++);
	int iInterpolationSpace = (int) *(cit++);

	if( iColorCount <= 0 )
	{
		vstr::errp() << " [SharedVtkLUT::SetPLF] invalid color count "
			<< iColorCount << endl;
		return false;
	}

	if( iInterpolationSpace != PLF_RGBA && iInterpolationSpace != PLF_HSVA )
	{
		vstr::errp() << " [SharedVtkLUT::SetPLF] invalid interpolation space "
			<< iInterpolationSpace << endl;
		return false;
	}

	vtkPiecewiseFunction *aFuncs[4];
	for( int i=0; i<4; ++i )
	{
		aFuncs[i] = vtkPiecewiseFunction::New();
	}

	bool bEarlyOut = false;
	float fPos, fValue;

	for( int i=0; i<4 && !bEarlyOut; ++i )
	{
		bool bValid = false;
		while( cit != refPLFDescription.end() )
		{
			fPos = *(cit++);

			if( fPos < 0 )
				break;

			if( cit == refPLFDescription.end() )
			{
				bEarlyOut = true;
				break;
			}

			fValue = *(cit++);
			bValid = true;
			aFuncs[i]->AddPoint( fPos, fValue );
		}

		bEarlyOut |= !bValid;
	}

	if( bEarlyOut )
	{
		vstr::errp() << " [SharedVtkLUT::SetPLF] not enough function values..." << endl;

		for( int i=0; i<4; ++i )
			aFuncs[i]->Delete();

		return false;
	}

	// now go for some serious interpolation...
	float *aColors = new float[4*iColorCount];
	float *pColor = aColors;

	fPos = 0;
	float fDelta = 1.0f / (iColorCount - 1);

	//for each color
	for( int j=0; j<iColorCount; ++j )
	{
		//for each component
		for( int i=0; i<4; ++i )
		{
			*pColor = aFuncs[i]->GetValue( fPos );
			++pColor;
		}
		fPos += fDelta;
	}

	// finally, if we're talking about hsv colors, convert them to rgb
	if( iInterpolationSpace == PLF_HSVA )
		ConvertHSVAtoRGBA( aColors, iColorCount );

	// (really) finally, set the VTK colors
	m_pLookupTable->SetNumberOfTableValues( iColorCount );
	pColor = aColors;
	for( int i=0; i<iColorCount; ++i )
	{
		m_pLookupTable->SetTableValue( 
			i, pColor[0], pColor[1], pColor[2], pColor[3] );
		pColor += 4;
	}

	delete [] aColors;

	for( int i=0; i<4; ++i )
	{
		aFuncs[i]->Delete();
	}

	m_liPLFDescription = refPLFDescription;

	Notify( MSG_VALUES_CHANGED );

	return true;
}

bool VflVtkLookupTable::SetTableRange( float fMin, float fMax )
{		
	float aRange[2];
	if( !GetTableRange( aRange[0], aRange[1] ) )
		return false;

	if( aRange[0]==fMin && aRange[1]==fMax )
		return false;

	m_pLookupTable->SetTableRange( fMin, fMax );

	Notify( MSG_TABLE_RANGE_CHANGED );
	return true;
}


bool VflVtkLookupTable::SetHueRange( float fMin, float fMax )
{
	float aRange[2];
	if( !GetHueRange( aRange[0], aRange[1] ) )
		return false;

	if( aRange[0]==fMin && aRange[1]==fMax && m_liPLFDescription.empty() )
		return false;

	m_liPLFDescription.clear();
	m_pLookupTable->SetHueRange( fMin, fMax );
	Notify( MSG_HUE_RANGE_CHANGED );

	UpdateTableValues();
	return true;
}

bool VflVtkLookupTable::SetSaturationRange( float fMin, float fMax )
{
	float aRange[2];
	if( !GetSaturationRange( aRange[0], aRange[1] ) )
		return false;

	if( aRange[0]==fMin && aRange[1]==fMax && m_liPLFDescription.empty() )
		return false;

	m_liPLFDescription.clear();
	m_pLookupTable->SetSaturationRange( fMin, fMax );
	Notify( MSG_SATURATION_RANGE_CHANGED );

	UpdateTableValues();
	return true;
}

bool VflVtkLookupTable::SetValueRange( float fMin, float fMax )
{
	float aRange[2];
	if( !GetValueRange( aRange[0], aRange[1] ) )
		return false;

	if( aRange[0]==fMin && aRange[1]==fMax && m_liPLFDescription.empty() )
		return false;

	m_liPLFDescription.clear();
	m_pLookupTable->SetValueRange( fMin, fMax );
	Notify( MSG_VALUE_RANGE_CHANGED );

	UpdateTableValues();
	return true;
}

bool VflVtkLookupTable::SetAlphaRange( float fMin, float fMax )
{
	float aRange[2];
	if( !GetAlphaRange( aRange[0], aRange[1] ) )
		return false;

	if( aRange[0]==fMin && aRange[1]==fMax && m_liPLFDescription.empty() )
		return false;

	m_liPLFDescription.clear();
	m_pLookupTable->SetAlphaRange( fMin, fMax );
	Notify( MSG_ALPHA_RANGE_CHANGED );

	UpdateTableValues();
	return true;
}

bool VflVtkLookupTable::SetScale( const std::string &sScaleName )
{
	if( sScaleName == "LINEAR" )
		return SetScaleId( VTK_SCALE_LINEAR );
	else if( sScaleName == "LOG10" )
		return SetScaleId( VTK_SCALE_LOG10 );
	return false;
}

bool VflVtkLookupTable::SetScaleId( int nId )
{
	int nScale = GetScaleId();
	if( m_pLookupTable && ( nScale != nId ) )
	{
		m_liPLFDescription.clear();
		m_pLookupTable->SetScale(nId);
		Notify(MSG_SCALE_CHANGE);
		return true;
	}
	return false;
}

bool VflVtkLookupTable::SetValueCount( unsigned int iNumColors )
{
	if(!m_pLookupTable)
		return false;

	if( iNumColors<1 && iNumColors==GetValueCount() )
		return false;

	m_liPLFDescription.clear();
	m_pLookupTable->SetNumberOfTableValues( iNumColors );
	Notify( MSG_NUMBER_OF_COLORS_CHANGED );
	UpdateTableValues();
	return true;
}

bool VflVtkLookupTable::SetTableValues( const std::vector<VistaColor> &vecValues )
{
	size_t nNumValues = vecValues.size();

	if( nNumValues<1 )
		return false;

	if( nNumValues != GetValueCount() )
	{
		m_pLookupTable->SetNumberOfTableValues( nNumValues );
		Notify( MSG_NUMBER_OF_COLORS_CHANGED );
	}

	for( size_t n = 0; n<nNumValues; ++n )
	{
		double aColor[4];
		vecValues[n].GetValues( aColor, VistaColor::RGBA );
		m_pLookupTable->SetTableValue( n, aColor );
	}

	Notify( MSG_VALUES_CHANGED );
	return true;
}

bool VflVtkLookupTable::SetTableValues( const std::vector<float>& vecValues )
{
	size_t nNumValues = vecValues.size();

	if( nNumValues<4 || nNumValues%4!=0 )
		return false;

	nNumValues/=4;

	if( nNumValues != GetValueCount() )
	{
		m_pLookupTable->SetNumberOfTableValues( nNumValues );
		Notify( MSG_NUMBER_OF_COLORS_CHANGED );
	}

	for( size_t n = 0; n<nNumValues; ++n )
	{
		double aColor[4];

		aColor[0] =  vecValues[ 4*n + 0];
		aColor[1] =  vecValues[ 4*n + 1];
		aColor[2] =  vecValues[ 4*n + 2];
		aColor[3] =  vecValues[ 4*n + 3];

		m_pLookupTable->SetTableValue( n, aColor );
	}

	Notify( MSG_VALUES_CHANGED );
	return true;
}

bool VflVtkLookupTable::SetTableValue( unsigned int nIndex, const VistaColor& rColor )
{
	if( nIndex >= GetValueCount() )
		return false;

	double aColor[4];
	rColor.GetValues( aColor, VistaColor::RGBA );

	m_pLookupTable->SetTableValue( nIndex, aColor );

	Notify( MSG_VALUES_CHANGED );
	return true;
}

#pragma endregion Setter
/******************************************************************************/
#pragma region Reflectionable API
REFL_IMPLEMENT_FULL( VflVtkLookupTable, IVflLookupTable );

static IVistaPropertyGetFunctor *aCgFunctors[] =
{

	new TVistaPropertyGet< std::list<float>, VflVtkLookupTable, VistaProperty::PROPT_LIST>(
			"PIECEWISE_LINEAR", SsReflectionName,
			&VflVtkLookupTable::GetPiecewiseLinearFunction
		),
	new TVistaPropertyGet< std::string, VflVtkLookupTable, VistaProperty::PROPT_STRING>(
			"SCALE", SsReflectionName,
			&VflVtkLookupTable::GetScale
		),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet< const std::list<float> &, std::list<float>, VflVtkLookupTable >(
			"PIECEWISE_LINEAR", SsReflectionName,
			&VflVtkLookupTable::SetPiecewiseLinearFunction
		),
	new TVistaPropertySet<const std::string &, string, VflVtkLookupTable>(
			"SCALE", SsReflectionName,
			&VflVtkLookupTable::SetScale
		),
	NULL
};

#pragma endregion Reflectionable API
/******************************************************************************/
void VflVtkLookupTable::UpdateTableValues()
{
	if(m_liPLFDescription.empty())
	{
		m_pLookupTable->ForceBuild();
		m_pLookupTable->Modified();
		Notify( MSG_VALUES_CHANGED );
	}
}
/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

