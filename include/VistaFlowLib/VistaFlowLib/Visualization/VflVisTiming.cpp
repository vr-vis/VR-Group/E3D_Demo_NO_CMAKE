/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES					                                                  */
/*============================================================================*/
#include "VflVisTiming.h" 
#include <cmath>
#include <algorithm>

#if defined(SUNOS)
#include <sunmath.h>
#include <math.h>
#endif


/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
static const double DEFAULT_LOOP_TIME = 10.0f;


/*============================================================================*/
/* IMPLEMENTATION			                                                  */
/*============================================================================*/
VflVisTiming::VflVisTiming( double dCreationTime )
:	m_dCurrentVisTime( 0.0 )
,   m_dLoopTime( DEFAULT_LOOP_TIME )
,	m_bAnimationPlaying( true )
,	m_dCurrentTime( dCreationTime )
,   m_dFrameTimeStamp( dCreationTime )
,   m_dStartupTimeStamp( dCreationTime )
,   m_dStartVisTime( 0.0 )
,   m_dEndVisTime( 1.0 )
,   m_dAnimationSpeedFactor( 1.0 )
,   m_dVisRangeSpeedFactor( 1.0 )
{
	m_dBaseLoopTime = GetLoopTime();
}

VflVisTiming::~VflVisTiming()
{ }


bool VflVisTiming::SetVisualizationTime( double dVisTime )
{
	m_dCurrentVisTime = std::max( dVisTime, m_dStartVisTime );
	if( m_dCurrentVisTime > m_dEndVisTime )
	{
		m_dCurrentVisTime = fmod( m_dCurrentVisTime, m_dEndVisTime - m_dStartVisTime );
		m_dCurrentVisTime += m_dStartVisTime;
	}

	// Vis time might lie in a time window [start, end] which does represent how much
	// of this window has been passed. For this, the vis time needs to be renormalized
	// with the time window.
	const double dRelVisTime = ( m_dCurrentVisTime - m_dStartVisTime )
		/ ( m_dEndVisTime - m_dStartVisTime );
	const double dPassedLoopTime = dRelVisTime * m_dLoopTime;
	SetFrameTimeStamp( m_dCurrentTime - dPassedLoopTime );

    this->Notify(MSG_VISTIME_CHG);
	return true;
}


bool VflVisTiming::SetBaseLoopTime( double dLoopTime )
{
	if( dLoopTime <= 0.0 )
	{
		return false;
	}
		
	m_dBaseLoopTime = dLoopTime;
	UpdateTiming();	

	this->Notify( MSG_LOOPTIME_CHG );
	return true;
}


bool VflVisTiming::SetAnimationSpeed(double dFactor)
{
	if( dFactor <= 0.0 )
	{
		return false;
	}

	// A higher speed factor means a shorter animation time. For efficiency reasons
	// the time multiplier is what gets stored.
	m_dAnimationSpeedFactor = 1.0 / dFactor;
	
	UpdateTiming();
	Notify( MSG_ANIMSPEED_CHG );
	return true;
}


void VflVisTiming::SetAnimationPlaying( bool bSet, double dCurrentTime )
{
	if( m_bAnimationPlaying == bSet )
	{
		return;
	}

	m_bAnimationPlaying = bSet;

	if( m_bAnimationPlaying )
	{
		SetFrameTimeStamp( dCurrentTime - ( GetVisualizationTime() - m_dStartVisTime ) / ( m_dEndVisTime - m_dStartVisTime ) * GetLoopTime() );
	}

    this->Notify( MSG_ANIMSTATUS_CHG );
}

void VflVisTiming::SetAnimationPlaying(bool bSet)
{
	this->SetAnimationPlaying( bSet, this->GetCurrentClock() );
}


void VflVisTiming::ToggleAnimation( double dCurrentTime )
{ 
	SetAnimationPlaying( !GetAnimationPlaying(), dCurrentTime );
}

void VflVisTiming::ToggleAnimation()
{
	this->ToggleAnimation( this->GetCurrentClock() );
}


double VflVisTiming::SetCurrentClock( double dCurTime )
{
	m_dCurrentTime = dCurTime;
	return m_dCurrentTime;
}


void VflVisTiming::UpdateAnimationTime()
{
	// proceed in animation by computing the new normalized visualization time
	if( GetAnimationPlaying() )
	{
        const double dVisNormalized = ( m_dCurrentTime - m_dFrameTimeStamp ) / m_dLoopTime;
		SetVisualizationTime(  m_dStartVisTime + dVisNormalized * ( m_dEndVisTime - m_dStartVisTime ) );
	}
}


void VflVisTiming::SetVisTimeRange( double dStart, double dEnd, bool bKeepUserTime )
{
    if ( dStart < 0.0 || dEnd > 1.0 || dStart > dEnd )
	{
        return;
	}

    m_dStartVisTime = dStart;
    m_dEndVisTime	= dEnd;

	if( bKeepUserTime )
	{
		m_dVisRangeSpeedFactor = dEnd - dStart;
	}
	else
	{
		m_dVisRangeSpeedFactor = 1.0;
	}
	
	UpdateTiming();
	this->Notify( MSG_RANGE_CHG );
}

bool VflVisTiming::GetVisTimeRange(double& dStart, double& dEnd) const
{
    dStart	= m_dStartVisTime;
    dEnd	= m_dEndVisTime;

    return true;
}


bool VflVisTiming::Last()
{
	return this->SetVisualizationTime( m_dEndVisTime );
}

bool VflVisTiming::First()
{
	return this->SetVisualizationTime( m_dStartVisTime );
}


double VflVisTiming::SetFrameTimeStamp( double dCurrentClock )
{
	m_dFrameTimeStamp = dCurrentClock;
	return m_dFrameTimeStamp;
}


void VflVisTiming::UpdateTiming()
{
	// compute real value, i.e. modified by all loop time factors
	const double dRealLoopTime = m_dBaseLoopTime * m_dVisRangeSpeedFactor
		* m_dAnimationSpeedFactor;
	
	// reset time stamp to ensure that the current vis time is not changed
	// as the normalized visualization time is computed by the (current time - time stamp)/loop time,
	// a change of the loop time will "move" the visualization time, which is not desired
	SetFrameTimeStamp ( -( dRealLoopTime * ( m_dCurrentTime - m_dFrameTimeStamp ) / m_dLoopTime ) + m_dCurrentTime );

	m_dLoopTime = dRealLoopTime;
}

/*============================================================================*/
/* END OF FILE					                                              */
/*============================================================================*/

