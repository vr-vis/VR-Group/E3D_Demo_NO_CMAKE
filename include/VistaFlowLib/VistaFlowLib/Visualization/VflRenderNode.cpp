/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "VflRenderNode.h"

// Directly needed by RenderNode.
#include <VistaKernel/GraphicsManager/VistaOpenGLNode.h>
#include <VistaKernel/GraphicsManager/VistaNodeBridge.h>
#include <VistaKernel/GraphicsManager/VistaOpenGLDraw.h>
#include <VistaKernel/GraphicsManager/VistaGroupNode.h>
#include <VistaKernel/GraphicsManager/VistaTransformNode.h>
#include <VistaKernel/VistaSystem.h>
#include <VistaAspects/VistaTransformable.h>
#include <VistaFlowLib/Visualization/VflVisController.h>
#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/VflRenderable.h>

#include <VistaMath/VistaBoundingBox.h>
#include <VistaInterProcComm/Concurrency/VistaMutex.h>

#include <VistaKernel/DisplayManager/VistaDisplayManager.h>
#include <VistaKernel/DisplayManager/VistaViewport.h>

#include <vtkRenderer.h>
#include <vtkOpenGLRenderer.h>
#include <vtkObjectFactory.h>

#ifdef WIN32
	#include <vtkWin32OpenGLRenderWindow.h>
#elif defined(DARWIN)
        #include <vtkCocoaRenderWindow.h>
#else
	#include <vtkXOpenGLRenderWindow.h>
#endif

#include <limits>
#include <cassert>
#include <algorithm>

using namespace std;


/*============================================================================*/
/* DEFINES																	  */
/*============================================================================*/
#ifdef WIN32
	class vtkWin32OpenGLRenderWindow;
	typedef vtkWin32OpenGLRenderWindow VTKOGLRENWIN;
	#pragma warning(disable : 4786)
#elif defined(DARWIN)
	class vtkCocoaRenderWindow;
	typedef vtkCocoaRenderWindow VTKOGLRENWIN;
#else
	class vtkXOpenGLRenderWindow;
	typedef vtkXOpenGLRenderWindow VTKOGLRENWIN;
#endif


/*============================================================================*/
/* HELPER CLASSES															  */
/*============================================================================*/
/*!
 * This is an OpenGL callback used by the VistaOpenGLNode to do the drawing
 * when its update function is called. This class is placed here as it is
 * supposed to only be used internally by the RenderNode and hence should
 * be made as invisible as possible for users of the RenderNode.
 */
class VflRenderNodeOGLCallback : public IVistaOpenGLDraw
{
public:
	VflRenderNodeOGLCallback( VflRenderNode *pOwner )
	:	m_pOwner(pOwner)
	{ }

	virtual ~VflRenderNodeOGLCallback()
	{ }

	virtual bool Do()
	{ 
		if( m_pOwner == NULL )
			return false;

		m_pOwner->DrawRenderables();

		return true;
	}

	virtual bool GetBoundingBox( VistaBoundingBox& oBB )
	{
		VistaVector3D v3Min, v3Max;
		m_pOwner->GetVisBounds( v3Min, v3Max );

		float aMin[3], aMax[3];
		v3Min.GetValues( aMin );
		v3Max.GetValues( aMax );

		oBB = VistaBoundingBox( aMin, aMax );
		
		return true;
	}

private:
	VflRenderNode* m_pOwner;
};


class RendererHelper : public vtkOpenGLRenderer
{
public:
	virtual ~RendererHelper()
	{ }

	static RendererHelper* New();

	void SetRenderNode( VflRenderNode* pRenderNode )
	{
		m_pRenderNode = pRenderNode;
	}

protected:
	int UpdateGeometry()
	{
		if( m_pRenderNode )
		{
			m_pRenderNode->DrawRenderables();
			return m_pRenderNode->GetNumberOfRenderables();
		}

		return 0;
	}
private:
	VflRenderNode* m_pRenderNode;
};

vtkStandardNewMacro( RendererHelper );


class VflRenderWindow : public VTKOGLRENWIN
{
public:
	virtual ~VflRenderWindow() { }

	static VflRenderWindow* New();
	void MakeCurrent () { }
};

vtkStandardNewMacro( VflRenderWindow );


/*============================================================================*/
/* CON-/DESTRUCTORS                                                           */
/*============================================================================*/
VflRenderNode::VflRenderNode(VistaGroupNode *pParentNode,
	IVistaNodeBridge *pNodeBridge, vtkRenderWindow *pVtkRenderWindow)
:	m_bIsValid(false)
,   m_pVisCtrl(NULL)
,   m_pTransformNode(NULL)
,   m_pVisRefPointNode(NULL)
,   m_pOGLNode(NULL)
,   m_pVisTiming(NULL)
,   m_bHasLocalTiming(true)
,   m_pListMutex(new VistaMutex)
,   m_sName("")
,   m_bIsFrameRateCappingOn(false)
,   m_dMinFrameTime(0.02) // 50 fps
,	m_dBoundsTimeStamp(0.0)
,   m_bBoundsValid(false)
,   m_bBoundsSetByUser(false)
,   m_pVtkRenderer(NULL)
,	m_pVtkRenderWindow(pVtkRenderWindow)
{
	// Create the TransformNode used to transform the RenderNode.
	m_pTransformNode = pNodeBridge->NewTransformNode(pParentNode,
		pNodeBridge->NewTransformNodeData(), "VflRenderNode - transform node");
	
	// Create the TransformNode used to hold the vis ref point.
	m_pVisRefPointNode = pNodeBridge->NewTransformNode(m_pTransformNode,
		pNodeBridge->NewTransformNodeData(), "VflRenderNode - vis ref point");
	m_pVisRefPointNode->SetTranslation(0.0f, 0.0f, 0.0f);
	
	// Create the OpenGLNode which is used to receive render calls from the
	// underlying scene graph.
	m_pOGLNode =  pNodeBridge->NewOpenGLNode(m_pTransformNode,
		new VflRenderNodeOGLCallback(this),
		pNodeBridge->NewOpenGLNodeData(), "VflRenderNode - OpenGL node");
	m_pOGLNode->Init();

	// Set the right node relations.
	pParentNode->AddChild(m_pTransformNode);
	m_pTransformNode->AddChild(m_pVisRefPointNode);
	m_pTransformNode->AddChild(m_pOGLNode);

	// Some basic initial state.
	m_v3BoundsMin		= VistaVector3D(-1.0f, -1.0f, -1.0f);
	m_v3BoundsMax		= VistaVector3D( 1.0f,  1.0f,  1.0f);
	m_v3TargetBoundsMin = VistaVector3D(-1.0f, -1.0f, -1.0f);
	m_v3TargetBoundsMax = VistaVector3D( 1.0f,  1.0f,  1.0f);

	m_v3LightDirGlobal = VistaVector3D(0.0f, 0.0f, -1.0f, 0.0f);
	m_v3ViewDirGlobal = VistaVector3D(0.0f, 0.0f, -1.0f, 0.0f);
}

VflRenderNode::~VflRenderNode()
{
	if( m_pVisCtrl )
		m_pVisCtrl->RemoveRenderNode( this );

	if( m_pVtkRenderer )
	{
		m_pVtkRenderer->SetRenderWindow( NULL );
		m_pVtkRenderer->Delete();
	}

	if( m_pVtkRenderWindow )
	{
		m_pVtkRenderWindow->RemoveRenderer( m_pVtkRenderer );
		m_pVtkRenderWindow->Delete();
	}

	if( m_bHasLocalTiming )
		delete m_pVisTiming;

	delete m_pListMutex;
	
	if( m_pOGLNode )
	{
		// Delete the OpenGLDrawInterface we gave to the OpenGLNode.
		if( m_pOGLNode->GetExtension() )
			delete m_pOGLNode->GetExtension();
		m_pOGLNode->SetExtension( NULL );
	}
	delete m_pOGLNode;

	delete m_pVisRefPointNode;
	delete m_pTransformNode;
}


/*============================================================================*/
/* RENDERNODE & RENDERABLE MANAGEMENT                                         */
/*============================================================================*/
bool VflRenderNode::Init()
{
	m_bIsValid = false;

	if( !m_pVisCtrl )
		return false;

	// Create VisTiming.
	if( m_bHasLocalTiming )
	{
		delete m_pVisTiming;

		double dSystemTime = m_pVisCtrl->GetVistaSystem()->GetFrameClock();
		m_pVisTiming = new VflVisTiming( dSystemTime );
	}
	
	if( !m_pVtkRenderer )
	{
		RendererHelper *pHelper = dynamic_cast<RendererHelper*>(
			RendererHelper::New() );
		pHelper->SetRenderNode( this );
		m_pVtkRenderer = pHelper;
	}

	if( !m_pVtkRenderWindow )
	{
		m_pVtkRenderWindow = VflRenderWindow::New();
		m_pVtkRenderWindow->AddRenderer( m_pVtkRenderer );

#if VTK_MAJOR_VERSION > 5
		m_pVtkRenderWindow->InitializeFromCurrentContext();
#else
		// @todo!
#endif

		// Update vtkWindow correctly ( at least according to first viewport )
		std::map<std::string, VistaViewport*> mapViewports =
			m_pVisCtrl->GetVistaSystem()->GetDisplayManager()->GetViewportsConstRef();
		VistaViewport* pViewport = mapViewports.begin()->second;
		int nVPWidth = 640;
		int nVPHeight = 480;
		if( pViewport != NULL )
			pViewport->GetViewportProperties()->GetSize( nVPWidth, nVPHeight );
		
		UpdateVtkWindowSize( nVPWidth, nVPHeight );
	}	

	return (m_bIsValid = true);
}

bool VflRenderNode::SetVisController(VflVisController *pVisCtrl)
{
	if(m_pVisCtrl == pVisCtrl)
		return true;

	m_pVisCtrl = pVisCtrl;

	// Init to adapt to no vis controller.
	return Init();
}

VflVisController* VflRenderNode::GetVisController() const
{
	return m_pVisCtrl;
}


bool VflRenderNode::AddRenderable(IVflRenderable *pRen)
{
	m_pListMutex->Lock();

	list<IVflRenderable*>::iterator it = m_liRenderables.begin();

	// See, if the renderable is already in the renderable list.
	while(it != m_liRenderables.end())
	{
		if((*it) == pRen)
		{
			m_pListMutex->Unlock();
			return false;
		}

		++it;
	}

	// Add to list of Renderables.
	m_liRenderables.push_back(pRen);
	pRen->SetRenderNode(this);

	// Add to update list.
	int iMode = pRen->GetRegistrationMode();
	if(iMode & IVflRenderable::OLI_UPDATE)
	{
		m_liUpdateList.push_back(pRen);
	}

	// Add to draw opaque list.
	if(iMode & IVflRenderable::OLI_DRAW_OPAQUE)
	{
		m_liDrawOpaqueList.push_back(pRen);
	}

	// Add to draw transparent list.
	if(iMode & IVflRenderable::OLI_DRAW_TRANSPARENT)
	{
		m_liDrawTransparentList.push_back(pRen);
	}

	// Add to draw 2D list.
	if(iMode & IVflRenderable::OLI_DRAW_2D)
	{
		m_liDraw2DList.push_back(pRen);
	}

	m_pListMutex->Unlock();

	return true;
}

bool VflRenderNode::RemoveRenderable(IVflRenderable *pRen)
{
	m_pListMutex->Lock();

	pRen->SetRenderNode(NULL);

	list<IVflRenderable*>::iterator itBegin, itEnd, it;
	itBegin = m_liRenderables.begin();
	itEnd = m_liRenderables.end();

	// See, if the renderable is in the renderable list. If so, remove it from
	// the list of renderables.
	bool bRenderableRemoved = false;
	it = std::find(itBegin, itEnd, pRen);
	if(it != itEnd)
	{
		m_liRenderables.erase(it);

		int iMode = pRen->GetRegistrationMode();
			
		// Remove from update list.
		if(iMode & IVflRenderable::OLI_UPDATE)
		{
			m_liUpdateList.remove(pRen);
		}

		// Remove from draw opaque list.
		if(iMode & IVflRenderable::OLI_DRAW_OPAQUE)
		{
			m_liDrawOpaqueList.remove(pRen);
		}

		// Remove from draw transparent list.
		if(iMode & IVflRenderable::OLI_DRAW_TRANSPARENT)
		{
			m_liDrawTransparentList.remove(pRen);
		}

		// Remove from draw 2D list.
		if(iMode & IVflRenderable::OLI_DRAW_2D)
		{
			m_liDraw2DList.remove(pRen);
		}

		bRenderableRemoved = true;
	}

	m_pListMutex->Unlock();
	return bRenderableRemoved;
}


int VflRenderNode::GetNumberOfRenderables() const
{
	return static_cast<int>(m_liRenderables.size());
}

void VflRenderNode::SetVisTiming(VflVisTiming *pVisTiming)
{
	if(m_bHasLocalTiming)
	{
		if(m_pVisTiming && m_pVisTiming != pVisTiming)
			delete m_pVisTiming;
	}

	m_pVisTiming = pVisTiming;
	m_bHasLocalTiming = false;
}

IVflRenderable * VflRenderNode::GetRenderable( const unsigned int i )
{
	if( i < m_liRenderables.size() )
	{
	  std::list<IVflRenderable*>::iterator it = m_liRenderables.begin();
	  for( unsigned int cnt = 0; cnt < i; ++cnt )
		++it;
	  return *it;
	}
	else
	  return NULL;
}

bool VflRenderNode::Update(double dSystemTime)
{
	if(!m_bIsValid)
		return false;

	// VisTiming update.
	m_pVisTiming->SetCurrentClock(dSystemTime);
	m_pVisTiming->UpdateAnimationTime();

	// Refresh matrices sincs the RenderNode's TransformNode could have been
	// modified (e.g. in a XML file).
	RefreshMatrices();

	// Transform the world view & light info into the local coordinate frame.
	// View info.
	m_v3ViewPosLocal		= m_mWorldInvTrans.Transform(m_v3ViewPosGlobal);
	m_qViewOriLocal			= VistaQuaternion(m_mWorldInvTrans) * m_qViewOriGlobal;
	m_qViewOriLocal.Normalize();
	m_v3ViewDirLocal		= m_mWorldInvTrans.Transform(m_v3ViewDirGlobal);
	m_v3ViewDirLocal.Normalize();
	m_v3ViewDirLocal[3]		= 0.0f;
	// Light info.
	m_v3LightDirLocal		= m_mWorldInvTrans.Transform(m_v3LightDirGlobal);
	m_v3LightDirLocal.Normalize();
	m_v3LightDirLocal[3]	= 0.0f;

	// Loop over the renderables and call their individual update functions.
	m_pListMutex->Lock();

	list<IVflRenderable*>::iterator it = m_liUpdateList.begin();
	while(it != m_liUpdateList.end())
	{
		assert((*it)
			&& "Ooops, there shouldn't be any invalid renderables in here!");

		(*it)->Update();

		++it;
	}

	m_pListMutex->Unlock();

	return true;
}

bool VflRenderNode::DrawRenderables()
{
	if(!m_bIsValid)
		return false;
 
	// Loop over the renderables and call their individual draw functions.
	// NOTE: Since the loops are executed seperately for each RenderNode
	//		 the calls to the below render functions will eventually be
	//		 interleaved. This might lead to side effects if drawing
	//		 transparent and opaque entities which are managed by different
	//		 RenderNodes.
	
	// Acquire list lock so no-one fiddles around with the list while
	// the drawing is happening.
 	m_pListMutex->Lock();

	// First, the opaque pass.
	list<IVflRenderable*>::iterator it = m_liDrawOpaqueList.begin();
	while(it != m_liDrawOpaqueList.end())
	{
		// We only do this assertion once here, since later loops would
		// basically do there very same thing. And that hasn't got to be :-).
		assert((*it)
			&& "Ooops, there shouldn't be any invalid renderable in here!");

		if((*it)->GetProperties()->GetVisible())
		   (*it)->DrawOpaque();
		++it;
	}

	// Then, the transparent pass.
	it = m_liDrawTransparentList.begin();
	while(it != m_liDrawTransparentList.end())
	{
		if((*it)->GetProperties()->GetVisible())
		   (*it)->DrawTransparent();
		++it;
	}

	// Finally, the 2D pass.
	it = m_liDraw2DList.begin();
	while(it != m_liDraw2DList.end())
	{
		if((*it)->GetProperties()->GetVisible())
		   (*it)->Draw2D();
		++it;
	}
	
	// Return the lock.
	m_pListMutex->Unlock();

	return true;
}

VflVisTiming* VflRenderNode::GetVisTiming() const
{
	return m_pVisTiming;
}

/*============================================================================*/
/* TRANSFORMATION MANAGEMENT                                                  */
/*============================================================================*/
bool VflRenderNode::ResetTransformation()
{
	if(!m_bBoundsSetByUser)
	{
		if(!RecomputeVisBounds()
			|| m_v3BoundsMin[0] > m_v3BoundsMax[0]
			|| m_v3BoundsMin[1] > m_v3BoundsMax[1]
			|| m_v3BoundsMin[2] > m_v3BoundsMax[2])
		{
			// If the bounds could not be made valid, simply reset the
			// RenderNode's transformation matrix to the identity matrix.
			VistaTransformMatrix mId;
			mId.SetToIdentity();

			SetTransform(mId);

			return false;
		}
	}

	// TODO_HIGH: Comment this!
	// Calc the scale.
	VistaVector3D v3DeltaTargetBounds
		= VistaVector3D(m_v3TargetBoundsMax - m_v3TargetBoundsMin);
	VistaVector3D v3DeltaBounds
		= VistaVector3D(m_v3BoundsMax - m_v3BoundsMin);	
	
	float fHelper, fScale = std::numeric_limits<float>::max();

	for(int i=0; i<3; ++i)
	{
		fHelper = v3DeltaTargetBounds[i] / v3DeltaBounds[i];
		fScale = min(fHelper, fScale);
	}
	
	SetScaleUniform(fScale);

	// Reset rotation.
	SetRotation(VistaQuaternion(0.0f, 0.0f, 0.0f, 1.0f));

	// Calc translation.
	VistaVector3D v3TargetBoundsCenter
		= VistaVector3D(0.5f * (m_v3TargetBoundsMin + m_v3TargetBoundsMax));
	VistaVector3D v3BoundsCenter
		= VistaVector3D(0.5f * (m_v3BoundsMin + m_v3BoundsMax));
	
	v3BoundsCenter = m_mTrans.Transform(v3BoundsCenter);
	v3TargetBoundsCenter -= v3BoundsCenter;

	Translate(v3TargetBoundsCenter);

	return true;
}

IVistaTransformable* VflRenderNode::GetTransformable() const
{
	return static_cast<IVistaTransformable*>(m_pTransformNode);
}

IVistaTransformable* VflRenderNode::GetVisRefPointTransformable() const
{
	return static_cast<IVistaTransformable*>(m_pVisRefPointNode);
}

bool VflRenderNode::Translate(const VistaVector3D &v3Trans)
{
	if(!m_pTransformNode)
		return false;

	bool bResult = m_pTransformNode->Translate(v3Trans);
	RefreshMatrices();

	return bResult;
}

bool VflRenderNode::SetTranslation(const VistaVector3D &v3Trans)
{
	if(!m_pTransformNode)
		return false;
	
	bool bResult = m_pTransformNode->SetTranslation(v3Trans);
	RefreshMatrices();

	return bResult;
}
	
VistaVector3D VflRenderNode::GetTranslation() const
{
	VistaVector3D v3Helper;

	if(m_pTransformNode)
		m_pTransformNode->GetTranslation(v3Helper);

	return v3Helper;
}

bool VflRenderNode::GetTranslation(VistaVector3D &v3Trans) const
{
	if(!m_pTransformNode)
		return false;

	return m_pTransformNode->GetTranslation(v3Trans);
}

bool VflRenderNode::Rotate(const VistaQuaternion &qRot)
{
	if(!m_pTransformNode)
		return false;

	// TODO_HIGH: Write docu for this!
	VistaVector3D v3VisRefPoint;
	m_pVisRefPointNode->GetTranslation(v3VisRefPoint);
	v3VisRefPoint[3] = 1.0f;
	v3VisRefPoint = m_mTrans.Transform(v3VisRefPoint);

	bool bResult = m_pTransformNode->Translate(-1.0f * v3VisRefPoint);
	bResult &= m_pTransformNode->Rotate(qRot);
	bResult &= m_pTransformNode->Translate(v3VisRefPoint);

	RefreshMatrices();

	return bResult;
}
	
bool VflRenderNode::SetRotation(const VistaQuaternion &qRot)
{
	if(!m_pTransformNode)
		return false;

	bool bResult = m_pTransformNode->SetRotation(qRot);
	RefreshMatrices();

	return bResult;
}

VistaQuaternion VflRenderNode::GetRotation() const
{
	VistaQuaternion qHelper;

	if(m_pTransformNode)
		m_pTransformNode->GetRotation(qHelper);

	return qHelper;
}

bool VflRenderNode::GetRotation(VistaQuaternion &qRot)
{
	if(!m_pTransformNode)
		return false;

	return m_pTransformNode->GetRotation(qRot);
}

bool VflRenderNode::ScaleUniformly(float fScale)
{
	return Scale(fScale, fScale, fScale);
}

bool VflRenderNode::SetScaleUniform(float fScale)
{
	return SetScale(fScale, fScale, fScale);
}

bool VflRenderNode::GetScaleUniform(float &fScale) const
{
	float fXScale, fYScale, fZScale;

	m_pTransformNode->GetScale(fXScale, fYScale, fZScale);

	fScale = max(fXScale, fYScale);
	fScale = max(fScale, fZScale);

	if(fXScale == fYScale && fXScale == fZScale)
		return true;

	return false;
}

float VflRenderNode::GetScaleUniform() const
{
	float fScale;
	GetScaleUniform(fScale);

	return fScale;
}

bool VflRenderNode::Scale(float fXScale, float fYScale, float fZScale)
{
	if(!m_pTransformNode)
		return false;
	
	VistaVector3D v3VisRefPoint;
	m_pVisRefPointNode->GetTranslation(v3VisRefPoint);
	v3VisRefPoint[3] = 1.0f;
	v3VisRefPoint = m_mTrans.Transform(v3VisRefPoint);

	bool bResult = m_pTransformNode->Translate(-1.0f * v3VisRefPoint);
	bResult &= m_pTransformNode->Scale(fXScale, fYScale, fZScale);
	bResult &= m_pTransformNode->Translate(v3VisRefPoint);

	RefreshMatrices();
	return bResult;
}

bool VflRenderNode::SetScale(float fXScale, float fYScale, float fZScale)
{
	if(!m_pTransformNode)
		return false;

	bool bResult = m_pTransformNode->SetScale(fXScale, fYScale, fZScale);
	RefreshMatrices();

	return bResult;
}

bool VflRenderNode::GetScale(float &fXScale, float &fYScale, float &fZScale) const
{
	if(!m_pTransformNode)
		return false;

	return m_pTransformNode->GetScale(fXScale, fYScale, fZScale);
}

bool VflRenderNode::Transform(const VistaTransformMatrix &mTrans)
{
	if(!m_pTransformNode)
		return false;

	VistaTransformMatrix mNewTrans = mTrans * m_mTrans;
	bool bResult = m_pTransformNode->SetTransform(mNewTrans);
	RefreshMatrices();

	return bResult;
}

bool VflRenderNode::SetTransform(const VistaTransformMatrix &mTrans)
{
	if(!m_pTransformNode)
		return false;

	bool bResult = m_pTransformNode->SetTransform(mTrans);
	RefreshMatrices();

	return bResult;
}

VistaTransformMatrix VflRenderNode::GetTransform() const
{
	return m_mTrans;
}
	
bool VflRenderNode::GetTransform(VistaTransformMatrix &mTrans) const
{
	if(!m_pTransformNode)
		return false;

	mTrans = m_mTrans;

	return true;
}

VistaTransformMatrix VflRenderNode::GetInverseTransform() const
{
	return m_mInvTrans;
}
	
bool VflRenderNode::GetInverseTransform(VistaTransformMatrix &mInvTrans) const
{
	if(!m_pTransformNode)
		return false;

	mInvTrans = m_mInvTrans;

	return true;
}

VistaTransformMatrix VflRenderNode::GetWorldTransform() const
{
	return m_mWorldTrans;
}

bool VflRenderNode::GetWorldTransform(VistaTransformMatrix &mTrans) const
{
	if(!m_pTransformNode)
		return false;

	mTrans = m_mWorldTrans;

	return true;
}

VistaTransformMatrix VflRenderNode::GetWorldInverseTransform() const
{
	return m_mWorldInvTrans;
}

bool VflRenderNode::GetWorldInverseTransform(VistaTransformMatrix &mInvTrans) const
{
	if(!m_pTransformNode)
		return false;

	mInvTrans = m_mWorldInvTrans;

	return true;
}
bool VflRenderNode::SetVisRefPoint(const VistaVector3D &v3VisRefPoint)
{
	if(!m_pVisRefPointNode)
		return false;

	m_pVisRefPointNode->SetTranslation(v3VisRefPoint);

	return true;
}

VistaVector3D VflRenderNode::GetVisRefPoint() const
{
	VistaVector3D v3VisRefPoint;

	if(m_pVisRefPointNode)
		m_pVisRefPointNode->GetTranslation(v3VisRefPoint);

	return v3VisRefPoint;
}

bool VflRenderNode::GetVisRefPoint(VistaVector3D &v3VisRefPoint)
{
	if(!m_pVisRefPointNode)
		return false;

	m_pVisRefPointNode->GetTranslation(v3VisRefPoint);

	return true;;
}


/*============================================================================*/
/* VISUALIZATION BOUNDS                                                       */
/*============================================================================*/
bool VflRenderNode::GetVisBounds(VistaVector3D &v3Min, VistaVector3D &v3Max)
{
	bool bResult = false;

	if(!m_bBoundsValid && !m_bBoundsSetByUser)
	{
		RecomputeVisBounds();
		bResult = true;
	}

	v3Min = m_v3BoundsMin;
	v3Max = m_v3BoundsMax;

	return bResult;
}

bool VflRenderNode::RecomputeVisBounds()
{
	m_bBoundsValid		= false;
	m_bBoundsSetByUser	= false;

	RefreshBounds();

	if(!m_bBoundsValid)
	{
		m_v3BoundsMin	= VistaVector3D(-1.0f, -1.0f, -1.0f, 1.0f);
		m_v3BoundsMax	= VistaVector3D( 1.0f,  1.0f,  1.0f, 1.0f);
		m_bBoundsValid	= true;
	}
	
	m_dBoundsTimeStamp	= m_pVisTiming->GetCurrentClock();

	return true;
}

bool VflRenderNode::OverrideVisBounds(const VistaVector3D &v3Min,
									   const VistaVector3D &v3Max)
{
	for(int i=0; i<3; ++i)
	{
		m_v3BoundsMin[i] = v3Min[i] <= v3Max[i] ? v3Min[i] : v3Max[i];
		m_v3BoundsMax[i] = v3Min[i] <= v3Max[i] ? v3Max[i] : v3Min[i];
	}

	m_bBoundsSetByUser = true;

	return true;
}

void VflRenderNode::SetBoundsToModified()
{
	m_bBoundsValid = false;
}

bool VflRenderNode::SetTargetBounds(const VistaVector3D &v3Min,
									 const VistaVector3D &v3Max)
{
	for(int i=0; i<3; ++i)
	{
		m_v3TargetBoundsMin[i] = v3Min[i] <= v3Max[i] ? v3Min[i] : v3Max[i];
		m_v3TargetBoundsMax[i] = v3Min[i] <= v3Max[i] ? v3Max[i] : v3Min[i];
	}

	return false;
}

bool VflRenderNode::GetTargetBounds(VistaVector3D &v3Min,
									 VistaVector3D &v3Max) const
{
	v3Min = m_v3TargetBoundsMin;
	v3Max = m_v3TargetBoundsMax;

	return true;
}


/*============================================================================*/
/* VTK WINDOW & RENDERER MANAGEMENT				                              */
/*============================================================================*/
void VflRenderNode::UpdateVtkWindowSize(int nWidth, int nHeight)
{
	if(m_pVtkRenderWindow)
		m_pVtkRenderWindow->SetSize(nWidth, nHeight);
}

vtkRenderer* VflRenderNode::GetVtkRenderer() const
{
	return m_pVtkRenderer;
}


/*============================================================================*/
/* HELPER FUNCTIONS	                                                          */
/*============================================================================*/
void VflRenderNode::RefreshMatrices()
{
	if(m_pTransformNode)
	{
		m_pTransformNode->GetTransform(m_mTrans);
		m_mInvTrans = m_mTrans.GetInverted();

		
		float aMat[16];
		m_pTransformNode->GetWorldTransform(aMat);
		m_mWorldTrans = VistaTransformMatrix (aMat);
		m_mWorldInvTrans = m_mWorldTrans.GetInverted();
		
	}	
}

void VflRenderNode::RefreshBounds()
{
	// @todo Linux Multilock
	// RenderNode.Update -> loop over renderables -> renderable.update -> RefreshBounds
	//m_pListMutex->Lock();
	m_pListMutex->TryLock();

	VistaVector3D v3Min, v3Max;

	list<IVflRenderable*>::iterator it = m_liRenderables.begin();

	while(it != m_liRenderables.end())
	{
		if((*it) && (*it)->GetBounds(v3Min, v3Max))
		{
			if(!m_bBoundsValid)
			{
				m_v3BoundsMin = v3Min;
				m_v3BoundsMax = v3Max;

				m_bBoundsValid = true;
			}
			else
			{
				for(int i=0; i<3; ++i)
				{
					m_v3BoundsMin[i] = min(m_v3BoundsMin[i], v3Min[i]);
					m_v3BoundsMax[i] = max(m_v3BoundsMax[i], v3Max[i]);
				}
			}
		}

		++it;
	}

	m_pListMutex->Unlock();
}


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
