/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VflVtkProperty_h
#define _VflVtkProperty_h

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>

#include <VistaFlowLib/VistaFlowLibConfig.h>
#include <VistaAspects/VistaReflectionable.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkProperty;
class VflPropertyLoader;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflVtkProperty contains a vtkProperty object, which can be shared
 * between several different objects.
 * 
 * @author  Marc Schirski
 */    
class VISTAFLOWLIBAPI VflVtkProperty : public IVistaReflectionable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
	VflVtkProperty();
    virtual ~VflVtkProperty();

    // IMPLEMENTATION
    /**
     * Returns a pointer the the shared vtk property.
     */
    vtkProperty *GetVtkProperty() const;

	//virtual int SetProperty(const VistaProperty &refProp);
	//virtual int GetProperty(VistaProperty &refProp);
	//virtual int GetPropertySymbolList(list<string> &rStorageList);

	bool GetColor(float &r, float &g, float &b) const;
	bool SetColor(float r, float g, float b);

	bool GetAmbientColor(float &r, float &g, float &b) const;
	bool SetAmbientColor(float r, float g, float b);

	bool GetDiffuseColor(float &r, float &g, float &b) const;
	bool SetDiffuseColor(float r, float g, float b);

	bool GetSpecularColor(float &r, float &g, float &b) const;
	bool SetSpecularColor(float r, float g, float b);

	bool GetEdgeColor(float &r, float &g, float &b) const;
	bool SetEdgeColor(float r, float g, float b);

	double GetAmbient() const;
	bool   SetAmbient(double dAmbient);

	double GetDiffuse() const;
	bool   SetDiffuse(double dDiffuse);

	double GetSpecular() const;
	bool   SetSpecular( double dSpecular );


	double GetSpecularPower() const;
	bool   SetSpecularPower(double dSpecP);

	double GetOpacity() const;
	bool   SetOpacity(double dOpacity);


	std::string GetInterpolation() const;
	bool SetInterpolation( const std::string &sInterpolation);

	int GetInterpolationId() const;
	bool SetInterpolationId( int id );

	std::string GetRepresentation() const;
	bool SetRepresentation(const std::string &sRepresentation);

	int GetRepresentationId() const;
	bool SetRepresentationId( int nRep );

	std::string GetCulling() const;
	bool SetCulling(const std::string &sCulling);

	enum eCullId
	{
		CULL_NONE = 0,
		CULL_FRONT = 1,
		CULL_BACK = 2,
		CULL_BOTH = 3
	};

	eCullId GetCullingId() const;
	bool SetCullingId(eCullId nId);

	bool GetEdgeVisibility() const;
	bool SetEdgeVisibility(bool bEdgeV);

	enum
	{
		MSG_COLOR_CHANGE = IVistaReflectionable::MSG_LAST,
		MSG_AMBIENT_COLOR_CHANGE,
		MSG_DIFFUSE_COLOR_CHANGE,
		MSG_SPECULAR_COLOR_CHANGE,
		MSG_SPECULAR_POWER_CHANGE,
		MSG_EDGE_COLOR_CHANGE,
		MSG_AMBIENT_CHANGE,
		MSG_DIFFUSE_CHANGE,
		MSG_SPECULAR_CHANGE,
		MSG_OPACITY_CHANGE,
		MSG_CULLING_CHANGE,
		MSG_EDGE_VISIBILITY_CHANGE,
		MSG_REPRESENTATION_CHANGE,
		MSG_INTERPOLATION_CHANGE,
		MSG_LAST
	};


	std::string GetReflectionableType() const;
protected:
	int AddToBaseTypeList(std::list<std::string> &rBtList) const;

private:
       vtkProperty *m_pProperty;
};

#endif // Include guard
