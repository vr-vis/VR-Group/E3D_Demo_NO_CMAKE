/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflVisObject.h"
#include "VflRenderNode.h"
#include "IVflTransformer.h"

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaVisExt/Data/VveUnsteadyData.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>

using namespace std;

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
IVflVisObject::IVflVisObject()
:	m_pTransformer(NULL)
,	m_bHaveTransformerOwnership(false)
,   m_pUnsteadyData(NULL)
{ }

IVflVisObject::~IVflVisObject()
{
	if(m_pTransformer)
		ReleaseObserveable(m_pTransformer, E_TRANSFORMER_TICKET);
	if(m_bHaveTransformerOwnership)
		delete m_pTransformer;

	
	if(m_pUnsteadyData)
		ReleaseObserveable(m_pUnsteadyData, E_UNSTEADYDATA_TICKET);
}


std::string IVflVisObject::GetFactoryType()
{
	return std::string("<NOT SPECIFIED>");
}


IVflTransformer* IVflVisObject::GetTransformer() const
{
	return m_pTransformer;
}

IVflTransformer* IVflVisObject::SetTransformer(IVflTransformer* pTrans, bool bTransferOwnership)
{
	IVflTransformer *pResult = m_pTransformer;

	if(m_pTransformer)
		ReleaseObserveable(m_pTransformer, E_TRANSFORMER_TICKET);
	
	if(m_bHaveTransformerOwnership)
		delete m_pTransformer;

	m_pTransformer = pTrans;

	if(m_pTransformer)
		Observe(m_pTransformer, E_TRANSFORMER_TICKET);

	m_bHaveTransformerOwnership = bTransferOwnership;

	return pResult;
}

VveUnsteadyData *IVflVisObject::GetUnsteadyData() const
{
	return m_pUnsteadyData;
}

bool IVflVisObject::SetUnsteadyData(VveUnsteadyData* pData)
{
	if(pData != m_pUnsteadyData)
	{
		if(m_pUnsteadyData)
		{
			ReleaseObserveable(m_pUnsteadyData, E_UNSTEADYDATA_TICKET);
		}
		m_pUnsteadyData = pData;

		if(m_pUnsteadyData)
		{
			Observe(m_pUnsteadyData, E_UNSTEADYDATA_TICKET);

		}
		return true;
	}
	return false;
}


std::string IVflVisObject::GetType() const
{
    return IVflRenderable::GetType() + "::VflVisObject";
}


IVflRenderable::VflRenderableProperties *IVflVisObject::CreateProperties() const
{
	return new VflVisObjProperties();
}

bool IVflVisObject::DeleteProperties(IVflVisObject::VflVisObjProperties *pObj) const
{
	delete pObj;
	return true;
}


//==============================================================================

REFL_IMPLEMENT_FULL(IVflVisObject::VflVisObjProperties, IVflRenderable::VflRenderableProperties)

namespace
{

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<string, IVflVisObject::VflVisObjProperties,
		VistaProperty::PROPT_STRING>(
			"TRANSFORMERNAME", SsReflectionName,
			&IVflVisObject::VflVisObjProperties::GetTransformerName),
	NULL
};

}


IVflVisObject::VflVisObjProperties::VflVisObjProperties()
{ }

IVflVisObject::VflVisObjProperties::~VflVisObjProperties()
{ }


string IVflVisObject::VflVisObjProperties::GetTransformerName() const
{
	IVflVisObject *pParent = dynamic_cast<IVflVisObject*>(GetParentRenderable());

	if(pParent && pParent->GetTransformer())
		return pParent->GetTransformer()->GetNameForNameable();
	
	return "<null>";
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
