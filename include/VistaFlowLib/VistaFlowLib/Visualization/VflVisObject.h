/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFLVISOBJECT_H
#define _VFLVISOBJECT_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VistaFlowLibConfig.h"
#include "VflRenderable.h"

#include <list>
#include <string>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVflTransformer;
class VveUnsteadyData;
class VveTimeMapper;


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * When subclassing: do overload
 * - one of the draw routines
 *   @see Update()
 *	 @see DrawOpaque()
 *	 @see DrawTransparent()
 *	 @see Draw2D()
 * - mandatory: overload GetBounds()
 * - mandatory: overload GetType()
 * - mandatory: overload GetRegistrationMode()
 * - optional: GetFactoryType()
 * Create your own properties as soon as you want to set some using an ini file
 * or some other means of symbolic in- and output, e.g., GUI or VistaPropertyLists.
 * Otherwise, you can ignore that.
 */
class VISTAFLOWLIBAPI IVflVisObject : public IVflRenderable
{
	friend class VflVisObjProperties;

public:
    IVflVisObject();
    virtual ~IVflVisObject();
	
	static std::string GetFactoryType();

	/*
	 * If you transferOwnership, the transformer will be deleted when setting a new or deleting this object.
	 */
	virtual IVflTransformer* SetTransformer(IVflTransformer* pTrans, bool bTransferOwnership = true);
	virtual IVflTransformer* GetTransformer() const;
	
	VveUnsteadyData* GetUnsteadyData() const;
	virtual bool SetUnsteadyData(VveUnsteadyData* pData);

	// *** IVflRenderable interface. ***
	virtual std::string GetType() const;

	// *** VflVisObject property class. ***
	class VISTAFLOWLIBAPI VflVisObjProperties : public VflRenderableProperties
	{
		REFL_DECLARE

	public:
		VflVisObjProperties();
		virtual ~VflVisObjProperties();

		std::string GetTransformerName() const;

	private:
		VflVisObjProperties(const VflVisObjProperties&) {}
		VflVisObjProperties& operator=(const VflVisObjProperties&)
			{ return *this; }
	};	

protected:
	enum LocalObserverTickets
	{
		E_TRANSFORMER_TICKET = IVflRenderable::E_TICKET_LAST,
		E_UNSTEADYDATA_TICKET,
		E_TICKET_LAST
	};

	//==========================================================================
	// Overload these in sub-classes.
	virtual VflRenderableProperties   *CreateProperties() const;
	virtual bool DeleteProperties(VflVisObjProperties *) const;
	//==========================================================================

	bool m_bHaveTransformerOwnership;
	IVflTransformer* m_pTransformer;
private:
	
	VveUnsteadyData* m_pUnsteadyData;
};


#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

