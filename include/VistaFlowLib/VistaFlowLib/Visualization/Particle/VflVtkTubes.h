/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVTKTUBES_H
#define _VFLVTKTUBES_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VflRenderNode.h"
#include "VflParticleRenderer.h"
#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisController;
class VtkObjectStack;
class vtkProperty;
class vtkTubeFilter;
class vtkActor;
class vtkPolyData;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflVtkTubes renders particle data via geometrical tubes.
 */    
class VISTAFLOWLIBAPI VflVtkTubes : public VflParticleRenderer
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflVtkTubes(VflVisParticles *pOwner, 
				 bool bImmediateMode=false,
				 int  nNumberOfSides=3,
		         vtkProperty *pProperty=NULL );
    virtual ~VflVtkTubes();

    /**
     * Initialize renderer.
     */    
    virtual bool Init();

	/**
	 * Destroy internal data.
	 */
	virtual void Destroy();

	/**
	 * Update the renderer, i.e. allow it to perform any data updates and such
	 * outside the actual render process.
	 */
	virtual void Update();

	/**
	 * Draw any opaque stuff.
	 */
	virtual void DrawOpaque();

	/**
	 * Draw any transparent stuff.
	 */
	virtual void DrawTransparent();


    /**
     * Returns the type of the particle renderer as a string.
     */    
	virtual std::string GetType() const;

	void SetNumberOfSides(int iSides);

	vtkProperty *GetProperty() const;

protected:
	void UpdateParticleData();
	void UpdateVisParams();

	std::vector<vtkPolyData *> m_vecPositions;
	std::vector<vtkTubeFilter *> m_vecTubeFilters;
	std::vector<vtkActor *> m_vecActors;

	vtkProperty *m_pProperty;

	VtkObjectStack *m_pVtkObjectStack;
	VtkObjectStack *m_pParticleDataStack;

	float m_fTimeWindow;
	int m_iMaxParticles;
	int m_iCurrentTimeIndex;
	int m_iNumberOfSides;
	bool m_bImmediateMode;
	bool m_bInitialized;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/
inline vtkProperty *VflVtkTubes::GetProperty() const
{
	return m_pProperty;
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLVTKTUBES_H)
