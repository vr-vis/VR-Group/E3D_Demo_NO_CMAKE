/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflVtkGlyphs.h"
#include "VflVisParticles.h"

#include "../VflVisTiming.h"
//#include "VflVisParticlesParams.h"
#include "../VflVtkLookupTable.h"
#include "../VflRenderNode.h"



#include <VistaVisExt/Tools/VtkObjectStack.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>

#include <vtkProperty.h>
#include <vtkGlyph3D.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkLookupTable.h>
#include <vtkRenderer.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>

#include <VistaTools/VistaProgressBar.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
//VflVtkGlyphs::VflVtkGlyphs(VflVisParticles *pOwner, const VistaPropertyList &refProps, vtkProperty *pProperty)
//: VflParticleRenderer(pOwner), 
//m_pGlyph(NULL), m_pProperty(pProperty), 
//m_pVtkObjectStack(NULL), m_bInitialized(false),
//m_bNeedGlyphUpdate(true), 
//m_pParticles(NULL), m_pParticleDataStack(NULL)
//{
//	m_bImmediateMode = false;
//	if (refProps.HasProperty("IMMEDIATE_MODE"))
//	{
//		m_bImmediateMode = refProps.GetValue<bool>("IMMEDIATE_MODE");
//		vstr::outi() << " [VflVtkGlyphs] - " << (m_bImmediateMode?"":"not ") << "using immediate mode..." << endl;
//	}
//	else
//	{
//#ifdef DEBUG
//		vstr::outi() << " [VflVtkGlyphs] - " << (m_bImmediateMode?"":"not ") << "using immediate mode (default)..." << endl;
//#endif
//	}
//}

VflVtkGlyphs::VflVtkGlyphs(VflVisParticles *pOwner, bool bImmediateMode, vtkProperty *pProperty)
: VflParticleRenderer(pOwner), 
  m_pGlyph(NULL), m_pProperty(pProperty), 
  m_pVtkObjectStack(NULL), m_bInitialized(false),
  m_bNeedGlyphUpdate(true), 
  m_bImmediateMode(bImmediateMode), 
  m_pParticles(NULL), 
  m_pParticleDataStack(NULL)
{
}

VflVtkGlyphs::~VflVtkGlyphs()
{
	this->Destroy();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflVtkGlyphs::Init()
{
	m_pVtkObjectStack = new VtkObjectStack();
	m_pParticleDataStack = new VtkObjectStack();
	m_pParticles = new VveParticlePopulation();

	// create a default glyph, if none is present...
	if (!m_pGlyph)
	{
		vtkSphereSource *pSphere = vtkSphereSource::New();
		m_pGlyph = pSphere;
	}

	// if no rendering property is available, create one
	if (!m_pProperty)
	{
		m_pProperty = vtkProperty::New();
		m_pVtkObjectStack->PushObject(m_pProperty);
	}

	vtkLookupTable *pLookupTable = m_pOwner->GetLookupTable()->GetLookupTable();

	// create rendering pipelines for every time level
	VveTimeMapper *pTimeMapper = m_pOwner->GetTimeMapper();
	int iNumberOfLevels = pTimeMapper->GetNumberOfTimeIndices();
	int i;

	m_vecPositions.resize(iNumberOfLevels);
	m_vecGlyphObjects.resize(iNumberOfLevels);
	m_vecActors.resize(iNumberOfLevels);

	for (i=0; i<iNumberOfLevels; ++i)
	{
		vtkPolyData *pPolys = vtkPolyData::New();
		m_pParticleDataStack->PushObject(pPolys);
		pPolys->Allocate(1, 1);
		m_vecPositions[i] = pPolys;

		vtkGlyph3D *pGlyph = vtkGlyph3D::New();
		m_pVtkObjectStack->PushObject(pGlyph);
#if VTK_MAJOR_VERSION > 5
		pGlyph->SetInputData(pPolys);
#else
		pGlyph->SetInput(pPolys);
#endif
		pGlyph->SetColorModeToColorByScalar();
		m_vecGlyphObjects[i] = pGlyph;

		vtkPolyDataMapper *pMapper = vtkPolyDataMapper::New();
		m_pVtkObjectStack->PushObject(pMapper);
		pMapper->SetInputConnection(pGlyph->GetOutputPort());
		pMapper->SetLookupTable(pLookupTable);
		pMapper->SetScalarVisibility(true);
		pMapper->UseLookupTableScalarRangeOn();
		pMapper->SetImmediateModeRendering(m_bImmediateMode);

		vtkActor *pActor = vtkActor::New();
		m_pVtkObjectStack->PushObject(pActor);
		pActor->SetMapper(pMapper);
		pActor->SetProperty(m_pProperty);
		m_vecActors[i] = pActor;
	}

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Destroy                                                     */
/*                                                                            */
/*============================================================================*/
void VflVtkGlyphs::Destroy()
{
	m_pVtkObjectStack->DeleteObjects();
	m_pParticleDataStack->DeleteObjects();

	m_vecActors.clear();
	m_vecGlyphObjects.clear();
	m_vecPositions.clear();

	m_pGlyph->Delete();
	m_pGlyph = NULL;
	m_pProperty = NULL;

	delete m_pParticleDataStack;
	m_pParticleDataStack = NULL;
	delete m_pVtkObjectStack;
	m_pVtkObjectStack = NULL;
	delete m_pParticles;
	m_pParticles = NULL;

	m_bInitialized = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                     */
/*                                                                            */
/*============================================================================*/
void VflVtkGlyphs::Update()
{
	if (m_dDataTimeStamp < m_pOwner->GetDataChangeTimeStamp())
		UpdateParticleData();
	if (m_bNeedGlyphUpdate)
		UpdateGlyphs();
	if (m_dVisParamsTimeStamp < m_pOwner->GetVisParamsChangeTimeStamp())
		UpdateVisParams();

	// determine time level to be rendered
	VveTimeMapper *pTimeMapper = m_pOwner->GetTimeMapper();
	m_iCurrentTimeIndex = pTimeMapper->GetTimeIndex(
		m_pOwner->GetRenderNode()->GetVisTiming()->GetVisualizationTime());
	if (m_iCurrentTimeIndex < 0)
		return;

	// kick of update of current mapper
	m_vecActors[m_iCurrentTimeIndex]->GetMapper()->Update();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVtkGlyphs::DrawOpaque()
{
	if (m_iCurrentTimeIndex < 0)
		return;

	m_vecActors[m_iCurrentTimeIndex]->RenderOpaqueGeometry(
		m_pOwner->GetRenderNode()->GetVtkRenderer());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVtkGlyphs::DrawTransparent()
{
	if (m_iCurrentTimeIndex < 0)
		return;

#if VTK_MAJOR_VERSION > 5 || (VTK_MAJOR_VERSION == 5 && VTK_MINOR_VERSION >=1)
	
	m_vecActors[m_iCurrentTimeIndex]->RenderTranslucentPolygonalGeometry(
		m_pOwner->GetRenderNode()->GetVtkRenderer());
#else
	m_vecActors[m_iCurrentTimeIndex]->RenderTranslucentGeometry(
		m_pOwner->GetRenderNode()->GetVtkRenderer());
#endif
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVtkGlyphs::GetType() const
{
    return VflParticleRenderer::GetType() + std::string("::VflVtkGlyphs");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetGlyph                                                    */
/*                                                                            */
/*============================================================================*/
void VflVtkGlyphs::SetGlyph (vtkPolyDataAlgorithm *pGlyph)
{
	m_pGlyph = pGlyph;
	m_bNeedGlyphUpdate = true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateGlyphs                                                */
/*                                                                            */
/*============================================================================*/
void VflVtkGlyphs::UpdateGlyphs()
{
	const size_t nNumberOfLevels = m_vecGlyphObjects.size();
	for (size_t i=0; i<nNumberOfLevels; ++i)
	{
		m_vecGlyphObjects[i]->SetSourceConnection(m_pGlyph->GetOutputPort());
	}
	m_bNeedGlyphUpdate = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateParticleData                                          */
/*                                                                            */
/*============================================================================*/
void VflVtkGlyphs::UpdateParticleData()
{
	VveTimeMapper *pMapper = m_pOwner->GetTimeMapper();
	m_pOwner->GetParticleData()->GetData()->LockData();
	VveParticlePopulation *pOriginalParticles = m_pOwner->GetParticleData()->GetData()->GetData();

    VflVisParticles::VflVisParticlesProperties *pParams = 
        dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(
        m_pOwner->GetProperties());

	// update local (reduced) particle data
	vstr::outi() << " [VflVtkGlyphs] - reducing trajectory information..." << endl;
	pOriginalParticles->ReduceInformation(pMapper, pParams->GetTimeArray(), m_pParticles);

	const int iPathlineCount = m_pParticles->GetNumTrajectories();
    std::vector<int> vecParticleLevel(iPathlineCount);
	
	for (int i=0; i<iPathlineCount; ++i)
	{
		vecParticleLevel[i] = 0;

        VveParticleTrajectory *pTraj = m_pParticles->GetTrajectory(i);
        VveParticleDataArrayBase *pVelocityArray = NULL;
        VveParticleDataArrayBase *pRadiusArray = NULL;

        if( pTraj &&
            pTraj->GetArrayByName(pParams->GetVelocityArray(), pVelocityArray) &&
            pTraj->GetArrayByName(pParams->GetRadiusArray(), pRadiusArray) &&
            pVelocityArray && pRadiusArray )
		{
            for(size_t j=0; j<pVelocityArray->GetSize(); ++j)
            {
                float fRadius = pRadiusArray->GetElementValueAsFloat(j);
                pVelocityArray->SetElementValue(j,fRadius);
		}
        }	
	}

	// delete old vtk particle data
	m_pParticleDataStack->DeleteObjects();

	const int iLevelCount = static_cast<int>(m_vecPositions.size());
	vstr::outi() << " [VflVtkGlyphs] - updating internal vtk poly data structures..." << endl;

	VistaProgressBar oBar(iLevelCount);
	oBar.SetBarTicks(30);
	oBar.SetPrefixString("                  ");
	oBar.Start();

	for(int i=0; i<iLevelCount; ++i)
	{
		oBar.Increment();

//		vstr::outi() << "                time level " << (i+1) << " of " << iLevelCount << "\r";
//		vstr::outi().flush();

		vtkFloatArray *pScalars = vtkFloatArray::New();
		m_pParticleDataStack->PushObject(pScalars);
		pScalars->Initialize();

		vtkFloatArray *pVectors = vtkFloatArray::New();
		m_pParticleDataStack->PushObject(pVectors);
		pVectors->SetNumberOfComponents(3);
		pVectors->Initialize();

		vtkPoints *pPoints = vtkPoints::New();
		m_pParticleDataStack->PushObject(pPoints);
//		pPoints->SetData(pScalars);
		int iPointCount = 0;
		pPoints->Initialize();

		vtkPolyData *pPolys = vtkPolyData::New();
		m_pParticleDataStack->PushObject(pPolys);
		pPolys->Allocate(1, 1);
		pPolys->SetPoints(pPoints);
		pPolys->GetPointData()->SetScalars(pScalars);
		pPolys->GetPointData()->SetVectors(pVectors);

		float fSimTime = pMapper->GetSimulationTime(int(i));

		// find particle data to be displayed during the current time level
		for(int j=0; j<iPathlineCount; ++j)
		{
			//SVveParticleTrajectory &refParticle = m_pParticles->vecPopulation[j];

            VveParticleTrajectory *pTraj = m_pParticles->GetTrajectory(j);
            VveParticleDataArrayBase *pTimeArray = NULL;
            VveParticleDataArrayBase *pPosArray = NULL;
            VveParticleDataArrayBase *pVelArray = NULL;
            VveParticleDataArrayBase *pScalarArray = NULL;
            bool bInitArrays = pTraj &&
                pTraj->GetArrayByName(pParams->GetTimeArray(), pTimeArray) &&
                pTraj->GetArrayByName(pParams->GetPositionArray(), pPosArray) &&
                pTraj->GetArrayByName(pParams->GetVelocityArray(), pVelArray) &&
                pTraj->GetArrayByName(pParams->GetScalarArray(), pScalarArray) &&
                pTimeArray && pPosArray && pVelArray && pScalarArray;
            if( !bInitArrays)
                continue;

            const int iTrajectorySize = static_cast<int>(pTimeArray->GetSize());


			while( vecParticleLevel[j] < iTrajectorySize
				&& pTimeArray->GetElementValueAsFloat(vecParticleLevel[j]) < fSimTime )
			{
				++vecParticleLevel[j];
			}

			if (vecParticleLevel[j] == iTrajectorySize)
			{
				continue;
			}

			if (pTimeArray->GetElementValueAsFloat(vecParticleLevel[j]) == fSimTime)
			{
				//SVveParticleInstant &refInstant = refParticle.vecTrajectory[vecParticleLevel[j]];

                float aPos[3], aVel[3], fScalar;
                pPosArray->GetElementCopy(vecParticleLevel[j], aPos);
                pVelArray->GetElementCopy(vecParticleLevel[j], aVel);
                fScalar = pScalarArray->GetElementValueAsFloat(vecParticleLevel[j]);

				/**
				 * TODO - memory leak??
				 */
//				vtkVertex *pVertex = vtkVertex::New();
				vtkIdType iPointId = pPoints->InsertNextPoint(aPos);
				pScalars->InsertNextValue(fScalar);
				pVectors->InsertNextTuple(aVel);
//				pVertex->GetPointIds()->InsertNextId(iPointId);
				pPolys->InsertNextCell(VTK_VERTEX, 1, &iPointId);
//				pPolys->InsertNextCell(pVertex->GetCellType(), pVertex->GetPointIds());
			}
		}

#if VTK_MAJOR_VERSION > 5
		m_vecGlyphObjects[i]->SetInputData(pPolys);
#else
		m_vecGlyphObjects[i]->SetInput(pPolys);
#endif

		vtkPolyData *pTemp = m_vecPositions[i];

		// we all already deleted the poly data object along
		// with its companions with the help of the particle
		// data stack...
		// m_vecPositions[i]->Delete();
		m_vecPositions[i] = pPolys;
	}
	oBar.Finish();

//	vstr::outi() << endl;

	m_dDataTimeStamp = m_pOwner->GetDataChangeTimeStamp();

	m_pOwner->GetParticleData()->GetData()->UnlockData();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateVisParams                                             */
/*                                                                            */
/*============================================================================*/
void VflVtkGlyphs::UpdateVisParams()
{
	VflVisParticles::VflVisParticlesProperties *pParams = 
		dynamic_cast<VflVisParticles::VflVisParticlesProperties *>(m_pOwner->GetProperties());

	if(!pParams)
		return;


	float fRadiusScale = 2.0f*pParams->GetRadiusScale();
	float fRadiusAbsolute = 2.0f*pParams->GetRadiusAbsolute();
	bool bUseParticleRadius = pParams->GetUseParticleRadius();

	vtkGlyph3D *pGlyph;

	const size_t nNumberOfLevels = m_vecGlyphObjects.size();
	for (size_t i=0; i<nNumberOfLevels; ++i)
	{
		pGlyph = m_vecGlyphObjects[i];
		
		if (bUseParticleRadius)
		{
			// this is kind of a hack - to make this work,
			// we have to set all vector components of the vtk
			// data to the respective particle's radius
			pGlyph->SetScaleModeToScaleByVectorComponents();
			pGlyph->SetScaleFactor(fRadiusScale);
		}
		else
		{
			pGlyph->SetScaleModeToDataScalingOff();
			pGlyph->SetScaleFactor(fRadiusAbsolute);
		}
	}

	m_dVisParamsTimeStamp = m_pOwner->GetVisParamsChangeTimeStamp();
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflVtkGlyphs.cpp"                                            */
/*============================================================================*/
