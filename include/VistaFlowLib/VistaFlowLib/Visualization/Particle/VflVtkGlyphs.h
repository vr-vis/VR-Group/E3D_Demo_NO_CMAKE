/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVTKGLYPHS_H
#define _VFLVTKGLYPHS_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflParticleRenderer.h"
#include <vector>
#include <VistaAspects/VistaPropertyAwareable.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkActor;
class vtkProperty;
class vtkPolyData;
class vtkPolyDataAlgorithm;
class vtkGlyph3D;
class VtkObjectStack;
class VflVisController;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflVtkGlyphs renders particle data via vtk glyphs.
 */    
class VISTAFLOWLIBAPI VflVtkGlyphs : public VflParticleRenderer
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    /**
     * Creates a VflVtkGlyphs object. The following property keys are recognized:
     *
     * IMMEDIATE_MODE [true|false]  -   optional
     */
//    VflVtkGlyphs(VflVisParticles *pOwner, const VistaPropertyList &refProps, vtkProperty *pProperty=NULL);
    VflVtkGlyphs(VflVisParticles *pOwner, 
		bool bImmediateMode=false,
		vtkProperty *pProperty=NULL);


    virtual ~VflVtkGlyphs();

    /**
     * Initialize renderer.
     */    
    virtual bool Init();

	/**
	 * Destroy internal data.
	 */
	virtual void Destroy();

	/**
	 * Update the renderer, i.e. allow it to perform any data updates and such
	 * outside the actual render process.
	 */
	virtual void Update();

	/**
	 * Draw any opaque stuff.
	 */
	virtual void DrawOpaque();

	/**
	 * Draw any transparent stuff.
	 */
	virtual void DrawTransparent();


    /**
     * Returns the type of the particle renderer as a string.
     */    
	virtual std::string GetType() const;

	vtkPolyDataAlgorithm * GetGlyph() const;
	void SetGlyph(vtkPolyDataAlgorithm *pGlyph);

	vtkProperty *GetProperty() const;

protected:
	void UpdateGlyphs();
	void UpdateParticleData();
	void UpdateVisParams();

	vtkPolyDataAlgorithm * m_pGlyph;
	vtkProperty *m_pProperty;

	std::vector<vtkPolyData *> m_vecPositions;
	std::vector<vtkGlyph3D *> m_vecGlyphObjects;
	std::vector<vtkActor *> m_vecActors;

	VtkObjectStack *m_pVtkObjectStack;
	VtkObjectStack *m_pParticleDataStack;

	VveParticlePopulation *m_pParticles;

	int m_iCurrentTimeIndex;
	bool m_bImmediateMode;
	bool m_bInitialized;
	bool m_bNeedGlyphUpdate;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/
inline vtkPolyDataAlgorithm *VflVtkGlyphs::GetGlyph() const
{
	return m_pGlyph;
}

inline vtkProperty *VflVtkGlyphs::GetProperty() const
{
	return m_pProperty;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLVTKGLYPHS_H)
