/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflVirtualTubelets.h"
#include "VflVisParticles.h"
#include "../VflVisTiming.h"
#include "../VflRenderNode.h"
#include "../VflLookupTexture.h"
#include "../VflVtkLookupTable.h"

#include <VistaBase/VistaStreamUtils.h>

#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaBufferObject.h>
#include <VistaOGLExt/VistaShaderRegistry.h>
#include <VistaOGLExt/VistaVertexArrayObject.h>
#include <VistaOGLExt/VistaOGLUtils.h>
#include <VistaOGLExt/Rendering/VistaParticleTraceRenderingCore.h>
#include <VistaOGLExt/Rendering/VistaParticleRenderingProperties.h>


using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

VflVirtualTubelets::VflVirtualTubelets( VflVisParticles *pOwner )
	:	VflParticleRenderer(pOwner)
	,	m_bInitialized(false)
	,	m_bInterpolat(false)
	,	m_pBodyVAO(NULL)
	,	m_pBodyVBO(NULL)
	,	m_pBodyIBO(NULL)
	,	m_pCapVAO(NULL)
	,	m_pCapVBO(NULL)
	,	m_pCore( new VistaParticleTraceRenderingCore() )
{
	// write all particles from population into selected set
	VveParticlePopulation *pParticles = m_pOwner->GetParticleData()->GetData()->GetData();
	for (int i=0; i < pParticles->GetNumTrajectories(); ++i)
		m_vecSelectedTrajectories.push_back(i);
}

VflVirtualTubelets::~VflVirtualTubelets()
{ }

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
VistaParticleRenderingProperties* 
	VflVirtualTubelets::GetParticleRenderingProperties()
{
	return m_pCore->GetProperties();
}

bool VflVirtualTubelets::Init()
{
	if( m_bInitialized ) 
		return false;

	if( !m_pCore->Init() )
		return false;

	const unsigned int nSpride = 10 * sizeof(float);
	const  unsigned int aOffsets[] = {
		0 * sizeof(float),
		3 * sizeof(float),
		6 * sizeof(float),
		7 * sizeof(float),
		8 * sizeof(float) 
	};

	m_pCapVBO = new VistaBufferObject();
	m_pCapVBO->BindAsVertexDataBuffer();
	m_pCapVBO->Release();

	m_pCapVAO = new VistaVertexArrayObject();
	m_pCapVAO->Bind();

	m_pCapVAO->EnableAttributeArray( 0 ); // position
	m_pCapVAO->EnableAttributeArray( 1 ); // tangent
	m_pCapVAO->EnableAttributeArray( 2 ); // scalar value
	m_pCapVAO->EnableAttributeArray( 3 ); // luminance
	m_pCapVAO->EnableAttributeArray( 4 ); // vertex offsets

	m_pCapVAO->SpecifyAttributeArrayFloat(0, 3, GL_FLOAT, false, nSpride, aOffsets[0], m_pCapVBO );
	m_pCapVAO->SpecifyAttributeArrayFloat(1, 3, GL_FLOAT, false, nSpride, aOffsets[1], m_pCapVBO );
	m_pCapVAO->SpecifyAttributeArrayFloat(2, 1, GL_FLOAT, false, nSpride, aOffsets[2], m_pCapVBO );
	m_pCapVAO->SpecifyAttributeArrayFloat(3, 1, GL_FLOAT, false, nSpride, aOffsets[3], m_pCapVBO );
	m_pCapVAO->SpecifyAttributeArrayFloat(4, 2, GL_FLOAT, false, nSpride, aOffsets[4], m_pCapVBO );

	m_pCapVAO->Release();

	m_pBodyIBO = new VistaBufferObject();
	m_pBodyIBO->BindAsIndexBuffer();
	m_pBodyIBO->Release();

	m_pBodyVBO = new VistaBufferObject();
	m_pBodyVBO->BindAsVertexDataBuffer();
	m_pBodyVBO->Release();

	m_pBodyVAO = new VistaVertexArrayObject();
	m_pBodyVAO->Bind();

	m_pBodyVAO->EnableAttributeArray( 0 ); // position
	m_pBodyVAO->EnableAttributeArray( 1 ); // tangent
	m_pBodyVAO->EnableAttributeArray( 2 ); // scalar value
	m_pBodyVAO->EnableAttributeArray( 3 ); // luminance
	m_pBodyVAO->EnableAttributeArray( 4 ); // vertex offsets

	m_pBodyVAO->SpecifyAttributeArrayFloat(0, 3, GL_FLOAT, false, nSpride, aOffsets[0], m_pBodyVBO );
	m_pBodyVAO->SpecifyAttributeArrayFloat(1, 3, GL_FLOAT, false, nSpride, aOffsets[1], m_pBodyVBO );
	m_pBodyVAO->SpecifyAttributeArrayFloat(2, 1, GL_FLOAT, false, nSpride, aOffsets[2], m_pBodyVBO );
	m_pBodyVAO->SpecifyAttributeArrayFloat(3, 1, GL_FLOAT, false, nSpride, aOffsets[3], m_pBodyVBO );
	m_pBodyVAO->SpecifyAttributeArrayFloat(4, 2, GL_FLOAT, false, nSpride, aOffsets[4], m_pBodyVBO );

	m_pBodyVAO->SpecifyIndexBufferObject( m_pBodyIBO );

	m_pBodyVAO->Release();


	m_pLUT = new VflLookupTexture( m_pOwner->GetLookupTable() );
	// init rendering core
	float fMin, fMax;
	m_pOwner->GetLookupTable()->GetTableRange( fMin, fMax );
	m_pCore->SetLookupRange( fMin, fMax );
	m_pCore->SetLookupTexture( m_pLUT->GetLookupTexture() );

	m_pCore->SetBodyVBO( m_pBodyVAO );
	m_pCore->SetCapVBO( m_pCapVAO );

	m_bInitialized = true;

	return true;
}

/*============================================================================*/
/*  NAME      :   Destroy                                                     */
/*============================================================================*/
void VflVirtualTubelets::Destroy()
{
	if(!m_bInitialized)
		return;

	delete m_pBodyVAO;
	delete m_pBodyVBO;
	delete m_pBodyIBO;

	delete m_pCapVAO;
	delete m_pCapVBO;

	m_bInitialized = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflVirtualTubelets::Update()
{
	if (!m_pOwner->GetVisible())
		return;

	bool bUpdateVAO = false;


	// make sure our data is up-to-date...
	if (m_dVisParamsTimeStamp < m_pOwner->GetVisParamsChangeTimeStamp())
	{
		UpdateVisParams();
		bUpdateVAO = true;
	}

	if (m_dDataTimeStamp < m_pOwner->GetDataChangeTimeStamp())
	{
		UpdateParticleData();
		bUpdateVAO = true;
	}

	double dVisTime = m_pOwner->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	double dSimTime = m_pOwner->GetTimeMapper()->GetSimulationTime( dVisTime );

	if( fabs(dSimTime - m_dStartTime) > numeric_limits<float>::epsilon() )
	{
		m_dStartTime = dSimTime;
		bUpdateVAO = true;
	}

	if( m_fTimeWindow < 0 )
		m_dEndTime = 0;
	else
		m_dEndTime = m_dStartTime - m_fTimeWindow;

	m_pCore->SetMaxLuminanceFactor( static_cast<float>(m_dStartTime) );
	m_pCore->SetViewerPosition(  m_pOwner->GetTransformedLocalViewPosition()   );
	m_pCore->SetLightDirection( -m_pOwner->GetTransformedLocalLightDirection() );
	m_pCore->SetLuminanceReductionFactor( 
		static_cast<float>(1.0/(m_dStartTime-m_dEndTime)) );
	
	if( bUpdateVAO )
		UpdateBufferObjects();
}

void VflVirtualTubelets::UpdateBufferObjects()
{
	VflVisParticles::VflVisParticlesProperties *pParams = m_pOwner->GetProperties();

	const string strTimeArray   = pParams->GetTimeArray();
	const string strPosArray    = pParams->GetPositionArray();
	const string strScalarArray = pParams->GetScalarArray();

	VveParticlePopulationItem* pPopulationItem = m_pOwner->GetParticleData()->GetData();
	VveParticlePopulation*     pPopulation     = pPopulationItem->GetData();

	pPopulationItem->LockData();

	m_vecBodyVertices.clear();
	m_vecBodyIndices.clear();
	m_vecCapVertices.clear();

	size_t nPathlineCount = m_vecSelectedTrajectories.size();
	for( size_t n=0; n<nPathlineCount; ++n )
	{
		int iTrajIndex = m_vecSelectedTrajectories[n];
		VveParticleTrajectory* pTraj = pPopulation->GetTrajectory( iTrajIndex );

		if( !pTraj )
			continue;

		VveParticleDataArrayBase* pTimeArray   = NULL;
		VveParticleDataArrayBase* pPosArray    = NULL;
		VveParticleDataArrayBase* pScalarArray = NULL;

		if( pTraj->GetArrayByName( strTimeArray,   pTimeArray   ) && pTimeArray &&
			pTraj->GetArrayByName( strPosArray,    pPosArray    ) && pPosArray  &&
			pTraj->GetArrayByName( strScalarArray, pScalarArray ) && pScalarArray )
		{
			UpdateTrajectoryData( pTimeArray, pPosArray, pScalarArray, 
				m_vecTrajectoryInfos[iTrajIndex] );
		}
	}

	pPopulationItem->UnlockData();

	// Possibly no trajectories means no data
	if(m_vecBodyVertices.empty() || m_vecBodyIndices.empty() || m_vecCapVertices.empty())
	{
		return;
	}

	// upload body vertices/indides to GPU
	m_pBodyVBO->BindAsVertexDataBuffer();
	m_pBodyVBO->BufferData(
		m_vecBodyVertices.size()*sizeof(float), 
		&m_vecBodyVertices[0], GL_STATIC_DRAW );
	m_pBodyVBO->Release();

	m_pBodyIBO->BindAsIndexBuffer();
	m_pBodyIBO->BufferData(
		m_vecBodyIndices.size()*sizeof(unsigned int), 
		&m_vecBodyIndices[0], GL_STATIC_DRAW );
	m_pBodyIBO->Release();

	m_pCore->SetNumBodyIndices( static_cast<int>(m_vecBodyIndices.size()) );

	// upload cap vertices to GPU
	m_pCapVBO->BindAsVertexDataBuffer();
	m_pCapVBO->BufferData(
		m_vecCapVertices.size()*sizeof(float), 
		&m_vecCapVertices[0], GL_STATIC_DRAW );
	m_pCapVBO->Release();

	 // we have 10 float values for every vertex and 4 vertices per Cap
	m_pCore->SetCapCount( static_cast<int>(m_vecCapVertices.size())/(10*4) );
}

void VflVirtualTubelets::UpdateTrajectoryData(
	VveParticleDataArrayBase* pTimeArray, 
	VveParticleDataArrayBase* pPosArray, 
	VveParticleDataArrayBase* pScalarArray,
	STrajectoryInfo& rTrajectoryInfo )
{
	if( m_dStartTime < rTrajectoryInfo.fMinTime ||
		m_dEndTime   > rTrajectoryInfo.fMaxTime )
		return;

	const int nTimeLevelCount = static_cast<int>( pTimeArray->GetSize() );
	if( nTimeLevelCount == 0 )
		return;

	// try to find beginning of pathline (i.e. the highest applicable time value)
	int iStartIndex = rTrajectoryInfo.iCurrentIndex;
	if( iStartIndex < 0 || pTimeArray->GetElementValueAsFloat( iStartIndex ) > m_dStartTime )
		iStartIndex = 0;

	float fStartTime  = pTimeArray->GetElementValueAsFloat( 0 );
	while( iStartIndex < nTimeLevelCount-1 && fStartTime < m_dStartTime )
		fStartTime = pTimeArray->GetElementValueAsFloat( ++iStartIndex );

	rTrajectoryInfo.iCurrentIndex = ( iStartIndex > 0 ) ?iStartIndex-1 : 0;

	// now, we have the head of the particle track, so let's go for the tail
	int   iEndIndex = iStartIndex;

	float fEndTime  = pTimeArray->GetElementValueAsFloat( iEndIndex );
	while( iEndIndex > 0 && fEndTime > m_dEndTime )
		fEndTime = pTimeArray->GetElementValueAsFloat( --iEndIndex );

	if( iEndIndex == iStartIndex )
		return;

	VistaVector3D v3Pos1;
	VistaVector3D v3Pos2;
	// Add vertices for the head of the particle track
	if( !m_bInterpolat )
	{
		VistaVector3D& v3Dir = rTrajectoryInfo.vecDirections[ iStartIndex-1 ];
		pPosArray->GetElementCopy( iStartIndex-1, &v3Pos1[0] );
		float fScalar = pScalarArray->GetElementValueAsFloat(iStartIndex-1);
		float fTime   = pTimeArray->GetElementValueAsFloat(iStartIndex-1);

		AddCap( v3Pos1, v3Dir, fScalar, fTime);
		AddBodyVetex( v3Pos1, v3Dir, fScalar, fTime, 1.0f );
	}
	else
	{
		VistaVector3D v3Dir1 = rTrajectoryInfo.vecDirections[ iStartIndex   ];
		VistaVector3D v3Dir2 = rTrajectoryInfo.vecDirections[ iStartIndex-1 ];
		pPosArray->GetElementCopy( iStartIndex,   &v3Pos1[0] );
		pPosArray->GetElementCopy( iStartIndex-1, &v3Pos2[0] );
		float fScalar1 = pScalarArray->GetElementValueAsFloat( iStartIndex   );
		float fScalar2 = pScalarArray->GetElementValueAsFloat( iStartIndex-1 );
		float fTime1   = pTimeArray->GetElementValueAsFloat( iStartIndex   );
		float fTime2   = pTimeArray->GetElementValueAsFloat( iStartIndex-1 );

		float fAlpha = (static_cast<float>(m_dStartTime) - fTime1) / (fTime2 - fTime1);
		fAlpha = Vista::Clamp( fAlpha, 0.0f, 1.0f );

		v3Pos1   = (1-fAlpha) * v3Pos1   + fAlpha * v3Pos2;
		v3Dir1   = (1-fAlpha) * v3Dir1   + fAlpha * v3Dir2;
		fScalar1 = (1-fAlpha) * fScalar1 + fAlpha * fScalar2;
		fTime1   = (1-fAlpha) * fTime1   + fAlpha * fTime2;

		v3Dir1.Normalize();

		AddCap( v3Pos1, v3Dir1, fScalar1, fTime1 );
		AddBodyVetex( v3Pos1, v3Dir1, fScalar1, fTime1, 1.0f );
		AddBodyVetex( v3Pos1, v3Dir1, fScalar1, fTime1, 0.0f );
	}

	// Add vertices for the body of the particle track
	for( int j=iStartIndex-1; j>iEndIndex; --j )
	{
		pPosArray->GetElementCopy( j, &v3Pos1[0] );
		AddBodyVetex( v3Pos1,  
			rTrajectoryInfo.vecDirections[j], 
			pScalarArray->GetElementValueAsFloat(j), 
			pTimeArray->GetElementValueAsFloat(j), 0.0f );
	}

	// Add vertices for the tail of the particle track
	if( !m_bInterpolat )
	{
		VistaVector3D& v3Dir = rTrajectoryInfo.vecDirections[ iEndIndex];
		pPosArray->GetElementCopy( iEndIndex, &v3Pos1[0] );
		float fScalar = pScalarArray->GetElementValueAsFloat(iEndIndex);
		float fTime   = pTimeArray->GetElementValueAsFloat(iEndIndex);

		AddBodyVetex( v3Pos1, v3Dir, fScalar, fTime, 0.0f );
		AddBodyVetex( v3Pos1, v3Dir, fScalar, fTime, 1.0f );
		AddCap( v3Pos1, -v3Dir, fScalar, fTime);
	}
	else
	{
		VistaVector3D v3Dir1 = rTrajectoryInfo.vecDirections[ iEndIndex+1 ];
		VistaVector3D v3Dir2 = rTrajectoryInfo.vecDirections[ iEndIndex   ];
		pPosArray->GetElementCopy( iEndIndex+1, &v3Pos1[0] );
		pPosArray->GetElementCopy( iEndIndex,   &v3Pos2[0] );
		float fScalar1 = pScalarArray->GetElementValueAsFloat( iEndIndex+1 );
		float fScalar2 = pScalarArray->GetElementValueAsFloat( iEndIndex   );
		float fTime1   = pTimeArray->GetElementValueAsFloat( iEndIndex+1 );
		float fTime2   = pTimeArray->GetElementValueAsFloat( iEndIndex   );

		float fAlpha = (static_cast<float>(m_dEndTime) - fTime1) / (fTime2 - fTime1);
		fAlpha = Vista::Clamp( fAlpha, 0.0f, 1.0f );

		v3Pos1   = (1-fAlpha) * v3Pos1   + fAlpha * v3Pos2;
		v3Dir1   = (1-fAlpha) * v3Dir1   + fAlpha * v3Dir2;
		fScalar1 = (1-fAlpha) * fScalar1 + fAlpha * fScalar2;
		fTime1   = (1-fAlpha) * fTime1   + fAlpha * fTime2;

		v3Dir1.Normalize();

		AddBodyVetex( v3Pos1, v3Dir1, fScalar1, fTime1, 0.0f );
		AddBodyVetex( v3Pos1, v3Dir1, fScalar1, fTime1, 1.0f );
		AddCap( v3Pos1, -v3Dir1, fScalar1, fTime1 );
	}

	m_vecBodyIndices.push_back( m_pCore->GetPrimitiveRestartIndex() );
}

void VflVirtualTubelets::AddBodyVetex( 
	const VistaVector3D& v3Pos, 
	const VistaVector3D& v3Dir, 
	float fScalar, float fTime, float fXOffset)
{
	// Add bottom Vertex
	m_vecBodyIndices.push_back( static_cast<int>(m_vecBodyVertices.size())/10 );

	m_vecBodyVertices.push_back( v3Pos[0] );
	m_vecBodyVertices.push_back( v3Pos[1] );
	m_vecBodyVertices.push_back( v3Pos[2] );

	m_vecBodyVertices.push_back( v3Dir[0] );
	m_vecBodyVertices.push_back( v3Dir[1] );
	m_vecBodyVertices.push_back( v3Dir[2] );

	m_vecBodyVertices.push_back( fScalar );
	m_vecBodyVertices.push_back( fTime   );

	m_vecBodyVertices.push_back( fXOffset );
	m_vecBodyVertices.push_back(    -1.0f );

	// Add top Vertex
	m_vecBodyIndices.push_back( static_cast<int>(m_vecBodyVertices.size())/10 );

	m_vecBodyVertices.push_back( v3Pos[0] );
	m_vecBodyVertices.push_back( v3Pos[1] );
	m_vecBodyVertices.push_back( v3Pos[2] );

	m_vecBodyVertices.push_back( v3Dir[0] );
	m_vecBodyVertices.push_back( v3Dir[1] );
	m_vecBodyVertices.push_back( v3Dir[2] );

	m_vecBodyVertices.push_back( fScalar );
	m_vecBodyVertices.push_back( fTime   );

	m_vecBodyVertices.push_back( fXOffset );
	m_vecBodyVertices.push_back(     1.0f );
}

void VflVirtualTubelets::AddCap( 
	const VistaVector3D& v3Pos, 
	const VistaVector3D& v3Dir,
	float fScalar, float fTime )
{
	// bottom right corner
	m_vecCapVertices.push_back( v3Pos[0] );
	m_vecCapVertices.push_back( v3Pos[1] );
	m_vecCapVertices.push_back( v3Pos[2] );

	m_vecCapVertices.push_back( v3Dir[0] );
	m_vecCapVertices.push_back( v3Dir[1] );
	m_vecCapVertices.push_back( v3Dir[2] );

	m_vecCapVertices.push_back( fScalar );

	m_vecCapVertices.push_back( fTime   );

	m_vecCapVertices.push_back(  1.0f );
	m_vecCapVertices.push_back( -1.0f );

	// bottom left corner
	m_vecCapVertices.push_back( v3Pos[0] );
	m_vecCapVertices.push_back( v3Pos[1] );
	m_vecCapVertices.push_back( v3Pos[2] );

	m_vecCapVertices.push_back( v3Dir[0] );
	m_vecCapVertices.push_back( v3Dir[1] );
	m_vecCapVertices.push_back( v3Dir[2] );

	m_vecCapVertices.push_back( fScalar );

	m_vecCapVertices.push_back( fTime   );

	m_vecCapVertices.push_back( -1.0f );
	m_vecCapVertices.push_back( -1.0f );

	// top left corner
	m_vecCapVertices.push_back( v3Pos[0] );
	m_vecCapVertices.push_back( v3Pos[1] );
	m_vecCapVertices.push_back( v3Pos[2] );

	m_vecCapVertices.push_back( v3Dir[0] );
	m_vecCapVertices.push_back( v3Dir[1] );
	m_vecCapVertices.push_back( v3Dir[2] );

	m_vecCapVertices.push_back( fScalar );

	m_vecCapVertices.push_back( fTime   );

	m_vecCapVertices.push_back( -1.0f );
	m_vecCapVertices.push_back(  1.0f );

	// top right corner
	m_vecCapVertices.push_back( v3Pos[0] );
	m_vecCapVertices.push_back( v3Pos[1] );
	m_vecCapVertices.push_back( v3Pos[2] );

	m_vecCapVertices.push_back( v3Dir[0] );
	m_vecCapVertices.push_back( v3Dir[1] );
	m_vecCapVertices.push_back( v3Dir[2] );

	m_vecCapVertices.push_back( fScalar );

	m_vecCapVertices.push_back( fTime   );

	m_vecCapVertices.push_back(  1.0f );
	m_vecCapVertices.push_back(  1.0f );
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVirtualTubelets::DrawOpaque()
{
	if (!m_pOwner->GetVisible())
		return;

	m_pCore->Draw();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVirtualTubelets::GetType() const
{
	return VflParticleRenderer::GetType() + string("::VflVirtualTubeletsVpFp");
}

void VflVirtualTubelets::SetDrawMode( int iMode )
{
	m_pCore->GetProperties()->SetDrawMode( iMode );
}
void VflVirtualTubelets::SetDrawMode( const std::string &strMode )
{
	if( strMode == "DM_SIMPLE" )
		m_pCore->GetProperties()->SetDrawMode( 
		VistaParticleRenderingProperties::DM_SIMPLE );

	if( strMode == "DM_SMOKE" )
		m_pCore->GetProperties()->SetDrawMode( 
		VistaParticleRenderingProperties::DM_SMOKE );

	if( strMode == "DM_BILLBOARDS" )
		m_pCore->GetProperties()->SetDrawMode( 
		VistaParticleRenderingProperties::DM_BILLBOARDS );

	if( strMode == "DM_BUMPED_BILLBOARDS" )
		m_pCore->GetProperties()->SetDrawMode( 
		VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS );

	if( strMode == "DM_BUMPED_BILLBOARDS_DEPTH_REPLACE" )
		m_pCore->GetProperties()->SetDrawMode( 
		VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS_DEPTH_REPLACE );
}
int VflVirtualTubelets::GetDrawMode() const
{
	return m_pCore->GetProperties()->GetDrawMode();
}
std::string VflVirtualTubelets::GetDrawModeAsString() const
{
	return GetDrawModeString( m_pCore->GetProperties()->GetDrawMode() );
}
std::string VflVirtualTubelets::GetDrawModeString(int iMode) const
{
	switch (iMode)
	{
	case VistaParticleRenderingProperties::DM_SIMPLE:
		return "DM_SIMPLE";
	case VistaParticleRenderingProperties::DM_SMOKE:
		return "DM_SMOKE";
	case VistaParticleRenderingProperties::DM_BILLBOARDS:
		return "DM_BILLBOARDS";
	case VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS:
		return "DM_BUMPED_BILLBOARDS";
	case VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS_DEPTH_REPLACE:
		return "DM_BUMPED_BILLBOARDS_DEPTH_REPLACE";
	}
	return VflParticleRenderer::GetDrawModeString( iMode );
}

int VflVirtualTubelets::GetDrawModeCount() const
{
	return VistaParticleRenderingProperties::DM_LAST;
}

void VflVirtualTubelets::SetSelectedTrajectories (const std::vector<int> & vecSelectedTrajectories)
{
	// overwrite set of selected trajectories
	m_vecSelectedTrajectories = vecSelectedTrajectories;

	// set back time stamp, so the next Update() call will call UpdateParticleData
	// and update visual information
	m_dDataTimeStamp = 0;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateParticleData                                          */
/*                                                                            */
/*============================================================================*/
void VflVirtualTubelets::UpdateParticleData()
{
	VflVisParticles::VflVisParticlesProperties *pParams = m_pOwner->GetProperties();

	const string strPosArray  = pParams->GetPositionArray();
	const string strTimeArray = pParams->GetTimeArray();

	VveParticlePopulationItem* pPopulationItem = m_pOwner->GetParticleData()->GetData();
	VveParticlePopulation*     pPopulation     = pPopulationItem->GetData();

	pPopulationItem->LockData();

	int iPathlineCount = pPopulation->GetNumTrajectories();

	m_vecTrajectoryInfos.resize( iPathlineCount );

	for( int i=0; i<iPathlineCount; ++i )
	{
		VveParticleTrajectory* pTraj = pPopulation->GetTrajectory( i );
		STrajectoryInfo& rTrajInfo   = m_vecTrajectoryInfos[i];

		if( !pTraj )
			continue;

		VveParticleDataArrayBase* pPosArray  = NULL;
		VveParticleDataArrayBase* pTimeArray = NULL;
		if( pTraj->GetArrayByName(  strPosArray, pPosArray  ) && pPosArray &&
			pTraj->GetArrayByName( strTimeArray, pTimeArray ) && pTimeArray )
		{
			int iTimeLevelCount = static_cast<int>(pTimeArray->GetSize());

			rTrajInfo.iCurrentIndex = 0;
			rTrajInfo.fMinTime = pTimeArray->GetElementValueAsFloat( 0 );
			rTrajInfo.fMaxTime = pTimeArray->GetElementValueAsFloat( iTimeLevelCount -1 );
			rTrajInfo.vecDirections.resize( iTimeLevelCount );

			for(int j=iTimeLevelCount-1; j>0; --j)
			{
				float aPos1[3], aPos2[3];
				pPosArray->GetElementCopy(j, aPos1);
				pPosArray->GetElementCopy(j-1, aPos2);

				VistaVector3D& rDirection = rTrajInfo.vecDirections[j];

				rDirection[0] = aPos1[0] - aPos2[0];
				rDirection[1] = aPos1[1] - aPos2[1];
				rDirection[2] = aPos1[2] - aPos2[2];
				rDirection.Normalize();
			}
			if( iTimeLevelCount>1)
				rTrajInfo.vecDirections[0] = rTrajInfo.vecDirections[1];
		}
		else
		{
			rTrajInfo.iCurrentIndex = 0;
		}
	}

	pPopulationItem->UnlockData();
	m_dDataTimeStamp = m_pOwner->GetDataChangeTimeStamp();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateVisParams                                             */
/*                                                                            */
/*============================================================================*/
void VflVirtualTubelets::UpdateVisParams()
{
	VflVisParticles::VflVisParticlesProperties *pProperties = m_pOwner->GetProperties();

	m_fTimeWindow = pProperties->GetTimeWindow();

	m_pCore->SetParticleRadius( pProperties->GetRadiusAbsolute() );

	m_dVisParamsTimeStamp = m_pOwner->GetVisParamsChangeTimeStamp();
}

/*============================================================================*/
/*  END OF FILE "VflVirtualTubeletsVpFp.cpp"                                  */
/*============================================================================*/
