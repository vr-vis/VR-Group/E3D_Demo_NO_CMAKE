/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflVtkTubes.h"
#include "VflVisParticles.h"
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include "../VflVisTiming.h"

//#include "VflVisParticlesParams.h"
#include "../VflVtkLookupTable.h"

#include <VistaVisExt/Tools/VtkObjectStack.h>

#include <vtkProperty.h>
#include <vtkTubeFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkLookupTable.h>
#include <vtkRenderer.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkCleanPolyData.h>
#include <vtkPolyLine.h>

#include <VistaTools/VistaProgressBar.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

VflVtkTubes::VflVtkTubes(VflVisParticles *pOwner, bool bImmediateMode,
						    int nNumberOfSides,
							vtkProperty *pProperty)
: VflParticleRenderer(pOwner), 
  m_iCurrentTimeIndex(-1), 
  m_bInitialized(false),
  m_pProperty(pProperty), 
  m_pVtkObjectStack(NULL), 
  m_pParticleDataStack(NULL),
  m_bImmediateMode(bImmediateMode), 
  m_iNumberOfSides(nNumberOfSides), 
  m_fTimeWindow(0), 
  m_iMaxParticles(-1)
{
}


VflVtkTubes::~VflVtkTubes()
{
	this->Destroy();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflVtkTubes::Init()
{
	m_pVtkObjectStack = new VtkObjectStack();
	m_pParticleDataStack = new VtkObjectStack();

	// if no rendering property is available, create one
	if (!m_pProperty)
	{
		m_pProperty = vtkProperty::New();
		m_pVtkObjectStack->PushObject(m_pProperty);
	}

	vtkLookupTable *pLookupTable = m_pOwner->GetLookupTable()->GetLookupTable();

	// create rendering pipelines for every time level
	VveTimeMapper *pTimeMapper = m_pOwner->GetParticleData()->GetTimeMapper();
	int iNumberOfLevels = pTimeMapper->GetNumberOfTimeIndices();
	int i;

	m_vecPositions.resize(iNumberOfLevels);
	m_vecTubeFilters.resize(iNumberOfLevels);
	m_vecActors.resize(iNumberOfLevels);

	for (i=0; i<iNumberOfLevels; ++i)
	{
		vtkPolyData *pPolys = vtkPolyData::New();
		pPolys->Allocate(1, 1);
		m_vecPositions[i] = pPolys;
		// TODO - memory leak, fixed??
		m_pVtkObjectStack->PushObject(pPolys);

		vtkCleanPolyData *pClean = vtkCleanPolyData::New();
		m_pVtkObjectStack->PushObject(pClean);
#if VTK_MAJOR_VERSION > 5
		pClean->SetInputData(pPolys);
#else
		pClean->SetInput(pPolys);
#endif

		vtkTubeFilter *pTubes = vtkTubeFilter::New();
		m_pVtkObjectStack->PushObject(pTubes);
		m_vecTubeFilters[i]=pTubes;
		pTubes->SetInputConnection(pClean->GetOutputPort());
		pTubes->SetNumberOfSides(m_iNumberOfSides);
		pTubes->CappingOn();

		pClean = vtkCleanPolyData::New();
		m_pVtkObjectStack->PushObject(pClean);
		pClean->SetInputConnection(pTubes->GetOutputPort());

		vtkPolyDataMapper *pMapper = vtkPolyDataMapper::New();
		m_pVtkObjectStack->PushObject(pMapper);
		pMapper->SetInputConnection(pClean->GetOutputPort());
		pMapper->SetLookupTable(pLookupTable);
		pMapper->SetScalarVisibility(true);
		pMapper->UseLookupTableScalarRangeOn();
		pMapper->SetImmediateModeRendering(m_bImmediateMode);

		vtkActor *pActor = vtkActor::New();
		m_pVtkObjectStack->PushObject(pActor);
		pActor->SetMapper(pMapper);
		pActor->SetProperty(m_pProperty);
		m_vecActors[i] = pActor;
	}

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Destroy                                                     */
/*                                                                            */
/*============================================================================*/
void VflVtkTubes::Destroy()
{
	m_pVtkObjectStack->DeleteObjects();
	m_pParticleDataStack->DeleteObjects();

	m_vecActors.clear();
	m_vecTubeFilters.clear();
	m_vecPositions.clear();

	m_pProperty = NULL;

	delete m_pParticleDataStack;
	m_pParticleDataStack = NULL;
	delete m_pVtkObjectStack;
	m_pVtkObjectStack = NULL;

	m_bInitialized = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                     */
/*                                                                            */
/*============================================================================*/
void VflVtkTubes::Update()
{
	if (m_dVisParamsTimeStamp < m_pOwner->GetVisParamsChangeTimeStamp())
		UpdateVisParams();
	if (m_dDataTimeStamp < m_pOwner->GetDataChangeTimeStamp())
		UpdateParticleData();

	// determine time level to be rendered
	VveTimeMapper *pTimeMapper = m_pOwner->GetParticleData()->GetTimeMapper();
	m_iCurrentTimeIndex = pTimeMapper->GetTimeIndex(
		m_pOwner->GetRenderNode()->GetVisTiming()->GetVisualizationTime());

	if (m_iCurrentTimeIndex < 0)
		return;

	// kick of update of current mapper
	m_vecActors[m_iCurrentTimeIndex]->GetMapper()->Update();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVtkTubes::DrawOpaque()
{
	if (m_iCurrentTimeIndex < 0)
		return;

	m_vecActors[m_iCurrentTimeIndex]->RenderOpaqueGeometry(
		m_pOwner->GetRenderNode()->GetVtkRenderer());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVtkTubes::DrawTransparent()
{
	if (m_iCurrentTimeIndex < 0)
		return;


#if VTK_MAJOR_VERSION > 5 || (VTK_MAJOR_VERSION == 5 && VTK_MINOR_VERSION >=1)
	m_vecActors[m_iCurrentTimeIndex]->RenderTranslucentPolygonalGeometry(
		m_pOwner->GetRenderNode()->GetVtkRenderer());
#else
	m_vecActors[m_iCurrentTimeIndex]->RenderTranslucentGeometry(
		m_pOwner->GetRenderNode()->GetVtkRenderer());
#endif
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVtkTubes::GetType() const
{
	return VflParticleRenderer::GetType() + std::string("::VflVtkTubes");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetNumberOfSides                                            */
/*                                                                            */
/*============================================================================*/
void VflVtkTubes::SetNumberOfSides(int iSides)
{
	if (iSides < 3)
		return;

	m_iNumberOfSides = iSides;

	const size_t nLevelCount = m_vecTubeFilters.size();
	for (size_t i=0; i<nLevelCount; ++i)
	{
		m_vecTubeFilters[i]->SetNumberOfSides(m_iNumberOfSides);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateParticleData                                          */
/*                                                                            */
/*============================================================================*/
void VflVtkTubes::UpdateParticleData()
{
	vstr::outi() << " [VflVtkTubes] - updating internal data..." << endl;

	VveTimeMapper *pMapper = m_pOwner->GetTimeMapper();

	m_pOwner->GetParticleData()->GetData()->LockData();
	VveParticlePopulation *pParticles = m_pOwner->GetParticleData()->GetData()->GetData();

    VflVisParticles::VflVisParticlesProperties *pParams = 
        dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(
        m_pOwner->GetProperties());

	int iPathlineCount = pParticles->GetNumTrajectories();

	// delete old vtk particle data - this invalidates all pointers in m_vecPositions!!!
	m_pParticleDataStack->DeleteObjects();

	// hm, we've been stumbling about this pointer problem before, so 
	// we just set 'em all to NULL...
	const size_t nLevelCount = m_vecPositions.size();
	for(size_t i=0; i<nLevelCount; ++i)
	{
		m_vecPositions[i] = NULL;
	}

	int iParticleStep = 1;
	if (m_iMaxParticles>0)
	{
		vstr::outi() << " [VflVtkTubes] - maximum number of particles requested: " << m_iMaxParticles << endl;
		vstr::outi() << "                 size of whole population: " << iPathlineCount << endl;

		// determine particle step size i (i.e. take every ith particle)
		iParticleStep = (iPathlineCount-1) / m_iMaxParticles;
		++iParticleStep;
		iPathlineCount = iPathlineCount / iParticleStep;

		vstr::outi() << "                 drawing at most " << iPathlineCount << " particles..." << endl;
	}

	std::vector<int> vecParticleLevel(iPathlineCount);

	for (int i=0; i<iPathlineCount; ++i)
	{
		vecParticleLevel[i] = 0;
	}

	vstr::outi() << " [VflVtkTubes] - updating internal vtk poly data structures..." << endl;

	VistaProgressBar oBar(nLevelCount);
	oBar.SetBarTicks(30);
	oBar.SetPrefixString("                 ");
	oBar.Start();

	for (size_t i=0; i<nLevelCount; ++i)
	{
		oBar.Increment();
//		vstr::outi() << "                time level " << (i+1) << " of " << iLevelCount << "\r";
//		vstr::outi().flush();

		vtkFloatArray *pScalars = vtkFloatArray::New();
		m_pParticleDataStack->PushObject(pScalars);
		pScalars->Initialize();

		vtkFloatArray *pVectors = vtkFloatArray::New(); 
		m_pParticleDataStack->PushObject(pVectors);
		pVectors->SetNumberOfComponents(3);
		pVectors->Initialize();

		vtkPoints *pPoints = vtkPoints::New();
		m_pParticleDataStack->PushObject(pPoints);
//		pPoints->SetData(pScalars);
		int iPointCount = 0;
		pPoints->Initialize();

		vtkPolyData *pPolys = vtkPolyData::New();
		m_pParticleDataStack->PushObject(pPolys);
		pPolys->Allocate(1, 1);
		pPolys->SetPoints(pPoints);
		pPolys->GetPointData()->SetScalars(pScalars);
		pPolys->GetPointData()->SetVectors(pVectors);

		float fSimTime = pMapper->GetSimulationTime(static_cast<int>(i));

		// find particle data to be displayed during the current time step
		int iParticle = 0, iEndIndex;
		float fPathlineEndTime = fSimTime - m_fTimeWindow;
		for (int j=0; j<iPathlineCount; ++j, iParticle+=iParticleStep)
		{
            VveParticleTrajectory *pTraj = pParticles->GetTrajectory(iParticle);
            VveParticleDataArrayBase *pTimeArray = NULL;
            VveParticleDataArrayBase *pPosArray = NULL;
            VveParticleDataArrayBase *pVelArray = NULL;
            VveParticleDataArrayBase *pScalarArray = NULL;
            bool bInitArrays = pTraj &&
                pTraj->GetArrayByName(pParams->GetTimeArray(), pTimeArray) &&
                pTraj->GetArrayByName(pParams->GetPositionArray(), pPosArray) &&
                pTraj->GetArrayByName(pParams->GetVelocityArray(), pVelArray) &&
                pTraj->GetArrayByName(pParams->GetScalarArray(), pScalarArray) &&
                pTimeArray && pPosArray && pVelArray && pScalarArray;
            if( !bInitArrays)
                continue;

	        const int iTrajectorySize = static_cast<int>(pTimeArray->GetSize());

			while( vecParticleLevel[j] < iTrajectorySize-1 &&
				   pTimeArray->GetElementValueAsFloat(vecParticleLevel[j]) <= fSimTime )
			{
				++vecParticleLevel[j];
			}

			// now, we have the head of the particle track in vecParticleLevel[j],
			// so we "just" have to find its tail...
			iEndIndex = vecParticleLevel[j];
			while( iEndIndex > 0 &&
				   pTimeArray->GetElementValueAsFloat(iEndIndex) > fPathlineEndTime )
			{
				--iEndIndex;
			}

			if (vecParticleLevel[j] == iEndIndex)
			{
				// hm, obviously no data here...
				continue;
			}

			// alright, got it - so copy the corresponding data into a vtk polyline
			vtkPolyLine *pPolyLine = vtkPolyLine::New();
			int iPointId;
			m_pParticleDataStack->PushObject(pPolyLine);

			for (int k=vecParticleLevel[j]; k>=iEndIndex; --k)
			{
                float aPos[3], aVel[3], fScalar;
                pPosArray->GetElementCopy(k, aPos);
                pVelArray->GetElementCopy(k, aVel);
                fScalar = pScalarArray->GetElementValueAsFloat(k);
                
                iPointId = pPoints->InsertNextPoint(aPos);
				pScalars->InsertNextValue(fScalar);
				pVectors->InsertNextTuple(aVel);
				pPolyLine->GetPointIds()->InsertNextId(iPointId);
			}
			pPolys->InsertNextCell(pPolyLine->GetCellType(), pPolyLine->GetPointIds());
		}
#if VTK_MAJOR_VERSION > 5
		m_vecTubeFilters[i]->SetInputData(pPolys);
#else
		m_vecTubeFilters[i]->SetInput(pPolys);
#endif
		m_vecPositions[i] = pPolys;
	}
	oBar.Finish();

	m_dDataTimeStamp = m_pOwner->GetDataChangeTimeStamp();

	m_pOwner->GetParticleData()->GetData()->UnlockData();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateVisParams                                             */
/*                                                                            */
/*============================================================================*/
void VflVtkTubes::UpdateVisParams()
{
	VflVisParticles::VflVisParticlesProperties *pParams = 
		dynamic_cast<VflVisParticles::VflVisParticlesProperties *>(m_pOwner->GetProperties());

	if(!pParams)
		return;

	float fRadius = pParams->GetRadiusAbsolute();
	const size_t nLevelCount = m_vecTubeFilters.size();
	for (size_t i=0; i<nLevelCount; ++i)
	{
		m_vecTubeFilters[i]->SetRadius(fRadius);
	}

	if (m_fTimeWindow != pParams->GetTimeWindow())
	{
		m_fTimeWindow = pParams->GetTimeWindow();

		// force data update...
		m_dDataTimeStamp = -1;
	}

	if (m_iMaxParticles != pParams->GetMaxCount())
	{
		m_iMaxParticles = pParams->GetMaxCount();

		// force data update...
		m_dDataTimeStamp = -1;
	}

	m_dVisParamsTimeStamp = m_pOwner->GetVisParamsChangeTimeStamp();
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflVtkTubes.cpp"                                            */
/*============================================================================*/
