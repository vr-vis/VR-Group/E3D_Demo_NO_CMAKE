/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflParticleColorData.h"
#include "VflStaticEllipsoidTensorIcon.h"
#include "VflVisParticles.h"

#include "../VflVisController.h"
#include "../VflVisTiming.h"
#include "../VflVtkLookupTable.h"

#include <typeinfo>
#include "VistaVisExt/Data/VveParticleTrajectory.h"

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflStaticEllipsoidTensorIcon::VflStaticEllipsoidTensorIcon(VflVisParticles *pOwner, 
												 bool bImmediateMode)
: VflEllipsoidTensorIcon(pOwner, bImmediateMode), m_bNeedToUpdate(true)
{
}

VflStaticEllipsoidTensorIcon::~VflStaticEllipsoidTensorIcon()
{
	this->Destroy();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflStaticEllipsoidTensorIcon::Update()
{
	if (m_dDataTimeStamp < m_pOwner->GetDataChangeTimeStamp())
	{
		UpdateParticleData();
	}
	if (m_dVisParamsTimeStamp < m_pOwner->GetVisParamsChangeTimeStamp())
	{
		UpdateVisParams();
	}

	if(!m_bNeedToUpdate)
		return;

    // get properties as pParam from owner
    VflVisParticles::VflVisParticlesProperties *pParams = 
        dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(m_pOwner->GetProperties());

	unsigned int iPathlineCount = (unsigned int) m_pPopulation->GetNumTrajectories();

	//=================
	// Transformations
	//=================

	// clean old data
	for(unsigned int i=0; i<m_vecTransMatrix.size(); ++i)  
	{
		delete[] m_vecTransMatrix[i];
	}
	m_vecTransMatrix.clear();

	for(size_t i=0; i<m_vecAxisLength.size(); ++i)
	{
		delete[] m_vecAxisLength[i];
	}
	m_vecAxisLength.clear();


	// iterate over the collection of pathlines
	for(unsigned int i=0; i<iPathlineCount; ++i)
	{
        VveParticleTrajectory *pTraj = m_pPopulation->GetTrajectory(i);
        VveParticleDataArrayBase *pPosArray = NULL;
        VveParticleDataArrayBase *pEigenValueArray = NULL;
        VveParticleDataArrayBase *pEigenVectorArray = NULL;

        if( !pTraj ||
            !pTraj->GetArrayByName(pParams->GetPositionArray(), pPosArray) || !pPosArray ||
            !pTraj->GetArrayByName(pParams->GetEigenValueArray(), pEigenValueArray) || !pEigenValueArray ||
            !pTraj->GetArrayByName(pParams->GetEigenVectorArray(), pEigenVectorArray) || !pEigenVectorArray )
            continue;

        const size_t nTrajectorySize = pTraj->GetSize();

		// iterate over the collection of trace points in i-th pathline
		for(size_t j=0; j<nTrajectorySize; ++j)
		{
            GLfloat *fTmpMatrix = new GLfloat[16];
            GLfloat *fTmpAxisLength = new GLfloat[3];

            // Get position
            float aPos[3];
            pPosArray->GetElementCopy(j, aPos, 9);

            // Get eigenvalue 
            pEigenValueArray->GetElementCopy(j, fTmpAxisLength );

            // Get eigenvectors
            float aEigenVectors[9];
            pEigenVectorArray->GetElementCopy(j, aEigenVectors, 9);

            // Swap 0<->1: eigenvalues
            float fTemp = fTmpAxisLength[0];
            fTmpAxisLength[0] = fTmpMatrix[1];
            fTmpAxisLength[1] = fTemp;            
            
			// translate & scale & rotate as base change matrix M
			//      (  EV1  EV0  EV2  aPos[0]  )     (  m0  m4  m8  m12  )
			// M := (  EV1  EV0  EV2  aPos[1]  )  =  (  m1  m5  m9  m13  )
			//      (  EV1  EV0  EV2  aPos[2]  )     (  m2  m6  m10 m14  )
			//      (   0    0    0    1       )     (  m3  m7  m11 m15  )   column-major (openGL's "style")		
            //
            // eigenvector 0 & 1 are swapped as well!

			for(int iIdx=0; iIdx<3; ++iIdx) // 0->x, 1->y, 2->z
			{
                fTmpMatrix[iIdx]    = fTmpAxisLength[iIdx] * aEigenVectors[3+iIdx];
                fTmpMatrix[4+iIdx]  = fTmpAxisLength[iIdx] * aEigenVectors[0+iIdx];
                fTmpMatrix[8+iIdx]  = fTmpAxisLength[iIdx] * aEigenVectors[6+iIdx];
                fTmpMatrix[12+iIdx] = aPos[iIdx];
			}

            // last row of transformation matrix -> ext. coordinates:
            fTmpMatrix[3]  = 0.0; //vector
            fTmpMatrix[7]  = 0.0; //vector
            fTmpMatrix[11] = 0.0; //vector
            fTmpMatrix[15] = 1.0; //point

			m_vecTransMatrix.push_back(fTmpMatrix);
			m_vecAxisLength.push_back(fTmpAxisLength);
		}
	}

	//========
	// Colors
	//========

	if(m_bUpdatePopulationColor)
	{
		GLfloat *fTmpColor;
		// get current color data
		const VflParticleColorData::POPULATION_COLORS &refColors = m_pColorData->GetColors();

		// clear old data
		for(unsigned int i=0; i<m_vecColor.size(); ++i)
		{
			delete[] m_vecColor[i];
		}
		m_vecColor.clear();

		// iterate over the collection of pathlines
		for(unsigned int i=0; i<iPathlineCount; ++i)
		{
			//SVveParticleTrajectory &refTrajectory = m_pPopulation->vecPopulation[i];
            VveParticleTrajectory *pTraj = m_pPopulation->GetTrajectory(i);
            if( !pTraj )
                continue;

			size_t iParticleCount = pTraj->GetSize();

			// iterate over the collection of trace points in i-th pathline
			for(size_t j=0; j<iParticleCount; ++j)
			{
				fTmpColor = new GLfloat[4];
				fTmpColor[0] = refColors[i][j].aColor[0];
				fTmpColor[1] = refColors[i][j].aColor[1];
				fTmpColor[2] = refColors[i][j].aColor[2];
				fTmpColor[3] = refColors[i][j].aColor[3];

				m_vecColor.push_back(fTmpColor);
			}
		}
		m_bUpdatePopulationColor = false;
	}

	// update m_iCurrentTimeIndex    -> todo: make this better
	m_bNeedToUpdate = false;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateParticleData                                          */
/*                                                                            */
/*============================================================================*/
void VflStaticEllipsoidTensorIcon::UpdateParticleData()
{
	m_pOwner->GetParticleData()->GetData()->LockData();
	VveParticlePopulation *pOriginalPopulation = m_pOwner->GetParticleData()->GetData()->GetData();

	// update local particle data
	//vstr::debugi() << " [VflStaticEllipsoidTensorIcon] - update local particle data... (test)" << endl;

    // @TODO: Is it required to perform a deep copy of the original Particle Data?
    m_pPopulation = pOriginalPopulation;

    if( m_pPopulation->GetNumTrajectories() <= 0)
	{
		m_pOwner->GetParticleData()->GetData()->UnlockData();
        return;
	}

	int iMaxNumOfTracePoints = 0;
	for(int i=0; i<m_pPopulation->GetNumTrajectories(); ++i)
	{
        VveParticleTrajectory* pTraj = m_pPopulation->GetTrajectory(i);
        if( pTraj )
            iMaxNumOfTracePoints = std::max(iMaxNumOfTracePoints, static_cast<int>(pTraj->GetSize()));
	}
	m_vecTransMatrix.reserve(m_pPopulation->GetNumTrajectories() * iMaxNumOfTracePoints);


    VflVisParticles::VflVisParticlesProperties *pParams = 
        dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(
        m_pOwner->GetProperties());

	// test whether the new data has the required information for this renderer
    // by checking if the appropriate particle data arrays are present for the first trajectory
    VveParticleTrajectory *pTraj = m_pPopulation->GetTrajectory(0);
    VveParticleDataArrayBase *pEigenValueArray = NULL;
    VveParticleDataArrayBase *pEigenVectorArray = NULL;

    if( !pTraj ||
        !pTraj->GetArrayByName(pParams->GetEigenValueArray(), pEigenValueArray) || !pEigenValueArray ||
        !pTraj->GetArrayByName(pParams->GetEigenVectorArray(), pEigenVectorArray) || !pEigenVectorArray)
	{
        vstr::errp() << "[VflEllipsoidTensorIcon] in UpdateParticleData(): required particle data array not found" << endl;
		m_pOwner->GetParticleData()->GetData()->UnlockData();
		return;
	}

	m_pPopulationItem->SetData(m_pPopulation);
	m_pPopulationItem->Notify();	

	m_dDataTimeStamp = m_pOwner->GetDataChangeTimeStamp();
	m_pOwner->GetParticleData()->GetData()->UnlockData();

	m_bNeedToUpdate = true;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflStaticEllipsoidTensorIcon.cpp"                            */
/*============================================================================*/
