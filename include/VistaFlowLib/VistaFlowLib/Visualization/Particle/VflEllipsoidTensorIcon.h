/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLELLIPSOIDTENSORICON_H
#define _VFLELLIPSOIDTENSORICON_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <GL/glew.h>

#include "VflParticleRenderer.h"

#if defined(DARWIN)
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
#endif

#include <iostream>
#include <string>
#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisParticles;
class VflParticleColorData;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflEllipsoidTesorIcon is a special renderer for particle data which 
 * additionally contains 3 positive eigenvalues and corresponding 3 eigenvectors
 * of a symmetric 3x3 tensor. This eigensystem is than used to define the axis
 * of an ellipsoid icon.
 * Only particle datas which have the extention type "VveParticleExtTensor"
 * can be renderer by this class.
 */    
class VISTAFLOWLIBAPI VflEllipsoidTensorIcon : public VflParticleRenderer
{
public:
    // CONSTRUCTORS / DESTRUCTOR
	VflEllipsoidTensorIcon(VflVisParticles *pOwnerbool, 
		bool bImmediateMode=false);
	virtual ~VflEllipsoidTensorIcon();

	/**
     * Initialize renderer.
     */    
    virtual bool Init();

	/**
	 * Destroy internal data.
	 */
	virtual void Destroy();

	/**
	 * Update the renderer, i.e. allow it to perform any data updates and such
	 * outside the actual render process.
	 */
	virtual void Update();

	/**
	 * Draw any opaque stuff.
	 */
	virtual void DrawOpaque();

	/**
	 * Draw any transparent stuff.
	 */
	virtual void DrawTransparent();

	bool GetColoringXYZaxes();
	void SetColoringXYZaxes(bool bColoring);

	void SetQuadricSize(double dSize);
	double GetQuadricSize();

	void SetAxesRadiusFactor(double dFactor);
	double GetAxesRadiusFactor();

	/**
	 * Color adjustment
	 * ambient := (R, G, B, Alpha)
	 * diffuse := (., ., ., Alpha),   (R,G,B) values depend on given LUT
	 */
	void SetAmbientColor(float aAmbient[4]);
	void GetAmbientColor(float aAmbient[4]);

	void SetDiffuseOpacity(float fAlpha);
	float GetDiffuseOpacity();

protected:

	void UpdateParticleData();
	void UpdateVisParams();

	VveParticlePopulation *m_pPopulation;
	VveParticlePopulationItem *m_pPopulationItem;
	
	unsigned int m_iCurrentTimeIndex;
	bool m_bInitialized;
	bool m_bImmediateMode;

	GLUquadricObj *m_pQuadric;
	GLUquadricObj *m_pQuadricAxis[3];
	double m_dQuadricSize;

	/**
	* hold transformation matrix for each tensor glyph
	* size := num particles to be renderer 
	*/
	std::vector<GLfloat*> m_vecTransMatrix;

	/**
	 * hold length of axis (x-y-z)
	 * size := 3 * num particles to be rendered	 
	 */
	std::vector<GLfloat*> m_vecAxisLength;

	/**
	* define color table to visualize scalar datas
	*/
	VflParticleColorData *m_pColorData;
	bool m_bUpdatePopulationColor;
	std::vector<GLfloat*> m_vecColor;

	bool m_bColoringXYZaxes;
	double m_dAxesRadiusFactor;    // relative to quadric size

	float m_aAmbientColor[4];
	float m_fDiffuseOpacity;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLELLIPSOIDTENSORICON_H)
