/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLSTATICELLIPSOIDTENSORICON_H
#define _VFLSTATICELLIPSOIDTENSORICON_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "VflEllipsoidTensorIcon.h"

#if defined(DARWIN)
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
#endif

#include <iostream>
#include <string>
#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisParticles;
class VflParticleColorData;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflStaticEllipsoidTesorIcon is the static version of VflEllipsoidTensorIcon. 
 * Only particle datas which have the extention type "VveParticleExtTensor"
 * can be renderer by this class.
 */    
class VISTAFLOWLIBAPI VflStaticEllipsoidTensorIcon : public VflEllipsoidTensorIcon
{
public:
    // CONSTRUCTORS / DESTRUCTOR
	VflStaticEllipsoidTensorIcon(VflVisParticles *pOwnerbool, 
		bool bImmediateMode=false);
	virtual ~VflStaticEllipsoidTensorIcon();

	/**
	 * Update the renderer, i.e. allow it to perform any data updates and such
	 * outside the actual render process.
	 */
	virtual void Update();

protected:

	void UpdateParticleData();
	bool m_bNeedToUpdate;

};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLSTATICELLIPSOIDTENSORICON_H)
