/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifdef WIN32
	#pragma warning(disable:4786)
#endif

#include <GL/glew.h>

#include "VflVisParticles.h"
//#include "VflVisController.h"
#include "VflParticleRenderer.h"
//#include "VflVisParticlesParams.h"
#include "../VflVtkLookupTable.h"
#include "../VflVisTiming.h"
#include "../VflRenderNode.h"
#include "../IVflTransformer.h"

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaMath/VistaBoundingBox.h>
#include <VistaKernel/GraphicsManager/VistaGroupNode.h>
#include <VistaKernel/GraphicsManager/VistaTransformNode.h>
#include <VistaKernel/GraphicsManager/VistaGeometryFactory.h>

#include <cassert>
#include <algorithm>
#include "VistaVisExt/Data/VveParticleTrajectory.h"

using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflVisParticles::VflVisParticles()
: IVflVisObject(),
  m_pData(NULL),
  m_pLookupTable(NULL),
  m_iCurrentRenderer(-1),
  m_dDataTimeStamp(0),
  m_dVisParamsTimeStamp(0),
  m_bBoundsDirty(false)
{
	m_afMinB[0] = m_afMinB[1] = m_afMinB[2] = 0.0f;
	m_afMaxB[0] = m_afMaxB[1] = m_afMaxB[2] = 0.0f;
}

VflVisParticles::~VflVisParticles()
{
#ifdef DEBUG
	vstr::debugi() << "destroying VflVisParticles: " << endl << (*this) << endl;
#endif

	// unregister self with objects to be observed
	//if(m_pData && m_pData->GetData())
	//	ReleaseObserveable(m_pData->GetData(), IVistaObserveable::TICKET_NONE);

	if(m_pLookupTable)
		ReleaseObserveable(m_pLookupTable, IVistaObserveable::TICKET_NONE);

	// we don't destroy the renderers here, because we haven't been responsible
	// for creating them, and thus we are not responsible for destroying them ;-)
	// UPDATE: well, we DO destroy them - unless they have explicitly been
	// removed beforehand...
	for (std::vector<VflParticleRenderer*>::size_type i=0;
		 i<m_vecRenderers.size(); ++i)
	{
		delete m_vecRenderers[i];
	}
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
bool VflVisParticles::SetUnsteadyData(VveUnsteadyData *pD)
{
	VveParticleDataCont *pPrtData = 
                             dynamic_cast<VveParticleDataCont*>(pD);
	if(!pPrtData)
		return false; // reject, not for me...

	if(!IVflVisObject::SetUnsteadyData(pD))
	{
		return false; // reject, something went wrong
	}

	SetParticleData(pPrtData);
	return true; // ok, everything is fine
}

void VflVisParticles::SetParticleData(VveParticleDataCont *pData)
{
	if(m_pData && m_pData->GetData())
		m_pData->GetData()->DetachObserver(this);

	m_pData = pData;

	if( m_pData && m_pData->GetData())
		Observe(m_pData->GetData(), E_PARTICLEDATA_CHANGE );

	m_bBoundsDirty = true;
}



void VflVisParticles::SetLookupTable(VflVtkLookupTable *pLUT)
{
	assert(pLUT);
	if(m_pLookupTable)
		m_pLookupTable->DetachObserver(this);
	
	m_pLookupTable = pLUT;
	Observe(m_pLookupTable, E_LOOKUPTABLE_CHANGE);
}


bool VflVisParticles::Init()
{
	if(IVflVisObject::Init() == false)
		return false;

	// set appropriate time stamps
	m_dDataTimeStamp      = 0; //pController->GetVisTiming()->GetLifeTime();
	m_dVisParamsTimeStamp = 0; //pController->GetVisTiming()->GetLifeTime();




	return true;
}

void VflVisParticles::Update()
{
	if (m_iCurrentRenderer < 0)
		return;

	const double dVisTime =
		GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	const double dSimTime =
		m_pData->GetTimeMapper()->GetSimulationTime(dVisTime);

	VistaTransformMatrix oCurTrans;
	if(NULL != GetTransformer())
	{
		GetTransformer()->GetUnsteadyTransform(dSimTime, oCurTrans);
	}
	else
	{
		oCurTrans.SetToIdentity();
	}
	oCurTrans.GetTransposedValues(m_aCurTrans);
	
	if(m_dDataTimeStamp == 0)
		m_dDataTimeStamp = GetRenderNode()->GetVisTiming()->GetLifeTime();

	if(m_dVisParamsTimeStamp == 0)
		m_dVisParamsTimeStamp = GetRenderNode()->GetVisTiming()->GetLifeTime();

	if(GetParticleData() && GetParticleData()->GetData())
		m_vecRenderers[m_iCurrentRenderer]->Update();
}


void VflVisParticles::DrawOpaque()
{
	if(m_iCurrentRenderer<0)
		return;

	glPushMatrix();
	glMultMatrixf(m_aCurTrans);

	if(GetParticleData() && GetParticleData()->GetData())
		m_vecRenderers[m_iCurrentRenderer]->DrawOpaque();

	glPopMatrix();
}

void VflVisParticles::DrawTransparent()
{
	if (m_iCurrentRenderer<0)
		return;

	glPushMatrix();
	glMultMatrixf(m_aCurTrans);

	if(GetParticleData() && GetParticleData()->GetData())
		m_vecRenderers[m_iCurrentRenderer]->DrawTransparent();

	glPopMatrix();
}

unsigned int VflVisParticles::GetRegistrationMode() const
{
	return OLI_UPDATE | OLI_DRAW_OPAQUE | OLI_DRAW_TRANSPARENT;
}


std::string VflVisParticles::GetType() const
{
	return IVflVisObject::GetType()+string("::VflVisParticles");
}


void VflVisParticles::Debug(std::ostream & out) const
{
	IVflVisObject::Debug(out);

	out << " [VflVisParticles] Data time stamp:          " << m_dDataTimeStamp << endl;
	out << " [VflVisParticles] Vis parameter time stamp: " << m_dVisParamsTimeStamp << endl;
	out << " [VflVisParticles] NumberOfRenderers: " << GetNumberOfRenderers() << endl;

	if (m_iCurrentRenderer >= 0)
	{
		out << " [VflVisParticles] Active renderer:" << endl;
		m_vecRenderers[m_iCurrentRenderer]->Debug(out);
	}
}


void VflVisParticles::ObserverUpdate(IVistaObserveable *pObserveable,
									  int msg, int ticket)
{
    switch(msg)
    {
        case IVistaObserveable::MSG_ATTACH:
        case IVistaObserveable::MSG_DETACH:
        {
            IVflVisObject::ObserverUpdate(pObserveable, msg, ticket);
            break;
        }
		case IVveDataItem::MSG_DATACHANGE:
		{
			if( ticket == E_PARTICLEDATA_CHANGE && GetRenderNode())
			{
				m_dDataTimeStamp      = GetRenderNode()->GetVisTiming()->GetLifeTime();
				m_bBoundsDirty        = true;
			}
			break;
		}
        default:
        {
            switch (ticket)
            {
            case E_VISPARAMETERS_CHANGE:
            case E_LOOKUPTABLE_CHANGE:
				if(GetRenderNode())
				{
					m_dVisParamsTimeStamp = GetRenderNode()->GetVisTiming()->GetLifeTime();
				}
                break;
            default:
                if(GetRenderNode() &&
                    (ticket == IVflRenderable::E_PROP_TICKET)
                    && (msg >= VflVisParticlesProperties::MSG_MAXCOUNT_CHANGE
                    && msg < VflVisParticlesProperties::MSG_LAST))
                {
                    // invalidate time stamp!
                    // @todo: clean this up
                    m_dVisParamsTimeStamp = GetRenderNode()->GetVisTiming()->GetLifeTime();
                }
                IVflVisObject::ObserverUpdate(pObserveable, msg, ticket);
                break;
            }
            break;
        }
    }
}


bool VflVisParticles::AddRenderer(VflParticleRenderer *pRenderer)
{
	if (!pRenderer)
		return false;

	m_vecRenderers.push_back(pRenderer);

	if (m_iCurrentRenderer<0)
		m_iCurrentRenderer = static_cast<int>(GetNumberOfRenderers()) - 1; // switch to current

	return true;
}


bool VflVisParticles::RemoveRenderer(VflParticleRenderer *pRenderer)
{
	std::vector<VflParticleRenderer*>::iterator it = std::find(m_vecRenderers.begin(),
		m_vecRenderers.end(), pRenderer);
	if(it != m_vecRenderers.end())
	{
		// remove from vector
		m_vecRenderers.erase(it);

		m_iCurrentRenderer = m_vecRenderers.empty() ? -1 : 0;
		return true;
	}
	return false;
}


VflParticleRenderer *VflVisParticles::GetRenderer(unsigned int iIndex) const
{
	if (iIndex<0 || iIndex>=GetNumberOfRenderers())
		return NULL;

	return m_vecRenderers[iIndex];
}


int VflVisParticles::GetCurrentRenderer() const
{
	return m_iCurrentRenderer;
}


bool VflVisParticles::SetCurrentRenderer(unsigned int iIndex)
{
	if (iIndex<0 || iIndex>=GetNumberOfRenderers())
		return false;

	m_iCurrentRenderer = iIndex;

	return true;
}


size_t VflVisParticles::GetNumberOfRenderers() const
{
	return m_vecRenderers.size();
}


const VistaVector3D VflVisParticles::GetTransformedLocalViewPosition() const
{
	assert( GetRenderNode() && m_pData && m_pData->GetTimeMapper() );

	IVflTransformer* pTrans = GetTransformer();

	VistaVector3D v3ViewPos = GetRenderNode()->GetLocalViewPosition();
	if( pTrans )
	{
		const double dVisTime =
			GetRenderNode()->GetVisTiming()->GetVisualizationTime();
		const double dSimTime =
			m_pData->GetTimeMapper()->GetSimulationTime(dVisTime);

		VistaTransformMatrix m;
		pTrans->GetUnsteadyTransform( dSimTime, m );
		v3ViewPos = m.GetInverted().TransformPoint( v3ViewPos );
	}

	return v3ViewPos;
}

const VistaQuaternion VflVisParticles::GetTransformedLocalViewOrientation() const
{
	assert( GetRenderNode() && m_pData && m_pData->GetTimeMapper() );

	IVflTransformer* pTrans = GetTransformer();

	VistaQuaternion qViewOri = GetRenderNode()->GetLocalViewOrientation();
	if( pTrans )
	{
		const double dVisTime =
			GetRenderNode()->GetVisTiming()->GetVisualizationTime();
		const double dSimTime =
			m_pData->GetTimeMapper()->GetSimulationTime(dVisTime);

		VistaTransformMatrix m;
		pTrans->GetUnsteadyTransform( dSimTime, m );
		qViewOri = m.GetInverted().Transform( qViewOri );
	}

	return qViewOri;
}

const VistaVector3D VflVisParticles::GetTransformedLocalLightDirection() const
{
	assert( GetRenderNode() && m_pData && m_pData->GetTimeMapper() );

	IVflTransformer* pTrans = GetTransformer();

	VistaVector3D v3LightDir = GetRenderNode()->GetLocalLightDirection();
	if( pTrans )
	{
		const double dVisTime =
			GetRenderNode()->GetVisTiming()->GetVisualizationTime();
		const double dSimTime =
			m_pData->GetTimeMapper()->GetSimulationTime(dVisTime);

		VistaTransformMatrix m;
		pTrans->GetUnsteadyTransform( dSimTime, m );
		v3LightDir = m.GetInverted().TransformVector( v3LightDir );
	}

	return v3LightDir;
}


ostream & operator<< ( ostream & out, const VflVisParticles & object )
{
    object.Debug ( out );
    return out;
}


VflVisParticles::VflVisParticlesProperties *VflVisParticles::GetProperties() const
{
	return static_cast<VflVisParticlesProperties*>(
		IVflRenderable::GetProperties());
}

std::string VflVisParticles::GetFactoryType()
{
	return "VIS_PARTICLES";
}


IVflVisObject::VflVisObjProperties    *VflVisParticles::CreateProperties() const
{
	return new VflVisParticlesProperties;
}

VveTimeMapper *VflVisParticles::GetTimeMapper() const
{
	if(GetParticleData())
		return GetParticleData()->GetTimeMapper();

	return NULL;
}

bool VflVisParticles::GetBounds(VistaVector3D &minBounds,
	                             VistaVector3D &maxBounds)
{
	VistaVector3D bbMax = GetProperties()->GetBoundingBoxMax();
	VistaVector3D bbMin = GetProperties()->GetBoundingBoxMin();

	if((bbMax == VistaVector3D(0.0f, 0.0f, 0.0f)) && (bbMax == VistaVector3D(0.0f, 0.0f, 0.0f)) )
	{

		if(m_bBoundsDirty)
		{
			// recompute bounds

			// do we have data?
			if(!GetParticleData())
			{
				// no, empty bounds
				m_afMinB[0] = m_afMinB[1] = m_afMinB[2] = 0.0f;
				m_afMaxB[0] = m_afMaxB[1] = m_afMaxB[2] = 0.0f;
			}
			else
			{
				// yes
				VistaBoundingBox b;

				// iterate over each trajectory in the population
				VveParticlePopulation *pPop = GetParticleData()->GetData()->GetData();

				for (int iTrajIdx = 0; iTrajIdx < pPop->GetNumTrajectories(); ++iTrajIdx)
				{
					VveParticleTrajectory* pTraj = pPop->GetTrajectory(iTrajIdx);

					VveParticleDataArrayBase* pPositionsArray = NULL;
                    
					// Check if the array with the given name exists
					if (pTraj && 
						pTraj->GetArrayByName(GetProperties()->GetPositionArray(), pPositionsArray) &&
						pPositionsArray)
					{
						// go on with the data array
						VistaBoundingBox oTrajBox;
						float fMin[3], fMax[3];
						pPositionsArray->GetBounds(fMin, fMax );
						b.m_v3Min[0] = fMin[0];
						b.m_v3Min[1] = fMin[1];
						b.m_v3Min[2] = fMin[2];

						b.m_v3Max[0] = fMax[0];
						b.m_v3Max[1] = fMax[1];
						b.m_v3Max[2] = fMax[2];
						b.Include(oTrajBox);
						}
					}

				// ok, finished
				// set bounds to target array
				m_afMinB[0] = b.m_v3Min[0];
				m_afMinB[1] = b.m_v3Min[1];
				m_afMinB[2] = b.m_v3Min[2];

				m_afMaxB[0] = b.m_v3Max[0];
				m_afMaxB[1] = b.m_v3Max[1];
				m_afMaxB[2] = b.m_v3Max[2];
			}

			// toggle flag to false
			// in order to avoid query on every GetBounds() call
			// when nothing has changed.
			m_bBoundsDirty = false;

		}


		// copy to target vectors
		minBounds[0] = m_afMinB[0];
		minBounds[1] = m_afMinB[1];
		minBounds[2] = m_afMinB[2];

		maxBounds[0] = m_afMaxB[0];
		maxBounds[1] = m_afMaxB[1];
		maxBounds[2] = m_afMaxB[2];
	} // If bbMin == (0,0,0) and bbMax == (0,0,0)
	else
	{
		minBounds[0] = bbMin[0];
		minBounds[1] = bbMin[1];
		minBounds[2] = bbMin[2];

		maxBounds[0] = bbMax[0];
		maxBounds[1] = bbMax[1];
		maxBounds[2] = bbMax[2];
	}


	return true;
}

// #############################################################################
// #############################################################################
// #############################################################################

static const string SsReflectionType("VflVisParticles");


int VflVisParticles::VflVisParticlesProperties::AddToBaseTypeList(
	                                       std::list<std::string> &rBtList) const
{
	int nSize = IVflVisObject::VflVisObjProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize;
}

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<int,
	          VflVisParticles::VflVisParticlesProperties,
			  VistaProperty::PROPT_DOUBLE>
	      ("MAX_COUNT", SsReflectionType,
		  &VflVisParticles::VflVisParticlesProperties::GetMaxCount),
	new TVistaPropertyGet<float,
	          VflVisParticles::VflVisParticlesProperties,
			  VistaProperty::PROPT_DOUBLE>
	      ("TIME_WINDOW", SsReflectionType,
		  &VflVisParticles::VflVisParticlesProperties::GetTimeWindow),
	new TVistaPropertyGet<float,
	          VflVisParticles::VflVisParticlesProperties,
			  VistaProperty::PROPT_DOUBLE>
	      ("RADIUS_SCALE", SsReflectionType,
		  &VflVisParticles::VflVisParticlesProperties::GetRadiusScale),
	new TVistaPropertyGet<float,
	          VflVisParticles::VflVisParticlesProperties,
			  VistaProperty::PROPT_DOUBLE>
	      ("RADIUS_ABSOLUTE", SsReflectionType,
		  &VflVisParticles::VflVisParticlesProperties::GetRadiusAbsolute),
	new TVistaPropertyGet<bool,
	          VflVisParticles::VflVisParticlesProperties,
			  VistaProperty::PROPT_BOOL>
	      ("USE_PARTICLE_RADIUS", SsReflectionType,
		  &VflVisParticles::VflVisParticlesProperties::GetUseParticleRadius),
	new TVistaPropertyGet<bool,
	          VflVisParticles::VflVisParticlesProperties,
			  VistaProperty::PROPT_BOOL>
	      ("DECREASE_LUMINANCE", SsReflectionType,
		  &VflVisParticles::VflVisParticlesProperties::GetDecreaseLuminanceWithTime),
    new TVistaPropertyGet<std::string,
	          VflVisParticles::VflVisParticlesProperties,
              VistaProperty::PROPT_STRING>
	      ("DRAWMODE_NAME", SsReflectionType,
          &VflVisParticles::VflVisParticlesProperties::GetDrawModeString),
    new TVistaPropertyGet<int,
	          VflVisParticles::VflVisParticlesProperties,
              VistaProperty::PROPT_INT>
	      ("DRAWMODE", SsReflectionType,
          &VflVisParticles::VflVisParticlesProperties::GetDrawMode),
    new TVistaPropertyGet<int,
	          VflVisParticles::VflVisParticlesProperties,
              VistaProperty::PROPT_INT>
	      ("RENDERER", SsReflectionType,
          &VflVisParticles::VflVisParticlesProperties::GetRendererIndex),
	new TVistaPropertyGet<VistaVector3D,
			VflVisParticles::VflVisParticlesProperties,
			VistaProperty::PROPT_LIST>
			("BOUNDINGBOX_MAX", SsReflectionType,
			&VflVisParticles::VflVisParticlesProperties::GetBoundingBoxMax),
	new TVistaPropertyGet<VistaVector3D,
			VflVisParticles::VflVisParticlesProperties,
			VistaProperty::PROPT_LIST>
			("BOUNDINGBOX_MIN", SsReflectionType,
			&VflVisParticles::VflVisParticlesProperties::GetBoundingBoxMin),

	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<int, int,
	                    VflVisParticles::VflVisParticlesProperties>
		("MAX_COUNT", SsReflectionType,
		&VflVisParticles::VflVisParticlesProperties::SetMaxCount),
	new TVistaPropertySet<float, float,
	                    VflVisParticles::VflVisParticlesProperties>
		("TIME_WINDOW", SsReflectionType,
		&VflVisParticles::VflVisParticlesProperties::SetTimeWindow),
	new TVistaPropertySet<float, float,
	                    VflVisParticles::VflVisParticlesProperties>
		("RADIUS_SCALE", SsReflectionType,
		&VflVisParticles::VflVisParticlesProperties::SetRadiusScale),
	new TVistaPropertySet<float, float,
	                    VflVisParticles::VflVisParticlesProperties>
		("RADIUS_ABSOLUTE", SsReflectionType,
		&VflVisParticles::VflVisParticlesProperties::SetRadiusAbsolute),
	new TVistaPropertySet<bool, bool,
	                    VflVisParticles::VflVisParticlesProperties>
		("USE_PARTICLE_RADIUS", SsReflectionType,
		&VflVisParticles::VflVisParticlesProperties::SetUseParticleRadius),
	new TVistaPropertySet<bool, bool,
	                    VflVisParticles::VflVisParticlesProperties>
		("DECREASE_LUMINANCE", SsReflectionType,
		&VflVisParticles::VflVisParticlesProperties::SetDecreaseLuminanceWithTime),
    new TVistaPropertySet<const std::string&, std::string,
	                    VflVisParticles::VflVisParticlesProperties>
		("DRAWMODE_NAME", SsReflectionType,
        &VflVisParticles::VflVisParticlesProperties::SetDrawMode),
    new TVistaPropertySet<int, int,
	                    VflVisParticles::VflVisParticlesProperties>
		("DRAWMODE", SsReflectionType,
        &VflVisParticles::VflVisParticlesProperties::SetDrawMode),
    new TVistaPropertySet<int, int,
	                    VflVisParticles::VflVisParticlesProperties>
		("RENDERER", SsReflectionType,
        &VflVisParticles::VflVisParticlesProperties::SetRendererIndex),
	new TVistaPropertySet<VistaVector3D, VistaVector3D,
						VflVisParticles::VflVisParticlesProperties>
		("BOUNDINGBOX_MAX", SsReflectionType,
		&VflVisParticles::VflVisParticlesProperties::SetBoundingBoxMax),
	new TVistaPropertySet<VistaVector3D, VistaVector3D,
						VflVisParticles::VflVisParticlesProperties>
		("BOUNDINGBOX_MIN", SsReflectionType,
		&VflVisParticles::VflVisParticlesProperties::SetBoundingBoxMin),
	NULL
};

VflVisParticles::VflVisParticlesProperties::VflVisParticlesProperties()
: IVflVisObject::VflVisObjProperties(),
  m_bDecreaseLuminanceWithTime(true),
  m_bUseParticleRadius(false),
  m_fRadiusAbsolute(1.0f),
  m_fRadiusScale(1.0f),
  m_fTimeWindow(0),
  m_iMaxCount(-1),
  m_sPositionArray(VveParticlePopulation::sPositionArrayDefault),
  m_sVelocityArray(VveParticlePopulation::sVelocityArrayDefault),
  m_sScalarArray(VveParticlePopulation::sScalarArrayDefault),
  m_sRadiusArray(VveParticlePopulation::sRadiusArrayDefault),
  m_sTimeArray(VveParticlePopulation::sTimeArrayDefault),
  m_sEigenValueArray(VveParticlePopulation::sEigenValueArrayDefault),
  m_sEigenVectorArray(VveParticlePopulation::sEigenVectorArrayDefault)
{
}

VflVisParticles::VflVisParticlesProperties::~VflVisParticlesProperties()
{
}

string VflVisParticles::VflVisParticlesProperties::GetReflectionableType() const
{
	return SsReflectionType;
}


int  VflVisParticles::VflVisParticlesProperties::GetMaxCount() const
{
	return m_iMaxCount;
}

bool VflVisParticles::VflVisParticlesProperties::SetMaxCount(int iCount)
{
	if(compAndAssignFunc<int>(iCount, m_iMaxCount))
	{
		Notify(MSG_MAXCOUNT_CHANGE);
		return true;
	}
	return false;
}

float VflVisParticles::VflVisParticlesProperties::GetTimeWindow() const
{
	return m_fTimeWindow;

}

bool  VflVisParticles::VflVisParticlesProperties::SetTimeWindow(float fTimeWindow)
{
	if(compAndAssignFunc<float>(fTimeWindow, m_fTimeWindow))
	{
		Notify(MSG_TIMEWINDOW_CHANGE);
		return true;
	}
	return false;

}


float VflVisParticles::VflVisParticlesProperties::GetRadiusScale() const
{
	return m_fRadiusScale;
}

bool  VflVisParticles::VflVisParticlesProperties::SetRadiusScale(float fRadiusScale)
{
	if(compAndAssignFunc<float>(fRadiusScale, m_fRadiusScale))
	{
		Notify(MSG_RADIUS_SCALE_CHANGE);
		return true;
	}
	return false;

}


float VflVisParticles::VflVisParticlesProperties::GetRadiusAbsolute() const
{
	return m_fRadiusAbsolute;

}

bool  VflVisParticles::VflVisParticlesProperties::SetRadiusAbsolute(float fRadiusAbsolute)
{
	if(compAndAssignFunc<float>(fRadiusAbsolute, m_fRadiusAbsolute))
	{
		Notify(MSG_RADIUS_ABSOLUTE_CHANGE);
		return true;
	}
	return false;

}


bool VflVisParticles::VflVisParticlesProperties::GetUseParticleRadius() const
{
	return m_bUseParticleRadius;

}

bool VflVisParticles::VflVisParticlesProperties::SetUseParticleRadius(bool bUseParticleRadius)
{
	if(compAndAssignFunc<bool>(bUseParticleRadius, m_bUseParticleRadius))
	{
		Notify(MSG_USE_PARTICLE_RADIUS_CHANGE);
		return true;
	}
	return false;

}


bool VflVisParticles::VflVisParticlesProperties::GetDecreaseLuminanceWithTime() const
{
	return m_bDecreaseLuminanceWithTime;

}

bool VflVisParticles::VflVisParticlesProperties::SetDecreaseLuminanceWithTime(bool bDecrease)
{
	if(compAndAssignFunc<bool>(bDecrease, m_bDecreaseLuminanceWithTime))
	{
		Notify(MSG_DECREASE_LUMINANCE_CHANGE);
		return true;
	}
	return false;

}

VistaVector3D VflVisParticles::VflVisParticlesProperties::GetBoundingBoxMax() const
{
	return m_v3BBMax;
}

bool VflVisParticles::VflVisParticlesProperties::SetBoundingBoxMax(VistaVector3D v3BBMax)
{
	m_v3BBMax = v3BBMax;
	Notify(MSG_BOUNDINGBOX_CHANGE);
	return true;
}

VistaVector3D VflVisParticles::VflVisParticlesProperties::GetBoundingBoxMin() const
{
	return m_v3BBMin;
}

bool VflVisParticles::VflVisParticlesProperties::SetBoundingBoxMin(VistaVector3D v3BBMin)
{
	m_v3BBMin = v3BBMin;
	Notify(MSG_BOUNDINGBOX_CHANGE);
	return true;
}

std::string VflVisParticles::VflVisParticlesProperties::GetDrawModeString() const
{
    VflVisParticles *pParent = dynamic_cast<VflVisParticles*>(GetParentRenderable());

#if defined(DEBUG)
    if(pParent == NULL)
        return "";
#endif

    VflParticleRenderer *pRend = pParent->GetRenderer( pParent->GetCurrentRenderer() );
    if(pRend == NULL)
        return "";

    return pRend->GetDrawModeAsString();
}

int VflVisParticles::VflVisParticlesProperties::GetDrawMode() const
{
    VflVisParticles *pParent = dynamic_cast<VflVisParticles*>(GetParentRenderable());

#if defined(DEBUG)
    if(pParent == NULL)
        return -1;
#endif

    VflParticleRenderer *pRend = pParent->GetRenderer( pParent->GetCurrentRenderer() );
    if(pRend == NULL)
        return -1;

    return pRend->GetDrawMode();
}

bool VflVisParticles::VflVisParticlesProperties::SetDrawMode( const std::string &sName )
{
    VflVisParticles *pParent = dynamic_cast<VflVisParticles*>(GetParentRenderable());

#if defined(DEBUG)
    if(pParent == NULL)
        return false;
#endif

    VflParticleRenderer *pRend = pParent->GetRenderer( pParent->GetCurrentRenderer() );
    if(pRend == NULL)
        return false;

    std::string strCurrentMode = pRend->GetDrawModeAsString();
    pRend->SetDrawMode( sName );
    std::string strNewMode = pRend->GetDrawModeAsString();

    if( strCurrentMode == strNewMode )
    {
        Notify( MSG_DRAWMODE_CHANGE );
        return true;
    }

    return false;
}

bool VflVisParticles::VflVisParticlesProperties::SetDrawMode( int nMode )
{
    VflVisParticles *pParent = dynamic_cast<VflVisParticles*>(GetParentRenderable());

#if defined(DEBUG)
    if(pParent == NULL)
        return false;
#endif

    VflParticleRenderer *pRend = pParent->GetRenderer( pParent->GetCurrentRenderer() );
    if(pRend == NULL)
        return false;

    int nOldMode = pRend->GetDrawMode();
    if(nOldMode == nMode)
        return false; // no need for a change

    pRend->SetDrawMode( nMode );

    if( pRend->GetDrawMode() != nOldMode )
    {
        Notify( MSG_DRAWMODE_CHANGE );
        return true;
    }

    return false;
}

int VflVisParticles::VflVisParticlesProperties::GetRendererIndex() const
{
    VflVisParticles *pParent = dynamic_cast<VflVisParticles*>(GetParentRenderable());

#if defined(DEBUG)
    if(pParent == NULL)
        return -1;
#endif
    return pParent->GetCurrentRenderer();
}

bool VflVisParticles::VflVisParticlesProperties::SetRendererIndex( int nIndex )
{
    VflVisParticles *pParent = dynamic_cast<VflVisParticles*>(GetParentRenderable());

#if defined(DEBUG)
    if(pParent == NULL)
        return false;
#endif
    if( pParent->SetCurrentRenderer( nIndex ) )
    {
        Notify(MSG_RENDERER_CHANGE);
        return true;
    }
    return false;
}

std::string VflVisParticles::VflVisParticlesProperties::GetPositionArray() const
{
    return m_sPositionArray;
}

std::string VflVisParticles::VflVisParticlesProperties::GetVelocityArray() const
{
    return m_sVelocityArray;
}

std::string VflVisParticles::VflVisParticlesProperties::GetRadiusArray() const
{
    return m_sRadiusArray;
}

std::string VflVisParticles::VflVisParticlesProperties::GetScalarArray() const
{
    return m_sScalarArray;
}

std::string VflVisParticles::VflVisParticlesProperties::GetTimeArray() const
{
    return m_sTimeArray;
}
std::string VflVisParticles::VflVisParticlesProperties::GetEigenValueArray() const
{
    return m_sEigenValueArray;
}
std::string VflVisParticles::VflVisParticlesProperties::GetEigenVectorArray() const
{
    return m_sEigenVectorArray;
}

bool VflVisParticles::VflVisParticlesProperties::SetPositionArray( std::string sPositionArray )
{
    if(compAndAssignFunc<std::string>(sPositionArray, m_sPositionArray))
    {
        Notify(MSG_POSITION_ARRAY_CHANGE);
        return true;
    }
    return false;
}

bool VflVisParticles::VflVisParticlesProperties::SetVelocityArray( std::string sVelocityArray )
{
    if(compAndAssignFunc<std::string>(sVelocityArray, m_sVelocityArray ))
    {
        Notify(MSG_VELOCITY_ARRAY_CHANGE);
        return true;
    }
    return false;
}

bool VflVisParticles::VflVisParticlesProperties::SetRadiusArray( std::string sRadiusArray )
{
    if(compAndAssignFunc<std::string>(sRadiusArray, m_sRadiusArray))
    {
        Notify(MSG_RADIUS_ARRAY_CHANGE);
        return true;
    }
    return false;
}

bool VflVisParticles::VflVisParticlesProperties::SetScalarArray( std::string sScalarArray )
{
    if(compAndAssignFunc<std::string>(sScalarArray, m_sScalarArray))
    {
        Notify(MSG_SCALAR_ARRAY_CHANGE);
        return true;
    }
    return false;
}

bool VflVisParticles::VflVisParticlesProperties::SetTimeArray( std::string sTimeArray )
{
    if(compAndAssignFunc<std::string>(sTimeArray, m_sTimeArray))
    {
        Notify(MSG_TIME_ARRAY_CHANGE);
        return true;
    }
    return false;
}

bool VflVisParticles::VflVisParticlesProperties::SetEigenValueArray( std::string sEigenValueArray )
{
    if(compAndAssignFunc<std::string>(sEigenValueArray, m_sEigenValueArray))
    {
        Notify(MSG_EIGENVALUE_ARRAY_CHANGE);
        return true;
    }
    return false;
}

bool VflVisParticles::VflVisParticlesProperties::SetEigenVectorArray( std::string sEigenVectorArray )
{
    if(compAndAssignFunc<std::string>(sEigenVectorArray, m_sEigenVectorArray))
    {
        Notify(MSG_EIGENVECTOR_ARRAY_CHANGE);
        return true;
    }
    return false;
}



// #############################################################################
// #############################################################################
// #############################################################################


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VisObject.cpp"                                               */
/*============================================================================*/
