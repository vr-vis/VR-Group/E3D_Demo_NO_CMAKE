/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflParticleColorData.h"

#include <assert.h>
#include <VistaVisExt/Data/VveParticleDataArray.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflParticleColorData::VflParticleColorData( VveParticlePopulationItem *pParticleData,
                                           VflVtkLookupTable *pLut,
                                           VflVisParticles::VflVisParticlesProperties* pProperties)
: m_bResizeNecessary(true), m_bColorUpdateNecessary(true)
{
	assert(pLut);
	assert(pParticleData);
    assert(pProperties);
	m_pParticleData = pParticleData;
	m_pLUT = pLut;
    m_pParams = pProperties;

	// register self as observer of particle data and lookup table
	Observe(m_pParticleData, NI_PARTICLEDATA_CHANGE);
	Observe(m_pLUT, NI_LUT_CHANGE);

    // TODO: Observe scalar array change
    Observe(pProperties, NI_SCALAR_ARRAY_CHANGE);

}

VflParticleColorData::~VflParticleColorData()
{
	ReleaseObserveable(m_pParticleData, IVistaObserveable::TICKET_NONE);
	ReleaseObserveable(m_pLUT, IVistaObserveable::TICKET_NONE);
    ReleaseObserveable(m_pParams, IVistaObserveable::TICKET_NONE);
	m_pLUT = NULL;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Notify                                                      */
/*                                                                            */
/*============================================================================*/
void VflParticleColorData::ObserverUpdate(IVistaObserveable *pObserveable,
										   int msg, 
										   int ticket)
{
	switch (ticket)
	{
    case NI_SCALAR_ARRAY_CHANGE:
        if(msg != VflVisParticles::VflVisParticlesProperties::MSG_SCALAR_ARRAY_CHANGE)
            break;
	case NI_PARTICLEDATA_CHANGE:
		m_bResizeNecessary = true;
	case NI_LUT_CHANGE:
		m_bColorUpdateNecessary = true;
		break;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflParticleColorData::Update()
{
	if (!m_bResizeNecessary && !m_bColorUpdateNecessary)
		return;

	VveParticlePopulation *pPop = m_pParticleData->GetData();

	if (!pPop)
		return;

    m_pParticleData->LockData();

	int iPopulationSize = pPop->GetNumTrajectories();

	if (m_bResizeNecessary)
	{
		m_vecColors.resize(iPopulationSize);

		for (int i=0; i<iPopulationSize; ++i)
		{
            VveParticleTrajectory* pTraj = pPop->GetTrajectory(i);
			VveParticleDataArrayBase* pScalarArray = NULL;

			assert( pTraj );
			pTraj->GetArrayByName(m_pParams->GetScalarArray(), pScalarArray);
			assert( pScalarArray && 
					"Make sure to set the right scalar array name by VflVisParticlesProperties::SetScalarArray()");

			m_vecColors[i].resize( pScalarArray->GetSize());
		}
		m_bResizeNecessary = false;
	}

	if (m_bColorUpdateNecessary)
	{
		for (int i=0; i<iPopulationSize; ++i)
		{
			TRAJECTORY_COLORS & refTrajectoryColors = m_vecColors[i];

            // Get the current particle trajectory
            VveParticleTrajectory* pTraj = pPop->GetTrajectory(i);
            VveParticleDataArrayBase* pScalarArray = NULL;

			assert( pTraj );
			pTraj->GetArrayByName(m_pParams->GetScalarArray(), pScalarArray);
			assert( pScalarArray && 
					"Make sure to set the right scalar array name by VflVisParticlesProperties::SetScalarArray()");

            for (size_t j=0; j<pScalarArray->GetSize(); ++j)
            {
                float fScalar = pScalarArray->GetElementValueAsFloat(j);
                float* aDestColor = refTrajectoryColors[j].aColor;

				VistaColor oColor = m_pLUT->GetColor(fScalar);
                aDestColor[0] = oColor[0];
                aDestColor[1] = oColor[1];
                aDestColor[2] = oColor[2];
                aDestColor[3] = oColor[3];
			}
		}
		m_bColorUpdateNecessary = false;
	}

    m_pParticleData->UnlockData();
}



/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflParticleColorData.cpp"                                    */
/*============================================================================*/
