/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <VistaBase/VistaVector3D.h>
#include <VistaBase/VistaStreamUtils.h>

#include "VflVirtualParticles.h"
#include "VflVisParticles.h"
#include "../VflVisTiming.h"
#include "../VflRenderNode.h"
#include "../VflLookupTexture.h"
#include "../VflVtkLookupTable.h"

#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>

#include <VistaOGLExt/VistaTexture.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaBufferObject.h>
#include <VistaOGLExt/VistaShaderRegistry.h>
#include <VistaOGLExt/VistaVertexArrayObject.h>
#include <VistaOGLExt/Rendering/VistaParticleRenderingProperties.h>
#include <VistaOGLExt/Rendering/VistaParticleRenderingCore.h>

using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflVirtualParticles::VflVirtualParticles(
	VflVisParticles *pOwner, int iTextureSize, bool bUseMipMaps )
	:	VflParticleRenderer( pOwner )
	,	m_pCore( new VistaParticleRenderingCore )
	,	m_pParticles( NULL )
	,	m_pPopulation( NULL )
	,	m_pLUT( NULL )
	,	m_iCurrentParticleCount( 0 )
	,	m_iCurrentTimeIndex( -1 )
	,	m_dCurrentSimTime( 0.0 )
	,	m_pParticleData( NULL )
	,	m_pParticleRadii( NULL )
	,	m_pParticleDataTexture( NULL )
	,	m_pParticleRadiiTexture( NULL )
	,	m_bInitialized( false )
	,	m_bInterpolat( false )
{
	VistaParticleRenderingProperties* pProps = m_pCore->GetProperties();
	pProps->SetTextureResolution( iTextureSize );
	pProps->SetGenerateMipmaps( bUseMipMaps );

	// write all particles from population into selected set
	int iNumTrajectories = 
		m_pOwner->GetParticleData()->GetData()->GetData()->GetNumTrajectories();

    for( int i=0; i < iNumTrajectories; ++i )
		m_vecSelectedTrajectories.push_back(i);
}

VflVirtualParticles::~VflVirtualParticles()
{
	this->Destroy();

	delete m_pCore;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
#pragma region public intervace

VistaParticleRenderingProperties* 
	VflVirtualParticles::GetParticleRenderingProperties()
{
	return m_pCore->GetProperties();
}
bool VflVirtualParticles::Init()
{
	if( m_bInitialized ) 
		return false;

	// find out whether the required openGL version is supported
	if( !GLEW_VERSION_3_3 )
	{
		vstr::errp() << " [VflVirtualParticles] GLEW_VERSION_3_3 not supported..." << endl;
		return false;
	}

	m_pPopulation = new VveParticlePopulation;
	m_pParticles  = new VveParticlePopulationItem;
	m_pParticles->SetData(m_pPopulation);

	if( !CreateParticleDataTexture() )
		return false;

	// create lookup texture
	m_pLUT = new VflLookupTexture( m_pOwner->GetLookupTable() );

	// init rendering core
	float fMin, fMax;
	m_pOwner->GetLookupTable()->GetTableRange( fMin, fMax );
	m_pCore->SetLookupRange( fMin, fMax );
	m_pCore->SetLookupTexture( m_pLUT->GetLookupTexture() );
	if( !m_pCore->Init() )
		return false;

	m_bInitialized = true;

	return true;
}

void VflVirtualParticles::Destroy()
{
	delete m_pParticles;
	m_pParticles = NULL;

	delete m_pPopulation;
	m_pPopulation = NULL;

	delete[] m_pParticleData;
	m_pParticleData = NULL;

	m_pCore->SetLookupTexture( NULL );

	delete m_pLUT;
	m_pLUT = NULL;

	delete[] m_pParticleData;
	m_pParticleData = NULL;

	delete[] m_pParticleRadii;
	m_pParticleRadii = NULL;

	delete m_pParticleDataTexture;
	m_pParticleDataTexture = NULL;

	delete m_pParticleRadiiTexture;
	m_pParticleRadiiTexture = NULL;

	m_bInitialized = false;
}

void VflVirtualParticles::Update()
{
	if( !m_pOwner->GetVisible() )
		return;

	m_pCore->SetViewerPosition( m_pOwner->GetTransformedLocalViewPosition() );
	m_pCore->SetLightDirection(-m_pOwner->GetRenderNode()->GetLocalLightDirection() );

	bool bUpdateParticleDataTexture = false;

	// If we want to render smoke using alpha bending, we have to sort the particles.
	// Therefore the particle data texture must be updated.
	if( VistaParticleRenderingProperties::DM_SMOKE ==
		m_pCore->GetProperties()->GetDrawMode() &&
		VistaParticleRenderingProperties::ALPHA_BLENDING == 
		m_pCore->GetProperties()->GetBlendingMode()  )
	{
		bUpdateParticleDataTexture = true;
	}
 
	if( m_dDataTimeStamp < m_pOwner->GetDataChangeTimeStamp() )
	{
		UpdateParticleData();
		bUpdateParticleDataTexture = true;
	}	

	if( m_dVisParamsTimeStamp < m_pOwner->GetVisParamsChangeTimeStamp() )
	{
		UpdateVisParams();
		bUpdateParticleDataTexture = true;
	}

	VveTimeMapper* pTimeMapper = m_pOwner->GetTimeMapper();
	double dVisualizationTime = 
		m_pOwner->GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	// determine time level to be rendered
	int    iLastIndex   = m_iCurrentTimeIndex;
	double dLastSimTime = m_dCurrentSimTime;
	m_iCurrentTimeIndex = pTimeMapper->GetTimeIndex( dVisualizationTime );
	m_dCurrentSimTime   = pTimeMapper->GetSimulationTime( dVisualizationTime );

	if( m_iCurrentTimeIndex < 0 )
		return;

	if( m_iCurrentTimeIndex != iLastIndex )
		bUpdateParticleDataTexture = true;

	if( m_bInterpolat && m_dCurrentSimTime != dLastSimTime )
		bUpdateParticleDataTexture = true;

	if( bUpdateParticleDataTexture )
		UpdateParticleDataTexture();
}

void VflVirtualParticles::DrawOpaque()
{
	if( m_iCurrentTimeIndex < 0 )
		return;

	m_pCore->Draw();
}

std::string VflVirtualParticles::GetType() const
{
	return VflParticleRenderer::GetType() + string("::VflVirtualParticles");
}

void VflVirtualParticles::SetDrawMode( int iMode )
{
	m_pCore->GetProperties()->SetDrawMode( iMode );
}
void VflVirtualParticles::SetDrawMode( const std::string &strMode )
{
	if( strMode == "DM_SIMPLE" )
		m_pCore->GetProperties()->SetDrawMode( 
			VistaParticleRenderingProperties::DM_SIMPLE );

	if( strMode == "DM_SMOKE" )
		m_pCore->GetProperties()->SetDrawMode( 
		VistaParticleRenderingProperties::DM_SMOKE );

	if( strMode == "DM_BILLBOARDS" )
		m_pCore->GetProperties()->SetDrawMode( 
			VistaParticleRenderingProperties::DM_BILLBOARDS );

	if( strMode == "DM_BUMPED_BILLBOARDS" )
		m_pCore->GetProperties()->SetDrawMode( 
			VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS );

	if( strMode == "DM_BUMPED_BILLBOARDS_DEPTH_REPLACE" )
		m_pCore->GetProperties()->SetDrawMode( 
			VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS_DEPTH_REPLACE );
}
int  VflVirtualParticles::GetDrawMode() const
{
	return m_pCore->GetProperties()->GetDrawMode();
}
std::string VflVirtualParticles::GetDrawModeAsString() const
{
	return GetDrawModeString( m_pCore->GetProperties()->GetDrawMode() );
}
std::string VflVirtualParticles::GetDrawModeString(int iMode) const
{
	switch (iMode)
	{
	case VistaParticleRenderingProperties::DM_SIMPLE:
		return "DM_SIMPLE";
	case VistaParticleRenderingProperties::DM_SMOKE:
		return "DM_SMOKE";
	case VistaParticleRenderingProperties::DM_BILLBOARDS:
		return "DM_BILLBOARDS";
	case VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS:
		return "DM_BUMPED_BILLBOARDS";
	case VistaParticleRenderingProperties::DM_BUMPED_BILLBOARDS_DEPTH_REPLACE:
		return "DM_BUMPED_BILLBOARDS_DEPTH_REPLACE";
	}
	return VflParticleRenderer::GetDrawModeString( iMode );
}

int VflVirtualParticles::GetDrawModeCount() const
{
	return VistaParticleRenderingProperties::DM_LAST;
}

void VflVirtualParticles::SetSelectedTrajectories(
	const std::vector<int> & vecSelectedTrajectories )
{
	size_t nOldSize = m_vecSelectedTrajectories.size();

	// overwrite set of selected trajectories
	m_vecSelectedTrajectories = vecSelectedTrajectories;

	if( m_vecSelectedTrajectories.size() != nOldSize )
	{
		if( m_pParticleData )
			delete[] m_pParticleData;

		m_pParticleData = new float[ 4*m_vecSelectedTrajectories.size() ];
	}

	// set back time stamp, so the next Update() call will call UpdateParticleData
	// and update visual information
	m_dDataTimeStamp = 0;
}

void VflVirtualParticles::SetInterpolate( bool bInterpolate )
{
	m_bInterpolat = bInterpolate;
}
bool VflVirtualParticles::GetInterpolate() const
{
	return m_bInterpolat;
}
#pragma endregion
/******************************************************************************/
#pragma region protected intervace

bool VflVirtualParticles::CreateParticleDataTexture()
{
	m_pParticleDataTexture = new VistaTexture( GL_TEXTURE_2D );
	m_pParticleDataTexture->Bind();
	m_pParticleDataTexture->SetMagFilter( GL_NEAREST );
	m_pParticleDataTexture->SetMinFilter( GL_NEAREST );
	m_pParticleDataTexture->Unbind();

	m_pCore->SetDataTexture( m_pParticleDataTexture );

	if( !m_pParticleData )
		m_pParticleData  = new float[ 4*m_vecSelectedTrajectories.size() ];

	m_pParticleRadiiTexture = new VistaTexture( GL_TEXTURE_2D );
	m_pParticleRadiiTexture->Bind();
	m_pParticleRadiiTexture->SetMagFilter( GL_NEAREST );
	m_pParticleRadiiTexture->SetMinFilter( GL_NEAREST );
	m_pParticleRadiiTexture->Unbind();

	if( !m_pParticleRadii )
		m_pParticleRadii = new float[ m_vecSelectedTrajectories.size() ];

	return true;
}

void VflVirtualParticles::UpdateParticleData()
{
	VveTimeMapper *pMapper = m_pOwner->GetTimeMapper();

	VflVisParticles::VflVisParticlesProperties* pProperties = 
		dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(
			m_pOwner->GetProperties() );

	VveParticlePopulationItem* pPopulationItem = m_pOwner->GetParticleData()->GetData();
	VveParticlePopulation*     pPopulation     = pPopulationItem->GetData();

	string strTimeArrayName = pProperties->GetTimeArray();

	pPopulationItem->LockData();
	
	// update local (reduced) particle data
	vstr::debugi() << "[VflVirtualParticles] - reducing trajectory information..." << endl;
	pPopulation->ReduceInformation( pMapper, strTimeArrayName, m_pPopulation );

	m_pParticles->Notify();

	// rebuild lookup data for particle population (per timelevel)
	vstr::debugi() << "[VflVirtualParticles] - rebuilding lookup data..." << endl;
	int iTimeIndexCount = pMapper->GetNumberOfTimeIndices();

	const size_t nPathlineCount = m_vecSelectedTrajectories.size();
	vector<int> vecParticleLevel( nPathlineCount, 0 );

	m_vecTimeLevels.resize( iTimeIndexCount );

	for( int i=0; i<iTimeIndexCount; ++i )
	{
		m_vecTimeLevels[i].clear();

		const float fSimTime = static_cast<float>( pMapper->GetSimulationTime(i) );
		unsigned int iCurrentIndex;

		for(size_t j=0; j<nPathlineCount; ++j)
		{
            // iCurrentIndex will initially be set to 0
			iCurrentIndex = vecParticleLevel[j];

            VveParticleTrajectory *pTraj = 
				m_pPopulation->GetTrajectory( m_vecSelectedTrajectories[j] );
            VveParticleDataArrayBase *pTimeArray = NULL;

            if( pTraj && 
				pTraj->GetArrayByName( strTimeArrayName, pTimeArray ) &&
                pTimeArray )
			{
				float fTime = pTimeArray->GetElementValueAsFloat( iCurrentIndex );

                while( iCurrentIndex < pTimeArray->GetSize() && fTime < fSimTime )
					fTime = pTimeArray->GetElementValueAsFloat( iCurrentIndex++ );

			    if( iCurrentIndex == pTimeArray->GetSize() )
					continue;

				/* @FIXME should this be set, if time cannot be verfified ?
				If so, then maybe put it into while loop ? Or check for currentindex out of bounds. */
				vecParticleLevel[j] = iCurrentIndex;

                if( fTime == fSimTime )
				{
					SParticleLookup oParticle;
					oParticle.iTrajectoryIndex = m_vecSelectedTrajectories[j];
					oParticle.iParticleIndex   = iCurrentIndex;

					m_vecTimeLevels[i].push_back(oParticle);
				}
			}
		}
	}

	m_dDataTimeStamp = m_pOwner->GetDataChangeTimeStamp();

	vstr::debugi() << " [VflVirtualParticles] - done..." << endl;

	pPopulationItem->UnlockData();
}

void VflVirtualParticles::UpdateVisParams()
{
	VflVisParticles::VflVisParticlesProperties* pProperties = 
		dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(m_pOwner->GetProperties());

	if( pProperties )
	{
		if( pProperties->GetUseParticleRadius() )
		{
			m_bUpdatePadticleRadii = true;
			m_pCore->SetParticleRadiusScale( pProperties->GetRadiusScale() );
			m_pCore->SetRadiiTexture( m_pParticleRadiiTexture );
		}
		else
		{
			m_bUpdatePadticleRadii = false;
			m_pCore->SetParticleRadiusScale( pProperties->GetRadiusAbsolute() );
			m_pCore->SetRadiiTexture( NULL );
		}

		m_dVisParamsTimeStamp = m_pOwner->GetVisParamsChangeTimeStamp();
	}
}

void VflVirtualParticles::UpdateParticleDataTexture()
{
	// Fist we need the position and scalar values of the particles 
	std::vector<Particle> vecParticles;

	if( m_bInterpolat )
		GetInterpolatedParticles( vecParticles );
	else
		GetParticles( vecParticles );

	if( VistaParticleRenderingProperties::DM_SMOKE == 
		m_pCore->GetProperties()->GetDrawMode() && 
		VistaParticleRenderingProperties::ALPHA_BLENDING ==
		m_pCore->GetProperties()->GetBlendingMode() )
	{
		SortParticles( vecParticles );
	}

	vector<Particle>::iterator itCur = vecParticles.begin();
	vector<Particle>::iterator itEnd = vecParticles.end();

	float *pParticle = m_pParticleData;
	float *pRadius   = m_pParticleRadii;

	for( ; itCur!=itEnd; ++itCur )
	{		
		pParticle[0] = itCur->aPos[0];
		pParticle[1] = itCur->aPos[1];
		pParticle[2] = itCur->aPos[2];
		pParticle[3] = itCur->fScalar;
		pParticle += 4;

		if( m_bUpdatePadticleRadii )
			*(pRadius++) = itCur->fRadius;
	}

	m_pParticleDataTexture->Bind();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F,
		m_iCurrentParticleCount, 1, 0, GL_RGBA, GL_FLOAT, m_pParticleData);
	m_pParticleDataTexture->Unbind();

	m_pCore->SetDataTextureSize( m_iCurrentParticleCount, 1 );
	m_pCore->SetParticleCount( m_iCurrentParticleCount );

	if( m_bUpdatePadticleRadii )
	{
		m_pParticleRadiiTexture->Bind();
		glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE,
			m_iCurrentParticleCount, 1, 0, GL_LUMINANCE, GL_FLOAT, m_pParticleRadii );
		m_pParticleRadiiTexture->Unbind();
	}

}

void VflVirtualParticles::SortParticles( std::vector<Particle>& vecParticles )
{
	VistaVector3D v3View = m_pOwner->GetTransformedLocalViewPosition();

	vector<Particle>::iterator itBegin = vecParticles.begin();
	vector<Particle>::iterator itEnd   = vecParticles.end();
	vector<Particle>::iterator itCur   = itBegin;

	for( ;itCur != itEnd; ++itCur)
	{
		itCur->fDist = (itCur->aPos[0] - v3View[0])*(itCur->aPos[0] - v3View[0])
			         + (itCur->aPos[1] - v3View[1])*(itCur->aPos[1] - v3View[1])
			         + (itCur->aPos[2] - v3View[2])*(itCur->aPos[2] - v3View[2]);
	}

	std::sort( itBegin, itEnd, CompareParticls );
}

bool VflVirtualParticles::CompareParticls( const Particle& p1, const Particle& p2)
{
	return p1.fDist > p2.fDist;
}

void VflVirtualParticles::GetParticles( vector<Particle>& vecParticles )
{
	STimeLevelLookup& refTimeLevelLookup = m_vecTimeLevels[m_iCurrentTimeIndex];
	m_iCurrentParticleCount = static_cast<int>( refTimeLevelLookup.size() );

	VflVisParticles::VflVisParticlesProperties *pProperties = 
		dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(
			m_pOwner->GetProperties() );

	const std::string strPositionArrayName = pProperties->GetPositionArray();
	const std::string strScalarArrayName   = pProperties->GetScalarArray();
	const std::string strRadiusArrayName   = pProperties->GetRadiusArray();

	vecParticles.reserve( m_iCurrentParticleCount );

	for( int i=0; i<m_iCurrentParticleCount; ++i )
	{
		// fill the vertex data right away...
		SParticleLookup &refLookup = refTimeLevelLookup[i];

		VveParticleDataArrayBase* pPositionArray = NULL;
		VveParticleDataArrayBase* pScalarArray   = NULL;
		VveParticleDataArrayBase* pRadiusArray   = NULL;
		VveParticleTrajectory* pTraj = 
			m_pPopulation->GetTrajectory( refLookup.iTrajectoryIndex );

		bool bInitArrays =  pTraj &&
			pTraj->GetArrayByName( strPositionArrayName, pPositionArray ) &&
			pTraj->GetArrayByName( strScalarArrayName,   pScalarArray   ) &&
			( !m_bUpdatePadticleRadii || 
			pTraj->GetArrayByName( strRadiusArrayName, pRadiusArray ) );

		if( !bInitArrays )
			continue;

		int iParticleIndex = refLookup.iParticleIndex;
		Particle oParticle;

		pPositionArray->GetElementCopy( iParticleIndex, oParticle.aPos );
		oParticle.fScalar = pScalarArray->GetElementValueAsFloat( iParticleIndex );
		if( m_bUpdatePadticleRadii )
			oParticle.fRadius = pRadiusArray->GetElementValueAsFloat( iParticleIndex );

		vecParticles.push_back( oParticle );
	}
}

void VflVirtualParticles::GetInterpolatedParticles( 
	vector<Particle>& vecParticles )
{
	STimeLevelLookup& refTimeLevelLookup = m_vecTimeLevels[m_iCurrentTimeIndex];
	m_iCurrentParticleCount = static_cast<int>( refTimeLevelLookup.size() );

	VflVisParticles::VflVisParticlesProperties *pProperties = 
		dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(
			m_pOwner->GetProperties() );

	const std::string strTimeArrayName     = pProperties->GetTimeArray();
	const std::string strPositionArrayName = pProperties->GetPositionArray();
	const std::string strScalarArrayName   = pProperties->GetScalarArray();
	const std::string strRadiusArrayName   = pProperties->GetRadiusArray();

	vecParticles.reserve( m_iCurrentParticleCount );

	for( int i=0; i<m_iCurrentParticleCount; ++i )
	{
		// fill the vertex data right away...
		SParticleLookup &refLookup = refTimeLevelLookup[i];

		int iStartIdx = refLookup.iParticleIndex;

		VveParticleTrajectory* pTraj = 
			m_pPopulation->GetTrajectory( refLookup.iTrajectoryIndex );
		VveParticleDataArrayBase* pTimeArray     = NULL;
		VveParticleDataArrayBase* pPositionArray = NULL;
		VveParticleDataArrayBase* pScalarArray   = NULL;
		VveParticleDataArrayBase* pRadiusArray   = NULL;

		bool bInitArrays =  pTraj &&
			pTraj->GetArrayByName( strTimeArrayName,     pTimeArray     ) &&
			pTraj->GetArrayByName( strPositionArrayName, pPositionArray ) &&
			pTraj->GetArrayByName( strScalarArrayName,   pScalarArray   ) &&
			( !m_bUpdatePadticleRadii || 
				pTraj->GetArrayByName( strRadiusArrayName, pRadiusArray ) );

		if( !bInitArrays )
			continue; // Skip data, if no Time, Position or Scalar information
		
		const int iTimeLevelCount = static_cast<int>(pTimeArray->GetSize());
		float fStartTime = pTimeArray->GetElementValueAsFloat(iStartIdx);

		// These two loops finds the correct first particle.
		while( fStartTime < m_dCurrentSimTime && iStartIdx < iTimeLevelCount-1 )
			fStartTime = pTimeArray->GetElementValueAsFloat( ++iStartIdx );

		while( fStartTime > m_dCurrentSimTime && iStartIdx > 0 )
			fStartTime = pTimeArray->GetElementValueAsFloat( --iStartIdx );

		// Write back the last start position to speed up searching in future updates.
		refLookup.iParticleIndex = iStartIdx;

		Particle oParticle;
		if( fStartTime == m_dCurrentSimTime )
		{
			// If we we exactly hit a particle we can simply take it's position 
			// and scalar value, ...
			pPositionArray->GetElementCopy( iStartIdx, oParticle.aPos, 3 );
			oParticle.fScalar = pScalarArray->GetElementValueAsFloat( iStartIdx );
			if( m_bUpdatePadticleRadii )
				oParticle.fRadius = pRadiusArray->GetElementValueAsFloat( iStartIdx );
		}
		else 
		{
			// ... but in case we lie between two particles, we need to interpolate.
			int   iEndIdx  = iStartIdx;
			float fEndTime = fStartTime;
			// This loop finds the right second particle.
			while( fEndTime < m_dCurrentSimTime && iEndIdx < iTimeLevelCount-1 )
				fEndTime = pTimeArray->GetElementValueAsFloat( ++iEndIdx );

			// Get particles for interpolation.
			float fScalarA = pScalarArray->GetElementValueAsFloat( iStartIdx );
			float fScalarB = pScalarArray->GetElementValueAsFloat( iEndIdx   );

			float aPosA[3], aPosB[3];
			pPositionArray->GetElementCopy( iStartIdx, aPosA );
			pPositionArray->GetElementCopy( iEndIdx,   aPosB );

			// Calculate interpolation parameter.
			float fAlpha = (static_cast<float>(m_dCurrentSimTime) - fStartTime)
			             / (fEndTime - fStartTime);

			// Simple linear interpolations.
			oParticle.fScalar = (1-fAlpha) * fScalarA + fAlpha * fScalarB;
			for( int i=0; i<3; ++i )
				oParticle.aPos[i] = (1-fAlpha) * aPosA[i] + fAlpha * aPosB[i];

			if( m_bUpdatePadticleRadii )
			{
				float fRadiusA = pRadiusArray->GetElementValueAsFloat( iStartIdx );
				float fRadiusB = pRadiusArray->GetElementValueAsFloat( iEndIdx   );

				oParticle.fRadius = (1-fAlpha) * fRadiusA + fAlpha * fRadiusB;
			}
		}

		vecParticles.push_back( oParticle );
	}
}
#pragma endregion
/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/
