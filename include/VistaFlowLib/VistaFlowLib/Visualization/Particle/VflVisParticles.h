/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVISPARTICLES_H
#define _VFLVISPARTICLES_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include <list>

#include <VistaBase/VistaVector3D.h>
#include <VistaBase/VistaQuaternion.h>

#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>
#include "../VflVisObject.h"
#include <VistaFlowLib/VistaFlowLibConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisParticles;
class VflParticleRenderer;
class VveVisParticlesParams;
class VflVtkLookupTable;
std::ostream & operator<< ( std::ostream &, const VflVisParticles & );

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflVisParticles provides a visualization object for particle data.
 */
class VISTAFLOWLIBAPI VflVisParticles : public IVflVisObject
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflVisParticles();
    virtual ~VflVisParticles();

	// ########################################################
	// RENDERABLE API
	// ########################################################

    virtual bool Init();
    virtual void Update();
    virtual void DrawOpaque();
    virtual void DrawTransparent();
	virtual unsigned int GetRegistrationMode() const;
	virtual std::string GetType() const;
    virtual void  Debug(std::ostream & out) const;

	virtual bool GetBounds(VistaVector3D &minBounds,
		                   VistaVector3D &maxBounds);


	// ########################################################
	// VISPARTICLES API
	// ########################################################

	bool AddRenderer(VflParticleRenderer *pRenderer);
	bool RemoveRenderer(VflParticleRenderer *pRenderer);


	VflParticleRenderer *GetRenderer(unsigned int iIndex) const;
	int                   GetCurrentRenderer() const;
	bool                  SetCurrentRenderer(unsigned int iIndex);
	size_t                GetNumberOfRenderers() const;

	double GetDataChangeTimeStamp() const;
	double GetVisParamsChangeTimeStamp() const;

	const VistaVector3D GetTransformedLocalViewPosition() const;
	const VistaQuaternion GetTransformedLocalViewOrientation() const;
	const VistaVector3D GetTransformedLocalLightDirection() const;

	/**
	 * re-implemented from VflVisObject. Checks the type of this
	 * data, and sets the cached particle data pointer appropriately
	 */
	bool SetUnsteadyData(VveUnsteadyData *pD);
    VveParticleDataCont  *GetParticleData() const;
    void SetParticleData(VveParticleDataCont *pData);

    VflVtkLookupTable *GetLookupTable() const;
	void                      SetLookupTable(VflVtkLookupTable *pLUT);

	VveTimeMapper           *GetTimeMapper() const;


	/**
	 * API to get and set particle properties.
	 * Use this API when programming, but it can also be
	 * used as a reflectionable or can be observed by
	 * others. Retrieve by using the GetProperties() method
	 * and an appropriate downcast.
	 * @see IVflRenderable::GetProperties()
	 */
	class VISTAFLOWLIBAPI VflVisParticlesProperties : public IVflVisObject::VflVisObjProperties
	{
		friend class VflVisParticles;
	public:
		VflVisParticlesProperties();
		virtual ~VflVisParticlesProperties();

		virtual std::string GetReflectionableType() const;

		int  GetMaxCount() const;
		bool SetMaxCount(int iCount);

		float GetTimeWindow() const;
		bool  SetTimeWindow(float fTimeWindow);

		float GetRadiusScale() const;
		bool  SetRadiusScale(float fRadiusScale);

		float GetRadiusAbsolute() const;
		bool  SetRadiusAbsolute(float fRadiusAbsolute);

		bool GetUseParticleRadius() const;
		bool SetUseParticleRadius(bool bUseParticleRadius);

		bool GetDecreaseLuminanceWithTime() const;
		bool SetDecreaseLuminanceWithTime(bool bDecrease);

		VistaVector3D GetBoundingBoxMax() const;
		bool SetBoundingBoxMax(VistaVector3D v3BBMax);
		
		VistaVector3D GetBoundingBoxMin() const;
		bool SetBoundingBoxMin(VistaVector3D v3BBMin);
		
		
        /** 
         * Getters and Setters for the names of the particle data arrays
         */    
        std::string GetPositionArray() const;
        bool SetPositionArray(std::string sPositionArray);
        
        std::string GetVelocityArray() const;
        bool SetVelocityArray(std::string sVelocityArray);
        
        std::string GetRadiusArray() const;
        bool SetRadiusArray(std::string sRadiusArray);
        
        std::string GetScalarArray() const;
        bool SetScalarArray(std::string sScalarArray);
        
        std::string GetTimeArray() const;
        bool SetTimeArray(std::string sTimeArray);
        
        std::string GetEigenValueArray() const;
        bool SetEigenValueArray(std::string sEigenValueArray);
        
        std::string GetEigenVectorArray() const;
        bool SetEigenVectorArray(std::string sEigenVectorArray);

        std::string GetDrawModeString() const;
        int GetDrawMode() const;
        bool SetDrawMode( const std::string &sString );
        bool SetDrawMode( int nMode );

        int GetRendererIndex() const;
        bool SetRendererIndex( int nIndex );

		enum
		{
			MSG_MAXCOUNT_CHANGE = VflVisObjProperties::MSG_LAST,
			MSG_TIMEWINDOW_CHANGE,
			MSG_RADIUS_SCALE_CHANGE,
			MSG_RADIUS_ABSOLUTE_CHANGE,
			MSG_USE_PARTICLE_RADIUS_CHANGE,
			MSG_DECREASE_LUMINANCE_CHANGE,
            MSG_DRAWMODE_CHANGE,
            MSG_RENDERER_CHANGE,
            MSG_POSITION_ARRAY_CHANGE,
            MSG_VELOCITY_ARRAY_CHANGE,
            MSG_RADIUS_ARRAY_CHANGE,
            MSG_SCALAR_ARRAY_CHANGE,
            MSG_TIME_ARRAY_CHANGE,
            MSG_EIGENVALUE_ARRAY_CHANGE,
            MSG_EIGENVECTOR_ARRAY_CHANGE,
			MSG_BOUNDINGBOX_CHANGE,
			MSG_LAST
		};

	protected:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	protected:
	private:
		int     m_iMaxCount;
		float   m_fTimeWindow;
		float	m_fRadiusScale;
		float	m_fRadiusAbsolute;
		bool	m_bUseParticleRadius;
		bool	m_bDecreaseLuminanceWithTime;

		VistaVector3D m_v3BBMax;
		VistaVector3D m_v3BBMin;
		
        // Particle data array names ( + default in c'tor)
        std::string m_sPositionArray; /* = "positions" */
        std::string m_sVelocityArray; /* = "velocities" */
        std::string m_sScalarArray;   /* = "scalars" */
        std::string m_sRadiusArray;   /* = "radius" */
        std::string m_sTimeArray;     /* = "sim_time" */
        std::string m_sEigenValueArray;  /* = "eigenvalues" */
        std::string m_sEigenVectorArray; /* = "eigenvectors" */
            
	};

	virtual VflVisParticlesProperties *GetProperties() const;

	static std::string   GetFactoryType();


	// ########################################################
	// OBSERVER API
	// ########################################################

	virtual void ObserverUpdate(IVistaObserveable *pObserveable,
		                        int msg,
								int ticket);

protected:
	enum
	{
		E_PARTICLEDATA_CHANGE = IVflVisObject::E_TICKET_LAST,
		E_VISPARAMETERS_CHANGE,
		E_LOOKUPTABLE_CHANGE,
		E_TICKET_LAST
	};

	// ########################################################
	// RENDERABLE API
	// ########################################################
	virtual VflVisObjProperties    *CreateProperties() const;

private:
    VveParticleDataCont                 *m_pData;
    VflVtkLookupTable				*m_pLookupTable;

	std::vector<VflParticleRenderer*>	m_vecRenderers;
	int									m_iCurrentRenderer;

	double	m_dDataTimeStamp;
	double	m_dVisParamsTimeStamp;
	bool    m_bBoundsDirty;

	float   m_afMinB[3],
		    m_afMaxB[3];

	float	m_aCurTrans[16];

private:
	friend class VflVisParticlesProperties;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/
inline double VflVisParticles::GetDataChangeTimeStamp() const
{
	return m_dDataTimeStamp;
}

inline double VflVisParticles::GetVisParamsChangeTimeStamp() const
{
	return m_dVisParamsTimeStamp;
}

inline VveParticleDataCont *VflVisParticles::GetParticleData() const
{
	return m_pData;
}

//inline VveVisParticlesParams *VflVisParticles::GetVisParams() const
//{
//	return m_pVisParams;
//}

inline VflVtkLookupTable *VflVisParticles::GetLookupTable() const
{
	return m_pLookupTable;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLVISPARTICLES_H)
