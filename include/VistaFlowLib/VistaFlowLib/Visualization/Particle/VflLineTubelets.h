/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLLINETUBELETS_H
#define _VFLLINETUBELETS_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "../VflRenderNode.h"
#include "VflParticleRenderer.h"
#include <vector>

#include <VistaAspects/VistaPropertyAwareable.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>
#include <VistaVisExt/Data/VveParticleDataArray.h>
#include "VflVisParticles.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisController;
class VflParticleColorData;
class VveParticleDataArrayBase;
class VveParticlePopulation;

class VistaGLSLShader;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * VflLineTubelets renders particle data cylindrical billboards.
 */
class VISTAFLOWLIBAPI VflLineTubelets : public VflParticleRenderer
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    /**
     * Creates a VflLineTubelets object. The following property keys are recognized:
     *
     * TEXTURE_SIZE [edge length]   -   optional
     * USE_MIPMAPS [true|false]     -   optional [default: false]
     * STYLE                        -   optional [default: "DM_LINES_CYLINDER"]
     */
    //VflLineTubelets(VflVisParticles *pOwner, const VistaPropertyList &refProps);

	enum EDrawMode
	{
		DM_LINES_TUBLETS_SINPLE = DM_FIRST,
		DM_LINES_TUBLETS_DIFFUSE,
		DM_LINES_TUBLETS_DIFFUSE_SPECULAR,
		DM_LINES_CYLINDER_SINPLE,
		DM_LINES_CYLINDER_DIFFUSE,
		DM_LINES_CYLINDER_DIFFUSE_SPECULAR,
		DM_LINES_RAW,
		DM_COUNT
	};


    VflLineTubelets(VflVisParticles *pOwner,
						EDrawMode eStyle    = DM_LINES_CYLINDER_SINPLE);


    virtual ~VflLineTubelets();

    /**
     * Initialize renderer.
     */
    virtual bool Init();

	/**
	 * Destroy internal data.
	 */
	virtual void Destroy();

	/**
	 * Update the renderer, i.e. allow it to perform any data updates and such
	 * outside the actual render process.
	 */
	virtual void Update();

	/**
	 * Draw any opaque stuff.
	 */
	virtual void DrawOpaque();


    /**
     * Returns the type of the particle renderer as a string.
     */
	virtual std::string GetType() const;

	/**
	 * Manage different draw modes.
	 */
	virtual void SetDrawMode(int iMode);
	virtual void SetDrawMode(const std::string &strMode);
	virtual int GetDrawMode() const;
	virtual std::string GetDrawModeAsString() const;
	virtual std::string GetDrawModeString(int iMode) const;
	virtual int GetDrawModeCount() const;

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

protected:

	void UpdateParticleData();
	void UpdateVisParams();
	void CleanUp();

	bool CalculateRange( size_t nTrajectoryIdx,
		VveParticleDataArray<float> *pTimeArray, float fCurrentTime,
		float fMinTime, size_t &nCurrentIndex, size_t &nMinIndex );

	VveParticlePopulation *m_pParticles;
	VflVisParticles::VflVisParticlesProperties *m_pParams;
	VflParticleColorData *m_pColorData;

	GLuint  m_aVbo;

	int m_iDrawMode;
	bool m_bInitialized;

	// additional information for a more efficient
	// traversal of the particle data...
	struct STrajectoryInfo
	{
		float fMinTime;
		float fMaxTime;
		size_t nCurrentIndex;
		size_t nPositionsArrayIndex;
		size_t nTimeArrayIndex;
		size_t nScalarArrayIndex;
	};

	int m_nVboVertices;

	std::vector<STrajectoryInfo> m_vecTrajectoryIndex;
	std::vector<int> m_vecTubeletVertexCounts;
	std::vector<GLsizei> m_vecPathlineStarts;
	std::vector<GLsizei> m_vecPathlineCounts;

	float m_fTimeWindow;
	float m_fMaxTimeWindow;
	int m_iMaxParticles;
	int m_iPathlineCount;
	int m_iParticleStep;

	int m_nPositionComponents;
	int m_nColorComponents;

	VistaGLSLShader* m_pShader[6];

	int	m_iShaderHandle[6];

	float m_fCurrentTime;
	float m_fDeltaLight;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLVIRTUALTUBELETS_H)
