/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVIRTUALTUBELETS_H
#define _VFLVIRTUALTUBELETS_H
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <VistaBase/VistaVector3D.h>
#include <VistaBase/VistaColor.h>

#include "VflParticleRenderer.h"

#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisController;
class VistaGLSLShader;
class VistaTexture;
class VflLookupTexture;
class VistaBufferObject;
class VistaVertexArrayObject;
class VveParticleDataArrayBase;
class VistaParticleTraceRenderingCore;
class VistaParticleRenderingProperties;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflVirtualTubelets : public VflParticleRenderer
{
public:
    // CONSTRUCTORS / DESTRUCTOR

    VflVirtualTubelets(	VflVisParticles *pOwner );

    virtual ~VflVirtualTubelets();

	VistaParticleRenderingProperties* GetParticleRenderingProperties();

	/**
	 * Sets whether the head and tail of the particle track should be interpolated.
	 */
	void SetInterpolate( bool bInterpolate );
	bool GetInterpolate() const;

    /**
     * Initialize renderer.
     */    
    virtual bool Init();

	/**
	 * Destroy internal data.
	 */
	virtual void Destroy();

	/**
	 * Update the renderer, i.e. allow it to perform any data updates and such
	 * outside the actual render process.
	 */
	virtual void Update();

	/**
	 * Draw any opaque stuff.
	 */
	virtual void DrawOpaque();
	
    /**
     * Returns the type of the particle renderer as a string.
     */    
	virtual std::string GetType() const;

	/**
	 * Manage different draw modes.
	 */
	virtual void SetDrawMode( int iMode );
	virtual void SetDrawMode( const std::string &strMode );
	virtual int GetDrawMode() const;
	virtual std::string GetDrawModeAsString() const;
	virtual std::string GetDrawModeString(int iMode) const;
	virtual int GetDrawModeCount() const;
	
	/**
	 * Select only a subset of particle trajectories to render from the whole population.
	 * By default, all trajectories are selected. You can change the selection set here.
	 * Just specify the set by the indices of the desired trajectories in the original population.
	 */
	void SetSelectedTrajectories (const std::vector<int> & vecSelectedTrajectories);

protected:
	void UpdateVisParams();
	void UpdateParticleData();

	void UpdateBufferObjects();

	// additional information for a more efficient 
	// traversal of the particle data...
	struct STrajectoryInfo
	{
		float fMinTime;
		float fMaxTime;
		int iCurrentIndex;
		std::vector<VistaVector3D> vecDirections;
	};

	void UpdateTrajectoryData(
		VveParticleDataArrayBase* pTimeArray, 
		VveParticleDataArrayBase* pPosArray, 
		VveParticleDataArrayBase* pScalarArray,
		STrajectoryInfo& rTrajectoryInfo );

	void AddBodyVetex( 
		const VistaVector3D& v3Pos, 
		const VistaVector3D& v3Dir, 
		float fScalar, float fTime, float fXOffset );
	
	void AddCap( 
		const VistaVector3D& v3Pos, 
		const VistaVector3D& v3Dir,
		float fScalar, float fTime );

private:
	bool m_bInitialized;
	bool m_bInterpolat;

	double m_dStartTime;
	double m_dEndTime;

	float m_fTimeWindow;

	std::vector<float> m_vecBodyVertices;
	std::vector<float> m_vecCapVertices;

	std::vector<unsigned int> m_vecBodyIndices;

	VistaVertexArrayObject* m_pBodyVAO;
	VistaBufferObject*		m_pBodyVBO;
	VistaBufferObject*		m_pBodyIBO;

	VistaVertexArrayObject* m_pCapVAO;
	VistaBufferObject*		m_pCapVBO;

	VflLookupTexture*	m_pLUT;

	VistaParticleTraceRenderingCore* m_pCore;

	std::vector<STrajectoryInfo> m_vecTrajectoryInfos;

	std::vector<int> m_vecSelectedTrajectories;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLVIRTUALTUBELETSVPFP_H)
