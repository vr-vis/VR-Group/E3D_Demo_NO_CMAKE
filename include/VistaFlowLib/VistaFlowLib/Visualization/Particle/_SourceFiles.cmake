

set( RelativeDir "./Visualization/Particle" )
set( RelativeSourceGroup "Source Files\\Visualization\\Particle" )
set( SubDirs shaders)

set( DirFiles
	VflEllipsoidTensorIcon.cpp
	VflEllipsoidTensorIcon.h
	VflParticleColorData.cpp
	VflParticleColorData.h
	VflParticleRenderer.cpp
	VflParticleRenderer.h
	VflStaticEllipsoidTensorIcon.cpp
	VflStaticEllipsoidTensorIcon.h
	VflVirtualParticles.cpp
	VflVirtualParticles.h
	VflVirtualTubelets.cpp
	VflVirtualTubelets.h
	VflLineTubelets.h	
	VflLineTubelets.cpp
	VflVisParticles.cpp
	VflVisParticles.h
	VflVtkGlyphs.cpp
	VflVtkGlyphs.h
	VflVtkTubes.cpp
	VflVtkTubes.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()
