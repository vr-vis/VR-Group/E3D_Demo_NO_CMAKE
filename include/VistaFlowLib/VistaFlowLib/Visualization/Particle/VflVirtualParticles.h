/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VFL_VIRTUAL_PARTICLES_H
#define VFL_VIRTUAL_PARTICLES_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include "../VflVisController.h"
#include "VflParticleRenderer.h"

#include <VistaFlowLib/VistaFlowLibConfig.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>

#include <vector>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTexture;
class VistaGLSLShader;
class VflLookupTexture;
class VistaParticleRenderingCore;
class VistaParticleRenderingProperties;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflVirtualParticlesVpFp renders particle data via billboards, which get
 * aligned to the viewer through a vertex program.
 */    
class VISTAFLOWLIBAPI VflVirtualParticles : public VflParticleRenderer
{
public:
    // CONSTRUCTORS / DESTRUCTOR

    VflVirtualParticles( VflVisParticles *pOwner, 
	                        int iTextureSize = 32, 
	                        bool bUseMipMaps = true );

    virtual ~VflVirtualParticles();


	VistaParticleRenderingProperties* GetParticleRenderingProperties();
    /**
     * Initialize renderer.
     */    
    virtual bool Init();

	/**
	 * Destroy internal data.
	 */
	virtual void Destroy();

	/**
	 * Update the renderer, i.e. allow it to perform any data updates and such
	 * outside the actual render process.
	 */
	virtual void Update();

	/**
	 * Draw any opaque stuff.
	 */
	virtual void DrawOpaque();


    /**
     * Returns the type of the particle renderer as a string.
     */    
	virtual std::string GetType() const;

	/**
	 * Manage different draw modes.
	 */	
	virtual void SetDrawMode( int iMode );
	virtual void SetDrawMode( const std::string &strMode );
	virtual int  GetDrawMode() const;
	virtual std::string GetDrawModeAsString() const;
	virtual std::string GetDrawModeString(int iMode) const;
	virtual int GetDrawModeCount() const;

	/**
	 * Select only a subset of particle trajectories to render from thw whole population.
	 * By default, all trajectories are selected. You can change the selection set here.
	 * Just specify the set by the indices of the desired trajectories in the original population.
	 */
	void SetSelectedTrajectories( const std::vector<int> & vecSelectedTrajectories );

	/**
	 * Sets whether the position and scalar values of particles should be interpolated.
	 */
	void SetInterpolate( bool bInterpolate );
	bool GetInterpolate() const;

protected:
	bool CreateParticleDataTexture();

	void UpdateParticleData();
	void UpdateVisParams();
	void UpdateParticleDataTexture();

	struct Particle {
		float aPos[3]; // position of the particle
		float fScalar; // scalar value of the particle
		float fRadius; // scalar value of the particle
		float fDist;   // square distance to the viewer. used for sorting particles
	};

	void SortParticles( std::vector<Particle>& vecParticles );

	static bool CompareParticls( const Particle& p1, const Particle& p2);

	void GetParticles( std::vector<Particle>& vecParticles );
	void GetInterpolatedParticles( std::vector<Particle>& vecParticles );

protected:
	VistaParticleRenderingCore* m_pCore;

	VflLookupTexture* m_pLUT;

	int    m_iCurrentParticleCount;
	int    m_iCurrentTimeIndex;
	double m_dCurrentSimTime;

	float*        m_pParticleData;
	float*        m_pParticleRadii;
	VistaTexture* m_pParticleDataTexture;
	VistaTexture* m_pParticleRadiiTexture;

	bool m_bUpdatePadticleRadii;

	bool m_bInitialized;
	bool m_bInterpolat;

	std::vector<int> m_vecSelectedTrajectories;

protected:
	struct SParticleLookup
	{
		int iParticleIndex;
		int iTrajectoryIndex;
	};

	typedef std::vector<SParticleLookup> STimeLevelLookup;

	std::vector<STimeLevelLookup> m_vecTimeLevels;

	VveParticlePopulationItem* m_pParticles;
	VveParticlePopulation*     m_pPopulation;
};

#endif // !defined(_VFLVIRTUALPARTICLESVPFP_H)
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
