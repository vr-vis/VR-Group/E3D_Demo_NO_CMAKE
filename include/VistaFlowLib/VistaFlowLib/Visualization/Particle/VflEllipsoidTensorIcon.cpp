/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflParticleColorData.h"
#include "VflEllipsoidTensorIcon.h"
#include "VflVisParticles.h"

#include "../VflRenderNode.h"
#include "../VflVisTiming.h"
#include "../VflVtkLookupTable.h"

#include <VistaVisExt/Data/VveParticleTrajectory.h>

#include <VistaBase/VistaStreamUtils.h>

#include <typeinfo>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflEllipsoidTensorIcon::VflEllipsoidTensorIcon(VflVisParticles *pOwner, 
												 bool bImmediateMode)
: VflParticleRenderer(pOwner), m_iCurrentTimeIndex(-1), 
  m_bImmediateMode(bImmediateMode), m_bInitialized(false), 
  m_dQuadricSize(0.0), m_bUpdatePopulationColor(true), m_bColoringXYZaxes(false),
  m_dAxesRadiusFactor(0.05)
{
	m_aAmbientColor[0] = 0.1f;
	m_aAmbientColor[1] = 0.1f;
	m_aAmbientColor[2] = 0.1f;
	m_aAmbientColor[3] = 0.3f;
	m_fDiffuseOpacity = 0.5f;

	m_pOwner->GetParticleData()->GetData()->LockData();

	// @TODO: Is it required to perform a deep copy of the original Particle Data?
	m_pPopulation = m_pOwner->GetParticleData()->GetData()->GetData();

	m_pPopulationItem = new VveParticlePopulationItem();
	m_pPopulationItem->SetData(m_pPopulation);
	m_pPopulationItem->Notify();	

	m_pOwner->GetParticleData()->GetData()->UnlockData();
}

VflEllipsoidTensorIcon::~VflEllipsoidTensorIcon()
{
	this->Destroy();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflEllipsoidTensorIcon::Init()
{
	if (m_bInitialized) return false;

	m_pColorData = new VflParticleColorData( m_pPopulationItem,
                                             m_pOwner->GetLookupTable(),
                                             m_pOwner->GetProperties());

	// init quadric and its common entities
	m_pQuadric = gluNewQuadric();
	gluQuadricOrientation(m_pQuadric, GLU_OUTSIDE);
	gluQuadricDrawStyle(m_pQuadric, GLU_FILL);
	gluQuadricNormals(m_pQuadric, GLU_SMOOTH);

	if(m_dQuadricSize<=0.0)
	{
		vstr::warnp() << "[VflEllipsoidTensorIcon::Init] m_dQuadricSize is automaticaly set to 0.5" << std::endl;
		m_dQuadricSize = 0.5;
	}

	// init quadric which visualize the orthogonal eigenvector axis
	for (int i=0; i<3; ++i) 
	{
		m_pQuadricAxis[i] = gluNewQuadric(); 
		gluQuadricOrientation(m_pQuadricAxis[i], GLU_OUTSIDE);
		gluQuadricDrawStyle(m_pQuadricAxis[i], GLU_FILL);
		gluQuadricNormals(m_pQuadricAxis[i], GLU_SMOOTH);
	}

	m_bInitialized = true;
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Destroy                                                     */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::Destroy()
{
	delete m_pPopulation;
	m_pPopulation = NULL;
	delete m_pColorData;
	m_pColorData=NULL;

	m_bInitialized = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::Update()
{
	if (m_dDataTimeStamp < m_pOwner->GetDataChangeTimeStamp())
		UpdateParticleData();
	if (m_dVisParamsTimeStamp < m_pOwner->GetVisParamsChangeTimeStamp())
		UpdateVisParams();

	// determine which time index to be rendered
	VveTimeMapper *pTimeMapper = m_pOwner->GetTimeMapper();
	int iCurrentTimeIndex = pTimeMapper->GetTimeIndex(
		m_pOwner->GetRenderNode()->GetVisTiming()->GetVisualizationTime());

	if(iCurrentTimeIndex < 0)
		return;   // if invalid time index or no update is needed then return

    // get properties as pParam from owner
    VflVisParticles::VflVisParticlesProperties *pParams = 
        dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(m_pOwner->GetProperties());
	
    unsigned int iPathlineCount = (unsigned int) m_pPopulation->GetNumTrajectories();

	//=================
	// Transformations
	//=================

	if(iCurrentTimeIndex != m_iCurrentTimeIndex)
	{
		// clean old data
		for(unsigned int i=0; i<m_vecTransMatrix.size(); ++i)  
		{
			delete[] m_vecTransMatrix[i];
		}
		m_vecTransMatrix.clear();

		for(size_t i=0; i<m_vecAxisLength.size(); ++i)
		{
			delete[] m_vecAxisLength[i];
		}
		m_vecAxisLength.clear();

		// iterate over the collection of pathlines
		for(unsigned int i=0; i<iPathlineCount; ++i)
		{
			//SVveParticleTrajectory &refTrajectory = m_pPopulation->vecPopulation[i];
            VveParticleTrajectory *pTraj = m_pPopulation->GetTrajectory(i);
            VveParticleDataArrayBase *pPosArray = NULL;
            VveParticleDataArrayBase *pEigenValueArray = NULL;
            VveParticleDataArrayBase *pEigenVectorArray = NULL;

            if( !pTraj ||
                !pTraj->GetArrayByName(pParams->GetPositionArray(), pPosArray) || !pPosArray ||
                !pTraj->GetArrayByName(pParams->GetEigenValueArray(), pEigenValueArray) || !pEigenValueArray ||
                !pTraj->GetArrayByName(pParams->GetEigenVectorArray(), pEigenVectorArray) || !pEigenVectorArray )
                continue;


			GLfloat *fTmpMatrix = new GLfloat[16];
			GLfloat *fTmpAxisLength = new GLfloat[3];

			if(iCurrentTimeIndex < static_cast<int>(pTraj->GetSize()))
			{
                // Get position
                float aPos[3];
                pPosArray->GetElementCopy(iCurrentTimeIndex, aPos, 9);

                // Get eigenvalue 
                pEigenValueArray->GetElementCopy(iCurrentTimeIndex, fTmpAxisLength );

                // Get eigenvectors
                float aEigenVectors[9];
                pEigenVectorArray->GetElementCopy(iCurrentTimeIndex, aEigenVectors, 9);

                // Swap 0<->1: eigenvalues
                float fTemp = fTmpAxisLength[0];
                fTmpAxisLength[0] = fTmpMatrix[1];
                fTmpAxisLength[1] = fTemp;

				// translate & scale & rotate as base change matrix M
				//      (  EV1  EV0  EV2  aPos[0]  )     (  m0  m4  m8  m12  )
				// M := (  EV1  EV0  EV2  aPos[1]  )  =  (  m1  m5  m9  m13  )
				//      (  EV1  EV0  EV2  aPos[2]  )     (  m2  m6  m10 m14  )
				//      (   0    0    0    1       )     (  m3  m7  m11 m15  )   column-major (openGL's "style")		
                //
                // eigenvector 0 & 1 are swapped as well!

				for(int iIdx=0; iIdx<3; ++iIdx) // 0->x, 1->y, 2->z
				{
					fTmpMatrix[iIdx]    = fTmpAxisLength[iIdx] * aEigenVectors[3+iIdx];
					fTmpMatrix[4+iIdx]  = fTmpAxisLength[iIdx] * aEigenVectors[0+iIdx];
					fTmpMatrix[8+iIdx]  = fTmpAxisLength[iIdx] * aEigenVectors[6+iIdx];
                    fTmpMatrix[12+iIdx] = aPos[iIdx];
				}

                // last row of transformation matrix -> ext. coordinates:
                fTmpMatrix[3]  = 0.0; //vector
				fTmpMatrix[7]  = 0.0; //vector
				fTmpMatrix[11] = 0.0; //vector
				fTmpMatrix[15] = 1.0; //point
			}
			else
			{
				for(int iIdx=0; iIdx<16; ++iIdx)  fTmpMatrix[iIdx] = 0.0;
				fTmpAxisLength[0] = 0.0;
				fTmpAxisLength[1] = 0.0;
				fTmpAxisLength[2] = 0.0;
			}
			m_vecTransMatrix.push_back(fTmpMatrix);
			m_vecAxisLength.push_back(fTmpAxisLength);
		}
	}

	//========
	// Colors
	//========

	if(m_bUpdatePopulationColor || iCurrentTimeIndex != m_iCurrentTimeIndex)
	{
		GLfloat *fTmpColor;
		// get current color data
		const VflParticleColorData::POPULATION_COLORS &refColors = m_pColorData->GetColors();

		// clear old data
		for(unsigned int i=0; i<m_vecColor.size(); ++i)
		{
			delete[] m_vecColor[i];
		}
		m_vecColor.clear();

		// iterate over the collection of pathlines
		for(unsigned int i=0; i<iPathlineCount; ++i)
		{
			fTmpColor = new GLfloat[4];

			if(iCurrentTimeIndex < static_cast<int>(refColors[i].size()))
			{
				//refColors[i][m_iCurrentTimeIndex];    // current color?
				fTmpColor[0] = refColors[i][iCurrentTimeIndex].aColor[0];
				fTmpColor[1] = refColors[i][iCurrentTimeIndex].aColor[1];
				fTmpColor[2] = refColors[i][iCurrentTimeIndex].aColor[2];
				fTmpColor[3] = refColors[i][iCurrentTimeIndex].aColor[3];
			}
			else
			{
				for(int iIdx=0; iIdx<4; ++iIdx) fTmpColor[iIdx] = 0.0;
			}
			m_vecColor.push_back(fTmpColor);
		}
		m_bUpdatePopulationColor = false;
	}

	// update m_iCurrentTimeIndex
	m_iCurrentTimeIndex = (unsigned int) iCurrentTimeIndex;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::DrawOpaque()
{
	if (m_iCurrentTimeIndex < 0)
		return;

	glPushAttrib(GL_POLYGON_BIT|GL_LIGHTING_BIT|GL_ENABLE_BIT);
	glDisable(GL_CULL_FACE);

	GLfloat glFloatAxisLength[3];
	GLfloat glFloatTransMatrix[16];    // column-major

	// encapsulate every transformation, so it only affects these lines --- begin --+
	glMatrixMode(GL_MODELVIEW);                                                   //|
	//------------------------------------------------------------------------------+
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_SPECULAR);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 128.0f);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
	glColor4f(0.1f, 0.1f, 0.1f, 1.0f);
	glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);

	size_t iNumParticles =  m_vecTransMatrix.size();
	// make sure that every datas were correctly updated
	if(m_vecAxisLength.size() != iNumParticles || m_vecColor.size() != iNumParticles)
	{
		vstr::warnp() << "[VflEllipsoidTensorIcon] in DrawOpaque(): something wrong with rendering datas" 
	         << endl;
		return;
	}

	double dOriginalAxisLength = 2.0 * m_dQuadricSize;
	double dAxisRadius = m_dAxesRadiusFactor * m_dQuadricSize;

	for(size_t i=0; i<iNumParticles; ++i)
	{
		glPushMatrix();
		glFloatAxisLength[0] = m_vecAxisLength[i][0];
		glFloatAxisLength[1] = m_vecAxisLength[i][1];
		glFloatAxisLength[2] = m_vecAxisLength[i][2];

		// normalize each eigenvectors part
		for(int j=0; j<4; ++j)
		{
			// first column (eigenvector1)
			glFloatTransMatrix[j] = m_vecTransMatrix[i][j] / glFloatAxisLength[0];
			// second column (eigenvector0)
			glFloatTransMatrix[4+j] = m_vecTransMatrix[i][4+j] / glFloatAxisLength[1];
			// third colum (eigenvector2)
			glFloatTransMatrix[8+j] = m_vecTransMatrix[i][8+j] / glFloatAxisLength[2];
		}

		// copy translation part
		for(int j=12; j<16; ++j)
			glFloatTransMatrix[j] = m_vecTransMatrix[i][j];

		glMultMatrixf(glFloatTransMatrix);

		if(!m_bColoringXYZaxes) glColor4fv(m_vecColor[i]);	
		
		// x axis
		glPushMatrix();
		if(m_bColoringXYZaxes) 
			glColor4f(0.9f,0.2f,0.2f,1.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glTranslatef(0.0f, 0.0f, static_cast<float>(-m_dQuadricSize)*glFloatAxisLength[0]*0.95f);
		gluCylinder(m_pQuadricAxis[0], dAxisRadius, dAxisRadius, dOriginalAxisLength*static_cast<double>(glFloatAxisLength[0])*0.95, 8, 8);
		glPopMatrix();

		// y axis
		glPushMatrix();
		if(m_bColoringXYZaxes) 
			glColor4f(0.2f,0.9f,0.2f,1.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		glTranslatef(0.0f, 0.0f, static_cast<float>(-m_dQuadricSize)*glFloatAxisLength[1]*0.95f);
		gluCylinder(m_pQuadricAxis[1], dAxisRadius, dAxisRadius, dOriginalAxisLength*static_cast<double>(glFloatAxisLength[1])*0.95, 8, 8);
		glPopMatrix();

		// z axis
		if(m_bColoringXYZaxes) 
			glColor4f(0.2f,0.2f,0.9f,1.0f);
		glTranslatef(0.0f, 0.0f, static_cast<float>(-m_dQuadricSize)*glFloatAxisLength[2]*0.95f);
		gluCylinder(m_pQuadricAxis[2], dAxisRadius, dAxisRadius, dOriginalAxisLength*static_cast<double>(glFloatAxisLength[2])*0.95, 8, 8);
		glPopMatrix();
	}	

	// encapsulate every transformation, so it only affects these lines --- end --+
	glMatrixMode(GL_MODELVIEW);	                                                //|
	//----------------------------------------------------------------------------+	

	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::DrawTransparent()
{
	if (m_iCurrentTimeIndex < 0)
		return;

	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDisable(GL_ALPHA_TEST);

	glShadeModel(GL_SMOOTH);

	// encapsulate every transformation, so it only affects these lines --- begin --+
	glMatrixMode(GL_MODELVIEW);                                                   //|
	//------------------------------------------------------------------------------+

	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_SPECULAR);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 128.0f);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
	glColor4f(m_aAmbientColor[0], m_aAmbientColor[1], m_aAmbientColor[2], m_aAmbientColor[3]);
	glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);

	const size_t nNumParticles = m_vecTransMatrix.size();

	for(size_t i=0; i<nNumParticles; ++i)
	{
		glPushMatrix();

		glMultMatrixf(m_vecTransMatrix[i]);

		glColor4f(m_vecColor[i][0], m_vecColor[i][1], m_vecColor[i][2], m_fDiffuseOpacity);
		gluSphere(m_pQuadric,m_dQuadricSize,16,16);

		glPopMatrix();
	}	

	// encapsulate every transformation, so it only affects these lines --- end --+
	glMatrixMode(GL_MODELVIEW);	                                                //|
	//----------------------------------------------------------------------------+	
	glEnable(GL_ALPHA_TEST);
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateParticleData                                          */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::UpdateParticleData()
{
	VveTimeMapper *pMapper = m_pOwner->GetTimeMapper();
	m_pOwner->GetParticleData()->GetData()->LockData();
	VveParticlePopulation *pOriginalPopulation = m_pOwner->GetParticleData()->GetData()->GetData();

    VflVisParticles::VflVisParticlesProperties *pParams = 
        dynamic_cast<VflVisParticles::VflVisParticlesProperties*>(
        m_pOwner->GetProperties());

	// update local (reduced) particle data
	vstr::outi() << " [VflEllipsoidTensorIcon] - reducing trajectory information..." << endl;
	pOriginalPopulation->ReduceInformation(pMapper, pParams->GetTimeArray(), m_pPopulation);
	m_vecTransMatrix.reserve(m_pPopulation->GetNumTrajectories());

	// test whether the new data has the required information for this renderer
    // by checking if the appropriate particle data arrays are present for the first trajectory

    VveParticleTrajectory *pTraj = m_pPopulation->GetTrajectory(0);
    VveParticleDataArrayBase *pEigenValueArray = NULL;
    VveParticleDataArrayBase *pEigenVectorArray = NULL;

	if( !pTraj ||
        !pTraj->GetArrayByName(pParams->GetEigenValueArray(), pEigenValueArray) || !pEigenValueArray ||
        !pTraj->GetArrayByName(pParams->GetEigenVectorArray(), pEigenVectorArray) || !pEigenVectorArray)
	{
		vstr::errp() << "[VflEllipsoidTensorIcon] in UpdateParticleData(): required particle data array not found" << endl;
		m_pOwner->GetParticleData()->GetData()->UnlockData();
		return;
	}

	m_pPopulationItem->Notify();	

	m_dDataTimeStamp = m_pOwner->GetDataChangeTimeStamp();
	m_pOwner->GetParticleData()->GetData()->UnlockData();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateVisParams                                             */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::UpdateVisParams()
{
	//VflVisParticles::VflVisParticlesProperties *pParams = 
	//	dynamic_cast<VflVisParticles::VflVisParticlesProperties *>(m_pOwner->GetProperties());

	//if(!pParams)
	//	return;

	m_bUpdatePopulationColor = true;

	m_dVisParamsTimeStamp = m_pOwner->GetVisParamsChangeTimeStamp();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetColoringXYZaxes                                          */
/*                                                                            */
/*============================================================================*/
bool VflEllipsoidTensorIcon::GetColoringXYZaxes()
{
	return m_bColoringXYZaxes;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetColoringXYZaxes                                          */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::SetColoringXYZaxes(bool bColoring)
{
	m_bColoringXYZaxes = bColoring;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetQuadricSize                                              */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::SetQuadricSize(double dSize) 
{
	m_dQuadricSize=dSize;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetQuadricSize                                              */
/*                                                                            */
/*============================================================================*/
double VflEllipsoidTensorIcon::GetQuadricSize() 
{
	return m_dQuadricSize;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetAxesRadiusFactor                                         */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::SetAxesRadiusFactor(double dFactor)
{
	m_dAxesRadiusFactor = dFactor;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetAxesRadiusFactor                                         */
/*                                                                            */
/*============================================================================*/
double VflEllipsoidTensorIcon::GetAxesRadiusFactor()
{
	return m_dAxesRadiusFactor;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetAmbientColor                                             */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::SetAmbientColor(float aAmbient[4])
{
	m_aAmbientColor[0] = aAmbient[0];
	m_aAmbientColor[1] = aAmbient[1];
	m_aAmbientColor[2] = aAmbient[2];
	m_aAmbientColor[3] = aAmbient[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetAmbientColor                                             */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::GetAmbientColor(float aAmbient[4])
{
	aAmbient[0] = m_aAmbientColor[0];
	aAmbient[1] = m_aAmbientColor[1];
	aAmbient[2] = m_aAmbientColor[2];
	aAmbient[3] = m_aAmbientColor[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDiffuseOpacity                                           */
/*                                                                            */
/*============================================================================*/
void VflEllipsoidTensorIcon::SetDiffuseOpacity(float fAlpha)
{
	m_fDiffuseOpacity = fAlpha;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDiffuseOpacity                                           */
/*                                                                            */
/*============================================================================*/
float VflEllipsoidTensorIcon::GetDiffuseOpacity()
{
	return m_fDiffuseOpacity;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflEllipsoidTensorIcon.cpp"                                  */
/*============================================================================*/
