/*============================================================================*/
/* SHADER FUNKTION															  */
/*============================================================================*/

vec4 ShadeFragment(vec3 v3Normal, vec3 v3Position, vec4 v4MatColor)
{
	vec3 light_dir  = normalize(gl_LightSource[0].position.xyz);
	vec3 r 		= normalize(reflect(-light_dir, v3Normal));
	float NdotL = dot(light_dir, v3Normal);
	float fSpecular = pow(max(dot(r, normalize(-v3Position)), 0.0), 16.0);
	return v4MatColor * NdotL + vec4(fSpecular);
}

/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/