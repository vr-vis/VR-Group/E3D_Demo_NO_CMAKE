

set( RelativeDir "./Visualization/Particle/shaders" )
set( RelativeSourceGroup "Source Files\\Visualization\\Particle\\Shaders" )

set( DirFiles
	VflLineTubelets_DiffuseLighting_frag.glsl
	VflLineTubelets_PhongLighting_frag.glsl
	VflLineTubelets_SimpleLighting_frag.glsl
	VflLineTubelets_vert.glsl
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

