#version 330 compatibility
/*============================================================================*/
/* UNIFORM VARIABLES														  */
/*============================================================================*/
uniform float fStartTime;
uniform float fDeltaTime;
/*============================================================================*/
/* VARYING VARIABLES														  */
/*============================================================================*/
out vec4 var_v4PrimaryColor;
out vec4 var_v4SecondaryColor;
/*============================================================================*/
/* VERTEX ATTRIBUTES														  */
/*============================================================================*/
layout(location = 1) in float fTime;
in vec4 gl_Vertex;
in vec4 gl_Color;
in vec4 gl_SecondaryColor;
/*============================================================================*/
/* SHADER MAIN																  */
/*============================================================================*/
void main(void)
{
	var_v4PrimaryColor   = gl_Color * (1.0f - fDeltaTime*(fStartTime - fTime));
	var_v4SecondaryColor = gl_SecondaryColor;
	gl_Position          = gl_ModelViewMatrix*gl_Vertex;
}
/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
