/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLPARTICLECOLORDATA_H
#define _VFLPARTICLECOLORDATA_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>
#include "../VflVtkLookupTable.h"
#include "../../Data/VflObserver.h"

#include <VistaFlowLib/VistaFlowLibConfig.h>
#include "VistaVisExt/Data/VveParticlePopulation.h"
#include "VistaVisExt/Data/VveParticleDataArray.h"
#include "VflVisParticles.h"
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VflParticleColorData;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflParticleColorData contains and manages coloration data of a particle population
 * according to a given lookup table object.
 */    
class VISTAFLOWLIBAPI VflParticleColorData : public VflObserver
{
public:
	// CONSTRUCTORS / DESTRUCTOR
	VflParticleColorData( VveParticlePopulationItem *pParticleData, VflVtkLookupTable *pLut,
                          VflVisParticles::VflVisParticlesProperties* pProperties);
	virtual ~VflParticleColorData();

	/**
	 * This method notifies the encapsulated color data
	 * of changes within the referenced sub-objects.
	 */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);
//	virtual void Notify(int iId);

	/**
	 * This method performs an update on the encapsulated color data
	 * if necessary.
	 */
	virtual void Update();

	/**
	 * This enumeration defines the parameters for change notification
	 * callbacks from the lookup table and the particle data objects.
	 */
	enum NOTIFICATION_ID
	{
		NI_LUT_CHANGE,
		NI_PARTICLEDATA_CHANGE,
        NI_SCALAR_ARRAY_CHANGE
	};

	struct SVflParticleColor
	{
		float aColor[4];
	};
	
	typedef std::vector<SVflParticleColor> TRAJECTORY_COLORS;
	typedef std::vector<TRAJECTORY_COLORS> POPULATION_COLORS;

	/**
	 * Acquire a reference to the color arrays. These are declared constant,
	 * because they are supposed to be read only. If necessary, an update
	 * on the data is performed before the reference is returned.
	 */
	const POPULATION_COLORS &GetColors() 
	{ 
		Update(); 
		return m_vecColors; 
	};

protected:
	VveParticlePopulationItem *m_pParticleData;
	VflVtkLookupTable *m_pLUT;

    VflVisParticles::VflVisParticlesProperties *m_pParams;

	bool m_bResizeNecessary;		// do we have to resize the data arrays?
	bool m_bColorUpdateNecessary;	// do we have to re-evaluate the color values?

	POPULATION_COLORS m_vecColors;
	
	VflParticleColorData();
private:
					
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLPARTICLECOLORDATA_H)
