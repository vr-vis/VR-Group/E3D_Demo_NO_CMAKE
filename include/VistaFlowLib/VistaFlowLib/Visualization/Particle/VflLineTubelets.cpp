/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflLineTubelets.h"
#include "VflVisParticles.h"
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include "../VflVisTiming.h"

#include "VflParticleColorData.h"

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>
#include <VistaOGLExt/VistaGLLine.h>
#include <VistaOGLExt/VistaGLSLShader.h>
#include <VistaOGLExt/VistaShaderRegistry.h>
#include <VistaBase/VistaTimeUtils.h>
#include <VistaBase/VistaTimer.h>
#include <assert.h>


using namespace std;
/*============================================================================*/
/*  STATIC Variables                                                          */
/*============================================================================*/
namespace
{
	const string g_strDefault_VS          = "VflLineTubelets_vert.glsl";
	const string g_strDefault_GS          = "VistaGLLine_Default_geom.glsl";
	const string g_strTubelets_FS         = "VistaGLLine_Tubelets_frag.glsl";
	const string g_strCylinder_GS         = "VistaGLLine_Cylinder_geom.glsl";
	const string g_strCylinder_FS         = "VistaGLLine_Cylinder_frag.glsl";
	const string g_strSimpleLighting_Ext  = "VflLineTubelets_SimpleLighting_frag.glsl";
	const string g_strDiffuseLighting_Ext = "VflLineTubelets_DiffuseLighting_frag.glsl";
	const string g_strPhongLighting_Ext   = "VflLineTubelets_PhongLighting_frag.glsl";
};
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

#ifdef WIN32
#define hypot(x, y) _hypot(x, y)
#endif

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

VflLineTubelets::VflLineTubelets(VflVisParticles *pOwner,
										 EDrawMode eStyle)
	:	VflParticleRenderer(pOwner)
	,	m_bInitialized(false)
	,	m_fTimeWindow(0)
	,	m_fMaxTimeWindow(0)
	,	m_iDrawMode(eStyle)
	,	m_iMaxParticles(-1)
	,	m_iParticleStep(1)
	,	m_iPathlineCount(0)
	,	m_pColorData(NULL)
	,	m_pParticles(NULL)
	,	m_nVboVertices(0)
	,	m_nPositionComponents(3)
	,	m_nColorComponents(3)
{
	m_pShader[0] = NULL;
	m_pShader[1] = NULL;
	m_pShader[2] = NULL;
	m_pShader[3] = NULL;
	m_pShader[4] = NULL;
	m_pShader[5] = NULL;

	m_aVbo = 0;
}

VflLineTubelets::~VflLineTubelets()
{
	this->Destroy();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

VistaGLSLShader* CreateShader( 
		const string& strVS_Name, 
		const string& strGS_Name, 
		const string& strFS_Name, 
		const string& strEXT_Name
	)
{
	VistaShaderRegistry* pShaderReg = &VistaShaderRegistry::GetInstance();

	string strVS  = pShaderReg->RetrieveShader( strVS_Name  );
	string strGS  = pShaderReg->RetrieveShader( strGS_Name  );
	string strFS  = pShaderReg->RetrieveShader( strFS_Name  );
	string strEXT = pShaderReg->RetrieveShader( strEXT_Name );

	if( strVS.empty() || strGS.empty() || strFS.empty() || strEXT.empty() )
	{
		vstr::errp() << "[VflLineTubelets] - required shader not found."  << endl;
		vstr::IndentObject oIndent;
		if(  strVS.empty() ) vstr::erri() << "Can't find " << strVS_Name  << endl;
		if(  strGS.empty() ) vstr::erri() << "Can't find " << strGS_Name  << endl;
		if(  strFS.empty() ) vstr::erri() << "Can't find " << strFS_Name  << endl;
		if( strEXT.empty() ) vstr::erri() << "Can't find " << strEXT_Name << endl;
		return NULL;
	}

	VistaGLSLShader* pShader = new VistaGLSLShader();

	pShader->InitVertexShaderFromString(   strVS  );
	pShader->InitGeometryShaderFromString( strGS  );
	pShader->InitFragmentShaderFromString( strFS  );
	pShader->InitFragmentShaderFromString( strEXT );

	if( !pShader->Link() ) { delete pShader; return NULL; }

	return pShader;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflLineTubelets::Init()
{
	if (m_bInitialized)
		return false;

	VveParticlePopulationItem *pData = m_pOwner->GetParticleData()->GetData();
	m_pColorData = new VflParticleColorData( pData,
                                             m_pOwner->GetLookupTable(), 
                                             m_pOwner->GetProperties());

	VistaShaderRegistry* pShaderReg = &VistaShaderRegistry::GetInstance();

	string strDefaultVS  = pShaderReg->RetrieveShader( g_strDefault_VS          );
	string strDefaultGS  = pShaderReg->RetrieveShader( g_strDefault_GS          );
	string strTubeletFS  = pShaderReg->RetrieveShader( g_strTubelets_FS         );
	string strCylinderGS = pShaderReg->RetrieveShader( g_strCylinder_GS         );
	string strCylinderFS = pShaderReg->RetrieveShader( g_strCylinder_FS         );
	string strSimpleEXT  = pShaderReg->RetrieveShader( g_strSimpleLighting_Ext  );
	string strDiffuseEXT = pShaderReg->RetrieveShader( g_strDiffuseLighting_Ext );
	string strPhongEXT   = pShaderReg->RetrieveShader( g_strPhongLighting_Ext   );

	if( strDefaultVS.empty()  || strDefaultGS.empty()  || 
		strTubeletFS.empty()  || strCylinderGS.empty() || 
		strCylinderFS.empty() || strSimpleEXT.empty()  || 
		strDiffuseEXT.empty() || strPhongEXT.empty()   )
	{
		vstr::errp() << "[VflLineTubelets] - required shader not found."  << endl;
		vstr::IndentObject oIndent;
		if(  strDefaultVS.empty() ) vstr::erri() << "Can't find " << g_strDefault_VS          << endl;
		if(  strDefaultGS.empty() ) vstr::erri() << "Can't find " << g_strDefault_GS          << endl;
		if(  strTubeletFS.empty() ) vstr::erri() << "Can't find " << g_strTubelets_FS         << endl;
		if( strCylinderGS.empty() ) vstr::erri() << "Can't find " << g_strCylinder_GS         << endl;
		if( strCylinderFS.empty() ) vstr::erri() << "Can't find " << g_strCylinder_FS         << endl;
		if(  strSimpleEXT.empty() ) vstr::erri() << "Can't find " << g_strSimpleLighting_Ext  << endl;
		if( strDiffuseEXT.empty() ) vstr::erri() << "Can't find " << g_strDiffuseLighting_Ext << endl;
		if(   strPhongEXT.empty() ) vstr::erri() << "Can't find " << g_strPhongLighting_Ext   << endl;
		return false;
	}

	m_pShader[0] = new VistaGLSLShader();
	m_pShader[0]->InitFromStrings( strDefaultVS, strDefaultGS, strTubeletFS );
	m_pShader[0]->InitFragmentShaderFromString( strSimpleEXT );
	m_pShader[0]->Link();

	m_pShader[1] = new VistaGLSLShader();
	m_pShader[1]->InitFromStrings( strDefaultVS, strDefaultGS, strTubeletFS );
	m_pShader[1]->InitFragmentShaderFromString( strDiffuseEXT );
	m_pShader[1]->Link();

	m_pShader[2] = new VistaGLSLShader();
	m_pShader[2]->InitFromStrings( strDefaultVS, strDefaultGS, strTubeletFS );
	m_pShader[2]->InitFragmentShaderFromString( strPhongEXT );
	m_pShader[2]->Link();

	m_pShader[3] = new VistaGLSLShader();
	m_pShader[3]->InitFromStrings( strDefaultVS, strCylinderGS, strCylinderFS );
	m_pShader[3]->InitFragmentShaderFromString( strSimpleEXT );
	m_pShader[3]->Link();

	m_pShader[4] = new VistaGLSLShader();
	m_pShader[4]->InitFromStrings( strDefaultVS, strCylinderGS, strCylinderFS );
	m_pShader[4]->InitFragmentShaderFromString( strDiffuseEXT );
	m_pShader[4]->Link();

	m_pShader[5] = new VistaGLSLShader();
	m_pShader[5]->InitFromStrings( strDefaultVS, strCylinderGS, strCylinderFS );
	m_pShader[5]->InitFragmentShaderFromString( strPhongEXT );
	m_pShader[5]->Link();

	m_iShaderHandle[0] = VistaGLLine::AddLineShader(m_pShader[0]);
	m_iShaderHandle[1] = VistaGLLine::AddLineShader(m_pShader[1]);
	m_iShaderHandle[2] = VistaGLLine::AddLineShader(m_pShader[2]);
	m_iShaderHandle[3] = VistaGLLine::AddLineShader(m_pShader[3]);
	m_iShaderHandle[4] = VistaGLLine::AddLineShader(m_pShader[4]);
	m_iShaderHandle[5] = VistaGLLine::AddLineShader(m_pShader[5]);

	m_bInitialized = true;
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Destroy                                                     */
/*                                                                            */
/*============================================================================*/
void VflLineTubelets::Destroy()
{
	delete m_pColorData;
	m_pColorData = NULL;

	VistaGLLine::RemoveLineShader(m_pShader[0]);
	VistaGLLine::RemoveLineShader(m_pShader[1]);
	VistaGLLine::RemoveLineShader(m_pShader[2]);
	VistaGLLine::RemoveLineShader(m_pShader[3]);
	VistaGLLine::RemoveLineShader(m_pShader[4]);
	VistaGLLine::RemoveLineShader(m_pShader[5]);

	delete m_pShader[0];
	delete m_pShader[1];
	delete m_pShader[2];
	delete m_pShader[3];
	delete m_pShader[4];
	delete m_pShader[5];

	m_vecTubeletVertexCounts.clear();
	m_vecTrajectoryIndex.clear();
	m_vecPathlineStarts.clear();
	m_vecPathlineCounts.clear();

	glDeleteBuffers(1, &m_aVbo);

	m_aVbo = 0;

	m_iPathlineCount = 0;
	m_iParticleStep = 0;

	m_bInitialized = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflLineTubelets::DrawOpaque()
{
	if( m_iPathlineCount == 0)
		return;

	glPushAttrib( GL_ENABLE_BIT | GL_POINT_BIT | GL_LINE_BIT );

	glDisable(GL_LIGHTING);
	glDisable(GL_BLEND);
	glEnable(GL_CULL_FACE);

	glBindBuffer(GL_ARRAY_BUFFER, m_aVbo);

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_COLOR_ARRAY );
	glEnableVertexAttribArray(1);

	glVertexPointer( 3, GL_FLOAT, m_nPositionComponents * sizeof(float), BUFFER_OFFSET(0) );
	glColorPointer(  3, GL_FLOAT, m_nColorComponents * sizeof(float),    BUFFER_OFFSET( m_nVboVertices*m_nPositionComponents*sizeof(float)) );
	
	// Use time as additional vertex attribute
	glVertexAttribPointer( 1, 1, GL_FLOAT, GL_FALSE, 0,  BUFFER_OFFSET( m_nVboVertices*(m_nPositionComponents+m_nColorComponents)*sizeof(float)) );

	if( m_iDrawMode >= DM_LINES_TUBLETS_SINPLE &&
		m_iDrawMode <= DM_LINES_CYLINDER_DIFFUSE_SPECULAR )
	{
		int iIndex = m_iDrawMode-DM_LINES_TUBLETS_SINPLE;
		VistaGLLine::Enable( m_iShaderHandle[iIndex] );

		int iUniLoc = m_pShader[iIndex]->GetUniformLocation("fStartTime");
		if(-1 != iUniLoc)
			glUniform1f(iUniLoc, m_fCurrentTime);

		iUniLoc = m_pShader[iIndex]->GetUniformLocation("fDeltaTime");
		if(-1 != iUniLoc)
			glUniform1f(iUniLoc, m_fDeltaLight);
	}

	glPointSize(5.0f);
	glLineWidth(2.0f);

	for( int i=0; i<m_iPathlineCount; i++)
	{
		//a line strip must consist of more than one Point
		if( m_vecPathlineCounts[i] > 1 )
		{
			glDrawArrays( GL_LINE_STRIP, 
				m_vecPathlineStarts[i], 
				m_vecPathlineCounts[i]);
		}
	} 

	if( m_iDrawMode >= DM_LINES_TUBLETS_SINPLE &&
		m_iDrawMode <= DM_LINES_CYLINDER_DIFFUSE_SPECULAR )
	{
		VistaGLLine::Disable();
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	glDisableVertexAttribArray(1);

	glEnable(GL_BLEND);
	glEnable(GL_LIGHTING);

	glPopAttrib();
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflLineTubelets::Update()
{
	if (!m_pOwner->GetVisible())
		return;

	// make sure our data is up-to-date...
	if (m_dVisParamsTimeStamp < m_pOwner->GetVisParamsChangeTimeStamp())
		UpdateVisParams();
	if (m_dDataTimeStamp < m_pOwner->GetDataChangeTimeStamp())
	{
		UpdateParticleData();
	}

	if( m_iPathlineCount == 0 )
		//nothing to render
		return;

	m_pOwner->GetParticleData()->GetData()->LockData();
	m_pParticles = m_pOwner->GetParticleData()->GetData()->GetData();
	assert(m_pParticles);

	if( m_pParticles->GetNumTrajectories() == 0 )
	{
		CleanUp();
		m_pOwner->GetParticleData()->GetData()->UnlockData();
		return;
	}

	m_pParams = dynamic_cast<VflVisParticles::VflVisParticlesProperties *>(m_pOwner->GetProperties());
	assert(m_pParams);

	float fTubeletRadius = m_pParams->GetRadiusAbsolute();
	VistaGLLine::SetLineWidth(fTubeletRadius);

	float fCurrentTime = static_cast<float>
		(m_pOwner->GetTimeMapper()->GetSimulationTime(
		m_pOwner->GetRenderNode()->GetVisTiming()->GetVisualizationTime()));

	float fMinTime = fCurrentTime - m_fTimeWindow;
	if( fMinTime < 0.0f )
		fMinTime = 0.0f;

	size_t nCurrentIndex, nMinIndex;

	VveParticleTrajectory* pTraj = NULL;
	VveParticleDataArray<float>* pTimeArray = NULL;

	int iPosition = 0;
	int iTrajectoryOffset = 0;

	int iCurrentOffset = 0;
	for (int i=0; i<m_iPathlineCount; ++i)
	{
		// update trajectory offset
		iTrajectoryOffset += iCurrentOffset;
		iCurrentOffset = m_vecTubeletVertexCounts[i];

		// check time bounds of current trajectory
		if ((fCurrentTime < m_vecTrajectoryIndex[i].fMinTime) ||
			(fMinTime > m_vecTrajectoryIndex[i].fMaxTime))
		{
			// nothing to render here..
			m_vecPathlineStarts[i] = m_vecPathlineCounts[i] = 0;
			continue;
		}

		// get arrays from current trajectory
		m_pParticles->GetTrajectory(i, pTraj);
		pTraj->GetTypedArray( m_vecTrajectoryIndex[i].nTimeArrayIndex, pTimeArray);

		// calculate start and end index of trajectory according to time range
		if( CalculateRange(i, pTimeArray, fCurrentTime, fMinTime, nCurrentIndex, nMinIndex))
		{
			// store range: [ off+min, off+curr ]
			m_vecPathlineStarts[i] = iTrajectoryOffset + static_cast<int>(nMinIndex);
			m_vecPathlineCounts[i] = static_cast<int>(nCurrentIndex - nMinIndex +1);
		}
		else
		{
			// nothing to render here..
			m_vecPathlineStarts[i] = m_vecPathlineCounts[i] = 0;
		}
	}

	m_fCurrentTime = fCurrentTime;
	m_fDeltaLight  = (m_pParams->GetDecreaseLuminanceWithTime() ? 1.0f/m_fTimeWindow : 0);

	m_pOwner->GetParticleData()->GetData()->UnlockData();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateParticleData                                          */
/*                                                                            */
/*============================================================================*/
void VflLineTubelets::UpdateParticleData()
{
	m_pOwner->GetParticleData()->GetData()->LockData();
	m_pParticles = m_pOwner->GetParticleData()->GetData()->GetData();

	// find out, how much memory we need for particle storage
	m_iPathlineCount = m_pParticles->GetNumTrajectories();
	if( m_iPathlineCount == 0)
	{
		m_pOwner->GetParticleData()->GetData()->UnlockData();
		return;
	}

	// update pathline count
	int iLastPathline = 0;
	for( int i=0; i<m_iPathlineCount; i++)
		if( m_pParticles->GetTrajectory(i)->GetSize() > 0 )
			iLastPathline = i+1;
	m_iPathlineCount = iLastPathline;

	m_vecTubeletVertexCounts.resize(m_iPathlineCount);
	m_vecTrajectoryIndex.resize(m_iPathlineCount);
	m_vecPathlineStarts.resize(m_iPathlineCount);
	m_vecPathlineCounts.resize(m_iPathlineCount);

	m_pParams = dynamic_cast<VflVisParticles::VflVisParticlesProperties *>(m_pOwner->GetProperties());

	VveParticleTrajectory* pTraj = NULL;
	VveParticleDataArray<float> *pPosArray = NULL;
	VveParticleDataArray<float> *pTimeArray = NULL;

	// Calculate total number of vertices to store
	int nVboVertices = 0;
	for( int i=0; i<m_iPathlineCount; i++)
	{
		pTraj = m_pParticles->GetTrajectory(i);
		assert( pTraj && "requested trajectory does not exist!" );

		m_vecTubeletVertexCounts[i] = static_cast<int>(pTraj->GetSize());
		nVboVertices += m_vecTubeletVertexCounts[i];

		// Store indices of particle data arrays for faster access (avoid strcmp)
		pTraj->GetArrayIndexByName(m_pParams->GetPositionArray(), m_vecTrajectoryIndex[i].nPositionsArrayIndex );
		pTraj->GetArrayIndexByName(m_pParams->GetTimeArray(),     m_vecTrajectoryIndex[i].nTimeArrayIndex );
		pTraj->GetArrayIndexByName(m_pParams->GetScalarArray(),   m_vecTrajectoryIndex[i].nScalarArrayIndex );

		// Estimate number of components per position by first position in first trajectory
		if( i == 0)
		{
			pTraj->GetTypedArray(m_vecTrajectoryIndex[i].nPositionsArrayIndex, pPosArray);
			assert( pPosArray && "no array named like m_pParams->GetPositionArray() present!" );
			m_nPositionComponents = static_cast<int>(pPosArray->GetNumComponents());
		}
	}

	// Allocate memory on device, VBO structure: PPPP(x)_CCCC(y)_TTTT(1)
	if( !m_aVbo )
		glGenBuffers(1, &m_aVbo);

	m_nColorComponents = 3;
	int iMemPerVertex = m_nPositionComponents + m_nColorComponents + 1;
	
	glBindBuffer(GL_ARRAY_BUFFER, m_aVbo);
	glBufferData(GL_ARRAY_BUFFER, nVboVertices*(iMemPerVertex)*sizeof(float), NULL, GL_STATIC_DRAW);

	//float* bufferData;
	//bufferData = (float*)malloc(nVboVertices* iMemPerVertex * sizeof(float));

	int bufferSizeInFloats = nVboVertices* iMemPerVertex;
	int bufferSizeInByte = bufferSizeInFloats * sizeof(float);

	int iPositionOffset = 0;
	int iColorOffset = nVboVertices*m_nPositionComponents;
	int iTimeOffset =  nVboVertices*(m_nPositionComponents+m_nColorComponents);

	const VflParticleColorData::POPULATION_COLORS &refColors = m_pColorData->GetColors();

	for(int i = 0; i < m_iPathlineCount; i++)
	{
		int nSize = m_vecTubeletVertexCounts[i];
		if( nSize == 0)
			continue;

		m_pParticles->GetTrajectory(i, pTraj);

		pTraj->GetTypedArray( m_vecTrajectoryIndex[i].nPositionsArrayIndex, pPosArray);

		float* posData = pPosArray->GetData();
		int posArraySize = pPosArray->GetSize() * pPosArray->GetNumComponents(); // in float-count

		pTraj->GetTypedArray( m_vecTrajectoryIndex[i].nTimeArrayIndex, pTimeArray);
		assert( pTimeArray && "no array named like m_pParams->GetTimeArray() present!" );

		// write time data to VBO
		float* timeData = pTimeArray->GetData();
		int timeArraySize = pTimeArray->GetSize() * pTimeArray->GetNumComponents(); // in float-count

		//std::copy(&posData[0], &posData[posArraySize], &bufferData[iPositionOffset]);
		//std::copy(&timeData[0], &timeData[timeArraySize], &bufferData[iTimeOffset]);
		glBufferSubData(GL_ARRAY_BUFFER, iPositionOffset*sizeof(float), posArraySize*sizeof(float), posData);
		glBufferSubData(GL_ARRAY_BUFFER, iTimeOffset * sizeof(float), timeArraySize*sizeof(float), timeData);


		iPositionOffset += posArraySize;
		iTimeOffset += timeArraySize;

		m_vecTrajectoryIndex[i].nCurrentIndex = 0;
		pTimeArray->GetBounds( &m_vecTrajectoryIndex[i].fMinTime, &m_vecTrajectoryIndex[i].fMaxTime);


		for (int j = 0; j < nSize; j++)
		{
			//std::copy(&(refColors[i][j].aColor[0]), &(refColors[i][j].aColor[m_nColorComponents]), &bufferData[iColorOffset]);
			glBufferSubData(GL_ARRAY_BUFFER, iColorOffset * sizeof(float), m_nColorComponents * sizeof(float), refColors[i][j].aColor);
			iColorOffset += m_nColorComponents;
		}

	}

	//glBufferData(GL_ARRAY_BUFFER, nVboVertices*(iMemPerVertex)*sizeof(float), &bufferData[0], GL_STATIC_DRAW);

	//free(bufferData);
	

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	m_nVboVertices = nVboVertices;

	m_dDataTimeStamp = m_pOwner->GetDataChangeTimeStamp();
	m_pOwner->GetParticleData()->GetData()->UnlockData();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateVisParams                                             */
/*                                                                            */
/*============================================================================*/
void VflLineTubelets::UpdateVisParams()
{
	m_pParams = dynamic_cast<VflVisParticles::VflVisParticlesProperties *>(m_pOwner->GetProperties());

	if(!m_pParams)
		return;

	if (m_iMaxParticles != m_pParams->GetMaxCount())
	{
		m_iMaxParticles = m_pParams->GetMaxCount();
		m_dDataTimeStamp = -1;
	}

	m_fTimeWindow = m_pParams->GetTimeWindow();
	if( m_fTimeWindow < 0)
		m_fTimeWindow = 0;

	m_dVisParamsTimeStamp = m_pOwner->GetVisParamsChangeTimeStamp();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflLineTubelets::GetType() const
{
	return VflParticleRenderer::GetType() + std::string("::VflLineTubelets");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDrawMode(AsString)                                   */
/*                                                                            */
/*============================================================================*/
void VflLineTubelets::SetDrawMode(int iMode)
{
	if (iMode >= 0 && iMode <= DM_COUNT)
		m_iDrawMode = iMode;
}

void VflLineTubelets::SetDrawMode(const std::string &strMode)
{
	std::string strLocalMode = VistaAspectsConversionStuff::ConvertToUpper(strMode);

	if (strLocalMode == "TUBLETS_SINPLE")
		m_iDrawMode = DM_LINES_TUBLETS_SINPLE;
	else if (strLocalMode == "TUBLETS_DIFFUSE")
		m_iDrawMode = DM_LINES_TUBLETS_DIFFUSE;
	else if (strLocalMode == "TUBLETS_DIFFUSE_SPECULAR")
		m_iDrawMode = DM_LINES_TUBLETS_DIFFUSE_SPECULAR;
	else if (strLocalMode == "CYLINDER_SINPLE")
		m_iDrawMode = DM_LINES_CYLINDER_SINPLE;
	else if (strLocalMode == "CYLINDER_DIFFUSE")
		m_iDrawMode = DM_LINES_CYLINDER_DIFFUSE;
	else if (strLocalMode == "TUBLETS_DIFFUSE_SPECULAR")
		m_iDrawMode = DM_LINES_CYLINDER_DIFFUSE_SPECULAR;
	else if (strLocalMode == "RAW")
		m_iDrawMode = DM_LINES_RAW;
}

int VflLineTubelets::GetDrawMode() const
{
	return m_iDrawMode;
}

std::string VflLineTubelets::GetDrawModeAsString() const
{
	return GetDrawModeString(m_iDrawMode);
}

std::string VflLineTubelets::GetDrawModeString(int iMode) const
{
	std::string strResult = "NONE";

	switch (iMode)
	{
	case DM_LINES_TUBLETS_SINPLE:
		strResult = "TUBLETS_SINPLE";
		break;
	case DM_LINES_TUBLETS_DIFFUSE:
		strResult = "TUBLETS_DIFFUSE";
		break;
	case DM_LINES_TUBLETS_DIFFUSE_SPECULAR:
		strResult = "TUBLETS_DIFFUSE_SPECULAR";
		break;
	case DM_LINES_CYLINDER_SINPLE:
		strResult = "CYLINDER_SINPLE";
		break;
	case DM_LINES_CYLINDER_DIFFUSE:
		strResult = "CYLINDER_DIFFUSE";
		break;
	case DM_LINES_CYLINDER_DIFFUSE_SPECULAR:
		strResult = "CYLINDER_DIFFUSE_SPECULAR";
		break;
	case DM_LINES_RAW:
		strResult = "RAW";
		break;
	}
	return strResult;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDrawModeCount                                            */
/*                                                                            */
/*============================================================================*/
int VflLineTubelets::GetDrawModeCount() const
{
	return DM_COUNT;
}

/*============================================================================*/
/*                                                                            */
/*  NAME: CalculateRange                                                      */
/*                                                                            */
/*============================================================================*/

bool VflLineTubelets::CalculateRange( size_t nTrajectoryIdx, 
									 VveParticleDataArray<float> *pTimeArray, 
									 float fCurrentTime, 
									 float fMinTime, 
									 size_t &nCurrentIndex, 
									 size_t &nMinIndex )
{
	if( pTimeArray->GetSize() == 0)
		return false;

	// try to find beginning of pathline (i.e. the highest applicable time value)
	nCurrentIndex = m_vecTrajectoryIndex[nTrajectoryIdx].nCurrentIndex;

	if( *pTimeArray->GetElementAt(nCurrentIndex) > fCurrentTime)
		nCurrentIndex = 0;

	while( nCurrentIndex < pTimeArray->GetSize()-1 &&
		*pTimeArray->GetElementAt(nCurrentIndex) < fCurrentTime)
	{
		nCurrentIndex++;
	}

	if (nCurrentIndex > 0)
		m_vecTrajectoryIndex[nTrajectoryIdx].nCurrentIndex = nCurrentIndex-1;
	else
		m_vecTrajectoryIndex[nTrajectoryIdx].nCurrentIndex = 0;

	// now, we have the head of the particle track, so let's go for the tail
	nMinIndex = nCurrentIndex;
	while( nMinIndex > 0 &&
		*pTimeArray->GetElementAt(nMinIndex) > fMinTime )
	{
		nMinIndex--;
	}

	if (nMinIndex == nCurrentIndex)
	{
		// hm, no data here...
		return false;
	}
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME: CleanUp                                                        */
/*                                                                            */
/*============================================================================*/

void VflLineTubelets::CleanUp()
{
	m_iPathlineCount = 0;
	m_vecTrajectoryIndex.clear();
	m_vecTubeletVertexCounts.clear();
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflLineTubelets.cpp"                                      */
/*============================================================================*/
