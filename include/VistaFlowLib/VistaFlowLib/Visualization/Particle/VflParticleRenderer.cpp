/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflParticleRenderer.h"
#include "VflVisParticles.h"

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflParticleRenderer::VflParticleRenderer(VflVisParticles *pOwner)
:	m_pOwner(pOwner)
,	m_dDataTimeStamp(-1)
,	m_dVisParamsTimeStamp(-1)
{
}

VflParticleRenderer::~VflParticleRenderer()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflParticleRenderer::Init()
{
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Destroy                                                     */
/*                                                                            */
/*============================================================================*/
void VflParticleRenderer::Destroy()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Activate                                                    */
/*                                                                            */
/*============================================================================*/
void VflParticleRenderer::Activate()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Deactivate                                                  */
/*                                                                            */
/*============================================================================*/
void VflParticleRenderer::Deactivate()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflParticleRenderer::Update()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflParticleRenderer::DrawOpaque()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflParticleRenderer::DrawTransparent()
{
}

///*============================================================================*/
///*                                                                            */
///*  NAME      :   ProcessCommand                                              */
///*                                                                            */
///*============================================================================*/
//void VflParticleRenderer::ProcessCommand(VflVOCEvent *pEvent)
//{
//}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetName                                                 */
/*                                                                            */
/*============================================================================*/
void VflParticleRenderer::SetName(string strName)
{
	m_strName = strName;
}

std::string VflParticleRenderer::GetName() const
{
	return m_strName;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflParticleRenderer::GetType() const
{
	return string("VflParticleRenderer");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflParticleRenderer::Debug(std::ostream & out) const
{
	out << " [VflParticleRenderer] Type: " << GetType() << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDrawMode                                             */
/*                                                                            */
/*============================================================================*/
void VflParticleRenderer::SetDrawMode(int iMode)
{
}

void VflParticleRenderer::SetDrawMode(const std::string &strMode)
{
}

int VflParticleRenderer::GetDrawMode() const
{
	return DM_NONE;
}

std::string VflParticleRenderer::GetDrawModeAsString() const
{
	return GetDrawModeString(-1);
}

std::string VflParticleRenderer::GetDrawModeString(int iMode) const
{
	return "NONE";
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDrawModeCount                                            */
/*                                                                            */
/*============================================================================*/
int VflParticleRenderer::GetDrawModeCount() const
{
	return 0;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   operator<<                                                  */
/*                                                                            */
/*============================================================================*/
ostream & operator<< ( ostream & out, const VflParticleRenderer & object )
{
    object.Debug ( out );
    return out;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflParticleRenderer.cpp"                                     */
/*============================================================================*/
