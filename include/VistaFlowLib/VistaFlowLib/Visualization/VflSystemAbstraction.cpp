/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VflSystemAbstraction.h" 

#include <cassert>

using namespace std;

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
IVflSystemAbstraction *IVflSystemAbstraction::m_pSystemAbs = 0;

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
IVflSystemAbstraction::IVflSystemAbstraction()
{
}

IVflSystemAbstraction::~IVflSystemAbstraction()
{
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
IVflSystemAbstraction* IVflSystemAbstraction::GetSystemAbs()
{
	return m_pSystemAbs;
}

bool IVflSystemAbstraction::SetSystemAbs(IVflSystemAbstraction *pSysAbs)
{
	if(!pSysAbs)
		return false;

	// The system abstraction _must_ only be set once.
	assert(m_pSystemAbs == 0 && "A system abstraction has already been set!");

	m_pSystemAbs = pSysAbs;

	return true;
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "MYDEMO.CPP"                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/


