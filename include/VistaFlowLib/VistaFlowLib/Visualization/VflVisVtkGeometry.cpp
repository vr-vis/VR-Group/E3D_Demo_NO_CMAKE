/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <GL/glew.h>

#include <VistaMath/VistaVector.h>

#include "VflVisVtkGeometry.h"
#include "VflRenderNode.h"
#include "VflUnsteadyDataFilter.h"
#include "IVflTransformer.h"
#include "VflVisTiming.h"
#include <vtkRenderer.h>
#include <vtkViewport.h>
#include <vtkPolyDataMapper.h>
#include <vtkLookupTable.h>
#include <vtkProperty.h>
#include <vtkMatrix4x4.h>

#include <cassert>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflVisVtkGeometry::VflVisVtkGeometry()
: IVflVisObject(), m_pProperty(NULL), m_pBackfaceProperty(NULL), 
  m_pLookupTable(NULL), 
  m_pTexture(NULL), 
  m_bBoundsValid(false), 
  m_pData(NULL),
  m_bNeedRewire(true),
  m_dLastDrawSimTime(-1.0),
  m_dLastDrawTransSimTime(-1.0)
{
	m_v3BoundsMin = VistaVector3D(0, 0, 0);
	m_v3BoundsMax = VistaVector3D(0, 0, 0);

}

VflVisVtkGeometry::~VflVisVtkGeometry()
{
	m_oVtkObjectStack.DeleteObjects();

	if(m_pData)
	{
		// unregister as observer of level data...
		for (int i=0; i<m_pData->GetNumberOfLevels(); ++i)
		{
			VveVtkPolyDataItem *pLevelData = m_pData->GetTypedLevelDataByLevelIndex(i);
			ReleaseObserveable(pLevelData, i);
		}
	}
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflVisVtkGeometry::Init()
{
	if(IVflVisObject::Init())
	{
		return InitPipeline();
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflVisVtkGeometry::Update()
{
	if(!GetTransformer())
		return;

	int iIndex      = m_pData->GetTimeMapper()->GetLevelIndex(GetRenderNode()->GetVisTiming()->GetVisualizationTime());
	double dSimTime = m_pData->GetTimeMapper()->GetSimulationTime(GetRenderNode()->GetVisTiming()->GetVisualizationTime());

	// skip if time index not valid
	if (iIndex<0)
		return;

	if(m_bTransformerDirty || (m_dLastDrawSimTime != dSimTime))
	{
		VistaTransformMatrix m;
		if(GetTransformer()->GetUnsteadyTransform(dSimTime, m)==true)
		{
			// APPLY THIS!
			if(m_vecActors[iIndex]->GetUserMatrix() == NULL)
			{
				vtkMatrix4x4 *mt = vtkMatrix4x4::New();
				m_vecActors[iIndex]->SetUserMatrix(mt);
				m_oVtkObjectStack.PushObject(mt);
			}

			vtkMatrix4x4 *pM = m_vecActors[iIndex]->GetUserMatrix();

			for(register int r=0; r < 4; ++r)
			{
				for(register int c=0; c < 4; ++c)
				{
					(*pM)[r][c] = m[r][c];
				}
			}
			(*pM).Modified();
			m_bBoundsValid = false;
			GetRenderNode()->RecomputeVisBounds();
		}
		m_dLastDrawSimTime = dSimTime;
		m_bTransformerDirty = false;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflVisVtkGeometry::DrawOpaque()
{
	if (!GetVisible())
		return;

	// find out about time level to be displayed
	int iIndex = m_pData->GetTimeMapper()->GetLevelIndex(GetRenderNode()->GetVisTiming()->GetVisualizationTime());

	if (iIndex < 0)
		return;

	assert(iIndex<m_pData->GetNumberOfLevels() && "time index too big!");

	// don't forget to lock the data object
	VveVtkPolyDataItem *pLevelData = m_pData->GetTypedLevelDataByLevelIndex(iIndex);
	pLevelData->LockData();

	// do we need to re-wire our pipeline?
	if (m_bNeedRewire)
		RewireFilters();

	// check, whether our data is up-to-date
	if (m_vecDataChanged[iIndex])
	{
		// well, it has changed, so update the corresponding poly data mapper
		//vtkPolyData *pPolyData = pLevelData->GetData();
		vtkPolyData *pPolyData = this->GetFilteredPolyData( iIndex );

		if (!pPolyData)
		{
			// if the polydata is empty, just return (without resetting the changed flag!)
			// (but don't forget to unlock the data object!)
			pLevelData->UnlockData();
			return;
		}

		vtkPolyDataMapper *pMapper =
			(vtkPolyDataMapper *)(m_vecActors[iIndex]->GetMapper());
#if VTK_MAJOR_VERSION > 5
		pMapper->SetInputData(pPolyData);
#else
		pMapper->SetInput(pPolyData);
#endif

		m_vecDataChanged[iIndex] = false;
		m_vecBoundsChanged[iIndex] = true; // indicate individual bounds change

		// force re-computation of visualization boundaries
		m_bBoundsValid = false;
		GetRenderNode()->SetBoundsToModified();
	}

	glPushAttrib(GL_ALL_ATTRIB_BITS);
    m_vecActors[iIndex]->RenderOpaqueGeometry(
		GetRenderNode()->GetVtkRenderer());
	glPopAttrib();

	// unlock the underlying data object
	pLevelData->UnlockData();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawTransparent                                             */
/*                                                                            */
/*============================================================================*/
void VflVisVtkGeometry::DrawTransparent()
{
	if (!GetVisible())
		return;

	// find out about time level to be displayed
	// rely on time mapper of unsteady data for that ;-)
	// oh, did I mention, that we rely on the visualization controller as well???
	int iIndex = m_pData->GetTimeMapper()->GetLevelIndex(GetRenderNode()->GetVisTiming()->GetVisualizationTime());

	if (iIndex < 0)
		return;

	assert(iIndex<m_pData->GetNumberOfLevels() && "time index too big!");

	// don't forget to lock the data object
	VveVtkPolyDataItem *pLevelData = m_pData->GetTypedLevelDataByLevelIndex(iIndex);
	pLevelData->LockData();

	// do we need to re-wire our pipeline?
	if (m_bNeedRewire)
		RewireFilters();

	// check, whether our data is up-to-date
	if (m_vecDataChanged[iIndex])
	{
		// well, it has changed, so update the corresponding poly data mapper
		vtkPolyData *pPolyData = this->GetFilteredPolyData( iIndex );

		if (!pPolyData)
		{
			// if the polydata is empty, just return (without resetting the changed flag!)
			// (but don't forget to unlock the data object!)
			pLevelData->UnlockData();
			return;
		}

		vtkPolyDataMapper *pMapper = (vtkPolyDataMapper *)(m_vecActors[iIndex]->GetMapper());
#if VTK_MAJOR_VERSION > 5
		pMapper->SetInputData(pPolyData);
#else
		pMapper->SetInput(pPolyData);
#endif
		m_vecDataChanged[iIndex] = false;

		// force recomputation of visualization boundaries
		GetRenderNode()->SetBoundsToModified();
	}

	glPushAttrib(GL_ALL_ATTRIB_BITS);
#if VTK_MAJOR_VERSION > 5 || (VTK_MAJOR_VERSION == 5 && VTK_MINOR_VERSION >=1)
	m_vecActors[iIndex]->RenderTranslucentPolygonalGeometry(
		GetRenderNode()->GetVtkRenderer());
#else
	m_vecActors[iIndex]->RenderTranslucentGeometry(
		GetRenderNode()->GetVtkRenderer());
#endif
	glPopAttrib();

	// unlock the underlying data object
	pLevelData->UnlockData();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflVisVtkGeometry::GetRegistrationMode() const
{
	return OLI_UPDATE | OLI_DRAW_OPAQUE | OLI_DRAW_TRANSPARENT;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBounds                                                   */
/*                                                                            */
/*============================================================================*/
bool VflVisVtkGeometry::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	if (!m_bBoundsValid)
	{
		if (!ComputeBounds())
			return false;
	}

	minBounds = m_v3BoundsMin;
	maxBounds = m_v3BoundsMax;

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVisVtkGeometry::GetType() const
{
	return IVflVisObject::GetType()+string("::VflVisVtkGeometry");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Debug                                                       */
/*                                                                            */
/*============================================================================*/
void VflVisVtkGeometry::Debug(std::ostream & out) const
{
	IVflVisObject::Debug(out);
	out << " [VflVisVtkGeo] Number of time levels:  " << m_vecActors.size() << endl;
	out << " [VflVisVtkGeo] Bounds: ";
	if (m_bBoundsValid)
		out << m_v3BoundsMin << " - " << m_v3BoundsMax << endl;
	else
		out << "*invalid*" << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetProperty                                                 */
/*                                                                            */
/*============================================================================*/
bool VflVisVtkGeometry::SetProperty(vtkProperty *pProperty)
{
	if (m_vecActors.empty())
	{
		m_pProperty = pProperty;
		return true;
	}

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBackfaceProperty                                         */
/*                                                                            */
/*============================================================================*/
bool VflVisVtkGeometry::SetBackfaceProperty(vtkProperty *pProperty)
{
	if (m_vecActors.empty())
	{
		m_pBackfaceProperty = pProperty;
		return true;
	}

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetLookupTable                                              */
/*                                                                            */
/*============================================================================*/
bool VflVisVtkGeometry::SetLookupTable(vtkLookupTable *pLookupTable)
{
	if (!pLookupTable)
	{
		return false;
	}

	if (m_vecActors.empty())
	{
		m_pLookupTable = pLookupTable;		
	}
	else
	{
		m_pLookupTable = pLookupTable;
		for (size_t i=0; i<m_vecActors.size(); ++i)
		{			
			m_vecActors[i]->GetMapper()->SetLookupTable(m_pLookupTable);			
		}
	}	
	return true;	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetData                                                 */
/*                                                                            */
/*============================================================================*/
bool VflVisVtkGeometry::SetData(VveUnsteadyVtkPolyData *pData)
{
	if (m_vecActors.empty())
	{
		m_pData = pData;
		return true;
	}

	return false;
}

bool VflVisVtkGeometry::SetUnsteadyData(VveUnsteadyData *pD)
{
	IVflVisObject::SetUnsteadyData(pD);

	SetData(dynamic_cast<VveUnsteadyVtkPolyData*>(pD));
	return (m_pData != NULL);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetScalarVisibility                                         */
/*                                                                            */
/*============================================================================*/
void VflVisVtkGeometry::SetScalarVisibility(bool bScalarVisibility)
{
	if (m_vecActors.empty())
	{
		return;
	}

	vtkActor *pActor;
	for (ACVEC::size_type i=0; i<m_vecActors.size(); ++i)
	{
		pActor = m_vecActors[i];
		pActor->GetMapper()->SetScalarVisibility(bScalarVisibility);
	}
}

void VflVisVtkGeometry::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	switch(ticket)
	{
	case IVflVisObject::E_PROP_TICKET:
		{
			if(HandlePropertyChange(dynamic_cast<VflVisVtkGeometry::VflVtkGeometryProperties *>
				                    (GetProperties()), msg) == true)
				return;
			break;
		}
	case IVflVisObject::E_TRANSFORMER_TICKET:
		{
			m_bTransformerDirty = true;
			break;
		}
	case E_FILTER_TICKET:
		{
			// find filter
			if(HandleFilterChange(dynamic_cast<VflUnsteadyDataFilter*>(pObserveable), msg) == true)
				return;
			break;
		}
	case E_LEVELDATA_TICKET:
		{
			if(HandleLevelDataChange(dynamic_cast<VveVtkPolyDataItem*>(pObserveable), msg) == true)
				return;
			break;
		}
	default:
		break;
	}

	IVflVisObject::ObserverUpdate(pObserveable, msg, ticket);
}

bool VflVisVtkGeometry::HandlePropertyChange(VflVtkGeometryProperties *p, int msg)
{
	switch(msg)
	{
	case VflVisVtkGeometry::VflVtkGeometryProperties::MSG_SCALARVISIBILITY_CHANGE:
		{
			SetScalarVisibility(p->GetScalarVisibility());
			break;
		}
	case VflVisVtkGeometry::VflVtkGeometryProperties::MSG_IMMEDIATEMODE_CHANGE:
		{
			SetImmediateMode(p->GetImmediateMode());
			break;
		}
	default:
		break;
	}

	return false;
}

bool VflVisVtkGeometry::HandleLevelDataChange(VveVtkPolyDataItem *pCont, int msg)
{
	if(!pCont)
		return false;

	int n = 0;
	for(; n < m_pData->GetNumberOfLevels(); ++n)
	{
		if(m_pData->GetLevelDataByLevelIndex(n) == pCont)
			break;
	}

	if(n < m_pData->GetNumberOfLevels())
	{
			m_vecDataChanged[n] = true;
			m_vecBoundsChanged[n]= true;
			m_bBoundsValid = false;
			return true; // ok to stop here
	}
	return false;
}

bool VflVisVtkGeometry::HandleFilterChange(VflUnsteadyDataFilter *pFilter, int msg)
{
	if(msg == VflUnsteadyDataFilter::MSG_NEED_REWIRE)
	{
		m_bNeedRewire = true;
		return true; // ok to stop here
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetImmediateMode                                            */
/*                                                                            */
/*============================================================================*/
void VflVisVtkGeometry::SetImmediateMode(bool bImmediateMode)
{
	if (m_vecActors.empty())
	{
		return;
	}

	vtkActor *pActor;
	for (ACVEC::size_type i=0; i<m_vecActors.size(); ++i)
	{
		pActor = m_vecActors[i];
		pActor->GetMapper()->SetImmediateModeRendering(bImmediateMode);
	}
}

bool VflVisVtkGeometry::SetTexture(vtkTexture *pTexture)
{
	if(m_vecActors.empty())
	{
		m_pTexture = pTexture;
		return true;
	}

	for (ACVEC::size_type i=0; i<m_vecActors.size(); ++i)
	{
		m_vecActors[i]->SetTexture(pTexture);
	}
	return true;
}

vtkTexture *VflVisVtkGeometry::GetTexture() const
{
	if(m_vecActors.empty())
		return m_pTexture;
	return m_vecActors[0]->GetTexture();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   InitPipeline                                                */
/*                                                                            */
/*============================================================================*/
bool VflVisVtkGeometry::InitPipeline()
{
	if (!m_vecActors.empty())
	{
		vstr::warnp() << " [VflVisVtkGeo] Actors already initialized..." << endl;
		return false;
	}

	if (!m_pData)
	{
		vstr::errp() << " [VflVisVtkGeo] Unable to init pipeline! No VflUnsteadyData object given!" << endl;
		return false;
	}

	if (!m_pData->GetNumberOfLevels())
	{
		vstr::errp() << " [VflVisVtkGeo] Unable to init pipeline! Cannot find data..." << endl;
		return false;
	}

	// what we do here is create a vtkProperties object and a vtkLookupTable (if necessary)
	// and associate them with every single actor (and its mapper, respectively)
	// we don't create a special backface property object, though. vtk uses the same property
	// for frontface and backface rendering, if the backface property is not set - basically,
	// we do the same thing...
	if (!m_pProperty)
	{
        m_pProperty = vtkProperty::New();
		m_oVtkObjectStack.PushObject(m_pProperty);
	}

	if (!m_pLookupTable)
	{
		m_pLookupTable = vtkLookupTable::New();
		m_oVtkObjectStack.PushObject(m_pLookupTable);
	}

	// create actors and poly data mappers for the data objects 
	// and associate them with the poly data from the data object
	// in addition set the corresponding lookup table and property
	int i;
	vtkPolyDataMapper *pMapper;
	vtkActor *pActor;
	vtkPolyData *pPolyData;
	m_vecActors.resize(m_pData->GetNumberOfLevels());
	m_vecDataChanged.resize(m_pData->GetNumberOfLevels());
    m_vecBoundsChanged.resize(m_pData->GetNumberOfLevels());
	for (i=0; i<m_pData->GetNumberOfLevels(); ++i)
	{
		// create the pipeline
		pMapper = vtkPolyDataMapper::New();
		VflVtkGeometryProperties *p = dynamic_cast<VflVtkGeometryProperties*>(GetProperties());

		m_oVtkObjectStack.PushObject(pMapper);
		pMapper->SetLookupTable(m_pLookupTable);
		pMapper->SetScalarVisibility(p->GetScalarVisibility());
		if (p->GetScalarVisibility())
			pMapper->UseLookupTableScalarRangeOn();
		pMapper->SetImmediateModeRendering(p->GetImmediateMode());

		pActor = vtkActor::New();
		m_oVtkObjectStack.PushObject(pActor);
		pActor->SetMapper(pMapper);
		pActor->SetProperty(m_pProperty);
		if (m_pBackfaceProperty)
			pActor->SetBackfaceProperty(m_pBackfaceProperty);

		m_vecActors[i] = pActor;

		// now, retrieve the data and (if present) register it
		VveVtkPolyDataItem *pLevelData = m_pData->GetTypedLevelDataByLevelIndex(i);
		//pPolyData = pLevelData->GetData();
		pPolyData = this->GetFilteredPolyData( i );

		// while we're at it -> register self to be notified of data updates
		Observe(pLevelData, E_LEVELDATA_TICKET);

		if (pPolyData)
		{
#if VTK_MAJOR_VERSION > 5
			pMapper->SetInputData(pPolyData);
#else
			pMapper->SetInput(pPolyData);
#endif
			// well, we're still initializing, so it might be a good
			// idea to do any pending update now...
			pMapper->Update();

			m_vecDataChanged[i] = false;
            m_vecBoundsChanged[i] = true;
		}
		else
		{
			// we set the "changed" flag to true, to allow for checking
			// the presence of valid data
			m_vecDataChanged[i] = true;
            /* maybe the bounds changed but there is currently no inbound for 
            the mapper and this might be fatal if recomputation of bounds is attempted nonetheless */
            m_vecBoundsChanged[i] = false;
		}

		// we now have actors, so we can safely set pre-set textures

	}
	if(m_pTexture)
	{
		SetTexture(m_pTexture);
	}

	// don't forget the objects' boundaries
	ComputeBounds();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
bool VflVisVtkGeometry::ComputeBounds()
{
	bool bBoundsValid = false;
	double aBounds[6], aTemp[6];

	const int nNumVectors = static_cast<int>(m_vecActors.size());
	for(int i=0; i<nNumVectors; ++i)
	{
		assert(m_vecActors[i] && "ERROR - NULL pointer in actor vector...");

		// @todo: skip empty vtkPolyDatas
		// as GetBounds() does return non-sense
		// on empty vtkPolys
		if (true)
		{
			// lock data...
			m_pData->GetLevelDataByLevelIndex(i)->LockData();

			// bBoundsValid == true iff we already got
			// some values to aBounds (first-time-switch)
			if (bBoundsValid)
			{
				m_vecActors[i]->GetBounds(aTemp);
				if (aBounds[0] > aTemp[0])
					aBounds[0] = aTemp[0];
				if (aBounds[1] < aTemp[1])
					aBounds[1] = aTemp[1];
				if (aBounds[2] > aTemp[2])
					aBounds[2] = aTemp[2];
				if (aBounds[3] < aTemp[3])
					aBounds[3] = aTemp[3];
				if (aBounds[4] > aTemp[4])
					aBounds[4] = aTemp[4];
				if (aBounds[5] < aTemp[5])
					aBounds[5] = aTemp[5];
			}
			else
			{
				// initialize aBounds with bounds
				// from actor[i]
				m_vecActors[i]->GetBounds(aBounds);

				// switch first-time flag to true
				bBoundsValid = true;
			}

			// unlock data...
			m_pData->GetLevelDataByLevelIndex(int(i))->UnlockData();

            m_vecBoundsChanged[i] = false;
		}
	}

	if (bBoundsValid)
	{
		m_v3BoundsMin = VistaVector3D(aBounds[0], aBounds[2], aBounds[4]);
		m_v3BoundsMax = VistaVector3D(aBounds[1], aBounds[3], aBounds[5]);
		m_bBoundsValid = true;
		return true;
	}

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Notify                                                      */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddFilter                                                   */
/*                                                                            */
/*============================================================================*/
void VflVisVtkGeometry::AddFilter( VflUnsteadyDataFilter * pFilter )
{
	if( pFilter )
	{
		pFilter->CreateFilters(m_pData->GetNumberOfLevels());
		m_vecFilters.push_back(pFilter);
		m_bNeedRewire = true;

		Observe(pFilter, E_FILTER_TICKET);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemoveFilter                                                */
/*                                                                            */
/*============================================================================*/
void VflVisVtkGeometry::RemoveFilter(FLTIDX iIndex)
{
	if (iIndex<0 || iIndex>=m_vecFilters.size())
		return;

	VflUnsteadyDataFilter *pFilter = m_vecFilters[iIndex];
	ReleaseObserveable(pFilter, E_FILTER_TICKET);

	m_vecFilters.erase(m_vecFilters.begin()+iIndex);

	m_bNeedRewire = true;

}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilterIndex                                              */
/*                                                                            */
/*============================================================================*/
VflVisVtkGeometry::FLTIDX VflVisVtkGeometry::GetFilterIndex(VflUnsteadyDataFilter *pFilter) const
{
	if (!pFilter)
		return -1;

	for (FLTIDX i=0; i < m_vecFilters.size(); ++i)
	{
		if (m_vecFilters[i] == pFilter)
			return i;
	}

	return FLTIDX(-1);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilter                                                   */
/*                                                                            */
/*============================================================================*/
VflUnsteadyDataFilter *VflVisVtkGeometry::GetFilter(FLTIDX iIndex) const
{
	if (0<=iIndex && iIndex<m_vecFilters.size())
		return m_vecFilters[iIndex];

	return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilterCount                                              */
/*                                                                            */
/*============================================================================*/
VflVisVtkGeometry::FLTIDX VflVisVtkGeometry::GetFilterCount() const
{
	return m_vecFilters.size();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RewireFilters                                               */
/*                                                                            */
/*============================================================================*/
void VflVisVtkGeometry::RewireFilters()
{
	if (!m_pData)
		return;

	vector<vtkPolyData *> vecPolyData;
	vecPolyData.resize(m_pData->GetNumberOfLevels());

	for (int i=0; i<m_pData->GetNumberOfLevels(); ++i)
	{
		vecPolyData[i]= m_pData->GetTypedLevelDataByLevelIndex(i)->GetData();
		m_vecDataChanged[i] = true;
	}

	for (FLTIDX j=0; j<m_vecFilters.size(); ++j)
	{
		VflUnsteadyDataFilter *pFilter = m_vecFilters[j];

		// include only active filters
		if (pFilter->GetActive())
		{
			const int nNumPolyDatas = static_cast<int>(vecPolyData.size());
			for (int i=0; i<nNumPolyDatas; ++i)
			{
				pFilter->SetInput(i, vecPolyData[i]);
				vecPolyData[i] = pFilter->GetOutput(i);
			}
		}
	}

	m_bNeedRewire = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilteredPolyData                                         */
/*                                                                            */
/*============================================================================*/
vtkPolyData * VflVisVtkGeometry::GetFilteredPolyData( int iIndex )
{
	vtkPolyData * pUnfilteredData = m_pData->GetTypedLevelDataByLevelIndex(iIndex)->GetData();
	if( pUnfilteredData )
	{
		if( m_vecFilters.empty() )
		{
			return pUnfilteredData;
		}
		else
		{
			if (pUnfilteredData != m_vecFilters[0]->GetInput(iIndex))
			{
				// whoops - data input has changed. Double buffered polydata
				// or something like that?
				// anyway - we have to re-wire
				RewireFilters();
			}

			return m_vecFilters[m_vecFilters.size()-1]->GetOutput( iIndex );
		}
	}
	else
		return NULL;
}

VflVisVtkGeometry::VflVtkGeometryProperties *VflVisVtkGeometry::GetProperties() const
{
    return dynamic_cast<VflVtkGeometryProperties*>(IVflVisObject::GetProperties());
}

std::string   VflVisVtkGeometry::GetFactoryType()
{
	return std::string("VTK_GEOMETRY");
}

IVflVisObject::VflVisObjProperties    *VflVisVtkGeometry::CreateProperties() const
{
	return new VflVtkGeometryProperties;
}

// ############################################################################


static const string SsReflectionType("VflVisVtkGeometry");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, VflVisVtkGeometry::VflVtkGeometryProperties, VistaProperty::PROPT_BOOL>
	("SCALAR_VISIBILITY", SsReflectionType,
	&VflVisVtkGeometry::VflVtkGeometryProperties::GetScalarVisibility),
	new TVistaPropertyGet<bool, VflVisVtkGeometry::VflVtkGeometryProperties, VistaProperty::PROPT_BOOL>
	("IMMEDIATE_MODE", SsReflectionType,
	&VflVisVtkGeometry::VflVtkGeometryProperties::GetImmediateMode),
	new TVistaPropertyGet<std::string, VflVisVtkGeometry::VflVtkGeometryProperties, VistaProperty::PROPT_STRING>
	("TRANSFERMODE_STRING", SsReflectionType,
	&VflVisVtkGeometry::VflVtkGeometryProperties::GetTransferMode),
	new TVistaPropertyGet<VflVisVtkGeometry::ETransferMode, 
	                      VflVisVtkGeometry::VflVtkGeometryProperties, 
						  VistaProperty::PROPT_DOUBLE>
	("TRANSFERMODE", SsReflectionType,
	&VflVisVtkGeometry::VflVtkGeometryProperties::GetTransferModeId),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool, bool,
	VflVisVtkGeometry::VflVtkGeometryProperties>
	("SCALAR_VISIBILITY", SsReflectionType,
	&VflVisVtkGeometry::VflVtkGeometryProperties::SetScalarVisibility),
	new TVistaPropertySet<bool, bool,
	VflVisVtkGeometry::VflVtkGeometryProperties>
	("IMMEDIATE_MODE", SsReflectionType,
	&VflVisVtkGeometry::VflVtkGeometryProperties::SetImmediateMode),
	new TVistaPropertySet<const std::string &, std::string,
	VflVisVtkGeometry::VflVtkGeometryProperties>
	("TRANSFERMODE_STRING", SsReflectionType,
	&VflVisVtkGeometry::VflVtkGeometryProperties::SetTransferMode),
	NULL
};

VflVisVtkGeometry::VflVtkGeometryProperties::VflVtkGeometryProperties()
: IVflVisObject::VflVisObjProperties(),
  m_bScalarVisibility(true),
  m_bImmediateMode(false),
  m_iTransfermode(TM_DISPLAY_LIST)
{
}

VflVisVtkGeometry::VflVtkGeometryProperties::~VflVtkGeometryProperties()
{
}
		


bool VflVisVtkGeometry::VflVtkGeometryProperties::GetScalarVisibility() const
{
	return m_bScalarVisibility;
}

bool VflVisVtkGeometry::VflVtkGeometryProperties::SetScalarVisibility(bool bScalarVisibility)
{
	if(compAndAssignFunc<bool>(bScalarVisibility, m_bScalarVisibility))
	{
		Notify(MSG_SCALARVISIBILITY_CHANGE);
		return true;
	}
	return false;
}


bool VflVisVtkGeometry::VflVtkGeometryProperties::GetImmediateMode() const
{
	return m_bImmediateMode;
}


bool VflVisVtkGeometry::VflVtkGeometryProperties::SetImmediateMode(bool bImmediateMode)
{
	if(compAndAssignFunc<bool>(bImmediateMode, m_bImmediateMode) == 1)
	{
		Notify(MSG_IMMEDIATEMODE_CHANGE);
		return true;
	}
	return false;
}


VflVisVtkGeometry::ETransferMode  VflVisVtkGeometry::VflVtkGeometryProperties::GetTransferModeId() const
{
	return m_iTransfermode;
}

bool VflVisVtkGeometry::VflVtkGeometryProperties::SetTransferModeId(ETransferMode nTransferMode)
{
	if(compAndAssignFunc<ETransferMode>(nTransferMode, m_iTransfermode) == 1)
	{
		Notify(MSG_TRANSFERMODE_CHANGE);
		return true;
	}
	return false;
}


std::string VflVisVtkGeometry::VflVtkGeometryProperties::GetTransferMode() const
{
	switch(m_iTransfermode)
	{
	case TM_NONE:
		{
			return "NONE";
		}
	case TM_IMMEDIATE_MODE:
		{
			return "IMMEDIATE_MODE";
		}
	case TM_DISPLAY_LIST:
		{
			return "DISPLAY_LIST";
		}
	case TM_VERTEX_ARRAY:
		{
			return "VERTEX_ARRAY";
		}
	case TM_VERTEX_BUFFER_OBJECT:
		{
			return "VERTEX_BUFFER_OBJECT";
		}
	case TM_LAST_MODE:
	default:
		{
			break;
		}
	}
	return "<none>";
}

bool VflVisVtkGeometry::VflVtkGeometryProperties::SetTransferMode(const std::string &sTransferMode)
{
	if(sTransferMode == "DISPLAYLIST")
	{
		return SetTransferModeId(TM_DISPLAY_LIST);
	}
	else if(sTransferMode == "IMMEDIATE_MODE")
	{
		return SetTransferModeId(TM_IMMEDIATE_MODE);
	}
	else if(sTransferMode == "VERTEX_BUFFER_OBJECT")
	{
		return SetTransferModeId(TM_VERTEX_BUFFER_OBJECT);
	}
	else if(sTransferMode == "VERTEX_ARRAY")
	{
		return SetTransferModeId(TM_VERTEX_ARRAY);
	}
	return false;
}


std::string VflVisVtkGeometry::VflVtkGeometryProperties::GetReflectionableType() const
{
	return SsReflectionType;
}



int VflVisVtkGeometry::VflVtkGeometryProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = VflVisObjProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VisObject.cpp"                                               */
/*============================================================================*/
