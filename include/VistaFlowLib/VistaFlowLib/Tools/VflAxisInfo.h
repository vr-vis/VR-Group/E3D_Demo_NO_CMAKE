/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLAXISINFO_H
#define _VFLAXISINFO_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include "../Visualization/VflRenderable.h"

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisController;
class VflPropertyLoader;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflAxisInfo : public IVflRenderable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflAxisInfo();

    virtual ~VflAxisInfo();

    /**
     * Initializes the component info object according to the given properties.
     * The following keys are recognized:
     * 
     * VISIBLE [true|false]                 -   optional [default: true]
     * LINE_WIDTH                           -   optional [default: 2.0f]
     * X_AXIS_COLOR [r, g, b]               -   optional [1.0f, 0.0f, 0.0f]
     * X_AXIS_RANGE [min, max]              -   optional [0.0f, 1.0f]
     * Y_AXIS_COLOR [r, g, b]               -   optional [0.0f, 1.0f, 0.0f]
     * Y_AXIS_RANGE [min, max]              -   optional [0.0f, 1.0f]
     * Z_AXIS_COLOR [r, g, b]               -   optional [0.5f, 0.5f, 1.0f]
     * Z_AXIS_RANGE [min, max]              -   optional [0.0f, 1.0f]
     *
     * INI_SECTION [section name]       -   optional
     * INI_FILE [file name]             -   optional
     *
     * If the ini section and file are given, a property loader is created
     * (through the resource manager) and this object is registered as its target.
     */
//    virtual bool Init(const VistaPropertyList &refProps);
//    virtual bool Init();


    virtual void DrawOpaque();
    
	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);

	class VISTAFLOWLIBAPI VflAxisInfoProperties : public IVflRenderable::VflRenderableProperties
    {
    public:
        VflAxisInfoProperties();
        virtual ~VflAxisInfoProperties();

        //virtual int SetProperty(const VistaProperty &refProp);
        //virtual int GetProperty(VistaProperty &refProp);
        //virtual int GetPropertySymbolList(list<string> &rStorageList);

		float GetLineWidth() const;
		bool  SetLineWidth(float fWidth);
		
		bool  GetXAxisColor(float &r, float &g, float &b) const;
		bool  SetXAxisColor(float r, float g, float b);

		bool  GetYAxisColor(float &r, float &g, float &b) const;
		bool  SetYAxisColor(float r, float g, float b);

		bool  GetZAxisColor(float &r, float &g, float &b) const;
		bool  SetZAxisColor(float r, float g, float b);

		bool  GetXAxisRange(float &s, float &e) const;
		bool  SetXAxisRange(float s, float e);

		bool  GetYAxisRange(float &s, float &e) const;
		bool  SetYAxisRange(float s, float e);

		bool  GetZAxisRange(float &s, float &e) const;
		bool  SetZAxisRange(float s, float e);

		enum
		{
			MSG_X_RANGE_CHANGE = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_Y_RANGE_CHANGE,
			MSG_Z_RANGE_CHANGE,

			MSG_X_COLOR_CHANGE,
			MSG_Y_COLOR_CHANGE,
			MSG_Z_COLOR_CHANGE,

			MSG_LINE_WIDTH_CHANGE,
			MSG_LAST
		};

		virtual std::string GetReflectionableType() const;
	protected:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		VflAxisInfoProperties(const VflAxisInfoProperties &);
		VflAxisInfoProperties &operator=(VflAxisInfoProperties &);

        float   m_aXCol[3], m_aXRange[2];
        float   m_aYCol[3], m_aYRange[2];
        float   m_aZCol[3], m_aZRange[2];
        float   m_fLineWidth;

        friend class VflAxisInfo;
    };

//    CProperties *GetProperties();

	virtual std::string GetType() const;
	static std::string GetFactoryType();
	virtual unsigned int GetRegistrationMode() const;
protected:
	virtual VflRenderableProperties    *CreateProperties() const;

    VflPropertyLoader *m_pLoader;

//    CProperties m_oProperties;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLAXISINFO_H
