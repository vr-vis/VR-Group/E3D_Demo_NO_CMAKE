/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflFilterPropMultiplexer.h"
#include "../Visualization/VflUnsteadyDataFilter.h"
#include "../Tools/VflPropertyLoader.h"
#include <VistaAspects/VistaAspectsUtils.h>
#include <algorithm>
#include <VistaAspects/VistaPropertyAwareable.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflFilterPropMultiplexer::VflFilterPropMultiplexer()
: m_pLoader(NULL)
{
    m_pLoader = NULL;
}

VflFilterPropMultiplexer::~VflFilterPropMultiplexer()
{
   
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   (Un)RegisterFilter                                          */
/*                                                                            */
/*============================================================================*/
int VflFilterPropMultiplexer::RegisterFilter(VflUnsteadyDataFilter *pFilter)
{
	VistaMutexLock oLock(m_oMutex);
	m_liFilters.push_back(pFilter);
	return static_cast<int>(m_liFilters.size());
}

int VflFilterPropMultiplexer::UnregisterFilter(VflUnsteadyDataFilter *pFilter)
{
	VistaMutexLock oLock(m_oMutex);
	list<VflUnsteadyDataFilter *>::iterator it = find(m_liFilters.begin(), 
		m_liFilters.end(), pFilter);

	if (it != m_liFilters.end())
	{
		m_liFilters.erase(it);
	}
	return static_cast<int>(m_liFilters.size());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilterCount                                              */
/*                                                                            */
/*============================================================================*/
int VflFilterPropMultiplexer::GetFilterCount() const
{
	return static_cast<int>(m_liFilters.size());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilters                                                  */
/*                                                                            */
/*============================================================================*/
std::list<VflUnsteadyDataFilter *> VflFilterPropMultiplexer::GetFilters() const
{
	return m_liFilters;
}

/*============================================================================*/
/*  VflPropertyContainer stuff                                               */
/*============================================================================*/
bool VflFilterPropMultiplexer::SetPropertyByName(const string &sPropName, 
												  const string &sPropValue)
{
	VistaMutexLock oLock(m_oMutex);
	bool bResult = true;
	list<VflUnsteadyDataFilter *>::iterator it;

	for (it=m_liFilters.begin(); it!=m_liFilters.end(); ++it)
	{
		bResult &= (*it)->SetPropertyByName(sPropName, sPropValue);
	}

	return bResult;
}

int VflFilterPropMultiplexer::SetPropertiesByList(const VistaPropertyList & liProps)
{
	VistaMutexLock oLock(m_oMutex);
	int iFinalResult = -1;
	list<VflUnsteadyDataFilter *>::iterator it;

	for (it=m_liFilters.begin(); it!=m_liFilters.end(); ++it)
	{
		int iResult = (*it)->SetPropertiesByList(liProps);
		if (iFinalResult < 0)
			iFinalResult = iResult;
		else if (iResult < iFinalResult)
			iFinalResult = iResult;
	}

	return iFinalResult;
}

int VflFilterPropMultiplexer::SetProperty(const VistaProperty &refProp)
{
	VistaMutexLock oLock(m_oMutex);
	int iFinalResult = IVistaPropertyAwareable::PROP_OK;
	list<VflUnsteadyDataFilter *>::iterator it;

	for (it=m_liFilters.begin(); it!=m_liFilters.end(); ++it)
	{
		int iResult = (*it)->SetProperty(refProp);
		if (iResult > iFinalResult)
			iFinalResult = iResult;
	}

	return iFinalResult;
}

int VflFilterPropMultiplexer::GetProperty(VistaProperty &refProp)
{
   	string strKey 
		= VistaAspectsConversionStuff::ConvertToLower(refProp.GetNameForNameable());

  	VistaMutexLock oLock(m_oMutex);
	if (m_liFilters.empty())
		return IVistaPropertyAwareable::PROP_NOT_FOUND;
	
	return m_liFilters.front()->GetProperty(refProp);
}

int VflFilterPropMultiplexer::GetPropertySymbolList(std::list<std::string> &rStorageList)
{
	VistaMutexLock oLock(m_oMutex);
	if (m_liFilters.empty())
		return 0;
	
	return m_liFilters.front()->GetPropertySymbolList(rStorageList);
}

/*============================================================================*/
/*  LOCAL FUNCTIONS                                                           */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflFilterPropMultiplexer.cpp"                                 */
/*============================================================================*/
