/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFL3DTEXTLABEL_H
#define _VFL3DTEXTLABEL_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../VistaFlowLibConfig.h"
#include "../Visualization/VflRenderable.h"
#include <VistaBase/VistaVectorMath.h>
#include <string>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaColor;
class VtkObjectStack;
class vtkTextSource;
class vtkVectorText;
class vtkProperty;
class vtkActor;
class VistaVirtualPlatform;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A 3D text label that exists in vis space. Optionally it can automatically
 * be aligned perpendicular to the user's view direction.
 */
class VISTAFLOWLIBAPI Vfl3DTextLabel : public IVflRenderable
{
	friend class VflTextProperties;

public: 
	Vfl3DTextLabel();
	virtual ~Vfl3DTextLabel();

	virtual void Update();
	virtual void DrawOpaque();

	virtual unsigned int GetRegistrationMode() const;

	virtual bool GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds);
	virtual bool GetBounds(VistaBoundingBox &);
	// There is only a degenerate obb normal as the label is flat. If a normal
	// for lighting is required, calculate the cross from the right and up
	// vectors.
	bool GetOrientedBounds(VistaVector3D& v3Position, VistaVector3D& v3Right,
		VistaVector3D& v3Up);
	
	void SetText(const std::string& strT);
	std::string GetText() const;

	void SetPosition(const VistaVector3D &v3Pos);
	void SetPosition(double dPos[3]);
	void SetPosition(float fPos[3]);
	/**
	 * Due to some problems with the way vtk set the position to a text
	 * label it usually happens that the specified position and the final
	 * position do not exactly match. This function will perform some
	 * additional, moderately costly calculations to set the text label
	 * position exactly to the one specified.
	 */
	void SetPositionExact(const VistaVector3D& v3Position);

	VistaVector3D GetPosition() const;
	void GetPosition(VistaVector3D &v3Pos) const;
	void GetPosition(double dPos[3]) const;
	void GetPosition(float fPos[3]) const;

	void SetOrientation(const VistaQuaternion& qOri);
	VistaQuaternion GetOrientation() const;
	void GetOrientation(VistaQuaternion& qOri) const;

	void SetTextFollowViewDir(bool bFollowViewDir);
	bool GetTextFollowViewDir() const;

	void SetColor(const VistaColor& color, float fOpacity = 1.0);
	VistaColor GetColor() const;
	float GetOpacity() const;
	void SetColor(float fColor[4]);
	void GetColor(float fColor[4]) const;

	void SetTextSize(float fSize);
	float GetTextSize() const;

	/**
     * Prints out some debug information to the given output stream.
     * @param[in]   std::ostream & out
     * @return  --
     */    
    virtual void Debug(std::ostream & out) const;
    /**
     * Returns the type of the visualization object as a string.
     * @param[in]   --
     * @return  std::string
     */    
	virtual std::string GetType() const;

	vtkProperty* GetRenderProperty() const;

	class VISTAFLOWLIBAPI VflTextProperties : public VflRenderableProperties
	{
	public:
		VflTextProperties();
		virtual ~VflTextProperties();

		float GetTextSize() const;
		bool SetTextSize(float fNewSize);

		bool SetTextFollowViewDir(bool bFollowViewDir);
		bool GetTextFollowViewDir() const;

		VistaVector3D GetPosition() const;
		bool SetPosition(const VistaVector3D& v3Position);
		
		VistaQuaternion GetOrientation() const;
		bool SetOrientation(const VistaQuaternion& qOri);

		VistaVector3D GetOffset() const;
		bool SetOffset(const VistaVector3D &v3Offset);

		std::string GetText() const;
		bool SetText(const std::string &strText);

		void GetColor(double rgba[4]) const;
		bool SetColor(double rgba[4]);

		bool GetUseLighting() const ;
		bool SetUseLighting( bool bUseLighting );

		enum
		{
			MSG_TEXT_SIZE_CHG = VflRenderableProperties::MSG_LAST,
			MSG_FOLLOW_VIEWDIR,
			MSG_POSITION_CHG,
			MSG_ORIENTATION_CHG,
			MSG_TEXT_CHG,
			MSG_COLOR_CHG,
			MSG_OFFSET_CHG,
			MSG_LAST
		};

		REFL_DECLARE
	private:
		bool m_bTextFollowViewDir;
		VistaVector3D m_v3Position;
		VistaQuaternion m_qOri;
		VistaVector3D m_v3Offset;
		float m_fSize;
		std::string m_strText;
	};

	VflTextProperties* GetProperties() const;

protected:
	virtual VflRenderableProperties* CreateProperties() const;
	bool GetIsDirty() const;

private:
	bool m_bIsDirty;

	// Text rendering via vtk.
	VtkObjectStack* m_pObjectStack;
	vtkVectorText* m_pTextSource;
	vtkProperty* m_pTextProperty;
	vtkActor* m_pTextActor;
	VistaVirtualPlatform* m_pRefFrame;

	double m_aVtkBounds[6];
};


#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

