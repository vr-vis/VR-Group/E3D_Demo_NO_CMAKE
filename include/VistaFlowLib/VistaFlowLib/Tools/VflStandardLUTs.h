/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLSTANDARDLUTS_H
#define _VFLSTANDARDLUTS_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
#include <VistaBase/VistaColor.h>

/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVflLookupTable;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*/
class VISTAFLOWLIBAPI VflStandardLUTs
{
public:
    static void SetRainbowLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);
	static void SetInverseRainbowLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);

	static void SetWhiteRedLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);
	static void SetRedWhiteLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);

	static void SetWhiteBlueLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);
	static void SetBlueWhiteLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);

	static void SetBlackWhiteLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);
	static void SetWhiteBlackLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);

	static void SetGreyWhiteLUT(IVflLookupTable *pLUT, 
								float fGreyLevel,
								unsigned int iNumColors);
	static void SetWhiteGreyLUT(IVflLookupTable *pLUT, 
								float fGreyLevel,
								unsigned int iNumColors);

	static void SetBlueWhiteRedLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);
	static void SetInverseBlueWhiteRedLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);
	
	static void SetBlueRedYellowWhiteLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);
	static void SetInverseBlueRedYellowWhiteLUT(IVflLookupTable *pLUT, 
								unsigned int iNumColors);

	/*
	 * Set an own color gradient. This method accepts a vector of pairs of
	 * doubles and VistaColors that specify which color is located at
	 * which location in the gradient. The vector must be sorted in ascending
	 * order by the first element of the pair and those elements have to be 
	 * in the range of [0.0; 1.0]. Otherwise undefined behavior occurs.
	 */
	static void SetMultiColorGradientLUT(
		IVflLookupTable *pLUT, unsigned int iNumColors,
		const std::vector<std::pair<double, VistaColor> > &vecStopsAndColors,
		double dMinValue = 0., double dMaxValue = 1.,
		VistaColor::EFormat eMixMethod = VistaColor::RGBA);


protected:
    VflStandardLUTs(){}
	virtual ~VflStandardLUTs(){}

private:
};

/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLSTANDARDLUTS_H

