/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifdef WIN32
	#pragma warning (disable: 4786)
#endif

// ViSTA FlowLib includes
#include "VflPropertyLoader.h"
//#include "../Data/VflPropertyContainer.h"

// ViSTA includes
#include <VistaAspects/VistaPropertyAwareable.h>
#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaTools/VistaProfiler.h>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflPropertyLoader::VflPropertyLoader()
: m_iConversionMode(CONVERSION_NONE)
{
}

VflPropertyLoader::VflPropertyLoader(std::string strIniSection,
                                       std::string strIniFile)
: m_strIniSection(strIniSection),
  m_strIniFile(strIniFile),
  m_iConversionMode(CONVERSION_NONE)
{
}


VflPropertyLoader::~VflPropertyLoader()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Execute                                                     */
/*                                                                            */
/*============================================================================*/
void VflPropertyLoader::Execute()
{
    if (m_strIniSection.empty())
    {
        vstr::errp() << " [VflPropLdr] no ini section given..." << endl;
        return;
    }

    if (m_strIniFile.empty())
    {
       vstr::errp() << " [VflPropLdr] no ini file given..." << endl;
        return;
    }

    VistaPropertyList oList;

    if (FillPropertyList(oList, m_strIniSection, m_strIniFile, m_iConversionMode))
    {
        set<IVistaPropertyAwareable *>::iterator itTarget;

        for (itTarget=m_setTargets.begin(); itTarget!=m_setTargets.end(); ++itTarget)
        {
            (*itTarget)->SetPropertiesByList(oList);
        }
    }
    else
    {
        vstr::warnp() << " [VflPropLdr] unable to find properties..." << endl
					  << "                ini section: " << m_strIniSection << endl
					  << "                ini file:    " << m_strIniFile << endl;
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Execute                                                     */
/*                                                                            */
/*============================================================================*/
void VflPropertyLoader::Execute(IVistaPropertyAwareable *pTarget)
{
    if (m_strIniSection.empty())
    {
        vstr::errp() << " [VflPropLdr] no ini section given..." << endl;
        return;
    }

    if (m_strIniFile.empty())
    {
        vstr::errp() << " [VflPropLdr] no ini file given..." << endl;
        return;
    }

    if (!pTarget)
    {
        vstr::errp() << " [VflPropLdr] no target given..." << endl;
        return;
    }

    VistaPropertyList oList;

    if (FillPropertyList(oList, m_strIniSection, m_strIniFile, m_iConversionMode))
    {
        pTarget->SetPropertiesByList(oList);
    }
    else
    {
        vstr::warnp() << " [VflPropLdr] unable to find properties..." << endl
					  << "                ini section: " << m_strIniSection << endl
					  << "                ini file:    " << m_strIniFile << endl;
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   FillPropertyList                                            */
/*                                                                            */
/*============================================================================*/
int VflPropertyLoader::FillPropertyList(VistaPropertyList &refList,
                                         std::string strIniSection,
                                         std::string strIniFile,
                                         int iConversionMode)
{
    VistaProfiler oProf;

    list<string> liSections;
    oProf.GetTheProfileSectionEntries(strIniSection, liSections, strIniFile);

    list<string>::iterator itSection;
    for (itSection = liSections.begin(); itSection != liSections.end(); ++itSection)
    {
        string strKey = *itSection;
        string strValue;

        oProf.GetTheProfileString(strIniSection, strKey, "", strValue, strIniFile);
        if (!strValue.empty())
        {
            switch (iConversionMode)
            {
            case CONVERSION_KEY_TO_UPPER:
                strKey = oProf.ToUpper(strKey);
                break;
            case CONVERSION_KEY_TO_LOWER:
                strKey = oProf.ToLower(strKey);
                break;
            }

            refList.SetValue(strKey, strValue);
        }
    }

    return !refList.empty();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ParseIniFile	                                              */
/*                                                                            */
/*============================================================================*/
int VflPropertyLoader::ParseIniFile(VistaPropertyList &refList,
                                         std::string strIniFile,
                                         int iConversionMode)
{
    VistaProfiler oProf;

    list<string> liSections;
    oProf.GetTheProfileSections(liSections, strIniFile);

    list<string>::iterator itSection;
    for (itSection = liSections.begin(); itSection != liSections.end(); ++itSection)
    {
		VistaPropertyList oProps;

		FillPropertyList(oProps, *itSection, strIniFile, iConversionMode);

		refList.SetPropertyListValue(*itSection, oProps);
    }

    return !refList.empty();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTargets                                                  */
/*                                                                            */
/*============================================================================*/
std::set<IVistaPropertyAwareable *> VflPropertyLoader::GetTargets() const
{
    return m_setTargets;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Add/RemoveTarget                                            */
/*                                                                            */
/*============================================================================*/
void VflPropertyLoader::AddTarget(IVistaPropertyAwareable *pTarget)
{
    m_setTargets.insert(pTarget);
}

void VflPropertyLoader::RemoveTarget(IVistaPropertyAwareable *pTarget)
{
    m_setTargets.erase(pTarget);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Get/SetConversionMode                                       */
/*                                                                            */
/*============================================================================*/
int VflPropertyLoader::GetConversionMode() const
{
    return m_iConversionMode;
}

void VflPropertyLoader::SetConversionMode(int iMode)
{
    if (CONVERSION_NONE<=iMode && iMode<=CONVERSION_MODE_COUNT)
    {
        m_iConversionMode = iMode;
    }
    Notify();
}

std::string VflPropertyLoader::GetConversionModeString() const
{
    return GetConversionModeString(m_iConversionMode);
}

std::string VflPropertyLoader::GetConversionModeString(int iConversionMode)
{
    switch (iConversionMode)
    {
    case CONVERSION_NONE:
        return "NONE";
    case CONVERSION_KEY_TO_UPPER:
        return "KEY_TO_UPPER";
    case CONVERSION_KEY_TO_LOWER:
        return "KEY_TO_LOWER";
    }

    return "UNKNOWN";
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetProperty                                                 */
/*                                                                            */
/*============================================================================*/
int VflPropertyLoader::SetProperty(const VistaProperty &refProp)
{
	string strKey 
		= VistaAspectsConversionStuff::ConvertToLower(refProp.GetNameForNameable());

    if (strKey == "ini_file")
    {
        vstr::warnp() << " [VflPropLdr] unable to set read-only value 'INI_FILE'..." << endl;
        return PROP_INVALID_VALUE;
    }
    else if (strKey == "ini_section")
    {
        vstr::warnp() << " [VflPropLdr] unable to set read-only value 'INI_SECTION'..." << endl;
        return PROP_INVALID_VALUE;
    }
    else if (strKey == "conversion_mode")
    {
        string strValue = VistaAspectsConversionStuff::ConvertToLower(refProp.GetValue());

        if (strValue == "none")
            m_iConversionMode = CONVERSION_NONE;
        else if (strValue == "key_to_upper")
            m_iConversionMode = CONVERSION_KEY_TO_UPPER;
        else if (strValue == "key_to_lower")
            m_iConversionMode = CONVERSION_KEY_TO_LOWER;
        else
            return PROP_INVALID_VALUE;
    }

    Notify();

    return PROP_OK;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperty                                                 */
/*                                                                            */
/*============================================================================*/
int VflPropertyLoader::GetProperty(VistaProperty &refProp)
{
   	string strKey 
		= VistaAspectsConversionStuff::ConvertToLower(refProp.GetNameForNameable());

    if (strKey == "ini_file")
    {
        refProp.SetValue(m_strIniFile);
    }
    else if (strKey == "ini_section")
    {
        refProp.SetValue(m_strIniSection);
    }
    else if (strKey == "conversion_mode")
    {
        refProp.SetValue(GetConversionModeString());
    }

    return PROP_OK;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPropertySymbolList                                       */
/*                                                                            */
/*============================================================================*/
int VflPropertyLoader::GetPropertySymbolList(std::list<std::string> &rStorageList)
{
    IVistaReflectionable::GetPropertySymbolList(rStorageList);

    rStorageList.push_back("INI_FILE");
    rStorageList.push_back("INI_SECTION");
    rStorageList.push_back("CONVERSION_MODE");

    return static_cast<int>(rStorageList.size());
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
