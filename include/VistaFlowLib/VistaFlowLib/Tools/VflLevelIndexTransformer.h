/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLLEVELINDEXTRANSFORMER_H
#define _VFLLEVELINDEXTRANSFORMER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../Visualization/IVflTransformer.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <vector>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTimeMapper;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 */
class VISTAFLOWLIBAPI VflLevelIndexTransformer : public IVflTransformer
{
public:
	VflLevelIndexTransformer(VveTimeMapper* pTM);

	/** Inits the transformer from a matrix. Note, that there are certain restrictions
	   * to the transform matrix, like uniform scaling of all axes.
	*/
	virtual ~VflLevelIndexTransformer();


	virtual bool GetUnsteadyTransform(double dSimTime, 
		                              VistaTransformMatrix &oStorage);


	void           GetTranslation(int iLevelIndex, VistaVector3D &) const;
	bool           SetTranslation(int iLevelIndex, const VistaVector3D &);

	void				GetRotation(int iLevelIndex, VistaQuaternion &qRot) const;
	bool				SetRotation(int iLevelIndex, const VistaQuaternion &qRot);

	float	GetScale(int iLevelIndex) const;
	bool	SetScale(int iLevelIndex, float f);

	std::string GetReflectionableType() const;
	static  std::string GetFactoryType();

	enum
	{
		MSG_TRANSFORM_CHANGE = IVflTransformer::MSG_LAST,
		MSG_LAST
	};
protected:
    /**
     *  This method <b>must</b> be overloaded by subclasses in order
     * to build a propert hierarchy of base-type strings (i.e. strings 
	 * that encode a unique class identifier). This Method will add its 
	 * base type string as vanilla string to the rBtList.
     * @return the number of entries in the list
     * @param rBtList the list storage to keep track of base types
     */
    virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	void UpdateTransform(int iLevelIndex);
private:
	VveTimeMapper* m_pTimeMapper;

	std::vector<VistaVector3D> m_vecTranslation;
	std::vector<VistaQuaternion> m_vecRotation;
	std::vector<float> m_vecScale;

	std::vector<VistaTransformMatrix> m_vecTotalTransform;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

#endif // !defined(_VFLLEVELINDEXTRANSFORMER_H)
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
