/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLREGGRIDBACKGROUND_H
#define _VFLREGGRIDBACKGROUND_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * This class renders a regular grid background, e.g. as background on a plot.
 *
 * Most important properties to be defined:
 *	. the center position of the quad -> v3Pos  
 *	. the normal of the quad -> v3QuadNormal 
 *	. the width and the height of the quad -> fWidth, fHeight
 *  . the horizontal and vertical resolution of the grid -> fHRes, fVRes
 *
 *      +-----------+-----------+-----------+ (fWidth, fHeight)
 *      |           |      v3Pos|           |
 *      +-----------+-----*-----+-----------+
 *      |           |           |           |  fHRes=3 and fVRes=2
 *      +-----------+-----------+-----------+
 */
class VISTAFLOWLIBAPI VflRegGridBackground : public IVflRenderable
{
public:
	VflRegGridBackground();
    virtual ~VflRegGridBackground();

	virtual void DrawOpaque();
	virtual std::string GetType() const;
	virtual unsigned int GetRegistrationMode() const;

	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	void UpdateGridPoints();


	class VISTAFLOWLIBAPI VflRegGridBackgroundProperties
		: public IVflRenderable::VflRenderableProperties
	{
	public:
		VflRegGridBackgroundProperties();
		virtual ~VflRegGridBackgroundProperties();

		void SetGridResolution(int iHRes, int iVRes);
		void GetGridResolution(int &iHRes, int &iVRes) const; 

		void SetGridWidth(float f);
		float GetGridWidth() const;

		void SetGridHeight(float f);
		float GetGridHeight() const;

		void SetPosition(VistaVector3D v3);
		void GetPosition(VistaVector3D &v3) const;

		void SetNormal(VistaVector3D v3);
		void GetNormal(VistaVector3D &v3) const;

		void SetLineWidth(float fW);
		float GetLineWidth() const;

		void SetColor(float fR, float fG, float fB);
		void GetColor(float &fR, float &fG, float &fB) const;

		//void SetFogColor(const float col[3]);
		//void GetFogColor(float col[3]) const;

		//void SetFogEnabled(bool state);
		//bool GetFogEnabled() const;

		enum ePropMsg
		{
			MSG_LINE_WIDTH_CHANGED = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_COLOR_CHANGED,
			MSG_GRID_WIDTH_CHANGED,
			MSG_GRID_HEIGHT_CHANGED,
			MSG_RESOLUTION_CHANGED,
			MSG_POSITION_CHANGED,
			MSG_NORMAL_CHANGED,
		};

	private:
		int				m_iHRes,
						m_iVRes;
		float			m_fWidth,
						m_fHeight;
		VistaVector3D	m_v3Pos;
		VistaVector3D	m_v3QuadNormal;       // current: (0,0,1), i.e. x-y-plane -> todo: more general

		float			m_fLineWidth;
		float			m_fRed,
						m_fGreen,
						m_fBlue;
		//float           m_cFog[3];
		//bool            m_bFog;
	};

	//! override to return native property type
	virtual VflRegGridBackgroundProperties* GetProperties();

protected:
	virtual VflRegGridBackgroundProperties *CreateProperties() const;	

private:

	struct sLine
	{
		sLine(VistaVector3D v3Begin, VistaVector3D v3End)
		{
			fBegin[0] = v3Begin[0];
			fBegin[1] = v3Begin[1];
			fBegin[2] = v3Begin[2];
			fEnd[0] = v3End[0]; 
			fEnd[1] = v3End[1];
			fEnd[2] = v3End[2];
		}

		float fBegin[3];
		float fEnd[3];
	};
	std::vector<sLine> m_vecLines;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLREGGRIDBACKGROUND_H)
