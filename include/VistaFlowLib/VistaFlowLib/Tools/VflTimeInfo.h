/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLTIMEINFO_H
#define _VFLTIMEINFO_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include "../Visualization/VflRenderable.h"
#include <VistaVisExt/Tools/VveTimeMapper.h>

#include "../Visualization/VflSystemAbstraction.h"

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTimeMapper;
class VflPropertyLoader;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflTimeInfo : public IVflRenderable
{
public:
	// CONSTRUCTORS / DESTRUCTOR
	VflTimeInfo(VveTimeMapper *pVflTimeMapper);
	virtual ~VflTimeInfo();


	virtual void Draw2D();

	virtual std::string GetType() const;

	VveTimeMapper* GetTimeMapper() const;

	class VISTAFLOWLIBAPI VflTimeInfoProperties : public IVflRenderable::VflRenderableProperties
	{
	public:
		VflTimeInfoProperties();
		VflTimeInfoProperties(VveTimeMapper *pTimeMapper );

		virtual ~VflTimeInfoProperties();

		virtual std::string GetReflectionableType() const;


		bool GetDrawVisTime() const;
		bool SetDrawVisTime(bool bDraw);

		bool GetDrawSimTime() const;
		bool SetDrawSimTime(bool bDraw);

		bool GetDrawTimeLevel() const;
		bool SetDrawTimeLevel(bool bDraw);

		bool GetDrawTimeIndex() const;
		bool SetDrawTimeIndex(bool bDraw);

		bool GetDrawLevelIndex() const;
		bool SetDrawLevelIndex(bool bDraw);

		bool GetDisplayOnCurrentNode() const;
		bool SetDisplayOnCurrentNode(bool bDraw);

		std::list<std::string> GetNodeNamesForDisplay() const;
		bool SetNodeNamesForDisplay(const std::list<std::string> &liNodeNames);


		bool GetTextPosition(float &fX, float &fY) const;
		bool SetTextPosition(float fX, float fY);

		bool GetTextColor(float &fRed, float &fGreen, float &fBlue) const;
		bool SetTextColor(float fRed, float fGreen, float fBlue);

		float GetTextSize() const;
		bool SetTextSize(float fSize);

		bool   SetText(const std::string &sText);
		std::string GetText() const;

		std::string GetTimeFileName() const;
		bool   SetTimeFileName(const std::string &sTimeFile);

		bool SetShowIndexIcons(bool bShow);
		bool GetShowIndexIcons() const;


		enum
		{
			MSG_NODEDISPLAYCHANGE = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_DRAWVISTIME,
			MSG_DRAWSIMTIME,
			MSG_DRAWTIMELEVEL,
			MSG_DRAWTIMEINDEX,
			MSG_DRAWLEVELINDEX,
			MSG_DISPLAYONCURRENTNODE,
			MSG_DRAWINDEXICONS,

			MSG_POSITIONCHANGE,
			MSG_COLORCHANGE,
			MSG_TEXTCHANGE,
			MSG_SIZECHANGE,

			MSG_TIMEFILECHANGE,

			MSG_LAST
		};




	protected:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	private:
		VflTimeInfoProperties(const VflTimeInfoProperties &)
		{
		}

		VflTimeInfoProperties &operator=(const VflTimeInfoProperties &)
		{
			return *this;
		}

		bool m_bDrawVisTime;
		bool m_bDrawSimTime;
		bool m_bDrawTimeLevel;
		bool m_bDrawTimeIndex;
		bool m_bDrawLevelIndex;
		bool m_bShowIndexIcons;

		bool m_bDisplayOnCurrentNode;
		std::list<std::string> m_liNodeNamesForDisplay;
		std::string m_sTimeFile;

		IVflSystemAbstraction::IVflOverlayText *m_pText;
		VveTimeMapper *m_pMapper;

		friend class VflTimeInfo;
	};

	virtual VflTimeInfoProperties *GetProperties() const;

	static std::string GetFactoryType();

	virtual unsigned int GetRegistrationMode() const;
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);
protected:
	VflTimeInfo() {}
	virtual VflRenderableProperties    *CreateProperties() const;

	VflPropertyLoader *m_pPropertyLoader;
// 	VflResourceKey<VflTimeMapper> m_oKey;
// 	bool m_bUseResourceKey;
	VveTimeMapper *m_pTimeMapper;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLTIMEINFO_H
