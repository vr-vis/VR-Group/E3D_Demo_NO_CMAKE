/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLSTATICTRANSFORMER_H
#define _VFLSTATICTRANSFORMER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../Visualization/IVflTransformer.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 */
class VISTAFLOWLIBAPI VflStaticTransformer : public IVflTransformer
{
public:
	VflStaticTransformer();
	VflStaticTransformer(	const VistaVector3D &translation,
							const VistaQuaternion &rotation,
							float fScale=1.0f);
  /** Inits the transformer from a matrix. Note, that there are certain restrictions
   * to the transform matrix, like uniform scaling of all axes.
   */
  VflStaticTransformer(const VistaTransformMatrix &m4Transform);

	virtual ~VflStaticTransformer();


	virtual bool GetUnsteadyTransform(double dSimTime, 
		                              VistaTransformMatrix &oStorage);


	VistaVector3D GetTranslation() const;
	void           GetTranslation(VistaVector3D &) const;
	bool           SetTranslation(const VistaVector3D &);
	bool           SetTranslation(float afTranslation[3]);

	VistaQuaternion	GetRotation() const;
	void				GetRotation(VistaQuaternion &qRot) const;
	bool				SetRotation(const VistaQuaternion &qRot);
	bool				SetRotation(float afRotation[4]);

	float	GetScale() const;
	bool	SetScale(float f);

	enum
	{
		MSG_TRANSLATION_CHANGE = IVflTransformer::MSG_LAST,
		MSG_ROTATION_CHANGE,
		MSG_SCALE_CHANGE,
		
		MSG_LAST
	};

	std::string GetReflectionableType() const;
	static  std::string GetFactoryType();
protected:
    /**
     *  This method <b>must</b> be overloaded by subclasses in order
     * to build a propert hierarchy of base-type strings (i.e. strings 
	 * that encode a unique class identifier). This Method will add its 
	 * base type string as vanilla string to the rBtList.
     * @return the number of entries in the list
     * @param rBtList the list storage to keep track of base types
     */
    virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	void UpdateTransform();
private:
	VistaVector3D m_v3Translation;
	VistaQuaternion m_qRotation;
	float m_fScale;

	VistaTransformMatrix m_m4TotalTransform;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

#endif // !defined(_VflStaticTransformer_H)
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
