/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* PRAGMAS                                                                    */
/*============================================================================*/
#ifdef WIN32
    #pragma warning(disable:4786)
#endif

#include "VflTokenTranslator.h"
#include <VistaAspects/VistaPropertyAwareable.h>

#include <stdio.h>
#include <cassert>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflTokenTranslator::VflTokenTranslator()
{
}

VflTokenTranslator::~VflTokenTranslator()
{
	this->ResetToIdentity();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
void VflTokenTranslator::Init(const VistaPropertyList& oProps)
{
	//re-init the translation table
	this->ResetToIdentity();

#ifdef DEBUG
	vstr::debugi()<<"[VflTokenTranslator::Init] Configuring from proplist...\n";
#endif
	VistaPropertyList::const_iterator itCurrent = oProps.begin();
	VistaPropertyList::const_iterator itEnd = oProps.end();
	string strSource, strTranslation;
	for(;itCurrent != itEnd; ++itCurrent)
	{
		strSource = itCurrent->second.GetNameForNameable();
		strTranslation = itCurrent->second.GetValue();
		this->RegisterToken( strSource, strTranslation );
#ifdef DEBUG
		vstr::debugi()<<"\tNew translation ["<<strSource.c_str()<<" |-->  "<<strTranslation.c_str()<<"]\n";
#endif
	}
#ifdef DEBUG
	vstr::debugi()<<"[VflTokenTranslator::Init] DONE!\n";
#endif
	//assert proper function
	//either the input has been empty, and so should be the current token table OR we should not have an empty token table at all!
	assert((oProps.empty() && m_mapTokenTable.empty()) || (!m_mapTokenTable.empty()) &&
		("[VflTokenTranslator::Init] Token table non-empty for empty input!"));
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   RegisterToken                                               */
/*                                                                            */
/*============================================================================*/
void VflTokenTranslator::RegisterToken(const std::string& strInputToken, const std::string& strTranslatedToken)
{
	//sanity check
	if(strInputToken.empty() || strTranslatedToken.empty())
	{
		vstr::errp()<<"[VflTokenTranslator::RegisterToken] Won't register empty token(s)!"<<endl;
		return;
	}

	map<string,string>::iterator itFind = m_mapTokenTable.find(strInputToken);
	if(itFind == m_mapTokenTable.end())
	{
		m_mapTokenTable[strInputToken] = strTranslatedToken;
	}
	else
	{
		vstr::warnp()<<"[VflTokenTranslator::RegisterToken] Tried to register duplicate source token ("<<strInputToken.c_str()<<")!"<<endl;
	}
	//ASSERT that a valid token has been inserted!
	assert(	m_mapTokenTable.find(strInputToken) != m_mapTokenTable.end() && 
			"[VflTokenTranslator::RegisterToken] Token was not inserted correctly!");
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   TranslateToken                                              */
/*                                                                            */
/*============================================================================*/
std::string VflTokenTranslator::TranslateToken(const std::string& strInputToken) const
{
	map<string,string>::const_iterator itFind = m_mapTokenTable.find(strInputToken);
	if(itFind != m_mapTokenTable.end())
	{
		return itFind->second;
	}
	else
	{ 
#ifdef DEBUG
		vstr::warnp()<<"[VflTokenTranslator::TranslateToken] No entry for input token ("<<strInputToken.c_str()<<")! Returning input."<<endl;
#endif
		return strInputToken;
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeleteToken                                                 */
/*                                                                            */
/*============================================================================*/
void VflTokenTranslator::DeleteToken(const std::string& strInputToken)
{
	map<string,string>::iterator itFind = m_mapTokenTable.find(strInputToken);
	if(itFind != m_mapTokenTable.end())
	{
		m_mapTokenTable.erase(itFind);
	}
	else
	{ 
#ifdef DEBUG
		vstr::warnp()<<"[VflTokenTranslator::DeleteToken] Token ("<<strInputToken.c_str()<<") not registered!"<<endl;
#endif
	}
	//ASSERT that the token has been successfully deleted
	assert(m_mapTokenTable.find(strInputToken) == m_mapTokenTable.end() &&
			"[VflTokenTranslator::DeleteToken] Token was not deleted correctly!");
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ResetToIdentity                                             */
/*                                                                            */
/*============================================================================*/
void VflTokenTranslator::ResetToIdentity()
{
	m_mapTokenTable.clear();
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflTokenTranslator.cpp"                                           */
/*============================================================================*/
