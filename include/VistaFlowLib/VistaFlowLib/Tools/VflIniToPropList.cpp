/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VflIniToPropList.h"
#include <iostream>
#include <VistaTools/VistaProfiler.h>
#include <VistaAspects/VistaAspectsUtils.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

//#define DEBUG_TALKATIVE

static int ReadTypesFromIni(VistaPropertyList &refTypes,
                     const std::string &strIniFile);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflIniToPropList::VflIniToPropList(const VistaPropertyList &oTypes)
{
	ConvertToKeyLists(oTypes, m_oKeyLists);
}

VflIniToPropList::VflIniToPropList(const std::string &strIniFile)
{
    VistaPropertyList oTypes;
    ReadTypesFromIni(oTypes, strIniFile);
    ConvertToKeyLists(oTypes, m_oKeyLists);
}

VflIniToPropList::~VflIniToPropList()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ReadIniSection                                              */
/*                                                                            */
/*============================================================================*/
VistaPropertyList VflIniToPropList::ReadIniSection(const std::string &strSection,
										   const std::string &strIniFile,
										   const VistaPropertyList &refTypes)
{
	VistaPropertyList oKeyList;
	ConvertToKeyLists(refTypes, oKeyList);

	VistaPropertyList oResult;
	ReadIniSection(oResult, strSection, strIniFile, oKeyList);
	return oResult;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ReadIniSection                                              */
/*                                                                            */
/*============================================================================*/
VistaPropertyList VflIniToPropList::ReadIniSection(const std::string &strSection,
										   const std::string &strIniFile)
{
	VistaPropertyList oResult;
	ReadIniSection(oResult, strSection, strIniFile, m_oKeyLists);
	return oResult;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ConvertToKeyLists                                           */
/*                                                                            */
/*============================================================================*/
void VflIniToPropList::ConvertToKeyLists(const VistaPropertyList &oSource, VistaPropertyList &oDest)
{
#ifdef DEBUG_TALKATIVE
    vstr::debugi() << " [VflIniToPropList] - converting key lists..." << endl;
#endif

	oDest.clear();

	VistaPropertyList::const_iterator citType;
	list<string> liKeys;
	list<string>::iterator itKey;
	for (citType=oSource.begin(); citType!=oSource.end(); ++citType)
	{
		const VistaProperty &refType = (*citType).second;
		if( refType.GetPropertyType() != VistaProperty::PROPT_PROPERTYLIST )
			continue;
		const VistaPropertyList &refPropList = refType.GetPropertyListConstRef();

		VistaPropertyList oNewProps;

		if (refPropList.HasProperty("VISTAPROPERTYLISTS"))
		{
			refPropList.GetValue("VISTAPROPERTYLISTS", liKeys);
			for (itKey=liKeys.begin(); itKey!=liKeys.end(); ++itKey)
			{
                string strKey = VistaAspectsConversionStuff::ConvertToUpper(*itKey);
                oNewProps.SetValue(strKey, "VistaPropertyList");
			}
		}

		if (refPropList.HasProperty("VISTAPROPERTYLISTLISTS"))
		{
			refPropList.GetValue("VISTAPROPERTYLISTLISTS", liKeys);
			for (itKey=liKeys.begin(); itKey!=liKeys.end(); ++itKey)
			{
                string strKey = VistaAspectsConversionStuff::ConvertToUpper(*itKey);
                oNewProps.SetValue(strKey, "VistaPropertyListLIST");
			}
		}

		if (refPropList.HasProperty("VISTAPROPERTYLISTLISTS_ORDERED"))
		{
			refPropList.GetValue("VVISTAPROPERTYLISTLISTS_ORDERED", liKeys);
			for (itKey=liKeys.begin(); itKey!=liKeys.end(); ++itKey)
			{
				string strKey = VistaAspectsConversionStuff::ConvertToUpper(*itKey);
				oNewProps.SetValue(strKey, "VISTAPROPERTYLISTLIST_ORDERED");
			}
		}

        oDest.SetPropertyListValue(
            VistaConversion::StringToUpper(refType.GetNameForNameable()), 
            oNewProps);
	}

#ifdef DEBUG_TALKATIVE
    vstr::debugi() << " [VflIniToPropList] - conversion done - result: " << endl;
    oDest.PrintVistaPropertyList(2);
#endif
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ReadIniSection                                              */
/*                                                                            */
/*============================================================================*/
void VflIniToPropList::ReadIniSection(VistaPropertyList &refDest,
									   const std::string &strSection,
									   const std::string &strIniFile,
									   const VistaPropertyList &refKeys)
{
    VistaPropertyList oTemp;

	// @DRCLUSTER@ check these accessors - should they throw exeptions

    // first of all - fill the VistaPropertyList
    if (FillPropertyList(oTemp, strSection, strIniFile, true)==0)
        return;

    // find out about the type...
    if (!oTemp.HasProperty("TYPE"))
    {
#ifdef DEBUG
        vstr::warnp() << " [VflIniToPropList] no type key given for ini section..." << endl
			 << "                                ini section: " << strSection << endl
			 << "                                file name: " << strIniFile << endl;
#endif
        refDest = oTemp;

    	// add information about data source
    	if (!refDest.HasProperty("INI_FILE"))
    		refDest.SetValue("INI_FILE", strIniFile);
    	if (!refDest.HasProperty("INI_SECTION"))
    		refDest.SetValue("INI_SECTION", strSection);
    
        return;
    }

	string strType = oTemp.GetValue<std::string>("TYPE");
    strType = VistaAspectsConversionStuff::ConvertToUpper(strType);

    // do we have information about this type?
    if (refKeys.GetPropertyConstRef(strType).GetPropertyType()!=VistaProperty::PROPT_PROPERTYLIST)
    {
        vstr::warnp() << " [VflIniToPropList] no key information for type '" << strType << "'..." << endl
			 << "                                ini section: " << strSection << endl
			 << "                                file name: " << strIniFile << endl;
        refDest = oTemp;

        // add information about data source
	    if (!refDest.HasProperty("INI_FILE"))
	    	refDest.SetValue<std::string>("INI_FILE", strIniFile);
	    if (!refDest.HasProperty("INI_SECTION"))
	    	refDest.SetValue<std::string>("INI_SECTION", strSection);

        return;
    }

    const VistaPropertyList &refTypeKeys = refKeys.GetPropertyConstRef(strType).GetPropertyListConstRef();

    // iterate through all keys in our section and check, whether we've got some 
    // sub-VistaPropertyListS or lists of sub-VistaPropertyListS...
    VistaPropertyList::iterator it;
    for (it=oTemp.begin(); it!=oTemp.end(); ++it)
    {
        VistaProperty &refProp = (*it).second;

		std::string sType = refTypeKeys.GetValueOrDefault<std::string>(refProp.GetNameForNameable(), "" );

        if (sType=="VISTAPROPERTYLIST")
        {
            // we seem to have a sub-VistaPropertyList...
            string strSubSection = refProp.GetValue();

            refDest[refProp.GetNameForNameable()] = VistaProperty(refProp.GetNameForNameable());
            refDest[refProp.GetNameForNameable()].SetPropertyListValue(VistaPropertyList());
            VistaPropertyList &refSubList = refDest[refProp.GetNameForNameable()].GetPropertyListRef();
            ReadIniSection(refSubList, strSubSection, strIniFile, refKeys);

			//VistaPropertyList targetList;
			//// we have a list of properties now
			//list<string> liSections = VistaAspectsConversionStuff::ConvertToStringList(refProp.GetValue());
   //         for (list<string>::iterator itSection=liSections.begin(); 
			//	 itSection!=liSections.end(); ++itSection)
   //         {
			//	VistaPropertyList destList;
			//	ReadIniSection(destList, *itSection, strIniFile, refKeys);
			//	targetList.SetPropertyListValue(*itSection, destList);
			//}

			//refDest[refProp.GetNameForNameable()] = VistaProperty(refProp.GetNameForNameable());
			//refDest[refProp.GetNameForNameable()].SetPropertyListValue(targetList);
        }
        else if (sType=="VISTAPROPERTYLISTLIST")
        {
            // we seem to have a list of sub-VistaPropertyLists...
            list<string> liSections = VistaAspectsConversionStuff::ConvertToStringList(refProp.GetValue());
            refDest[refProp.GetNameForNameable()] = VistaProperty(refProp.GetNameForNameable());
            refDest[refProp.GetNameForNameable()].SetPropertyType(VistaProperty::PROPT_PROPERTYLIST);

            VistaPropertyList &refList = refDest[refProp.GetNameForNameable()].GetPropertyListRef();

            list<string>::iterator itSection;
            for (itSection=liSections.begin(); itSection!=liSections.end(); ++itSection)
            {
				VistaPropertyList oList;
                //refList.push_back(VistaPropertyList());
                //VistaPropertyList &refSubList = refList.back();
                ReadIniSection(oList, *itSection, strIniFile, refKeys);
				refList.SetPropertyListValue(*itSection, oList);
            }
        }
		else if (sType=="VISTAPROPERTYLISTLIST_ORDERED")
		{
			// we seem to have an ordered list of sub-VistaPropertyLists...
			list<string> liSections = VistaAspectsConversionStuff::ConvertToStringList(refProp.GetValue());

            refDest[refProp.GetNameForNameable()] = VistaProperty(refProp.GetNameForNameable());
            refDest[refProp.GetNameForNameable()].SetPropertyType(VistaProperty::PROPT_PROPERTYLIST);

            VistaPropertyList &refList = refDest[refProp.GetNameForNameable()].GetPropertyListRef();

            list<string>::iterator itSection;
			int iCount = 0;
            for (itSection=liSections.begin(); itSection!=liSections.end(); ++itSection, ++iCount)
            {
				VistaPropertyList oList;
                //refList.push_back(VistaPropertyList());
                //VistaPropertyList &refSubList = refList.back();
                ReadIniSection(oList, *itSection, strIniFile, refKeys);
				char buf[16];
				sprintf(buf, "%03d_", iCount);
				string strKey(buf);
				strKey += *itSection;
				refList.SetPropertyListValue(strKey, oList);
            }
		}
        else
        {
            // just copy it...
            refDest[refProp.GetNameForNameable()] = refProp;
        }
    }
	// add information about data source
	if (!refDest.HasProperty("INI_FILE"))
		refDest.SetValue<std::string>("INI_FILE", strIniFile);
	if (!refDest.HasProperty("INI_SECTION"))
		refDest.SetValue<std::string>("INI_SECTION", strSection);
	
	// convert type string to upper case as well
	refDest.SetValue<std::string>("TYPE", strType);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   FillPropertyList                                            */
/*                                                                            */
/*============================================================================*/
int VflIniToPropList::FillPropertyList(VistaPropertyList &refProps, 
                                        const std::string &strIniSection,
                                        const std::string &strIniFile,
                                        bool bConvertKeyToUpper)
{
    VistaProfiler oProf;

    if (!oProf.GetTheProfileSection(strIniSection, strIniFile))
    {
        vstr::warnp() << " [VflIniToPropList] unable to find ini section..." << endl
			 << "                                ini file: " << strIniFile << endl
			 << "                                section: " << strIniSection << endl;

        return 0;
    }

    refProps.clear();

    list<string> liKeys;
    oProf.GetTheProfileSectionEntries(strIniSection, liKeys, strIniFile);
    list<string>::iterator itKey;
    for (itKey = liKeys.begin(); itKey!=liKeys.end(); ++itKey)
    {
        string strKey = *itKey;
        string strValue;
        oProf.GetTheProfileString(strIniSection, *itKey, "", strValue, strIniFile);

        if (bConvertKeyToUpper)
            strKey = VistaAspectsConversionStuff::ConvertToUpper(strKey);

        refProps.SetValue<std::string>(strKey, strValue);
    }

    return static_cast<int>(refProps.size());
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/
static int ReadTypesFromIni(VistaPropertyList &refTypes,
                     const std::string &strIniFile)
{
    VistaProfiler oProf;

    list<string> liTypes;

    oProf.GetTheProfileSections(liTypes, strIniFile);

    list<string>::iterator itType;
    list<string> liTemp;
    for (itType=liTypes.begin(); itType!=liTypes.end(); ++itType)
    {
        string strType = *itType;

        VistaPropertyList oKeys;

        liTemp.clear();
        oProf.GetTheProfileList(strType, "VistaPropertyListS", liTemp, strIniFile);
        if (!liTemp.empty())
            oKeys.SetValue("VistaPropertyListS", liTemp);

        liTemp.clear();
        oProf.GetTheProfileList(strType, "VistaPropertyListLISTS", liTemp, strIniFile);
        if (!liTemp.empty())
            oKeys.SetValue("VistaPropertyListLISTS", liTemp);

		liTemp.clear();
		oProf.GetTheProfileList(strType, "VistaPropertyListLISTS_ORDERED", liTemp, strIniFile);
		if (!liTemp.empty())
			oKeys.SetValue("VistaPropertyListLISTS_ORDERED", liTemp);

        strType = VistaAspectsConversionStuff::ConvertToUpper(strType);
        refTypes.SetValue(strType, oKeys);
    }

    return static_cast<int>(refTypes.size());
}


/*============================================================================*/
/*  END OF FILE "VflIniToPropList.cpp"                                        */
/*============================================================================*/
