/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLLEGENDINFO_H
#define _VFLLEGENDINFO_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include <list>

#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflRenderNode;
class Vfl3DTextLabel;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This class provide a tool to draw coordinate axes and its labels.
 *   
 *              | max_y
 *              |
 *              | label_y
 *              |
 *              | min_y             
 *              *---------------------------
 *      min_z  / min_x     label_x     max_x
 *            /
 *   label_z /
 *          /
 *   max_z /
 *
 *   "legend" := "min" + "max" + "label"
 *   This class renders "legend" + axis.
 *
 *   min_x, max_x, label_x, min_y, max_y, label_y are on the x,y-plane
 *   min_z, max_z, label_z are on the y,z-plane
 */
class VISTAFLOWLIBAPI VflLegendInfo : public IVflRenderable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflLegendInfo(VflRenderNode* pRenderNode);
    virtual ~VflLegendInfo();
    
	/**
	 *
	 */
	//virtual void SetVisController(VflVisController *pController);
	
	/** 
	 *
	 */
	// ### m_pTarget --> BoundingBox
	//void SetRenderable(IVflRenderable *pRenderable);
	//IVflRenderable *GetRenderable() const;
	void SetTargetBoundingBox(VistaVector3D v3Min, VistaVector3D v3Max);
	void GetTargetBoundingBox(VistaVector3D &v3Min, VistaVector3D &v3Max) const;
    /**
	 *
	 */
	virtual void DrawOpaque();
    /**
	 *
	 */
	virtual void Update();
    /**
	 *
	 */
	virtual std::string GetType() const;
    /**
	 *
	 */
	virtual unsigned int GetRegistrationMode() const;
	
	/**
	 *
	 */
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);
	/**
	 *
	 */
	class VISTAFLOWLIBAPI VflLegendInfoProperties 
		: public IVflRenderable::VflRenderableProperties
    {
    public:
        VflLegendInfoProperties();
        virtual ~VflLegendInfoProperties();
        virtual std::string GetReflectionableType() const;

		/**
		 * Set the labels for the different ranges
		 */
		bool SetLegendText(int iAxis, const std::string& strText);
		std::string GetLegendText(int iAxis) const;

		/**
		 * set ranges values for the labeled coordinate axes
		 * per default the renderer's bounds will be assumed as ranges
		 */
		bool SetLegendRanges(float fRanges[6]);
		void GetLegendRanges(float fRanges[6]) const;
	
		/**
		 * Indicate whether the bounds are overridden by the
		 * ranges specified via SetLegendRanges
		 */
		bool SetOverrideBounds(bool b);
		bool GetOverrideBounds() const;

		/**
		 * if there is a sign change in any of the axes' ranges
		 * SetDrawLegendAxesAtRangeZero will draw the axes at the zero
		 * point in the corresponding range.
		 */
		bool SetDrawLegendAxesAtRangeZero(bool b);
		bool GetDrawLegendAxesAtRangeZero() const;

		/**
		 * Specify the offsets of the legends (min/max,label) relativ to the default.
		 * Default: (seen from this view:                   )          
		 *                                   y | 
		 *                                     *--- x
		 *                                  z /
		 *
		 *          x-legends are below the x-axes            
		 *			y-label is on the left side of y-axes
         *          y-min/max are on the right side of y-axes
		 *          z-legends are under the z-axes
		 *
		 * the only reasonable offset for x-legends is in y-direction (because they are defined on x,y-plane) 
		 *															  -> m_fXLabelOffset, m_fXMinMaxOffset
		 *                                y-legends is in x-direction (because they are defined on x,y-plane)
         *															  -> m_fYLabelOffset, m_fYMinMaxOffset
		 *                                z-legends is in y-direction (because they are defined on y,z-plane) 
		 *															  -> m_fZLabelOffset, m_fZMinMaxOffset
		 */
		bool SetLabelOffset(float fXLabelOffset, float fYLabelOffset, float fZLabelOffset);
		void GetLabelOffset(float &fXLabelOffset, float &fYLabelOffset, float &fZLabelOffset) const;

		bool SetMinMaxOffset(float fXMinMaxOffset, float fYMinMaxOffset, float fZMinMaxOffset);
		void GetMinMaxOffset(float &fXMinMaxOffset, float &fYMinMaxOffset, float &fZMinMaxOffset) const;
		

		/**
		 * Set the color of the axis and text by RGB-values, R,G,B is [0,1].
		 * Default: black
		 */
		bool SetLegendColor(float r, float g, float b);
		bool GetLegendColor(float &r, float &g, float &b) const;
		
		/**
		 *
		 */
		bool SetLegendTextSize(float fFontSize);
		float GetLegendTextSize() const;
		

		/**
		 *
		 */
		bool SetLineWidth(float fLineWidth);
		float GetLineWidth() const;
		
		/**
		 *
		 */
		bool SetOriginOffset(float fOO[3]);
		bool GetOriginOffset(float fOO[3]) const;

		/**
		 * auto	: in the middle
		 * min	: the ending of the axes, where the min value is 
		 * max  : the ending of the axes, where the max value is
		 */
		enum eLabelAlignment
		{
			LBL_ALIGN_AUTO,			
			LBL_ALIGN_MIN,
			LBL_ALIGN_MAX
		};

		bool SetLabelAlignment(int iAxis, eLabelAlignment eLblAlign);
		int GetLabelAlignment(int iAxis) const;

		/**
		 * center	: the center of the min/max text is the ending of the axis
		 *               _____________________
		 *              min                  max
		 *
		 * outside	: the min/max text is "outside" of the axis
		 *               _____________________
		 *           min                       max
		 *
		 * inside	: the min/max text is "inside" of the axis
		 *               _____________________
		 *               min               max
		 */
		enum eMinMaxAlignment
		{
			MINMAX_ALIGN_CENTER,
			MINMAX_ALIGN_OUTSIDE,
			MINMAX_ALIGN_INSIDE
		};

		bool SetMinMaxAlignment(int iAxis, eMinMaxAlignment eMMAlign);
		int GetMinMaxAlignment(int iAxis) const;

		/*
		* Dis/enable a single axis. Per default, all axes are enabled.
		*
		*/
		bool SetEnableAxis(int iAxis, bool bEnable);
		bool GetEnableAxis(int iAxis) const;

		enum
		{
			MSG_LABEL_CHANGED = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_RANGES_CHANGED,
			MSG_OVERRIDEBOUNDS_CHANGED,
			MSG_DRAWZERO_CHANGED,
			MSG_LEGENDOFFSET_CHANGED,
			MSG_LEGENDCOLOR_CHANGED,
			MSG_LEGENDFONTSIZE_CHANGED,
			MSG_LINEWIDTH_CHANGED,
			MSG_ORIGINOFFSET_CHANGED,
			MSG_LABELALIGNMENT_CHANGED,
			MSG_LAST
		};
	protected:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const; 

	private:
		std::string m_strLabels[3];
		float m_fRanges[6];
		float m_fDeltas[3];
		bool  m_bEnableAxis[3];

		bool m_bOverrideBounds;
		bool m_bDrawAtZero;
        
		//float m_fOffset[3];
		float m_fXLabelOffset;   // offset for x-legends is in y-direction
		float m_fYLabelOffset;   // offset for y-legends is in x-direction
		float m_fZLabelOffset;   // offset for z-legends is in y-direction
		float m_fXMinMaxOffset;
		float m_fYMinMaxOffset;
		float m_fZMinMaxOffset;

		float m_fColor[3];
		float m_fFontSize;
        float m_fLineWidth;
		float m_fOriginOffset[3];
		int m_iLblAlignment[3];
		int m_iMinMaxAlignment[3];
        
		VflLegendInfoProperties(const VflLegendInfoProperties &) 
		{
		}

		VflLegendInfoProperties &operator=(const VflLegendInfoProperties &)
		{
			return *this;
		}
        friend class VflLegendInfo;
    };
	virtual VflLegendInfoProperties *GetProperties() const;
protected:
	virtual VflRenderableProperties *CreateProperties() const;

	bool InitLabel(Vfl3DTextLabel *pLabel, 
				   const std::string &strInitText = "undefined",
				   bool bIsVisible = false);
	
	void ResetLabelPositions(float fPlotBounds[6]);
	void ResizeText();
	void UpdateMinMaxLabels(float fBounds[6]);
	void UpdateAxesOffsets();
	float ComputeZeroAxesOffset(float fRange[2], float fAxesBounds[2]);

private:
	//IVflRenderable	*m_pTarget;
	
	//text labels for the axes
	Vfl3DTextLabel *m_pLabelX, *m_pLabelY, *m_pLabelZ;
	//text labels for the min/max values of each axis
	Vfl3DTextLabel *m_pMinX, *m_pMaxX,
					*m_pMinY, *m_pMaxY,
					*m_pMinZ, *m_pMaxZ;					
	
	//cache the label bounds
	float m_fTextBoundsX[6], m_fTextBoundsY[6], m_fTextBoundsZ[6],
		  m_fMinXBounds[6], m_fMaxXBounds[6],
		  m_fMinYBounds[6], m_fMaxYBounds[6],
		  m_fMinZBounds[6], m_fMaxZBounds[6];
	
	//temp vars 
	VistaVector3D m_v3Min, m_v3Max;

	// axis offsets
	float m_fXOffset, m_fYOffset, m_fZOffset;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLLEGENDINFO_H
