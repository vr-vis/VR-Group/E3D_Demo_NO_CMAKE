/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLPROPERTYLOADER_H
#define _VFLPROPERTYLOADER_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaVisExt/Tools/VveExecutable.h>


#include <set>

//ViSTA includes

// ViSTA FlowLib includes
#include <VistaFlowLib/VistaFlowLibConfig.h>
#include <VistaAspects/VistaReflectionable.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVistaPropertyAwareable;
class VistaPropertyList;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * The class VflPropertyLoader parses a given ini section in a given ini file
 * and fills the given VflPropertyContainer object with the corresponding data.
 * In addition it offers a public static method to parse an ini file and an ini 
 * section and write the information into a given VistaPropertyList object.
 */
class VISTAFLOWLIBAPI VflPropertyLoader : IVistaReflectionable
{
public:
    enum EConversionMode
    {
        CONVERSION_NONE = 0,
        CONVERSION_KEY_TO_UPPER,
        CONVERSION_KEY_TO_LOWER,
        CONVERSION_MODE_COUNT
    };

    VflPropertyLoader();
    VflPropertyLoader(std::string strIniSection, std::string strIniFile);

    virtual ~VflPropertyLoader();

    std::set<IVistaPropertyAwareable *> GetTargets() const;
    void AddTarget(IVistaPropertyAwareable *pTarget);
    void RemoveTarget(IVistaPropertyAwareable *pTarget);

    std::string GetIniSection() const;
    void SetIniSection(const std::string &strIniSection);

    std::string GetIniFile() const;
    void SetIniFile(const std::string &strIniFile);

    int GetConversionMode() const;
    void SetConversionMode(int iMode);
    std::string GetConversionModeString() const;
    static std::string GetConversionModeString(int iConversionMode);

    /**
     * 'Execute' this object, i.e. make it load properties (i.e. key-value
     * pairs) from an ini section and write it into the given property object.
     */
    virtual void Execute();

    /**
     *`'Execute' this object for a given target, i.e. make it load properties
     * and write it into the given property object. This way, the target can
     * be initialized without calling the common 'Execute()' method, which might
     * result in superfluous notifications for all targets.
     */
    virtual void Execute(IVistaPropertyAwareable *pTarget);

    static int FillPropertyList(VistaPropertyList & refList,
        std::string strIniSection, std::string strIniFile,
        int iConversionMode = CONVERSION_NONE);

	static int ParseIniFile(VistaPropertyList &refList,
        std::string strIniFile, int iConversionMode = CONVERSION_NONE);

    /**
     * Interface with the rest of the world through 
     * the VistaPropertyAwareable interface.
     */
    virtual int SetProperty(const VistaProperty &refProp);
    virtual int GetProperty(VistaProperty &refProp);
    virtual int GetPropertySymbolList(std::list<std::string> &rStorageList);
	
	REFL_INLINEIMP(VflPropertyLoader,IVistaReflectionable)
protected:
    std::set<IVistaPropertyAwareable *> m_setTargets;
    std::string m_strIniSection;
    std::string m_strIniFile;
    int m_iConversionMode;
};


/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIniSection                                               */
/*                                                                            */
/*============================================================================*/
inline std::string VflPropertyLoader::GetIniSection() const
{
    return m_strIniSection;
}

inline void VflPropertyLoader::SetIniSection(const std::string &strIniSection)
{
	m_strIniSection = strIniSection;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIniFile                                                  */
/*                                                                            */
/*============================================================================*/
inline std::string VflPropertyLoader::GetIniFile() const
{
    return m_strIniFile;
}

inline void VflPropertyLoader::SetIniFile(const std::string &strIniFile)
{
	m_strIniFile = strIniFile;
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLPROPERTYLOADER_H

