/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifdef WIN32
	#pragma warning(disable:4786)
#endif

#include "VflVisComposite.h"

#include <iostream>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;


/*============================================================================*/
/*  IMPLEMENTATION FOR VflVisComposite                                          */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflVisComposite::VflVisComposite()
: IVflVisComponent(),
  m_iComponentCount(0)
{
}

VflVisComposite::~VflVisComposite()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Execute                                                     */
/*                                                                            */
/*============================================================================*/
void VflVisComposite::Execute()
{
    int i;
    for (i=0; i<m_iComponentCount; ++i)
    {
        m_vecComponents[i]->Execute();
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetVisible                                                  */
/*                                                                            */
/*============================================================================*/
void VflVisComposite::SetVisible(bool bVisible)
{
    int i;
    for (i=0; i<m_iComponentCount; ++i)
    {
        m_vecComponents[i]->SetVisible(bVisible);
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisible                                                  */
/*                                                                            */
/*============================================================================*/
bool VflVisComposite::GetVisible() const
{
	for (int i=0; i<m_iComponentCount; ++i)
	{
		if (m_vecComponents[i]->GetVisible())
			return true;
	}

    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetVisible                                                  */
/*                                                                            */
/*============================================================================*/
void VflVisComposite::SetVisible(int iIndex, bool bVisible)
{
    if (0<=iIndex && iIndex<m_iComponentCount)
    {
        m_vecComponents[iIndex]->SetVisible(bVisible);
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisible                                                  */
/*                                                                            */
/*============================================================================*/
bool VflVisComposite::GetVisible(int iIndex) const
{
    if (0<=iIndex && iIndex<m_iComponentCount)
    {
        return m_vecComponents[iIndex]->GetVisible();
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetHelpersVisible                                           */
/*                                                                            */
/*============================================================================*/
void VflVisComposite::SetHelpersVisible(bool bVisible)
{
    for (unsigned int i=0; i<m_vecComponents.size(); ++i)
    {
        (*m_vecComponents[i]).SetHelpersVisible(bVisible);
    }

    IVflVisComponent::SetHelpersVisible(bVisible);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVisComposite::GetType() const
{
    return IVflVisComponent::GetType()+"::VflVisComposite";
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddVisComponent                                             */
/*                                                                            */
/*============================================================================*/
int VflVisComposite::AddVisComponent(IVflVisComponent *pComp)
{
    if (pComp)
    {
        if (m_iComponentCount>=int(m_vecComponents.size()))
            m_vecComponents.resize((unsigned int)m_iComponentCount+1);
        m_vecComponents[m_iComponentCount] = pComp;

        ++m_iComponentCount;
    }

    return m_iComponentCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemoveVisComponent                                             */
/*                                                                            */
/*============================================================================*/
int VflVisComposite::RemoveVisComponent(IVflVisComponent *pComp)
{
    int iIndex = GetVisComponentIndex(pComp);

    if (iIndex >= 0)
    {
        RemoveVisComponent(iIndex);
    }

    return m_iComponentCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemoveVisObject                                             */
/*                                                                            */
/*============================================================================*/
int VflVisComposite::RemoveVisComponent(int iIndex)
{
    if (iIndex>=0 && iIndex<m_iComponentCount)
    {
        int i;
        for (i=iIndex; i<m_iComponentCount-1; ++i)
        {
            m_vecComponents[i] = m_vecComponents[i+1];
        }
        m_vecComponents[i] = NULL;
    }

    --m_iComponentCount;

    return m_iComponentCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumberOfComponents                                       */
/*                                                                            */
/*============================================================================*/
int VflVisComposite::GetNumberOfComponents() const
{
    return m_iComponentCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisComponent                                             */
/*                                                                            */
/*============================================================================*/
IVflVisComponent *VflVisComposite::GetVisComponent(int iIndex) const
{
    if (iIndex>=0 && iIndex<m_iComponentCount)
    {
        return m_vecComponents[iIndex];
    }

    return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisComponentIndex                                        */
/*                                                                            */
/*============================================================================*/
int VflVisComposite::GetVisComponentIndex(IVflVisComponent *pComp) const
{
    int i;
    for (i=0; i<m_iComponentCount; ++i)
    {
        if (m_vecComponents[i] == pComp)
        {
            return i;
        }
    }

    return -1;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflVisComposite.cpp"                                               */
/*============================================================================*/
