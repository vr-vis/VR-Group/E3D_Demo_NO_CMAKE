/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLCOMPOSITETRANSFORMER_H
#define _VFLCOMPOSITETRANSFORMER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/IVflTransformer.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <string>
#include <list>
#include <vector>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaTransformMatrix;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Class for combining multiple VflTransformers.
 */
class VISTAFLOWLIBAPI VflCompositeTransformer : public IVflTransformer
{
public:
	VflCompositeTransformer();
	virtual ~VflCompositeTransformer();

	/**
	 * Invokes corresponding method of listed transformers
	 * and postmultiplies them in the listed order.
	 */
	virtual bool GetUnsteadyTransform(double dSimTime, 
		                              VistaTransformMatrix &oStorage);

	/**
	 * Adds an IVflTransformer to the end of the list.
	 */
	bool AddTransformer(IVflTransformer* pTransformer);

    virtual std::string GetReflectionableType() const;
	static  std::string GetFactoryType();

	enum
	{
		MSG_TIMELEVELSNAP_CHANGE = IVistaReflectionable::MSG_LAST,
		MSG_LAST
	};

protected:
    /**
     *  This method <b>must</b> be overloaded by subclasses in order
     * to build a propert hierarchy of base-type strings (i.e. strings 
	 * that encode a unique class identifier). This Method will add its 
	 * base type string as vanilla string to the rBtList.
     * @return the number of entries in the list
     * @param rBtList the list storage to keep track of base types
     */
    virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

private:

	/**
	 * Listorder = transformationorder.
	 */
	std::vector<IVflTransformer*> m_vecTransformers;

};

/*============================================================================*/
/* INLINE METHODS                                                             */
/*============================================================================*/

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFLCOMPOSITETRANSFORMER_H
