/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifdef WIN32
	#pragma warning (disable: 4786)
#endif

// VistaFlowLib includes
#include "VflStandardLUTs.h"

#include <VistaBase/VistaExceptionBase.h>

#include <VistaKernel/GraphicsManager/VistaGraphicsManager.h>

#include <VistaFlowLib/Visualization/VflLookupTable.h>
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRainbowLUT                                               */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetRainbowLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetHueRange(0.6667f, 0.0f);
	pLUT->SetSaturationRange(1.0f, 1.0f);
	pLUT->SetValueRange(1.0f, 1.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);
	pLUT->UpdateTableValues();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetInverseRainbowLUT                                        */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetInverseRainbowLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetHueRange(0.0f, 0.666f);
	pLUT->SetSaturationRange(1.0f, 1.0f);
	pLUT->SetValueRange(1.0f, 1.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);
	pLUT->UpdateTableValues();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetWhiteRedLUT                                              */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetWhiteRedLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetHueRange(0.0f, 0.0f);
	pLUT->SetSaturationRange(0.0f, 1.0f);
	pLUT->SetValueRange(1.0f, 1.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);
	pLUT->UpdateTableValues();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRedWhiteLUT	                                          */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetRedWhiteLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetHueRange(0.0f, 0.0f);
	pLUT->SetSaturationRange(1.0f, 0.0f);
	pLUT->SetValueRange(1.0f, 1.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);
	pLUT->UpdateTableValues();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetWhiteBlueLUT									          */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetWhiteBlueLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetHueRange(0.6667f, 0.6667f);
	pLUT->SetSaturationRange(0.0f, 1.0f);
	pLUT->SetValueRange(1.0f, 1.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);
	pLUT->UpdateTableValues();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBlueWhiteLUT									          */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetBlueWhiteLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetHueRange(0.6667f, 0.6667f);
	pLUT->SetSaturationRange(1.0f, 0.0f);
	pLUT->SetValueRange(1.0f, 1.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);
	pLUT->UpdateTableValues();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBlackWhiteLUT									          */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetBlackWhiteLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetHueRange(0.0f, 0.0f);
	pLUT->SetSaturationRange(0.0f, 0.0);
	pLUT->SetValueRange(0.0f, 1.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);
	pLUT->UpdateTableValues();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetWhiteBlackLUT											  */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetWhiteBlackLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetHueRange(0.0f, 0.0f);
	pLUT->SetSaturationRange(0.0f, 0.0f);
	pLUT->SetValueRange(1.0f, 0.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);
	pLUT->UpdateTableValues();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetGreyWhiteLUT									          */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetGreyWhiteLUT(	IVflLookupTable *pLUT,
										float fGreyLevel,
										unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetHueRange(0.0f, 0.0f);
	pLUT->SetSaturationRange(0.0f, 0.0f);
	pLUT->SetValueRange(fGreyLevel, 1.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);
	pLUT->UpdateTableValues();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetWhiteGreyLUT											  */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetWhiteGreyLUT(IVflLookupTable *pLUT, 
									   float fGreyLevel,
									   unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetHueRange(0.0f, 0.0f);
	pLUT->SetSaturationRange(0.0f, 0.0f);
	pLUT->SetValueRange(1.0f, fGreyLevel);
	pLUT->SetAlphaRange(1.0f, 1.0f);
	pLUT->UpdateTableValues();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBlueWhiteRedLUT								          */
/*																			  */
/*============================================================================*/
void VflStandardLUTs::SetBlueWhiteRedLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetValueRange(1.0f, 0.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);

	double dChange = 2.0/(iNumColors - 1);
	double red = 0;
	double green = 0;
	double blue = 1;
	unsigned int numCol = 0;

	pLUT->SetTableValue(numCol, VistaColor(red, green, blue, 1.0));

	for (numCol = 1; numCol < iNumColors; numCol++)
	{
		if(numCol <= ((iNumColors - 1) / 2) )
		{
			if (numCol == iNumColors)
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red = 1, green = 1,blue,1.0));
			}
			else
			{
				pLUT->SetTableValue(numCol, VistaColor(
					(red += dChange), (green += dChange), blue, 1.0));
			}
		}
		else
		{
			if (numCol == iNumColors -1)
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red = 1, green = 0, blue = 0, 1.0));
			}
			else
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red, (green -= dChange), (blue -= dChange), 1.0));
			}
		}
	}
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetInverseBlueWhiteRedLUT								      */
/*																			  */
/*============================================================================*/
void VflStandardLUTs::SetInverseBlueWhiteRedLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetValueRange(1.0f, 0.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);

	double dChange = 2.0/(iNumColors - 1);
	double red = 1;
	double green = 0;
	double blue = 0;
	unsigned int numCol = 0;

	pLUT->SetTableValue(numCol, VistaColor(red, green, blue, 1.0));

	for (numCol = 1; numCol < iNumColors; numCol++)
	{
		if(numCol <= ((iNumColors - 1) / 2) )
		{
			if (numCol == iNumColors)
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red = 1, green = 1, blue = 1, 1.0));
			}
			else
			{
				pLUT->SetTableValue(numCol,VistaColor(
					red, (green += dChange), (blue += dChange), 1.0));
			}
		}
		else
		{
			if (numCol == iNumColors -1)
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red = 0, green = 0, blue = 1, 1.0));
			}
			else
			{
				pLUT->SetTableValue(numCol, VistaColor(
					(red-= dChange), (green -= dChange), blue, 1.0));
			}
		}
	}
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetBlueRedYellowWhiteLUT									  */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetBlueRedYellowWhiteLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetValueRange(1.0f, 0.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);

	double dChange = 3.0/(iNumColors - 1);
	double red = 0;
	double green = 0;
	double blue = 1;
	unsigned int numCol = 0;
	
	pLUT->SetTableValue(numCol, VistaColor(0.0, 0.0, 1.0, 1.0));
	
	for (numCol = 1; numCol < iNumColors; numCol++)
	{
		if(numCol <= ((iNumColors - 1) / 3) )
		{
			if (numCol == ((iNumColors-1)/3))
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red = 1, green = 0, blue = 0, 1.0));
			}
			else
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red += dChange, green, blue-=dChange, 1.0));
			}
		}
		else if (numCol <= ((2*(iNumColors-1))/3))
		{
			if (numCol == iNumColors)
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red, green = 1, blue, 1.0));
			}
			else
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red, green += dChange , blue, 1.0));
			}
		}
		else
		{
			if (numCol == (iNumColors-1))
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red, green, blue = 1, 1.0));
			}
			else
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red, green, blue += dChange, 1.0));
			}
		}
	}
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetInverseBlueRedYellowWhiteLUT							  */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetInverseBlueRedYellowWhiteLUT(IVflLookupTable *pLUT, 
									 unsigned int iNumColors)
{
	pLUT->SetValueCount(iNumColors);
	pLUT->SetValueRange(1.0f, 0.0f);
	pLUT->SetAlphaRange(1.0f, 1.0f);

	double dChange = 3.0/(iNumColors - 1);
	double red = 1;
	double green = 1;
	double blue = 1;
	unsigned int numCol = 0;

	pLUT->SetTableValue(numCol, VistaColor(red, green, blue, 1.0));
	
	for (numCol = 1; numCol < iNumColors; numCol++)
	{
		if(numCol <= ((iNumColors - 1) / 3) )
		{
			if (numCol == ((iNumColors-1)/3))
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red = 1, green = 1, blue = 0, 1.0));
			}
			else
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red, green, blue-=dChange, 1.0));
			}
		}
		else if (numCol <= ((2*(iNumColors-1))/3))
		{
			if (numCol == iNumColors)
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red, green = 0, blue = 0, 1.0));
			}
			else
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red, green  -= dChange, blue, 1.0));
			}
		}
		else
		{
			if (numCol == (iNumColors-1))
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red = 0, green = 0, blue = 1, 1.0));
			}
			else
			{
				pLUT->SetTableValue(numCol, VistaColor(
					red -= dChange, green, blue += dChange, 1.0));
			}
		}
	}
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetMultiColorGradientLUT									  */
/*                                                                            */
/*============================================================================*/
void VflStandardLUTs::SetMultiColorGradientLUT(
	IVflLookupTable *pLUT, unsigned int iNumColors, 
	const std::vector<std::pair<double, VistaColor> > &vecStopsAndColors, 
	double dMinValue /*= 0.*/, double dMaxValue /*= 1. */,
	VistaColor::EFormat eMixMethod /*= VistaColor::RGBA */)
{
	pLUT->SetTableRange(float(dMinValue), float(dMaxValue));
	pLUT->SetValueCount(iNumColors);

	std::vector<VistaColor> vecColors;
	vecColors.resize(iNumColors);

	// iterate over the table and set the values for each index
	// in the lookup table
	for (unsigned int i = 0; i < iNumColors; ++i)
	{
		// normalized between 0 and 1
		double dProgress = 1.0;
		if (iNumColors != 1)
			dProgress = static_cast<double>(i) / static_cast<double>(iNumColors - 1);

		// current table value corresponding to the progress
		double dValue = 
			dMinValue + dProgress * (dMaxValue - dMinValue);

		// get color for current index
		VistaColor oColor;
		if (vecStopsAndColors.size() > 0)
		{
			if (dValue <= vecStopsAndColors[0].first)
			{
				// value is smaller than the first stop,
				// so take the first stops color
				oColor = vecStopsAndColors[0].second;
			}
			else if (dValue >= 
				vecStopsAndColors[vecStopsAndColors.size() - 1].first)
			{
				// value is bigger than the last stop,
				// so take the last stops color
				oColor = vecStopsAndColors[vecStopsAndColors.size() - 1].second;
			}
			else
			{
				int iTopIndex = 1; // we can start with 1 because we already 
				// know the value is bigger than the first value

				// the control point vector is sorted, so we can just iterate over it 
				// and stop when we reach a control point with a smaller value than 
				// the value we are searching for
				for (; dValue > vecStopsAndColors[iTopIndex].first; ++iTopIndex);

				int iBottomIndex = iTopIndex - 1;

				double dLowerValue = vecStopsAndColors[iBottomIndex].first;
				double dUpperValue = vecStopsAndColors[iTopIndex].first;

				// mix between upper and lower index color
				double dMixingRatio = (dValue - dLowerValue) / 
					(dUpperValue - dLowerValue);
				
				oColor = vecStopsAndColors[iBottomIndex].second.Mix(
					vecStopsAndColors[iTopIndex].second, float(dMixingRatio),
					eMixMethod);
			}
		}
		else 
		{
			// if the input vector is empty, assign white
			oColor = VistaColor(1.f, 1.f, 1.f, 0.f);
		}

		// save color
		vecColors[i] = oColor;
		
	}

	// set table values all in one go (note: this is significantly faster than
	// calling SetTableValue() for each color, because the texture is updated
	// in each call).
	pLUT->SetTableValues(vecColors);

}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
