/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVISCOMPOSITE_H
#define _VFLVISCOMPOSITE_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VflVisComponent.h"
#include <vector>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 */
class VISTAFLOWLIBAPI VflVisComposite : public IVflVisComponent
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflVisComposite();
    virtual ~VflVisComposite();

public:
    /**
     * Execute anything that is to be executed, i.e. 
     * a loader or remote extraction or whatever...
     */
    virtual void Execute();

	/**
     * Visibility control.
     */
	virtual void SetVisible(bool bVisible);
	virtual bool GetVisible() const;
    virtual void SetHelpersVisible(bool bVisible);

    /**
     * Visibility control for single components.
     */
    virtual void SetVisible(int iIndex, bool bVisible);
    virtual bool GetVisible(int iIndex) const;

    /**
     * Returns the type of the component as string.
     */    
	virtual std::string GetType() const;

    /**
     * Component management.
     */
    int AddVisComponent(IVflVisComponent *pComp);
    int RemoveVisComponent(IVflVisComponent *pComp);
    int RemoveVisComponent(int iIndex);
    int GetNumberOfComponents() const;
    IVflVisComponent *GetVisComponent(int iIndex) const;
    int GetVisComponentIndex(IVflVisComponent *pComp) const;

protected:
    std::vector<IVflVisComponent *> m_vecComponents;
    int m_iComponentCount;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLVISCOMPOSITE_H)
