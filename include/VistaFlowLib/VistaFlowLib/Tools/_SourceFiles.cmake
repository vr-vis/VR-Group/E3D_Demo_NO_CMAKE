

set( RelativeDir "./Tools" )
set( RelativeSourceGroup "Source Files\\Tools" )

set( DirFiles
	Vfl3DTextLabel.cpp
	Vfl3DTextLabel.h
	VflAxisInfo.cpp
	VflAxisInfo.h
	VflAxisRotateTransformer.cpp
	VflAxisRotateTransformer.h
	VflBoundsInfo.cpp
	VflBoundsInfo.h
	VflComponentInfo.cpp
	VflComponentInfo.h
	VflCompositeTransformer.cpp
	VflCompositeTransformer.h
	VflDataRegistry.cpp
	VflDataRegistry.h
	VflFilterPropMultiplexer.cpp
	VflFilterPropMultiplexer.h
	VflIniToPropList.cpp
	VflIniToPropList.h
	VflKeyFrameTransformer.cpp
	VflKeyFrameTransformer.h
	VflLegendInfo.cpp
	VflLegendInfo.h
	VflLevelIndexTransformer.cpp
	VflLevelIndexTransformer.h
	VflPathlineLoader.cpp
	VflPathlineLoader.h
	VflPropertyLoader.cpp
	VflPropertyLoader.h
	VflRegGridBackground.cpp
	VflRegGridBackground.h
	VflScalarBar.cpp
	VflScalarBar.h
	VflStandardLUTs.cpp
	VflStandardLUTs.h
	VflStaticTransformer.cpp
	VflStaticTransformer.h
	VflTimeInfo.cpp
	VflTimeInfo.h
	VflTimeLine.cpp
	VflTimeLine.h
	VflTimeMapperLoader.cpp
	VflTimeMapperLoader.h
	VflTokenTranslator.cpp
	VflTokenTranslator.h
	VflVisComponent.cpp
	VflVisComponent.h
	VflVisComponentInfo.cpp
	VflVisComponentInfo.h
	VflVisComposite.cpp
	VflVisComposite.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

