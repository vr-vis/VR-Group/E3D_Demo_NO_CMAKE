/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLKEYFRAMETRANSFORMER_H
#define _VFLKEYFRAMETRANSFORMER_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
#include <VistaFlowLib/Visualization/IVflTransformer.h>

#include <vector>
#include <list>
#include <string>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAFLOWLIBAPI VflKeyFrameTransformer : public IVflTransformer
{
public:  
	VflKeyFrameTransformer();
	virtual ~VflKeyFrameTransformer();

	enum eKeyFrameMode
	{
		LINEAR =0,
		TIMELEVELSNAP =1
	};

	enum
	{
		MSG_INTERPOLATIONMODE_CHANGE = IVflTransformer::MSG_LAST,
		MSG_TRANSFORMTABLE_CHANGE,
		MSG_LAST
	};

	class VISTAFLOWLIBAPI VflKeyFrame
	{
	public:
		VflKeyFrame();	
		virtual ~VflKeyFrame();

		void   SetTime( double t );
		double GetTime() const;

		void SetPosition(const VistaVector3D &pos);
		void SetPosition(const float x, float y, float z);

		void GetPosition(VistaVector3D &pos) const;
		void GetPosition(float &x, float &y, float&z) const;

		void SetOrientation(const VistaQuaternion &q4Ori);
		void SetOrientation(const float x, float y, float z, float w);
		void GetOrientation(VistaQuaternion &q4Ori) const;

		void SetScale(const float f[3]);
		void GetScale(float f[3]) const;


		// ++++++++++++++++++++++++++++++++++++++++++++++++++
		double           m_dTime;
		VistaVector3D   m_v3Pos;
		VistaQuaternion m_q4Ori;
		float            m_fScale[3];
	};


	// ###############################################################
	// VflTransformer API
	// ###############################################################
	virtual bool GetUnsteadyTransform(double dSimTime, 
		                              VistaTransformMatrix &oStorage);


	
	virtual bool GetDoesTimeLevelSnap() const;
	virtual bool SetDoesTimeLevelSnap(bool bDoesIt);


	// ###############################################################
	// SELF API
	// ###############################################################
	bool        ReadTransformTable(const std::string &sTransformTableName);
	std::string GetCurrentFileName() const;
	bool        SetInterpolationMode(const std::string &sInterpolationMode);

	bool SetInterpolationMode(eKeyFrameMode eMode);

	std::string   GetInterpolationModeName() const;
	eKeyFrameMode GetInterpolationMode() const;

	bool GetKeyFrame(unsigned int nIdx, VflKeyFrame &oStorage) const;
	void SetKeyFrame(unsigned int nIdx, const VflKeyFrame &oFrame);

	// ################################################################
	// VflFactory API
	// ################################################################
	static  std::string GetFactoryType();

	// ################################################################
	// REFLECTIONABLE API
	// ################################################################
	virtual std::string GetReflectionableType() const;	
protected:
    /**
     *  This method <b>must</b> be overloaded by subclasses in order
     * to build a propert hierarchy of base-type strings (i.e. strings 
	 * that encode a unique class identifier). This Method will add its 
	 * base type string as vanilla string to the rBtList.
     * @return the number of entries in the list
     * @param rBtList the list storage to keep track of base types1
     */
    virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

private:

	eKeyFrameMode m_eKFMode;
	std::vector<VflKeyFrame> m_vecKeyFrameParameter;
	VflKeyFrame *m_pCurrentFrameParameter, 
		         *m_pPrevCFP, 
				 *m_pNextCFP;

	VistaVector3D   m_v3T1, 
		             m_v3T2;
	VistaQuaternion m_q4T1, 
		             m_q4T2;

	float m_fTArray[3];

	bool SetCurrentFrameParameter(double dSimTime);
	std::string      m_sFileName;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFLKEYFRAMETRANSFORMER_H

