/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLTOKENTRANSLATOR_H
#define _VFLTOKENTRANSLATOR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include <map>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaPropertyList;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* VflTokenTranslator is a simple wrapper for an stl map which translates an input string to an
* output string according to previously entered Input/Output pairs.
* This can be used (e.g.) to translate from VFL/VFV vocabulary to VCE vocabulary. The idea behind this
* is to specify a consistent translation pattern ONCE and then use it throughout the entire
* application. Any upcoming changes to the vocabulary then only induce a simple change in 
* the initialization code for the translator.
*
* NOTE: An empty token translator will act as identity map!
*
* @author	Bernd Hentschel
* @date		July, 05
*/
class VISTAFLOWLIBAPI VflTokenTranslator
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflTokenTranslator();
    virtual ~VflTokenTranslator();
	
	/**
	* Init the translator from a given VistaPropertyList. This can be used for easy initialization
	* e.g. from a .ini-filr or GUI.
	* For each property P in VistaPropertyList this will create a translation entry which maps
	* P.KEY to P.VALUE.
	*/
	void Init(const VistaPropertyList& oProps);
	/**
	* register a token pair i.e. create an entry in which strInputToken 
	* translates to strTranslatedToken
	*/
	void RegisterToken(const std::string& strInputToken, const std::string& strTranslatedToken);
	/**
	* Retrieve the translated token for strInputToken. 
	* NOTE: If no corresponding entry is found, strInputToken is returned, i.e. the TokenTranslator
	* acts like an identity map in this case!
	*/
	std::string TranslateToken(const std::string& strInputToken) const;
	/**
	* Delete a specific token entry
	*/
	void DeleteToken(const std::string& strInputToken);
	/**
	* Reset the whole token table!
	*/
	void ResetToIdentity();
private:
	std::map<std::string,std::string> m_mapTokenTable;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLTOKENTRANSLATOR_H
