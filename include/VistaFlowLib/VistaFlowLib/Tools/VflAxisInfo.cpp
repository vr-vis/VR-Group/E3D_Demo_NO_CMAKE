/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* PRAGMAS                                                                    */
/*============================================================================*/
#ifdef WIN32
    #pragma warning(disable:4786)
#endif

#include <GL/glew.h>

#include "VflAxisInfo.h"

#include "VflPropertyLoader.h"


#include <VistaAspects/VistaAspectsUtils.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflAxisInfo::VflAxisInfo()
: IVflRenderable(),
  m_pLoader(NULL)
{
//    m_oProperties.m_pParent = this;
}

VflAxisInfo::~VflAxisInfo()
{
  delete m_pLoader;
  m_pLoader=NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
//bool VflAxisInfo::Init()
//{
//    if (!GetVisController())
//        return false;
//    
//#ifdef DEBUG
//    vstr::debugi() << " [VflAxisInfo] - initializing..." << endl;
//    vstr::debugi() << " [VflAxisInfo] - calling base class's initialization..." << endl;
//#endif
//
//    int iRegistrationMode = VflVisController::OLI_DRAW_OPAQUE;
//
//
//	// IAR 
//    //if (!VflVisObject::Init(GetVisController(), iRegistrationMode))
//    //{
//    //    return false;
//    //}
//
//    vstr::debugi() << " [VflAxisInfo] - registered as object " << m_iId << endl;
//    
//    return true;
//}

//bool VflAxisInfo::Init(const VistaPropertyList &refProps)
//{
//    if (!Init())
//        return false;
//    
//    // set values according to property object
//    m_oProperties.SetPropertiesByList(refProps);
//
//    VflResourceKey<VflPropertyLoader> oLoaderKey(refProps);
//    m_pLoader = VflResourceManager::GetResourceManager()->GetResource(oLoaderKey, NULL);
//    if (m_pLoader)
//    {
//        m_pLoader->AddTarget(&m_oProperties);
//		m_pLoader->Execute(&m_oProperties);
//    }
//
//    return true;
//}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflAxisInfo::DrawOpaque()
{
	// IAR
	VflAxisInfoProperties *props = static_cast<VflAxisInfoProperties*>(GetProperties());

	glPushAttrib( GL_LINE_BIT | GL_LIGHTING_BIT );

	glDisable(GL_LIGHTING);
	glLineWidth((*props).m_fLineWidth);

	glBegin(GL_LINES);
		// X axis
		glColor3fv((*props).m_aXCol);
        glVertex3f((*props).m_aXRange[0], 0.0f, 0.0f); 
        glVertex3f((*props).m_aXRange[1], 0.0f, 0.0f);

	    // Y axis
	    glColor3fv((*props).m_aYCol);
        glVertex3f(0.0f, (*props).m_aYRange[0], 0.0f); 
        glVertex3f(0.0f, (*props).m_aYRange[1], 0.0f);

		// Z axis
	    glColor3fv((*props).m_aZCol);
        glVertex3f(0.0f, 0.0f, (*props).m_aZRange[0]); 
        glVertex3f(0.0f, 0.0f, (*props).m_aZRange[1]);
    glEnd();

	glPopAttrib();

}

unsigned int VflAxisInfo::GetRegistrationMode() const
{
	return OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflAxisInfo::GetType() const
{
    return IVflRenderable::GetType()+string("::VflAxisInfo");
}

std::string VflAxisInfo::GetFactoryType()
{
	return std::string("AXIS_INFO");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPropertyObj                                              */
/*                                                                            */
/*============================================================================*/


IVflRenderable::VflRenderableProperties *VflAxisInfo::CreateProperties() const
{
	return new VflAxisInfoProperties;
}


bool VflAxisInfo::GetBounds(VistaVector3D &minBounds, VistaVector3D &maxBounds)
{
	return false;
}

/*============================================================================*/
/*  methods of VflAxisInfo::CProperties                                      */
/*============================================================================*/


static const string SsReflectionType("VflAxisInfo");


static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<float, VflAxisInfo::VflAxisInfoProperties, VistaProperty::PROPT_DOUBLE>
	      ("LINE_WIDTH", SsReflectionType,
		  &VflAxisInfo::VflAxisInfoProperties::GetLineWidth),
    new TVistaProperty2RefGet<float, VflAxisInfo::VflAxisInfoProperties>
	( "X_AXIS_RANGE", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::GetXAxisRange),
    new TVistaProperty2RefGet<float, VflAxisInfo::VflAxisInfoProperties>
	( "Y_AXIS_RANGE", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::GetYAxisRange),
    new TVistaProperty2RefGet<float, VflAxisInfo::VflAxisInfoProperties>
	( "Z_AXIS_RANGE", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::GetZAxisRange),
    new TVistaProperty3RefGet<float, VflAxisInfo::VflAxisInfoProperties>
	( "X_AXIS_COLOR", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::GetXAxisColor),
    new TVistaProperty3RefGet<float, VflAxisInfo::VflAxisInfoProperties>
	( "Y_AXIS_COLOR", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::GetYAxisColor),
    new TVistaProperty3RefGet<float, VflAxisInfo::VflAxisInfoProperties>
	( "Z_AXIS_COLOR", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::GetZAxisColor),
	  NULL
};


static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<float, float,
	                    VflAxisInfo::VflAxisInfoProperties>
		("LINE_WIDTH", SsReflectionType,
		&VflAxisInfo::VflAxisInfoProperties::SetLineWidth),

	new TVistaProperty2ValSet<float, VflAxisInfo::VflAxisInfoProperties>
	( "X_AXIS_RANGE", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::SetXAxisRange),

	new TVistaProperty2ValSet<float, VflAxisInfo::VflAxisInfoProperties>
	( "Y_AXIS_RANGE", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::SetYAxisRange),

	new TVistaProperty2ValSet<float, VflAxisInfo::VflAxisInfoProperties>
	( "Z_AXIS_RANGE", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::SetZAxisRange),

	new TVistaProperty3ValSet<float, VflAxisInfo::VflAxisInfoProperties>
	( "X_AXIS_COLOR", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::SetXAxisColor),

	new TVistaProperty3ValSet<float, VflAxisInfo::VflAxisInfoProperties>
	( "Y_AXIS_COLOR", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::SetYAxisColor),

	new TVistaProperty3ValSet<float, VflAxisInfo::VflAxisInfoProperties>
	( "Z_AXIS_COLOR", SsReflectionType,
	&VflAxisInfo::VflAxisInfoProperties::SetZAxisColor),

	NULL
};



VflAxisInfo::VflAxisInfoProperties::VflAxisInfoProperties()
: IVflRenderable::VflRenderableProperties(),
  m_fLineWidth(2.0f)
{
    m_aXCol[0] = 1.0f;
    m_aXCol[1] = 0.0f;
    m_aXCol[2] = 0.0f;

    m_aXRange[0] = 0.0f;
    m_aXRange[1] = 1.0f;

    m_aYCol[0] = 0.0f;
    m_aYCol[1] = 1.0f;
    m_aYCol[2] = 0.0f;

    m_aYRange[0] = 0.0f;
    m_aYRange[1] = 1.0f;

    m_aZCol[0] = 0.5f;
    m_aZCol[1] = 0.5f;
    m_aZCol[2] = 1.0f;

    m_aZRange[0] = 0.0f;
    m_aZRange[1] = 1.0f;
}


VflAxisInfo::VflAxisInfoProperties::~VflAxisInfoProperties()
{
}

string VflAxisInfo::VflAxisInfoProperties::GetReflectionableType() const
{
	return SsReflectionType;
}

int VflAxisInfo::VflAxisInfoProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = VflRenderableProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}



bool  VflAxisInfo::VflAxisInfoProperties::GetXAxisRange(float &s, float &e) const
{
	s = m_aXRange[0];
	e = m_aXRange[1];
	return true;
}

bool  VflAxisInfo::VflAxisInfoProperties::SetXAxisRange(float s, float e)
{
	bool bChange =            (compAndAssignFunc<float>(s, m_aXRange[0]) == 1) ? true : false;
	bChange      = ((compAndAssignFunc<float>(e, m_aXRange[1]) == 1) ? true : false) || bChange;

	if(bChange)
	{
		Notify(MSG_X_RANGE_CHANGE);
		return true;
	}
	return false;
}


bool  VflAxisInfo::VflAxisInfoProperties::GetYAxisRange(float &s, float &e) const
{
	s = m_aYRange[0];
	e = m_aYRange[1];
	return true;
}

bool  VflAxisInfo::VflAxisInfoProperties::SetYAxisRange(float s, float e)
{
	bool bChange =            (compAndAssignFunc<float>(s, m_aYRange[0]) == 1) ? true : false;
	bChange      = ((compAndAssignFunc<float>(e, m_aYRange[1]) == 1) ? true : false) || bChange;

	if(bChange)
	{

		Notify(MSG_Y_RANGE_CHANGE);
		return true;
	}

	return false;
}

bool  VflAxisInfo::VflAxisInfoProperties::GetZAxisRange(float &s, float &e) const
{
	s = m_aZRange[0];
	e = m_aZRange[1];
	return true;
}

bool  VflAxisInfo::VflAxisInfoProperties::SetZAxisRange(float s, float e)
{
	bool bChange =            (compAndAssignFunc<float>(s, m_aZRange[0]) == 1) ? true : false;
	bChange      = ((compAndAssignFunc<float>(e, m_aZRange[1]) == 1) ? true : false) || bChange;

	if(bChange)
	{
		Notify(MSG_Z_RANGE_CHANGE);
		return true;
	}
	return false;
}


float VflAxisInfo::VflAxisInfoProperties::GetLineWidth() const
{
	return m_fLineWidth;
}

bool  VflAxisInfo::VflAxisInfoProperties::SetLineWidth(float fWidth)
{
	if(compAndAssignFunc<float>(fWidth, m_fLineWidth))
	{
		Notify(MSG_LINE_WIDTH_CHANGE);
		return true;
	}
	return false;
}


bool  VflAxisInfo::VflAxisInfoProperties::GetXAxisColor(float &r, float &g, float &b) const
{
	r = m_aXCol[0];
	g = m_aXCol[1];
	b = m_aXCol[2];
	return true;
}

bool  VflAxisInfo::VflAxisInfoProperties::SetXAxisColor(float r, float g, float b)
{
	bool bChange =            (compAndAssignFunc<float>(r, m_aXCol[0]) == 1) ? true : false;
	bChange      = ((compAndAssignFunc<float>(g, m_aXCol[1]) == 1) ? true : false) || bChange;
	bChange      = ((compAndAssignFunc<float>(b, m_aXCol[1]) == 1) ? true : false) || bChange;
	if(bChange)
	{
		Notify(MSG_X_COLOR_CHANGE);
		return true;
	}
	return false;
}


bool  VflAxisInfo::VflAxisInfoProperties::GetYAxisColor(float &r, float &g, float &b) const
{
	r = m_aXCol[0];
	g = m_aXCol[1];
	b = m_aXCol[2];
	return true;
}

bool  VflAxisInfo::VflAxisInfoProperties::SetYAxisColor(float r, float g, float b)
{
	bool bChange =            (compAndAssignFunc<float>(r, m_aYCol[0]) == 1) ? true : false;
	bChange      = ((compAndAssignFunc<float>(g, m_aYCol[1]) == 1) ? true : false) || bChange;
	bChange      = ((compAndAssignFunc<float>(b, m_aYCol[1]) == 1) ? true : false) || bChange;
	if(bChange)
	{
		Notify(MSG_Y_COLOR_CHANGE);
		return true;
	}
	return false;
}


bool  VflAxisInfo::VflAxisInfoProperties::GetZAxisColor(float &r, float &g, float &b) const
{
	r = m_aXCol[0];
	g = m_aXCol[1];
	b = m_aXCol[2];
	return true;
}

bool  VflAxisInfo::VflAxisInfoProperties::SetZAxisColor(float r, float g, float b)
{
	bool bChange =            (compAndAssignFunc<float>(r, m_aZCol[0]) == 1) ? true : false;
	bChange      = ((compAndAssignFunc<float>(g, m_aZCol[1]) == 1) ? true : false) || bChange;
	bChange      = ((compAndAssignFunc<float>(b, m_aZCol[1]) == 1) ? true : false) || bChange;
	if(bChange)
	{
		Notify(MSG_Z_COLOR_CHANGE);
		return true;
	}
	return false;

}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflAxisInfo.cpp"                                           */
/*============================================================================*/
