/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VFLINITOPROPLIST_H
#define _VFLINITOPROPLIST_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaPropertyAwareable.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflIniToPropList
{
public:
	/** 
         * Creates an ini file to proplist converter. The given VistaPropertyList
	 * defines the vocabulary to be understood. The keys in this list
	 * contain the type flags for the ini file sections to be parsed
	 * and the values contain another VistaPropertyList, which defines the keys
	 * in the said section, which point to other sections, i.e. to
	 * other VistaPropertyListS, or to lists of sections, i.e. to lists of 
	 * VistaPropertyListS, respectively. These keys are given as members of 
	 * string lists for the VistaPropertyList keys "VistaPropertyListS" and 
	 * "VistaPropertyListLISTS".
	 * UPDATE: Additionally, "VistaPropertyListLIST_ORDERED" specifies a list of
	 *         VistaPropertyListS/sections, which are prefixed with a counter
	 *         to maintain their order.
	 * Well, this might have been quite complicated, so let's go for an
	 * example:
	 *	
	 * Given the VistaPropertyList oTypes with
	 * {
	 * 	TYPE_1 =	{
	 * 		VistaPropertyListS = "KEY_1_1", "KEY_1_2"
	 * 		VistaPropertyListLISTS = "KEY_1_3", "KEY_1_4"
	 *      VistaPropertyListLISTS_ORDERED = "KEY_1_5"
	 *				}
	 *	TYPE_2 =	{
	 *		VistaPropertyListS = "KEY_2_1"
	 *				}
	 *	TYPE_3 =	{
	 *		VistaPropertyListLISTS = "KEY_3_1"
	 *				}
	 * }
	 * an ini file to proplist converter will parse
	 * the following subsections into sub-VistaPropertyListS:
	 * For TYPE = TYPE_1:
	 * 	    KEY_1_1 and KEY_1_2 give sub-VistaPropertyListS,
	 * 	    KEY_1_3 and KEY_1_4 give lists of sub-VistaPropertyListS.
	 *      KEY_1_5 gives a prefixed and ordered list of sub-VistaPropertyListS.
	 * For TYPE = TYPE_2:
	 * 	    KEY_2_1 gives a sub-VistaPropertyList.
	 * For TYPE = TYPE_3:
	 * 	    KEY_3_1 gives a list of sub-VistaPropertyListS.
	 */
	VflIniToPropList(const VistaPropertyList &oTypes);

    /**
     * Creates an ini file to proplist converter. The given
     * ini section in the given ini file is supposed to define
     * the types being known. The example above would read:
     *
     * [TYPE_1]
     * VistaPropertyListS        =   KEY_1_1, KEY_1_2
     * VistaPropertyListLISTS    =   KEY_1_3, KEY_1_4
	 * VistaPropertyListLISTS_ORDERED = KEY_1_5
     *
     * [TYPE_2]
     * VistaPropertyListS        =   KEY_2_1
     *
     * [TYPE_3]
     * VistaPropertyListLISTS    =   KEY_3_1
     *
     */
    VflIniToPropList(const std::string &strIniFile);

    virtual ~VflIniToPropList();

	static VistaPropertyList ReadIniSection(const std::string &strSection,
		const std::string &strIniFile, const VistaPropertyList &refTypes);

	VistaPropertyList ReadIniSection(const std::string &strSection,
		const std::string &strIniFile);

    static int FillPropertyList(VistaPropertyList &refProps, const std::string &strIniSection, 
        const std::string &strIniFile, bool bConvertKeyToUpper);

protected:
	/**
	 * This method converts a type list as given in the constructor into
	 * a key lists as given in the member variable m_oKeyLists.
	 */
	static void ConvertToKeyLists(const VistaPropertyList &oSource, VistaPropertyList &oDest);

	/**
	 * Read an ini section according to the given type list.
	 */
	static void ReadIniSection(VistaPropertyList &refDest,
		const std::string &strSection, const std::string &strIniFile,
		const VistaPropertyList &refKeys);

	/**
	 * This VistaPropertyList gives a type list, for which keys are defined as containing
	 * sub-VistaPropertyListS and lists of sub-VistaPropertyListS.
	 * Example:
	 * {
	 *		TYPE_1 =	{
	 *			KEY_1_1 = "VistaPropertyList"
	 *			KEY_1_2 = "VistaPropertyList"
	 *			KEY_1_3 = "VistaPropertyListLIST"
	 *			KEY_1_4 = "VistaPropertyListLIST"
	 *          KEY_1_5 = "VistaPropertyListLIST_ORDERED"
	 *					}
	 *		TYPE_2 =	{
	 *			KEY_2_1 = "VistaPropertyList"
	 *					}
	 *		TYPE_3 =	{
	 *			KEY_3_1 = "VistaPropertyListLIST"
	 *					}
	 *	}
	 */
	VistaPropertyList	m_oKeyLists;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLINITOVistaPropertyList_H
