/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflAxisRotateTransformer.h"
#include <VistaVisExt/Tools/VveUtils.h>
#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaBase/VistaExceptionBase.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string SVflAxisRotateTransformerType("VflAxisRotateTransformer");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<VistaVector3D, VflAxisRotateTransformer, VistaProperty::PROPT_LIST>
	      ("AXIS", SVflAxisRotateTransformerType,
	      &VflAxisRotateTransformer::GetAxis ),
	new TVistaPropertyGet<float, VflAxisRotateTransformer, VistaProperty::PROPT_DOUBLE>
	      ("VELOCITY", SVflAxisRotateTransformerType,
	      &VflAxisRotateTransformer::GetVelocity),	      
	new TVistaPropertyGet<VistaVector3D, VflAxisRotateTransformer, VistaProperty::PROPT_LIST>
	      ("ORIGIN", SVflAxisRotateTransformerType,
	      &VflAxisRotateTransformer::GetOrigin ),
	new TVistaPropertyGet<std::string, VflAxisRotateTransformer, VistaProperty::PROPT_STRING>
	      ("TIME_FILE", SVflAxisRotateTransformerType,
	      &VflAxisRotateTransformer::GetTimeFile),	 	      	      
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<const VistaVector3D &, VistaVector3D,
	                    VflAxisRotateTransformer>
		("AXIS", SVflAxisRotateTransformerType,
		&VflAxisRotateTransformer::SetAxis),
	new TVistaPropertySet<const VistaVector3D &, VistaVector3D,
	                    VflAxisRotateTransformer>
		("ORIGIN", SVflAxisRotateTransformerType,
		&VflAxisRotateTransformer::SetOrigin),
	new TVistaPropertySet<float, float,
	                    VflAxisRotateTransformer>
		("VELOCITY", SVflAxisRotateTransformerType,
		&VflAxisRotateTransformer::SetVelocity),
	new TVistaPropertySet<const std::string &, std::string,
	                    VflAxisRotateTransformer>
		("TIME_FILE", SVflAxisRotateTransformerType,
		&VflAxisRotateTransformer::SetTimeFile),		
	NULL
};
//  		string axis = props("AXIS").GetValue();
// 		string vel  = props("VELOCITY").GetValue();
// 		string origin = props("ORIGIN").GetValue();


/*============================================================================*/
/*  IMPLEMENTATION FOR VflConversionStuff                                    */
/*============================================================================*/
VflAxisRotateTransformer::VflAxisRotateTransformer()
: m_afOrigin(VistaVector3D(0,0,0)),
  m_afAxis(VistaVector3D(0,0,1)),
  m_afVelocity(1.0f),
  m_matV(VistaVector3D()),
  m_matVm(VistaVector3D()),
  m_pSnapMapper(NULL)
{
}

VflAxisRotateTransformer::VflAxisRotateTransformer(const VistaVector3D &rotationAxis,
								float fVelocity,
								const VistaVector3D &origin)
								: m_afOrigin(origin),
								  m_afAxis(rotationAxis),
								  m_afVelocity(fVelocity),
								  m_matV(-origin),
								  m_matVm(origin),
								  m_pSnapMapper(NULL)
{
}


VflAxisRotateTransformer::~VflAxisRotateTransformer()
{

}



bool VflAxisRotateTransformer::GetUnsteadyTransform(double dSimTime, 
	                              VistaTransformMatrix &oStorage)
{
	if(GetDoesTimeLevelSnap() && m_pSnapMapper)
	{
		// convert sim time
		double fVisTime = m_pSnapMapper->GetVisualizationTime(float(dSimTime));
		int nIdx = m_pSnapMapper->GetTimeIndex(fVisTime);
		
		// get time index from mapper
		dSimTime = m_pSnapMapper->GetSimulationTime(nIdx);
	}
	
	
	VistaTransformMatrix ma(VistaAxisAndAngle(m_afAxis, 
				Vista::DegToRad(float(double(m_afVelocity)*dSimTime))));
	
	oStorage = m_matVm*ma*m_matV;

	return true;
}



VistaVector3D VflAxisRotateTransformer::GetOrigin() const
{
	return m_afOrigin;
}

void           VflAxisRotateTransformer::GetOrigin(VistaVector3D &oStore)
{
	oStore = m_afOrigin;
}

bool           VflAxisRotateTransformer::SetOrigin(const VistaVector3D &oReplace)
{
	return SetOrigin((float*)&oReplace[0]);
}

bool VflAxisRotateTransformer::SetOrigin(float afOrigin[3])
{
	bool bChange = compAndAssignFunc<float>(afOrigin[0], m_afOrigin[0]) == 1 ? true : false;
	bChange = compAndAssignFunc<float>(afOrigin[1], m_afOrigin[1]) || bChange;
	bChange = compAndAssignFunc<float>(afOrigin[2], m_afOrigin[2]) || bChange;
	if(bChange)
	{
		m_matV = VistaTransformMatrix(-m_afOrigin);
		m_matVm = VistaTransformMatrix(m_afOrigin);
		Notify(MSG_ORIGIN_CHANGE);
		return true;
	}
	return false;
}



VistaVector3D VflAxisRotateTransformer::GetAxis() const
{
	return m_afAxis;
}

void           VflAxisRotateTransformer::GetAxis(VistaVector3D &oStorage)
{
	oStorage = m_afAxis;
}


bool           VflAxisRotateTransformer::SetAxis(const VistaVector3D &oAxis)
{
	return SetAxis((float*)&oAxis[0]);
}

bool           VflAxisRotateTransformer::SetAxis(float afAxis[3])
{
	bool bChange = compAndAssignFunc<float>(afAxis[0], m_afAxis[0]) == 1 ? true : false;
	bChange = compAndAssignFunc<float>(afAxis[1], m_afAxis[1]) || bChange;
	bChange = compAndAssignFunc<float>(afAxis[2], m_afAxis[2]) || bChange;
	if(bChange)
	{
		Notify(MSG_AXIS_CHANGE);
		return true;
	}
	return false;
}



float          VflAxisRotateTransformer::GetVelocity() const
{
	return m_afVelocity;
}

bool           VflAxisRotateTransformer::SetVelocity(float fVel)
{
	if(compAndAssignFunc<float>(fVel, m_afVelocity) == 1)
	{
		Notify(MSG_VELOCITY_CHANGE);
		return true;
	}
	return false;
}


bool VflAxisRotateTransformer::SetTimeFile(const std::string &sTimeFile)
{
	VISTA_THROW("NOT WORKING, PLEASE FIX",-1)
	return false;
}
/*******************************************************************
HI
	I commented a Working method here because i removed the Ressourcemanager and the Timemapper
	unfortunately i dont know how to parse a VveTimeMapper from a Timefile.
	So if you want to use this method feel free to fix it.
AH
***********************************************************************/
	
// anything new here?
// 	if(compAndAssignFunc<std::string>(sTimeFile, m_sTimeFile) == 1)
// 	{
// 		if(m_pSnapMapper)
// 		{
// 			// free time mapper
// 			delete m_pSnapMapper;
// 			m_pSnapMapper = NULL;			
// 		}
// 
// 		// check if this is a valid file (means: is it empty?)
// 		if(m_sTimeFile.empty())
// 		{	
// 			SetDoesTimeLevelSnap(false);
// 			Notify(MSG_TIMER_FILE_CHANGE);
// 			// we return true, 
// 			// as we changed some values of this instance
// 			return true; 
// 		}
// 		
// 		
// 		VflResourceKey<VflTimeMapper> oKey(sTimeFile);
// 		m_pSnapMapper = pMgr->GetResource(oKey, NULL);
// 		
// 
// 		if(!m_pSnapMapper)
// 			SetDoesTimeLevelSnap(false);
// 		
// 		Notify(MSG_TIMER_FILE_CHANGE);
// 		return true;			
// 	}
// 	return false; // we did not change anything
//}

std::string VflAxisRotateTransformer::GetTimeFile() const
{
	return m_sTimeFile;
}


std::string VflAxisRotateTransformer::GetReflectionableType() const
{
	return SVflAxisRotateTransformerType;
}


int VflAxisRotateTransformer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nRet = IVflTransformer::AddToBaseTypeList(rBtList);
	rBtList.push_back(SVflAxisRotateTransformerType);
	return nRet+1;
}

std::string VflAxisRotateTransformer::GetFactoryType()
{
	return std::string("AXIS_ROTATE");
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflUtils.cpp"                                                */
/*============================================================================*/
