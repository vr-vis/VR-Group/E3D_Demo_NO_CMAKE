/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <GL/glew.h>

#include "Vfl3DTextLabel.h"

#include <VistaVisExt/Tools/VtkObjectStack.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

#include <VistaMath/VistaVector.h>
#include <VistaBase/VistaVectorMath.h>
#include <VistaMath/VistaBoundingBox.h>
#include <VistaAspects/VistaAspectsUtils.h>

#include <VistaKernel/VistaSystem.h>
#include <VistaKernel/DisplayManager/VistaDisplayManager.h>
#include <VistaKernel/DisplayManager/VistaDisplaySystem.h>
#include <VistaKernel/DisplayManager/VistaVirtualPlatform.h>

#include <VistaKernel/GraphicsManager/VistaGeometry.h>

#include <vtkPolyDataMapper.h>
#include <vtkVectorText.h>
#include <vtkRenderer.h>
#include <vtkProperty.h>
#include <vtkMatrix4x4.h>

#include <cassert>

using namespace std;


/*============================================================================*/
/*  IMPLEMENTATION															  */
/*============================================================================*/
Vfl3DTextLabel::Vfl3DTextLabel()
: 	m_bIsDirty(true)
,	m_pObjectStack(new VtkObjectStack)
, 	m_pTextSource(vtkVectorText::New())
,	m_pTextProperty(vtkProperty::New())
, 	m_pTextActor(vtkActor::New())
,	m_pRefFrame(NULL)
{
	VistaSystem* pVS = GetVistaSystem();
	VistaDisplaySystem* pDS = pVS->GetDisplayManager()->GetDisplaySystem();
	m_pRefFrame = pDS->GetReferenceFrame();
	
	m_pTextSource->SetText("");
	(*m_pObjectStack).PushObject(m_pTextSource);
	
	vtkPolyDataMapper *pMapper = vtkPolyDataMapper::New();
	pMapper->ImmediateModeRenderingOn();
	pMapper->ScalarVisibilityOff();
	
	(*m_pObjectStack).PushObject(pMapper);
	pMapper->SetInputConnection(m_pTextSource->GetOutputPort());
	
	m_pTextProperty->SetInterpolationToFlat();
	(*m_pObjectStack).PushObject(m_pTextProperty);

	(*m_pObjectStack).PushObject(m_pTextActor);
	m_pTextActor->SetMapper(pMapper);
	
	m_pTextProperty->SetAmbient(0.2f);
	m_pTextProperty->SetDiffuse(1.0f);
	m_pTextProperty->SetSpecular(0.0f);
	
	double aColor[4] = {1.0, 1.0, 1.0, 1.0};
	m_pTextProperty->SetAmbientColor(aColor);
	m_pTextProperty->SetDiffuseColor(aColor);
	m_pTextProperty->SetSpecularColor(aColor);
	m_pTextProperty->SetSpecularPower(0.0);
	// We don't need no lighting for text labels!
	m_pTextProperty->SetRepresentationToSurface();
	m_pTextProperty->SetInterpolationToFlat();
	
	m_pTextActor->SetProperty(m_pTextProperty);
	
	//create a user matrix for the actor because we will most likely need it!
	vtkMatrix4x4 *pMatrix = vtkMatrix4x4::New();
	pMatrix->Identity();
	m_pTextActor->SetUserMatrix(pMatrix);
	//remeber to delete our reference to the matrix!
	pMatrix->Delete();
	pMapper->Update();
}

Vfl3DTextLabel::~Vfl3DTextLabel()
{
	(*m_pObjectStack).DeleteObjects();
	delete m_pObjectStack;
	if(GetRenderNode())
		GetRenderNode()->RemoveRenderable(this);
}


bool Vfl3DTextLabel::GetIsDirty() const
{
	return m_bIsDirty;
}


void Vfl3DTextLabel::Update()
{
	m_pTextActor->GetMapper()->Update();

	// Set scale anyway!
	VflTextProperties *pProps = GetProperties();
	m_pTextActor->SetScale(pProps->GetTextSize());
	const VistaVector3D v3TextPos = pProps->GetPosition() + pProps->GetOffset();
	
	VistaVector3D v3Normal = VistaVector3D(0.0f, 0.0f, 1.0f);
	VistaVector3D v3Up = VistaVector3D(0.0f, 1.0f, 0.0f);
	VistaVector3D v3Right = VistaVector3D(1.0f, 0.0f, 0.0f);

	if(!pProps->GetTextFollowViewDir())
	{
		const VistaQuaternion qOri = GetProperties()->GetOrientation();

		v3Normal = qOri.Rotate(v3Normal);
		v3Up = qOri.Rotate(v3Up);
		v3Right	= qOri.Rotate(v3Right);
	}
	else
	{
		//reset position because it will now be integrated in the user matrix.
		m_pTextActor->SetPosition(0, 0, 0);

		const VistaVector3D v3ViewPos = GetRenderNode()->GetLocalViewPosition();
	
		//view position has changed? (well we assume that it always changes along with the direction!)
		if(v3ViewPos == pProps->GetPosition() && !m_bIsDirty)
			return;
		m_bIsDirty = false;

		// The normal the label has to acquire to face the user.	
		v3Normal = (v3ViewPos - v3TextPos).GetNormalized();
	
		// The text label needs to have a consistent up direction w.r.t. the
		// ground. This is defined by the up direction of the virtual platform's
		// reference frame. To be able to use it, the up direction first needs
		// to be translated into the render node's coordinate frame. Then, the
		// full coordinate system of the label can be created.
		VistaTransformMatrix mPlatfrom = m_pRefFrame->GetMatrix();
		v3Up = mPlatfrom.TransformVector(v3Up).GetNormalized();

		VistaTransformMatrix mRN = GetRenderNode()->GetInverseTransform();
		v3Up = mRN.TransformVector(v3Up).GetNormalized();
		v3Right = v3Up.Cross(v3Normal).GetNormalized();
		v3Up = v3Normal.Cross(v3Right).GetNormalized();

		// As the orientation can be accessed by the user via the properties,
		// the changes made by automatically orienting towards the user should
		// be relfected there.
		float fMatrix[4][4] = {
			v3Right[0], v3Up[0], v3Normal[0], 0,
			v3Right[1], v3Up[1], v3Normal[1], 0,
			v3Right[2], v3Up[2], v3Normal[2], 0,
			0,			0,		 0,			  1
		};
		VistaTransformMatrix mat(fMatrix);		
		VistaQuaternion qOri(mat);
		GetProperties()->SetOrientation(qOri);
	}

	// Create the text's new transformation matrix.
	float fMatrix[4][4] = {
		v3Right[0], v3Up[0], v3Normal[0], v3TextPos[0],
		v3Right[1], v3Up[1], v3Normal[1], v3TextPos[1],
		v3Right[2], v3Up[2], v3Normal[2], v3TextPos[2],
		0,			0,		 0,			  1
	};

	// Retrieve text transformation, reset it, and extract extents.
	vtkMatrix4x4 *pMatrix = m_pTextActor->GetUserMatrix();
	assert(pMatrix != NULL && "User matrix should have been set in INIT!");
	pMatrix->Identity();
	m_pTextActor->GetBounds(m_aVtkBounds);

	// Feed new matrix in.
	for(register int i=0; i<4; ++i)
		for(register int j=0; j<4; ++j)
			pMatrix->SetElement(i, j, fMatrix[i][j]);
}


void Vfl3DTextLabel::DrawOpaque()
{
	if(!this->GetProperties()->GetVisible())
		return;
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	m_pTextActor->RenderOpaqueGeometry(GetRenderNode()->GetVtkRenderer());
	glPopAttrib();
}


unsigned int Vfl3DTextLabel::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE | IVflRenderable::OLI_UPDATE;
}


bool Vfl3DTextLabel::GetBounds(VistaVector3D &minBounds,
	VistaVector3D &maxBounds)
{
	double *d =	m_pTextActor->GetBounds();
	for(int i=0; i<3; ++i)
	{
		minBounds[i] = d[2*i];
		maxBounds[i] = d[2*i+1];
	}
	return false;
}


bool Vfl3DTextLabel::GetBounds(VistaBoundingBox &b)
{
	VistaVector3D v3Min, v3Max;
	GetBounds(v3Min, v3Max);
	b.SetBounds(v3Min, v3Max);
	return false;
}


bool Vfl3DTextLabel::GetOrientedBounds(VistaVector3D& v3Position,
	VistaVector3D& v3Right, VistaVector3D& v3Up)
{
	const VistaQuaternion qOri = GetProperties()->GetOrientation();
	v3Right = qOri.Rotate(VistaVector3D(1,0,0)) * (m_aVtkBounds[1] - m_aVtkBounds[0]);
	v3Up = qOri.Rotate(VistaVector3D(0,1,0)) * (m_aVtkBounds[3] - m_aVtkBounds[2]);
	v3Position = GetProperties()->GetPosition()
		+ m_aVtkBounds[0] * v3Right.GetNormalized()
		+ m_aVtkBounds[2] * v3Up.GetNormalized();
	// m_aVtkBounds index 5 and 6 are 0 as there is no extent.
	return false;
}


IVflRenderable::VflRenderableProperties* Vfl3DTextLabel::CreateProperties() const
{
	return new VflTextProperties;
}


void Vfl3DTextLabel::SetText(const std::string& strT)
{
	GetProperties()->SetText(strT);
}

std::string Vfl3DTextLabel::GetText() const
{
	return GetProperties()->GetText();
}


void Vfl3DTextLabel::SetPosition(const VistaVector3D &v3Pos)
{
	m_bIsDirty = GetProperties()->SetPosition(v3Pos);
}

void Vfl3DTextLabel::SetPosition(double dPos[3])
{
	GetProperties()->SetPosition( VistaVector3D(dPos[0], dPos[1], dPos[2]) );
}

void Vfl3DTextLabel::SetPosition(float fPos[3])
{
	GetProperties()->SetPosition( VistaVector3D(fPos[0], fPos[1], fPos[2]) );
}

void Vfl3DTextLabel::SetPositionExact(const VistaVector3D& v3Position)
{
	SetPosition(v3Position);
	// Due to some accuracy issues, a correction has to be applied
	// to the target position. We use the difference between the actual
	// target position and the position returned by the bounds code.
	VistaVector3D v3Origin, v3Right, v3Up;
	GetOrientedBounds(v3Origin, v3Right, v3Up);
	const VistaVector3D v3VtkCorrection = GetPosition() - v3Origin;
	SetPosition(v3Position + v3VtkCorrection);
}


VistaVector3D Vfl3DTextLabel::GetPosition() const
{
	return GetProperties()->GetPosition();
}

void Vfl3DTextLabel::GetPosition(VistaVector3D &v3Pos) const
{
	v3Pos = GetPosition();
}

void Vfl3DTextLabel::GetPosition(double dPos[3]) const
{
	VistaVector3D v3Pos = GetPosition();
	dPos[0] = v3Pos[0];
	dPos[1] = v3Pos[1];
	dPos[2] = v3Pos[2];
}

void Vfl3DTextLabel::GetPosition(float fPos[3]) const
{
	VistaVector3D v3Pos = GetPosition();
	v3Pos.GetValues(fPos);
}


void Vfl3DTextLabel::SetOrientation(const VistaQuaternion& qOri)
{
	GetProperties()->SetOrientation(qOri);
}

VistaQuaternion Vfl3DTextLabel::GetOrientation() const
{
	return GetProperties()->GetOrientation();
}

void Vfl3DTextLabel::GetOrientation(VistaQuaternion& qOri) const
{
	qOri = GetOrientation();
}


Vfl3DTextLabel::VflTextProperties *Vfl3DTextLabel::GetProperties() const
{
	return static_cast<VflTextProperties*>(IVflRenderable::GetProperties());
}


void Vfl3DTextLabel::SetTextFollowViewDir(bool bFollowViewDir)
{
	GetProperties()->SetTextFollowViewDir(bFollowViewDir);
}

bool Vfl3DTextLabel::GetTextFollowViewDir() const
{
	return GetProperties()->GetTextFollowViewDir();
}


void Vfl3DTextLabel::SetColor(const VistaColor& color, float fOpacity)
{
	float fColor[4];
	color.GetValues(fColor);
	fColor[3] = fOpacity;
	this->SetColor(fColor);
}

void Vfl3DTextLabel::SetColor(float fColor[4])
{
	double dColor[4]={fColor[0], fColor[1], fColor[2], fColor[3]};
	//m_pTextProperty->SetDiffuseColor(dColor);
	GetProperties()->SetColor( dColor );
}


VistaColor Vfl3DTextLabel::GetColor() const
{
	float fColor[4];
	this->GetColor(fColor);
	return VistaColor(fColor);
}

void Vfl3DTextLabel::GetColor(float fColor[4]) const
{
	double dColor[4]= {0.0, 0.0, 0.0, 1.0};
	GetProperties()->GetColor(dColor);
	fColor[0] = dColor[0]; fColor[1] = dColor[1]; 
	fColor[2] = dColor[2]; fColor[3] = dColor[3]; 
}


float Vfl3DTextLabel::GetOpacity() const
{
	double dColor[4];
	GetProperties()->GetColor(dColor);
	return dColor[3];
}


void Vfl3DTextLabel::SetTextSize(float fSize)
{
	GetProperties()->SetTextSize(fSize);
}

float Vfl3DTextLabel::GetTextSize() const
{
	return GetProperties()->GetTextSize();
}


void  Vfl3DTextLabel::Debug(std::ostream & out) const
{
	out << " [Vfl3DTextLabel] Type           :  " << this->GetType() << endl;
	out << " [Vfl3DTextLabel] Name           :  " << this->GetNameForNameable() << endl;
	out << " [Vfl3DTextLabel] RenderNode     :  " << this->GetRenderNode() << endl;
	out << " [Vfl3DTextLabel] Id             :  " << this->GetId() << endl;
	out << " [Vfl3DTextLabel] Text           :  " << this->GetText() << endl;
	out << " [Vfl3DTextLabel] Given Position :  " << GetProperties()->GetPosition() << endl;
	out << " [Vfl3DTextLabel] FollowViewer   :  " << (GetProperties()->GetTextFollowViewDir() ? "TRUE" : "FALSE") << endl;
	double *d = m_pTextActor->GetPosition();
	out << " [Vfl3DTextLabel] Actor position :  " << d[0] << ", " << d[1] << ", " << d[2] << endl;
	d = m_pTextActor->GetOrigin();
	out << " [Vfl3DTextLabel] Actor origin   :  " << d[0] << ", " << d[1] << ", " << d[2] << endl;
	d = m_pTextActor->GetScale();
	out << " [Vfl3DTextLabel] Actor scale    :  " << d[0] << ", " << d[1] << ", " << d[2] << endl;
	d = m_pTextActor->GetBounds();
	out << " [Vfl3DTextLabel] Actor bounds   : ";
	out << " X: " << d[0] << "<>" << d[1];
	out << " Y: " << d[2] << "<>" << d[3];
	out << " Z: " << d[4] << "<>" << d[5] << endl;
}


std::string Vfl3DTextLabel::GetType() const
{
	return "Vfl3DTextLabel";
}


vtkProperty *Vfl3DTextLabel::GetRenderProperty() const
{
	return m_pTextProperty;
}

////////////////////////////////////////////////////////////////////////////////

static const std::string SsReflectionName = "Vfl3DTextLabel::VflTextProperties";

REFL_IMPLEMENT( Vfl3DTextLabel::VflTextProperties, IVflRenderable::VflRenderableProperties );

namespace 
{
	IVistaPropertyGetFunctor *aCgFunctors[] =
	{
		new TVistaPropertyGet<float, Vfl3DTextLabel::VflTextProperties, VistaProperty::PROPT_DOUBLE>
			  ("SIZE", SsReflectionName,
			  &Vfl3DTextLabel::VflTextProperties::GetTextSize),
		new TVistaPropertyGet<VistaVector3D, Vfl3DTextLabel::VflTextProperties, VistaProperty::PROPT_LIST>
			  ("POSITION", SsReflectionName,
			  &Vfl3DTextLabel::VflTextProperties::GetPosition),
		new TVistaPropertyGet<VistaVector3D, Vfl3DTextLabel::VflTextProperties, VistaProperty::PROPT_LIST>
			  ("OFFSET", SsReflectionName,
			  &Vfl3DTextLabel::VflTextProperties::GetOffset),
 	    new TVistaPropertyGet<bool, Vfl3DTextLabel::VflTextProperties, VistaProperty::PROPT_BOOL>
			  ("FOLLOWVIEW", SsReflectionName,
			  &Vfl3DTextLabel::VflTextProperties::GetTextFollowViewDir),
		new TVistaPropertyGet<std::string, Vfl3DTextLabel::VflTextProperties, VistaProperty::PROPT_STRING>
			  ("TEXT", SsReflectionName,
			  &Vfl3DTextLabel::VflTextProperties::GetText),
			  new TVistaPropertyArrayGet<Vfl3DTextLabel::VflTextProperties, double, 4>
			  ( "COLOR", SsReflectionName,
			  &Vfl3DTextLabel::VflTextProperties::GetColor),
		NULL
	};

	IVistaPropertySetFunctor *aCsFunctors[] =
	{
		new TVistaPropertySet<float, float,
							Vfl3DTextLabel::VflTextProperties>
			("SIZE", SsReflectionName,
			&Vfl3DTextLabel::VflTextProperties::SetTextSize ),
		new TVistaPropertySet<const VistaVector3D &, VistaVector3D,
							Vfl3DTextLabel::VflTextProperties>
			("POSITION", SsReflectionName,
			&Vfl3DTextLabel::VflTextProperties::SetPosition),
		new TVistaPropertySet<const VistaVector3D &, VistaVector3D,
							Vfl3DTextLabel::VflTextProperties>
			("OFFSET", SsReflectionName,
			&Vfl3DTextLabel::VflTextProperties::SetOffset),
		new TVistaPropertySet<bool, bool,
							Vfl3DTextLabel::VflTextProperties>
			("FOLLOWVIEW", SsReflectionName,
			&Vfl3DTextLabel::VflTextProperties::SetTextFollowViewDir),
		new TVistaPropertySet<const std::string&,std::string,
						Vfl3DTextLabel::VflTextProperties>
		("TEXT", SsReflectionName,
		&Vfl3DTextLabel::VflTextProperties::SetText),

		new TVistaPropertyArraySet<Vfl3DTextLabel::VflTextProperties, double, 4>
		( "COLOR", SsReflectionName,
		&Vfl3DTextLabel::VflTextProperties::SetColor),
		
		NULL
	};

}

////////////////////////////////////////////////////////////////////////////////

Vfl3DTextLabel::VflTextProperties::VflTextProperties()
: 	m_bTextFollowViewDir(true)
,	m_fSize(1.0f)
{ }

Vfl3DTextLabel::VflTextProperties::~VflTextProperties()
{ }


float Vfl3DTextLabel::VflTextProperties::GetTextSize() const
{
	return m_fSize;
}

bool  Vfl3DTextLabel::VflTextProperties::SetTextSize(float fNewSize)
{
	if(compAndAssignFunc(fNewSize, m_fSize))
	{
		Notify(MSG_TEXT_SIZE_CHG);
		static_cast<Vfl3DTextLabel*>(GetParentRenderable())->m_bIsDirty = true;
		return true;
	}
	return false;
}


bool Vfl3DTextLabel::VflTextProperties::SetTextFollowViewDir(
	bool bFollowViewDir)
{
	if(compAndAssignFunc(bFollowViewDir, m_bTextFollowViewDir))
	{
		Notify(MSG_FOLLOW_VIEWDIR);
		static_cast<Vfl3DTextLabel*>(GetParentRenderable())->m_bIsDirty = true;
		return true;
	}
	return false;
}

bool Vfl3DTextLabel::VflTextProperties::GetTextFollowViewDir() const
{
	return m_bTextFollowViewDir;
}


VistaVector3D Vfl3DTextLabel::VflTextProperties::GetOffset() const
{
	return m_v3Offset;
}

bool Vfl3DTextLabel::VflTextProperties::SetOffset(const VistaVector3D &v3Offset)
{
	if(compAndAssignFunc(v3Offset, m_v3Offset))
	{
		Notify(MSG_OFFSET_CHG);
		static_cast<Vfl3DTextLabel*>(GetParentRenderable())->m_bIsDirty = true;
		return true;
	}
	return false;
}


VistaVector3D Vfl3DTextLabel::VflTextProperties::GetPosition() const
{
	return m_v3Position;
}

bool Vfl3DTextLabel::VflTextProperties::SetPosition(
	const VistaVector3D &v3Position)
{
	if(compAndAssignFunc(v3Position, m_v3Position))
	{
		Notify(MSG_POSITION_CHG);
		static_cast<Vfl3DTextLabel*>(GetParentRenderable())->m_bIsDirty = true;
		return true;
	}
	return false;
}


VistaQuaternion Vfl3DTextLabel::VflTextProperties::GetOrientation() const
{
	return m_qOri;
}

bool Vfl3DTextLabel::VflTextProperties::SetOrientation(
	const VistaQuaternion& qOri)
{
	if(compAndAssignFunc(qOri, m_qOri))
	{
		Notify(MSG_ORIENTATION_CHG);
		static_cast<Vfl3DTextLabel*>(GetParentRenderable())->m_bIsDirty = true;
		return true;
	}
	return false;
}


std::string Vfl3DTextLabel::VflTextProperties::GetText() const
{
	const char *pcText = static_cast<Vfl3DTextLabel*>(
		GetParentRenderable())->m_pTextSource->GetText();
	return (pcText ? std::string(pcText) : "");
}

bool Vfl3DTextLabel::VflTextProperties::SetText(const std::string &strText)
{
	std::string strMyText = GetText();
	if(strText.compare(strMyText) != 0)
	{
		static_cast<Vfl3DTextLabel*>(
			GetParentRenderable())->m_pTextSource->SetText(strText.c_str());
		static_cast<Vfl3DTextLabel*>(GetParentRenderable())->m_bIsDirty = true;
		Notify(MSG_TEXT_CHG);
		return true;
	}
	return false;
}


void Vfl3DTextLabel::VflTextProperties::GetColor(double rgba[4]) const
{
	// will write only the first three values
	static_cast<Vfl3DTextLabel*>(
		GetParentRenderable())->m_pTextProperty->GetDiffuseColor(rgba);

	// get opacity (have to check whether this is alpha or 1-alpha
	rgba[3] = static_cast<Vfl3DTextLabel*>(
		GetParentRenderable())->m_pTextProperty->GetOpacity();
}

bool Vfl3DTextLabel::VflTextProperties::SetColor(double rgba[4])
{
	double _rgba[4];
	GetColor(_rgba);
	if(memcmp( _rgba, rgba, 4*sizeof(double) ) != 0)
	{
		static_cast<Vfl3DTextLabel*>(
			GetParentRenderable())->m_pTextProperty->SetAmbientColor(rgba);
		static_cast<Vfl3DTextLabel*>(
			GetParentRenderable())->m_pTextProperty->SetDiffuseColor(rgba);
		static_cast<Vfl3DTextLabel*>(
			GetParentRenderable())->m_pTextProperty->SetSpecularColor(rgba);
		static_cast<Vfl3DTextLabel*>(
			GetParentRenderable())->m_pTextProperty->SetOpacity(rgba[3]);
		static_cast<Vfl3DTextLabel*>(
			GetParentRenderable())->m_pTextActor->Modified();
		Notify(MSG_COLOR_CHG);
		return true;
	}
	return false;
}

bool Vfl3DTextLabel::VflTextProperties::GetUseLighting() const
{
	return static_cast<Vfl3DTextLabel*>(
		GetParentRenderable())->m_pTextProperty->GetLighting();
}

bool Vfl3DTextLabel::VflTextProperties::SetUseLighting( bool bUseLighting )
{
	if( bUseLighting != GetUseLighting() )
	{
		static_cast<Vfl3DTextLabel*>(
			GetParentRenderable())->m_pTextProperty->SetLighting( bUseLighting );
		return true;
	}
	return false;
}

/*============================================================================*/
/*  END OF FILE																  */
/*============================================================================*/
