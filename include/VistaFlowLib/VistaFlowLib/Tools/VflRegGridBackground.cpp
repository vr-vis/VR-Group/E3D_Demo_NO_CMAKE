/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <GL/glew.h>

#include "VflRegGridBackground.h"

#include <VistaFlowLib/Visualization/VflVisController.h>

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflRegGridBackground::VflRegGridBackground()
: IVflRenderable()
{
}

VflRegGridBackground::~VflRegGridBackground()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflRegGridBackground::DrawOpaque()
{
	if(m_vecLines.empty())
		return;

	VflRegGridBackgroundProperties *pProps = 
		static_cast<VflRegGridBackgroundProperties*>(this->GetProperties());

	glPushAttrib(GL_ENABLE_BIT|GL_LINE_BIT);
	glDisable(GL_LIGHTING);

	float fWidth = pProps->GetLineWidth();
	glLineWidth(fWidth);

	float fR, fG, fB;
	pProps->GetColor(fR, fG, fB);
	glColor3f(fR, fG, fB);

	//if(pProps->GetFogEnabled())
	//{
	//	glEnable(GL_FOG);
	//	float c[4] = {0, 0, 0, 1};
	//	pProps->GetFogColor(c);
	//	glFogfv(GL_FOG_COLOR, c);
	//
	//}

#if 1
	glPushClientAttrib(GL_CLIENT_ALL_ATTRIB_BITS);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, &m_vecLines[0]);
	glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(m_vecLines.size())*2);
	glPopClientAttrib();
#else
	glBegin(GL_LINES);

	int iNumOfLines = m_vecLines.size();
	for(int i=0; i<iNumOfLines; ++i)
	{
		glVertex3fv(m_vecLines[i].fBegin);
		glVertex3fv(m_vecLines[i].fEnd);
	}

	glEnd();
#endif 
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateGridPoints                                            */
/*                                                                            */
/*============================================================================*/
void VflRegGridBackground::UpdateGridPoints()
{
	m_vecLines.clear();
	
	VflRegGridBackgroundProperties *pProps = this->GetProperties();

	int iHRes, iVRes;
	float fWidth, fHeight;
	VistaVector3D v3Center;

	pProps->GetGridResolution(iHRes, iVRes);
	fWidth = pProps->GetGridWidth();
	fHeight = pProps->GetGridHeight();
	pProps->GetPosition(v3Center);

	
	VistaVector3D v3u(1.0f, 0.0f, 0.0f, 0.0f);
	VistaVector3D v3v(0.0f, 1.0f, 0.0f, 0.0f);
	VistaVector3D v3Origin, v3Begin, v3End;

	// calculate rotation
	VistaVector3D n; GetProperties()->GetNormal(n);
	if(n != VistaVector3D(0, 0, 1, 0))
	{
		VistaQuaternion q(VistaVector3D(0, 0, 1, 0), n);
		v3u = q.Rotate(v3u);
		v3v = q.Rotate(v3v);
	}

	//                 Hstep -->
	//  +----------+-----------+----------+
    //  |          |  v3Center |          |          .
 	//  +----------+-----*-----+----------+         /|\       
    //  |          |           |          |   Vstep  |
	//  *----------+-----------+----------+
	// v3Origin

	v3Origin = v3Center - 0.5f*fWidth*v3u - 0.5f*fHeight*v3v;

	// compute points for the horizontal lines
	float fVStep = fHeight / iVRes;
	// the lowest horizontal line
	v3Begin = v3Origin;
	v3End   = v3Origin + fWidth * v3u;
	m_vecLines.push_back(sLine(v3Begin, v3End));
	for(int i=0; i<iVRes; ++i)
	{
		v3Begin += fVStep * v3v;
		v3End   += fVStep * v3v;
		m_vecLines.push_back(sLine(v3Begin, v3End));
	}

	// compute points for the vertical lines
	float fHStep = fWidth / iHRes; 
	// the most left vertical line
	v3Begin = v3Origin;
	v3End   = v3Origin + fHeight * v3v;
	m_vecLines.push_back(sLine(v3Begin, v3End));
	for(int i=0; i<iHRes; ++i)
	{
		v3Begin += fHStep * v3u;
		v3End   += fHStep * v3u;
		m_vecLines.push_back(sLine(v3Begin, v3End));
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflRegGridBackground::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	if( msg == VflRegGridBackgroundProperties::MSG_GRID_HEIGHT_CHANGED ||
		msg == VflRegGridBackgroundProperties::MSG_GRID_WIDTH_CHANGED  ||
		msg == VflRegGridBackgroundProperties::MSG_POSITION_CHANGED ||
		msg == VflRegGridBackgroundProperties::MSG_NORMAL_CHANGED ||
		msg == VflRegGridBackgroundProperties::MSG_RESOLUTION_CHANGED )
		UpdateGridPoints();


}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflRegGridBackground::GetType() const
{
    return IVflRenderable::GetType()+std::string("::VflRegGridBackground");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
VflRegGridBackground::VflRegGridBackgroundProperties *VflRegGridBackground::CreateProperties() const
{
	return new VflRegGridBackgroundProperties;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflRegGridBackground::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}

VflRegGridBackground::VflRegGridBackgroundProperties* VflRegGridBackground::GetProperties()
{
	assert(this->IVflRenderable::GetProperties());
	return static_cast<VflRegGridBackgroundProperties*>(this->IVflRenderable::GetProperties());
}


/*============================================================================*/
/*  methods of VflRegGridBackground::CProperties                             */
/*============================================================================*/

VflRegGridBackground::VflRegGridBackgroundProperties::VflRegGridBackgroundProperties()
: IVflRenderable::VflRenderableProperties(),
  m_iHRes(2), m_iVRes(2),
  m_fWidth(2.0f), m_fHeight(2.0f),
  m_v3Pos(VistaVector3D(0.0f, 0.0f, 0.0f)),
  m_v3QuadNormal(VistaVector3D(0.0f, 0.0f, 1.0f)),
  m_fLineWidth(1.0f),
  m_fRed(1.0f), m_fGreen(1.0f), m_fBlue(1.0f)
{
}

VflRegGridBackground::VflRegGridBackgroundProperties::~VflRegGridBackgroundProperties()
{
}
void VflRegGridBackground::VflRegGridBackgroundProperties::SetGridResolution(
	int iHRes, int iVRes)
{
	m_iHRes = iHRes;
	m_iVRes = iVRes;
	this->Notify(MSG_RESOLUTION_CHANGED);
}
void VflRegGridBackground::VflRegGridBackgroundProperties::GetGridResolution(
	int &iHRes, int &iVRes) const
{
	iHRes = m_iHRes;
	iVRes = m_iVRes;
}
void VflRegGridBackground::VflRegGridBackgroundProperties::SetGridWidth(float f)
{
	m_fWidth = f;
	this->Notify(MSG_GRID_WIDTH_CHANGED);
}
float VflRegGridBackground::VflRegGridBackgroundProperties::GetGridWidth() const
{
	return m_fWidth;
}
void VflRegGridBackground::VflRegGridBackgroundProperties::SetGridHeight(float f)
{
	m_fHeight = f;
	this->Notify(MSG_GRID_HEIGHT_CHANGED);
}
float VflRegGridBackground::VflRegGridBackgroundProperties::GetGridHeight() const
{
	return m_fHeight;
}
void VflRegGridBackground::VflRegGridBackgroundProperties::SetPosition(VistaVector3D v3)
{
	m_v3Pos = v3;
	this->Notify(MSG_POSITION_CHANGED);
}
void VflRegGridBackground::VflRegGridBackgroundProperties::GetPosition(VistaVector3D &v3) const
{
	v3 = m_v3Pos;
}
void VflRegGridBackground::VflRegGridBackgroundProperties::SetNormal(VistaVector3D v3)
{
	m_v3QuadNormal = v3;
	this->Notify(MSG_NORMAL_CHANGED);
}
void VflRegGridBackground::VflRegGridBackgroundProperties::GetNormal(VistaVector3D &v3) const
{
	v3 = m_v3QuadNormal;
}
void VflRegGridBackground::VflRegGridBackgroundProperties::SetLineWidth(float fW)
{
	m_fLineWidth = fW;
}
float VflRegGridBackground::VflRegGridBackgroundProperties::GetLineWidth() const
{
	return m_fLineWidth;
}
void VflRegGridBackground::VflRegGridBackgroundProperties::SetColor(float fR, float fG, float fB)
{
	m_fRed = fR;
	m_fGreen = fG;
	m_fBlue = fB;
}
void VflRegGridBackground::VflRegGridBackgroundProperties::GetColor(
	float &fR, float &fG, float &fB) const
{
	fR = m_fRed;
	fG = m_fGreen;
	fB = m_fBlue;
}

//void VflRegGridBackground::VflRegGridBackgroundProperties::SetFogColor( const float col[3])
//{
//	m_cFog[0] = col[0];
//	m_cFog[1] = col[1];
//	m_cFog[2] = col[2];
//}
//
//void VflRegGridBackground::VflRegGridBackgroundProperties::GetFogColor( float col[3]) const
//{
//	col[0] = m_cFog[0];
//	col[1] = m_cFog[1];
//	col[2] = m_cFog[2];
//}
//
//void VflRegGridBackground::VflRegGridBackgroundProperties::SetFogEnabled( bool state )
//{
//	m_bFog = state;
//}
//
//bool VflRegGridBackground::VflRegGridBackgroundProperties::GetFogEnabled() const
//{
//	return m_bFog;
//}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflRegGridBackground.cpp"                                    */
/*============================================================================*/
