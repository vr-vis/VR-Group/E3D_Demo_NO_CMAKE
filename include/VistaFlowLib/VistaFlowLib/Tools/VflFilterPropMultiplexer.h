/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLFILTERPROPMULTIPLEXER_H
#define _VFLFILTERPROPMULTIPLEXER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include <list>
#include <VistaInterProcComm/Concurrency/VistaMutex.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflUnsteadyDataFilter;
class VflPropertyLoader;
class VistaPropertyList;
class VistaProperty;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflFilterPropMultiplexer contains properties for (one or multiple) unsteady 
 * data filters and forwards calls to set and get routines to them.
 */    
class VISTAFLOWLIBAPI VflFilterPropMultiplexer
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflFilterPropMultiplexer();    
    virtual ~VflFilterPropMultiplexer();

	int RegisterFilter(VflUnsteadyDataFilter *pFilter);
	int UnregisterFilter(VflUnsteadyDataFilter *pFilter);

	int GetFilterCount() const;
	std::list<VflUnsteadyDataFilter *> GetFilters() const;

    // IMPLEMENTATION
    virtual bool SetPropertyByName(const std::string &sPropName, 
                                   const std::string &sPropValue);
    virtual int SetPropertiesByList(const VistaPropertyList & liProps);
	virtual int SetProperty(const VistaProperty &refProp);
	virtual int GetProperty(VistaProperty &refProp);
	virtual int GetPropertySymbolList(std::list<std::string> &rStorageList);

protected:
	std::list<VflUnsteadyDataFilter *>	m_liFilters;
    VflPropertyLoader  *m_pLoader;
	VistaMutex			m_oMutex;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLFILTERPROPMULTIPLEXER_H)
