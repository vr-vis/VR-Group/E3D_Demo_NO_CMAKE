/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* PRAGMAS                                                                    */
/*============================================================================*/
#ifdef WIN32
    #pragma warning(disable:4786)
#endif
#include <GL/glew.h>

#include "VflBoundsInfo.h"

#include "VflPropertyLoader.h"

#include "../Visualization/VflRenderNode.h"

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaVisExt/Tools/VveUtils.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflBoundsInfo::VflBoundsInfo()
: IVflRenderable(), 
  m_pLoader(NULL),
  m_dBoundsTs(0.0)
{
//    m_oProperties.m_pParent = this;
}

VflBoundsInfo::~VflBoundsInfo()
{
   delete m_pLoader;
   m_pLoader=NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/

unsigned int VflBoundsInfo::GetRegistrationMode() const
{
	return OLI_DRAW_OPAQUE | OLI_UPDATE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflBoundsInfo::DrawOpaque()
{
	VflBoundsInfoProperties *p = static_cast<VflBoundsInfoProperties*>(GetProperties());

	glPushAttrib(GL_LINE_BIT | GL_LIGHTING_BIT );

    glLineWidth((*p).m_fLineWidth);


    glEnable(GL_LINE_STIPPLE);
    glLineStipple((*p).m_iStippleScale, (*p).m_iStipplePattern);
    glDisable(GL_LIGHTING);
    
    // draw bounding volume
    glColor3fv((*p).m_aColor);
    glBegin(GL_LINE_LOOP);
        glVertex3f(m_v3BoundsMin[0], m_v3BoundsMin[1], m_v3BoundsMin[2]);
        glVertex3f(m_v3BoundsMin[0], m_v3BoundsMax[1], m_v3BoundsMin[2]);
        glVertex3f(m_v3BoundsMin[0], m_v3BoundsMax[1], m_v3BoundsMax[2]);
        glVertex3f(m_v3BoundsMin[0], m_v3BoundsMin[1], m_v3BoundsMax[2]);
    glEnd();

    glBegin(GL_LINE_LOOP);
        glVertex3f(m_v3BoundsMax[0], m_v3BoundsMin[1], m_v3BoundsMin[2]);
        glVertex3f(m_v3BoundsMax[0], m_v3BoundsMax[1], m_v3BoundsMin[2]);
        glVertex3f(m_v3BoundsMax[0], m_v3BoundsMax[1], m_v3BoundsMax[2]);
        glVertex3f(m_v3BoundsMax[0], m_v3BoundsMin[1], m_v3BoundsMax[2]);
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(m_v3BoundsMin[0], m_v3BoundsMin[1], m_v3BoundsMin[2]);
        glVertex3f(m_v3BoundsMax[0], m_v3BoundsMin[1], m_v3BoundsMin[2]);

        glVertex3f(m_v3BoundsMin[0], m_v3BoundsMax[1], m_v3BoundsMin[2]);
        glVertex3f(m_v3BoundsMax[0], m_v3BoundsMax[1], m_v3BoundsMin[2]);

        glVertex3f(m_v3BoundsMin[0], m_v3BoundsMax[1], m_v3BoundsMax[2]);
        glVertex3f(m_v3BoundsMax[0], m_v3BoundsMax[1], m_v3BoundsMax[2]);

        glVertex3f(m_v3BoundsMin[0], m_v3BoundsMin[1], m_v3BoundsMax[2]);
        glVertex3f(m_v3BoundsMax[0], m_v3BoundsMin[1], m_v3BoundsMax[2]);
    glEnd();

    // reset local settings
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflBoundsInfo::Update()
{
	double dTs = GetRenderNode()->GetBoundsLastRefreshTimeStamp();
	if(m_dBoundsTs != dTs)
	{
		GetRenderNode()->GetVisBounds(m_v3BoundsMin, m_v3BoundsMax);
		m_dBoundsTs = dTs;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflBoundsInfo::GetType() const
{
    return IVflRenderable::GetType()+string("::VflBoundsInfo");
}

std::string VflBoundsInfo::GetFactoryType()
{
	return std::string("BOUNDS_INFO");
}

IVflRenderable::VflRenderableProperties *VflBoundsInfo::CreateProperties() const
{
	return new VflBoundsInfoProperties;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPropertyObj                                              */
/*                                                                            */
/*============================================================================*/

/*============================================================================*/
/*  methods of VflBoundsInfo::CProperties                                      */
/*============================================================================*/

static const std::string SsReflectionType("VflBoundsInfo");

static unsigned short ConvertToUnsignedShort(const std::string &sValue)
{
	unsigned short i= VistaAspectsConversionStuff::ConvertToInt(sValue);
	return i;
}

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<float, VflBoundsInfo::VflBoundsInfoProperties, VistaProperty::PROPT_DOUBLE>
	      ("LINE_WIDTH", SsReflectionType,
		  &VflBoundsInfo::VflBoundsInfoProperties::GetLineWidth),
	new TVistaPropertyGet<unsigned short, VflBoundsInfo::VflBoundsInfoProperties, VistaProperty::PROPT_INT>
	      ("STIPPLE_PATTERN", SsReflectionType,
		  &VflBoundsInfo::VflBoundsInfoProperties::GetStipplePattern),
	new TVistaPropertyGet<int, VflBoundsInfo::VflBoundsInfoProperties, VistaProperty::PROPT_INT>
	      ("STIPPLE_SCALE", SsReflectionType,
		  &VflBoundsInfo::VflBoundsInfoProperties::GetStippleScale),
	new TVistaProperty3RefGet<float, VflBoundsInfo::VflBoundsInfoProperties>
	( "COLOR", SsReflectionType,
	&VflBoundsInfo::VflBoundsInfoProperties::GetBoundsColor),
  NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<float, float,
	                    VflBoundsInfo::VflBoundsInfoProperties>
		("LINE_WIDTH", SsReflectionType,
		&VflBoundsInfo::VflBoundsInfoProperties::SetLineWidth),
	new TVistaPropertySet<int, int,
	                    VflBoundsInfo::VflBoundsInfoProperties>
		("STIPPLE_SCALE", SsReflectionType,
		&VflBoundsInfo::VflBoundsInfoProperties::SetStippleScale),
	new TVistaPropertySet<unsigned short, unsigned short,
	                    VflBoundsInfo::VflBoundsInfoProperties>
		("STIPPLE_PATTERN", SsReflectionType,
		&VflBoundsInfo::VflBoundsInfoProperties::SetStipplePattern),
     new TVistaProperty3ValSet<float, VflBoundsInfo::VflBoundsInfoProperties>
	 ( "COLOR", SsReflectionType,
	 &VflBoundsInfo::VflBoundsInfoProperties::SetBoundsColor),
	NULL
};

VflBoundsInfo::VflBoundsInfoProperties::VflBoundsInfoProperties()
: IVflRenderable::VflRenderableProperties(),
  m_fLineWidth(2.0f),
  m_iStipplePattern(0xFFFF),
  m_iStippleScale(1)
{
	m_aColor[0] = 0.0f;
	m_aColor[1] = 0.0f;
	m_aColor[2] = 0.0f;
}

VflBoundsInfo::VflBoundsInfoProperties::~VflBoundsInfoProperties()
{
}

string VflBoundsInfo::VflBoundsInfoProperties::GetReflectionableType() const
{
	return SsReflectionType;
}


int VflBoundsInfo::VflBoundsInfoProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = IVflRenderable::VflRenderableProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize+1;
}


bool VflBoundsInfo::VflBoundsInfoProperties::GetBoundsColor(float &r, float &g, float &b) const
{
	r = m_aColor[0];
	g = m_aColor[1];
	b = m_aColor[2];

	return true;
}

bool VflBoundsInfo::VflBoundsInfoProperties::SetBoundsColor(float r, float g, float b)
{
	bool bChange = (compAndAssignFunc<float>(r, m_aColor[0]) == 1) ? true : false;
	bChange = ((compAndAssignFunc<float>(g, m_aColor[1]) == 1) ? true : false) || bChange;
	bChange = ((compAndAssignFunc<float>(b, m_aColor[2]) == 1) ? true : false) || bChange;
	if(bChange)
	{
		Notify(MSG_BOUNDS_COLORCHANGE);
		return true;
	}

	return false;
}


float VflBoundsInfo::VflBoundsInfoProperties::GetLineWidth() const
{
	return m_fLineWidth;
}

bool VflBoundsInfo::VflBoundsInfoProperties::SetLineWidth(float fLineWidth)
{
	if(compAndAssignFunc<float>(fLineWidth, m_fLineWidth) == 1)
	{
		Notify(MSG_BOUNDS_LINEWIDTHCHANGE);
		return true;
	}
	return false;
}


unsigned short VflBoundsInfo::VflBoundsInfoProperties::GetStipplePattern() const
{
	return m_iStipplePattern;
}

bool VflBoundsInfo::VflBoundsInfoProperties::SetStipplePattern(unsigned short uiPattern)
{
	if(compAndAssignFunc<unsigned short>(uiPattern, m_iStipplePattern) == 1)
	{
		Notify(MSG_BOUNDS_STIPPLEPATTERNCHANGE);
		return true;
	}
	return false;
}


int VflBoundsInfo::VflBoundsInfoProperties::GetStippleScale() const
{
	return m_iStippleScale;
}

bool VflBoundsInfo::VflBoundsInfoProperties::SetStippleScale(int nStippleScale)
{
	if(compAndAssignFunc<unsigned short>(nStippleScale, m_iStipplePattern) == 1)
	{
		Notify(MSG_BOUNDS_STIPPLESCALECHANGE);
		return true;
	}
	return false;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflBoundsInfo.cpp"                                           */
/*============================================================================*/

