/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifdef WIN32
	#pragma warning(disable:4786)
#endif

#include "VflVisComponent.h"

#include "../Visualization/VflVisObject.h"

#include <iostream>
#include <algorithm>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION FOR IVflVisComponent                                          */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
IVflVisComponent::IVflVisComponent()
{
}

IVflVisComponent::~IVflVisComponent()
{
#ifdef DEBUG
	vstr::debugi() << "destroying IVflVisComponent: " << this->GetNameForNameable() << endl;
#endif
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Activate                                                    */
/*                                                                            */
/*============================================================================*/
void IVflVisComponent::Activate()
{
	SetHelpersVisible(true);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Deactivate                                                  */
/*                                                                            */
/*============================================================================*/
void IVflVisComponent::Deactivate()
{
	SetHelpersVisible(false);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetHelpersVisible                                       */
/*                                                                            */
/*============================================================================*/
void IVflVisComponent::SetHelpersVisible(bool bVisible)
{
    list<IVflRenderable *>::iterator itHelper;
    for (itHelper=m_liHelpers.begin(); itHelper!=m_liHelpers.end(); ++itHelper)
    {
        (*itHelper)->SetVisible(bVisible);
    }
}

bool IVflVisComponent::GetHelpersVisible() const
{
    list<IVflRenderable *>::const_iterator itHelper = m_liHelpers.begin();
    if (itHelper != m_liHelpers.end())
    {
        return (*itHelper)->GetVisible();
    }

    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetHelpers                                                  */
/*                                                                            */
/*============================================================================*/
const std::list<IVflRenderable *> &IVflVisComponent::GetHelpers() const
{
    return m_liHelpers;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string IVflVisComponent::GetType() const
{
	return string("IVflVisComponent");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Add/RemoveHelper                                            */
/*                                                                            */
/*============================================================================*/
int IVflVisComponent::AddHelper(IVflRenderable *pHelper)
{
    if (pHelper)
    {
        m_liHelpers.push_back(pHelper);
        return static_cast<int>(m_liHelpers.size());
    }
    else
        return -1;
}

int IVflVisComponent::RemoveHelper(IVflRenderable *pHelper)
{
	list<IVflRenderable *>::iterator it = find(m_liHelpers.begin(), m_liHelpers.end(), pHelper);

	if (it != m_liHelpers.end())
	{
		m_liHelpers.erase(it);
		return static_cast<int>(m_liHelpers.size());
	}
	else
	{
		return -1;
	}
}

/*============================================================================*/
/*  IMPLEMENTATION FOR VflVisComponent                                          */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflVisComponent::VflVisComponent()
: IVflVisComponent()
{
}

VflVisComponent::~VflVisComponent()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Execute                                                     */
/*                                                                            */
/*============================================================================*/
void VflVisComponent::Execute()
{
    list<IVveExecutable *>::iterator itSource;
    for (itSource=m_liSources.begin(); itSource!=m_liSources.end(); ++itSource)
    {
        (*itSource)->Execute();
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetVisible                                                  */
/*                                                                            */
/*============================================================================*/
void VflVisComponent::SetVisible(bool bVisible)
{
    list<IVflRenderable *>::iterator itSink;
    for (itSink=m_liSinks.begin(); itSink!=m_liSinks.end(); ++itSink)
    {
        (*itSink)->SetVisible(bVisible);
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisible                                                  */
/*                                                                            */
/*============================================================================*/
bool VflVisComponent::GetVisible() const
{
    list<IVflRenderable *>::const_iterator cit;
    for (cit=m_liSinks.begin(); cit!=m_liSinks.end(); ++cit)
    {
    	if ((*cit)->GetVisible())
			return true;
    }

    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData                                                     */
/*                                                                            */
/*============================================================================*/
std::list<VveUnsteadyData *> VflVisComponent::GetData() const
{
    return m_liData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDataSources                                              */
/*                                                                            */
/*============================================================================*/
std::list<IVveExecutable *> VflVisComponent::GetDataSources() const
{
    return m_liSources;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDataSinks                                                */
/*                                                                            */
/*============================================================================*/
std::list<IVflRenderable *> VflVisComponent::GetDataSinks() const
{
    return m_liSinks;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetResources                                                */
/*                                                                            */
/*============================================================================*/
std::list<VflResource *> VflVisComponent::GetResources() const
{
    return m_liResources;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVisComponent::GetType() const
{
    return IVflVisComponent::GetType()+"::VflVisComponent";
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Add/RemoveData                                              */
/*                                                                            */
/*============================================================================*/
int VflVisComponent::AddData(VveUnsteadyData *pData)
{
    if (pData)
    {
        m_liData.push_back(pData);
        return static_cast<int>(m_liData.size());
    }
    else
        return -1;
}

int VflVisComponent::RemoveData(VveUnsteadyData *pData)
{
	list<VveUnsteadyData *>::iterator it = find(m_liData.begin(), m_liData.end(), pData);

    if (it != m_liData.end())
    {
        m_liData.erase(it);
        return static_cast<int>(m_liData.size());
    }
    else
    {
        return -1;
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Add/RemoveDataSource                                        */
/*                                                                            */
/*============================================================================*/
int VflVisComponent::AddDataSource(IVveExecutable *pSource)
{
    if (pSource)
    {
        m_liSources.push_back(pSource);
        return static_cast<int>(m_liSources.size());
    }
    else
        return -1;
}

int VflVisComponent::RemoveDataSource(IVveExecutable *pSource)
{
	list<IVveExecutable *>::iterator it = find(m_liSources.begin(), m_liSources.end(), pSource);

	if (it != m_liSources.end())
    {
        m_liSources.erase(it);
        return static_cast<int>(m_liSources.size());
    }
    else
    {
        return -1;
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Add/RemoveDataSink                                          */
/*                                                                            */
/*============================================================================*/
int VflVisComponent::AddDataSink(IVflRenderable *pSink)
{
    if (pSink)
    {
        m_liSinks.push_back(pSink);
        return static_cast<int>(m_liSinks.size());
    }
    else
        return -1;
}

int VflVisComponent::RemoveDataSink(IVflRenderable *pSink)
{
	list<IVflRenderable *>::iterator it = find(m_liSinks.begin(), m_liSinks.end(), pSink);
    if (it != m_liSinks.end())
    {
        m_liSinks.erase(it);
        return static_cast<int>(m_liSinks.size());
    }
    else
    {
        return -1;
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Add/RemoveResource                                          */
/*                                                                            */
/*============================================================================*/
int VflVisComponent::AddResource(VflResource *pResource)
{
    if (pResource)
    {
        m_liResources.push_back(pResource);
        return static_cast<int>(m_liResources.size());
    }
    else
        return -1;
}

int VflVisComponent::RemoveResource(VflResource *pResource)
{
    // we don't use the remove() operator on purpose, because a resource might
    // be present in this list several times, which forces us to remove 'em one
    // by one to release them one by one, as well...
    list<VflResource *>::iterator itRes;
    for (itRes=m_liResources.begin(); itRes!=m_liResources.end(); ++itRes)
    {
        if (*itRes == pResource)
        {
            m_liResources.erase(itRes);
            return static_cast<int>(m_liResources.size());
        }
    }

    return -1;
}


/*============================================================================*/
/*  END OF FILE																  */
/*============================================================================*/
