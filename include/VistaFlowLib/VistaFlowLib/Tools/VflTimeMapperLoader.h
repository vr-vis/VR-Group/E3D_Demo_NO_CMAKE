/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef __VFLTIMEMAPPERLOADER_H
#define __VFLTIMEMAPPERLOADER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <string>
#include <VistaFlowLib/VistaFlowLibConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTimeMapper;
class VistaPropertyList;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflTimeMapperLoader loads data from disk, either threaded or blocking.
 */    
class VISTAFLOWLIBAPI VflTimeMapperLoader
{
public:
    // CONSTRUCTORS / DESTRUCTOR
	VflTimeMapperLoader() {};
	virtual ~VflTimeMapperLoader() {};

   /*
   * Configures a time mapper from an input section and ini file.
   * 
   * 
   *
   */
    bool CreateTimeMapperFromIni(VveTimeMapper* pTimeMapper,
							 const std::string& strIniFile, 
							 const std::string& strIniSection);

	/**
	 * Configures a timemapper from an implicit proplist description.
	 */
 	static bool CreateImplicitTimeMapperFromPropList(
		VveTimeMapper* pTimeMapper, const VistaPropertyList &pProps);
   
protected:
	bool CreateImplicitTimeMapperFromIni(VveTimeMapper* pTimeMapper,
							 const std::string& strIniFile, 
							 const std::string& strIniSection);
	bool CreateExplicitTimeMapperFromIni(VveTimeMapper* pTimeMapper,
							 const std::string& strIniFile, 
							 const std::string& strIniSection);

private:
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(__VFLTIMEMAPPERLOADER_H)
