/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <GL/glew.h>

#include "VflScalarBar.h"
#include <vtkScalarBarActor.h>
#include <vtkTextProperty.h>
#include <vtkLookupTable.h>
#include <vtkRenderer.h>

#include <VistaAspects/VistaAspectsUtils.h>

#include <VistaFlowLib/Tools/VflPropertyLoader.h>

#include <VistaFlowLib/Visualization/VflSystemAbstraction.h>

#include "../Visualization/VflRenderNode.h"

#include <algorithm>
#include <cassert>

using namespace std;

VflScalarBar::VflScalarBar()
:	m_pLoader(NULL)
{ }

VflScalarBar::~VflScalarBar()
{ }

//bool VflScalarBar::Init(VflVisController *pVisController,
//						 int nRegistrationMode)
//{
//    if (!m_pLut)
//        return false;
//
//    vstr::debugi() << " [VflScalarBar] - initializing..." << endl;
//
//	vstr::debugi() << " [VflScalarBar] - calling base class's initialization..." << endl;
//    int iRegistrationMode = VflVisController::OLI_DRAW_2D|VflVisController::OLI_DRAW_OPAQUE;
//
//	// IAR
//	//VflVisObject::Init(pVisController, iRegistrationMode);
//
//	vstr::debugi() << " [VflScalarBar] - registered as object " << m_iId << endl;
//
//	vstr::debugi() << " [VflScalarBar] - building vtk data structures..." << endl;
//	m_oProperties.m_pActor->SetLookupTable(m_pLut->GetLookupTable());
//
//	return true;
//}

//bool VflScalarBar::Init(VflVisController *pVisController,
//						 int nRegistrationMode,
//						 const VistaPropertyList &refProps)
//{
//    if (!Init(pVisController, nRegistrationMode))
//        return false;
//
//    // set values according to properties
//    m_oProperties.SetPropertiesByList(refProps);
//
//    string strIniSection = refProps.GetValue<std::string>("INI_SECTION");
//    string strIniFile = refProps.GetValue<std::string>("INI_FILE");
//
//    if (!strIniSection.empty() && !strIniSection.empty())
//    {
//        VflResourceKey<VflPropertyLoader> oLoaderKey(strIniSection, strIniFile);
//
//        m_pLoader = VflResourceManager::GetResourceManager()->GetResource(oLoaderKey, NULL);
//
//        if (m_pLoader)
//        {
//            m_pLoader->AddTarget(&m_oProperties);
//            m_pLoader->Execute(&m_oProperties);
//        }
//    }
//
//    return true;
//}

bool VflScalarBar::Init()
{

	if(IVflRenderable::Init())
	{
		CScalarBarProperties *p = static_cast<CScalarBarProperties*>(GetProperties());
		if (p->GetLUT()!=NULL) return true;
	}
	
	return false;
}

void VflScalarBar::Draw2D()
{
	CScalarBarProperties *p = static_cast<CScalarBarProperties*>(GetProperties());

	if(!(*p).m_stCbIdx.empty() && (*p).m_nMaxCnt > 0)
	{
		// find current mod
		int i = ((*p).m_nCurCnt + 1) % (*p).m_nMaxCnt;

		if(i == 0)
		{
			// reset count when max count is reached
			(*p).m_nCurCnt = 0;
		}
		else
			++(*p).m_nCurCnt;

		// check whether this frame is to be rendered
		// or not
		if((*p).m_stCbIdx.find(i) != (*p).m_stCbIdx.end())
		{
			// no, skip frame
			return;
		}
	}

	if (!(*p).m_bDisplayOnCurrentNode)
		return;

	vtkRenderer *pRenderer = GetRenderNode()->GetVtkRenderer();

	glPushAttrib(GL_ENABLE_BIT);
	// don't let the scalar bar turn invisible...
		glDisable(GL_CULL_FACE);
		(*p).m_pActor->RenderOpaqueGeometry(pRenderer);
		(*p).m_pActor->RenderOverlay(pRenderer);
	glPopAttrib();

}

std::string VflScalarBar::GetType() const
{
	return IVflRenderable::GetType()+string("::VflScalarBar");
}

VflScalarBar::CScalarBarProperties *VflScalarBar::GetProperties() const
{
	return static_cast<CScalarBarProperties*>(
		IVflRenderable::GetProperties());
}

std::string VflScalarBar::GetFactoryType()
{
	return std::string("SCALAR_BAR");
}

unsigned int VflScalarBar::GetRegistrationMode() const
{
	return OLI_DRAW_2D;
}

IVflRenderable::VflRenderableProperties *VflScalarBar::CreateProperties() const
{
	return new CScalarBarProperties;
}


class CFrameSkipGet : public IVistaPropertyGetFunctor
{
public:
	CFrameSkipGet(const std::string &sName,
		const std::string &sType)
		: IVistaPropertyGetFunctor(sName, sType) {}

	virtual bool operator()(const IVistaPropertyAwareable &obj, VistaProperty &p) const
	{
		const VflScalarBar::CScalarBarProperties *o =
			           &dynamic_cast<const VflScalarBar::CScalarBarProperties&>(obj);
		if(o)
		{
			std::list<int> liFrSkips;
			std::set<int> sFrSkList = o->GetFrameSkipSet();

			for(std::set<int>::const_iterator it = sFrSkList.begin();
				it != sFrSkList.end(); ++it)
			{
				liFrSkips.push_back(*it);
			}
			p.SetValue(VistaAspectsConversionStuff::ConvertToString(liFrSkips));
			return true;
		}
		return false;
	}
};

class CFrameSkipSet : public IVistaPropertySetFunctor
{
public:
	CFrameSkipSet(const std::string &sName,
		const std::string &sType)
		: IVistaPropertySetFunctor(sName, sType) {}

	bool operator()(IVistaPropertyAwareable &obj, const VistaProperty &p)
	{
		VflScalarBar::CScalarBarProperties *o = &dynamic_cast<VflScalarBar::CScalarBarProperties&>(obj);
		if(o)
		{
			std::list<int> liFrSkips = VistaAspectsConversionStuff::ConvertToIntList(p.GetValue());
			std::set<int> stSkipSet;
			for(std::list<int>::const_iterator it = liFrSkips.begin();
				it != liFrSkips.end(); ++it)
			{
				stSkipSet.insert(*it);
			}
			return o->SetFrameSkipSet(stSkipSet);
		}
		return false;
	}

};


static const string SsReflectionType("VflScalarBar");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, VflScalarBar::CScalarBarProperties, VistaProperty::PROPT_BOOL>
	      ("BOLD", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::GetIsBold),
	new TVistaPropertyGet<bool, VflScalarBar::CScalarBarProperties, VistaProperty::PROPT_BOOL>
	      ("ITALIC", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::GetIsItalic),
	new TVistaPropertyGet<bool, VflScalarBar::CScalarBarProperties, VistaProperty::PROPT_BOOL>
	      ("SHADOW", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::GetIsShadow),

	new TVistaPropertyGet<int, VflScalarBar::CScalarBarProperties, VistaProperty::PROPT_DOUBLE>
	      ("NUMBER_OF_LABELS", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::GetNumberOfLabels),
	new TVistaPropertyGet<int, VflScalarBar::CScalarBarProperties, VistaProperty::PROPT_DOUBLE>
	      ("MAXIMUM_NUMBER_OF_COLORS", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::GetMaximumNumberOfColors),
	new TVistaPropertyGet<int, VflScalarBar::CScalarBarProperties, VistaProperty::PROPT_DOUBLE>
	      ("FRAME_SKIP_MOD", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::GetFrameSkipMod),
	new TVistaPropertyGet<string, VflScalarBar::CScalarBarProperties>
	      ("ORIENTATION", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::GetOrientation),
	new TVistaPropertyGet<string, VflScalarBar::CScalarBarProperties>
	      ("TITLE", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::GetTitle),
	new TVistaPropertyGet<std::list<std::string>, VflScalarBar::CScalarBarProperties, VistaProperty::PROPT_LIST>
	      ("DISPLAY_ON_NODES", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::GetNodeNamesForDisplay),
  	new TVistaPropertyGet<bool, VflScalarBar::CScalarBarProperties, VistaProperty::PROPT_BOOL>
	      ("DISPLAYONCURRENTNODE", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::GetDisplayOnCurrentNode),

    new TVistaProperty2RefGet<float, VflScalarBar::CScalarBarProperties> // implicitly PROPT_LIST
		("POSITION", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::GetPosition),
    new TVistaProperty2RefGet<float, VflScalarBar::CScalarBarProperties> // implicitly PROPT_LIST
		("SIZE", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::GetSize),

	new TVistaPropertyGet<VistaColor, VflScalarBar::CScalarBarProperties>
		("TITLE_COLOR", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::GetTitleColor),

	new TVistaPropertyGet<VistaColor, VflScalarBar::CScalarBarProperties>
		("LABEL_COLOR", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::GetLabelColor),

	new CFrameSkipGet("FRAME_SKIP_LIST", SsReflectionType),

	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{

	new TVistaPropertySet<bool, bool,
	                    VflScalarBar::CScalarBarProperties>
		("BOLD", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::SetBold),
	new TVistaPropertySet<bool, bool,
	                    VflScalarBar::CScalarBarProperties>
		("ITALIC", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::SetItalic),
	new TVistaPropertySet<bool, bool,
	                    VflScalarBar::CScalarBarProperties>
		("SHADOW", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::SetShadow),

	new TVistaPropertySet<int, int,
	                    VflScalarBar::CScalarBarProperties>
		("NUMBER_OF_LABELS", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::SetNumberOfLabels),
	new TVistaPropertySet<int, int,
	                    VflScalarBar::CScalarBarProperties>
		("MAXIMUM_NUMBER_OF_COLORS", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::SetMaximumNumberOfColors),
	new TVistaPropertySet<int, int,
	                    VflScalarBar::CScalarBarProperties>
		("FRAME_SKIP_MOD", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::SetFrameSkipMod),
		new TVistaPropertySet<const std::string&, std::string,
	                    VflScalarBar::CScalarBarProperties>
		("TITLE", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::SetTitle),
		new TVistaPropertySet<const std::string&, std::string,
	                    VflScalarBar::CScalarBarProperties>
		("ORIENTATION", SsReflectionType,
		&VflScalarBar::CScalarBarProperties::SetOrientation),
	new TVistaPropertySet<const std::list<std::string>&,
		  std::list<std::string>, VflScalarBar::CScalarBarProperties>
	      ("DISPLAY_ON_NODES", SsReflectionType,
		  &VflScalarBar::CScalarBarProperties::SetNodeNamesForDisplay),
	new TVistaProperty2ValSet<float, VflScalarBar::CScalarBarProperties>
	( "POSITION", SsReflectionType,
	&VflScalarBar::CScalarBarProperties::SetPosition),
	new TVistaProperty2ValSet<float, VflScalarBar::CScalarBarProperties>
	( "SIZE", SsReflectionType,
	&VflScalarBar::CScalarBarProperties::SetSize),

	new TVistaPropertySet<const VistaColor&, bool, VflScalarBar::CScalarBarProperties>
	("TITLE_COLOR", SsReflectionType, &VflScalarBar::CScalarBarProperties::SetTitleColor),

	new TVistaPropertySet<const VistaColor&, bool, VflScalarBar::CScalarBarProperties>
	("LABEL_COLOR", SsReflectionType, &VflScalarBar::CScalarBarProperties::SetLabelColor),

	new CFrameSkipSet("FRAME_SKIP_SET", SsReflectionType),
	NULL
};

VflScalarBar::CScalarBarProperties::CScalarBarProperties()
: IVflRenderable::VflRenderableProperties(),
  m_bDisplayOnCurrentNode(true),
  m_pActor(NULL),
  m_nMaxCnt(-1),
  m_nCurCnt(0),
  m_pLut(new VflVtkLookupTable)//NULL
{
	m_pActor = vtkScalarBarActor::New();

	//VflResourceManager *pRM = VflResourceManager::GetResourceManager();
	//m_pLut = pRM->GetResource(oKey, NULL);
 //   if (!m_pLut)
 //   {
 //       vstr::debugi() << " [VflScalarBar] - ERROR - unable to retrieve lookup table..." << endl;
 //   }
	//else
	//{
	//	m_pActor->SetLookupTable(m_pLut->GetLookupTable());
	//}
}

VflScalarBar::CScalarBarProperties::~CScalarBarProperties()
{
	m_pActor->Delete();

	//VflResourceManager *pRM = VflResourceManager::GetResourceManager();
	//pRM->ReleaseResource(m_pLut, true, NULL);
	//m_pLut = NULL;

 //   if (m_pLoader)
 //   {
 //       //m_pLoader->RemoveTarget(&m_oProperties);
 //       pRM->ReleaseResource(m_pLoader, true, NULL);
 //   }
}

int VflScalarBar::CScalarBarProperties::GetMaximumNumberOfColors() const
{
	return m_pActor->GetMaximumNumberOfColors();
}

bool VflScalarBar::CScalarBarProperties::SetMaximumNumberOfColors(int iNum)
{
	if(GetMaximumNumberOfColors() != iNum)
	{
		m_pActor->SetMaximumNumberOfColors(iNum);
		Notify(MSG_MAXCOLOR_CHANGE);
		return true;
	}
	return false;
}

int VflScalarBar::CScalarBarProperties::GetNumberOfLabels() const
{
	return m_pActor->GetNumberOfLabels();
}

bool VflScalarBar::CScalarBarProperties::SetNumberOfLabels(int iNum)
{
	if(GetNumberOfLabels() != iNum)
	{
		m_pActor->SetNumberOfLabels(iNum);
		Notify(MSG_NUMLABELS_CHANGE);
		return true;
	}
	return false;
}

int VflScalarBar::CScalarBarProperties::GetOrientationId() const
{
	if (m_pActor->GetOrientation() == VTK_ORIENT_HORIZONTAL)
		return ORIENT_HORIZONTAL;
	else
		return ORIENT_VERTICAL;
}

bool VflScalarBar::CScalarBarProperties::SetOrientationId(int iOrient)
{
	if(GetOrientationId() != iOrient)
	{
		if (iOrient == ORIENT_HORIZONTAL)
			m_pActor->SetOrientationToHorizontal();
		else
			m_pActor->SetOrientationToVertical();
		Notify(MSG_ORIENTATION_CHANGE);
		return true;
	}
	return false;
}

string VflScalarBar::CScalarBarProperties::GetOrientation() const
{
	if(GetOrientationId() == ORIENT_HORIZONTAL)
		return "HORIZONTAL";
	else
		return "VERTICAL";
}

bool   VflScalarBar::CScalarBarProperties::SetOrientation(const std::string &sOrientation)
{
	if(VistaAspectsComparisonStuff::StringEquals(sOrientation, "HORIZONTAL", false) == true)
		return SetOrientationId(ORIENT_HORIZONTAL);
	else
		return SetOrientationId(ORIENT_VERTICAL);

}

bool VflScalarBar::CScalarBarProperties::GetIsBold() const
{
	return m_pActor->GetTitleTextProperty()->GetBold() ? true : false;
}

bool VflScalarBar::CScalarBarProperties::SetBold(bool bBold)
{
	if(GetIsBold() != bBold)
	{
		m_pActor->GetTitleTextProperty()->SetBold(bBold);
		m_pActor->GetLabelTextProperty()->SetBold(bBold);
		Notify(MSG_BOLD_CHANGE);
		return true;
	}
	return false;
}

bool VflScalarBar::CScalarBarProperties::GetIsItalic() const
{
	return m_pActor->GetTitleTextProperty()->GetItalic() ? true : false;
}

bool VflScalarBar::CScalarBarProperties::SetItalic(bool bItalic)
{
	if(GetIsItalic() != bItalic)
	{
		m_pActor->GetTitleTextProperty()->SetItalic(bItalic);
		m_pActor->GetLabelTextProperty()->SetItalic(bItalic);
		Notify(MSG_ITALIC_CHANGE);
		return true;
	}
	return false;
}

bool VflScalarBar::CScalarBarProperties::SetTitleColor(const VistaColor& oColor)
{

	double color[3]; //yeah convert float to double...
	
	for( int i = 0; i < 3; ++i )
		color[i] = oColor[i];

	m_pActor->GetTitleTextProperty()->SetColor(&color[0]);
	Notify(MSG_TITLE_COLOR_CHANGE);
	return true;
}

bool VflScalarBar::CScalarBarProperties::SetLabelColor(const VistaColor& oColor)
{

	double color[3]; //yeah convert float to double...

	for( int i = 0; i < 3; ++i )
		color[i] = oColor[i];

	m_pActor->GetLabelTextProperty()->SetColor(&color[0]);
	Notify(MSG_LABEL_COLOR_CHANGE);
	return true;
}


VistaColor VflScalarBar::CScalarBarProperties::GetTitleColor() const
{
	
	double color[3];
	m_pActor->GetTitleTextProperty()->GetColor(color);
	return VistaColor(color);
}


VistaColor VflScalarBar::CScalarBarProperties::GetLabelColor() const
{

	double color[3];
	m_pActor->GetLabelTextProperty()->GetColor(color);
	return VistaColor(color);
}

bool VflScalarBar::CScalarBarProperties::GetIsShadow() const
{
	return m_pActor->GetTitleTextProperty()->GetShadow() ? true : false;
}

bool VflScalarBar::CScalarBarProperties::SetShadow(bool bShadow)
{
	if(GetIsShadow() != bShadow)
	{
		m_pActor->GetTitleTextProperty()->SetShadow(bShadow);
		m_pActor->GetLabelTextProperty()->SetShadow(bShadow);
		Notify(MSG_SHADOW_CHANGE);
		return true;
	}
	return false;
}

std::string VflScalarBar::CScalarBarProperties::GetTitle() const
{
	char *title = m_pActor->GetTitle();

	return string(title ? title : "<none>");
}

bool VflScalarBar::CScalarBarProperties::SetTitle(const std::string &strTitle)
{
	if(GetTitle() != strTitle)
	{
		m_pActor->SetTitle(strTitle.c_str());
		Notify(MSG_TITLE_CHANGE);
		return true;
	}
	return false;
}

bool VflScalarBar::CScalarBarProperties::GetPosition(float &x, float &y) const
{
	if(!m_pActor)
		return false;
    double *pos = m_pActor->GetPosition();
    x = float(pos[0]);
    y = float(pos[1]);

	return true;
}

bool VflScalarBar::CScalarBarProperties::SetPosition(float x, float y)
{
	float X,Y;
	GetPosition(X,Y);
	if(X != x || Y != y)
	{
		m_pActor->SetPosition(x, y);
		Notify(MSG_POSITION_CHANGE);
		return true;
	}
	return false;
}

bool VflScalarBar::CScalarBarProperties::GetSize(float &width, float &height) const
{
	if(!m_pActor)
		return false;
    width = m_pActor->GetWidth();
    height = m_pActor->GetHeight();

	return true;
}

bool VflScalarBar::CScalarBarProperties::SetSize(float width, float height)
{
	float W,H;
	GetSize(W,H);
	if(W != width || H != height)
	{
		m_pActor->SetWidth(width);
		m_pActor->SetHeight(height);
	    Notify(MSG_SIZE_CHANGE);
		return true;
	}
	return false;
}

bool VflScalarBar::CScalarBarProperties::GetDisplayOnCurrentNode() const
{
	return m_bDisplayOnCurrentNode;
}

bool VflScalarBar::CScalarBarProperties::SetDisplayOnCurrentNode(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bDisplayOnCurrentNode))
	{
		Notify(MSG_DISPLAYONCURRENTNODE);
		return true;
	}
	return false;
}

std::list<std::string> VflScalarBar::CScalarBarProperties::
                                       GetNodeNamesForDisplay() const
{
	return m_liNodeNamesForDisplay;
}

bool VflScalarBar::CScalarBarProperties::SetNodeNamesForDisplay(
	                                    const std::list<std::string> &liNodeNames)
{
	// IAR @todo we could check for equality
	m_liNodeNamesForDisplay = liNodeNames;


	if (m_liNodeNamesForDisplay.empty())
	{
		SetDisplayOnCurrentNode(true);
	}
	else
	{
		IVflSystemAbstraction *pSystem = IVflSystemAbstraction::GetSystemAbs();
		if (pSystem->IsLeader())
		{
			SetDisplayOnCurrentNode(true);
		}
		else
		{
			// cluster - client
			string strNodeName = pSystem->GetClientName();
			if(std::find(m_liNodeNamesForDisplay.begin(), m_liNodeNamesForDisplay.end(), strNodeName)
				== m_liNodeNamesForDisplay.end())
			{
				SetDisplayOnCurrentNode(false);
			}
			else
				SetDisplayOnCurrentNode(true);
		}
	}

	Notify(MSG_NODEDISPLAYCHANGE);
	return true;
}

string VflScalarBar::CScalarBarProperties::GetReflectionableType() const
{
	return SsReflectionType;
}

int VflScalarBar::CScalarBarProperties::AddToBaseTypeList(list<string> &rBtList) const
{
	int nSize = IVflRenderable::VflRenderableProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}

VflVtkLookupTable *VflScalarBar::CScalarBarProperties::GetLookupTable() const
{
	return m_pLut;
}

std::set<int> VflScalarBar::CScalarBarProperties::GetFrameSkipSet() const
{
	return this->m_stCbIdx;
}

bool VflScalarBar::CScalarBarProperties::SetFrameSkipSet(const std::set<int> &sSkipSet)
{
	this->m_stCbIdx = sSkipSet;
	Notify(MSG_FRAME_SKIP_CHANGE);
	return true;
}

int VflScalarBar::CScalarBarProperties::GetFrameSkipMod() const
{
	return this->m_nMaxCnt;
}

bool VflScalarBar::CScalarBarProperties::SetFrameSkipMod(int iMod)
{
	if(m_nMaxCnt != iMod)
	{
		this->m_nMaxCnt = iMod;
		this->m_nCurCnt = 0;
		Notify(MSG_FRAME_SKIPMOD_CHANGE);
		return true;
	}
	return false;
}

int VflScalarBar::CScalarBarProperties::GetCurFrameCnt() const
{
	return this->m_nCurCnt;
}

std::string VflScalarBar::CScalarBarProperties::GetLutName() const
{
	if(m_pLut)
		return m_pLut->GetNameForNameable();
	return "<none>";
}
// bool VflScalarBar::CScalarBarProperties::SetLutByName(const std::string &sName)
// {
// 	if(!m_pLut)
// 	{
// 		// create and assign LUT
// 		VflResourceManager *pRM = VflResourceManager::GetResourceManager();
// 
// 		VflResourceKey<VflVtkLookupTable> oKey(sName);
// 		m_pLut = pRM->GetResource(oKey, NULL);
// 		if(!m_pLut)
// 			return false;
// 
// 		m_pActor->SetLookupTable(m_pLut->GetLookupTable());
// 		Notify(MSG_LUT_CHANGE);
// 		return true;
// 	}
// 	else if(m_pLut->GetNameForNameable() != sName)
// 	{
// 		VflResourceManager *pRM = VflResourceManager::GetResourceManager();
// 
// 		VflResourceKey<VflVtkLookupTable> oKey(sName);
// 		VflVtkLookupTable *pT = pRM->GetResource(oKey, NULL);
// 		if(!pT)
// 			return false;
// 
// 		pRM->ReleaseResource(m_pLut, true, NULL);
// 
// 		m_pLut = pT;
// 		m_pActor->SetLookupTable(m_pLut->GetLookupTable());
// 
// 		Notify(MSG_LUT_CHANGE);
// 		return true;
// 	}
// 	return false;
// }

VflVtkLookupTable *VflScalarBar::CScalarBarProperties::GetLUT() const
{
	return m_pLut;
}

bool VflScalarBar::CScalarBarProperties::SetLUT(VflVtkLookupTable *pLut)
{
	assert(pLut);

	if(compAndAssignFunc<VflVtkLookupTable*>(pLut,m_pLut))
	{
		m_pActor->SetLookupTable(m_pLut->GetLookupTable());
		Notify(MSG_LUT_CHANGE);
	}
	return true;
}

/*============================================================================*/
/*  END OF FILE "VflScalarBar.cpp"                                            */
/*============================================================================*/
