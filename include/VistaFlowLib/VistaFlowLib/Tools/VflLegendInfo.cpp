/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* PRAGMAS                                                                    */
/*============================================================================*/
#ifdef WIN32
    #pragma warning(disable:4786)
#endif
#include <GL/glew.h>

#include <cstring>
#include <sstream>

#include "VflLegendInfo.h"
#include <VistaFlowLib/Tools/Vfl3DTextLabel.h>    //#include "Vfl3DTextLabel.h"
#include <VistaFlowLib/Visualization/VflRenderNode.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflLegendInfo::VflLegendInfo(VflRenderNode* pRenderNode)
	:	IVflRenderable(),
		//m_pTarget(NULL),
		m_pLabelX(new Vfl3DTextLabel),
		m_pLabelY(new Vfl3DTextLabel),
		m_pLabelZ(new Vfl3DTextLabel),
		m_pMinX(new Vfl3DTextLabel),
		m_pMaxX(new Vfl3DTextLabel),
		m_pMinY(new Vfl3DTextLabel),
		m_pMaxY(new Vfl3DTextLabel),
		m_pMinZ(new Vfl3DTextLabel),
		m_pMaxZ(new Vfl3DTextLabel),
		m_fXOffset(0.0f),
		m_fYOffset(0.0f),
		m_fZOffset(0.0f)
{
	this->SetRenderNode(pRenderNode);
	this->InitLabel(m_pLabelX, "");
	this->InitLabel(m_pLabelY, "");
	this->InitLabel(m_pLabelZ, "");

	this->InitLabel(m_pMinX, "");
	this->InitLabel(m_pMaxX, "");
	this->InitLabel(m_pMinY, "");
	this->InitLabel(m_pMaxY, "");
	this->InitLabel(m_pMinZ, "");
	this->InitLabel(m_pMaxZ, "");

	m_pLabelX->SetTextFollowViewDir(false);
	m_pLabelY->SetTextFollowViewDir(false);
	m_pLabelZ->SetTextFollowViewDir(false);
	m_pMinX->SetTextFollowViewDir(false);
	m_pMaxX->SetTextFollowViewDir(false);
	m_pMinY->SetTextFollowViewDir(false);
	m_pMaxY->SetTextFollowViewDir(false);
	m_pMinZ->SetTextFollowViewDir(false);
	m_pMaxZ->SetTextFollowViewDir(false);

	VistaQuaternion qXZPlane = VistaQuaternion();
	VistaQuaternion qYZPlane = VistaQuaternion(VistaAxisAndAngle(
		VistaVector3D(0.0f, 1.0f, 0.0f), Vista::DegToRad(90.0f)));

	m_pLabelX->SetOrientation(qXZPlane);
	m_pLabelY->SetOrientation(qXZPlane);
	m_pLabelZ->SetOrientation(qYZPlane);

	m_pLabelX->SetTextSize(0.5f);
	m_pLabelY->SetTextSize(0.5f);
	m_pLabelZ->SetTextSize(0.5f);

	m_pMinX->SetOrientation(qXZPlane);
	m_pMaxX->SetOrientation(qXZPlane);
	m_pMinY->SetOrientation(qXZPlane);
	m_pMaxY->SetOrientation(qXZPlane);
	m_pMinZ->SetOrientation(qYZPlane);
	m_pMaxZ->SetOrientation(qYZPlane);
	m_pMinX->SetTextSize(0.5f);
	m_pMaxX->SetTextSize(0.5f);
	m_pMinY->SetTextSize(0.5f);
	m_pMaxY->SetTextSize(0.5f);
	m_pMinZ->SetTextSize(0.5f);
	m_pMaxZ->SetTextSize(0.5f);

	m_v3Min = VistaVector3D(0.0f, 0.0f, 0.0f);
	m_v3Max = VistaVector3D(0.0f, 0.0f, 0.0f);
}

VflLegendInfo::~VflLegendInfo()
{
	VflRenderNode *pRN = this->GetRenderNode();
	if(pRN)
		pRN->RemoveRenderable(this);
	
	delete m_pLabelX;
	delete m_pLabelY;
	delete m_pLabelZ;

	delete m_pMinX;
	delete m_pMaxX;
	delete m_pMinY;
	delete m_pMaxY;
	delete m_pMinZ;
	delete m_pMaxZ;
}

/*
void VflLegendInfo::SetVisController(VflVisController *pCtl)
{
	VflVisController *pMyCtl = this->GetVisController();
	if(pCtl == pMyCtl)
		return;
	//forward call to parent
	IVflRenderable::SetVisController(pCtl);
}
*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   InitLabel                                                   */
/*                                                                            */
/*============================================================================*/
bool VflLegendInfo::InitLabel(Vfl3DTextLabel *pLabel, 
							   const std::string &strInitText,
							   bool bIsVisible)
{
	if(!pLabel)
		return false;

	pLabel->SetRenderNode(this->GetRenderNode());
	pLabel->Init();
	pLabel->SetText(strInitText);
	pLabel->GetProperties()->SetVisible(bIsVisible);

	return true;
}

//void VflLegendInfo::SetRenderable(IVflRenderable *pRenderable)
//{
//	m_pTarget = pRenderable;
//
//	VflLegendInfo::VflLegendInfoProperties *pProps =
//		dynamic_cast<VflLegendInfo::VflLegendInfoProperties *>(GetProperties());
//
//	// Determine initial text size.
//	if(pProps)
//	{
//		float fMinSize;
//		
//		VistaVector3D v3Min, v3Max;
//		m_pTarget->GetBounds(v3Min, v3Max);
//
//		fMinSize = v3Max[0] - v3Min[0];
//		for(int i=1; i<3; ++i)
//			fMinSize = (v3Max[i] - v3Min[i]) < fMinSize ? (v3Max[i] - v3Min[i]) : fMinSize;
//
//		pProps->SetLegendTextSize(0.075f * fMinSize);
//	}
//
//	// Notify in order to update ranges correctly.
//	pProps->Notify(VflLegendInfo::VflLegendInfoProperties::MSG_RANGES_CHANGED);
//}
//IVflRenderable *VflLegendInfo::GetRenderable() const
//{
//	return m_pTarget;
//}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTargetBoundingBox                                        */
/*                                                                            */
/*============================================================================*/
void VflLegendInfo::SetTargetBoundingBox(VistaVector3D v3Min, VistaVector3D v3Max)
{
	m_v3Min = v3Min;
	m_v3Max = v3Max;

	VflLegendInfo::VflLegendInfoProperties *pProps =
		dynamic_cast<VflLegendInfo::VflLegendInfoProperties *>(GetProperties());

	// Determine initial text size.
	if(pProps)
	{
		float fMinSize = 0.0f;		
		fMinSize = v3Max[0] - v3Min[0];
		for(int i=1; i<3; ++i)
			fMinSize = (v3Max[i] - v3Min[i]) < fMinSize ? (v3Max[i] - v3Min[i]) : fMinSize;

		pProps->SetLegendTextSize(0.075f * fMinSize);
	}

	// Notify in order to update ranges correctly.
	pProps->Notify(VflLegendInfo::VflLegendInfoProperties::MSG_RANGES_CHANGED);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTargetBoundingBox                                        */
/*                                                                            */
/*============================================================================*/
void VflLegendInfo::GetTargetBoundingBox(VistaVector3D &v3Min, VistaVector3D &v3Max) const
{
	v3Min = m_v3Min;
	v3Max = m_v3Max;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflLegendInfo::GetRegistrationMode() const
{
	return OLI_DRAW_OPAQUE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflLegendInfo::ObserverUpdate(IVistaObserveable *pObserveable, 
									int msg, int ticket)
{
	VflLegendInfoProperties *p = this->GetProperties();
	VistaVector3D v3Min, v3Max;

	if(msg == VflLegendInfoProperties::MSG_LABEL_CHANGED)
	{
		std::string strLabelText = p->GetLegendText(0);
		if(strLabelText.empty())
		{
			m_pLabelX->GetProperties()->SetVisible(false);
		}
		else
		{
			m_pLabelX->SetText(strLabelText);
			m_pLabelX->GetProperties()->SetVisible(true);
			m_pLabelX->Update();
			m_pLabelX->GetBounds(v3Min, v3Max);
			m_fTextBoundsX[0] = v3Min[0];
			m_fTextBoundsX[2] = v3Min[1];
			m_fTextBoundsX[4] = v3Min[2];
			m_fTextBoundsX[1] = v3Max[0];
			m_fTextBoundsX[3] = v3Max[1];
			m_fTextBoundsX[5] = v3Max[2];
		}
		// Y AXIS
		strLabelText = p->GetLegendText(1);
		if(strLabelText.empty())
		{
			m_pLabelY->GetProperties()->SetVisible(false);
		}
		else
		{
			m_pLabelY->SetText(strLabelText);
			m_pLabelY->GetProperties()->SetVisible(true);
			m_pLabelY->Update();
			m_pLabelY->GetBounds(v3Min, v3Max);
			m_fTextBoundsY[0] = v3Min[0];
			m_fTextBoundsY[2] = v3Min[1];
			m_fTextBoundsY[4] = v3Min[2];
			m_fTextBoundsY[1] = v3Max[0];
			m_fTextBoundsY[3] = v3Max[1];
			m_fTextBoundsY[5] = v3Max[2];
		}
		//Z AXIS
		strLabelText = p->GetLegendText(2);
		if(strLabelText.empty())
			m_pLabelZ->GetProperties()->SetVisible(false);
		else
		{
			m_pLabelZ->SetText(strLabelText);
			m_pLabelZ->GetProperties()->SetVisible(true);
			m_pLabelZ->Update();
			m_pLabelZ->GetBounds(v3Min, v3Max);
			m_fTextBoundsZ[0] = v3Min[0];
			m_fTextBoundsZ[2] = v3Min[1];
			m_fTextBoundsZ[4] = v3Min[2];
			m_fTextBoundsZ[1] = v3Max[0];
			m_fTextBoundsZ[3] = v3Max[1];
			m_fTextBoundsZ[5] = v3Max[2];
		}
		//m_pTarget->GetBounds(m_v3Min, m_v3Max);
		float fBounds[6] = {m_v3Min[0], m_v3Max[0],
							m_v3Min[1], m_v3Max[1],
							m_v3Min[2], m_v3Max[2]};
		this->ResetLabelPositions(fBounds);
	}
	else if(msg == VflLegendInfoProperties::MSG_LEGENDCOLOR_CHANGED)
	{
		float fC[4] = {p->m_fColor[0], p->m_fColor[1], p->m_fColor[2], 1.0f};
		m_pLabelX->SetColor(fC);
		m_pLabelY->SetColor(fC);
		m_pLabelZ->SetColor(fC);
		m_pMinX->SetColor(fC);
		m_pMaxX->SetColor(fC);
		m_pMinY->SetColor(fC);
		m_pMaxY->SetColor(fC);
		m_pMinZ->SetColor(fC);
		m_pMaxZ->SetColor(fC);
	}
	else if(msg == VflLegendInfoProperties::MSG_OVERRIDEBOUNDS_CHANGED ||
		msg == VflLegendInfoProperties::MSG_RANGES_CHANGED)
	{
		if(p->m_bOverrideBounds)
		{
			std::string sTmp;
			std::ostringstream sStrStream;
			sStrStream.precision (2);
			sStrStream.setf (std::ios_base::fixed, std::ios_base::floatfield);
			//sStrStream.width (6);
			sStrStream << p->m_fRanges[0];
			m_pMinX->SetText(sStrStream.str());
						
			sStrStream.str(""); sStrStream.clear();
			sStrStream << p->m_fRanges[1];
			m_pMaxX->SetText(sStrStream.str());
					
			sStrStream.str(""); sStrStream.clear();
			sStrStream << p->m_fRanges[2];
			m_pMinY->SetText(sStrStream.str());
						
			sStrStream.str(""); sStrStream.clear();
			sStrStream << p->m_fRanges[3];
			m_pMaxY->SetText(sStrStream.str());
						
			sStrStream.str(""); sStrStream.clear();
			sStrStream << p->m_fRanges[4];
			m_pMinZ->SetText(sStrStream.str());
			
			sStrStream.str(""); sStrStream.clear();
			sStrStream << p->m_fRanges[5];
			m_pMaxZ->SetText(sStrStream.str());
		}
		else
		{
			//m_pTarget->GetBounds(m_v3Min, m_v3Max);
			m_pMinX->SetText(VistaAspectsConversionStuff::ConvertToString(m_v3Min[0]));
			m_pMaxX->SetText(VistaAspectsConversionStuff::ConvertToString(m_v3Max[0]));
			m_pMinY->SetText(VistaAspectsConversionStuff::ConvertToString(m_v3Min[1]));
			m_pMaxY->SetText(VistaAspectsConversionStuff::ConvertToString(m_v3Max[1]));
			m_pMinZ->SetText(VistaAspectsConversionStuff::ConvertToString(m_v3Min[2]));
			m_pMaxZ->SetText(VistaAspectsConversionStuff::ConvertToString(m_v3Max[2]));
		}

		m_pMinX->SetVisible(true);
		m_pMaxX->SetVisible(true);
		m_pMinY->SetVisible(true);
		m_pMaxY->SetVisible(true);
		m_pMinZ->SetVisible(true);
		m_pMaxZ->SetVisible(true);
	
		m_pMinX->Update();
		m_pMaxX->Update();
		m_pMinY->Update();
		m_pMaxY->Update();
		m_pMinZ->Update();
		m_pMaxZ->Update();

		//m_pTarget->GetBounds(m_v3Min, m_v3Max);
		float fBounds[6] = {m_v3Min[0], m_v3Max[0],
							m_v3Min[1], m_v3Max[1],
							m_v3Min[2], m_v3Max[2]};
		this->UpdateMinMaxLabels(fBounds);	
		this->UpdateAxesOffsets();
	}
	else if(msg == VflLegendInfoProperties::MSG_DRAWZERO_CHANGED)
	{
		float fBounds[6] = {m_v3Min[0], m_v3Max[0],
							m_v3Min[1], m_v3Max[1],
							m_v3Min[2], m_v3Max[2]};
		this->UpdateAxesOffsets();
		this->UpdateMinMaxLabels(fBounds);
		this->ResetLabelPositions(fBounds);
	}
	else if(msg == VflLegendInfoProperties::MSG_LABELALIGNMENT_CHANGED ||
		msg == VflLegendInfoProperties::MSG_ORIGINOFFSET_CHANGED ||
		msg == VflLegendInfoProperties::MSG_LEGENDFONTSIZE_CHANGED ||
		msg == VflLegendInfoProperties::MSG_LEGENDOFFSET_CHANGED)
	{
		VflLegendInfoProperties *pProps = 
			dynamic_cast<VflLegendInfoProperties*>(this->GetProperties());

		m_pLabelX->SetTextSize(pProps->GetLegendTextSize());
		m_pLabelY->SetTextSize(pProps->GetLegendTextSize());
		m_pLabelZ->SetTextSize(pProps->GetLegendTextSize());
	
		m_pMinX->SetTextSize(pProps->GetLegendTextSize());
		m_pMaxX->SetTextSize(pProps->GetLegendTextSize());
		m_pMinY->SetTextSize(pProps->GetLegendTextSize());
		m_pMaxY->SetTextSize(pProps->GetLegendTextSize());
		m_pMinZ->SetTextSize(pProps->GetLegendTextSize());
		m_pMaxZ->SetTextSize(pProps->GetLegendTextSize());

		m_pLabelX->Update();
		m_pLabelY->Update();
		m_pLabelZ->Update();
		m_pMinX->Update();
		m_pMaxX->Update();
		m_pMinY->Update();
		m_pMaxY->Update();
		m_pMinZ->Update();
		m_pMaxZ->Update();

		//m_pTarget->GetBounds(m_v3Min, m_v3Max);
		float fBounds[6] = {m_v3Min[0], m_v3Max[0],
							m_v3Min[1], m_v3Max[1],
							m_v3Min[2], m_v3Max[2]};

		ResetLabelPositions(fBounds);
		UpdateMinMaxLabels(fBounds);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties()                                             */
/*                                                                            */
/*============================================================================*/
VflLegendInfo::VflLegendInfoProperties*
VflLegendInfo::GetProperties() const
{
	return static_cast<VflLegendInfoProperties*>(
		IVflRenderable::GetProperties());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ResetLabelPositions()                                       */
/*                                                                            */
/*============================================================================*/
void VflLegendInfo::ResetLabelPositions(float fPlotBounds[6])
{
	float fPos[3];
	VistaVector3D v3LblMin, v3LblMax;

	float fOriginOffset[3];
	float fXLabelOffset=0.0f, fYLabelOffset=0.0f, fZLabelOffset=0.0f;
	VflLegendInfoProperties *p = static_cast<VflLegendInfoProperties*>(GetProperties());
	p->GetLabelOffset(fXLabelOffset, fYLabelOffset, fZLabelOffset);
	p->GetOriginOffset(fOriginOffset);

	//m_oAxesNamesMutex.Lock();

	// --------------------------------------------------------------------- //
	// X Label.
	m_pLabelX->GetBounds(v3LblMin, v3LblMax);
	if(p->GetLabelAlignment(0) == VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_AUTO)
	{
		fPos[0] = ((fPlotBounds[0] + fPlotBounds[1]) - (v3LblMax[0] - v3LblMin[0])) * 0.5f;
		fPos[1] = fPlotBounds[2] - (v3LblMax[1] - v3LblMin[1]); // + fOffset[0];
	}
	else
	{
		 if(p->GetLabelAlignment(0) == VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_MIN)
			 fPos[0] = fPlotBounds[0] - 1.2f * (v3LblMax[0] - v3LblMin[0]);
		 else if(p->GetLabelAlignment(0) == VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_MAX)
			 fPos[0] = fPlotBounds[1] + 0.1f * (v3LblMax[0] - v3LblMin[0]);
		 
		 fPos[1] = fPlotBounds[2] + 0.25f * (v3LblMax[1] - v3LblMin[1]);
	}
	fPos[2] = fPlotBounds[4] - (v3LblMax[2] - v3LblMin[2]) * 0.5f;
	
	fPos[0] += fOriginOffset[0];
	fPos[1] += fOriginOffset[1] + m_fYOffset + fXLabelOffset;
	fPos[2] += fOriginOffset[2] + m_fZOffset;
	m_pLabelX->SetPosition(fPos);
	m_pLabelX->Update();

	// --------------------------------------------------------------------- //
	// Y Label.
	m_pLabelY->GetBounds(v3LblMin, v3LblMax);

	fPos[0] = fPlotBounds[0] - 0.5f * (v3LblMax[0] - v3LblMin[0]);
	if(p->GetLabelAlignment(1) == VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_AUTO)
		fPos[1] = ((fPlotBounds[2] + fPlotBounds[3]) - (v3LblMax[1] - v3LblMin[1])) * 0.5f;
	else if(p->GetLabelAlignment(1) == VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_MIN)
		fPos[1] = fPlotBounds[2] - 1.2f * (v3LblMax[1] - v3LblMin[1]);
	else if(p->GetLabelAlignment(1) == VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_MAX)
		fPos[1] = fPlotBounds[3] + 0.1f * (v3LblMax[1] - v3LblMin[1]);
	fPos[2] = fPlotBounds[4] - (abs(v3LblMax[2] - v3LblMin[2])); //+ fOffset[1];
	
	fPos[0] += fOriginOffset[0] + m_fXOffset + fYLabelOffset;
	fPos[1] += fOriginOffset[1];
	fPos[2] += fOriginOffset[2] + m_fZOffset;
	m_pLabelY->SetPosition(fPos);
	m_pLabelY->Update();
	
	// --------------------------------------------------------------------- //
	// Z Label.
	m_pLabelZ->GetBounds(v3LblMin, v3LblMax);
	fPos[0] = fPlotBounds[0]; // + fOffset[2];
	
	if(p->GetLabelAlignment(2) == VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_AUTO)
	{
		fPos[1] = fPlotBounds[2] - (v3LblMax[1] - v3LblMin[1]);
		fPos[2] = ((fPlotBounds[4] + fPlotBounds[5]) + (abs(v3LblMax[2] - v3LblMin[2]))) * 0.5f;
	}
	else
	{
		fPos[1] = fPlotBounds[2] + 0.25f * (v3LblMax[1] - v3LblMin[1]);
		if(p->GetLabelAlignment(2) == VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_MIN)
			fPos[2] = fPlotBounds[5] + 1.2f * (v3LblMax[2] - v3LblMin[2]);
		else if(p->GetLabelAlignment(2) == VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_MAX)
			fPos[2] = fPlotBounds[4] - 0.1f * (v3LblMax[2] - v3LblMin[2]);
	}
		
	fPos[0] += fOriginOffset[0] + m_fXOffset;
	fPos[1] += fOriginOffset[1] + m_fYOffset + fZLabelOffset;
	fPos[2] += fOriginOffset[2];
	m_pLabelZ->SetPosition(fPos);
	m_pLabelZ->Update();

	//m_oAxesNamesMutex.Unlock();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateMinMaxLabels                                          */
/*                                                                            */
/*============================================================================*/
void VflLegendInfo::UpdateMinMaxLabels(float fBounds[6])
{
	float fPos[3];
	VistaVector3D v3LblMin, v3LblMax;

	float fOriginOffset[3];
	float fXMinMaxOffset=0.0f, fYMinMaxOffset=0.0f, fZMinMaxOffset=0.0f;
	VflLegendInfoProperties *p = 
		static_cast<VflLegendInfoProperties*>(GetProperties());
	p->GetMinMaxOffset(fXMinMaxOffset, fYMinMaxOffset, fZMinMaxOffset);
	p->GetOriginOffset(fOriginOffset);

	// ===== min x =====
	m_pMinX->GetBounds(v3LblMin, v3LblMax);
	fPos[0] = fBounds[0];
	fPos[1] = fBounds[2] - (v3LblMax[1] - v3LblMin[1]);
	fPos[2] = fBounds[4];

	if(p->GetMinMaxAlignment(0) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_OUTSIDE)
		fPos[0] -= 1.1f * (v3LblMax[0] - v3LblMin[0]);
	else if(p->GetMinMaxAlignment(0) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_CENTER)
		fPos[0] -= 0.5f * (v3LblMax[0] - v3LblMin[0]);
	else // MINMAX_ALIGN_INSIDE
		fPos[0] -= 0.1f * (v3LblMax[0] - v3LblMin[0]);
	
	fPos[0] += fOriginOffset[0];
	fPos[1] += fOriginOffset[1] + m_fYOffset + fXMinMaxOffset;
	fPos[2] += fOriginOffset[0] + m_fZOffset;
	m_pMinX->SetPosition(fPos);
	m_pMinX->Update();

	// ===== max x =====
	m_pMaxX->GetBounds(v3LblMin, v3LblMax);
	fPos[0] = fBounds[1];
	fPos[1] = fBounds[2] - (v3LblMax[1] - v3LblMin[1]);
	fPos[2] = fBounds[4];

	if(p->GetMinMaxAlignment(0) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_OUTSIDE)
		fPos[0] += 0.0f;
	else if(p->GetMinMaxAlignment(0) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_CENTER)
		fPos[0] -= 0.5f * (v3LblMax[0] - v3LblMin[0]);
	else // MINMAX_ALIGN_INSIDE
		fPos[0] -= 1.1f * (v3LblMax[0] - v3LblMin[0]);
	
	fPos[0] += fOriginOffset[0];
	fPos[1] += fOriginOffset[1] + m_fYOffset + fXMinMaxOffset;
	fPos[2] += fOriginOffset[2] + m_fZOffset;
	m_pMaxX->SetPosition(fPos);
	m_pMaxX->Update();

	// ===== min y =====
	m_pMinY->GetBounds(v3LblMin, v3LblMax);
	fPos[0] = fBounds[0] + (v3LblMax[0] - v3LblMin[0]);
	fPos[1] = fBounds[2]; // - 1.2f * (v3LblMax[1] - v3LblMin[1]);
	fPos[2] = fBounds[4] - (v3LblMax[2] - v3LblMin[2]);

	if(p->GetMinMaxAlignment(1) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_OUTSIDE)
		fPos[1] -= 1.1f * (v3LblMax[1] - v3LblMin[1]);
	else if(p->GetMinMaxAlignment(1) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_CENTER)
		fPos[1] -= 0.5f * (v3LblMax[1] - v3LblMin[1]);
	else // MINMAX_ALIGN_INSIDE
		fPos[1] -= 0.1f * (v3LblMax[1] - v3LblMin[1]);
	
	fPos[0] += fOriginOffset[0] + m_fXOffset + fYMinMaxOffset;
	fPos[1] += fOriginOffset[1];
	fPos[2] += fOriginOffset[2] + m_fZOffset;
	m_pMinY->SetPosition(fPos);
	m_pMinY->Update();

	// ===== max y =====
	m_pMaxY->GetBounds(v3LblMin, v3LblMax);
	fPos[0] = fBounds[0] + (v3LblMax[0] - v3LblMin[0]);
	fPos[1] = fBounds[3]; // + 0.1f * (v3LblMax[1] - v3LblMin[1]);
	fPos[2] = fBounds[4] - (v3LblMax[2] - v3LblMin[2]);
	
	if(p->GetMinMaxAlignment(1) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_OUTSIDE)
		fPos[1] += 0.0f;
	else if(p->GetMinMaxAlignment(1) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_CENTER)
		fPos[1] -= 0.5f * (v3LblMax[1] - v3LblMin[1]);
	else // MINMAX_ALIGN_INSIDE
		fPos[1] -= 1.1f * (v3LblMax[1] - v3LblMin[1]);

	fPos[0] += fOriginOffset[0] + m_fXOffset + fYMinMaxOffset;
	fPos[1] += fOriginOffset[1];
	fPos[2] += fOriginOffset[2] + m_fZOffset;
	m_pMaxY->SetPosition(fPos);
	m_pMaxY->Update();

	// ===== min z =====
	m_pMinZ->GetBounds(v3LblMin, v3LblMax);
	fPos[0] = fBounds[0];
	fPos[1] = fBounds[2] - (v3LblMax[1] - v3LblMin[1]) + fZMinMaxOffset;
	fPos[2] = fBounds[5]; // + 1.2f * (v3LblMax[2] - v3LblMin[2]); 
	
	if(p->GetMinMaxAlignment(2) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_OUTSIDE)
		fPos[2] += 1.1f * (v3LblMax[2] - v3LblMin[2]);
	else if(p->GetMinMaxAlignment(2) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_CENTER)
		fPos[2] += 0.5f * (v3LblMax[2] - v3LblMin[2]);
	else // MINMAX_ALIGN_INSIDE
		fPos[2] += 0.1f * (v3LblMax[2] - v3LblMin[2]);

	fPos[0] += fOriginOffset[0] + m_fXOffset;
	fPos[1] += fOriginOffset[1] + m_fYOffset;
	fPos[2] += fOriginOffset[2];
	m_pMinZ->SetPosition(fPos);
	m_pMinZ->Update();

	// ===== max z =====
	m_pMaxZ->GetBounds(v3LblMin, v3LblMax);
	fPos[0] = fBounds[0];
	fPos[1] = fBounds[2] - (v3LblMax[1] - v3LblMin[1]) + fZMinMaxOffset;
	fPos[2] = fBounds[4]; // - 0.1f * (v3LblMax[2] - v3LblMin[2]);
	
	if(p->GetMinMaxAlignment(2) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_OUTSIDE)
		fPos[2] += 0.0f;
	else if(p->GetMinMaxAlignment(2) == VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_CENTER)
		fPos[2] += 0.5f * (v3LblMax[2] - v3LblMin[2]);
	else // MINMAX_ALIGN_INSIDE
		fPos[2] += 1.1f * (v3LblMax[2] - v3LblMin[2]);

	fPos[0] += fOriginOffset[0] + m_fXOffset;
	fPos[1] += fOriginOffset[1] + m_fYOffset;
	fPos[2] += fOriginOffset[2];
	m_pMaxZ->SetPosition(fPos);
	m_pMaxZ->Update();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   UpdateAxesOffset()										  */
/*                                                                            */
/*============================================================================*/
void VflLegendInfo::UpdateAxesOffsets()
{
	float fXRanges[2], fYRanges[2], fZRanges[2];
	float fXBounds[2], fYBounds[2], fZBounds[2];
	VistaVector3D v3Min, v3Max;


	VflLegendInfoProperties *pProps = 
		dynamic_cast<VflLegendInfoProperties*>(this->GetProperties());
	//m_pTarget->GetBounds(m_v3Min, m_v3Max);

	// Preparation for axes-offset computation.
	if(pProps->GetOverrideBounds())
	{
		fXRanges[0] = pProps->m_fRanges[0];
		fXRanges[1] = pProps->m_fRanges[1];
		fYRanges[0] = pProps->m_fRanges[2];
		fYRanges[1] = pProps->m_fRanges[3];
		fZRanges[0] = pProps->m_fRanges[4];
		fZRanges[1] = pProps->m_fRanges[5];
	}
	else
	{
		fXRanges[0] = m_v3Min[0];
		fXRanges[1] = m_v3Max[0];
		fYRanges[0] = m_v3Min[1];
		fYRanges[1] = m_v3Max[1];
		fZRanges[0] = m_v3Min[2];
		fZRanges[1] = m_v3Max[2];
	}

	fXBounds[0] = m_v3Min[0];
	fXBounds[1] = m_v3Max[0];
	fYBounds[0] = m_v3Min[1];
	fYBounds[1] = m_v3Max[1];
	fZBounds[0] = m_v3Min[2];
	fZBounds[1] = m_v3Max[2];

	if(pProps->GetDrawLegendAxesAtRangeZero())
	{
		m_fXOffset = ComputeZeroAxesOffset(fXRanges, fXBounds);
		m_fYOffset = ComputeZeroAxesOffset(fYRanges, fYBounds);
		m_fZOffset = ComputeZeroAxesOffset(fZRanges, fZBounds);
	}
	else
	{
		m_fXOffset = m_fYOffset = m_fZOffset = 0.0f;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeZeroAxesOffset()                                     */
/*                                                                            */
/*============================================================================*/
float VflLegendInfo::ComputeZeroAxesOffset(float fRange[2], float fAxesBounds[2])
{
	// if 0 lies somewhere between fRange[0] and fRange[1]
	if(fRange[0]<0 && fRange[1]>0)
	{
		return ((fRange[0]*-1.0f)/(fRange[1]-fRange[0])) * (fAxesBounds[1]-fAxesBounds[0]); 
	}
	else // both are positive / negative
	{
		return 0.0f;
	}

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflLegendInfo::DrawOpaque()
{
	//won't draw without visible target!
	//if(!m_pTarget || !m_pTarget->GetProperties()->GetVisible())
		//return;

	VflLegendInfoProperties *p = static_cast<VflLegendInfoProperties*>(GetProperties());
	//m_pTarget->GetBounds(m_v3Min, m_v3Max);
	float fDelta[3] = {	m_v3Max[0] - m_v3Min[0],
						m_v3Max[1] - m_v3Min[1],
						m_v3Max[2] - m_v3Min[2] };
	
	glPushAttrib(GL_LINE_BIT | GL_LIGHTING_BIT );

	/**
	 * draw labels first
	 */
	//for(int i=0; i<3; ++i)
	//{
	//	m_pAxesLabels[i]->DrawOpaque();
	//	m_pRangeLabels[2*i]->DrawOpaque();
	//	m_pRangeLabels[2*i+1]->DrawOpaque();
	//}
	if (p->GetEnableAxis (0))
	{
		m_pLabelX->DrawOpaque();
		m_pMinX->DrawOpaque();
		m_pMaxX->DrawOpaque();
	}

	if (p->GetEnableAxis (1))
	{
		m_pLabelY->DrawOpaque();
		m_pMinY->DrawOpaque();
		m_pMaxY->DrawOpaque();
	}
	if (p->GetEnableAxis (2))
	{
		m_pLabelZ->DrawOpaque();
		m_pMinZ->DrawOpaque();
		m_pMaxZ->DrawOpaque();
	}

	/**
	 * draw legend
	 */
	float fTickSize = 0.005f * std::max(fDelta[0], std::max(fDelta[1], fDelta[2]));

	glLineWidth(p->m_fLineWidth);
    glDisable(GL_LIGHTING);
    // draw bounding volume
    glColor3fv(p->m_fColor);

	glBegin(GL_LINES);
	if (p->GetEnableAxis (0))
	{
		//X axis
		glVertex3f(m_v3Min[0], m_v3Min[1]+m_fYOffset, m_v3Min[2]+m_fZOffset);
		glVertex3f(m_v3Max[0], m_v3Min[1]+m_fYOffset, m_v3Min[2]+m_fZOffset);
		//tag the end
		glVertex3f(m_v3Max[0], m_v3Min[1]+m_fYOffset, m_v3Min[2]+m_fZOffset);
		glVertex3f(m_v3Max[0], m_v3Min[1]+m_fYOffset-fTickSize, m_v3Min[2]+m_fZOffset);
	}
	if (p->GetEnableAxis (1))
	{
		//Y axis
		glVertex3f(m_v3Min[0]+m_fXOffset, m_v3Min[1], m_v3Min[2]+m_fZOffset);
		glVertex3f(m_v3Min[0]+m_fXOffset, m_v3Max[1], m_v3Min[2]+m_fZOffset);
		//tag the end
		glVertex3f(m_v3Min[0]+m_fXOffset, m_v3Max[1], m_v3Min[2]+m_fZOffset);
		glVertex3f(m_v3Min[0]+m_fXOffset-fTickSize, m_v3Max[1], m_v3Min[2]+m_fZOffset);
	}
	if (p->GetEnableAxis (2))
	{
		//Z axis
		glVertex3f(m_v3Min[0]+m_fXOffset, m_v3Min[1]+m_fYOffset, m_v3Min[2]);
		glVertex3f(m_v3Min[0]+m_fXOffset, m_v3Min[1]+m_fYOffset, m_v3Max[2]);
		//tag the end
		glVertex3f(m_v3Min[0]+m_fXOffset, m_v3Min[1]+m_fYOffset, m_v3Max[2]);
		glVertex3f(m_v3Min[0]+m_fXOffset-fTickSize, m_v3Min[1]+m_fYOffset, m_v3Max[2]);
	}
	glEnd();

    // reset local settings
	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
void VflLegendInfo::Update()
{
	m_pLabelX->Update(); //m_pAxesLabels[0]->Update();
	m_pLabelY->Update(); //m_pAxesLabels[1]->Update();
	m_pLabelZ->Update(); //m_pAxesLabels[2]->Update();
	
	m_pMinX->Update();   //m_pRangeLabels[0]->Update();
	m_pMaxX->Update();   //m_pRangeLabels[1]->Update();
	m_pMinY->Update();   //m_pRangeLabels[2]->Update();
	m_pMaxY->Update();   //m_pRangeLabels[3]->Update();
	m_pMinZ->Update();   //m_pRangeLabels[4]->Update();
	m_pMaxZ->Update();   //m_pRangeLabels[5]->Update();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflLegendInfo::GetType() const
{
    return IVflRenderable::GetType()+string("::VflLegendInfo");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties *VflLegendInfo::CreateProperties() const
{
	return new VflLegendInfoProperties;
}


/*============================================================================*/
/*  methods of VflLegendInfo::CProperties                                    */
/*============================================================================*/

static const std::string SsReflectionType("VflLegendInfo");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<float, VflLegendInfo::VflLegendInfoProperties, VistaProperty::PROPT_DOUBLE>
			("LINE_WIDTH", SsReflectionType,
				&VflLegendInfo::VflLegendInfoProperties::GetLineWidth),
	new TVistaProperty3RefGet<float, VflLegendInfo::VflLegendInfoProperties>
			( "COLOR", SsReflectionType,
				&VflLegendInfo::VflLegendInfoProperties::GetLegendColor),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<float, float,
	                    VflLegendInfo::VflLegendInfoProperties>
		("LINE_WIDTH", SsReflectionType,
		&VflLegendInfo::VflLegendInfoProperties::SetLineWidth),
	new TVistaProperty3ValSet<float, VflLegendInfo::VflLegendInfoProperties>
		( "COLOR", SsReflectionType,
		&VflLegendInfo::VflLegendInfoProperties::SetLegendColor),
	NULL
};

VflLegendInfo::VflLegendInfoProperties::VflLegendInfoProperties()
	:	IVflRenderable::VflRenderableProperties(),
		m_bOverrideBounds(false),
		m_bDrawAtZero(true),
		m_fFontSize(0.5f),
		m_fLineWidth(2.0f)
{
	m_strLabels[0] = "x";
	m_strLabels[1] = "y";
	m_strLabels[2] = "z";

	m_fRanges[0] = m_fRanges[1] = 0.0f;
	m_fRanges[2] = m_fRanges[3] = 0.0f;
	m_fRanges[4] = m_fRanges[5] = 0.0f;

	m_fDeltas[0] = 0.0f;
	m_fDeltas[1] = 0.0f;
	m_fDeltas[2] = 0.0f;

	m_fXLabelOffset = 0.0f;
	m_fYLabelOffset = 0.0f;
	m_fZLabelOffset = 0.0f;
	m_fXMinMaxOffset = 0.0f;
	m_fYMinMaxOffset = 0.0f;
	m_fZMinMaxOffset = 0.0f;

	m_fColor[0] = 0.0f;
	m_fColor[1] = 0.0f;
	m_fColor[2] = 0.0f;

	m_fOriginOffset[0] = 0.0f;
	m_fOriginOffset[1] = 0.0f;
	m_fOriginOffset[2] = 0.0f;

	m_iLblAlignment[0] = VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_AUTO;
	m_iLblAlignment[1] = VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_AUTO;
	m_iLblAlignment[2] = VflLegendInfo::VflLegendInfoProperties::LBL_ALIGN_AUTO;

	m_iMinMaxAlignment[0] = VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_OUTSIDE;
	m_iMinMaxAlignment[1] = VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_OUTSIDE;
	m_iMinMaxAlignment[2] = VflLegendInfo::VflLegendInfoProperties::MINMAX_ALIGN_OUTSIDE;

	m_bEnableAxis[0] = true;
	m_bEnableAxis[1] = true;
	m_bEnableAxis[2] = true;

}

VflLegendInfo::VflLegendInfoProperties::~VflLegendInfoProperties()
{
}

string VflLegendInfo::VflLegendInfoProperties::GetReflectionableType() const
{
	return SsReflectionType;
}


int VflLegendInfo::VflLegendInfoProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = IVflRenderable::VflRenderableProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize+1;
}

bool VflLegendInfo::VflLegendInfoProperties::SetLegendText(int iAxis, const std::string& strText)
{
	if(compAndAssignFunc(strText, m_strLabels[iAxis]))
	{
		this->Notify(MSG_LABEL_CHANGED);
		return true;
	}
	return false;
}
std::string VflLegendInfo::VflLegendInfoProperties::GetLegendText(int iAxis) const
{
	return m_strLabels[iAxis];
}


bool VflLegendInfo::VflLegendInfoProperties::SetLegendRanges(float fRanges[6])
{
	bool bChange = false;
	for(int i=0; i<6; ++i)
		bChange |= (compAndAssignFunc(fRanges[i], m_fRanges[i]) ? true : false);
	if(bChange)
	{
		this->Notify(MSG_RANGES_CHANGED);
		return true;
	}
	return false;
}

void VflLegendInfo::VflLegendInfoProperties::GetLegendRanges(float fRanges[6]) const
{
	memcpy(fRanges, m_fRanges, 6*sizeof(float));
}

bool VflLegendInfo::VflLegendInfoProperties::SetOverrideBounds(bool b)
{
	if(compAndAssignFunc(b, m_bOverrideBounds))
	{
		this->Notify(MSG_OVERRIDEBOUNDS_CHANGED);
		return true;
	}
	return false;
}
bool VflLegendInfo::VflLegendInfoProperties::GetOverrideBounds() const
{
	return m_bOverrideBounds;
}

/**
* if there is a sign change in any of the axes' ranges
* SetDrawLegendAxesAtRangeZero will draw the axes at the zero
* point in the corresponding range.
*/
bool VflLegendInfo::VflLegendInfoProperties::SetDrawLegendAxesAtRangeZero(bool b)
{
	if(compAndAssignFunc(b, m_bDrawAtZero))
	{
		this->Notify(MSG_DRAWZERO_CHANGED);
		return true;
	}
	return false;
}

bool VflLegendInfo::VflLegendInfoProperties::GetDrawLegendAxesAtRangeZero() const
{
	return m_bDrawAtZero;
}


bool VflLegendInfo::VflLegendInfoProperties::SetLabelOffset(
	float fXLabelOffset, float fYLabelOffset, float fZLabelOffset)
{
	bool bChg = false;
	bChg |= (compAndAssignFunc(fXLabelOffset, m_fXLabelOffset) ? true : false);
	bChg |= (compAndAssignFunc(fYLabelOffset, m_fYLabelOffset) ? true : false);
	bChg |= (compAndAssignFunc(fZLabelOffset, m_fZLabelOffset) ? true : false);
	if(bChg)
	{
		this->Notify(MSG_LEGENDOFFSET_CHANGED);
		return true;
	}
	return false;
}

void VflLegendInfo::VflLegendInfoProperties::GetLabelOffset(
	float &fXLabelOffset, float &fYLabelOffset, float &fZLabelOffset) const
{
	fXLabelOffset = m_fXLabelOffset;
	fYLabelOffset = m_fYLabelOffset;
	fZLabelOffset = m_fZLabelOffset;
}

bool VflLegendInfo::VflLegendInfoProperties::SetMinMaxOffset(
	float fXMinMaxOffset, float fYMinMaxOffset, float fZMinMaxOffset)
{
	bool bChg = false;
	bChg |= (compAndAssignFunc(fXMinMaxOffset, m_fXMinMaxOffset) ? true : false);
	bChg |= (compAndAssignFunc(fYMinMaxOffset, m_fYMinMaxOffset) ? true : false);
	bChg |= (compAndAssignFunc(fZMinMaxOffset, m_fZMinMaxOffset) ? true : false);
	if(bChg)
	{
		this->Notify(MSG_LEGENDOFFSET_CHANGED);
		return true;
	}
	return false;
}

void VflLegendInfo::VflLegendInfoProperties::GetMinMaxOffset(
	float &fXMinMaxOffset, float &fYMinMaxOffset, float &fZMinMaxOffset) const
{
	fXMinMaxOffset = m_fXMinMaxOffset;
	fYMinMaxOffset = m_fYMinMaxOffset;
	fZMinMaxOffset = m_fZMinMaxOffset;
}


//bool VflLegendInfo::VflLegendInfoProperties::SetLegendOffset(float fOffset[3])
//{
//	bool bChg = false;
//	for(int i=0; i<3; ++i)
//		bChg |= (compAndAssignFunc(fOffset[i], m_fOffset[i]) ? true : false);
//	if(bChg)
//	{
//		this->Notify(MSG_LEGENDOFFSET_CHANGED);
//		return true;
//	}
//	return false;
//}
//
//void VflLegendInfo::VflLegendInfoProperties::GetLegendOffset(float fOffset[3]) const
//{
//	memcpy(fOffset, m_fOffset, 3*sizeof(float));
//}


bool VflLegendInfo::VflLegendInfoProperties::SetLegendColor(
												float r, float g, float b)
{
	bool bChange = (compAndAssignFunc<float>(r, m_fColor[0]) == 1) ? true : false;
	bChange |= ((compAndAssignFunc<float>(g, m_fColor[1]) == 1) ? true : false) || bChange;
	bChange |= ((compAndAssignFunc<float>(b, m_fColor[2]) == 1) ? true : false) || bChange;
	if(bChange)
	{
		Notify(MSG_LEGENDCOLOR_CHANGED);
		return true;
	}

	return false;
}

bool VflLegendInfo::VflLegendInfoProperties::GetLegendColor(
											float &r, float &g, float &b) const
{
	r = m_fColor[0];
	g = m_fColor[1];
	b = m_fColor[2];
	return true;
}


bool VflLegendInfo::VflLegendInfoProperties::SetLegendTextSize(float fFontSize)
{
	fFontSize = fFontSize < 0.0f ? 0.0f : fFontSize;
	
	if(m_fFontSize == fFontSize)
		return false;

	m_fFontSize = fFontSize;

	Notify(MSG_LEGENDFONTSIZE_CHANGED);
	return true;
}

float VflLegendInfo::VflLegendInfoProperties::GetLegendTextSize() const
{
	return m_fFontSize;
}


bool VflLegendInfo::VflLegendInfoProperties::SetLineWidth(float fLineWidth)
{
	if(compAndAssignFunc<float>(fLineWidth, m_fLineWidth) == 1)
	{
		Notify(MSG_LINEWIDTH_CHANGED);
		return true;
	}
	return false;
}

float VflLegendInfo::VflLegendInfoProperties::GetLineWidth() const
{
	return m_fLineWidth;
}


bool VflLegendInfo::VflLegendInfoProperties::SetOriginOffset(float fOO[3])
{
	memcpy(m_fOriginOffset, fOO, 3*sizeof(float));

	Notify(MSG_ORIGINOFFSET_CHANGED);

	return true;
}

bool VflLegendInfo::VflLegendInfoProperties::GetOriginOffset(float fOO[3]) const
{
	memcpy(fOO, m_fOriginOffset, 3*sizeof(float));

	return true;
}

bool VflLegendInfo::VflLegendInfoProperties::SetLabelAlignment
	(int iAxis,
	VflLegendInfo::VflLegendInfoProperties::eLabelAlignment eLblAlign)
{
	if(iAxis < 0 || iAxis > 2)
		return false;

	m_iLblAlignment[iAxis] = eLblAlign;

	Notify(MSG_LABELALIGNMENT_CHANGED);

	return true;
}

int VflLegendInfo::VflLegendInfoProperties::GetLabelAlignment(int iAxis) const
{
	if(iAxis < 0 || iAxis > 2)
		return -1;
	
	return m_iLblAlignment[iAxis];
}

bool VflLegendInfo::VflLegendInfoProperties::SetMinMaxAlignment(int iAxis, eMinMaxAlignment eMMAlign)
{
	if(iAxis < 0 || iAxis > 2)
		return false;

	m_iMinMaxAlignment[iAxis] = eMMAlign;
	Notify(MSG_LABELALIGNMENT_CHANGED);
	return true;
}

int VflLegendInfo::VflLegendInfoProperties::GetMinMaxAlignment(int iAxis) const
{
	if(iAxis < 0 || iAxis > 2)
		return -1;

	return m_iMinMaxAlignment[iAxis];
}


bool VflLegendInfo::VflLegendInfoProperties::SetEnableAxis(int iAxis, bool bEnable)
{
	if(iAxis < 0 || iAxis > 2)
		return false;

	m_bEnableAxis[iAxis] = bEnable;

	Notify(MSG_LABEL_CHANGED);

	return true;
}

bool VflLegendInfo::VflLegendInfoProperties::GetEnableAxis(int iAxis) const
{
	if(iAxis < 0 || iAxis > 2)
		return false;

	return m_bEnableAxis[iAxis];
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflLegendInfo.cpp"                                           */
/*============================================================================*/

