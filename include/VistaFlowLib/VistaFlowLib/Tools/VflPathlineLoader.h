/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLPATHLINELOADER_H
#define _VFLPATHLINELOADER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaVisExt/IO/VveUnsteadyDataLoader.h>
#include <VistaFlowLib/VistaFlowLibConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflPathlineLoader loads precalculated pathlines from disk by 
 * using different file format readers.
 */    
class VISTAFLOWLIBAPI VflPathlineLoader : public VveUnsteadyDataLoader
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflPathlineLoader(VveUnsteadyData *pTarget);
    virtual ~VflPathlineLoader();

    /**
     * Initialize the loader.
     * Basically, this method just does a sanity check of the 
     * given parameters. The property list gets finally parsed
     * and used when data loading is being invoked.
	 *
	 * Lists of (required) properties:
	 *
	 * For vtk with timing information given through start time and delta time
	 *  FILE_NAME	(string)
	 *  START_TIME	(float)
	 *  DELTA_TIME	(float)
	 * (FORMAT)
	 * (SCALAR)
	 * (RADIUS)
	 *
	 * For vtk with timing information contained in a scalar field of the vtk file itself
	 *  FILE_NAME		(string)
	 *  TIME_FIELDNAME	(string)
	 * (FORMAT)
	 * (SCALAR)
	 * (RADIUS)
	 *
	 * TODO - for trk file
	 * TODO - for ptl file
     */
    virtual bool Init(const VistaPropertyList &refProps);

    virtual void LoadDataThreadSafe();

	/**
	 * This is an interface to define the kind of particle extention you want to load,
	 * if there are any.
     * VveParticleExt is the superclass for all kind of particle extensions, the VistaPropertyList holds the
	 * information about the extention needed in the loading process.
	 *
	 * Current existing extension:
	 * VveParticleExtTensor 
	 *      - list<string> PARTICLEEXT_FIELDNAMES : eigenvalue 0, eigenvalue 1, eigenvalue 2,
	 *												eigenvector 0, eigenvector 1, eigenvector 2
	 */
	void SetParticleExtention( const VistaPropertyList &refPropsParticleExt);

protected:
    // loading different data files
    int LoadTrkFile(std::string strFilename, bool bBinary,
        std::string strScalar, std::string strRadius, float fScale);
    int LoadPtlFile(std::string strFilename,
        std::string strScalar, std::string strRadius);
    int LoadVtkFile(std::string strFilename, float fStartTime, float fDeltaTime,
        std::string strScalar, std::string strRadius);
	int LoadVtkFile(std::string strFilename, std::string strTimeFieldName, 
		std::string strScalar, std::string strRadius);

    VistaPropertyList oProps;                      // remember your parameters...
	VistaPropertyList         m_oPropsPraticleExt; // and which properties this extention has
    bool                m_bParticleExt;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLPATHLINELOADER_H)
