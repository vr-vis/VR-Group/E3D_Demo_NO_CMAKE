/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VflKeyFrameTransformer.h" 
#include <fstream>
#include <VistaAspects/VistaAspectsUtils.h>


/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
static std::string SKeyFrameTransformerType("VflKeyFrameTransformer");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<std::string, 
						  VflKeyFrameTransformer, VistaProperty::PROPT_STRING>
	      ("KEYFRAMEFILE", SKeyFrameTransformerType,
		  &VflKeyFrameTransformer::GetCurrentFileName),
	new TVistaPropertyGet<std::string, VflKeyFrameTransformer, VistaProperty::PROPT_STRING>
	      ("INTERPOLATION", SKeyFrameTransformerType,
		  &VflKeyFrameTransformer::GetInterpolationModeName),      	      
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{

	new TVistaPropertySet<const std::string &, std::string,
	                    VflKeyFrameTransformer>
		("KEYFRAMEFILE", SKeyFrameTransformerType,
		&VflKeyFrameTransformer::ReadTransformTable),
	new TVistaPropertySet<const std::string &, std::string,
	                    VflKeyFrameTransformer>
		("INTERPOLATION", SKeyFrameTransformerType,
		&VflKeyFrameTransformer::SetInterpolationMode),
	NULL
};
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VflKeyFrameTransformer::VflKeyFrameTransformer()
{
	m_pCurrentFrameParameter = new VflKeyFrame;
	m_fTArray[0] = m_fTArray[1]= m_fTArray[2] =0;
	m_eKFMode = VflKeyFrameTransformer::LINEAR;
	m_pPrevCFP = NULL;
	m_pNextCFP = NULL;
}

VflKeyFrameTransformer::~VflKeyFrameTransformer ()
{
	delete m_pCurrentFrameParameter;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
bool VflKeyFrameTransformer::GetDoesTimeLevelSnap() const
{
	return (m_eKFMode == VflKeyFrameTransformer::TIMELEVELSNAP ? true:false);
}

bool VflKeyFrameTransformer::SetDoesTimeLevelSnap(bool bDoesIt)
{
	if(IVflTransformer::SetDoesTimeLevelSnap(bDoesIt) == false)
		return false;

	if (bDoesIt)
	{
		SetInterpolationMode(TIMELEVELSNAP);
		return true;
	}
	else
	{
		SetInterpolationMode(LINEAR);
		return true;
	}
//	return false; // please use SetInterpolationMode or .ini to close TimeLevelSnap 
}


bool VflKeyFrameTransformer::SetInterpolationMode(const std::string &sInterpolationMode)
{
	if (VistaAspectsComparisonStuff::StringEquals(sInterpolationMode,"linear",false))
	{
		SetInterpolationMode(LINEAR);
	}
	else if(VistaAspectsComparisonStuff::StringEquals(sInterpolationMode,"timelevelsnap",false))
	{
		SetInterpolationMode(TIMELEVELSNAP);
	}
	else 
	{
		return false;
	}	
	return true;
}

bool VflKeyFrameTransformer::SetInterpolationMode(eKeyFrameMode eMode)
{
	if(eMode == LINEAR || eMode == TIMELEVELSNAP)
	{
		m_eKFMode = eMode;
		Notify(MSG_INTERPOLATIONMODE_CHANGE);
		return true;
	}
	return false;
}

std::string VflKeyFrameTransformer::GetInterpolationModeName() const
{
	if(m_eKFMode == LINEAR)
		return "linear";
	else
		return "timelevelsnap";
}


VflKeyFrameTransformer::eKeyFrameMode VflKeyFrameTransformer::GetInterpolationMode() const
{
	return m_eKFMode;
}


bool VflKeyFrameTransformer::GetUnsteadyTransform(double dSimTime, 
		                              VistaTransformMatrix &oStorage)
{
	if (SetCurrentFrameParameter(dSimTime))
	{
		// Set storage
		m_pCurrentFrameParameter->GetPosition(m_v3T1);
		m_pCurrentFrameParameter->GetOrientation(m_q4T1);
		VistaTransformMatrix mTranslation(m_v3T1);
		VistaTransformMatrix mRotation(m_q4T1);
		VistaTransformMatrix mScale;
		float scale[3];
		m_pCurrentFrameParameter->GetScale(scale);

		mScale.SetToScaleMatrix(	scale[0], 
															scale[1],
															scale[2]);

		oStorage = mTranslation*mRotation*mScale;
		return true;
	}
	return false;
}

bool VflKeyFrameTransformer::ReadTransformTable(const std::string &sTransformTableName)
{
	std::ifstream infile(sTransformTableName.c_str());
	char cBuffer[1024];
	std::list<float> l;
	float f[11];
	if (infile.good())
	{
		std::vector<VflKeyFrame> vecTmp;
		//if(!m_vecKeyFrameParameter.empty())
		//	m_vecKeyFrameParameter.clear();
		while (!infile.eof())
		{
			if(infile.peek() == '#')
			{
				infile.getline(cBuffer, 1024);
			}
			else
			{
				infile.getline(cBuffer, 1024);
				l = VistaAspectsConversionStuff::ConvertToFloatList(cBuffer);
				if (l.size() == 11)
				{
					int i = 0;
					std::list<float>::iterator it;
					for (it = l.begin(); it!=l.end(); ++it, ++i)
					{
						f[i] = (*it);
						//vstr::debugi() << *it << endl;
					}
					VflKeyFrame pNewParameter;

					pNewParameter.SetTime((double)f[0]);
					pNewParameter.SetPosition(f[1], f[2], f[3]);
					pNewParameter.SetOrientation(f[4], f[5], f[6], f[7]);
					pNewParameter.SetScale(&f[8]);
					vecTmp.push_back(pNewParameter);
				}
				else
					vstr::errp() << "[KeyFrame]: Number of Params wrong, expecting 11, got " 
					     << m_vecKeyFrameParameter.size() << std::endl
						 << "\tin Line: [" << cBuffer << "]\n";
			}
		}
		infile.close();
		m_sFileName = sTransformTableName;
		m_vecKeyFrameParameter = vecTmp; // assign
		m_pPrevCFP = m_pNextCFP = NULL;

		Notify(MSG_TRANSFORMTABLE_CHANGE);
		return true;
	}


	return false;
}

bool VflKeyFrameTransformer::SetCurrentFrameParameter(double dSimTime)
{
	if(m_vecKeyFrameParameter.empty())
		return false;

	bool bRegular = true;
	const int iSize = static_cast<int>(m_vecKeyFrameParameter.size());

	// find out the prev and next parameter
	if (dSimTime <= m_vecKeyFrameParameter[0].GetTime() ||
		dSimTime >= m_vecKeyFrameParameter[iSize-1].GetTime())
	{
		m_pPrevCFP = &m_vecKeyFrameParameter[iSize-1];
		m_pNextCFP = &m_vecKeyFrameParameter[1];
		//vstr::debugi() << "Intervall: [" << iSize-1 << ", " << 1 << "]\n";
		bRegular = false;
	}
	else
	{
		int iBegin	= 0;
		int iEnd	= static_cast<int>(m_vecKeyFrameParameter.size()) - 1;

		while(iBegin != iEnd - 1)
		{
			int i = (iEnd+iBegin)/2;
			VflKeyFrame * pCFP = &m_vecKeyFrameParameter[i];
			if (dSimTime < pCFP->GetTime())
			{
				iEnd = i;
			}
			else
			{
				iBegin = i;
			}
		}
		m_pPrevCFP = &m_vecKeyFrameParameter[iBegin];
		m_pNextCFP = &m_vecKeyFrameParameter[iEnd];
		//vstr::debugi() << "Intervall: [" << iBegin << ", " << iEnd << "]\n";
	}
	
	// for different process
	float fPrev = (float)m_pPrevCFP->GetTime();
	float fNext = (float)m_pNextCFP->GetTime();

	switch (m_eKFMode)
	{
	case VflKeyFrameTransformer::LINEAR : // Linear appr.
		{
			// Linear faktor
			float fFaktor;
			if (bRegular)			// 0......fPrev...dSimTime...fNext...1
			{
				fFaktor =  (float)((dSimTime - fPrev) / (fNext - fPrev));
			}
			else
			{
				if (dSimTime<fPrev) // 0...dSimTime...fNext......fPrev...1
				{
					fFaktor = (float)((dSimTime + 1.0 - fPrev) / (fNext +1.0 - fPrev));
				}
				else				// 0...fNext......fPrev...dSimTime...1
				{
					fFaktor = (float)((dSimTime - fPrev) / (fNext + 1.0 - fPrev));
				}
			}

			// Apro.
			// Position
			m_pCurrentFrameParameter->SetTime(dSimTime);
			m_pPrevCFP->GetPosition(m_v3T1);
			m_pNextCFP->GetPosition(m_v3T2);
			m_v3T1 += (m_v3T2 - m_v3T1)*fFaktor;
			m_pCurrentFrameParameter->SetPosition(m_v3T1);
			// Orientation
			m_pPrevCFP->GetOrientation(m_q4T1);
			m_pNextCFP->GetOrientation(m_q4T2);

			m_pCurrentFrameParameter->SetOrientation(m_q4T1.Slerp(m_q4T2, fFaktor));
			// Scale
			float f1[3], f2[3];
			m_pPrevCFP->GetScale(f1);
			m_pNextCFP->GetScale(f2);
			f1[0] += (f2[0] - f1[0])*fFaktor;
			f1[1] += (f2[1] - f1[1])*fFaktor;
			f1[2] += (f2[2] - f1[2])*fFaktor;
			m_pCurrentFrameParameter->SetScale(f1);

			break;
		}
	case VflKeyFrameTransformer::TIMELEVELSNAP : // TimeLevelSnap appr.
		{
			if (!bRegular)	
			{
				fNext += 1.0;
			}
			*m_pCurrentFrameParameter = *(dSimTime-fPrev < fNext-dSimTime ? m_pPrevCFP : m_pNextCFP);
			break;
		}	
	default:
		{
			return false;
		}

	}
	return true;
}

std::string VflKeyFrameTransformer::GetCurrentFileName() const
{
	return m_sFileName;
}

std::string VflKeyFrameTransformer::GetReflectionableType() const
{
	return SKeyFrameTransformerType;
}


int VflKeyFrameTransformer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nRet = IVflTransformer::AddToBaseTypeList(rBtList);
	rBtList.push_back(SKeyFrameTransformerType);
	return nRet+1;
}

bool VflKeyFrameTransformer::GetKeyFrame(unsigned int nIdx, VflKeyFrame &oStorage) const
{
	if(nIdx >= m_vecKeyFrameParameter.size())
		return false;

	oStorage = m_vecKeyFrameParameter[nIdx];
	return true;
}

void VflKeyFrameTransformer::SetKeyFrame(unsigned int nIdx, const VflKeyFrame &oFrame)
{
	if(nIdx >= m_vecKeyFrameParameter.size())
	{
		m_vecKeyFrameParameter.resize(nIdx + 1);
	}
	m_vecKeyFrameParameter[nIdx] = oFrame;
	Notify(MSG_TRANSFORMTABLE_CHANGE);
}


VflKeyFrameTransformer::VflKeyFrame::VflKeyFrame()
{
	m_fScale[0] = m_fScale[1]= m_fScale[2] = 1;
	m_dTime = 0;
}

VflKeyFrameTransformer::VflKeyFrame::~VflKeyFrame()
{
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void VflKeyFrameTransformer::VflKeyFrame::SetTime(const double t)
{
	m_dTime = t;
}
double VflKeyFrameTransformer::VflKeyFrame::GetTime() const
{
	return m_dTime;
}

void VflKeyFrameTransformer::VflKeyFrame::SetPosition(const VistaVector3D &pos)
{
	m_v3Pos = pos;
}

void VflKeyFrameTransformer::VflKeyFrame::SetPosition(const float x, float y, float z)
{
	m_v3Pos[0] = x;
	m_v3Pos[1] = y;
	m_v3Pos[2] = z;
}

void VflKeyFrameTransformer::VflKeyFrame::GetPosition(VistaVector3D &pos) const
{
	pos = m_v3Pos;
}

void VflKeyFrameTransformer::VflKeyFrame::GetPosition(float &x, float &y, float&z) const
{
	x = m_v3Pos[0];
	y = m_v3Pos[1];
	z = m_v3Pos[2];
}

void VflKeyFrameTransformer::VflKeyFrame::SetOrientation(const VistaQuaternion &q4Ori)
{
	m_q4Ori = q4Ori; 
}
	
void VflKeyFrameTransformer::VflKeyFrame::SetOrientation(const float x, float y, float z, float w)
{
	m_q4Ori[0] = x;
	m_q4Ori[1] = y;
	m_q4Ori[2] = z;
	m_q4Ori[3] = w;
}

void VflKeyFrameTransformer::VflKeyFrame::GetOrientation(VistaQuaternion &q4Ori) const
{
	q4Ori = m_q4Ori;

}


void VflKeyFrameTransformer::VflKeyFrame::SetScale(const float f[3])
{
	m_fScale[0] = f[0];
	m_fScale[1] = f[1];
	m_fScale[2] = f[2];
}
	
void VflKeyFrameTransformer::VflKeyFrame::GetScale(float f[3]) const
{
	f[0] = m_fScale[0];
	f[1] = m_fScale[1];
	f[2] = m_fScale[2];
}
	
std::string VflKeyFrameTransformer::GetFactoryType()
{
	return std::string("KEYFRAME_TRANSFORM");
}


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "MYDEMO.CPP"                                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/


