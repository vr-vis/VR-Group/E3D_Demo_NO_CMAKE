/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflLevelIndexTransformer.h"
#include <VistaVisExt/Tools/VveUtils.h>
#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string SVflLevelIndexTransformerType("VflLevelIndexTransformer");

/*============================================================================*/
/*  IMPLEMENTATION FOR VflConversionStuff                                    */
/*============================================================================*/
VflLevelIndexTransformer::VflLevelIndexTransformer(VveTimeMapper* pTM)	
:m_pTimeMapper(pTM)
{
	m_vecTranslation.resize(m_pTimeMapper->GetNumberOfTimeLevels(), VistaVector3D (0.0f, 0.0f, 0.0f));
	m_vecRotation.resize(m_pTimeMapper->GetNumberOfTimeLevels(), VistaQuaternion(0.0f,0.0f,0.0f,1.0f));
	m_vecScale.resize(m_pTimeMapper->GetNumberOfTimeLevels(), 1.0f);
	m_vecTotalTransform.resize(m_pTimeMapper->GetNumberOfTimeLevels());
}

VflLevelIndexTransformer::~VflLevelIndexTransformer()
{
}



bool VflLevelIndexTransformer::GetUnsteadyTransform(double dSimTime, 
	                              VistaTransformMatrix &oStorage)
{
	int iCurrentLevelIndex = m_pTimeMapper->GetLevelIndex((m_pTimeMapper->GetVisualizationTime((float) dSimTime)));
	if ((iCurrentLevelIndex<0)||(iCurrentLevelIndex>=m_pTimeMapper->GetNumberOfTimeLevels()))
		return false;

	oStorage = m_vecTotalTransform [iCurrentLevelIndex];
	return true;
}


void VflLevelIndexTransformer::GetTranslation(int iLevelIndex, VistaVector3D &v3) const
{
	if ((iLevelIndex>=0)&&(iLevelIndex<m_pTimeMapper->GetNumberOfTimeLevels()))
		v3 = m_vecTranslation[iLevelIndex];
}

bool VflLevelIndexTransformer::SetTranslation(int iLevelIndex, const VistaVector3D &v3)
{
	if ((iLevelIndex<0)||(iLevelIndex>=m_pTimeMapper->GetNumberOfTimeLevels()))
		return false;
	m_vecTranslation[iLevelIndex] = v3;
	UpdateTransform(iLevelIndex);
	return true;
}


void VflLevelIndexTransformer::GetRotation(int iLevelIndex, VistaQuaternion &qRot) const
{
	if ((iLevelIndex>=0)&&(iLevelIndex<m_pTimeMapper->GetNumberOfTimeLevels()))
		qRot = m_vecRotation[iLevelIndex];
}

bool VflLevelIndexTransformer::SetRotation(int iLevelIndex, const VistaQuaternion &qRot)
{
	if ((iLevelIndex<0)||(iLevelIndex>=m_pTimeMapper->GetNumberOfTimeLevels()))
		return false;
	m_vecRotation[iLevelIndex] = qRot;
	UpdateTransform(iLevelIndex);
	return true;
}



float VflLevelIndexTransformer::GetScale(int iLevelIndex) const
{
	if ((iLevelIndex>=0)&&(iLevelIndex<m_pTimeMapper->GetNumberOfTimeLevels()))
		return m_vecScale[iLevelIndex];
	
	return -1.0f;
}

bool VflLevelIndexTransformer::SetScale(int iLevelIndex, float f)
{
	if ((iLevelIndex<0)||(iLevelIndex>=m_pTimeMapper->GetNumberOfTimeLevels()))
		return false;

	m_vecScale[iLevelIndex] = f;
	UpdateTransform(iLevelIndex);
	return true;
}

string VflLevelIndexTransformer::GetReflectionableType() const
{
	return SVflLevelIndexTransformerType;
}


int VflLevelIndexTransformer::AddToBaseTypeList(list<string> &rBtList) const
{
	int nRet = IVflTransformer::AddToBaseTypeList(rBtList);
	rBtList.push_back(SVflLevelIndexTransformerType);
	return nRet+1;
}

string VflLevelIndexTransformer::GetFactoryType()
{
	return std::string("LEVELINDEX_TRANSFORM");
}

void VflLevelIndexTransformer::UpdateTransform(int iLevelIndex)
{
	if ((iLevelIndex>=0)&&(iLevelIndex<m_pTimeMapper->GetNumberOfTimeLevels()))
	{

		VistaTransformMatrix S;
		S.SetToIdentity();
		S.SetToScaleMatrix(m_vecScale[iLevelIndex], m_vecScale[iLevelIndex], m_vecScale[iLevelIndex]);
		VistaTransformMatrix R(m_vecRotation[iLevelIndex]);
		VistaTransformMatrix T;
		T.SetToIdentity();
		T.SetToTranslationMatrix(m_vecTranslation[iLevelIndex]);

		//first scale, then rotate, then translate.
		m_vecTotalTransform[iLevelIndex] = T * R * S;
		Notify(MSG_TRANSFORM_CHANGE);
	}
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflUtils.cpp"                                                */
/*============================================================================*/
