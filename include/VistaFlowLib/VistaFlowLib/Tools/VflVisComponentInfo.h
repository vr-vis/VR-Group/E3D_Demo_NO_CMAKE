/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVISCOMPONENTINFO_H
#define _VFLVISCOMPONENTINFO_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include "../Visualization/VflRenderable.h"
#include "../Visualization/VflSystemAbstraction.h"

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class Vista2DText;
class VflVisController;
class VflPropertyLoader;
class IVflVisComponent;
class VflVisComponent;
class VflVisComposite;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflVisComponentInfo : public IVflRenderable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflVisComponentInfo(IVflVisComponent *pTarget);
	VflVisComponentInfo();

    virtual ~VflVisComponentInfo();
    virtual std::string GetType() const;

	class VISTAFLOWLIBAPI CVisComponentInfoProperties : public IVflRenderable::VflRenderableProperties
    {
    public:
        CVisComponentInfoProperties();
        virtual ~CVisComponentInfoProperties();

		bool GetDisplayStatus() const;
		bool SetDisplayStatus(bool bDisplayState);

		std::string GetText() const;
		bool        SetText( const std::string &sText );

		bool GetDisplayOnCurrentNode() const;
		bool SetDisplayOnCurrentNode(bool bDraw);

		std::list<std::string> GetNodeNamesForDisplay() const;
		bool SetNodeNamesForDisplay(const std::list<std::string> &liNodeNames);


		bool GetTextPosition(float &fX, float &fY) const;
		bool SetTextPosition(float fX, float fY);

		bool GetTextColor(float &fRed, float &fGreen, float &fBlue) const;
		bool SetTextColor(float fRed, float fGreen, float fBlue);


		virtual std::string GetReflectionableType() const;

		enum
		{
			MSG_DISPLAYSTATE_CHANGED = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_CURRENTTEXT_CHANGED,
			MSG_DISPLAYONCURRENTNODE_CHANGED,
			MSG_DISPLAYNODENAMES_CHANGED,
			MSG_POSITIONCHANGE,
			MSG_COLORCHANGE,
			MSG_LAST
		};
    protected:
        virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

	private:
		CVisComponentInfoProperties(const CVisComponentInfoProperties &) {}
		CVisComponentInfoProperties &operator=(const CVisComponentInfoProperties &) 
		{
			return *this;
		}

		bool m_bDisplayStatus;
		IVflSystemAbstraction::IVflOverlayText *m_pText;
		bool m_bDisplayOnCurrentNode;
		std::list<std::string> m_liNodeNamesForDisplay;

        friend class VflVisComponentInfo;
    };

	virtual unsigned int GetRegistrationMode() const;
	virtual void ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket);

	static std::string GetFactoryType();
protected:
	enum
	{
		E_TARGET_TICKET = IVflRenderable::E_TICKET_LAST,
		E_TICKET_LAST
	};

	virtual VflRenderableProperties    *CreateProperties() const;

    VflPropertyLoader *m_pLoader;

    // we do it the easy (well, cheap) way...
    // we just memorize the already type casted vis component...
    IVflVisComponent *m_pTarget;
    VflVisComponent *m_pTargetLeaf;
    VflVisComposite *m_pTargetComposite;

private:
	bool HandlePropertyUpdate(int msg);
	bool HandleTargetUpdate(int msg);
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLVISCOMPONENTINFO_H
