/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLTIMELINE_H
#define _VFLTIMELINE_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaFlowLib/Visualization/VflRenderable.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>

#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 *
 */
class VISTAFLOWLIBAPI VflTimeLine : public IVflRenderable
{
public:
	VflTimeLine();
    virtual ~VflTimeLine();

	virtual void DrawOpaque();
	virtual std::string GetType() const;
	virtual unsigned int GetRegistrationMode() const;

	class VISTAFLOWLIBAPI VflTimeLineProperties
		: public IVflRenderable::VflRenderableProperties
	{
	public:
		VflTimeLineProperties();
		virtual ~VflTimeLineProperties();
	
		void SetShowTimeLine(bool b);
		bool GetShowTimeLine() const;

		void SetTimeLineWidth(float fW);
		float GetTimeLineWidth() const;

		void SetTimePointerSize(float fSize);
		float GetTimePointerSize() const;
		
		void SetLinePoints(VistaVector3D v3Start, VistaVector3D v3End);
		void GetLinePoints(VistaVector3D &v3Start, VistaVector3D &v3End);

		void SetColor(float fR, float fG, float fB);
		void GetColor(float &fR, float &fG, float &fB);

		void SetVisTimeRange(double dStart, double dEnd);
		void GetVisTimeRange(double &dStart, double &dEnd);

	private:
		bool			m_bShowTimeLine;
		float			m_fTimePointerSize;
		float			m_fTimeLineWidth;

		float			m_fRed,
						m_fGreen,
						m_fBlue;

		// position of the line's start and end points
		VistaVector3D	m_v3Start;
		VistaVector3D	m_v3End;

		double			m_dStartVisTime;
		double			m_dEndVisTime;
	};

protected:
	virtual VflRenderableProperties *CreateProperties() const;	

private:

};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLTIMELINE_H)
