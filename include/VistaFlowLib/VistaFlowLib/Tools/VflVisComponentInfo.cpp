/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* PRAGMAS                                                                    */
/*============================================================================*/
#ifdef WIN32
    #pragma warning(disable:4786)
#endif

#include "VflVisComponentInfo.h"

#include "VflVisComponent.h"
#include "VflVisComposite.h"
#include "VflPropertyLoader.h"

#include <VistaAspects/VistaAspectsUtils.h>

#include <cstdio>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflVisComponentInfo::VflVisComponentInfo(IVflVisComponent *pTarget)
: IVflRenderable(),
  m_pLoader(NULL),
  m_pTarget(pTarget),
  m_pTargetLeaf(NULL),
  m_pTargetComposite(NULL)
{
	
    m_pTargetLeaf = dynamic_cast<VflVisComponent *>(m_pTarget);
    m_pTargetComposite = dynamic_cast<VflVisComposite *>(m_pTarget);

	if(m_pTarget)
		Observe(dynamic_cast<IVistaObserveable*>(m_pTarget), E_TARGET_TICKET);
}

VflVisComponentInfo::VflVisComponentInfo() 
: IVflRenderable(),
m_pLoader(NULL),
m_pTarget(NULL),
m_pTargetLeaf(NULL),
m_pTargetComposite(NULL)
{}

VflVisComponentInfo::~VflVisComponentInfo()
{
	ReleaseObserveable(dynamic_cast<IVistaObserveable*>(m_pTarget), E_TARGET_TICKET);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/

void VflVisComponentInfo::ObserverUpdate(IVistaObserveable *pObserveable, 
										  int msg, int ticket)
{
	switch(ticket)
	{
	case E_TARGET_TICKET:
		{
			HandleTargetUpdate(msg);
			return;
		}
	case IVflRenderable::E_PROP_TICKET:
		{
			if(HandlePropertyUpdate(msg) == true)
			return;
		}
	default:
		break;
	}

	IVflRenderable::ObserverUpdate(pObserveable, msg, ticket);
}

bool VflVisComponentInfo::HandlePropertyUpdate(int msg)
{
	// intercept visibility change to turn text on or off
	if(msg == IVflRenderable::VflRenderableProperties::MSG_VISIBILITY)
	{
		CVisComponentInfoProperties *p = dynamic_cast<CVisComponentInfoProperties*>(GetProperties());
		if(p)
		{
			p->m_pText->SetEnabled(p->GetVisible());
		}
	}
	return false; // we indicate "failure" as we want this to be passed to
	              // VflVisObject::ObserverUpdate(), too!
}

bool VflVisComponentInfo::HandleTargetUpdate(int msg)
{
	CVisComponentInfoProperties *p = dynamic_cast<CVisComponentInfoProperties*>(GetProperties());
	if(p)
	{
		std::string strResult;
		if(m_pTarget)
		{
			 strResult = m_pTarget->GetNameForNameable();
			if ((*p).m_bDisplayStatus)
			{
				if (m_pTargetLeaf)
				{
					strResult += " - component (source status: ";
					// retrieve information about sub-objects of the composite...
					// in this case, just take the first source...
					if (m_pTargetLeaf->GetDataSources().empty())
						strResult += "*none*";
					else
					{
						IVveExecutable *pSource = *(m_pTargetLeaf->GetDataSources().begin());
						strResult += pSource->GetStateString();
					}
					strResult += ")";
				}
				else if (m_pTargetComposite)
				{
					char buf[4096];
					sprintf(buf, "- composition (%d sub-objects)", 
						m_pTargetComposite->GetNumberOfComponents());
					strResult += buf;
				}
			}

			if (!m_pTarget->GetVisible())
			{
				strResult += " (hidden)";
			}
		}
		else
		{
			strResult = "[VflVisCompInfo] - ERROR - no vis component given!";
		}

		// update properties
		(*p).SetText(strResult);
		return true;
	}
	return false;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflVisComponentInfo::GetType() const
{
    return IVflRenderable::GetType()+string("::VflVisComponentInfo");
}

std::string VflVisComponentInfo::GetFactoryType()
{
	return std::string("VISCOMP_INFO");
}

IVflRenderable::VflRenderableProperties    *VflVisComponentInfo::CreateProperties() const
{
	return new CVisComponentInfoProperties;
}

unsigned int VflVisComponentInfo::GetRegistrationMode() const
{
	return OLI_UPDATE;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPropertyObj                                              */
/*                                                                            */
/*============================================================================*/

static string SsReflectionType("VflVisComponentInfo");
static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, VflVisComponentInfo::CVisComponentInfoProperties, VistaProperty::PROPT_BOOL>
	      ("DISPLAYONCURRENTNODE", SsReflectionType,
		  &VflVisComponentInfo::CVisComponentInfoProperties::GetDisplayOnCurrentNode),
	new TVistaPropertyGet<bool, VflVisComponentInfo::CVisComponentInfoProperties, VistaProperty::PROPT_BOOL>
	      ("DISPLAYSTATUS", SsReflectionType,
		  &VflVisComponentInfo::CVisComponentInfoProperties::GetDisplayStatus),
	new TVistaPropertyGet<std::string, 
		  VflVisComponentInfo::CVisComponentInfoProperties, VistaProperty::PROPT_STRING>
	      ("INFOTEXT", SsReflectionType,
		  &VflVisComponentInfo::CVisComponentInfoProperties::GetText),
	new TVistaPropertyGet<std::list<std::string>, 
		  VflVisComponentInfo::CVisComponentInfoProperties, VistaProperty::PROPT_LIST>
	      ("DISPLAY_ON_NODES", SsReflectionType,
		  &VflVisComponentInfo::CVisComponentInfoProperties::GetNodeNamesForDisplay),
    new TVistaProperty3RefGet<float, VflVisComponentInfo::CVisComponentInfoProperties> // implicitely PROPT_LIST
	("COLOR", SsReflectionType,
	&VflVisComponentInfo::CVisComponentInfoProperties::GetTextColor),
    new TVistaProperty2RefGet<float, VflVisComponentInfo::CVisComponentInfoProperties> // implicitely PROPT_LIST
		("POSITION", SsReflectionType,
		&VflVisComponentInfo::CVisComponentInfoProperties::GetTextPosition),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool, bool,
	                    VflVisComponentInfo::CVisComponentInfoProperties>
		("DISPLAYONCURRENTNODE", SsReflectionType,
		&VflVisComponentInfo::CVisComponentInfoProperties::SetDisplayOnCurrentNode),
	new TVistaPropertySet<bool, bool,
	                    VflVisComponentInfo::CVisComponentInfoProperties>
		("DISPLAYSTATUS", SsReflectionType,
		&VflVisComponentInfo::CVisComponentInfoProperties::SetDisplayStatus),

	new TVistaPropertySet<const std::string &, std::string ,
	                    VflVisComponentInfo::CVisComponentInfoProperties>
		("INFOTEXT", SsReflectionType,
		&VflVisComponentInfo::CVisComponentInfoProperties::SetText),

	new TVistaPropertySet<const std::list<std::string>&, 
		  std::list<std::string>, VflVisComponentInfo::CVisComponentInfoProperties>
	      ("DISPLAY_ON_NODES", SsReflectionType,
		  &VflVisComponentInfo::CVisComponentInfoProperties::SetNodeNamesForDisplay),
	new TVistaProperty3ValSet<float, VflVisComponentInfo::CVisComponentInfoProperties>
	( "COLOR", SsReflectionType,
	&VflVisComponentInfo::CVisComponentInfoProperties::SetTextColor),

	new TVistaProperty2ValSet<float, VflVisComponentInfo::CVisComponentInfoProperties>
	( "POSITION", SsReflectionType,
	&VflVisComponentInfo::CVisComponentInfoProperties::SetTextPosition),

	NULL
};

VflVisComponentInfo::CVisComponentInfoProperties::CVisComponentInfoProperties()
: IVflRenderable::VflRenderableProperties(),
  m_bDisplayStatus(false),
  m_pText(NULL),
  m_bDisplayOnCurrentNode(true)
{
	m_pText = IVflSystemAbstraction::GetSystemAbs()->AddOverlayText();

	//if(m_pText)
	//	m_pText->Init("component info -- no component given?", 0.0f, 0.0f);
}

VflVisComponentInfo::CVisComponentInfoProperties::~CVisComponentInfoProperties()
{
	if(m_pText)
	{
		IVflSystemAbstraction::GetSystemAbs()->DeleteOverlayText(m_pText);
	}
}



string VflVisComponentInfo::CVisComponentInfoProperties::GetReflectionableType() const
{
	return SsReflectionType;
}

int VflVisComponentInfo::CVisComponentInfoProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = IVflRenderable::VflRenderableProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize+1;
}

bool VflVisComponentInfo::CVisComponentInfoProperties::GetDisplayStatus() const
{
	return m_bDisplayStatus;
}

bool VflVisComponentInfo::CVisComponentInfoProperties::SetDisplayStatus(bool bDisplayState)
{
	if(compAndAssignFunc<bool>(bDisplayState, m_bDisplayStatus) == 1)
	{
		Notify(MSG_DISPLAYSTATE_CHANGED);
		return true;
	}
	return false;
}


std::string VflVisComponentInfo::CVisComponentInfoProperties::GetText() const
{
	
	if(m_pText)
	{
		std::string sText;
		m_pText->GetText(sText);
		return sText;
	}
	return "<none>";
}

bool        VflVisComponentInfo::CVisComponentInfoProperties::SetText( const std::string &sText )
{
	if(m_pText)
	{
		if(m_pText->SetText(sText))
		{
			Notify(MSG_CURRENTTEXT_CHANGED);
			return true;
		}
	}
	return false;
}


bool VflVisComponentInfo::CVisComponentInfoProperties::GetDisplayOnCurrentNode() const
{
	return m_bDisplayOnCurrentNode;
}

bool VflVisComponentInfo::CVisComponentInfoProperties::SetDisplayOnCurrentNode(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bDisplayOnCurrentNode) == 1)
	{
		Notify(MSG_DISPLAYONCURRENTNODE_CHANGED);
		return true;
	}
	return false;
}

std::list<std::string> VflVisComponentInfo::CVisComponentInfoProperties::GetNodeNamesForDisplay() const
{
	return m_liNodeNamesForDisplay;
}

bool VflVisComponentInfo::CVisComponentInfoProperties::SetNodeNamesForDisplay(const std::list<std::string> &liNodeNames)
{
	m_liNodeNamesForDisplay = liNodeNames;
	Notify(MSG_DISPLAYNODENAMES_CHANGED);
	return true;
}

bool VflVisComponentInfo::CVisComponentInfoProperties::GetTextPosition(float &fX, float &fY) const
{
	if(m_pText)
	{
		m_pText->GetPosition(fX, fY);
		return true;
	}
	return false;
}

bool VflVisComponentInfo::CVisComponentInfoProperties::SetTextPosition(float fX, float fY)
{
	if(m_pText)
	{

		m_pText->SetPosition(fX, fY);
		Notify(MSG_POSITIONCHANGE);
		return true;
	}

	return false;
}


bool VflVisComponentInfo::CVisComponentInfoProperties::GetTextColor(float &fRed, float &fGreen, float &fBlue) const
{
	if(m_pText)
	{
		return m_pText->GetColor(fRed, fGreen, fBlue);
	}
	return false;
}

bool VflVisComponentInfo::CVisComponentInfoProperties::SetTextColor(float fRed, float fGreen, float fBlue)
{
	if(m_pText && m_pText->SetColor(fRed, fGreen, fBlue))
	{
		Notify(MSG_COLORCHANGE);
		return true;
	}
	return true;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflVisComponentInfo.cpp"                                           */
/*============================================================================*/
