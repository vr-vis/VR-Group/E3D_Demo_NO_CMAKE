/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifdef WIN32
	#pragma warning (disable: 4786)
#endif

#include "VflDataRegistry.h"
// ViSTA FlowLib includes
#include <VistaAspects/VistaObjectRegistry.h>
#include <VistaAspects/VistaPropertyAwareable.h>
#include <VistaTools/VistaFileSystemFile.h>

// ViSTA includes
#include <VistaVisExt/IO/VveUnsteadyCartesianGridLoader.h>
#include <VistaVisExt/IO/VveUnsteadyVtkDataLoader.h>

#include <cassert>


/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/


/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/



/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

VflDataRegistry::VflDataRegistry()
{
	m_pRegistry=new VistaObjectRegistry();
}

VflDataRegistry::~VflDataRegistry()
{
	//todo delete everything
}

VveUnsteadyVtkPolyData* VflDataRegistry::GetVtkPolyData(const VistaPropertyList &oProps, 
														  VveTimeMapper* const pTimeMapper )
{
	assert(pTimeMapper);
	string sfilename;
	if (oProps.GetValue("FILE_NAME", sfilename ) == false || sfilename.empty())
	{
		vstr::errp() << "["<< __FILE__ << "] Unable to Load Data. No FILE_NAME given" << std::endl;
	}
	VveUnsteadyVtkPolyData *pTarget;
 	VveUnsteadyVtkDataLoader *pLoader
 				=static_cast<VveUnsteadyVtkDataLoader*>(m_pRegistry->RetrieveNameable(sfilename));
 	if (!pLoader) // dataloader is not registered so create a new one and register it
 	{
		pTarget = new VveUnsteadyVtkPolyData(pTimeMapper);
		pLoader = new VveUnsteadyVtkDataLoader(pTarget);
		pLoader->GetProperties()->SetPropertiesByList(oProps);
		std::string sPvdEnding=".vtp";
		if (sfilename.rfind(sPvdEnding)==sfilename.size()-sPvdEnding.size())
		{
			pLoader->GetProperties()->SetFileFormat("VTP_POLYDATA");
		}
		else
		{
			pLoader->GetProperties()->SetFileFormat("VTK_POLYDATA");
		}		
		pLoader->LoadDataThreadSafe();
		pLoader->GetTarget()->SetNameForNameable(sfilename);
		pLoader->SetNameForNameable(sfilename);
		m_pRegistry->RegisterNameable(pLoader);
 	}
	else //data is registered check if loaderpoperties are equal
	{
		//todo check equality
		pTarget=dynamic_cast<VveUnsteadyVtkPolyData*>(pLoader->GetTarget());
	}
	return pTarget;
}

VveUnsteadyCartesianGrid* VflDataRegistry::GetCartGridData(const VistaPropertyList &oProps,
															 VveTimeMapper* const pTimeMapper )
{	
	assert(pTimeMapper);
	string sfilename;
	if (oProps.GetValue("FILE_NAME", sfilename ) == false || sfilename.empty())
	{
		vstr::errp() << "["<< __FILE__ << "] Unable to Load Data. No FILE_NAME given" << std::endl;
	}
	VveUnsteadyCartesianGrid* pTarget=NULL;
	VveUnsteadyCartesianGridLoader *pLoader
		=static_cast<VveUnsteadyCartesianGridLoader*>(m_pRegistry->RetrieveNameable(sfilename));
	if (!pLoader) // dataloader is not registered so create a new one and register it
	{
		pTarget=new VveUnsteadyCartesianGrid(pTimeMapper);
		for(int i = 0; i < pTimeMapper->GetNumberOfTimeLevels(); ++i)
		{
			pTarget->GetTypedLevelDataByLevelIndex(i)->SetData(new VveCartesianGrid());
		}
		pLoader = new VveUnsteadyCartesianGridLoader(pTarget);
		pLoader->GetProperties()->SetPropertiesByList(oProps);
		pLoader->LoadDataThreadSafe();
		pLoader->GetTarget()->SetNameForNameable(sfilename);
		pLoader->SetNameForNameable(sfilename);
		m_pRegistry->RegisterNameable(pLoader);
	}
	else //data is registered check if loaderpoperties are equal
	{
		//todo check equality
		pTarget=dynamic_cast<VveUnsteadyCartesianGrid*>(pLoader->GetTarget());
	}
	return pTarget;
}

