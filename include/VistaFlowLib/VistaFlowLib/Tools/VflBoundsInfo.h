/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLBOUNDSINFO_H
#define _VFLBOUNDSINFO_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include "../Visualization/VflRenderable.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VflVisController;
class VflPropertyLoader;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflBoundsInfo : public IVflRenderable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflBoundsInfo();
    
    virtual ~VflBoundsInfo();
    
    /**
    * Initializes the bounds info object according to the given properties.
    * The following keys are recognized:
    * 
    * VISIBLE [true|false]                 -   optional [default: true]
    * LINE_WIDTH                           -   optional [default: 2.0f]
    * COLOR [r, g, b]                      -   optional [1.0f, 1.0f, 1.0f]
    *
    * INI_SECTION [section name]       -   optional
    * INI_FILE [file name]             -   optional
    *
    * If the ini section and file are given, a property loader is created
    * (through the resource manager) and this object is registered as its target.
    */
    //virtual bool Init(const VistaPropertyList &refProps);
    //virtual bool Init();


    virtual void DrawOpaque();
    virtual void Update();
    virtual std::string GetType() const;
    
	class VISTAFLOWLIBAPI VflBoundsInfoProperties : public IVflRenderable::VflRenderableProperties
    {
    public:
        VflBoundsInfoProperties();
        virtual ~VflBoundsInfoProperties();
        virtual std::string GetReflectionableType() const;


		bool GetBoundsColor(float &r, float &g, float &b) const;
		bool SetBoundsColor(float r, float g, float b);

		float GetLineWidth() const;
		bool SetLineWidth(float fLineWidth);
	
		unsigned short GetStipplePattern() const;
		bool SetStipplePattern(unsigned short);

		int GetStippleScale() const;
		bool SetStippleScale(int nStippleScale);

		enum
		{
			MSG_BOUNDS_COLORCHANGE = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_BOUNDS_LINEWIDTHCHANGE,
			MSG_BOUNDS_STIPPLEPATTERNCHANGE,
			MSG_BOUNDS_STIPPLESCALECHANGE,
			MSG_LAST
		};

        
	protected:
		virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const; 

	private:
        float   m_aColor[3];
        float   m_fLineWidth;
        unsigned short  m_iStipplePattern;
        int     m_iStippleScale;

    
		VflBoundsInfoProperties(const VflBoundsInfoProperties &) 
		{
		}

		VflBoundsInfoProperties &operator=(const VflBoundsInfoProperties &)
		{
			return *this;
		}
        friend class VflBoundsInfo;
    };
       
	static std::string   GetFactoryType();
	virtual unsigned int GetRegistrationMode() const;
protected:
	virtual VflRenderableProperties    *CreateProperties() const;

    VflPropertyLoader *m_pLoader;
	VistaVector3D m_v3BoundsMin, 
				  m_v3BoundsMax;
	double m_dBoundsTs;

    //CProperties m_oProperties;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLBOUNDSINFO_H
