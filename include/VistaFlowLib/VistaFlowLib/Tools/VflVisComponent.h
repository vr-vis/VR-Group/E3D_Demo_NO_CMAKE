/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLVISCOMPONENT_H
#define _VFLVISCOMPONENT_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include <VistaVisExt/Tools/VveExecutable.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVflRenderable;
class VveUnsteadyData;
class VflResource;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 */
class VISTAFLOWLIBAPI IVflVisComponent : public IVveExecutable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    IVflVisComponent();
    virtual ~IVflVisComponent();

public:
    /**
     * Execute anything that is to be executed, i.e. 
     * a loader or remote extraction or whatever...
     */
    virtual void Execute() = 0;

	/**
	 * (De-)Activate this component. Use this if you distinguish
	 * between "active"/"current" and "inactive" objects.
	 * In its simplest form, SetHelpersVisible() is called with
	 * appropriate values.
	 */
	virtual void Activate();
	virtual void Deactivate();

	/**
     * Visibility control (to be provided by subclasses)
     */
	virtual void SetVisible(bool bVisible) = 0;
	virtual bool GetVisible() const = 0;

    /**
     * Visibility control for helper objects.
     */
    virtual void SetHelpersVisible(bool bVisible);
    virtual bool GetHelpersVisible() const;

    /** 
     * Get a list of helpers.
     */
    const std::list<IVflRenderable *> &GetHelpers() const;

    /**
     * Returns the type of the component as string.
     */    
	virtual std::string GetType() const;

    /**
     * Helper management - to be used by factories.
     */
    int AddHelper(IVflRenderable *);
    int RemoveHelper(IVflRenderable *);

	REFL_INLINEIMP(IVflVisComponent,IVveExecutable);
protected:
	std::list<IVflRenderable *> m_liHelpers;

public:
    template <class T>
    bool GetHelper(T*& pHelpers) const
    {
        std::list<IVflRenderable *>::const_iterator itHelpers = m_liHelpers.begin();
        while (itHelpers != m_liHelpers.end())
        {
            T* pCurrentHelpers = dynamic_cast<T*>(*itHelpers);
            if (pCurrentHelpers)
            {
                pHelpers = pCurrentHelpers;
                return true;
            }
            ++itHelpers;
        }
        return false;
    }
};

/**
 */    
class VISTAFLOWLIBAPI VflVisComponent : public IVflVisComponent
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VflVisComponent();
    virtual ~VflVisComponent();

public:
    /**
     * Execute anything that is to be executed, i.e. 
     * a loader or remote extraction or whatever...
     */
    virtual void Execute();

    /**
     * Visibility control.
     */
	void SetVisible(bool bVisible);
	bool GetVisible() const;

    /**
     * Retrieve lists of contained objects.
     */
    std::list<VveUnsteadyData *> GetData() const;
    std::list<IVveExecutable *> GetDataSources() const;
    std::list<IVflRenderable *> GetDataSinks() const;
    std::list<VflResource *> GetResources() const;

    /**
     * Returns the type of the visualization component as a string.
     */    
	virtual std::string GetType() const;

    /**
     * Interior object management.
     */
    int AddData(VveUnsteadyData *pData);
    int RemoveData(VveUnsteadyData *pData);

    int AddDataSource(IVveExecutable *pSource);
    int RemoveDataSource(IVveExecutable *pSource);

    int AddDataSink(IVflRenderable *pSink);
    int RemoveDataSink(IVflRenderable *pSink);

    int AddResource(VflResource *pResource);
    int RemoveResource(VflResource *pResource);

protected:
    std::list<IVveExecutable *> m_liSources;
    std::list<VveUnsteadyData *> m_liData;
    std::list<IVflRenderable *> m_liSinks;
    std::list<VflResource *> m_liResources;

public:
    /**
     * helper methods to get data of desired type...
     */
    template <class T>
    bool GetUnsteadyData(T*& pData) const
    {
        std::list<VveUnsteadyData *>::const_iterator itData = m_liData.begin();
        while (itData != m_liData.end())
        {
            T* pCurrentData = dynamic_cast<T*>(*itData);
            if (pCurrentData)
            {
                pData = pCurrentData;
                return true;
            }
            ++itData;
        }
        return false;
    }

    template <class T>
    bool GetDataSource(T*& pSource) const
    {
        std::list<IVveExecutable *>::const_iterator itSource = m_liSources.begin();
        while (itSource != m_liSources.end())
        {
            T* pCurrentSource = dynamic_cast<T*>(*itSource);
            if (pCurrentSource)
            {
                pSource = pCurrentSource;
                return true;
            }
            ++itSource;
        }
        return false;
    }

    template <class T>
    bool GetDataSink(T*& pSink) const
    {
        std::list<IVflRenderable *>::const_iterator itSink= m_liSinks.begin();
        while (itSink != m_liSinks.end())
        {
            T* pCurrentSink= dynamic_cast<T*>(*itSink);
            if (pCurrentSink)
            {
                pSink = pCurrentSink;
                return true;
            }
            ++itSink;
        }
        return false;
    }

    template <class T>
    bool GetResource(T*& pResource) const
    {
        std::list<VflResource *>::const_iterator itRes= m_liResources.begin();
        while (itRes != m_liResources.end())
        {
            T* pCurrentRes= dynamic_cast<T*>(*itRes);
            if (pCurrentRes)
            {
                pResource = pCurrentRes;
                return true;
            }
            ++itRes;
        }
        return false;
    }
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLVISCOMPONENT_H)
