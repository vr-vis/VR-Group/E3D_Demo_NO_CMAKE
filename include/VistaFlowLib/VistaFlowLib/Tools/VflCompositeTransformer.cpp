/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflCompositeTransformer.h"

#include <VistaBase/VistaVectorMath.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string VflCompositeTransformerType("VflCompositeTransformer");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	NULL
};

/*============================================================================*/
/*  IMPLEMENTATION FOR VflConversionStuff                                    */
/*============================================================================*/
VflCompositeTransformer::VflCompositeTransformer()
{
	m_vecTransformers.clear();
}


VflCompositeTransformer::~VflCompositeTransformer()
{
	for(std::vector<IVflTransformer*>::iterator itTrans=m_vecTransformers.begin();
		itTrans!=m_vecTransformers.end(); ++itTrans)
	{
		delete (*itTrans);
	}
}



bool VflCompositeTransformer::GetUnsteadyTransform(double dSimTime, 
	                              VistaTransformMatrix &oStorage)
{
	VistaTransformMatrix m;
	m.SetToIdentity();

	for(std::vector<IVflTransformer*>::iterator itTrans=m_vecTransformers.begin();
		itTrans!=m_vecTransformers.end(); ++itTrans)
	{
		VistaTransformMatrix mTrans;
		(*itTrans)->GetUnsteadyTransform(dSimTime, mTrans);
		m = mTrans * m;
	}

	oStorage = m;

	return true;
}

bool VflCompositeTransformer::AddTransformer(IVflTransformer* pTransformer)
{
	if(pTransformer)
	{
		m_vecTransformers.push_back(pTransformer);
		return true;
	}
	else
	{
		vstr::errp() << "[VflCompositeTransformer::AddTransformer] Unable to add transformer."
				<< std::endl;
		return false;
	}
}


std::string VflCompositeTransformer::GetReflectionableType() const
{
	return VflCompositeTransformerType;
}

int VflCompositeTransformer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nRet = IVflTransformer::AddToBaseTypeList(rBtList);
	rBtList.push_back(VflCompositeTransformerType);
	return nRet+1;
}

std::string VflCompositeTransformer::GetFactoryType()
{
	return std::string("COMPOSITE");
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflCompositeTransformer.cpp"                                */
/*============================================================================*/
