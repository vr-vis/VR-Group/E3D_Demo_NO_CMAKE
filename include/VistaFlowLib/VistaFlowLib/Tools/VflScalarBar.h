/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLSCALARBAR_H
#define _VFLSCALARBAR_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../Visualization/VflRenderable.h"
#include "../Visualization/VflVtkLookupTable.h"

#include <set>

#include <VistaFlowLib/VistaFlowLibConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkScalarBarActor;
class VflPropertyLoader;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class VISTAFLOWLIBAPI VflScalarBar : public IVflRenderable
{
public:
    //class CProperty;

public:
    // CONSTRUCTORS / DESTRUCTOR
	VflScalarBar();
    virtual ~VflScalarBar();

    /**
     * Initializes the time info according to the given properties.
     * The following keys are recognized:
     *
     * - VISIBLE [true|false]                 -   optional [default: true]
     * - MAXIMUM_NUMBER_OF_COLORS             -   optional
     * - NUMBER_OF_LABELS                     -   optional
     * - ORIENTATION [HORIZONTAL|VERTICAL]    -   optional
     * - BOLD [true|false]                    -   optional
     * - ITALIC [true|false]                  -   optional
     * - SHADOW [true|false]                  -   optional
     * - TITLE [scalar bar title]             -   optional
     * - POSITION [x, y]                      -   optional
     * - SIZE [width, height]                 -   optional
	 * - DISPLAY_ON_NODES [list of names]     -   optional [default: empty -> display on all nodes]
     *
     * - INI_SECTION [section name]           -   optional
     * - INI_FILE [file name]                 -   optional
     *
     * If the ini section and file are given, a property loader is created
     * (through the resource manager) and this object is registered as target.
     */
	virtual bool Init();
	//virtual bool Init(VflVisController *pVisController,
	//	              int iRegistrationMode,
	//	              const VistaPropertyList &refProps);
    //virtual bool Init(VflVisController *pVisController,
	//	              int iRegistrationMode);

	virtual void Draw2D();

	virtual std::string GetType() const;

	class VISTAFLOWLIBAPI CScalarBarProperties : public IVflRenderable::VflRenderableProperties
	{
	public:
		CScalarBarProperties();
		virtual ~CScalarBarProperties();

		int GetMaximumNumberOfColors() const;
		bool SetMaximumNumberOfColors(int iNum);

		int GetNumberOfLabels() const;
		bool SetNumberOfLabels(int iNum);

		int GetOrientationId() const;
		bool SetOrientationId(int iOrient);

		std::string GetOrientation() const;
		bool   SetOrientation(const std::string &sOrientation);

		bool GetIsBold() const;
		bool SetBold(bool bBold);

		bool GetIsItalic() const;
		bool SetItalic(bool bItalic);

		bool GetIsShadow() const;
		bool SetShadow(bool bShadow);

		std::string GetTitle() const;
		bool SetTitle(const std::string & strTitle);

		bool SetTitleColor(const VistaColor& oColor);
		VistaColor GetTitleColor() const;

		bool SetLabelColor(const VistaColor& oColor);
		VistaColor GetLabelColor() const;

        /**
         * NOTE: positional data is given as fractions of viewport size!!!
         */
        bool GetPosition(float &x, float &y) const;
        bool SetPosition(float x, float y);

        bool GetSize(float &width, float &height) const;
        bool SetSize(float width, float height);


		std::set<int>  GetFrameSkipSet() const;
		bool           SetFrameSkipSet(const std::set<int> &sSkipSet);

		int           GetFrameSkipMod() const;
		bool          SetFrameSkipMod(int iMod);

		int           GetCurFrameCnt() const;

		bool GetDisplayOnCurrentNode() const;
		bool SetDisplayOnCurrentNode(bool bDraw);

		std::list<std::string> GetNodeNamesForDisplay() const;
		bool SetNodeNamesForDisplay(const std::list<std::string> &liNodeNames);

		std::string GetLutName() const;		

		VflVtkLookupTable *GetLUT() const;
		bool SetLUT(VflVtkLookupTable *);

		//virtual int SetProperty(const VistaProperty &refProp);
		//virtual int GetProperty(VistaProperty &refProp);
		//virtual int GetPropertySymbolList(list<string> &rStorageList);

		enum ORIENTATION
		{
			ORIENT_HORIZONTAL,
			ORIENT_VERTICAL
		};

		enum
		{
			MSG_MAXCOLOR_CHANGE = IVflRenderable::VflRenderableProperties::MSG_LAST,
			MSG_DISPLAYONCURRENTNODE,
			MSG_NODEDISPLAYCHANGE,
			MSG_TITLE_COLOR_CHANGE,
			MSG_LABEL_COLOR_CHANGE,

			MSG_NUMLABELS_CHANGE,
			MSG_ORIENTATION_CHANGE,
			MSG_BOLD_CHANGE,
			MSG_ITALIC_CHANGE,
			MSG_SHADOW_CHANGE,
			MSG_TITLE_CHANGE,
			MSG_POSITION_CHANGE,
			MSG_SIZE_CHANGE,

			MSG_FRAME_SKIP_CHANGE,
			MSG_FRAME_SKIPMOD_CHANGE,

			MSG_LUT_CHANGE,

			MSG_LAST
		};
		VflVtkLookupTable *GetLookupTable() const;

		virtual std::string GetReflectionableType() const;
	protected:
        virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	private:
		CScalarBarProperties(const CScalarBarProperties &) {}
		CScalarBarProperties &operator=(const CScalarBarProperties &)
		{
			return *this;
		}

		VflVtkLookupTable	*m_pLut;
		vtkScalarBarActor   *m_pActor;
		bool                   m_bDisplayOnCurrentNode;
		std::list<std::string> m_liNodeNamesForDisplay;

		// FRAME-SELECT-HACK IAR
		// skip frames for overlay paint
		// select surfaces to draw scalar bar on (hb)
		std::set<int>                m_stCbIdx;
		int                          m_nMaxCnt, m_nCurCnt;

		friend class VflScalarBar;
	};

    virtual CScalarBarProperties *GetProperties() const;

	static std::string GetFactoryType();
	virtual unsigned int GetRegistrationMode() const;
protected:
	virtual VflRenderableProperties    *CreateProperties() const;
  
    VflPropertyLoader          *m_pLoader;

};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VFLSCALARBAR_H
