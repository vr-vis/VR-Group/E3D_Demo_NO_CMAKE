/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include <GL/glew.h>

#include "VflTimeLine.h"

#include <VistaFlowLib/Visualization/VflVisTiming.h>
#include <VistaFlowLib/Visualization/VflRenderNode.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflTimeLine::VflTimeLine()
: IVflRenderable() //, m_fStartVisTime(0.0f), m_fEndVisTime(1.0f)
{
}

VflTimeLine::~VflTimeLine()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DrawOpaque                                                  */
/*                                                                            */
/*============================================================================*/
void VflTimeLine::DrawOpaque()
{
	VflTimeLineProperties *pProps =
		static_cast<VflTimeLineProperties*>(this->GetProperties());

	// color
	float fR, fG, fB;
	pProps->GetColor(fR, fG, fB);
	glColor3f(fR, fG, fB);

	// start and end points of the time line
	VistaVector3D v3Start, v3End;
	pProps->GetLinePoints(v3Start, v3End);

	// vis time range
	double dStartVisTime, dEndVisTime;
	pProps->GetVisTimeRange(dStartVisTime, dEndVisTime);

	glPushAttrib(GL_ENABLE_BIT|GL_LINE_BIT);
	glDisable(GL_LIGHTING);

	float fWidth = pProps->GetTimeLineWidth();
	glLineWidth(fWidth);

	// draw time line
	bool bShowLine = pProps->GetShowTimeLine();
	if(bShowLine)
	{
		glBegin(GL_LINES);
			glVertex3f(v3Start[0], v3Start[1], v3Start[2]);
			glVertex3f(v3End[0], v3End[1], v3End[2]);
		glEnd();
	}	

	// draw pointer
	double dVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	
	// compute position of the pointer along the time line
	// assumption:
	//    |---------------------------------------|
	// v3Start               ^                   v3End
    // startvistime       vistime             endvistime
	if(dVisTime >= dStartVisTime && dVisTime <= dEndVisTime)
	{
		float fTimeFactor = static_cast<float>((dVisTime-dStartVisTime)/(dEndVisTime-dStartVisTime));
		VistaVector3D v3Pos = v3Start;
		v3Pos += fTimeFactor * (v3End-v3Start);

		float fSize = pProps->GetTimePointerSize();
		// tmp: assume time line is parallel to x-axes
		// -> todo: computation for more general cases (time line is no longer parallel to x-axes)
		glBegin(GL_LINES);
		glVertex3f(v3Pos[0], v3Pos[1]-0.5f*fSize, v3Pos[2]);
		glVertex3f(v3Pos[0], v3Pos[1]+0.5f*fSize, v3Pos[2]);
		glEnd();
	}

	glPopAttrib();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflTimeLine::GetType() const
{
    return IVflRenderable::GetType()+std::string("::VflTimeLine");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties *VflTimeLine::CreateProperties() const
{
	return new VflTimeLineProperties;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflTimeLine::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_OPAQUE;
}


/*============================================================================*/
/*  methods of VflTimeLine::CProperties                                       */
/*============================================================================*/

VflTimeLine::VflTimeLineProperties::VflTimeLineProperties()
: IVflRenderable::VflRenderableProperties(),
  m_bShowTimeLine(true),
  m_fTimeLineWidth(1.0f),
  m_fTimePointerSize(1.0f),
  m_v3Start(0.0f, 0.0f, 0.0f),
  m_v3End(1.0f, 0.0f, 0.0f),
  m_fRed(1.0f), m_fGreen(1.0f), m_fBlue(1.0f),
  m_dStartVisTime(0.0), m_dEndVisTime(1.0)
{
}

VflTimeLine::VflTimeLineProperties::~VflTimeLineProperties()
{
}

void VflTimeLine::VflTimeLineProperties::SetShowTimeLine(bool b)
{
	m_bShowTimeLine = b;
}
bool VflTimeLine::VflTimeLineProperties::GetShowTimeLine() const
{
	return m_bShowTimeLine;
}

void VflTimeLine::VflTimeLineProperties::SetTimeLineWidth(float fW)
{
	m_fTimeLineWidth = fW;
}
float VflTimeLine::VflTimeLineProperties::GetTimeLineWidth() const
{
	return m_fTimeLineWidth;
}

void VflTimeLine::VflTimeLineProperties::SetTimePointerSize(float fSize)
{
	m_fTimePointerSize = fSize;
}
float VflTimeLine::VflTimeLineProperties::GetTimePointerSize() const
{
	return m_fTimePointerSize;
}

void VflTimeLine::VflTimeLineProperties::SetLinePoints(VistaVector3D v3Start, 
														 VistaVector3D v3End)
{
	m_v3Start = v3Start;
	m_v3End   = v3End;
}
void VflTimeLine::VflTimeLineProperties::GetLinePoints(VistaVector3D &v3Start, 
														 VistaVector3D &v3End)
{
	v3Start = m_v3Start;
	v3End	= m_v3End;
}

void VflTimeLine::VflTimeLineProperties::SetColor(float fR, float fG, float fB)
{
	m_fRed = fR;
	m_fGreen = fG;
	m_fBlue = fB;
}
void VflTimeLine::VflTimeLineProperties::GetColor(float &fR, float &fG, float &fB)
{
	fR = m_fRed;
	fG = m_fGreen;
	fB = m_fBlue;
}
void VflTimeLine::VflTimeLineProperties::SetVisTimeRange(double dStart, double dEnd)
{
	m_dStartVisTime = dStart;
	m_dEndVisTime = dEnd;
}
void VflTimeLine::VflTimeLineProperties::GetVisTimeRange(double &dStart, double &dEnd)
{
	dStart = m_dStartVisTime;
	dEnd   = m_dEndVisTime;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflTimeLine.cpp"                                             */
/*============================================================================*/
