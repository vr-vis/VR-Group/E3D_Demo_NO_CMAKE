/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflPathlineLoader.h"


#include <VistaVisExt/IO/VvePathlineReaderPtl.h>
#include <VistaVisExt/IO/VvePathlineReaderTrk.h>
#include <VistaVisExt/IO/VvePathlineReaderVtk.h>

#include <VistaTools/VistaProfiler.h>
#include <VistaTools/VistaProgressBar.h>

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>
#include <VistaBase/VistaVector3D.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflPathlineLoader::VflPathlineLoader(VveUnsteadyData *pTarget)
: VveUnsteadyDataLoader(pTarget), m_bParticleExt(false)
{
}

VflPathlineLoader::~VflPathlineLoader()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VflPathlineLoader::Init(const VistaPropertyList &refProps)
{
	// check whether we've got the correct kind of data...
	VveParticleDataCont *pData = dynamic_cast<VveParticleDataCont *>(m_pTarget);
	if (!pData)
	{
		vstr::errp() << " [VflPathlineLoader] invalid target data..." << endl;
		return false;
	}

    oProps = refProps;

    // do some sanity checks...
    string strFilename;
    if( oProps.GetValue( "FILE_NAME", strFilename ) == false || strFilename.empty())
    {
        vstr::errp() << " [VflPathlineLoader] no file name given..." << endl;
        return false;
    }

    m_bBlocking = oProps.GetValueOrDefault<bool>( "Blocking", false );
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetParticleExtention                                        */
/*                                                                            */
/*============================================================================*/
void VflPathlineLoader::SetParticleExtention( const VistaPropertyList &refPropsParticleExt)
{
	m_oPropsPraticleExt = refPropsParticleExt;
    m_bParticleExt = true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadDataThreadSafe                                          */
/*                                                                            */
/*============================================================================*/
void VflPathlineLoader::LoadDataThreadSafe()
{
	SetState(LOADING);

	// check whether we've got the correct kind of data...
	VveParticleDataCont *pUnsteadyData = dynamic_cast<VveParticleDataCont*>(m_pTarget);

	if (!pUnsteadyData)
	{
		vstr::errp() << " [VflPathlineLoader] invalid target data..." << endl;
        SetState(FINISHED_FAILURE);
		return;
	}

    VveParticlePopulationItem *pData = pUnsteadyData->GetData();
    if (!pData)
    {
        vstr::errp() << " [VflPathlineLoader] no data container to be filled..." << endl;
        SetState(FINISHED_FAILURE);
        return;
    }

	// get data file name
	string strFileName;
	if( oProps.GetValue( "FILE_NAME", strFileName ) == false || strFileName.empty() )
	{
        vstr::errp() << " [VflPathlineLoader] no file name given..." << endl;
        SetState(FINISHED_FAILURE);
		return;
	}

	// determine file type
	string strExtension;
	size_t iExtensionStart = strFileName.find_last_of('.');
	if (iExtensionStart == string::npos 
		|| iExtensionStart == strFileName.size()-1)
	{
		vstr::warnp() << " [VflPathlineLoader] unable to determine particle data file extension..." << endl
			 << "                       file name: " << strFileName << endl;
	}
	else
	{
		++iExtensionStart;
		strExtension = strFileName.substr(iExtensionStart);
		strExtension = VistaProfiler::ToUpper(strExtension);
	}

	string strFormat;
	if ( oProps.GetValue("FORMAT", strFormat) )
	{
		strFormat = VistaProfiler::ToUpper(strFormat);
		// avoid file identification through the extension
		strExtension = "";
	}

	// determine scalars and vectors to be loaded
	string strScalar = oProps.GetValue<std::string>("SCALAR",
								VistaConversion::ON_ERROR_PRINT_WARNING ); // @DRCLUSTER@ Handling? warning/exception/default?
    string strRadius = oProps.GetValue<std::string>("RADIUS",
								VistaConversion::ON_ERROR_PRINT_WARNING ); // @DRCLUSTER@ Handling? warning/exception/default?

	// find out about file type
	if (strFormat == "TRK_BINARY" || strExtension == "TRK")
	{
		float fScale = oProps.GetValueOrDefault<float>( "SCALE", 1.0f );
		if (fScale <= 0)
			fScale = 1.0f;

		vstr::outi() << " [VflPathlineLoader] - loading binary trk file..." << endl;
		vstr::outi() << "                       scale factor: " << fScale << endl;
		if (LoadTrkFile(strFileName, true, strScalar, strRadius, fScale))
            SetState(FINISHED_SUCCESS);
        else
            SetState(FINISHED_FAILURE);
	}
	else if (strFormat == "TRK_ASCII" || strExtension == "TRKC")
	{
		float fScale = oProps.GetValueOrDefault<float>( "SCALE", 1.0f );
		if (fScale <= 0)
			fScale = 1.0f;
		vstr::outi() << " [VflPathlineLoader] - loading ascii trk file..." << endl;
		vstr::outi() << "                       scale factor: " << fScale << endl;
		if (LoadTrkFile(strFileName, false, strScalar, strRadius, fScale))
            SetState(FINISHED_SUCCESS);
        else
            SetState(FINISHED_FAILURE);
	}
	else if (strFormat == "PTL" || strExtension == "PTL")
	{
		vstr::outi() << " [VflPathlineLoader] - loading ptl file..." << endl;
		if (LoadPtlFile(strFileName, strScalar, strRadius))
            SetState(FINISHED_SUCCESS);
        else
            SetState(FINISHED_FAILURE);
	}
	else if (strFormat == "VTK" || strExtension == "VTK")
	{
		// determine timing information
		float fStartTime = 0.0f, fDeltaTime = 0.0f;

		// if we have timing information inside the data set
		if(oProps.HasProperty("TIME_FIELDNAME"))
		{
			// than it has to be stored as a scalar field <TIME_FIELDNAME>
			string strTimeFieldName = oProps.GetValue<std::string>( "TIME_FIELDNAME",
													VistaConversion::ON_ERROR_PRINT_WARNING ); // @DRCLUSTER@ Handling? warning/exception/default?
			vstr::outi() << " [VflPathlineLoader] - loading vtk file with implicit given timing information..." << endl;			
			if(LoadVtkFile(strFileName, strTimeFieldName, strScalar, strRadius))
				SetState(FINISHED_SUCCESS);
			else
				SetState(FINISHED_FAILURE);
		}
		else
		{
			// one has to define START_TIME and DELTA_TIME
			if(oProps.HasProperty("START_TIME")&&oProps.HasProperty("DELTA_TIME"))
			{
				fStartTime = oProps.GetValue<float>("START_TIME",
													VistaConversion::ON_ERROR_PRINT_WARNING ); // @DRCLUSTER@ Handling? warning/exception/default?
				fDeltaTime = oProps.GetValue<float>("DELTA_TIME",
													VistaConversion::ON_ERROR_PRINT_WARNING ); // @DRCLUSTER@ Handling? warning/exception/default?
				vstr::outi() << " [VflPathlineLoader] - loading vtk file with explicit given start_time and delta_time..." << endl;
				if (LoadVtkFile(strFileName, fStartTime, fDeltaTime, strScalar, strRadius))
					SetState(FINISHED_SUCCESS);
				else
					SetState(FINISHED_FAILURE);
			}
			else
			{
				vstr::errp() << " [VflPathlineLoader] no timing information given..." << endl;
				SetState(FINISHED_FAILURE);
				return;
			}
		}
	}
	else
	{
		vstr::errp() << " [VflPathlineLoader] unknown file type..." << endl
			 << "                       file name: " << strFileName << endl;
		if (!strExtension.empty())
			vstr::err() << "                       extension: " << strExtension << endl;
		if (!strFormat.empty())
			vstr::err() << "                       format: " << strFormat << endl;
	}

	if (GetState() == FINISHED_SUCCESS)
	{
		if( oProps.GetValueOrDefault( "REDUCE_PATHLINES", false ) )
		{
			VveParticlePopulation* oOrigPopulation = pData->GetData();
			pData->LockData();
			oOrigPopulation->ReduceInformation( pUnsteadyData->GetTimeMapper(),
                                                VveParticlePopulation::sTimeArrayDefault,
				pData->GetData());
			pData->UnlockData();
		}

		VveParticlePopulation *pPopulation = pData->GetData();

        // Get Bounds
        float fScalarMin, fScalarMax, fRadiusMin, fRadiusMax, fTimeMin, fTimeMax;
        VistaVector3D vecVelMin, vecVelMax;
        pPopulation->GetBounds(VveParticlePopulation::sTimeArrayDefault,   &fTimeMin,   &fTimeMax,   1);
        pPopulation->GetBounds(VveParticlePopulation::sScalarArrayDefault, &fScalarMin, &fScalarMax, 1);
        pPopulation->GetBounds(VveParticlePopulation::sRadiusArrayDefault, &fRadiusMin, &fRadiusMax, 1);
        pPopulation->GetBounds(VveParticlePopulation::sVelocityArrayDefault, &vecVelMin[0], &vecVelMax[0], 3);

		vstr::outi() << " [VflPathlineLoader] - particle data loaded successfully..." << endl
					 << "                       scalar range: " << fScalarMin << " - " << fScalarMax << endl
					 << "                       radius range: " << fRadiusMin << " - " << fRadiusMax << endl
					 << "                       velocity range: " << vecVelMin.GetLength() << " - " << vecVelMax.GetLength() << endl
					 << "                       time range: " << fTimeMin << " - " << fTimeMax << endl;

		// signal successful data load
		pData->Notify();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadTrkFile                                                 */
/*                                                                            */
/*============================================================================*/
int VflPathlineLoader::LoadTrkFile(std::string strFilename, bool bBinary,
		std::string strScalar, std::string strRadius, float fScale)
{
	// determine which values are to be mapped to the particles' scalar value
	// and / or the particles' radius
	strScalar = VistaProfiler::ToUpper(strScalar);
	strRadius = VistaProfiler::ToUpper(strRadius);

	VvePathlineReaderTrk::SCALAR eScalar, eRadius;

	if (strScalar == "VELOCITY")
		eScalar = VvePathlineReaderTrk::VELOCITY;
	else if (strScalar == "COUNT")
		eScalar = VvePathlineReaderTrk::COUNT;
	else if (strScalar == "RADIUS")
		eScalar = VvePathlineReaderTrk::RADIUS;
	else if (strScalar == "MASS")
		eScalar = VvePathlineReaderTrk::MASS;
	else if (strScalar == "TEMPERATURE")
		eScalar = VvePathlineReaderTrk::TEMPERATURE;
	else
		eScalar = VvePathlineReaderTrk::VELOCITY;	// default value

	if (strRadius == "VELOCITY")
		eRadius = VvePathlineReaderTrk::VELOCITY;
	else if (strRadius == "COUNT")
		eRadius = VvePathlineReaderTrk::COUNT;
	else if (strRadius == "RADIUS")
		eRadius = VvePathlineReaderTrk::RADIUS;
	else if (strRadius == "MASS")
		eRadius = VvePathlineReaderTrk::MASS;
	else if (strRadius == "TEMPERATURE")
		eRadius = VvePathlineReaderTrk::TEMPERATURE;
	else
		eRadius = VvePathlineReaderTrk::RADIUS;	// default value

	VvePathlineReaderTrk oLoader(strFilename, bBinary, fScale);

    VveParticleDataCont *pUnsteadyData = dynamic_cast<VveParticleDataCont *>(m_pTarget);
    VveParticlePopulationItem *pData = pUnsteadyData->GetData();

	bool bSuccess = false;
	if (oLoader.LoadDataFromFile())
	{
		pData->LockData();
		bSuccess = oLoader.CopyCacheToPopulation(pData->GetData(), eScalar, eRadius);
        pData->Notify();
		pData->UnlockData();
	}

	return bSuccess;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadVtkFile                                                 */
/*                                                                            */
/*============================================================================*/
int VflPathlineLoader::LoadVtkFile(std::string strFilename, 
									 float fStartTime,
									 float fDeltaTime,
									 std::string strScalar,
									 std::string strRadius)
{
	VvePathlineReaderVtk oLoader(strFilename, fStartTime, fDeltaTime);

    VveParticleDataCont *pUnsteadyData = dynamic_cast<VveParticleDataCont *>(m_pTarget);
    VveParticlePopulationItem *pData = pUnsteadyData->GetData();

	// avoid blocking of the render thread (ms)
    // Create new particle population
	VveParticlePopulation* pNewParticles = new VveParticlePopulation();
    VveParticlePopulation* pOldParticles = NULL;

    bool bSuccess = oLoader.Load( pNewParticles, strScalar, strRadius);
	if (bSuccess)
	{
		pData->LockData();
        pOldParticles = pData->SetData(pNewParticles);
		pData->Notify();
		pData->UnlockData();
        delete pOldParticles;
	}

	// TODO (ms) - make sure, we don't run out of memory...

	return bSuccess;
}

int VflPathlineLoader::LoadVtkFile(std::string strFilename,
									std::string strTimeFieldName, 
									std::string strScalar, 
									std::string strRadius)
{
	bool bSuccess = false;
	VvePathlineReaderVtk oLoader(strFilename, strTimeFieldName);

    VveParticleDataCont *pUnsteadyData = dynamic_cast<VveParticleDataCont *>(m_pTarget);
    VveParticlePopulationItem *pData = pUnsteadyData->GetData();

	if(m_bParticleExt)
		oLoader.SetParticleExtention(m_oPropsPraticleExt);

	VveParticlePopulation *pParticles = new VveParticlePopulation();
	bSuccess = oLoader.Load( pParticles, strScalar, strRadius);
	if (bSuccess)
	{
		pData->LockData();
	    pData->SetData(pParticles);
		pData->Notify();
		pData->UnlockData();
	}

	return bSuccess;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadPtlFile                                                 */
/*                                                                            */
/*============================================================================*/
int VflPathlineLoader::LoadPtlFile(std::string strFilename,
		std::string strScalar, std::string strRadius)
{
	// determine which values are to be mapped to the particles' scalar value
	// and / or the particles' radius
	strScalar = VistaProfiler::ToUpper(strScalar);
	strRadius = VistaProfiler::ToUpper(strRadius);

	VvePathlineReaderPtl::SCALAR eScalar, eRadius;

	if (strScalar == "VELOCITY")
		eScalar = VvePathlineReaderPtl::VELOCITY;
	else if (strScalar == "COUNT")
		eScalar = VvePathlineReaderPtl::COUNT;
	else if (strScalar == "RADIUS")
		eScalar = VvePathlineReaderPtl::RADIUS;
	else if (strScalar == "MASS")
		eScalar = VvePathlineReaderPtl::MASS;
	else if (strScalar == "TEMPERATURE")
		eScalar = VvePathlineReaderPtl::TEMPERATURE;
	else
		eScalar = VvePathlineReaderPtl::VELOCITY;	// default value

	if (strRadius == "VELOCITY")
		eRadius = VvePathlineReaderPtl::VELOCITY;
	else if (strRadius == "COUNT")
		eRadius = VvePathlineReaderPtl::COUNT;
	else if (strRadius == "RADIUS")
		eRadius = VvePathlineReaderPtl::RADIUS;
	else if (strRadius == "MASS")
		eRadius = VvePathlineReaderPtl::MASS;
	else if (strRadius == "TEMPERATURE")
		eRadius = VvePathlineReaderPtl::TEMPERATURE;
	else
		eRadius = VvePathlineReaderPtl::RADIUS;	// default value

	VvePathlineReaderPtl oLoader(strFilename);

    VveParticleDataCont *pUnsteadyData = dynamic_cast<VveParticleDataCont *>(m_pTarget);
    VveParticlePopulationItem *pData = pUnsteadyData->GetData();

	bool bSuccess = false;
	if (oLoader.LoadDataFromFile())
	{
		pData->LockData();
		bSuccess = oLoader.CopyCacheToPopulation(pData->GetData(), eScalar, eRadius);
        pData->Notify();
		pData->UnlockData();
	}

	return bSuccess;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
