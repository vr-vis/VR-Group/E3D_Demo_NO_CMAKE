/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* PRAGMAS                                                                    */
/*============================================================================*/
#ifdef WIN32
    #pragma warning(disable:4786)
#endif

/*============================================================================*/
/*  INCLUDES		                                                          */
/*============================================================================*/
#include <GL/glew.h>

#include "VflComponentInfo.h"

#include <VistaVisExt/Tools/VveTimeMapper.h>
#include "../Visualization/VflVisTiming.h"
#include "VflPropertyLoader.h"

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaFlowLib/Visualization/VflSystemAbstraction.h>

#include <algorithm>

#ifdef WIN32
	#include <windows.h>
#endif

#if defined(DARWIN)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflComponentInfo::VflComponentInfo(std::list<IVflVisObject*> * liVisObjects)
: m_liVisObjects(*liVisObjects)
{
}

VflComponentInfo::~VflComponentInfo()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Draw2D                                                      */
/*                                                                            */
/*============================================================================*/
void VflComponentInfo::Draw2D()
{
	VflComponentInfo::VflComponentInfoProperties *props
		= static_cast<VflComponentInfo::VflComponentInfoProperties*>(GetProperties());

	std::string strResult;

	for(std::list<IVflVisObject*>::iterator it = m_liVisObjects.begin(); it != m_liVisObjects.end(); ++it)
	{
		if((*it)->GetVisible())
		{
			strResult += (*it)->GetNameForNameable() + " ";
		}
	}

	// here: we bypass the official setter, as this will try
	// to compare the incoming and the current text
	// and utter a notify. This might be too slow, so
	// we, well... bypass it ;)

	if((*props).m_pText)
	{
		(*props).m_pText->SetText(strResult);
//		(*props).SetTextPosition(0.00f,0.97f);
		(*props).Notify(VflComponentInfoProperties::MSG_TEXTCHANGE);
		(*props).m_pText->Update();
	}
}


std::string VflComponentInfo::GetFactoryType()
{
	return std::string("ComponentInfo");
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflComponentInfo::GetType() const
{
    return IVflRenderable::GetType()+string("::VflComponentInfo");
}

IVflRenderable::VflRenderableProperties *VflComponentInfo::CreateProperties() const
{
	return new VflComponentInfoProperties();
}

void VflComponentInfo::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	// intercept visibility change to turn text on or off
	if(msg == VflRenderableProperties::MSG_VISIBILITY)
	{
		VflComponentInfoProperties *p = dynamic_cast<VflComponentInfoProperties*>(pObserveable);
		if(p)
		{
			p->m_pText->SetEnabled(p->GetVisible());
		}
	}

	// in any case, pass the update to the super-class
	IVflRenderable::ObserverUpdate(pObserveable, msg, ticket);
}

unsigned int VflComponentInfo::GetRegistrationMode() const
{
    return IVflRenderable::OLI_DRAW_2D;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VflComponentInfo::VflComponentInfoProperties*
	VflComponentInfo::GetProperties() const
{
	return static_cast<VflComponentInfoProperties*>(
		IVflRenderable::GetProperties());
}

/*============================================================================*/
/*  methods of VflTimeInfo::CProperties                                    */
/*============================================================================*/
static const string SsReflectionType("VflComponentInfo");
static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, VflComponentInfo::VflComponentInfoProperties, VistaProperty::PROPT_BOOL>
		("DISPLAYONCURRENTNODE", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::GetDisplayOnCurrentNode),
	new TVistaPropertyGet<std::string, VflComponentInfo::VflComponentInfoProperties, VistaProperty::PROPT_STRING>
	    ("INFOTEXT", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::GetText),
	new TVistaPropertyGet<std::list<std::string>, VflComponentInfo::VflComponentInfoProperties, VistaProperty::PROPT_LIST>
	    ("DISPLAY_ON_NODES", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::GetNodeNamesForDisplay),
    new TVistaProperty3RefGet<float, VflComponentInfo::VflComponentInfoProperties> // implicitely PROPT_LIST
		("COLOR", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::GetTextColor),
    new TVistaProperty2RefGet<float, VflComponentInfo::VflComponentInfoProperties> // implicitely PROPT_LIST
		("POSITION", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::GetTextPosition),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool, bool,
	    VflComponentInfo::VflComponentInfoProperties>
		("DISPLAYONCURRENTNODE", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::SetDisplayOnCurrentNode),
	new TVistaPropertySet<const std::string &, std::string ,
	    VflComponentInfo::VflComponentInfoProperties>
		("INFOTEXT", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::SetText),
	new TVistaPropertySet<const std::list<std::string>&, 
		std::list<std::string>, VflComponentInfo::VflComponentInfoProperties>
	    ("DISPLAY_ON_NODES", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::SetNodeNamesForDisplay),
	new TVistaProperty3ValSet<float, VflComponentInfo::VflComponentInfoProperties>
		( "COLOR", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::SetTextColor),
	new TVistaProperty2ValSet<float, VflComponentInfo::VflComponentInfoProperties>
		( "POSITION", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::SetTextPosition),
	NULL
};

VflComponentInfo::VflComponentInfoProperties::VflComponentInfoProperties(const std::string &inpText)
: IVflRenderable::VflRenderableProperties(),
  m_pText(NULL),
  m_bDisplayOnCurrentNode(true)
{
	m_pText = IVflSystemAbstraction::GetSystemAbs()->AddOverlayText();
}

VflComponentInfo::VflComponentInfoProperties::VflComponentInfoProperties()
: IVflRenderable::VflRenderableProperties(),
  m_pText(NULL),
  m_bDisplayOnCurrentNode(true)
{
	m_pText = IVflSystemAbstraction::GetSystemAbs()->AddOverlayText();
}

VflComponentInfo::VflComponentInfoProperties::~VflComponentInfoProperties()
{
	if(m_pText)
	{
		// IAR: todo: fix kernel for this!
		IVflSystemAbstraction::GetSystemAbs()->DeleteOverlayText(m_pText);
	}
}

bool VflComponentInfo::VflComponentInfoProperties::GetTextPosition(float &fX, float &fY) const
{
	if(m_pText)
	{
		m_pText->GetPosition(fX, fY);
		return true;
	}
	return false;
}

bool VflComponentInfo::VflComponentInfoProperties::SetTextPosition(float fX, float fY)
{
	if(m_pText)
	{

		m_pText->SetPosition(fX, fY);
		Notify(MSG_POSITIONCHANGE);
		return true;
	}

	return false;
}


bool VflComponentInfo::VflComponentInfoProperties::GetTextColor(float &fRed, float &fGreen, float &fBlue) const
{
	if(m_pText)
	{
		return m_pText->GetColor(fRed, fGreen, fBlue);
	}
	return false;
}

bool VflComponentInfo::VflComponentInfoProperties::SetTextColor(float fRed, float fGreen, float fBlue)
{
	if(m_pText && m_pText->SetColor(fRed, fGreen, fBlue))
	{
		Notify(MSG_COLORCHANGE);
		return true;
	}
	return true;
}

bool VflComponentInfo::VflComponentInfoProperties::SetText(const std::string &sText)
{
	if(m_pText)
	{
		std::string sTmp;
		m_pText->GetText(sTmp);
		if(sTmp != sText)
		{
			m_pText->SetText(sText);
			Notify(MSG_TEXTCHANGE);
			return true;
		}
	}
	return false;
}

std::string VflComponentInfo::VflComponentInfoProperties::GetText() const
{
	if(m_pText)
	{
		std::string sTmp;
		m_pText->GetText(sTmp);
		return sTmp;
	}
	return "<null>";
}


std::string VflComponentInfo::VflComponentInfoProperties::GetReflectionableType() const
{
	return SsReflectionType;
}

int VflComponentInfo::VflComponentInfoProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nSize = IVflRenderable::VflRenderableProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}

bool VflComponentInfo::VflComponentInfoProperties::GetDisplayOnCurrentNode() const
{
	return m_bDisplayOnCurrentNode;
}

bool VflComponentInfo::VflComponentInfoProperties::SetDisplayOnCurrentNode(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bDisplayOnCurrentNode))
	{
		if(m_pText)
			m_pText->SetEnabled(m_bDisplayOnCurrentNode);

		Notify(MSG_DISPLAYONCURRENTNODE);
		return true;
	}
	return false;
}

std::list<std::string> VflComponentInfo::VflComponentInfoProperties::
                                       GetNodeNamesForDisplay() const
									   
{
	return m_liNodeNamesForDisplay;
}


bool VflComponentInfo::VflComponentInfoProperties::SetNodeNamesForDisplay(
	                                    const std::list<std::string> &liNodeNames)
{
	// IAR @todo we could check for equality
	m_liNodeNamesForDisplay = liNodeNames;


	if (m_liNodeNamesForDisplay.empty())
	{
		SetDisplayOnCurrentNode(true);
	}
	else
	{
		IVflSystemAbstraction *pSystem = IVflSystemAbstraction::GetSystemAbs();
		if (pSystem->IsLeader())
		{
			SetDisplayOnCurrentNode(true);
		}
		else
		{
			// cluster - client/slave
			string strNodeName = pSystem->GetClientName();
			if(std::find(m_liNodeNamesForDisplay.begin(), m_liNodeNamesForDisplay.end(), strNodeName)
				== m_liNodeNamesForDisplay.end())
			{
				SetDisplayOnCurrentNode(false);
			}
			else
				SetDisplayOnCurrentNode(true);
		}
	}

	Notify(MSG_NODEDISPLAYCHANGE);
	return true;
}

namespace 
{
	IVistaPropertyGetFunctor *aCgFunctors[] =
	{	
		new TVistaPropertyGet<string, VflComponentInfo::VflComponentInfoProperties, VistaProperty::PROPT_STRING>
		("TEXT", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::GetText),
		NULL
	};

	IVistaPropertySetFunctor *aCsFunctors[] =
	{	
		new TVistaPropertySet<const std::string&,std::string,VflComponentInfo::VflComponentInfoProperties>
		("TEXT", SsReflectionType,
		&VflComponentInfo::VflComponentInfoProperties::SetText),
		NULL
	};
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflComponentInfo.cpp"                                          */
/*============================================================================*/

