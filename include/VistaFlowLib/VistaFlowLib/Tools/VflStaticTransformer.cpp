/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VflStaticTransformer.h"
#include <VistaVisExt/Tools/VveUtils.h>
#include <VistaAspects/VistaAspectsUtils.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
static std::string SVflStaticTransformerType("VflStaticTransformer");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<
						VistaVector3D, VflStaticTransformer, VistaProperty::PROPT_LIST>(
									"TRANSLATION", SVflStaticTransformerType,
									&VflStaticTransformer::GetTranslation),
	
	new TVistaPropertyGet<
						VistaQuaternion, VflStaticTransformer, VistaProperty::PROPT_LIST>(
									"ROTATION", SVflStaticTransformerType,
									&VflStaticTransformer::GetRotation),
									
	new TVistaPropertyGet<float, VflStaticTransformer, VistaProperty::PROPT_DOUBLE>(
									"SCALE", SVflStaticTransformerType,
									&VflStaticTransformer::GetScale),	      
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<
				const VistaVector3D &, VistaVector3D, VflStaticTransformer>(
						"TRANSLATION", SVflStaticTransformerType,
						&VflStaticTransformer::SetTranslation),
	new TVistaPropertySet<
				const VistaQuaternion &, VistaQuaternion, VflStaticTransformer>(
						"ROTATION", SVflStaticTransformerType,
						&VflStaticTransformer::SetRotation),
	new TVistaPropertySet<float, float,VflStaticTransformer>(
						"SCALE", SVflStaticTransformerType,
						&VflStaticTransformer::SetScale),
	NULL
};
//  		string axis = props("AXIS").GetValue();
// 		string vel  = props("VELOCITY").GetValue();
// 		string origin = props("ORIGIN").GetValue();


/*============================================================================*/
/*  IMPLEMENTATION FOR VflConversionStuff                                    */
/*============================================================================*/
VflStaticTransformer::VflStaticTransformer()
	:	m_v3Translation(0.0f,0.0f,0.0f),
		m_qRotation(0.0f,0.0f,0.0f,1.0f),
		m_fScale(1.0f)
{
}

VflStaticTransformer::VflStaticTransformer(
							const VistaVector3D &translation,
							const VistaQuaternion &qRot,
							float fScale/*=1.0f*/)
	:	m_v3Translation(translation),
		m_qRotation(qRot),
		m_fScale(fScale)
{
	this->UpdateTransform();
}

VflStaticTransformer::VflStaticTransformer(const VistaTransformMatrix &m4Transform)
{
  // retrieve rotation from m4Transform
  
  float s0, s1, s2;
  int k0, k1, k2, k3;
  float q[4];
  float m[16];
  m4Transform.GetValues(m);

  // following code snippet is originally from:
  // From Quaternion to Matrix and Back
  // February 27th 2005 J.M.P. van Waveren
  // 2005, Id Software, Inc.
  if ( m[0 * 4 + 0] + m[1 * 4 + 1] + m[2 * 4 + 2] > 0.0f ) 
  {
    k0 = 3;
    k1 = 2;
    k2 = 1;
    k3 = 0;
    s0 = 1.0f;
    s1 = 1.0f;
    s2 = 1.0f;
  } else if ( m[0 * 4 + 0] > m[1 * 4 + 1] && m[0 * 4 + 0] > m[2 * 4 + 2] ) 
  {
    k0 = 0;
    k1 = 1;
    k2 = 2;
    k3 = 3;
    s0 = 1.0f;
    s1 = -1.0f;
    s2 = -1.0f;
  } else if ( m[1 * 4 + 1] > m[2 * 4 + 2] ) 
  {
    k0 = 1;
    k1 = 0;
    k2 = 3;
    k3 = 2;
    s0 = -1.0f;
    s1 = 1.0f;
    s2 = -1.0f;
  } else 
  {
    k0 = 2;
    k1 = 3;
    k2 = 0;
    k3 = 1;
    s0 = -1.0f;
    s1 = -1.0f;
    s2 = 1.0f;
  }

  float t = s0 * m[0 * 4 + 0] + s1 * m[1 * 4 + 1] + s2 * m[2 * 4 + 2] + 1.0f;
  float s = (1.0f/sqrt( t )) * 0.5f;
  q[k0] = s * t;
  q[k1] = ( m[0 * 4 + 1] - s2 * m[1 * 4 + 0] ) * s;
  q[k2] = ( m[2 * 4 + 0] - s1 * m[0 * 4 + 2] ) * s;
  q[k3] = ( m[1 * 4 + 2] - s0 * m[2 * 4 + 1] ) * s;
  // end code snippet

  m_qRotation = VistaQuaternion(q[0],q[1],q[2],q[3]);

  // get translation
  VistaVector3D translation;
  m4Transform.GetTranslation(translation);

  m_v3Translation = translation;

  // reset scale
	m_fScale = 1.0f;

  // update internal representation
  m_m4TotalTransform = m4Transform;
 
}

VflStaticTransformer::~VflStaticTransformer()
{
}



bool VflStaticTransformer::GetUnsteadyTransform(double dSimTime, 
	                              VistaTransformMatrix &oStorage)
{
	oStorage = m_m4TotalTransform;
	return true;
}


VistaVector3D VflStaticTransformer::GetTranslation() const
{
	return m_v3Translation;
}

void VflStaticTransformer::GetTranslation(VistaVector3D &v3) const
{
	v3 = m_v3Translation;
}

bool VflStaticTransformer::SetTranslation(const VistaVector3D &v3)
{
	if(v3 == m_v3Translation)
		return false;
	m_v3Translation = v3;
	UpdateTransform();
	this->Notify(MSG_TRANSLATION_CHANGE);
	return true;
}


bool VflStaticTransformer::SetTranslation(float afTranslation[3])
{
	if(	m_v3Translation[0] == afTranslation[0] &&
		m_v3Translation[1] == afTranslation[1] &&
		m_v3Translation[2] == afTranslation[2] )
		return false;
	m_v3Translation.SetValues(afTranslation);
	UpdateTransform();
	this->Notify(MSG_TRANSLATION_CHANGE);
	return true;
}

VistaQuaternion VflStaticTransformer::GetRotation() const
{
	return m_qRotation;
}

void VflStaticTransformer::GetRotation(VistaQuaternion &qRot) const
{
	qRot = m_qRotation;
}

bool VflStaticTransformer::SetRotation(const VistaQuaternion &qRot)
{
	if(qRot == m_qRotation)
		return false;
	m_qRotation = qRot;
	UpdateTransform();
	this->Notify(MSG_ROTATION_CHANGE);
	return true;
}

bool VflStaticTransformer::SetRotation(float afRotation[4])
{
	if(	m_qRotation[0] == afRotation[0] &&
		m_qRotation[1] == afRotation[1] &&
		m_qRotation[2] == afRotation[2] &&
		m_qRotation[3] == afRotation[3] )
		return false;
	m_qRotation.SetValues(afRotation);
	UpdateTransform();
	this->Notify(MSG_ROTATION_CHANGE);
	return true;
}

float VflStaticTransformer::GetScale() const
{
	return m_fScale;
}

bool VflStaticTransformer::SetScale(float f)
{
	if(f == m_fScale)
		return false;
	m_fScale = f;
	UpdateTransform();
	this->Notify(MSG_SCALE_CHANGE);
	return true;
}

std::string VflStaticTransformer::GetReflectionableType() const
{
	return SVflStaticTransformerType;
}


int VflStaticTransformer::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int nRet = IVflTransformer::AddToBaseTypeList(rBtList);
	rBtList.push_back(SVflStaticTransformerType);
	return nRet+1;
}

std::string VflStaticTransformer::GetFactoryType()
{
	return std::string("STATIC_TRANSFORM");
}

void VflStaticTransformer::UpdateTransform()
{
	VistaTransformMatrix S;
	S.SetToIdentity();
	S.SetToScaleMatrix(m_fScale, m_fScale, m_fScale);
	VistaTransformMatrix R(m_qRotation);
	VistaTransformMatrix T;
	T.SetToIdentity();
	T.SetToTranslationMatrix(m_v3Translation);

	//first scale, then rotate, then translate.
	m_m4TotalTransform = T * R * S;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflUtils.cpp"                                                */
/*============================================================================*/
