/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLAXISROTATETRANSFORMER_H
#define _VFLAXISROTATETRANSFORMER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../Visualization/IVflTransformer.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaFlowLib/VistaFlowLibConfig.h>

#include <string>
#include <list>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveTimeMapper;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 */
class VISTAFLOWLIBAPI VflAxisRotateTransformer : public IVflTransformer
{
public:
	VflAxisRotateTransformer();
	VflAxisRotateTransformer(const VistaVector3D &rotationAxis,
								float fVelocity,
								const VistaVector3D &origin = VistaVector3D(0,0,0)
		                      );

	virtual ~VflAxisRotateTransformer();


	virtual bool GetUnsteadyTransform(double dSimTime, 
		                              VistaTransformMatrix &oStorage);


	VistaVector3D GetOrigin() const;
	void           GetOrigin(VistaVector3D &);
	bool           SetOrigin(const VistaVector3D &);
	bool           SetOrigin(float afOrigin[3]);


	VistaVector3D GetAxis() const;
	void           GetAxis(VistaVector3D &);

	bool           SetAxis(const VistaVector3D &);
	bool           SetAxis(float afAxis[3]);


	float          GetVelocity() const;
	bool           SetVelocity(float );
    virtual std::string GetReflectionableType() const;	
	
	bool SetTimeFile(const std::string &sTimeFile);
	std::string GetTimeFile() const;


	enum
	{
		MSG_ORIGIN_CHANGE = IVflTransformer::MSG_TIMELEVELSNAP_CHANGE,
		MSG_AXIS_CHANGE,
		MSG_VELOCITY_CHANGE,
		MSG_TIMER_FILE_CHANGE,

		MSG_LAST
	};

	static  std::string GetFactoryType();
protected:
    /**
     *  This method <b>must</b> be overloaded by subclasses in order
     * to build a propert hierarchy of base-type strings (i.e. strings 
	 * that encode a unique class identifier). This Method will add its 
	 * base type string as vanilla string to the rBtList.
     * @return the number of entries in the list
     * @param rBtList the list storage to keep track of base types
     */
    virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;
private:
	VistaVector3D m_afOrigin;
	VistaVector3D m_afAxis;
	VistaTransformMatrix m_matV, m_matVm;

	float          m_afVelocity;
	VveTimeMapper *m_pSnapMapper;
	std::string         m_sTimeFile;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

#endif // !defined(_VFLAXISROTATETRANSFORMER_H)
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
