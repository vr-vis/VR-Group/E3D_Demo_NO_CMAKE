/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* PRAGMAS                                                                    */
/*============================================================================*/
#ifdef WIN32
	#pragma warning(disable:4786)
#endif
#include <GL/glew.h>

#include "VflTimeInfo.h"

#include <VistaVisExt/Tools/VveTimeMapper.h>
#include "../Visualization/VflVisTiming.h"
#include "../Visualization/VflRenderNode.h"
#include "VflPropertyLoader.h"

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaFlowLib/Visualization/VflSystemAbstraction.h>

#include <cstdio>

#ifdef WIN32
	#include <windows.h>
#endif
#include <assert.h>
#include <algorithm>
#include <VistaBase/VistaExceptionBase.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VflTimeInfo::VflTimeInfo(VveTimeMapper *pTimeMapper)
: IVflRenderable(),
m_pPropertyLoader(NULL),
 m_pTimeMapper(pTimeMapper)
{
	assert(pTimeMapper);
}

VflTimeInfo::~VflTimeInfo()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VflTimeInfo::Draw2D()
{
	VflTimeInfo::VflTimeInfoProperties *props
		= static_cast<VflTimeInfo::VflTimeInfoProperties*>(GetProperties());

	string strResult;
	char buf[256];
	double fVisTime = GetRenderNode()->GetVisTiming()->GetVisualizationTime();
	double fVisStart, fVisEnd;
	this->GetRenderNode()->GetVisTiming()->GetVisTimeRange (fVisStart, fVisEnd);
	int iIndexStart = (*props).m_pMapper->GetTimeIndex(fVisStart);
	int iIndexEnd = (*props).m_pMapper->GetTimeIndex(fVisEnd);

	if ((*props).m_bDrawVisTime)
	{
		sprintf(buf, "vis time: %f", fVisTime);
		strResult += buf;
	}

	if ((*props).m_bDrawSimTime)
	{
		double fSimTime = (*props).m_pMapper->GetSimulationTime(fVisTime);
		sprintf(buf, "%ssim time: %f", strResult.empty()?"":" - ", fSimTime);
		strResult += buf;
	}

	if ((*props).m_bDrawTimeIndex)
	{
		int iTimeIndex = (*props).m_pMapper->GetTimeIndex(fVisTime);
		sprintf(buf, "%stime index: %d", strResult.empty()?"":" - ", iTimeIndex);
		strResult += buf;
	}

	if ((*props).m_bDrawTimeLevel)
	{
		int iTimeLevel = (*props).m_pMapper->GetTimeLevel(fVisTime);
		sprintf(buf, "%stime level: %d", strResult.empty()?"":" - ", iTimeLevel);
		strResult += buf;
	}

	if ((*props).m_bDrawLevelIndex)
	{
		int iLevelIndex = (*props).m_pMapper->GetLevelIndex(fVisTime);
		sprintf(buf, "%slevel index: %d", strResult.empty()?"":" - ", iLevelIndex);
		strResult += buf;
	}


	// here: we bypass the official setter, as this will try
	// to compare the incoming and the current text
	// and utter a notify. This might be too slow, so
	// we, well... bypass it ;)

	if((*props).m_pText)
	{
		(*props).m_pText->SetText(strResult);
		(*props).Notify(VflTimeInfoProperties::MSG_TEXTCHANGE);
		(*props).m_pText->Update();
	}




	// draw icons for every time index
	if ((*props).m_bShowIndexIcons && (*props).GetDisplayOnCurrentNode())
	{
		glPushAttrib(GL_LIGHTING_BIT | GL_LINE_BIT | GL_POINT_BIT | GL_COLOR_BUFFER_BIT);
		glDisable(GL_LIGHTING);
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GEQUAL, 0.01f);
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);

			glPushMatrix();
			glLoadIdentity();
			glMatrixMode(GL_PROJECTION);
			glPushMatrix();
			glLoadIdentity();
			glOrtho(0, 1.0, 0, 1.0, -1.0, 1.0);

			int iNumIndices = (*props).m_pMapper->GetNumberOfTimeIndices();

			float fDeltaX = 1.0f / (2*iNumIndices + 1);
			float x = fDeltaX;
			float y1 = 0.98f;
			float y3 = 0.96f;
			float y2 = 1.0f;

			int iCurrentIndex = (*props).m_pMapper->GetTimeIndex(fVisTime);

			glBegin(GL_QUADS);
			glColor4f(0, 1.0f, 0, 1.0f);

			for (int i=0; i<iNumIndices; ++i, x+=2*fDeltaX)
			{
				if ((i>=iIndexStart) && (i<=iIndexEnd))
				{
					glVertex2f(x, y1);
					glVertex2f(x+fDeltaX, y1);
					glVertex2f(x+fDeltaX, y2);
					glVertex2f(x, y2);
					if (i==iCurrentIndex)
					{
						glVertex2f(x, y2);
						glVertex2f(x+fDeltaX, y2);
						glVertex2f(x+fDeltaX, y3);
						glVertex2f(x, y3);
					}

				}
			}

			glColor4f(0.0f, 1.0f, 0.0f, 0.5f);
			x = fDeltaX;

			for (int i=0; i<iNumIndices; ++i, x+=2*fDeltaX)
			{
				if ((i<iIndexStart) || (i>iIndexEnd))
				{
					glVertex2f(x, y1);
					glVertex2f(x+fDeltaX, y1);
					glVertex2f(x+fDeltaX, y2);
					glVertex2f(x, y2);
					if (i==iCurrentIndex)
					{
						glVertex2f(x, y2);
						glVertex2f(x+fDeltaX, y2);
						glVertex2f(x+fDeltaX, y3);
						glVertex2f(x, y3);
					}

				}
			}

			glEnd();
			glPopMatrix();
			glMatrixMode(GL_MODELVIEW);
			glPopMatrix();

		glPopAttrib();
	}



}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VflTimeInfo::VflTimeInfoProperties *VflTimeInfo::GetProperties() const
{
	return static_cast<VflTimeInfoProperties*>(IVflRenderable::GetProperties());
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFactoryType                                              */
/*                                                                            */
/*============================================================================*/
std::string VflTimeInfo::GetFactoryType()
{
	return std::string("TIME_INFO");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
std::string VflTimeInfo::GetType() const
{
	return IVflRenderable::GetType()+string("::VflTimeInfo");
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeMapper                                               */
/*                                                                            */
/*============================================================================*/
VveTimeMapper* VflTimeInfo::GetTimeMapper() const
{
	return m_pTimeMapper;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CreateProperties                                            */
/*                                                                            */
/*============================================================================*/
IVflRenderable::VflRenderableProperties *VflTimeInfo::CreateProperties() const
{
		return new VflTimeInfoProperties(m_pTimeMapper);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ObserverUpdate                                              */
/*                                                                            */
/*============================================================================*/
void VflTimeInfo::ObserverUpdate(IVistaObserveable *pObserveable, int msg, int ticket)
{
	// intercept visibility change to turn text on or off
	if(msg == VflRenderableProperties::MSG_VISIBILITY)
	{
		VflTimeInfoProperties *p = dynamic_cast<VflTimeInfoProperties*>(pObserveable);
		if(p)
		{
			p->m_pText->SetEnabled(p->GetVisible());
		}
	}

	// in any case, pass the update to the super-class
	IVflRenderable::ObserverUpdate(pObserveable, msg, ticket);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRegistrationMode                                         */
/*                                                                            */
/*============================================================================*/
unsigned int VflTimeInfo::GetRegistrationMode() const
{
	return IVflRenderable::OLI_DRAW_2D;
}



/*============================================================================*/
/*  methods of VflTimeInfo::CProperties                                       */
/*============================================================================*/



static const string SsReflectionType("VflTimeInfo");

static IVistaPropertyGetFunctor *aCgFunctors[] =
{
	new TVistaPropertyGet<bool, VflTimeInfo::VflTimeInfoProperties, VistaProperty::PROPT_BOOL>
		  ("DRAW_SIM_TIME", SsReflectionType,
		  &VflTimeInfo::VflTimeInfoProperties::GetDrawSimTime),
	new TVistaPropertyGet<bool, VflTimeInfo::VflTimeInfoProperties, VistaProperty::PROPT_BOOL>
		  ("DRAW_VIS_TIME", SsReflectionType,
		  &VflTimeInfo::VflTimeInfoProperties::GetDrawVisTime),
	new TVistaPropertyGet<bool, VflTimeInfo::VflTimeInfoProperties, VistaProperty::PROPT_BOOL>
		  ("DRAW_TIME_LEVEL", SsReflectionType,
		  &VflTimeInfo::VflTimeInfoProperties::GetDrawTimeLevel),
	new TVistaPropertyGet<bool, VflTimeInfo::VflTimeInfoProperties, VistaProperty::PROPT_BOOL>
		  ("DRAW_TIME_INDEX", SsReflectionType,
		  &VflTimeInfo::VflTimeInfoProperties::GetDrawTimeIndex),
	new TVistaPropertyGet<bool, VflTimeInfo::VflTimeInfoProperties, VistaProperty::PROPT_BOOL>
		  ("DRAW_LEVEL_INDEX", SsReflectionType,
		  &VflTimeInfo::VflTimeInfoProperties::GetDrawLevelIndex),
	new TVistaPropertyGet<bool, VflTimeInfo::VflTimeInfoProperties, VistaProperty::PROPT_BOOL>
		  ("DISPLAYONCURRENTNODE", SsReflectionType,
		  &VflTimeInfo::VflTimeInfoProperties::GetDisplayOnCurrentNode),
	new TVistaPropertyGet<std::string, VflTimeInfo::VflTimeInfoProperties, VistaProperty::PROPT_STRING>
		  ("INFOTEXT", SsReflectionType,
		  &VflTimeInfo::VflTimeInfoProperties::GetText),
	new TVistaPropertyGet<std::list<std::string>, VflTimeInfo::VflTimeInfoProperties, VistaProperty::PROPT_LIST>
		  ("DISPLAY_ON_NODES", SsReflectionType,
		  &VflTimeInfo::VflTimeInfoProperties::GetNodeNamesForDisplay),
	new TVistaPropertyGet<std::string, VflTimeInfo::VflTimeInfoProperties, VistaProperty::PROPT_STRING>
		  ("TIME_FILE", SsReflectionType,
		  &VflTimeInfo::VflTimeInfoProperties::GetTimeFileName),
	new TVistaPropertyGet<bool, VflTimeInfo::VflTimeInfoProperties, VistaProperty::PROPT_BOOL>
		("SHOW_INDEX_ICONS", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::GetShowIndexIcons),

	new TVistaProperty3RefGet<float, VflTimeInfo::VflTimeInfoProperties> // implicitely PROPT_LIST
	("COLOR", SsReflectionType,
	&VflTimeInfo::VflTimeInfoProperties::GetTextColor),
	new TVistaProperty2RefGet<float, VflTimeInfo::VflTimeInfoProperties> // implicitely PROPT_LIST
		("POSITION", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::GetTextPosition),
	new TVistaPropertyGet<float, VflTimeInfo::VflTimeInfoProperties>
		("SIZE", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::GetTextSize),
	NULL
};

static IVistaPropertySetFunctor *aCsFunctors[] =
{
	new TVistaPropertySet<bool, bool,
						VflTimeInfo::VflTimeInfoProperties>
		("DRAW_SIM_TIME", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetDrawSimTime),
	new TVistaPropertySet<bool, bool,
						VflTimeInfo::VflTimeInfoProperties>
		("DRAW_VIS_TIME", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetDrawVisTime),
	new TVistaPropertySet<bool, bool,
						VflTimeInfo::VflTimeInfoProperties>
		("DRAW_TIME_LEVEL", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetDrawTimeLevel),
	new TVistaPropertySet<bool, bool,
						VflTimeInfo::VflTimeInfoProperties>
		("DRAW_TIME_INDEX", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetDrawTimeIndex),
	new TVistaPropertySet<bool, bool,
						VflTimeInfo::VflTimeInfoProperties>
		("DISPLAYONCURRENTNODE", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetDisplayOnCurrentNode),

	new TVistaPropertySet<bool, bool,
						VflTimeInfo::VflTimeInfoProperties>
		("DRAW_LEVEL_INDEX", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetDrawLevelIndex),
	new TVistaPropertySet<const std::string &, std::string ,
						VflTimeInfo::VflTimeInfoProperties>
		("INFOTEXT", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetText),

	new TVistaPropertySet<const std::list<std::string>&,
		  std::list<std::string>, VflTimeInfo::VflTimeInfoProperties>
		  ("DISPLAY_ON_NODES", SsReflectionType,
		  &VflTimeInfo::VflTimeInfoProperties::SetNodeNamesForDisplay),
	new TVistaPropertySet<const std::string &, std::string ,
						VflTimeInfo::VflTimeInfoProperties>
		("TIME_FILE", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetTimeFileName),
	new TVistaPropertySet<bool, bool,
						VflTimeInfo::VflTimeInfoProperties>
		("SHOW_INDEX_ICONS", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetShowIndexIcons),

	new TVistaProperty3ValSet<float, VflTimeInfo::VflTimeInfoProperties>
		( "COLOR", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetTextColor),

	new TVistaProperty2ValSet<float, VflTimeInfo::VflTimeInfoProperties>
		( "POSITION", SsReflectionType,
		&VflTimeInfo::VflTimeInfoProperties::SetTextPosition),

	new TVistaPropertySet<float, float,	VflTimeInfo::VflTimeInfoProperties>
		("SIZE", SsReflectionType, 
		&VflTimeInfo::VflTimeInfoProperties::SetTextSize),

	NULL
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

VflTimeInfo::VflTimeInfoProperties::VflTimeInfoProperties(VveTimeMapper *pTM )
: IVflRenderable::VflRenderableProperties(),
  m_bDrawVisTime(true),
  m_bDrawSimTime(true),
  m_bDrawTimeLevel(true),
  m_bDrawTimeIndex(false),
  m_bDrawLevelIndex(false),
  m_pText(NULL),
  m_bShowIndexIcons(false),
  m_bDisplayOnCurrentNode(true),
  m_pMapper(pTM)
{
	m_pText = IVflSystemAbstraction::GetSystemAbs()->AddOverlayText();
	assert(pTM);
}


VflTimeInfo::VflTimeInfoProperties::VflTimeInfoProperties()
: IVflRenderable::VflRenderableProperties(),
  m_bDrawVisTime(true),
  m_bDrawSimTime(true),
  m_bDrawTimeLevel(true),
  m_bDrawTimeIndex(false),
  m_bDrawLevelIndex(false),
  m_pText(NULL),
  m_bShowIndexIcons(false),
  m_bDisplayOnCurrentNode(true),
  m_pMapper(NULL)
{
	m_pText = IVflSystemAbstraction::GetSystemAbs()->AddOverlayText();
}

VflTimeInfo::VflTimeInfoProperties::~VflTimeInfoProperties()
{
	if(m_pText)
	{
		// IAR: todo: fix kernel for this!
		IVflSystemAbstraction::GetSystemAbs()->DeleteOverlayText(m_pText);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDrawVisTime                                              */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::GetDrawVisTime() const
{
	return m_bDrawVisTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDrawVisTime                                              */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetDrawVisTime(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bDrawVisTime))
	{
		Notify(MSG_DRAWVISTIME);
		return true;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDrawSimTime                                              */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::GetDrawSimTime() const
{
	return m_bDrawSimTime;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDrawSimTime                                              */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetDrawSimTime(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bDrawSimTime))
	{
		Notify(MSG_DRAWSIMTIME);
		return true;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDrawTimeLevel                                            */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::GetDrawTimeLevel() const
{
	return m_bDrawTimeLevel;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDrawTimeLevel                                            */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetDrawTimeLevel(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bDrawTimeLevel))
	{
		Notify(MSG_DRAWTIMELEVEL);
		return true;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDrawTimeIndex                                            */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::GetDrawTimeIndex() const
{
	return m_bDrawTimeIndex;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDrawTimeIndex                                            */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetDrawTimeIndex(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bDrawTimeIndex))
	{
		Notify(MSG_DRAWTIMEINDEX);
		return true;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDrawLevelIndex                                           */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::GetDrawLevelIndex() const
{
	return m_bDrawLevelIndex;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDrawLevelIndex                                           */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetDrawLevelIndex(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bDrawLevelIndex))
	{
		Notify(MSG_DRAWLEVELINDEX);
		return true;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetShowIndexIcons                                           */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::GetShowIndexIcons() const
{
	return m_bShowIndexIcons;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetShowIndexIcons                                           */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetShowIndexIcons(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bShowIndexIcons))
	{
		Notify(MSG_DRAWINDEXICONS);
		return true;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDisplayOnCurrentNode                                     */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::GetDisplayOnCurrentNode() const
{
	return m_bDisplayOnCurrentNode;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDisplayOnCurrentNode                                     */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetDisplayOnCurrentNode(bool bDraw)
{
	if(compAndAssignFunc<bool>(bDraw, m_bDisplayOnCurrentNode))
	{
		if(m_pText)
			m_pText->SetEnabled(m_bDisplayOnCurrentNode);

		Notify(MSG_DISPLAYONCURRENTNODE);
		return true;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNodeNamesForDisplay                                      */
/*                                                                            */
/*============================================================================*/
std::list<std::string> VflTimeInfo::VflTimeInfoProperties::
									   GetNodeNamesForDisplay() const
{
	return m_liNodeNamesForDisplay;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetNodeNamesForDisplay                                      */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetNodeNamesForDisplay(
										const std::list<std::string> &liNodeNames)
{
	// IAR @todo we could check for equality
	m_liNodeNamesForDisplay = liNodeNames;


	if (m_liNodeNamesForDisplay.empty())
	{
		SetDisplayOnCurrentNode(true);
	}
	else
	{
		IVflSystemAbstraction *pSystem = IVflSystemAbstraction::GetSystemAbs();
		if (pSystem->IsLeader())
		{
			SetDisplayOnCurrentNode(true);
		}
		else
		{
			// cluster - client
			string strNodeName = pSystem->GetClientName();
			if(std::find(m_liNodeNamesForDisplay.begin(), m_liNodeNamesForDisplay.end(), strNodeName)
				== m_liNodeNamesForDisplay.end())
			{
				SetDisplayOnCurrentNode(false);
			}
			else
				SetDisplayOnCurrentNode(true);
		}
	}

	Notify(MSG_NODEDISPLAYCHANGE);
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTextPosition                                             */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::GetTextPosition(float &fX, float &fY) const
{
	if(m_pText)
	{
		m_pText->GetPosition(fX, fY);
		return true;
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTextPosition                                             */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetTextPosition(float fX, float fY)
{
	if(m_pText)
	{

		m_pText->SetPosition(fX, fY);
		Notify(MSG_POSITIONCHANGE);
		return true;
	}

	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTextColor                                                */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::GetTextColor(float &fRed,
		float &fGreen, float &fBlue) const
{
	if(m_pText)
	{
		return m_pText->GetColor(fRed, fGreen, fBlue);
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTextColor                                                */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetTextColor(float fRed, float fGreen, float fBlue)
{
	if(m_pText && m_pText->SetColor(fRed, fGreen, fBlue))
	{
		Notify(MSG_COLORCHANGE);
		return true;
	}
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTextSize                                                */
/*                                                                            */
/*============================================================================*/
float VflTimeInfo::VflTimeInfoProperties::GetTextSize() const
{
	if(m_pText)
	{
		float fSize;
		m_pText->GetSize(fSize);
		return fSize;
	}
	return 0.f;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTextSize                                                */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetTextSize(float fSize)
{
	if(m_pText && m_pText->SetSize(fSize))
	{
		Notify(MSG_SIZECHANGE);
		return true;
	}
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetText                                                     */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetText(const std::string &sText)
{
	if(m_pText)
	{
		std::string sTmp;
		m_pText->GetText(sTmp);
		if(sTmp != sText)
		{
			m_pText->SetText(sText);
			Notify(MSG_TEXTCHANGE);
			return true;
		}
	}
	return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetText                                                     */
/*                                                                            */
/*============================================================================*/
string VflTimeInfo::VflTimeInfoProperties::GetText() const
{
	if(m_pText)
	{
		std::string sTmp;
		m_pText->GetText(sTmp);
		return sTmp;
	}
	return "<null>";
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VflTimeInfo::VflTimeInfoProperties::GetReflectionableType() const
{
	return SsReflectionType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VflTimeInfo::VflTimeInfoProperties::AddToBaseTypeList(list<string> &rBtList) const
{
	int nSize = IVflRenderable::VflRenderableProperties::AddToBaseTypeList(rBtList);
	rBtList.push_back(SsReflectionType);
	return nSize + 1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeFileName                                             */
/*                                                                            */
/*============================================================================*/
string VflTimeInfo::VflTimeInfoProperties::GetTimeFileName() const
{
	return m_sTimeFile;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTimeFileName                                             */
/*                                                                            */
/*============================================================================*/
bool VflTimeInfo::VflTimeInfoProperties::SetTimeFileName(const std::string &sTimeFile)
{
	VISTA_THROW("NOT IMPLEMENTED PROPERLY",-33);
	return false;
/************************************************************************
	AH:
	//This method should be reimplemented properly. unfortunately i dont know how to parse 
	//	a VveTimeMapper from a Timefile 
**************************************************************************/
// 	if(sTimeFile != m_oMapperKey.m_strFilename)
// 	{
// 		// release old mapper
// 		if(m_pMapper)
// 		{
// 			VflResourceManager::GetResourceManager()->ReleaseResource(m_pMapper, true, NULL);
// 		}
// 
// 		VflResourceKey<VflTimeMapper> oKey = m_oMapperKey;
// 		oKey.m_strFilename = sTimeFile;
// 
// 
// 		m_pMapper = VflResourceManager::GetResourceManager()->GetResource(oKey,NULL);
// 		if(m_pMapper)
// 		{
// 			m_oMapperKey = oKey;
// 			Notify(MSG_TIMEFILECHANGE);
// 			return true; // we indicate "change-success"!
// 		}
// 	}
// 	return false;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflTimeInfo.cpp"                                             */
/*============================================================================*/
