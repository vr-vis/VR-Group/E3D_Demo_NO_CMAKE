/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VflTimeMapperLoader.h"

#include <VistaFlowLib/Tools/VflPropertyLoader.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>

#include <VistaTools/VistaProfiler.h>
#include <VistaAspects/VistaAspectsUtils.h>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
bool VflTimeMapperLoader::CreateTimeMapperFromIni(VveTimeMapper* pTimeMapper,
							 const std::string& strIniFile,
							 const std::string& strIniSection)
{
	VistaProfiler oProf;
	std::string strValue;

	bool bReturnValue = false;
	strValue = oProf.ToUpper(oProf.GetTheProfileString(strIniSection, "TYPE", "", strIniFile));
	if(!strValue.empty()&&(strValue=="IMPLICIT"))
	{
		bReturnValue = this->CreateImplicitTimeMapperFromIni (pTimeMapper, strIniFile, strIniSection);
	}
	if(!strValue.empty()&&(strValue=="EXPLICIT"))
	{
		bReturnValue = this->CreateExplicitTimeMapperFromIni (pTimeMapper, strIniFile, strIniSection);
	}

	return bReturnValue; 
}


bool VflTimeMapperLoader::CreateImplicitTimeMapperFromIni(VveTimeMapper* pTimeMapper,
							 const std::string& strIniFile,
							 const std::string& strIniSection)
{
	VistaProfiler oProf;
	std::string strValue;

	VveTimeMapper::Timings oTimings;
	oTimings.m_iFirstLevel = oProf.GetTheProfileInt(strIniSection, "FIRST_LEVEL", 0, strIniFile);
	oTimings.m_iNumberOfLevels = oProf.GetTheProfileInt(strIniSection,"NUM_LEVELS",0,strIniFile);
	oTimings.m_iNumberOfIndices = oProf.GetTheProfileInt(strIniSection, "NUM_INDICES",0, strIniFile);
	oTimings.m_dSimStart = oProf.GetTheProfileFloat(strIniSection, "SIM_TIME_START",0.0f, strIniFile);	
	oTimings.m_dSimEnd = oProf.GetTheProfileFloat(strIniSection, "SIM_TIME_END",0.0f, strIniFile);
	oTimings.m_dVisStart = oProf.GetTheProfileFloat(strIniSection, "VIS_TIME_START",0.0f, strIniFile);	
	oTimings.m_dVisEnd = oProf.GetTheProfileFloat(strIniSection, "VIS_TIME_END",1.0f, strIniFile);
	oTimings.m_iLevelStride = oProf.GetTheProfileInt(strIniSection, "LEVEL_STRIDE",1, strIniFile);

	return pTimeMapper->SetByImplicitTimeMapping(oTimings);
}

bool VflTimeMapperLoader::CreateExplicitTimeMapperFromIni(VveTimeMapper* pTimeMapper,
							 const std::string& strIniFile,
							 const std::string& strIniSection)
{
	VistaProfiler oProf;
	std::string strValue;

	VveTimeMapper::Timings oTimings;
	oTimings.m_dVisStart = oProf.GetTheProfileFloat(strIniSection, "VIS_TIME_START",0.0f, strIniFile);	
	oTimings.m_dVisEnd = oProf.GetTheProfileFloat(strIniSection, "VIS_TIME_END",1.0f, strIniFile);
	pTimeMapper->SetByImplicitTimeMapping(oTimings);

	std::vector<VveTimeMapper::IndexInfo> oExplicitTimings;

	/*
	* ok, for reading explicit timings, we don't rely on VistaProfiler, as it has a maximum line length
	* For large data, i.e., 1000+ time steps, this line length is easily reached...
	* therefore we read the file directly
	*/
	std::ifstream oFile (strIniFile.c_str());
	std::list<std::string> slistTL;
	std::list<std::string> slistSimTimes;
	std::string sInput;
	char buffer[255];
	while (!oFile.eof())
	{
		oFile.getline (buffer, 255);
		sInput.assign (buffer);
		if (sInput=="[TIMELEVELS]")
		{
			oFile >> sInput;
			slistTL = VistaAspectsConversionStuff::ConvertToStringList (sInput);
		}
		if (sInput=="[SIMTIMES]")
		{
			oFile >> sInput;
			slistSimTimes = VistaAspectsConversionStuff::ConvertToStringList (sInput);
		}
	}

	if (slistTL.size() != slistSimTimes.size())
	{
		vstr::warnp() << "[VflTimeMapperLoader] Number of time levels and according sim times are different, can not create time mapper \n";
		return false;
	}
	VveTimeMapper::IndexInfo oIndexInfo;
	std::list<std::string>::iterator itTimeLevels = slistTL.begin();
	std::list<std::string>::iterator itSimTimes = slistSimTimes.begin();
	for (; itTimeLevels != slistTL.end(), itSimTimes != slistSimTimes.end(); itTimeLevels++, itSimTimes++)
	{	
		oIndexInfo.m_iLevel = VistaAspectsConversionStuff::ConvertToInt (*itTimeLevels);
		oIndexInfo.m_dSimTime = VistaAspectsConversionStuff::ConvertToDouble (*itSimTimes);
		oExplicitTimings.push_back (oIndexInfo);
	}
	return pTimeMapper->SetByExplicitTimeMapping(oExplicitTimings);

}

bool VflTimeMapperLoader::CreateImplicitTimeMapperFromPropList(
	VveTimeMapper* pTimeMapper, const VistaPropertyList &oProps)
{
	VveTimeMapper::Timings oTimings;

	double nDouble;
	int nInt;

	if(oProps.GetValue("SIM_TIME_START", nDouble) )
		oTimings.m_dSimStart = nDouble;
	if(oProps.GetValue("SIM_TIME_END", nDouble) )
		oTimings.m_dSimEnd = nDouble;
	if(oProps.GetValue("VIS_TIME_START", nDouble) )
		oTimings.m_dVisStart = nDouble;
	if(oProps.GetValue("VIS_TIME_END", nDouble) )
		oTimings.m_dVisEnd = nDouble;
	if(oProps.GetValue("NUM_INDICES", nInt) )
		oTimings.m_iNumberOfIndices = nInt;
	if(oProps.GetValue("NUM_LEVELS", nInt) )
		oTimings.m_iNumberOfLevels = nInt;
	if(oProps.GetValue("FIRST_LEVEL", nInt) )
		oTimings.m_iFirstLevel = nInt;
	if(oProps.GetValue("LEVEL_STRIDE", nInt) )
		oTimings.m_iLevelStride = nInt;	

	return pTimeMapper->SetByImplicitTimeMapping(oTimings);

}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
