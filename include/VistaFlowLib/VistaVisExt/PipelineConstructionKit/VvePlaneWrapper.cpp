/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 

#include "VvePlaneWrapper.h"
#include "VveWrapperConvUtils.h"

#include <vtkPlane.h>
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

static const std::string STR_REFLECTIONABLE_TYPENAME("VvePlaneWrapper");

//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = {
	//                     type to be gotten                              property name                                    pointer to getter method
	//                            ^                    reflectionable type       ^     type name of reflectionable as in base type list   ^
	//                            |                            ^                 |                ^                                       |
	new TVistaPropertyGet<const std::list<double>, VvePlaneWrapper>("NORMAL", STR_REFLECTIONABLE_TYPENAME, &VvePlaneWrapper::GetNormal),
	new TVistaPropertyGet<const std::list<double>, VvePlaneWrapper>("ORIGIN", STR_REFLECTIONABLE_TYPENAME, &VvePlaneWrapper::GetOrigin),
	NULL // alway use a terminating NULL here
};

//setter stuff
//NOTE: the setter's return type is assumed to be bool. If you don't want/need this you'll have to implement your own functor!
static IVistaPropertySetFunctor *setFunctors[] = {
	//                                                                 reflectionable type       type name of reflectionable as in base type list
	//                   input type of setter                               ^                   property name              ^                   pointer to setter method         pointer to conversion method
	//                               ^        output type of conversion     |                        ^                     |                               ^                               ^
	//                               |                   ^                  |                        |                     |                               |                               |
	new TVistaPropertySet<const std::list<double> &, std::list<double>, VvePlaneWrapper>("NORMAL", STR_REFLECTIONABLE_TYPENAME, &VvePlaneWrapper::SetNormal),
	new TVistaPropertySet<const std::list<double> &, std::list<double>, VvePlaneWrapper>("ORIGIN", STR_REFLECTIONABLE_TYPENAME, &VvePlaneWrapper::SetOrigin),
	NULL // alway use a terminating NULL here
};

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

VvePlaneWrapper::VvePlaneWrapper(void) : m_pPlane(vtkPlane::New())
{
}

VvePlaneWrapper::VvePlaneWrapper(const VvePlaneWrapper &)
{
}

VvePlaneWrapper::~VvePlaneWrapper(void)
{
	if(m_pPlane) 
		m_pPlane->Delete();
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

// Getter

std::string VvePlaneWrapper::GetReflectionableType() const
{
	return STR_REFLECTIONABLE_TYPENAME;
}

vtkObject * VvePlaneWrapper::GetOutput(const std::string & sPort) const
{
	// ???
	if(sPort != "OUTPUT")
		return 0;
	
	return m_pPlane;	
}

const std::list<double> VvePlaneWrapper::GetNormal() const 
{
	std::list<double> Normal;
	VveWrapperConvUtils::ConvertArrayToList(m_pPlane->GetNormal(), Normal);
	
	return Normal;	
}

const std::list<double> VvePlaneWrapper::GetOrigin() const 
{
	std::list<double> Origin;
	VveWrapperConvUtils::ConvertArrayToList(m_pPlane->GetOrigin(), Origin);

	return Origin;
}

const vtkPlane * VvePlaneWrapper::GetPlane() const 
{
	return m_pPlane;
}

// More getters for convenience only

void VvePlaneWrapper::GetNormal(VistaVector3D & rVector) const
{
	std::list<double> Normal(GetNormal());
	VveWrapperConvUtils::ConvertListToVec3D(Normal, rVector);
}

void VvePlaneWrapper::GetNormal(float fArray[3]) const
{
	std::list<double> Normal(GetNormal());
	VveWrapperConvUtils::ConvertListToArray(Normal, fArray);	
}

void VvePlaneWrapper::GetNormal(double dArray[3]) const
{
	std::list<double> Normal(GetNormal());
	VveWrapperConvUtils::ConvertListToArray(Normal, dArray);	
}

void VvePlaneWrapper::GetOrigin(VistaVector3D & rVector) const
{
	std::list<double> Origin(GetOrigin());
	VveWrapperConvUtils::ConvertListToVec3D(Origin, rVector);
}

void VvePlaneWrapper::GetOrigin(float fArray[3]) const
{
	std::list<double> Origin(GetOrigin());
	VveWrapperConvUtils::ConvertListToArray(Origin, fArray);	
}

void VvePlaneWrapper::GetOrigin(double dArray[3]) const
{
	std::list<double> Origin(GetOrigin());
	VveWrapperConvUtils::ConvertListToArray(Origin, dArray);	
}

// Setter

bool VvePlaneWrapper::SetNormal(const std::list<double> & rNormal)
{
	double pNormal[3];
	VveWrapperConvUtils::ConvertListToArray(rNormal, pNormal);
	m_pPlane->SetNormal (pNormal);
	return true;
}

bool VvePlaneWrapper::SetOrigin(const std::list<double> & rOrigin)
{
	double pOrigin[3];
	VveWrapperConvUtils::ConvertListToArray(rOrigin, pOrigin);
	m_pPlane->SetOrigin (pOrigin);
	return true;
}


// More setters for convenience only
bool VvePlaneWrapper::SetNormal(const VistaVector3D & rVector)
{
	std::list<double> Normal;
	VveWrapperConvUtils::ConvertVec3DToList(rVector, Normal);
	return SetNormal(Normal);
}

bool VvePlaneWrapper::SetNormal(float fArray[3])
{
	std::list<double> Normal;
	VveWrapperConvUtils::ConvertArrayToList(fArray, Normal);
	return SetNormal(Normal);
}

bool VvePlaneWrapper::SetNormal(double dArray[3])
{
	std::list<double> Normal;
	VveWrapperConvUtils::ConvertArrayToList(dArray, Normal);
	return SetNormal(Normal);
}

bool VvePlaneWrapper::SetOrigin(const VistaVector3D & rVector)
{
	std::list<double> Origin;
	VveWrapperConvUtils::ConvertVec3DToList(rVector, Origin);
	return SetOrigin(Origin);
}

bool VvePlaneWrapper::SetOrigin(float fArray[3])
{
	std::list<double> Origin;
	VveWrapperConvUtils::ConvertArrayToList(fArray, Origin);
	return SetOrigin(Origin);
}

bool VvePlaneWrapper::SetOrigin(double dArray[3])
{
	std::list<double> Origin;
	VveWrapperConvUtils::ConvertArrayToList(dArray, Origin);
	return SetOrigin(Origin);
}

void VvePlaneWrapper::SetPlane(vtkPlane * rPlane)
{
	m_pPlane = rPlane; 	
}

int VvePlaneWrapper::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int iSize(IVistaReflectionable::AddToBaseTypeList(rBtList));
	rBtList.push_back(STR_REFLECTIONABLE_TYPENAME);

	// @todo: Get rid of this cast.
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/* END OF FILE "VvePlaneWrapper.cpp"                                          */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

