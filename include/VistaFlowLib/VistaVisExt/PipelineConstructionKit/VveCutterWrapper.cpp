/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#include "VveCutterWrapper.h"
#include <vtkImplicitFunction.h>
#include <vtkCutter.h>
#include <vtkPolyData.h>

#include <list>
#include <string>
#include <map>
using namespace std;

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

static const string STR_REFLECTIONABLE_TYPENAME("VveCutterWrapper");


//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = {
	//                     type to be gotten                              property name                                    pointer to getter method
	//                            ^                    reflectionable type       ^     type name of reflectionable as in base type list   ^
	//                            |                            ^                 |                ^                                       |
	new TVistaPropertyGet<const list<double>, VveCutterWrapper>("VALUES", STR_REFLECTIONABLE_TYPENAME, &VveCutterWrapper::GetValues),
	NULL // alway use a terminating NULL here
};

//setter stuff
//NOTE: the setter's return type is assumed to be bool. If you don't want/need this you'll have to implement your own functor!
static IVistaPropertySetFunctor *setFunctors[] = {
	//                                                                 reflectionable type       type name of reflectionable as in base type list
	//                   input type of setter                               ^                   property name              ^                   pointer to setter method         pointer to conversion method
	//                               ^        output type of conversion     |                        ^                     |                               ^                               ^
	//                               |                   ^                  |                        |                     |                               |                               |
	new TVistaPropertySet<const list<double> &, list<double>, VveCutterWrapper>("VALUES", STR_REFLECTIONABLE_TYPENAME, &VveCutterWrapper::SetValues),
	NULL // alway use a terminating NULL here
};

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

VveCutterWrapper::VveCutterWrapper() : m_pCutter(vtkCutter::New ())
{
}

VveCutterWrapper::VveCutterWrapper(const VveCutterWrapper &)
{
}

VveCutterWrapper::~VveCutterWrapper()
{
	if(m_pCutter) 
		m_pCutter->Delete();
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

// Getter
string VveCutterWrapper::GetReflectionableType() const
{
	return STR_REFLECTIONABLE_TYPENAME;
}

vtkObject * VveCutterWrapper::GetOutput(const string & sPort) const
{
	if(sPort != "OUTPUT")
	{
		vstr::errp() << "[VveCutterWrapper::GetOutput] unknown output port "
				<< sPort << endl;
		return 0;
	}
	
	return m_pCutter->GetOutput();	
}

const list<double> VveCutterWrapper::GetValues() const 
{
	int iMaxContours(m_pCutter->GetNumberOfContours());
	list<double> Values;

	for(int iCounter = 0; iCounter<iMaxContours; ++iCounter)
	{
		Values.push_back(static_cast<double>(m_pCutter->GetValue(iCounter)));
	}

	return Values;
}

// Setter
bool VveCutterWrapper::SetInput(vtkObject * pObj, const string & sPort)
{
	if(sPort == "INPUT") 
	{
		vtkDataSet* pIN(vtkDataSet::SafeDownCast(pObj));
		if(pIN) 
		{
#if VTK_MAJOR_VERSION > 5
			m_pCutter->SetInputData(pIN);
#else
			m_pCutter->SetInput(pIN);
#endif
			return true;
		}
		vstr::errp() << "[VveCutterWrapper::SetInput] got no vtkDataSet as input on port " 
			<< sPort << endl;
		return false;
	}
	else if(sPort == "CUT_FUNCTION") 
	{
		vtkImplicitFunction * pIFct(vtkImplicitFunction::SafeDownCast(pObj));
		if(pIFct) 
		{
			m_pCutter->SetCutFunction(pIFct);
			return true;
		}
		vstr::errp() << "[VveCutterWrapper::SetInput] got no vtkImplicitFunction as input on port " 
			<< sPort << endl;
		return false;
	}
	return false;
}

bool VveCutterWrapper::SetValues(const list<double> & rValues)
{	
	int iCounter(0);

	for(list<double>::const_iterator cit = rValues.begin(); cit != rValues.end(); ++cit)
	{
		m_pCutter->SetValue(iCounter, (float) *cit);
		++iCounter;
	}
	
	return true;
}

// protected
vtkCutter * VveCutterWrapper::GetCutter() const 
{
	return m_pCutter; 
}

bool VveCutterWrapper::SetCutter(vtkCutter * pC)
{
	m_pCutter = pC;
	return true;
}

int VveCutterWrapper::AddToBaseTypeList(list<string> &rBtList) const
{
	int iSize(IVistaReflectionable::AddToBaseTypeList(rBtList));
	rBtList.push_back(STR_REFLECTIONABLE_TYPENAME);

	// @todo: Get rid of this cast.
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/* END OF FILE "VveCutterWrapper.cpp"                                         */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

