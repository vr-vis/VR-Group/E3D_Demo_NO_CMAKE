/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id:


#ifndef _VVECONTOURWRAPPER_H
#define _VVECONTOURWRAPPER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveAlgorithmWrapper.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkContourFilter;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveContourWrapper :
	public IVveAlgorithmWrapper
{
public:
	VveContourWrapper(void);
	virtual ~VveContourWrapper(void);
	virtual IVveWrapperBase *CreateInstance() const{
		return new VveContourWrapper;
	}
	virtual std::string GetReflectionableType() const;
	
	// Getter
	const std::list<double> GetContourValues() const;
	virtual vtkObject * GetOutput(const std::string & sPort = "OUTPUT") const;
	
	// Setter
	bool SetContourValues(const std::list<double> & LiVals);
	virtual bool SetInput(vtkObject * pObj, const std::string & sPort = "INPUT");

protected:
	VveContourWrapper(const VveContourWrapper &);

	void SetContourFilter(vtkContourFilter * pF);
	vtkContourFilter * GetContourFilter() const;

	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

private:
	vtkContourFilter * m_pContourFilter;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
