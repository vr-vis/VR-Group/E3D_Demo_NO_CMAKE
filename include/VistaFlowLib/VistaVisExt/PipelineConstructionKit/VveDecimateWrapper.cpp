/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#include "VveDecimateWrapper.h"
#include "VveWrapperConvUtils.h"

#include <vtkDecimatePro.h>
#include <vtkPolyData.h>
#include <vtkDataSet.h>

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

static const std::string STR_REFLECTIONABLE_TYPENAME("VveDecimateWrapper");

//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = {
	//                     type to be gotten                              property name                                    pointer to getter method
	//                            ^                    reflectionable type       ^     type name of reflectionable as in base type list   ^
	//                            |                            ^                 |                ^                                       |
	new TVistaPropertyGet<const std::list<double>, VveDecimateWrapper>("INFL_POINTS", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::GetInflectionPoints),
	new TVistaPropertyGet<double, VveDecimateWrapper>("TARGET_REDUCTION", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::GetTargetReduction),
	new TVistaPropertyGet<double, VveDecimateWrapper>("FEATURE_ANGLE", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::GetFeatureAngle),
	new TVistaPropertyGet<double, VveDecimateWrapper>("SPLIT_ANGLE", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::GetSplitAngle ),
	new TVistaPropertyGet<double, VveDecimateWrapper>("MAX_ERR", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::GetMaximumError ),
	new TVistaPropertyGet<double, VveDecimateWrapper>("ABS_ERR", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::GetAbsoluteError),
	new TVistaPropertyGet<double, VveDecimateWrapper>("INFL_POINT_RATIO", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::GetInflectionPointRatio),
	new TVistaPropertyGet<bool, VveDecimateWrapper>("PRESERVE_TOPOL", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::GetPreserveTopology),
	new TVistaPropertyGet<bool, VveDecimateWrapper>("SPLITTING", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::GetSplitting),
	NULL // alway use a terminating NULL here
};

//setter stuff
//NOTE: the setter's return type is assumed to be bool. If you don't want/need this you'll have to implement your own functor!
static IVistaPropertySetFunctor *setFunctors[] = {
	//                                                                 reflectionable type       type name of reflectionable as in base type list
	//                   input type of setter                               ^                   property name              ^                   pointer to setter method         pointer to conversion method
	//                               ^        output type of conversion     |                        ^                     |                               ^                               ^
	//                               |                   ^                  |                        |                     |                               |                               |
	new TVistaPropertySet<const double &, double, VveDecimateWrapper>("TARGET_REDUCTION", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::SetTargetReduction),
	new TVistaPropertySet<const double &, double, VveDecimateWrapper>("FEATURE_ANGLE", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::SetFeatureAngle),
	new TVistaPropertySet<const double &, double, VveDecimateWrapper>("SPLIT_ANGLE", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::SetSplitAngle ),
	new TVistaPropertySet<const double &, double, VveDecimateWrapper>("MAX_ERR", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::SetMaximumError ),
	new TVistaPropertySet<const double &, double, VveDecimateWrapper>("ABS_ERR", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::SetAbsoluteError),
	new TVistaPropertySet<const double &, double, VveDecimateWrapper>("INFL_POINT_RATIO", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::SetInflectionPointRatio),
	new TVistaPropertySet<const bool &, bool, VveDecimateWrapper>("PRESERVE_TOPOL", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::SetPreserveTopology),
	new TVistaPropertySet<const bool &, bool, VveDecimateWrapper>("SPLITTING", STR_REFLECTIONABLE_TYPENAME, &VveDecimateWrapper::SetSplitting),
	NULL // alway use a terminating NULL here
};

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

VveDecimateWrapper::VveDecimateWrapper() : m_pDecimate(vtkDecimatePro::New())
{
}

VveDecimateWrapper::VveDecimateWrapper(const VveDecimateWrapper &)
{
}

VveDecimateWrapper::~VveDecimateWrapper() 
{
	if(m_pDecimate) m_pDecimate->Delete();
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

// Getter
std::string VveDecimateWrapper::GetReflectionableType() const 
{
	return STR_REFLECTIONABLE_TYPENAME;
}

vtkObject * VveDecimateWrapper::GetOutput(const std::string & sPort) const
{
	if(sPort != "OUTPUT")
	{
		vstr::errp() << "[VveDecimateWrapper::GetOutput] unknown output port "
				<< sPort << std::endl;
		return 0; 
	}
	
	return m_pDecimate->GetOutput();
}

const std::list<double> VveDecimateWrapper::GetInflectionPoints() const 
{
	std::list<double> InflPoints;	
	VveWrapperConvUtils::ConvertArrayToList(m_pDecimate->GetInflectionPoints(), InflPoints);	

	return InflPoints;	
}

double VveDecimateWrapper::GetTargetReduction() const 
{
	return static_cast<double>(m_pDecimate->GetTargetReduction());
}

double VveDecimateWrapper::GetFeatureAngle() const 
{
	return static_cast<double>(m_pDecimate->GetFeatureAngle());
}

double VveDecimateWrapper::GetSplitAngle () const 
{
	return static_cast<double>(m_pDecimate->GetSplitAngle());
}

double VveDecimateWrapper::GetMaximumError () const 
{
	m_pDecimate->SetErrorIsAbsolute(0);
	return static_cast<double>(m_pDecimate->GetMaximumError());
}

double VveDecimateWrapper::GetAbsoluteError() const 
{
	m_pDecimate->SetErrorIsAbsolute(1);
	return static_cast<double>(m_pDecimate->GetAbsoluteError());
}

double VveDecimateWrapper::GetInflectionPointRatio() const 
{
	return static_cast<double>(m_pDecimate->GetInflectionPointRatio());
}

bool VveDecimateWrapper::GetPreserveTopology() const 
{
	return (m_pDecimate->GetPreserveTopology() != 0);
}


bool VveDecimateWrapper::GetSplitting() const 
{
	return (m_pDecimate->GetSplitting() != 0);
}

// Protected
vtkDecimatePro * VveDecimateWrapper::GetDecimate () const
{
	return m_pDecimate;
}
	
// Setter
bool VveDecimateWrapper::SetInput(vtkObject * pObj, const std::string & sPort)
{
	vtkPolyData * pPD(vtkPolyData::SafeDownCast(pObj));
	if((pPD != 0) && (sPort == "INPUT")) 
	{
#if VTK_MAJOR_VERSION > 5
		m_pDecimate->SetInputData(pPD);
#else
		m_pDecimate->SetInput(pPD);
#endif
		return true;
	}
	vstr::errp() << "[VveDecimateWrapper::SetInput] got no vtkPolyData as input on port " 
				<< sPort << std::endl;
	return false;
}


bool VveDecimateWrapper::SetTargetReduction(const double & rTargetReduction)
{
	m_pDecimate->SetTargetReduction(static_cast<float>(rTargetReduction));
	return true;
}

bool VveDecimateWrapper::SetFeatureAngle(const double & rFeatureAngle)
{
	m_pDecimate->SetFeatureAngle(static_cast<float>(rFeatureAngle));
	return true;
}

bool VveDecimateWrapper::SetSplitAngle (const double & rSplitAngle )
{
	m_pDecimate->SetSplitAngle(static_cast<float>(rSplitAngle));
	return true;
}

bool VveDecimateWrapper::SetMaximumError (const double & rMaximumError )
{
	m_pDecimate->SetErrorIsAbsolute(0);
	m_pDecimate->SetMaximumError(static_cast<float>(rMaximumError));
	return true;
}

bool VveDecimateWrapper::SetAbsoluteError(const double & rAbsoluteError)
{
	m_pDecimate->SetErrorIsAbsolute(1);
	m_pDecimate->SetAbsoluteError(static_cast<float>(rAbsoluteError));
	return true;
}

bool VveDecimateWrapper::SetInflectionPointRatio(const double & rInflectionPointRatio)
{
	m_pDecimate->SetInflectionPointRatio(static_cast<float>(rInflectionPointRatio));
	return true;
}

bool VveDecimateWrapper::SetPreserveTopology(const bool & rPreserveTopology)
{
	if(rPreserveTopology) m_pDecimate->SetPreserveTopology(1);
	else m_pDecimate->SetPreserveTopology(0);

	return true;
}

bool VveDecimateWrapper::SetSplitting(const bool & rSplitting)
{
	if(rSplitting) m_pDecimate->SetSplitting(1);
	else m_pDecimate->SetSplitting(0);

	return true;
}

bool VveDecimateWrapper::SetDecimate (vtkDecimatePro * pD )
{
	m_pDecimate = pD;
	return true;
}


int VveDecimateWrapper::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int iSize(IVistaReflectionable::AddToBaseTypeList(rBtList));
	rBtList.push_back(STR_REFLECTIONABLE_TYPENAME);

	// @todo: Get rid of this cast.
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/* END OF FILE "VveDecimateWrapper.cpp"                                       */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

