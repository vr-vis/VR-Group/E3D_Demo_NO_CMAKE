/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id:


#ifndef _VVEALGORITHMWRAPPER_H
#define _VVEALGORITHMWRAPPER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveWrapperBase.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkObject;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI IVveAlgorithmWrapper : public IVveWrapperBase
{
public:
	virtual ~IVveAlgorithmWrapper() {}

	virtual std::string GetReflectionableType() const 
	{
		return std::string("IVveAlgorithmWrapper");
	}
	
	virtual vtkObject * GetOutput(const std::string & sPort) const = 0;

	virtual bool SetInput(vtkObject * pObj, const std::string & sPort) = 0;

protected:
	IVveAlgorithmWrapper() {}
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const
	{
		IVistaReflectionable::AddToBaseTypeList(rBtList);
		rBtList.push_back("IVveAlgorithmWrapper");
		
		// @todo: Get rid of this cast.
		return static_cast<int>(rBtList.size());
	}

private:
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

