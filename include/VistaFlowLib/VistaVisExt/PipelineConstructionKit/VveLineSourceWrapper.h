/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id:


#ifndef _VVELINESOURCEWRAPPER_H
#define _VVELINESOURCEWRAPPER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveWrapperBase.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkLineSource;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveLineSourceWrapper : public IVveWrapperBase
{
public:

	VveLineSourceWrapper(void);
	virtual ~VveLineSourceWrapper(void);
	virtual IVveWrapperBase *CreateInstance() const{
		return new VveLineSourceWrapper;
	}
	

	//Getter
	virtual std::string GetReflectionableType() const;
	virtual vtkObject * GetOutput(const std::string & sPort="OUTPUT") const;

	const std::list<double> GetPoint1() const;
	const std::list<double> GetPoint2() const;
	const int GetResolution() const;
	
	// More getters for convenience only
	void GetPoint1(VistaVector3D & rVector) const;
	void GetPoint1(float fArray[3]) const;
	void GetPoint1(double dArray[3]) const;

	void GetPoint2(VistaVector3D & rVector) const;
	void GetPoint2(float fArray[3]) const;
	void GetPoint2(double dArray[3]) const;


	//Setter
	bool SetPoint1(const std::list<double> & rPoint1);
	bool SetPoint2(const std::list<double> & rPoint2);
	bool SetResolution(const int & rResolution);

	// More setters for convenience only
	bool SetPoint1(const VistaVector3D & rVector);
	bool SetPoint1(float fArray[3]);
	bool SetPoint1(double dArray[3]);

	bool SetPoint2(const VistaVector3D & rVector);
	bool SetPoint2(float fArray[3]);
	bool SetPoint2(double dArray[3]);
	
protected:
	VveLineSourceWrapper(const VveLineSourceWrapper &);

	const vtkLineSource * GetLineSource() const;
	void SetLineSource(vtkLineSource * pLineSource);

	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

private:
	vtkLineSource * m_pLineSource;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
