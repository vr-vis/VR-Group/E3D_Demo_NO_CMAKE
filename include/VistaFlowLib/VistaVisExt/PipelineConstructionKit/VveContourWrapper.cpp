/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#include "VveContourWrapper.h"
#include <vtkContourFilter.h>
#include <vtkDataSet.h>
#include <vtkPolyData.h>

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

static const std::string STR_REFLECTIONABLE_TYPENAME("VveContourWrapper");

//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = {
	//         type to be gotten                 property name                                    pointer to getter method
	//                     ^  reflectionable type       ^     type name of reflectionable as in base type list   ^
	//                     |          ^                 |                ^                                       |
	new TVistaPropertyGet<const std::list<double>, VveContourWrapper>("ISO_VALS", STR_REFLECTIONABLE_TYPENAME, &VveContourWrapper::GetContourValues),
	NULL // alway use a terminating NULL here
};
//setter stuff
//NOTE: the setter's return type is assumed to be bool. If you don't want/need this you'll have to implement your own functor!
static IVistaPropertySetFunctor *setFunctors[] = {
	//                              reflectionable type       type name of reflectionable as in base type list
	//            input type of setter                property name              ^                   pointer to setter method         pointer to conversion method
	//                     ^  output type of conversion     ^                    |                             ^                               ^
	//                     |    ^                           |                    |                             |                               |
	new TVistaPropertySet<const std::list<double> &, std::list<double>, VveContourWrapper>("ISO_VALS", STR_REFLECTIONABLE_TYPENAME, &VveContourWrapper::SetContourValues),
	NULL // alway use a terminating NULL here
};

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/



VveContourWrapper::VveContourWrapper(void) : m_pContourFilter(vtkContourFilter::New())
{
}

VveContourWrapper::VveContourWrapper(const VveContourWrapper &)
{
}

VveContourWrapper::~VveContourWrapper(void)
{
	if(m_pContourFilter) 
		m_pContourFilter->Delete();	
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

// Getter

std::string VveContourWrapper::GetReflectionableType() const
{
	return STR_REFLECTIONABLE_TYPENAME;
}

vtkObject * VveContourWrapper::GetOutput(const std::string & sPort) const
{
	if(sPort != "OUTPUT")
	{
		vstr::errp() << " [VveContourWrapper::GetOutput] unknown output port "
				<< sPort << std::endl;
		return 0;
	}
	
	return m_pContourFilter->GetOutput();	
}

const std::list<double> VveContourWrapper::GetContourValues() const
{
	std::list<double> IsoVals;
	int iNumOfContours(m_pContourFilter->GetNumberOfContours());

	for(int iCounter=0; iCounter<iNumOfContours; ++iCounter)
	{
		IsoVals.push_back((double) m_pContourFilter->GetValue(iCounter));
	}

	return IsoVals;
}


// Setter
bool VveContourWrapper::SetInput(vtkObject * pObj, const std::string & sPort)
{
	vtkDataSet * pDS(vtkDataSet::SafeDownCast(pObj));
	if((pDS != 0) && (sPort == "INPUT")) 
	{
#if VTK_MAJOR_VERSION > 5
		m_pContourFilter->SetInputData(pDS);
#else
		m_pContourFilter->SetInput(pDS);
#endif
		return true;
	}
	vstr::errp() << "[VveContourWrapper::SetInput] got no vtkDataSet as input on port " 
				<< sPort << std::endl;
	return false;
}

bool VveContourWrapper::SetContourValues(const std::list<double> & LiVals)
{
	m_pContourFilter->SetNumberOfContours(0);

	int iCounter(0);
	for( std::list<double>::const_iterator cit = LiVals.begin();
		cit != LiVals.end(); ++cit )
	{
		m_pContourFilter->SetValue(iCounter, (float) *cit);
		++iCounter;
	}

	if(iCounter != 0) {return true;} else {return false;};
}


void VveContourWrapper::SetContourFilter(vtkContourFilter *pF)
{
	m_pContourFilter = pF;
}	

vtkContourFilter * VveContourWrapper::GetContourFilter() const
{
	return m_pContourFilter;
}

int VveContourWrapper::AddToBaseTypeList(std::list<std::string> &rBtList) const{
	int iSize = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(STR_REFLECTIONABLE_TYPENAME);
	
	// @todo: Get rid of this cast.
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/* END OF FILE "VveContourWrapper.cpp"                                        */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

