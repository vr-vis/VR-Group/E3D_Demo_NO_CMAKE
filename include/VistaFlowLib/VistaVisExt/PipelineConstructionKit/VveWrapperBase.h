/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id:


#ifndef _VVEWRAPPERBASE_H
#define _VVEWRAPPERBASE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaReflectionable.h>
#include <VistaAspects/VistaPropertyFunctor.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkObject;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI IVveWrapperBase :	public IVistaReflectionable
{

public:
	virtual ~IVveWrapperBase(){}

	virtual IVveWrapperBase *CreateInstance() const = 0;

	//Getter
	virtual std::string GetReflectionableType() const { return std::string("IVveWrapperBase");}
	virtual vtkObject * GetOutput(const std::string & sPort) const = 0;
	
	virtual void GetOutputList(std::list<std::string>& listOut)
		{
		listOut.push_back("OUTPUT");
		};

	virtual void GetInputList(std::list<std::string>& listIn)
		{
		listIn.push_back("INPUT");
		};
protected:
	IVveWrapperBase() {}
	IVveWrapperBase(const IVveWrapperBase &) {}

	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const
	{
		IVistaReflectionable::AddToBaseTypeList(rBtList);
		rBtList.push_back("IVveWrapperBase");
		
		// @todo: Get rid of this cast.
		return static_cast<int>(rBtList.size());
	}
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/


#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
