/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id:


#ifndef _VVEPLANEWRAPPER_H
#define _VVEPLANEWRAPPER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveWrapperBase.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkPlane;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VvePlaneWrapper :
	public IVveWrapperBase
{
public:
	VvePlaneWrapper(void);
	virtual ~VvePlaneWrapper(void);
	virtual IVveWrapperBase *CreateInstance() const{
		return new VvePlaneWrapper;
	}
	

	//Getter
	virtual std::string GetReflectionableType() const;
	virtual vtkObject * GetOutput(const std::string & sPort="OUTPUT") const;

	const std::list<double> GetNormal() const;
	const std::list<double> GetOrigin() const;

	// More getters for convenience only

	void GetNormal(VistaVector3D & rVector) const;
	void GetNormal(float fArray[3]) const;
	void GetNormal(double dArray[3]) const;

	void GetOrigin(VistaVector3D & rVector) const;
	void GetOrigin(float fArray[3]) const;
	void GetOrigin(double dArray[3]) const;

	//Setter
	bool SetNormal(const std::list<double> & rNormal);
	bool SetOrigin(const std::list<double> & rOrigin);

	// More setters for convenience only
	bool SetNormal(const VistaVector3D & rVector);
	bool SetNormal(float fArray[3]);
	bool SetNormal(double dArray[3]);

	bool SetOrigin(const VistaVector3D & rVector);
	bool SetOrigin(float fArray[3]);
	bool SetOrigin(double dArray[3]);

protected:
	VvePlaneWrapper(const VvePlaneWrapper &);

	const vtkPlane * GetPlane() const;
	void SetPlane(vtkPlane * rPlane);
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

private:
	vtkPlane * m_pPlane;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
