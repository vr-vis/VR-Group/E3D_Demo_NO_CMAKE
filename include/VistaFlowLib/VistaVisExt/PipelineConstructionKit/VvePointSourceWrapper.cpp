/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 

#include "VvePointSourceWrapper.h"
#include "VveWrapperConvUtils.h"
#include <vtkPointSource.h>
#include <vtkPolyData.h>

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

static const std::string STR_REFLECTIONABLE_TYPENAME("VvePointSourceWrapper");

//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = {
	//                     type to be gotten                              property name                                    pointer to getter method
	//                            ^                    reflectionable type       ^     type name of reflectionable as in base type list   ^
	//                            |                            ^                 |                ^                                       |
	new TVistaPropertyGet<int, VvePointSourceWrapper>("NUMBER_OF_POINTS", STR_REFLECTIONABLE_TYPENAME, &VvePointSourceWrapper::GetNumberOfPoints),
	new TVistaPropertyGet<const std::list<double>, VvePointSourceWrapper>("CENTER", STR_REFLECTIONABLE_TYPENAME, &VvePointSourceWrapper::GetCenter),
	new TVistaPropertyGet<double, VvePointSourceWrapper>("RADIUS", STR_REFLECTIONABLE_TYPENAME, &VvePointSourceWrapper::GetRadius),
	new TVistaPropertyGet<const std::string, VvePointSourceWrapper>("DISTRIBUTION", STR_REFLECTIONABLE_TYPENAME, &VvePointSourceWrapper::GetDistribution),
	NULL // alway use a terminating NULL here
};

//setter stuff
//NOTE: the setter's return type is assumed to be bool. If you don't want/need this you'll have to implement your own functor!
static IVistaPropertySetFunctor *setFunctors[] = {
	//                                                                 reflectionable type       type name of reflectionable as in base type list
	//                   input type of setter                               ^                   property name              ^                   pointer to setter method         pointer to conversion method
	//                               ^        output type of conversion     |                        ^                     |                               ^                               ^
	//                               |                   ^                  |                        |                     |                               |                               |	
	new TVistaPropertySet<const int &, int, VvePointSourceWrapper>("NUMBER_OF_POINTS", STR_REFLECTIONABLE_TYPENAME, &VvePointSourceWrapper::SetNumberOfPoints),
	new TVistaPropertySet<const std::list<double> &, std::list<double>, VvePointSourceWrapper>("CENTER", STR_REFLECTIONABLE_TYPENAME, &VvePointSourceWrapper::SetCenter),
	new TVistaPropertySet<const double &, double, VvePointSourceWrapper>("RADIUS", STR_REFLECTIONABLE_TYPENAME, &VvePointSourceWrapper::SetRadius),
	new TVistaPropertySet<const std::string &, std::string, VvePointSourceWrapper>("DISTRIBUTION", STR_REFLECTIONABLE_TYPENAME, &VvePointSourceWrapper::SetDistribution),
	NULL // alway use a terminating NULL here
};

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VvePointSourceWrapper::VvePointSourceWrapper(void) : m_pPointSource(vtkPointSource::New())
{
}

VvePointSourceWrapper::VvePointSourceWrapper(const VvePointSourceWrapper &)
{	
}

VvePointSourceWrapper::~VvePointSourceWrapper(void)
{
	if(m_pPointSource)
		m_pPointSource->Delete();
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

//Getter

std::string VvePointSourceWrapper::GetReflectionableType() const
{
	return STR_REFLECTIONABLE_TYPENAME;
}

vtkObject * VvePointSourceWrapper::GetOutput(const std::string & sPort) const
{
	if(sPort != "OUTPUT")
	{
		vstr::errp() << "[VvePointSourceWrapper::GetOutput] unknown output port "
				<< sPort << std::endl;
		return 0;
	}
	
	return m_pPointSource->GetOutput();	
}

int VvePointSourceWrapper::GetNumberOfPoints() const 
{	
	return m_pPointSource->GetNumberOfPoints(); 
}

const std::list<double> VvePointSourceWrapper::GetCenter() const 
{
	std::list<double> Center;
	VveWrapperConvUtils::ConvertArrayToList(m_pPointSource->GetCenter(), Center);
	return Center;
}

double VvePointSourceWrapper::GetRadius() const 
{	
	return m_pPointSource->GetRadius(); 
}

const std::string VvePointSourceWrapper::GetDistribution() const 
{
	std::string sDistribution;	

	switch(m_pPointSource->GetDistribution())
	{
	case VTK_POINT_UNIFORM:
		sDistribution="UNIFORM";
		break;
		
	case VTK_POINT_SHELL:
		sDistribution="SHELL";
		break;	
	}

	return sDistribution;
}

// More getters for convenience only

void VvePointSourceWrapper::GetCenter(VistaVector3D & rVector) const
{
	std::list<double> Center(GetCenter());
	VveWrapperConvUtils::ConvertListToVec3D(Center, rVector);
}

void VvePointSourceWrapper::GetCenter(float fArray[3]) const
{
	std::list<double> Center(GetCenter());
	VveWrapperConvUtils::ConvertListToArray(Center, fArray);	
}

void VvePointSourceWrapper::GetCenter(double dArray[3]) const
{
	std::list<double> Center(GetCenter());
	VveWrapperConvUtils::ConvertListToArray(Center, dArray);	
}

//Setter

bool VvePointSourceWrapper::SetNumberOfPoints(const int & rNumberOfPoints)
{
	m_pPointSource->SetNumberOfPoints(rNumberOfPoints);	
	return true;
}

bool VvePointSourceWrapper::SetCenter(const std::list<double> & rCenter)
{
	double fArray[3];
	VveWrapperConvUtils::ConvertListToArray(rCenter, fArray);
	m_pPointSource->SetCenter(fArray);
	return true;
}

bool VvePointSourceWrapper::SetRadius(const double & rRadius)
{	
	m_pPointSource->SetRadius(rRadius);
	return true;
}

bool VvePointSourceWrapper::SetDistribution(const std::string & rDistribution)
{
	if(rDistribution == "UNIFORM") {m_pPointSource->SetDistribution(VTK_POINT_UNIFORM);}
	else if(rDistribution == "SHELL") {m_pPointSource->SetDistribution(VTK_POINT_SHELL);}	
	else {/* error */ return false;}
	return true;
}

// More setters for convenience only

bool VvePointSourceWrapper::SetCenter(const VistaVector3D & rVector)
{
	std::list<double> Center;
	VveWrapperConvUtils::ConvertVec3DToList(rVector, Center);
	return SetCenter(Center);
}

bool VvePointSourceWrapper::SetCenter(float fArray[3])
{
	std::list<double> Center;
	VveWrapperConvUtils::ConvertArrayToList(fArray, Center);
	return SetCenter(Center);
}

bool VvePointSourceWrapper::SetCenter(double dArray[3])
{
	std::list<double> Center;
	VveWrapperConvUtils::ConvertArrayToList(dArray, Center);
	return SetCenter(Center);
}

// protected

int VvePointSourceWrapper::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int iSize(IVistaReflectionable::AddToBaseTypeList(rBtList));
	rBtList.push_back(STR_REFLECTIONABLE_TYPENAME);

	// @todo: Get rid of this cast.
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/* END OF FILE "VvePointSourceWrapper.cpp"                                    */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

