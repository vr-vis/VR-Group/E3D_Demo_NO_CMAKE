/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VveVtkBuilder.h"
#include "VveVtkPipeline.h"

#include "VveContourWrapper.h"
#include "VveDecimateWrapper.h"
#include "VvePDNormalsWrapper.h"
#include "VveCutterWrapper.h"
#include "VveStreamTracerWrapper.h"
#include "VvePlaneWrapper.h"
#include "VveLineSourceWrapper.h"
#include "VvePointSourceWrapper.h"

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
map<string, const IVveWrapperBase*> VveVtkBuilder::m_mapPrototypes;
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveVtkBuilder::VveVtkBuilder()
{
}

VveVtkBuilder::~VveVtkBuilder()
{
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
void VveVtkBuilder::InitStandardWrappers() 
{
	VveVtkBuilder::RegisterWrapper("CONTOUR", new VveContourWrapper());
	VveVtkBuilder::RegisterWrapper("DECIMATE", new VveDecimateWrapper());
	VveVtkBuilder::RegisterWrapper("NORMALS", new VvePDNormalsWrapper());
	VveVtkBuilder::RegisterWrapper("CUTTER", new VveCutterWrapper());
	VveVtkBuilder::RegisterWrapper("STREAM_TRACER", new VveStreamTracerWrapper());
	VveVtkBuilder::RegisterWrapper("PLANE", new VvePlaneWrapper());
	VveVtkBuilder::RegisterWrapper("LINE_SOURCE", new VveLineSourceWrapper());
	VveVtkBuilder::RegisterWrapper("POINT_SOURCE", new VvePointSourceWrapper());	
}
	
VveVtkPipeline * VveVtkBuilder::Build(const VistaPropertyList & rParameters)
{

	if(! rParameters.HasProperty("COMPONENTS"))
	{
		vstr::errp() << "[VveVtkBuilder::Build] COMPONENTS missing!" << endl;
		return 0;
	}

	if(! rParameters.HasProperty("CONNECTIONS"))
	{
		vstr::errp() << "[VveVtkBuilder::Build] CONNECTIONS missing!" << endl;
		return 0;
	}

	VveVtkPipeline * pPipeline = new VveVtkPipeline;

	// COMPONENTS
	// @DRCLUSTER@ warning if no component available?
	if( rParameters.HasSubList( "COMPONENTS" ) )
	{
		const VistaPropertyList & Components = rParameters.GetSubListConstRef("COMPONENTS");
		for(VistaPropertyList::const_iterator cit = Components.begin(); cit != Components.end(); ++cit)
		{
			const string sName(VistaAspectsConversionStuff::ConvertToUpper((*cit).first));
			const string sType(VistaAspectsConversionStuff::ConvertToUpper((*cit).second.GetValue()));

			//create a concrete wrapper from the map of abstract prototypes
			map<string,const IVveWrapperBase*>::iterator itWrapper = m_mapPrototypes.find(sType);
			if(itWrapper == m_mapPrototypes.end())
			{
				vstr::errp() << "[VveVtkBuilder::Build] Wrapper type not supported : " << sType << endl;
				delete pPipeline;
				return NULL;
			}
			IVveWrapperBase *pWrapper = itWrapper->second->CreateInstance();
			if(!pWrapper)
			{
				vstr::errp() << "[VveVtkBuilder::Build] Unable to create wrapper instance [" << sType  << "]" << endl;
				delete pPipeline;
				return NULL;
			}
			pWrapper->SetNameForNameable(sName);
			pPipeline->AddWrapper(pWrapper);

#ifdef DEBUG
			vstr::debugi() << "Creating: " << sName << endl;
#endif

		}

#ifdef DEBUG
		vstr::debugi() << endl;
#endif
	}

	// CONNECTIONS
	// @DRCLUSTER@ warning if fails?
	if( rParameters.HasSubList( "CONNECTIONS" ) )
	{
		const VistaPropertyList & Connections = rParameters.GetSubListConstRef("CONNECTIONS");
		for(VistaPropertyList::const_iterator cit = Connections.begin(); cit != Connections.end(); ++cit)
		{
			const string sKey = (*cit).first;
			if( Connections.HasSubList( sKey ) == false )
				continue; // @DRCLUSTER@ warning?
			const VistaPropertyList & Connection = Connections.GetSubListConstRef(sKey);

			string sSourceObjName;
			string sOutputPort;
			string sSinkObjName;
			string sInputPort;

			if(! Connection.GetValue("SOURCE_OBJ_NAME", sSourceObjName))
			{
				vstr::errp() << "[VveVtkBuilder::Build] identifier SOURCE_OBJ_NAME is missing at" 
					<< sKey << "!" << endl;
				delete pPipeline;
				return 0;
			}

			if(! Connection.GetValue("OUTPUT_PORT", sOutputPort))
			{
				vstr::errp() << "[VveVtkBuilder::Build] identifier OUTPUT_PORT is missing at" 
					<< sKey << "!" << endl;
				delete pPipeline;
				return 0;
			}

			if(! Connection.GetValue("SINK_OBJ_NAME", sSinkObjName))
			{
				vstr::errp() << "[VveVtkBuilder::Build] identifier SINK_OBJ_NAME is missing at" 
					<< sKey << "!" << endl;
				delete pPipeline;
				return 0;
			}

			if(! Connection.GetValue("INPUT_PORT", sInputPort))
			{
				vstr::errp() << "[VveVtkBuilder::Build] identifier INPUT_PORT is missing at" 
					<< sKey << "!" << endl;
				delete pPipeline;
				return 0;
			}

			sOutputPort = VistaConversion::StringToUpper( sOutputPort );
			sInputPort = VistaConversion::StringToUpper( sInputPort );

			IVveWrapperBase * pSourceWrapper(pPipeline->GetWrapper(sSourceObjName));
			if(! pSourceWrapper)
			{
				vstr::errp() << "[VveVtkBuilder::Build] got no wrapper " << sSourceObjName << endl;
				delete pPipeline;
				return 0;
			}

			IVveWrapperBase * pSinkWrapper(pPipeline->GetWrapper(sSinkObjName));
			if(! pSinkWrapper)
			{
				vstr::errp() << "[VveVtkBuilder::Build] got no wrapper " << sSinkObjName << endl;
				delete pPipeline;
				return 0;
			}

			IVveAlgorithmWrapper * pSinkAlgoWrapper(dynamic_cast<IVveAlgorithmWrapper *>(pSinkWrapper));
			if(! pSinkAlgoWrapper)
			{
				vstr::errp() << "[VveVtkBuilder::Build] it is not possible to set input for " << sSinkObjName << endl;
				delete pPipeline;
				return 0;
			}

			vtkObject * pSourceObj(pSourceWrapper->GetOutput(sOutputPort));
			if(! pSourceObj)
			{
				vstr::errp() << "[VveVtkBuilder::Build] got no output from " 
					<< sSourceObjName << " on port " << sOutputPort << endl;
				delete pPipeline;
				return 0;
			}
			
			if(! pSinkAlgoWrapper->SetInput(pSourceObj, sInputPort))
			{
				vstr::errp() << "[VveVtkBuilder::Build] setting input for " << sSinkObjName << " on port " 
					<< sInputPort << " failed!" << endl; 
				delete pPipeline;
				return 0;
			}

	#ifdef DEBUG
			vstr::debugi() << "Creating Connection " << sKey << ": " << sSourceObjName << " -> " << sSinkObjName	<< endl;
	#endif

		}
		vstr::outi() << endl;
	}

	return pPipeline;
}


bool VveVtkBuilder::RegisterWrapper(const string& strWrapperName, IVveWrapperBase *pWrapper)
{
	if(!pWrapper || strWrapperName.empty())
		return false;
	map<string, const IVveWrapperBase*>::const_iterator it = m_mapPrototypes.find(strWrapperName);
	if(it != m_mapPrototypes.end())
		return false;
	m_mapPrototypes.insert(pair<string, const IVveWrapperBase*>(strWrapperName,pWrapper));
	return true;
}

const IVveWrapperBase *VveVtkBuilder::UnregisterWrapper(const string& strWrapperName)
{
	map<string, const IVveWrapperBase*>::iterator it = m_mapPrototypes.find(strWrapperName);
	if(it == m_mapPrototypes.end())
		return NULL;
	const IVveWrapperBase *pWrapper = it->second;
	m_mapPrototypes.erase(it);
	return pWrapper;
}

/*============================================================================*/
/* END OF FILE "VveVtkBuilder.cpp"                                            */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

