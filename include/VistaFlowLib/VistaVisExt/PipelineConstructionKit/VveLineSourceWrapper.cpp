/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 

#include "VveLineSourceWrapper.h"
#include "VveWrapperConvUtils.h"
#include <vtkLineSource.h>
#include <vtkPolyData.h>

#include <list>
#include <string>
using namespace std;

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

static const std::string STR_REFLECTIONABLE_TYPENAME("VveLineSourceWrapper");

//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = {
	//                     type to be gotten                              property name                                    pointer to getter method
	//                            ^                    reflectionable type       ^     type name of reflectionable as in base type list   ^
	//                            |                            ^                 |                ^                                       |
	new TVistaPropertyGet<const std::list<double> , VveLineSourceWrapper>("POINT1", STR_REFLECTIONABLE_TYPENAME, &VveLineSourceWrapper::GetPoint1),
	new TVistaPropertyGet<const std::list<double> , VveLineSourceWrapper>("POINT2", STR_REFLECTIONABLE_TYPENAME, &VveLineSourceWrapper::GetPoint2),
	new TVistaPropertyGet<const int , VveLineSourceWrapper>("RESOLUTION", STR_REFLECTIONABLE_TYPENAME, &VveLineSourceWrapper::GetResolution),
	NULL // alway use a terminating NULL here
};

//setter stuff
//NOTE: the setter's return type is assumed to be bool. If you don't want/need this you'll have to implement your own functor!
static IVistaPropertySetFunctor *setFunctors[] = {
	//                                                                 reflectionable type       type name of reflectionable as in base type list
	//                   input type of setter                               ^                   property name              ^                   pointer to setter method         pointer to conversion method
	//                               ^        output type of conversion     |                        ^                     |                               ^                               ^
	//                               |                   ^                  |                        |                     |                               |                               |
	new TVistaPropertySet<const std::list<double> &, std::list<double>, VveLineSourceWrapper>("POINT1", STR_REFLECTIONABLE_TYPENAME, &VveLineSourceWrapper::SetPoint1),
	new TVistaPropertySet<const std::list<double> &, std::list<double>, VveLineSourceWrapper>("POINT2", STR_REFLECTIONABLE_TYPENAME, &VveLineSourceWrapper::SetPoint2),
	new TVistaPropertySet<const int &, int, VveLineSourceWrapper>("RESOLUTION", STR_REFLECTIONABLE_TYPENAME, &VveLineSourceWrapper::SetResolution),
	NULL // alway use a terminating NULL here
};

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

VveLineSourceWrapper::VveLineSourceWrapper(void) : m_pLineSource(vtkLineSource::New())
{
}

VveLineSourceWrapper::VveLineSourceWrapper(const VveLineSourceWrapper &)
{
}

VveLineSourceWrapper::~VveLineSourceWrapper(void)
{
	if(m_pLineSource)
		m_pLineSource->Delete();
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

//Getter

std::string VveLineSourceWrapper::GetReflectionableType() const
{
	return STR_REFLECTIONABLE_TYPENAME;
}

vtkObject * VveLineSourceWrapper::GetOutput(const std::string & sPort) const
{
	if(sPort != "OUTPUT")
	{
		vstr::errp() << "[VveLineSourceWrapper::GetOutput] unknown output port "
				<< sPort << std::endl;
		return 0;
	}
	
	return m_pLineSource->GetOutput();	
}

const std::list<double> VveLineSourceWrapper::GetPoint1() const 
{
	list<double> Point1;
	VveWrapperConvUtils::ConvertArrayToList(m_pLineSource->GetPoint1(), Point1);

	return Point1;
}

const std::list<double> VveLineSourceWrapper::GetPoint2() const 
{
	list<double> Point2;
	VveWrapperConvUtils::ConvertArrayToList(m_pLineSource->GetPoint2(), Point2);

	return Point2;
}

const int VveLineSourceWrapper::GetResolution() const 
{
	return m_pLineSource->GetResolution();
}

const vtkLineSource * VveLineSourceWrapper::GetLineSource() const 
{
	return m_pLineSource;
}

// More getters for convenience only

void VveLineSourceWrapper::GetPoint1(VistaVector3D & rVector) const
{
	std::list<double> Point1(GetPoint1());
	VveWrapperConvUtils::ConvertListToVec3D(Point1, rVector);
}

void VveLineSourceWrapper::GetPoint1(float fArray[3]) const
{
	std::list<double> Point1(GetPoint1());
	VveWrapperConvUtils::ConvertListToArray(Point1, fArray);	
}

void VveLineSourceWrapper::GetPoint1(double dArray[3]) const
{
	std::list<double> Point1(GetPoint1());
	VveWrapperConvUtils::ConvertListToArray(Point1, dArray);	
}

void VveLineSourceWrapper::GetPoint2(VistaVector3D & rVector) const
{
	std::list<double> Point2(GetPoint2());
	VveWrapperConvUtils::ConvertListToVec3D(Point2, rVector);
}

void VveLineSourceWrapper::GetPoint2(float fArray[3]) const
{
	std::list<double> Point2(GetPoint2());
	VveWrapperConvUtils::ConvertListToArray(Point2, fArray);	
}

void VveLineSourceWrapper::GetPoint2(double dArray[3]) const
{
	std::list<double> Point2(GetPoint2());
	VveWrapperConvUtils::ConvertListToArray(Point2, dArray);	
}

//Setter

bool VveLineSourceWrapper::SetPoint1(const std::list<double> & rPoint1)
{
	double fPoint[3];
	VveWrapperConvUtils::ConvertListToArray(rPoint1, fPoint);
	m_pLineSource->SetPoint1(fPoint);
	
	return true;
}

bool VveLineSourceWrapper::SetPoint2(const std::list<double> & rPoint2)
{
	double fPoint[3];
	VveWrapperConvUtils::ConvertListToArray(rPoint2, fPoint);
	m_pLineSource->SetPoint2(fPoint);
	return true;
}

bool VveLineSourceWrapper::SetResolution(const int & rResolution)
{
	m_pLineSource->SetResolution(rResolution);
		
	return true;
}

// More setters for convenience only

bool VveLineSourceWrapper::SetPoint1(const VistaVector3D & rVector)
{
	std::list<double> Point1;
	VveWrapperConvUtils::ConvertVec3DToList(rVector, Point1);
	return SetPoint1(Point1);
}

bool VveLineSourceWrapper::SetPoint1(float fArray[3])
{
	std::list<double> Point1;
	VveWrapperConvUtils::ConvertArrayToList(fArray, Point1);
	return SetPoint1(Point1);
}

bool VveLineSourceWrapper::SetPoint1(double dArray[3])
{
	std::list<double> Point1;
	VveWrapperConvUtils::ConvertArrayToList(dArray, Point1);
	return SetPoint1(Point1);
}

bool VveLineSourceWrapper::SetPoint2(const VistaVector3D & rVector)
{
	std::list<double> Point2;
	VveWrapperConvUtils::ConvertVec3DToList(rVector, Point2);
	return SetPoint2(Point2);
}

bool VveLineSourceWrapper::SetPoint2(float fArray[3])
{
	std::list<double> Point2;
	VveWrapperConvUtils::ConvertArrayToList(fArray, Point2);
	return SetPoint2(Point2);
}

bool VveLineSourceWrapper::SetPoint2(double dArray[3])
{
	std::list<double> Point2;
	VveWrapperConvUtils::ConvertArrayToList(dArray, Point2);
	return SetPoint2(Point2);
}

// protected

void VveLineSourceWrapper::SetLineSource(vtkLineSource * pLineSource)
{
	m_pLineSource = pLineSource;
}

int VveLineSourceWrapper::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int iSize(IVistaReflectionable::AddToBaseTypeList(rBtList));
	rBtList.push_back(STR_REFLECTIONABLE_TYPENAME);

	// @todo: Get rid of this cast.
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/* END OF FILE "VveLineSourceWrapper.cpp"                                     */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

