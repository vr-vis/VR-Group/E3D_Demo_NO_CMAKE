/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#ifndef _VVEVTKBUILDER_H
#define _VVEVTKBUILDER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveWrapperBase.h"
#include <map>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveVtkPipeline;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveVtkBuilder
{
public:
	virtual ~VveVtkBuilder();

	/**
	* this call will register all wrappers known within ViSTA VtkExt with the builder
	* Make sure you add your wrappers here, if they are part of VtkExt.
	*/
	static void InitStandardWrappers();
	/**
	* build a pipeline from a symbolic representation given in a VistaPropertyList
	* (see ViSTA WiKi for details!)
	*/
	static VveVtkPipeline * Build(const VistaPropertyList & rParameters);
	/**
	* Register an additional wrapper with a given name. This wrapper will then be available 
	* for construction.
	*/
	static bool RegisterWrapper(const std::string& strWrapperName, IVveWrapperBase *pWrapper);
	/**
	* Unregister a wrapper
	*/
	static const IVveWrapperBase *UnregisterWrapper(const std::string& strWrapperName);

protected:
	VveVtkBuilder();

	static std::map<std::string, const IVveWrapperBase*> m_mapPrototypes;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
