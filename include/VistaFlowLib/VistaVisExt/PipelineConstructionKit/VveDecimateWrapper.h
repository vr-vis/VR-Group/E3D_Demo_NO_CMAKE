/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id:


#ifndef _VVEDECIMATEWRAPPER_H
#define _VVEDECIMATEWRAPPER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveAlgorithmWrapper.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkDecimatePro;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveDecimateWrapper :	public IVveAlgorithmWrapper
{

public:
	VveDecimateWrapper();
	virtual ~VveDecimateWrapper();
	virtual IVveWrapperBase *CreateInstance() const{
		return new VveDecimateWrapper;
	}
	
	// Getter
	virtual std::string GetReflectionableType() const;
	virtual vtkObject * GetOutput(const std::string & sPort = "OUTPUT") const;

	const std::list<double> GetInflectionPoints() const;
	double GetTargetReduction() const;
	double GetFeatureAngle() const;
	double GetSplitAngle () const;
	double GetMaximumError () const;
	double GetAbsoluteError() const;
	double GetInflectionPointRatio() const;

	bool GetPreserveTopology() const;
	bool GetSplitting() const;

public:
	// Setter
	virtual bool SetInput(vtkObject * pObj, const std::string & sPort = "INPUT");

	bool SetTargetReduction(const double & rTargetReduction);
	bool SetFeatureAngle(const double & rFeatureAngle);
	bool SetSplitAngle (const double & rSplitAngle);
	bool SetMaximumError (const double & rMaximumError);
	bool SetAbsoluteError(const double & rAbsoluteError);
	bool SetInflectionPointRatio(const double & rInflectionPointRatio);

	bool SetPreserveTopology(const bool & rPreserveTopology);
	bool SetSplitting(const bool & rSplitting);
	
protected:
	VveDecimateWrapper(const VveDecimateWrapper &);

	vtkDecimatePro * GetDecimate () const;
	bool SetDecimate (vtkDecimatePro * pD );
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

private:
	
	vtkDecimatePro * m_pDecimate;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

#endif
