/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id:


#ifndef _VVEWRAPPERCONVUTILS_H
#define _VVEWRAPPERCONVUTILS_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaBase/VistaVectorMath.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveWrapperConvUtils 
{

public:
	
	static void ConvertArrayToList(float pArrayIn[3], std::list<double> & rListOut)
	{
		for(int iCounter=0; iCounter<3; ++iCounter)
		{
			rListOut.push_back(static_cast<double>(pArrayIn[iCounter]));
		}
	}
	
	//for future use
	static void ConvertArrayToList(double pArrayIn[3], std::list<double> & rListOut)
	{
		for(int iCounter=0; iCounter<3; ++iCounter)
		{
			rListOut.push_back(pArrayIn[iCounter]);
		}
	}

	static void ConvertListToArray(const std::list<double> & rListIn, float pArrayOut[3])
	{
		int iCounter(0);		

		for(std::list<double>::const_iterator cit = rListIn.begin(); 
			cit!=rListIn.end(); ++cit)
		{			
			pArrayOut[iCounter] = static_cast<float>(*cit);
			++iCounter;
		}
	}

	//for future use
	static void ConvertListToArray(const std::list<double> & rListIn, double pArrayOut[3])
	{
		int iCounter(0);

		for(std::list<double>::const_iterator cit = rListIn.begin(); 
			cit!=rListIn.end(); ++cit)
		{
			pArrayOut[iCounter] = *cit;
			++iCounter;
		}
	}

	static void ConvertListToVec3D(const std::list<double> & rListIn, VistaVector3D & rVec3DOut)
	{
		float fArray[3];
		ConvertListToArray(rListIn, fArray);
		rVec3DOut = VistaVector3D(fArray);
	}

	static void ConvertVec3DToList(const VistaVector3D & rVec3D, std::list<double> & rListOut)
	{
		float fArray[3];
		rVec3D.GetValues(fArray);
		ConvertArrayToList(fArray, rListOut);
	}

protected:
	
private:
	VveWrapperConvUtils(){}
	virtual ~VveWrapperConvUtils(){}
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
