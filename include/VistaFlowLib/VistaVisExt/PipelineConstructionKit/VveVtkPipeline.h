/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#ifndef _VVEVTKPIPELINE_H
#define _VVEVTKPIPELINE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveWrapperBase.h"

#include <VistaVisExt/VistaVisExtConfig.h>

#include <string>
#include <map>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkDataObject;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveVtkPipeline
{

public:

	VveVtkPipeline();
	VveVtkPipeline(const VveVtkPipeline & rhs);
	virtual ~VveVtkPipeline();

	// Getter
	
	IVveWrapperBase * GetWrapper(const std::string & sName) const;
	const VistaPropertyList GetParameters(const std::string & sWrapperName) const;
	vtkDataObject * GetOutput(const std::string & sName, const std::string & sPort) const;

	// Operations

	bool AddWrapper(IVveWrapperBase * pWrapper);
	
	// Setter
	
	bool SetParameters(const VistaPropertyList & rParameters);
	bool SetInput(vtkDataObject * pDataObj, const std::string & sName, const std::string & sPort);

private:
	std::map<std::string, IVveWrapperBase *> m_Wrappers;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

#endif
