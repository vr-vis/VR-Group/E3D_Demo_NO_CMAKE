/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#include "VvePDNormalsWrapper.h"
#include "VveWrapperConvUtils.h"

#include <vtkPolyDataNormals.h>
#include <vtkPolyData.h>
#include <vtkDataSet.h>

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

static const std::string STR_REFLECTIONABLE_TYPENAME("VvePDNormalsWrapper");

//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = {
	//                     type to be gotten                              property name                                    pointer to getter method
	//                            ^                    reflectionable type       ^     type name of reflectionable as in base type list   ^
	//                            |                            ^                 |                ^                                       |
	new TVistaPropertyGet<double, VvePDNormalsWrapper>("FEATURE_ANGLE", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::GetFeatureAngle),
	new TVistaPropertyGet<bool, VvePDNormalsWrapper>("SPLITTING", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::GetSplitting),
	new TVistaPropertyGet<bool, VvePDNormalsWrapper>("CONSISTENCY", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::GetConsistency),
	new TVistaPropertyGet<bool, VvePDNormalsWrapper>("POINT_NORMALS", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::GetComputePointNormals),
	new TVistaPropertyGet<bool, VvePDNormalsWrapper>("CELL_NORMALS", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::GetComputeCellNormals),
	new TVistaPropertyGet<bool, VvePDNormalsWrapper>("FLIP_NORMALS", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::GetFlipNormals),
	new TVistaPropertyGet<bool, VvePDNormalsWrapper>("NON_MANIFOLD_TRAVERSAL", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::GetNonManifoldTraversal),
	NULL // alway use a terminating NULL here
};

//setter stuff
//NOTE: the setter's return type is assumed to be bool. If you don't want/need this you'll have to implement your own functor!
static IVistaPropertySetFunctor *setFunctors[] = {
	//                                                                 reflectionable type       type name of reflectionable as in base type list
	//                   input type of setter                               ^                   property name              ^                   pointer to setter method         pointer to conversion method
	//                               ^        output type of conversion     |                        ^                     |                               ^                               ^
	//                               |                   ^                  |                        |                     |                               |                               |
	new TVistaPropertySet<const double &, double, VvePDNormalsWrapper>("FEATURE_ANGLE", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::SetFeatureAngle),
	new TVistaPropertySet<const bool &, bool, VvePDNormalsWrapper>("SPLITTING", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::SetSplitting),
	new TVistaPropertySet<const bool &, bool, VvePDNormalsWrapper>("CONSISTENCY", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::SetConsistency),
	new TVistaPropertySet<const bool &, bool, VvePDNormalsWrapper>("POINT_NORMALS", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::SetComputePointNormals),
	new TVistaPropertySet<const bool &, bool, VvePDNormalsWrapper>("CELL_NORMALS", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::SetComputeCellNormals),
	new TVistaPropertySet<const bool &, bool, VvePDNormalsWrapper>("FLIP_NORMALS", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::SetFlipNormals),
	new TVistaPropertySet<const bool &, bool, VvePDNormalsWrapper>("NON_MANIFOLD_TRAVERSAL", STR_REFLECTIONABLE_TYPENAME, &VvePDNormalsWrapper::SetNonManifoldTraversal),
	NULL // alway use a terminating NULL here
};

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

VvePDNormalsWrapper::VvePDNormalsWrapper() : m_pPDataNormals(vtkPolyDataNormals::New())
{
}

VvePDNormalsWrapper::VvePDNormalsWrapper(const VvePDNormalsWrapper &)
{
}

VvePDNormalsWrapper::~VvePDNormalsWrapper()
{
	if(m_pPDataNormals) 
		m_pPDataNormals->Delete();
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

// Getter
std::string VvePDNormalsWrapper::GetReflectionableType() const
{
	return STR_REFLECTIONABLE_TYPENAME;
}

vtkObject * VvePDNormalsWrapper::GetOutput(const std::string & sPort) const
{
	if(sPort != "OUTPUT")
	{
		vstr::errp() << "[VvePDNormalsWrapper::GetOutput] unknown output port "
				<< sPort << std::endl;
		return 0;
	}

	return m_pPDataNormals->GetOutput();	
}

double VvePDNormalsWrapper::GetFeatureAngle() const 
{
	return (static_cast<double>(m_pPDataNormals->GetFeatureAngle())); 
}

bool VvePDNormalsWrapper::GetSplitting() const 
{
	return (m_pPDataNormals->GetSplitting() != 0);	
}
bool VvePDNormalsWrapper::GetConsistency() const 
{
	return (m_pPDataNormals->GetConsistency() != 0);
}

bool VvePDNormalsWrapper::GetComputePointNormals() const 
{
	return (m_pPDataNormals->GetComputePointNormals() != 0);
}

bool VvePDNormalsWrapper::GetComputeCellNormals() const 
{
	return (m_pPDataNormals->GetComputeCellNormals() != 0);
}

bool VvePDNormalsWrapper::GetFlipNormals() const 
{
	return (m_pPDataNormals->GetFlipNormals() != 0);
}

bool VvePDNormalsWrapper::GetNonManifoldTraversal() const 
{
	return (m_pPDataNormals->GetNonManifoldTraversal() != 0);
}

//Setter
bool VvePDNormalsWrapper::SetInput(vtkObject * pObj, const std::string & sPort)
{
	vtkPolyData * pPD(vtkPolyData::SafeDownCast(pObj));
	if((pPD != 0) && (sPort == "INPUT")) 
	{
#if VTK_MAJOR_VERSION > 5
		m_pPDataNormals->SetInputData(pPD);
#else
		m_pPDataNormals->SetInput(pPD);
#endif
		return true;
	}
	vstr::errp() << "[VvePDNormalsWrapper::SetInput] got no vtkPolyData as input on port " 
			<< sPort << std::endl;
	return false;	
}

bool VvePDNormalsWrapper::SetFeatureAngle(const double & rFeatureAngle)
{
	m_pPDataNormals->SetFeatureAngle((float) rFeatureAngle);
	return true;
}


bool VvePDNormalsWrapper::SetSplitting(const bool & rSplitting)
{
	if(rSplitting) m_pPDataNormals->SetSplitting(1);
	else m_pPDataNormals->SetSplitting(0);

	return true;
}

bool VvePDNormalsWrapper::SetConsistency(const bool & rConsistency)
{
	if(rConsistency) m_pPDataNormals->SetConsistency(1);
	else m_pPDataNormals->SetConsistency(0);

	return true;
}
bool VvePDNormalsWrapper::SetComputePointNormals(const bool & rComputePointNormals)
{
	if(rComputePointNormals) m_pPDataNormals->SetComputePointNormals(1);
	else m_pPDataNormals->SetComputePointNormals(0);

	return true;
}

bool VvePDNormalsWrapper::SetComputeCellNormals(const bool & rComputeCellNormals)
{
	if(rComputeCellNormals) m_pPDataNormals->SetComputeCellNormals(1);
	else m_pPDataNormals->SetComputeCellNormals(0);

	return true;
}

bool VvePDNormalsWrapper::SetFlipNormals(const bool & rFlipNormals)
{
	if(rFlipNormals) m_pPDataNormals->SetFlipNormals(1);
	else m_pPDataNormals->SetFlipNormals(0);

	return true;
}

bool VvePDNormalsWrapper::SetNonManifoldTraversal(const bool & rNonManifoldTraversal)
{
	if(rNonManifoldTraversal) m_pPDataNormals->SetNonManifoldTraversal(1);
	else m_pPDataNormals->SetNonManifoldTraversal(0);

	return true;
}

int VvePDNormalsWrapper::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int iSize(IVistaReflectionable::AddToBaseTypeList(rBtList));
	rBtList.push_back(STR_REFLECTIONABLE_TYPENAME);

	// @todo: Get rid of this cast.
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/* END OF FILE "VvePDNormalsWrapper.cpp"                                      */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

