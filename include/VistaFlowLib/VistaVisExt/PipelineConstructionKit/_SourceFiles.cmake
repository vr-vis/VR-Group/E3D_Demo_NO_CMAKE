

set( RelativeDir "./PipelineConstructionKit" )
set( RelativeSourceGroup "Source Files\\PipelineConstructionKit" )

set( DirFiles
	VveAlgorithmWrapper.h
	VveContourWrapper.cpp
	VveContourWrapper.h
	VveCutterWrapper.cpp
	VveCutterWrapper.h
	VveDecimateWrapper.cpp
	VveDecimateWrapper.h
	VveLineSourceWrapper.cpp
	VveLineSourceWrapper.h
	VvePDNormalsWrapper.cpp
	VvePDNormalsWrapper.h
	VvePlaneWrapper.cpp
	VvePlaneWrapper.h
	VvePointSourceWrapper.cpp
	VvePointSourceWrapper.h
	VveStreamTracerWrapper.cpp
	VveStreamTracerWrapper.h
	VveVtkBuilder.cpp
	VveVtkBuilder.h
	VveVtkPipeline.cpp
	VveVtkPipeline.h
	VveWrapperBase.h
	VveWrapperConvUtils.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

