/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id:


#ifndef _VVESTREAMTRACERWRAPPER_H
#define _VVESTREAMTRACERWRAPPER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveAlgorithmWrapper.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkStreamTracer;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveStreamTracerWrapper :	public IVveAlgorithmWrapper
{
public:

	VveStreamTracerWrapper(void);
	virtual ~VveStreamTracerWrapper(void);
	virtual IVveWrapperBase *CreateInstance() const{
		return new VveStreamTracerWrapper;
	}
	
	// Getter
	virtual std::string GetReflectionableType() const;
	virtual vtkObject * GetOutput(const std::string & sPort) const;
	
	const std::list<double> GetStartPosition() const;
	std::string GetIntegrator() const;
	int GetMaxNumOfSteps() const;
	double GetTerminalSpeed() const;
	std::string GetIntegrationDirection() const;
	
	//NOTE: Deprecated with VTK 5.6
	//std::string GetMaxPropagationUnit() const;
	double GetMaxPropagationValue() const;

	std::string GetIntegrationStepUnit() const;
	double GetInitialIntegrationStepValue() const;

	// more getters for convenience only

	void GetStartPosition(VistaVector3D & rVector) const;
	void GetStartPosition(float fArray[3]) const;
	void GetStartPosition(double dArray[3]) const;

	// Setter
	virtual bool SetInput(vtkObject * pObj, const std::string & sPort);

	bool SetStartPosition(const std::list<double> & rPosition);
	bool SetIntegrator(const std::string & rType);
	bool SetMaxNumOfSteps(const int & rNumOfSteps);
	bool SetTerminalSpeed(const double & rSpeed);
	bool SetIntegrationDirection(const std::string & rType);
	
	//NOTE: Deprecated with VTK 5.6
	//bool SetMaxPropagationUnit (const std::string & rUnit);
	bool SetMaxPropagationValue (const double & rValue);

	bool SetIntegrationStepUnit (const std::string & rUnit);		
	bool SetInitialIntegrationStepValue (const double & rValue);

	// more setters for convenience only
	
	bool SetStartPosition(const VistaVector3D & rVector);
	bool SetStartPosition(float fArray[3]);
	bool SetStartPosition(double dArray[3]);


protected:
	VveStreamTracerWrapper(const VveStreamTracerWrapper &);

	void SetStreamTracer(vtkStreamTracer * pST);
  	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	
	vtkStreamTracer * GetStreamTracer() const;

private:

	vtkStreamTracer * m_pStreamTracer;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
