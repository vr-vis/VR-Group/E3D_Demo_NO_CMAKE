/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id:


#ifndef _VVEPOINTSOURCEWRAPPER_H
#define _VVEPOINTSOURCEWRAPPER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveWrapperBase.h"
#include <VistaBase/VistaVectorMath.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkPointSource;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VvePointSourceWrapper : public IVveWrapperBase
{
public:
	VvePointSourceWrapper(void);
	virtual ~VvePointSourceWrapper(void);
	virtual IVveWrapperBase *CreateInstance() const{
		return new VvePointSourceWrapper;
	}
	

	//Getter
	virtual std::string GetReflectionableType() const;
	virtual vtkObject * GetOutput(const std::string & sPort="OUTPUT") const;

	int GetNumberOfPoints() const;
	const std::list<double> GetCenter() const;
	double GetRadius() const;
	const std::string GetDistribution() const;

	// More getters for convenience only	
	void GetCenter(VistaVector3D & rVector) const;
	void GetCenter(float fArray[3]) const;
	void GetCenter(double dArray[3]) const;

	//Setter
	bool SetNumberOfPoints(const int & rNumberOfPoints);
	bool SetCenter(const std::list<double> & rCenter);
	bool SetRadius(const double & rRadius);
	bool SetDistribution(const std::string & rDistribution);

	// More setters for convenience only
	bool SetCenter(const VistaVector3D & rVector);
	bool SetCenter(float fArray[3]);
	bool SetCenter(double dArray[3]);	
	
protected:
	VvePointSourceWrapper(const VvePointSourceWrapper &);
	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;
	vtkPointSource * m_pPointSource;

private:
	
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
