/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#include "VveVtkPipeline.h"
#include "VveAlgorithmWrapper.h"

#include <vtkPolyData.h>

#include <VistaAspects/VistaAspectsUtils.h>

#include <map>
using std::map;
#include <string>
using std::string;

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

VveVtkPipeline::VveVtkPipeline() : m_Wrappers()
{
}

VveVtkPipeline::VveVtkPipeline(const VveVtkPipeline & rhs)
{
	m_Wrappers = rhs.m_Wrappers;
}

VveVtkPipeline::~VveVtkPipeline()
{
	IVveWrapperBase * pPtr(0);
	
	for(map<std::string, IVveWrapperBase *>::const_iterator cit = m_Wrappers.begin(); 
		cit != m_Wrappers.end(); ++cit)
	{
		pPtr = (*cit).second;
		if(pPtr) 
			delete pPtr;
	}
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

// Getter
IVveWrapperBase * VveVtkPipeline::GetWrapper(const std::string & sName) const
{
	const string strTmpName(VistaAspectsConversionStuff::ConvertToUpper(sName));	
	map<std::string, IVveWrapperBase *>::const_iterator cit(m_Wrappers.find(strTmpName));
	
	if(cit == m_Wrappers.end()) 
		return 0;

	return (*cit).second;
}

const VistaPropertyList VveVtkPipeline::GetParameters(const std::string & sWrapperName) const
{
	VistaPropertyList Properties;
	GetWrapper(sWrapperName)->GetPropertiesByList(Properties);

	return Properties;
}


vtkDataObject * VveVtkPipeline::GetOutput(const std::string & sName, 
										   const std::string & sPort) const
{
	IVveWrapperBase * pWrapper(GetWrapper(sName));	
	return vtkDataObject::SafeDownCast(pWrapper->GetOutput(sPort));	
}

// Operations

bool VveVtkPipeline::AddWrapper(IVveWrapperBase * pWrapper)
{	
	const std::string & sName(pWrapper->GetNameForNameable());

	if(sName.empty())
	{
		vstr::errp() << "[VveVtkPipeline::AddWrapper] empty wrapper name!" << endl << endl;
		return false;
	}
	if(GetWrapper(sName))
	{
		vstr::errp() << "[VveVtkPipeline::AddWrapper] non-unique wrapper name!" << endl << endl;
		return false;
	}
	m_Wrappers[sName] = pWrapper;
	return true;
}

// Setter

bool VveVtkPipeline::SetParameters(const VistaPropertyList & rParameters)
{
#ifdef DEBUG
	vstr::debugi() << "try to set parameters... " << std::endl;
#endif

	for(VistaPropertyList::const_iterator cit = rParameters.begin(); cit != rParameters.end(); ++cit)
	{
		const std::string & sKey((*cit).first);
		if( rParameters.HasSubList( sKey ) == false )
			continue; // @DRCLUSTER@ warning?
		const VistaPropertyList & Parameter = rParameters.GetSubListConstRef(sKey);
		
		IVveWrapperBase * pWrapper(GetWrapper(sKey));
		if(!pWrapper) 
		{
			vstr::errp() << "[VveVtkPipeline::SetParameters] could not set parameters for " << sKey << std::endl;
			return false;
		}	
		if(pWrapper->SetPropertiesByList(Parameter) != Parameter.size())
		{
			vstr::warnp() << "[VveVtkPipeline::SetParameters] unable to set all parameters!" << std::endl;
		}

#ifdef DEBUG
		vstr::debugi() << "set parameters for " << sKey << std::endl<<std::endl;
#endif
	}
	
	return true;
}

bool VveVtkPipeline::SetInput(vtkDataObject * pDataObj, const std::string & sName, 
							   const std::string & sPort)
{
	IVveAlgorithmWrapper * pWrapper
		(dynamic_cast<IVveAlgorithmWrapper *>(GetWrapper(sName)));

	if(!pWrapper)
	{
		vstr::errp() << "[VveVtkPipeline::SetInput]" << sName 
			<< " not found or cant set input!" << std::endl;
		return false;
	}
	return pWrapper->SetInput(pDataObj, sPort);
}

/*============================================================================*/
/* END OF FILE "VveVtkPipeline.cpp"                                           */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

