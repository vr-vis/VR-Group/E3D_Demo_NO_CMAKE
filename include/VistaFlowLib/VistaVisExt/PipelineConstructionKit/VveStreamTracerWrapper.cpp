/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 

#include "VveStreamTracerWrapper.h"
#include "VveWrapperConvUtils.h"
#include <vtkStreamTracer.h>
#include <vtkPolyData.h>
#include <vtkDataSet.h>
#include <assert.h>

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

static const std::string STR_REFLECTIONABLE_TYPENAME("VveStreamTracerWrapper");

//getter stuff
static IVistaPropertyGetFunctor *getFunctors[] = {
	//                     type to be gotten                              property name                                    pointer to getter method
	//                            ^                    reflectionable type       ^     type name of reflectionable as in base type list   ^
	//                            |                            ^                 |                ^                                       |
	new TVistaPropertyGet<const std::list<double>, VveStreamTracerWrapper>("START_POS", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::GetStartPosition),
	new TVistaPropertyGet<std::string, VveStreamTracerWrapper>("INTEGRATOR", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::GetIntegrator),
	new TVistaPropertyGet<int, VveStreamTracerWrapper>("MAX_NUM_STEPS", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::GetMaxNumOfSteps),
	new TVistaPropertyGet<double, VveStreamTracerWrapper>("TERM_SPEED", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::GetTerminalSpeed),
	new TVistaPropertyGet<std::string, VveStreamTracerWrapper>("INT_DIRECTION", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::GetIntegrationDirection),
	//new TVistaPropertyGet<std::string, VveStreamTracerWrapper>("MAX_PROPAGATION_UNIT", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::GetMaxPropagationUnit),
	new TVistaPropertyGet<double, VveStreamTracerWrapper>("MAX_PROPAGATION_VAL", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::GetMaxPropagationValue),
	new TVistaPropertyGet<std::string, VveStreamTracerWrapper>("INIT_INT_STEP_UNIT", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::GetIntegrationStepUnit),
	new TVistaPropertyGet<double, VveStreamTracerWrapper>("INIT_INT_STEP_VAL", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::GetInitialIntegrationStepValue),
	NULL // alway use a terminating NULL here
};

//setter stuff
//NOTE: the setter's return type is assumed to be bool. If you don't want/need this you'll have to implement your own functor!
static IVistaPropertySetFunctor *setFunctors[] = {
	//                                                                 reflectionable type       type name of reflectionable as in base type list
	//                   input type of setter                               ^                   property name              ^                   pointer to setter method         pointer to conversion method
	//                               ^        output type of conversion     |                        ^                     |                               ^                               ^
	//                               |                   ^                  |                        |                     |                               |                               |
	new TVistaPropertySet<const std::list<double> &, std::list<double>, VveStreamTracerWrapper>("START_POS", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::SetStartPosition ),
	new TVistaPropertySet<const std::string &, std::string, VveStreamTracerWrapper>("INTEGRATOR", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::SetIntegrator ),
	new TVistaPropertySet<const int &, int, VveStreamTracerWrapper>("MAX_NUM_STEPS", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::SetMaxNumOfSteps ),
	new TVistaPropertySet<const double &, double, VveStreamTracerWrapper>("TERM_SPEED", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::SetTerminalSpeed ),
	new TVistaPropertySet<const std::string &, std::string, VveStreamTracerWrapper>("INT_DIRECTION", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::SetIntegrationDirection ),
	//new TVistaPropertySet<const std::string &, std::string, VveStreamTracerWrapper>("MAX_PROPAGATION_UNIT", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::SetMaxPropagationUnit ),
	new TVistaPropertySet<const double &, double, VveStreamTracerWrapper>("MAX_PROPAGATION_VAL", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::SetMaxPropagationValue ),
	new TVistaPropertySet<const std::string &, std::string, VveStreamTracerWrapper>("INIT_INT_STEP_UNIT", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::SetIntegrationStepUnit ),
	new TVistaPropertySet<const double &, double, VveStreamTracerWrapper>("INIT_INT_STEP_VAL", STR_REFLECTIONABLE_TYPENAME, &VveStreamTracerWrapper::SetInitialIntegrationStepValue ),
	NULL // alway use a terminating NULL here
};

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

VveStreamTracerWrapper::VveStreamTracerWrapper(void) : 
	m_pStreamTracer (vtkStreamTracer::New())
{
}

VveStreamTracerWrapper::VveStreamTracerWrapper(const VveStreamTracerWrapper &)
{
}

VveStreamTracerWrapper::~VveStreamTracerWrapper(void)
{
	if(m_pStreamTracer) 
		m_pStreamTracer->Delete();	
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

// Getter
std::string VveStreamTracerWrapper::GetReflectionableType() const
{
	return STR_REFLECTIONABLE_TYPENAME;
}

vtkObject * VveStreamTracerWrapper::GetOutput(const std::string & sPort) const
{
	if(sPort != "OUTPUT")
	{
		vstr::errp() << "[VveStreamTracerWrapper::GetOutput] unknown output port "
				<< sPort << std::endl;
		return 0; 
	}
	
	return m_pStreamTracer->GetOutput();
}

const std::list<double> VveStreamTracerWrapper::GetStartPosition() const 
{
	std::list<double> StartPos;
	VveWrapperConvUtils::ConvertArrayToList(m_pStreamTracer->GetStartPosition(), StartPos);

	return StartPos;
}

std::string VveStreamTracerWrapper::GetIntegrator() const 
{
	std::string sIntegrator;

	switch(m_pStreamTracer->GetIntegratorType())
	{
	case vtkStreamTracer::RUNGE_KUTTA2:
		sIntegrator="RK2";
		break;
		
	case vtkStreamTracer::RUNGE_KUTTA4:
		sIntegrator="RK4";
		break;

	case vtkStreamTracer::RUNGE_KUTTA45:
		sIntegrator="RK45";
		break;
	}

	return sIntegrator;
}

int VveStreamTracerWrapper::GetMaxNumOfSteps() const 
{
	return m_pStreamTracer->GetMaximumNumberOfSteps();
}

double VveStreamTracerWrapper::GetTerminalSpeed() const 
{
	return ((double) m_pStreamTracer->GetTerminalSpeed());
}

std::string VveStreamTracerWrapper::GetIntegrationDirection() const 
{
	std::string sIntDirection;

	switch(m_pStreamTracer->GetIntegrationDirection())
	{
	case vtkStreamTracer::FORWARD:
		sIntDirection="FORWARD";
		break;
		
	case vtkStreamTracer::BACKWARD:
		sIntDirection="BACKWARD";
		break;

	case vtkStreamTracer::BOTH:
		sIntDirection="BOTH";
		break;
	}

	return sIntDirection;
}
/* //NOTE : DEPRECATED WITH VTK 5.6
std::string VveStreamTracerWrapper::GetMaxPropagationUnit() const
{
	std::string sMaxPropagationUnit;

	switch(m_pStreamTracer->GetMaximumPropagationUnit())
	{
	case vtkStreamTracer::TIME_UNIT:
		sMaxPropagationUnit="TIME_UNIT";
		break;

	case vtkStreamTracer::LENGTH_UNIT:
		sMaxPropagationUnit="LENGTH_UNIT";
		break;

	case vtkStreamTracer::CELL_LENGTH_UNIT:
		sMaxPropagationUnit="CELL_LENGTH_UNIT";
		break;
	}

	return sMaxPropagationUnit;
}
*/

double VveStreamTracerWrapper::GetMaxPropagationValue() const
{
	return ((double) m_pStreamTracer->GetMaximumPropagation());
}

std::string VveStreamTracerWrapper::GetIntegrationStepUnit() const
{
	std::string sUnit("");
	switch(m_pStreamTracer->GetIntegrationStepUnit())
	{
	case vtkStreamTracer::LENGTH_UNIT:
		sUnit = "LENGTH_UNIT";
		break;
	case vtkStreamTracer::CELL_LENGTH_UNIT:
		sUnit = "CELL_LENGTH_UNIT";
		break;
	}
	return sUnit;
}

double VveStreamTracerWrapper::GetInitialIntegrationStepValue() const
{
	return ((double) m_pStreamTracer->GetInitialIntegrationStep());
}

vtkStreamTracer * VveStreamTracerWrapper::GetStreamTracer() const
{
	return m_pStreamTracer;
}

// More getters for convenience only

void VveStreamTracerWrapper::GetStartPosition(VistaVector3D & rVector) const
{
	std::list<double> Pos(GetStartPosition());
	VveWrapperConvUtils::ConvertListToVec3D(Pos, rVector);	
}

void VveStreamTracerWrapper::GetStartPosition(float fArray[3]) const
{
	std::list<double> Pos(GetStartPosition());
	VveWrapperConvUtils::ConvertListToArray(Pos, fArray);	
}

void VveStreamTracerWrapper::GetStartPosition(double dArray[3]) const
{
	std::list<double> Pos(GetStartPosition());
	VveWrapperConvUtils::ConvertListToArray(Pos, dArray);	
}

// Setter
bool VveStreamTracerWrapper::SetInput(vtkObject * pObj, const std::string & sPort)
{
	if(sPort == "INPUT") 
	{
		vtkDataSet * pDS(vtkDataSet::SafeDownCast(pObj));
		
		if(pDS != 0) 
		{
#if VTK_MAJOR_VERSION > 5
			m_pStreamTracer->SetInputData(pDS);
#else
			m_pStreamTracer->SetInput(pDS);
#endif
			return true;
		}
		vstr::errp() << "[VveStreamTracerWrapper::SetInput] got no vtkDataSet as input on port " 
			<< sPort << std::endl;		
	}

	else if(sPort == "SEED") 
	{
		vtkDataSet * pSource(vtkDataSet::SafeDownCast(pObj));		
		if(pSource != 0) 
		{
#if VTK_MAJOR_VERSION > 5
			m_pStreamTracer->SetSourceData(pSource);
#else
			m_pStreamTracer->SetSource(pSource);
#endif
			return true;
		}
		vstr::errp() << "[VveStreamTracerWrapper::SetInput] got no vtkDataSet as input on port " 
			<< sPort << std::endl;
		
	}

	return false;
}

bool VveStreamTracerWrapper::SetStartPosition (const std::list<double> & rPosition)
{
	assert(rPosition.size() == 3);
	double fPoint[3];
	VveWrapperConvUtils::ConvertListToArray(rPosition, fPoint);
	m_pStreamTracer->SetStartPosition (fPoint);
	return true;
}

bool VveStreamTracerWrapper::SetIntegrator (const std::string & rType)
{	
	if(rType == "RK2") {m_pStreamTracer->SetIntegratorType(vtkStreamTracer::RUNGE_KUTTA2);}
	else if(rType == "RK4") {m_pStreamTracer->SetIntegratorType(vtkStreamTracer::RUNGE_KUTTA4);}
	else if(rType == "RK45") {m_pStreamTracer->SetIntegratorType(vtkStreamTracer::RUNGE_KUTTA45);}
	else {/* error */ return false;}

	return true;
}

bool VveStreamTracerWrapper::SetMaxNumOfSteps (const int & rNumOfSteps)
{
	m_pStreamTracer->SetMaximumNumberOfSteps(rNumOfSteps);
	return true;
}

bool VveStreamTracerWrapper::SetTerminalSpeed (const double & rSpeed)
{
	m_pStreamTracer->SetTerminalSpeed((float) rSpeed);
	return true;
}

bool VveStreamTracerWrapper::SetIntegrationDirection (const std::string & rType)
{
	if(rType == "FORWARD") {m_pStreamTracer->SetIntegrationDirectionToForward();}
	else if(rType == "BACKWARD") {m_pStreamTracer->SetIntegrationDirectionToBackward();}
	else if(rType == "BOTH") {m_pStreamTracer->SetIntegrationDirectionToBoth();}
	else {/* error */ return false;}
	return true;
}
/* //NOTE : Interface deprecated with VTK 5.6
bool VveStreamTracerWrapper::SetMaxPropagationUnit (const std::string & rUnit) 
{
	if(rUnit == "TIME_UNIT") {m_pStreamTracer->SetMaximumPropagationUnit(vtkStreamTracer::TIME_UNIT);}
	else if(rUnit == "LENGTH_UNIT") {m_pStreamTracer->SetMaximumPropagationUnit(vtkStreamTracer::LENGTH_UNIT);}
	else if(rUnit == "CELL_LENGTH_UNIT") {m_pStreamTracer->SetMaximumPropagationUnit(vtkStreamTracer::CELL_LENGTH_UNIT);}
	else {return false;}

	return true;
}
*/

bool VveStreamTracerWrapper::SetMaxPropagationValue (const double & rValue) 
{
	m_pStreamTracer->SetMaximumPropagation((float) rValue);
	
	return true;
}
bool VveStreamTracerWrapper::SetIntegrationStepUnit(const std::string & rUnit) 
{
	//if(rUnit == "TIME_UNIT") {m_pStreamTracer->SetIntegrationStepUnit(vtkStreamTracer::TIME_UNIT);}
	if(rUnit == "LENGTH_UNIT") 
	{
		m_pStreamTracer->SetIntegrationStepUnit(vtkStreamTracer::LENGTH_UNIT);
	}
	else if(rUnit == "CELL_LENGTH_UNIT") 
	{
		m_pStreamTracer->SetIntegrationStepUnit(vtkStreamTracer::CELL_LENGTH_UNIT);
	}
	else 
	{
		return false;
	}
	return true;
}
bool VveStreamTracerWrapper::SetInitialIntegrationStepValue (const double & rValue) 
{
	m_pStreamTracer->SetInitialIntegrationStep((float) rValue);
	
	return true;
}

// more setters for convenience only

bool VveStreamTracerWrapper::SetStartPosition(const VistaVector3D & rVector)
{
	std::list<double> Pos;
	VveWrapperConvUtils::ConvertVec3DToList(rVector, Pos);
	return SetStartPosition(Pos);
}

bool VveStreamTracerWrapper::SetStartPosition(float fArray[3])
{
	std::list<double> Pos;
	VveWrapperConvUtils::ConvertArrayToList(fArray, Pos);
	return SetStartPosition(Pos);
}

bool VveStreamTracerWrapper::SetStartPosition(double dArray[3])
{
	std::list<double> Pos;
	VveWrapperConvUtils::ConvertArrayToList(dArray, Pos);
	return SetStartPosition(Pos);
}

void VveStreamTracerWrapper::SetStreamTracer(vtkStreamTracer * pST)
{
	m_pStreamTracer = pST;
}

int VveStreamTracerWrapper::AddToBaseTypeList(std::list<std::string> & rBtList) const
{
	IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(STR_REFLECTIONABLE_TYPENAME);
	
	// @todo: Get rid of this cast.
	return static_cast<int>(rBtList.size());
}

/*============================================================================*/
/* END OF FILE "VveStreamTracerWrapper.cpp"                                   */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

