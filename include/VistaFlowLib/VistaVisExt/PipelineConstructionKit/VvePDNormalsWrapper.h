/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id:


#ifndef _VVEPDNORMALSWRAPPER_H
#define _VVEPDNORMALSWRAPPER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveAlgorithmWrapper.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkPolyDataNormals;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VvePDNormalsWrapper : public IVveAlgorithmWrapper
{
public:
	VvePDNormalsWrapper();
	virtual ~VvePDNormalsWrapper();
	virtual IVveWrapperBase *CreateInstance() const{
		return new VvePDNormalsWrapper;
	}
	

	// Getter
	virtual std::string GetReflectionableType() const;

	virtual vtkObject * GetOutput(const std::string & sPort = "OUTPUT") const;
	
	double GetFeatureAngle() const;
	bool GetSplitting() const;
	bool GetConsistency() const;
	bool GetComputePointNormals() const;
	bool GetComputeCellNormals() const;
	bool GetFlipNormals() const;
	bool GetNonManifoldTraversal() const;

	//Setter

	virtual bool SetInput(vtkObject * pObj, const std::string & sPort = "INPUT");

	bool SetFeatureAngle(const double & rFeatureAngle);

	bool SetSplitting(const bool & rSplitting);
	bool SetConsistency(const bool & rConsistency);
	bool SetComputePointNormals(const bool & rComputePointNormals);
	bool SetComputeCellNormals(const bool & rComputeCellNormals);
	bool SetFlipNormals(const bool & rFlipNormals);
	bool SetNonManifoldTraversal(const bool & rNonManifoldTraversal);
	

	
protected:
	VvePDNormalsWrapper(const VvePDNormalsWrapper &);

	virtual int AddToBaseTypeList(std::list<std::string> &rBtList) const;

private:

	vtkPolyDataNormals * m_pPDataNormals;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
