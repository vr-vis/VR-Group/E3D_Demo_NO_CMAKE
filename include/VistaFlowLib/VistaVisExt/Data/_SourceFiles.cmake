

set( RelativeDir "./Data" )
set( RelativeSourceGroup "Source Files\\Data" )
set( SubDirs DatasetDescription )

set( DirFiles
	VveCartesianGrid.cpp
	VveCartesianGrid.h
	VveContinuousData.cpp
	VveContinuousData.h
	VveContinuousDataTyped.cpp
	VveContinuousDataTyped.h
	VveDBDataContainer.cpp
	VveDBDataContainer.h
	VveDataContainer.cpp
	VveDataContainer.h
	VveDataItem.cpp
	VveDataItem.h
	VveDiscreteData.cpp
	VveDiscreteData.h
	VveDiscreteDataTyped.cpp
	VveDiscreteDataTyped.h
	VveIntervalTree.cpp
	VveIntervalTree.h
	VveKdTree.cpp
	VveKdTree.h
	VveOctree.cpp
	VveOctree.h
	VveOctreeDFSIterator.cpp
	VveOctreeDFSIterator.h
	VveParticleDataArray.cpp
	VveParticleDataArray.h
	VveParticleTrajectory.cpp
	VveParticleTrajectory.h
	VveParticlePopulation.cpp
	VveParticlePopulation.h
	VvePointSetOctreeBuilder.cpp
	VvePointSetOctreeBuilder.h
	VvePropertyContainer.cpp
	VvePropertyContainer.h
	VvePropertyList.cpp
	VvePropertyList.h
	VveSparseDataBlocks.h
	VveStatsSet.cpp
	VveStatsSet.h
	VveTetGrid.cpp
	VveTetGrid.h
	VveTimeHistogram.cpp
	VveTimeHistogram.h
	VveTimeSeries.cpp
	VveTimeSeries.h
	VveUnsteadyData.cpp
	VveUnsteadyData.h
	VveUnsteadyTetGrid.cpp
	VveUnsteadyTetGrid.h
	VveVtkData.cpp
	VveVtkData.h
	_SourceFiles.cmake
)

set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()
