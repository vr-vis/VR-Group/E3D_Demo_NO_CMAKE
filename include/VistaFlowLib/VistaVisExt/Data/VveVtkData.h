/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VveVtkData               RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaFlowLib                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VveVtkDataContainer                                              */
/*        - VveVtkData                                                       */
/*                                                                            */
/*============================================================================*/
#ifndef _VVEVTKDATA_H
#define _VVEVTKDATA_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveDataItem.h"
#include "VveDiscreteDataTyped.h"

/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkPolyData;
class vtkStructuredPoints;
class vtkRectilinearGrid;
class vtkDataSet;
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/**
 * Here, we just typedef certain types of VveDataContainer and VveContinuousDataTyped
 * for easier declaration of unsteady vtk data objects.
 */
typedef VveDataItem<vtkPolyData> VveVtkPolyDataItem;
typedef VveDiscreteDataTyped<vtkPolyData> VveUnsteadyVtkPolyData;

typedef VveDataItem<vtkStructuredPoints> VveVtkStructuredPointsItem;
typedef VveDiscreteDataTyped<vtkStructuredPoints> VveUnsteadyVtkStructuredPoints;

typedef VveDataItem<vtkRectilinearGrid> VveVtkRectilinearGridItem;
typedef VveDiscreteDataTyped<vtkRectilinearGrid> VveUnsteadyVtkRectilinearGrid;

typedef VveDataItem<vtkDataSet> VveVtkDataSetItem;
typedef VveDiscreteDataTyped<vtkDataSet> VveUnsteadyVtkDataSet;
#endif // _VVEVTKDATA_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

