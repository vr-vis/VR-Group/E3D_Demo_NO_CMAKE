/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVECONTINUOUSDATA_H
#define _VVECONTINUOUSDATA_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
//ViSTA includes

// ViSTA FlowLib includes
#include "VveUnsteadyData.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVveDataItem;
class VveTimeMapper;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*/
class VISTAVISEXTAPI VveContinuousData : public VveUnsteadyData
{
public:
	VveContinuousData(VveTimeMapper *pMapper);
    virtual ~VveContinuousData();

    IVveDataItem *GetData() const;
    void          SetData(IVveDataItem *pData);

protected:
    IVveDataItem *m_pData;
};

/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData                                                     */
/*                                                                            */
/*============================================================================*/
inline IVveDataItem *VveContinuousData::GetData() const
{
    return m_pData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetData                                                     */
/*                                                                            */
/*============================================================================*/
inline void VveContinuousData::SetData(IVveDataItem *pData)
{
    m_pData = pData;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VVECONTINUOUSDATA_H

