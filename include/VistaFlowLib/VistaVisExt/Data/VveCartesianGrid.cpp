/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// VistaFlowLib includes
#include "VveCartesianGrid.h"
#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaAspects/VistaPropertyAwareable.h>
#include <VistaBase/Half/VistaHalf.h>
#include <VistaBase/VistaStreamUtils.h>

// reader stuff...
#include <vtkImageData.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#define MAX_COMPONENTS 4

#define INDEX(x, y, z, stride) (x*stride[0] + y*stride[1] + z*stride[2])

#define INTERPOLATE( target, t1, t2, dAlpha, components ) \
	for (register int _i=0; _i<components; ++_i) \
		(target)[_i] = Type((t1)[_i] + dAlpha * ((t2)[_i]-(t1)[_i]));


template<class T> int __RLEEncode(T* data, int iNumEntries, IVistaSerializer &oSer);
template<class TAccess, class TData> int __RLEDecode(TAccess &oDeSer, TData* tgt, int iNumEntries);

//helper classes for type safe access to data in the serializer
class __CFloatAccess{
public:
	__CFloatAccess(IVistaDeSerializer &oDeSer) : m_rDeSer(oDeSer) {}
	virtual ~__CFloatAccess(){};

	inline int GetNextCount(int &iCount)
	{
		return m_rDeSer.ReadInt32(iCount);
	}
	inline int GetNextElemet(float &f)
	{
		return m_rDeSer.ReadFloat32(f);
	}
private:
	IVistaDeSerializer &m_rDeSer;
};

class __CHalfAccess{
public:
	__CHalfAccess(IVistaDeSerializer &oDeSer) : m_rDeSer(oDeSer) {}
	virtual ~__CHalfAccess(){};

	inline int GetNextCount(int &iCount)
	{
		return m_rDeSer.ReadInt32(iCount);
	}
	inline int GetNextElemet(VistaHalf &h)
	{
		unsigned short s;
		int iRet = m_rDeSer.ReadShort16(s);
		h.setBits(s);
		return iRet;
	}
private:
	IVistaDeSerializer &m_rDeSer;
};

class __CCharAccess{
public:
	__CCharAccess(IVistaDeSerializer &oDeSer) : m_rDeSer(oDeSer) {}
	virtual ~__CCharAccess(){};

	inline int GetNextCount(int &iCount)
	{
		return m_rDeSer.ReadInt32(iCount);
	}
	inline int GetNextElemet(unsigned char &c)
	{
		return m_rDeSer.ReadRawBuffer(&c, 1);
	}
private:
	IVistaDeSerializer &m_rDeSer;
};

class __CShortAccess{
public:
	__CShortAccess(IVistaDeSerializer &oDeSer) : m_rDeSer(oDeSer) {}
	virtual ~__CShortAccess(){};

	inline int GetNextCount(int &iCount)
	{
		return m_rDeSer.ReadInt32(iCount);
	}
	inline int GetNextElemet(unsigned short &us)
	{
		int iRet = m_rDeSer.ReadShort16(us);
    return iRet;
	}
private:
	IVistaDeSerializer &m_rDeSer;
};

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VveCartesianGrid::VveCartesianGrid(int iComponents /* = 4 */,
									 int iDataType /* = DT_FLOAT */)
: m_pMetaData(new VistaPropertyList),
  m_iComponents(iComponents),
  m_iDataType(iDataType),
  m_iByteCount(0),
  m_bValid(false)
{
	memset(m_aDimensions, 0, 3*sizeof(int));
	memset(m_aDataDimensions, 0, 3*sizeof(int));
	memset(m_aStride, 0, 3*sizeof(int));
	m_oData.pv = NULL;
}

VveCartesianGrid::VveCartesianGrid(int iDimX, int iDimY, int iDimZ,
									 int iComponents, int iDataType /*= DT_FLOAT*/,
									 void *pData /* = NULL */ )
: m_pMetaData(new VistaPropertyList),
  m_iComponents(iComponents),
  m_iDataType(iDataType),
  m_iByteCount(0),
  m_bValid(false)
{
	m_oData.pv = NULL;

	m_aDimensions[0] = m_aDataDimensions[0] = iDimX;
	m_aDimensions[1] = m_aDataDimensions[1] = iDimY;
	m_aDimensions[2] = m_aDataDimensions[2] = iDimZ;

	if (AllocateMemory())
	{
		m_aStride[0] = iComponents;
		m_aStride[1] = m_aStride[0] * m_aDimensions[0];
		m_aStride[2] = m_aStride[1] * m_aDimensions[1];

		if (pData)
			memcpy(m_oData.pv, pData, m_iByteCount);
	}
	else
	{
		memset(m_aDimensions, 0, 3*sizeof(int));
		memset(m_aDataDimensions, 0, 3*sizeof(int));
		memset(m_aStride, 0, 3*sizeof(int));
	}
}

VveCartesianGrid::~VveCartesianGrid()
{
	delete m_pMetaData;
	m_pMetaData = NULL;
	switch (m_iDataType)
	{
	case DT_FLOAT:
		delete [] m_oData.pf;
		break;
	case DT_HALF:
		delete [] m_oData.ph;
		break;
	case DT_UNSIGNED_CHAR:
		delete [] m_oData.puc;
		break;
	case DT_UNSIGNED_SHORT:
		delete [] m_oData.pus;
		break;
	}
	m_oData.pv = NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Reset                                                       */
/*                                                                            */
/*============================================================================*/
bool VveCartesianGrid::Reset(int iDimX, int iDimY, int iDimZ, 
							  int iComponents, int iDataType /*= DT_FLOAT*/,
							  void *pData /* = NULL */)
{
	m_aDimensions[0] = m_aDataDimensions[0] = iDimX;
	m_aDimensions[1] = m_aDataDimensions[1] = iDimY;
	m_aDimensions[2] = m_aDataDimensions[2] = iDimZ;
	m_iComponents = iComponents;
	m_iDataType = iDataType;
	m_bValid = false;

	if (!AllocateMemory())
	{
		memset(m_aDimensions, 0, 3*sizeof(int));
		memset(m_aDataDimensions, 0, 3*sizeof(int));
		memset(m_aStride, 0, 3*sizeof(int));
		return false;
	}

	m_aStride[0] = iComponents;
	m_aStride[1] = m_aStride[0] * m_aDimensions[0];
	m_aStride[2] = m_aStride[1] * m_aDimensions[1];

	if (pData)
		memcpy(m_oData.pv, pData, m_iByteCount);

	m_pMetaData->clear();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDimensions                                               */
/*                                                                            */
/*============================================================================*/
void VveCartesianGrid::GetDimensions(int &iDimX, int &iDimY, int &iDimZ) const
{
	iDimX = m_aDimensions[0];
	iDimY = m_aDimensions[1];
	iDimZ = m_aDimensions[2];
}

void VveCartesianGrid::GetDimensions(int aDims[3]) const
{
	memcpy(aDims, m_aDimensions, 3*sizeof(int));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetComponents                                               */
/*                                                                            */
/*============================================================================*/
int VveCartesianGrid::GetComponents() const
{
	return m_iComponents;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDataType                                                 */
/*                                                                            */
/*============================================================================*/
int VveCartesianGrid::GetDataType() const
{
	return m_iDataType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDataByteCount                                            */
/*                                                                            */
/*============================================================================*/
int VveCartesianGrid::GetDataByteCount() const
{
	return m_iByteCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetMetaData                                                 */
/*                                                                            */
/*============================================================================*/
VistaPropertyList *VveCartesianGrid::GetMetaData() const
{
	return m_pMetaData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetValid                                                */
/*                                                                            */
/*============================================================================*/
void VveCartesianGrid::SetValid(bool bValid)
{
	m_bValid = bValid;
}

bool VveCartesianGrid::GetValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDataDimensions                                       */
/*                                                                            */
/*============================================================================*/
void VveCartesianGrid::SetDataDimensions(int iDimX, int iDimY, int iDimZ)
{
	m_aDataDimensions[0] = iDimX;
	m_aDataDimensions[1] = iDimY;
	m_aDataDimensions[2] = iDimZ;
}

void VveCartesianGrid::SetDataDimensions(int aDims[3])
{
	memcpy(m_aDataDimensions, aDims, 3*sizeof(int));
}

void VveCartesianGrid::GetDataDimensions(int &iDimX, int &iDimY, int &iDimZ) const
{
	iDimX = m_aDataDimensions[0];
	iDimY = m_aDataDimensions[1];
	iDimZ = m_aDataDimensions[2];
}

void VveCartesianGrid::GetDataDimensions(int aDims[3]) const
{
	memcpy(aDims, m_aDataDimensions, 3*sizeof(int));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetBounds                                               */
/*                                                                            */
/*============================================================================*/
void VveCartesianGrid::SetBounds(const VistaVector3D &v3Min, 
								  const VistaVector3D &v3Max)
{
	m_v3BoundsMin = v3Min;
	m_v3BoundsMax = v3Max;
	m_v3BoundsDelta = m_v3BoundsMax - m_v3BoundsMin;
}

void VveCartesianGrid::SetBounds(float fBounds[6])
{
	m_v3BoundsMin[0] = fBounds[0];
	m_v3BoundsMin[1] = fBounds[2];
	m_v3BoundsMin[2] = fBounds[4];
	m_v3BoundsMax[0] = fBounds[1];
	m_v3BoundsMax[1] = fBounds[3];
	m_v3BoundsMax[2] = fBounds[5];
	m_v3BoundsDelta = m_v3BoundsMax - m_v3BoundsMin;
}

void VveCartesianGrid::SetBounds(double dBounds[6])
{
	float fB[6] = {	dBounds[0], dBounds[1], dBounds[2], 
					dBounds[3], dBounds[4], dBounds[5]};
	this->SetBounds(fB);
}

void VveCartesianGrid::GetBounds(VistaVector3D &v3Min,
								  VistaVector3D &v3Max) const
{
	v3Min = m_v3BoundsMin;
	v3Max = m_v3BoundsMax;
}

void VveCartesianGrid::GetBounds(float fBounds[6]) const
{
	fBounds[0] = m_v3BoundsMin[0];
	fBounds[1] = m_v3BoundsMax[0];
	fBounds[2] = m_v3BoundsMin[1];
	fBounds[3] = m_v3BoundsMax[1];
	fBounds[4] = m_v3BoundsMin[2];
	fBounds[5] = m_v3BoundsMax[2];
}

void VveCartesianGrid::GetBounds(double dBounds[6]) const
{
	float fB[6];
	this->GetBounds(fB);
	dBounds[0] = fB[0]; dBounds[1] = fB[1]; dBounds[2] = fB[2];
	dBounds[3] = fB[3]; dBounds[4] = fB[4]; dBounds[5] = fB[5];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSpacing                                                  */
/*                                                                            */
/*============================================================================*/
void VveCartesianGrid::GetSpacing(float fSpacing[3]) const
{
	fSpacing[0] = m_v3BoundsDelta[0] / (float)(m_aDataDimensions[0]-1);
	fSpacing[1] = m_v3BoundsDelta[1] / (float)(m_aDataDimensions[1]-1);
	fSpacing[2] = m_v3BoundsDelta[2] / (float)(m_aDataDimensions[2]-1);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetScalarRange                                          */
/*                                                                            */
/*============================================================================*/
void VveCartesianGrid::SetScalarRange(float aRange[2])
{
	memcpy(m_aRange, aRange, 2*sizeof(float));
}

void VveCartesianGrid::GetScalarRange(float aRange[2]) const
{
	memcpy(aRange, m_aRange, 2*sizeof(float));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetData*                                                */
/*                                                                            */
/*============================================================================*/
void VveCartesianGrid::SetData(void *pData)
{
	memcpy(m_oData.pv, pData, m_iByteCount);
}

void VveCartesianGrid::SetData(int x, int y, int z,
								float *pData)
{
	memcpy(&m_oData.pf[INDEX(x, y, z, m_aStride)], pData, m_iComponents * sizeof(float));
}

void VveCartesianGrid::SetData(int x, int y, int z,
								VistaHalf *pData)
{
	memcpy(&m_oData.ph[INDEX(x, y, z, m_aStride)], pData, m_iComponents * sizeof(VistaHalf));
}

void VveCartesianGrid::SetData(int x, int y, int z,
								unsigned char *pData)
{
	memcpy(&m_oData.puc[INDEX(x, y, z, m_aStride)], pData, m_iComponents*sizeof(unsigned char));
}

void VveCartesianGrid::SetData(int x, int y, int z,
								unsigned short *pData)
{
	memcpy(&m_oData.pus[INDEX(x, y, z, m_aStride)], pData, m_iComponents*sizeof(unsigned short));
}

void *VveCartesianGrid::GetData() const
{
	return m_oData.pv;
}

float *VveCartesianGrid::GetDataAsFloat() const
{
	return m_oData.pf;
}

VistaHalf *VveCartesianGrid::GetDataAsHalf() const
{
	return m_oData.ph;
}

unsigned char *VveCartesianGrid::GetDataAsUnsignedChar() const
{
	return m_oData.puc;
}
	
unsigned short *VveCartesianGrid::GetDataAsUnsignedShort() const
{
	return m_oData.pus;
}

void VveCartesianGrid::GetData(int x, int y, int z, float *pData) const
{
	memcpy(pData, &m_oData.pf[INDEX(x, y, z, m_aStride)], m_iComponents * sizeof(float));
}

void VveCartesianGrid::GetData(int x, int y, int z, VistaHalf *pData) const
{
	memcpy(pData, &m_oData.ph[INDEX(x, y, z, m_aStride)], m_iComponents * sizeof(VistaHalf));
}

void VveCartesianGrid::GetData(int x, int y, int z, unsigned char *pData) const
{
	memcpy(pData, &m_oData.puc[INDEX(x, y, z, m_aStride)], m_iComponents * sizeof(unsigned char));
}

void VveCartesianGrid::GetData(int x, int y, int z, unsigned short *pData) const
{
	memcpy(pData, &m_oData.pus[INDEX(x, y, z, m_aStride)], m_iComponents * sizeof(unsigned short));
}

template <class Type>
void _VCGRetrieveDataNormalized(const float aPosIn[3], 
								const int aDims[3],
								const int aStride[3],
								int iComponents,
								Type *pSourceData,
								Type *pTargetData)
{
	float aPos[3];
	memcpy(aPos, aPosIn, 3*sizeof(float));

	if (aPos[0] < 0)
		aPos[0] = 0;
	else if (aPos[0] > 1)
		aPos[0] = 1;
	if (aPos[1] < 0)
		aPos[1] = 0;
	else if (aPos[1] > 1)
		aPos[1] = 1;
	if (aPos[2] < 0)
		aPos[2] = 0;
	else if (aPos[2] > 1)
		aPos[2] = 1;

	float fTemp = aPos[0] * (aDims[0]-1);
	int x1 = (int) floor(fTemp);
	int x2 = (int) ceil(fTemp);
	float fAlphaX = fTemp - x1;

	fTemp = aPos[1] * (aDims[1]-1);
	int y1 = (int) floor(fTemp);
	int y2 = (int) ceil(fTemp);
	float fAlphaY = fTemp - y1;

	fTemp = aPos[2] * (aDims[2]-1);
	int z1 = (int) floor(fTemp);
	int z2 = (int) ceil(fTemp);
	float fAlphaZ = fTemp - z1;

	// tri-linear interpolation
	Type aTemp1[MAX_COMPONENTS];
	Type aTemp2[MAX_COMPONENTS];
	Type aTemp3[MAX_COMPONENTS];

	int iByteCount = iComponents * sizeof(Type);

	Type aTempOp1[MAX_COMPONENTS];
	Type aTempOp2[MAX_COMPONENTS];
	memcpy(aTempOp1, &pSourceData[INDEX(x1, y1, z1, aStride)], iByteCount);
	memcpy(aTempOp2, &pSourceData[INDEX(x2, y1, z1, aStride)], iByteCount);

	Type *p1 = &pSourceData[INDEX(x1, y1, z1, aStride)];
	Type *p2 = &pSourceData[INDEX(x2, y1, z1, aStride)];

	INTERPOLATE(aTemp1, &pSourceData[INDEX(x1, y1, z1, aStride)], 
		&pSourceData[INDEX(x2, y1, z1, aStride)], 
		fAlphaX, iComponents);

	INTERPOLATE(aTemp2, &pSourceData[INDEX(x1, y2, z1, aStride)], 
		&pSourceData[INDEX(x2, y2, z1, aStride)], 
		fAlphaX, iComponents);

	INTERPOLATE(aTemp1, aTemp1, aTemp2, fAlphaY, iComponents);

	INTERPOLATE(aTemp2, &pSourceData[INDEX(x1, y1, z2, aStride)],
		&pSourceData[INDEX(x2, y1, z2, aStride)], 
		fAlphaX, iComponents);

	INTERPOLATE(aTemp3, &pSourceData[INDEX(x1, y2, z2, aStride)],
		&pSourceData[INDEX(x2, y2, z2, aStride)], 
		fAlphaX, iComponents);

	INTERPOLATE(aTemp2, aTemp2, aTemp3, fAlphaY, iComponents);

	INTERPOLATE(pTargetData, aTemp1, aTemp2, fAlphaZ, iComponents);
}

void VveCartesianGrid::GetData(float x, float y, float z, float *pData) const
{
	float aPos[3];

	// convert coordinates given in visualization space into local space
	aPos[0] = (x - m_v3BoundsMin[0]) / m_v3BoundsDelta[0];
	aPos[1] = (y - m_v3BoundsMin[1]) / m_v3BoundsDelta[1];
	aPos[2] = (z - m_v3BoundsMin[2]) / m_v3BoundsDelta[2];

	_VCGRetrieveDataNormalized<float>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.pf, pData);
}

void VveCartesianGrid::GetData(const float aPos[3], float *pData) const
{
	float aPosLocal[3];

	// convert coordinates given in visualization space into local space
	aPosLocal[0] = (aPos[0] - m_v3BoundsMin[0]) / m_v3BoundsDelta[0];
	aPosLocal[1] = (aPos[1] - m_v3BoundsMin[1]) / m_v3BoundsDelta[1];
	aPosLocal[2] = (aPos[2] - m_v3BoundsMin[2]) / m_v3BoundsDelta[2];

	_VCGRetrieveDataNormalized<float>(aPosLocal, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.pf, pData);
}

void VveCartesianGrid::GetDataNormalized(float x, float y, float z, float *pData) const
{
	float aPos[3] = { x, y, z };

	_VCGRetrieveDataNormalized<float>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.pf, pData);
}

void VveCartesianGrid::GetDataNormalized(const float aPos[3], float *pData) const
{
	_VCGRetrieveDataNormalized<float>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.pf, pData);
}

void VveCartesianGrid::GetData(float x, float y, float z, VistaHalf *pData) const
{
	float aPos[3];

	// convert coordinates given in visualization space into local space
	aPos[0] = (x - m_v3BoundsMin[0]) / m_v3BoundsDelta[0];
	aPos[1] = (y - m_v3BoundsMin[1]) / m_v3BoundsDelta[1];
	aPos[2] = (z - m_v3BoundsMin[2]) / m_v3BoundsDelta[2];

	_VCGRetrieveDataNormalized<VistaHalf>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.ph, pData);
}

void VveCartesianGrid::GetData(const float aPos[3], VistaHalf *pData) const
{
	float aPosLocal[3];

	// convert coordinates given in visualization space into local space
	aPosLocal[0] = (aPos[0] - m_v3BoundsMin[0]) / m_v3BoundsDelta[0];
	aPosLocal[1] = (aPos[1] - m_v3BoundsMin[1]) / m_v3BoundsDelta[1];
	aPosLocal[2] = (aPos[2] - m_v3BoundsMin[2]) / m_v3BoundsDelta[2];

	_VCGRetrieveDataNormalized<VistaHalf>(aPosLocal, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.ph, pData);
}

void VveCartesianGrid::GetDataNormalized(float x, float y, float z, VistaHalf *pData) const
{
	float aPos[3] = { x, y, z };

	_VCGRetrieveDataNormalized<VistaHalf>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.ph, pData);
}

void VveCartesianGrid::GetDataNormalized(const float aPos[3], VistaHalf *pData) const
{
	_VCGRetrieveDataNormalized<VistaHalf>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.ph, pData);
}

void VveCartesianGrid::GetData(float x, float y, float z, unsigned char *pData) const
{
	float aPos[3];

	// convert coordinates given in visualization space into local space
	aPos[0] = (x - m_v3BoundsMin[0]) / m_v3BoundsDelta[0];
	aPos[1] = (y - m_v3BoundsMin[1]) / m_v3BoundsDelta[1];
	aPos[2] = (z - m_v3BoundsMin[2]) / m_v3BoundsDelta[2];

	_VCGRetrieveDataNormalized<unsigned char>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.puc, pData);
}

void VveCartesianGrid::GetData(const float aPos[3], unsigned char *pData) const
{
	float aPosLocal[3];

	// convert coordinates given in visualization space into local space
	aPosLocal[0] = (aPos[0] - m_v3BoundsMin[0]) / m_v3BoundsDelta[0];
	aPosLocal[1] = (aPos[1] - m_v3BoundsMin[1]) / m_v3BoundsDelta[1];
	aPosLocal[2] = (aPos[2] - m_v3BoundsMin[2]) / m_v3BoundsDelta[2];

	_VCGRetrieveDataNormalized<unsigned char>(aPosLocal, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.puc, pData);
}

void VveCartesianGrid::GetDataNormalized(float x, float y, float z, unsigned char *pData) const
{
	float aPos[3] = { x, y, z };

	_VCGRetrieveDataNormalized<unsigned char>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.puc, pData);
}

void VveCartesianGrid::GetDataNormalized(const float aPos[3], unsigned char *pData) const
{
	_VCGRetrieveDataNormalized<unsigned char>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.puc, pData);
}

void VveCartesianGrid::GetData(float x, float y, float z, unsigned short *pData) const
{
	float aPos[3];

	// convert coordinates given in visualization space into local space
	aPos[0] = (x - m_v3BoundsMin[0]) / m_v3BoundsDelta[0];
	aPos[1] = (y - m_v3BoundsMin[1]) / m_v3BoundsDelta[1];
	aPos[2] = (z - m_v3BoundsMin[2]) / m_v3BoundsDelta[2];

	_VCGRetrieveDataNormalized<unsigned short>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.pus, pData);
}

void VveCartesianGrid::GetData(const float aPos[3], unsigned short *pData) const
{
	float aPosLocal[3];

	// convert coordinates given in visualization space into local space
	aPosLocal[0] = (aPos[0] - m_v3BoundsMin[0]) / m_v3BoundsDelta[0];
	aPosLocal[1] = (aPos[1] - m_v3BoundsMin[1]) / m_v3BoundsDelta[1];
	aPosLocal[2] = (aPos[2] - m_v3BoundsMin[2]) / m_v3BoundsDelta[2];

	_VCGRetrieveDataNormalized<unsigned short>(aPosLocal, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.pus, pData);
}

void VveCartesianGrid::GetDataNormalized(float x, float y, float z, unsigned short *pData) const
{
	float aPos[3] = { x, y, z };

	_VCGRetrieveDataNormalized<unsigned short>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.pus, pData);
}

void VveCartesianGrid::GetDataNormalized(const float aPos[3], unsigned short *pData) const
{
	_VCGRetrieveDataNormalized<unsigned short>(aPos, m_aDataDimensions, 
		m_aStride, m_iComponents,
		m_oData.pus, pData);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIndex                                                    */
/*                                                                            */
/*============================================================================*/
int VveCartesianGrid::GetIndex(const float aPos[3]) const
{
	int aIndex[3];
	GetIndex(aPos, aIndex);
	return aIndex[0] + aIndex[1]*m_aDimensions[0] + aIndex[2]*m_aDimensions[0]*m_aDimensions[1];
}

void VveCartesianGrid::GetIndex(const float aPos[3], int aIndex[3]) const
{
	float aPosLocal[3];

	// convert coordinates given in visualization space into local space
	aPosLocal[0] = (aPos[0] - m_v3BoundsMin[0]) / m_v3BoundsDelta[0];
	aPosLocal[1] = (aPos[1] - m_v3BoundsMin[1]) / m_v3BoundsDelta[1];
	aPosLocal[2] = (aPos[2] - m_v3BoundsMin[2]) / m_v3BoundsDelta[2];

	if (aPosLocal[0] < 0)
		aPosLocal[0] = 0;
	else if (aPosLocal[0] > 1)
		aPosLocal[0] = 1;
	if (aPosLocal[1] < 0)
		aPosLocal[1] = 0;
	else if (aPosLocal[1] > 1)
		aPosLocal[1] = 1;
	if (aPosLocal[2] < 0)
		aPosLocal[2] = 0;
	else if (aPosLocal[2] > 1)
		aPosLocal[2] = 1;

	aIndex[0] = int(floor(aPosLocal[0]*(m_aDimensions[0]-1) + 0.5f));
	aIndex[1] = int(floor(aPosLocal[1]*(m_aDimensions[1]-1) + 0.5f));
	aIndex[2] = int(floor(aPosLocal[2]*(m_aDimensions[2]-1) + 0.5f));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Dump                                                        */
/*                                                                            */
/*============================================================================*/
void VveCartesianGrid::Dump(ostream &os) const
{
	os << " [VveCartesianGrid] - dimensions:  " << m_aDimensions[0] << "x"
		<< m_aDimensions[1] << "x" << m_aDimensions[2] << endl;
	os << " [VveCartesianGrid] - data dims:   " << m_aDataDimensions[0] << "x"
		<< m_aDataDimensions[1] << "x" << m_aDataDimensions[2] << endl;
	os << " [VveCartesianGrid] - components:  " << m_iComponents << endl;
	os << " [VveCartesianGrid] - data type:   ";
	switch (m_iDataType)
	{
	case DT_INVALID:
		os << "invalid";
		break;
	case DT_FLOAT:
		os << "float";
		break;
	case DT_HALF:
		os << "half";
		break;
	case DT_UNSIGNED_CHAR:
		os << "unsigned char";
    break;
  case DT_UNSIGNED_SHORT:
		os << "unsigned short";
    break;
	default:
		os << "*unknown*";
		break;
	}
	os << endl;

	os << " [VveCartesianGrid] - bounds:      " << m_v3BoundsMin
		<< " - " << m_v3BoundsMax << endl;
	os << " [VveCartesianGrid] - scalar range: " << m_aRange[0] << " - " 
		<< m_aRange[1] << endl;;
	os << " [VveCartesianGrid] - byte count:  " << m_iByteCount << endl;
	os << " [VveCartesianGrid] - valid:       " << (m_bValid ? "true" : "false") << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   (De)SerializeCartesianGrid                                  */
/*                                                                            */
/*============================================================================*/
int VveCartesianGrid::DeSerializeCartesianGrid(IVistaDeSerializer &rDeSer,
												VveCartesianGrid &refGrid)
{
	int iRet = 0;

	int iDimX, iDimY, iDimZ, iComponents, iDataType;
	iRet += rDeSer.ReadInt32(iDimX);
	iRet += rDeSer.ReadInt32(iDimY);
	iRet += rDeSer.ReadInt32(iDimZ);

	iRet += rDeSer.ReadInt32(iComponents);
	iRet += rDeSer.ReadInt32(iDataType);

	bool bSuccess = refGrid.Reset(iDimX, iDimY, iDimZ, iComponents, iDataType);

	iRet += rDeSer.ReadInt32(refGrid.m_aDataDimensions[0]);
	iRet += rDeSer.ReadInt32(refGrid.m_aDataDimensions[1]);
	iRet += rDeSer.ReadInt32(refGrid.m_aDataDimensions[2]);

	VistaVector3D v3Min, v3Max;
	iRet += rDeSer.ReadFloat32(v3Min[0]);
	iRet += rDeSer.ReadFloat32(v3Min[1]);
	iRet += rDeSer.ReadFloat32(v3Min[2]);
	iRet += rDeSer.ReadFloat32(v3Max[0]);
	iRet += rDeSer.ReadFloat32(v3Max[1]);
	iRet += rDeSer.ReadFloat32(v3Max[2]);
	refGrid.SetBounds(v3Min, v3Max);

	iRet += rDeSer.ReadFloat32(refGrid.m_aRange[0]);
	iRet += rDeSer.ReadFloat32(refGrid.m_aRange[1]);

	bool bValid;
	iRet += rDeSer.ReadBool(bValid);

	string strTemp;
	iRet += VistaPropertyList::DeSerializePropertyList(rDeSer, *(refGrid.m_pMetaData), strTemp);

	int iCount = iComponents*iDimX*iDimY*iDimZ;
	switch (iDataType)
	{
	case DT_FLOAT:
		if (bSuccess)
		{
			float *pPos = refGrid.GetDataAsFloat();
			for (int i=0; i<iCount; ++i, ++pPos)
				iRet += rDeSer.ReadFloat32(*pPos);
		}
		else
		{
			float fTemp;
			for (int i=0; i<iCount; ++i)
				iRet += rDeSer.ReadFloat32(fTemp);
		}
		break;
	case DT_HALF:
		if (bSuccess)
		{
			VistaHalf *pPos = refGrid.GetDataAsHalf();
			unsigned short usTemp;
			for (int i=0; i<iCount; ++i, ++pPos)
			{
				iRet += rDeSer.ReadShort16(usTemp);
				pPos->setBits(usTemp);
			}
		}
		else
		{
			unsigned short usTemp;
			for (int i=0; i<iCount; ++i)
				iRet += rDeSer.ReadShort16(usTemp);
		}
		break;
	case DT_UNSIGNED_CHAR:
		if (bSuccess)
		{
			unsigned char *pPos = refGrid.GetDataAsUnsignedChar();
			// read byte for byte in order to avoid byte swapping
			for (int i=0; i<iCount; ++i, ++pPos)
				iRet += rDeSer.ReadRawBuffer(pPos, 1);
		}
		else
		{
			unsigned char ucTemp;
			for (int i=0; i<iCount; ++i)
				iRet += rDeSer.ReadRawBuffer(&ucTemp, 1);
		}
		break;
  case DT_UNSIGNED_SHORT:
		if (bSuccess)
		{
			unsigned short *pPos = refGrid.GetDataAsUnsignedShort();
			for (int i=0; i<iCount; ++i, ++pPos)
				iRet += rDeSer.ReadShort16(*pPos);
		}
		else
		{
			unsigned short usTemp;
			for (int i=0; i<iCount; ++i)
				iRet += rDeSer.ReadShort16(usTemp);
		}
		break;
	default:
		// TODO: handle error!!!
		break;
	}

	refGrid.m_bValid = bValid;

	return iRet;
}

int VveCartesianGrid::SerializeCartesianGrid(IVistaSerializer &rSer,
											  const VveCartesianGrid &refGrid)
{
	int iRet = 0;

	iRet += rSer.WriteInt32(refGrid.m_aDimensions[0]);
	iRet += rSer.WriteInt32(refGrid.m_aDimensions[1]);
	iRet += rSer.WriteInt32(refGrid.m_aDimensions[2]);

	iRet += rSer.WriteInt32(refGrid.m_iComponents);
	iRet += rSer.WriteInt32(refGrid.m_iDataType);

	iRet += rSer.WriteInt32(refGrid.m_aDataDimensions[0]);
	iRet += rSer.WriteInt32(refGrid.m_aDataDimensions[1]);
	iRet += rSer.WriteInt32(refGrid.m_aDataDimensions[2]);

	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMin[0]);
	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMin[1]);
	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMin[2]);
	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMax[0]);
	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMax[1]);
	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMax[2]);

	iRet += rSer.WriteFloat32(refGrid.m_aRange[0]);
	iRet += rSer.WriteFloat32(refGrid.m_aRange[1]);

	iRet += rSer.WriteBool(refGrid.m_bValid);

	iRet += VistaPropertyList::SerializePropertyList(rSer, *(refGrid.m_pMetaData), "PROPERTIES");

	int iCount = refGrid.m_iComponents * refGrid.m_aDimensions[0] 
		* refGrid.m_aDimensions[1] * refGrid.m_aDimensions[2];

	switch (refGrid.m_iDataType)
	{
	case DT_FLOAT:
		{
			float *pPos = refGrid.GetDataAsFloat();
			for (int i=0; i<iCount; ++i, ++pPos)
				iRet += rSer.WriteFloat32(*pPos);
		}
		break;
	case DT_HALF:
		{
			VistaHalf *pPos = refGrid.GetDataAsHalf();
			for (int i=0; i<iCount; ++i, ++pPos)
				iRet += rSer.WriteShort16(pPos->bits());
		}
		break;
	case DT_UNSIGNED_CHAR:
		{
			unsigned char *pPos = refGrid.GetDataAsUnsignedChar();
			// write byte for byte to avoid byte swapping
			for (int i=0; i<iCount; ++i, ++pPos)
				iRet += rSer.WriteRawBuffer(pPos, 1);
		}
		break;
  case DT_UNSIGNED_SHORT:
		{
			unsigned short *pPos = refGrid.GetDataAsUnsignedShort();
			for (int i=0; i<iCount; ++i, ++pPos)
				iRet += rSer.WriteShort16(*pPos);
		}
		break;
	default:
		// TODO: handle error!!!
		break;
	}

	return iRet;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   RLEDecodeCartesianGrid                                      */
/*                                                                            */
/*============================================================================*/
int VveCartesianGrid::RLEDecodeCartesianGrid(IVistaDeSerializer &rDeSer, 
		VveCartesianGrid &refGrid)
{
	//read standard data as for normal deserialization
	int iRet = 0;

	int iDimX, iDimY, iDimZ, iComponents, iDataType;
	iRet += rDeSer.ReadInt32(iDimX);
	iRet += rDeSer.ReadInt32(iDimY);
	iRet += rDeSer.ReadInt32(iDimZ);

	iRet += rDeSer.ReadInt32(iComponents);
	iRet += rDeSer.ReadInt32(iDataType);

	bool bSuccess = refGrid.Reset(iDimX, iDimY, iDimZ, iComponents, iDataType);

	iRet += rDeSer.ReadInt32(refGrid.m_aDataDimensions[0]);
	iRet += rDeSer.ReadInt32(refGrid.m_aDataDimensions[1]);
	iRet += rDeSer.ReadInt32(refGrid.m_aDataDimensions[2]);

	VistaVector3D v3Min, v3Max;
	iRet += rDeSer.ReadFloat32(v3Min[0]);
	iRet += rDeSer.ReadFloat32(v3Min[1]);
	iRet += rDeSer.ReadFloat32(v3Min[2]);
	iRet += rDeSer.ReadFloat32(v3Max[0]);
	iRet += rDeSer.ReadFloat32(v3Max[1]);
	iRet += rDeSer.ReadFloat32(v3Max[2]);
	refGrid.SetBounds(v3Min, v3Max);

	iRet += rDeSer.ReadFloat32(refGrid.m_aRange[0]);
	iRet += rDeSer.ReadFloat32(refGrid.m_aRange[1]);

	bool bValid;
	iRet += rDeSer.ReadBool(bValid);

	string strTemp;
	iRet += VistaPropertyList::DeSerializePropertyList(rDeSer, *(refGrid.m_pMetaData), strTemp);

	int iCount = iComponents*iDimX*iDimY*iDimZ;
	switch (iDataType)
	{
	case DT_FLOAT:
		if (bSuccess)
		{
			float *pPos = refGrid.GetDataAsFloat();
			__CFloatAccess oF(rDeSer);
			__RLEDecode(oF, pPos, iCount);
		}
		else
		{
			float fTemp;
			for (int i=0; i<iCount; ++i)
				iRet += rDeSer.ReadFloat32(fTemp);
		}
		break;
	case DT_HALF:
		if (bSuccess)
		{
			VistaHalf *pPos = refGrid.GetDataAsHalf();
			__CHalfAccess oH(rDeSer);
			__RLEDecode(oH, pPos, iCount);
		}
		else
		{
			unsigned short usTemp;
			for (int i=0; i<iCount; ++i)
				iRet += rDeSer.ReadShort16(usTemp);
		}
		break;
	case DT_UNSIGNED_CHAR:
		if (bSuccess)
		{
			unsigned char *pPos = refGrid.GetDataAsUnsignedChar();
			__CCharAccess oC(rDeSer);
			__RLEDecode(oC, pPos, iCount);
		}
		else
		{
			unsigned char ucTemp;
			for (int i=0; i<iCount; ++i)
				iRet += rDeSer.ReadRawBuffer(&ucTemp, 1);
		}
		break;
  case DT_UNSIGNED_SHORT:
		if (bSuccess)
		{
			unsigned short *pPos = refGrid.GetDataAsUnsignedShort();
			__CShortAccess oC(rDeSer);
			__RLEDecode(oC, pPos, iCount);
		}
		else
		{
			unsigned short usTemp;
			for (int i=0; i<iCount; ++i)
				iRet += rDeSer.ReadShort16(usTemp);
		}
		break;
	default:
		// TODO: handle error!!!
		break;
	}

	refGrid.m_bValid = bValid;

	return iRet;

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RLEEncodeCartesianGrid                                      */
/*                                                                            */
/*============================================================================*/
int VveCartesianGrid::RLEEncodeCartesianGrid(IVistaSerializer &rSer, 
		VveCartesianGrid &refGrid)
{
	int iCount =	refGrid.m_iComponents * 
					refGrid.m_aDimensions[0] * 
					refGrid.m_aDimensions[1] * 
					refGrid.m_aDimensions[2];

	//same as for usual encoding
	int iRet = 0;
	
	iRet += rSer.WriteInt32(refGrid.m_aDimensions[0]);
	iRet += rSer.WriteInt32(refGrid.m_aDimensions[1]);
	iRet += rSer.WriteInt32(refGrid.m_aDimensions[2]);

	iRet += rSer.WriteInt32(refGrid.m_iComponents);
	iRet += rSer.WriteInt32(refGrid.m_iDataType);

	iRet += rSer.WriteInt32(refGrid.m_aDataDimensions[0]);
	iRet += rSer.WriteInt32(refGrid.m_aDataDimensions[1]);
	iRet += rSer.WriteInt32(refGrid.m_aDataDimensions[2]);

	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMin[0]);
	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMin[1]);
	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMin[2]);
	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMax[0]);
	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMax[1]);
	iRet += rSer.WriteFloat32(refGrid.m_v3BoundsMax[2]);

	iRet += rSer.WriteFloat32(refGrid.m_aRange[0]);
	iRet += rSer.WriteFloat32(refGrid.m_aRange[1]);

	iRet += rSer.WriteBool(refGrid.m_bValid);

	iRet += VistaPropertyList::SerializePropertyList(rSer, *(refGrid.m_pMetaData), "PROPERTIES");

	//now starts the RLE encode
	switch (refGrid.m_iDataType)
	{
	case DT_FLOAT:
		{
			float *pPos = refGrid.GetDataAsFloat();
			iRet += __RLEEncode(pPos, iCount, rSer);
		}
		break;
	case DT_HALF:
		{
			VistaHalf *pPos = refGrid.GetDataAsHalf();
			iRet += __RLEEncode(pPos, iCount, rSer);
		}
		break;
	case DT_UNSIGNED_CHAR:
		{
			unsigned char *pPos = refGrid.GetDataAsUnsignedChar();
			iRet += __RLEEncode(pPos, iCount, rSer);
		}
		break;
  case DT_UNSIGNED_SHORT:
		{
			unsigned short *pPos = refGrid.GetDataAsUnsignedShort();
			iRet += __RLEEncode(pPos, iCount, rSer);
		}
		break;
	default:
		// TODO: handle error!!!
		break;
	}

	return iRet;


}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CopyCartesianGrid                                           */
/*                                                                            */
/*============================================================================*/
void VveCartesianGrid::CopyCartesianGrid(VveCartesianGrid &refDest,
										  const VveCartesianGrid &refSource)
{
	if (!refDest.Reset(refSource.m_aDimensions[0], refSource.m_aDimensions[1], 
		refSource.m_aDimensions[2], refSource.m_iComponents, refSource.m_iDataType))
	{
		return;
	}

	memcpy(refDest.m_aDataDimensions, refSource.m_aDataDimensions, 3*sizeof(int));

	refDest.m_v3BoundsMin = refSource.m_v3BoundsMin;
	refDest.m_v3BoundsMax = refSource.m_v3BoundsMax;
	refDest.m_v3BoundsDelta = refSource.m_v3BoundsDelta;

	memcpy(refDest.m_aRange, refSource.m_aRange, 2*sizeof(float));
	memcpy(refDest.m_oData.pv, refSource.m_oData.pv, refDest.m_iByteCount);

	*(refDest.m_pMetaData) = *(refSource.m_pMetaData);

	refDest.m_bValid = refSource.m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AllocateMemory                                              */
/*                                                                            */
/*============================================================================*/
bool VveCartesianGrid::AllocateMemory()
{
	switch (m_iDataType)
	{
	case DT_FLOAT:
		delete [] m_oData.pf;
		break;
	case DT_HALF:
		delete [] m_oData.ph;
		break;
	case DT_UNSIGNED_CHAR:
		delete [] m_oData.puc;
		break;
  case DT_UNSIGNED_SHORT:
		delete [] m_oData.puc;
		break;
	}
	m_oData.pv = NULL;
	m_iByteCount = 0;

	if (m_iDataType < DT_FLOAT || m_iDataType >= DT_LAST)
	{
		vstr::errp() << " [VveCartesianGrid] unknown data type: " 
			<< m_iDataType << endl;
		return false;
	}

	if (m_iComponents < 0)
	{
		vstr::errp() << " [VveCartesianGrid] invalid component count: "
			<< m_iComponents << endl;
		return false;
	}

	if (m_iComponents > MAX_COMPONENTS)
	{
		vstr::warnp() << " [VveCartesianGrid] too many components (" << m_iComponents
			<< " > " << MAX_COMPONENTS << ")..." << endl;
		vstr::warni() << "                                setting components to " 
			<< MAX_COMPONENTS << "..." << endl;
		m_iComponents = MAX_COMPONENTS;
	}

	int iCount = m_aDimensions[0] * m_aDimensions[1] * m_aDimensions[2]
		* m_iComponents;

	switch (m_iDataType)
	{
	case DT_FLOAT:
		m_oData.pf = new float[iCount];
		m_iByteCount = iCount * sizeof(float);
		break;
	case DT_HALF:
		m_oData.ph = new VistaHalf[iCount];
		m_iByteCount = iCount * sizeof(VistaHalf);
		break;
	case DT_UNSIGNED_CHAR:
		m_oData.puc = new unsigned char[iCount];
		m_iByteCount = iCount * sizeof(unsigned char);
    break;
	case DT_UNSIGNED_SHORT:
		m_oData.pus = new unsigned short[iCount];
		m_iByteCount = iCount * sizeof(unsigned short);
    break;
  }

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ConvertToImageData                                          */
/*                                                                            */
/*============================================================================*/
bool VveCartesianGrid::ConvertToImageData (VveCartesianGrid *pSource, vtkImageData* pImageData)
{
	if (pSource->GetDataType() == DT_HALF || pSource->GetDataType() == DT_UNSIGNED_CHAR)
		return false;

	int aDataDim[3];
	float aExtents[3];
	float aBounds[6];
	float aRange[2];

	// get information from source
	VistaVector3D aMin, aMax;
	pSource->GetDimensions(aDataDim[0], aDataDim[1], aDataDim[2]);
	pSource->GetBounds(aMin,aMax);

	aBounds[0] = aMin[0];
	aBounds[2] = aMin[1];
	aBounds[4] = aMin[2];
	aBounds[1] = aMax[0];
	aBounds[3] = aMax[1];
	aBounds[5] = aMax[2];

	aExtents[0] = aBounds[1]-aBounds[0];
	aExtents[1] = aBounds[3]-aBounds[2];
	aExtents[2] = aBounds[5]-aBounds[4];
	
	pSource->GetScalarRange(aRange);

	// set origin, dimension, spacing and extent
	pImageData->SetOrigin (aMin[0], aMin[1], aMin[2]);
	pImageData->SetDimensions (aDataDim[0], aDataDim[1], aDataDim[2]);
	pImageData->SetSpacing(	aExtents[0]/float(aDataDim[0]-1),
							aExtents[1]/float(aDataDim[1]-1),
							aExtents[2]/float(aDataDim[2]-1));
	pImageData->SetExtent(0,aDataDim[0]-1, 0, aDataDim[1]-1, 0, aDataDim[2]-1);

	vtkFloatArray* sa = NULL;
	vtkFloatArray* va = NULL;

	int iDataPos = 0;

	// REMARK: this class uses floats (as it is used in GPU computation,
	// but VTK 5 uses double only. 
	// vtk 5.6: scalar values can be set as float by using SetValue()

	// convert only scalar data
	if (pSource->GetComponents()==1)
	{
		sa = vtkFloatArray::New();
		sa->SetNumberOfComponents (1);
		// sa->Allocate(aDataDim[0]*aDataDim[1]*aDataDim[2]); 
		// this does not work after the update to vtk 5.6
		sa->SetNumberOfValues(aDataDim[0]*aDataDim[1]*aDataDim[2]);
		float fValue; 
		for (int z=0; z<aDataDim[2]; ++z)
		{
			for (int y=0; y<aDataDim[1]; ++y)
			{
				for (int x=0; x<aDataDim[0]; ++x)
				{
					pSource->GetData (x,y,z, &fValue);
					sa->SetValue(iDataPos, fValue);
					++iDataPos;
				}
			}
		}
	}

	// convert only vector data
	if (pSource->GetComponents()==3)
	{
		va = vtkFloatArray::New();
		va->SetNumberOfComponents (3);
		va->SetNumberOfTuples(aDataDim[0]*aDataDim[1]*aDataDim[2]);
		float fValue[3];
		for (int z=0; z<aDataDim[2]; ++z)
		{
			for (int y=0; y<aDataDim[1]; ++y)
			{
				for (int x=0; x<aDataDim[0]; ++x)
				{
					pSource->GetData (x,y,z, fValue);
					va->SetTuple3 (iDataPos, (double) fValue[0], (double) fValue[1], (double) fValue[2]);
					iDataPos++;
				}
			}
		}
	}

	// convert vector and scalar data
	if (pSource->GetComponents()==4)
	{
		sa = vtkFloatArray::New();
		sa->SetNumberOfComponents (1);
		sa->SetNumberOfValues(aDataDim[0]*aDataDim[1]*aDataDim[2]);
		va = vtkFloatArray::New();
		va->SetNumberOfComponents (3);
		va->SetNumberOfTuples(aDataDim[0]*aDataDim[1]*aDataDim[2]);
		float fValue[4];

		for (int z=0; z<aDataDim[2]; ++z)
		{
			for (int y=0; y<aDataDim[1]; ++y)
			{
				for (int x=0; x<aDataDim[0]; ++x)
				{
					pSource->GetData (x,y,z, fValue);
					va->SetTuple3 (iDataPos, (double) fValue[0], (double) fValue[1], (double) fValue[2]);
					sa->SetValue(iDataPos, fValue[3]);
					iDataPos++;
				}
			}
		}
	}

	// set these fields into vtkImageData
	// REMARK: names are missing
	pImageData->GetPointData()->SetScalars(sa);
	//@TODO Is simply removing the next line ok to resolve vtk6.x issues?
	//		Does setting a FloatArray in the line above suffice maybe?
	//pImageData->SetScalarTypeToFloat();
	pImageData->GetPointData()->SetVectors(va);

	if(sa) 
		sa->Delete();
	if(va) 
		va->Delete();

	return true;
}


template<class T> 
inline int __RLEEncode(T* data, int iNumEntries, IVistaSerializer &oSer)
{
	int iSize  = sizeof(data[0]);
	
	int iCount = 0;
	int iNumBytes = 0;

	int i=0;
	T current = data[0];
	
	while(i<iNumEntries)
	{
		//init next iteration
		current = data[i];
		iCount = 1;
		++i;
		//go ahead while we see the current value and have values left
		while(data[i] == current && i<iNumEntries)
		{
			++iCount;
			++i;
		}
		//write data for the current symbol
		iNumBytes += oSer.WriteInt32(iCount);
		switch(iSize)
		{
		case 1: iNumBytes += oSer.WriteRawBuffer((char*) &(current), 1); break;
		case 2: iNumBytes += oSer.WriteShort16(((VistaHalf)current).bits()); break;
		case 4: iNumBytes += oSer.WriteFloat32(current); break;
		case 8: iNumBytes += oSer.WriteDouble(current); break;
		default: return -1;
		}
	}
	return iNumBytes;
}


template<class TAccess, class TData> 
inline int __RLEDecode(TAccess &oDeSer, TData* tgt, int iNumEntries)
{
	int iNumBytes = 0;

	int iNumEntriesRead = 0;

	TData current;
	int iCount;
	int iPos = 0;
	int iBase = 0;

	int iSize = sizeof(tgt[0]);

	while(iPos < iNumEntries)
	{
		//init next iteration
		//read how many items to unpack
		oDeSer.GetNextCount(iCount);
		//read unpack value
		oDeSer.GetNextElemet(current);
		//unpack data (assuming that there is enough of it!)
		iBase = iPos;
		iPos += iCount;
		for(register int k=iBase; k<iPos; ++k)
			tgt[k] = current;
	}
	return iNumBytes;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
