/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#include "VvePropertyList.h"
#include <iostream>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VvePropertyList::VvePropertyList()
: VvePropertyContainer()
{
}

VvePropertyList::~VvePropertyList()
{

}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetProperty                                                 */
/*                                                                            */
/*============================================================================*/
int VvePropertyList::SetProperty(const VistaProperty &refProp)
{
    if (VvePropertyContainer::SetProperty(refProp) != PROP_OK)
    {
        m_oPropList[refProp.GetNameForNameable()] = refProp;

        Notify();
    }

    return PROP_OK;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperty                                                 */
/*                                                                            */
/*============================================================================*/
int VvePropertyList::GetProperty(VistaProperty &refProp)
{
    if (VvePropertyContainer::GetProperty(refProp) != PROP_OK)
    {
        if (m_oPropList.HasProperty(refProp.GetNameForNameable()))
        {
            refProp = m_oPropList[refProp.GetNameForNameable()];
        }
        else
        {
            return PROP_NOT_FOUND;
        }
    }

    return PROP_OK;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPropertySymbolList                                       */
/*                                                                            */
/*============================================================================*/
int VvePropertyList::GetPropertySymbolList(list<string> &rStorageList)
{
    VvePropertyContainer::GetPropertySymbolList(rStorageList);

    VistaPropertyList::iterator itProp;
    for (itProp=m_oPropList.begin(); itProp!=m_oPropList.end(); ++itProp)
    {
        rStorageList.push_back((*itProp).first);
    }

	// @todo: Cannot fix this within FlowLib.
    return static_cast<int>(rStorageList.size());
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   operator <<                                                 */
/*                                                                            */
/*============================================================================*/
std::ostream& operator<< (std::ostream& output, const VvePropertyList& oList)
{
    VistaPropertyList::const_iterator itCurrent = oList.m_oPropList.begin();
    VistaPropertyList::const_iterator itEnd = oList.m_oPropList.end();
    for(;itCurrent!=itEnd; ++itCurrent)
    {
        output << itCurrent->first << " = " << itCurrent->second.GetValue() << endl;
    };
    return output;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPropertyList                                             */
/*                                                                            */
/*============================================================================*/
const VistaPropertyList * VvePropertyList::GetPropertyList() const
{
    return &m_oPropList;
}

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/
