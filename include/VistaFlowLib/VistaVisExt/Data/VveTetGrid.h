/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVETETGRID_H
#define _VVETETGRID_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaBase/VistaVectorMath.h>
#include <iosfwd>
#include <iostream>
#include <list>
#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTetGridPointLocator;

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 * VveTetGrid contains information about a tetrahedral grid. 
 * Note, that memory is allocated by external means, i.e. this object
 * is just a collection of pointers and status information.
 * By providing a TopologyGrid object, topology information (i.e. vertex 
 * positions, vertex indices for cells, and cell neighborhood information)
 * is shared, i.e. retrieving this information from this object, returns
 * the corresponding pointers from the shared object.
 * Point attributes are stored locally in any case!
 * In order to share topology information between multiple tetrahedral grids,
 * create a grid containing the topology information, and set this object
 * as TopologyGrid object for all objects supposed to share the same information.
 * Note, that upon destruction it is a good idea to destroy the shared object
 * last.
 * One more note: There's a method for padding vertex and cell data, which 
 * does exactly what its name suggests. However, the values returned by 
 * GetVertexCount() and GetCellCount() do not change. Instead, querying via 
 * GetPaddedVertexCount() and GetPaddedCellCount() retrieves the correctly
 * padded values. Calling GetVertexCount() and GetCellCount() still returns
 * the number of valid (i.e. well-defined) vertices and cells.
 */
class VISTAVISEXTAPI VveTetGrid
{
public:
	VveTetGrid();
	virtual ~VveTetGrid();

	// allocate some memory (to make sure, we use the same 
	// memory allocation/dealloction scheme throughout
	// the whole application (at least for this class...)
	static float *NewFloatArray(int iCount);
	static int *NewIntArray(int iCount);
	static void DestroyArray(float *pData);
	static void DestroyArray(int *pData);

	// clear all data
	void Reset();

	// manage topology information
	// If a topology grid is defined, the calls
	// get routed to this object.
	void SetVertices(float *pData);
	float *GetVertices() const;
	void SetVertexCount(int iCount);
	int GetVertexCount() const;
	void SetPaddedVertexCount(int iCount);
	int GetPaddedVertexCount() const;

	void SetCells(int *pData);
	int *GetCells() const;
	void SetCellCount(int iCount);
	int GetCellCount() const;
	void SetPaddedCellCount(int iCount);
	int GetPaddedCellCount() const;

	void SetCellNeighbors(int *pData);
	int *GetCellNeighbors() const;

	// manage payload information
	void SetPointData(float *pData);
	float *GetPointData() const;
	void SetComponents(int iComponents);
	int GetComponents() const;

	// manage data validity
	void SetValid (bool bValid);
	bool GetValid() const;

	// manage data sharing
	void SetTopologyGrid(VveTetGrid *pGrid);
	VveTetGrid *GetTopologyGrid() const;

	// manage point locator
	// (setting the point locator registers the grid with the locator, too)
	void SetPointLocator(VveTetGridPointLocator *pLocator);
	VveTetGridPointLocator *GetPointLocator() const;

	// define the boundaries of the (valid) data
	void SetBounds(const VistaVector3D &v3Min, const VistaVector3D &v3Max);
	void GetBounds(VistaVector3D &v3Min, VistaVector3D &v3Max) const;

	// manage scalar range
	void SetScalarRange(float aRange[2]);
	void GetScalarRange(float aRange[2]) const;

	// retrieve data at a given position
	// NOTE: a locator has to be set and correctly initialized!
	int GetData(const float aPos[3], float *pData, bool bRetry=false) const;

	void GatherInformation();	// compute bounding box and byte count
	void ComputeByteCount();	
	void PadData(int iVertexCount, int iCellCount);
	void ComputeCellNeighbors();

	void ComputeBCs(float aBC[4], const float *pPos, int iCellIndex);

	void Dump(std::ostream &os) const;
	int GetDataByteCount() const;

protected:
	float	*m_pVertices;
	float	*m_pPointData;
	int		*m_pCells;
	int		*m_pCellNeighbors;
	int		m_iVertexCount;
	int		m_iPaddedVertexCount;
	int		m_iCellCount;
	int		m_iPaddedCellCount;
	int		m_iComponents;
	VistaVector3D	m_v3BoundsMin, m_v3BoundsMax;
	int				m_iByteCount;
	bool			m_bValid;
	float			m_aRange[2];

	VveTetGrid	*m_pTopologyGrid;
	VveTetGridPointLocator	*m_pLocator;
};

#endif // _VveTetGrid_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

