/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEDATAITEM_H
#define _VVEDATAITEM_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaInterProcComm/Concurrency/VistaMutex.h>
#include <VistaAspects/VistaObserveable.h>
//#include "VveObservedSubject.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This class offers an interface for access to various data types to be
 * contained in discrete or continuous data objects.
 *
 * To make sure, that objects referencing this data are always up-to-date
 * data is normally being locked during notification of the registered
 * observers - so, don't lock the observed data during the notification 
 * process, as this might result in some serious deadlocking!
 */
class VISTAVISEXTAPI IVveDataItem : public IVistaObserveable 
{
public:    
    IVveDataItem();
    virtual ~IVveDataItem();

    /**
     * Lock the data object.
     */
    void LockData();

    /**
     * Unlock the data object.
     */
    void UnlockData();

    /**
     * Check, whether the data is locked.
     */
    bool TryLockData();

    /**
    * inherited from IVistaNameable
    */
    virtual std::string GetNameForNameable() const;

    virtual void        SetNameForNameable(const std::string &sNewName);

	enum
	{
		MSG_NAMECHANGE = IVistaObserveable::MSG_LAST,
		MSG_DATACHANGE,
		MSG_LAST
	};

protected:    
    VistaMutex m_oMutex;

private:
    std::string m_strName;
};

template <class TRawVisData>
class VveDataItem : public IVveDataItem
{
public:
	typedef TRawVisData      value_type;

    VveDataItem();
    virtual ~VveDataItem();

    TRawVisData* GetData() const;
    /**
    * SetData sets the internal pointer to the given one and returns the
    * old pointer in turn. This is a convenience feature in order to be 
    * able to "take care" of the old content correctly.
    */
    TRawVisData* SetData(TRawVisData *pData);
	
	void SignalDataChange();
    
protected:
    TRawVisData *m_pData;
};

/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
template<class TRawVisData>
inline VveDataItem<TRawVisData>::VveDataItem()
: m_pData(NULL)
{
}

template<class TRawVisData>
inline VveDataItem<TRawVisData>::~VveDataItem()
{
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetData                                                              */
/*                                                                            */
/*============================================================================*/
template<class TRawVisData>
inline TRawVisData *VveDataItem<TRawVisData>::GetData() const
{
    return m_pData;
}

/*============================================================================*/
/*                                                                            */
/* NAME: SetData                                                              */
/*                                                                            */
/*============================================================================*/
template<class TRawVisData>
inline TRawVisData* VveDataItem<TRawVisData>::SetData(TRawVisData *pData)
{
    TRawVisData* pOldData = m_pData;
    bool bNotify = false;
    LockData();
	if(m_pData != pData)
	{
		m_pData = pData;
		bNotify = true;
	}
    UnlockData();

	if( bNotify )
		SignalDataChange();
	  
    return pOldData;
}

/*============================================================================*/
/*                                                                            */
/* NAME: SignalDataChange                                                     */
/*                                                                            */
/*============================================================================*/
template<class TRawVisData>
inline void VveDataItem<TRawVisData>::SignalDataChange()
{
	Notify(MSG_DATACHANGE);
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VVEDATACONTAINER_H
