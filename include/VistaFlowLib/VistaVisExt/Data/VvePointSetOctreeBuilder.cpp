/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VvePointSetOctreeBuilder.h"
#include "VveOctree.h"
#include <vtkDataSet.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkCellCenters.h>
#include <vtkMath.h>
#include <stack>
#include <cassert>
#include <limits>

#include <VistaBase/VistaStreamUtils.h>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
static float fMaxY = -std::numeric_limits<float>::max();

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

VveOctree* VvePointSetOctreeBuilder::BuildOctreeForPoints(vtkDataSet *pDS, int iBucketSize)
{
	VveOctree *pNewTree = new VveOctree;
	VveOctreeNode *pCurrentNode = pNewTree->GetRoot();

	//if (pDS->IsA ("vtkImageData")||pDS->IsA ("vtkRectilinearGrid"))
	//{
		for(register int i=0; i<pDS->GetNumberOfPoints(); ++i)
		{
			double* dPoint = new double [3];
			pDS->GetPoint (i, dPoint);
			pCurrentNode->PushPoint (dPoint, i);
		}
		pCurrentNode->SetDeletePositions(true);
	//}
	//else
	//{
	//	for(register int i=0; i<pDS->GetNumberOfPoints(); ++i)
	//	{
	//		pCurrentNode->PushPoint(pDS->GetPoint(i), i);
	//	}
	//}
	//pCurrentNode->UpdateBounds();
	
	double dBounds[6];
	pDS->GetBounds(dBounds);
	pCurrentNode->SetBounds(dBounds);

	VvePointSetOctreeBuilder::BuildOctree(pNewTree, iBucketSize);

	return pNewTree;
}

VveOctree* VvePointSetOctreeBuilder::BuildOctreeForCells(vtkDataSet *pDS, int iBucketSize)
{
	vtkCellCenters *pCellCenters = vtkCellCenters::New();

#if VTK_MAJOR_VERSION > 5
	pCellCenters->SetInputData(pDS);
#else
	pCellCenters->SetInput(pDS);
#endif
	pCellCenters->Update();

	vtkPolyData *pCenters = pCellCenters->GetOutput();
	if(pCenters->GetNumberOfPoints() != pDS->GetNumberOfCells())
	{
		vstr::errp() << "[VvePointSetOctreeBuilder::BuildOctreeForCells] Count mismatch : number of cells != number of cell centers" << endl << endl;
		return 0;
	}

	VveOctree *pNewTree = new VveOctree;
	VveOctreeNode *pCurrentNode = pNewTree->GetRoot();

	//if (pDS->IsA ("vtkImageData")||pDS->IsA ("vtkRectilinearGrid"))
	//{
		for(register int i=0; i<pCenters->GetNumberOfPoints(); ++i)
		{
			double* dPoint = new double [3];
			pCenters->GetPoint(i, dPoint);
			pCurrentNode->PushPoint (dPoint, i);
		}
		pCurrentNode->SetDeletePositions(true);
	//}
	//else
	//{
	//	for(register int i=0; i<pCenters->GetNumberOfPoints(); ++i)
	//	{
	//		pCurrentNode->PushPoint(pCenters->GetPoint(i), i);
	//	}
	//}
	//for(register int i=0; i<pCenters->GetNumberOfPoints(); ++i)
	//	pCurrentNode->PushPoint(pCenters->GetPoint(i), i);
	
	double dBounds[6];
	pCenters->GetBounds(dBounds);
	pCurrentNode->SetBounds(dBounds);

	VvePointSetOctreeBuilder::BuildOctree(pNewTree, iBucketSize);
	
	//NOTE: this might invalidate the point's coordinates in the octree, since they are not copied!
	pCellCenters->Delete();
	
	return pNewTree;
}

void VvePointSetOctreeBuilder::BuildOctree(VveOctree *pTree, int iBucketSize)
{
	VveOctreeNode *pCurrentNode = pTree->GetRoot();
	std::stack<VveOctreeNode*> stNodes;
	stNodes.push(pCurrentNode);

	while(!stNodes.empty())
	{
		pCurrentNode = stNodes.top();
		stNodes.pop();
		//terminate nodes that have been split sufficiently
		if(static_cast<int>(pCurrentNode->GetNumPoints()) <= iBucketSize)
			continue;
		//check for duplicate points
		if(pCurrentNode->GetNumPoints() == 2)
		{
			double *fPt1 = pCurrentNode->GetPoint(0);
			double *fPt2 = pCurrentNode->GetPoint(1);
			double fDist = sqrt(vtkMath::Distance2BetweenPoints(fPt1,fPt2));
			//NOTE: This threshold is somewhat arbitrary
			if(fDist < 10 * std::numeric_limits<double>::min())
				continue;
		}

		VvePointSetOctreeBuilder::SplitNode(pCurrentNode);

		for(int i=0; i<8; ++i)
		{
			//pCurrentNode->GetChild((VveOctreeNode::EChildID)i)->UpdateBounds();
			stNodes.push(pCurrentNode->GetChild((VveOctreeNode::EChildID)i));
		}
	}
}

void VvePointSetOctreeBuilder::SplitNode(VveOctreeNode *pNode)
{
#ifdef DEBUG
	size_t nNumSourcePts = pNode->GetNumPoints();
#endif

	pNode->Split();
	//
	double *dCurPt;
	unsigned int iCurId;
	double dSplitCenter[3];
	pNode->GetCenter(dSplitCenter);
	VveOctreeNode *pRBB = pNode->GetRBBChild();
	VveOctreeNode *pRTB = pNode->GetRTBChild();
	VveOctreeNode *pRBF = pNode->GetRBFChild();
	VveOctreeNode *pRTF = pNode->GetRTFChild();
	VveOctreeNode *pLBB = pNode->GetLBBChild();
	VveOctreeNode *pLTB = pNode->GetLTBChild();
	VveOctreeNode *pLBF = pNode->GetLBFChild();
	VveOctreeNode *pLTF = pNode->GetLTFChild();
	while(!pNode->IsEmpty())
	{
		//fetch data for current item...
		dCurPt = pNode->GetPoint();
		iCurId = pNode->GetId();
		pNode->PopPoint();
		//decide where it's going
		if(dCurPt[0] < dSplitCenter[0])//left
		{
			if(dCurPt[1] < dSplitCenter[1])//bottom
			{
				
				if(dCurPt[2] < dSplitCenter[2])//back
				{
					pLBB->PushPoint(dCurPt, iCurId);
				}
				else//front
				{
					pLBF->PushPoint(dCurPt, iCurId);
				}
			}
			else//top
			{
				if(dCurPt[2] < dSplitCenter[2])//back
				{
					pLTB->PushPoint(dCurPt, iCurId);
				}
				else//front
				{
					pLTF->PushPoint(dCurPt, iCurId);
				}
			}
		}
		else//right
		{
			if(dCurPt[1] < dSplitCenter[1])//bottom
			{
				if(dCurPt[2] < dSplitCenter[2])//back
				{
					pRBB->PushPoint(dCurPt, iCurId);
				}
				else//front
				{
					pRBF->PushPoint(dCurPt, iCurId);
				}
			}
			else//top
			{
				if(dCurPt[2] < dSplitCenter[2])//back
				{
					pRTB->PushPoint(dCurPt, iCurId);
				}
				else//front
				{
					pRTF->PushPoint(dCurPt, iCurId);
				}
			}
		}
	}//while
	
#ifdef DEBUG
	assert(pNode->IsEmpty());
	//make sure we didn't loose points on the way
	size_t nNumPts = pRBB->GetNumPoints() + pRTB->GetNumPoints();
	nNumPts += pRBF->GetNumPoints() + pRTF->GetNumPoints();
	nNumPts += pLBB->GetNumPoints() + pLTB->GetNumPoints();
	nNumPts += pLBF->GetNumPoints() + pLTF->GetNumPoints();
	assert(nNumPts == nNumSourcePts); 

	
	//vstr::debugi() << "Distributing " << iNumSourcePts << " points to " << endl;
	//for(int i=0; i<8; ++i)
	//	vstr::debugi() << "Node " << i << " -> " << pNode->GetChild((VveOctreeNode::EChildID) i)->GetNumPoints() << endl;
	//vstr::debug() << endl;
	
#endif
	//give away memory
	pNode->Clear();

	//set children bounds
	double fBounds[6];
	double fParentBounds[6];
	double fParentCenter[3];
	pNode->GetCenter(fParentCenter);
	pNode->GetBounds(fParentBounds);
	//set the children's bounds
	fBounds[0] = fParentCenter[0];
	fBounds[1] = fParentBounds[1];
	fBounds[2] = fParentBounds[2];
	fBounds[3] = fParentCenter[1];
	fBounds[4] = fParentBounds[4];
	fBounds[5] = fParentCenter[2];
	pNode->GetChild(VveOctreeNode::CID_RightBottomBack)->SetBounds(fBounds);
	fBounds[2] = fParentCenter[1];
	fBounds[3] = fParentBounds[3];
	pNode->GetChild(VveOctreeNode::CID_RightTopBack)->SetBounds(fBounds);
	fBounds[4] = fParentCenter[2];
	fBounds[5] = fParentBounds[5];
	pNode->GetChild(VveOctreeNode::CID_RightTopFront)->SetBounds(fBounds);
	fBounds[2] = fParentBounds[2];
	fBounds[3] = fParentCenter[1];
	pNode->GetChild(VveOctreeNode::CID_RightBottomFront)->SetBounds(fBounds);
	fBounds[0] = fParentBounds[0];
	fBounds[1] = fParentCenter[0];
	pNode->GetChild(VveOctreeNode::CID_LeftBottomFront)->SetBounds(fBounds);
	fBounds[2] = fParentCenter[1];
	fBounds[3] = fParentBounds[3];
	pNode->GetChild(VveOctreeNode::CID_LeftTopFront)->SetBounds(fBounds);
	fBounds[4] = fParentBounds[4];
	fBounds[5] = fParentCenter[2];
	pNode->GetChild(VveOctreeNode::CID_LeftTopBack)->SetBounds(fBounds);
	fBounds[2] = fParentBounds[2];
	fBounds[3] = fParentCenter[1];
	pNode->GetChild(VveOctreeNode::CID_LeftBottomBack)->SetBounds(fBounds);
	
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VVEPOINTSETOCTREEBUILDER.CPP"                                 */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

