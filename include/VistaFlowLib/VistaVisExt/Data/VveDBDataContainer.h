/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#error DO NOT INCLUDE DIZ (DEPRECATED)
#if 0
#ifndef _VVEDBDATACONTAINER_H
#define _VVEDBDATACONTAINER_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
//ViSTA includes
#include "VveDataItem.h"
#include <VistaInterProcComm/Concurrency/VistaMutex.h>

#include <vtkPolyData.h>
/*============================================================================*/
/* DEFINES                                                                   */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
	* VveDBDataItem represents a visualization data object which can be 
    * filled with user generated data. 
	* The template parameter <TRawVisData> is the underlying basic vis data type
	* (e.g. vtkPolyData). VveDBDataItem can handle any such data type since it is
	* merely a container that doesn't access the basic data structure itself.
	* For the purpose of easy synchronization between several threads that might access
	* VveDBDataItem it is internally implemented as DOUBLE BUFFER architecture. The
	* front buffer can be used for rendering after locking it correctly. The BACK BUFFER
	* is used as target for any incoming data meanwhile. See description of members for
	* further information...
	*
	* NOTE: There are two (yes just 2 of them) specific ways to change the data contained by this object:
	*
	*	1)	- Fetch a pointer to the back buffer 
	*		- Lock the back buffer	
	*		- Modify the data 
	*		- Unlock the back buffer (...BEFORE you call SwapBuffers())
	*		- call SwapBuffers(...)
	*
	*	2)	- Process your data
	*		- Call SetData(...) to set the data pointer in the container
	*		
	* The first way is the method of choice, when you want to change portions of the 
    * current back buffer's content. 
    * In contrast use the second one if you just want to put data into the container whithout
    * bothering about the double buffering. It operates directly on the data pointer just like 
    * VveDataContainer::SetData(...) does. NOTE: If you only use 2) consider taking a single buffer
	* object instead.
	*
	* @author     Bernd Hentschel
	* @date       August 2004
*/
template<class TRawVisData>
    class VveDBDataItem : public VveDataItem<TRawVisData>
{
public:    
    VveDBDataItem();
    virtual ~VveDBDataItem();

    /**
	* Return a pointer to the current back buffer for updating purposes. Remember to lock the 
	* back buffer during update in concurrent environments
	*
	* @param[in]	---
	* @param[out]	---
	* @return	TRawVisData*		Pointer to current back buffer 
	*
	* @author	Bernd Hentschel
	* @date		August 2004
    */
    virtual TRawVisData* GetBackBuffer();
	/**
	* return a pointer to the active (i.e. front) buffer.
	*
	* @param[in]	---
	* @param[out]	---	
	* @return	TRawVisData*	pointer to the currently bound front buffer data
	*
    */
    TRawVisData* GetFrontBuffer(){return VveDataItem<TRawVisData>::GetData();}

	/**
	* Swap FRONT and BACK buffer. Needs to lock both buffers. Thus...
	* CRITICAL!
    *
	* @param[in]	---
	* @param[out]	---	
	* @return	void
	*
	* @author	Bernd Hentschel
	* @date		August 2004
    */
    void SwapBuffers();
	
	/**
	* External methods for locking the front buffer during rendering in order
	* to ensure that no swapping operations occur meanwhile. NOTE: YOU are 
	* responsible for locking and unlocking in order to prevent thread clashes
	* or deadlocks respectively.
	* CRITICAL!
    *
	* @param[in]	---
	* @param[out]	---	
	* @return	---
	*
	* @author	Bernd Hentschel
	* @date		August 2004
    */
    void LockFrontBuffer() { VveDataItem<TRawVisData>::LockData(); };
    void UnlockFrontBuffer() { VveDataItem<TRawVisData>::UnlockData(); };
	
	/**
	* External methods for locking the back buffer during modification in order
	* to ensure that no swapping operations occur meanwhile. NOTE: YOU are 
	* responsible for locking and unlocking in order to prevent thread clashes
	* or deadlocks respectively.
	* CRITICAL!
    *
	* @param[in]	---
	* @param[out]	---	
	* @return	---
	*
	* @author	Bernd Hentschel
	* @date		August 2004
    */
    void LockBackBuffer() { m_oBackBufferLock.Lock(); };
    void UnlockBackBuffer() { m_oBackBufferLock.Unlock(); };

	/**
	* Re-set the data held by this object. This data pointer is transferred to the 
    * current FRONT BUFFER as in the case of the base class VveDataContainer.
	* CRITICAL!
	*
	* @param[in]	TRawVisData* pData   a pointer to the new data.
	* @param[out]	---	
	* @return	TRawVisData*         old front buffer content (...for proper disposal)   
	*
    */
	virtual TRawVisData* SetData(TRawVisData *);

protected:    
    TRawVisData*    m_arrVisBuffers[2];
    unsigned char   m_iCurrentBackBuffer;
	VistaMutex		m_oBackBufferLock;
};

/*============================================================================*/
/*  INLINE MEMBERS & FRIEND FUNCTIONS	                                      */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CONSTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
template<class TRawVisData> 
inline VveDBDataItem<TRawVisData>::VveDBDataItem() 
	:	VveDataItem<TRawVisData>(), 
		m_iCurrentBackBuffer(1)
{
    m_arrVisBuffers[0] = NULL;
    m_arrVisBuffers[1] = NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DESTRUCTOR                                                  */
/*                                                                            */
/*============================================================================*/
template<class TRawVisData> 
inline VveDBDataItem<TRawVisData>::~VveDBDataItem()
{
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBackBuffer                                               */
/*                                                                            */
/*============================================================================*/
template<class TRawVisData> 
inline TRawVisData* VveDBDataItem<TRawVisData>::GetBackBuffer()
{
	return m_arrVisBuffers[m_iCurrentBackBuffer];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetData                                                     */
/*                                                                            */
/*============================================================================*/
template<class TRawVisData> 
inline TRawVisData* VveDBDataItem<TRawVisData>::SetData(TRawVisData* pNewData)
{
    TRawVisData* pOldData = VveDataContainer<TRawVisData>::m_pData;

	VveDataContainer<TRawVisData>::m_oMutex.Lock();
    //put the data to the current front buffer
	m_arrVisBuffers[m_iCurrentBackBuffer^1] = pNewData;
    VveDataContainer<TRawVisData>::m_pData = pNewData;
	VveDataContainer<TRawVisData>::m_oMutex.Unlock();
    this->Notify();

    return pOldData;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SwapBuffers		                                          */
/*                                                                            */
/*============================================================================*/
template<class TRawVisData>
inline void VveDBDataItem<TRawVisData>::SwapBuffers()
{
	TRawVisData* pOldBack  = m_arrVisBuffers[m_iCurrentBackBuffer];
	LockBackBuffer();
	LockFrontBuffer();
		//Swap buffer index -> Flip bit per XOR: 0 XOR 1 = 1,1 XOR 1 = 0
		m_iCurrentBackBuffer ^= 1;
		VveDataContainer<TRawVisData>::m_pData = pOldBack;
	UnlockFrontBuffer();
	UnlockBackBuffer();
	this->Notify();
}
#endif // _VVEDBDATACONTAINER_H

#endif // #if 0
