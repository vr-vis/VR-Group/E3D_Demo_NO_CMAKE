/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// VistaFlowLib includes
#include "VveTimeHistogram.h"

#include <VistaBase/VistaStreamUtils.h>

#include "../Tools/Vve2DHistogram.h"
#include "../Tools/VveHistogram.h"
#include <iostream>
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveTimeHistogram::VveTimeHistogram(VveTimeMapper *pTM) 
	:	VveUnsteadyData(pTM),
		m_pTarget(new Vve2DHistogram),
		m_iLastLevel(-1),
		m_pLastHist(new VveHistogram)
{
	//set a default resolution (at least for the number of time levels
	//this makes sense
	int iRes[2] = {pTM->GetNumberOfTimeLevels() + 2, 100};
	m_pTarget->SetResolution(iRes);
}

VveTimeHistogram::~VveTimeHistogram()
{
	delete m_pTarget;
	delete m_pLastHist;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
VveTimeHistogram &VveTimeHistogram::operator=(const VveTimeHistogram& other)
{
	const int T=this->GetTimeMapper()->GetNumberOfTimeLevels();
	if(other.GetTimeMapper()->GetNumberOfTimeLevels() != T)
		return *this;
	this->SetHistogram(other.GetHistogram());
	return (*this);
}
/**
*
*/
void VveTimeHistogram::SetHistogram(const Vve2DHistogram *pHistogram)
{
	int iRes[2];
	pHistogram->GetResolution(iRes);
	//make sure this matches the number of time levels!
	//NOTE: we do not count the two extra bins for outliers here!
	if(iRes[0]-2 != this->GetTimeMapper()->GetNumberOfTimeLevels())
	{
		vstr::errp() << "[VveTimeHistogram::SetHistogram] Histogram resolution does not match time resolution!" << endl;
		return;
	}
	//NOTE: we manage our internal copy here for ourselves -> this nobody needs
	//create and delete that stuff. However, this comes at the cost of an additional copy
	(*m_pTarget) = (*pHistogram);
	this->Notify();
}

Vve2DHistogram *VveTimeHistogram::GetHistogram() const
{
	return m_pTarget;
}
/**
*
*/
void VveTimeHistogram::GetHistogramForLevelIdx(int iLevelIdx, VveHistogram &oHist)
{
	assert(m_pTarget != NULL);
	
	//use cached values if possible
	if(m_iLastLevel != iLevelIdx)
	{
		//NOTE: in column 0 we got the outliers!
		m_pTarget->GetColumn(iLevelIdx+1, *m_pLastHist);
		m_iLastLevel = iLevelIdx;
	}
	oHist = *m_pLastHist;
}

/**
*
*/
void VveTimeHistogram::GetHistogramForSimTime(double fSimTime, VveHistogram &oHist)
{
	double fVisTime = this->GetTimeMapper()->GetVisualizationTime(fSimTime);
	this->GetHistogramForVisTime(fVisTime, oHist);
}

/**
*
*/
void VveTimeHistogram::GetHistogramForVisTime(double fVisTime, VveHistogram &oHist)
{
	int iLevelIdx = this->GetTimeMapper()->GetLevelIndex(fVisTime);
	this->GetHistogramForLevelIdx(iLevelIdx, oHist);
}

/**
*
*/
void VveTimeHistogram::GetBinCountsForLevelIdx(int iLevelIdx, std::vector<int>& vecCounts)
{
	assert(m_pTarget != NULL);
	
	//use cached values if possible
	if(m_iLastLevel != iLevelIdx)
	{
		//NOTE: in column 0 we got the outliers!
		m_pTarget->GetColumn(iLevelIdx+1, *m_pLastHist);
		m_iLastLevel = iLevelIdx;
	}

	vecCounts.resize(m_pLastHist->GetNumBins()-2);
	for(int i=1; i<m_pLastHist->GetNumBins()-1; ++i)
		vecCounts[i-1] = m_pLastHist->GetBinCount(i);
}

/**
*
*/
void VveTimeHistogram::GetBinCountsForSimTime(double fSimTime, std::vector<int>& vecCounts)
{
	double fVisTime = this->GetTimeMapper()->GetVisualizationTime(fSimTime);
	this->GetBinCountsForVisTime(fVisTime, vecCounts);
}

/**
*
*/
void VveTimeHistogram::GetBinCountsForVisTime(double fVisTime, std::vector<int>& vecCounts)
{
	int iLevelIdx = this->GetTimeMapper()->GetLevelIndex(fVisTime);
	this->GetBinCountsForLevelIdx(iLevelIdx, vecCounts);
}

/**
*
*/
void VveTimeHistogram::GetNormalizedCountsForLevelIdx(int iLevelIdx, std::vector<float>& vecDensity)
{
	assert(m_pTarget != NULL);
	
	//use cached values if possible
	if(m_iLastLevel != iLevelIdx)
	{
		//NOTE: in column 0 we got the outliers!
		m_pTarget->GetColumn(iLevelIdx+1, *m_pLastHist);
		m_iLastLevel = iLevelIdx;
	}

	float f1DivMax = 1.0f/(float) m_pTarget->GetMaxBinCount();

	vecDensity.resize(m_pLastHist->GetNumBins()-2);
	for(int i=1; i<m_pLastHist->GetNumBins()-1; ++i)
		vecDensity[i-1] = f1DivMax * (float) m_pLastHist->GetBinCount(i);
}
/**
*
*/
void VveTimeHistogram::GetNormalizedCountsForSimTime(double fSimTime, std::vector<float>& vecDensity)
{
	double fVisTime = this->GetTimeMapper()->GetVisualizationTime(fSimTime);
	this->GetNormalizedCountsForVisTime(fVisTime, vecDensity);
}
/**
*
*/
void VveTimeHistogram::GetNormalizedCountsForVisTime(double fVisTime, std::vector<float>& vecDensity)
{
	int iLevelIdx = this->GetTimeMapper()->GetLevelIndex(fVisTime);
	this->GetNormalizedCountsForLevelIdx(iLevelIdx, vecDensity);
}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
