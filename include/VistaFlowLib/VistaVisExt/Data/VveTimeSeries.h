/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVETIMESERIES_H
#define _VVETIMESERIES_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveUnsteadyData.h"
#include <VistaAspects/VistaSerializable.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VveTimeSeries is a simple wrapper for a piecewise linear 1D function over
 * the time axis, i.e. a function curve of some scalar value over time.
 */
class VISTAVISEXTAPI VveTimeSeries : public VveUnsteadyData, public IVistaSerializable
{
public:    
	/**
	*
	*/
	VveTimeSeries(VveTimeMapper *pMapper);

	/**
	*
	*/
	virtual ~VveTimeSeries();

	VveTimeSeries& operator=(const VveTimeSeries& other);
	
	/**
	 * set the complete bunch of values with a single call
	 * NOTE: It is not neccessary to set the number of entries in the 
	 *		 float vector because it is assumed that it matches the number of
	 *		 TIME LEVELS in the underlying time mapper. 
	 *		 It is YOUR RESPONSIBILITY that this assumption holds!
	 *		 The array will be addressed via level indices.
	 */
	void SetValues(const float fVals[]);
	void SetValues(const double dVals[]);
	void SetValues(const std::vector<float>& vecVals);

	void SetValueForLevelIdx(int iLevelIdx, float f);

	float GetValueForLevelIdx(int iLevelIdx) const;
	float GetValueForVisTime(double fVisTime) const;
	float GetValueForSimTime(double fSimTime) const;
	
	/**
	 * convenience resetting stuff
	 */
	void ResetToNull();
	void ResetToMax();
	void ResetToMin();

	void GetRange(float fRange[2]) const;

	/**
	 * overloaded operator for raw data access via level index
	 */
	float &operator[](unsigned int iLevelIdx);
	
	float GetInterpolValueForVisTime(double fVisTime) const;
	float GetInterpolValueForSimTime(double fSimTime) const;

	virtual int Serialize(IVistaSerializer &oSer) const;
	virtual int DeSerialize(IVistaDeSerializer &oDeSer);
	virtual std::string GetSignature() const;

protected:  

private:
	float *m_pValues;
};

/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VVETIMESERIES_H

