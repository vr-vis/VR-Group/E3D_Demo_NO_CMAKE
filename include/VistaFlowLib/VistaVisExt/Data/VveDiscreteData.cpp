/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VveDiscreteData.cpp          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// VistaFlowLib includes
#include "VveDiscreteData.h"
#include "VveDataItem.h"

#include <VistaBase/VistaStreamUtils.h>

#include <iostream>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveDiscreteData::VveDiscreteData(VveTimeMapper *pTM) : VveUnsteadyData(pTM)
{
}

VveDiscreteData::~VveDiscreteData()
{

	vstr::debugi() << " [VveDiscreteData] - being destroyed..." << endl;

}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/* NAME: SetLevelData                                                         */
/*                                                                            */
/*============================================================================*/
bool VveDiscreteData::SetLevelData(int iIndex, IVveDataItem *pData)
{
	// Do we have any data?
	if(!pData)
		return false;

	// Do we have a valid time index?
	// @todo Looks awful.
	const size_t nNumTimeLevels = static_cast<size_t>(
		m_pTimeMapper->GetNumberOfTimeLevels());
	
	if(iIndex < 0 || iIndex >= static_cast<int>(nNumTimeLevels))
		return false;

	// Do we have to allocate additional data entries?
	if(m_vecData.size() < nNumTimeLevels)
	{
		// Cache the old size for later use ...
		const size_t nOldSize = m_vecData.size();
		// ... and resize the data vector to its new target size.
		m_vecData.resize(nNumTimeLevels);
		
		// Init all uninit'ed data entries with (semantically illegal(!)) NULL
		// pointers. We'll use the cached old size to avoid unneccesary inits
		// and overwrites of existing entries.
		for(size_t i=nOldSize; i<nNumTimeLevels; ++i)
			m_vecData[i] = NULL;
	}

	// Make sure, we don't overwrite data.
	if(m_vecData[iIndex])
		return false;

	m_vecData[iIndex] = pData;

	//Notify(MSG_SETLEVELDATA_CHANGE);
	return true;
}

IVveDataItem *VveDiscreteData::RemLevelData(unsigned int nIndex)
{
	unsigned int n=0;
	std::vector<IVveDataItem*>::iterator it = m_vecData.end();
	for(it = m_vecData.begin();
		it != m_vecData.end(); ++it, ++n)
	{
		if(n == nIndex)
			break;
	}

	IVveDataItem *pOldItem = NULL;
	if(it != m_vecData.end())
	{
		pOldItem = *it;
		m_vecData.erase(it);
		//Notify(MSG_REMLEVELDATA);
	}

	return pOldItem;

}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
