/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEPARTICLEDATAARRAY_H
#define _VVEPARTICLEDATAARRAY_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaVisExt/VistaVisExtConfig.h>
#include <vector>
#include <string>

#include <vtkType.h>
#include <limits>
#include <cassert>
#include <algorithm>

/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveParticleDataArrayBase
{    
public:

    virtual ~VveParticleDataArrayBase();

    // Get the type of the base array
    virtual int GetTypeId() const = 0;

    // Get the name of this data array
    std::string GetName() const;

    // Get the number of components of the used type
    size_t GetNumComponents() const;

/*============================================================================*/
/* Pure Virtual Functions                                                     */
/*============================================================================*/

    // Get the number of elements in the data array
    virtual size_t GetSize() const = 0;

    // Resize the data array to a specific size, either by dropping elements 
    // or by inserting new elements at the end of the array
    virtual void Resize( size_t nCount ) = 0;

    // Allocate memory the store iCount many data items
    virtual void Reserve( size_t nCount ) = 0;

    // Test whether the data array is empty
    virtual bool IsEmpty() const = 0;

    // Clear the data array
    virtual void Clear() = 0;

    // Get the bounds of the elements in the data array
    // Given arrays must be initialized with the correct length,
    // according to GetNumComponents()!
    virtual void GetBounds( float* aMinElement, 
                            float* aMaxElement ) = 0;

    // Set the the value of all components of an Element at once
    virtual void SetElementValue( size_t nElementIndex, float fVal ) = 0;

    // Set the the value of all components of an Element at once
    virtual void SetElementValue( size_t nElementIndex, double dVal ) = 0;

    // Set the value of an element
    virtual void SetElement( size_t nElementIndex, const float* aVal ) = 0;

    // Set the value of an element
    virtual void SetElement( size_t nElementIndex, const double* aVal ) = 0;

    // Get the value of an element with single component as float
    virtual float GetElementValueAsFloat( size_t nElementIndex ) const = 0;

    // Get the value of an element with single component as double
    virtual double GetElementValueAsDouble( size_t nElementIndex ) const = 0;

    // Copy an element into a float array
    virtual int GetElementCopy( size_t nElementIndex, float* aDest, size_t nComponents = 3 ) const = 0;

    // Copy an element into a double array
    virtual int GetElementCopy( size_t nElementIndex, double* aDest, size_t nComponents = 3) const = 0;

    // Create new array with the same signature
    virtual VveParticleDataArrayBase* CreateInstance() = 0;
 
    // Copy the element at position and append to the destination array
    virtual void CopyElementAt( size_t nPosition, 
                                VveParticleDataArrayBase* pDest ) = 0;

	// Interpolate between given positions and add to destination array
	virtual void InterpolateElement( size_t nPos1, size_t nPos2, 
                                     double dAlpha,
                                     VveParticleDataArrayBase* pDest ) = 0;

protected:
    // the name of this array
    const std::string m_sName;

    // number of components of the used type
    const size_t m_nComponents;

    VveParticleDataArrayBase( const size_t nNumComponents, const std::string &sName );
};


template<class T>
class VveParticleDataArray : public VveParticleDataArrayBase
{
public:

    VveParticleDataArray( const size_t		 nNumComponents=1, 
                          const std::string &sName="unnamed" );

    VveParticleDataArray( VveParticleDataArrayBase* other);

    virtual ~VveParticleDataArray();

    // Resize the data array to a specific size, either by dropping elements 
    // or by inserting new elements at the end of the array
    virtual void Resize( size_t nCount );

    // Allocate memory the store iCount many data items
    virtual void Reserve( size_t nCount );

    // Test whether the data array is empty
    virtual bool IsEmpty() const;

    // Clear the data array
    virtual void Clear();

    // Append an element given as array
    void AppendElement( T* pElement );
    
    // Append an element given as single value.
    // All components of the element are set to this value
    void AppendElement( T tElement );

    // Insert an element at a position within the array
    void InsertElement( size_t nPosition, T* pElement );

    // Get a typed pointer to the element at position iPosition
    T* GetElementAt( size_t nPosition );

    // Like above, but with additional range check
    int GetElementAt( size_t nPosition, T*& pElement );

    // Get a typed pointer to the data array
    T* GetData();

    virtual VveParticleDataArrayBase* CreateInstance();

    // Get the number of elements in the data array
    virtual size_t GetSize() const;

    // Set the the value of all components of an Element at once
    virtual void SetElementValue( size_t nElementIndex, double dVal );

    // Set the the value of all components of an Element at once
    virtual void SetElementValue( size_t nElementIndex, float fVal );

    // Set the value of an element
    virtual void SetElement( size_t nElementIndex, const float* aVal );

    // Set the value of an element
    virtual void SetElement( size_t nElementIndex, const double* aVal );

    // Get the value of an element with single component as float
    virtual float GetElementValueAsFloat( size_t nElementIndex ) const;

    // Get the value of an element with single component as double
    virtual double GetElementValueAsDouble( size_t nElementIndex ) const;

    // Copy an element into a float array
    virtual int GetElementCopy( size_t nElementIndex, float* aDest, size_t nComponents = 3 ) const;

    // Copy an element into a double array
    virtual int GetElementCopy( size_t nElementIndex, double* aDest, size_t nComponents = 3 ) const;

    // Get the bounds of the elements in the data array
    virtual void GetBounds(  float* aMinElement, 
                             float* aMaxElement );

    // Get the type of the base array
    virtual int GetTypeId() const;

    // Copy the element at position and append to the destination array
    virtual void CopyElementAt( size_t nPosition, VveParticleDataArrayBase* pDest );

    // Interpolate between given positions and add to destination array
    virtual void InterpolateElement( size_t nPos1, size_t nPos2, 
                                     double dAlpha,
                                     VveParticleDataArrayBase* pDest );

protected:
    
    // The std::vector containing the actual data
    std::vector<T> m_aData;

private:
    VveParticleDataArray( const VveParticleDataArray&); // Not implemented
    void operator=(const VveParticleDataArray&); // Not implemented

};

/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

template<class T>
VveParticleDataArray<T>::VveParticleDataArray( const size_t nNumComponents, 
                                               const std::string &sName )
: VveParticleDataArrayBase(nNumComponents, sName)
{ };

template<class T>
VveParticleDataArray<T>::VveParticleDataArray( VveParticleDataArrayBase* other)
: VveParticleDataArrayBase( other->GetNumComponents(), other->GetName() )
{ };

template<class T>
VveParticleDataArray<T>::~VveParticleDataArray()
{ };

/*============================================================================*/
/*  GetTypeIdByType                                                           */
/*============================================================================*/
template<>
inline int VveParticleDataArray<float>::GetTypeId() const
{
    return VTK_FLOAT;
};

template<>
inline int VveParticleDataArray<int>::GetTypeId() const
{
	return VTK_INT;
};

template<>
inline int VveParticleDataArray<double>::GetTypeId() const
{
	return VTK_DOUBLE;
};



/*============================================================================*/
/*                                                                            */
/* NAME: Copy                                                                 */
/*                                                                            */
/*============================================================================*/

template<class T>
VveParticleDataArrayBase* VveParticleDataArray<T>::CreateInstance()
{
    return new VveParticleDataArray<T>(this);
}


/*============================================================================*/
/*                                                                            */
/* NAME: GetSize                                                              */
/*                                                                            */
/*============================================================================*/

template<class T>
inline size_t VveParticleDataArray<T>::GetSize() const
{ 
    return (static_cast<size_t>(this->m_aData.size()) / GetNumComponents()); 
}

/*============================================================================*/
/*                                                                            */
/* NAME: Resize                                                               */
/*                                                                            */
/*============================================================================*/

template<class T>
inline void VveParticleDataArray<T>::Resize( size_t nCount )
{ 
    this->m_aData.resize( nCount * GetNumComponents() ); 
}

/*============================================================================*/
/*                                                                            */
/* NAME: Reserve                                                              */
/*                                                                            */
/*============================================================================*/

template<class T>
inline void VveParticleDataArray<T>::Reserve( size_t nCount )
{ 
    this->m_aData.reserve( nCount * GetNumComponents() ); 
}

/*============================================================================*/
/*                                                                            */
/* NAME: IsEmpty                                                              */
/*                                                                            */
/*============================================================================*/

template<class T>
inline bool VveParticleDataArray<T>::IsEmpty() const
{ 
    return this->m_aData.empty(); 
}

/*============================================================================*/
/*                                                                            */
/* NAME: Clear                                                                */
/*                                                                            */
/*============================================================================*/

template<class T>
inline void VveParticleDataArray<T>::Clear()
{ 
    this->m_aData.clear(); 
}

/*============================================================================*/
/*                                                                            */
/* NAME: AddElement                                                           */
/*                                                                            */
/*============================================================================*/

template<class T>
inline void VveParticleDataArray<T>::AppendElement( T* pElement )
{
    for( size_t i=0; i<GetNumComponents(); i++ )
    {
        this->m_aData.push_back( pElement[i] ); 
    }
}

template<class T>
inline void VveParticleDataArray<T>::AppendElement( T tElement )
{ 
    for( size_t i=0; i<GetNumComponents(); i++ )
    {
        this->m_aData.push_back( tElement ); 
    }
}

/*============================================================================*/
/*                                                                            */
/* NAME: InsertElement                                                        */
/*                                                                            */
/*============================================================================*/

template<class T>
inline void VveParticleDataArray<T>::InsertElement( size_t nPosition, T* pElement )
{
    // calculate position in array
    size_t nOffset = nPosition*GetNumComponents();

    // allocate memory at given position
    this->m_aData.insert( m_aData.begin() + nOffset, T() );

    // copy values to allocated memory
    for( size_t i=0; i<GetNumComponents(); i++ )
    {
        this->m_aData[nOffset+i] = pElement[i]; 
    }

}

/*============================================================================*/
/*                                                                            */
/* NAME: GetElementAt                                                         */
/*                                                                            */
/*============================================================================*/

template<class T>
inline T* VveParticleDataArray<T>::GetElementAt( size_t nPosition )
{ 
	return &(m_aData[nPosition*this->GetNumComponents()]); 
}

template<class T>
inline int VveParticleDataArray<T>::GetElementAt( size_t nPosition, T*& pElement )
{
    if( nPosition >= this->GetSize() )
		return 0;
    
	pElement = this->GetElementAt(nPosition);
    return 1;
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetElementCopy                                                       */
/*                                                                            */
/*============================================================================*/

template<class T>
inline int VveParticleDataArray<T>::GetElementCopy( size_t nElementIndex, 
                                                    float* aDest, 
                                                    size_t nComponents ) const
{
	assert(aDest != NULL);

    const size_t nMinComp = std::min(nComponents, this->GetNumComponents());

	const T* data = &m_aData[nElementIndex*this->GetNumComponents()];
    
	for( size_t i=0; i<nMinComp; ++i )
		aDest[i] = static_cast<float>(data[i]);
	return 1;
}

template<class T>
inline int VveParticleDataArray<T>::GetElementCopy( size_t nElementIndex, 
                                                    double* aDest,
                                                    size_t nComponents ) const
{
	assert(aDest != NULL);

	const size_t nMinComp = std::min(nComponents, this->GetNumComponents());

	const T* data = &m_aData[nElementIndex*this->GetNumComponents()];

	for( size_t i=0; i<nMinComp; ++i )
		aDest[i] = static_cast<double>(data[i]);
	return 1;
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetElementValue                                                      */
/*                                                                            */
/*============================================================================*/

template<class T>
inline float VveParticleDataArray<T>::GetElementValueAsFloat( size_t nElementIndex ) const
{
    return static_cast<float>
                (this->m_aData.at( nElementIndex * GetNumComponents()));
}

template<class T>
inline double VveParticleDataArray<T>::GetElementValueAsDouble( size_t nElementIndex ) const
{
    // return the first component of the requested element
    return static_cast<double>
                (this->m_aData.at( nElementIndex * GetNumComponents()));
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetData                                                              */
/*                                                                            */
/*============================================================================*/

template<class T>
inline T* VveParticleDataArray<T>::GetData()
{ 
    return &(this->m_aData[0]);
}

/*============================================================================*/
/*                                                                            */
/* NAME: SetElementValue                                                      */
/*                                                                            */
/*============================================================================*/

template<class T>
inline void VveParticleDataArray<T>::SetElementValue( size_t nElementIndex, float fVal )
{
    T* aElement = NULL;
    if( GetElementAt(nElementIndex, aElement))
    {
        for( size_t i=0; i<GetNumComponents(); ++i )
            aElement[i] = static_cast<T>(fVal);
    }
}

template<class T>
inline void VveParticleDataArray<T>::SetElementValue( size_t nElementIndex, double dVal )
{
    T* aElement = NULL;
    if( GetElementAt(nElementIndex, aElement))
    {
        for( size_t i=0; i<GetNumComponents(); ++i )
            aElement[i] = static_cast<T>(dVal);
    }
}
/*============================================================================*/
/*                                                                            */
/* NAME: SetElement                                                           */
/*                                                                            */
/*============================================================================*/

template<class T>
void VveParticleDataArray<T>::SetElement( size_t nElementIndex, const float* aVal )
{
    T* aElement = NULL;
    if( GetElementAt(nElementIndex, aElement))
    {
        for( size_t i=0; i<GetNumComponents(); ++i )
            aElement[i] = static_cast<T>(aVal[i]);
    }
}

template<class T>
void VveParticleDataArray<T>::SetElement( size_t nElementIndex, const double* aVal )
{
    T* aElement = NULL;
    if( GetElementAt(nElementIndex, aElement))
    {
        for( size_t i=0; i<GetNumComponents(); ++i )
            aElement[i] = static_cast<T>(aVal[i]);
    }
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetBounds                                                            */
/*                                                                            */
/*============================================================================*/

template<class T>
void VveParticleDataArray<T>::GetBounds( float* aMinElement, float* aMaxElement )
{
    if (!aMinElement || !aMaxElement)
        return;

    const size_t nComponents = GetNumComponents();

    // initialize
    if( GetSize() == 0 )
    {
        for( size_t i=0; i<nComponents; ++i )
        {
            aMinElement[i] = static_cast<float>(0);
            aMaxElement[i] = static_cast<float>(0);
        }
        return;
    }
    else
    {
        for( size_t i=0; i<nComponents; ++i )
        {
            aMinElement[i] = std::numeric_limits<float>::max();
            aMaxElement[i] = -std::numeric_limits<float>::max();
        }
    }

    // calculate the minimum and maximum value for each component
    for( size_t j=0; j<GetSize(); ++j )
    {
        T* aElement = GetElementAt(j);

        for( size_t i=0; i<nComponents; ++i )
        {
            float fVal = static_cast<float>(aElement[i]);

            if( fVal < aMinElement[i])
                aMinElement[i] = fVal;

            if( fVal > aMaxElement[i])
                aMaxElement[i] = fVal;
        }
    }
}

/*============================================================================*/
/*                                                                            */
/* NAME: AppendElement                                                          */
/*                                                                            */
/*============================================================================*/

template<class T>
void VveParticleDataArray<T>::
CopyElementAt( size_t nPosition, VveParticleDataArrayBase* pDest )
{
    VveParticleDataArray<T> *pDestTyped = 
        dynamic_cast<VveParticleDataArray<T> *>(pDest);

    if (NULL != pDestTyped && nPosition < this->GetSize())
    {
        pDestTyped->AppendElement(this->GetElementAt(nPosition));
    }
}

/*============================================================================*/
/*                                                                            */
/* NAME: InterpolateElement                                                   */
/*                                                                            */
/*============================================================================*/

template<class T>
void VveParticleDataArray<T>::InterpolateElement( size_t nPos1, size_t nPos2, 
												  double dAlpha,
												  VveParticleDataArrayBase* pDest )
{
    VveParticleDataArray<T> *pDestTyped = 
        dynamic_cast<VveParticleDataArray<T> *>(pDest);

    if (NULL != pDestTyped && 
        nPos1 >= 0 && nPos1 < this->GetSize() && 
        nPos2 >= 0 && nPos2 < this->GetSize())
    {
        // get elements to be interpolated
        T* pElement1 = this->GetElementAt(nPos1);
        T* pElement2 = this->GetElementAt(nPos2);

        // allocate mem
        T* pResult   = new T[GetNumComponents()];

        // interpolate component-wise
        for( size_t i=0; i<GetNumComponents(); ++i )
        {
            pResult[i] = 
                static_cast<T>((1-dAlpha)*pElement1[i] + dAlpha*pElement2[i]);
        }

        pDestTyped->AppendElement(pResult);
    }
}


#endif // _VVEPARTICLEDATAARRAY_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

