/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// VistaFlowLib includes
#include "VveTimeSeries.h"

#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaBase/VistaBaseTypes.h>

#include <limits>
#include <cassert>
#include <cstring>
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;
using namespace VistaType;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveTimeSeries::VveTimeSeries(VveTimeMapper *pTM)
	:	VveUnsteadyData(pTM)
{
	m_pValues = new float[this->GetTimeMapper()->GetNumberOfTimeLevels()];
}

VveTimeSeries::~VveTimeSeries()
{
	delete[] m_pValues;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
VveTimeSeries& VveTimeSeries::operator=(const VveTimeSeries& other)
{
	const int T = this->GetTimeMapper()->GetNumberOfTimeLevels();
	if(other.GetTimeMapper()->GetNumberOfTimeLevels() != T)
		return *this;
	memcpy(m_pValues, other.m_pValues, T*sizeof(m_pValues[0]));

	this->Notify();

	return *this;
}

void VveTimeSeries::SetValues(const float fVals[])
{
	const int N=this->GetTimeMapper()->GetNumberOfTimeLevels();
	memcpy(m_pValues, fVals, N*sizeof(m_pValues[0]));
	this->Notify();
}

void VveTimeSeries::SetValues(const double dVals[])
{
	const int N=this->GetTimeMapper()->GetNumberOfTimeLevels();
	for(register int i=0; i<N; ++i)
		m_pValues[i] = (float)dVals[i];
	this->Notify();
}

void VveTimeSeries::SetValues(const std::vector<float>& vecVals)
{
	assert(vecVals.size() == this->GetTimeMapper()->GetNumberOfTimeLevels());
	this->SetValues(&(vecVals[0]));
}

void VveTimeSeries::SetValueForLevelIdx(int iLevelIdx, float f)
{
	assert(iLevelIdx>=0 && iLevelIdx < this->GetTimeMapper()->GetNumberOfTimeLevels());
	m_pValues[iLevelIdx] = f;
	this->Notify();
}

float VveTimeSeries::GetValueForLevelIdx(int iLevelIdx) const
{
	assert(iLevelIdx>=0 && iLevelIdx < this->GetTimeMapper()->GetNumberOfTimeLevels());
	return m_pValues[iLevelIdx];
}

float VveTimeSeries::GetValueForVisTime(double fVisTime) const
{
	const int iLevelIdx = this->GetTimeMapper()->GetLevelIndex(fVisTime);
	assert(iLevelIdx>=0 && iLevelIdx < this->GetTimeMapper()->GetNumberOfTimeLevels());
	return m_pValues[iLevelIdx];
}

float VveTimeSeries::GetValueForSimTime(double fSimTime) const
{
	const double fVisTime = this->GetTimeMapper()->GetVisualizationTime(fSimTime);
	return this->GetValueForVisTime(fVisTime);
}

void VveTimeSeries::ResetToNull()
{
	const int T = this->GetTimeMapper()->GetNumberOfTimeLevels();
	for(register int t=0; t<T; ++t)
		m_pValues[t] = 0.0f;
}

void VveTimeSeries::ResetToMin()
{
	const int T = this->GetTimeMapper()->GetNumberOfTimeLevels();
	for(register int t=0; t<T; ++t)
		m_pValues[t] = -std::numeric_limits<float>::max();
}

void VveTimeSeries::ResetToMax()
{
	const int T = this->GetTimeMapper()->GetNumberOfTimeLevels();
	for(register int t=0; t<T; ++t)
		m_pValues[t] = std::numeric_limits<float>::max();
}

void VveTimeSeries::GetRange(float fRange[2]) const
{
	fRange[0] =  std::numeric_limits<float>::max();
	fRange[1] = -std::numeric_limits<float>::max();

	const int T=this->GetTimeMapper()->GetNumberOfTimeLevels();
	for(register int t=0; t<T; ++t)
	{
		fRange[0] = std::min<float>(m_pValues[t], fRange[0]);
		fRange[1] = std::max<float>(m_pValues[t], fRange[1]);
	}
}

float& VveTimeSeries::operator [](unsigned int iLevelIdx)
{
	return m_pValues[iLevelIdx];
}

float VveTimeSeries::GetInterpolValueForVisTime(double dVisTime) const
{
	VveTimeMapper *pTM = this->GetTimeMapper();
	int iIdx1, iIdx2;
	float fAlpha = static_cast<float>(
		pTM->GetWeightedLevelIndices(dVisTime, iIdx1, iIdx2));
	return (1.0f-fAlpha) * m_pValues[iIdx1] + fAlpha * m_pValues[iIdx2];
}

float VveTimeSeries::GetInterpolValueForSimTime(double fSimTime) const
{
	const double fVisTime = this->GetTimeMapper()->GetVisualizationTime(fSimTime);
	return this->GetInterpolValueForVisTime(fVisTime);
}

int VveTimeSeries::Serialize(IVistaSerializer &oSer) const
{
	int iNumBytes = 0;
	int iRet = 0;
	std::string strSig = this->GetSignature();

	//write signature
	iRet = oSer.WriteInt32((VistaType::uint32)strSig.length());
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	iRet = oSer.WriteString(strSig);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write raw data
	int T=this->GetTimeMapper()->GetNumberOfTimeLevels();
	iRet = oSer.WriteInt32(T);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	iRet = oSer.WriteRawBuffer(m_pValues, T*sizeof(m_pValues[0]));
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	return iNumBytes;
}

int VveTimeSeries::DeSerialize(IVistaDeSerializer &oDeSer)
{
	int iNumBytes = 0;
	int iRet = 0;
	int iInput = 0;
	std::string strSig = this->GetSignature();
	std::string strIn;

	//read signature
	iRet = oDeSer.ReadInt32(iInput);
	if(iRet < 0 || iInput != strSig.length())
		return -1;
	iNumBytes += iRet;

	iRet = oDeSer.ReadString(strIn, iInput);
	if(iRet < 0 || strSig != strIn)
		return -1;
	iNumBytes += iRet;

	//read raw data
	int T=this->GetTimeMapper()->GetNumberOfTimeLevels();
	iRet = oDeSer.ReadInt32(iInput);
	if(iRet < 0 || iInput != T)
		return -1;
	iNumBytes += iRet;

	iRet = oDeSer.ReadRawBuffer(m_pValues, T*sizeof(m_pValues[0]));
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	return iNumBytes;
}

std::string VveTimeSeries::GetSignature() const
{
	return "__VveTimeSeries__";
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
