/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VveOctree.h"
#include "VveOctreeDFSIterator.h"

#include <VistaBase/VistaStreamUtils.h>

#include <limits>
#include <iostream>
#include <algorithm>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

VveOctree::VveOctree() : m_pRoot(new VveOctreeNode())
{
	//m_pRoot->SetType((VveOctreeNode::EChildID)7);
}
VveOctree::~VveOctree()
{
	delete m_pRoot;
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

VveOctreeNode *VveOctree::GetRoot() const
{
	return m_pRoot;
}

void VveOctree::ComputeBounds(double dBounds[6])
{
	//m_pRoot->UpdateBounds();
	m_pRoot->GetBounds(dBounds);
}

int VveOctree::ComputeNumNodes()
{
	VveOctreeDFSIterator iter(m_pRoot);
	int iNum = 1;
	while(!iter.end())
	{
		++iter;
		++iNum;
	}
	return iNum;
}

int VveOctree::ComputeMaxLevel()
{
	VveOctreeDFSIterator iter(m_pRoot);
	int iMaxLevel = 0;
	while(!iter.end())
	{
		iMaxLevel = std::max<int>(iter.GetLevel(), iMaxLevel);
		++iter;
	}
	return iMaxLevel;
}

bool VveOctree::GetLeafNodes (std::vector<VveOctreeNode*> & vecLeafs)
{
	VveOctreeDFSIterator iter(m_pRoot);
	vecLeafs.clear();
	while(!iter.end())
	{
		if (iter.GetCurrentNode() && iter.GetCurrentNode()->IsLeaf())
			vecLeafs.push_back (iter.GetCurrentNode());
		++iter;
	}
	return true;
}


VveOctreeNode::VveOctreeNode() : m_dBounds(0), m_iType(CID_INVALID), m_bDelete(false)
{
	for(int i=0; i<8; ++i)
		m_pChildren[i] = 0;
}
VveOctreeNode::~VveOctreeNode()
{
	if(!this->IsLeaf())
	{
		for(int i=0; i<8; ++i)
			delete m_pChildren[i];
	}
	if(m_dBounds)
		delete[] m_dBounds;
	if (m_bDelete)
	{
		for (unsigned int i=0; i < m_vecPts.size(); ++i)
		{
			delete [] m_vecPts[i];
			m_vecPts[i] = NULL;
		}
	}
}

void VveOctreeNode::SetDeletePositions (bool bDelete)
{
	m_bDelete = bDelete;
}
/**
* bound in vtk format (xmin,xmax,ymin,ymax,zmin,zmax)
*/
void VveOctreeNode::ComputeBounds()
{
	if(!m_dBounds)
		m_dBounds = new double[6];
	m_dBounds[0] = numeric_limits<double>::max();
	m_dBounds[1] = -numeric_limits<double>::max();
	m_dBounds[2] = numeric_limits<double>::max();
	m_dBounds[3] = -numeric_limits<double>::max();
	m_dBounds[4] = numeric_limits<double>::max();
	m_dBounds[5] = -numeric_limits<double>::max();
	double *fPt;
	for(register vector<double*>::size_type i =0; i<m_vecPts.size(); ++i)
	{
		fPt = m_vecPts[i];
		m_dBounds[0] = std::min(fPt[0], m_dBounds[0]);
		m_dBounds[1] = std::max(fPt[0], m_dBounds[1]);
		m_dBounds[2] = std::min(fPt[1], m_dBounds[2]);
		m_dBounds[3] = std::max(fPt[1], m_dBounds[3]);
		m_dBounds[4] = std::min(fPt[2], m_dBounds[4]);
		m_dBounds[5] = std::max(fPt[2], m_dBounds[5]);
	}
}

void VveOctreeNode::ClearBounds(){
	delete[] m_dBounds;
	m_dBounds = 0;
}

void VveOctreeNode::Split()
{
	if(!this->IsLeaf())
		return;
	for(int i=0; i<8; ++i)
	{
		m_pChildren[i] = new VveOctreeNode;
		m_pChildren[i]->SetType((EChildID)i);
		m_pChildren[i]->SetDeletePositions (m_bDelete);
	}
}

void VveOctreeNode::Merge()
{
	vstr::warnp() << "[VveOctreeNode::Merge] not implemented yet!" << endl;
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VVEOCTREE.CPP"                                                */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

