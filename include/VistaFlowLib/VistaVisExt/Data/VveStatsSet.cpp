/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VveStatsSet.h"
#include "VveTimeHistogram.h"
#include "VveTimeSeries.h"
#include "../Tools/VveTimeMapper.h"
#include "../Tools/Vve2DHistogram.h"
#include "../IO/VveIOUtils.h"

#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>

using namespace std;
using namespace VistaType;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveStatsSet::VveStatsSet()
	:	m_strFieldName(""),
		m_pTimeMapper(NULL),
		m_pMinSeries(NULL),
		m_pMaxSeries(NULL),
		m_pAvgSeries(NULL),
		m_pHistogram(NULL),
		m_iOriginalNumTL(0)
{
}

VveStatsSet::VveStatsSet(const std::string& strFieldName,
						   VveTimeMapper *pTM,
						   bool bCreateHist)
	:	m_strFieldName(strFieldName),
		m_pTimeMapper(NULL),
		m_pMinSeries(NULL),
		m_pMaxSeries(NULL),
		m_pAvgSeries(NULL),
		m_pHistogram(NULL),
		m_iOriginalNumTL(pTM->GetNumberOfTimeLevels())
{
	//create our own little time mapper thing here...
	VveTimeMapper::Timings oTimings;
	pTM->GetImplicitTimeMapping(oTimings);
	m_pTimeMapper = new VveTimeMapper(oTimings);

	m_pMinSeries = new VveTimeSeries(m_pTimeMapper);
	m_pMaxSeries = new VveTimeSeries(m_pTimeMapper);
	m_pAvgSeries = new VveTimeSeries(m_pTimeMapper);

	if(bCreateHist)
		m_pHistogram = new VveTimeHistogram(m_pTimeMapper);
}

VveStatsSet::~VveStatsSet()
{
	this->Cleanup();
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
void VveStatsSet::SetFieldName(const std::string &strFieldName)
{
	m_strFieldName = strFieldName;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFieldName	     				                          */
/*                                                                            */
/*============================================================================*/
string VveStatsSet::GetFieldName() const
{
	return m_strFieldName;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetMinSeries						                          */
/*                                                                            */
/*============================================================================*/
VveTimeSeries *VveStatsSet::GetMinSeries() const
{
	return m_pMinSeries;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetMax							                          */
/*                                                                            */
/*============================================================================*/
VveTimeSeries *VveStatsSet::GetMaxSeries() const
{
	return m_pMaxSeries;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetAverage	     				                          */
/*                                                                            */
/*============================================================================*/
VveTimeSeries *VveStatsSet::GetAvgSeries() const
{
	return m_pAvgSeries;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetHistogram						                          */
/*                                                                            */
/*============================================================================*/
VveTimeHistogram* VveStatsSet::GetHistogram() const
{
	return m_pHistogram;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemapTiming						                          */
/*                                                                            */
/*============================================================================*/
bool VveStatsSet::RemapTiming(const VveTimeMapper::Timings &oTimings)
{
	//make sure we do not overstrain ourselves...
	//the time series have allocated memory only for m_iOriginalNumTL many
	//time levels
	if(oTimings.m_iNumberOfLevels > m_iOriginalNumTL)
		return false;
	return m_pTimeMapper->SetByImplicitTimeMapping(oTimings);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Serialize								                      */
/*                                                                            */
/*============================================================================*/
int VveStatsSet::Serialize(IVistaSerializer & oSerializer) const
{
	int iNumBytes = 0;
	int iRet = 0;
	Vve2DHistogram *pHist;
	bool bWasCompressed = true;

	//write signature
	std::string strSig = this->GetSignature();
	iRet = oSerializer.WriteInt32((VistaType::uint32)strSig.length());
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	iRet = oSerializer.WriteString(strSig);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write time mapper
	iRet = VveIOUtils::SerializeImplicitTimeMapper(m_pTimeMapper, oSerializer);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write field name
	iRet = oSerializer.WriteInt32((VistaType::uint32)m_strFieldName.length());
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	iRet = oSerializer.WriteString(m_strFieldName);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write min series
	iRet = m_pMinSeries->Serialize(oSerializer);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write max series
	iRet = m_pMaxSeries->Serialize(oSerializer);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write avg series
	iRet = m_pAvgSeries->Serialize(oSerializer);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write histogram if any
	iRet = oSerializer.WriteBool(m_pHistogram != NULL);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	if(m_pHistogram == NULL)
		return iNumBytes;

	//Always write compressed histograms
	pHist = m_pHistogram->GetHistogram();
	if(!pHist->GetIsCompressed())
	{
		bWasCompressed = false;
		pHist->SetCompress(true);
	}
	iRet = pHist->Serialize(oSerializer);
	if(iRet<0)
		return -1;
	iNumBytes += iRet;
	if(!bWasCompressed)
	{
		pHist->SetCompress(false);
		bWasCompressed = true;
	}
	return iNumBytes;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeSerialize								                  */
/*                                                                            */
/*============================================================================*/
int VveStatsSet::DeSerialize(IVistaDeSerializer & oDeSerializer)
{
	this->Cleanup();

	int iNumBytes = 0;
	int iRet = 0;

	int iLen = 0;
	//Read signature
	std::string strSig = this->GetSignature();
	std::string strTmp;
	iRet = oDeSerializer.ReadInt32(iLen);
	if(iRet < 0 || iLen != strSig.length())
		return -1;
	iNumBytes += iRet;

	iRet = oDeSerializer.ReadString(strTmp, iLen);
	if(iRet < 0 || strTmp != strSig)
		return -1;
	iNumBytes += iRet;

	//Read time mapper
	m_pTimeMapper = new VveTimeMapper;
	iRet = VveIOUtils::DeSerializeImplicitTimeMapper(oDeSerializer, m_pTimeMapper);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;
	//mind to init this because this member holds the number
	//of time steps which have been allocated
	m_iOriginalNumTL = m_pTimeMapper->GetNumberOfTimeLevels();

	//Read field name
	iRet = oDeSerializer.ReadInt32(iLen);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	iRet = oDeSerializer.ReadString(m_strFieldName, iLen);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//Read min series
	m_pMinSeries = new VveTimeSeries(m_pTimeMapper);
	iRet = m_pMinSeries->DeSerialize(oDeSerializer);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//Read max series
	m_pMaxSeries = new VveTimeSeries(m_pTimeMapper);
	iRet = m_pMaxSeries->DeSerialize(oDeSerializer);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//Read avg series
	m_pAvgSeries = new VveTimeSeries(m_pTimeMapper);
	iRet = m_pAvgSeries->DeSerialize(oDeSerializer);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//Read histogram if any
	bool bHasHist = false;
	iRet = oDeSerializer.ReadBool(bHasHist);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	if(!bHasHist)
		return iNumBytes;

	m_pHistogram = new VveTimeHistogram(m_pTimeMapper);
	iRet = m_pHistogram->GetHistogram()->DeSerialize(oDeSerializer);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	return iNumBytes;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSignature							                      */
/*                                                                            */
/*============================================================================*/
std::string VveStatsSet::GetSignature() const
{
	return "__VveStatsSet__";
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Cleanup								                      */
/*                                                                            */
/*============================================================================*/
void VveStatsSet::Cleanup()
{
	//cleanup carefully!
	delete m_pMinSeries;
	m_pMinSeries = NULL;
	delete m_pMaxSeries;
	m_pMaxSeries = NULL;
	delete m_pAvgSeries;
	m_pAvgSeries = NULL;
	delete m_pHistogram;
	m_pHistogram = NULL;
	delete m_pTimeMapper;
	m_pTimeMapper = NULL;
	m_iOriginalNumTL = 0;
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveStatsSet.cpp"				                              */
/*============================================================================*/

