/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEPARTICLEPOPULATION_H
#define _VVEPARTICLEPOPULATION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <vector>

#include <VistaVisExt/VistaVisExtConfig.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Data/VveDataItem.h>
#include <VistaVisExt/Data/VveContinuousDataTyped.h>

/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveParticleTrajectory;

/*============================================================================*/
/* CLASS DEFINITIONS                                                         */
/*============================================================================*/

class VISTAVISEXTAPI VveParticlePopulation
{
public:

    VveParticlePopulation();
    ~VveParticlePopulation();

    // Add a VveParticleTrajectory to the particle population
    void AddTrajectory( VveParticleTrajectory* pTrajectory );

    // Get the VveParticleTrajectory at index
    VveParticleTrajectory* GetTrajectory( size_t iIndex ) const;

	// Same as above, but with range check
	bool GetTrajectory( size_t nIndex, VveParticleTrajectory*& pTraj ) const;

    // Get the number of particle trajectories
    int GetNumTrajectories() const;

	void Resize( size_t nNumTrajectories );
	void Reserve( size_t nNumTrajectories );
    void Clear();

    // reduce the particle information of the given trajectory to the time steps
    // given in the time mapper and copy it into the given particle trajectory.
    // Particle time data must be stored either as float or double in the array
    // with the given name
    void ReduceInformation( VveTimeMapper* pMapper,
                            std::string sTimeArrayName,
                            VveParticlePopulation *pDest);

    // Get the min/max values of all elements' components
    // of all trajectories which contains the given array
    void GetBounds( std::string sArrayName, 
                    float *aMinElement, 
                    float* aMaxElement,
                    size_t nComponents = 3) const;

    // default array names for mostly used particle data arrays
    // the strings are initialized in VveParticlePopulation.cpp
    const static std::string sPositionArrayDefault; /*= "positions"*/
    const static std::string sVelocityArrayDefault; /*= "velocities"*/
    const static std::string sScalarArrayDefault;   /*= "scalars"*/
    const static std::string sRadiusArrayDefault;   /*= "radius"*/
    const static std::string sTimeArrayDefault;     /*= "sim_time"*/
    const static std::string sEigenValueArrayDefault;  /*= "eigenvalues"*/
    const static std::string sEigenVectorArrayDefault; /*= "eigenvectors"*/

protected:
    // vector of references to the VveParticleDataArrays
    std::vector<VveParticleTrajectory*> m_vecTrajectories;

private:
    
    VveParticlePopulation( const VveParticlePopulation&); // Not implemented
    void operator=(const VveParticlePopulation&); // Not implemented

};

typedef VveDataItem<VveParticlePopulation> VveParticlePopulationItem;
typedef VveContinuousDataTyped<VveParticlePopulation> VveParticleDataCont;

#endif // _VVEPARTICLEPOPULATION_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

