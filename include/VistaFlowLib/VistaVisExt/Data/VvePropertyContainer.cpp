/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#include "VvePropertyContainer.h"
#include <VistaAspects/VistaAspectsUtils.h>

using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VvePropertyContainer::VvePropertyContainer()
: IVistaReflectionable()
{
}

VvePropertyContainer::~VvePropertyContainer()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetAvoidMultipleNotifications                           */
/*                                                                            */
/*============================================================================*/

string VvePropertyContainer::GetReflectionableType() const
{
	return "VvePropertyContainer";
}


int VvePropertyContainer::AddToBaseTypeList(list<string> &rBtList) const
{
	int iRet = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(VvePropertyContainer::GetReflectionableType());
	return iRet+1;
}

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/
