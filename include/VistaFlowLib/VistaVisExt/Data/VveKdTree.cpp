
//
// (c) Matthew B. Kennel, Institute for Nonlinear Science, UCSD (2004)
//
// Licensed under the Academic Free License version 1.1 found in file LICENSE
// with additional provisions in that same file.


#include "VveKdTree.h"

#include <VistaBase/VistaStreamUtils.h>

#include <algorithm> 
#include <iostream>
#include <stdio.h>

// utility
namespace
{
	inline float squared(const float x) {
		return(x*x);
	}

	inline void swap(int& a, int&b) {
		int tmp;
		tmp = a;
		a = b;
		b = tmp; 
	}

	inline void swap(float& a, float&b) {
		float tmp;
		tmp = a;
		a = b;
		b = tmp; 
	}

} // anon namespace

	//
	//       KDTREE2_RESULT implementation
	// 

static inline bool operator<(const SKdtreeResult& e1, const SKdtreeResult& e2) {
	return (e1.fDistance < e2.fDistance);
}



//
//       KDTREE2_RESULT_VECTOR implementation
// 
float VveKdTreeResultVector::max_value() {
	return( (*begin()).fDistance ); // very first element
}

void VveKdTreeResultVector::push_element_and_heapify(SKdtreeResult& e) {
	push_back(e); // what a vector does.
	std::push_heap( begin(), end() ); // and now heapify it, with the new elt.
}
float VveKdTreeResultVector::replace_maxpri_elt_return_new_maxpri(SKdtreeResult& e) {
	// remove the maximum priority element on the queue and replace it
	// with 'e', and return its priority.
	//
	// here, it means replacing the first element [0] with e, and re heapifying.

	std::pop_heap( begin(), end() );
	pop_back();
	push_back(e); // insert new
	std::push_heap(begin(), end() );  // and heapify. 
	return( (*this)[0].fDistance );
}

//
//        KDTREE2 implementation
//

// constructor
VveKdTree::VveKdTree(float* data_in,int iN, int iDim, bool rearrange_in,int dim_in)
: m_pData(data_in),
m_iNumberOfPoints  ( iN ),
m_iDim( iDim ),
m_bSortResults(false),
m_bRearrange(rearrange_in), 
m_pRearrangedData (NULL),
m_pRoot(NULL),
m_pInternalData(NULL),
m_vecTreeLeaves(m_iNumberOfPoints)
{
	//
	// initialize the constant references using this unusual C++
	// feature.
	//
	if (dim_in > 0) 
		m_iDim = dim_in;

	BuildTree();

	if (m_bRearrange) {
		// if we have a rearranged tree.
		// allocate the memory for it. 
		vstr::outi()<<"[VveKdTree] rearranging"<<std::endl; 
		delete [] m_pRearrangedData;
		m_pRearrangedData = new float [m_iNumberOfPoints*m_iDim]; 

		// permute the data for it.
		for (int i=0; i<m_iNumberOfPoints; i++) {
			for (int j=0; j<m_iDim; j++) {
				m_pRearrangedData[i*m_iDim+j] = m_pData[m_vecTreeLeaves[i]*m_iDim+j];
			}
		}
		m_pInternalData = m_pRearrangedData;
	} else {
		m_pInternalData = m_pData;
	}
}


// destructor
VveKdTree::~VveKdTree() {
	delete m_pRoot;
	delete [] m_pRearrangedData;
}

// building routines
void VveKdTree::BuildTree() {
	for (int i=0; i<m_iNumberOfPoints; i++) m_vecTreeLeaves[i] = i; 
	m_pRoot = BuildTreeForRange(0,m_iNumberOfPoints-1,NULL); 
}

VveKdTreeNode* VveKdTree::BuildTreeForRange(int l, int u, VveKdTreeNode* parent) {
	// recursive function to build 
	VveKdTreeNode* node = new VveKdTreeNode(m_iDim);
	// the newly created node. 

	if (u<l) {
		return(NULL); // no data in this node. 
	}


	if ((u-l) <= m_iBucketSize) {
		// create a terminal node. 

		// always compute true bounding box for terminal node. 
		for (int i=0;i<m_iDim;i++) {
			SpreadInCoordinate(i,l,u,node->m_vecBounds[i]);
		}

		node->m_iCutDimension = 0; 
		node->m_fCutValue = 0.0;
		node->l = l;
		node->u = u;
		node->m_pLeftNode = node->m_pRightNode = NULL;


	} else {
		//
		// Compute an APPROXIMATE bounding box for this node.
		// if parent == NULL, then this is the root node, and 
		// we compute for all dimensions.
		// Otherwise, we copy the bounding box from the parent for
		// all coordinates except for the parent's cut dimension.  
		// That, we recompute ourself.
		//
		int c = -1;
		float maxspread = 0.0;
		int m; 

		for (int i=0;i<m_iDim;i++) {
			if ((parent == NULL) || (parent->m_iCutDimension == i)) {
				SpreadInCoordinate(i,l,u,node->m_vecBounds[i]);
			} else {
				node->m_vecBounds[i] = parent->m_vecBounds[i];
			}
			float spread = node->m_vecBounds[i].fUpper - node->m_vecBounds[i].fLower; 
			if (spread>maxspread) {
				maxspread = spread;
				c=i; 
			}
		}

		// 
		// now, c is the identity of which coordinate has the greatest spread
		//

		if (false) {
			m = (l+u)/2;
			SelectOnCoordinate(c,m,l,u);  
		} else {
			float sum; 
			float average;

			if (true) {
				sum = 0.0;
				for (int k=l; k <= u; k++) {
					sum += m_pData[m_vecTreeLeaves[k]*m_iDim+c];
				}
				average = sum / static_cast<float> (u-l+1);
			} else {
				// average of top and bottom nodes.
				average = (node->m_vecBounds[c].fUpper + node->m_vecBounds[c].fLower)*0.5f; 
			}

			m = SelectOnCoordinateValue(c,average,l,u);
		}


		// move the indices around to cut on dim 'c'.
		node->m_iCutDimension=c;
		node->l = l;
		node->u = u;

		node->m_pLeftNode = BuildTreeForRange(l,m,node);
		node->m_pRightNode = BuildTreeForRange(m+1,u,node);

		if (node->m_pRightNode == NULL) {
			for (int i=0; i<m_iDim; i++) 
				node->m_vecBounds[i] = node->m_pLeftNode->m_vecBounds[i]; 
			node->m_fCutValue = node->m_pLeftNode->m_vecBounds[c].fUpper;
			node->m_fCutValueLeft = node->m_fCutValueRight = node->m_fCutValue;
		} else if (node->m_pLeftNode == NULL) {
			for (int i=0; i<m_iDim; i++) 
				node->m_vecBounds[i] = node->m_pRightNode->m_vecBounds[i]; 
			node->m_fCutValue =  node->m_pRightNode->m_vecBounds[c].fUpper;
			node->m_fCutValueLeft = node->m_fCutValueRight = node->m_fCutValue;
		} else {
			node->m_fCutValueRight = node->m_pRightNode->m_vecBounds[c].fLower;
			node->m_fCutValueLeft  = node->m_pLeftNode->m_vecBounds[c].fUpper;
			node->m_fCutValue = (node->m_fCutValueLeft + node->m_fCutValueRight) / 2.0f; 
			//
			// now recompute true bounding box as union of subtree boxes.
			// This is now faster having built the tree, being logarithmic in
			// N, not linear as would be from naive method.
			//
			for (int i=0; i<m_iDim; i++) {
				node->m_vecBounds[i].fUpper = std::max(node->m_pLeftNode->m_vecBounds[i].fUpper,
					node->m_pRightNode->m_vecBounds[i].fUpper);

				node->m_vecBounds[i].fLower = std::min(node->m_pLeftNode->m_vecBounds[i].fLower,
					node->m_pRightNode->m_vecBounds[i].fLower);
			}
		}
	}
	return(node);
}



void VveKdTree:: SpreadInCoordinate(int c, int l, int u, SInterval& interv)
{
	// return the minimum and maximum of the indexed data between l and u in
	// smin_out and smax_out.
	float smin, smax;
	float lmin, lmax;
	int i; 

	smin = m_pData[m_vecTreeLeaves[l]*m_iDim+c];
	smax = smin;


	// process two at a time.
	for (i=l+2; i<= u; i+=2) {
		lmin = m_pData[m_vecTreeLeaves[i-1]*m_iDim+c];
		lmax = m_pData[m_vecTreeLeaves[i]*m_iDim+c];

		if (lmin > lmax) {
			swap(lmin,lmax); 
			//      float t = lmin;
			//      lmin = lmax;
			//      lmax = t;
		}

		if (smin > lmin) smin = lmin;
		if (smax <lmax) smax = lmax;
	}
	// is there one more element? 
	if (i == u+1) {
		float last = m_pData[m_vecTreeLeaves[u]*m_iDim+c];
		if (smin>last) smin = last;
		if (smax<last) smax = last;
	}
	interv.fLower = smin;
	interv.fUpper = smax;
	//  printf("Spread in coordinate %d=[%f,%f]\n",c,smin,smax);
}


void VveKdTree::SelectOnCoordinate(int c, int k, int l, int u) {
	//
	//  Move indices in ind[l..u] so that the elements in [l .. k] 
	//  are less than the [k+1..u] elmeents, viewed across dimension 'c'. 
	// 
	while (l < u) {
		int t = m_vecTreeLeaves[l];
		int m = l;

		for (int i=l+1; i<=u; i++) {
			if ( m_pData[ m_vecTreeLeaves[i]*m_iDim+c] < m_pData[t*m_iDim+c]) {
				m++;
				swap(m_vecTreeLeaves[i],m_vecTreeLeaves[m]); 
			}
		} // for i 
		swap(m_vecTreeLeaves[l],m_vecTreeLeaves[m]);

		if (m <= k) l = m+1;
		if (m >= k) u = m-1;
	} // while loop
}

int VveKdTree::SelectOnCoordinateValue(int c, float alpha, int l, int u) {
	//
	//  Move indices in ind[l..u] so that the elements in [l .. return]
	//  are <= alpha, and hence are less than the [return+1..u]
	//  elmeents, viewed across dimension 'c'.
	// 
	int lb = l, ub = u;

	while (lb < ub) {
		if (m_pData[m_vecTreeLeaves[lb]*m_iDim+c] <= alpha) {
			lb++; // good where it is.
		} else {
			swap(m_vecTreeLeaves[lb],m_vecTreeLeaves[ub]); 
			ub--;
		}
	}

	// here ub=lb
	if (m_pData[m_vecTreeLeaves[lb]*m_iDim+c] <= alpha)
		return(lb);
	else
		return(lb-1);

}


// void kdtree2::dump_data() {
//   int upper1, upper2;

//   upper1 = N;
//   upper2 = dim;

//   printf("Rearrange=%d\n",rearrange);
//   printf("N=%d, dim=%d\n", upper1, upper2);
//   for (int i=0; i<upper1; i++) {
//     printf("the_data[%d][*]=",i); 
//     for (int j=0; j<upper2; j++)
//       printf("%f,",the_data[i][j]); 
//     printf("\n"); 
//   }
//   for (int i=0; i<upper1; i++)
//     printf("Indexes[%d]=%d\n",i,ind[i]); 
//   for (int i=0; i<upper1; i++) {
//     printf("data[%d][*]=",i); 
//     for (int j=0; j<upper2; j++)
//       printf("%f,",(*data)[i][j]); 
//     printf("\n"); 
//   }
// }



//
// search record substructure
//
// one of these is created for each search.
// this holds useful information  to be used
// during the search



static const float infinity = 1.0e38f;

class CSearchRecord {

private:
	friend class VveKdTree;
	friend class VveKdTreeNode;

	std::vector<float>& qv; 
	int dim;
	bool rearrange;
	unsigned int nn; // , nfound;
	float ballsize;
	int centeridx, correltime;

	VveKdTreeResultVector& result;  // results
	const float* data; 
	const std::vector<int>& ind; 
	// constructor

public:
	CSearchRecord(std::vector<float>& qv_in, const VveKdTree& tree_in,
		VveKdTreeResultVector& result_in) :  
	qv(qv_in),
		result(result_in),
		data(tree_in.m_pInternalData),
		ind(tree_in.m_vecTreeLeaves) 
	{
		dim = tree_in.m_iDim;
		rearrange = tree_in.m_bRearrange;
		ballsize = infinity; 
		nn = 0; 
	};

};


void VveKdTree::GetNNearest_BruteForce(std::vector<float>& qv, int nn, VveKdTreeResultVector& result) const
{

	result.clear();

	for (int i=0; i<m_iNumberOfPoints; i++) 
	{
		float dis = 0.0;
		SKdtreeResult e; 
		for (int j=0; j<m_iDim; j++) 
		{
			dis += squared( m_pData[i*m_iDim+j] - qv[j]);
		}
		e.fDistance = dis;
		e.iIndex = i;
		result.push_back(e);
	}
	std::sort(result.begin(), result.end() ); 

}


void VveKdTree::GetNNearest(std::vector<float>& qv, int nn, VveKdTreeResultVector& result) const
{
	CSearchRecord sr(qv,*this,result);
	std::vector<float> vdiff(m_iDim,0.0); 

	result.clear(); 

	sr.centeridx = -1;
	sr.correltime = 0;
	sr.nn = nn; 

	m_pRoot->search(sr); 

	if (m_bSortResults) 
		std::sort(result.begin(), result.end());

}
// search for n nearest to a given query vector 'qv'.


void VveKdTree::GetNNearest_AroundPoint(int idxin, int correltime, int nn,
										 VveKdTreeResultVector& result) const
{
	 std::vector<float> qv(m_iDim);  //  query vector

	 result.clear(); 

	 for (int i=0; i<m_iDim; i++) 
	 {
		 qv[i] = m_pData[idxin*m_iDim+i]; 
	 }
	 // copy the query vector.

	 CSearchRecord sr(qv, *this, result);
	 // construct the search record.
	 sr.centeridx = idxin;
	 sr.correltime = correltime;
	 sr.nn = nn; 
	 m_pRoot->search(sr); 
	
	 if(m_bSortResults) 
		 std::sort(result.begin(), result.end());
}


void VveKdTree::GetNearestByRadius(std::vector<float>& qv, float r2, VveKdTreeResultVector& result) const
{
	// search for all within a ball of a certain radius
	CSearchRecord sr(qv,*this,result);
	std::vector<float> vdiff(m_iDim,0.0); 

	result.clear(); 

	sr.centeridx = -1;
	sr.correltime = 0;
	sr.nn = 0; 
	sr.ballsize = r2; 

	m_pRoot->search(sr); 

	if (m_bSortResults) 
		std::sort(result.begin(), result.end());

}

size_t VveKdTree::GetNumberOfNeighborsWithin(std::vector<float>& qv, float r2) const
{
	// search for all within a ball of a certain radius
	VveKdTreeResultVector result; 
	CSearchRecord sr(qv,*this,result);

	sr.centeridx = -1;
	sr.correltime = 0;
	sr.nn = 0; 
	sr.ballsize = r2; 

	m_pRoot->search(sr); 
	return result.size();
}

void VveKdTree::GetNearestByRadius_AroundPoint(int idxin, int correltime, float r2,
												VveKdTreeResultVector& result) const
{
	std::vector<float> qv(m_iDim);  //  query vector

	result.clear(); 

	for (int i=0; i<m_iDim; i++) 
	{
		qv[i] = m_pData[idxin*m_iDim+i]; 
	}
	// copy the query vector.
	CSearchRecord sr(qv, *this, result);
	// construct the search record.
	sr.centeridx = idxin;
	sr.correltime = correltime;
	sr.ballsize = r2; 
	sr.nn = 0; 
	m_pRoot->search(sr); 

	if (m_bSortResults) 
		std::sort(result.begin(), result.end());
}


size_t VveKdTree::GetNumberOfNeighbors_AroundPoint(int idxin, int correltime, float r2) const
{
	std::vector<float> qv(m_iDim);  //  query vector


	for (int i=0; i<m_iDim; i++) 
	{
		qv[i] = m_pData[idxin*m_iDim+i]; 
	}
	// copy the query vector.
	VveKdTreeResultVector result; 
	CSearchRecord sr(qv, *this, result);
	// construct the search record.
	sr.centeridx = idxin;
	sr.correltime = correltime;
	sr.ballsize = r2; 
	sr.nn = 0; 
	m_pRoot->search(sr); 
	
	return result.size();
}


// 
//        KDTREE2_NODE implementation
//

// constructor
VveKdTreeNode::VveKdTreeNode(int dim) : m_vecBounds(dim) 
{ 
	m_pLeftNode = m_pRightNode = NULL; 
	//
	// all other construction is handled for real in the 
	// kdtree2 building operations.
	// 
}

// destructor
VveKdTreeNode::~VveKdTreeNode() 
{
	if (m_pLeftNode != NULL) 
		delete m_pLeftNode; 
	if (m_pRightNode != NULL) 
		delete m_pRightNode; 
	// maxbox and minbox 
	// will be automatically deleted in their own destructors. 
}


void VveKdTreeNode::search(CSearchRecord& sr) const
{
	// the core search routine.
	// This uses true distance to bounding box as the
	// criterion to search the secondary node. 
	//
	// This results in somewhat fewer searches of the secondary nodes
	// than 'search', which uses the vdiff vector,  but as this
	// takes more computational time, the overall performance may not
	// be improved in actual run time. 
	//

	if ( (m_pLeftNode == NULL) && (m_pRightNode == NULL)) 
	{
		// we are on a terminal node
		if (sr.nn == 0) 
		{
			this->process_terminal_node_fixedball(sr);
		} 
		else 
		{
			this->process_terminal_node(sr);
		}
	} 
	else 
	{
		VveKdTreeNode *ncloser, *nfarther;

		float extra;
		float qval = sr.qv[m_iCutDimension]; 
		// value of the wall boundary on the cut dimension. 
		if (qval < m_fCutValue) 
		{
			ncloser = m_pLeftNode;
			nfarther = m_pRightNode;
			extra = m_fCutValueRight-qval;
		} 
		else 
		{
			ncloser = m_pRightNode;
			nfarther = m_pLeftNode;
			extra = qval-m_fCutValueLeft; 
		};

		if (ncloser != NULL) ncloser->search(sr);

		if ((nfarther != NULL) && (squared(extra) < sr.ballsize)) 
		{
			// first cut
			if (nfarther->box_in_search_range(sr)) 
			{
				nfarther->search(sr); 
			}      
		}
	}
}


inline float dis_from_bnd(float x, float amin, float amax) {
	if (x > amax) {
		return(x-amax); 
	} else if (x < amin)
		return (amin-x);
	else
		return 0.0;

}

inline bool VveKdTreeNode::box_in_search_range(CSearchRecord& sr) const
{
	//
	// does the bounding box, represented by minbox[*],maxbox[*]
	// have any point which is within 'sr.ballsize' to 'sr.qv'??
	//

	int dim = sr.dim;
	float dis2 =0.0; 
	float ballsize = sr.ballsize; 
	for (int i=0; i<dim;i++) {
		dis2 += squared(dis_from_bnd(sr.qv[i],m_vecBounds[i].fLower,m_vecBounds[i].fUpper));
		if (dis2 > ballsize)
			return(false);
	}
	return(true);
}


void VveKdTreeNode::process_terminal_node(CSearchRecord& sr) const
{
	int centeridx  = sr.centeridx;
	int correltime = sr.correltime;
	unsigned int nn = sr.nn; 
	int dim        = sr.dim;
	float ballsize = sr.ballsize;
	//
	bool rearrange = sr.rearrange; 
	const float* data = sr.data;

	const bool debug = false;

	if (debug) {
		vstr::debugi()<<"Processing terminal node "<<l<<", "<<u<<std::endl;
		vstr::debugi() << "Query vector = [";
		for (int i=0; i<dim; i++) vstr::debug() << sr.qv[i] << ','; 
		vstr::debug() << "]\n";
		vstr::debug() << "nn = " << nn << '\n';
	}

	for (int i=l; i<=u;i++) {
		int indexofi;  // sr.ind[i]; 
		float dis;
		bool early_exit; 

		if (rearrange) {
			early_exit = false;
			dis = 0.0;
			for (int k=0; k<dim; k++) {
				dis += squared(data[i*dim+k] - sr.qv[k]);
				if (dis > ballsize) {
					early_exit=true; 
					break;
				}
			}
			if(early_exit) continue; // next iteration of mainloop
			// why do we do things like this?  because if we take an early
			// exit (due to distance being too large) which is common, then
			// we need not read in the actual point index, thus saving main
			// memory bandwidth.  If the distance to point is less than the
			// ballsize, though, then we need the index.
			//
			indexofi = sr.ind[i];
		} else {
			// 
			// but if we are not using the rearranged data, then
			// we must always 
			indexofi = sr.ind[i];
			early_exit = false;
			dis = 0.0;
			for (int k=0; k<dim; k++) {
				dis += squared(data[indexofi*dim+k] - sr.qv[k]);
				if (dis > ballsize) {
					early_exit= true; 
					break;
				}
			}
			if(early_exit) continue; // next iteration of mainloop
		} // end if rearrange. 

		if (centeridx > 0) {
			// we are doing decorrelation interval
			if (std::abs(indexofi-centeridx) < correltime) continue; // skip this point. 
		}

		// here the point must be added to the list.
		//
		// two choices for any point.  The list so far is either
		// undersized, or it is not.
		//
		if (sr.result.size() < nn) {
			SKdtreeResult e;
			e.iIndex = indexofi;
			e.fDistance = dis;
			sr.result.push_element_and_heapify(e); 
			if (debug) vstr::debugi() << "unilaterally pushed dis=" << dis;
			if (sr.result.size() == nn) ballsize = sr.result.max_value();
			// Set the ball radius to the largest on the list (maximum priority).
			if (debug) {
				vstr::debug() << " ballsize = " << ballsize << "\n"; 
				vstr::debug() << "sr.result.size() = "  << sr.result.size() << '\n';
			}
		} else {
			//
			// if we get here then the current node, has a squared 
			// distance smaller
			// than the last on the list, and belongs on the list.
			// 
			SKdtreeResult e;
			e.iIndex = indexofi;
			e.fDistance = dis;
			ballsize = sr.result.replace_maxpri_elt_return_new_maxpri(e); 
			if (debug) {
				vstr::debug() << "Replaced maximum dis with dis=" << dis << 
					" new ballsize =" << ballsize << '\n';
			}
		}
	} // main loop
	sr.ballsize = ballsize;
}

void VveKdTreeNode::process_terminal_node_fixedball(CSearchRecord& sr) const
{
	int centeridx  = sr.centeridx;
	int correltime = sr.correltime;
	int dim        = sr.dim;
	float ballsize = sr.ballsize;
	//
	bool rearrange = sr.rearrange; 
	const float* data = sr.data;

	for (int i=l; i<=u;i++) {
		int indexofi = sr.ind[i]; 
		float dis;
		bool early_exit; 

		if (rearrange) {
			early_exit = false;
			dis = 0.0;
			for (int k=0; k<dim; k++) {
				dis += squared(data[i*dim+k] - sr.qv[k]);
				if (dis > ballsize) {
					early_exit=true; 
					break;
				}
			}
			if(early_exit) continue; // next iteration of mainloop
			// why do we do things like this?  because if we take an early
			// exit (due to distance being too large) which is common, then
			// we need not read in the actual point index, thus saving main
			// memory bandwidth.  If the distance to point is less than the
			// ballsize, though, then we need the index.
			//
			indexofi = sr.ind[i];
		} else {
			// 
			// but if we are not using the rearranged data, then
			// we must always 
			indexofi = sr.ind[i];
			early_exit = false;
			dis = 0.0;
			for (int k=0; k<dim; k++) {
				dis += squared(data[indexofi*dim+k] - sr.qv[k]);
				if (dis > ballsize) {
					early_exit= true; 
					break;
				}
			}
			if(early_exit) continue; // next iteration of mainloop
		} // end if rearrange. 

		if (centeridx > 0) {
			// we are doing decorrelation interval
			if (std::abs(indexofi-centeridx) < correltime) continue; // skip this point. 
		}

		{
			SKdtreeResult e;
			e.iIndex = indexofi;
			e.fDistance = dis;
			sr.result.push_back(e);
		}

	}
}
