/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVECARTESIANGRID_H
#define _VVECARTESIANGRID_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaBase/VistaVectorMath.h>
#include "VveDataItem.h"
#include "VveDiscreteDataTyped.h"

#include <VistaVisExt/VistaVisExtConfig.h>

#include <ostream>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVistaDeSerializer;
class IVistaSerializer;
class VistaPropertyList;
class vtkImageData;
class VistaHalf;

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 * VveCartesianGrid contains information about a cartesian grid. 
 * (Whoa - imagine *that*...)
 */
class VISTAVISEXTAPI VveCartesianGrid
{
public:
	// enumerate possible data types (to be extended later...)
	enum {
		DT_INVALID	= -1,
		DT_FLOAT = 0,
		DT_HALF,
		DT_UNSIGNED_CHAR,
    DT_UNSIGNED_SHORT,
		DT_LAST
	};

	VveCartesianGrid(int iComponents = 4, int iDataType = DT_FLOAT);
	VveCartesianGrid(int iDimX, int iDimY, int iDimZ, int iComponents,
		int iDataType = DT_FLOAT, void *pData = NULL);
	virtual ~VveCartesianGrid();

	// general attributes
	bool Reset(int iDimX, int iDimY, int iDimZ, int iComponents,
		int iDataType = DT_FLOAT, void *pData = NULL);
	void GetDimensions(int &iDimX, int &iDimY, int &iDimZ) const;
	void GetDimensions(int aDims[3]) const;
	int GetComponents() const;
	int GetDataType() const;
	int GetDataByteCount() const;

	// meta data
	VistaPropertyList *GetMetaData() const;
	// manage data validity
	void SetValid (bool bValid);
	bool GetValid() const;

	// if only a subset of the data is valid (e.g. because of padding)
	// store this information as well
	void SetDataDimensions(int iDimX, int iDimY, int iDimZ);
	void SetDataDimensions(int aDims[3]);
	void GetDataDimensions(int &iDimX, int &iDimY, int &iDimZ) const;
	void GetDataDimensions(int aDims[3]) const;

	// define the boundaries of the (valid) data 
	//once in ViSTA notation (min+max) and once in VTK style (interleaved)
	void SetBounds(const VistaVector3D &v3Min, const VistaVector3D &v3Max);
	void SetBounds(float fBounds[6]);
	void SetBounds(double fBounds[6]);
	void GetBounds(VistaVector3D &v3Min, VistaVector3D &v3Max) const;
	void GetBounds(float fBounds[6]) const;
	void GetBounds(double fBounds[6]) const;

	//get the data's spacing (i.e. the dimensions of a single cell)
	void GetSpacing(float fSpacing[3]) const;
	
	
	// manage scalar range
	void SetScalarRange(float aRange[2]);
	void GetScalarRange(float aRange[2]) const;

	// data access
	void SetData(void *pData);
	void SetData(int x, int y, int z, float *pData);
	void SetData(int x, int y, int z, VistaHalf *pData);
	void SetData(int x, int y, int z, unsigned char *pData);
  void SetData(int x, int y, int z, unsigned short *pData);

	void *GetData() const;
	void GetData(int x, int y, int z, float *pData) const;
	void GetData(float x, float y, float z, float *pData) const;
	void GetData(const float aPos[3], float *pData) const;
	void GetDataNormalized(float x, float y, float z, float *pData) const;
	void GetDataNormalized(const float aPos[3], float *pData) const;
	float *GetDataAsFloat() const;

	void GetData(int x, int y, int z, VistaHalf *pData) const;
	void GetData(float x, float y, float z, VistaHalf *pData) const;
	void GetData(const float aPos[3], VistaHalf *pData) const;
	void GetDataNormalized(float x, float y, float z, VistaHalf *pData) const;
	void GetDataNormalized(const float aPos[3], VistaHalf *pData) const;
	VistaHalf *GetDataAsHalf() const;

	void GetData(int x, int y, int z, unsigned char *pData) const;
	void GetData(float x, float y, float z, unsigned char *pData) const;
	void GetData(const float aPos[3], unsigned char *pData) const;
	void GetDataNormalized(float x, float y, float z, unsigned char *pData) const;
	void GetDataNormalized(const float aPos[3], unsigned char *pData) const;
	unsigned char *GetDataAsUnsignedChar() const;

  void GetData(int x, int y, int z, unsigned short *pData) const;
	void GetData(float x, float y, float z, unsigned short *pData) const;
	void GetData(const float aPos[3], unsigned short *pData) const;
	void GetDataNormalized(float x, float y, float z, unsigned short *pData) const;
	void GetDataNormalized(const float aPos[3], unsigned short *pData) const;
	unsigned short *GetDataAsUnsignedShort() const;

	
	// retrieve data indices (nearest neighbor)
	int GetIndex(const float aPos[3]) const;
	void GetIndex(const float aPos[3], int aIndex[3]) const;
	void Dump(std::ostream &os) const;

	static int DeSerializeCartesianGrid(IVistaDeSerializer &rDeSer, 
		VveCartesianGrid &refGrid);

	static int SerializeCartesianGrid(IVistaSerializer &rSer,
		const VveCartesianGrid &refGrid);

	static int RLEDecodeCartesianGrid(IVistaDeSerializer &rDeSer, 
		VveCartesianGrid &refGrid);

	static int RLEEncodeCartesianGrid(IVistaSerializer &rSer, 
		VveCartesianGrid &refGrid);

	static void CopyCartesianGrid(VveCartesianGrid &refDest, 
		const VveCartesianGrid &refSource);

	/*
	* Convert CartesianGrid back to vtkImageData
	* As CartesianGrid does not save attribute names, there will be no valid
	* attribute names in the vtkImageData.
	* This method does not cater for Cartesian grids stored as half floats
	* or unsigned chars. In these cases, this method returns false.
	*
	*/
	static bool ConvertToImageData (VveCartesianGrid *pSource, vtkImageData* pImageData);

protected:
	bool AllocateMemory();

	VistaPropertyList *m_pMetaData;
	int	m_aDimensions[3];
	int m_aDataDimensions[3];

	int m_aStride[3];	// internal data structure: allow for easier 
						// indexing into the data

	int				m_iDataType;
	int				m_iComponents;
	VistaVector3D	m_v3BoundsMin, m_v3BoundsMax, m_v3BoundsDelta;
	int				m_iByteCount;
	bool			m_bValid;
	float			m_aRange[2];

	union
	{
		float *pf;
		VistaHalf *ph;
		unsigned char *puc;
		unsigned short *pus;
		void *pv;
	} m_oData;
};

/**
 * Here, we just typedef certain types of VveDataContainer and VveDiscreteDataTyped
 * for easier declaration of particle data objects.
 */
typedef VveDataItem<VveCartesianGrid> VveCartesianGridItem;
typedef VveDiscreteDataTyped<VveCartesianGrid> VveUnsteadyCartesianGrid;

#endif // _VVECARTESIANGRID_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

