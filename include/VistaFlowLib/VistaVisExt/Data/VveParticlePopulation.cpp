/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// VistaFlowLib includes
#include "VveParticlePopulation.h"
#include "VveParticleTrajectory.h"

#include <iostream>
#include <algorithm>
#include <functional>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
//using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*  Initialize const static members                                           */
/*============================================================================*/

const std::string VveParticlePopulation::sPositionArrayDefault    = "positions";
const std::string VveParticlePopulation::sVelocityArrayDefault    = "velocities";
const std::string VveParticlePopulation::sScalarArrayDefault      = "scalars";
const std::string VveParticlePopulation::sRadiusArrayDefault      = "radius";
const std::string VveParticlePopulation::sTimeArrayDefault        = "sim_time";
const std::string VveParticlePopulation::sEigenValueArrayDefault  = "eigenvalues";
const std::string VveParticlePopulation::sEigenVectorArrayDefault = "eigenvectors";


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/


VveParticlePopulation::VveParticlePopulation()
{
}

VveParticlePopulation::~VveParticlePopulation()
{
    // clean up
    Clear();
}

/*============================================================================*/
/*  AddTrajectory                                                             */
/*============================================================================*/

void VveParticlePopulation::AddTrajectory( VveParticleTrajectory* pTrajectory )
{
    this->m_vecTrajectories.push_back(pTrajectory);
}

/*============================================================================*/
/*  GetTrajectory                                                             */
/*============================================================================*/

VveParticleTrajectory* VveParticlePopulation::GetTrajectory( size_t nIndex ) const
{
	if(nIndex >= 0 && nIndex < m_vecTrajectories.size())
	{
		return m_vecTrajectories[nIndex];
	}
	else
	{
		return NULL;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME: GetTrajectory                                                        */
/*                                                                            */
/*============================================================================*/

bool VveParticlePopulation::GetTrajectory( size_t nIndex, VveParticleTrajectory*& pTraj ) const
{
	pTraj = GetTrajectory(nIndex);
	if(pTraj)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*============================================================================*/
/*  GetNumTrajectories                                                        */
/*============================================================================*/

int VveParticlePopulation::GetNumTrajectories() const
{
    return static_cast<int>(m_vecTrajectories.size());
}

/*============================================================================*/
/*  Resize                                                                    */
/*============================================================================*/
void VveParticlePopulation::Resize( size_t nNumTrajectories )
{
	m_vecTrajectories.resize( nNumTrajectories );
}

/*============================================================================*/
/*  Reserve                                                                    */
/*============================================================================*/
void VveParticlePopulation::Reserve( size_t nNumTrajectories )
{
	m_vecTrajectories.reserve( nNumTrajectories );
}

/*============================================================================*/
/*  Clear                                                                     */
/*============================================================================*/
void VveParticlePopulation::Clear()
{
    while (!m_vecTrajectories.empty())
    {
        delete m_vecTrajectories.back();
        m_vecTrajectories.pop_back();
    }
}

/*============================================================================*/
/*  ReduceInformation                                                         */
/*============================================================================*/

void VveParticlePopulation::ReduceInformation( VveTimeMapper* pMapper, 
                                               std::string sTimeArrayName, 
                                               VveParticlePopulation *pDest )
{
    if( !pDest )
        return;

    // clean up destination population
    pDest->Clear();

    for( size_t i=0; i<m_vecTrajectories.size(); i++)
    {
        VveParticleTrajectory* pTrajectory = new VveParticleTrajectory();

        m_vecTrajectories[i]->ReduceInformation( pMapper,
                                               sTimeArrayName, 
                                               pTrajectory );
        pDest->AddTrajectory( pTrajectory );
    }
}

/*============================================================================*/
/*  GetBounds                                                                 */
/*============================================================================*/

void VveParticlePopulation::GetBounds( std::string sArrayName, 
                                       float *aMinElement, 
                                       float* aMaxElement,
                                       size_t nComponents ) const
{
    if (!aMinElement || !aMaxElement)
        return;

    float* aMinCurr = new float[nComponents];
    float* aMaxCurr = new float[nComponents];

    // Initialize
    for( size_t i=0; i<nComponents; ++i )
    {
        aMinCurr[i] = aMinElement[i] = std::numeric_limits<float>::max();
        aMaxCurr[i] = aMaxElement[i] = -std::numeric_limits<float>::max();
    }

    VveParticleTrajectory* pTraj = NULL;
    VveParticleDataArrayBase* pArray = NULL;

    // find the array in all trajectories and calculate bounds
    for( int i=0; i<GetNumTrajectories(); ++i )
    {
        pTraj = GetTrajectory(i);
        if( pTraj && pTraj->GetArrayByName(sArrayName, pArray) && pArray 
			&& pArray->GetNumComponents() == nComponents )
        {
            pArray->GetBounds( aMinCurr, aMaxCurr);

            // compare component-wise, current with global bounds
            for( size_t j=0; j<nComponents; ++j )
            {
                if( aMinCurr[j] < aMinElement[j])
                    aMinElement[j] = aMinCurr[j];

                if( aMaxCurr[j] > aMaxElement[j])
                    aMaxElement[j] = aMaxCurr[j];
            }
        }
    }

    delete[] aMinCurr;
    delete[] aMaxCurr;

}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
