/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// include header here

#include "VveOctreeDFSIterator.h"
#include "VveOctree.h"

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/


VveOctreeDFSIterator::VveOctreeDFSIterator(VveOctreeNode *pNode)
: m_iLevel(0)
{
	this->InitIteration(pNode);
}

VveOctreeDFSIterator::~VveOctreeDFSIterator()
{
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void VveOctreeDFSIterator::InitIteration(VveOctreeNode *pNode)
{
	while(!m_stWalk.empty())
		m_stWalk.pop();
	m_stWalk.push(pNode);
	m_iLevel = 0;
	this->operator++();
}

void VveOctreeDFSIterator::operator++()
{
	if(m_stWalk.empty())
	{
		m_pCurrentNode = NULL;
		return;
	}

	m_pCurrentNode = m_stWalk.top();
	m_stWalk.pop();
	
	//we begin a new level with node 7 and end it with node 0 (remember: stack is LIFO) -> update level accordingly
	if(m_pCurrentNode->GetType() == 7)
		++m_iLevel;
	else if(m_pCurrentNode->GetType() == 0)
		--m_iLevel;

	if(!m_pCurrentNode->IsLeaf())
	{
		for(int i=0; i<8; ++i)
			m_stWalk.push(m_pCurrentNode->GetChild((VveOctreeNode::EChildID)i));
	}	
}

bool VveOctreeDFSIterator::end() const
{
	return m_pCurrentNode == NULL;
}

VveOctreeNode *VveOctreeDFSIterator::GetCurrentNode() const
{
	return m_pCurrentNode;
}

int VveOctreeDFSIterator::GetLevel() const
{
	return m_iLevel;
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VVEOCTREEDFSITERATOR.CPP"                                     */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/

