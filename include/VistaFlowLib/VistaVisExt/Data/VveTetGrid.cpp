/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// VistaFlowLib includes
#include "VveTetGrid.h"
#include <VistaVisExt/Algorithms/VveTetGridPointLocator.h>

#include <VistaTools/VistaFileSystemFile.h>
#include <VistaBase/VistaTimer.h>
#include <VistaBase/VistaStreamUtils.h>
#include <VistaBase/VistaSerializingToolset.h>

#include <cstring>
#include <list>
#include <map>

using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#define MAX_COMPONENTS 4

#define VEC_CROSS(dest, v1, v2) \
	dest[0] = v1[1]*v2[2]-v1[2]*v2[1]; \
	dest[1] = v1[2]*v2[0]-v1[0]*v2[2]; \
	dest[2] = v1[0]*v2[1]-v1[1]*v2[0];

#define VEC_DOT(v1, v2) \
	(v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])

#define HASH_TABLE_SIZE_MULTIPLIER (0.01f)

#define P1 73856093
#define P2 19349663
#define P3 83492791
#define HASH_KEY(pFace, iSize) \
	((unsigned int)(((pFace[0]*P1) ^ (pFace[1]*P2) ^ (pFace[2]*P3)) % iSize))


/*============================================================================*/
/*  FORWARD DECLARATIONS                                                      */
/*============================================================================*/
static inline void SortIndices(int *pFace);
static inline bool IsEqual(int *pFace1, int *pFace2);
static inline int FindNeighbor(int *pFace, int *pCell);


/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VveTetGrid::VveTetGrid()
:	m_pVertices(NULL)
,	m_pPointData(NULL)
,	m_pCells(NULL)
,	m_pCellNeighbors(NULL)
,	m_iVertexCount(0)
,	m_iPaddedVertexCount(0)
,	m_iCellCount(0)
,	m_iPaddedCellCount(0)
,	m_iComponents(MAX_COMPONENTS)
,	m_iByteCount(0)
,	m_bValid(false)
,	m_pTopologyGrid(NULL)
,	m_pLocator(NULL)
{
	memset(m_aRange, 0, 2*sizeof(float));
}

VveTetGrid::~VveTetGrid()
{
	Reset();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   [New*|Delete]Array                                          */
/*                                                                            */
/*============================================================================*/
float *VveTetGrid::NewFloatArray(int iCount)
{
	return new float[iCount];
}

int *VveTetGrid::NewIntArray(int iCount)
{
	return new int[iCount];
}

void VveTetGrid::DestroyArray(float *pData)
{
	delete [] pData;
}

void VveTetGrid::DestroyArray(int *pData)
{
	delete [] pData;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Reset                                                       */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::Reset()
{
	// clean up all data arrays
	delete [] m_pVertices;
	m_pVertices = NULL;

	delete [] m_pPointData;
	m_pPointData = NULL;

	delete [] m_pCells;
	m_pCells = NULL;

	delete [] m_pCellNeighbors;
	m_pCellNeighbors = NULL;

	m_iVertexCount = 0;
	m_iPaddedVertexCount = 0;
	m_iCellCount = 0;
	m_iPaddedCellCount = 0;
	m_iComponents = MAX_COMPONENTS;
	m_v3BoundsMin = m_v3BoundsMax = VistaVector3D();
	m_iByteCount = 0;
	m_bValid = false;
	memset(m_aRange, 0, 2*sizeof(float));
	m_pTopologyGrid = NULL;

	if (m_pLocator)
		m_pLocator->Reset();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetVertices                                             */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetVertices(float *pData)
{
	if (m_pTopologyGrid)
		m_pTopologyGrid->SetVertices(pData);
	else
		m_pVertices = pData;
}

float *VveTetGrid::GetVertices() const
{
	if (m_pTopologyGrid)
		return m_pTopologyGrid->GetVertices();
	else
		return m_pVertices;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetVertexCount                                          */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetVertexCount(int iCount)
{
	if (m_pTopologyGrid)
		m_pTopologyGrid->SetVertexCount(iCount);
	else
		m_iVertexCount = iCount;
}

int VveTetGrid::GetVertexCount() const
{
	if (m_pTopologyGrid)
		return m_pTopologyGrid->GetVertexCount();
	else
		return m_iVertexCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetPaddedVertexCount                                    */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetPaddedVertexCount(int iCount)
{
	if (m_pTopologyGrid)
		m_pTopologyGrid->SetPaddedVertexCount(iCount);
	else
		m_iPaddedVertexCount = iCount;
}

int VveTetGrid::GetPaddedVertexCount() const
{
	if (m_pTopologyGrid)
		return m_pTopologyGrid->GetPaddedVertexCount();
	else
		return m_iPaddedVertexCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetCells                                                */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetCells(int *pData)
{
	if (m_pTopologyGrid)
		m_pTopologyGrid->SetCells(pData);
	else
		m_pCells = pData;
}

int *VveTetGrid::GetCells() const
{
	if (m_pTopologyGrid)
		return m_pTopologyGrid->GetCells();
	else
		return m_pCells;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetCellCount                                            */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetCellCount(int iCount)
{
	if (m_pTopologyGrid)
		m_pTopologyGrid->SetCellCount(iCount);
	else
		m_iCellCount = iCount;
}

int VveTetGrid::GetCellCount() const
{
	if (m_pTopologyGrid)
		return m_pTopologyGrid->GetCellCount();
	else
		return m_iCellCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetPaddedCellCount                                      */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetPaddedCellCount(int iCount)
{
	if (m_pTopologyGrid)
		m_pTopologyGrid->SetPaddedCellCount(iCount);
	else
		m_iPaddedCellCount = iCount;
}

int VveTetGrid::GetPaddedCellCount() const
{
	if (m_pTopologyGrid)
		return m_pTopologyGrid->GetPaddedCellCount();
	else
		return m_iPaddedCellCount;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetCellNeighbors                                        */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetCellNeighbors(int *pData)
{
	if (m_pTopologyGrid)
		m_pTopologyGrid->SetCellNeighbors(pData);
	else
		m_pCellNeighbors = pData;
}

int *VveTetGrid::GetCellNeighbors() const
{
	if (m_pTopologyGrid)
		return m_pTopologyGrid->GetCellNeighbors();
	else
		return m_pCellNeighbors;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetPointData[Dims]                                      */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetPointData(float *pData)
{
	m_pPointData = pData;
}

float *VveTetGrid::GetPointData() const
{
	return m_pPointData;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetComponents                                           */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetComponents(int iComponents)
{
	if (iComponents < 0)
		iComponents = 0;
	if (iComponents > MAX_COMPONENTS)
		iComponents = MAX_COMPONENTS;

	m_iComponents = iComponents;
}

int VveTetGrid::GetComponents() const
{
	return m_iComponents;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetValid                                                */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetValid(bool bValid)
{
	m_bValid = bValid;
}

bool VveTetGrid::GetValid() const
{
	return m_bValid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTopologyGrid                                         */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetTopologyGrid(VveTetGrid *pGrid)
{
	m_pTopologyGrid = pGrid;
}

VveTetGrid *VveTetGrid::GetTopologyGrid() const
{
	return m_pTopologyGrid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetPointLocator                                         */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetPointLocator(VveTetGridPointLocator *pLocator)
{
	if (m_pTopologyGrid)
		m_pTopologyGrid->SetPointLocator(pLocator);
	else
	{
		if (m_pLocator)
			m_pLocator->SetTetGrid(NULL);

		m_pLocator = pLocator;
		if (pLocator)
			pLocator->SetTetGrid(this);
	}
}

VveTetGridPointLocator *VveTetGrid::GetPointLocator() const
{
	if (m_pTopologyGrid)
		return m_pTopologyGrid->GetPointLocator();
	else
		return m_pLocator;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetBounds                                               */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetBounds(const VistaVector3D &v3Min,
							  const VistaVector3D &v3Max)
{
	if (m_pTopologyGrid)
		m_pTopologyGrid->SetBounds(v3Min, v3Max);
	else
	{
		m_v3BoundsMin = v3Min;
		m_v3BoundsMax = v3Max;
	}
}

void VveTetGrid::GetBounds(VistaVector3D &v3Min,
							  VistaVector3D &v3Max) const
{
	if (m_pTopologyGrid)
		m_pTopologyGrid->GetBounds(v3Min, v3Max);
	else
	{
		v3Min = m_v3BoundsMin;
		v3Max = m_v3BoundsMax;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetScalarRange                                          */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::SetScalarRange(float aRange[2])
{
	memcpy(m_aRange, aRange, 2*sizeof(float));
}

void VveTetGrid::GetScalarRange(float aRange[2]) const
{
	memcpy(aRange, m_aRange, 2*sizeof(float));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData                                                     */
/*                                                                            */
/*============================================================================*/
int VveTetGrid::GetData(const float aPos[3], float *pData, bool bRetry) const
{
	if (!m_pLocator)
		return -1;

	int iCell = m_pLocator->GetCellId(aPos);
	if (iCell<0 && !bRetry)
		return -1;

	float aBC[4];

	if (iCell<0)
	{
		list<int> liTreeSteps, liKdSteps;
		iCell = m_pLocator->GetCellId(aPos, liTreeSteps, liKdSteps);

		list<int>::reverse_iterator rit;
		for (rit=liKdSteps.rbegin(); rit!=liKdSteps.rend() && iCell<0; ++rit)
			iCell = m_pLocator->TetWalkLimit(*rit, aPos, aBC, 1000);

		if (iCell<0)
			return -1;
	}
	else
		m_pLocator->TetWalk(iCell, aPos, aBC);

	int *pVerts = &m_pCells[4*iCell];

	for (int i=0; i<m_iComponents; ++i)
	{
		pData[i] = aBC[0]*m_pPointData[m_iComponents*pVerts[0]+i]
			+ aBC[1]*m_pPointData[m_iComponents*pVerts[1]+i]
			+ aBC[2]*m_pPointData[m_iComponents*pVerts[2]+i]
			+ aBC[3]*m_pPointData[m_iComponents*pVerts[3]+i];
	}

	return iCell;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GatherInformation                                           */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::GatherInformation()
{
#ifdef DEBUG
	vstr::debugi() << " [VveTetGrid] - gathering information..." << endl;
#endif

	if (!m_bValid || GetVertexCount() <= 0 || GetCellCount() <=0)
	{
		m_v3BoundsMin = m_v3BoundsMax = VistaVector3D();
		m_aRange[0] = m_aRange[1] = 0;
		m_iByteCount = 0;
		m_bValid = false;
	}

	ComputeByteCount();

	if (!m_pTopologyGrid && m_pVertices)
	{
		m_v3BoundsMin = m_v3BoundsMax = VistaVector3D(m_pVertices);
		for (int iIdx=1; iIdx<m_iVertexCount; ++iIdx)
		{
			float *pVertex = &m_pVertices[3*iIdx];

			if (pVertex[0] < m_v3BoundsMin[0])
				m_v3BoundsMin[0] = pVertex[0];
			if (pVertex[0] > m_v3BoundsMax[0])
				m_v3BoundsMax[0] = pVertex[0];
			if (pVertex[1] < m_v3BoundsMin[1])
				m_v3BoundsMin[1] = pVertex[1];
			if (pVertex[1] > m_v3BoundsMax[1])
				m_v3BoundsMax[1] = pVertex[1];
			if (pVertex[2] < m_v3BoundsMin[2])
				m_v3BoundsMin[2] = pVertex[2];
			if (pVertex[2] > m_v3BoundsMax[2])
				m_v3BoundsMax[2] = pVertex[2];
		}
	}
	else
		m_v3BoundsMin = m_v3BoundsMax = VistaVector3D();

	if (m_iComponents == 4 && m_pPointData)
	{
		m_aRange[0] = m_aRange[1] = m_pPointData[3];
		for (int iIdx=1; iIdx<m_iVertexCount; ++iIdx)
		{
			float *pData = &m_pPointData[4*iIdx];
			if (pData[3] < m_aRange[0])
				m_aRange[0] = pData[3];
			if (pData[3] > m_aRange[1])
				m_aRange[1] = pData[3];
		}
	}
	else
		m_aRange[0] = m_aRange[1] = 0;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeByteCount                                            */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::ComputeByteCount()
{
	m_iByteCount = 0;

	if (m_pPointData)
		m_iByteCount += m_iComponents*GetPaddedVertexCount()*sizeof(float);	// point data
	if (m_pVertices)
		m_iByteCount += 3*GetPaddedVertexCount()*sizeof(float);		// vertex positions
	if (m_pCells)
		m_iByteCount += 4*GetPaddedCellCount()*sizeof(int);			// vertex indices per cell
	if (m_pCellNeighbors)
		m_iByteCount += 4*GetPaddedCellCount()*sizeof(int);			// cell neighbors
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   PadData                                                     */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::PadData(int iVertexCount, int iCellCount)
{
	if (m_pTopologyGrid)
	{
		m_pTopologyGrid->PadData(iVertexCount, iCellCount);
	}

	if (iVertexCount > GetPaddedVertexCount())
	{
		if (m_pVertices)
		{
#ifdef DEBUG
			vstr::debugi() << " [TetraSet] - padding vertex data..." << endl;
#endif
			float *pTemp = new float[3*iVertexCount];
			memcpy(pTemp, m_pVertices, 3*m_iVertexCount*sizeof(float));
			for (int i=m_iVertexCount; i<iVertexCount; ++i)
				memcpy(&pTemp[3*i], m_pVertices, 3*sizeof(float));
			delete [] m_pVertices;
			m_pVertices = pTemp;
		}

		if (m_pPointData)
		{
#ifdef DEBUG
			vstr::debugi() << " [TetraSet] - padding point data..." << endl;
#endif
			float *pTemp = new float[m_iComponents*iVertexCount];
			memcpy(pTemp, m_pPointData, m_iComponents*m_iVertexCount*sizeof(float));
			for (int i=m_iVertexCount; i<iVertexCount; ++i)
				memcpy(&pTemp[m_iComponents*i], m_pPointData, m_iComponents*sizeof(float));
			delete [] m_pPointData;
			m_pPointData = pTemp;
		}

		SetPaddedVertexCount(iVertexCount);
	}

	if (iCellCount > GetPaddedCellCount())
	{
		if (m_pCells)
		{
#ifdef DEBUG
			vstr::debugi() << " [TetraSet] - padding cell data..." << endl;
#endif
			int *pTemp = new int[4*iCellCount];
			memcpy(pTemp, m_pCells, 4*m_iCellCount*sizeof(int));
			for (int i=m_iCellCount; i<iCellCount; ++i)
			{
				pTemp[4*i+0] = pTemp[4*i+1] = pTemp[4*i+2]
				= pTemp[4*i+3] = -1;
			}
			delete [] m_pCells;
			m_pCells = pTemp;
		}

		if (m_pCellNeighbors)
		{
			int *pTemp = new int[4*iCellCount];
			memcpy(pTemp, m_pCellNeighbors, 4*m_iCellCount*sizeof(int));
			for (int i=m_iCellCount; i<iCellCount; ++i)
			{
				pTemp[4*i+0] = pTemp[4*i+1] = pTemp[4*i+2]
					= pTemp[4*i+3] = -1;
			}
			delete [] m_pCellNeighbors;
			m_pCellNeighbors = pTemp;
		}

		SetPaddedCellCount(iCellCount);
	}

	// recompute memory usage...
	ComputeByteCount();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeCellNeighbors                                        */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::ComputeCellNeighbors()
{
	if (m_pTopologyGrid)
		return;

	VistaTimer oTimer;

	vstr::outi() << " [VveTetGrid] - computing cell neighbors..." << endl;

	// throw away old neighbor data
	delete [] m_pCellNeighbors;
	m_pCellNeighbors = new int[4*m_iPaddedCellCount];
	int *piNeighPos = m_pCellNeighbors;
	for (int i=0; i<4*m_iPaddedCellCount; ++i, ++piNeighPos)
		*piNeighPos = -1;
	piNeighPos = m_pCellNeighbors;

#if 1
	const unsigned int iTableSize((int)(HASH_TABLE_SIZE_MULTIPLIER*m_iCellCount));

	vector< list<int> > vecCells;
	vecCells.resize(iTableSize);

	int aFace[3];
	int *pCell;

	unsigned int iKey;
	bool bSuccess;
	int iNeighbor;
	list<int>::iterator it;
	for (int i=0; i<m_iCellCount; ++i)
	{
		pCell = &m_pCells[4*i];

		if (m_pCellNeighbors[4*i + 0] < 0)
		{
			aFace[0] = pCell[1];
			aFace[1] = pCell[2];
			aFace[2] = pCell[3];
			SortIndices(aFace);
			iKey = HASH_KEY(aFace, iTableSize);
			{
				list<int> &refList(vecCells[iKey]);
				bSuccess = false;
				for (it=refList.begin(); it!=refList.end(); ++it)
				{
					if (*it==i)
						continue;

					iNeighbor = FindNeighbor(aFace, &m_pCells[4*(*it)]);
					if (iNeighbor < 0)
						continue;

					// found a neighbor for cell i: (*it) is neighbor iNeighbor
					m_pCellNeighbors[4*i + 0] = *it;
					m_pCellNeighbors[4*(*it) + iNeighbor] = i;
					refList.erase(it);
					bSuccess = true;
					break;
				}
				if (!bSuccess)
				{
					vecCells[iKey].push_back(i);
				}
			}
		}

		if (m_pCellNeighbors[4*i + 1] < 0)
		{
			aFace[0] = pCell[0];
			aFace[1] = pCell[2];
			aFace[2] = pCell[3];
			SortIndices(aFace);
			iKey = HASH_KEY(aFace, iTableSize);
			{
				list<int> &refList = vecCells[iKey];
				bSuccess = false;
				for (it=refList.begin(); it!=refList.end(); ++it)
				{
					if (*it==i)
						continue;

					iNeighbor = FindNeighbor(aFace, &m_pCells[4*(*it)]);
					if (iNeighbor < 0)
						continue;

					// found a neighbor for cell i: (*it) is neighbor iNeighbor
					m_pCellNeighbors[4*i + 1] = *it;
					m_pCellNeighbors[4*(*it) + iNeighbor] = i;
					refList.erase(it);
					bSuccess = true;
					break;
				}
				if (!bSuccess)
				{
					vecCells[iKey].push_back(i);
				}
			}
		}

		if (m_pCellNeighbors[4*i + 2] < 0)
		{
			aFace[0] = pCell[0];
			aFace[1] = pCell[1];
			aFace[2] = pCell[3];
			SortIndices(aFace);
			iKey = HASH_KEY(aFace, iTableSize);
			{
				list<int> &refList = vecCells[iKey];
				bSuccess = false;
				for (it=refList.begin(); it!=refList.end(); ++it)
				{
					if (*it==i)
						continue;

					iNeighbor = FindNeighbor(aFace, &m_pCells[4*(*it)]);
					if (iNeighbor < 0)
						continue;

					// found a neighbor for cell i: (*it) is neighbor iNeighbor
					m_pCellNeighbors[4*i + 2] = *it;
					m_pCellNeighbors[4*(*it) + iNeighbor] = i;
					refList.erase(it);
					bSuccess = true;
					break;
				}
				if (!bSuccess)
				{
					vecCells[iKey].push_back(i);
				}
			}
		}

		if (m_pCellNeighbors[4*i + 3] < 0)
		{
			aFace[0] = pCell[0];
			aFace[1] = pCell[1];
			aFace[2] = pCell[2];
			SortIndices(aFace);
			iKey = HASH_KEY(aFace, iTableSize);
			{
				list<int> &refList = vecCells[iKey];
				bSuccess = false;
				for (it=refList.begin(); it!=refList.end(); ++it)
				{
					if (*it==i)
						continue;

					iNeighbor = FindNeighbor(aFace, &m_pCells[4*(*it)]);
					if (iNeighbor < 0)
						continue;

					// found a neighbor for cell i: (*it) is neighbor iNeighbor
					m_pCellNeighbors[4*i + 3] = *it;
					m_pCellNeighbors[4*(*it) + iNeighbor] = i;
					refList.erase(it);
					bSuccess = true;
					break;
				}
				if (!bSuccess)
				{
					vecCells[iKey].push_back(i);
				}
			}
		}
	}
#else
	// first step: find out, which point is used by which cell...
	vector< list<int> * > vecUsedBy;
	vecUsedBy.resize(m_iVertexCount);	// use the new (i.e. reduced) vertex set
	for (int i=0; i<m_iVertexCount; ++i)
		vecUsedBy[i] = new list<int>;

	int *piPos = m_pCells;
	for (int i=0; i<m_iCellCount; ++i)
	{
		for (int j=0; j<4; ++j, ++piPos)
		{
			if (*piPos < 0)
				continue;

			vecUsedBy[*piPos]->push_back(i);
		}
	}

	// now, find neighbors for every single cell
	list<int>::iterator it;
	for (int i=0; i<m_iCellCount; ++i)
	{
		map<int, char> mapCells;

		piPos = &m_pCells[4*i];

		// collect all neighboring cells and mark the shared vertex
		// with a bit mask
		for (int j=0; j<4; ++j, ++piPos)
		{
			int iVtx = *piPos;
			if (iVtx < 0)
				continue;

			list<int> *pCells = vecUsedBy[iVtx];

			for (it=pCells->begin(); it!=pCells->end(); ++it)
			{
				if (*it == i)
					continue;

//				if (mapCells.find(*it) == mapCells.end())
//					mapCells[*it] = 0;

				mapCells[*it] |= (1 << j);
			}
		}

		// collect 'em
		map<int, char>::iterator itCells;
		for (int j=0; j<4; ++j)
			piNeighPos[j] = -1;
		int iNeighCount = 0;

		for (itCells=mapCells.begin(); itCells!=mapCells.end()&&iNeighCount<4; ++itCells)
		{
			switch ((*itCells).second)
			{
			case 7:		// vertices 0, 1, 2 are shared
				piNeighPos[3] = (*itCells).first;
				++iNeighCount;
				break;
			case 11:	// vertices 0, 1, 3 are shared
				piNeighPos[2] = (*itCells).first;
				++iNeighCount;
				break;
			case 13:	// vertices 0, 2, 3 are shared
				piNeighPos[1] = (*itCells).first;
				++iNeighCount;
				break;
			case 14:	// vertices 1, 2, 3 are shared
				piNeighPos[0] = (*itCells).first;
				++iNeighCount;
				break;
			}
		}

		piNeighPos += 4;
	}
	for (unsigned int i=0; i<vecUsedBy.size(); ++i)
		delete vecUsedBy[i];
#endif

	vstr::outi() << " [VveTetGrid] - cell neighbor search completed..." << endl;
	vstr::outi() << "                [time needed: " << oTimer.GetLifeTime() << "]" << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBCs                                                  */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::ComputeBCs(float aBC[4], const float *pPos, int iCellIndex)
{
	float *pVertices = GetVertices();
	int *pCells = GetCells();
#if 0
	VistaVector3D x1(&pVertices[3*pCells[4*iCellIndex+0]]);
	VistaVector3D x2(&pVertices[3*pCells[4*iCellIndex+1]]);
	VistaVector3D x3(&pVertices[3*pCells[4*iCellIndex+2]]);
	VistaVector3D x4(&pVertices[3*pCells[4*iCellIndex+3]]);

	VistaVector3D v41(x4-x1);
	VistaVector3D v34(x3-x4);
	VistaVector3D v12(x1-x2);
	VistaVector3D v23(x2-x3);
	VistaVector3D v21(x2-x1);
	VistaVector3D v31(x3-x1);

	float fDet = v21.Dot(v31.Cross(v41));
	VistaVector3D vp1ByDet((VistaVector3D(pPos)-x1) / fDet);

	aBC[1] = v34.Cross(v41).Dot(vp1ByDet);
	aBC[2] = v12.Cross(v41).Dot(vp1ByDet);
	aBC[3] = v12.Cross(v23).Dot(vp1ByDet);

	aBC[0] = 1.0f - aBC[1] - aBC[2] - aBC[3];

#else
	float *x1 = &pVertices[3*pCells[4*iCellIndex+0]];
	float *x2 = &pVertices[3*pCells[4*iCellIndex+1]];
	float *x3 = &pVertices[3*pCells[4*iCellIndex+2]];
	float *x4 = &pVertices[3*pCells[4*iCellIndex+3]];

	float v41[3], v34[3], v12[3], v23[3], v21[3], v31[3];
	float vTemp[3], vp1ByDet[3];

	v41[0] = x4[0]-x1[0];
	v41[1] = x4[1]-x1[1];
	v41[2] = x4[2]-x1[2];

	v34[0] = x3[0]-x4[0];
	v34[1] = x3[1]-x4[1];
	v34[2] = x3[2]-x4[2];

	v12[0] = x1[0]-x2[0];
	v12[1] = x1[1]-x2[1];
	v12[2] = x1[2]-x2[2];

	v23[0] = x2[0]-x3[0];
	v23[1] = x2[1]-x3[1];
	v23[2] = x2[2]-x3[2];

	v21[0] = x2[0]-x1[0];
	v21[1] = x2[1]-x1[1];
	v21[2] = x2[2]-x1[2];

	v31[0] = x3[0]-x1[0];
	v31[1] = x3[1]-x1[1];
	v31[2] = x3[2]-x1[2];

	VEC_CROSS(vTemp, v31, v41);
	float f1ByDet = 1.0f / VEC_DOT(v21, vTemp);

	vp1ByDet[0] = (pPos[0]-x1[0]) * f1ByDet;
	vp1ByDet[1] = (pPos[1]-x1[1]) * f1ByDet;
	vp1ByDet[2] = (pPos[2]-x1[2]) * f1ByDet;

	VEC_CROSS(vTemp, v34, v41);
	aBC[1] = VEC_DOT(vTemp, vp1ByDet);
	VEC_CROSS(vTemp, v12, v41);
	aBC[2] = VEC_DOT(vTemp, vp1ByDet);
	VEC_CROSS(vTemp, v12, v23);
	aBC[3] = VEC_DOT(vTemp, vp1ByDet);

	aBC[0] = 1.0f - aBC[1] - aBC[2] - aBC[3];
#endif
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Dump                                                        */
/*                                                                            */
/*============================================================================*/
void VveTetGrid::Dump(ostream &os) const
{
	os << " [VveTetGrid] - vertex count: " << m_iVertexCount << endl;
	os << " [VveTetGrid] - cell count:   " << m_iCellCount << endl;
	os << " [VveTetGrid] - components:   " << m_iComponents << endl;
	os << " [VveTetGrid] - scalar range: " << m_aRange[0] << " - "
		<< m_aRange[1] << endl;
	os << " [VveTetGrid] - bounds:       " << m_v3BoundsMin
		<< " - " << m_v3BoundsMax << endl;
	os << " [VveTetGrid] - byte count:   " << m_iByteCount << endl;
	os << " [VveTetGrid] - valid:        " << (m_bValid ? "true" : "false") << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDataByteCount                                            */
/*                                                                            */
/*============================================================================*/
int VveTetGrid::GetDataByteCount() const
{
	return m_iByteCount;
}

/*============================================================================*/
/* LOCAL FUNCTIONS                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SortIndices                                                 */
/*                                                                            */
/*============================================================================*/
static inline void SortIndices(int *pFace)
{
	int iMin, iMax;
	iMin = iMax = pFace[0];
	if (pFace[1]<iMin)
		iMin = pFace[1];
	else if (pFace[1]>iMax)
		iMax = pFace[1];
	if (pFace[2]<iMin)
		iMin = pFace[2];
	else if (pFace[2]>iMax)
		iMax = pFace[2];
	if (pFace[0] > iMin && pFace[0] < iMax)
		pFace[1] = pFace[0];
	else if (pFace[2] > iMin && pFace[2] < iMax)
		pFace[1] = pFace[2];
	pFace[0] = iMin;
	pFace[2] = iMax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsEqual                                                     */
/*                                                                            */
/*============================================================================*/
static inline bool IsEqual(int *pFace1, int *pFace2)
{
	return pFace1[0]==pFace2[0] && pFace1[1]==pFace2[1]
		&& pFace1[2]==pFace2[2];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   FindNeighbor                                                */
/*                                                                            */
/*============================================================================*/
static inline int FindNeighbor(int *pFace, int *pCell)
{
	int aLocalFace[3];

	aLocalFace[0] = pCell[1];
	aLocalFace[1] = pCell[2];
	aLocalFace[2] = pCell[3];
	SortIndices(aLocalFace);
	if (IsEqual(pFace, aLocalFace))
		return 0;

	aLocalFace[0] = pCell[0];
	aLocalFace[1] = pCell[2];
	aLocalFace[2] = pCell[3];
	SortIndices(aLocalFace);
	if (IsEqual(pFace, aLocalFace))
		return 1;

	aLocalFace[0] = pCell[0];
	aLocalFace[1] = pCell[1];
	aLocalFace[2] = pCell[3];
	SortIndices(aLocalFace);
	if (IsEqual(pFace, aLocalFace))
		return 2;

	aLocalFace[0] = pCell[0];
	aLocalFace[1] = pCell[1];
	aLocalFace[2] = pCell[2];
	SortIndices(aLocalFace);
	if (IsEqual(pFace, aLocalFace))
		return 3;

	return -1;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
