/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VveIntervalTree.h            RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - ...               :   ...                                         */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEINTERVALTREE_H
#define _VVEINTERVALTREE_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaVisExt/VistaVisExtConfig.h>
#include <VistaBase/VistaBaseTypes.h>
#include <VistaAspects/VistaSerializable.h>

#include <vector>
#include <cstddef>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveIntervalTreeNode;
class VveIntervalInfo;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 *
 */
class VISTAVISEXTAPI VveIntervalTree : public IVistaSerializable
{
public:
	VveIntervalTree(const size_t iMaxDepth);
	virtual ~VveIntervalTree();

	/**
	 * Build a tree from an unordered set of intervals
	 */
	void Build(const std::vector<VveIntervalInfo> &vecIntervals);

	/**
	 * query for a given value and determine all intervals that
	 * contain this value
	 */
	size_t GetIntersectingIntervals(float fVal, 
								    std::vector<VveIntervalInfo> &vecResult) const;

	/**
	 * return the maximum number of intervals stored in a single node
	 */
	size_t GetMaxDepth() const;

	/**
	 * return the root node
	 * the root always is node 0.
	 */
	VveIntervalTreeNode GetRoot() const;
	/**
	 * return a copy of the node designated by i
	 */
	VveIntervalTreeNode GetNode(const size_t i) const;
	/**
	 * get a const ref for the given tree node
	 */
	const VveIntervalTreeNode &GetNodeConstRef(const size_t iNode) const;

	/*
	 * Get the number of all intervals
	 */
	size_t GetNumberOfIntervals(){return m_vecIntervals.size();}
	/**
	 * interval access
	 */
	VveIntervalInfo GetInterval(const size_t iInterval) const;
	
	const VveIntervalInfo& GetIntervalConstRef(const size_t iInterval);
	
	virtual int Serialize(IVistaSerializer &) const;
   
	virtual int DeSerialize(IVistaDeSerializer &);
    
	virtual std::string GetSignature() const;
private:
	std::vector<VveIntervalTreeNode> m_vecTree;   /*!< the tree's nodes with root residing at pos 0*/
	std::vector<VveIntervalInfo> m_vecIntervals;  /*!< the intervals saved in this tree*/

	size_t m_iMaxDepth;  /*!< maximum depth used for splitting termination */
	size_t m_iMaxNumIntervals; /*!< maximum number of intervals in a single node*/
	
};

/**
 * 
 */
class VISTAVISEXTAPI VveIntervalTreeNode : public IVistaSerializable
{
public:
	VveIntervalTreeNode();
	virtual ~VveIntervalTreeNode();

	/**
	 * check if 
	 * this->SplitValue < fRHS
	 */
	bool operator<(const float fRHS);
	/**
	 * check if 
	 * this->SplitValue > fRHS
	 */
	bool operator>(const float fRHS);

	/**
	 * return the split value of this node
	 * NOTE: for leaf nodes, not all intervals have
	 *		 to overlap the split value
	 */
	float GetSplitVal() const;
	void SetSplitVal(const float fSplitVal);

	/**
	 * return the id of the left child node
	 */
	VistaType::uint32 GetLeftChild() const;
	void SetLeftChild(const VistaType::uint32 val);

	/**
	 * get the id of the right child node
	 */
	VistaType::uint32 GetRightChild() const;
	void SetRightChild(const VistaType::uint32 val);

	/**
	 * return the id (offset into the interval vector) 
	 * of the first interval stored in this node
	 */
	VistaType::uint64 GetFirstIntervalId() const;
	void SetFirstIntervalId(const VistaType::uint64 val);

	/**
	 * return the number of intervals that are
	 * stored in this node
	 */
	VistaType::uint64 GetNumIntervals() const;
	void SetNumIntervals(const VistaType::uint64 val);

	/**
	 * check if this node is a leaf node
	 */
	bool IsLeaf() const;

	/**
	 *  check if node is empty
	 */
	bool IsEmpty() const;

	/**
	 * set the range altogether
	 */
	void SetRange(float fRng[2]);
	void GetRange(float fRng[2]);

	/**
	 * Check if the node's range contains the given value, i.e.
	 * if there are intervals that might contain fVal AND
	 * if the node is non-empty.
	 */
	bool HasIntevalsForVal(const float fVal) const;

	virtual int Serialize(IVistaSerializer &) const;
   
	virtual int DeSerialize(IVistaDeSerializer &);
    
	virtual std::string GetSignature() const;

private:
	float				m_fSplitVal;		/*!< The value at which the node is split */
	float				m_fRange[2];		/*!< The range over all intervals stored in this node*/
	VistaType::uint32	m_iLeftChild;		/*!< Array index of the left child node */
	VistaType::uint32	m_iRightChild;		/*!< Array index of the right child node */
	VistaType::uint64	m_iStartInterval;	/*!< Offset into the interval array. */
	VistaType::uint64	m_iNumIntervals;	/*!< Number of intervals belonging to this ndoe */
};

/**
 * 
 */
class VISTAVISEXTAPI VveIntervalInfo : public IVistaSerializable
{
public:
	VveIntervalInfo();
	VveIntervalInfo(float fRange[2], const VistaType::uint64 iCellId);
	VveIntervalInfo(float fMin, float fMax, const VistaType::uint64 iCellId);
	
	virtual ~VveIntervalInfo();

	/**
	 * just check if an interval is completely below the given rhs value, i.e.
	 * this->max < fRHS
	 */
	bool operator<(const float fRHS);
	/**
	 * just check if an interval is completely above the given rhs value, i.e.
	 * this->min > fRHS
	 */
	bool operator>(const float fRHS);

	/**
	 * return the id of the cell associated with this interval
	 */
	VistaType::uint64 GetCellId() const;

	/**
	 * return the interval's full range
	 */
	void GetRange(float fRng[2]) const;

	/**
	 * return the interval's lower bound
	 */
	float min() const;

	/**
	 * return the interval's upper bound
	 */
	float max() const;

	/**
	 * check if the given float value is contained in the interval
	 */
	bool Contains(const float fVal) const;

	/**
	 * check if two intervals intersect
	 */
	bool Intersects(const VveIntervalInfo &rOther) const;

	virtual int Serialize(IVistaSerializer &) const;
   
	virtual int DeSerialize(IVistaDeSerializer &);
    
	virtual std::string GetSignature() const;

private:
	float m_fRange[2];
	VistaType::uint64 m_iCellId;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* VveIntervalTree														      */
/*============================================================================*/
inline size_t VveIntervalTree::GetMaxDepth() const 
{ 
	return m_iMaxDepth; 
}

inline VveIntervalTreeNode VveIntervalTree::GetRoot() const
{
	return m_vecTree[0];
}

inline VveIntervalTreeNode VveIntervalTree::GetNode( const size_t i ) const
{
	return m_vecTree[i];
}


inline const VveIntervalTreeNode& VveIntervalTree::GetNodeConstRef( const size_t iNode ) const
{
	return m_vecTree[iNode];
}

inline VveIntervalInfo VveIntervalTree::GetInterval( const size_t iInterval ) const
{
	return m_vecIntervals[iInterval];
}

inline const VveIntervalInfo& VveIntervalTree::GetIntervalConstRef( const size_t iInterval )
{
	return m_vecIntervals[iInterval];
}
/*============================================================================*/
/* VveIntervalTreeNode													      */
/*============================================================================*/
inline bool VveIntervalTreeNode::operator<(const float fRHS)
{
	return m_fSplitVal < fRHS;
}

inline bool VveIntervalTreeNode::operator>(const float fRHS)
{
	return m_fSplitVal > fRHS;
}

inline float VveIntervalTreeNode::GetSplitVal() const 
{ 
	return m_fSplitVal; 
}
inline void VveIntervalTreeNode::SetSplitVal(const float val) 
{
	m_fSplitVal = val; 
}

inline VistaType::uint32 VveIntervalTreeNode::GetLeftChild() const 
{
	return m_iLeftChild; 
}
inline void VveIntervalTreeNode::SetLeftChild(const VistaType::uint32 val) 
{ 
	m_iLeftChild = val; 
}

inline VistaType::uint32 VveIntervalTreeNode::GetRightChild() const 
{ 
	return m_iRightChild; 
}
inline void VveIntervalTreeNode::SetRightChild(const VistaType::uint32 val) 
{ 
	m_iRightChild = val; 
}

inline VistaType::uint64 VveIntervalTreeNode::GetFirstIntervalId() const 
{ 
	return m_iStartInterval; 
}

inline void VveIntervalTreeNode::SetFirstIntervalId(const VistaType::uint64 val) 
{ 
	m_iStartInterval = val; 
}

inline VistaType::uint64 VveIntervalTreeNode::GetNumIntervals() const 
{ 
	return m_iNumIntervals; 
}
inline void VveIntervalTreeNode::SetNumIntervals(const VistaType::uint64 val) 
{ 
	m_iNumIntervals = val; 
}

inline bool VveIntervalTreeNode::IsLeaf() const
{
	return m_iLeftChild == 0 && m_iRightChild == 0;
}

inline bool VveIntervalTreeNode::IsEmpty() const
{
	return m_iNumIntervals == 0;
}

inline void VveIntervalTreeNode::SetRange( float fRng[2] )
{
	m_fRange[0] = fRng[0];
	m_fRange[1] = fRng[1];
}

inline void VveIntervalTreeNode::GetRange( float fRng[2] )
{
	fRng[0] = m_fRange[0];
	fRng[1] = m_fRange[1];
}

inline bool VveIntervalTreeNode::HasIntevalsForVal( const float fVal ) const
{
	return	m_iNumIntervals > 0 &&
			m_fRange[0] <= fVal  &&
			m_fRange[1] >= fVal;
}



/*============================================================================*/
/* VveIntervalInfo														      */
/*============================================================================*/
inline bool VveIntervalInfo::operator<( const float fRHS )
{
	return m_fRange[1] <= fRHS;
}

inline bool VveIntervalInfo::operator>( const float fRHS )
{
	return m_fRange[0] >= fRHS;
}

inline VistaType::uint64 VveIntervalInfo::GetCellId() const
{
	return m_iCellId;
}

inline void VveIntervalInfo::GetRange( float fRng[2] ) const
{
	fRng[0] = m_fRange[0];
	fRng[1] = m_fRange[1];
}

inline float VveIntervalInfo::min() const
{
	return m_fRange[0];
}

inline float VveIntervalInfo::max() const
{
	return m_fRange[1];
}

inline bool VveIntervalInfo::Contains( const float fVal ) const
{
	return m_fRange[0] <= fVal && fVal <= m_fRange[1];
}

inline bool VveIntervalInfo::Intersects( const VveIntervalInfo &rOther ) const
{
	//the intervals intersect if 
	//this does not end before other starts
	//and this does not start after the other ends
	return !(m_fRange[1]<rOther.m_fRange[0] || m_fRange[0]>rOther.m_fRange[1]);
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VveIntervalTree_H
