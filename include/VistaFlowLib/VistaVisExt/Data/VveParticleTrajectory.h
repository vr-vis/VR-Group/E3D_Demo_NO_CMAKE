/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEPARTICLETRAJECTORY_H
#define _VVEPARTICLETRAJECTORY_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaVisExt/VistaVisExtConfig.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <vector>

#include "VveParticleDataArray.h"

/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

struct SVveParticleInstant;

/*============================================================================*/
/* CLASS DEFINITIONS                                                         */
/*============================================================================*/

class VISTAVISEXTAPI VveParticleTrajectory
{
public:
	/**
	 * Reason for termination
	 * These values define why a trajectory has been terminated
	 */
	enum TERM_REASON
	{
		TERM_ACTIVE,		  /** no termination criterion has yet been determined for this trajectory */
		TERM_TIME_FRAME_LEFT, /** the trajectory left the intended time frame */
		TERM_OUT_OF_DOMAIN,   /** the trajectory left the data domain */
		TERM_LOW_SPEED,       /** the "velocity" along the trajectory fell below a user-defined threshold */
		TERM_NUM_STEPS,		  /** a predefined integration limit was hit */
		TERM_UNKNOWN		  /** the trajectory was terminated but for no apparent reason */
	};

    VveParticleTrajectory();
    virtual ~VveParticleTrajectory();

    /**
     * Add a VveParticleDataArray to the particle trajectory.
     * NOTE: From here on, the trajectory will take ownership for the data array, i.e.
     *       it will delete the array upon its own destruction. Client code therefore must
	 *       not do this again!
     */ 
    void AddArray( VveParticleDataArrayBase* pArray );
	 /**
	  * Create and add a data array of type T with the given name and number of
	  * components and allocate iReserve entries in the array.
	  */
	template<class T>
	VveParticleDataArray<T>* CreateArray(const std::string &strName, const size_t iNumComponents, const size_t iReserve);
	/**
	 * Get the number of arrays available
	 */
	int GetNumArrays() const;

    /**
     * Get the (untyped) VveParticleDataArrayBase at index
     */
    int GetArray( size_t nIndex, VveParticleDataArrayBase*& pArray );
    VveParticleDataArrayBase* GetArray( size_t nIndex);

    /**
     * Same as above, but array referenced by its name
     */ 
    int GetArrayByName( const std::string& sName, VveParticleDataArrayBase*& pArray);
	
	 
    /**
     * Get the VveParticleDataArray with index nIndex
     */
    template<class T> 
    int GetTypedArray( const size_t& nIndex, VveParticleDataArray<T>*& pArray );

    /**
     * Get the VveParticleDataArray with name cName
     */
    template<class T> 
    int GetTypedArrayByName( const std::string& sName, 
                             VveParticleDataArray<T>*& pArray );

    /**
     * Get the index of the VveParticleDataArray with name cName 
     */
    int GetArrayIndexByName( const std::string& cName, size_t& nIndex);

	/**
	 * define the reason for termination for this trajectory
	 */
	void SetReasonForTermination(const int iReason);

	/**
	 * retrieve the reason for termination for this trajectory
	 */
	int GetReasonForTermination() const;

    /**
     * nix the entire trajectory
	 * NOTE: This will also delete all the attached arrays, so make sure that 
	 *       client code does not delete these arrays again!
     */
	void Clear();
		
    /**
     * reduce the particle information of the given trajectory to the time steps
     * given in the time mapper and copy it into the given particle trajectory
     * according to the given time data array
	 */
    void ReduceInformation( VveTimeMapper* pMapper,
                            std::string sTimeArrayName,
                            VveParticleTrajectory *pDestination);
	/**
     * Get the size of this trajectory as minimum of all array lengths
     */
    size_t GetSize();

    /**
	 * Resize all particle data arrays to the given number of elements
	 */
    void Resize( size_t nNumElements );

    /** 
	 * Reserve space for all particle data arrays
	 */
    void Reserve( size_t nNumElements );

    /** 
	 * Get a specific position within all arrays as SVveParticleInstant
	 */
    //SVveParticleInstant GetParticleInstant( int iIndex, std::string sPosArray, std::string sVelArrayId, std::string sTimeArrayId, std::string sRadiusArrayName = "", std::string sScalarArrayName = "");

protected:
	VveParticleTrajectory( const VveParticleTrajectory&); // Not implemented
	void operator=(const VveParticleTrajectory&); // Not implemented
	/** 
	 * Create arrays with the same signature in the destination trajectory
	 */
    void InitializeArrays( VveParticleTrajectory *pDestination );
    /** 
	 * Copy elements at index from this trajectory and append to the destination trajectory
	 */
    void CopyElements( const int& iIndex, VveParticleTrajectory *pDestination );
    /** 
	 * Interpolate all array elements and copy to destination array
	 */
    void InterpolateElements( int iIndex1, int iIndex2,
                              double dAlpha,
                              VveParticleTrajectory * pDestination );
	/** 
	 * Get the names of all particle data arrays as string
	 */
	std::string GetArrayNames();

private:
	
	/**
	 * remember the reason for termination
	 */
	int m_iTermReason;

	/**
	 * keep track of the arrays making this trajectory up
	 */
	std::vector<VveParticleDataArrayBase*> m_vecArrays;
};


/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/* NAME: CreateArray						                                  */
/*                                                                            */
/*============================================================================*/
template<class TData>
inline VveParticleDataArray<TData>* VveParticleTrajectory::CreateArray( 
														const std::string &strName, 
														const size_t iNumComponents, 
														const size_t iReserve )
{
	VveParticleDataArray<TData> *pArray = new 
		VveParticleDataArray<TData>(iNumComponents, strName);
	pArray->Reserve(iReserve);
	this->AddArray(pArray);
	return pArray;
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetTypedArray                                                        */
/*                                                                            */
/*============================================================================*/

template<class T>
inline int VveParticleTrajectory::
GetTypedArray( const size_t& nIndex, VveParticleDataArray<T>*& pArray )
{
    if( nIndex<m_vecArrays.size() )
    {
        pArray = dynamic_cast<VveParticleDataArray<T>*>(m_vecArrays[nIndex]);
        if( pArray ) 
        {
            return 1;
        }
    }
    return 0;
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetTypedArrayByName                                                  */
/*                                                                            */
/*============================================================================*/

template<class T>
inline int VveParticleTrajectory::
GetTypedArrayByName( const std::string& sName, VveParticleDataArray<T>*& pArray )
{
    size_t nIndex = 0;
    if( GetArrayIndexByName(sName, nIndex) )
    {
        return GetTypedArray(nIndex, pArray);
    }
    return 0;
}

#endif // _VVEPARTICLETRAJECTORY_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

