/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  SparseDataBlocks.h           RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA FlowLib demos          RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                        Rheinisch-Westfaelische      */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/


#ifndef _SPARSEDATABLOCKS_H
#define _SPARSEDATABLOCKS_H

#if defined(_WIN32)
#pragma warning (disable: 4786)
#endif
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaVisExt/VistaVisExtConfig.h>
#include <VistaVisExt/Data/VveUnsteadyData.h>
#include<map>
#include <cassert>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Unsteady data object for sparse storage of discrete multi block data.
 * Each discrete time step contains a number of blocks, e.g. resulting from 
 * a domain decomposition. In order not to have to store TxN possibly empty
 * entries (where T is the number of time levels and N the number of blocks
 * per time level) , this class enables sparse storage of txN entries where
 * t<<T.
 */
template<class TRawData>
class VveSparseDataBlocks : public VveUnsteadyData
{
public:
	typedef std::vector<TRawData*> TBlockVector;
	typedef std::map<int,TBlockVector> TTimeBlockMap;
	
	/**
	 * 
	 */
	VveSparseDataBlocks(VveTimeMapper *pTM);
	virtual ~VveSparseDataBlocks();

	/**
	 *  check if data object is empty
	 */
	bool IsEmpty() const;
	/**
	 * Retrieve the block for the given level index and block id
	 * This operation runs in O(log T) average time.
	 * NOTE that the order of blocks depends on the order of their insertion.
	 */
	TRawData *GetBlock(int iLevelIndex, int iBlockId) const;
	/**
	 * Returns the number of blocks that are stored for the given level index. 
	 * NOTE that the order of blocks depends on the order of their insertion.
	 */
	int GetNumBlocksForLevelIndex(int iLevelIndex) const;
	/**
	 * Retrieve a const reference to the block vector stored for a given
	 * level index. NOTE that the objects in that vector may have a block ID
	 * which is different from their index in the vector, i.e. the vector
	 * is densely populated and does not care for the block IDs at all.
	 * Use this for fast iteration over a query
	 */
	const TBlockVector& GetBlockVectorConstRefForLevelIndex(int iLevelIndex) const;
	/**
	 * just a convenience wrapper for the somewhat lengthy name above...
	 */
	const TBlockVector& operator[](unsigned int iLevelIndex) const;
	/**
	 * Set the block at the given level index and block id
	 *
	 * NOTE: This will NOT automatically update the valid time frame!
	 */
	bool AppendBlockToTimeLevel(int iLevelIndex, TRawData *pBlock);
	
	/**
	 * Retrieve the time frame (in sim time) for which this data bundle
	 * contains valid information
	 * NOTE: It is assumed that recurrent processes (e.g. a rotating pump
	 *		 are handled via the corresponding mechanisms in the underlying
	 *		 time mapper (cf. the VFL time model). Therefore, this interval
	 *		 should always be contiguous and well-defined.
	 */
	void GetValidTimeFrame(double dVal[2]) const;

	/**
	 * Set the time frame (in sim time) for which this data bundle
	 * contains valid information
	 * NOTE: It is assumed that recurrent processes (e.g. a rotating pump
	 *		 are handled via the corresponding mechanisms in the underlying
	 *		 time mapper (cf. the VFL time model). Therefore, this interval
	 *		 should always be contiguous and well-defined.
	 */
	void SetValidTimeFrame(double dVal[2]);

	/**
	 * Remove all blocks from a given time level.
	 * NOTE: The individual data item objects will NOT be physically deleted 
	 *		 by this operation! It is the client's responsibility to do so.
	 * NOTE: This will NOT automatically update the valid time frame.
	 */
	bool RemoveTimeLevel(int iLevelIndex);
	/**
	 * Remove all blocks contained in here
	 * NOTE: The blocks will NOT be deleted by this operation!
	 * NOTE: This WILL reset the valid time frame to an empty frame!
	 */
	void RemoveAllTimeLevels();

	/**
	 * assignment operator
	 */
	VveSparseDataBlocks<TRawData> &operator=(const VveSparseDataBlocks<TRawData>&);

protected:
	

private:
	/**
	 * keep track of the data in a memory efficient way
	 * Keep in mind, we may have many blocks in many time levels, so a 
	 * non-sparse structure might bring us O(1) access but at a severe 
	 * memory impact.
	 */
	TTimeBlockMap m_mapTimeBlocks;
	/**
	 * just keep an empty vector for empty time steps as a default return value
	 * NOTE: Technically, a static member would suffice for the intended use?
	 */
	TBlockVector m_vecEmpty;
	/**
	 * keep track of the time interval for which we are providing data
	 */
	double m_dValidTimeFrame[2];
};


/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
template<class TRawData>
VveSparseDataBlocks<TRawData>::VveSparseDataBlocks( VveTimeMapper *pTM )
	:	VveUnsteadyData(pTM)
{
	m_dValidTimeFrame[0] = m_dValidTimeFrame[1] = 
		m_pTimeMapper->GetSimulationTime(0.0);
}

template<class TRawData>
VveSparseDataBlocks<TRawData>::~VveSparseDataBlocks()
{
	this->RemoveAllTimeLevels();
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
template<class TRawData>
bool VveSparseDataBlocks<TRawData>::IsEmpty() const
{
	return m_mapTimeBlocks.empty();
}


template<class TRawData>
inline TRawData *VveSparseDataBlocks<TRawData>::GetBlock( int iLevelIndex, int iBlock ) const
{
	TTimeBlockMap::const_iterator itTimeIter = m_mapTimeBlocks.find(iLevelIndex);
	//no time step -> no data
	if(itTimeIter == m_mapTimeBlocks.end() || iBlock >= itTimeIter->second.size())
		return NULL;

	//return the i-th entry here
	return itTimeIter->second[iBlock];
}


// TODO TBC: This function doesn't appear to be called somewhere
template<class TRawData>
inline int VveSparseDataBlocks<TRawData>::GetNumBlocksForLevelIndex( int iLevelIndex ) const
{
	TTimeBlockMap::const_iterator itTimeIter = m_mapTimeBlocks.find(iLevelIndex);
	//no time step -> no data
	if(itTimeIter == m_mapTimeBlocks.end())
		return -1; // TODO TBC: guessed value. Not checked, because this function is never called

	return  static_cast<int>(itTimeIter->second.size());
}


template<class TRawData>
inline const typename VveSparseDataBlocks<TRawData>::TBlockVector&
	VveSparseDataBlocks<TRawData>::GetBlockVectorConstRefForLevelIndex( int iLevelIndex) const
{
	TTimeBlockMap::const_iterator itTimeIter = m_mapTimeBlocks.find(iLevelIndex);
	//no time step -> no data
	if(itTimeIter == m_mapTimeBlocks.end())
		return m_vecEmpty;

	return itTimeIter->second;
}

template<class TRawData>
inline const typename VveSparseDataBlocks<TRawData>::TBlockVector& 
	VveSparseDataBlocks<TRawData>::operator[](unsigned int iLevelIndex) const
{
	return this->GetBlockVectorConstRefForLevelIndex(iLevelIndex);
}

template<class TRawData>
inline bool VveSparseDataBlocks<TRawData>::AppendBlockToTimeLevel( int iLevelIndex, TRawData *pBlock )
{
	//only non-null entries make sense here
	//@todo may replace this by a simple assertion if this becomes a factor here!
	if(pBlock == NULL)
		return false;

	if(iLevelIndex<0 || iLevelIndex >= m_pTimeMapper->GetNumberOfTimeLevels())
		return false;

	//just push it to the back of the local vector
	//NOTE: here the block's ID may be different from the index into the vector
	m_mapTimeBlocks[iLevelIndex].push_back(pBlock);
	return true;
}

template<class TRawData>
inline void VveSparseDataBlocks<TRawData>::GetValidTimeFrame( double dVal[2] ) const
{
	dVal[0] = m_dValidTimeFrame[0];
	dVal[1] = m_dValidTimeFrame[1];
}

template<class TRawData>
inline void VveSparseDataBlocks<TRawData>::SetValidTimeFrame( double dVal[2] )
{
	m_dValidTimeFrame[0] = dVal[0];
	m_dValidTimeFrame[1] = dVal[1];
}

template<class TRawData>
inline bool VveSparseDataBlocks<TRawData>::RemoveTimeLevel( int iLevelIndex )
{
	TTimeBlockMap::iterator itTarget = m_mapTimeBlocks.find(iLevelIndex);

	if(itTarget == m_mapTimeBlocks.end())
		return false;

	m_mapTimeBlocks.erase(itTarget);
	return true;
}

template<class TRawData>
inline void VveSparseDataBlocks<TRawData>::RemoveAllTimeLevels()
{
	TTimeBlockMap::iterator itTimeCurrent = m_mapTimeBlocks.begin();
	TTimeBlockMap::iterator itTimeEnd = m_mapTimeBlocks.end();

	for(;itTimeCurrent != itTimeEnd; ++itTimeCurrent)
	{
		itTimeCurrent->second.clear();
	}

	m_mapTimeBlocks.clear();

	//since we have no data at all -> reset the valid time frame to [empty]!
	m_dValidTimeFrame[0] = m_dValidTimeFrame[1] = 
		m_pTimeMapper->GetSimulationTime(0.0);
}

template<class TRawData>
inline VveSparseDataBlocks<TRawData> & VveSparseDataBlocks<TRawData>::operator=( const VveSparseDataBlocks<TRawData>& rOther)
{
	this->m_mapTimeBlocks = rOther.m_mapTimeBlocks;
	return *this;
}



#endif /* SPARSEDATABLOCKS_H_ */
