/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VveContinuousData.cpp        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaFlowLib                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
// VistaFlowLib includes
#include "VveContinuousData.h"
#include <VistaBase/VistaStreamUtils.h>


#include <iostream>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveContinuousData::VveContinuousData(VveTimeMapper *pM)
: VveUnsteadyData(pM), m_pData(NULL)
{
}


VveContinuousData::~VveContinuousData()
{

    vstr::debugi() << " [VveContinuousData] - being destroyed..." << endl;

}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
