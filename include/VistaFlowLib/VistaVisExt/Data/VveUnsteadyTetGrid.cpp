/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// VistaFlowLib includes
#include "VveUnsteadyTetGrid.h"
#include "VveTetGrid.h"

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VveUnsteadyTetGrid::VveUnsteadyTetGrid(VveTimeMapper *pTM)
: VveDiscreteDataTyped<VveTetGrid>(pTM),
  m_pTopologyGrid(NULL)
{
}

VveUnsteadyTetGrid::~VveUnsteadyTetGrid()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTopologyGrid                                         */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyTetGrid::SetTopologyGrid(VveTetGridItem *pGrid)
{
	m_pTopologyGrid = pGrid;
	Notify();
}

VveTetGridItem *VveUnsteadyTetGrid::GetTopologyGrid() const
{
	return m_pTopologyGrid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetBounds                                               */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyTetGrid::SetBounds(const VistaVector3D &v3Min, const VistaVector3D &v3Max)
{
	VistaMutexLock oLock(m_oMutex);
	bool bChanged = false;
	if (m_v3BoundsMin != v3Min)
	{
		m_v3BoundsMin = v3Min;
		bChanged = true;
	}

	if (m_v3BoundsMax != v3Max)
	{
		m_v3BoundsMax = v3Max;
		bChanged = true;
	}

	if (bChanged)
		Notify();
}

void VveUnsteadyTetGrid::GetBounds(VistaVector3D &v3Min, VistaVector3D &v3Max) const
{
	v3Min = m_v3BoundsMin;
	v3Max = m_v3BoundsMax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBounds                                               */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyTetGrid::ComputeBounds()
{
	bool bValid = false;
	VistaVector3D v3MinOld(m_v3BoundsMin);
	VistaVector3D v3MaxOld(m_v3BoundsMax);
	{
		VistaMutexLock oLock(m_oMutex);
		int iCount = GetNumberOfLevels();
		m_v3BoundsMin = VistaVector3D(0, 0, 0);
		m_v3BoundsMax = VistaVector3D(0, 0, 0);
		VistaVector3D v3Min, v3Max;
		for (int i=0; i<int(m_vecData.size()); ++i)
		{
			VveTetGridItem *pContainer = GetTypedLevelDataByLevelIndex(i);
			VveTetGrid *pGrid = pContainer->GetData();
			if (!pGrid->GetValid())
				continue;

			if (!bValid)
			{
				pGrid->GetBounds(m_v3BoundsMin, m_v3BoundsMax);
				bValid = true;
			}
			else
			{
				pGrid->GetBounds(v3Min, v3Max);
				for (int j=0; j<3; ++j)
				{
					if (v3Min[j] < m_v3BoundsMin[j])
						m_v3BoundsMin[j] = v3Min[j];
					if (v3Max[j] > m_v3BoundsMax[j])
						m_v3BoundsMax[j] = v3Max[j];
				}
			}
		}
	}
	if (v3MinOld!=m_v3BoundsMin || v3MaxOld!=m_v3BoundsMax)
		Notify();
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
