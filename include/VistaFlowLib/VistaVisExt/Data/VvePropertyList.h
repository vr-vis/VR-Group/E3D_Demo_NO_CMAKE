/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEPROPERTYLIST_H
#define _VVEPROPERTYLIST_H

#ifdef WIN32
	#pragma warning (disable: 4786)
#endif

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VvePropertyContainer.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*	A class for all kinds of meta data in the form of a list.
*/
class VISTAVISEXTAPI VvePropertyList : public VvePropertyContainer
{
    //an output operator might come in handy for this one...
    friend std::ostream& operator<< (std::ostream& output, const VvePropertyList& oList);
public:
    VvePropertyList();
    virtual ~VvePropertyList();

    /**
     * Set the given property, i.e. overwrite the previous value
     * of the given key with the new value.
     */
    virtual int SetProperty(const VistaProperty &refProp);

    /**
     * Get the given property, i.e. overwrite the property's
     * value with the one from the property list.
     */
    virtual int GetProperty(VistaProperty &refProp);

    /**
     * Retrieves all keys within the property list and returns the number of
	 * keys stored in the retrieved list.
     */
    virtual int GetPropertySymbolList(std::list<std::string> &rStorageList);

    /**
     * Retrieve the property list itself.
     */
    virtual const VistaPropertyList *GetPropertyList() const;

protected:
    VistaPropertyList    m_oPropList;
};

//output operator
std::ostream& operator<< (std::ostream& output, const VvePropertyList& oList);

#endif // _VVEPROPERTYLIST_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/
