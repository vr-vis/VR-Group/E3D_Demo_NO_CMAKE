/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVETIMEHISTOGRAM_H
#define _VVETIMEHISTOGRAM_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveUnsteadyData.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class Vve2DHistogram;
class VveHistogram;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * This class provides a "wrapper" around a generic 2D histogram in that it
 * interpretes that histogram's horizontal axis as time axis. Along this 
 * time axis, one 1D histogram is given per time step.
 */
class VISTAVISEXTAPI VveTimeHistogram : public VveUnsteadyData
{
public:    
	/**
	*
	*/
	VveTimeHistogram(VveTimeMapper *pMapper);

	/**
	*
	*/
	virtual ~VveTimeHistogram();

	/**
	 *
	 */
	VveTimeHistogram &operator=(const VveTimeHistogram& other);
	/**
	 *
	 */
	void SetHistogram(const Vve2DHistogram *pHistogram);
	Vve2DHistogram *GetHistogram() const;

	/**
	 * retrieve the 1D histogram for the given level index, i.e.
	 * the respective column of the 2D histogram
	 *
	 * NOTE: This column will contain outlier values for min and max!
	 *
	 */
	void GetHistogramForLevelIdx(int iLevelIdx, VveHistogram& oHist);

	/**
	 *
	 */
	void GetHistogramForSimTime(double fSimTime, VveHistogram& oHist);

	/**
	 *
	 */
	void GetHistogramForVisTime(double fVisTime, VveHistogram& oHist);

	/**
	 * retrieve the raw count values for a given level idx i.e. the counts
	 * of a column in the underlying 2D histogram.
	 *
	 * NOTE: In contrast to the GetHistogram functions, this will NOT return
	 * the counts for the outlier values, i.e. it will only give the values
	 * within the valid data range!
	 */
	void GetBinCountsForLevelIdx(int iLevelIdx, std::vector<int>& vecCounts);

	/**
	 *
	 */
	void GetBinCountsForSimTime(double fSimTime, std::vector<int>& vecCounts);

	/**
	 *
	 */
	void GetBinCountsForVisTime(double fVisTime, std::vector<int>& vecCounts);

	/**
	 *
	 */
	void GetNormalizedCountsForLevelIdx(int iLevelIdx, std::vector<float>& vecDensity);
	/**
	 *
	 */
	void GetNormalizedCountsForSimTime(double fSimTime, std::vector<float>& vecDensity);
	/**
	 *
	 */
	void GetNormalizedCountsForVisTime(double fVisTime, std::vector<float>& vecDensity);


protected:  
	
private:
	Vve2DHistogram *m_pTarget;
	
	//cache data for the last level...
	int m_iLastLevel;
	VveHistogram *m_pLastHist;
};

/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VVETIMEHISTOGRAM_H

