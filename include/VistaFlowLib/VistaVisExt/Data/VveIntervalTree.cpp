/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VveIntervalTree.cpp                RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/


// include header here

#include "VveIntervalTree.h"

#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>

#include <limits>
#include <queue>
#include <algorithm>

#include <cmath>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

struct SplitInfo
{
	size_t iNodeId;
	size_t iDepth;
	float fSubRange[2];
	std::vector<VveIntervalInfo> vecIntervalsToSplit;
};

class IntervalHistogram
{
public: 
	IntervalHistogram();
	virtual ~IntervalHistogram();

	void HashIntervals(const size_t iResolution,
					   const std::vector<VveIntervalInfo> &vecIntervals);

	float ComputeSplitVal(float fSubRange[2]) const;

	void GetRange(float fRng[2]) const;

private:
	float m_fRange[2];
	std::vector<float> m_vecBinCenters;
	std::vector<unsigned int> m_vecBins;
};

class _IntervalStartPtComp
{
public:
	bool operator()(const VveIntervalInfo &rLeft, const VveIntervalInfo &rRight) const
	{
		return rLeft.min() < rRight.min();
	}
};
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

VveIntervalTree::VveIntervalTree(const size_t iMaxNumIntervals)
	: m_iMaxDepth(iMaxNumIntervals)
{
}
VveIntervalTree::~VveIntervalTree()
{
}

/*============================================================================*/
/* IMPLEMENTATION   VveIntervalTree                                           */
/*============================================================================*/
void VveIntervalTree::Build(const std::vector<VveIntervalInfo> &vecIntervals)
{
	m_vecTree.clear();
	m_iMaxNumIntervals = 0;
	//reserve memory for tree
	m_vecTree.reserve(size_t(pow(2.0f,float(m_iMaxDepth+1))));

	IntervalHistogram oHist;
	oHist.HashIntervals(10000, vecIntervals);

	//create initial split info
	VveIntervalTreeNode oRoot;
	m_vecTree.push_back(oRoot);
	SplitInfo oInfo;
	oInfo.iNodeId = 0;
	oInfo.iDepth = 0;
	oInfo.vecIntervalsToSplit = vecIntervals;
	oHist.GetRange(oInfo.fSubRange);
		
	std::queue<SplitInfo> quNodesToSplit;
	quNodesToSplit.push(oInfo);

	_IntervalStartPtComp oComp;
	size_t iStartIntervalOffset = 0;

	while(!quNodesToSplit.empty())
	{
		oInfo = quNodesToSplit.front();
		quNodesToSplit.pop();

		VveIntervalTreeNode &rNode = m_vecTree[oInfo.iNodeId];

		//compute split value
		float fSplitVal = oHist.ComputeSplitVal(oInfo.fSubRange);
		
		//counter for intervals that are inserted into
		//the current node
		size_t iNumMidIntervals = 0;
		
		//check if we have a leaf node or an inner node
		
		if(oInfo.iDepth < m_iMaxDepth && !oInfo.vecIntervalsToSplit.empty())
		{
			//==> SPLIT THE CURRENT NODE
			//create children
			VveIntervalTreeNode oNewNode;
			m_vecTree.push_back(oNewNode);
			SplitInfo oLeft;
			oLeft.iNodeId = m_vecTree.size()-1;
			oLeft.iDepth = oInfo.iDepth+1;
			oLeft.fSubRange[0] = std::numeric_limits<float>::max();
			oLeft.fSubRange[1] = -std::numeric_limits<float>::max();

			m_vecTree.push_back(oNewNode);
			SplitInfo oRight;
			oRight.iNodeId = m_vecTree.size()-1;
			oRight.iDepth = oInfo.iDepth+1;
			oRight.fSubRange[0] = std::numeric_limits<float>::max();
			oRight.fSubRange[1] = -std::numeric_limits<float>::max();

			//link children to father
			rNode.SetLeftChild(VistaType::uint32(m_vecTree.size()-2));
			rNode.SetRightChild(VistaType::uint32(m_vecTree.size()-1));

			//distribute current interval vector
			//between current node and its children
			const size_t iNumIntervals = oInfo.vecIntervalsToSplit.size();
			for(size_t i=0; i<iNumIntervals; ++i)
			{
				VveIntervalInfo &rInterval =  oInfo.vecIntervalsToSplit[i];
				//Check if the current interval is propagated to the left child...
				if(rInterval < fSplitVal)
				{
					oLeft.vecIntervalsToSplit.push_back(rInterval);
					oLeft.fSubRange[0] = std::min<float>(oLeft.fSubRange[0], rInterval.min());
					oLeft.fSubRange[1] = std::max<float>(oLeft.fSubRange[1], rInterval.max());
				}
				//... or the right child ...
				else if(rInterval > fSplitVal)
				{
					oRight.vecIntervalsToSplit.push_back(rInterval);
					oRight.fSubRange[0] = std::min<float>(oRight.fSubRange[0], rInterval.min());
					oRight.fSubRange[1] = std::max<float>(oRight.fSubRange[1], rInterval.max());
				}
				//... or stays with the current node.
				else
				{
					m_vecIntervals.push_back(rInterval);
					++iNumMidIntervals;
				}
			}

			//push child info
			quNodesToSplit.push(oLeft);
			quNodesToSplit.push(oRight);

		}
		else
		{
			//we do not split the node any more
			//==> all intervals are inserted in this node
			iNumMidIntervals = oInfo.vecIntervalsToSplit.size();
			m_vecIntervals.insert(m_vecIntervals.end(), 
				oInfo.vecIntervalsToSplit.begin(), 
				oInfo.vecIntervalsToSplit.end());
		}
		//finally, link inserted intervals to current node
		//and sort node's sub-sequence w.r.t interval starting pts
		rNode.SetSplitVal(fSplitVal);
		rNode.SetRange(oInfo.fSubRange);
		rNode.SetFirstIntervalId(iStartIntervalOffset);
		rNode.SetNumIntervals(iNumMidIntervals);
		m_iMaxNumIntervals = std::max<size_t>(m_iMaxNumIntervals, iNumMidIntervals);
				
		std::sort(m_vecIntervals.begin()+iStartIntervalOffset, 
				  m_vecIntervals.end(),
				  oComp);
		
		//advance current interval offset
		iStartIntervalOffset += iNumMidIntervals;
	}
}

size_t VveIntervalTree::GetIntersectingIntervals(float fVal, 
												 std::vector<VveIntervalInfo> &vecResult) const
{
	vecResult.clear();
	vecResult.reserve(10000);

	size_t iCurrentNodeId = 0;
	
	//DLR
	for(size_t i=0; i<m_vecIntervals.size(); ++i)
	{
		const VveIntervalInfo &rI = m_vecIntervals[i];
		if(rI.Contains(fVal))
			vecResult.push_back(rI);
	}
	return vecResult.size();
	//DLR

	while(!m_vecTree[iCurrentNodeId].IsLeaf())
	{
		//check all intervals in rCurrent
		const VveIntervalTreeNode &rCurrent = m_vecTree[iCurrentNodeId];
		//skip empty nodes altogehter
		if(rCurrent.HasIntevalsForVal(fVal))
		{
			size_t iStart = static_cast<size_t>(rCurrent.GetFirstIntervalId());
			const size_t iEnd = iStart + static_cast<size_t>(rCurrent.GetNumIntervals());
			for(size_t i=iStart; i<iEnd; ++i)
			{
				const VveIntervalInfo &rI = m_vecIntervals[i];
				if(rI.Contains(fVal))
					vecResult.push_back(rI);
			}
		}
		
		//move rCurrent
		if(fVal < rCurrent.GetSplitVal())
			iCurrentNodeId = rCurrent.GetLeftChild();
		else
			iCurrentNodeId = rCurrent.GetRightChild();
	}
	//Get also the intervals from the leaf node if it contains the iso value
	if(m_vecTree[iCurrentNodeId].IsLeaf())
	{
		//check all intervals in rCurrent
		const VveIntervalTreeNode &rCurrent = m_vecTree[iCurrentNodeId];
		//skip empty nodes altogehter
		if(rCurrent.HasIntevalsForVal(fVal))
		{
			size_t iStart = static_cast<size_t>(rCurrent.GetFirstIntervalId());
			const size_t iEnd = iStart + static_cast<size_t>(rCurrent.GetNumIntervals());
			for(size_t i=iStart; i<iEnd; ++i)
			{
				const VveIntervalInfo &rI = m_vecIntervals[i];
				if(rI.Contains(fVal))
					vecResult.push_back(rI);
			}
		}
	}
	return vecResult.size();
}

int VveIntervalTree::Serialize(IVistaSerializer &ser) const
{
	ser.WriteInt32((VistaType::uint32)m_iMaxDepth);
	ser.WriteInt32((VistaType::uint32)m_iMaxNumIntervals);
	ser.WriteInt32((VistaType::uint32)m_vecIntervals.size());
	for(int x=0; x<m_vecIntervals.size();++x)
		ser.WriteSerializable(m_vecIntervals[x]);
	ser.WriteInt32((VistaType::uint32)m_vecTree.size());
	for(int x=0; x<m_vecTree.size();++x)
		ser.WriteSerializable(m_vecTree[x]);
	return 0;
}
   
int VveIntervalTree::DeSerialize(IVistaDeSerializer &des)
{
	int numIntervals=0;
	int numNodes=0;
	VistaType::uint32 maxDepth = 0;
	VistaType::uint32 maxNumIntervals = 0;
	des.ReadInt32(maxDepth);
	m_iMaxDepth = maxDepth;
	des.ReadInt32(maxNumIntervals);
	m_iMaxNumIntervals=maxNumIntervals;
	des.ReadInt32(numIntervals);
	m_vecIntervals.resize(numIntervals);
	for(int x=0; x<numIntervals;++x){
		VveIntervalInfo info;
		des.ReadSerializable(info);
		m_vecIntervals[x]=(info);
	}
	des.ReadInt32(numNodes);
	m_vecTree.resize(numNodes);
	for(int x=0; x<numNodes;++x){
		VveIntervalTreeNode node;
		des.ReadSerializable(node);
		m_vecTree[x]=(node);
	}
	return 0;
}
    
std::string VveIntervalTree::GetSignature() const
{
	return "VveIntervalTree";
}



/*============================================================================*/
/* IMPLEMENTATION      VveIntervalTreeNode                                    */
/*============================================================================*/
VveIntervalTreeNode::VveIntervalTreeNode()
	:	m_fSplitVal(0.0f),
	m_iLeftChild(0),
	m_iRightChild(0),
	m_iStartInterval(0),
	m_iNumIntervals(0)
{
	m_fRange[0] = std::numeric_limits<float>::max();
	m_fRange[1] = -std::numeric_limits<float>::max();
}


VveIntervalTreeNode::~VveIntervalTreeNode()
{

}

int VveIntervalTreeNode::Serialize(IVistaSerializer &ser) const
{
	ser.WriteFloat32(m_fSplitVal);
	ser.WriteFloat32(m_fRange[0]);
	ser.WriteFloat32(m_fRange[1]);
	ser.WriteInt32(m_iLeftChild);
	ser.WriteInt32(m_iRightChild);
	ser.WriteUInt64(m_iStartInterval);
	ser.WriteUInt64(m_iNumIntervals);
	return 0;
}
   
int VveIntervalTreeNode::DeSerialize(IVistaDeSerializer &des)
{
	des.ReadFloat32(m_fSplitVal);
	des.ReadFloat32(m_fRange[0]);
	des.ReadFloat32(m_fRange[1]);
	des.ReadInt32(m_iLeftChild);
	des.ReadInt32(m_iRightChild);
	des.ReadUInt64(m_iStartInterval);
	des.ReadUInt64(m_iNumIntervals);
	return 0;
}
    
std::string VveIntervalTreeNode::GetSignature() const
{
	return "VveIntervalTreeNode";
}


/*============================================================================*/
/* IMPLEMENTATION      VveIntervalInfo                                        */
/*============================================================================*/
VveIntervalInfo::VveIntervalInfo()
{
	m_iCellId=0;
	m_fRange[0]=0;
	m_fRange[1]=0;
}
VveIntervalInfo::VveIntervalInfo( float fRange[2], const VistaType::uint64 iCellId )
	:	m_iCellId(iCellId)
{
	m_fRange[0] = fRange[0];
	m_fRange[1] = fRange[1];

}

VveIntervalInfo::VveIntervalInfo( float fMin, float fMax, const VistaType::uint64 iCellId )
	:	m_iCellId(iCellId)
{
	m_fRange[0] = fMin;
	m_fRange[1] = fMax;

}


VveIntervalInfo::~VveIntervalInfo()
{

}

int VveIntervalInfo::Serialize(IVistaSerializer &ser) const
{
	ser.WriteFloat32(m_fRange[0]);
	ser.WriteFloat32(m_fRange[1]);
	ser.WriteUInt64(m_iCellId);
	return 0;
}
   
int VveIntervalInfo::DeSerialize(IVistaDeSerializer &des)
{
	des.ReadFloat32(m_fRange[0]);
	des.ReadFloat32(m_fRange[1]);
	des.ReadUInt64(m_iCellId);
	return 0;
}
    
std::string VveIntervalInfo::GetSignature() const
{
	return "VveIntervalInfo";
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
IntervalHistogram::IntervalHistogram()
{
	m_fRange[0] =  std::numeric_limits<float>::max();
	m_fRange[1]	= -std::numeric_limits<float>::max();
}

IntervalHistogram::~IntervalHistogram()
{

}

void IntervalHistogram::HashIntervals(const size_t iResolution,
									   const std::vector<VveIntervalInfo> &vecIntervals)
{
	//determine range
	m_fRange[0] =  std::numeric_limits<float>::max();
	m_fRange[1]	= -std::numeric_limits<float>::max();
	const size_t iNumIntervals = vecIntervals.size();
	for(size_t i=0; i<iNumIntervals; ++i)
	{
		m_fRange[0] = std::min<float>(m_fRange[0], vecIntervals[i].min());
		m_fRange[1] = std::max<float>(m_fRange[1], vecIntervals[i].max());
	}

	//determine bin centers
	const float fRange = m_fRange[1] - m_fRange[0];
	const float fBinDelta = fRange / float(iResolution);

	m_vecBinCenters.resize(iResolution);
	m_vecBinCenters[0] = m_fRange[0];
	m_vecBinCenters[iResolution-1] = m_fRange[1];

	for(size_t i=1; i<iResolution-1; ++i)
		//DLR changed m_vecBinCenters must be within range of m_fRange
		m_vecBinCenters[i] = m_fRange[0] +float(i) * fBinDelta;
		//m_vecBinCenters[i] = float(i) * fBinDelta;
	
	//hash intervals
	m_vecBins.resize(iResolution, 0);

	for(size_t i=0; i<iNumIntervals; ++i)
	{
		const VveIntervalInfo &rCurrent=vecIntervals[i];
		//TODO Changed by DLR (Speedup factor 3x)
		size_t StartID = (size_t) ((rCurrent.min()-m_fRange[0])/(fBinDelta)-0.5);
		size_t EndID   = (size_t) ((rCurrent.max()-m_fRange[0])/(fBinDelta)+0.5);
		for(size_t j=StartID; j<EndID; ++j)
		{
			//check if we are done with this iteration
			if(m_vecBinCenters[j] > rCurrent.max())
				break;
			//check if the interval has to be counted
			if(rCurrent.Contains(m_vecBinCenters[j]))
				++m_vecBins[j];
		}
	}

}

float IntervalHistogram::ComputeSplitVal(float fSubRange[2]) const
{
	const size_t iNumIntervals = m_vecBins.size();
	size_t iSumCounts = 0;
	size_t arrBinRng[2] = {iNumIntervals-1, 0};

	//determine the total number of intervals
	for(size_t i=0; i<iNumIntervals; ++i)
	{
		//skip smaller counts
		if(m_vecBinCenters[i] < fSubRange[0])
			continue;
		//stop if we are above the range
		if(m_vecBinCenters[i] > fSubRange[1])
			break;
		
		//update bin range
		arrBinRng[0] = std::min<size_t>(arrBinRng[0], i);
		arrBinRng[1] = std::max<size_t>(arrBinRng[1], i);

		//sum up the number of intervals
		iSumCounts += m_vecBins[i];
	}

	//if we overlap just one or two bins -> split middle
	if(arrBinRng[1]-arrBinRng[0]<=1)
		return 0.5f*(fSubRange[1]-fSubRange[0]);

	//seek for the split value
	const size_t iMedian = iSumCounts / 2;
	//size_t i=arrBinRng[0]+1;
	//Changed by DLR
	size_t i=arrBinRng[0];
	//size_t iCurrentSum = m_vecBins[i-1];
	//Changed by DLR
	size_t iCurrentSum = m_vecBins[i];
	while(iCurrentSum < iMedian && i <= arrBinRng[1])
	{
		iCurrentSum += m_vecBins[i];
		++i;
	}
	return m_vecBinCenters[i];

}

inline void IntervalHistogram::GetRange(float fRng[2]) const
{
	fRng[0] = m_fRange[0];
	fRng[1] = m_fRange[1];
}




/*============================================================================*/
/* END OF FILE "VveIntervalTree.CPP"                                          */
/*============================================================================*/

/************************** CR / LF nicht vergessen! **************************/









