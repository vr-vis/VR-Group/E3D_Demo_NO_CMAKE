/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEPOINTSETOCTREEBUILDER_H
#define _VVEPOINTSETOCTREEBUILDER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaVisExt/VistaVisExtConfig.h>

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkDataSet;
class VveOctree;
class VveOctreeNode;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/


class VISTAVISEXTAPI VvePointSetOctreeBuilder
{
public:
	static VveOctree* BuildOctreeForPoints(vtkDataSet *pDS, int iBucketSize);
	static VveOctree* BuildOctreeForCells(vtkDataSet *pDS, int iBucketSize);
private:
	static void BuildOctree(VveOctree *pTree, int iBucketSize);
	static void SplitNode(VveOctreeNode *pNode);

	VvePointSetOctreeBuilder(){}
	virtual ~VvePointSetOctreeBuilder(){}
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VVEPOINTSETOCTREEBUILDER_H
