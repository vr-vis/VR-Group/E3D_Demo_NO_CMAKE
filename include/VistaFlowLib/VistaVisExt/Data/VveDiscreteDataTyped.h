/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEDISCRETEDATATYPED_H
#define _VVEDISCRETEDATATYPED_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
//ViSTA includes

// ViSTA FlowLib includes
#include "VveDiscreteData.h"
#include "VveDataItem.h"

#include <iostream>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VveDiscreteDataTyped is a generic container for data given in discrete time
 * steps. 
 * NOTE: This object will allocate items of type VveDataItem<TRawVisData> for
 *	     each time level upon construction and it will delete these upon 
 *		 deletion. So consequently make sure you delete whatever raw data you
 *		 put into the underlying data items BEFORE deleting the discrete data
 *       object itself.
 */
template<class TRawVisData>
class VveDiscreteDataTyped : public VveDiscreteData
{
public:   
	typedef TRawVisData                         value_type;
	typedef VveDataItem< TRawVisData > container_type;
	/**
	 * 
	 */
	VveDiscreteDataTyped(VveTimeMapper *pTM);
    virtual ~VveDiscreteDataTyped();
	/**
	 *
	 */
	VveDataItem<TRawVisData> *GetTypedLevelDataByVisTime(double dVisTime) const;
	/**
	 *
	 */
	VveDataItem<TRawVisData> *GetTypedLevelDataByLevelIndex(int iLevel) const;
};

/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
template<class TRawVisData>
inline VveDiscreteDataTyped<TRawVisData>::VveDiscreteDataTyped(
															VveTimeMapper *pTM) 
	:	VveDiscreteData(pTM)
{
	for(int i=0; i<pTM->GetNumberOfTimeLevels(); ++i)
		this->SetLevelData(i, new VveDataItem<TRawVisData>);
}


template<class TRawVisData>
inline VveDiscreteDataTyped<TRawVisData>::~VveDiscreteDataTyped()
{
#ifdef DEBUG
    vstr::debugi() << " [VveDiscreteDataTyped] - being destroyed..." << std::endl;
#endif
	for(int i=0; i<this->GetNumberOfLevels(); ++i)
		delete this->GetTypedLevelDataByLevelIndex(i);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLevelData                                                */
/*                                                                            */
/*============================================================================*/
template<class TRawVisData>
inline VveDataItem<TRawVisData> 
*VveDiscreteDataTyped<TRawVisData>::GetTypedLevelDataByVisTime(double dVisTime) const
{
    return static_cast<VveDataItem<TRawVisData>*>(
							VveDiscreteData::GetLevelDataByVisTime(dVisTime));
}

template<class TRawVisData>
inline VveDataItem<TRawVisData> 
*VveDiscreteDataTyped<TRawVisData>::GetTypedLevelDataByLevelIndex(int iLevel) const
{
    return static_cast<VveDataItem<TRawVisData>*>(
							VveDiscreteData::GetLevelDataByLevelIndex(iLevel));
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VVEDISCRETEDATATYPED_H

