/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVESTATSSET_H
#define _VVESTATSSET_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaSerializable.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTimeSeries;
class VveTimeHistogram;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * A stats set just holds a set of statistics which usually belong together
 * i.e. a min, max, average time series and an (optional) time histogram
 * It assumes responsibility for these objects i.e. it will consistently
 * create and delete them.
 * Conveniently, the stat set is serializable as well and takes care of
 * handling serialization of the contained data.
 */
class VISTAVISEXTAPI VveStatsSet : public IVistaSerializable
{
public:
	VveStatsSet();
	/**
	 * create a stat set
	 *
	 * NOTE: The time mapper is currently assumed to be IMPLICIT because 
	 *       only for these serialization works properly
	 *
	 * histogram creation is optional
	 */
	VveStatsSet(	const std::string& strFieldName,
					VveTimeMapper *pTM, bool bCreateHist = true);
	virtual ~VveStatsSet();

	/**
	 * member access
	 */
	void SetFieldName(const std::string &strFieldName);
	std::string GetFieldName() const;
	VveTimeSeries *GetMinSeries() const;
	VveTimeSeries *GetMaxSeries() const;
	VveTimeSeries *GetAvgSeries() const;
	VveTimeHistogram *GetHistogram() const;

	/**
	 * Consistently remap the internal timing for all time series 
	 * and the time histogram.
	 * This is needed e.g. in order to change the time series' validity
	 * frame i.e. to restrict it to some subset of the original time range
	 * A typical use case would be the restriction to a feature's life span
	 * which typically is only a subset of the entire data set's life span.
	 *
	 * NOTE: Currently this only works with implicit timings!
	 *
	 * NOTE: The number of time levels given in the data needs to be less
	 *       than or equal to the number of time levels that have originally 
	 *		 been allocated!
	 */
	bool RemapTiming(const VveTimeMapper::Timings &oTimings);
	
	/**
     * Think of this as "SAVE"
     */
    virtual int Serialize(IVistaSerializer &) const;
    /**
     * Think of this as "LOAD"
     */
    virtual int DeSerialize(IVistaDeSerializer &);

    virtual std::string GetSignature() const;
protected:
	void Cleanup();
private:
	std::string m_strFieldName;
	VveTimeMapper *m_pTimeMapper;
	VveTimeSeries *m_pMinSeries;
	VveTimeSeries *m_pMaxSeries;
	VveTimeSeries *m_pAvgSeries;
	VveTimeHistogram *m_pHistogram;

	int m_iOriginalNumTL;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VveStatsSet_H

