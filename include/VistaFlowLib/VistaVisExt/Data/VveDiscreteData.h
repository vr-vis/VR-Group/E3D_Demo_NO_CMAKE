/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEDISCRETEDATA_H
#define _VVEDISCRETEDATA_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>

//ViSTA includes

// ViSTA FlowLib includes
#include "VveUnsteadyData.h"

/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVveDataItem;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*/
class VISTAVISEXTAPI VveDiscreteData : public VveUnsteadyData
{
public:
	VveDiscreteData(VveTimeMapper *pTM);

    virtual ~VveDiscreteData();

    /**
	* Retrieve the data that belongs to the discrete time level matching
	* the given visualization time.
	*
	* @param[in]	float fVisTime    the current visualization time (which is supposed to lie between 0 and 1).
	* @return	TRawVisData *     a pointer to the corresponding data.
	*
    */
	IVveDataItem *GetLevelDataByVisTime(double dVisTime) const;

    /**
	* Retrieve the data that belongs to the given discrete time level.
	*
	* @param[in]	int iLevel                      the discrete time level.
	* @return	IVveDataItem *     a pointer to the corresponding data.
	*
    */
	IVveDataItem *GetLevelDataByLevelIndex(int iLevel) const;

    /**
	* Retrieve the number of discrete time levels inside this data object.
	*
	* @param[in]	---
	* @return	int               the number of time levels.
	*
    */
	int GetNumberOfLevels() const;

    /**
	* Set the data that belongs to the given discrete time level. Note,
	* that this method will succeed at most once for any given time level - 
	* especially overwriting the underlying data is not possible. If you
	* plan to update the underlying data of any single time level, use the
	* respective methods of the underlying IVveDataContainer objects.
	*
	* @param[in]	int iIndex                      the discrete time index.
	* @param[in]	IVveDataItem *     a pointer to the data to be set.
	* @return	bool    true iff index could be set
	*
    */
	bool SetLevelData(int iIndex, IVveDataItem *pData);

	IVveDataItem *RemLevelData(unsigned int nIndex);

	enum
	{
		MSG_SETLEVELDATA_CHANGE = VveUnsteadyData::MSG_LAST,
		MSG_REMLEVELDATA,
		MSG_LAST
	};
protected:  
	std::vector<IVveDataItem *> m_vecData;
};

/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/* NAME: GetLevelData                                                         */
/*                                                                            */
/*============================================================================*/
inline IVveDataItem *VveDiscreteData::GetLevelDataByVisTime(double dVisTime) const
{
	//int iIndex = m_pTimeMapper->GetTimeIndex(fVisTime);
	int iIndex = m_pTimeMapper->GetLevelIndex(dVisTime);
	if (iIndex >= 0 && iIndex < (int)m_vecData.size())
	{
		return m_vecData[iIndex];
	}

	return NULL;
}

inline IVveDataItem *VveDiscreteData::GetLevelDataByLevelIndex(int iLevel) const
{
	if (iLevel >= 0 && iLevel < (int)m_vecData.size())
	{
		return m_vecData[iLevel];
	}

	return NULL;
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetNumberOfLevels                                                    */
/*                                                                            */
/*============================================================================*/
inline int VveDiscreteData::GetNumberOfLevels() const
{
	return int(m_vecData.size());
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetTimeMapper                                                        */
/*                                                                            */
/*============================================================================*
inline VveTimeMapper *VveDiscreteData::GetTimeMapper() const
{
	return m_pTimeMapper;
}
*/
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VVEDISCRETEDATA_H

