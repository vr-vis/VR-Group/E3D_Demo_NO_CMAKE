/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/




#ifndef _VVEKDTREE_H
#define _VVEKDTREE_H

/**
* (c) Matthew B. Kennel, Institute for Nonlinear Science, UCSD (2004)
*
* Licensed under the Academic Free License version 1.1 found in file LICENSE
* with additional provisions in that same file.
*
* The code was modified by Marc Wolter in order to remove boost data structures,
* adapt the code to the majority of ViSTA style guides and improve readability.
*
**/




/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <vector>
#include <algorithm>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/
typedef struct {
	float fLower, fUpper;
} SInterval;

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveKdTreeNode; 
class CSearchRecord;
class VveKdTreeResultVector;
struct SKdtreeResult;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
* class VveKdTree
*
* Implement a kd tree for fast searching of points in a fixed data base
* in k-dimensional Euclidean space.
*
* The main data structure, one for each k-d tree, pointing
* to a tree of an indeterminate number of "VveKdTreeNode"s.
* 
**/

class VISTAVISEXTAPI VveKdTree {

public:
	/**
	* Constructor, passing in a 2D-float array containing the data, with the number of points iN
	* and for each point iDim values.
	*
	* Constructor has optional 'dim_in' feature, to use only
	* first 'dim_in' components for definition of nearest neighbors.
	**/

	VveKdTree(float* data_in,int iN, int iDim, bool rearrange_in = true,int dim_in=-1);

	// destructor
	virtual ~VveKdTree();


public:
	// search routines

	void GetNNearest_BruteForce(std::vector<float>& qv, int nn, VveKdTreeResultVector& result) const;
	// search for n nearest to a given query vector 'qv' usin
	// exhaustive slow search.  For debugging, usually.

	void GetNNearest(std::vector<float>& qv, int nn, VveKdTreeResultVector& result) const;
	// search for n nearest to a given query vector 'qv'.

	void GetNNearest_AroundPoint(int idxin, int correltime, int nn,
		VveKdTreeResultVector& result) const;
	// search for 'nn' nearest to point [idxin] of the input data, excluding
	// neighbors within correltime 

	void GetNearestByRadius(std::vector<float>& qv, float r2,VveKdTreeResultVector& result) const; 
	// search for all neighbors in ball of size (square Euclidean distance)
	// r2.   Return number of neighbors in 'result.size()', 

	void GetNearestByRadius_AroundPoint(int idxin, int correltime, float r2,
		VveKdTreeResultVector& result) const;
	// like 'r_nearest', but around existing point, with decorrelation
	// interval. 

	size_t GetNumberOfNeighborsWithin(std::vector<float>& qv, float r2) const;
	// count number of neighbors within square distance r2.
	size_t GetNumberOfNeighbors_AroundPoint(int idxin, int correltime, float r2) const;
	// like r_count, c

	friend class VveKdTreeNode;
	friend class CSearchRecord;

public: 
	/**
	* "the_data" is a reference to the underlying multi_array of the
	* data to be included in the tree.
	*
	* NOTE: this structure does *NOT* own the storage underlying this.
	* Hence, it would be a very bad idea to change the underlying data
	* during use of the search facilities of this tree.
	* Also, the user must deallocate the memory underlying it.
	**/
	const float* m_pData;   


	const int m_iNumberOfPoints;   // number of data points
	int m_iDim; //
	bool m_bSortResults;  // USERS set to 'true'. 
	const bool m_bRearrange; // are we rearranging? 

private:
	// private data members

	VveKdTreeNode* m_pRoot; // the root pointer

	// pointing either to the_data or an internal
	// rearranged data as necessary
	const float* m_pInternalData;

	// the index for the tree leaves.  Data in a leaf with bounds [l,u] are
	// in  'the_data[ind[l],*] to the_data[ind[u],*]
	std::vector<int> m_vecTreeLeaves; 

	// if rearrange is true then this is the rearranged data storage. 
	float* m_pRearrangedData;  


	static const int m_iBucketSize = 12;  // global constant. 

private:
	void SetData(float& din); 
	void BuildTree(); // builds the tree.  Used upon construction. 
	VveKdTreeNode* BuildTreeForRange(int l, int u, VveKdTreeNode* parent);
	void SelectOnCoordinate(int c, int k, int l, int u); 
	int SelectOnCoordinateValue(int c, float alpha, int l, int u); 
	void SpreadInCoordinate(int c, int l, int u, SInterval& interv);
};



/**
* class VveKdTreeNode
*
* a node in the tree.  Many are created per tree dynamically.. 
* 
**/
class VISTAVISEXTAPI VveKdTreeNode {
public:
	// constructor
	VveKdTreeNode(int dim);

	// destructor
	virtual ~VveKdTreeNode();

private:
	// visible to self and VveKdTree.
	friend class VveKdTree;  // allow VveKdTree to access private 

	int m_iCutDimension;                                 // dimension to cut; 
	float m_fCutValue, m_fCutValueLeft, m_fCutValueRight;  //cut value
	int l,u;  // extents in index array for searching

	std::vector<SInterval> m_vecBounds; // [min,max] of the box enclosing all points

	VveKdTreeNode *m_pLeftNode, *m_pRightNode;  // pointers to left and right nodes. 

	// recursive innermost core routine for searching.. 
	void search(CSearchRecord& sr) const; 

	// return true if the bounding box for this node is within the
	// search range given by the searchvector and maximum ballsize in 'sr'. 
	bool box_in_search_range(CSearchRecord& sr) const;

	// for processing final buckets. 
	void process_terminal_node(CSearchRecord& sr) const;
	void process_terminal_node_fixedball(CSearchRecord& sr) const;

};

/**
* struct SKdtreeResult
* class  VveKdTreeResultVector
**/



struct SKdtreeResult {
	// 
	// the search routines return a (wrapped) vector
	// of these. 
	//
public:
	float fDistance;  // its square Euclidean distance
	int iIndex;    // which neighbor was found
}; 

class VISTAVISEXTAPI VveKdTreeResultVector : public std::vector<SKdtreeResult> {
	// inherit a std::vector<SKdtreeResult>
	// but, optionally maintain it in heap form as a priority
	// queue.
public:

	//  
	// add one new element to the list of results, and
	// keep it in heap order.  To keep it in ordinary, as inserted,
	// order, then simply use push_back() as inherited
	// via vector<> 

	void push_element_and_heapify(SKdtreeResult&);
	float replace_maxpri_elt_return_new_maxpri(SKdtreeResult&);

	// return the distance which has the maximum value of all on list, 
	// assuming that ALL insertions were made by
	// push_element_and_heapify() 
	float max_value(); 
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif
