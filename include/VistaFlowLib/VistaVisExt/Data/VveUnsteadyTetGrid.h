/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VFLUNSTEADYTETGRID_H
#define _VFLUNSTEADYTETGRID_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Data/VveDataItem.h>
#include <VistaVisExt/Data/VveDiscreteDataTyped.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTetGrid;

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 * Here, we just typedef a certain type of VveDataItem
 * for easier declaration of particle data objects.
 */
typedef VveDataItem<VveTetGrid> VveTetGridItem;

/**
 * VveUnsteadyTetGrid manages an unsteady tetrahedral grid. In addition
 * to normal operation of VflDiscreteData it optionally provides a grid
 * for storing topological information (e.g. vertex positions and cell
 * information), which can be shared among all time levels.
 */
class VISTAVISEXTAPI VveUnsteadyTetGrid : public VveDiscreteDataTyped<VveTetGrid>
{
public:
    VveUnsteadyTetGrid(VveTimeMapper *pTM);
	virtual ~VveUnsteadyTetGrid();

	void SetTopologyGrid(VveTetGridItem *pGrid);
	VveTetGridItem *GetTopologyGrid() const;

	void SetBounds(const VistaVector3D &v3Min, const VistaVector3D &v3Max);
	void GetBounds(VistaVector3D &v3Min, VistaVector3D &v3Max) const;
	void ComputeBounds();

protected:
	VveTetGridItem *m_pTopologyGrid;
	VistaVector3D	m_v3BoundsMin, m_v3BoundsMax;
};

#endif // _VFLUNSTEADYTETGRID_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

