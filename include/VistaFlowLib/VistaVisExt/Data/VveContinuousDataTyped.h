/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVECONTINUOUSDATATYPED_H
#define _VVECONTINUOUSDATATYPED_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
//ViSTA includes

// ViSTA FlowLib includes
#include "VveContinuousData.h"
#include "VveDataItem.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*/
template<class TRawVisData>
class VveContinuousDataTyped : public VveContinuousData
{
public:
	typedef TRawVisData                         value_type;
	typedef VveDataItem< TRawVisData > container_type;

	VveContinuousDataTyped(VveTimeMapper *pTM)
		: VveContinuousData(pTM)
	{
	}

    virtual ~VveContinuousDataTyped();
	
    VveDataItem<TRawVisData> *GetData() const;
};

/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

template<class TRawVisData>
inline VveContinuousDataTyped<TRawVisData>::~VveContinuousDataTyped()
{
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData                                                     */
/*                                                                            */
/*============================================================================*/
template<class TRawVisData>
inline VveDataItem<TRawVisData> *VveContinuousDataTyped<TRawVisData>::GetData() const
{
    return dynamic_cast<VveDataItem<TRawVisData>*>(VveContinuousData::GetData());
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VVECONTINUOUSDATATYPED_H

