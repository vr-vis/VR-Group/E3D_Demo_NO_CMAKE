/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEDATASETDESCRIPTION_H
#define _VVEDATASETDESCRIPTION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaAspects/VistaNameable.h>
#include <string>
#include <map>
#include <list>
#include <VistaVisExt/VistaVisExtConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveComponentDescription;
class VveTimeMapper;
class VveMetaInfoDescription;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/* A dataset represents a collection of different data components of possibly
 * different types that belong together (e.g., a structured grid representing
 * volume information and polygonal geometry information). This components
 * may have separate timing information (time mappers); in the simplest case,
 * the dataset has a time mapper that is shared by all components. */
class VISTAVISEXTAPI VveDatasetDescription : public IVistaNameable
{
public:
	
	VveDatasetDescription();
	virtual ~VveDatasetDescription();

	/* Add a component to the dataset. Components are managed depending on
	 * their name, so each component must have one, and that one must be
	 * unique. If the dataset does not have a time mapper, the component
	 * must have one (!= NULL).
	 * Components are managed by this dataset, i.e., they are deleted when
	 * the dataset is deleted.
	*/
	virtual bool AddComponent(VveComponentDescription* pComponent);

	/* Remove the given component from the dataset */
	virtual bool RemoveComponent(std::string strComponentName);

	/* Retrieve the component with the given, unique name. Returns NULL
	 * if the component is not set */
	virtual VveComponentDescription* GetComponent(std::string strComponentName);

	/* Retrieve the number of components of this dataset */
	virtual int GetNumberOfComponents();

	/* Retrieve a list of names of components in this dataset */
	virtual std::list<std::string> GetListOfComponentNames();

	/* Set the time mapper for this dataset. Might be NULL, if all components
	 * have their own time mapper. */
	virtual void SetTimeMapper(VveTimeMapper *pTimeMapper);

	/* Retrieve the time mapper for this dataset */
	virtual VveTimeMapper* GetTimeMapper();


	/* Set an optional meta info storing additional information about this
	 * dataset */
	virtual void SetMetaInfo(VveMetaInfoDescription *pMetaInfo);

	/* Retrieve the (optional) meta info */
	virtual VveMetaInfoDescription* GetMetaInfo();

	/* Set an arbitrary description for this dataset. */
	virtual void SetDescription(std::string strDescription);

	/* Get description of this dataset */
	virtual std::string GetDescription();

	/* Retrieve the name of the dataset. It is optional as long as the
	 * the dataset is not part of an ensemble. Within an ensemble, the dataset
	 * name has to be unique. */
	virtual std::string GetNameForNameable() const;

	/* Set the name of the dataset. It is optional as long as the
	* the dataset is not part of an ensemble. Within an ensemble, the dataset
	* name has to be unique. */
	virtual void SetNameForNameable( const std::string &sNewName );


	
protected:

	std::map<std::string,VveComponentDescription*> m_mapComponents;

	VveTimeMapper							*m_pTimeMapper;
	
	std::string								m_strName;
	std::string								m_strDescription;
	
	VveMetaInfoDescription							*m_pMetaInfo;
	
};

#endif 


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
