/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveComponentDescription.h"
#include "VveTimestepDescription.h"
#include "VveMetaInfoDescription.h"

#include <VistaVisExt/Tools/VveTimeMapper.h>



/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveComponentDescription::VveComponentDescription(std::string strName,
	DataComponentType eType, 
	VveTimeMapper *pTimeMapper)
	:	m_eType(eType)
	,	m_strName(strName)
	,	m_pTimeMapper(pTimeMapper)
	,	m_strScalars("")
	,	m_strVectors("")
	,	m_pMetaInfo(NULL)
	,	m_strDescription("")
{
}

VveComponentDescription::~VveComponentDescription()
{
	// delete all time levels
	std::map<int,VveTimestepDescription*>::iterator it;
	for(it = m_mapTimesteps.begin(); it != m_mapTimesteps.end(); it++)
	{
		if(it->second)
			delete it->second;
	}
	// delete meta info
	if(m_pMetaInfo) delete m_pMetaInfo;
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/


VveTimeMapper* VveComponentDescription::GetTimeMapper()
{
	return m_pTimeMapper;
}

bool VveComponentDescription::SetTimelevel( int iLevel, VveTimestepDescription *pTimeStep )
{
	// illegal index?
	if(m_pTimeMapper->GetLevelIndexForTimeLevel(iLevel) == -1)
		return false;

	// null?
	if(pTimeStep == NULL)
	{
		m_mapTimesteps[iLevel] = NULL;
		return true;
	}

	// wrong datatype?
	if(pTimeStep->GetType() != m_eType) return false;

	// all fine.
	m_mapTimesteps[iLevel] = pTimeStep;

	return true;
}


VveTimestepDescription* VveComponentDescription::GetTimelevel( int iLevel )
{
	// illegal index?
	std::map<int, VveTimestepDescription*>::iterator it =
		m_mapTimesteps.find(iLevel);
	if(it == m_mapTimesteps.end()) return NULL;

	// all fine.
	return it->second;
}


void VveComponentDescription::SetMetaInfo( VveMetaInfoDescription *pMetaInfo )
{
	m_pMetaInfo = pMetaInfo;
}

VveMetaInfoDescription* VveComponentDescription::GetMetaInfo()
{
	return m_pMetaInfo;
}

VveComponentDescription::DataComponentType VveComponentDescription::GetType()
{
	return m_eType;
}

void VveComponentDescription::SetScalars( std::string strScalars )
{
	m_strScalars = strScalars;
}

std::string VveComponentDescription::GetScalars()
{
	return m_strScalars;
}

void VveComponentDescription::SetVectors( std::string strVectors )
{
	m_strVectors = strVectors;
}

std::string VveComponentDescription::GetVectors()
{
	return m_strVectors;
}

std::string VveComponentDescription::GetNameForNameable() const
{
	return m_strName;
}

void VveComponentDescription::SetNameForNameable( const std::string &sNewName )
{
	m_strName = sNewName;
}

void VveComponentDescription::SetDescription( std::string strDescription )
{
	m_strDescription = strDescription;
}

std::string VveComponentDescription::GetDescription()
{
	return m_strDescription;
}

std::list<int> VveComponentDescription::GetListOfTimelevels()
{
	std::list<int> lisRet;
	std::map<int,VveTimestepDescription*>::iterator it;
	for(it = m_mapTimesteps.begin(); it != m_mapTimesteps.end(); it++)
	{
		lisRet.push_back(it->first);
	}
	return lisRet;
}



/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

