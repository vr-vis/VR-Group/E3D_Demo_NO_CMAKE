/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VVECOMPONENTDESCRIPTION_H
#define _VVECOMPONENTDESCRIPTION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaAspects/VistaNameable.h>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <VistaVisExt/VistaVisExtConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveTimeMapper;
class VveTimestepDescription;
class VveMetaInfoDescription;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/* A data component represents one unsteady, self-consistent set of data
 * (i.e., one structured grid). It must have a time mapper that may be shared
 * across different components (in the simplest case, the same for all
 * components of this dataset). */
class VISTAVISEXTAPI VveComponentDescription : public IVistaNameable
{
public:
	
	enum DataComponentType
	{
		DCT_STRUCTURED_POINTS,
		DCT_STRUCTURED_GRID,
		DCT_UNSTRUCTURED_GRID,
		DCT_POLYDATA,
		DCT_RECTILINEAR_GRID
	};

	VveComponentDescription(std::string strName,
		DataComponentType eType, VveTimeMapper *pTimeMapper);
	virtual ~VveComponentDescription();

	/* Retrieve the time mapper of this data component */
	virtual VveTimeMapper* GetTimeMapper();
	
	/* Set the timestep for the given time level. It must have the same
	 * data type as the data component.
	 * Timesteps are managed by this component, i.e., they are deleted
	 * when the component is deleted. */
	virtual bool SetTimelevel(int iLevel, VveTimestepDescription *pTimeStep);

	/* Retrieve the timestep object for the given time level. 
	   Returns NULL if it is not set. */
	virtual VveTimestepDescription* GetTimelevel(int iLevel);

	/* Retrieve a list of all timesteps */
	virtual std::list<int> GetListOfTimelevels();

	/* Set an optional meta info storing additional information about this
	 * component */
	virtual void SetMetaInfo(VveMetaInfoDescription *pMetaInfo);

	/* Retrieve the (optional) meta info */
	virtual VveMetaInfoDescription* GetMetaInfo();

	/* Retrieve the data type of this component */
	virtual DataComponentType GetType();

	/* Set the scalars that should be loaded in this component */
	virtual void SetScalars(std::string strScalars);

	/* Returns the scalars that are loaded in this component */
	virtual std::string GetScalars();

	/* Set the vectors that should be loaded in this component */
	virtual void SetVectors(std::string strVectors);

	/* Returns the vectors that are loaded in this component */
	virtual std::string GetVectors();

	/* Set an arbitrary description for this component. */
	virtual void SetDescription(std::string strDescription);

	/* Get description of this component */
	virtual std::string GetDescription();

	/* Retrieve the name of the component. All components must have a name
	 * that has to be unique within their dataset. */
	virtual std::string GetNameForNameable() const;

	/* Set the name of the component. All components must have a name
	 * that has to be unique within their dataset. */
	virtual void SetNameForNameable( const std::string &sNewName );

protected:

	VveTimeMapper					*m_pTimeMapper;
	std::map<int,VveTimestepDescription*>	m_mapTimesteps;
	VveMetaInfoDescription					*m_pMetaInfo;

	DataComponentType				m_eType;

	std::string						m_strScalars;
	std::string						m_strVectors;

	std::string						m_strName;
	std::string						m_strDescription;
};

#endif 


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
