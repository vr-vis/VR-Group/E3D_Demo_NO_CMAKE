/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveMetaInfoDescription.h"
#include <VistaAspects/VistaPropertyList.h>



/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveMetaInfoDescription::VveMetaInfoDescription(VistaPropertyList *pPropertyList)
	:	m_pPropertyList(pPropertyList)
{

}

VveMetaInfoDescription::~VveMetaInfoDescription()
{
	if(m_pPropertyList)
		delete m_pPropertyList;
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/


VistaPropertyList* VveMetaInfoDescription::GetPropertyList()
{
	return m_pPropertyList;
}


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

