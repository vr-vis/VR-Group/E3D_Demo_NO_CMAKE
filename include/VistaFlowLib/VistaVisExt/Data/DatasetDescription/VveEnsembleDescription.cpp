/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VveEnsembleDescription.h"
#include "VveDatasetDescription.h"
#include "VveMetaInfoDescription.h"

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveEnsembleDescription::VveEnsembleDescription()
	:	m_pMetaInfo(NULL)
	,	m_strName("")
	,	m_strDescription("")
{
}

VveEnsembleDescription::~VveEnsembleDescription()
{
	// delete components
	std::map<std::string, VveDatasetDescription*>::iterator it;
	for(it = m_mapDatasets.begin(); it != m_mapDatasets.end(); it++)
	{
		delete it->second;
	}
	// delete meta info
	if(m_pMetaInfo) 
		delete m_pMetaInfo;
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/


bool VveEnsembleDescription::AddDataset( VveDatasetDescription *pDataset )
{
	// no empty datasets
	if(pDataset == NULL) return false;

	// is a component with that name already set?
	if(m_mapDatasets.find(pDataset->GetNameForNameable()) !=
		m_mapDatasets.end())
		return false;

	// ok, set
	std::string strName = pDataset->GetNameForNameable();
	m_mapDatasets[strName] = pDataset;

	return true;
}

bool VveEnsembleDescription::RemoveDataset( std::string strDatasetName )
{
	std::map<std::string,VveDatasetDescription*>::iterator it =
		m_mapDatasets.find(strDatasetName);
	if(it == m_mapDatasets.end()) return false;

	m_mapDatasets.erase(it);

	return true;
}

VveDatasetDescription* VveEnsembleDescription::GetDataset( std::string strDatasetName )
{
	std::map<std::string,VveDatasetDescription*>::iterator it =
		m_mapDatasets.find(strDatasetName);
	if(it == m_mapDatasets.end()) return NULL;

	return it->second;
}

int VveEnsembleDescription::GetNumberOfDatasets()
{
	return (int)m_mapDatasets.size();
}

std::list<std::string> VveEnsembleDescription::GetListOfDatasetNames()
{
	std::list<std::string> lisRet;
	std::map<std::string, VveDatasetDescription*>::iterator it;
	for(it = m_mapDatasets.begin(); it != m_mapDatasets.end(); it++)
	{
		lisRet.push_back(it->first);
	}

	return lisRet;
}

std::string VveEnsembleDescription::GetNameForNameable() const
{
	return m_strName;
}

void VveEnsembleDescription::SetNameForNameable( const std::string &sNewName )
{
	m_strName = sNewName;
}

void VveEnsembleDescription::SetMetaInfo( VveMetaInfoDescription *pMetaInfo )
{
	m_pMetaInfo = pMetaInfo;
}

VveMetaInfoDescription* VveEnsembleDescription::GetMetaInfo()
{
	return m_pMetaInfo;
}

void VveEnsembleDescription::SetDescription( std::string strDescription )
{
	m_strDescription = strDescription;
}

std::string VveEnsembleDescription::GetDescription()
{
	return m_strDescription;
}





/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

