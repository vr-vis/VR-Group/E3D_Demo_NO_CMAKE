/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveBlockDescription.h"
#include "VveMetaInfoDescription.h"
#include "VveComponentDescription.h"



/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveBlockDescription::VveBlockDescription(std::string strFilename,
	VveComponentDescription::DataComponentType eDataType)
	:	m_strFilename(strFilename)
	,	m_eType(eDataType)
	,	m_pMetaInfo(NULL)
{
}

VveBlockDescription::~VveBlockDescription()
{
	// delete meta info
	if(m_pMetaInfo) delete m_pMetaInfo;
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void VveBlockDescription::SetMetaInfo( VveMetaInfoDescription *pMetaInfo )
{
	m_pMetaInfo = pMetaInfo;
}

VveMetaInfoDescription* VveBlockDescription::GetMetaInfo()
{
	return m_pMetaInfo;
}


VveComponentDescription::DataComponentType VveBlockDescription::GetType()
{
	return m_eType;
}

std::string VveBlockDescription::GetFilename()
{
	return m_strFilename;
}


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

