/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveTimestepDescription.h"
#include "VveMetaInfoDescription.h"
#include "VveBlockDescription.h"
#include "VveComponentDescription.h"


/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveTimestepDescription::VveTimestepDescription(VveComponentDescription::DataComponentType eDataType)
	:	m_eType(eDataType)
	,	m_pMetaInfo(NULL)
{
	
}

VveTimestepDescription::~VveTimestepDescription()
{
	// delete all blocks
	for(int i = 0; i < (int)m_vecBlocks.size(); i++)
	{
		delete m_vecBlocks[i];
	}
	// delete meta info
	if(m_pMetaInfo) delete m_pMetaInfo;
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void VveTimestepDescription::SetMetaInfo( VveMetaInfoDescription *pMetaInfo )
{
	m_pMetaInfo = pMetaInfo;
}

VveMetaInfoDescription* VveTimestepDescription::GetMetaInfo()
{
	return m_pMetaInfo;
}


int VveTimestepDescription::AddBlock( VveBlockDescription *pBlock )
{
	// no block given? we don't want NULLs.
	if(pBlock == NULL) return -1;

	// block has wrong datatype?
	if(pBlock->GetType() != m_eType) return -1;

	// ok, all fine
	m_vecBlocks.push_back(pBlock);
	return (int)(m_vecBlocks.size()-1);
}

VveBlockDescription* VveTimestepDescription::GetBlock( int iNr )
{
	// out of bounds
	if(iNr < 0 || iNr >= (int)m_vecBlocks.size())
		return NULL;

	return m_vecBlocks[iNr];
}

VveComponentDescription::DataComponentType VveTimestepDescription::GetType()
{
	return m_eType;
}

int VveTimestepDescription::GetNumberOfBlocks()
{
	return (int)m_vecBlocks.size();
}



/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

