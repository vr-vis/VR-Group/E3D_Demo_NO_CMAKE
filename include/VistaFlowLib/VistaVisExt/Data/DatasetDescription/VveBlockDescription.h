/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEBLOCKDESCRIPTION_H
#define _VVEBLOCKDESCRIPTION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VveComponentDescription.h"
#include <string>
#include <VistaVisExt/VistaVisExtConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveMetaInfoDescription;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/* A data block is the fundamental storage unit and consists of one file in
 * the file system. */
class VISTAVISEXTAPI VveBlockDescription
{
public:
	
	VveBlockDescription(std::string strFilename,
		VveComponentDescription::DataComponentType eDataType);
	virtual ~VveBlockDescription();


	/* Set an optional meta info storing additional information about this
	 * block */
	virtual void SetMetaInfo(VveMetaInfoDescription *pMetaInfo);

	/* Retrieve the (optional) meta info */
	virtual VveMetaInfoDescription* GetMetaInfo();

	/* Retrieve the data type of this block. This is always equal to the type
	 * of the timestep / component this block belongs to. */
	virtual VveComponentDescription::DataComponentType GetType();

	/* Retrieve the filename storing this block's data in the file system. */
	virtual std::string GetFilename();
	
protected:

	VveMetaInfoDescription							*m_pMetaInfo;
	VveComponentDescription::DataComponentType		m_eType;

	std::string								m_strFilename;
};

#endif 


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
