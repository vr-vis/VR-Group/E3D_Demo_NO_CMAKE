/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEENSEMBLEDESCRIPTION_H
#define _VVEENSEMBLEDESCRIPTION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaAspects/VistaNameable.h>
#include <string>
#include <map>
#include <list>
#include <VistaVisExt/VistaVisExtConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveDatasetDescription;
class VveTimeMapper;
class VveMetaInfoDescription;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/* An ensemble represents a collection of different (per se) independent
 * datasets. If we have only one dataset, this class is not needed. */
class VISTAVISEXTAPI VveEnsembleDescription : public IVistaNameable
{
public:
	
	VveEnsembleDescription();
	virtual ~VveEnsembleDescription();

	/* Add a dataset to the ensemble. They are managed using their name,
	 * so each dataset must have a unique one.
	 * Datasets registered here are managed by the ensemble, i.e., they
	 * are deleted when the ensemble is deleted.
	 */
	virtual bool AddDataset(VveDatasetDescription *pDataset);

	/* Remove the given dataset from the ensemble */
	virtual bool RemoveDataset(std::string strDatasetName);

	/* Returns the dataset with the given name. Returns NULL if the dataset
	 * is not in this ensemble */
	virtual VveDatasetDescription* GetDataset(std::string strDatasetName);

	/* Returns the number of datasets in this component */
	virtual int GetNumberOfDatasets();

	/* Retrieve a list of names of datasets in this ensemble */
	virtual std::list<std::string> GetListOfDatasetNames();

	/* Retrieve the (optional) name of the ensemble. */
	virtual std::string GetNameForNameable() const;

	/* Set the (optional) name of the ensemble. */
	virtual void SetNameForNameable( const std::string &sNewName );

	/* Set an optional meta info storing additional information about this
	 * ensemble. */
	virtual void SetMetaInfo(VveMetaInfoDescription *pMetaInfo);

	/* Retrieve the (optional) meta info */
	virtual VveMetaInfoDescription* GetMetaInfo();

	/* Set an arbitrary description for this ensemble. */
	virtual void SetDescription(std::string strDescription);

	/* Get description of this ensemble */
	virtual std::string GetDescription();


	
protected:

	std::map<std::string,VveDatasetDescription*>	m_mapDatasets;

	std::string								m_strName;
	std::string								m_strDescription;
	VveMetaInfoDescription							*m_pMetaInfo;
	
};

#endif 


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
