/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VveDatasetDescription.h"
#include "VveComponentDescription.h"
#include "VveMetaInfoDescription.h"

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveDatasetDescription::VveDatasetDescription()
	:	m_pTimeMapper(NULL)
	,	m_strName("")
	,	m_pMetaInfo(NULL)
	,	m_strDescription("")
{
}

VveDatasetDescription::~VveDatasetDescription()
{
	// delete components
	std::map<std::string, VveComponentDescription*>::iterator it;
	for(it = m_mapComponents.begin(); it != m_mapComponents.end(); it++)
	{
		delete it->second;
	}
	// delete metainfo
	if(m_pMetaInfo) delete m_pMetaInfo;
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/


bool VveDatasetDescription::AddComponent( VveComponentDescription* pComponent )
{
	// no empty components
	if(pComponent == NULL) return false;

	// check: if we don't have a time mapper, the component must have one!
	if(m_pTimeMapper == NULL)
		if(pComponent->GetTimeMapper() == NULL)
			return false;

	// is a component with that name already set?
	if(m_mapComponents.find(pComponent->GetNameForNameable()) !=
		m_mapComponents.end())
		return false;

	// ok, set
	std::string strName = pComponent->GetNameForNameable();
	m_mapComponents[strName] = pComponent;

	return true;
}

bool VveDatasetDescription::RemoveComponent( std::string strComponentName )
{
	std::map<std::string,VveComponentDescription*>::iterator it =
		m_mapComponents.find(strComponentName);
	if(it == m_mapComponents.end()) return false;

	m_mapComponents.erase(it);

	return true;
}

VveComponentDescription* VveDatasetDescription::GetComponent( std::string strComponentName )
{
	std::map<std::string,VveComponentDescription*>::iterator it =
		m_mapComponents.find(strComponentName);
	if(it == m_mapComponents.end()) return NULL;

	return it->second;
}

int VveDatasetDescription::GetNumberOfComponents()
{
	return (int)m_mapComponents.size();
}

void VveDatasetDescription::SetTimeMapper( VveTimeMapper *pTimeMapper )
{
	m_pTimeMapper = pTimeMapper;
}

VveTimeMapper* VveDatasetDescription::GetTimeMapper()
{
	return m_pTimeMapper;
}

std::list<std::string> VveDatasetDescription::GetListOfComponentNames()
{
	std::list<std::string> lisRet;
	std::map<std::string, VveComponentDescription*>::iterator it;
	for(it = m_mapComponents.begin(); it != m_mapComponents.end(); it++)
	{
		lisRet.push_back(it->first);
	}

	return lisRet;
}

std::string VveDatasetDescription::GetNameForNameable() const
{
	return m_strName;
}

void VveDatasetDescription::SetNameForNameable( const std::string &sNewName )
{
	m_strName = sNewName;
}

void VveDatasetDescription::SetMetaInfo( VveMetaInfoDescription *pMetaInfo )
{
	m_pMetaInfo = pMetaInfo;
}

VveMetaInfoDescription* VveDatasetDescription::GetMetaInfo()
{
	return m_pMetaInfo;
}

void VveDatasetDescription::SetDescription( std::string strDescription )
{
	m_strDescription = strDescription;
}

std::string VveDatasetDescription::GetDescription()
{
	return m_strDescription;
}




/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

