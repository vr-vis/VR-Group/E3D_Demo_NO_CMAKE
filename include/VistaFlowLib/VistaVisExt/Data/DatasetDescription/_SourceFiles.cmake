

set( RelativeDir "./Data/DatasetDescription" )
set( RelativeSourceGroup "Source Files\\Data\\DatasetDescription" )

set( DirFiles
	VveBlockDescription.h
	VveBlockDescription.cpp
	VveComponentDescription.h
	VveComponentDescription.cpp
	VveDatasetDescription.h
	VveDatasetDescription.cpp
	VveEnsembleDescription.h
	VveEnsembleDescription.cpp
	VveMetaInfoDescription.h
	VveMetaInfoDescription.cpp
	VveTimestepDescription.h
	VveTimestepDescription.cpp
	_SourceFiles.cmake
)

set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

