/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VVETIMESTEPDESCRIPTION_H
#define _VVETIMESTEPDESCRIPTION_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <vector>
#include "VveComponentDescription.h"
#include <VistaVisExt/VistaVisExtConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveMetaInfoDescription;
class VveBlockDescription;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/* A timestep manages a single time level of one component. It consists of
 * blocks representing the data in the file system (in the simplest case,
 * there is one block per timestep). */
class VISTAVISEXTAPI VveTimestepDescription 
{
public:
	
	VveTimestepDescription(VveComponentDescription::DataComponentType eDataType);
	virtual ~VveTimestepDescription();

	/* Add a data block to this time step. The data block must have the same
	 * data type as this timestep and may not be NULL. The data block is
	 * henceforth managed by this timestep (i.e., it is also deleted when
	 * this timestep is deleted).
	 * Returns the (internal) number of the data block (for future reference)
	 * or -1 on error. */
	virtual int AddBlock(VveBlockDescription *pBlock);

	/* Retrieve block with the given number. Returns NULL if that block
	 * does not exist. */
	virtual VveBlockDescription* GetBlock(int iNr);

	/* Retrieve the number of blocks in this time step. */
	virtual int GetNumberOfBlocks();

	/* Set an optional meta info storing additional information about this
	 * timestep */
	virtual void SetMetaInfo(VveMetaInfoDescription *pMetaInfo);

	/* Retrieve the (optional) meta info */
	virtual VveMetaInfoDescription* GetMetaInfo();

	/* Retrieve the data type of this timestep. This is always equal to the
	 * data type of the component this timestep belongs to. */
	virtual VveComponentDescription::DataComponentType GetType();
	
protected:

	VveMetaInfoDescription							*m_pMetaInfo;
	std::vector<VveBlockDescription*>				m_vecBlocks;

	VveComponentDescription::DataComponentType		m_eType;
};

#endif 


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
