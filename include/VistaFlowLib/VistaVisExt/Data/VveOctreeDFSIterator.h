/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEOCTREEDFSITERATOR_H
#define _VVEOCTREEDFSITERATOR_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveOctree.h"
#include <stack>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveOctreeNode;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/



/**
* simple depth first iterator for ViSTA VtkExt octrees.
*/
class VISTAVISEXTAPI VveOctreeDFSIterator{
public:
	VveOctreeDFSIterator(VveOctreeNode *pNode);
	virtual ~VveOctreeDFSIterator();

	void InitIteration(VveOctreeNode *pNode);

	/*
	* we only provide forward iteration via prefix operator
	*/
	void operator++();

	bool end() const;

	VveOctreeNode *GetCurrentNode() const;
	int GetLevel() const;

private:
	void operator=(const VveOctreeDFSIterator&){}
	VveOctreeDFSIterator(const VveOctreeDFSIterator&){}

	std::stack<VveOctreeNode *> m_stWalk;
	VveOctreeNode *m_pCurrentNode;

	int m_iLevel;
};


/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VVEOCTREEDFSITERATOR_H
