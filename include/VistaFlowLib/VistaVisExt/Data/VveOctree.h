/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEOCTREE_H
#define _VVEOCTREE_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>
#include <stack>
#include <memory.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveOctreeNode;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/


/**
* Implementation class for an octree data structure
* With the pointer-based octree node below this yields a
* branch-on-need octree (BONO) which can be used for adaptive
* octree generation on a given point cloud.
*/
class VISTAVISEXTAPI VveOctree{
public:
	VveOctree();
	virtual ~VveOctree();

	VveOctreeNode *GetRoot() const;
	void ComputeBounds(double dBounds[6]);
	int ComputeNumNodes();
	/**
	* Compute the maximum depths
	*/
	int ComputeMaxLevel();

	bool GetLeafNodes (std::vector<VveOctreeNode*> & vecLeafs);

private:
	VveOctreeNode *m_pRoot;
};

/**
* Implementation class for a single octree node for a BONO
* NOTE: Currently the "payload" of each octree node is limited
* to a point coordinate and a corresonding point id. There is no
* scalar field information or any fancy stuff like that...
*
* @todo Generalize payload and out-source it from here...
*
*/
class VISTAVISEXTAPI VveOctreeNode
{
public:
	/**
	* child indices
	*/
	enum EChildID{
		CID_INVALID = -1,
		CID_RightBottomBack = 0,
		CID_RightTopBack,
		CID_RightBottomFront,
		CID_RightTopFront,
		CID_LeftBottomBack,
		CID_LeftTopBack,
		CID_LeftBottomFront,
		CID_LeftTopFront,
	};

	VveOctreeNode();
	virtual ~VveOctreeNode();

	/**
	* allocate memory for iNewSize many points
	*/
	void AllocPoints(const std::vector<double*>::size_type iNewSize);
	/**
	* insert a new point
	*/
	void   PushPoint(double *fPt, const unsigned int id);
	/**
	* remove last inserted point (LIFO)
	*/
	void   PopPoint();
	/**
	*  get numberd point position
	*/
	double* GetPoint(const unsigned int iPos) const;
	/**
	* get numbered point id
	*/
	int    GetId(const unsigned int iPos) const;
	/**
	* get point position for last inserted item
	*/
	double* GetPoint() const;
	/**
	* get point id for last inserted item
	*/
	int    GetId() const;
	/**
	*
	*/
	void SetPoints(const std::vector<double*>& vecPoints, const std::vector<unsigned int>& vecIds);
	/**
	* clear points and ids and release all unnecessary memory
	*/
	void Clear();
	/**
	*
	*/
	bool IsEmpty() const;
	/**
	*
	*/
	std::vector<double*>::size_type GetNumPoints() const;

	/**
	* bounds in vtk format (xmin,xmax,ymin,ymax,zmin,zmax)
	*/
	void ComputeBounds();
	/**
	* override bounds from the outside (e.g. for a regular octree setup)
	*/
	void SetBounds(double dBounds[6]);
	void GetBounds(double dBounds[6]) const;
	void GetCenter(double dCenter[3]) const;
	void ClearBounds();

	void Split();
	void Merge();

	bool IsLeaf() const;
	void SetType(EChildID i);
	EChildID GetType() const;

	VveOctreeNode *GetChild(const EChildID i) const;

	//convenience methods
	VveOctreeNode *GetRBBChild() const;
	VveOctreeNode *GetRTBChild() const;
	VveOctreeNode *GetRBFChild() const;
	VveOctreeNode *GetRTFChild() const;
	VveOctreeNode *GetLBBChild() const;
	VveOctreeNode *GetLTBChild() const;
	VveOctreeNode *GetLBFChild() const;
	VveOctreeNode *GetLTFChild() const;

	void SetDeletePositions (bool bDelete);
private:
	VveOctreeNode *m_pChildren[8];
	std::vector<double*> m_vecPts;
	std::vector<unsigned int> m_vecIds;
	/*
	* this will be used to cache once computed bounds
	* FORMAT: boundsXmin, boundsXmax...boundsZmax.
	* == NULL if not computed yet
	*/
	double *m_dBounds;
	/**
	* remember type
	*/
	char m_iType;

	bool m_bDelete;
};

//****************************************************************
//****************************************************************
//                   INLINE IMPLEMENTATION
//****************************************************************
//****************************************************************
inline void VveOctreeNode::AllocPoints(const std::vector<double*>::size_type iNewSize)
{
	m_vecPts.reserve(iNewSize);
	m_vecPts.reserve(iNewSize);
}
//****************************************************************
//****************************************************************
inline void VveOctreeNode::PushPoint(double *fPt, const unsigned int id)
{
	m_vecPts.push_back(fPt);
	m_vecIds.push_back(id);
}
//****************************************************************
//****************************************************************
inline void VveOctreeNode::PopPoint()
{
	m_vecPts.pop_back();
	m_vecIds.pop_back();
}
//****************************************************************
//****************************************************************
inline double* VveOctreeNode::GetPoint(const unsigned int iPos) const
{
	return m_vecPts[iPos];
}
//****************************************************************
//****************************************************************
inline int VveOctreeNode::GetId(const unsigned int iPos) const
{
	return m_vecIds[iPos];
}
//****************************************************************
//****************************************************************
inline double* VveOctreeNode::GetPoint() const
{
	return m_vecPts.back();
}
//****************************************************************
//****************************************************************
inline int VveOctreeNode::GetId() const
{
	return m_vecIds.back();
}
//****************************************************************
//****************************************************************
inline void VveOctreeNode::SetPoints(const std::vector<double*>& vecPoints, const std::vector<unsigned int>& vecIds)
{
	m_vecPts = vecPoints;
	m_vecIds = vecIds;
}
//****************************************************************
//****************************************************************
inline void VveOctreeNode::Clear()
{
	m_vecPts.clear();
	m_vecIds.clear();
}
//****************************************************************
//****************************************************************
inline bool VveOctreeNode::IsEmpty() const
{
	return m_vecPts.empty();
}
//****************************************************************
//****************************************************************
inline std::vector<double*>::size_type VveOctreeNode::GetNumPoints() const
{
	return m_vecPts.size();
}
//****************************************************************
//****************************************************************
inline void VveOctreeNode::SetBounds(double fBounds[6])
{
	if(!m_dBounds)
		m_dBounds = new double[6];
	for(int i=0; i<6; ++i)
		m_dBounds[i] = fBounds[i];
}
//****************************************************************
//****************************************************************
inline void VveOctreeNode::GetBounds(double fBounds[6]) const
{
	for(int i=0; i<6; ++i)
		fBounds[i] = m_dBounds[i];
}
//****************************************************************
//****************************************************************
inline void VveOctreeNode::GetCenter(double fCenter[3]) const
{
	fCenter[0] = 0.5f * (m_dBounds[1] + m_dBounds[0]);
	fCenter[1] = 0.5f * (m_dBounds[3] + m_dBounds[2]);
	fCenter[2] = 0.5f * (m_dBounds[5] + m_dBounds[4]);
}
//****************************************************************
//****************************************************************
inline bool VveOctreeNode::IsLeaf() const
{
	return m_pChildren[0] == NULL;
}
//****************************************************************
//****************************************************************
inline void VveOctreeNode::SetType(EChildID i)
{
	m_iType = (char) i;
}
//****************************************************************
//****************************************************************
inline VveOctreeNode::EChildID VveOctreeNode::GetType() const
{
	return (EChildID) m_iType;
}
//****************************************************************
//****************************************************************
inline VveOctreeNode *VveOctreeNode::GetChild(const EChildID i) const
{
	return m_pChildren[i];
}
//****************************************************************
//****************************************************************
inline VveOctreeNode* VveOctreeNode::GetRBBChild() const
{
	return this->GetChild(CID_RightBottomBack);
}
//****************************************************************
//****************************************************************
inline VveOctreeNode* VveOctreeNode::GetRTBChild() const
{
	return this->GetChild(CID_RightTopBack);
}
//****************************************************************
//****************************************************************
inline VveOctreeNode* VveOctreeNode::GetRBFChild() const
{
	return this->GetChild(CID_RightBottomFront);
}
//****************************************************************
//****************************************************************
inline VveOctreeNode* VveOctreeNode::GetRTFChild() const
{
	return this->GetChild(CID_RightTopFront);
}
//****************************************************************
//****************************************************************
inline VveOctreeNode* VveOctreeNode::GetLBBChild() const
{
	return this->GetChild(CID_LeftBottomBack);
}
//****************************************************************
//****************************************************************
inline VveOctreeNode* VveOctreeNode::GetLTBChild() const
{
	return this->GetChild(CID_LeftTopBack);
}
//****************************************************************
//****************************************************************
inline VveOctreeNode* VveOctreeNode::GetLBFChild() const
{
	return this->GetChild(CID_LeftBottomFront);
}
//****************************************************************
//****************************************************************
inline VveOctreeNode* VveOctreeNode::GetLTFChild() const
{
	return this->GetChild(CID_LeftTopFront);
}

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VVEOCTREE_H
