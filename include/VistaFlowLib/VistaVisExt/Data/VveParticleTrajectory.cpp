/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



// VistaFlowLib includes
#include "VveParticleTrajectory.h"

#include <VistaBase/VistaStreamUtils.h>

#include <iostream>
#include <cstdio>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
//using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveParticleTrajectory::VveParticleTrajectory()
	:	m_iTermReason(TERM_ACTIVE)
{
}

VveParticleTrajectory::~VveParticleTrajectory()
{
    Clear();
}

/*============================================================================*/
/*                                                                            */
/* NAME: AddArray                                                             */
/*                                                                            */
/*============================================================================*/

void VveParticleTrajectory::AddArray( VveParticleDataArrayBase* array )
{
    m_vecArrays.push_back(array);
}


/*============================================================================*/
/*                                                                            */
/* NAME: GetNumArrays                                                         */
/*                                                                            */
/*============================================================================*/
int VveParticleTrajectory::GetNumArrays() const
{
	return int(m_vecArrays.size());
}


/*============================================================================*/
/*                                                                            */
/* NAME: GetArray                                                             */
/*                                                                            */
/*============================================================================*/

int VveParticleTrajectory::GetArray( size_t nIndex, 
                                     VveParticleDataArrayBase*& pArray )
{
    if (nIndex < m_vecArrays.size())
    {
        pArray = m_vecArrays[nIndex];
        return 1;
    }
    return 0;
}

VveParticleDataArrayBase* VveParticleTrajectory::GetArray(size_t nIndex)
{
    if (nIndex < m_vecArrays.size())
    {
        return m_vecArrays[nIndex];
    }
    return 0;
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetArrayByName                                                       */
/*                                                                            */
/*============================================================================*/

int VveParticleTrajectory::GetArrayByName(const std::string& sName, 
										  VveParticleDataArrayBase*& pArray )
{
	size_t nArrayIdx = 0;
    if( GetArrayIndexByName(sName, nArrayIdx))
    {
        return GetArray(nArrayIdx, pArray);
    }
#ifdef DEBUG
	vstr::warnp()<<"[VveParticleTrajectory] A particle data array named <"<<sName.c_str()<<"> does not exist!"<<std::endl
		<<"Available arrays are: <"<< GetArrayNames().c_str()<<">"<<std::endl;
#endif
    return 0;
}


/*============================================================================*/
/*                                                                            */
/* NAME: GetArrayIdByName                                                     */
/*                                                                            */
/*============================================================================*/

int VveParticleTrajectory::GetArrayIndexByName(const std::string& cName, 
											   size_t& nIndex)
{
    for( size_t i=0; i<m_vecArrays.size(); i++)
    {
        if( cName == m_vecArrays.at(i)->GetName() )
        {
            nIndex = i;
            return 1;
        }
    }
    return 0;
}

/*============================================================================*/
/*                                                                            */
/* NAME: SetReasonForTermination                                              */
/*                                                                            */
/*============================================================================*/
void VveParticleTrajectory::SetReasonForTermination( const int iReason )
{
	m_iTermReason = iReason;
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetReasonForTermination                                              */
/*                                                                            */
/*============================================================================*/
int VveParticleTrajectory::GetReasonForTermination() const
{
	return m_iTermReason;
}

/*============================================================================*/
/*                                                                            */
/* NAME: Clear                                                                */
/*                                                                            */
/*============================================================================*/

void VveParticleTrajectory::Clear()
{
    while (!m_vecArrays.empty())
    {
        delete m_vecArrays.back();
        m_vecArrays.pop_back();
    }
}

/*============================================================================*/
/*                                                                            */
/* NAME: ReduceInformation                                                    */
/*                                                                            */
/*============================================================================*/

void VveParticleTrajectory::ReduceInformation( VveTimeMapper* pMapper, 
                                               std::string sTimeArrayName, 
                                               VveParticleTrajectory *pDestination )
{
    if (!pDestination)
        return;

    // initialize destination trajectory
    InitializeArrays(pDestination);

    VveParticleDataArrayBase* pTimeArray = NULL;
    VveParticleDataArrayBase* pTimeArrayDest = NULL;

    if( this->GetArrayByName( sTimeArrayName, pTimeArray ) &&
        pDestination->GetArrayByName( sTimeArrayName, pTimeArrayDest ) &&
        pTimeArray && pTimeArrayDest)
    {
        int iTimeLevelCount = static_cast<int>(pTimeArray->GetSize());
        int iMapperIndexCount = pMapper->GetNumberOfTimeIndices();
        int iTimeIndex = 0;
        int iNextLevelIndex = 0;

        for( int iMapperIndex = 0; 
             iMapperIndex < iMapperIndexCount && iTimeIndex < iTimeLevelCount;
             ++iMapperIndex )
        {
            float fSimTime = static_cast<float>(pMapper->GetSimulationTime(iMapperIndex));
            float fTime = pTimeArray->GetElementValueAsFloat(iTimeIndex);

            if( fSimTime < fTime )
            {
                // if we're looking at the first time index for the given 
                // trajectory, allow this time index to be used, if it lies 
                // closer to the current time index than to the next time index.
                if( iTimeIndex == 0 &&
                    iMapperIndex+1 < iMapperIndexCount &&
                    fTime < 0.5f * (fSimTime + 
                    pMapper->GetSimulationTime(iMapperIndex+1)))
                {
                    CopyElements(iTimeIndex, pDestination);
                    pTimeArrayDest->SetElementValue(iNextLevelIndex, fSimTime);
                    ++iNextLevelIndex;
                }
                continue;
            }
            else if (fSimTime == fTime)
            {
                // in this case, the time indices match, so keep the 
                // current time index, but reset its time value
                CopyElements(iTimeIndex, pDestination);
                pTimeArrayDest->SetElementValue(iNextLevelIndex, fSimTime);
                ++iNextLevelIndex;
                continue;
            }

            // now, find the particle time indices, which lie "around" 
            // the mapper's current time index
            while( (iTimeIndex+1) < iTimeLevelCount && 
                   fSimTime > pTimeArray->GetElementValueAsFloat(iTimeIndex+1))
            {
                ++iTimeIndex;
            }

            if( iTimeIndex+1 >= iTimeLevelCount)
            {
                // arrived at the last particle data. Now, check, whether it's 
                // closer to the mapper's current time index than to its last one

                if( iMapperIndex > 0 &&
                    pTimeArray->GetElementValueAsFloat(iTimeIndex) >
                        0.5f * (fSimTime + pMapper->GetSimulationTime(iMapperIndex-1)))
                {
                    // it's closer, so keep it, but modify the time value
                    CopyElements(iTimeIndex, pDestination);
                    pTimeArrayDest->SetElementValue(iNextLevelIndex, fSimTime);
                    ++iNextLevelIndex;
                    continue;
                }
                // we're through with the particle data, so stop the loop...
                break;
            }
            else
            {
                // all right - interpolate particle data
                float fTime1 = pTimeArray->GetElementValueAsFloat(iTimeIndex);
                float fTime2 = pTimeArray->GetElementValueAsFloat(iTimeIndex+1);

                float fAlpha = (fSimTime-fTime1)/(fTime2-fTime1);
                InterpolateElements(iTimeIndex, iTimeIndex+1, fAlpha, pDestination);

                pTimeArrayDest->SetElementValue(iNextLevelIndex, fSimTime);
                ++iNextLevelIndex;
            }
        }
    }
    else
    {
        vstr::warnp() << "[VveParticleTrajectory::ReduceInformation]"
                  << " The given time data array \'" << sTimeArrayName <<"\' does not exist!" 
                  << std::endl;
    }
}

/*============================================================================*/
/*                                                                            */
/* NAME: InitializeArrays                                                     */
/*                                                                            */
/*============================================================================*/

void VveParticleTrajectory::InitializeArrays( VveParticleTrajectory *pDestination )
{
    pDestination->Clear();
    
    // Initialize arrays
    for( size_t i=0; i<m_vecArrays.size(); ++i )
    {
        VveParticleDataArrayBase* pSrc = m_vecArrays[i];
        pDestination->AddArray( pSrc->CreateInstance() );
    }
}

/*============================================================================*/
/*                                                                            */
/* NAME: CopyElements                                                         */
/*                                                                            */
/*============================================================================*/

void VveParticleTrajectory::
CopyElements( const int& iIndex, VveParticleTrajectory *pDestination )
{
    // Copy value at index position to destination arrays
    for( size_t i=0; i<m_vecArrays.size(); ++i )
    {
        m_vecArrays[i]->CopyElementAt( iIndex, pDestination->GetArray(i) );
    }
}

/*============================================================================*/
/*                                                                            */
/* NAME: InterpolateElements                                                  */
/*                                                                            */
/*============================================================================*/

void VveParticleTrajectory::
InterpolateElements( int iIndex1, int iIndex2,
                     double dAlpha,
                     VveParticleTrajectory * pDestination )
{
    // Copy value at index position to destination arrays
    for( size_t i=0; i<m_vecArrays.size(); ++i )
    {
        m_vecArrays[i]->InterpolateElement( iIndex1, iIndex2,
                                          dAlpha,
                                          pDestination->GetArray(i));
    }  
}

/*============================================================================*/
/*                                                                            */
/* NAME: GetParticleInstant                                                   */
/*                                                                            */
/*============================================================================*/

//SVveParticleInstant VveParticleTrajectory::GetParticleInstant( int iIndex, 
//                                                   std::string sPosArray, 
//                                                   std::string sVelArrayId, 
//                                                   std::string sTimeArrayId, 
//                                                   std::string sRadiusArrayName /*= ""*/, 
//                                                   std::string sScalarArrayName /*= ""*/ )
//{
//    SVveParticleInstant pi;
//
//    // SVveParticleInstant uses float as common data type
//    VveParticleDataArrayBase* pAPos;
//    VveParticleDataArrayBase* pAVel;
//    VveParticleDataArrayBase* pATime;
//    VveParticleDataArrayBase* pARadius;
//    VveParticleDataArrayBase* pAScalar;
//
//    if (GetArrayByName(sPosArray, pAPos) &&
//        pAPos->GetNumComponents() == 3 )
//        pAPos->GetElementCopy(iIndex, pi.aPos );
//
//    if (GetArrayByName(sVelArrayId, pAVel) &&
//        pAVel->GetNumComponents() == 3 )
//        pAVel->GetElementCopy(iIndex, pi.aVel);
//
//    if (GetArrayByName(sTimeArrayId, pATime) &&
//        pATime->GetNumComponents() == 1)
//        pi.fTime = pATime->GetElementValueAsFloat(iIndex);
//
//    if (GetArrayByName(sRadiusArrayName, pARadius) &&
//        pARadius->GetNumComponents() == 1)
//        pi.fRadius = pARadius->GetElementValueAsFloat(iIndex);
//
//    if (GetArrayByName(sScalarArrayName, pAScalar) &&
//        pAScalar->GetNumComponents() == 1)
//        pi.fScalar = pAScalar->GetElementValueAsFloat(iIndex);
//
//    pi.m_pExt = NULL; // not supported!
//
//    return pi;
//}

/*============================================================================*/
/*                                                                            */
/* NAME: GetSize                                                              */
/*                                                                            */
/*============================================================================*/
size_t VveParticleTrajectory::GetSize()
{
    if (m_vecArrays.empty())
        return 0;

    // look for the minimum size of all PDAs, start HUGE
    size_t nResult = std::numeric_limits<size_t>::max();
    bool bAllArraysEmpty = true;

    for( size_t i=0; i<m_vecArrays.size(); ++i)
    {
        // ignore empty arrays
        if( !m_vecArrays[i]->IsEmpty() )
        {
            nResult = std::min( nResult, m_vecArrays[i]->GetSize() );
            bAllArraysEmpty = false;
        }
    }

    return bAllArraysEmpty ? 0 : nResult;
}

/*============================================================================*/
/*                                                                            */
/* NAME: Resize                                                               */
/*                                                                            */
/*============================================================================*/
void VveParticleTrajectory::Resize( size_t nNumElements )
{
    for( size_t i=0; i<m_vecArrays.size(); ++i )
    {
        m_vecArrays[i]->Resize(nNumElements);
    }  
}
/*============================================================================*/
/*                                                                            */
/* NAME: Reserve                                                              */
/*                                                                            */
/*============================================================================*/
void VveParticleTrajectory::Reserve( size_t nNumElements )
{
    for( size_t i=0; i<m_vecArrays.size(); ++i )
    {
        m_vecArrays[i]->Reserve(nNumElements);
    }  
}
/*============================================================================*/
/*                                                                            */
/*  NAME: GetArrayNames                                                        */
/*                                                                            */
/*============================================================================*/

std::string VveParticleTrajectory::GetArrayNames()
{
	std::string result;
	for( size_t i=0; i<m_vecArrays.size(); ++i )
	{
		// @todo Check if this can be converted to the usual csv-style of value
		//		 lists without breaking something in dependend classes.
		result += std::string(" ") + m_vecArrays[i]->GetName();
	}  
	return result;
}



/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
