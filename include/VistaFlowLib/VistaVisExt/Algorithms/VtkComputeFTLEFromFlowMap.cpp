/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VtkComputeFTLEFromFlowMap.h RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VtkComputeFTLEFromFlowMap.h"

#include <vtkObjectFactory.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkMath.h>

#include <VistaBase/VistaStreamUtils.h>

#include <limits>

#ifdef _OPENMP
#include <omp.h>
#endif
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
vtkCxxRevisionMacro(vtkComputeFTLEFromFlowMap, "$Id: VtkComputeFTLEFromFlowMap.cpp  $");
vtkStandardNewMacro(vtkComputeFTLEFromFlowMap);
/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
vtkComputeFTLEFromFlowMap::vtkComputeFTLEFromFlowMap()
	:	m_strFlowMapField("flowmap"),
		m_strErrorField("error"),
		m_dOutsideValue(std::numeric_limits<double>::max()),
		m_strFTLEField("ftle"),
		m_dFTLEHorizon(-1.0),
		m_bPassIntermediateFields(false)
{

}

vtkComputeFTLEFromFlowMap::~vtkComputeFTLEFromFlowMap()
{

}

void vtkComputeFTLEFromFlowMap::PrintSelf(ostream& os, vtkIndent indent)
{
	vtkImageAlgorithm::PrintSelf(os, indent);
	os << indent << "Output  field name : " << m_strFTLEField << endl;
	os << indent << "FlowMap field name : " << m_strFlowMapField << endl;
	os << indent << "Error field name   : " << m_strErrorField << endl;
}

void vtkComputeFTLEFromFlowMap::SetFlowMapField( const std::string &strFieldName )
{
	m_strFlowMapField = strFieldName;
}

std::string vtkComputeFTLEFromFlowMap::GetFlowMapField() const
{
	return m_strFlowMapField;
}

void vtkComputeFTLEFromFlowMap::SetErrorField( const std::string &strErrorField )
{
	m_strErrorField = strErrorField;
}

std::string vtkComputeFTLEFromFlowMap::GetErrorField() const
{
	return m_strErrorField;
}

void vtkComputeFTLEFromFlowMap::SetOutsideValue( const double val )
{
	m_dOutsideValue = val;
}

double vtkComputeFTLEFromFlowMap::GetOutsideValue() const
{
	return m_dOutsideValue;
}

void vtkComputeFTLEFromFlowMap::SetFTLEField( const std::string &strFieldName)
{
	m_strFTLEField = strFieldName;
}

std::string vtkComputeFTLEFromFlowMap::GetFTLEField() const
{
	return m_strFTLEField;
}


void vtkComputeFTLEFromFlowMap::SetFTLEHorizon( const double val )
{
	m_dFTLEHorizon = val;
}

double vtkComputeFTLEFromFlowMap::GetFTLEHorizon() const
{
	return m_dFTLEHorizon;
}

void vtkComputeFTLEFromFlowMap::SetPassIntermediateFields( const bool val )
{
	m_bPassIntermediateFields = val;
}

bool vtkComputeFTLEFromFlowMap::GetPassIntermediateFields() const
{
	return m_bPassIntermediateFields;
}



int vtkComputeFTLEFromFlowMap::RequestData( vtkInformation *, 
											vtkInformationVector **inputVector, 
											vtkInformationVector *outputVector )
{
	// get the info objects
	//our input is the first input on the first port of this filter
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkImageData *pInput = vtkImageData::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkImageData *pOutput = vtkImageData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pOutput)
	{
		vstr::errp() << "[vtkComputeFTLEFromFlowMap::RequestData] no output!"<<endl;
		return -1;
	}
	if(!pInput)
	{
		vstr::errp() << "[vtkComputeFTLEFromFlowMap::RequestData] no input!"<<endl;
		return -1;
	}
	vtkPointData *pInPD = pInput->GetPointData();
	vtkDoubleArray *pFlowMap = vtkDoubleArray::SafeDownCast(pInPD->GetArray(m_strFlowMapField.c_str()));
	if(!pFlowMap || pFlowMap->GetNumberOfComponents() != 3)
	{
		vstr::errp() << "[vtkComputeFTLEFromFlowMap::RequestData] no valid flow map given in <"<<m_strFlowMapField.c_str()<<">!"<<endl;
		return -1;
	}
	vtkDoubleArray *pErrors = vtkDoubleArray::SafeDownCast(pInPD->GetArray(m_strErrorField.c_str()));
	if(!pErrors || pErrors->GetNumberOfComponents() != 1)
	{
		vstr::errp() << "[vtkComputeFTLEFromFlowMap::RequestData] no valid error field given in <"<<m_strErrorField.c_str()<<">!"<<endl;
		return -1;
	}
	if(m_dFTLEHorizon < 0)
	{
		vstr::errp() << "[vtkComputeFTLEFromFlowMap::RequestData] invalid FTLE horizon!"<<endl;
		return -1;
	}


	//1) configure the output
	pOutput->CopyStructure(pInput);


	//2) retrieve original data arrays
	int iDims[3];
	pInput->GetDimensions(iDims);
	//make sure our grid is at least 1 layer in each dimension
	iDims[0] = std::max<int>(iDims[0], 1);
	iDims[1] = std::max<int>(iDims[1], 1);
	iDims[2] = std::max<int>(iDims[2], 1);

	double dSpacing[3];
	pInput->GetSpacing(dSpacing);
	double d2Spacing[3] = {
		dSpacing[0] * 2,
		dSpacing[1] * 2,
		dSpacing[2] * 2
	};
	const int iNumPts = iDims[0] * iDims[1] * iDims[2];

	//allocate memory for FTLE field and attach to output
	vtkDoubleArray *pFTLE = vtkDoubleArray::New();
	pFTLE->SetNumberOfComponents(1);
	pFTLE->SetNumberOfTuples(iNumPts);
	pFTLE->SetName(m_strFTLEField.c_str());
	pFTLE->FillComponent(0,0.0);
	pOutput->GetPointData()->AddArray(pFTLE);
			
	const double dT = 1.0 / m_dFTLEHorizon;

	//low level data access
	double *dFMArr = pFlowMap->GetPointer(0);
	double *dErrorArr = pErrors->GetPointer(0);
	double *dFTLEArr = pFTLE->GetPointer(0);

//each iteration computes the FTLE val for a single point in the output field
//using only reading access to all inputs. Hence, iterations are independent of
//each other and can be processed in parallel
#pragma omp parallel for
	for(int k=0; k<iDims[2]; ++k)
	{
		//check if we are along a k-boundary
		bool bIsZB = k == 0 || k==iDims[2]-1;
		for(int j=0; j<iDims[1]; ++j)
		{
			//check if we are along a j-boundary
			bool bIsYB = j == 0 || j==iDims[1]-1;
			for(int i=0; i<iDims[0]; ++i)
			{
				int iPtId = this->GridToId(i,j,k, iDims);
				//filter out "outside" pts
				if(dErrorArr[iPtId] == m_dOutsideValue)
				{
					dFTLEArr[iPtId] = 0;
					continue;
				}
				//check if we are along an i-boundary
				bool bIsXB = i == 0 || i==iDims[0]-1;
				int idx = 0;
				//compute (clamped) neighbors for central differences
				int iNeighs[6] = {
					this->GridToId_clamp(i-1,j,k,iDims),
					this->GridToId_clamp(i+1,j,k,iDims),
					this->GridToId_clamp(i,j-1,k,iDims),
					this->GridToId_clamp(i,j+1,k,iDims),
					this->GridToId_clamp(i,j,k-1,iDims),
					this->GridToId_clamp(i,j,k+1,iDims)
				};
				//comptue offsets into the flowmap vector field
				int iNeighOfs[6] = {
					3*iNeighs[0], 3*iNeighs[1], 3*iNeighs[2], 
					3*iNeighs[3], 3*iNeighs[4], 3*iNeighs[5]
				};
				//determine sample values for differencing scheme
				double dVals[18] = {
					dFMArr[iNeighOfs[0]],	//0  == i-1, j, k
					dFMArr[iNeighOfs[0]+1], //1
					dFMArr[iNeighOfs[0]+2],	//2
					dFMArr[iNeighOfs[1]],	//3  == i+1, j, k
					dFMArr[iNeighOfs[1]+1],	//4
					dFMArr[iNeighOfs[1]+2],	//5
					dFMArr[iNeighOfs[2]],	//6  == i, j-1, k
					dFMArr[iNeighOfs[2]+1],	//7
					dFMArr[iNeighOfs[2]+2],	//8
					dFMArr[iNeighOfs[3]],	//9  == i, j+1, k
					dFMArr[iNeighOfs[3]+1],	//10 
					dFMArr[iNeighOfs[3]+2],	//11
					dFMArr[iNeighOfs[4]],	//12 == i, j, k-1
					dFMArr[iNeighOfs[4]+1],	//13
					dFMArr[iNeighOfs[4]+2],	//14
					dFMArr[iNeighOfs[5]],	//15 == i, j, k+1
					dFMArr[iNeighOfs[5]+1],	//16
					dFMArr[iNeighOfs[5]+2]	//17
				};
				//step-width for differencing scheme depends on whether
				//current pt is a boundary vertex or not
				double dH[3] = {
					(bIsXB ? dSpacing[0] : d2Spacing[0]),
					(bIsYB ? dSpacing[1] : d2Spacing[1]),
					(bIsZB ? dSpacing[2] : d2Spacing[2])
				};
				//2) compute J (Jacobi matrix) of the flow map using central diff.
				double dJ[9] = {
					//first row -- dF_u/dx, dF_u/dy, dF_u/dz
					(dVals[3]-dVals[0])   / dH[0], 
					(dVals[9]-dVals[6])   / dH[1], 
					(dVals[15]-dVals[12]) / dH[2],
					//second row -- dF_v/dx, dF_v/dy, dF_v/dz
					(dVals[4] -dVals[1])  / dH[0], 
					(dVals[10]-dVals[7])  / dH[1], 
					(dVals[16]-dVals[13]) / dH[2],
					//third row -- dF_w/dx, dF_w/dy, dF_w/dz
					(dVals[5]-dVals[2])   /dH[0], 
					(dVals[11]-dVals[8])  /dH[1], 
					(dVals[17]-dVals[14]) /dH[2]
				};


				//3) compute Js = J^t*J
				double dJs[9] ={
					//first row
					dJ[0]*dJ[0] + dJ[3]*dJ[3] + dJ[6]*dJ[6],
					dJ[0]*dJ[1] + dJ[3]*dJ[4] + dJ[6]*dJ[7],
					dJ[0]*dJ[2] + dJ[3]*dJ[5] + dJ[6]*dJ[8],
					//second row
					0,
					dJ[1]*dJ[1] + dJ[4]*dJ[4] + dJ[7]*dJ[7],
					dJ[1]*dJ[2] + dJ[4]*dJ[5] + dJ[7]*dJ[8],
					//third row
					0,
					0,
					dJ[2]*dJ[2] + dJ[5]*dJ[5] + dJ[8]*dJ[8]
				};
				//use symmetry!
				dJs[3] = dJs[1];
				dJs[6] = dJs[2];
				dJs[7] = dJs[5];

				//convert to ptr struct for vtk call
				double *dJs_p[3] = {&dJs[0],&dJs[3],&dJs[6]};

				double dEVals[3];
				double dEVecs[9];
				double *dEVecs_p[3] = {&dEVecs[0], &dEVecs[3], &dEVecs[6]};

				//4) compute Js's eigenvals
				int iret = vtkMath::Jacobi(dJs_p, dEVals, dEVecs_p);
				if(iret != 0)
				{
					//5) FTLE val = 1/integrationInterval * log(sqrt(max_eval))
					dFTLEArr[iPtId] = (dEVals[0]>0 ? dT * log(sqrt(dEVals[0])) : 0.0);
				}
				else
				{
					vstr::errp() <<"[vtkComputeFTLEFromFlowMap::RequestData] Failed to compute eigensystem!"<<endl<<endl;
					dFTLEArr[iPtId] = 0.0;
					continue;
				}
			}//for all i
		}//for all j
	}//for all k

	//assemble output
	const int iNumFields = pInPD->GetNumberOfArrays();
	vtkPointData *pOutPD = pOutput->GetPointData();
	if(m_bPassIntermediateFields)
	{
		//just pass all fields
		for(int i=0; i<iNumFields; ++i)
		{
			pOutPD->AddArray(pInPD->GetArray(i));
		}
	}
	else
	{
		//skip error and flow map fields in the output
		for(int i=0; i<iNumFields; ++i)
		{
			vtkDataArray *pCurrent = pInPD->GetArray(i);
			if( std::string(pCurrent->GetName()) == m_strFlowMapField ||
				std::string(pCurrent->GetName()) == m_strErrorField)
			{
				continue;
			}
			pOutPD->AddArray(pCurrent);
		}
	}
	
	//finally, make FTLE the active scalar field
	pOutPD->SetActiveScalars(m_strFTLEField.c_str());

	return 0;
}









/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

