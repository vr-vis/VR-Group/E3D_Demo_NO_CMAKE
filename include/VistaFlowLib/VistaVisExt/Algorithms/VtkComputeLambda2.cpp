/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VtkComputeLambda2.h"

#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkGenericCell.h>
#include <vtkCellDataToPointData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkIdList.h>

#include <VistaBase/VistaStreamUtils.h>

#ifdef _OPENMP
#include <omp.h>
#endif
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
vtkCxxRevisionMacro(vtkComputeLambda2, "$Id$");
vtkStandardNewMacro(vtkComputeLambda2);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
vtkComputeLambda2::vtkComputeLambda2() : m_bComputePointData(true), m_strOutputFieldName("lambda2")
{
}
//------------------------------------------------------------
//------------------------------------------------------------
vtkComputeLambda2::~vtkComputeLambda2()
{
}
/*============================================================================*/
/*  IMPLEMENTATION															  */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
void vtkComputeLambda2::PrintSelf(ostream& os, vtkIndent indent)
{
	vtkDataSetAlgorithm::PrintSelf(os, indent);
	os << indent << "Output field name : " << m_strOutputFieldName << endl;
	os << indent << "Generating " << (m_bComputePointData ? "point " : "cell ") << "data." << endl;
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkComputeLambda2::ComputePointDataOn()
{
	m_bComputePointData = true;
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkComputeLambda2::ComputePointDataOff()
{
	m_bComputePointData = false;
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
bool vtkComputeLambda2::GetComputePointData() const
{
	return m_bComputePointData;
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkComputeLambda2::SetOutputFieldName(const std::string& strOutputFieldName)
{
	m_strOutputFieldName = strOutputFieldName;
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
std::string vtkComputeLambda2::GetOutputFieldName() const
{
	return m_strOutputFieldName;
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtkComputeLambda2::RequestData(vtkInformation *, vtkInformationVector **inputVector, vtkInformationVector *outputVector)
{
	// get the info objects
	//our input is the first input on the first port of this filter
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkDataSet *pInput = vtkDataSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkDataSet *pOutput = vtkDataSet::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pOutput)
	{
		vstr::errp() << "[vtkComputeLambda2::RequestData] no output!" << endl;
		return -1;
	}
	if(!pInput)
	{
		vstr::errp() << "[vtkComputeLambda2::RequestData] no input!" << endl;
		return -1;
	}
	if(!this->GetComputePointData() && pInput->GetCellData()->GetArray(m_strOutputFieldName.c_str()) != NULL)
	{
		vstr::errp() << "[vtkComputeLambda2::RequestData] field <" << m_strOutputFieldName <<"> already exists in input!" << endl;
		return -1;
	}
	if(this->GetComputePointData() && pInput->GetPointData()->GetArray(m_strOutputFieldName.c_str()) != NULL)
	{
		vstr::errp() << "[vtkComputeLambda2::RequestData] field <" << m_strOutputFieldName <<"> already exists in input!" << endl;
		return -1;
	}
	// Initialize
	vtkDebugMacro(<<"Computing lambda2 values for velocity field!");
	const int iNumInCells = pInput->GetNumberOfCells();
	
	// First, copy the input to the output as a starting point
	pOutput->CopyStructure( pInput );	
	// Pass appropriate data through to output
	pOutput->GetPointData()->PassData(pInput->GetPointData());
	pOutput->GetCellData()->PassData(pInput->GetCellData());

	//retrieve vector field
	vtkDataArray *pVelocity = pInput->GetPointData()->GetVectors();
	int iVectorDim = pVelocity->GetNumberOfComponents();
	//NOTE: Velocity field is either float or double -> We'll have to know for later!
	bool bVelocityIsDouble = vtkDoubleArray::SafeDownCast(pVelocity) != NULL;
	void *pRawVel = pVelocity->GetVoidPointer(0);

	//setup array for lambda2
	vtkDataArray *pLambda2 = vtkDoubleArray::New();
	pLambda2->SetNumberOfComponents(1);
	pLambda2->SetNumberOfTuples(pInput->GetNumberOfCells());
	pLambda2->SetName(m_strOutputFieldName.c_str());

	double *fLambda2 = (double*)pLambda2->GetVoidPointer(0);

	//thread safety issues -> look at vtkCell's vtk documentation 
	vtkGenericCell *pTmpCell = vtkGenericCell::New(); //one gen cell for each thread
	pInput->GetCell(0,pTmpCell);
	pTmpCell->Delete();
	
#pragma omp parallel shared(fLambda2, pRawVel) 
	{
		//use generic cell interface for thread safety
		vtkGenericCell *pCell = vtkGenericCell::New();
		vtkIdList *pPtIds;  //point ids of cell's nodes
		
		double fCenter[24]; //parametric cell center
		//maximum number of cell points assumed here is 24 as VTK does not support bigger cells anyway
		double fVals[24*3]; //velocity values at cell's nodes
		double fTmp[9];
		double fJacobi[3][3];
		double fJSquared[3][3];		
		//double fJTransposed[3][3];
		double fJTSquared[3][3];
		double fEigenVals[3];
		double **fCompoundTensor = new double*[3];  //resulting real symmetric tensor for l2 computation
		double **fEigenVecs		= new double*[3]; //"matrix" of eigenvectors
		int iSubID = 0;
		int j=0;
		int i=0;
		int iValOfs = 0;
		int iVecOfs = 0;

		for(i=0; i<3; ++i)
		{
			fEigenVecs[i] = new double[3];
			fCompoundTensor[i] = new double[3];
		}

#pragma omp for
		for(i=0; i<iNumInCells; ++i)
		{
			//1) Compute velocity gradient at cell center 
			pInput->GetCell(i,pCell);
			pPtIds = pCell->GetPointIds();
			for(j=0; j<pPtIds->GetNumberOfIds(); ++j)
			{
				iValOfs = iVectorDim*j;
				iVecOfs = iVectorDim*pPtIds->GetId(j);
				//NOTE: Do not use vtkDataArray::GetTuple3(i)[j] here since it is NOT THREAD SAFE!
				if(bVelocityIsDouble)
					//double values can be copied right away!
					memcpy(&(fVals[iValOfs]),&(((double*)pRawVel)[iVecOfs]), iVectorDim*sizeof(double));
				else
				{
					//float values need to be casted and assigned
					fVals[iValOfs]	 = ((float*)pRawVel)[iVecOfs];
					fVals[iValOfs+1] = ((float*)pRawVel)[iVecOfs+1];
					fVals[iValOfs+2] = ((float*)pRawVel)[iVecOfs+2];
				}	
			}
			iSubID = pCell->GetParametricCenter(fCenter);
			pCell->Derivatives(iSubID, fCenter, fVals, 3, fTmp);
			memcpy(&(fJacobi[0][0]),fTmp,9*sizeof(double));
			/*
			fJacobi[0][0] = fTmp[0]; fJacobi[0][1] = fTmp[1]; fJacobi[0][2] = fTmp[2];
			fJacobi[1][0] = fTmp[3]; fJacobi[1][1] = fTmp[4]; fJacobi[1][2] = fTmp[5];
			fJacobi[2][0] = fTmp[6]; fJacobi[2][1] = fTmp[7]; fJacobi[2][2] = fTmp[8];
			*/
			//2) compute compound tensor for lambda2 calculation
			vtkMath::Multiply3x3(fJacobi,fJacobi,fJSquared);
			vtkMath::Transpose3x3(fJSquared,fJTSquared);
			//vtkMath::Multiply3x3(fJTransposed,fJTransposed,fJTSquared);
			// *** unrolled loop *** remember: compound tensor is symmetric! 
			fCompoundTensor[0][0] = 0.5f * (fJSquared[0][0] + fJTSquared[0][0]);
			fCompoundTensor[1][1] = 0.5f * (fJSquared[1][1] + fJTSquared[1][1]);
			fCompoundTensor[2][2] = 0.5f * (fJSquared[2][2] + fJTSquared[2][2]);
			fCompoundTensor[0][1] = fCompoundTensor[1][0] = 0.5f * (fJSquared[0][1] + fJTSquared[0][1]);
			fCompoundTensor[0][2] = fCompoundTensor[2][0] = 0.5f * (fJSquared[0][2] + fJTSquared[0][2]);
			fCompoundTensor[1][2] = fCompoundTensor[2][1] = 0.5f * (fJSquared[1][2] + fJTSquared[1][2]);
			//3) compute eigenvals : compound tensor is real symmetric -> 3 real eigenvalues
			vtkMath::Jacobi(fCompoundTensor, fEigenVals, fEigenVecs);
			//eigenvals are already sorted in decreasing order -> just take the second one
			fLambda2[i] = fEigenVals[1];
		}
		//CLEANUP
		for(int i=0; i<3; ++i)
		{
			delete[] fCompoundTensor[i];
			delete[] fEigenVecs[i];
		}
		delete[] fCompoundTensor;
		delete[] fEigenVecs;
		pCell->Delete();
	} //END OPENMP PARALLEL SECTION
	
	//if cell data computation is sufficient -> skip translation to point data
	if(!m_bComputePointData)
	{
		pOutput->GetCellData()->AddArray(pLambda2);
		pOutput->GetCellData()->SetActiveScalars(m_strOutputFieldName.c_str());
		pOutput->Squeeze();
		pLambda2->Delete();
		return 1;
	}

	//translation to point data -> create tmp data set and put it through vtkCellDataToPointData filter
	//NOTE: Don't use output here directly since this will result in an endless recursion!
	vtkDataSet *pTmpDataSet = pInput->NewInstance();
	pTmpDataSet->ShallowCopy(pInput);
	pTmpDataSet->GetCellData()->AddArray(pLambda2);
	pLambda2->Delete();
	//compute point data attributes by interpolation of cell data
	vtkCellDataToPointData *pC2P = vtkCellDataToPointData::New();

#if VTK_MAJOR_VERSION > 5
	pC2P->SetInputData(pTmpDataSet);
#else
	pC2P->SetInput(pTmpDataSet);
#endif

	pC2P->Update();
	pLambda2 = pC2P->GetOutput()->GetPointData()->GetArray(m_strOutputFieldName.c_str());
	if(!pLambda2)
	{
		vstr::errp() << "[vtkComputeLambda2::RequestData] unable to retrieve point data!" << endl;
		pC2P->Delete();
		return -1;
	}
	//move lambda2 array from cell to point data
	pOutput->GetPointData()->AddArray(pLambda2);
	pOutput->GetCellData()->RemoveArray(m_strOutputFieldName.c_str());
	pOutput->Squeeze();
	
	//finally clean up!
	pC2P->Delete();
	pTmpDataSet->Delete();
	
	return 1;
}

