/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VtkRescaleData.h"

#include <vtkDataArray.h>
#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>

#include <VistaBase/VistaStreamUtils.h>

#ifdef _OPENMP
#include <omp.h>
#endif
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

vtkCxxRevisionMacro(vtkRescaleData, "$Id$");
vtkStandardNewMacro(vtkRescaleData);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
vtkRescaleData::vtkRescaleData()
{
}
//------------------------------------------------------------
//------------------------------------------------------------
vtkRescaleData::~vtkRescaleData()
{
}
/*============================================================================*/
/*  IMPLEMENTATION															  */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
void vtkRescaleData::PrintSelf(ostream& os, vtkIndent indent)
{
	vtkDataSetAlgorithm::PrintSelf(os, indent);
}

//------------------------------------------------------------
//------------------------------------------------------------
void vtkRescaleData::AddFieldToRescale(	const std::string& strFieldName, 
										const float fMin, const float fMax)
{
	m_vecRescales.push_back(__SRescaleData(strFieldName, fMin, fMax));
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkRescaleData::RemoveRescale(const std::string& strFieldName)
{
	vector<__SRescaleData>::iterator itCurrent = m_vecRescales.begin();
	for(;itCurrent != m_vecRescales.end(); ++itCurrent)
		if(itCurrent->strFieldName == strFieldName)
		{
			m_vecRescales.erase(itCurrent);
			return;
		}

}
//------------------------------------------------------------
//------------------------------------------------------------
int vtkRescaleData::RequestData(vtkInformation *, 
								vtkInformationVector **inputVector, 
								vtkInformationVector *outputVector)
{
	// get the info objects
	//our input is the first input on the first port of this filter
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkDataSet *pInput = vtkDataSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkDataSet *pOutput = vtkDataSet::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pOutput)
	{
		vstr::errp() << "[VtkRescaleData::RequestData] no output!" << endl;
		return -1;
	}
	if(!pInput)
	{
		vstr::errp() << "[VtkRescaleData::RequestData] no input!" << endl;
		return -1;
	}
	// Initialize
	const int iNumInCells = pInput->GetNumberOfCells();
	
	// First, copy the input to the output as a starting point
	pOutput->CopyStructure(pInput);	
	
	vtkPointData *pInPD = pInput->GetPointData();
	vtkPointData *pOutPD = pOutput->GetPointData();

	vtkDataArray *pCurrentArray = NULL;
	vtkDataArray *pNewArray = NULL;
	double dCurrentVal;
	double dCurrentRange[9][2];
	double d1DivCurrentRange[9], dTargetRange;
	
	for(size_t i=0; i<m_vecRescales.size(); ++i)
	{
		pCurrentArray = pInPD->GetArray(m_vecRescales[i].strFieldName.c_str());
		if(!pCurrentArray)
			continue;
				
		//allocate new space 
		pNewArray = pCurrentArray->NewInstance();
		pNewArray->SetNumberOfComponents(pCurrentArray->GetNumberOfComponents());
		pNewArray->SetNumberOfTuples(pCurrentArray->GetNumberOfTuples());
		pNewArray->SetName(pCurrentArray->GetName());
		//rescale the data
		dTargetRange = m_vecRescales[i].fMax - m_vecRescales[i].fMin;
		
		if(pCurrentArray->GetNumberOfComponents() > 9)
		{
			vstr::warnp() << "[VtkRescaleData::RequestData]: unable to rescale more than 9 dim data!" << endl;
			continue;
		}
		
		for(int j=0; j<pCurrentArray->GetNumberOfComponents(); ++j)
		{
			pCurrentArray->GetRange(dCurrentRange[j], j);
			d1DivCurrentRange[j] = 1.0 / (dCurrentRange[j][1] - dCurrentRange[j][0]); 
		}

		for(int k=0; k<pCurrentArray->GetNumberOfTuples(); ++k)
		{
			for(int j=0; j<pCurrentArray->GetNumberOfComponents(); ++j)
			{
				dCurrentVal = pCurrentArray->GetComponent(j,k);
				dCurrentVal -= dCurrentRange[j][0];
				dCurrentVal *= d1DivCurrentRange[j];
				dCurrentVal *= dTargetRange;
				dCurrentVal += m_vecRescales[i].fMin;
				pNewArray->SetComponent(j,k,dCurrentVal);
			}
		}
		//attach data to output
		pOutPD->AddArray(pNewArray);
		pNewArray->Delete();
	}
	return 1;
}

