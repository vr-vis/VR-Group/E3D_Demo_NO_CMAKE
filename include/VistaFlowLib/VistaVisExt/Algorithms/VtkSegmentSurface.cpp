/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	vtkSegmentSurface.cpp RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VtkSegmentSurface.h"

#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkCell.h>
#include <vtkGenericCell.h>
#include <vtkIdList.h>
#include <vtkDataArray.h>

#include <vtkIntArray.h>
#include <vtkFloatArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>

#include <cassert>
#include <queue>
#include <vector>
#include <list>

#include <VistaBase/VistaStreamUtils.h>

/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
vtkCxxRevisionMacro(vtkSegmentSurface, "$Revision: 1.4 $");
vtkStandardNewMacro(vtkSegmentSurface);

/**
 * Helper class for a connected region in the mesh
 */
class vtkSegmentSurface::Region
{
public: 
	Region(vtkIdType iRID) 
		:	m_iRID(iRID)
	{}

	~Region()
	{}

	vtkIdType GetRegionId() const
	{
		return m_iRID;
	}

	void AddPointId(vtkIdType iPt)
	{
		m_vecPts.push_back(iPt);
	}

	vtkIdType GetNumberOfPoints() const
	{
		return (vtkIdType)m_vecPts.size();
	}

	vtkIdType GetPointId(vtkIdType i) const
	{
		return m_vecPts[i];
	}


private:
	vtkIdType m_iRID;
	std::vector<vtkIdType> m_vecPts;
};

class vtkSegmentSurface::Edge
{
public:
	Edge(vtkIdType iRID1, vtkIdType iRID2, double dCrossVal)
		:	m_iRID1(std::min(iRID1, iRID2)),
			m_iRID2(std::max(iRID1, iRID2)),
			m_dCrossingVal(dCrossVal){}
	
	~Edge(){}

	vtkIdType GetRegion1() const 
	{
		return m_iRID1;
	}
	vtkIdType GetRegion2() const 
	{
		return m_iRID2;
	}

	void SetCrossingVal(double dNewVal)
	{
		m_dCrossingVal = dNewVal;
	}

	double GetCrossingVal() const
	{
		return m_dCrossingVal;
	}

	bool operator<(const Edge &rOther) const
	{
		if(this->m_iRID1 < rOther.m_iRID1)
			return true;
		if(this->m_iRID1 == rOther.m_iRID1)
			return this->m_iRID2 < rOther.m_iRID2;
		return false;
	}

private:
	vtkIdType m_iRID1;
	vtkIdType m_iRID2;
	double m_dCrossingVal;
};

class vtkSegmentSurface::RegionGraph
{
public:
	RegionGraph(const vtkIdType iNumRegions)
	{
		m_vecRegions.resize(iNumRegions);
		for(int r=0; r<iNumRegions; ++r)
			m_vecRegions[r] = new Region(r);
	}
	
	~RegionGraph()
	{
		const size_t iNumRegions = m_vecRegions.size();
		for(size_t r=0; r<iNumRegions; ++r)
			delete m_vecRegions[r];
	}

	vtkSegmentSurface::Region *GetRegion(vtkIdType iRID)
	{
		return m_vecRegions[iRID];
	}

	/**
	 * Insert new edge between region1 and region2, or replace 
	 * existing one's crossing val.
	 * returns true iff a new connection between two regions has been created
	 */
	bool InsertEdge(vtkIdType iRID1, vtkIdType iRID2, double dCrossingVal)
	{
		Edge oNewEdge(iRID1, iRID2, dCrossingVal);
		std::set<Edge>::iterator itEdge = m_setEdges.find(oNewEdge);
		if(itEdge == m_setEdges.end())
		{
			m_setEdges.insert(oNewEdge);
			return true;
		}
		else
		{
			//we got the edge already ==> just replace the crossing val if needed
			if(dCrossingVal < itEdge->GetCrossingVal())
			{
				m_setEdges.erase(itEdge);
				m_setEdges.insert(oNewEdge);
			}
			return false;
		}
	}
	
	void GetIncidentEdges(vtkIdType iRID, std::list<vtkSegmentSurface::Edge> &liEdges)
	{
		std::set<Edge>::iterator itEdge = m_setEdges.begin();
		std::set<Edge>::iterator itEnd = m_setEdges.end();

		for(;itEdge!=itEnd; ++itEdge)
		{
			if(itEdge->GetRegion1() == iRID || itEdge->GetRegion2() == iRID)
				liEdges.push_back(*itEdge);
		}
	}

private:
	std::vector<vtkSegmentSurface::Region*> m_vecRegions;
	std::set<vtkSegmentSurface::Edge> m_setEdges;
};


void __PrintConnectivityHistogram(vtkPolyData *pData)
{
	std::vector<size_t> vecConnectivity(20);
	vecConnectivity.assign(20, 0);
	const vtkIdType iNumPts = pData->GetNumberOfPoints();
	vtkIdList *pCells = vtkIdList::New();
	for(vtkIdType pt=0; pt<iNumPts; ++pt)
	{
		pData->GetPointCells(pt,pCells);
		const vtkIdType iNumCells = pCells->GetNumberOfIds();
		if((size_t)iNumCells > vecConnectivity.size())
		{
			size_t iCurrentSize = vecConnectivity.size();
			size_t iTargetSize = iNumCells;
			vecConnectivity.resize(iTargetSize);
			for(size_t i=iCurrentSize; iCurrentSize<iTargetSize; ++i)
				vecConnectivity[i] = 0;
		}
		++(vecConnectivity[iNumCells]);
	}

	vstr::outi()<<"Connectivity histogram:"<<endl;
	for(size_t i=0; i<vecConnectivity.size(); ++i)
		vstr::outi()<<i<<"\t-->\t"<< vecConnectivity[i]<<endl;
}


void vtkSegmentSurface::PrintSelf( ostream& os, vtkIndent indent )
{
	os << "vtkSegmentSurface -- nothing to report!" << endl;
}


vtkSegmentSurface::vtkSegmentSurface()
	:	m_strScalars(""),
		m_bIncludeMinimaInOutput(true),
		m_dRegionsMergeThreshold(0.0),
		m_p1RingNeighCells(vtkIdList::New()),
		m_p1RingNeighPts(vtkIdList::New()),
		m_p1RingNeighborhood(vtkIdList::New())
{ }

vtkSegmentSurface::~vtkSegmentSurface()
{
	m_p1RingNeighborhood->Delete();
	m_p1RingNeighPts->Delete();
	m_p1RingNeighCells->Delete();
}

void vtkSegmentSurface::SetScalarsToSegment( const char *pScalars )
{
	m_strScalars = pScalars;
}

const char* vtkSegmentSurface::GetScalarsToSegment() const
{
	return m_strScalars.c_str();
}

void vtkSegmentSurface::SetIncludeMinimaInOutput( bool bInclude )
{
	m_bIncludeMinimaInOutput = bInclude;
}

bool vtkSegmentSurface::GetIncludeMinimaInOutput() const
{
	return m_bIncludeMinimaInOutput;
}


double vtkSegmentSurface::GetRegionsMergeThreshold() const
{
	return m_dRegionsMergeThreshold;
}

void vtkSegmentSurface::SetRegionsMergeThreshold( const double dTreshold )
{
	m_dRegionsMergeThreshold = dTreshold;
}

int vtkSegmentSurface::RequestData( vtkInformation* request, 
									vtkInformationVector** inputVector, 
									vtkInformationVector* outputVector )
{
	// retrieve data objects from information vectors
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkPolyData *pInput = vtkPolyData::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkPolyData *pOutput = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

	if(pInput == NULL || pOutput == NULL)
	{
		vstr::errp() <<"[vtkSegmentSurface::RequestData] Invalid in- or output!"<<endl;
		return 0;
	}
	if(!pInput->GetPointData()->HasArray(m_strScalars.c_str()))
	{
		vstr::errp() <<"[vtkSegmentSurface::RequestData] Unable to find segmentation scalar field <"<<m_strScalars.c_str()<<">!"<<endl;
		return 0;
	}
		
	int iret = pInput->GetPointData()->SetActiveScalars(m_strScalars.c_str());
	vtkDataArray *pScalars = pInput->GetPointData()->GetScalars();

	const vtkIdType iNumPts = pInput->GetNumberOfPoints();

	//the output will structurally stay the same
	pOutput->ShallowCopy(pInput);
	
	vtkIdList *pMinima = vtkIdList::New();
	this->FindLocalMinima(pInput, pScalars, pMinima);

	vtkDataArray *pLabels = this->CreatePointLabels(pOutput);
	this->InitPointLabels(iNumPts, pMinima, pLabels);
	pOutput->GetPointData()->AddArray(pLabels);

	//label points
	vtkIdList *pTrail = vtkIdList::New();
	for(vtkIdType pt=0; pt<iNumPts; ++pt)
	{
		//if we have seen this point already ==> skip it
		if(pLabels->GetTuple1(pt) != NOT_YET_VISITED)
			continue;
		
		//descent from point to a minimum or another labeled point
		pTrail->Reset();
		vtkIdType iRegionLabel = UNKNOWN_REGION;
		iRegionLabel = this->DescendFromPoint(pt, pInput, pScalars, pLabels, pTrail);

		assert(iRegionLabel != UNKNOWN_REGION);
		assert(pTrail->GetNumberOfIds() >= 1);

		//we have reached a minimum or labeled pt ==> label entire trail that led there
		const vtkIdType iTrailLen = pTrail->GetNumberOfIds();
		for(vtkIdType iTrailPt=0; iTrailPt<iTrailLen; ++iTrailPt)
		{
			pLabels->SetTuple1(pTrail->GetId(iTrailPt), iRegionLabel);
		}
	}
	pTrail->Delete();

	//simplify output by merging regions
	if(m_dRegionsMergeThreshold > 0.0)
	{
		this->MergeRegions(pOutput, pScalars, pLabels, pMinima);
	}

	//create output for minima
	if(m_bIncludeMinimaInOutput)
	{
		vtkCellArray *pMinVertices = vtkCellArray::New();
		const vtkIdType iNumMinima = pMinima->GetNumberOfIds();
		pMinVertices->Allocate(2*iNumMinima);
		for(vtkIdType m=0; m<iNumMinima; ++m)
		{
			vtkIdType iCurrentMin = pMinima->GetId(m);
			pMinVertices->InsertNextCell(1, &iCurrentMin);
		}
		pOutput->SetVerts(pMinVertices);
		pMinVertices->Delete();
	}

	pMinima->Delete();

	return 1;
}

void vtkSegmentSurface::FindLocalMinima( vtkPolyData *pInput, vtkDataArray *pScalars, vtkIdList *pMinima ) const
{
	const size_t iNumPts = pInput->GetNumberOfPoints();

	for(size_t p=0; p<iNumPts; ++p)
	{
		if(IsLocalMinimum(p, pInput, pScalars))
			pMinima->InsertNextId(p);
	}
}

bool vtkSegmentSurface::IsLocalMinimum(size_t iPt, vtkPolyData *pInput, vtkDataArray *pScalars ) const
{
	assert(pScalars->GetNumberOfComponents() == 1);

	double dLocalVal = 0.0;
	pScalars->GetTuple(iPt, &dLocalVal);

	this->Compute1Ring(iPt, pInput, m_p1RingNeighborhood);

	const vtkIdType iNumNeighs = m_p1RingNeighborhood->GetNumberOfIds();
	//if we are not connected to anybody else, we are a minimum
	if(iNumNeighs == 0)
		return true;

	for(vtkIdType neigh=0; neigh<iNumNeighs; ++neigh)
	{
		//if there is any one neighbor with a scalar value lower than us ==> we are not in a minimum
		double dNeighborVal = pScalars->GetTuple1(m_p1RingNeighborhood->GetId(neigh));
		if(dNeighborVal < dLocalVal)
			return false;
	}
	
	//still here ? ==> all neighbors have scalar val <= our val.
	return true;
}

vtkDataArray * vtkSegmentSurface::CreatePointLabels( vtkPolyData *pData )
{
	const vtkIdType iNumPts = pData->GetNumberOfPoints();
	vtkIntArray *pLabels = vtkIntArray::New();
	pLabels->SetNumberOfComponents(1);
	pLabels->SetName("segment_label");
	pLabels->SetNumberOfTuples(iNumPts);
	return pLabels;
}

void vtkSegmentSurface::InitPointLabels(const vtkIdType iNumPts, vtkIdList *pMinima, vtkDataArray *pLabels)
{
	int iCurrentMinIdx = 0;
	for(vtkIdType pt=0; pt<iNumPts; ++pt)
	{
		if(pt == pMinima->GetId(iCurrentMinIdx))
		{
			//init a minimum with new region id
			pLabels->SetTuple1(pt, iCurrentMinIdx);
			++iCurrentMinIdx;
		}
		else
		{
			pLabels->SetTuple1(pt, NOT_YET_VISITED);
		}
	}
}



vtkIdType vtkSegmentSurface::DescendFromPoint( vtkIdType iStartPt, 
											   vtkPolyData *pData, 
											   vtkDataArray *pScalars, 
											   vtkDataArray *pLabels,
											   vtkIdList *pTrail )
{
	//perform steepest descent search from current pt
	vtkIdType iCurrentPt = iStartPt;
	double dCurrentVal = pScalars->GetTuple1(iStartPt);

	vtkIdType iCurrentLabel = UNKNOWN_REGION;
	
	while(iCurrentLabel == UNKNOWN_REGION)
	{
		//always remember current point
		pTrail->InsertNextId(iCurrentPt);

		//compute minimum neighbor in 1-ring neighborhood
		vtkIdType iMinNeighIdx = iCurrentPt;
		double dMinNeighVal = dCurrentVal;
		
		this->Compute1Ring(iCurrentPt, pData, m_p1RingNeighborhood);
		
		const vtkIdType iNumNeighs = m_p1RingNeighborhood->GetNumberOfIds();
		for(vtkIdType neigh=0; neigh<iNumNeighs; ++neigh)
		{
			vtkIdType iNeighbor = m_p1RingNeighborhood->GetId(neigh);
			double dNeighVal = pScalars->GetTuple1(iNeighbor);
			if(dNeighVal < dMinNeighVal)
			{
				dMinNeighVal = dNeighVal;
				iMinNeighIdx = iNeighbor;
			}
		}
		//check if lowest neighbor is labeled already
		int iMinNeighLabel = (int)pLabels->GetTuple1(iMinNeighIdx);
		if(iMinNeighLabel != NOT_YET_VISITED)
		{
			iCurrentLabel = pLabels->GetTuple1(iMinNeighIdx);
		}
		else
		{
			//we have not yet hit a labeled pt
			iCurrentPt = iMinNeighIdx;
			dCurrentVal = pScalars->GetTuple1(iCurrentPt);
		}		
	}

	return iCurrentLabel;
}

#if 0 //--- OLD CODE //
void vtkSegmentSurface::MergeRegions(vtkPolyData *pData, 
									 vtkDataArray *pScalars, 
									 vtkDataArray *pLabels, 
									 vtkIdList *pMinima)
{
	//at the beginning there is one region per minimum
	const size_t iNumRegions = (size_t)pMinima->GetNumberOfIds();
	RegionGraph oGraph(iNumRegions);
	
	this->ComputeRegionGraph(pData, pLabels, pScalars, oGraph);
	
	bool *bRegionVisited = new bool[iNumRegions];
	memset(bRegionVisited, 0, iNumRegions*sizeof(bool));

	vtkIdType iNextRegionID = 0;
	for(size_t iCurrentRegion=0; iCurrentRegion<iNumRegions; ++iCurrentRegion)
	{
		if(bRegionVisited[iCurrentRegion])
			continue;
		//
		printf("Starting new region [%d]\n", iNextRegionID);

		//start new connected component at region r
		std::queue<Region*> quRegionsToVisit;
		quRegionsToVisit.push(oGraph.GetRegion(iCurrentRegion));
		//try to expand
		int iNumPushes=0;
		int iMaxQueueLength = 0;
		while(!quRegionsToVisit.empty())
		{
			Region *pSource = quRegionsToVisit.front();
			quRegionsToVisit.pop();

			//re-label pts
			const size_t iNumPts = pSource->GetNumberOfPoints();
			for(size_t p=0; p<iNumPts; ++p)
				pLabels->SetTuple1(pSource->GetPointId(p), iNextRegionID);
			
			bRegionVisited[pSource->GetRegionId()] = true;

			//check neighbors 
			std::list<vtkSegmentSurface::Edge> liEdges;
			oGraph.GetIncidentEdges(pSource->GetRegionId(), liEdges);

			std::list<Edge>::iterator itEdge = liEdges.begin();
			std::list<Edge>::iterator itEnd = liEdges.end();
			for(; itEdge != itEnd; ++itEdge)
			{
				Edge &rCurrentEdge = *itEdge;
				size_t iRID1 = rCurrentEdge.GetRegion1();
				size_t iRID2 = rCurrentEdge.GetRegion2();
				size_t iNeighRID = (iRID1 == iCurrentRegion ? iRID2 : iRID1);
				
				if(bRegionVisited[iNeighRID])
					continue;

				double dCrossingVal = rCurrentEdge.GetCrossingVal();
				//compute delta that we would have to climb from the minimum 
				//in order to reach smallest crossing point
				double dR1Delta = dCrossingVal - pScalars->GetTuple1(pMinima->GetId(iRID1));
				double dR2Delta = dCrossingVal - pScalars->GetTuple1(pMinima->GetId(iRID2));

				//include this neighbor iff we meet the merge criterion for this edge
				//if(dR1Delta < m_dRegionsMergeThreshold && 
				//   dR2Delta < m_dRegionsMergeThreshold)
				{
					quRegionsToVisit.push(oGraph.GetRegion(iNeighRID));
					iMaxQueueLength = std::max<size_t>(quRegionsToVisit.size(), iMaxQueueLength);
					++iNumPushes;
				}
			}//-- for all edges
		}//-- breadth first region search

		printf("\tfinished breadth first pass (maxlen=%d, #pushes=%d)\n", iMaxQueueLength, iNumPushes);
		size_t iUnlabeled = 0;
		for(size_t k=0; k<iNumRegions; ++k)
			if(!(bRegionVisited[k]))
				++iUnlabeled;
		printf("\tremaining unvisited areas = %d\n\n", iUnlabeled);
		//we have just created a region ==> inc id
		++iNextRegionID;
	}
	delete[] bRegionVisited;
}
#endif


void vtkSegmentSurface::MergeRegions(vtkPolyData *pData, 
									 vtkDataArray *pScalars, 
									 vtkDataArray *pLabels, 
									 vtkIdList *pMinima)
{
	
	const vtkIdType iNumPts = pData->GetNumberOfPoints();
	bool *bHaveVisitedPt = new bool[iNumPts];
	memset(bHaveVisitedPt, 0, iNumPts*sizeof(bool));

	vtkIdType iNextRegionId = 0;

	for(vtkIdType iSeedPt=0; iSeedPt<iNumPts; ++iSeedPt)
	{
		if(bHaveVisitedPt[iSeedPt])
			continue;

		//NOTE: For every pt that already got assigned a new label, bHaveVisited[iNeighPt] is true
		//      so here we see only original region labels that have not yet been changed and thus
		//		are valid indices into the minima idList
		vtkIdType iOriginalRegionLabel = pLabels->GetTuple1(iSeedPt);
		
		//Start breadth-first search in order to expand the current region as far
		//as possible. Include all pts that 
		//	-	carry the same original region label as at least one pt 
		//		that has already been visited in this search
		//	-	that are connected to an already visited pt via a "mergable" edge
		std::queue< std::pair<vtkIdType, vtkIdType> > quPtsToVisit;
		quPtsToVisit.push(std::make_pair(iSeedPt,iOriginalRegionLabel));
			
		while(!quPtsToVisit.empty())
		{
			std::pair<vtkIdType, vtkIdType> paNext = quPtsToVisit.front();
			quPtsToVisit.pop();

			vtkIdType iCurrentPt = paNext.first;
			//skip if it has been visited meanwhile
			if(bHaveVisitedPt[iCurrentPt])
				continue;

			vtkIdType iCurrentLabel = paNext.second;
			double dCurrentVal = pScalars->GetTuple1(iCurrentPt);
			vtkIdType iCurrentMinPt = pMinima->GetId(iCurrentLabel);
			double dCurrentMinVal = pScalars->GetTuple1(iCurrentMinPt);
			
			//re-label current point with current (new) region
			pLabels->SetTuple1(iCurrentPt, iNextRegionId);
			bHaveVisitedPt[iCurrentPt] = true;

			//scan neighbors
			this->Compute1Ring(iCurrentPt, pData, m_p1RingNeighborhood);
			vtkIdType iNumNeighs = m_p1RingNeighborhood->GetNumberOfIds();
			for(vtkIdType neigh=0; neigh<iNumNeighs; ++neigh)
			{
				vtkIdType iNeighPt = m_p1RingNeighborhood->GetId(neigh);
				
				if(bHaveVisitedPt[iNeighPt])
					continue;
				
				vtkIdType iNeighLabel = pLabels->GetTuple1(iNeighPt);
				if(iNeighLabel == iCurrentLabel)
				{
					//if it carries the same label as the original point ==> directly take it in
					quPtsToVisit.push(std::make_pair(iNeighPt, iCurrentLabel));
				}
				else
				{
					//if the original region label is different ==> check if edge is mergeable
					//i.e. if it's shallow enough
					//determine crossing value as the higher of the two scalar value along the edge
					double dCrossVal = std::max<double>(dCurrentVal, pScalars->GetTuple1(iNeighPt));
					
					double dDelta1 = dCrossVal - dCurrentMinVal;
					
					vtkIdType iNeighMinPt = pMinima->GetId(iNeighLabel);
					double dDelta2 = dCrossVal - pScalars->GetTuple1(iNeighMinPt);
					
					if(dDelta1 < m_dRegionsMergeThreshold || dDelta2 < m_dRegionsMergeThreshold)
						quPtsToVisit.push(std::make_pair(iNeighPt, iNeighLabel));
				}
			}//-- end for all neighbors
		}//-- end while queue not empty
		++iNextRegionId;
	}//-- end for all pts

	delete[] bHaveVisitedPt;
}

void vtkSegmentSurface::ComputeRegionGraph( vtkPolyData *pData, 
											vtkDataArray *pLabels, 
											vtkDataArray *pScalars,
											vtkSegmentSurface::RegionGraph &rRegionGraph)
{
	
	const vtkIdType iNumPts = pData->GetNumberOfPoints();
	for(vtkIdType pt=0; pt<iNumPts; ++pt)
	{
		//insert point into its region
		int iRID = (int)pLabels->GetTuple1(pt);
		assert(iRID >= FIRST_MIN);
		rRegionGraph.GetRegion(iRID)->AddPointId(pt);

		//check if we have a border vertex
		this->ComputeEdgesAtPoint(pt, pData, pLabels, pScalars, rRegionGraph);
	}
}

void vtkSegmentSurface::ComputeEdgesAtPoint(vtkIdType iPt, 
											vtkPolyData *pData, 
											vtkDataArray *pLabels, 
											vtkDataArray *pScalars,
											RegionGraph &rRegionGraph)
{
	//check 1-ring neighborhood for differing labels
	int iPtLabel = (int)pLabels->GetTuple1(iPt);

	this->Compute1Ring(iPt, pData, m_p1RingNeighborhood);

	const vtkIdType iNumNeighs = m_p1RingNeighborhood->GetNumberOfIds();
	for(vtkIdType neigh=0; neigh<iNumNeighs; ++neigh)
	{
		vtkIdType iNeighbor = m_p1RingNeighborhood->GetId(neigh);
		int iNeighLabel = (int)pLabels->GetTuple1(iNeighbor);
		if(iNeighLabel != iPtLabel)
		{
			//crossing this edge means "climbing" across the max scalar value of either of the two pts
			double dCrossVal = std::max<double>(pScalars->GetTuple1(iPt), pScalars->GetTuple1(iNeighbor));
			rRegionGraph.InsertEdge(iPtLabel, iNeighLabel, dCrossVal);
		}
	}
}

void vtkSegmentSurface::Compute1Ring( vtkIdType iPt, vtkPolyData *pData, vtkIdList *pNeighbors ) const
{
	pNeighbors->Reset();

	pData->GetPointCells(iPt, m_p1RingNeighCells);
	const vtkIdType iNumCells = m_p1RingNeighCells->GetNumberOfIds();
	
	for(vtkIdType c=0; c<iNumCells; ++c)
	{
		vtkIdType iCurrentCell = m_p1RingNeighCells->GetId(c);
		pData->GetCellPoints(iCurrentCell,m_p1RingNeighPts);
		const vtkIdType iNumNeighPts = m_p1RingNeighPts->GetNumberOfIds();

		for(vtkIdType neigh=0; neigh<iNumNeighPts; ++neigh)
		{
			vtkIdType iNeighbor = m_p1RingNeighPts->GetId(neigh);
			if(iNeighbor != iPt)
				pNeighbors->InsertUniqueId(iNeighbor);
		}
	}
}

/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
