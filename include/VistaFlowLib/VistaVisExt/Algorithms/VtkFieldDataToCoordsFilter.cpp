#include "VtkFieldDataToCoordsFilter.h"

#include <vtkDataSet.h>
#include <vtkPolyData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkPoints.h>
#include <vtkObjectFactory.h>
#include <vtkCellArray.h>

#include <VistaBase/VistaStreamUtils.h>

vtkCxxRevisionMacro(vtkFieldDataToCoordsFilter, "$$");
vtkStandardNewMacro(vtkFieldDataToCoordsFilter);


vtkFieldDataToCoordsFilter::vtkFieldDataToCoordsFilter(void) 
: m_strXFieldName(""), m_strYFieldName(""), m_strZFieldName(""), m_bGenerateVertices(false)
{
	for(int i=0; i<3; ++i)
	{
		m_iFieldComponent[i] = 0;
		m_dDefaultVals[i] = 0;
		m_dTargetBounds[2*i] = 0;
		m_dTargetBounds[2*i+1] = 1;
	}
}

vtkFieldDataToCoordsFilter::~vtkFieldDataToCoordsFilter(void)
{
}



	
void vtkFieldDataToCoordsFilter::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
	os << indent << "X axis field         : " << m_strXFieldName;
	os << indent << "X axis component     : " << m_iFieldComponent[0];
	os << indent << "X axis default value : " << m_dDefaultVals[0];
	os << indent << "Y axis field         : " << m_strXFieldName;
	os << indent << "Y axis component     : " << m_iFieldComponent[1];
	os << indent << "Y axis default value : " << m_dDefaultVals[1];
	os << indent << "Z axis field         : " << m_strXFieldName;
	os << indent << "Z axis component     : " << m_iFieldComponent[2];
	os << indent << "Z axis default value : " << m_dDefaultVals[2];
	
}

void vtkFieldDataToCoordsFilter::SetXField(const std::string& strFName, int iComp/*=0*/)
{
	m_strXFieldName = strFName;
	m_iFieldComponent[0] = iComp;
	this->Modified();
}


std::string vtkFieldDataToCoordsFilter::GetXFieldName() const
{
	return m_strXFieldName;
}


int vtkFieldDataToCoordsFilter::GetXFieldDim() const
{
	return m_iFieldComponent[0];
}


void vtkFieldDataToCoordsFilter::SetYField(const std::string& strFName, int iComp/*=0*/)
{
	m_strYFieldName = strFName;
	m_iFieldComponent[1] = iComp;
	this->Modified();
}


std::string vtkFieldDataToCoordsFilter::GetYFieldName() const
{
	return m_strYFieldName;
}


int vtkFieldDataToCoordsFilter::GetYFieldDim() const
{
	return m_iFieldComponent[1];
}


void vtkFieldDataToCoordsFilter::SetZField(const std::string& strFName, int iComp/*=0*/)
{
	m_strZFieldName = strFName;
	m_iFieldComponent[2] = iComp;
	this->Modified();
}


std::string vtkFieldDataToCoordsFilter::GetZFieldName() const
{
	return m_strZFieldName;
}


int vtkFieldDataToCoordsFilter::GetZFieldDim() const
{
	return m_iFieldComponent[2];
}


void vtkFieldDataToCoordsFilter::SetDefaultVals(double d[3])
{
	m_dDefaultVals[0]=d[0];
	m_dDefaultVals[1]=d[1];
	m_dDefaultVals[2]=d[2];
	this->Modified();
}


void vtkFieldDataToCoordsFilter::GetDefaultVals(double d[3])
{
	d[0] = m_dDefaultVals[0];
	d[1] = m_dDefaultVals[1];
	d[2] = m_dDefaultVals[2];
}



void vtkFieldDataToCoordsFilter::SetXDefaultVal(double d)
{
	m_dDefaultVals[0] = d;
	this->Modified();
}

double vtkFieldDataToCoordsFilter::GetXDefaultVal() const
{
	return m_dDefaultVals[0];
}


void vtkFieldDataToCoordsFilter::SetYDefaultVal(double d)
{
	m_dDefaultVals[1] = d;
	this->Modified();
}


double vtkFieldDataToCoordsFilter::GetXYDefaultVal() const
{
	return m_dDefaultVals[1];
}


void vtkFieldDataToCoordsFilter::SetZDefaultVal(double d)
{
	m_dDefaultVals[2] = d;
	this->Modified();
}


double vtkFieldDataToCoordsFilter::GetZDefaultVal() const
{
	return m_dDefaultVals[2];
}


void vtkFieldDataToCoordsFilter::SetGenerateVertices(bool b)
{
	m_bGenerateVertices = b;
	this->Modified();
}


bool vtkFieldDataToCoordsFilter::GetGenerateVertices() const
{
	return m_bGenerateVertices;
}

void vtkFieldDataToCoordsFilter::SetTargetBounds(double d[6])
{
	m_dTargetBounds[0] = d[0];
	m_dTargetBounds[1] = d[1];
	m_dTargetBounds[2] = d[2];
	m_dTargetBounds[3] = d[3];
	m_dTargetBounds[4] = d[4];
	m_dTargetBounds[5] = d[5];
	this->Modified();
}

void vtkFieldDataToCoordsFilter::GetTargetBounds(double d[6]) const
{
	d[0] = m_dTargetBounds[0];
	d[1] = m_dTargetBounds[1];
	d[2] = m_dTargetBounds[2];
	d[3] = m_dTargetBounds[3];
	d[4] = m_dTargetBounds[4];
	d[5] = m_dTargetBounds[5];
}

int vtkFieldDataToCoordsFilter::RequestData(vtkInformation* request,
											vtkInformationVector** inputVector,
											vtkInformationVector* outputVector)
{
	// retrieve data objects from information vectors
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkDataSet *pInput = vtkDataSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkPolyData *pOutput = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pInput)
	{
		vstr::errp() << "[vtkFieldDataToCoordsFilter::RequestData] no input!" << endl;
		return 0;
	}
	if(!pOutput)
	{
		vstr::errp() << "[vtkFieldDataToCoordsFilter::RequestData] no output!" << endl;
		return 0;
	}
	const int iNumPts = pInput->GetNumberOfPoints();
	vtkPointData *pInPD = pInput->GetPointData();
	vtkPointData *pOutPD = pOutput->GetPointData();
	
	//check if data fields are there (and create dummies if necessary)
	vtkDataArray *pX = pInPD->GetArray(m_strXFieldName.c_str());
	vtkDataArray *pY = pInPD->GetArray(m_strYFieldName.c_str());
	vtkDataArray *pZ = pInPD->GetArray(m_strZFieldName.c_str());

	if(!pX)
	{
		if(m_strXFieldName.empty() && m_iFieldComponent[0] == 0)
		{
			pX = vtkDoubleArray::New();
			pX->SetNumberOfComponents(1);
			pX->SetNumberOfTuples(iNumPts);
			pX->FillComponent(0,m_dDefaultVals[0]);
		}
		else
		{
			vstr::errp() << "[vtkFieldDataToCoordsFilter::RequestData] unable to find data field" << endl;
			vstr::err() << "\t\t field name: " << m_strXFieldName << endl << endl;
			return 0;
		}
	}

	if(!pY)
	{
		if(m_strYFieldName.empty() && m_iFieldComponent[1] == 0)
		{
			pY = vtkDoubleArray::New();
			pY->SetNumberOfComponents(1);
			pY->SetNumberOfTuples(iNumPts);
			pY->FillComponent(0,m_dDefaultVals[1]);
		}
		else
		{
			vstr::errp() << "[vtkFieldDataToCoordsFilter::RequestData] unable to find data field" << endl;
			vstr::err() << "\t\t field name: " << m_strYFieldName << endl << endl;
			return 0;
		}
	}

	if(!pZ)
	{
		if(m_strZFieldName.empty() && m_iFieldComponent[2] == 0)
		{
			pZ = vtkDoubleArray::New();
			pZ->SetNumberOfComponents(1);
			pZ->SetNumberOfTuples(iNumPts);
			pZ->FillComponent(0,m_dDefaultVals[2]);
		}
		else
		{
			vstr::errp() << "[vtkFieldDataToCoordsFilter::RequestData] unable to find data field" << endl;
			vstr::err() << "\t\t field name: " << m_strZFieldName << endl << endl;
			return 0;
		}
	}

	//allocate memory for output points and point data
	vtkPoints *pPts = vtkPoints::New();
	pPts->Allocate(iNumPts);
	pOutPD->CopyAllocate(pInPD);

	// compute scale ranges
	double dXRange[2];
	pX->GetRange(dXRange,m_iFieldComponent[0]);
	double dXScale = (m_dTargetBounds[1] - m_dTargetBounds[0])/(dXRange[1] - dXRange[0]);
	
	double dYRange[2];
	pY->GetRange(dYRange,m_iFieldComponent[1]);
	double dYScale = (m_dTargetBounds[3] - m_dTargetBounds[2])/(dYRange[1] - dYRange[0]);
	
	double dZRange[2];
	pZ->GetRange(dZRange,m_iFieldComponent[2]);
	double dZScale = (m_dTargetBounds[5] - m_dTargetBounds[4])/(dZRange[1] - dZRange[0]);

	//Actually create output scatter plot
	double dCurrentPt[3];
	int iCurrentId;
	for(vtkIdType i=0; i<iNumPts; ++i)
	{
		//retrieve data values
		dCurrentPt[0] = m_dTargetBounds[0] + (pX->GetComponent(i,m_iFieldComponent[0])-dXRange[0]) * dXScale;
		dCurrentPt[1] = m_dTargetBounds[2] + (pY->GetComponent(i,m_iFieldComponent[1])-dYRange[0]) * dYScale;
		dCurrentPt[2] = m_dTargetBounds[4] + (pZ->GetComponent(i,m_iFieldComponent[2])-dZRange[0]) * dZScale;
		//insert new pt
		iCurrentId = pPts->InsertNextPoint(dCurrentPt);
		//copy associated point data (for later scatterplot coloring)
		pOutPD->CopyData(pInPD,i,iCurrentId);
	}

	pPts->Squeeze();
	pOutput->SetPoints(pPts);
	pPts->Delete();

	//cleanup tmp data structures
	if(m_strXFieldName.empty())
		pX->Delete();
	if(m_strYFieldName.empty())
		pY->Delete();
	if(m_strZFieldName.empty())
		pZ->Delete();
	
	if(!m_bGenerateVertices)
	{
		pOutput->Squeeze();
		return 1;
	}

	// create a vertex for every single point
	vtkCellArray *pVerts = vtkCellArray::New();
	pVerts->Allocate(iNumPts);
	for(vtkIdType i=0; i<iNumPts; ++i)
	{
		pVerts->InsertNextCell(1,&i);
	}
	pOutput->SetVerts(pVerts);
	pOutput->Squeeze();
	pVerts->Delete();
	return 1;
}


int vtkFieldDataToCoordsFilter::FillInputPortInformation(int port, vtkInformation *info)
{
	info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
	return 1;
}
