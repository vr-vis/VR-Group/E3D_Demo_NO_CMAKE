/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VtkComputeFTLEFromFlowMap.h RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VTKCOMPUTEFTLEFROMFLOWMAP_H
#define VTKCOMPUTEFTLEFROMFLOWMAP_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "../VistaVisExtConfig.h"

#include <vtkImageAlgorithm.h>

#include <string>
#include <algorithm>

/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 * This filter implements the actual computation of FTLE values from a given
 * flow map computed on a vtkImageData.
 * The flow map has to be given as a 3-component vtkDoubleArray attached
 * to the input's point data. Its default name is assumed to be "flowmap".
 * In addition to that, there has to be an error field, which flags "outside"
 * data entries with a given value. The outside field is assumed to be a 
 * 1-component vtkDoubleArray. Its default name is "error".
 *
 * NOTE that it does ONLY THE FTLE computation, i.e. the actually hard part of 
 * computing the flow map has to be done outside this filter. Consider
 * using the Vve particle building blocks under Vve/Algorithms/Particles for
 * achieving this.
 *
 * NOTE: Currently, this filter is not aware of VTK's internal streaming 
 *		 and/or parallel processing capabilities, i.e. the implementation
 *		 currently ignores things like the update extent.
 *
 * NOTE: However, the implementation uses OpenMP parallelism to speed up the
 *       computation internally.
 */
class VISTAVISEXTAPI vtkComputeFTLEFromFlowMap : public vtkImageAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkComputeFTLEFromFlowMap,vtkImageAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

	static vtkComputeFTLEFromFlowMap *New();

	
	/**
	 * Set the name of the flow map field that serves as input to the 
	 * computation. Defaults to "flowmap".
	 */
	void SetFlowMapField(const std::string &strFieldName);

	/** 
	 * Retrieve the name of the flow map field that serves as input to
	 * the computation.
	 */
	std::string GetFlowMapField() const;

	/**
	 * Set the name of the error field
	 */
	void SetErrorField(const std::string &strErrorField);
	
	/**
	 * Get the name of the error field
	 */
	std::string GetErrorField() const;

	/**
	 * Set the value marking "outside" points in the flow map field
	 * Defaults to std::numeric_limits<double>::max()
	 */
	void SetOutsideValue(const double val);

	/**
	 * retrieve the outside value.
	 */
	double GetOutsideValue() const;

	/**
	 * set the output field name
	 */
	void SetFTLEField(const std::string &strFieldName);
	
	/**
	 * Get the output field name
	 */
	std::string GetFTLEField() const;
	

		
	/**
	 * Set the time horizon used for integrating the flow map (in sim time units).
	 */
	void SetFTLEHorizon(const double val);

	/**
	 * Get the time horizon used for integrating the flow map (in sim time units).
	 */
	double GetFTLEHorizon() const;

	/**
	 * Determine whether or not the intermediate fields (flowmap and error)
	 * will be passed on to the output or if only the ftle field is appended.
	 * NOTE: All other point data will be kept in any case.
	 */
	void SetPassIntermediateFields(const bool val);

	/**
	 * Determine whether or not the intermediate fields (flowmap and error)
	 * will be passed on to the output. 
	 */
	bool GetPassIntermediateFields() const;
	
protected:
	vtkComputeFTLEFromFlowMap();
	virtual ~vtkComputeFTLEFromFlowMap();

	/**
	* this is where the real work is done.
	*/
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

	/**
	 * helper routines
	 */
	int GridToId(int i, int j, int k, int iDims[3]) const;
	int GridToId_clamp(int i, int j, int k, int iDims[3]) const;

private:
	/** the name of the flow map vector field - defaults to "flowmap"*/
	std::string m_strFlowMapField;

	/** the name of the error field, which among other things flags outside values */
	std::string m_strErrorField;

	/** set the value marking outside values in the flow map field */
	double m_dOutsideValue;
	
	/** default name for the output field */
	std::string m_strFTLEField;
	
	/** the time used to integrate the traces of the flow map */
	double m_dFTLEHorizon;

	/** determine whether or not to pass intermediate data fields to the output */
	bool m_bPassIntermediateFields;
	

};

/**
 * helper routine for converting grid coords to integer point id
 */
inline int vtkComputeFTLEFromFlowMap::GridToId(int i, int j, int k, int iDims[3]) const
{
	return i + iDims[0] * (j + iDims[1] * k);
}

/**
 * helper routine for converting grid coords to integer point id with 
 * additional clamping to grid bounds.
 */
inline int vtkComputeFTLEFromFlowMap::GridToId_clamp(int i, int j, int k, int iDims[3]) const
{
	int _ijk[3] = {
		std::max(0, std::min(iDims[0]-1, i)),
		std::max(0, std::min(iDims[1]-1, j)),
		std::max(0, std::min(iDims[2]-1, k))
	};
	return GridToId(_ijk[0], _ijk[1], _ijk[2], iDims);
}


#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/