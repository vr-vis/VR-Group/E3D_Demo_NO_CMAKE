/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VtkDemoAppl.CPP              RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VTK-Demo                     RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  Template for own                                          */
/*                  VTK-Applications                                          */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/

#include "VtkRegionGrowingFilter.h"

// vtk stuff
#include <vtkDataSet.h>
#include <vtkContourFilter.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkPolyData.h>
#include <vtkObjectFactory.h>
#include <vtkDataArray.h>
#include <vtkCell.h>
#include <vtkIdList.h>
#include <vtkPointData.h>

#include <VistaBase/VistaStreamUtils.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
vtkCxxRevisionMacro(VtkGrowIsosurface, "$Revision 0.00$");
vtkStandardNewMacro(VtkGrowIsosurface);

VtkGrowIsosurface::VtkGrowIsosurface() 
	:	m_bExtractLocal(true), 
		m_iNumComponents(0), 
		m_pContour(vtkContourFilter::New()), 
		m_pConnect(vtkPolyDataConnectivityFilter::New())
{
	m_fSeedPoint[0] = m_fSeedPoint[1] = m_fSeedPoint[2] = 0.0f;
	//we will likely encounter several updates -> use scalar tree to speed them up.
	m_pContour->UseScalarTreeOn();
	m_pConnect->SetInput(m_pContour->GetOutput());
	m_pConnect->ScalarConnectivityOff();
	
}

VtkGrowIsosurface::~VtkGrowIsosurface()
{
	m_pContour->Delete();
	m_pConnect->Delete();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetSeedPoint                                                */
/*                                                                            */
/*============================================================================*/
void VtkGrowIsosurface::SetSeedPoint(float fSeed[3])
{
	m_fSeedPoint[0] = fSeed[0];
	m_fSeedPoint[1] = fSeed[1];
	m_fSeedPoint[2] = fSeed[2];
	this->Modified();
}
void VtkGrowIsosurface::SetSeedPoint(float fx, float fy, float fz)
{
	m_fSeedPoint[0] = fx;
	m_fSeedPoint[1] = fy;
	m_fSeedPoint[2] = fz;
	this->Modified();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSeedPoint                                                */
/*                                                                            */
/*============================================================================*/
void VtkGrowIsosurface::GetSeedPoint(float fSeed[3]) const
{
	fSeed[0] = m_fSeedPoint[0];
	fSeed[1] = m_fSeedPoint[1];
	fSeed[2] = m_fSeedPoint[2];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetExtractLocal                                             */
/*                                                                            */
/*============================================================================*/
void VtkGrowIsosurface::SetExtractLocal(bool b)
{
	m_bExtractLocal = b;
	this->Modified();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetExtractLocal                                             */
/*                                                                            */
/*============================================================================*/
bool VtkGrowIsosurface::GetExtractLocal() const
{
	return m_bExtractLocal;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumberOfComponents                                       */
/*                                                                            */
/*============================================================================*/
int VtkGrowIsosurface::GetNumberOfComponents() const
{
	return m_iNumComponents;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetInput				                                      */
/*                                                                            */
/*============================================================================*/
void VtkGrowIsosurface::SetInput(vtkDataSet *input)
{
	vtkDataSetToPolyDataFilter::SetInput (input);
	// set input as contour input
	m_pContour->SetInput(input);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetActiveScalarsName	                                      */
/*                                                                            */
/*============================================================================*/
void VtkGrowIsosurface::SetActiveScalarsName (const std::string & strScalar)
{
	if (this->GetInput() && this->GetInput()->GetPointData())
	{	
		this->GetInput()->GetPointData()->SetActiveScalars(strScalar.c_str());
	}	

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ExecuteData                                                 */
/*                                                                            */
/*============================================================================*/
void VtkGrowIsosurface::ExecuteData(vtkDataObject *pNotUsed)
{
	vstr::outi() << "Updating isosurface growing..." << endl;
	vtkDataSet *pInput = this->GetInput();
	
	double dIsoVal = 0.0;

	if (m_bUseConstantIsoValue)
	{
		dIsoVal = m_dConstantIsoValue;
	}
	else
	{
		if(!this->DetermineIsoValue(pInput, m_fSeedPoint, dIsoVal))
			return;
	}
	
	vstr::debugi() << "\tIsovalue = " << dIsoVal << endl;
	vstr::debugi() << "\tUpdating contour...";
	
	//compute entire contour
	m_pContour->SetValue(0, dIsoVal);
	m_pContour->Update();

	vstr::debugi() << "DONE!" << endl;
	vstr::debugi() << "\t#contour polys : " << m_pContour->GetOutput()->GetNumberOfPolys() << endl;

	if (!m_bUseConstantIsoValue)
	{
		//analyze contour for connected components
		m_pConnect->SetClosestPoint(m_fSeedPoint);

		//extract either all components or only the seeded one
		if(m_bExtractLocal)
		{
			m_pConnect->SetExtractionModeToClosestPointRegion();
		}
		else
		{
			m_pConnect->SetExtractionModeToAllRegions();
			m_pConnect->ColorRegionsOn();
		}

		m_pConnect->Update();

		vstr::debugi() << "DONE!" << endl;
		vstr::debugi() << "\t#connected components = " << m_pConnect->GetNumberOfExtractedRegions() << endl;
		vstr::debugi() << "\t#polys in first comp  = " << m_pConnect->GetOutput()->GetNumberOfPolys() << endl;

		m_iNumComponents = m_pConnect->GetNumberOfExtractedRegions();
		this->GetOutput()->ShallowCopy(m_pConnect->GetOutput());
	}
	else
	{
		this->GetOutput()->ShallowCopy(m_pContour->GetOutput());
	}
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   DetermineIsoValue                                           */
/*                                                                            */
/*============================================================================*/
bool VtkGrowIsosurface::DetermineIsoValue(vtkDataSet *pDS, double fPt[3], double &dIsoVal)
{
	int subId;
	double pcoords[3], weights[32];
	
	vtkCell *pCell = pDS->FindAndGetCell(fPt, NULL, 0, 0.0, subId, pcoords, weights);
	
	if(!pCell)
		return false;
	
	dIsoVal = 0.0;
	vtkDataArray *pScalars = pDS->GetPointData()->GetScalars();

	for(register int i=0; i<pCell->GetNumberOfPoints(); ++i)
		dIsoVal += weights[i] * pScalars->GetTuple1(pCell->GetPointId(i));
	
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetConstantIsoValue                                         */
/*                                                                            */
/*============================================================================*/
void VtkGrowIsosurface::SetConstantIsoValue (double dIsoVal)
{
	m_dConstantIsoValue = dIsoVal;
	m_bUseConstantIsoValue = true;

}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VtkDemoAppl.cpp"                                             */
/*============================================================================*/
