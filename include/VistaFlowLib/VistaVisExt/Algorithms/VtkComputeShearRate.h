/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkComputeShearRate.h        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA VisExt                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VtkComputeShearRate    :                                          */
/*                                                                            */
/*============================================================================*/
#ifndef _VTKCOMPUTESHEARRATE_H
#define _VTKCOMPUTESHEARRATE_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkDataSetAlgorithm.h>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*
*
*/
class VISTAVISEXTAPI VtkComputeShearRate : public vtkDataSetAlgorithm
{
public:
	vtkTypeRevisionMacro(VtkComputeShearRate,vtkDataSetAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

	static VtkComputeShearRate *New();

	/**
	* Choose whether to compute point data or cell data.
	* Normally, point data will be the standard data attribute type handled by
	* a downstream pipeline. However, using cell data as output will circumvent one
	* additional interpolation step and thus yield more precise results in less time.
	* Default is ComputePointDataOn
	*/
	void ComputePointDataOn();
	void ComputePointDataOff();
	bool GetComputePointData() const;

	/**
	* Get/Set the name of the output data field (either point or cell data)
	* The default name is shear_rate. If this filter's input already has a data
	* field named like m_strOutputFieldName, execution will be terminated right away.
	*/
	void SetOutputFieldName(const std::string& strFieldName);
	std::string GetOutputFieldName() const;
protected:
	VtkComputeShearRate();
	virtual ~VtkComputeShearRate();

	/**
	* this is where the real work is done.
	*/
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

private:
	std::string m_strOutputFieldName;
	bool m_bComputePointData;
};


#endif //#ifdef _VtkComputeShearRate_H

