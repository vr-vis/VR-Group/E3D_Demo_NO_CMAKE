#ifndef _VTKMULTIFIELDTHRESHOLDFILTER_H
#define _VTKMULTIFIELDTHRESHOLDFILTER_H

#include <vtkUnstructuredGridAlgorithm.h>

#include <string>
#include <vector>

#include <VistaVisExt/VistaVisExtConfig.h>

class vtkDataArray;

/**
* Perform thresholding on a(ny) number of different data fields at the same time
* For reasonable data set sizes this should be faster than using multiple threshold filters.
* Currently, this supports only simple range querying.
*
* @todo Think about the boolean concatenation (are the thresholds or'ed and/or and'ed) -> Make this an option!
*/
class VISTAVISEXTAPI vtkMultiFieldThresholdFilter : public vtkUnstructuredGridAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkMultiFieldThresholdFilter,vtkUnstructuredGridAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);
	static vtkMultiFieldThresholdFilter *New();

	/**
	* query management
	*/
	void AddQuery(const std::string& strFieldName, double dRange[2]);
	size_t GetNumQueries() const;
	void GetQuery(const int i, std::string& strFieldName, double dRange[2]);
	void RemoveQuery(const int i);
	void RemoveQueriesOverField(const std::string& strFieldName);
	void ClearQueries();

	void SetExtractCells(bool b);
	void ExtractCellsOn();
	void ExtractCellsOff();
	bool GetExtractCells() const;

protected:
	vtkMultiFieldThresholdFilter();
	virtual ~vtkMultiFieldThresholdFilter();

	/**
	* define that input may be arbitrary vtkDataSet
	*/
	virtual int FillInputPortInformation(int port, vtkInformation *info);

	/**
	* update the output
	*/
	virtual int RequestData(vtkInformation* request,
                          vtkInformationVector** inputVector,
                          vtkInformationVector* outputVector);
private:
	//hide copy constructor and operator=
	vtkMultiFieldThresholdFilter(const vtkMultiFieldThresholdFilter&);
	const vtkMultiFieldThresholdFilter& operator=(const vtkMultiFieldThresholdFilter&);

	struct SQuery{
		SQuery(const std::string& str, double d[2]) : strFieldName(str) {
			dRange[0] = d[0];
			dRange[1] = d[1];
		}
		double dRange[2];
		//NOTE: The target field is generally addressed by its name. However, during execution
		//      the pointer to the actual field is cached.
		std::string strFieldName;
		vtkDataArray *pTargetField;
	};
	/* @todo : OPTIMIZATION We could aggregate several queries over the same data field into one
	                        single multi range query. In consequence the field's data value would
							have to be accessed only once.
	*/
	std::vector<SQuery> m_vecQueries;

	/**
	* remember of we want to extract the cells as well.
	*/
	bool m_bExtractCells;
};


#endif //_VTKMULTIFIELDTHRESHOLDFILTER_H
