/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
//------------
#include <vtkVersion.h>
//------------

#include "VtkInterpolatedScalarField.h"

#include <vtkDataSet.h>
#include <vtkDataArray.h>
#include <vtkGenericCell.h>
#include <vtkPointData.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VtkInterpolatedScalarField::VtkInterpolatedScalarField()
	:	m_pDataSet(0),
		m_pLastCell(0),
		m_pDummyCell(0),
		m_iLastCellId(-1),
		m_arrfWeights(0),
		m_bCaching(false)
{
	// we implement a map f: R^3 -> R
	NumIndepVars = 3;
	NumFuncs = 1;

	m_pLastCell = vtkGenericCell::New();
	m_pDummyCell = vtkGenericCell::New();
}

VtkInterpolatedScalarField::~VtkInterpolatedScalarField()
{
    if(m_arrfWeights)
        delete[] m_arrfWeights;
    if(m_pDataSet != 0) 
        this->m_pDataSet->UnRegister(this);

    m_pLastCell->Delete();
    m_pDummyCell->Delete();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :     New                                                       */
/*                                                                            */
/*============================================================================*/
VtkInterpolatedScalarField* VtkInterpolatedScalarField::New(){
	return new VtkInterpolatedScalarField();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :     SetDataSet                                                */
/*                                                                            */
/*============================================================================*/
void VtkInterpolatedScalarField::SetDataSet(vtkDataSet* pDataSet)
{
	if(pDataSet && m_pDataSet != pDataSet)
    {
		if(m_pDataSet != 0) 
			m_pDataSet->UnRegister(this);
		
		m_pDataSet = pDataSet;
		m_pDataSet->Register(this);
		this->Modified();

		if(m_arrfWeights)
			delete[] m_arrfWeights;
		m_arrfWeights = 0; 
		// Allocate space for the largest cell
		m_arrfWeights = new double[m_pDataSet->GetMaxCellSize()];
		ClearLastCellId();
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :     FunctionValues                                            */
/*                                                                            */
/*============================================================================*/
int VtkInterpolatedScalarField::FunctionValues(double* x, double* f)
{
	//check if data set pointer valid
	if(!m_pDataSet)
		return 0;

	f[0] = 0;
	vtkDataArray* pScalars = m_pDataSet->GetPointData()->GetScalars();
	
	//check if there are valid scalars
	if(!pScalars)
		return 0;

	int iSubId;
	
	//use caching if enabled && there has already been a valid cell
	if(m_bCaching && m_iLastCellId>=0)
	{
		int iReturnVal;
		double fDist2;
		iReturnVal = m_pLastCell->EvaluatePosition(x,0,iSubId,m_arrfLastParamCoords,fDist2,m_arrfWeights);
		if(iReturnVal == 0) //i.e. x is outside the cached cell
		{
			//CACHE MISS
			//start cell search with last known cell id
			m_iLastCellId = m_pDataSet->FindCell (x, m_pLastCell, m_pDummyCell, m_iLastCellId, 0.0f, iSubId, m_arrfLastParamCoords, m_arrfWeights);
			if(m_iLastCellId>=0)
				m_pDataSet->GetCell(m_iLastCellId, m_pLastCell);
			else 
				return 0;
		}
	}
	else
	{
		//find and get cell via (dumb) global search
		m_iLastCellId = m_pDataSet->FindCell (
            x, NULL, m_pDummyCell, -1, 0.0f, iSubId, m_arrfLastParamCoords, m_arrfWeights);
		if(m_iLastCellId>=0)
			m_pDataSet->GetCell(m_iLastCellId, m_pLastCell);
		else
			return 0;
	}
	
	int iNumPts = m_pLastCell->GetNumberOfPoints();
	int iPtId = 0;
	double fScalar = 0.0f;

	//finally interpolate
	for(register int j=0; j<iNumPts; ++j){
		iPtId = m_pLastCell->GetPointIds()->GetId(j);
		pScalars->GetTuple(iPtId, &fScalar);
		f[0] += fScalar * m_arrfWeights[j];
	}
	return 1;
}
