#include "VtkDataSetToPolyDataVertices.h"

#include <vtkDataSet.h>
#include <vtkPolyData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkPoints.h>
#include <vtkObjectFactory.h>
#include <vtkCellArray.h>

#include <VistaBase/VistaStreamUtils.h>

vtkCxxRevisionMacro(vtkDataSetToPolyDataVertices, "$$");
vtkStandardNewMacro(vtkDataSetToPolyDataVertices);

vtkDataSetToPolyDataVertices::vtkDataSetToPolyDataVertices(void)
{
}

vtkDataSetToPolyDataVertices::~vtkDataSetToPolyDataVertices(void)
{
}

void vtkDataSetToPolyDataVertices::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
}

int vtkDataSetToPolyDataVertices::RequestData(vtkInformation* request,
											vtkInformationVector** inputVector,
											vtkInformationVector* outputVector)
{
	// retrieve data objects from information vectors
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkDataSet *pInput = vtkPointSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkPolyData *pOutput = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

	if(!pOutput)
	{
		vstr::errp() << "[vtkDataSetToPolyDataVertices::RequestData] no output!" << endl;
		return 0;
	}

	//if it's a point set => just ref the points object
	if(pInput)
	{
		pOutput->SetPoints(static_cast<vtkPointSet*>(pInput)->GetPoints());
	}
	else //otherwise copy point by point
	{
		pInput = vtkDataSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
		if(!pInput)
		{
			vstr::errp() << "[vtkDataSetToPolyDataVertices::RequestData] no input!" << endl;
			return 0;
		}
		vtkPoints *pPts = vtkPoints::New();
		//double *pCurPt;
		pPts->SetNumberOfPoints(pInput->GetNumberOfPoints());
		for(int i=0; i<pInput->GetNumberOfPoints(); ++i)
			pPts->SetPoint(i, pInput->GetPoint(i));
	}
	//copy point data by reference
	vtkPointData *pInPD = pInput->GetPointData();
	vtkPointData *pOutPD = pOutput->GetPointData();
	pOutPD->ShallowCopy(pInPD);
	// create a vertex for every single point
	vtkCellArray *pVerts = vtkCellArray::New();
	pVerts->Allocate(pInput->GetNumberOfPoints());
	for(vtkIdType i=0; i<pInput->GetNumberOfPoints(); ++i)
		pVerts->InsertNextCell(1,&i);
	//assemble output and cleanup
	pOutput->SetVerts(pVerts);
	pOutput->Squeeze();
	pVerts->Delete();
	return 1;
}


int vtkDataSetToPolyDataVertices::FillInputPortInformation(int port, vtkInformation *info)
{
	info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
	return 1;
}
