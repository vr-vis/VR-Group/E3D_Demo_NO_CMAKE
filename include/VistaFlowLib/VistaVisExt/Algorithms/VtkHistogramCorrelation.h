/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  vtkHistogramCorrelation.h    RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA VisExt                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - vtkHistogramCorrelation    :                                          */
/*                                                                            */
/*============================================================================*/
#ifndef _VTKHISTOGRAMCORRELATION_H
#define _VTKHISTOGRAMCORRELATION_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkPolyDataAlgorithm.h>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 *
 */
class VISTAVISEXTAPI vtkHistogramCorrelation : public vtkPolyDataAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkHistogramCorrelation,vtkPolyDataAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

	static vtkHistogramCorrelation *New();

	/**
	 *
	 */
	void SetTargetBounds(float fBounds[6]);
	void GetTargetBounds(float fBounds[6]) const;

protected:
	vtkHistogramCorrelation();
	virtual ~vtkHistogramCorrelation();

	/**
	* this is where the real work is done.
	*/
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

	/**
	 *
	 */
	virtual int RequestUpdateExtent(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
	/**
	*
	*/
	virtual int FillInputPortInformation(int port, vtkInformation *info);

private:
	bool m_bGenerateVertices;

	float m_fTargetBounds[6];
};


#endif //#ifdef _VTKHISTOGRAMCORRELATION_H

