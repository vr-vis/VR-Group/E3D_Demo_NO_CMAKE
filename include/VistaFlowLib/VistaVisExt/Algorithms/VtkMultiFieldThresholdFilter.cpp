#include "VtkMultiFieldThresholdFilter.h"

#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkPoints.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkObjectFactory.h>
#include <vtkCellArray.h>

#include <VistaBase/VistaStreamUtils.h>

#include <cassert>

using namespace std;
//================================================================
//======				IMPLEMENTATION						======
//================================================================
//================================================================
/************************************
* vtkMultiFieldThreshold
*************************************/
vtkCxxRevisionMacro(vtkMultiFieldThresholdFilter, "$ $");
vtkStandardNewMacro(vtkMultiFieldThresholdFilter);

vtkMultiFieldThresholdFilter::vtkMultiFieldThresholdFilter() : m_bExtractCells(false)
{
}

vtkMultiFieldThresholdFilter::~vtkMultiFieldThresholdFilter()
{
}

void vtkMultiFieldThresholdFilter::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);
	os << indent << "ExtractCells = " << (this->GetExtractCells() ? "on" : "off") << endl;
}


void vtkMultiFieldThresholdFilter::AddQuery(const std::string& strFieldName, double dRange[2])
{
	m_vecQueries.push_back(SQuery(strFieldName,dRange));
	this->Modified();
}


size_t vtkMultiFieldThresholdFilter::GetNumQueries() const
{
	return m_vecQueries.size();
}


void vtkMultiFieldThresholdFilter::GetQuery(const int i, std::string& strFieldName, double dRange[2])
{
	assert(i<int(m_vecQueries.size()) && i>=0);
	
	strFieldName = m_vecQueries[i].strFieldName;
	dRange[0] = m_vecQueries[i].dRange[0];
	dRange[1] = m_vecQueries[i].dRange[1];
}

void vtkMultiFieldThresholdFilter::RemoveQuery(const int i)
{
	assert(i<int(m_vecQueries.size()) && i>=0);

	vector<SQuery>::iterator it(m_vecQueries.begin());
	for(int j=0; j<i; ++j) ++it;
	m_vecQueries.erase(it);
	this->Modified();
}


void vtkMultiFieldThresholdFilter::RemoveQueriesOverField(const std::string& strFieldName)
{
	vector<SQuery>::iterator it(m_vecQueries.begin());
	while(it!=m_vecQueries.end())
	{
		if(it->strFieldName == strFieldName)
		{
			it = m_vecQueries.erase(it);
			if(it == m_vecQueries.end())
				break;
		}
		++it;
	}
	this->Modified();
}

void vtkMultiFieldThresholdFilter::ClearQueries()
{
	m_vecQueries.clear();
	this->Modified();
}


void vtkMultiFieldThresholdFilter::SetExtractCells(bool b)
{
	m_bExtractCells = b;
	this->Modified();
}


void vtkMultiFieldThresholdFilter::ExtractCellsOn()
{
	this->SetExtractCells(true);
}


void vtkMultiFieldThresholdFilter::ExtractCellsOff()
{
	this->SetExtractCells(false);
}


bool vtkMultiFieldThresholdFilter::GetExtractCells() const
{
	return m_bExtractCells;
}


int vtkMultiFieldThresholdFilter::FillInputPortInformation(int port, vtkInformation *info)
{
	//tell inbound filter/executive that we'll take vtkDataSet (and subtypes) as input
	info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
	return 1;
}


int vtkMultiFieldThresholdFilter::RequestData(vtkInformation* request,
											  vtkInformationVector** inputVector,
											  vtkInformationVector* outputVector)
{
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkDataSet			*pInput = vtkDataSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkUnstructuredGrid *pOutput = vtkUnstructuredGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pOutput)
	{
		vstr::errp() << "[vtkMultiFieldThresholdFilter::RequestData] no output!" << endl;
		return 0;
	}
	if(!pInput)
	{
		vstr::errp() << "[vtkMultiFieldThresholdFilter::RequestData] no input!" << endl;
		return 0;
	}
	vtkPointData *pInPD(pInput->GetPointData());
	vtkPointData *pOutPD(pOutput->GetPointData());
	
	//get data fields for all queries
	for(unsigned int i=0; i<m_vecQueries.size(); ++i)
	{
		m_vecQueries[i].pTargetField = pInPD->GetArray(m_vecQueries[i].strFieldName.c_str());
		if(m_vecQueries[i].pTargetField == NULL)
		{
			vstr::errp() << "[vtkMultiFieldThresholdFilter::RequestData] input is missing field!" << endl;
			vstr::err() << "                                                        " << m_vecQueries[i].strFieldName << endl;
			return 0;
		}
		if(m_vecQueries[i].pTargetField->GetNumberOfComponents() > 1)
		{
			vstr::errp() << "[vtkMultiFieldThresholdFilter::RequestData] can't operate on non-scalar data!" << endl;
			return 0;
		}
		
	}
	const int iNumInPts = pInput->GetNumberOfPoints();
	//allocate memory for output data
	vtkPoints *pOutPts = vtkPoints::New();
	pOutPts->Allocate(iNumInPts);
	pOutPD->CopyAllocate(pInPD, iNumInPts);
	
	//allocate additional structures if we want to extract cells as well
	int *iPtIdMap = NULL;
	if(m_bExtractCells)
	{
		//remember for each input pt, where we put it in the new data set
		iPtIdMap = new int[iNumInPts];
		for(int i=0; i<iNumInPts; ++i)
			iPtIdMap[i] = -1;
	}

	bool bPtIn = true;
	double dPtVal;
	int iOutPtId;
	vector<SQuery>::iterator itQuery;
	vector<SQuery>::iterator itLast = m_vecQueries.end();
	//perform thresholding by checking each querry for each point
	for(int iPt=0; iPt<iNumInPts; ++iPt)
	{
		bPtIn = true;
		for(itQuery=m_vecQueries.begin(); itQuery!=itLast && bPtIn; ++itQuery)
		{
			//NOTE: Calls to GetTuple are NOT THREAD SAFE!
			dPtVal = itQuery->pTargetField->GetTuple1(iPt);
			bPtIn &= (dPtVal >= itQuery->dRange[0] && dPtVal <= itQuery->dRange[1]);
		}
		if(bPtIn)
		{
			//CRITICAL BEGIN
			iOutPtId = pOutPts->InsertNextPoint(pInput->GetPoint(iPt));
			pOutPD->CopyData(pInPD, iPt, iOutPtId);
			//CRITICAL END
			if(m_bExtractCells)
				iPtIdMap[iPt] = iOutPtId;
		}
	}
	//assemble output
	pOutPts->Squeeze();
	pOutPD->Squeeze();
	
	pOutput->SetPoints(pOutPts);
	pOutPts->Delete();
	pOutput->Squeeze();
	
	//if we don't want to extract cells we are done here!
	if(!m_bExtractCells || pInput->GetNumberOfCells() == 0)
	{
		delete[] iPtIdMap;
		return 1;
	}

	bool bCellIn = true;
	vtkIdList *pPts = vtkIdList::New();
	int iNumCells = pInput->GetNumberOfCells();
	int *pCellTypes = new int[iNumCells];
	int iCurrentCell = 0;
	vtkCellArray *pNewCells = vtkCellArray::New();
	//for each cell : add cell to output iff all its points are in the output
	for(int iCell =0; iCell<iNumCells; ++iCell)
	{
		pInput->GetCellPoints(iCell, pPts);
		bCellIn = true;
		for(int iPt=0; iPt<pPts->GetNumberOfIds() && bCellIn; ++iPt)
		{
			//point is in output iff its mapped value is != -1
			bCellIn &= (iPtIdMap[pPts->GetId(iPt)] != -1);
			//permute point id right here!
			pPts->SetId(iPt, iPtIdMap[pPts->GetId(iPt)]);
		}
		if(bCellIn)
		{
			pCellTypes[iCurrentCell] = pInput->GetCell(iCell)->GetCellType();
			pNewCells->InsertNextCell(pPts);
		}
	}
	pOutput->SetCells(pCellTypes, pNewCells);
	pOutput->Squeeze();

	delete[] iPtIdMap;
	delete[] pCellTypes;
	pNewCells->Delete();
	
	return 1;
}


