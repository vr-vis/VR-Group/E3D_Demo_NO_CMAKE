/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkGrowIsosurface.h          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  Vista VTK Ext                RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VtkGrowIsosurface      :                                         */
/*                                                                            */
/*============================================================================*/

#ifndef _VTKGROWISOSURFACE_H
#define _VTKGROWISOSURFACE_H


/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkDataSetToPolyDataFilter.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkContourFilter;
class vtkPolyDataConnectivityFilter;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 *
 */    
class VISTAVISEXTAPI VtkGrowIsosurface : public vtkDataSetToPolyDataFilter
{
public:
	vtkTypeRevisionMacro(VtkGrowIsosurface,vtkDataSetToPolyDataFilter);

	static VtkGrowIsosurface *New();

	// overloaded SetInput call
	void SetInput(vtkDataSet *input);
	void SetActiveScalarsName (const std::string & strScalar);

	void SetSeedPoint(float fSeed[3]);
	void SetSeedPoint(float fx, float fy, float fz);
	void GetSeedPoint(float fSeed[3]) const;

	void SetExtractLocal(bool b);
	bool GetExtractLocal() const;

	int GetNumberOfComponents() const;

	bool DetermineIsoValue(vtkDataSet *pDS, double fPt[3], double& fIsoVal);

	void SetConstantIsoValue (double fIsoVal);

protected:
	VtkGrowIsosurface();
    virtual ~VtkGrowIsosurface();
    
	virtual void ExecuteData(vtkDataObject *notUsed);
	
private:
	/*
	* this is the seed which is used for isovalue determination
	* and localization of the first isosruface component
	*/
	double m_fSeedPoint[3];
	/*
	* do we want to extract just the seeded component of the isosurface
	* or all of its components
	*/
	bool m_bExtractLocal;
	/**
	*
	*/
	int m_iNumComponents;
	/**
	*
	*/
	vtkContourFilter *m_pContour;
	/**
	*
	*/
	vtkPolyDataConnectivityFilter *m_pConnect;

	/**
	*
	*/
	bool m_bUseConstantIsoValue;
	double m_dConstantIsoValue;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VTKGROWISOSURFACE_H
