/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VtkThresholdSelection.h"

#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkIdList.h>
#include <vtkUnsignedCharArray.h>

#include <VistaBase/VistaStreamUtils.h>

#ifdef _OPENMP
#include <omp.h>
#endif
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
vtkCxxRevisionMacro(vtkThresholdSelection, "$Id$");
vtkStandardNewMacro(vtkThresholdSelection);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
vtkThresholdSelection::vtkThresholdSelection() 
	: m_strOutputFieldName("selection")
{
	m_fThresholdRange[0] = m_fThresholdRange[1] = 0.0f;
}
//------------------------------------------------------------
//------------------------------------------------------------
vtkThresholdSelection::~vtkThresholdSelection()
{
}
/*============================================================================*/
/*  IMPLEMENTATION															  */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
void vtkThresholdSelection::PrintSelf(ostream& os, vtkIndent indent)
{
	vtkDataSetAlgorithm::PrintSelf(os, indent);
	os << indent << "Output field name : " << m_strOutputFieldName << endl;
	os << indent << "Range = [" << m_fThresholdRange[0];
	os << ", " << m_fThresholdRange << "]" << endl;
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkThresholdSelection::SetThresholdRange(double dRange[2])
{
	m_fThresholdRange[0] = dRange[0];
	m_fThresholdRange[1] = dRange[1];
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkThresholdSelection::SetThresholdRange(float fRange[2])
{
	m_fThresholdRange[0] = fRange[0];
	m_fThresholdRange[1] = fRange[1];
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkThresholdSelection::GetThresholdRange(double dRange[2]) const
{
	dRange[0] = m_fThresholdRange[0];
	dRange[1] = m_fThresholdRange[1];
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkThresholdSelection::GetThresholdRange(float fRange[2]) const
{
	fRange[0] = m_fThresholdRange[0];
	fRange[1] = m_fThresholdRange[1];
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkThresholdSelection::SetOutputFieldName(const std::string& strOutputFieldName)
{
	m_strOutputFieldName = strOutputFieldName;
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
std::string vtkThresholdSelection::GetOutputFieldName() const
{
	return m_strOutputFieldName;
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtkThresholdSelection::RequestData(vtkInformation *, vtkInformationVector **inputVector, vtkInformationVector *outputVector)
{
	// get the info objects
	//our input is the first input on the first port of this filter
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkDataSet *pInput = vtkDataSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkDataSet *pOutput = vtkDataSet::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pOutput)
	{
		vstr::errp() << "[vtkThresholdSelection::RequestData] no output!" << endl;
		return -1;
	}
	if(!pInput)
	{
		vstr::errp() << "[vtkThresholdSelection::RequestData] no input!" << endl;
		return -1;
	}
	// Initialize
	vtkDebugMacro(<<"Computing selection values!");
	
	const int iNumPts = pInput->GetNumberOfPoints();

	// First, copy the input to the output as a starting point
	pOutput->CopyStructure(pInput);	
	// Pass appropriate data through to output
	pOutput->GetPointData()->PassData(pInput->GetPointData());
	pOutput->GetCellData()->PassData(pInput->GetCellData());

	//retrieve vector field
	vtkDataArray *pScalars = pInput->GetPointData()->GetScalars();
	if(pScalars == NULL)
	{
		vstr::errp() << "[vtkThresholdSelection::RequestData] No scalars!" << endl;
		return -1;
	}
	float *pfRawScalars = NULL;
	double *pdRawScalars = NULL;
	if(pScalars->GetDataType() == VTK_FLOAT)
		pfRawScalars = (float*) pScalars->GetVoidPointer(0);
	else if(pScalars->GetDataType() == VTK_DOUBLE)
		pdRawScalars = (double*) pScalars->GetVoidPointer(0);
	else
	{
		vstr::warnp() << "[vtkThresholdSelection::RequestData] invalid scalars" << endl;
		vstr::warni() << "\tCurrently selection is done on input floats or doubles only!" << endl;
		return -1;
	}
	//setup array for selection
	vtkDataArray *pSelection = vtkUnsignedCharArray::New();
	pSelection->SetNumberOfComponents(1);
	pSelection->SetNumberOfTuples(iNumPts);
	pSelection->SetName(m_strOutputFieldName.c_str());

	unsigned char *pcRawSelection = (unsigned char*)pSelection->GetVoidPointer(0);

#pragma omp parallel \
	default(none)\
	shared(pfRawScalars, pdRawScalars, pcRawSelection, pInput, pOutput, pScalars)
	{	
		//HANDLE FLOAT SCALARS
		if(pScalars->GetDataType() == VTK_FLOAT)
		{
#pragma omp for
		//evaluate range query for each point
			for(int i=0; i<iNumPts; ++i)
			{
				pcRawSelection[i] = (pfRawScalars[i] >= m_fThresholdRange[0] && 
									 pfRawScalars[i] <= m_fThresholdRange[1] ? 255 : 0);
			}//END OPENMP FOR
		}
		//HANDLE DOUBLE SCALARS
		else if(pScalars->GetDataType() == VTK_DOUBLE)
		{
#pragma omp for
			//evaluate range query for each point
			for(int i=0; i<iNumPts; ++i)
			{
				pcRawSelection[i] = (pdRawScalars[i] >= m_fThresholdRange[0] && 
									 pdRawScalars[i] <= m_fThresholdRange[1] ? 255 : 0);
			}//END OPENMP FOR
		}
	} //END OPENMP PARALLEL SECTION
	
	//move selection array from cell to point data
	pOutput->GetPointData()->AddArray(pSelection);
	pSelection->Delete();
	pOutput->Squeeze();
	
	return 1;
}

