/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VtkComputeShearRate.h"

#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkGenericCell.h>
#include <vtkCellDataToPointData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkIdList.h>

#include <VistaBase/VistaStreamUtils.h>

#ifdef _OPENMP
#include <omp.h>
#endif
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
vtkCxxRevisionMacro(VtkComputeShearRate, "$Id$");
vtkStandardNewMacro(VtkComputeShearRate);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
VtkComputeShearRate::VtkComputeShearRate() : m_bComputePointData(true), m_strOutputFieldName("shear_rate")
{
}
//------------------------------------------------------------
//------------------------------------------------------------
VtkComputeShearRate::~VtkComputeShearRate()
{
}
/*============================================================================*/
/*  IMPLEMENTATION															  */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
void VtkComputeShearRate::PrintSelf(ostream& os, vtkIndent indent)
{
	vtkDataSetAlgorithm::PrintSelf(os, indent);
	os << indent << "Output field name : " << m_strOutputFieldName << endl;
	os << indent << "Generating " << (m_bComputePointData ? "point " : "cell ") << "data." << endl;
}
//------------------------------------------------------------
//------------------------------------------------------------
void VtkComputeShearRate::ComputePointDataOn()
{
	m_bComputePointData = true;
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
void VtkComputeShearRate::ComputePointDataOff()
{
	m_bComputePointData = false;
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
bool VtkComputeShearRate::GetComputePointData() const
{
	return m_bComputePointData;
}
//------------------------------------------------------------
//------------------------------------------------------------
void VtkComputeShearRate::SetOutputFieldName(const std::string& strOutputFieldName)
{
	m_strOutputFieldName = strOutputFieldName;
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
std::string VtkComputeShearRate::GetOutputFieldName() const
{
	return m_strOutputFieldName;
}
//------------------------------------------------------------
//------------------------------------------------------------
int VtkComputeShearRate::RequestData(vtkInformation *, vtkInformationVector **inputVector, vtkInformationVector *outputVector)
{
	// get the info objects
	//our input is the first input on the first port of this filter
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkDataSet *pInput = vtkDataSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkDataSet *pOutput = vtkDataSet::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pOutput)
	{
		vstr::errp() << "[VtkComputeShearRate::RequestData] no output!" << endl;
		return -1;
	}
	if(!pInput)
	{
		vstr::errp() << "[VtkComputeShearRate::RequestData] no input!" << endl;
		return -1;
	}
	if(!this->GetComputePointData() && pInput->GetCellData()->GetArray(m_strOutputFieldName.c_str()) != NULL)
	{
		vstr::errp() << "[VtkComputeShearRate::RequestData] field <" << m_strOutputFieldName <<"> already exists in input!" << endl;
		return -1;
	}
	if(this->GetComputePointData() && pInput->GetPointData()->GetArray(m_strOutputFieldName.c_str()) != NULL)
	{
		vstr::errp() << "[VtkComputeShearRate::RequestData] field <" << m_strOutputFieldName <<"> already exists in input!" << endl;
		return -1;
	}
	// Initialize
	vtkDebugMacro(<<"Computing shear_rate values for velocity field!");
	const int iNumInCells = pInput->GetNumberOfCells();
	
	// First, copy the input to the output as a starting point
	pOutput->CopyStructure( pInput );	
	// Pass appropriate data through to output
	pOutput->GetPointData()->PassData(pInput->GetPointData());
	pOutput->GetCellData()->PassData(pInput->GetCellData());

	//retrieve vector field
	vtkDataArray *pVelocity = pInput->GetPointData()->GetVectors();
	int iVectorDim = pVelocity->GetNumberOfComponents();
	//NOTE: Velocity field is either float or double -> We'll have to know for later!
	bool bVelocityIsDouble = vtkDoubleArray::SafeDownCast(pVelocity) != NULL;
	void *pRawVel = pVelocity->GetVoidPointer(0);

	//setup array for shear_rate
	vtkDataArray *pShearRate = vtkDoubleArray::New();
	pShearRate->SetNumberOfComponents(1);
	pShearRate->SetNumberOfTuples(pInput->GetNumberOfCells());
	pShearRate->SetName(m_strOutputFieldName.c_str());

	double *fShearRate = (double*)pShearRate->GetVoidPointer(0);

	//thread safety issues -> look at vtkCell's vtk documentation 
	vtkGenericCell *pTmpCell = vtkGenericCell::New(); //one gen cell for each thread
	pInput->GetCell(0,pTmpCell);
	pTmpCell->Delete();
	
#pragma omp parallel shared(fShearRate, pRawVel) 
	{
		//use generic cell interface for thread safety
		vtkGenericCell *pCell = vtkGenericCell::New();
		vtkIdList *pPtIds;  //point ids of cell's nodes
		
		double fCenter[24]; //parametric cell center
		//maximum number of cell points assumed here is 24 as VTK does not support bigger cells anyway
		double fVals[24*3]; //velocity values at cell's nodes
		double fTmp[9];
		double fJacobi[3][3];
		double dEntry = 0.0;
		int iSubID = 0;
		int j=0;
		int i=0;
		int iValOfs = 0;
		int iVecOfs = 0;
		
#pragma omp for
		for(i=0; i<iNumInCells; ++i)
		{
			//1) Compute velocity gradient at cell center 
			pInput->GetCell(i,pCell);
			pPtIds = pCell->GetPointIds();
			for(j=0; j<pPtIds->GetNumberOfIds(); ++j)
			{
				iValOfs = iVectorDim*j;
				iVecOfs = iVectorDim*pPtIds->GetId(j);
				//NOTE: Do not use vtkDataArray::GetTuple3(i)[j] here since it is NOT THREAD SAFE!
				if(bVelocityIsDouble)
					//double values can be copied right away!
					memcpy(&(fVals[iValOfs]),&(((double*)pRawVel)[iVecOfs]), iVectorDim*sizeof(double));
				else
				{
					//float values need to be casted and assigned
					fVals[iValOfs]	 = ((float*)pRawVel)[iVecOfs];
					fVals[iValOfs+1] = ((float*)pRawVel)[iVecOfs+1];
					fVals[iValOfs+2] = ((float*)pRawVel)[iVecOfs+2];
				}	
			}
			iSubID = pCell->GetParametricCenter(fCenter);
			pCell->Derivatives(iSubID, fCenter, fVals, 3, fTmp);
			memcpy(&(fJacobi[0][0]),fTmp,9*sizeof(double));
			
			//rate of strain rensor: S = 0.5 * (J + J^T) 
			//ShearRate = sqrt(0.5 * S:S) where <A:B> means componentwise multiplication 
			//to obtain a new NxN tensor and then summation over all NxN components 
			fShearRate[i] = 0;
			for(register int k=0; k<iVectorDim; ++k)
			{
				for(register int l=0; l<iVectorDim; ++l)
				{
					dEntry = 0.5f * (fJacobi[k][l] + fJacobi[l][k]);
					fShearRate[i] += 0.5f * (dEntry*dEntry);
				}
			}
			fShearRate[i] = sqrt(fShearRate[i]);
		}
		//CLEANUP
		pCell->Delete();
	} //END OPENMP PARALLEL SECTION
	
	//if cell data computation is sufficient -> skip translation to point data
	if(!m_bComputePointData)
	{
		pOutput->GetCellData()->AddArray(pShearRate);
		pOutput->GetCellData()->SetActiveScalars(m_strOutputFieldName.c_str());
		pOutput->Squeeze();
		pShearRate->Delete();
		return 1;
	}

	//translation to point data -> create tmp data set and put it through vtkCellDataToPointData filter
	//NOTE: Don't use output here directly since this will result in an endless recursion!
	vtkDataSet *pTmpDataSet = pInput->NewInstance();
	pTmpDataSet->ShallowCopy(pInput);
	pTmpDataSet->GetCellData()->AddArray(pShearRate);
	pShearRate->Delete();
	//compute point data attributes by interpolation of cell data
	vtkCellDataToPointData *pC2P = vtkCellDataToPointData::New();

#if VTK_MAJOR_VERSION > 5
	pC2P->SetInputData(pTmpDataSet);
#else
	pC2P->SetInput(pTmpDataSet);
#endif

	pC2P->Update();
	pShearRate = pC2P->GetOutput()->GetPointData()->GetArray(m_strOutputFieldName.c_str());
	if(!pShearRate)
	{
		vstr::errp() << "[VtkComputeShearRate::RequestData] unable to retrieve point data!" << endl;
		pC2P->Delete();
		return -1;
	}
	//move shear_rate array from cell to point data
	pOutput->GetPointData()->AddArray(pShearRate);
	pOutput->GetCellData()->RemoveArray(m_strOutputFieldName.c_str());
	pOutput->Squeeze();
	
	//finally clean up!
	pC2P->Delete();
	pTmpDataSet->Delete();
	
	return 1;
}

