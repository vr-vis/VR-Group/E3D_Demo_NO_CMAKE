/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  vtkThresholdSelection.h          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA VtkExt                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - vtkThresholdSelection    :                                            */
/*                                                                            */
/*============================================================================*/
#ifndef _vtkThresholdSelection_H
#define _vtkThresholdSelection_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkDataSetAlgorithm.h>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* This is a straightforward thresholding filter which will compute a selection
* value for each point according to a 1D range query rather than entirely 
* removing cells/points which are not selected. It can be used in cases where 
* you just want to highlight certain parts of a volume.
* Thresholding will be applied to the active scalar field.
*
* NOTE: Currently only FLOAT and DOUBLE input scalar fields are treated, i.e.
*       if the input scalar field is of neither type, you'll get no selection
*       output.
*
* @author   Bernd Hentschel
* @date     April, 2009
*
*/
class VISTAVISEXTAPI vtkThresholdSelection : public vtkDataSetAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkThresholdSelection,vtkDataSetAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

	static vtkThresholdSelection *New();
	
	/**
	 *
	 */
	void SetThresholdRange(double dRange[2]);
	void SetThresholdRange(float fRange[2]);
	
	void GetThresholdRange(double dRange[2]) const;
	void GetThresholdRange(float fRange[2]) const;
	
	/**
	* Get/Set the name of the output data field (either point or cell data)
	* The default name is "selection". If this filter's input already has a data
	* field named like m_strOutputFieldName, execution will be terminated right away.
	*/
	void SetOutputFieldName(const std::string& strFieldName);
	std::string GetOutputFieldName() const;

protected:
	vtkThresholdSelection();
	virtual ~vtkThresholdSelection();

	/**
	* this is where the real work is done.
	*/
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

private:
	double m_fThresholdRange[2];
	std::string m_strOutputFieldName;
};


#endif //#ifdef _vtkThresholdSelection_H

