/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVETETGRIDPOINTLOCATOR_H
#define _VVETETGRIDPOINTLOCATOR_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaBase/VistaVectorMath.h>
#include <list>
#include <ostream>
#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTetGrid;

/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/**
 * VveTetGridPointLocator provides information and interfaces for locating
 * points within the referenced tetrahedral grid.
 * NOTE: For memory allocation and deallocation for setting the kd-tree and/or 
 *       the incident cells use the Allocate*Array() and DestroyArray() methods 
 *       from VveTetGrid!
 */
class VISTAVISEXTAPI VveTetGridPointLocator
{
public:
	VveTetGridPointLocator();
	virtual ~VveTetGridPointLocator();

	void Reset();

	void SetTetGrid(VveTetGrid *pGrid);
	VveTetGrid *GetTetGrid() const;

	void ComputeInformation();

	int GetCellId(const float *pPos) const;
	int GetCellId(const float *pPos, std::list<int> &liTreeSteps, std::list<int> &liTetWalkSteps) const;

	int GetCellId(const float *pPos, int iMaxSteps) const;
	int GetCellId(const float *pPos, std::list<int> &liTreeSteps, std::list<int> &liTetWalkSteps, int iMaxSteps) const;

	int KdTreeSearch(const float *pPos, std::list<int> &liSteps) const;

	int TetWalk(int iStartCell, const float *pPos, float aBC[4]) const;
	int TetWalk(int iStartCell, const float *pPos, float aBC[4], std::list<int> &liSteps) const;

	int TetWalkLimit(int iStartCell, const float *pPos, float aBC[4], int iMaxSteps) const;
	int TetWalkLimit(int iStartCell, const float *pPos, float aBC[4], std::list<int> &liSteps, int iMaxSteps) const;

	void SetKdTree(int *pTree);
	int *GetKdTree() const;

	void SetTreeRoot(int iRoot);
	int GetTreeRoot() const;

	void SetIncidentCells(int *pCells);
	int *GetIncidentCells() const;

	int GetKdTreeDepth() const;

	void Dump(std::ostream &os) const;

	enum
	{
		SA_X = 0,
		SA_Y,
		SA_Z,
		SA_LAST
	};

protected:
	VveTetGrid				*m_pGrid;
	int						*m_pKdTree;
	int						*m_pIncidentCells;
	int						m_iTreeRoot;
};


#endif // _VveTetGridPointLocator_H

/*============================================================================*/
/*  END OF FILE                                                               */
/*============================================================================*/

