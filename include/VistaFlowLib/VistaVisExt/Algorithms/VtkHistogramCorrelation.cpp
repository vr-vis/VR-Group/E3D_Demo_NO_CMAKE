/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VtkHistogramCorrelation.h"

#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkCharArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkImageData.h>
#include <vtkStreamingDemandDrivenPipeline.h>

#include <VistaBase/VistaStreamUtils.h>

#include <cassert>
#include <algorithm>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
vtkCxxRevisionMacro(vtkHistogramCorrelation, "$Id$");
vtkStandardNewMacro(vtkHistogramCorrelation);
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
vtkHistogramCorrelation::vtkHistogramCorrelation() 
	:	m_bGenerateVertices(true)
{
	this->SetNumberOfInputPorts(2);
	//this->SetNumberOfOutputPorts(1);
	m_fTargetBounds[0] = m_fTargetBounds[2] = m_fTargetBounds[4] = 0.0f;
	m_fTargetBounds[1] = m_fTargetBounds[3] = m_fTargetBounds[5] = 1.0f;
}
//------------------------------------------------------------
//------------------------------------------------------------
vtkHistogramCorrelation::~vtkHistogramCorrelation()
{
}
/*============================================================================*/
/*  IMPLEMENTATION															  */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
void vtkHistogramCorrelation::PrintSelf(ostream& os, vtkIndent indent)
{
	vtkPolyDataAlgorithm::PrintSelf(os, indent);
	os << indent << "GenerateVertices : " << (m_bGenerateVertices ? "TRUE" : "FALSE") << endl;
}

//------------------------------------------------------------
//------------------------------------------------------------
void vtkHistogramCorrelation::SetTargetBounds(float fBounds[6])
{
	memcpy(m_fTargetBounds, fBounds, 6*sizeof(float));
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkHistogramCorrelation::GetTargetBounds(float fBounds[6]) const
{
	memcpy(fBounds, m_fTargetBounds, 6*sizeof(float));
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtkHistogramCorrelation::RequestUpdateExtent (
  vtkInformation* vtkNotUsed(request),
  vtkInformationVector** inputVector,
  vtkInformationVector* vtkNotUsed( outputVector ))
{
	for(int i=0; i<2; ++i)
	{
		// get the info objects
		vtkInformation *inInfo = inputVector[i]->GetInformationObject(0);
		//request the entire input at once!
		int inExt[6];
		inInfo->Get(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(),inExt);
		inInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_EXTENT(),inExt,6);
	}
	return 1;
}

//------------------------------------------------------------
//------------------------------------------------------------
int vtkHistogramCorrelation::RequestData(vtkInformation *, 
									 vtkInformationVector **inputVector, 
									 vtkInformationVector *outputVector)
{
	// get the info objects
	//our input is the first input on the first port of this filter
	vtkInformation *inInfo1 = inputVector[0]->GetInformationObject(0);
	vtkInformation *inInfo2 = inputVector[1]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkImageData *pInput1 = vtkImageData::SafeDownCast(inInfo1->Get(vtkDataObject::DATA_OBJECT()));
	vtkImageData *pInput2 = vtkImageData::SafeDownCast(inInfo2->Get(vtkDataObject::DATA_OBJECT()));
	vtkPolyData  *pOutput = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pOutput)
	{
		vstr::errp() << "[vtkHistogramCorrelation::RequestData] no output!" << endl;
		return -1;
	}
	if(!pInput1 || !pInput2)
	{
		vstr::errp() << "[vtkHistogramCorrelation::RequestData] invalid/incomplete input!" << endl;
		return -1;
	}
	//check input dimensions compatibility
	int iDims1[3], iDims2[3];
	pInput1->GetDimensions(iDims1);
	pInput2->GetDimensions(iDims2);
	if(iDims1[0] != iDims2[0] || iDims1[1] != iDims2[1] || iDims1[2] != iDims2[2])
	{
		vstr::errp() << "[vtkHistogramCorrelation::RequestData] incompatible input!" << endl;
		return -1;
	}
	if(pInput1->GetScalarType() != VTK_FLOAT || pInput2->GetScalarType() != VTK_FLOAT)
	{
		vstr::errp() << "[vtkHistogramCorrelation::RequestData] currently only supporting float scalars!" << endl;
		return -1;
	}

	const int iNumPts = pInput1->GetNumberOfPoints();
	

	//allocate points object (2 points per grid coordinate)
	vtkPoints *pPts = vtkPoints::New();
	pPts->Allocate(2* iNumPts);

	vtkCellArray *pLines = vtkCellArray::New();
	pLines->Allocate(3*iNumPts);

	vtkCellArray *pVerts = vtkCellArray::New();
	pVerts->Allocate(4*iNumPts);

	vtkFloatArray *pOVals = vtkFloatArray::New();
	pOVals->SetNumberOfComponents(1);
	pOVals->Allocate(2*iNumPts);
	pOVals->SetName(pInput1->GetPointData()->GetScalars()->GetName());

	vtkCharArray *pDirs = vtkCharArray::New();
	pDirs->SetNumberOfComponents(1);
	pDirs->Allocate(2*iNumPts);
	pDirs->SetName("direction");

	//this will be cell data!
	vtkFloatArray *pVertDiffs = vtkFloatArray::New();
	pVertDiffs->SetNumberOfComponents(1);
	pVertDiffs->Allocate(2*iNumPts);
	
	vtkFloatArray *pLineDiffs = vtkFloatArray::New();
	pLineDiffs->SetNumberOfComponents(1);
	pLineDiffs->Allocate(1*iNumPts);

	const float *pVal1 = static_cast<float*>(pInput1->GetScalarPointer());
	const float *pVal2 = static_cast<float*>(pInput2->GetScalarPointer());

	double dImageBounds[6];
	pInput1->GetBounds(dImageBounds);
	double dInput1Range[2];
	pInput1->GetScalarRange(dInput1Range);
	double dInput2Range[2];
	pInput2->GetScalarRange(dInput2Range);
	
	double dDataRange[2] = {std::min(dInput1Range[0], dInput2Range[0]),
							std::max(dInput1Range[1], dInput2Range[1])  };
	double dRescale[3];
	//x and y axis are just scaled to the target bounds
	dRescale[0] = (m_fTargetBounds[1] - m_fTargetBounds[0]) / (dImageBounds[1] - dImageBounds[0]) ;
	dRescale[1] = (m_fTargetBounds[3] - m_fTargetBounds[2]) / (dImageBounds[3] - dImageBounds[2]) ;
	//z axis needs scaling according to maximum count range
	dRescale[2] = 1.0f / (dDataRange[1] - dDataRange[0]) ;
	double dZRange = m_fTargetBounds[5] - m_fTargetBounds[4];

	double dPt[3];
	vtkIdType pts[2];
	int iPtCount = 0;
	double dDiff;
	double dMaxD = 0;
	double dMin = 0;
	double dMax = 0;
	double d1, d2;
	double dZ = 0;

	for(int i=0; i<iNumPts; ++i)
	{
		//create vis only for points where at least one data item is available
		if(pVal1[i] == 0 && pVal2[i] == 0)
			continue;

		//compute scaled pt z coords
		d1 = (pVal1[i]-dDataRange[0]) * dRescale[2]; //rescale data to [0..1]
		d2 = (pVal2[i]-dDataRange[0]) * dRescale[2];
		//determine correlation
		//dZ = std::min<double>(d1, d2) / std::max<double>(d1, d2);
		dZ = 1.0 - (d1-d2)*(d1-d2);

		//create first point
		pInput1->GetPoint(i, dPt);
		//rescale to target bounds
		dPt[0] = m_fTargetBounds[0] + (dPt[0] - dImageBounds[0]) * dRescale[0];
		dPt[1] = m_fTargetBounds[2] + (dPt[1] - dImageBounds[2]) * dRescale[1];
		//dPt[2] = m_fTargetBounds[4] + (pVal1[i] - dDataRange[0])* dRescale[2];
		dPt[2] = m_fTargetBounds[4];
		pPts->InsertNextPoint(dPt);
		
		//create second point
		//dPt[2] = m_fTargetBounds[4] + (pVal2[i] - dDataRange[0])* dRescale[2];
		dPt[2] = m_fTargetBounds[4] + dZ * dZRange;
		iPtCount = pPts->InsertNextPoint(dPt);
		
		//original values are passed for reference
		pOVals->InsertNextTuple(&(pVal1[i]));
		pOVals->InsertNextTuple(&(pVal2[i]));
		//1 is assigned to the guy "on top"
		dDiff = (pVal1[i] > pVal2[i] ? 1 : 0);
		pDirs->InsertNextTuple(&dDiff); 
		dDiff = 1-dDiff;
		pDirs->InsertNextTuple(&dDiff); 

		//create line entry
		pts[0] = iPtCount-1;
		pts[1] = iPtCount;
		pLines->InsertNextCell(2, pts);

		//create vertices
		pVerts->InsertNextCell(1, &(pts[0]));
		pVerts->InsertNextCell(1, &(pts[1]));

		//the lines and verts are labeled with the differnce
		dDiff = (d1 - d2);
		dDiff *= dDiff;
		dMaxD = std::max<double>(dDiff, dMaxD);
		pVertDiffs->InsertNextTuple(&dDiff);
		pVertDiffs->InsertNextTuple(&dDiff);
		pLineDiffs->InsertNextTuple(&dDiff);
	}

	//assert that cell data and point data fit the counts
	assert(pPts->GetNumberOfPoints() == pOVals->GetNumberOfTuples());
	assert(pPts->GetNumberOfPoints() == pDirs->GetNumberOfTuples());
	assert(pVertDiffs->GetNumberOfTuples() == pVerts->GetNumberOfCells());
	assert(pLineDiffs->GetNumberOfTuples() == pLines->GetNumberOfCells());

	//aggregate cell data
	vtkFloatArray *pDiffs = vtkFloatArray::New();
	pDiffs->SetNumberOfComponents(1);
	pDiffs->SetNumberOfTuples(pLines->GetNumberOfCells() + pVerts->GetNumberOfCells());
	pDiffs->SetName("difference");
	float *pPtr = pDiffs->GetPointer(0);
	memcpy(&(pPtr[0]), pVertDiffs->GetPointer(0), pVerts->GetNumberOfCells() * sizeof(float));
	memcpy(&(pPtr[pVerts->GetNumberOfCells()]), pLineDiffs->GetPointer(0), pLines->GetNumberOfCells() * sizeof(float));
	pVertDiffs->Delete();
	pLineDiffs->Delete();

	//normalize output diff
	dMaxD = 1.0 / dMaxD;
	for(int i=0; i<pDiffs->GetNumberOfTuples(); ++i)
	{
		pPtr[i] *= dMaxD;
	}

	//assemble output
	pOutput->SetPoints(pPts);
	pOutput->SetVerts(pVerts);
	pOutput->SetLines(pLines);
	pOutput->GetPointData()->SetScalars(pDirs);
	pOutput->GetPointData()->AddArray(pOVals);
	pOutput->GetCellData()->AddArray(pDiffs);
	
	//cleanup
	pVerts->Squeeze();
	pVerts->Delete();
	
	pLines->Squeeze();
	pLines->Delete();

	pDiffs->Squeeze();
	pDiffs->Delete();
	pDirs->Squeeze();
	pDirs->Delete();
	pOVals->Squeeze();
	pOVals->Delete();

	return 1;
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtkHistogramCorrelation::FillInputPortInformation(int port, vtkInformation *info)
{
	//all right -> both input ports need a single image data
	info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkImageData");
	return 1;
}
//------------------------------------------------------------
//------------------------------------------------------------


