/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VveTetGridPointLocator.h"
#include <VistaVisExt/Data/VveTetGrid.h>

#include <VistaBase/VistaTimer.h>
#include <VistaBase/VistaStreamUtils.h>

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

#define MAX_TET_WALK_STEPS 20


static inline float GetSquaredDist(const float p1[3], const float p2[3]);
static int ComputeMaxDepth(int *pTree, int iCurrent, int iDepth);

struct VveTetGridPointLocator_KdTreeHelper
{
	VveTetGridPointLocator_KdTreeHelper(int iNewId, float *pNewPos)
		: iId(iNewId), pPos(pNewPos) {};
	int iId;
	float *pPos;
};

static inline int BuildKdTree(int *pTree, 
							  list<VveTetGridPointLocator_KdTreeHelper> &liNodes,
							  int iAxis);

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*                                                                            */
/*============================================================================*/
VveTetGridPointLocator::VveTetGridPointLocator()
: m_pGrid(NULL),
  m_pKdTree(NULL),
  m_pIncidentCells(NULL),
  m_iTreeRoot(-1)
{
}

VveTetGridPointLocator::~VveTetGridPointLocator()
{
	Reset();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Reset                                                       */
/*                                                                            */
/*============================================================================*/
void VveTetGridPointLocator::Reset()
{
	VveTetGrid::DestroyArray(m_pKdTree);
	m_pKdTree = NULL;
	VveTetGrid::DestroyArray(m_pIncidentCells);
	m_pIncidentCells = NULL;
	m_iTreeRoot = -1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTetGrid                                              */
/*                                                                            */
/*============================================================================*/
void VveTetGridPointLocator::SetTetGrid(VveTetGrid *pGrid)
{
	m_pGrid = pGrid;
}

VveTetGrid *VveTetGridPointLocator::GetTetGrid() const
{
	return m_pGrid;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeInformation                                          */
/*                                                                            */
/*============================================================================*/
void VveTetGridPointLocator::ComputeInformation()
{
	VistaTimer oTimer;

	vstr::outi() << " [VveTetGridPointLocator] - computing incident cells..." << endl;
	int iCount = m_pGrid->GetVertexCount();
	VveTetGrid::DestroyArray(m_pIncidentCells);
	m_pIncidentCells = VveTetGrid::NewIntArray(iCount);
	for (int i=0; i<iCount; ++i)
		m_pIncidentCells[i] = -1;
	int *pCells = m_pGrid->GetCells();
	int iCellCount = m_pGrid->GetCellCount();
	for (int i=0; i<iCellCount; ++i)
	{
		for (int j=0; j<4; ++j)
		{
			if (pCells[4*i+j]<0)
				continue;
			m_pIncidentCells[pCells[4*i+j]] = i;
		}
	}
	vstr::outi() << " [VveTetGridPointLocator] - incident cells finished..." << endl;
	vstr::outi() << "                            [time needed: " 
		<< oTimer.GetLifeTime() << "]" << endl;

	double dTimeStamp = oTimer.GetLifeTime();
	vstr::outi() << " [VveTetGridPointLocator] - building kd-tree..." << endl;

	// copy all vertices into a helper structure
	list<VveTetGridPointLocator_KdTreeHelper> liTemp;
	float *pVertices = m_pGrid->GetVertices();

	float *pPos = pVertices;
	for (int i=0; i<iCount; ++i, pPos+=3)
	{
		if (m_pIncidentCells[i] >= 0)
			liTemp.push_back(VveTetGridPointLocator_KdTreeHelper(i, pPos));
	}

	VveTetGrid::DestroyArray(m_pKdTree);
	m_pKdTree = VveTetGrid::NewIntArray(2*iCount);
	for (int i=0; i<2*iCount; ++i)
		m_pKdTree[i] = -1;

	m_iTreeRoot = BuildKdTree(m_pKdTree, liTemp, SA_X);

	vstr::outi() << " [VveTetGridPointLocator] - kd-tree finished..." << endl;
	vstr::outi() << "                            [time needed: " << oTimer.GetLifeTime()-dTimeStamp << "]" << endl;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCellId                                                   */
/*                                                                            */
/*============================================================================*/
int VveTetGridPointLocator::GetCellId(const float *pPos) const
{
	if (m_iTreeRoot < 0)
		return -1;

	// find fairly close point to our given position
	int iClosestId = -1;
	int iCurrentId = m_iTreeRoot;
	float fDist;
	float fMinDist = numeric_limits<float>::max();
	float *pVertices = m_pGrid->GetVertices();
	int iAxis = SA_X;

	while (iCurrentId >= 0)
	{
		fDist = GetSquaredDist(pPos, &pVertices[iCurrentId*3]);
		if (fDist < fMinDist)
		{
			fMinDist = fDist;
			iClosestId = iCurrentId;
		}

		if (pPos[iAxis] > pVertices[iCurrentId*3+iAxis])
			iCurrentId = m_pKdTree[2*iCurrentId+1];
		else
			iCurrentId = m_pKdTree[2*iCurrentId];

		iAxis = (iAxis+1)%3;
	}

	float aDummy[4];
	return TetWalk(m_pIncidentCells[iClosestId], pPos, aDummy);
}

int VveTetGridPointLocator::GetCellId(const float *pPos, 
									   std::list<int> &liTreeSteps, 
									   std::list<int> &liTetWalkSteps) const
{
	int iClosestId = KdTreeSearch(pPos, liTreeSteps);

	if (iClosestId < 0)
		return -1;

	float aDummy[4];
	return TetWalk(m_pIncidentCells[iClosestId], pPos, aDummy, liTetWalkSteps);
}

int VveTetGridPointLocator::GetCellId(const float *pPos, int iMaxSteps) const
{
	if (m_iTreeRoot < 0)
		return -1;

	// find fairly close point to our given position
	int iClosestId = -1;
	int iCurrentId = m_iTreeRoot;
	float fDist;
	float fMinDist = numeric_limits<float>::max();
	float *pVertices = m_pGrid->GetVertices();
	int iAxis = SA_X;

	while (iCurrentId >= 0)
	{
		fDist = GetSquaredDist(pPos, &pVertices[iCurrentId*3]);
		if (fDist < fMinDist)
		{
			fMinDist = fDist;
			iClosestId = iCurrentId;
		}

		if (pPos[iAxis] > pVertices[iCurrentId*3+iAxis])
			iCurrentId = m_pKdTree[2*iCurrentId+1];
		else
			iCurrentId = m_pKdTree[2*iCurrentId];

		iAxis = (iAxis+1)%3;
	}

	float aDummy[4];
	return TetWalkLimit(m_pIncidentCells[iClosestId], pPos, aDummy, iMaxSteps);
}

int VveTetGridPointLocator::GetCellId(const float *pPos, 
									   std::list<int> &liTreeSteps, 
									   std::list<int> &liTetWalkSteps, 
									   int iMaxSteps) const
{
	int iClosestId = KdTreeSearch(pPos, liTreeSteps);

	if (iClosestId < 0)
		return -1;

	float aDummy[4];
	return TetWalkLimit(m_pIncidentCells[iClosestId], pPos, aDummy, liTetWalkSteps, iMaxSteps);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   KdTreeSearch                                                */
/*                                                                            */
/*============================================================================*/
int VveTetGridPointLocator::KdTreeSearch(const float *pPos, 
										  std::list<int> &liSteps) const
{
	if (m_iTreeRoot < 0)
		return -1;

	// find fairly close point to our given position
	int iClosestId = -1;
	int iCurrentId = m_iTreeRoot;
	float fDist;
	float fMinDist = numeric_limits<float>::max();
	float *pVertices = m_pGrid->GetVertices();
	int iAxis = SA_X;

	while (iCurrentId >= 0)
	{
		liSteps.push_back(iCurrentId);

		fDist = GetSquaredDist(pPos, &pVertices[iCurrentId*3]);
		if (fDist < fMinDist)
		{
			fMinDist = fDist;
			iClosestId = iCurrentId;
		}

		if (pPos[iAxis] > pVertices[iCurrentId*3+iAxis])
			iCurrentId = m_pKdTree[2*iCurrentId+1];
		else
			iCurrentId = m_pKdTree[2*iCurrentId];

		iAxis = (iAxis+1)%3;
	}

	return iClosestId;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   TetWalk                                                     */
/*                                                                            */
/*============================================================================*/
int VveTetGridPointLocator::TetWalk(int iStartCell, const float *pPos, 
									 float aBC[4]) const
{
	int *pCellNeighbors = m_pGrid->GetCellNeighbors();
	int iMinIndex;

	int iCount = MAX_TET_WALK_STEPS;

	while (iStartCell >= 0)
	{
		m_pGrid->ComputeBCs(aBC, pPos, iStartCell);

		iMinIndex = 0;
		for (int i=1; i<4; ++i)
		{
			if (aBC[i] < aBC[iMinIndex])
				iMinIndex = i;
		}

		if (aBC[iMinIndex] < -0.001f)
			iStartCell = pCellNeighbors[4*iStartCell+iMinIndex];
		else
			break;

		if (iCount<0)
		{
			iStartCell = -1;
			break;
		}
		--iCount;
	}

	return iStartCell;
}

int VveTetGridPointLocator::TetWalk(int iStartCell, 
									 const float *pPos, 
									 float aBC[4], 
									 std::list<int> &liSteps) const
{
	int *pCellNeighbors = m_pGrid->GetCellNeighbors();
	int iMinIndex;

	int iCount = MAX_TET_WALK_STEPS;

	while (iStartCell >= 0)
	{
		liSteps.push_back(iStartCell);

		m_pGrid->ComputeBCs(aBC, pPos, iStartCell);

		iMinIndex = 0;
		for (int i=1; i<4; ++i)
		{
			if (aBC[i] < aBC[iMinIndex])
				iMinIndex = i;
		}

		if (aBC[iMinIndex] < -0.001f)
			iStartCell = pCellNeighbors[4*iStartCell+iMinIndex];
		else
			break;

		if (iCount<0)
		{
			iStartCell = -1;
			break;
		}
		--iCount;
	}

	return iStartCell;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   TetWalkLimit                                                */
/*                                                                            */
/*============================================================================*/
int VveTetGridPointLocator::TetWalkLimit(int iStartCell, const float *pPos, 
										  float aBC[4], int iMaxSteps) const
{
	int *pCellNeighbors = m_pGrid->GetCellNeighbors();
	int iMinIndex;

	int iCount = iMaxSteps;
	int iLastCell = -1;
	int iTemp;

	while (iStartCell >= 0)
	{
		m_pGrid->ComputeBCs(aBC, pPos, iStartCell);

		iMinIndex = 0;
		for (int i=1; i<4; ++i)
		{
			if (aBC[i] < aBC[iMinIndex])
				iMinIndex = i;
		}

		iTemp=iStartCell;

		if (aBC[iMinIndex] < -0.001f)
			iStartCell = pCellNeighbors[4*iStartCell+iMinIndex];
		else
			break;

		if (iLastCell==iStartCell || iCount<0)
		{
			iStartCell = -1;
			break;
		}
		--iCount;
		iLastCell = iTemp;
	}

	return iStartCell;
}

int VveTetGridPointLocator::TetWalkLimit(int iStartCell, const float *pPos, float aBC[4], 
										  std::list<int> &liSteps, int iMaxSteps) const
{
	int *pCellNeighbors = m_pGrid->GetCellNeighbors();
	int iMinIndex;

	int iCount = iMaxSteps;
	int iLastCell = -1;
	int iTemp;

	while (iStartCell >= 0)
	{
		liSteps.push_back(iStartCell);

		m_pGrid->ComputeBCs(aBC, pPos, iStartCell);

		iMinIndex = 0;
		for (int i=1; i<4; ++i)
		{
			if (aBC[i] < aBC[iMinIndex])
				iMinIndex = i;
		}

		iTemp = iStartCell;
		if (aBC[iMinIndex] < -0.001f)
			iStartCell = pCellNeighbors[4*iStartCell+iMinIndex];
		else
			break;

		if (iLastCell==iStartCell || iCount<0)
		{
			iStartCell = -1;
			break;
		}
		--iCount;
		iLastCell = iTemp;
	}

	return iStartCell;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetKdTree                                               */
/*                                                                            */
/*============================================================================*/
void VveTetGridPointLocator::SetKdTree(int *pTree)
{
	m_pKdTree = pTree;
}

int *VveTetGridPointLocator::GetKdTree() const
{
	return m_pKdTree;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTreeRoot                                             */
/*                                                                            */
/*============================================================================*/
void VveTetGridPointLocator::SetTreeRoot(int iRoot)
{
	m_iTreeRoot = iRoot;
}

int VveTetGridPointLocator::GetTreeRoot() const
{
	return m_iTreeRoot;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetIncidentCells                                        */
/*                                                                            */
/*============================================================================*/
void VveTetGridPointLocator::SetIncidentCells(int *pCells)
{
	m_pIncidentCells = pCells;
}

int *VveTetGridPointLocator::GetIncidentCells() const
{
	return m_pIncidentCells;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetKdTreeDepth                                              */
/*                                                                            */
/*============================================================================*/
int VveTetGridPointLocator::GetKdTreeDepth() const
{
	return ComputeMaxDepth(m_pKdTree, m_iTreeRoot, 0);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Dump                                                        */
/*                                                                            */
/*============================================================================*/
void VveTetGridPointLocator::Dump(std::ostream &os) const
{
	if (m_iTreeRoot < 0)
	{
		os << " [VveTetGridPointLocator] - no valid information available..." << endl;
		return;
	}

	os << " [VveTetGridPointLocator] - kd-tree root node: " << m_iTreeRoot << endl;
	os << " [VveTetGridPointLocator] - kd-tree depth:     " << GetKdTreeDepth() << endl;
}


/*============================================================================*/
/* LOCAL FUNCTIONS                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSquaredDist                                              */
/*                                                                            */
/*============================================================================*/
static inline float GetSquaredDist(const float p1[3], const float p2[3])
{
	float aTemp[3];
	aTemp[0] = p2[0] - p1[0];
	aTemp[1] = p2[1] - p1[1];
	aTemp[2] = p2[2] - p1[2];
	return aTemp[0]*aTemp[0] + aTemp[1]*aTemp[1] + aTemp[2]*aTemp[2];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeMaxDepth                                             */
/*                                                                            */
/*============================================================================*/
static inline int ComputeMaxDepth(int *pTree, int iCurrent, int iDepth)
{
	++iDepth;

	int iTemp1=iDepth, iTemp2=iDepth;

	if (pTree[2*iCurrent] >= 0)
	{
		iTemp1 = ComputeMaxDepth(pTree, pTree[2*iCurrent], iDepth);
	}

	if (pTree[2*iCurrent+1] >= 0)
	{
		iTemp2 = ComputeMaxDepth(pTree, pTree[2*iCurrent+1], iDepth);
	}

	if (iTemp2>iTemp1)
		return iTemp2;
	else 
		return iTemp1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   BuildKdTree                                                 */
/*                                                                            */
/*============================================================================*/
static bool CompareHelper0(const VveTetGridPointLocator_KdTreeHelper &left,
						   const VveTetGridPointLocator_KdTreeHelper &right)
{
	if (left.pPos[0] < right.pPos[0])
		return true;
	else return false;
}

static bool CompareHelper1(const VveTetGridPointLocator_KdTreeHelper &left, 
						   const VveTetGridPointLocator_KdTreeHelper &right)
{
	if (left.pPos[1] < right.pPos[1])
		return true;
	else return false;
}

static bool CompareHelper2(const VveTetGridPointLocator_KdTreeHelper &left, 
						   const VveTetGridPointLocator_KdTreeHelper &right)
{
	if (left.pPos[2] < right.pPos[2])
		return true;
	else return false;
}

typedef bool (*helperfn)(const VveTetGridPointLocator_KdTreeHelper &, const VveTetGridPointLocator_KdTreeHelper &);

helperfn aVveTetGridPointLocator_SortingHelpers[] =
{
	&CompareHelper0, &CompareHelper1, &CompareHelper2
};

static int BuildKdTree(int *pTree,
					   list<VveTetGridPointLocator_KdTreeHelper> &liNodes,
					   int iAxis)
{
	size_t nSize = liNodes.size();

	if (nSize == 1)
	{
		int iCurrentId = (*liNodes.begin()).iId;
		pTree[2*iCurrentId] = -1;
		pTree[2*iCurrentId+1] = -1;

		return iCurrentId;
	}

	liNodes.sort(aVveTetGridPointLocator_SortingHelpers[iAxis]);

	size_t nMedian = (nSize - 1) / 2;

	list<VveTetGridPointLocator_KdTreeHelper>::iterator it = liNodes.begin();

	for (size_t i=0; i<nMedian; ++i)
		++it;

	float fCompVal = (*it).pPos[iAxis];
	list<VveTetGridPointLocator_KdTreeHelper>::iterator it2 = it;
	++it2;
	while (it2 != liNodes.end()
		&& (*it).pPos[iAxis] == (*it2).pPos[iAxis])
	{
		++it2;
		++it;
	}

	// create list for left child
	list<VveTetGridPointLocator_KdTreeHelper> liLeft;
	liLeft.splice(liLeft.begin(), liNodes, liNodes.begin(), it);

	VveTetGridPointLocator_KdTreeHelper &refCurrentHelper = liNodes.front();
	int iCurrentId = refCurrentHelper.iId;

	// remove front of list (i.e. vertex for current node)
	liNodes.pop_front();

	iAxis = (iAxis + 1) % VveTetGridPointLocator::SA_LAST;

	// process left sublist
	if (liLeft.empty())
		pTree[2*iCurrentId] = -1;
	else
		pTree[2*iCurrentId] = BuildKdTree(pTree, liLeft, iAxis);

	// process right sublist
	if (liNodes.empty())
		pTree[2*iCurrentId+1] = -1;
	else
		pTree[2*iCurrentId+1] = BuildKdTree(pTree, liNodes, iAxis);

	return iCurrentId;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
