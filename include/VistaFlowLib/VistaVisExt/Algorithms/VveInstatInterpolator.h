/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEINSTATINTERPOLATOR_H
#define _VVEINSTATINTERPOLATOR_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkFunctionSet.h>
#include <vector>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkGenericCell;
class vtkDataSet;
class vtkInterpolatedVelocityField;
class VveTimeMapper;
class vtkDataArray;
template<class T> class VveDiscreteDataTyped;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* VveInstatInterpolator is used to interpolate a piecewise
* linear 4D data set representation from a set of 3D samples.
*
* @author	Bernd Hentschel
* @date		September 2006
*/
class VISTAVISEXTAPI VveInstatInterpolator : public vtkFunctionSet
{
public:
	enum{
		INT_VECTORS = 0,
		INT_TENSORS = 1,
		INT_SCALARS = 2
	};

	VveInstatInterpolator();
	virtual ~VveInstatInterpolator();
	/**
	*	Time information for different time levels
	*/
	void SetData(VveDiscreteDataTyped<vtkDataSet> *pData);
	/*
	*	Evaluate vector field at spacetime position x
	*/
	virtual int FunctionValues(double* x, double* f);
	/**
	* Determine which data field will be used if no explicit 
	* field name is set. If iMode = INT_VECTORS interpolation
	* will operate on input data's vectors. If iMode =
	* INT_SCALARS the first scalar field will be targeted.
	* Per default this value is set to false i.e. scalar 
	* data is interpolated if no field data name is set.
	*/
	void SetDefaultInterpolationMode(int iMode);
	int GetDefaultInterpolationMode() const;
	/**
	* set the data field to be interpolated by its name
	*/
	void SetDataFieldName(const std::string& strFieldName);
	/**
	*
	*/
	std::string GetDataFieldName() const;
	/**
	* return the number of input parameters for interpolation
	* ==4 => 3 spatial + 1 time
	*/
	virtual int GetNumberOfIndependentVariables(){
		return 4;
	}
	/*
	* return the number of function values computed by this function
	* set. This varies depending on the specific data field which needs
	* to be interpolated.
	*/
	virtual int GetNumberOfFunctions();


	/**
	 * get the number of cells used during the last interpolation
	 * NOTE: For now this will always return 2. Make it a good habit
	 *		 to look for the value though, since the 2 might change
	 *		 once we adopt a higher order interpolation scheme
	 */
	int GetNumInterpolCells() const;
	
	/**
	 * retrieve the last cells used during the last interpolation 
	 * process
	 */
	void GetLastCells(	vtkIdType *pCells, vtkIdType *pTimeIdx) const;
	
protected:
	/**
	*	setup data sets, i.e. determine the two data sets needed for
	*	interpolation and save data in the corresponding member variables
	*/
	bool SetupDataSets(double x[4]);
	/*
	*	interpolate two probes, one for each of the two data sets set up before
	*	Save the result in m_arrInterpolResults.
	*	This is, where the cell search is done!
	*/
	bool InterpolateProbes(double x[4]);
private:
	/**
	* the data set time levels (enumerated by their level indices)
	*/
	VveDiscreteDataTyped<vtkDataSet> *m_pData;
	/**
	* keep the name of the attribute to be interpolated here
	*/
	std::string m_strDataFieldName;
	/**
	* this time mapper allows for the identification of the two distinct
	* time levels which are used for any given time dependent linear interpolation
	*/
	VveTimeMapper *m_pMapper;
	/*
	*	Current indices used for time level data access
	*/
	int  m_arrTimeIndices[2];
	/**
	* simulation times for current interpolation data sets
	*/
	double m_dTimeIndexSimTimes[2];
	/*
	* indicate whether the time index has changed
	*/
	bool m_bTimeChanged;
	/**
	*	hold the two data sets which are used for interpolation
	*	These are stored in chronological order, i.e. 
	*	t(m_pActiveDataSets[0]) < t(m_pActiveDataSets[1]).
	*/
	vtkDataSet* m_pActiveDataSets[2];
	/**
	* keep a pointer on the active data arrays in order to 
	* prevent permanent re-retrieval
	*/
	vtkDataArray* m_pActiveArrays[2];
	/*
	*	last active cells in data sets
	*/
	vtkGenericCell* m_pActiveCells[2];
	/**
	*	save the indices of the last known cells in order to 
	*	enable a nearest neighbor search for successive interpolation
	*	requests.
	*/
	int m_iLastCell[2];
	/*
	* tmp variable used for storing the single evaluation results for
	* each of the two data sets
	* NOTE: We store 9 component arrays here since VTK allows at most
	* 9 components per data array (tensors). So this should do the trick for
	* all VTK related point data attributes.
	*/
	double m_arrInterpolResults[2][9];
	/**
	*
	*/
	bool m_bInterpolateVectors;
	bool m_bInterpolateTensors;
};

#endif //_VVEINSTATINTERPOLATOR_H
