/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  Vtk2DHistogramToImageData.h  RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA VisExt                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - Vtk2DHistogramToImageData    :                                    */
/*                                                                            */
/*============================================================================*/
#ifndef _VTK2DHISTOGRAMTOIMAGEDATA_H
#define _VTK2DHISTOGRAMTOIMAGEDATA_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkImageAlgorithm.h>
#include <string>
#include <ostream>
#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class Vve2DHistogram;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Given a 2D histogram (Vve2DHistogram) create a vtkImageData which can then
 * be used in any vtk pipeline downstream for visualization.
 * Particular uses include the use as texture (input to vtkTexture) and the use
 * as height map (use vtkGeometryFilter and vtkWarpScalar).
 */
class VISTAVISEXTAPI vtk2DHistogramToImageData : public vtkImageAlgorithm
{
public:
	vtkTypeRevisionMacro(vtk2DHistogramToImageData,vtkImageAlgorithm);
	void PrintSelf(std::ostream& os, vtkIndent indent);

	static vtk2DHistogramToImageData *New();

	/**
	 * Set the bounds of the target image data
	 * NOTE: the x and y coords will be used to determine the 
	 *       bounds of the plane, wheras the counts for each plane
	 *       pixel will be normalized to the z-coords' range (which is convenient
	 *		 if you want to do height plots)
	 */
	void SetTargetBounds(float fBounds[6]);
	void GetTargetBounds(float fBounds[6]) const;

	/**
	* Get/Set the name of the output data field (either point or cell data)
	* the default name is <count>
	*/
	void SetOutputFieldName(const std::string& strFieldName);
	std::string GetOutputFieldName() const;

	/**
	 *
	 */
	void SetHistogram(Vve2DHistogram *pHistogram);
	Vve2DHistogram *GetHistogram() const;

protected:
	vtk2DHistogramToImageData();
	virtual ~vtk2DHistogramToImageData();

	/**
	* this is where the real work is done.
	*/
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
	/**
	*
	*/
	virtual int FillInputPortInformation(int port, vtkInformation *info);
	/**
	 *
	 */
	int RequestInformation(vtkInformation* vtkNotUsed(request),
						   vtkInformationVector** inputVector,
						   vtkInformationVector* outputVector);
	/**
	 *
	 */
	void RecomputeTargetDims(bool bResChanged);

private:
	std::string m_strOutputFieldName;
	/**
	 *
	 */
	float m_fBounds[6];
	/**
	 *
	 */
	Vve2DHistogram *m_pHistogram;
};


#endif //#ifdef _VTK2DHISTOGRAMTOIMAGEDATA_H

