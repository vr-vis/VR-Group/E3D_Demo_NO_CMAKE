/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VTKINTERPOLSCALARFIELD_H
#define _VTKINTERPOLSCALARFIELD_H

/*============================================================================*/
// INCLUDES
/*============================================================================*/
#include <vtkFunctionSet.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
// MAKROS / DEFINES
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkDataSet;
class vtkGenericCell;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
class VISTAVISEXTAPI VtkInterpolatedScalarField : public vtkFunctionSet
{
protected:
	VtkInterpolatedScalarField();
	virtual ~VtkInterpolatedScalarField();
public:
	vtkTypeMacro(VtkInterpolatedScalarField,vtkFunctionSet);

	static VtkInterpolatedScalarField* New();

	void SetDataSet(vtkDataSet* pDataSet);
	vtkDataSet* GetDataSet();

	/**
	*	Evaluate scalar field at x and return value in f[0]
	*/
	virtual int FunctionValues(double* x, double* f);

	void ClearLastCellId(){
		m_iLastCellId = -1;
	}

	/**
	* Getter / Setter for caching
	*/
	void EnableCaching(){
		m_bCaching = true;
	}
	void DisableCaching(){
		m_bCaching = false;
	}
	bool IsCachingEnabled(){
		return m_bCaching;
	}

protected:
	vtkDataSet*		m_pDataSet;
	
	vtkGenericCell* m_pLastCell;
	vtkGenericCell* m_pDummyCell;

	vtkIdType m_iLastCellId;

	double* m_arrfWeights;
	double  m_arrfLastParamCoords[3];

	bool m_bCaching;
};

/**
*	INLINES
*/
inline vtkDataSet* VtkInterpolatedScalarField::GetDataSet(){
	return m_pDataSet;
}

#endif // Include guard.
