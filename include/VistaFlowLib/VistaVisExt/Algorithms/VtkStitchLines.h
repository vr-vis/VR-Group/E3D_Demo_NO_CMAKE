/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VTKSTITCHLINES_H
#define _VTKSTITCHLINES_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include<vtkPolyDataAlgorithm.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkIdList;
class vtkPoints;
class vtkPointData;
class vtkCellArray;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Stitch together polylines with common endpoints. These usually
* result from particle tracing, e.g. when traces are done
* forward and backward in time or when doing incremental PT.
* Usage: Provide the joined input to this filter, i.e. the lines
* that you want to have stitched need to be put into one
* single vtkPolyData object. The filter will then internally
* clean the input via vtkCleanPolyData and subsequently stitch
* all lines who have any two common endpoints.
* NOTE that any inbound CELL DATA will be LOST!  
* --> NOTE: for general vtkStitchLines: there must be a mechanismus to choose 
*           and set cell data (temp: "ChooseCurrentTermReason")
* 
* @author   Bernd Hentschel
* @date     March, 2007
*
*/
class VISTAVISEXTAPI vtkStitchLines : public vtkPolyDataAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkStitchLines,vtkPolyDataAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

	static vtkStitchLines *New();

protected:
	vtkStitchLines();
	virtual ~vtkStitchLines();

	/**
	 * 
	 */
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
	/**
	 * append line1 to line2
	 * assumes correct temporal order is given!
	 */
	void StitchLines(	vtkIdType npts1, vtkIdType *pts1, 
						vtkIdType npts2, vtkIdType *pts2,
						vtkCellArray *pOutLines	);

	/**
	 * helper function for reversing lines to
	 * get them in the right temporal order
	 */
	void ReverseLine(vtkIdType npts, vtkIdType *pts);
	
private:
	vtkStitchLines(const vtkStitchLines&);  // Not implemented.
	void operator=(const vtkStitchLines&);  // Not implemented.
	double ChooseCurrentTermReason(double dTermReason1, double dTermReason2);
};

#endif //_VTKSTITCHLINES_H


