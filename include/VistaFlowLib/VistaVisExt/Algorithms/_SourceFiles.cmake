

set( RelativeDir "./Algorithms" )
set( RelativeSourceGroup "Source Files\\Algorithms" )
set( SubDirs Particles Vortices )

set( DirFiles
	Vtk2DHistogramToImageData.cpp
	Vtk2DHistogramToImageData.h
	VtkCleanUnstructuredGrid.cpp
	VtkCleanUnstructuredGrid.h
	VtkComputeFTLEFromFlowMap.cpp
	VtkComputeFTLEFromFlowMap.h
	VtkComputeLambda2.cpp
	VtkComputeLambda2.h
	VtkComputeShearRate.cpp
	VtkComputeShearRate.h
	VtkDataSetToPolyDataVertices.cpp
	VtkDataSetToPolyDataVertices.h
	VtkFieldDataToCoordsFilter.cpp
	VtkFieldDataToCoordsFilter.h
	VtkHistogramCorrelation.cpp
	VtkHistogramCorrelation.h
	VtkInterpolatedScalarField.cpp
	VtkInterpolatedScalarField.h
	VtkLEBisectRemeshing.cpp
	VtkLEBisectRemeshing.h
	VtkLineDecimator.cpp
	VtkLineDecimator.h
	VtkMultiFieldThresholdFilter.cpp
	VtkMultiFieldThresholdFilter.h
#	VtkRegionGrowingFilter.cpp
#	VtkRegionGrowingFilter.h
	VtkRescaleData.cpp
	VtkRescaleData.h
	VtkSegmentSurface.cpp
	VtkSegmentSurface.h
	VtkSpatialHistogram.cpp
	VtkSpatialHistogram.h
	VtkStitchLines.cpp
	VtkStitchLines.h
	VtkThresholdSelection.cpp
	VtkThresholdSelection.h
	VveInstatInterpolator.cpp
	VveInstatInterpolator.h
	VvePathlineTracer.cpp
	VvePathlineTracer.h
	VveTetGridPointLocator.cpp
	VveTetGridPointLocator.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()
