/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	vtkSegmentSurface.h			 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VTKSEGMENTSURFACE_H
#define VTKSEGMENTSURFACE_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/
class vtkInformation;
class vtkInformationVector;
class vtkPolyData;
class vtkDataArray;
class vtkIdList;

/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include <VistaVisExt/VistaVisExtConfig.h>

#include <vtkPolyDataAlgorithm.h>

#include <string>
#include <vector>
#include <set>

/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 * vtkSegmentSurface is a straightforward implementation of the watershed
 * method on an arbitrary vtkPolyData. It can be used to segment a surface
 * into a set of contiguous basins based on a given scalar field. The algorithm
 * is conceptually based on:
 *
 * A. P. Mangan and R. Withaker: Partitioning 3D Surface Meshes Using Watershed 
 * Segmentation, IEEE TVCG 5(4):308--321, 1999.
 *
 * NOTE, however, that no explicit handling of flat areas (plateaus) has been 
 * implemented yet.
 */
class VISTAVISEXTAPI vtkSegmentSurface : public vtkPolyDataAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkSegmentSurface,vtkPolyDataAlgorithm);

	static vtkSegmentSurface *New();

	void PrintSelf(ostream& os, vtkIndent indent);

	
	void SetScalarsToSegment(const char *pScalars);
	const char *GetScalarsToSegment() const;

	void SetIncludeMinimaInOutput(bool bInclude);
	bool GetIncludeMinimaInOutput() const;

	double GetRegionsMergeThreshold() const;
	void SetRegionsMergeThreshold(const double val);

protected:
	class Region;
	class Edge;
	class RegionGraph;

	vtkSegmentSurface();
	virtual ~vtkSegmentSurface();

	/**
	 * segment the surface according to the ridge lines of the given
	 * scalar field
	 */
	virtual int RequestData(vtkInformation* request,
							vtkInformationVector** inputVector,
							vtkInformationVector* outputVector);

	/**
	 * Determine the ids of all points which feature a min value within their
	 * 1-ring neighborhood
	 */
	void FindLocalMinima(vtkPolyData *pInput, vtkDataArray *pScalars, vtkIdList *pMinima) const;

	/**
	 * Check if a point has only scalar values >= its own value in its
	 * 1-ring neighborhood
	 */
	bool IsLocalMinimum(size_t iPt, vtkPolyData *pInput, vtkDataArray *pScalars) const;

	/**
	 * Create a scalar int array for labeling points with region ids
	 */
	vtkDataArray *CreatePointLabels(vtkPolyData *pData);

	/**
	 * Init all labels according to the given minima:
	 * - A local minimum will get a unique int identifier
	 * - All non-minimal points will be given NOT_YET_VISITED
	 */
	void InitPointLabels(const vtkIdType iNumPts, vtkIdList *pMinima, vtkDataArray *pLabels);

	/**
	 * Starting at the given point, follow the (discrete) gradient downhill, i.e. always
	 * move to the neighbor within the 1-ring neighborhood with the lowest value until
	 * a labeled point or a local minimum is hit (which is labeled by default)
	 */
	vtkIdType DescendFromPoint(vtkIdType iStartPt, vtkPolyData *pData, vtkDataArray *pScalars, vtkDataArray *pLabels, vtkIdList *pTrail);

	/**
	 * Merge all regions which are separated by only a shallow pass whose difference from
	 * both minima is less than m_dRegionsMergeThreshold.
	 */
	void MergeRegions(vtkPolyData *pData, vtkDataArray *pScalars, vtkDataArray *pLabels, vtkIdList *pMinima);

	/**
	 * Build meta data structures for all regions (which points they contain) and borders (which
	 * edges cross region borders)
	 */
	void ComputeRegionGraph(vtkPolyData *pData, vtkDataArray *pLabels, vtkDataArray *pScalars, 
						    RegionGraph &rRegionGraph);
	
	/**
	 * Determine whether or not a given point is a border vertex and for each border edge
	 * found there - i.e. an edge to a neighbor vertex with a different region label - 
	 * note that edge in the border set.
	 */
	void ComputeEdgesAtPoint(vtkIdType iPt, vtkPolyData *pData, 
							 vtkDataArray *pLabels, vtkDataArray *pScalars, 
							 RegionGraph &rRegionGraph);
	/**
	 * 
	 */
	void Compute1Ring(vtkIdType iPt, vtkPolyData *pData, vtkIdList *pNeighbors) const;

private:
	std::string m_strScalars;
	bool m_bIncludeMinimaInOutput;
	double m_dRegionsMergeThreshold;

	enum{
		UNKNOWN_REGION = -2,
		NOT_YET_VISITED = -1,
		FIRST_MIN = 0
	};

	/** helper variables -- no need in re-creating these time and again */
	vtkIdList *m_p1RingNeighCells;
	vtkIdList *m_p1RingNeighPts;
	vtkIdList *m_p1RingNeighborhood;
};


#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/