/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkLEBisectRemeshing.h       RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA VtkExt                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - vtkLEBisectRemeshing    :                                         */
/*                                                                            */
/*============================================================================*/
#ifndef _VTKLEBISECTREMESHING_H
#define _VTKLEBISECTREMESHING_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkPolyDataAlgorithm.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkIdList;
class vtkPolyDataWriter;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* vtkLEBisectRemeshing implements a very simple remeshing strategy used to
* create a "nice" triangulation from a given input mesh. This is done as follows:
* The algorithm will check every triangle in the given input mesh and determine
* its longest edge E. If ||E|| is greater than a user-specified threshold, the edge will be
* split into half, replacing the current triangle by two smaller ones. Moreover, if there is
* an adjacent triangle along E, this will be split into half, too. For each triangle split, 
* the algorithm then checks, if the minimum inner angle of triangles in the neighborhood of the
* current triangle(s) can be increased by flipping one of the outer edges. If so, edge flips are#
* performed as appropriate.
* The entire procedure is iterated until either all edges are shorter than the MaximumEdgeLength
* threshold value or the given maximum number of splits is reached.
*
* NOTE: Along with the basic mesh operators defined in CvtkPolyDataTools, this serves as a hands
* on example on how to "mesh" with VTK. I hope I got most of it right by now...
*
* @todo As you can see from the algorithm desription, this is not quite a remeshing tool right now, but
* more of a "triangle splitter". In order to implement a full fledged remesher, we need two more components
* First, there has to be an inverse split operation in order to "make triangles bigger" again, i.e. in 
* order to group two or more triangles together. A good choice would probably be the so called "edge collapse".
* Second, there needs to be some kind of smoothing operator, which "intelligently" adapts the newly created 
* vertices' positions. Both are not implemented yet, but I guess that'll have to do for a while. If you 
* need either feature feel free...
*
* @todo Currently, the algorithm is not splitting boundary mesh edges. This is, because by the time 
* of this writing I need it to generate meshes with a given boundary loop. However, I think it would
* not be much of an act to augment the alg. in a way, that it optionally can handle splitting boundary edges.
* Again, if you need it...
*
*
* @author   Bernd Hentschel
* @date     Octobre, 2005
*
*/
class VISTAVISEXTAPI vtkLEBisectRemeshing : public vtkPolyDataAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkLEBisectRemeshing,vtkPolyDataAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

	// Description:
	// Constructor
	static vtkLEBisectRemeshing *New();

	/**
	* Get/Set the target length for the longest mesh edge. Bisection will be performed until all 
	* mesh edges are shorter than this value
	*/
	vtkSetMacro(MaximumEdgeLength, float);
	vtkGetMacro(MaximumEdgeLength, float);


	/**
	* Get/Set the maximum number of performed triangle splits. This is done in order to prevent
	* too long running times as well as exploding triangle count. Note, however, that an early
	* termination of the algorithm will likely result in a locally refined mesh.
	*/
	vtkSetMacro(MaximumNumberOfSplits, int);
	vtkGetMacro(MaximumNumberOfSplits, int);

	/**
	* Enable/Disable the bisection of boundary edges.
	* @todo <b>NOT OPERATIONAL YET!</b>
	*/
	vtkSetMacro(AllowBoundaryBisect, bool);
	vtkGetMacro(AllowBoundaryBisect, bool);
	vtkBooleanMacro(AllowBoundaryBisect, int);

	/**
	* symbols for termination reason. After each call to update you may inquire
	* the reason for termination with the method given below.
	*
	* TERMINATE_ERROR_NON_TRIANGULAR  The algorithm has detected a non-triangular cell and has been aborted.
	* TERMINATE_ERROR_NON_MANIFOLD,   The algotithm has detected that the mesh is not 2-manifold and has been aborted.
	* TERMINATE_ERROR_SPLIT_FAILED,   An edge split operation has failed.
	* TERMINATE_LENGTH_MET,           The algorithm has terminated because the longest mesh edge is shorter than the given threshold.
	* TERMINATE_SPLITS_MET            The algorithm has been terminated because the maximum number of splits has been reached.
	*/
	enum{
		TERMINATE_ERROR_NON_TRIANGULAR,
		TERMINATE_ERROR_NON_MANIFOLD,
		TERMINATE_ERROR_SPLIT_FAILED,
		TERMINATE_LENGTH_MET,
		TERMINATE_SPLITS_MET
	};
	/**
	* Inquire the reason for termination 
	* Either the alg. terminates due to the fact, that the desired threshold
	* edge length has been reached OR because the given number of split operations
	* has been performed. As always, another (not so good) reason for termination 
	* is, that some kind of error has been detected.
	*/
	vtkGetMacro(ReasonForTermination, int);

protected:
	vtkLEBisectRemeshing();
	virtual ~vtkLEBisectRemeshing();

	// Usual data generation method
	virtual int RequestData(	vtkInformation* request,
		vtkInformationVector** inputVector,
		vtkInformationVector* outputVector);

private:
	vtkLEBisectRemeshing(const vtkLEBisectRemeshing&);  // Not implemented.
	void operator=(const vtkLEBisectRemeshing&);  // Not implemented.

	float MaximumEdgeLength;
	int   MaximumNumberOfSplits;
	int   ReasonForTermination;
	bool  AllowBoundaryBisect;
	vtkIdList *m_pNeighCells;

	//for "posing" purposes only
	vtkPolyDataWriter *pWriter;

	struct SEdgeInfo{
		SEdgeInfo()
			: iTriangleID(-1), iV1(-1), iV2(-1) {}
		SEdgeInfo(vtkIdType iT, vtkIdType i1, vtkIdType i2) 
			: iTriangleID(iT), iV1(i1), iV2(i2) {}
		vtkIdType iTriangleID;
		vtkIdType iV1;
		vtkIdType iV2;
	};

};

#endif //_VTKLEBISECTREMESHING_H

