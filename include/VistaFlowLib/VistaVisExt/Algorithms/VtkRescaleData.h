/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkRescaleData.h             RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA VtkExt                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VtkRescaleData    :                                               */
/*                                                                            */
/*============================================================================*/
#ifndef _VTKRESCALEDATA_H
#define _VTKRESCALEDATA_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkDataSetAlgorithm.h>
#include <string>
#include <vector>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*
*/
class VISTAVISEXTAPI vtkRescaleData : public vtkDataSetAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkRescaleData,vtkDataSetAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

	static vtkRescaleData *New();

	void AddFieldToRescale(const std::string& strFieldName, const float fMin, const float fMax);
	void RemoveRescale(const std::string& strFieldName);

protected:
	vtkRescaleData();
	virtual ~vtkRescaleData();

	/**
	* this is where the real work is done.
	*/
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

private:
	struct __SRescaleData{
		__SRescaleData(const std::string& strFName, float f1, float f2)
			: strFieldName(strFName), fMin(f1), fMax(f2){}
		std::string strFieldName;
		float fMin, fMax;
	};

	std::vector<__SRescaleData> m_vecRescales;
};


#endif //#ifdef _VTKRESCALEDATA_H

