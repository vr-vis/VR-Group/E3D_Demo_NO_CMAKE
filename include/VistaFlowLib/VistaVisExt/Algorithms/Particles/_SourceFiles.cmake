

set( RelativeDir "./Algorithms/Particles" )
set( RelativeSourceGroup "Source Files\\Algorithms\\Particles" )

set( DirFiles
	_SourceFiles.cmake
	VveDefaultCellLocator.h
	VveIntegratorBase.h
	VveIntegratorDOPRI5.h
	VveIntegratorRK2.h
	VveIntegratorRK4.h
	VveLinearIntegStep.h
	VveLineRefinementsPreds.cpp
	VveLineRefinementsPreds.h
	VveParticleTracer.h
	VveParticleTracing.h
	VveQuarticIntegStep.h
	VveSpatialInterpolator.h
	VveTemporalInterpolator.h
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )
