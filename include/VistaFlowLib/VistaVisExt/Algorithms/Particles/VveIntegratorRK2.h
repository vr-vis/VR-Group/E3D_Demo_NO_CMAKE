/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveIntegratorRK2.h			 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VVEINTEGRATORRK2_H
#define VVEINTEGRATORRK2_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VveIntegratorBase.h"
#include "VveLinearIntegStep.h"

#include <cassert>

/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 * Straightforward implementation of the Heun two-step Runge Kutte integration 
 * scheme with fixed step size.
 *
 * \DATE	March 2012
 */
template <typename TInterpolator>
class VveIntegratorRK2 : public VveIntegratorBase<TInterpolator>
{
public:
	typedef VveLinearIntegStep TStep;

	VveIntegratorRK2(TInterpolator* pInterpolator);
	virtual ~VveIntegratorRK2();

	/** 
	 * initialize the integration step
	 * ==> for this fixed step integration scheme this actually does nothing
	 */
	void InitIntegrationStep(double pStart[4]);
	/**
	 * Perform one integration step for the field defined by the given 
	 * interpolator, starting at pIn. The output is given as linear 
	 * integration step in pOutput.
	 */
	int ComputeNextStep(double* pIn, TStep *pStep);

private:
};

/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
template <typename TInterpolator>
VveIntegratorRK2<TInterpolator>::VveIntegratorRK2(TInterpolator *pInterpol)
	:	VveIntegratorBase<TInterpolator>(pInterpol)
{
}

template <typename TInterpolator>
VveIntegratorRK2<TInterpolator>::~VveIntegratorRK2()
{
}


template <typename TInterpolator>
inline void VveIntegratorRK2<TInterpolator>::InitIntegrationStep( double pStart[4] )
{
	return;
}


template <typename TInterpolator>
inline int VveIntegratorRK2<TInterpolator>::ComputeNextStep(double* pIn, TStep *pStep)
{
	
	TInterpolator *pInterpol = this->GetInterpolator();
	assert(pInterpol != NULL);

	const double dDirection = (this->GetMaxStep() > 0.0 ? 1.0 : -1.0);

	//determine temporal integration limit
	double dTimeFrame[2];
	pInterpol->GetValidTimeFrame(dTimeFrame);
	const double dTimeLimit = (dDirection>0.0 ? dTimeFrame[1] : dTimeFrame[0]);

	double dStep = this->GetMaxStep();
	this->SetActualStep(dStep);

	//if we are on or beyond the time limit ==> bail out right away!
	if((pIn[3]+dStep-dTimeLimit)*dDirection >= 0.0)
		return VveIntegratorBase<TInterpolator>::T_MAX_REACHED;
	
	// do not run past time limit ==> adjust step to hit the end spot on
	if( (pIn[3]+1.01*dStep-dTimeLimit)*dDirection > 0.0 ) 
		dStep = dTimeLimit - pIn[3];
	
	//interpolation result
	double f[3];
	
	//Get f(x(i), t(i))
	if(!pInterpol->Interpolate(pIn, f))
		return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;
	
	//Calculate k1 == use 4-vector to allow compiler to "see" SIMD here
	const double k1[4] = {
		dStep * f[0],
		dStep * f[1],
		dStep * f[2],
		dStep
	};

	//compute midpoint for probing
	//midpoint = (pIn + 2/3 * k1)
	const double d2_3 = 2.0 / 3.0;
	double dMidPoint[4] = {
		pIn[0] + d2_3*k1[0],
		pIn[1] + d2_3*k1[1],
		pIn[2] + d2_3*k1[2],
		pIn[3] + d2_3*k1[3]
	};
	
	//Get f(pIn + k1, t + StepSize)
	if(!pInterpol->Interpolate(dMidPoint, f))
		return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;
	
	//Calculate k2 == use 4-vector to allow compiler to "see" SIMD here
	const double k2[4] = {
		dStep * f[0],
		dStep * f[1],
		dStep * f[2],
		dStep
	};

	//finally compute output
	double dOut[4] = 
	{
		pIn[0] + 0.25 * (k1[0] + 3.0*k2[0]),
		pIn[1] + 0.25 * (k1[1] + 3.0*k2[1]),
		pIn[2] + 0.25 * (k1[2] + 3.0*k2[2]),
		//NOTE : actually we just advance time (pOut[3]) by stepsize but writing 
		//it symmetrically allows the compiler to use SIMD extensions.
		pIn[3] + 0.25 * (k1[3] + 3.0*k2[3])
	};
	pStep->Set(pIn, dOut);
	
	return VveIntegratorBase<TInterpolator>::INTEG_OK;
}


#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
