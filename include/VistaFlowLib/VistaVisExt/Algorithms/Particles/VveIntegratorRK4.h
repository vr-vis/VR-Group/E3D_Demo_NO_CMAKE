/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveIntegratorRK4.h RRRR				WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VVEINTEGRATORRK4_H
#define VVEINTEGRATORRK4_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VveIntegratorBase.h"
#include "VveLinearIntegStep.h"
#include <cassert>

/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 * Implementation of the standard RK4, fixed step size integration scheme
 */
template <typename TInterpolator>
class VveIntegratorRK4 : public VveIntegratorBase<TInterpolator>
{
public:
	/** make output step type transparent for clients */
	typedef VveLinearIntegStep TStep; 

	VveIntegratorRK4(TInterpolator* pInterpolator);
	~VveIntegratorRK4();

	/** 
	 * initialize the integration step
	 * ==> for this fixed step integration scheme this actually does nothing
	 */
	void InitIntegrationStep(double pStart[4]);

	/**
	 * Perform one integration step for the field defined by the given 
	 * interpolator, starting at pIn. The output is given as linear 
	 * integration step in pOutput.
	 */
	int ComputeNextStep(double* pIn, TStep *pOut);
};

template <typename TInterpolator>
VveIntegratorRK4<TInterpolator>::VveIntegratorRK4(TInterpolator* pI)
	:	VveIntegratorBase<TInterpolator>(pI)
{
}

template <typename TInterpolator>
VveIntegratorRK4<TInterpolator>::~VveIntegratorRK4()
{

}

template <typename TInterpolator>
inline void VveIntegratorRK4<TInterpolator>::InitIntegrationStep( double pStart[4] )
{
	return;
}

template <typename TInterpolator>
inline int VveIntegratorRK4<TInterpolator>::ComputeNextStep(double* pIn, TStep *pStep)
{
	TInterpolator *pInterpolator = this->GetInterpolator();
	assert(pInterpolator != NULL);

	const double dDirection = (this->GetMaxStep() > 0.0 ? 1.0 : -1.0);

	//determine temporal integration limit
	double dTimeFrame[2];
	pInterpolator->GetValidTimeFrame(dTimeFrame);
	const double dTimeLimit = (dDirection>0.0 ? dTimeFrame[1] : dTimeFrame[0]);

	double dStep = this->GetMaxStep();
	this->SetActualStep(dStep);

	//if we are on or beyond the time limit ==> bail out right away!
	if((pIn[3]+dStep-dTimeLimit)*dDirection >= 0.0)
		return VveIntegratorBase<TInterpolator>::T_MAX_REACHED;

	// do not run past time limit ==> adjust step to hit the end spot on
	if( (pIn[3]+1.01*dStep-dTimeLimit)*dDirection > 0.0 ) 
		dStep = dTimeLimit - pIn[3];

	
	//space for interpolation result
	double f[4];
	
	//Get f(x(i), t(i))
	if(!pInterpolator->Interpolate(pIn, f))
	{
		return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;
	}

	double k1[4] = {
		dStep * f[0],
		dStep * f[1],
		dStep * f[2],
		dStep
	};

	//calculate interim position t(i) + 0.5 * stepsize, pIn + 0.5 * k1
	//write this in 4 component terms in order to allow the compiler to "see" 4way SIMD here.
	double dPt[4] = {
		pIn[0] + 0.5 * k1[0],
		pIn[1] + 0.5 * k1[1],
		pIn[2] + 0.5 * k1[2],
		pIn[3] + 0.5 * k1[3]
	};

	//Evaluate vector field at interim position
	if(!pInterpolator->Interpolate(dPt, f))
	{
		return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;
	}

	//compute k2
	double k2[4] = {
		dStep * f[0],
		dStep * f[1],
		dStep * f[2],
		dStep
	};

	//compute next interim position t(i)+0.5*stepsize, pIn+0.5*k2
	dPt[0] = pIn[0] + 0.5 * k2[0];
	dPt[1] = pIn[1] + 0.5 * k2[1];
	dPt[2] = pIn[2] + 0.5 * k2[2];
	dPt[3] = pIn[3] + 0.5 * k2[3];

	//Evaluate vector field at interim position
	if(!pInterpolator->Interpolate(dPt, f))
	{
		return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;
	}

	//compute k3
	double k3[4] = {
		dStep * f[0],
		dStep * f[1],
		dStep * f[2],
		dStep
	};

	//compute next interim position t(i)+0.5*stepsize, pIn+0.5*k2
	dPt[0] = pIn[0] + k3[0];
	dPt[1] = pIn[1] + k3[1];
	dPt[2] = pIn[2] + k3[2];
	dPt[3] = pIn[3] + k3[3];

	//Evaluate vector field at interim position
	if(!pInterpolator->Interpolate(dPt, f))
	{
		return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;
	}

	//compute k4
	double k4[4] = {
		dStep * f[0],
		dStep * f[1],
		dStep * f[2],
		dStep
	};

	//add up interim results for final step
	//do so in symmetric fashion (accepting additional ops) for all 4 components
	//in order to expose SIMD parallelism.
	const double d1_6 = 1.0/6.0;
	double dOut[4] = 
	{
		pIn[0] + d1_6 * (k1[0] + 2.0*k2[0] + 2.0*k3[0] + k4[0]),
		pIn[1] + d1_6 * (k1[1] + 2.0*k2[1] + 2.0*k3[1] + k4[1]),
		pIn[2] + d1_6 * (k1[2] + 2.0*k2[2] + 2.0*k3[2] + k4[2]),
		pIn[3] + d1_6 * (k1[3] + 2.0*k2[3] + 2.0*k3[3] + k4[3])
	};
	pStep->Set(pIn, dOut);
	
	return VveIntegratorBase<TInterpolator>::INTEG_OK;
}


#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
