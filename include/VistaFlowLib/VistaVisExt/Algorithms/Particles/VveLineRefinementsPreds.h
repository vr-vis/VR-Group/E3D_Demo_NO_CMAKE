/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveLineRefinementPreds.h	 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VVELINEREFINEMENTPREDS_H
#define VVELINEREFINEMENTPREDS_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "../../VistaVisExtConfig.h"

/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 * Gradient-based discontinuity checking, i.e. evaluate the gradient of the
 * node positions w.r.t. the s-parametrization (approx. gradient by central
 * differences across a triplet of points).
 */
class VISTAVISEXTAPI VveGradientDiscontinuityPred
{
public:
	/**
	 * Create a discontinuity predicate with the given max threshold for the
	 * point position gradient.
	 */
	VveGradientDiscontinuityPred(const double dMaxGrad);
	~VveGradientDiscontinuityPred();

	/**
	 * For a triplet of points (x,y,z,s) - i.e. the fourth component holds the
	 * s-parametrization along a line - check for discontinuity thresholding the
	 * length of the gradient dPos/ds.
	 */
	bool operator() (const double dPt1[4], const double dPt2[4], const double dPt3[4]) const;
private:
	const double m_dMaxGrad2;
};

/**
 * Distance-based refinement predicate for iterative line refinement.
 */
class VISTAVISEXTAPI VveNodeDistanceRefinementPred
{
public:
	/**
	 * Create new predicate with the given maximum distance threshold.
	 * The predicate will return true whenever the distance between any two
	 * given nodes is > dMaxDistance.
	 */
	VveNodeDistanceRefinementPred(const double dMaxDistance);
	~VveNodeDistanceRefinementPred();

	/**
	 * returns true iff either of the inter-node distances
	 * |dPt2-dPt1| and |dPt3-dPt2| 
	 * is above the given MaxDistance threshold which indicates a necessary refinement
	 */
	bool operator() (const double dPt1[3], const double dPt2[3], const double dPt3[3]) const;

protected:
private:
	const double m_dMaxD2;
};

/**
 * angle-based refinement predicate for iterative line refinement
 */
class VISTAVISEXTAPI VveAngleRefinementPred
{
public:
	VveAngleRefinementPred(const double dAngle);
	~VveAngleRefinementPred();

	/**
	 * 
	 */
	bool operator()  (const double dPt1[3], const double dPt2[3], const double dPt3[3]) const;
protected:
private:
	const double m_dMaxAngleCos;
};

/**
 * area-based refinement predicate for iterative line refinement
 */
class VISTAVISEXTAPI VveAreaRefinementPred
{
public:
	VveAreaRefinementPred(const double dArea);
	~VveAreaRefinementPred();

	/**
	 * 
	 */
	bool operator()  (const double dPt1[3], const double dPt2[3], const double dPt3[3]) const;
protected:
private:
	const double m_dMaxArea2;
};




#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
