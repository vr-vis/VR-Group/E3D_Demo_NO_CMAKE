/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VveSpatialInterpolator.H     RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*			- VveSpatialInterpolatorBase                                      */
/*			- VveSpatialInterpolator										  */
/*			- VveSpatialInterpolator<vtkDataSet>							  */
/*                                                                            */
/*============================================================================*/


#ifndef _VVESPATIALINTERPOLATOR_H
#define _VVESPATIALINTERPOLATOR_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveDefaultCellLocator.h"

#include "../../Data/VveDiscreteDataTyped.h"

#include <vtkDataSet.h>
#include <vtkDataArray.h>
#include <vtkPointData.h>
#include <vtkGenericCell.h>


#include <cstdio>
#include <cassert>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * GENERAL COMMENTS:
 * 
 * The interpolation objects defined in this header provide a general,
 * type-independent view onto the underlying raw data. Their primary intended
 * use case is particle tracing. However, other algorithms may rely on them,
 * e.g. (CPU-based) volume rendering via ray casting.
 *
 * The primary goal of this module is to decouple high-level algorithms from
 * the specific characteristics of the underlying data by providing a general,
 * yet efficient interface to the data. Therefore, we have chosen to implement
 * the interpoaltion methods using templates, because template specialization
 * allows us to generalize over multiple data types with minimal disadvantages
 * in terms of performance.
 */

/**
 * VveSpatialInterpolatorBase is the base class of the interpolator templates
 * for spatial interpolation inside a data sets. It implements general methods 
 * that are shared by all derived interpolator implementations.
 * 
 * The actual interpolation method is specified with the following signature
 * 
 * bool Interpolate(double *dPos, double *dResult);
 * 
 * All interpolator implementations have to implement this interface. Note
 * that no pure virtual declaration is used here due to performance reasons.
 * Template specialization is used to implement this method, hence be careful
 * to implement it correctly in specializations below.
 *
 * Two additional interfaces have to be implemented by subclasses:
 *
 *	void SetData(TData* pData);
 *  void SetInterpolatedField(const std::string &strFieldName);
 *
 * Both need to be handled by derived classes in order to be able to properly
 * react to different aspects of setting new data/fields, depending on the actual 
 * data type, e.g. retrieving an attribute array, setting/re-setting intermediate
 * data structures, etc.
 *
 * Unlike the Interpolate(...) call, these are not performance critical.
 * Consequently, a pure virtual interface declaration is used here.
 *
 * Apart from the type of raw data, a cell location strategy is specified as 
 * template argument. This allows us to conveniently exchange the way cells 
 * are searched. Since cell location has a significant effect on overall 
 * interpolation performance - particularly for unstructured meshes - this
 * aspect was separated from the interpolation algorithm itself. Moreover,
 * this allows "locator sharing" (see below). NOTE, that each interpolation 
 * object has to have a cell locator by definition (i.e. it's a mandatory 
 * c'tor argument). For convenience, interpolation will default to standard
 * cell location methods implemented in VveDefaultCellLocator.h
 *
 * Handling of multiple fields: 
 * Each interpolator provides access to exactly one data field. In order
 * to access multiple fields from within an algorithm, use multiple interpolators.
 * If implemented naively, this would however, result in multiple cell look-ups
 * for the exact same position. This can be remedied by having multiple 
 * interpolators share a common cell locator object. The default cell location
 * algorithm provides a caching facility and will return the last known result
 * immediately if the query position is identical to the previous call's 
 * input.
 *
 * @author	
 * @date		March 2012
 */
template<typename TRawVisData, 
		 typename TLocator=VveDefaultCellLocator<TRawVisData> >
 
class VveSpatialInterpolatorBase
{
public:
	/**
	 * Tell the outside that we only need one time step to do interpolation
	 * and thus only rely on a single locator object
	 */
	static size_t GetTemporalSupportSize();

	/**
	 * determine maximum time for which we get valid data
	 */
	void GetValidTimeFrame(double dFrame[2]) const;
	
	/**
	 * Set the data object on which interpolation is carried out
	 */
	virtual void SetData(TRawVisData *pData) = 0;

	/**
	 * Set the data object on which interpolation is carried out
	 * Convenience variant of the aboce method that accepts VveDiscreteDataTyped
	 * in order to be consistent with VveTemporalInterpolator. Only the
	 * first time level will be used for interpolation!
	 */
	virtual void SetData(VveDiscreteDataTyped<TRawVisData>* pData) = 0;

	/**
	 *  Getter for encapsulated underlying data field
	 */
	TRawVisData* GetData() const;
	
	/**
	 * get the size of the output data array ==> currently we only do vectors
	 */
	size_t GetNumberOfResults() const;
	/**
	 * set the name of the field that is interpolated by this object
	 */
	virtual void SetInterpolatedField(const std::string &strFieldName) = 0;
	
	/**
	 * retrieve the name of the field that is interpolated by this object
	 */
	std::string GetInterpolatedField() const;

	/**
	 * Retrieve the underlying point locator
	 */
	TLocator *GetLocator() const;

protected:
	/**
	 *  Just a base class, no instances are allowed
	 */
	VveSpatialInterpolatorBase(const std::vector<TLocator*> &vecLocators);
	virtual ~VveSpatialInterpolatorBase();

	/**
	 * Setter for the underlying data object. Automatically
	 * forwards the data object to the cell locator.
	 */
	void SetDataPtr(TRawVisData *pData);

	/**
	 * set the number of results that will be returned by interpolation
	 * 
	 */
	void SetNumberOfResults(const int iNum);

	/**
	 * Setter for the underlying data field name that's
	 * used for interpolation
	 */
	void SetFieldString(const std::string &strName);

	
private:
	/** store pointer to the underlying raw data */
	TRawVisData *m_pData;
	/** remember the number of results that we want to interpolate */
	size_t m_iNumResults;
	/** remember the field we want to interpolate */
	std::string m_strFieldName;
	/** cell location algorithm */
	TLocator *m_pLocator;
};

/************************************************************************/
/* EMPTY GENERIC INTERPOLATOR											*/
/************************************************************************/
/**
 * VveSpatialInterpolator is the implementation class for generic 
 * interpolation on arbitrary data sets. Template specialization is used 
 * in order to implement data type dependent behavior. Therefore, the 
 * general interface does nothing more than the base class.
 */
template<typename TRawVisData, 
		 typename TLocator=VveDefaultCellLocator<TRawVisData> >

class VveSpatialInterpolator 
	
	: public VveSpatialInterpolatorBase<TRawVisData, TLocator>
{
	//nothing to do here, because everything else depends on TRawVisData
};

/************************************************************************/
/* SPECIALIZATION FOR vtkDataSet										*/
/************************************************************************/
/**
 * VveSpatialInterpolator<vtkDataSet> template specialization for 
 * generic vtkDataSets
 */
template<typename TLocator>

class VveSpatialInterpolator<vtkDataSet, TLocator> 

	: public VveSpatialInterpolatorBase<vtkDataSet, TLocator>
{
public:
	VveSpatialInterpolator(const std::vector<TLocator*> &vecLocators);
	virtual ~VveSpatialInterpolator();

	/**
	 * Set the data object on which interpolation is carried out
	 */
	virtual void SetData(vtkDataSet *pData);

	/**
	 * Set the underlying data field.
	 * Implements pure virtual declaration in VveSpatialInterpolatorBase
	 */
	virtual void SetData(VveDiscreteDataTyped<vtkDataSet>* pData);

	/** 
	 * Set the field which is interpolated.
	 * Implements pure virtual declaration in VveSpatialInterpolatorBase
	 */
	virtual void SetInterpolatedField(const std::string &strFieldName);


	/**
	 * Interpolate values at position *dIn (4 components), output is 
 	 * in the array pointed to by dOut. 
	 * Make sure that dOut has (at least) GetNumberOfResults() many entries!
	 */
	bool Interpolate(double *dIn, double *dOut);

private:
	//vtkDataSet interpolator specific members...

	/** keep track of the data array used for interpolation */
	vtkDataArray* m_pActiveArray;

	/** avoid re-creating this generic cell time and again */
	vtkGenericCell *m_pGenCell;
};



/************************************************************************/
/* Implementation of InterpolatorBase	                                */
/************************************************************************/
template<typename TRawVisData, typename TLocator >
inline VveSpatialInterpolatorBase<TRawVisData, TLocator>::VveSpatialInterpolatorBase(const std::vector<TLocator*> &vecLocators)
	:	m_pData(NULL),
		m_iNumResults(0),
		m_strFieldName(""),
		m_pLocator(vecLocators[0])
{
}

template<typename TRawVisData, typename TLocator>
inline VveSpatialInterpolatorBase<TRawVisData, TLocator>::~VveSpatialInterpolatorBase()
{
}

template<typename TRawVisData, typename TLocator >
inline size_t VveSpatialInterpolatorBase<TRawVisData, TLocator>::GetTemporalSupportSize()
{
	return 1;
}


template<typename TRawVisData, typename TLocator >
inline void  VveSpatialInterpolatorBase<TRawVisData, TLocator>::GetValidTimeFrame(double dFrame[2]) const
{
	//unlimited time for steady-state data
	dFrame[0] = -std::numeric_limits<double>::max();
	dFrame[1] = std::numeric_limits<double>::max();
}


template<typename TRawVisData, typename TLocator >
inline TRawVisData* VveSpatialInterpolatorBase<TRawVisData, TLocator>::GetData() const
{
	return this->m_pData;
}

template<typename TRawVisData, typename TLocator >
inline size_t VveSpatialInterpolatorBase<TRawVisData, TLocator>::GetNumberOfResults() const
{
	return m_iNumResults;
}

template<typename TRawVisData, typename TLocator >
inline std::string VveSpatialInterpolatorBase<TRawVisData, TLocator>::GetInterpolatedField() const
{
	return m_strFieldName;
}

template<typename TRawVisData, typename TLocator >
inline TLocator* VveSpatialInterpolatorBase<TRawVisData, TLocator>::GetLocator() const
{
	return m_pLocator;
}

template<typename TRawVisData, typename TLocator >
inline void VveSpatialInterpolatorBase<TRawVisData, TLocator>::SetDataPtr(TRawVisData *pDataSet)
{
	if(pDataSet == m_pData)
		return;

	m_pData = pDataSet;
	m_pLocator->SetData(pDataSet);
	//whenever we change the data we have to re-establish the index data structure
	m_pLocator->Build();
}

template<typename TRawVisData, typename TLocator >
inline void VveSpatialInterpolatorBase<TRawVisData, TLocator>::SetNumberOfResults(int iNum)
{
	m_iNumResults = iNum;
}

template<typename TRawVisData, typename TLocator >
inline void VveSpatialInterpolatorBase<TRawVisData, TLocator>::SetFieldString(const std::string &str)
{
	m_strFieldName = str;
}

/************************************************************************/
/* Implementation of VveInterpolator<vtkDataSet>                        */
/************************************************************************/
template<typename TLocator>
inline VveSpatialInterpolator<vtkDataSet,TLocator>::VveSpatialInterpolator(const std::vector<TLocator*> &vecLocators)
	:	VveSpatialInterpolatorBase<vtkDataSet,TLocator>(vecLocators),
		m_pActiveArray(NULL),
		m_pGenCell(vtkGenericCell::New())
{
}

template<typename TLocator>
VveSpatialInterpolator<vtkDataSet, TLocator>::~VveSpatialInterpolator()
{
	m_pGenCell->Delete();
}

template<typename TLocator>
void VveSpatialInterpolator<vtkDataSet, TLocator>::SetData(vtkDataSet *pData)
{
	if(pData == NULL)
	{
		vstr::errp() << "[VveSpatialInterpolator<vtkDataSet>::SetData] No data to set!\n"<<endl;
		this->SetDataPtr(NULL);
		m_pActiveArray = NULL;
		return;
	}
	//tell base class about the data
	this->SetDataPtr(pData);

	//default to vector field interpolation or (if not available) scalars
	std::string strFieldName = this->GetInterpolatedField();
	if(strFieldName.empty())
	{
		m_pActiveArray = pData->GetPointData()->GetVectors();
		if(m_pActiveArray == NULL) //no vectors ?
		{
			m_pActiveArray = pData->GetPointData()->GetScalars();
		}
	}
	else
	{
		if(strFieldName == "vectors")
			m_pActiveArray = pData->GetPointData()->GetVectors();
		else if(strFieldName == "scalars")
			m_pActiveArray = pData->GetPointData()->GetScalars();
		else if(strFieldName == "tensors")
			m_pActiveArray = pData->GetPointData()->GetTensors();
		else
			m_pActiveArray = pData->GetPointData()->GetArray(strFieldName.c_str());
	}

	//make sure we got valid data here! -- otherwise we won't be going anywhere!
	assert(this->GetData() != NULL && m_pActiveArray != NULL);

	//finally tell base class about our number of results for future interpolations
	this->SetNumberOfResults(m_pActiveArray->GetNumberOfComponents());
}

template<typename TLocator>
void VveSpatialInterpolator<vtkDataSet, TLocator>::SetData(VveDiscreteDataTyped<vtkDataSet> *pData)
{
	//sanity check
	if(pData == NULL || pData->GetNumberOfLevels() == 0)
	{
		vstr::errp() << "*** ERROR *** [VveSpatialInterpolator<vtkDataSet>::SetData] No data to set!\n"<<endl;
		this->SetDataPtr(NULL);
		m_pActiveArray = NULL;
		return;
	}
	//peel out the first time level	== default to level 0
	vtkDataSet *pStep = pData->GetTypedLevelDataByLevelIndex(0)->GetData();
	this->SetData(pStep);
}

template<typename TLocator>
void VveSpatialInterpolator<vtkDataSet, TLocator>::SetInterpolatedField(const std::string &strField)
{
	this->SetFieldString(strField);
	
	vtkDataSet *pData = this->GetData();
	//if we have no data yet ==> telling the base class is all we can do at this point
	if(pData == NULL)
		return;
	
	//sanity check
	if(strField.empty())
	{
		//default to vectors
		m_pActiveArray = pData->GetPointData()->GetVectors();
		if(m_pActiveArray == NULL) //no vectors ?
		{
			m_pActiveArray = pData->GetPointData()->GetScalars();
		}
	}
	else
	{
		//check phony names first!
		if(strField == "vectors")
			m_pActiveArray = pData->GetPointData()->GetVectors();
		else if(strField == "scalars")
			m_pActiveArray = pData->GetPointData()->GetScalars();
		else if(strField == "tensors")
			m_pActiveArray = pData->GetPointData()->GetTensors();
		else
			m_pActiveArray = pData->GetPointData()->GetArray(strField.c_str());
		
		if(m_pActiveArray == NULL)
		{
#ifdef DEBUG
			vstr::errp() << "[VveInterpolator<vtkDataSet>::SetInterpolatedField] "
				<<"Unable to find array <"<<strField.c_str()<<">!"<<endl
				<<"\tOptions are:"<<endl;
			const int iNumArrays = pData->GetPointData()->GetNumberOfArrays();
			for(int a=0; a<iNumArrays; ++a)
				vstr::erri()<<"\t\t\""<<pData->GetPointData()->GetArray(a)->GetName()<<"\""<<endl;
			vstr::erri()<<endl;
#endif
			this->SetFieldString("");
			this->SetNumberOfResults(0);
			return;
		}
	}
	
	//make sure we got valid data here! -- otherwise we won't be going anywhere!
	assert(this->GetInterpolatedField() == strField && m_pActiveArray != NULL );

	//finally tell base class about our number of results for future interpolations
	this->SetNumberOfResults(m_pActiveArray->GetNumberOfComponents());
}

template<typename TLocator>
inline bool VveSpatialInterpolator<vtkDataSet, TLocator>::Interpolate(double *dIn, double *dOut)
{
	vtkDataSet *pData = this->GetData();
	
	if(pData == NULL || m_pActiveArray == NULL)
	{
		return false;
	}

	//use locator to find the cell we are in (if any)
	TLocator *pLocator = this->GetLocator();
	
	//make sure we now have a locator...
	assert(pLocator != NULL);
	pLocator->SetData(pData);
	vtkDataSet *pLocData = pLocator->GetData();
	//...with proper data.
	assert(pData == pLocData);
	
	int iNumCellPts;
	double dWeights[24];

	vtkIdType iCellId = pLocator->FindCell(dIn, iNumCellPts, dWeights);
	if(iCellId == -1)
	{
		//no valid cell returned ==> we seem to be out of domain
		return false;
	}

	//retrieve the cell and do the interpolation
	pData->GetCell(iCellId, m_pGenCell);

	//make sure we got the right number of results
	assert(m_pActiveArray->GetNumberOfComponents() == this->GetNumberOfResults());
	
	const int iNumComps = m_pActiveArray->GetNumberOfComponents();
	
	//init result
	memset(dOut, 0, iNumComps*sizeof(dOut[0]));

	double dData[9];
	const int iNumPts = m_pGenCell->GetNumberOfPoints();
	//for all cell points
	for(register int pt=0; pt<iNumPts; ++pt)
	{
		m_pActiveArray->GetTuple(m_pGenCell->GetPointId(pt),dData);
		//for all array components
		for(register int c=0; c<iNumComps; ++c)
			dOut[c] += dData[c] * dWeights[pt];
	}
	
	return true;
}

#endif //_VVESPATIALINTERPOLATOR_H