/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveStreamSurfaceUtils.h		 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VVESTREAMSURFACEUTILS_H
#define VVESTREAMSURFACEUTILS_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "../../VistaVisExtConfig.h"
#include "VveLineRefinementsPreds.h"

#include <vector>
/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
class VISTAVISEXTAPI VveStreamSurfaceUtils
{
public:
	VveStreamSurfaceUtils();
	virtual ~VveStreamSurfaceUtils();

	/**
	 * Check the given input (time) line for discontinuities given the detection 
	 * predicate pDiscontPred. The input line is given as a continuous vector
	 * of four-tuples (x,y,z,s) which represent a point's coordinate in
	 * 3-space as well as its s-coordinate along the line.
	 * The output indicates the positions at which the input line is found
	 * to be discontinuous. This info can be used to split the line into
	 * continuous pieces. The return value gives the number of detected
	 * discontinuities.
	 */
	template<typename TDiscontPred>
	size_t CheckDiscontinuities(const std::vector<double>& vecSourceLine, 
							    TDiscontPred *pDiscontPred,
							    std::vector<size_t> &vecSplits);
	
	/**
	 * Refine the time line given in vecSourceLine based on the geometry in 
	 * vecReferenceLine using the refinement predicate pRefinementPred. 
	 * Typically, a client would pass the time line T_i as a source line and
	 * T_i+1 as a reference line. In that case, T_i's points are copied over
	 * to the output line vecRefinedLine and refinement points are inserted
	 * wherever pRefinementPred indicates a necessary refinement. 
	 * The input line is given as a continuous vector of four-tuples 
	 * (x,y,z,s) which represent a point's coordinate in 3-space as well 
	 * as its s-coordinate along the line. vecRefinedLine is structured 
	 * analogously.
	 * vecRefOffsets indicates the base index (i.e. x-component) of points
	 * that have been inserted during refinement. The return value indicates 
	 * the number of refinements.
	 */
	template<class TRefPred>
	size_t InsertRefinementPoints(const std::vector<double>& vecSourceLine, 
						          const std::vector<double>& vecReferenceLine, 
						          TRefPred *pRefinementPred,
						          std::vector<double>& vecRefinedLine, 
						          std::vector<size_t>& vecRefOffsets) const;
protected:
	/**
	 * Convenience methods
	 */
	void PushPoint(const double dPt[4], std::vector<double> &vecPts) const;
	void PushMidPoint(const double dPt1[4], const double dPt2[4], std::vector<double> &vecPts) const;
};

/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
template<typename TDiscontPred>
size_t VveStreamSurfaceUtils::CheckDiscontinuities(const std::vector<double>& vecSourceLine, 
												   TDiscontPred *pDiscontPred, 
												   std::vector<size_t> &vecSplits )
{
	assert(vecSourceLine.size()%4==0);
	assert(pDiscontPred != NULL);

	const size_t iNumPts = vecSourceLine.size()/4;
	vecSplits.clear();
	
	//sanity check
	if(iNumPts < 3)
		return 0;
	
	vecSplits.reserve(iNumPts/10+1);
	
	const TDiscontPred &oPred = *pDiscontPred;

	//count successive discontinuity detections
	size_t iDiscontCounter = 0;

	//check start segment for discontinuity
	if(oPred(&(vecSourceLine[0]),&(vecSourceLine[4]),&(vecSourceLine[8])))
	{
		//split off the end anyway
		vecSplits.push_back(4);
		//start counting
		iDiscontCounter = 1;
	}

	for(size_t pt=3; pt<iNumPts-1; ++pt)
	{
		size_t iBase[3] = {4*(pt-2), 4*(pt-1), 4*pt};
		bool bIsDiscont = oPred(&(vecSourceLine[iBase[0]]),&(vecSourceLine[iBase[1]]),&(vecSourceLine[iBase[2]]));

		if(bIsDiscont)
		{
			if(iDiscontCounter==0)
			{
				//a single discontinuity has no overlap ==> no segment to eliminate
				++iDiscontCounter;
			}
			else
			{
				//we just got two in a row ==> split along the overlap
				//the overlapping segment is the first one of the current triplet
				//i.e. the center of the current triplet designates the start pt 
				//of the new line segment after the discontinuity
				vecSplits.push_back(iBase[1]);
				//take the current discont as the first for the next iteration
				//in case we got multiple overlaps
				iDiscontCounter = 1;
			}
		}
		else
		{
			//a single positive eval (e.g. right after detecting a double) doesn't make
			//for overlap ==> just get back to continuous.
			iDiscontCounter = 0;	
		}
	}
	//check last triplet and split off end if needed
	size_t iLTB = vecSourceLine.size()-12;
	if(oPred(&(vecSourceLine[iLTB]),&(vecSourceLine[iLTB+4]),&(vecSourceLine[iLTB+8])))
	{
		//if we had a discontinuity in the last iteration, there is an overlapping segment
		//==> split that off -- split off the very last line segment anyway.
		if(iDiscontCounter > 0)
			vecSplits.push_back(iLTB+4);
		vecSplits.push_back(iLTB+8);
	}

	return vecSplits.size();
}


template<class TRefPred>
inline size_t VveStreamSurfaceUtils::InsertRefinementPoints(const std::vector<double>& vecSourceLine, 
												            const std::vector<double>& vecReferenceLine, 
												            TRefPred *pRefinementPred, 
												            std::vector<double>& vecRefinedLine, 
												            std::vector<size_t>& vecRefOffsets) const
{
	//make sure we got correct points
	assert(vecSourceLine.size()%4==0);
	assert(vecReferenceLine.size() == vecSourceLine.size());
	assert(pRefinementPred != NULL);

	const TRefPred &oPred = *pRefinementPred;

	const size_t iNumPts = vecSourceLine.size()/4;
	
	//make sure we got a yet untouched reference line
	assert(iNumPts == vecReferenceLine.size()/4);

	vecRefinedLine.clear();
	//assume we add roughly iNumPts/2 many new pts
	vecRefinedLine.reserve(6*iNumPts);
	vecRefOffsets.clear();

	//first point is always in output
	this->PushPoint(&(vecSourceLine[0]), vecRefinedLine);
	
	bool bPrevRefine = false;
	for(size_t pt=2; pt<iNumPts; ++pt)
	{
		size_t iBase[3] = {4*(pt-2), 4*(pt-1), 4*pt};
		bool bRefine = oPred(&(vecSourceLine[iBase[0]]),&(vecSourceLine[iBase[1]]),&(vecSourceLine[iBase[2]]));
		if(bRefine || bPrevRefine)
		{
			//insert refinement for trailing edge of current triplet
			vecRefOffsets.push_back(vecRefinedLine.size());
			this->PushMidPoint(&(vecSourceLine[iBase[0]]), &(vecSourceLine[iBase[1]]), vecRefinedLine);
		}
		//copy over original end point for trailing edge of current triplet. 
		this->PushPoint(&(vecSourceLine[iBase[1]]), vecRefinedLine);
		//remember whether we refined for next iteration
		bPrevRefine = bRefine;		
	}

	//do an extra ref pass for last line segment
	size_t iLastOfs = vecSourceLine.size()-4;
	if(bPrevRefine)
	{
		//insert refinement for trailing edge of current triplet
		vecRefOffsets.push_back(vecRefinedLine.size());
		this->PushMidPoint(&(vecSourceLine[iLastOfs-4]), &(vecSourceLine[iLastOfs]), vecRefinedLine);
	}

	//last point is always in output
	this->PushPoint(&(vecSourceLine[iLastOfs]), vecRefinedLine);
	
	return vecRefOffsets.size();
}


#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
