/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveParticleTracing.h RRRR           WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* This header provides default access to all particle tracing related        */
/* definitions. In particular, it provides standard traits and definitions    */
/* for Vve's particle tracing module.		                                  */
/*                                                                            */
/*============================================================================*/

 

#ifndef VVEPARTICLETRACING_H
#define VVEPARTICLETRACING_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
//inlcude the standard stuff needed for particle tracing anyway
#include "VveParticleTracer.h"
#include "VveSpatialInterpolator.h"
#include "VveTemporalInterpolator.h"
#include "VveIntegratorRK2.h"
#include "VveIntegratorRK4.h"
#include "VveIntegratorDOPRI5.h"
#include "VveDefaultCellLocator.h"

/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 * GENERAL NOTES:
 * The VveParticleTracer relies on type trait classes that define the following
 * three components:
 * TLocator			:	The type of the cell locator to be used
 *						Look at VveDefaultCellLoactor for details.
 * TInterpolator	:	The interpolation type to be used, e.g. 
 *						VveSpatialInterpolator for streamlines
 * TIntegrator		:	The numerical integration scheme to be used.
 *
 * In the following, we give some standard definitions for traits and
 * respective typedefs for common particle tracer configurations.
 */

/**
 * The StreamlineTrait defines a default type trait that configures the 
 * VveParticleTracer for streamline tracing on a given raw data type.
 */
template<class TRawVisData>
struct SVveDefaultStreamlineTrait
{
	typedef VveDefaultCellLocator<TRawVisData> TLocator;

	typedef VveSpatialInterpolator<TRawVisData, TLocator> TInterpolator;

	typedef VveIntegratorDOPRI5<TInterpolator> TIntegrator;
};

/**
 * The StreamlineTrait defines a default type trait that configures the 
 * VveParticleTracer for pathline tracing on a given raw data type.
 */
template<class TRawVisData>
struct SVveDefaultPathlineTrait{
	typedef VveDefaultCellLocator<TRawVisData> TLocator;

	typedef VveSpatialInterpolator<TRawVisData, TLocator> TSpatialInterpolator;

	typedef VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator> TInterpolator;

	typedef VveIntegratorDOPRI5<TInterpolator> TIntegrator;
};

/**
 * The default streamline tracer on vtk data sets uses the aforementioned
 * streamline trait (i.e. standard vtk cell location and fixed-step RK4)
 */
typedef VveParticleTracer<vtkDataSet, SVveDefaultStreamlineTrait<vtkDataSet> > VveVtkStreamlineTracer;

/**
 * The default pathline tracer on vtk data sets uses the aforementioned
 * pathline trait (i.e. standard vtk cell location and fixed-step RK4)
 */
typedef VveParticleTracer<vtkDataSet, SVveDefaultPathlineTrait<vtkDataSet> > VveVtkPathlineTracer;

/**
 * \TODO Define more standard types for other raw data types, e.g. 
 *       VveTetrahedralGrid, VveCartesianGrid...
 */

#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/