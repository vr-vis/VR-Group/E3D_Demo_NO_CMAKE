/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VVEINTEGRALSURFACETRACER_H
#define VVEINTEGRALSURFACETRACER_H

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
struct TimeLineNode;
struct TimeLineRoot;

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveInterpolator.h"
#include "VveIntegratorRK4.h"

#include <vtkPolyData.h>
#include <math.h>

#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkDoubleArray.h>

/*============================================================================*/
/* CLASS DEFINITION                                                           */
/*============================================================================*/
/**
* VveIntegralSurfaceTraceer computes streamsurfaces or pathsurfaces for
* steady and time-varying input flow fields, respectively.
* Please NOTE that currently this algorithm does not provide means for 
* streak surfaces nor time surfaces.
* 
* The algorithm's input is a flow field field, and the algorithm outputs a 
* two-manifold surface mesh, a set of time lines, and a set of integral curves.
* All output data is given as vtkPolyData returned by the corresponding 
* Get-Functions, e.g. GetIntegralSurface()
* 
* The first template argument defines the input data field. Currently, 
* the algorithm is implemented for steady-state vtkDataSets and time-varying 
* VveDiscreteDataTyped<vtkDataSet>.
*/

/** 
* A little helper struct representing a linear list of
* nodes along a time line
*/ 
struct TimeLineNode
{
public:
	TimeLineNode();
	virtual ~TimeLineNode();

	TimeLineNode* pNextNodeRight;
	TimeLineNode* pNextNodeForward;
	double s, x, y, z, t;
	double dTimeLineValue;
	bool bExactlyOnTimeLine;
};


/** 
* A Root Node for the linear lists
*/
struct TimeLineRoot
{
public:
	TimeLineRoot();
	virtual ~TimeLineRoot();
	TimeLineNode* pNextNode;
};

//////////////////////////////////////////////////////////////////////////
// Implementations of the linear list

inline TimeLineNode::TimeLineNode()
{
	pNextNodeRight = NULL;
	pNextNodeForward = NULL;
	t = 0.0;
	bExactlyOnTimeLine = false;
}

inline TimeLineNode::~TimeLineNode()
{

}


inline TimeLineRoot::TimeLineRoot()
{
	pNextNode = NULL;
}


inline TimeLineRoot::~TimeLineRoot()
{
	if(pNextNode)
		delete(pNextNode);
}

template<typename TData, typename TIntegrator=IntegratorRK4<TData> >
class VveIntegralSurfaceTracer
{
public:
	VveIntegralSurfaceTracer();
	virtual ~VveIntegralSurfaceTracer();

	enum{
		TRACE_FORWARD,
		TRACE_BACKWARD,
		TRACE_BOTH
	};

	/**
	* Set the two end points of the seed line
	* Until now there can only be set a seed LINE, i.e. no arbitrary shaped seed polygon, for example
	*/
	void SetSeed(double* pts);
	double* GetSeed() const;

	void SetStepSize(double dStepSize);
	double GetStepSize();

	void SetTraceMode(int iMode);
	int GetTraceMode();

	void SetData(TData* data);
	void SetDataFieldName(const std::string& name);

	void SetAreaThreshold(double weight);
	double GetAreaThreshold();

	void SetDistanceThreshold(double weight);
	double GetDistanceThreshold();

	/**
	* Set the maximum number of steps. In the unsteady case, this is an additional break condition together with the time values
	* In the steady case this is the only break condition!
	*/
	void SetNumberOfSteps(int iNumSteps);
	int GetNumberOfSteps();

	void Execute();
	vtkPolyData* GetIntegralSurface();
	vtkPolyData* GetTimeLines();
	vtkPolyData* GetIntegralCurves();

protected:
	/** 
	* Parameter is the index of the node where it must be evaluated, 
	* i.e. the FIRST node of three, not the one in the middle!
	*/
	bool PredicateQrefineTimeLine(TimeLineNode* pStartNode);
	bool PredicateQrefineIntegralCurve(TimeLineNode* pStartNode);
	bool PredicateQrefineIntegralCurve(double pos1[], double pos2[]);

	bool PredicateQdiscont(TimeLineNode* pStartNode);

	/**
	* Refines a given Time Line as described in the original paper
	*/
	void RefineTimeLine(TimeLineNode* pStartNode, TimeLineNode* pEndNode, TimeLineNode* pLastTimeLineStartNode, TimeLineNode* pLastTimeLineEndNode);

	/**
	* The refinement of an integral curve only requires to look at the distance between two nodes
	*/
	void RefineIntegralCurve(TimeLineNode* pStartNode);

	/** 
	* Return the root node of the next time line
	*/
	TimeLineRoot* StepForward(TimeLineNode* pStartNode, TimeLineNode* pEndNode);

	/**
	* The Refinement of the start line is a special case of the general time line refinement, because new inserted nodes
	* can not be reprojected to the last time line, so the position of new nodes must directly be computed from the neighbouring nodes
	*/
	TimeLineRoot* RefineStartLine();

	/**
	* Recursively triangulates between two integral curves. If a new curve was inserted during the integration phase, both paths
	* are triangulated via two recursive calls of TriangulateRibbon(..)
	*/
	void TriangulateRibbon(TimeLineNode *pLeftNode, int pLeftNodeID, 
		TimeLineNode *pRightNode, int pRightNodeID, 
		vtkCellArray *pTriangles, vtkPoints *pPoints, 
		vtkDoubleArray *pTimeValues, vtkDoubleArray *pSValues);

	/**
	* Deletes all nodes that have been added during integration
	*/
	void DeleteNodeList();

private:
	vtkPolyData *m_pIntegralSurface;			//The whole surface mesh
	vtkPolyData *m_pTimeLines;					//Just the time lines
	vtkPolyData *m_pIntegralCurves;				//Just the integral curves

	double m_pSeeds[8];							//Coordinates of the seed line
	int m_iSeedSize;
	int m_iTraceMode;
	int m_iNumberOfSteps;						//The maximum number of steps; in time-invariant case: exactly the number of steps

	double m_dAreaThreshold;
	double m_dDistanceThreshold;

	bool m_bSurfaceOutputIsReady;
	bool m_bTimeLineOutputIsReady;				//true, if already generated, so the Get-methods can return the vtkPolyData without
	bool m_bCurvesOutputIsReady;				//generating again

	TData* m_pData;

	TIntegrator* m_pIntegrator;
	VveInterpolator<TData>* m_pInterpolator;

	std::list<TimeLineNode*> m_pTimelineList;
	std::list<TimeLineNode*> m_pCurveStartNodeList;	//Starting nodes of the curves, so that the refinement or the calculation of the curves
	//for output can be done faster; also used for the deletion of the nodes

	std::list<TimeLineNode*> m_pStopNodeList;		//Every node that shows a collision between a time line and the borderline of the interpolation area
	//is saved here, so it can be deleted at the end
};

/*============================================================================*/
/* LOCAL VARS FUNCS AND DEFINITIONS                                           */
/*============================================================================*/



/*============================================================================*/
/* The Constructor                                                            */
/*============================================================================*/

template<typename TData, typename TIntegrator>
inline VveIntegralSurfaceTracer<TData, TIntegrator>::VveIntegralSurfaceTracer()
{
	m_pIntegralSurface = vtkPolyData::New();
	m_pTimeLines = vtkPolyData::New();
	m_pIntegralCurves = vtkPolyData::New();
	m_bSurfaceOutputIsReady = false;
	m_bTimeLineOutputIsReady = false;
	m_bCurvesOutputIsReady = false;

	m_pInterpolator = new VveInterpolator<TData>;
	m_pIntegrator = new TIntegrator(m_pInterpolator);

	m_iNumberOfSteps = 100;		//Set some values; they should be overwritten by the user before starting the calculation!
	m_dAreaThreshold = 0.1;
	m_dDistanceThreshold = 0.1;
}


/*============================================================================*/
/* The Destructor                                                             */
/*============================================================================*/

template<typename TData, typename TIntegrator>
inline VveIntegralSurfaceTracer<TData, TIntegrator>::~VveIntegralSurfaceTracer()
{
	DeleteNodeList();

	m_pIntegralSurface->Delete();
	m_pTimelineList.clear();
	m_pStopNodeList.clear();
	m_pIntegralCurves->Delete();

	if(m_pIntegrator)
		delete(m_pIntegrator);
}

/**
* Deletes the nodes that are saved in the list
*/
template<typename TData, typename TIntegrator>
inline void VveIntegralSurfaceTracer<TData, TIntegrator>::DeleteNodeList()
{
#ifdef DEBUG
	cout << "VveIntegralSurfaceTracer::DeleteNodeList()" << endl;
	cout << "There are " << m_pCurveStartNodeList.size() << " starting nodes for the curves and " << m_pStopNodeList.size() << " stop nodes" << endl;
#endif

	std::list<TimeLineNode*>::iterator CurveStartNodeListIterator = m_pCurveStartNodeList.begin();
	std::list<TimeLineNode*>::iterator CurveStartNodeListEnd      = m_pCurveStartNodeList.end();

	TimeLineNode *pNodeCount = NULL;
	TimeLineNode *pNextNode  = NULL;

	int i = 0;

	//curve-wise deletion from left to right
	//nodes with s-value -1.0 are stop-nodes and saved in another list, so they will be deleted later
	for(; CurveStartNodeListIterator != CurveStartNodeListEnd; ++CurveStartNodeListIterator)
	{
		pNodeCount = *(CurveStartNodeListIterator);

		while(pNodeCount != NULL && pNodeCount->s != -1.0)
		{
			++i;
			pNextNode = pNodeCount->pNextNodeForward;
			delete(pNodeCount);
			pNodeCount = pNextNode;
		}

		if(pNodeCount != NULL && pNodeCount->s != -1.0)
		{
			delete(pNodeCount);
			++i;
		}
	}

	std::list<TimeLineNode*>::iterator StopNodeListIterator = m_pStopNodeList.begin();
	std::list<TimeLineNode*>::iterator StopNodeListEnd      = m_pStopNodeList.end();

	//Now the stop nodes
	for(; StopNodeListIterator != StopNodeListEnd; ++StopNodeListIterator)
	{
		pNodeCount = *(StopNodeListIterator);
		delete(pNodeCount);
		++i;
	}

#ifdef DEBUG
	cout << i << " nodes were deleted" << endl;
#endif


}

template<typename TData, typename TIntegrator>
inline void VveIntegralSurfaceTracer<TData, TIntegrator>::SetSeed(double* pts)
{
	//Just a seed LINE is valid until yet

	for(int i=0; i<8; ++i)
		m_pSeeds[i] = pts[i];

	m_iSeedSize = 2;
}

template<typename TData, typename TIntegrator>
inline double* VveIntegralSurfaceTracer<TData, TIntegrator>::GetSeed() const
{
	return m_pSeeds;
}

template<typename TData, typename TIntegrator>
inline void VveIntegralSurfaceTracer<TData, TIntegrator>::SetAreaThreshold(double threshold)
{
	m_dAreaThreshold = threshold;
}

template<typename TData, typename TIntegrator>
inline double VveIntegralSurfaceTracer<TData, TIntegrator>::GetAreaThreshold()
{
	return m_dAreaThreshold;
}

template<typename TData, typename TIntegrator>
inline void VveIntegralSurfaceTracer<TData, TIntegrator>::SetDistanceThreshold(double threshold)
{
	m_dDistanceThreshold = threshold;
}

template<typename TData, typename TIntegrator>
inline double VveIntegralSurfaceTracer<TData, TIntegrator>::GetDistanceThreshold()
{
	return m_dDistanceThreshold;
}

template<typename TData, typename TIntegrator>
inline void VveIntegralSurfaceTracer<TData, TIntegrator>::SetStepSize(double dStepSize)
{
	if(m_pIntegrator)
		m_pIntegrator->SetStepSize(dStepSize);
}

template<typename TData, typename TIntegrator>
inline double VveIntegralSurfaceTracer<TData, TIntegrator>::GetStepSize()
{
	return m_pIntegrator->GetStepSize();
}

template<typename TData, typename TIntegrator>
inline void VveIntegralSurfaceTracer<TData, TIntegrator>::SetTraceMode(int iMode)
{
	m_iTraceMode = iMode;
}

template<typename TData, typename TIntegrator>
inline int VveIntegralSurfaceTracer<TData, TIntegrator>::GetTraceMode()
{
	return m_iTraceMode;
}

template<typename TData, typename TIntegrator>
inline void VveIntegralSurfaceTracer<TData, TIntegrator>::SetNumberOfSteps(int iNumSteps)
{
	m_iNumberOfSteps = iNumSteps;
}

template<typename TData, typename TIntegrator>
inline int VveIntegralSurfaceTracer<TData, TIntegrator>::GetNumberOfSteps()
{
	return m_iNumberOfSteps;
}

template<typename TData, typename TIntegrator>
inline void VveIntegralSurfaceTracer<TData, TIntegrator>::SetData(TData* data)
{
	if(!data)
	{
		cout << "*** ERROR *** [IntegralSurfaceTracer::SetData(..)] ";
		cout << "Could not find any data. Aborting..." << endl;
		return;
	}

	m_pData = data;
	m_pInterpolator->SetData(data);
}

/**
* Set the name of the data field that has to be visualized                   
*/
template<typename TData, typename TIntegrator>
inline void VveIntegralSurfaceTracer<TData, TIntegrator>::SetDataFieldName(const std::string& name)
{
	m_pInterpolator->SetDataFieldName(name);
}

template<typename TData, typename TIntegrator>
TimeLineRoot* VveIntegralSurfaceTracer<TData, TIntegrator>::RefineStartLine()
{
	TimeLineRoot *rootnode = new TimeLineRoot;

	//The first node is the first node of the seed line
	TimeLineNode* node = new TimeLineNode;
	node->x = m_pSeeds[0];
	node->y = m_pSeeds[1];
	node->z = m_pSeeds[2];
	node->dTimeLineValue = m_pSeeds[3];
	node->t = m_pSeeds[3];
	node->s = 0.0;

	rootnode->pNextNode = node;

	m_pCurveStartNodeList.push_back(node);

	TimeLineNode* pHelpNode = rootnode->pNextNode;
	TimeLineNode* pNodeCount = NULL;
	TimeLineNode* pStopNode = NULL;

	//The second node is the midpoint of the seed line

	node = new TimeLineNode;
	node->x = m_pSeeds[0] + (0.5 * (m_pSeeds[4] - m_pSeeds[0]));
	node->y = m_pSeeds[1] + (0.5 * (m_pSeeds[5] - m_pSeeds[1]));
	node->z = m_pSeeds[2] + (0.5 * (m_pSeeds[6] - m_pSeeds[2]));
	node->dTimeLineValue = m_pSeeds[3];
	node->t = m_pSeeds[3];
	node->s = 50.0;

	rootnode->pNextNode->pNextNodeRight = node;

	m_pCurveStartNodeList.push_back(node);


	//The last node is the last point of the seed line

	node = new TimeLineNode;
	node->x = m_pSeeds[4];
	node->y = m_pSeeds[5];
	node->z = m_pSeeds[6];
	node->dTimeLineValue = m_pSeeds[3];
	node->t = m_pSeeds[3];
	node->s = 100.0;


	rootnode->pNextNode->pNextNodeRight->pNextNodeRight = node;

	m_pCurveStartNodeList.push_back(node);

	//Now there is a seed line consisting of 3 points -> use PredicateQRefineTimeLine(..) for further refinement

	bool bRefinementReady = false;
	pNodeCount = rootnode->pNextNode;
	TimeLineNode *pHelpNode1;

	std::list<double> liParameterList;
	double dParameterValue;

	while(!bRefinementReady)
	{
		pNodeCount = rootnode->pNextNode;
		bRefinementReady = true;

		while(pNodeCount->pNextNodeRight->pNextNodeRight != NULL)
		{

			if(PredicateQrefineTimeLine(pNodeCount) == true)
			{
				liParameterList.remove(0.5 * (pNodeCount->s + pNodeCount->pNextNodeRight->s));
				liParameterList.push_back(0.5 * (pNodeCount->s + pNodeCount->pNextNodeRight->s));

				liParameterList.remove(0.5 * (pNodeCount->pNextNodeRight->s + pNodeCount->pNextNodeRight->pNextNodeRight->s));
				liParameterList.push_back(0.5 * (pNodeCount->pNextNodeRight->s + pNodeCount->pNextNodeRight->pNextNodeRight->s));

				bRefinementReady = false;
			}
			pNodeCount = pNodeCount->pNextNodeRight;		
		}

		//Insert nodes with the parameter values saved in liParameterList
		pNodeCount = rootnode->pNextNode;

		while(!liParameterList.empty())
		{
			dParameterValue = liParameterList.front();
			liParameterList.pop_front();

			while(pNodeCount->pNextNodeRight->s < dParameterValue)
				pNodeCount = pNodeCount->pNextNodeRight;

			pHelpNode1 = new TimeLineNode;
			pHelpNode1->x = pNodeCount->x + (0.5 * (pNodeCount->pNextNodeRight->x - pNodeCount->x));
			pHelpNode1->y = pNodeCount->y + (0.5 * (pNodeCount->pNextNodeRight->y - pNodeCount->y));
			pHelpNode1->z = pNodeCount->z + (0.5 * (pNodeCount->pNextNodeRight->z - pNodeCount->z));
			pHelpNode1->s = pNodeCount->s + (0.5 * (pNodeCount->pNextNodeRight->s - pNodeCount->s));
			pHelpNode1->dTimeLineValue = m_pSeeds[3];
			pHelpNode1->t = m_pSeeds[3];
			pHelpNode1->pNextNodeRight = pNodeCount->pNextNodeRight;

			pNodeCount->pNextNodeRight = pHelpNode1;

			m_pCurveStartNodeList.push_back(pHelpNode1);
		}
	}

	return rootnode;
}

/**
* Calculates the surface in the time-invariant case
*/
template<typename TData, typename TIntegrator>
inline void VveIntegralSurfaceTracer<TData, TIntegrator>::Execute()
{
#ifdef DEBUG
	cout << "VveIntegralSurfaceTracer::Execute()" << endl;
#endif

	m_bSurfaceOutputIsReady  = false;
	m_bTimeLineOutputIsReady = false;
	m_bCurvesOutputIsReady   = false;

	m_pStopNodeList.clear();

	double dStepSize = m_pIntegrator->GetStepSize();

	TimeLineRoot *rootnode = RefineStartLine();

	TimeLineNode* pHelpNode = rootnode->pNextNode;
	TimeLineNode* pNodeCount = NULL;
	TimeLineNode* pStopNode = NULL;
	TimeLineNode* node = NULL;

	node = rootnode->pNextNode;
	while(node->pNextNodeRight != NULL)
		node = node->pNextNodeRight;


	//seed line is refined, now save the seed line in the list
	std::list<TimeLineNode*> nodelist;

	TimeLineRoot* nextrootnode = rootnode;

	m_pTimelineList.push_back(rootnode->pNextNode);

	TimeLineNode *pLastEndNode = NULL;
	TimeLineNode *pNextEndNode = NULL;

#ifdef DEBUG
	cout << "Starting Surface Generation" << endl;
#endif

	for(int i=0; i<m_iNumberOfSteps; ++i)
	{
#ifdef DEBUG
		cout << "Integration Step no." << i << endl;
#endif

		rootnode = nextrootnode;
		nextrootnode = StepForward(nextrootnode->pNextNode, node);

		pLastEndNode = rootnode->pNextNode;
		pNextEndNode  = nextrootnode->pNextNode;

		while(pLastEndNode->pNextNodeRight != NULL)
			pLastEndNode = pLastEndNode->pNextNodeRight;

		while(pNextEndNode->pNextNodeRight != NULL)
			pNextEndNode = pNextEndNode->pNextNodeRight;

		node = pNextEndNode;

		//Current line goes from nextrootnode->pNextNode to pNextEndNode
		//Last line goes from rootnode->pNextNode to pLastEndNode

		//Now set the Forward-Pointers

		pNodeCount = rootnode->pNextNode;
		pHelpNode = nextrootnode->pNextNode;

		while(pNodeCount != NULL && pHelpNode != NULL)
		{
			if(pNodeCount->pNextNodeForward == NULL)
			{
				//No stop-node was inserted
				pNodeCount->pNextNodeForward = pHelpNode;
				pNodeCount = pNodeCount->pNextNodeRight;
				pHelpNode = pHelpNode->pNextNodeRight;
			}
			else
			{
				//Nodes of the current time line have left the interpolation area
				//to show this, StepForward(..) returns nodes with forward-pointers to stop-nodes
				while(pNodeCount != NULL && pNodeCount->pNextNodeForward != NULL)
				{
					pNodeCount = pNodeCount->pNextNodeRight;
				}

				pHelpNode = pHelpNode->pNextNodeRight;	//There is just ONE stop-node for all points with next points out of the interpolation area

				/*							|==|
				|  V
				O------> O   ----->	O		The stop-node in the middle shows, that the mesh is divided here because on some point
				^					^		in the past a node left the area. It is still a stop-node (s == -1.0)
				|					|
				|		|==|		|
				|		|  V		|
				CurrentTimeLine		O------> O   ----->	O		The point in the middle would lie out of the area and is a stop-node (s == -1.0)
				^	    ^  ^		^		
				|	   /	\		|
				|	  /		|		|
				|	 /		|		|
				LastTimeLine		O->	O ---->	O ---->	O		Advection of the two points in the middle leads to points out of the interpolation area
				^   ^       ^       ^		-> both of them point to stop-nodes
				|   |       |       |
				Direction of Interpolation
				*/
			}
		}

		RefineTimeLine(nextrootnode->pNextNode, pNextEndNode, rootnode->pNextNode, pLastEndNode);

		m_pTimelineList.push_back(nextrootnode->pNextNode);
	}


	//Now refinement of the current line
	std::list<TimeLineNode*>::iterator CurveStartNodeListIterator = m_pCurveStartNodeList.begin();
	std::list<TimeLineNode*>::iterator CurveStartNodeListEnd      = m_pCurveStartNodeList.end();

	for(; CurveStartNodeListIterator != CurveStartNodeListEnd; ++CurveStartNodeListIterator)
	{
		RefineIntegralCurve(*(CurveStartNodeListIterator));
	}

	return;
}

/** 
* Calculates the surface in the time-variant case                            
*/
template<>
inline void VveIntegralSurfaceTracer<VveDiscreteDataTyped<vtkDataSet>, IntegratorRK4<VveDiscreteDataTyped<vtkDataSet>>>::Execute()
{
	double dCurrentTime;	//The current interpolation time
	double dEndTime;

	VveTimeMapper* pTimeMapper = m_pInterpolator->GetTimeMapper();

	if(!pTimeMapper)
	{
		cout << "*** ERROR *** [VveIntegralSurfaceTracer::Execute] ";
		cout << "No Time Mapper found!" << endl;
		return;
	}

	dCurrentTime = pTimeMapper->GetSimulationTime(0.0);
	dEndTime   = pTimeMapper->GetSimulationTime(1.0);

	m_bSurfaceOutputIsReady  = false;
	m_bTimeLineOutputIsReady = false;
	m_bCurvesOutputIsReady   = false;

	m_pStopNodeList.clear();

	double dStepSize = m_pIntegrator->GetStepSize();

	//Refinement of the seed line
	TimeLineRoot *rootnode = RefineStartLine();

	TimeLineNode* pHelpNode = rootnode->pNextNode;
	TimeLineNode* pNodeCount = NULL;
	TimeLineNode* pStopNode = NULL;
	TimeLineNode* node = NULL;

	node = rootnode->pNextNode;
	while(node->pNextNodeRight != NULL)
		node = node->pNextNodeRight;

	//seed line is refined, now save the seed line in the list
	std::list<TimeLineNode*> nodelist;

	TimeLineRoot* nextrootnode = rootnode;

	m_pTimelineList.push_back(rootnode->pNextNode);

	TimeLineNode *pLastEndNode = NULL;
	TimeLineNode *pNextEndNode = NULL;


	//Counts the number of steps, so the user-defined number of max. steps is not reached
	int i = 1;

	while(dCurrentTime <= dEndTime && i <= m_iNumberOfSteps)
	{
#ifdef DEBUG
		cout << "Integration Step no. " << i << ", IntegrationTime: " << dCurrentTime << endl;
#endif

		rootnode = nextrootnode;

		pNodeCount = nextrootnode->pNextNode;

		nextrootnode = StepForward(nextrootnode->pNextNode, node);

		pLastEndNode = rootnode->pNextNode;
		pNextEndNode  = nextrootnode->pNextNode;

		while(pLastEndNode->pNextNodeRight != NULL)
			pLastEndNode = pLastEndNode->pNextNodeRight;

		while(pNextEndNode->pNextNodeRight != NULL)
			pNextEndNode = pNextEndNode->pNextNodeRight;

		node = pNextEndNode;

		//Current line goes from nextrootnode->pNextNode to pNextEndNode
		//Last line goes from rootnode->pNextNode to pLastEndNode

		//Now set the Forward-Pointers

		pNodeCount = rootnode->pNextNode;
		pHelpNode = nextrootnode->pNextNode;

		while(pNodeCount != NULL && pHelpNode != NULL)
		{
			if(pNodeCount->pNextNodeForward == NULL)
			{
				//No stop-node was inserted
				pNodeCount->pNextNodeForward = pHelpNode;
				pNodeCount = pNodeCount->pNextNodeRight;
				pHelpNode = pHelpNode->pNextNodeRight;
			}
			else
			{
				//Nodes of the current time line have left the interpolation area
				//to show this, StepForward(..) returns nodes with forward-pointers to stop-nodes
				while(pNodeCount != NULL && pNodeCount->pNextNodeForward != NULL)
				{
					pNodeCount = pNodeCount->pNextNodeRight;
				}

				pHelpNode = pHelpNode->pNextNodeRight;	//There is just ONE stop-node for all points with next points out of the interpolation area

				//For further information please refer to the ASCII-Art in the time-invariant execute
			}
		}



		RefineTimeLine(nextrootnode->pNextNode, pNextEndNode, rootnode->pNextNode, pLastEndNode);

		m_pTimelineList.push_back(nextrootnode->pNextNode);


		dCurrentTime += dStepSize;
		++i;
	}

	//Now Curve Refinement
	std::list<TimeLineNode*>::iterator CurveStartNodeListIterator = m_pCurveStartNodeList.begin();
	std::list<TimeLineNode*>::iterator CurveStartNodeListEnd      = m_pCurveStartNodeList.end();

	for(; CurveStartNodeListIterator != CurveStartNodeListEnd; ++CurveStartNodeListIterator)
	{
		RefineIntegralCurve(*(CurveStartNodeListIterator));
	}

	return;
}


/** 
* After Execute() has been called, the surface can be returned if
* the user wants so. If e.g. just the time lines are neccessary, it
* is sufficient to just call GetTimeLines() without the following
* calculations.
*/
template<typename TData, typename TIntegrator>
vtkPolyData* VveIntegralSurfaceTracer<TData, TIntegrator>::GetIntegralSurface()
{
	//If already generated, return now
	if(m_bSurfaceOutputIsReady)
		return m_pIntegralSurface;

#ifdef DEBUG
	cout << "VveIntegralSurfaceTracer::GetIntegralSurface()" << endl;
#endif


	std::list<TimeLineNode*>::iterator TimelineListIterator = m_pTimelineList.begin();
	std::list<TimeLineNode*>::iterator TimelineListEnd      = m_pTimelineList.end();

	std::list<double> liParametersDone;
	std::list<double>::iterator ParameterListIterator = liParametersDone.begin();
	std::list<double>::iterator ParameterListEnd      = liParametersDone.end();

	bool bParameterAlreadyDone = false;
	bool bRefinementReady      = false;

	double dOriginalStepSize = m_pIntegrator->GetStepSize();

	std::list<TimeLineNode*> liNodesToRefineList;
	std::list<TimeLineNode*>::iterator liNodesToRefineIterator = liNodesToRefineList.begin();
	std::list<TimeLineNode*>::iterator liNodesToRefineEnd      = liNodesToRefineList.end();

	TimeLineNode *pNodeCountRight = NULL;
	TimeLineNode *pNodeCountForward = NULL;


	vtkIdList *pPointIDs = vtkIdList::New();
	vtkPoints *pPoints   = vtkPoints::New();
	vtkCellArray *pLines = vtkCellArray::New();

	vtkDoubleArray *pTimeValues = vtkDoubleArray::New();
	vtkDoubleArray *pSValues	= vtkDoubleArray::New();
	vtkDoubleArray *pVelocity   = vtkDoubleArray::New();

	pTimeValues->SetName("time_values");
	pTimeValues->SetNumberOfComponents(1);

	pSValues->SetName("s_values");
	pSValues->SetNumberOfComponents(1);

	pVelocity->SetName("velocity");
	pVelocity->SetNumberOfComponents(3);


	pNodeCountRight = *(m_pTimelineList.begin());

	int iLeftNodeID, iRightNodeID;
	iLeftNodeID = pPoints->InsertNextPoint(pNodeCountRight->x, pNodeCountRight->y, pNodeCountRight->z);
	pTimeValues->InsertTuple1(iLeftNodeID, pNodeCountRight->t);
	pSValues->InsertTuple1(iLeftNodeID, pNodeCountRight->s);

	//Triangulate between two curves with TriangulateRibbon(..)
	while(pNodeCountRight != NULL && pNodeCountRight->pNextNodeRight != NULL)
	{
		iRightNodeID = pPoints->InsertNextPoint(pNodeCountRight->pNextNodeRight->x, pNodeCountRight->pNextNodeRight->y, pNodeCountRight->pNextNodeRight->z);
		pTimeValues->InsertTuple1(iRightNodeID, pNodeCountRight->t);
		pSValues->InsertTuple1(iRightNodeID, pNodeCountRight->s);

		TriangulateRibbon(pNodeCountRight, iLeftNodeID, 
			pNodeCountRight->pNextNodeRight, 
			iRightNodeID, pLines, pPoints, pTimeValues, pSValues);

		iLeftNodeID = iRightNodeID;
		pNodeCountRight = pNodeCountRight->pNextNodeRight;
	}


	m_pIntegralSurface->SetPoints(pPoints);
	pPoints->Delete();

	m_pIntegralSurface->SetPolys(pLines);
	pLines->Delete();

	m_pIntegralSurface->GetPointData()->AddArray(pTimeValues);
	m_pIntegralSurface->GetPointData()->AddArray(pSValues);
	pTimeValues->Delete();

	m_pIntegrator->SetStepSize(dOriginalStepSize);

	return m_pIntegralSurface;
}

/**
* Just returns the integral curves as a vtkPolyData
*/
template<typename TData, typename TIntegrator>
vtkPolyData* VveIntegralSurfaceTracer<TData, TIntegrator>::GetIntegralCurves()
{
	if(m_bCurvesOutputIsReady)
		return m_pIntegralCurves;

#ifdef DEBUG
	cout << "VveIntegralSurfaceTracer::GetIntegralCurves()" << endl;
#endif

	vtkIdList *pLines = vtkIdList::New();
	vtkPoints *pPoints = vtkPoints::New();
	vtkCellArray *pCurves = vtkCellArray::New();

	TimeLineNode *pNodeCount = NULL;

	vtkDoubleArray *pVelocity = vtkDoubleArray::New();
	pVelocity->SetName("velocity");
	pVelocity->SetNumberOfComponents(3);

	vtkDoubleArray *pTimeValues = vtkDoubleArray::New();
	pTimeValues->SetName("time_values");
	pTimeValues->SetNumberOfComponents(1);

	vtkDoubleArray *pSValues = vtkDoubleArray::New();
	pSValues->SetName("curve_indices");
	pSValues->SetNumberOfComponents(1);

	double pos[4];
	double res[4];


	std::list<TimeLineNode*>::iterator CurveListIterator = m_pCurveStartNodeList.begin();
	std::list<TimeLineNode*>::iterator CurveListEnd      = m_pCurveStartNodeList.end();

	int id;
	int iNumberOfLines = 0;
	bool bLineReady = false;

	cout << "Number of Curves: " << m_pCurveStartNodeList.size() << endl;


	for(; CurveListIterator != CurveListEnd; CurveListIterator++)
	{

		pNodeCount = *(CurveListIterator);

		++iNumberOfLines;

		bLineReady = false;		//Forward-pointer of a stop-node points to the node itself, so an endless repetition of the
		//loop could happen

		while(pNodeCount != NULL && !bLineReady)
		{
			if(pNodeCount->s == -1.0)
			{
				bLineReady = true;
			}
			else
			{
				id = pPoints->InsertNextPoint(pNodeCount->x, pNodeCount->y, pNodeCount->z);
				pLines->InsertNextId(id);

				pos[0] = pNodeCount->x;
				pos[1] = pNodeCount->y;
				pos[2] = pNodeCount->z;
				pos[3] = pNodeCount->t;

				m_pInterpolator->Interpolate(pos, res);

				pVelocity->InsertTuple3(id, res[0], res[1], res[2]);
				pTimeValues->InsertTuple1(id, pos[3]);
				pSValues->InsertTuple1(id, pNodeCount->s);

				pNodeCount = pNodeCount->pNextNodeForward;
			}
		}

		pCurves->InsertNextCell(pLines);
		pLines->Reset();
	}


	//All curves are ready; now set the interpolated arrays and return the vtkPolyData
	m_pIntegralCurves->SetPoints(pPoints);
	m_pIntegralCurves->SetLines(pCurves);

	m_pIntegralCurves->GetPointData()->AddArray(pVelocity);
	m_pIntegralCurves->GetPointData()->AddArray(pTimeValues);
	m_pIntegralCurves->GetPointData()->AddArray(pSValues);

	m_bCurvesOutputIsReady = true;

#ifdef DEBUG
	cout << iNumberOfLines << " Lines Done" << endl;
#endif


	return m_pIntegralCurves;
}


/** 
* Just returns the time lines as a vtkPolyData
*/
template<typename TData, typename TIntegrator>
vtkPolyData* VveIntegralSurfaceTracer<TData, TIntegrator>::GetTimeLines()
{
#ifdef DEBUG
	cout << "VveIntegralSurfaceTracer::GetTimeLines()" << endl;
#endif


	//If already generated, return now
	if(m_bTimeLineOutputIsReady)
		return m_pTimeLines;

	vtkPoints* pPoints = vtkPoints::New();
	vtkCellArray* pLines = vtkCellArray::New();
	vtkIdList* pCell = vtkIdList::New();
	int id;

	std::list<TimeLineNode*> nodelist;
	TimeLineNode* node = new TimeLineNode;

	vtkDoubleArray* pVelocity = vtkDoubleArray::New();
	pVelocity->SetName("velocity");
	pVelocity->SetNumberOfComponents(3);

	vtkDoubleArray* pTimeValues = vtkDoubleArray::New();
	pTimeValues->SetName("time_values");
	pTimeValues->SetNumberOfComponents(1);

	std::list<TimeLineNode*>::iterator TimelineListIterator = m_pTimelineList.begin();
	std::list<TimeLineNode*>::iterator TimelineListEnd      = m_pTimelineList.end();

	for(; TimelineListIterator != TimelineListEnd; ++TimelineListIterator)
	{
		node = (*TimelineListIterator);

		while(node != NULL)
		{
			if(node->s != -1.0) 
			{
				//Current node is no stop-node
				id = pPoints->InsertNextPoint(node->x, node->y, node->z);
				pCell->InsertNextId(id);

				double res[4];
				double pos[4];
				pos[0] = node->x;
				pos[1] = node->y;
				pos[2] = node->z;
				pos[3] = node->dTimeLineValue;
				m_pInterpolator->Interpolate(pos, res);
				pVelocity->InsertTuple3(id, res[0], res[1], res[2]);
				pTimeValues->InsertTuple1(id, pos[3]);
			}
			else
			{
				//Current node is a stop-node
				//Finish the part of the line and start a new one of the same time line

				pLines->InsertNextCell(pCell);
				pCell->Reset();
			}

			node = node->pNextNodeRight;
		}

		pLines->InsertNextCell(pCell);

		pCell->Reset();
	}

	m_pTimeLines->SetPoints(pPoints);
	m_pTimeLines->SetLines(pLines);

	m_pTimeLines->GetPointData()->AddArray(pVelocity);
	m_pTimeLines->GetPointData()->AddArray(pTimeValues);

	pPoints->Delete();
	pCell->Delete();
	pLines->Delete();

	m_bTimeLineOutputIsReady = true;
	return m_pTimeLines;
}

template<typename TData, typename TIntegrator>
void VveIntegralSurfaceTracer<TData, TIntegrator>::RefineTimeLine(TimeLineNode* pStartNode, 
																  TimeLineNode* pEndNode, 
																  TimeLineNode* pLastTimeLineStartNode, 
																  TimeLineNode* pLastTimeLineEndNode)
{
	//Test the predicate QRefine for every point in the line and refine if necessary

	TimeLineNode *pHelpNode = NULL;

	//Iterator for the for-block
	TimeLineNode *pNodeCount = pStartNode;

	//List for the parameter values where the line has to be refined
	std::list<double> dNewParameterList;
	double dNewParameter;
	double si0, si1, si2;

	bool bRefinementReady = false;

	while (!bRefinementReady)
	{
		bRefinementReady = true;
		pNodeCount = pStartNode;

		while(pNodeCount->pNextNodeRight !=  NULL && pNodeCount->pNextNodeRight != pEndNode)
		{
			if(PredicateQdiscont(pNodeCount))
			{
				// RefineLine rekursiv aufrufen
				// Achtung: DAS INTERVALL WIRD NICHT GESPLITTET!
				// Es wird nur daf�r gesorgt, dass nicht in der N�he von Unstetigkeiten ein Refinement ausgef�hrt wird, da
				// hier evtl. keine Konvergenz erreichbar ist!
			}

			if(PredicateQrefineTimeLine(pNodeCount))
			{
				bRefinementReady = false;

				//Refinement is necessary -> Insert a new node
				si0 = pNodeCount->s;
				si1 = pNodeCount->pNextNodeRight->s;
				si2 = pNodeCount->pNextNodeRight->pNextNodeRight->s;

				dNewParameter = 0.5 * (si0 + si1);
				dNewParameterList.remove(dNewParameter);
				dNewParameterList.push_back(dNewParameter);

				dNewParameter = 0.5 * (si1 + si2);
				dNewParameterList.remove(dNewParameter);
				dNewParameterList.push_back(dNewParameter);
			}

			pNodeCount = pNodeCount->pNextNodeRight;
		}

		pNodeCount = pLastTimeLineStartNode;

		while(!dNewParameterList.empty())
		{
			//Get the f(i) of the new inserted parameter values
			double dParameter = dNewParameterList.front();
			dNewParameterList.pop_front();

			//Get the last node from the old time line with a smaller parameter value
			pNodeCount = pLastTimeLineStartNode;
			while(pNodeCount->pNextNodeRight->s < dParameter)
			{
				pNodeCount = pNodeCount->pNextNodeRight;
			}

			pHelpNode = new TimeLineNode;

			double weight = (dParameter - pNodeCount->s) / (pNodeCount->pNextNodeRight->s - pNodeCount->s);
			double pos[4];

			pos[0] = weight * pNodeCount->pNextNodeRight->x + ((1 - weight) * pNodeCount->x);
			pos[1] = weight * pNodeCount->pNextNodeRight->y + ((1 - weight) * pNodeCount->y);
			pos[2] = weight * pNodeCount->pNextNodeRight->z + ((1 - weight) * pNodeCount->z);
			pos[3] = pNodeCount->dTimeLineValue;

			if(m_pIntegrator->NextStep(pos, pos))
			{
				pHelpNode->x = pos[0];
				pHelpNode->y = pos[1];
				pHelpNode->z = pos[2];
				pHelpNode->t = pNodeCount->t + m_pIntegrator->GetStepSize();
				pHelpNode->dTimeLineValue = pStartNode->dTimeLineValue;
				pHelpNode->s = dParameter;

				//Save the start node in the list for use in the DeleteNode()-function
				m_pCurveStartNodeList.push_back(pHelpNode);

				//Now pHelpNode is ready and can be inserted into the current time line
				pNodeCount = pStartNode;
				while(pNodeCount->pNextNodeRight->s < dParameter)
					pNodeCount = pNodeCount->pNextNodeRight;

				pHelpNode->pNextNodeRight = pNodeCount->pNextNodeRight;
				pNodeCount->pNextNodeRight = pHelpNode;

				pHelpNode = new TimeLineNode;
			}
			else
				bRefinementReady = true;
		}
	}
}

template<typename TData, typename TIntegrator>
void VveIntegralSurfaceTracer<TData, TIntegrator>::RefineIntegralCurve(TimeLineNode* pStartNode)
{
#ifdef DEBUG
	cout << "Refinement of a new line, Parameter: " << pStartNode->s << endl;
#endif

	bool bRefinementReady = false;

	TimeLineNode *pNodeCount = NULL;
	TimeLineNode *pHelpNode = NULL;
	TimeLineNode *pInsertNode = NULL;

	std::list<TimeLineNode*> liNodeList;

	std::list<TimeLineNode*>::iterator NodeListIterator = liNodeList.begin();
	std::list<TimeLineNode*>::iterator NodeListEnd      = liNodeList.end();

	double dOriginalStepSize = m_pIntegrator->GetStepSize();
	double pos[4];
	double res[4];

	int iIterationCount = 0;

	while(!bRefinementReady && iIterationCount < 50)	//Max. 50 refinement-steps 
		//to avoid iteration until infinity on unsteady points
	{

		++iIterationCount;
		bRefinementReady = true;
		pNodeCount = pStartNode;
		liNodeList.clear();

		while(pNodeCount != NULL && pNodeCount->pNextNodeForward != pNodeCount)
		{
			if(PredicateQrefineIntegralCurve(pNodeCount))
			{
				bRefinementReady = false;
				liNodeList.push_back(pNodeCount);
			}

			pNodeCount = pNodeCount->pNextNodeForward;
		}

		NodeListIterator = liNodeList.begin();
		NodeListEnd      = liNodeList.end();

		m_pIntegrator->SetStepSize(m_pIntegrator->GetStepSize() * 0.5);


		//Refinement is ready, but the last node points forward to itself -> break
		if(NodeListIterator == NodeListEnd)
			bRefinementReady = true;

		for(; NodeListIterator != NodeListEnd; ++NodeListIterator)
		{
			pHelpNode = *(NodeListIterator);

			if(m_pIntegrator->GetStepSize() == 0.001)
				bRefinementReady = true;

			pos[0] = pHelpNode->x;
			pos[1] = pHelpNode->y;
			pos[2] = pHelpNode->z;
			pos[3] = pHelpNode->t;

			m_pIntegrator->NextStep(pos, res);

			pInsertNode = new TimeLineNode;

			pInsertNode->x = res[0];
			pInsertNode->y = res[1];
			pInsertNode->z = res[2];
			pInsertNode->dTimeLineValue = pHelpNode->dTimeLineValue;	//TimeValue is the same between two time lines
			pInsertNode->t = pHelpNode->t + m_pIntegrator->GetStepSize();	//t is the exactly time value
			pInsertNode->s = pHelpNode->s;
			pInsertNode->pNextNodeForward = pHelpNode->pNextNodeForward;

			pHelpNode->pNextNodeForward = pInsertNode;
		}

	}

	m_pIntegrator->SetStepSize(dOriginalStepSize);

	return;
}


/** 
* Takes a time line and advects every point one step forward.
* Refinement of the next line is done in RefineTimeLine(..).
*/
template<typename TData, typename TIntegrator>
TimeLineRoot* VveIntegralSurfaceTracer<TData, TIntegrator>::StepForward(TimeLineNode* pStartNode, 
																		TimeLineNode* pEndNode)
{
	//Integrate every point of the list one step forward

#ifdef DEBUG
	cout << "VveIntegralSurfaceTracer::StepForward(..)" << endl;
#endif


	TimeLineNode *pHelpNode = NULL;

	//Iterator for the for-block
	TimeLineNode *pNodeCount = pStartNode;

	//Time line for the next step
	std::list<TimeLineNode*> nodelist;

	//Stop Node for those curves who leave the interpolation area
	TimeLineNode *pStopNode = NULL;

	//For the integration step
	double pos[4];
	double res[4];
	double dStepSize = m_pIntegrator->GetStepSize();

	double dEndNodeSValue = pEndNode->s;

	bool bFrameLeft = false;

	while(pNodeCount != NULL)
	{
		bFrameLeft = false;

		if(pNodeCount->pNextNodeForward != pNodeCount)
		{
			//Current node is no stop-node -> integrate one step forward
			pos[0] = pNodeCount->x;
			pos[1] = pNodeCount->y;
			pos[2] = pNodeCount->z;
			pos[3] = pNodeCount->t;

			bFrameLeft = !(m_pIntegrator->NextStep(pos, res));
		}
		else
		{
			//Current node IS a stop node
#ifdef DEBUG
			cout << "Stop-Node found!" << endl;
#endif

			bFrameLeft = true;

			pos[0] = pos[1] = pos[2] = 0.0;
			pos[3] = pNodeCount->t;
		}

		if(pNodeCount == pEndNode && pEndNode->s == -1.0)
		{
			bFrameLeft = true;
		}

		if(!bFrameLeft)
		{
			//New node lies in the interpolation area
			pHelpNode = new TimeLineNode;
			pHelpNode->s = pNodeCount->s;
			pHelpNode->x = res[0];
			pHelpNode->y = res[1];
			pHelpNode->z = res[2];
			pHelpNode->dTimeLineValue = pNodeCount->dTimeLineValue + dStepSize;
			pHelpNode->bExactlyOnTimeLine = true;
			pHelpNode->t = pos[3] + dStepSize;

			nodelist.push_back(pHelpNode);

			pNodeCount = pNodeCount->pNextNodeRight;
		}
		else
		{
			//New node would lie out of the area

			pStopNode = new TimeLineNode;
			pStopNode->x = pStopNode->y = pStopNode->z = 0.0;
			pStopNode->dTimeLineValue = pNodeCount->dTimeLineValue + dStepSize;
			pStopNode->t = pNodeCount->t + dStepSize;
			pStopNode->pNextNodeForward = pStopNode;	//forward-pointer shows to the node itself
			pStopNode->s = -1.0;		//s == -1.0 shows that the node is a stop-node
			pStopNode->bExactlyOnTimeLine = true;

			while(pNodeCount != NULL && bFrameLeft && pNodeCount->pNextNodeRight != NULL && pNodeCount->s <= pEndNode->s)
			{
				pNodeCount->pNextNodeForward = pStopNode;

				pos[0] = pNodeCount->pNextNodeRight->x;
				pos[1] = pNodeCount->pNextNodeRight->y;
				pos[2] = pNodeCount->pNextNodeRight->z;
				pos[3] = pNodeCount->pNextNodeRight->t;

				bFrameLeft = !(m_pIntegrator->NextStep(pos, res));

				pNodeCount = pNodeCount->pNextNodeRight;
			}

			nodelist.push_back(pStopNode);
			m_pStopNodeList.push_back(pStopNode);

			//Some conditions for breaks

			//The last curve is ready
			if(pNodeCount->s == 100.0)
				break;
			if(pNodeCount == NULL)
				break;
			if(pNodeCount->pNextNodeRight == NULL)
				break;

			//If current node is a stop-node, the incrementation has still to be done
			if(pEndNode->s == -1.0)
				pNodeCount = pNodeCount->pNextNodeRight;

		}	
	}

	//Now nodelist contains the next time line, but refinement may be necessary
	//Attention: The pNext-Pointers in the list are NULL-Pointers!


	//Build a list with a root node as return value
	TimeLineRoot *rootnode = new TimeLineRoot;
	rootnode->pNextNode = nodelist.front();
	nodelist.pop_front();

	pNodeCount = rootnode->pNextNode;

	while(!nodelist.empty())
	{
		pNodeCount->pNextNodeRight = nodelist.front();
		nodelist.pop_front();
		pNodeCount = pNodeCount->pNextNodeRight;
	}

	return rootnode;
}

/**
* Tests whether there is a discontinuity, what means that
* refinement may be done infinitely many times without convergence
* to a 'good' result. Not necessary without the angle-criterion, but
* may possibly implemented later.
*/
template<typename TData, typename TIntegrator>
bool VveIntegralSurfaceTracer<TData, TIntegrator>::PredicateQdiscont(TimeLineNode* pStartNode)
{
	return false;
}

template<typename TData, typename TIntegrator>
bool VveIntegralSurfaceTracer<TData, TIntegrator>::PredicateQrefineTimeLine(TimeLineNode* pStartNode)
{
	TimeLineNode* node0 = pStartNode;
	TimeLineNode* node1;
	TimeLineNode* node2;

	if(node0->pNextNodeRight)
		node1 = node0->pNextNodeRight;
	else
		return false;

	if(node1->pNextNodeRight)
		node2 = node1->pNextNodeRight;
	else
		return false;

	//No refinement possible if one of the nodes is a stop-node
	if(node0->pNextNodeForward != NULL && node0->pNextNodeForward == node0)
		return false;
	if(node1->pNextNodeForward != NULL && node1->pNextNodeForward == node1)
		return false;
	if(node2->pNextNodeForward != NULL && node2->pNextNodeForward == node2)
		return false;

	//Calculation of the triangle area
	double area;
	double x1, x2, x3, y1, y2, y3, z1, z2, z3;
	double xresult, yresult, zresult;
	double dDistNode0Node1, dDistNode1Node2, dDistNode0Node2;

	x1 = node1->x - node0->x;
	y1 = node1->y - node0->y;
	z1 = node1->z - node0->z;

	x2 = node2->x - node1->x;
	y2 = node2->y - node1->y;
	z2 = node2->z - node1->z;

	x3 = node2->x - node0->x;
	y3 = node2->y - node0->y;
	z3 = node2->z - node0->z;

	//Calculate the inter-node distances
	dDistNode0Node1 = sqrt(x1 * x1 + y1 * y1 + z1 * z1);
	dDistNode0Node2 = sqrt(x3 * x3 + y3 * y3 + z3 * z3);
	dDistNode1Node2 = sqrt(x2 * x2 + y2 * y2 + z2 * z2);

	//Calculate the standard vector product
	xresult = y1 * z2 - z1 * y2;
	yresult = z1 * x2 - x1 * z2;
	zresult = x1 * y2 - y1 * x2;

	//Area of the triangle spanned by node0, node1 and node2
	area =  0.5 * sqrt(xresult * xresult + yresult * yresult + zresult * zresult);


	//Estimation of the node distance dist(node0, node2) already done

	//Calculate the cos of the angle between (node0, node1) and (node1, node2)
	//May be used later, but then also an implementation of PredicateQDiscont(..) may be necessary!
	//double dCosAngle = (pow(dDistNode1Node2, 2) + 
	//pow(dDistNode0Node1, 2) - pow(dDistNode0Node2, 2)) / (2.0 * dDistNode1Node2 * dDistNode0Node1);

	return dDistNode0Node2 > m_dDistanceThreshold || area > m_dAreaThreshold; // || (acos(dCosAngle) < 2.0 && dDistNode0Node1 > (0.001 * m_dDistanceThreshold) && dDistNode0Node2 > (0.001 * m_dDistanceThreshold));


}


/** Triangulates between two curves. If a new curve starts at any point
* between those curves, the function is called recursively between
* all those curves.
*/
template<typename TData, typename TIntegrator>
void VveIntegralSurfaceTracer<TData, TIntegrator>::TriangulateRibbon(TimeLineNode *pLeftNode, 
																	 int iLeftNodeID, 
																	 TimeLineNode *pRightNode, 
																	 int iRightNodeID, 
																	 vtkCellArray *pTriangles, 
																	 vtkPoints *pPoints, 
																	 vtkDoubleArray *pTimeValues,
																	 vtkDoubleArray *pSValues)
{
	vtkIdList *t = vtkIdList::New();
	int iLeftID, iRightID;

	//Stop, if one of the nodes has no following node
	if(pLeftNode->pNextNodeForward == NULL || pLeftNode->s == -1.0 || pRightNode->pNextNodeForward == NULL || pRightNode->s == -1.0)
	{
		return;
	}


	//At least one follower is a stop node
	if(pLeftNode->pNextNodeForward->s == -1.0 || pRightNode->pNextNodeForward->s == -1.0)
		return;


	//Both nodes have valid followers, but at least one of them could lie on the next time line

	double len1, len2;	//Length of the two diagonals
	double x1, y1, z1, x2, y2, z2, x3, y3, z3;

	x1 = pLeftNode->x;
	y1 = pLeftNode->y;
	z1 = pLeftNode->z;

	x2 = pRightNode->x;
	y2 = pRightNode->y;
	z2 = pRightNode->z;

	TimeLineNode *pNodeCount;
	TimeLineNode *pLastNode;


	if(pLeftNode->dTimeLineValue == pLeftNode->pNextNodeForward->dTimeLineValue && 
		pRightNode->dTimeLineValue == pRightNode->pNextNodeForward->dTimeLineValue && 
		pLeftNode->dTimeLineValue == pRightNode->dTimeLineValue)
	{
		//No new curve starts between the current curves
		x3 = pLeftNode->pNextNodeForward->x;
		y3 = pLeftNode->pNextNodeForward->y;
		z3 = pLeftNode->pNextNodeForward->z;

		len1 = sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2) + (z3 - z2) * (z3 - z2));

		x3 = pRightNode->pNextNodeForward->x;
		y3 = pRightNode->pNextNodeForward->y;
		z3 = pRightNode->pNextNodeForward->z;

		len2 = sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1) + (z3 - z1) * (z3 - z1));

		//Remember: Shortest-Diagonal-Approach!
		if(len1 < len2)
		{
			iLeftID = pPoints->InsertNextPoint(pLeftNode->pNextNodeForward->x, 
				pLeftNode->pNextNodeForward->y, 
				pLeftNode->pNextNodeForward->z);
			pTimeValues->InsertTuple1(iLeftID, pLeftNode->pNextNodeForward->t);
			pSValues->InsertTuple1(iLeftID, pLeftNode->pNextNodeForward->s);

			t->InsertNextId(iLeftNodeID);
			t->InsertNextId(iRightNodeID);
			t->InsertNextId(iLeftID);

			pTriangles->InsertNextCell(t);

			t->Delete();

			TriangulateRibbon(pLeftNode->pNextNodeForward, 
							  iLeftID, pRightNode, iRightNodeID, 
							  pTriangles, pPoints, pTimeValues, pSValues);

			return;
		}
		else
		{
			iRightID = pPoints->InsertNextPoint(pRightNode->pNextNodeForward->x, 
												pRightNode->pNextNodeForward->y, 
												pRightNode->pNextNodeForward->z);
			pTimeValues->InsertTuple1(iRightID, pRightNode->pNextNodeForward->t);
			pSValues->InsertTuple1(iRightID, pRightNode->pNextNodeForward->s);

			t->InsertNextId(iLeftNodeID);
			t->InsertNextId(iRightNodeID);
			t->InsertNextId(iRightID);

			pTriangles->InsertNextCell(t);

			t->Delete();

			TriangulateRibbon(pLeftNode, iLeftNodeID, pRightNode->pNextNodeForward, 
							  iRightID, pTriangles, pPoints, pTimeValues, pSValues);

			return;
		}
	}
	else
	{
		//If one of the following nodes lies on a new time line, fill with triangles until
		//on both curves the new time line is reached

		if(pLeftNode->dTimeLineValue < pLeftNode->pNextNodeForward->dTimeLineValue && 
		   pRightNode->dTimeLineValue < pRightNode->pNextNodeForward->dTimeLineValue)
		{
			//Both following nodes lie on a new time line
			pNodeCount = pLeftNode->pNextNodeForward;
			pLastNode  = pNodeCount;

			iLeftID = pPoints->InsertNextPoint(pLeftNode->pNextNodeForward->x, 
											   pLeftNode->pNextNodeForward->y, 
											   pLeftNode->pNextNodeForward->z);
			
			pTimeValues->InsertTuple1(iLeftID, pLeftNode->pNextNodeForward->t);
			pSValues->InsertTuple1(iLeftID, pLeftNode->pNextNodeForward->s);

			t->InsertNextId(iLeftNodeID);
			t->InsertNextId(iRightNodeID);
			t->InsertNextId(iLeftID);

			pTriangles->InsertNextCell(t);

			while(pNodeCount->s < pRightNode->s && 
				  pNodeCount->pNextNodeRight != NULL)
			{
				pLastNode  = pNodeCount;
				pNodeCount = pNodeCount->pNextNodeRight;

				t->Reset();
				iRightID = pPoints->InsertNextPoint(pNodeCount->x, 
					                                pNodeCount->y, 
													pNodeCount->z);
				pTimeValues->InsertTuple1(iRightID, pNodeCount->t);
				pSValues->InsertTuple1(iRightID, pNodeCount->s);

				t->InsertNextId(iRightNodeID);
				t->InsertNextId(iRightID);
				t->InsertNextId(iLeftID);

				pTriangles->InsertNextCell(t);

				TriangulateRibbon(pLastNode, iLeftID, pNodeCount, iRightID, 
					              pTriangles, pPoints, pTimeValues, pSValues);

				iLeftID = iRightID;
			}
		}
		else if(pLeftNode->dTimeLineValue < pLeftNode->pNextNodeForward->dTimeLineValue && 
			    pRightNode->dTimeLineValue == pRightNode->pNextNodeForward->dTimeLineValue)
		{
			//Just the following node of the LEFT curve lies on a new time line
			pNodeCount = pRightNode->pNextNodeForward;
				
			iRightID = pPoints->InsertNextPoint(pNodeCount->x, 
											    pNodeCount->y, 
												pNodeCount->z);
			pTimeValues->InsertTuple1(iRightID, pNodeCount->t);
			pSValues->InsertTuple1(iRightID, pNodeCount->s);

			t->Reset();

			t->InsertNextId(iLeftNodeID);
			t->InsertNextId(iRightNodeID);
			t->InsertNextId(iRightID);

			pTriangles->InsertNextCell(t);

			TriangulateRibbon(pLeftNode, iLeftNodeID, pNodeCount, iRightID, 
				              pTriangles, pPoints, pTimeValues, pSValues);

			return;
		}
		else if(pLeftNode->dTimeLineValue == pLeftNode->pNextNodeForward->dTimeLineValue && 
			    pRightNode->dTimeLineValue < pRightNode->pNextNodeForward->dTimeLineValue)
		{
			//Just the following node of the RIGHT node lies on a new time line
			pNodeCount = pLeftNode->pNextNodeForward;

			iLeftID = pPoints->InsertNextPoint(pNodeCount->x, 
											   pNodeCount->y, 
											   pNodeCount->z);
			pTimeValues->InsertTuple1(iLeftID, pNodeCount->t);
			pSValues->InsertTuple1(iLeftID, pNodeCount->s);

			t->Reset();

			t->InsertNextId(iRightNodeID);
			t->InsertNextId(iLeftID);
			t->InsertNextId(iLeftNodeID);

			pTriangles->InsertNextCell(t);

			TriangulateRibbon(pNodeCount, iLeftID, pRightNode, iRightNodeID, 
				              pTriangles, pPoints, pTimeValues, pSValues);

			return;
		}
	}
}


template<typename TData, typename TIntegrator>
bool VveIntegralSurfaceTracer<TData, TIntegrator>::PredicateQrefineIntegralCurve(TimeLineNode* pStartNode)
{
	//For the resampling of an integral curve: just the distance between nodes is important
	TimeLineNode* node0 = pStartNode;
	TimeLineNode* node1 = NULL;
	TimeLineNode* node2 = NULL;

	if(node0 == NULL)
		return false;

	if(node0->pNextNodeForward != NULL)
		node1 = node0->pNextNodeForward;
	else
		return false;

	if(node0->s == -1.0)
		return false;

	if(node1->s == -1.0)
		return false;

	double x, y, z;

	x = node1->x - node0->x;
	y = node1->y - node0->y;
	z = node1->z - node0->z;

	double distance = sqrt(x * x + y * y + z * z);

	return distance > m_dDistanceThreshold;
}

template<typename TData, typename TIntegrator>
bool VveIntegralSurfaceTracer<TData, TIntegrator>::PredicateQrefineIntegralCurve(double pos1[], double pos2[])
{
	//For the resampling of an integral curve: just the distance between two nodes is important
	double x, y, z;

	x = pos1[0] - pos2[0];
	y = pos1[1] - pos2[1];
	z = pos1[2] - pos2[2];

	double distance = sqrt(x * x + y * y + z * z);

	return distance > m_dDistanceThreshold;
}

#endif // Include guard.

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/