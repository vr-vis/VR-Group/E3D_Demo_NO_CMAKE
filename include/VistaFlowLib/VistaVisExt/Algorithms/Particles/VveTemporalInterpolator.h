/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VveTemporalInterpolator.H    RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*			- VveTemporalInterpolator										  */
/*                                                                            */
/*============================================================================*/


#ifndef _VVETEMPORALINTERPOLATOR_H
#define _VVETEMPORALINTERPOLATOR_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifdef WIN32
#pragma warning( disable : 4786 ) 
#pragma warning( disable : 4018 ) 
#endif

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkDataSet.h>
#include <vtkDataArray.h>
#include <vtkPointData.h>
#include <vtkGenericCell.h>
#include "../../Data/VveDiscreteDataTyped.h"

#include <cstdio>
#include <cassert>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Base class for temporal interpolation. This handles some general stuff
 * that are shared by all temporal interpolators.
 * 
 * Generally, the similar ideas as for spatial interpolators apply, as defined in
 * VveSpatialInterpolator.h  Specifically, all interpolator classes share the 
 * common interpolation interface:
 *
 * bool Interpolate(double *dIn, double *dOut);
 *
 * In contrast to the spatial interpolator, temporal interpolators do not
 * use cell locators themselves, though. However, the locator is a template parameter
 * because we need access to its type in order to create spatial interpolators.
 *
 * NOTE: For now, this interpolator is "specialized" to data of type
 *       VveDiscreteDataTyped<TRawVisData> i.e. to sequences of of discrete
 *       time steps. It, however, implements the temporal interpolation
 *		 w/o regard of the underlying raw data type. Therefore, this 
 *		 "specialization" should be widely applicable.
 *
 * @date		March 2012
 */
template<typename TRawVisData, 
		 typename TLocator=VveDefaultCellLocator<TRawVisData>,
		 typename TSpatialInterpolator=VveSpatialInterpolator<TRawVisData, TLocator> > 

class VveTemporalInterpolator
{
public:
	/**
	 * typedefs for convenience
	 */
	typedef VveDiscreteDataTyped<TRawVisData> TTemporalData;
	/**
	*  Just a base class, no instances are allowed
	*/
	VveTemporalInterpolator();
	VveTemporalInterpolator(const std::vector<TLocator*> &vecLocators);

	virtual ~VveTemporalInterpolator();

	/**
	 * Tell the outside that we need two time steps to do interpolation
	 * and thus only rely on two distinct locator objects
	 */
	static size_t GetTemporalSupportSize();

	/**
	 * determine time frame for which we have valid data
	 */
	void GetValidTimeFrame(double dFrame[2]) const;
	
	/**
	 * Set the data object on which interpolation is carried out
	 */
	void SetData(TTemporalData * pData);
	/**
	 *  Getter for encapsulated underlying data field
	 */
	TTemporalData* GetData() const;
	
	/**
	 * Get the number of interpolated components in the result, i.e. the size
	 * of the array dOut that is required for save interpolation.
	 *
	 * NOTE: Strictly speaking this is only guaranteed to give you a correct 
	 *       result if 
	 *		 a) the data object is set 
	 *		 AND
	 *		 b1) the data object's first time step is valid 
	 *		 OR
	 *		 b2) at least one successful interpolation has been carried out.      
	 */
	size_t GetNumberOfResults() const;
	
	/**
	 * set the name of the field that is interpolated by this object
	 */
	void SetInterpolatedField(const std::string &strFieldName);
	/**
	 * retrieve the name of the field that is interpolated by this object
	 */
	std::string GetInterpolatedField() const;

	/**
	 * the interpolation method: take a 4D double point (x,y,z,t) in dIn
	 * and return the resulting interpolated scalar/vector/tensor in dOut.
	 *
	 * NOTE: It is assumed that dOut points to an array of suitable size.
	 *
	 * \return true iff interpolation worked and result is valid, 
	 *		   false otherwise.
	 */
	bool Interpolate(double *dIn, double *dOut);

protected:


private:
	/** store pointer to the underlying raw data */
	TTemporalData *m_pData;
	/** remember the field we want to interpolate */
	std::string m_strFieldName;
	/** keep track of the time indices to be interpolated */
	int m_arrTimeIndices[2];
	/** keep track of the sim times of the respective time steps */
	double m_arrSimTimes[2];
	/** 
	 * Keep track of whether we need to rewire the data. This is triggered
	 * in two conditions: first, when a new data set is provided and second
	 * whenever the time steps to be interpolated change
	 */
	bool m_bNeedDataUpdate;
	/** temporal storage for interpolation results */
	double m_arrInterpolResults[2][9];
	/**
	 * keep track of locators (if and only if we created them ourselves)
	 */
	TLocator *m_arrLocators[2];
	/** 
	 * hold two spatial interpolators of suitable type to delegate
	 * single time step interpolation.
	 */
	TSpatialInterpolator *m_arrInterpolators[2];
};

/************************************************************************/
/*						IMPLEMENTATION									*/
/************************************************************************/
template<typename TRawVisData, typename TLocator, typename TSpatialInterpolator>
VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::VveTemporalInterpolator()
	:	m_pData(NULL),	
		m_strFieldName("")
{
	m_arrTimeIndices[0] = -1;
	m_arrTimeIndices[1] = -1;
	
	m_arrSimTimes[0] = 0.0;
	m_arrSimTimes[1] = 0.0;
	
	for(int i=0; i<9; ++i)
	{
		m_arrInterpolResults[0][i] = 0.0;
		m_arrInterpolResults[1][i] = 0.0;
	}

	m_arrLocators[0] = new TLocator;
	m_arrLocators[1] = new TLocator;

	std::vector<TLocator*> vecLoc(1);
	vecLoc[0] = m_arrLocators[0];
	m_arrInterpolators[0] = new TSpatialInterpolator(vecLoc);
	vecLoc[0] = m_arrLocators[1];
	m_arrInterpolators[1] = new TSpatialInterpolator(vecLoc);
}

template<typename TRawVisData, typename TLocator, typename TSpatialInterpolator>
VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::VveTemporalInterpolator(const std::vector<TLocator*> &vecLocators)
	:	m_pData(NULL),	
		m_strFieldName("")
{
	m_arrTimeIndices[0] = -1;
	m_arrTimeIndices[1] = -1;

	m_arrSimTimes[0] = 0.0;
	m_arrSimTimes[1] = 0.0;

	for(int i=0; i<9; ++i)
	{
		m_arrInterpolResults[0][i] = 0.0;
		m_arrInterpolResults[1][i] = 0.0;
	}

	//DO NOT store the locators 
	//==> we haven't allocated them so we don't need to keep track of them !
	m_arrLocators[0] = NULL;
	m_arrLocators[1] = NULL;

	std::vector<TLocator*> vecLoc(1);
	vecLoc[0] = vecLocators[0];
	m_arrInterpolators[0] = new TSpatialInterpolator(vecLoc);
	vecLoc[0] = vecLocators[1];
	m_arrInterpolators[1] = new TSpatialInterpolator(vecLoc);
}

template<typename TRawVisData, typename TLocator, typename TSpatialInterpolator>
VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::~VveTemporalInterpolator()
{
	delete m_arrInterpolators[0];
	delete m_arrInterpolators[1];

	delete m_arrLocators[0];
	delete m_arrLocators[1];
}

template<typename TRawVisData, typename TLocator, typename TSpatialInterpolator>
inline size_t VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::GetTemporalSupportSize()
{
	return 2;
}


template<typename TRawVisData, typename TLocator,typename TSpatialInterpolator>
inline void VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::GetValidTimeFrame(double dFrame[2]) const
{
	if(m_pData == NULL)
	{
		dFrame[0] = 0.0;
		dFrame[1] = 0.0;
	}
	dFrame[0] = m_pData->GetTimeMapper()->GetSimulationTime(0.0);
	dFrame[1] = m_pData->GetTimeMapper()->GetSimulationTime(1.0);
}


template<typename TRawVisData, typename TLocator, typename TSpatialInterpolator>
inline void VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::SetData( VveDiscreteDataTyped<TRawVisData> *pData )
{
	m_pData = pData;

	m_arrTimeIndices[0] = -1;
	m_arrTimeIndices[1] = -1;

	m_arrSimTimes[0] = 0.0;
	m_arrSimTimes[1] = 0.0;

	//tell the sub-locators about this
	m_arrInterpolators[0]->SetData(pData);
	m_arrInterpolators[1]->SetData(pData);

	m_bNeedDataUpdate = true;
}

template<typename TRawVisData, typename TLocator, typename TSpatialInterpolator>
inline VveDiscreteDataTyped<TRawVisData>* VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::GetData() const
{
	return m_pData;
}

template<typename TRawVisData, typename TLocator, typename TSpatialInterpolator>
inline size_t VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::GetNumberOfResults() const
{
	assert(m_arrInterpolators[0]->GetNumberOfResults() ==
		   m_arrInterpolators[1]->GetNumberOfResults());
	
	return m_arrInterpolators[0]->GetNumberOfResults();
}


template<typename TRawVisData, typename TLocator, typename TSpatialInterpolator>
void VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::SetInterpolatedField(const std::string &strFieldName)
{
	//just forward this info to the sub interpolators
	m_arrInterpolators[0]->SetInterpolatedField(strFieldName);
	m_arrInterpolators[1]->SetInterpolatedField(strFieldName);
	
	assert(m_arrInterpolators[0]->GetInterpolatedField() == 
		   m_arrInterpolators[1]->GetInterpolatedField());
}


template<typename TRawVisData, typename TLocator, typename TSpatialInterpolator>
std::string VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::GetInterpolatedField() const
{
	assert(m_arrInterpolators[0]->GetInterpolatedField() == 
		   m_arrInterpolators[1]->GetInterpolatedField());

	return m_arrInterpolators[0]->GetInterpolatedField();
}


template<typename TRawVisData, typename TLocator, typename TSpatialInterpolator>
bool VveTemporalInterpolator<TRawVisData, TLocator, TSpatialInterpolator>::Interpolate( double *dIn, double *dOut )
{
	//make sure we got valid data now!
	assert(m_pData != NULL);

	VveTimeMapper *pTM = m_pData->GetTimeMapper();
	double dSimTime = dIn[3];
	
	//determine position in time including interpolation weight
	double dVisTime = pTM->GetVisualizationTime(dSimTime);
	int iIdx1, iIdx2;
	double dTimeAlpha = pTM->GetWeightedTimeIndices(dVisTime, iIdx1, iIdx2);
	if(dTimeAlpha < 0.0 || iIdx1 < 0 || iIdx2 < 0)
	{
#ifdef DEBUG
		vstr::errp() <<"[VveTemporalInterpolator::Interpolate] Unable to find right time indices!"<<endl;
#endif
		return false;
	}

	//check if data or sim time frame have changed
	if( m_bNeedDataUpdate || 
		iIdx1 != m_arrTimeIndices[0] || 
		iIdx2 != m_arrTimeIndices[1])
	{
		int iLvlIdx1 = pTM->GetLevelIndex(iIdx1);
		int iLvlIdx2 = pTM->GetLevelIndex(iIdx2);

		//make sure these are valid!
		assert(iLvlIdx1 >= 0 && iLvlIdx1 < pTM->GetNumberOfTimeLevels());
		assert(iLvlIdx2 >= 0 && iLvlIdx2 < pTM->GetNumberOfTimeLevels());

		//fetch data
		TRawVisData *pData1 = m_pData->GetTypedLevelDataByLevelIndex(iLvlIdx1)->GetData();
		TRawVisData *pData2 = m_pData->GetTypedLevelDataByLevelIndex(iLvlIdx2)->GetData();

		//if we have either data not in memory ==> bail out!
		if(pData1 == NULL || pData2 == NULL)
		{
#ifdef DEBUG
			vstr::errp() << "[VveTemporalInterpolator::Interpolate] Data level not available!"<<endl;;
#endif
			return false;
		}
		
		//check if we can re-use one interpolator
		//usually we will move linearly through the data 
		//==> check if the new "lower" idx equals the old "upper" one
		if(iIdx1 == m_arrTimeIndices[1])
		{
			//make sure this one has the right data then!
			assert(m_arrInterpolators[1]->GetData() == pData1);
			//exchange
			TSpatialInterpolator *pTmp = m_arrInterpolators[0];
			//re-use "upper one"
			m_arrInterpolators[0] = m_arrInterpolators[1];
			//reconfigure old "lower one"
			m_arrInterpolators[1] = pTmp;
			m_arrInterpolators[1]->SetData(pData2);
		}
		else
		{
			//no reuse ==> reconfigure both interpolators
			m_arrInterpolators[0]->SetData(pData1);
			m_arrInterpolators[1]->SetData(pData2);
		}

		//finally assign the new time indices
		m_arrTimeIndices[0] = iIdx1;
		m_arrTimeIndices[1] = iIdx2;

		//fetch new sim times
		m_arrSimTimes[0] = pTM->GetSimulationTime(m_arrTimeIndices[0]);
		m_arrSimTimes[1] = pTM->GetSimulationTime(m_arrTimeIndices[1]);

		//remember not to force this in the next pass 
		m_bNeedDataUpdate = false;
	}

	//o.k. so the interpolators should be valid now
	//==> try to interpolate in both data sets
	if(!m_arrInterpolators[0]->Interpolate(dIn, m_arrInterpolResults[0]))
	{
		return false;
	}
	if(!m_arrInterpolators[1]->Interpolate(dIn, m_arrInterpolResults[1]))
	{
		return false;

	}

	//finally interpolate in time
	double dOMA = 1.0 - dTimeAlpha;
	const size_t iNumComps = this->GetNumberOfResults();
	for(register int i=0; i<iNumComps; ++i)
	{
		dOut[i] = dOMA * m_arrInterpolResults[0][i] + dTimeAlpha * m_arrInterpolResults[1][i];
	}

	//all seems to have worked...
	return true;
}


#endif //_VVETEMPORALINTERPOLATOR_H