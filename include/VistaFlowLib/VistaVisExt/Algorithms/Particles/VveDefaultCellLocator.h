/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VveCellLocator.H	         RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*			- VveDefaultCellLocatorBase										  */
/*			- VveDefaultCellLocator		                                      */
/*			- ...															  */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEDEFAULTCELLLOCATOR_H
#define _VVEDEFAULTCELLLOCATOR_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../../Data/VveDiscreteDataTyped.h"
#include "../../Tools/VveTimeMapper.h"

#include <vtkDataSet.h>
#include <vtkCell.h>
#include <vtkGenericCell.h>

#include <cassert>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * The default locator is the default strategy to locate a cell in a 3D data 
 * set for a given point in 3-space.
 * The locator uses template specialization in order to provide implementations
 * for different data types.
 *
 * All locators should share the same FindCell signature in order to make them 
 * usable by the interpolator classes:
 *		
 *		int FindCell(double *dPos[3], int &iNumCellPts, double *dWeights);
 *
 * By convention the returned int refers to the cell id in which dPos has
 * been located. -1 is returned in case of failure (i.e. dPos is outside the 
 * data). For subsequent interpolation iNumCellPts interpolation weights are
 * returned in dWeights. Note that these values are <bf>undefined</bf> if
 * the position is outside the grid, i.e. if -1 is returned.
 *
 * In addition to FindCell(...) specializations should be able to react to
 * changing input data in their own way. Therefore, they need to implement
 * 
 * void SetData(<data type>* pData);
 *
 */
template<typename TGrid> class VveDefaultCellLocatorBase
{
public:
	VveDefaultCellLocatorBase();
	virtual ~VveDefaultCellLocatorBase();

	TGrid* GetData() const;

protected:
	/**
	 * store cached results for the given position
	 */
	void SetCachedResult(const double dPos[3], const int iCellId, const int iNumCellPts, double* dWeights);
	/**
	 * check if the given position is cached and if so return cell id, iNumCellPts, and dWeights from
	 * the cache. In case of a cache miss (or a non-initialized cache), return -1 instead.
	 */
	int GetCachedResult(const double dPos[3], int &iNumCellPts, double* dWeights) const;
	/**
	 *  reset cached information to non-cached state.
	 */
	void ResetCache();

	/**
	 * helper func for straightforward early-out check against data set bounds
	 */
	bool IsInside(const double dPos[3], const double dBounds[6]) const;

	/**
	 * keep data members available for sub-classes
	 */
	TGrid *m_pData;
	int m_iLastCell;
	/** cache for last iteration result **/
	double m_dLastPos[3];
	int m_iLastCellNumPts;
	double m_dLastWeights[24];
};

/**
 * In order to be able to specialize, we need the correct general class first.
 * ==> Just inherit members and code from base class. Implementation is deferred to
 * specialization below.
 */
template<typename TGrid> class VveDefaultCellLocator : public VveDefaultCellLocatorBase<TGrid>
{
public:
protected:
private:
};


/**
 * SPECIALIZATION of point location for general vtkDataSets
 */
template<> 
class VveDefaultCellLocator<vtkDataSet> : public VveDefaultCellLocatorBase<vtkDataSet>
{
public:
	VveDefaultCellLocator();
	virtual ~VveDefaultCellLocator();

	/**
	 * Implement specialized version of SetData in order to reset cache locally.
	 */
	void SetData(vtkDataSet *pData);

	/**
	 * Build the locator.
	 * For the VTK specialization this amounts to calling FindCell once from 
	 * a single thread.
	 */
	void Build();
	
	/**
	 * Locate the cell which contains the given position
	 *
	 * NOTE: In order to handle different underlying data formats 
	 *       (e.g. vtkStructuredGrid, vtkImageData, VveCartesianGrid, ...)
	 *		 this method uses template specialization.
	 */
	int FindCell(double dPos[], int &iNumCellPts, double *dWeights);

protected:
	

private:
	/** vtkDataSet specific  members - avoid allocating these over and over again */
	vtkCell *m_pLastCell;
	vtkGenericCell *m_pGenCell;
	double m_dTolerance;
	int m_iSubId;
	double m_dPCoords[3];
};

/************************************************************************/
/* Implementation of VveDefaultCellLocatorBase                          */
/************************************************************************/
template<typename TGrid>
inline VveDefaultCellLocatorBase<TGrid>::VveDefaultCellLocatorBase()
	:	m_pData(NULL),
		m_iLastCell(-1),
		m_iLastCellNumPts(0)
{
}


template<typename TGrid>
inline VveDefaultCellLocatorBase<TGrid>::~VveDefaultCellLocatorBase()
{
}

template<typename TGrid>
inline TGrid* VveDefaultCellLocatorBase<TGrid>::GetData() const
{
	return m_pData;
}

template<typename TGrid>
inline void VveDefaultCellLocatorBase<TGrid>::SetCachedResult(const double dPos[3], 
													   const int iCellId, 
													   const int iNumCellPts, 
													   double *dWeights )
{
	memcpy(m_dLastPos, dPos, 3*sizeof(m_dLastPos[0]));
	m_iLastCell = iCellId;
	m_iLastCellNumPts = iNumCellPts;
	memcpy(m_dLastWeights, dWeights, iNumCellPts*sizeof(dWeights[0]));
}


template<typename TGrid>
inline int VveDefaultCellLocatorBase<TGrid>::GetCachedResult(const double dPos[3], 
															 int &iNumCellPts, 
															 double* dWeights ) const
{
	//for now only allow cached results if the position is bit-wise exactly the same!
	//this at least captures repeated querying of the same position by multiple
	//interpolators for different data fields.
	if( dPos[0] == m_dLastPos[0] &&
		dPos[1] == m_dLastPos[1] &&
		dPos[2] == m_dLastPos[2])
	{
		iNumCellPts = m_iLastCellNumPts;
		memcpy(dWeights, m_dLastWeights, iNumCellPts*sizeof(dWeights[0]));
		//NOTE: if we have no valid cached results, this will indicate it anyway, 
		//because in that case, m_iLastCell == -1. Consequently, there is no 
		//need to have an extra check on this
		return m_iLastCell;
	}

	//still here ==> cache miss
	return -1;
}


template<typename TGrid>
inline void VveDefaultCellLocatorBase<TGrid>::ResetCache()
{
	memset(m_dLastPos, 0, 3*sizeof(m_dLastPos[0]));
	m_iLastCell = -1;
	m_iLastCellNumPts = 0;
	memset(m_dLastWeights, 0, 24*sizeof(m_dLastWeights[0]));
}

template<typename TGrid>
inline bool VveDefaultCellLocatorBase<TGrid>::IsInside(const double dPos[3], const double dBounds[6]) const
{
	return	dPos[0] >= dBounds[0] && dPos[0] <= dBounds[1] &&
			dPos[1] >= dBounds[2] && dPos[1] <= dBounds[3] &&
			dPos[2] >= dBounds[4] && dPos[2] <= dBounds[5];
} 


/************************************************************************/
/* Implementation of specialization 
/* VveDefaultCellLocatorBase<vtkDataSet>
/************************************************************************/
inline VveDefaultCellLocator<vtkDataSet>::VveDefaultCellLocator()
	:	m_pLastCell(NULL),
		m_pGenCell(vtkGenericCell::New()),
		m_dTolerance(0.0),
		m_iSubId(0)
{
	m_dPCoords[0] = m_dPCoords[1] = m_dPCoords[2] = 0.0;
}

inline VveDefaultCellLocator<vtkDataSet>::~VveDefaultCellLocator()
{
	m_pGenCell->Delete();
}


//SPECIALIZED IMPLEMENTATION FOR DEFAULT VTK DATA SET
inline void VveDefaultCellLocator<vtkDataSet>::SetData(vtkDataSet *pData)
{
	m_pData = pData;
	//clear local info
	m_pLastCell = NULL;
	//clear base class info
	this->ResetCache();
}


inline void VveDefaultCellLocator<vtkDataSet>::Build()
{
	//because the first call to vtkDataSet::FindCell potentially alters the 
	//underlying data set's state make sure this is called exclusively 
	//by each thread
	//NOTE: It would be even better to just call it by one thread but omp single
	//		is not allowed within a parallel for loop.
	#pragma omp critical
	{

		//still here? ==> cache miss 
		vtkDataSet *pData = this->GetData();
		//dummy check ==> due to a bug in VTK we first check against the data set bounds here.
		double dBounds[6];
		pData->GetBounds(dBounds);
		//determine center as dummy query position
		double dPos[3] = {
			dBounds[1]-dBounds[0],
			dBounds[3]-dBounds[2],
			dBounds[5]-dBounds[4]
		};
		double dWeights[24];
		pData->FindCell(dPos, NULL, m_pGenCell, -1, m_dTolerance, m_iSubId, m_dPCoords, dWeights);
	}
}


//SPECIALIZED IMPLEMENTATION FOR DEFAULT VTK DATA SET CELL LOCATION
inline int VveDefaultCellLocator<vtkDataSet>::FindCell(double dPos[3], int &iNumCellPts, double *dWeights )
{
	int iCellId = this->GetCachedResult(dPos, iNumCellPts, dWeights);
	//cache hit? ==> done!
	if(iCellId >= 0)
		return iCellId;
	
	//still here? ==> cache miss 
	vtkDataSet *pData = this->GetData();
	
	
	//dummy check ==> due to a bug in VTK we first check against the data set bounds here.
	double dBounds[6];
	pData->GetBounds(dBounds);

	if(this->IsInside(dPos, dBounds))
	{
		if(m_iLastCell == -1)
		{
			//no last known cell ==> dumb search
			iCellId = pData->FindCell(dPos, NULL, m_pGenCell, -1, m_dTolerance, m_iSubId, m_dPCoords, dWeights);
		}
		else
		{
			//query the data with the last known cell as start point
			iCellId = pData->FindCell(dPos, m_pLastCell, m_pGenCell, m_iLastCell, m_dTolerance, m_iSubId, m_dPCoords, dWeights);
		}
		if(iCellId >= 0)
		{
			m_pLastCell = m_pData->GetCell(iCellId);
			assert(m_pLastCell != NULL);
			int iNumCellPts = m_pLastCell->GetNumberOfPoints();
			this->SetCachedResult(dPos, iCellId, iNumCellPts, dWeights);
			return iCellId;
		}
	}
	
	//complete miss ==> reset
	m_pLastCell = NULL;
	m_iLastCell = -1;
	this->ResetCache();

//#ifdef DEBUG
//	printf("*** ERROR *** [VveDefaultCellLocator<vtkDataSet>::FindCell] Position out of domain!\n");
//	printf("\tx= [%.3g, %.3g, %.3g]\n", dPos[0], dPos[1], dPos[2]);
//	printf("\tbounds= x in [%.3g, %.3g]; y in [%.3g, %.3g], z in [%.3g, %.3g]\n", 
//		dBounds[0], dBounds[1], dBounds[2], dBounds[3], dBounds[4], dBounds[5]);
//#endif

	return -1;
}



/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/


#endif //_VVEDEFAULTCELLLOCATOR_H