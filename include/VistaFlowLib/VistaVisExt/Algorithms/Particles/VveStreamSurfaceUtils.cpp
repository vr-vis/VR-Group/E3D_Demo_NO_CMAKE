/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveStreamSurfaceUtils.cpp	 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VveStreamSurfaceUtils.h"


/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
VveStreamSurfaceUtils::VveStreamSurfaceUtils()
{ }

VveStreamSurfaceUtils::~VveStreamSurfaceUtils()
{ }


void VveStreamSurfaceUtils::PushPoint( const double dPt[4], std::vector<double> &vecPts ) const
{
	vecPts.push_back(dPt[0]);
	vecPts.push_back(dPt[1]);
	vecPts.push_back(dPt[2]);
	vecPts.push_back(dPt[3]);
}


void VveStreamSurfaceUtils::PushMidPoint( const double dPt1[4], const double dPt2[4], std::vector<double> &vecPts ) const
{
	vecPts.push_back(0.5*(dPt1[0]+dPt2[0]));
	vecPts.push_back(0.5*(dPt1[1]+dPt2[1]));
	vecPts.push_back(0.5*(dPt1[2]+dPt2[2]));
	vecPts.push_back(0.5*(dPt1[3]+dPt2[3]));
}




/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

