/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveLineRefinementPreds.cpp	 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VveLineRefinementsPreds.h"

#include <cmath>



/*============================================================================*/
/* IMPLEMENTATION	---	VveGradientDiscontinuityPred						  */
/*============================================================================*/
VveGradientDiscontinuityPred::VveGradientDiscontinuityPred( const double dMaxGrad )
	: m_dMaxGrad2(dMaxGrad*dMaxGrad)
{

}

VveGradientDiscontinuityPred::~VveGradientDiscontinuityPred()
{

}

bool VveGradientDiscontinuityPred::operator()( const double dPt1[4], const double dPt2[4], const double dPt3[4] ) const
{
	double dDelta1[3] = 
	{
		(dPt2[0]-dPt1[0])/(dPt2[3]-dPt1[3]),
		(dPt2[1]-dPt1[1])/(dPt2[3]-dPt1[3]),
		(dPt2[2]-dPt1[2])/(dPt2[3]-dPt1[3])
	};
	double dDelta2[3] =
	{
		(dPt3[0]-dPt2[0])/(dPt3[3]-dPt2[3]),
		(dPt3[1]-dPt2[1])/(dPt3[3]-dPt2[3]),
		(dPt3[2]-dPt2[2])/(dPt3[3]-dPt2[3])
	};
	double dGrad[3] = 
	{
		0.5*(fabs(dDelta1[0])+fabs(dDelta2[0])),
		0.5*(fabs(dDelta1[1])+fabs(dDelta2[1])),
		0.5*(fabs(dDelta1[2])+fabs(dDelta2[2]))
	};
	double dGradMag2 = dGrad[0]*dGrad[0]+dGrad[1]*dGrad[1]+dGrad[2]*dGrad[2];
	return dGradMag2 > m_dMaxGrad2;
}

/*============================================================================*/
/* IMPLEMENTATION	---	VveNodeDistanceRefinementPred						  */
/*============================================================================*/
VveNodeDistanceRefinementPred::VveNodeDistanceRefinementPred( const double dMaxDistance )
	:	m_dMaxD2(dMaxDistance*dMaxDistance)
{

}

VveNodeDistanceRefinementPred::~VveNodeDistanceRefinementPred()
{

}

bool VveNodeDistanceRefinementPred::operator()( const double dPt1[3], const double dPt2[3], const double dPt3[3] ) const
{
	double dDist[3] =
	{
		dPt2[0]-dPt1[0],
		dPt2[1]-dPt1[1],
		dPt2[2]-dPt1[2]
	};
	
	if(dDist[0]*dDist[0]+dDist[1]*dDist[1]+dDist[2]*dDist[2]>m_dMaxD2)
		return true;
	
	dDist[0] = dPt3[0] - dPt2[0];
	dDist[1] = dPt3[1] - dPt2[1];
	dDist[2] = dPt3[2] - dPt2[2];

	return dDist[0]*dDist[0]+dDist[1]*dDist[1]+dDist[2]*dDist[2]>m_dMaxD2;
}

/*============================================================================*/
/* IMPLEMENTATION	---	VveAngleRefinementPred								  */
/*============================================================================*/
VveAngleRefinementPred::VveAngleRefinementPred( const double dAngle )
	: m_dMaxAngleCos(cos(dAngle))
{

}

VveAngleRefinementPred::~VveAngleRefinementPred()
{

}

bool VveAngleRefinementPred::operator()( const double dPt1[3], const double dPt2[3], const double dPt3[3] ) const
{
	double dV1[3] = 
	{
		dPt1[0]-dPt2[0],
		dPt1[1]-dPt2[1],
		dPt1[2]-dPt2[2]
	};
	double dV2[3] =
	{
		dPt3[0]-dPt2[0],
		dPt3[1]-dPt2[1],
		dPt3[2]-dPt2[2]
	};
	
	double dLen = sqrt(dV1[0]*dV1[0]+dV1[1]*dV1[1]+dV1[2]*dV1[2]) * sqrt(dV2[0]*dV2[0]+dV2[1]*dV2[1]+dV2[2]*dV2[2]);;
	double dDot = (dV1[0]*dV2[0] + dV1[1]*dV2[1] + dV1[2]*dV2[2])/dLen;

	//we need to refine if our dot product is smaller than (i.e. angle is larger than) cos(threshold)
	return dDot < m_dMaxAngleCos;
}


/*============================================================================*/
/* IMPLEMENTATION	---	VveAreaRefinementPred								  */
/*============================================================================*/
VveAreaRefinementPred::VveAreaRefinementPred( const double dArea )
	: m_dMaxArea2(dArea*dArea)
{

}

VveAreaRefinementPred::~VveAreaRefinementPred()
{

}

bool VveAreaRefinementPred::operator()( const double dPt1[3], const double dPt2[3], const double dPt3[3] ) const
{
	double dV1[3] = 
	{
		dPt1[0]-dPt2[0],
		dPt1[1]-dPt2[1],
		dPt1[2]-dPt2[2]
	};
	double dV2[3] =
	{
		dPt3[0]-dPt2[0],
		dPt3[1]-dPt2[1],
		dPt3[2]-dPt2[2]
	};

	double dCross[3] = 
	{
		dV1[1]*dV2[2] - dV1[2]*dV2[1],
		dV1[2]*dV2[0] - dV1[0]*dV2[2],
		dV1[0]*dV2[1] - dV1[1]*dV2[0]
	};

	double dArea2 = 0.25 * (dCross[0]*dCross[0] + dCross[1]*dCross[1] + dCross[2]*dCross[2]);

	return dArea2>m_dMaxArea2;
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/


