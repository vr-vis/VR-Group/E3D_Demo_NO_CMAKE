/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveIntegratorDOPRI5.h RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VVEINTEGRATORDOPRI5_H
#define VVEINTEGRATORDOPRI5_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VveIntegratorBase.h"
#include "VveQuarticIntegStep.h"
#include <cassert>
#include <limits>

/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 *	Implementation of the 5th order Dormand-Prince Runge-Kutta scheme according
 *	to :
 *	P.J. Prince and J.R. Dormand: A family of embedded Runge-Kutta formulae"
 *	Journal of Computational and Applied Mathematics 6(1):19-26, 1980.
 *
 *	This implementation heavily leans on - and in fact is mostly copied from - 
 *	code provided by Christoph Garth (University of Kaiserslautern). A 
 *	pseudo-code implementation can be found in Appendix 1 of:
 *	C. Garth: Visualization of Complex Three-Dimensional flow Structures, 
 *	Dissertation Thesis, University of Kaiserslautern, 2007.
 */
template<typename TInterpolator>
class VveIntegratorDOPRI5 : public VveIntegratorBase<TInterpolator>
{
public:
	/** make output step type transparent for clients */
	typedef VveQuarticIntegStep TStep; 

	VveIntegratorDOPRI5(TInterpolator* pInterpolator);
	~VveIntegratorDOPRI5();

	/** 
	 * Make an educated guess for the initial step size given max step size.
	 * Calling this for the DOPRI5 scheme is MANDATORY prior to integration,
	 * i.e. not calling InitIntegrationStep(...) will lead to undefined 
	 * results.
	 */
	void InitIntegrationStep(double pStart[4]);

	/**
	 * Perform one integration step for the field defined by the given 
	 * interpolator, starting at pIn. The output is given as linear 
	 * integration step in pOutput.
	 */
	int ComputeNextStep(double* pIn, TStep *pOut);

protected:
	
	/** helper routines */
	double sign( double a, double b );
	double modmin( double a, double b );
	double modmax( double a, double b );

private:
	const double m_dRelTol;
	const double m_dAbsTol;

	unsigned int m_iNumAccepted;
	unsigned int m_iNumRejected;
	unsigned int m_iNumSteps;
	unsigned int m_iNumInterpols;

	double k1[4];
	
	// predicted step size for next integration call
	double m_dStepPrediction;

	// stepsize control stabilization
	double     m_dacold;

	// stiffness detection
	double     m_dhlamb;
	int        m_iasti;
	int        m_iNonStiff;

	/** helper constants -- initialized at the end of this file*/
	static const double safe;
	static const double epsilon;
	static const double facl;
	static const double facr;
	static const double beta;
	static const unsigned int nstiff;

	/** Dormand Prince stepping constants */
	static const double a21, a31, a32, a41, a42, a43, 
						a51, a52, a53, a54, 
						a61, a62, a63, a64, a65,
						a71, a73, a74, a75, a76;

	static const double c2, c3, c4, c5;

	static const double d1, d3, d4, d5, d6, d7;

	static const double e1, e2, e3, e4, e5, e6, e7;

};

template<typename TInterpolator>
inline VveIntegratorDOPRI5<TInterpolator>::VveIntegratorDOPRI5( TInterpolator* pInterpolator )
	:	VveIntegratorBase<TInterpolator>(pInterpolator),
		m_dRelTol( 1e-10 ), m_dAbsTol( 1e-10 ), 
		m_iNumAccepted(0), m_iNumRejected(0), m_iNumSteps(0), m_iNumInterpols(0),
		m_dStepPrediction(0.0), m_dacold( 1e-4 ), m_dhlamb( 0.0 ), m_iasti(0)
{

}


template<typename TInterpolator>
inline VveIntegratorDOPRI5<TInterpolator>::~VveIntegratorDOPRI5()
{

}

template<typename TInterpolator>
inline void VveIntegratorDOPRI5<TInterpolator>::InitIntegrationStep(double pSeed[4])
{
	 TInterpolator *pInterpol = this->GetInterpolator();
	 double dMaxStep = this->GetMaxStep();

	const double dCurrentT = pSeed[3];
	double dDirection = (dMaxStep > 0.0 ? 1.0 : -1.0);

	//init k1 for first iteration
	pInterpol->Interpolate(pSeed, k1);
	m_iNumInterpols++;
	
	//NOTE: in here, a step is always positive!
	dMaxStep = fabs(dMaxStep);

	double sk, sqr;
	double dnf = 0.0;
	double dny = 0.0;

	for( int i=0; i<3; i++ ) 
	{
		sk = m_dAbsTol + m_dRelTol * fabs( pSeed[i] );
		sqr = k1[i] / sk;
		dnf += sqr * sqr;
		sqr = pSeed[i] / sk;
		dny += sqr * sqr;
	}

	double dCurrentStep;
	if( (dnf <= 1.0e-10) || (dny <= 1.0e-10) ) 
		dCurrentStep = 1.0e-6;
	else 
		dCurrentStep = sqrt( dny/dnf ) * 0.01;
	dCurrentStep = std::min( dCurrentStep, dMaxStep );

	//perform an explicit Euler step
	//NOTE: k1 has to be pre-initialized properly!
	double k3[4] =
	{
		pSeed[0] + dCurrentStep * dDirection * k1[0],
		pSeed[1] + dCurrentStep * dDirection * k1[1],
		pSeed[2] + dCurrentStep * dDirection * k1[2],
		pSeed[3] + dCurrentStep * dDirection
	};

	double k2[4];
	pInterpol->Interpolate(k3, k2);
	++m_iNumInterpols;

	// estimate the second derivative of the solution
	double der2 = 0.0;

	for( int i=0; i<3; i++) 
	{
		sk = m_dAbsTol + m_dRelTol * fabs( pSeed[i] );
		sqr = ( k2[i] - k1[i] ) / sk;
		der2 += sqr*sqr;
	}

	der2 = sqrt( der2 ) / dCurrentStep;

	// step size is computed such that
	// h**(1/5) * max( norm(k1), norm(der2) ) = 0.01
	double der12 = std::max( fabs(der2), sqrt(dnf) );

	double dStep1;
	if( der12 <= 1.0e-15 ) 
		dStep1 = std::max( 1.0e-6, dCurrentStep*1.0e-3 );
	else 
		dStep1 = pow( 0.01/der12, 0.2 );

	dCurrentStep = std::min( 100.0*dCurrentStep, dStep1 );
	dCurrentStep = std::min( dCurrentStep, dMaxStep );

	m_dStepPrediction = dDirection * dCurrentStep;

	//reset num steps
	m_iNumSteps = 0;
}


template<typename TInterpolator>
inline int VveIntegratorDOPRI5<TInterpolator>::ComputeNextStep( double* pStartPt, TStep *pOut )
{
	TInterpolator *pInterpol = this->GetInterpolator();

	const double dDirection = (this->GetMaxStep() > 0.0 ? 1.0 : -1.0);

	//copy in start pt
	double dCurrentPt[4] = {pStartPt[0], pStartPt[1], pStartPt[2], pStartPt[3]};
	const double &dCurrentT = dCurrentPt[3];

	//determine temporal integration limit
	double dTimeFrame[2];
	pInterpol->GetValidTimeFrame(dTimeFrame);
	const double dTimeLimit = (dDirection>0.0 ? dTimeFrame[1] : dTimeFrame[0]);

	// determine stepsize 
	// (either take the one from a previous call or try to guess one when we start anew)
	double dCurrentStep = std::min(m_dStepPrediction, this->GetMaxStep());

	//if we are on or beyond the time limit ==> bail out right away!
	if((dCurrentT+dCurrentStep-dTimeLimit)*dDirection >= 0.0)
		return VveIntegratorBase<TInterpolator>::T_MAX_REACHED;
		
	double k2[4], k3[4], k4[4], k5[4], k6[4], k7[4];
	bool bReject = false;

	// integration step loop
	// iterate until a suitable step size has been found 
	while( true )
	{
		bool bLastStep = false;
		double dNextPt[4], dStiffPt[4];

		// break on step size underflow 
		// i.e. this ignores the min step size set for this integrator
		// @TODO - check how to integrate min step size here 
		//==> use underflow error or cope with error and return "best seen so far"
		if( 0.1*fabs(dCurrentStep) <= fabs(dCurrentT)*epsilon ) 
		{
#ifdef DEBUG
			std::cout << "dopri5(): exiting at t = " << dCurrentT 
				<< ", step size too small (dCurrentStep = "
				<< dCurrentStep << ")\n";
#endif
			return VveIntegratorBase<TInterpolator>::STEPSIZE_UNDERFLOW;
		}
		// do not run past time limit ==> adjust step to hit the end spot on
		if( (dCurrentT + 1.01*dCurrentStep - dTimeLimit) * dDirection > 0.0 ) 
		{
			bLastStep = true;
			dCurrentStep = dTimeLimit - dCurrentT;
		}
		++m_iNumSteps;
		
		//std::cout << "dopri5 step: t = " << t << ", dCurrentStep = " << dCurrentStep << ", t+dCurrentStep = " << t+dCurrentStep << '\n';

		//-------------- perform stages ----------------------------
		dNextPt[0] = dCurrentPt[0] + dCurrentStep*a21*k1[0];
		dNextPt[1] = dCurrentPt[1] + dCurrentStep*a21*k1[1];
		dNextPt[2] = dCurrentPt[2] + dCurrentStep*a21*k1[2];
		dNextPt[3] = dCurrentPt[3] + c2*dCurrentStep;
		
		if(!pInterpol->Interpolate(dNextPt, k2))
			return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;
		
		dNextPt[0] = dCurrentPt[0] + dCurrentStep * (a31*k1[0] + a32*k2[0]);
		dNextPt[1] = dCurrentPt[1] + dCurrentStep * (a31*k1[1] + a32*k2[1]);
		dNextPt[2] = dCurrentPt[2] + dCurrentStep * (a31*k1[2] + a32*k2[2]);
		dNextPt[3] = dCurrentPt[3] + dCurrentStep * c3;

		if(!pInterpol->Interpolate(dNextPt, k3))
			return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;
		
		dNextPt[0] = dCurrentPt[0] + dCurrentStep * (a41*k1[0] + a42*k2[0] + a43*k3[0]);
		dNextPt[1] = dCurrentPt[1] + dCurrentStep * (a41*k1[1] + a42*k2[1] + a43*k3[1]);
		dNextPt[2] = dCurrentPt[2] + dCurrentStep * (a41*k1[2] + a42*k2[2] + a43*k3[2]);
		dNextPt[3] = dCurrentPt[3] + dCurrentStep * c4;

		if(!pInterpol->Interpolate(dNextPt, k4))
			return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;

		dNextPt[0] = dCurrentPt[0] + dCurrentStep * (a51*k1[0] + a52*k2[0] + a53*k3[0] + a54*k4[0]);
		dNextPt[1] = dCurrentPt[1] + dCurrentStep * (a51*k1[1] + a52*k2[1] + a53*k3[1] + a54*k4[0]);
		dNextPt[2] = dCurrentPt[2] + dCurrentStep * (a51*k1[2] + a52*k2[2] + a53*k3[2] + a54*k4[0]);
		dNextPt[3] = dCurrentPt[3] + dCurrentStep * c5;

		if(!pInterpol->Interpolate(dNextPt, k5))
			return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;

		dStiffPt[0] = dNextPt[0] = dCurrentPt[0] + dCurrentStep * (a61*k1[0]+a62*k2[0]+a63*k3[0]+a64*k4[0]+a65*k5[0]);
		dStiffPt[1] = dNextPt[1] = dCurrentPt[1] + dCurrentStep * (a61*k1[1]+a62*k2[1]+a63*k3[1]+a64*k4[1]+a65*k5[1]);
		dStiffPt[2] = dNextPt[2] = dCurrentPt[2] + dCurrentStep * (a61*k1[2]+a62*k2[2]+a63*k3[2]+a64*k4[2]+a65*k5[2]);
		dStiffPt[3] = dCurrentPt[3] + dCurrentStep;

		if(!pInterpol->Interpolate(dNextPt, k6))
			return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;
				
		dNextPt[0] = dCurrentPt[0] + dCurrentStep * (a71*k1[0] + a73*k3[0] + a74*k4[0] + a75*k5[0] + a76*k6[0]);
		dNextPt[1] = dCurrentPt[1] + dCurrentStep * (a71*k1[1] + a73*k3[1] + a74*k4[1] + a75*k5[1] + a76*k6[1]);
		dNextPt[2] = dCurrentPt[2] + dCurrentStep * (a71*k1[2] + a73*k3[2] + a74*k4[2] + a75*k5[2] + a76*k6[2]);
		dNextPt[3] = dCurrentPt[3] + dCurrentStep;

		if(!pInterpol->Interpolate(dNextPt, k7))
			return VveIntegratorBase<TInterpolator>::INTERPOL_FAILED;

		m_iNumInterpols += 6;

		
		//--------- error estimation and step size adaption ------------------
		double ee[3] =
		{
			dCurrentStep * (e1*k1[0] + e3*k3[0] + e4*k4[0] + e5*k5[0] + e6*k6[0] + e7*k7[0]),
			dCurrentStep * (e1*k1[1] + e3*k3[1] + e4*k4[1] + e5*k5[1] + e6*k6[1] + e7*k7[1]),
			dCurrentStep * (e1*k1[2] + e3*k3[2] + e4*k4[2] + e5*k5[2] + e6*k6[2] + e7*k7[2])
		};
		
		double sk, sqr, err=0.0;
		for( int i=0; i<3; i++ ) 
		{
			sk = m_dAbsTol + m_dRelTol * std::max(fabs(dCurrentPt[i]), fabs(dNextPt[i]));
			sqr = ee[i]/sk;
			err += sqr*sqr;
		}
		err = sqrt(err/3.0);

		// compute next (potential) stepsize
		double fac11 = pow( err, 0.2-beta*0.75 );
		// Lund-stabilization
		double fac = fac11 / pow( m_dacold, beta );
		// we require facl <= dNewStep/dCurrentStep <= facr
		fac = std::max( 1.0/facr, std::min( 1.0/facl, fac/safe ) );
		double dNewStep = dCurrentStep / fac;
		dNewStep = std::min(dNewStep, this->GetMaxStep());

		//printf("DOPRI5 -- exit -- step=%.5g; newstep=%.5g\n", dCurrentStep, dNewStep);

		if( err <= 1.0 ) 
		{
			// current step accepted
			m_dacold = std::max( err, 1.0e-4 );

			m_iNumAccepted++;

			// stiffness detection
			if( !(m_iNumAccepted % nstiff) || (m_iasti > 0) ) 
			{
				double stnum = 0.0, stden = 0.0, sqr;

				for( int i=0; i<3; i++ ) 
				{
					sqr = k7[i] - k6[i];
					stnum += sqr * sqr;
					sqr = dNextPt[i] - dStiffPt[i];
					stden += sqr * sqr;
				}

				if( stden > 0.0 ) 
					m_dhlamb = dCurrentStep * sqrt( stnum/stden );

				if( m_dhlamb > 3.25 ) 
				{
					m_iNonStiff = 0;
					m_iasti++;

					if( m_iasti == 15 ) 
					{
#ifdef DEBUG
						std::cout << "dopri5(): exiting at t = " << dCurrentT
							<< ", problem seems stiff (y = " 
							<< dCurrentPt << ")\n";
#endif
						return VveIntegratorBase<TInterpolator>::STIFFNESS_DETECTED;
					}
				}
				else 
				{
					m_iNonStiff++;
					if( m_iNonStiff == 6 ) 
						m_iasti = 0;
				}
			}//--end stiffness detection

			if( bReject ) 
				dNewStep = dDirection * std::min( fabs(dNewStep), fabs(dCurrentStep) );
			// --- step looks ok - prepare for return
			// fill in step
			// create interpolation polynomial
			double dStepCoeff[20];
			for(int i=0; i<4; ++i)
			{
				//--p0 == dCurrentPt
				dStepCoeff[i] = dCurrentPt[i];
				//--p1 == dNextPt - dCurrentPt
				dStepCoeff[4+i] = dNextPt[i]-dCurrentPt[i];
				//--p2 == dCurrentStep*k1 - p1
				dStepCoeff[8+i] = dCurrentStep*k1[i] - dStepCoeff[4+i];
				//--p3 == -dCurrentStep * k7 + p1 -p2
				dStepCoeff[12+i] =-dCurrentStep*k7[i] + dStepCoeff[4+i] - dStepCoeff[8+i];
				//--p4 == dCurrentStep *(d1*k1 + d3*k3 + d4*k4 + d5*k5 + d6*k6 + d7*k7)
				dStepCoeff[16+i] = dCurrentStep*(d1*k1[i]+d3*k3[i]+d4*k4[i]+d5*k5[i]+d6*k6[i]+d7*k7[i]);
			}
			//make sure we are moving forward
			assert((dNextPt[3]-dCurrentPt[3])*dDirection>0.0);

			pOut->Set(dCurrentPt, dNextPt, dStepCoeff);

			// update internal state
			// first-same-as-last for k1
			for(int i=0; i<4; ++i)
			{
				dCurrentPt[i] = dNextPt[i];
				k1[i] = k7[i];
			}
			//remember actual step size 
			this->SetActualStep(dCurrentStep);
			//...and educated guess for next call!
			m_dStepPrediction = dNewStep;

			// normal exit
			return bLastStep ? VveIntegratorBase<TInterpolator>::T_MAX_REACHED : VveIntegratorBase<TInterpolator>::INTEG_OK;
		}//--end if(err <= 1.0)
		else 
		{
			// step rejected ==> adapt step and 
			dNewStep = dCurrentStep / std::min( 1.0/facl, fac11/safe );
			bReject = true;
			if( m_iNumAccepted >= 1 ) 
				m_iNumRejected++;
			dCurrentStep = dNewStep;
		}//--end if(err <= 1.0) -> else
	}//-- end integration loop
}



template<typename TInterpolator>
inline double VveIntegratorDOPRI5<TInterpolator>::sign( double a, double b )
{
	return (b >= 0.0) ? fabs(a) : -fabs(a);
}

template<typename TInterpolator>
inline double VveIntegratorDOPRI5<TInterpolator>::modmin( double a, double b )
{
	return fabs(a) < fabs(b) ? a : b;
}

template<typename TInterpolator>
inline double VveIntegratorDOPRI5<TInterpolator>::modmax( double a, double b )
{
	return fabs(a) > fabs(b) ? a : b;
}


/*============================================================================*/
/* CONST INITIALIZATION														  */
/*============================================================================*/
/** helper constants */
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::safe = 0.9;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::epsilon = std::numeric_limits<double>::epsilon();
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::facl = 0.2;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::facr = 10.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::beta = 0.04;
template<typename TInterpolator> const unsigned int VveIntegratorDOPRI5<TInterpolator>::nstiff = 100;

/** Dormand Prince stepping constants */
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a21=0.2;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a31=3.0/40.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a32=9.0/40.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a41=44.0/45.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a42=-56.0/15.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a43=32.0/9.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a51=19372.0/6561.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a52=-25360.0/2187.0; 
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a53=64448.0/6561.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a54=-212.0/729.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a61=9017.0/3168.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a62=-355.0/33.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a63=46732.0/5247.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a64=49.0/176.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a65=-5103.0/18656.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a71=35.0/384.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a73=500.0/1113.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a74=125.0/192.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a75=-2187.0/6784.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::a76=11.0/84.0;

template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::c2=0.2;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::c3=0.3;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::c4=0.8;
template<typename TInterpolator>const double  VveIntegratorDOPRI5<TInterpolator>::c5=8.0/9.0;

template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::d1=-12715105075.0/11282082432.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::d3=87487479700.0/32700410799.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::d4=-10690763975.0/1880347072.0; 
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::d5=701980252875.0/199316789632.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::d6=-1453857185.0/822651844.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::d7=69997945.0/29380423.0;

template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::e1=71.0/57600.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::e3=-71.0/16695.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::e4=71.0/1920.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::e5=-17253.0/339200.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::e6=22.0/525.0;
template<typename TInterpolator> const double VveIntegratorDOPRI5<TInterpolator>::e7=-1.0/40.0;

#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
