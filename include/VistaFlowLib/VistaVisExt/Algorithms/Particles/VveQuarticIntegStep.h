/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveQuarticIntegStep.h		 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VVEQUARTICINTEGSTEP_H
#define VVEQUARTICINTEGSTEP_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include <cassert>

/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 *	VveQuarticIntegStep implements dense integration output of degree 4 custom
 *	tailored to data provided by the DOPRI5 scheme.
 *	This amounts to a polynomial of degree 4 that smoothly interpolates the 
 *	integrated field line over the interval t_start, t_end.
 *	NOTE once again: this only works properly with the DOPRI5 implementation!
 */
class VveQuarticIntegStep
{
public:
	VveQuarticIntegStep();
	VveQuarticIntegStep(const double dStart[4], const double dEnd[4], const double dPts[20]);
	~VveQuarticIntegStep();

	/**
	 * Set coefficients for interpolation
	 */
	void Set(const double dStart[4], const double dEnd[4], const double dPts[20]);
	/**
	 *	Determine the position between dStart and dEnd via the given sim time.
	 */
	void GetPosForTime(const double dSimTime, double dOut[4]) const;
	/**
	 *	Determine the position between dStart and dEnd via the given normalized
	 *	param from [0..1]
	 */
	void GetPosForParam(const double dAlpha, double dOut[4]) const;

private:
	double m_dStart[4], m_dEnd[4];
	double m_dCoeff[20];
};

inline VveQuarticIntegStep::VveQuarticIntegStep()
{

}

inline VveQuarticIntegStep::VveQuarticIntegStep( const double dStart[4], const double dEnd[4], const double dPts[20] )
{
	this->Set(dStart, dEnd, dPts);
}

inline VveQuarticIntegStep::~VveQuarticIntegStep()
{

}

inline void VveQuarticIntegStep::Set(const double dStart[4], const double dEnd[4], const double dPts[20])
{
	for(int i=0; i<4; ++i)
	{
		m_dStart[i] = dStart[i];
		m_dEnd[i] = dEnd[i];
	}

	for(int i=0; i<20; ++i)
		m_dCoeff[i]=dPts[i];
}

inline void VveQuarticIntegStep::GetPosForTime( const double dSimTime, double dOut[4] ) const
{
	assert(dSimTime >= m_dStart[3]);
	assert(dSimTime <= m_dEnd[3]);
	//always use linear interpol for time
	const double dAlpha = (dSimTime-m_dStart[3])/(m_dEnd[3]-m_dStart[3]);
	this->GetPosForParam(dAlpha, dOut);
}

inline void VveQuarticIntegStep::GetPosForParam( const double dAlpha, double dOut[4] ) const
{
	const double dBeta = 1.0-dAlpha;
	//result in Horn scheme :
	//result = p0 + dAlpha * (p1 + dBeta * (p2 + dAlpha * (p3 + dBeta * p4)));
	dOut[0] = m_dCoeff[0] + dAlpha*(m_dCoeff[4]+dBeta*(m_dCoeff[8] +dAlpha*(m_dCoeff[12]+dBeta*m_dCoeff[16])));
	dOut[1] = m_dCoeff[1] + dAlpha*(m_dCoeff[5]+dBeta*(m_dCoeff[9] +dAlpha*(m_dCoeff[13]+dBeta*m_dCoeff[17])));
	dOut[2] = m_dCoeff[2] + dAlpha*(m_dCoeff[6]+dBeta*(m_dCoeff[10]+dAlpha*(m_dCoeff[14]+dBeta*m_dCoeff[18])));
	//always use linear interpol for time component
	dOut[3] = dBeta*m_dStart[3] + dAlpha*m_dEnd[3];
}


#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/