/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveLinearIntegStep.h		 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VVELINEARINTEGSTEP_H
#define VVELINEARINTEGSTEP_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include <cassert>

/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 *  Linear interpolation step for dense integrator output, i.e. output in this 
 *	case is a linear interpolation between start and end point.
 */
class VveLinearIntegStep
{
public:
	VveLinearIntegStep();
	VveLinearIntegStep(const double dStart[4], const double dEnd[4]);
	virtual ~VveLinearIntegStep();

	/**
	 * Set start and end point
	 */
	void Set(const double dStart[4], const double dEnd[4]);
	/**
	 *	Determine the position between dStart and dEnd via the given sim time.
	 */
	void GetPosForTime(const double dSimTime, double dOut[4]) const;
	/**
	 *	Determine the position between dStart and dEnd via the given normalized
	 *	param from [0..1]
	 */
	void GetPosForParam(const double dAlpha, double dOut[4]) const;

private:
	double m_dPts[8];
};

inline VveLinearIntegStep::VveLinearIntegStep()
{
}

inline VveLinearIntegStep::VveLinearIntegStep( const double dStart[4], const double dEnd[4] )
{
	this->Set(dStart, dEnd);
}

inline VveLinearIntegStep::~VveLinearIntegStep()
{

}

inline void VveLinearIntegStep::Set( const double dStart[4], const double dEnd[4] )
{
	for(int i=0; i<4; ++i)
	{
		m_dPts[i] = dStart[i];
		m_dPts[4+i] = dEnd[i];
	}
}

inline void VveLinearIntegStep::GetPosForTime( const double dSimTime, double dOut[4] ) const
{
	assert(dSimTime >= m_dPts[3]);
	assert(dSimTime <= m_dPts[7]);

	double dAlpha = (dSimTime-m_dPts[3])/(m_dPts[7]-m_dPts[3]);
	this->GetPosForParam(dAlpha, dOut);
}

inline void VveLinearIntegStep::GetPosForParam( const double dAlpha, double dOut[4] ) const
{
	assert(dAlpha >= 0.0);
	assert(dAlpha <= 1.0);

	double dBeta = 1.0-dAlpha;

	for(int i=0; i<4; ++i)
		dOut[i] = dBeta*m_dPts[i] + dAlpha*m_dPts[4+i];
}



#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/