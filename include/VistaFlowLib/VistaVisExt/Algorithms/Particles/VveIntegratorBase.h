/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveIntegratorBase.h			 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VVEINTEGRATORBASE_H
#define VVEINTEGRATORBASE_H
/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/


/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 * Base class for numerical integration algorithms in vector fields.
 *
 * As a base class, it keeps track of the interpolation method that is used 
 * to access the underlying vector field and the integration step length.
 * The integration step length is always given in simulation time. 
 *
 * All derived integration schemes should equally implement the following
 * integration interface:
 *
 * void InitIntegrationStep(double *pSeed);
 * int ComputeNextStep(double *pIn, TStep *pOut);
 *
 * InitIntegrationStep(...) will try to initialize the integration step given
 * its interpolator and the seed point for the next integral curve to come. 
 * Adaptive step size schemes will perform an educated guess, while
 * fixed-step schemes will essentially do nothing.
 *
 * ComputeNextStep(...) takes a 4D space time position (x1,y1,z1,t1) as input 
 * and yields a TStep data structure as output from which the actual position 
 * throughout the step can be retrieved. The return value indicates whether 
 * or not integration was completed successfully.
 *
 * An integration scheme takes an interpolator upon construction. Make sure, 
 * the interpolator is pre-configured and ready to use. With the interpolator
 * being a template parameter, it does not necessarily have to be derived from 
 * a specific base class, however, the interpolator is expected to implement
 * the following interface:
 *
 * bool Interpolate(double *pIn, double *pResult);
 *
 * which for a given input position in 4-space (x,y,z,t) and compute the 
 * vector field at that point, returning it in pResult. The return value
 * indicates whether or not interpolation has been successful. See 
 * VistaVisExt/Algorithms/Particles/VveSpatialInterpolator.h for details.
 *
 * In case an integration scheme uses fixed step size only, the minimum step 
 * length will be ignored and the maximum step size will be used, exclusively.
 *
 * \DATE	March 2012
 */
template <typename TInterpolator>
class VveIntegratorBase
{

public:
	/** return information for ComputeNextStep(...) */
	enum 
	{
		INTERPOL_FAILED = -3,		/** interpolation failed, most likely for being outside the domain */
		T_MAX_REACHED = -2,			/** the maximum valid time (in either temporal direction) was reached */
		STEPSIZE_UNDERFLOW = -1,	/** step size control suggested a step below the given min step */
		STIFFNESS_DETECTED = 0,		/** the problem was determined to be stiff (very unlikely) */
		INTEG_OK = 1				/** integration went o.k. */
	};

	/**
	 * access the interpolator
	 */
	TInterpolator* GetInterpolator() const;

	/**
	 * Set the minimum step size for integrators that perform adaptive step
	 * size control. The integration step is always given in sim time.
	 */
	double GetMinStep() const;
	void SetMinStep(const double size);

	/**
	 * Set the maximum step size for integrators that perform adaptive step
	 * size control. The integration step is always given in sim time.
	 * The maximum step is the one used for fixed step integration schemes.
	 */
	double GetMaxStep() const;
	void SetMaxStep(const double val);

	/**
	 * Retrieve the actual step that was used for the last integration
	 * call. Defaults to 0
	 */
	double GetActualStep() const;

protected:
	/**
	 * hide c'tors because we have a pure base class here.
	 */
	VveIntegratorBase();
	/**
	 * standard constructor, i.e. integrators should per default 
	 * be given a pre-configured interpolator.
	 * NOTE: The interpolator is provided from the outside, so 
	 *	     it is the client's responsibility to delete it after use.
	 */
	VveIntegratorBase(TInterpolator* pInterpolator);
	virtual ~VveIntegratorBase();

	/**
	 * Adaptive integrators can use this call to set the 
	 * actual step size that they used during the last integration call.
	 */
	void SetActualStep(double dStep);
	
private:
	TInterpolator* m_pInterpolator;
	double m_dMinStep;
	double m_dMaxStep;
	double m_dActualStep;
	
};

/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
template <typename TInterpolator>
VveIntegratorBase<TInterpolator>::VveIntegratorBase()
	:	m_pInterpolator(NULL),
		m_dMinStep(0.0),
		m_dMaxStep(0.0),
		m_dActualStep(0.0)
{
}

template <typename TInterpolator>
VveIntegratorBase<TInterpolator>::VveIntegratorBase(TInterpolator* pInterpol)
	:	m_pInterpolator(pInterpol),
		m_dMinStep(0.0),
		m_dMaxStep(0.0),
		m_dActualStep(0.0)
{
}

template <typename TInterpolator>
VveIntegratorBase<TInterpolator>::~VveIntegratorBase()
{
}

template <typename TInterpolator>
inline TInterpolator* VveIntegratorBase<TInterpolator>::GetInterpolator() const
{
	return m_pInterpolator;
}

template <typename TInterpolator>
inline double VveIntegratorBase<TInterpolator>::GetMinStep() const
{
	return m_dMinStep;
}

template <typename TInterpolator>
inline void VveIntegratorBase<TInterpolator>::SetMinStep(const double size)
{
	m_dMinStep = size;
}


template <typename TInterpolator>
inline double VveIntegratorBase<TInterpolator>::GetMaxStep() const 
{ 
	return m_dMaxStep; 
}

template <typename TInterpolator>
inline void VveIntegratorBase<TInterpolator>::SetMaxStep(const double val) 
{ 
	m_dMaxStep = val; 
}


template <typename TInterpolator>
double VveIntegratorBase<TInterpolator>::GetActualStep() const
{
	return m_dActualStep;
}


template <typename TInterpolator>
inline void VveIntegratorBase<TInterpolator>::SetActualStep( double dStep )
{
	m_dActualStep = dStep;
}





#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
