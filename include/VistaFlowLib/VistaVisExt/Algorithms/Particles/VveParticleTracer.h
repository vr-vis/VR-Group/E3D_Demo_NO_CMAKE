/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :	VveParticleTracer.h			 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  			                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  			                   Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/


#ifndef VVEPARTICLETRACER_H
#define VVEPARTICLETRACER_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "../../Data/VveParticlePopulation.h"
#include "../../Data/VveParticleTrajectory.h"
#include "../../Data/VveParticleDataArray.h"
#include "../../Data/VveDiscreteDataTyped.h"
#include "../../Tools/VveTimeMapper.h"

#include <cassert>
#include <vector>

/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 * General class for particle tracing in vector fields
 * This class takes two template parameters as input: first you'll have to
 * define the input data type, e.g. vtkDataSet or VveDiscreteDataTyped<vtkDataSet>.
 * Then you'll have to provide a trait class which includes the following 
 * typedefs:
 *
 * TLocator			:	The type of the cell locator to be used
 *						Look at VveDefaultCellLoactor for details.
 * TInterpolator	:	The interpolation type to be used, e.g. 
 *						VveSpatialInterpolator for streamlines
 * TIntegrator		:	The numerical integration scheme to be used.
 *
 * Using the trait construct, this class template can be customized,
 * in particular for both, steady-state (streamline) and time-varying 
 * (pathline) tracing. Moreover, key parts of the algorithm can be exchanged 
 * without too much hassle.
 *
 * \date	April 2012
 *
 */
template<typename TRawVisData, typename TTracerTrait>
class VveParticleTracer
{
public:
	enum{
		TRACE_FORWARD,
		TRACE_BACKWARD,
		TRACE_BOTH
	};
	
	typedef VveDiscreteDataTyped<TRawVisData> TInputData;
	typedef typename TTracerTrait::TLocator TLocator;
	typedef typename TTracerTrait::TInterpolator TInterpolator;
	typedef typename TTracerTrait::TIntegrator TIntegrator;

	VveParticleTracer();
	virtual ~VveParticleTracer();

	/**
	 * Set the data for the tracing computation
	 * Note that this will also reset the time frame for integration to the entire 
	 * time frame of the given data object. If you want to integrate only for
	 * a temporal sub-interval, please make sure to set the time frame after
	 * the most recent SetData call.
	 */
	void SetInputData(TInputData *pData);
	/**
	 * retrieve the current data set
	 */
	TInputData *GetInputData();
	/**
	 * Provide seed points for tracing
	 * Seeds are provided as 4D space-time positions (x,y,z,t)
	 */
	void SetSeed(double* pSeeds, const size_t iNumSeeds);
	/**
	 * get a pointer to the underlying seed population
	 */
	double* GetSeedPtr();
	/**
	 * return the number of seeds currently stored 
	 */
	int GetNumSeeds() const;
	/**
	 * Copy out the seed set to the given data array. 
	 *
	 * Make sure that pSeeds points to a large enough memory block to 
	 * accommodate all seeds, i.e. 4*this->GetNumSeeds() doubles!
	 */
	void GetSeedCopy(double *pSeeds, size_t &iNumSeeds) const;
	/**
	 * define the maximum step size for integration 
	 * (given in positive simulation time units)
	 *
	 * In case of fixed step-size integration, only this maximum step
	 * will be used and the minimum step size will be ignored.
	 *
	 * NOTE: The integration step should be provided as a positive number
	 *       In order to trace particles backward in time, use the SetTraceMode
	 *		 method!
	 */ 
	void SetMaxIntegrationStep(const double stepsize);
	/**
	 * return the maximum integration step
	 */
	double GetMaxIntegrationStep() const;
	/**
	 * define the minimum step size for integration 
	 * (given in simulation time units)
	 *
	 * In case of fixed step-size integration, only this maximum step
	 * will be used and the minimum step size will be ignored.
	 *
	 * NOTE: The integration step should be provided as a positive number
	 *       In order to trace particles backward in time, use the SetTraceMode
	 *		 method!
	 */ 
	void SetMinIntegrationStep(const double stepsize);
	/**
	 * return the minimum integration step
	 */
	double GetMinIntegrationStep() const;
	/**
	 * define whether to trace forward in time, backward, or both
	 */
	void SetTraceMode(const int i);
	/**
	 * return the current trace mode
	 */
	int GetTraceMode() const;
	/**
	 * Set the maximum number of integrations performed per trace.
	 * Once this limit is hit, particles will be terminated and
	 * the trajectory's termination reason will be set to
	 * TERM_NUM_STEPS.
	 */
	void SetMaxNumIntegrations(const size_t iNumSteps);
	/**
	 * return the maximum number of integrations performed per trace
	 */
	size_t GetMaxNumIntegrations() const;
	/**
	 * Set the (simulation) time frame for which to integrate
	 * the particles, i.e. particles outside this frame will be
	 * terminated and the termination reason will be set to 
	 * TERM_TIME_FRAME_LEFT.
	 */
	void SetIntegrationTimeFrame(double dSimTimes[2]);
	/**
	 * retrieve the (simulation) time frame for integration
	 */
	void GetIntegrationTimeFrame(double dSimTimes[2]) const;
	/**
	 * set the name of the vector field to be used for integration
	 */
	void SetVectorFieldName(const std::string &strVectors);
	/**
	 * return the name of the currently used vector field
	 */
	std::string GetVectorFieldName() const;
	/**
	 * determine which fields in the data will be interpolated along the
	 * way. Note that interpolation of these fields will only take place
	 * at actual trace points and not inbetween, i.e. not "inside" the integration.
	 */
	void SetFieldsToInterpolate(const std::list<std::string> &liFieldNames);
	void GetFieldsToInterpolate(std::list<std::string>& liFieldNames) const;

	/**
	 * Finally the compute method ==> execute particle tracing with the given 
	 * set of parameters
	 */
	void TraceParticles();

	/**
	 * retrieve the output
	 */
	VveParticlePopulation *GetOutputData();

	/**
	 * Provide a target output data object from the outside.
	 * The tracer will use it for output instead of its own, internally 
	 * created object. The internally created object will be freed, so 
	 * please mind that referencing it hereafter WILL CRASH.
	 * Also note that the client setting an output via this interface will
	 * remain the owner of the given output data object, i.e. it is his/her
	 * responsibility to delete it properly.
	 */
	void SetOutputData(VveParticlePopulation *pTargetOutput);

protected:
	/**
	 * Create cell location & interpolation objects for unsteady data
	 * Potentially multiple locators are created, one for each time step involved in the 
	 * interpolation routine.
	 * The first entry of vecInterpols will always refer to the velocity field!
	 */
	void CreateDataAccess(std::vector<TLocator*> &vecLocators,
						  std::vector<TInterpolator*> &vecInterpols) const;

	/**
	 * Create an empty trajectory given the data fields determined by the set
	 * of interpolators passed to this fn
	 */
	VveParticleTrajectory *CreateEmptyTrajectory(
								const std::vector<TInterpolator*> &vecInterpolators,
								VveParticleDataArray<double>*& pPoints,
								VveParticleDataArray<double>*& pVelocity,
								VveParticleDataArray<double>*& pTime,
								std::vector<VveParticleDataArray<double>*> &vecFields) const;
	/**
	 * convenience function for reversing the order of an array
	 * which is needed when tracing backwards in time.
	 */
	void ReverseArray(double *pArray, const size_t iNumTuples, const size_t iNumComponents) const;
private:
	/**
	 * input data
	 */
	TInputData *m_pData;

	/**
	 * seed set
	 */
	double *m_dSeeds;
	size_t m_iNumSeeds;

	/**
	 * integration params
	 */
	double m_dMinIntegrationStep;
	double m_dMaxIntegrationStep;
	int m_iIntegrationMode;
	size_t m_iNumIntegrations;
	double m_dTimeFrame[2];

	/**
	 * data fields
	 */
	std::string m_strVectors;
	std::list<std::string> m_liDataFields;

	/**
	 * output particle population
	 */
	VveParticlePopulation *m_pPopulation;
	bool m_bOwnsOutput;
};


template<typename TRawVisData, typename TTracerTrait>
VveParticleTracer<TRawVisData, TTracerTrait>::VveParticleTracer()
	:	m_pData(NULL),
		m_dSeeds(NULL),
		m_iNumSeeds(0),
		m_dMinIntegrationStep(0.0),
		m_dMaxIntegrationStep(0.0),
		m_iIntegrationMode(TRACE_FORWARD),
		m_iNumIntegrations(5000),
		m_strVectors(""),
		m_pPopulation(new VveParticlePopulation),
		m_bOwnsOutput(true)
{
	m_dTimeFrame[0] = 0.0;
	m_dTimeFrame[1] = 1.0;
}

template<typename TRawVisData, typename TTracerTrait>
VveParticleTracer<TRawVisData, TTracerTrait>::~VveParticleTracer()
{
	delete[] m_dSeeds;
	//delete output if and only if it belongs to us
	if(m_bOwnsOutput)
		delete m_pPopulation;
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::SetInputData( VveDiscreteDataTyped<TRawVisData> *pData )
{
	m_pData = pData;
	VveTimeMapper *pTM = m_pData->GetTimeMapper();
	//reset the time frame for integration to the entire data set
	m_dTimeFrame[0] = pTM->GetSimulationTime(0.0);
	m_dTimeFrame[1] = pTM->GetSimulationTime(1.0);
}

template<typename TRawVisData, typename TTracerTrait>
inline VveDiscreteDataTyped<TRawVisData>* VveParticleTracer<TRawVisData, TTracerTrait>::GetInputData()
{
	return m_pData;
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::SetSeed( double* pSeeds, const size_t iNumSeeds )
{
	//check for old seed set
	if(m_dSeeds != NULL)
	{
		delete[] m_dSeeds;
	}
	//copy information
	m_iNumSeeds = iNumSeeds;
	m_dSeeds = new double[4*iNumSeeds];
	memcpy(m_dSeeds, pSeeds, 4*iNumSeeds*sizeof(double));
}

template<typename TRawVisData, typename TTracerTrait>
inline double* VveParticleTracer<TRawVisData, TTracerTrait>::GetSeedPtr()
{
	return m_dSeeds;
}

template<typename TRawVisData, typename TTracerTrait>
inline int VveParticleTracer<TRawVisData, TTracerTrait>::GetNumSeeds() const
{
	return m_iNumSeeds;
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::GetSeedCopy( double *pSeeds, size_t &iNumSeeds ) const
{
	//NOTE: we don't/can't check indices here so client code better provide enough memory
	iNumSeeds = m_iNumSeeds;
	memcpy(pSeeds, m_dSeeds, 4*iNumSeeds*sizeof(double));
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::SetMaxIntegrationStep( const double stepsize )
{
	m_dMaxIntegrationStep = stepsize;
}

template<typename TRawVisData, typename TTracerTrait>
inline double VveParticleTracer<TRawVisData, TTracerTrait>::GetMaxIntegrationStep() const
{
	return m_dMaxIntegrationStep;
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::SetMinIntegrationStep( const double stepsize )
{
	m_dMinIntegrationStep = stepsize;
}

template<typename TRawVisData, typename TTracerTrait>
inline double VveParticleTracer<TRawVisData, TTracerTrait>::GetMinIntegrationStep() const
{
	return m_dMinIntegrationStep;
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::SetTraceMode( const int i )
{
	m_iIntegrationMode = i;
}

template<typename TRawVisData, typename TTracerTrait>
inline int VveParticleTracer<TRawVisData, TTracerTrait>::GetTraceMode() const
{
	return m_iIntegrationMode;
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::SetMaxNumIntegrations( const size_t iNumSteps )
{
	m_iNumIntegrations = iNumSteps;
}

template<typename TRawVisData, typename TTracerTrait>
inline size_t VveParticleTracer<TRawVisData, TTracerTrait>::GetMaxNumIntegrations() const
{
	return m_iNumIntegrations;
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::SetIntegrationTimeFrame( double dSimTimes[2] )
{
	m_dTimeFrame[0] = dSimTimes[0];
	m_dTimeFrame[1] = dSimTimes[1];
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::GetIntegrationTimeFrame( double dSimTimes[2] ) const
{
	dSimTimes[0] = m_dTimeFrame[0];
	dSimTimes[1] = m_dTimeFrame[1];
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::SetVectorFieldName( const std::string &strVectors )
{
	m_strVectors = strVectors;
}

template<typename TRawVisData, typename TTracerTrait>
inline std::string VveParticleTracer<TRawVisData, TTracerTrait>::GetVectorFieldName() const
{
	return m_strVectors;
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::SetFieldsToInterpolate( const std::list<std::string> &liFieldNames )
{
	m_liDataFields = liFieldNames;
}

template<typename TRawVisData, typename TTracerTrait>
inline void VveParticleTracer<TRawVisData, TTracerTrait>::GetFieldsToInterpolate( std::list<std::string>& liFieldNames ) const
{
	liFieldNames = m_liDataFields;
}

/*============================================================================*/
/* IMPLEMENTATION OF PARTICLE TRACING										  */
/*============================================================================*/
template<typename TRawVisData, typename TTracerTrait>
void VveParticleTracer<TRawVisData, TTracerTrait>::TraceParticles()
{
	//basic sanity checks
	if(m_pData == NULL)
	{
		return;
	}
	if(m_dMinIntegrationStep <= 0.0 || m_dMaxIntegrationStep <= 0.0)
	{
		return;
	}

	VveTimeMapper *pTM = m_pData->GetTimeMapper();
	const double dSimStart = pTM->GetSimulationTime(0.0);
	const double dSimEnd = pTM->GetSimulationTime(1.0);
	const int iNumTimeIndices = pTM->GetNumberOfTimeIndices();

	const size_t iNumFields = m_liDataFields.size() + 1;
	
	//prepare forward and/or backward integration
	double dMinIntegStep[2];
	double dMaxIntegStep[2];
	//count the number of integration passes
	int iNumPasses = 0;
	//check if we have to trace backward in time at all
	if( m_iIntegrationMode == TRACE_BOTH || 
		m_iIntegrationMode == TRACE_BACKWARD)
	{
		dMinIntegStep[iNumPasses] = -m_dMinIntegrationStep;
		dMaxIntegStep[iNumPasses] = -m_dMaxIntegrationStep;
		++iNumPasses;
	}
	//check if we have to trace forward in time
	if( m_iIntegrationMode == TRACE_BOTH || 
		m_iIntegrationMode == TRACE_FORWARD)
	{
		dMinIntegStep[iNumPasses] = m_dMinIntegrationStep;
		dMaxIntegStep[iNumPasses] = m_dMaxIntegrationStep;
		++iNumPasses;
	}
	//start parallel region here ==> everything from here on goes on in threads
#pragma omp parallel
	{
		//-------------------
		// SETUP LOCATION, INTERPOLATION & INTEGRATION
		//-------------------
		std::vector<TLocator*> vecLocators; 
		std::vector<TInterpolator*> vecInterpolators(iNumFields);
		std::vector<VveParticleDataArray<double> *> vecFields;
	
		//NOTE: each thread gets its own set of locators and interpolators
		this->CreateDataAccess(vecLocators, vecInterpolators);
	
		//single out the vector field interpolator
		TInterpolator *pVInterpol = vecInterpolators[0];

		//create integrator according to trait
		TIntegrator *pIntegrator = new TIntegrator(pVInterpol);

		//-------------------
		// TRACE TRAJECTORIES
		//-------------------
		const int iNumSeeds = static_cast<int>(m_iNumSeeds);
		//parallelize over seeds
#pragma omp for
		for(int trace=0; trace < iNumSeeds; ++trace)
		{
			//create new trajectory
			VveParticleDataArray<double> *pPoints, *pVelocity, *pTime;
			VveParticleTrajectory *pTrajectory = this->CreateEmptyTrajectory(vecInterpolators, 
				pPoints, pVelocity, pTime, vecFields);

			//retrieve seed point
			size_t iTraceBase = 4*trace;
			double dCurrentPos[4] = {
				m_dSeeds[iTraceBase  ],
				m_dSeeds[iTraceBase+1],
				m_dSeeds[iTraceBase+2],
				m_dSeeds[iTraceBase+3],
			};
			double dInterpolVal[24];

			//sanity check if the seed is inside the time frame and data domain
			if( dCurrentPos[3] < m_dTimeFrame[0] ||
				dCurrentPos[3] > m_dTimeFrame[1] ||
				!pVInterpol->Interpolate(dCurrentPos, dInterpolVal))
			{
				//we are outside right away -> skip the trajectory entirely
				delete pTrajectory;
				continue;
			}
		
			//insert initial position including data
			pPoints->AppendElement(dCurrentPos);
			pVelocity->AppendElement(dInterpolVal);
			pTime->AppendElement(dCurrentPos[3]);

			//sample fields -- start with 1 since we got the velocity field already
			for(int f=1; f<iNumFields; ++f)
			{
				typename TTracerTrait::TInterpolator *pI = vecInterpolators[f];
				pI->Interpolate(dCurrentPos, dInterpolVal);

				VveParticleDataArray<double> *pA= vecFields[f];
				pA->AppendElement(dInterpolVal);
			}	
		
			//we have inserted one point ahead so far
			size_t iNumSteps = 1;
			typename TIntegrator::TStep oStep;
			double dNextPos[4];			

			//use 1 pass for backward and/or forward tracing
			//NOTE: this is (in its current form) NOT suitable for parallel execution!
			for(int d=0; d<iNumPasses; ++d)
			{
				//re-init the current position to the seed point
				dCurrentPos[0] = m_dSeeds[iTraceBase  ];
				dCurrentPos[1] = m_dSeeds[iTraceBase+1];
				dCurrentPos[2] = m_dSeeds[iTraceBase+2];
				dCurrentPos[3] = m_dSeeds[iTraceBase+3];
				
				//configure step length according to direction
				pIntegrator->SetMinStep(dMinIntegStep[d]);
				pIntegrator->SetMaxStep(dMaxIntegStep[d]);
				pIntegrator->InitIntegrationStep(dCurrentPos);

				//reset num steps as well
				iNumSteps = 1;

				//-----------------
				// MAIN TRACER LOOP
				//-----------------
				while(true)//do termination checking inside the loop
				{
					//check termination
					if(iNumSteps >= m_iNumIntegrations)
					{
						pTrajectory->SetReasonForTermination(VveParticleTrajectory::TERM_NUM_STEPS);
						break;
					}
					//check time frame ==> there must be enough time to have at least one more (big) step
					if((dCurrentPos[3]+dMaxIntegStep[d]<m_dTimeFrame[0]) ||
					   (dCurrentPos[3]+dMaxIntegStep[d]>m_dTimeFrame[1]))
					{
						pTrajectory->SetReasonForTermination(VveParticleTrajectory::TERM_TIME_FRAME_LEFT);
						break;
					}
					//try to integrate one step here -- ignore dense output, i.e. just take next pos.
					int iIntegResult = pIntegrator->ComputeNextStep(dCurrentPos, &oStep);
					oStep.GetPosForParam(1.0, dNextPos);
					//check if next point is still inside the domain
					if( iIntegResult != TIntegrator::INTEG_OK || 
						!pVInterpol->Interpolate(dNextPos, dInterpolVal))
					{
						pTrajectory->SetReasonForTermination(VveParticleTrajectory::TERM_OUT_OF_DOMAIN);
						break;
					}

					//insert position including data
					pPoints->AppendElement(dNextPos);
					pVelocity->AppendElement(dInterpolVal);
					pTime->AppendElement(dNextPos[3]);

					//sample fields -- start with 1 since we got the velocity field already
					for(int f=1; f<iNumFields; ++f)
					{
						typename TTracerTrait::TInterpolator *pI = vecInterpolators[f];
						pI->Interpolate(dNextPos, dInterpolVal);

						VveParticleDataArray<double> *pA= vecFields[f];
						pA->AppendElement(dInterpolVal);
					}

					//point added successful -- advance integration
					memcpy(dCurrentPos, dNextPos, 4*sizeof(double));
					++iNumSteps;
				}//end -- while integrating
				
				//determine if we need to sort the trace according to time
				if(dMinIntegStep[d] < 0)
				{
					//if we traced backwards, the data will have been inserted "the wrong way around"
					//==> reverse all arrays
					this->ReverseArray(pPoints->GetData(), iNumSteps, 3);
					this->ReverseArray(pVelocity->GetData(), iNumSteps, 3);
					this->ReverseArray(pTime->GetData(), iNumSteps, 1);
					for(int f=1; f<iNumFields; ++f)
					{
						VveParticleDataArray<double> *pF = vecFields[f];
						this->ReverseArray(pF->GetData(), iNumSteps, pF->GetNumComponents());
					}
				}
			}//end -- for forward/backward direction
		
			//append it to output -- NOTE: This is critical in a multi-threaded environment
#pragma omp critical
			{
				m_pPopulation->AddTrajectory(pTrajectory);
			}
		}//end -- for all seeds

		//cleanup
		delete pIntegrator;

		for(int f=0; f<iNumFields; ++f)
			delete vecInterpolators[f];
		
		for(int l=0; l<vecLocators.size(); ++l)
			delete vecLocators[l];
	}//-- end omp parallel
}

template<typename TRawVisData, typename TTracerTrait>
VveParticlePopulation * VveParticleTracer<TRawVisData, TTracerTrait>::GetOutputData()
{
	return m_pPopulation;
}

template<typename TRawVisData, typename TTracerTrait>
void VveParticleTracer<TRawVisData, TTracerTrait>::SetOutputData(VveParticlePopulation *pTargetOutput)
{
	//if we still own an output ==> delete it first
	if(m_bOwnsOutput)
	{
		delete m_pPopulation;
		m_bOwnsOutput = false;
	}
	m_pPopulation = pTargetOutput;
}

template<typename TRawVisData, typename TTracerTrait>
void VveParticleTracer<TRawVisData, TTracerTrait>::CreateDataAccess(std::vector<TLocator*> &vecLocators,
																	std::vector<TInterpolator*> &vecInterpolators) const
{
	//
	// SETUP LOCATION, INTERPOLATION & INTEGRATION
	//
	//create common locators for as many time steps as used by interpolator 
	const size_t iNumLocators = TInterpolator::GetTemporalSupportSize();
	vecLocators.resize(iNumLocators);
	for(int i=0; i<iNumLocators; ++i)
	{
		vecLocators[i] = new TLocator;
	}
	vecInterpolators.resize(m_liDataFields.size()+1);

	//create interpolator for vector field
	vecInterpolators[0] = new TInterpolator(vecLocators);
	vecInterpolators[0]->SetData(m_pData);
	if(!m_strVectors.empty())
		vecInterpolators[0]->SetInterpolatedField(m_strVectors);
	
	//make sure we got the right field!
	assert(vecInterpolators[0]->GetNumberOfResults() == 3);

	//create interpolators for all other fields
	std::list<std::string>::const_iterator itCurrent = m_liDataFields.begin();
	std::list<std::string>::const_iterator itEnd = m_liDataFields.end();
	int i = 1;
	for(;itCurrent != itEnd; ++itCurrent, ++i)
	{
		//create new interpolator sharing the same cell locator
		vecInterpolators[i] = new TInterpolator(vecLocators);
		vecInterpolators[i]->SetData(m_pData);
		vecInterpolators[i]->SetInterpolatedField(*itCurrent);
	}
}


template<typename TRawVisData, typename TTracerTrait>
VveParticleTrajectory * VveParticleTracer<TRawVisData, TTracerTrait>::CreateEmptyTrajectory( 
	const std::vector<TInterpolator*> &vecInterpolators,
	VveParticleDataArray<double>*& pPoints,
	VveParticleDataArray<double>*& pVelocity,
	VveParticleDataArray<double>*& pTime,
	std::vector<VveParticleDataArray<double>*> &vecFields) const
{
	//create new trajectory and relate data structures
	VveParticleTrajectory *pTrajectory = new VveParticleTrajectory;

	//default arrays
	pPoints = pTrajectory->CreateArray<double>(VveParticlePopulation::sPositionArrayDefault, 3, 1000);
	pVelocity = pTrajectory->CreateArray<double>(VveParticlePopulation::sVelocityArrayDefault, 3, 1000);
	pTime = pTrajectory->CreateArray<double>(VveParticlePopulation::sTimeArrayDefault, 1, 1000);
	
	//extra arrays
	const size_t iNumFields = vecInterpolators.size();
	vecFields.resize(iNumFields);
	vecFields[0] = pVelocity;
	for(int f=1; f<iNumFields; ++f)
	{
		TInterpolator *pI = vecInterpolators[f];
		vecFields[f] = pTrajectory->CreateArray<double>(pI->GetInterpolatedField(), pI->GetNumberOfResults(), 1000);
	}

	return pTrajectory;
}


template<typename TRawVisData, typename TTracerTrait>
void VveParticleTracer<TRawVisData, TTracerTrait>::ReverseArray(double *pArray, const size_t iNumTuples, const size_t iNumComponents) const
{
	//we currently use swap for up to 9 components
	assert(iNumComponents <= 9);
	
	const size_t iCenter = iNumTuples/2;
	double dSwap[9];
	for(size_t iTuple=0; iTuple<iCenter; ++iTuple)
	{
		//find base pointers for both tuples to exchange
		double *pTuple1 = pArray + (iTuple*iNumComponents);
		double *pTuple2 = pArray + ((iNumTuples-iTuple-1) * iNumComponents);
		//exchange the two values
		memcpy(dSwap, pTuple1, iNumComponents*sizeof(double));
		memcpy(pTuple1, pTuple2, iNumComponents*sizeof(double));
		memcpy(pTuple2, dSwap, iNumComponents*sizeof(double));
	}
}

#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/
