/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkSpatialHistogram.h        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA VisExt                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VtkSpatialHistogram    :                                          */
/*                                                                            */
/*============================================================================*/
#ifndef _VTKSPATIALHISTOGRAM_H
#define _VTKSPATIALHISTOGRAM_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkImageAlgorithm.h>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Given any form of vtkDataSet, this filter will produce a "spatial histogram"
 * i.e. it will create a 2D lattice whose axes are labeled by one coordinate
 * axis and one attribute value. It will then bin the input points to this 
 * lattice. The result will be a 2D pixel image which contains the hit counts
 * for each pixel. This can be interpreted as a series of single histograms
 * for the different spatial extents along the given coordinate axis.
 */
class VISTAVISEXTAPI vtkSpatialHistogram : public vtkImageAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkSpatialHistogram,vtkImageAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

	static vtkSpatialHistogram *New();

	/**
	 * Set the bounds of the target image data
	 * NOTE: the x and y coords will be used to determine the 
	 *       bounds of the plane, wheras the counts for each plane
	 *       pixel will be normalized to the z-coords (which is convenient
	 *		 if you want to do height plots)
	 * NOTE: The bounds will remain constant if either of the other two
	 *       defining parameters (spacing, resolution) is set, i.e. setting
	 *       the spacing will affect the resolution and v.v.
	 */
	void SetTargetBounds(float fBounds[6]);
	void GetTargetBounds(float fBounds[6]) const;

	/**
	 * Set the target resolution of the output image data
	 */
	void SetResolution(int iRes[2]);
	void GetResolution(int iRes[2]) const;

	/**
	 * set the target spacing of the output image data
	 */
	void SetSpacing(float fSpacing[2]);
	void GetSpacing(float fSpacing[2]) const;

	/**
	* Get/Set the name of the output data field (either point or cell data)
	* the default name is <count>
	*/
	void SetOutputFieldName(const std::string& strFieldName);
	std::string GetOutputFieldName() const;

	/**
	 *
	 */
	void SetArrayToPlot(const std::string& strFieldName);
	std::string GetArrayToPlot() const;

	/**
	 *
	 */
	void SetAxisToPlot(int iAxis);
	int GetAxisToPlot() const;

	/**
	 * override the range of the target axis or the target attribute
	 * in case you want to 'clamp' outliers in the data which otherwise
	 * would mess up your histogram plot
	 */
	void OverrideTargetAxisRange(float fRange[2]);
	void OverrideTargetAttributeRange(float fRange[2]);

	void CancelOverrideTargetAxisRange();
	void CancelOverrideTargetScalarRange();

protected:
	vtkSpatialHistogram();
	virtual ~vtkSpatialHistogram();

	/**
	* this is where the real work is done.
	*/
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
	/**
	*
	*/
	virtual int FillInputPortInformation(int port, vtkInformation *info);
	/**
	 *
	 */
	int RequestInformation(vtkInformation* vtkNotUsed(request),
						   vtkInformationVector** inputVector,
						   vtkInformationVector* outputVector);
	/**
	 *
	 */
	void RecomputeTargetDims(bool bResChanged);

private:
	std::string m_strOutputFieldName;
	std::string m_strArrayToPlot;
	int m_iAxisToPlot;
	/**
	 *
	 */
	float m_fBounds[6];
	int   m_iResolution[3];
	float m_fSpacing[3];

	/**
	 *
	 */
	bool m_bOverrideTargetAxis;
	float m_fTargetAxisRange[2];

	/**
	 *
	 */
	bool m_bOverrideTargetScalar;
	float m_fTargetScalarRange[2];
};


#endif //#ifdef _VtkSpatialHistogram_H

