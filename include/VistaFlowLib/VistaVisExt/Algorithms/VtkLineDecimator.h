/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  vtkLineDecimator.h       RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA VtkExt                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - vtkLineDecimator    :                                             */
/*                                                                            */
/*============================================================================*/
#ifndef _VTKLINEDECIMATOR_H
#define _VTKLINEDECIMATOR_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkPolyDataAlgorithm.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkIdList;
class vtkPoints;
class vtkPointData;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Decimate a set of lines given in a single vtkPolyData object. 
*
* This can be used, e.g., to get a reasonable representation for separatrices which 
* tend to have a very high resolution in the vicinity of critical points due to 
* extremely small integration step length.
*
* NOTE: This filter only works on lines. It will work if other cell types are 
*		contained in the input but no other cells than lines will be carried 
*		over to the output. The same holds for points which are not part of one
*		of the input lines.
*
* @author   Bernd Hentschel
* @date     June, 2006
*
*/
class VISTAVISEXTAPI vtkLineDecimator : public vtkPolyDataAlgorithm
{
public:
  vtkTypeRevisionMacro(vtkLineDecimator,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Constructor
  static vtkLineDecimator *New();
  
  /**
   * relative error when comparing current line segment's length to 
   * sum of original segments' lengths. The current segment is stopped 
   * if the sum of lenghts is greater than (1+fEpsilon * current length)
   * Therefore fEpsilon > 0 is to be fullfilled.
   * NOTE: A good starting point for the error (at least for separatrices)
   *       seems to be something on the order of n*10^-3
   */
  void SetMaximumRelativeError(const float fEpsilon);
  float GetMaximumRelativeError() const;
  
protected:
  vtkLineDecimator();
  virtual ~vtkLineDecimator();

  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // Decimate just a single line
  void DecimateLine(vtkPoints *pPts, vtkIdType iNumInPts, vtkIdType *pInPts, vtkIdType& iNumOutPts, vtkIdType *&pOutPts);

  // Copy a bunch of point data (coordinates & attributes) and renumerate the pPtIds accordingly
  void CopyLineToOutput(vtkPoints* pSrcPts, vtkPointData* pSrcPD, vtkIdType iNumPts, vtkIdType *pPtIds, vtkPoints *pTgtPts, vtkPointData *pTgtPD);
  
private:
  vtkLineDecimator(const vtkLineDecimator&);  // Not implemented.
  void operator=(const vtkLineDecimator&);  // Not implemented.
  
  float m_fMaximumRelativeError;
};

#endif //_vtkLineDecimator_H

