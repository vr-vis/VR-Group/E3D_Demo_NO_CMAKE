/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VtkStitchLines.h"
#include "VvePathlineTracer.h"

#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkMath.h>
#include <vtkBitArray.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkCleanPolyData.h>
#include <vtkInformationVector.h>
#include <vtkInformation.h>
#include <vtkMath.h>

#include <VistaBase/VistaStreamUtils.h>

#include <cassert>
#include <cmath>
using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
vtkCxxRevisionMacro(vtkStitchLines, "$Revision: 1.6 $");
vtkStandardNewMacro(vtkStitchLines);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
vtkStitchLines::vtkStitchLines()
{
}

vtkStitchLines::~vtkStitchLines()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   RequestData                                                 */
/*                                                                            */
/*============================================================================*/
int vtkStitchLines::RequestData(vtkInformation *pIn, 
								 vtkInformationVector **pInputVector, 
								 vtkInformationVector *pOutputVector)
{
	vtkInformation *pInInfo = pInputVector[0]->GetInformationObject(0);
	vtkInformation *pOutInfo = pOutputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkPolyData *pInput = vtkPolyData::SafeDownCast(pInInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkPolyData *pOutput = vtkPolyData::SafeDownCast(pOutInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pOutput)
	{
		vstr::errp() << "[vtkStitchLines::RequestData] no output!" << endl;
		return -1;
	}
	if(!pInput)
	{
		vstr::errp() << "[vtkStitchLines::RequestData] no input!" << endl;
		return -1;
	}
	vtkCellArray *pInLines = pInput->GetLines();
	if(pInLines->GetNumberOfCells() == 0)
	{
		vstr::warnp() << "[vtkStitchLines::RequestData] no lines in input!" << endl;
		return -1;
	}

	vtkDataArray *pSimTime = pInput->GetPointData()->GetScalars("sim_time");
	if(pSimTime == NULL)
	{
		vstr::errp() << "[vtkStitchLines::RequestData] no time field in input!" << endl;
		return -1;
	}

	vtkDataArray *pInTermReason = pInput->GetCellData()->GetScalars("TERM_REASON");
	if(pInTermReason == NULL)
	{
		vstr::errp() << "[vtkStitchLines::RequestData] no time field in input!" << endl;
		return -1;
	}
	
	vtkCellArray *pOutLines = vtkCellArray::New();
	vtkIdType npts1, npts2, *pts1, *pts2;
	vtkIdType loc1 = 0, loc2 = 0;
	vtkDataArray *pOutTermReason = vtkIntArray::New();
	pOutTermReason->SetNumberOfComponents(1);
	//pOutTermReason->Allocate(500,1000);
	pOutTermReason->SetNumberOfTuples(pInLines->GetNumberOfCells()); // notes: set the highest possible num of cells
	pOutTermReason->SetName("TERM_REASON");

	
	int iCurrentCellID = 0;
	bool *pIsInOutput = new bool[pInLines->GetNumberOfCells()]; 	// remember whether a line is already part of the output!
	double dCellDataTermReason[3];                                  // get cell data of stitched line pairs (or copied line)
		// [0] cell data of old line segments, 
		// [1] cell data of attached line segments
		// [2] cell data of the resulting stitched line
	double dPoint1[4];
	double dPoint2[4];

	for(int i=0; i<pInLines->GetNumberOfCells(); ++i)
		pIsInOutput[i] = false;
	//for all lines -> match line against all other lines
	for(int i=0; i<pInLines->GetNumberOfCells(); ++i) // note: GetNumberOfCells()-1 would reject a last line, that doesn't have a match
	{
		pInLines->GetCell(loc1, npts1, pts1);
		if(!pIsInOutput[i])  // optimization: skip if i-th line is already in output
		{
			loc2 = loc1 + npts1 + 1;
			for(int j=i+1; j<pInLines->GetNumberOfCells(); ++j)
			{
				pInLines->GetCell(loc2, npts2, pts2);
				loc2 += npts2 +1;
				
				// optimization: skip if j-th line is already in output
				if(pIsInOutput[j]) 
					continue;

				//line2 begins with last pt of line 1
				pInput->GetPoint (pts1[npts1-1], dPoint1);
				pInput->GetPoint (pts2[0], dPoint2);
				dPoint1[3] = pSimTime->GetTuple1(pts1[npts1-1]);
				dPoint2[3] = pSimTime->GetTuple1(pts2[0]);
				if (dPoint1[0]==dPoint2[0] && dPoint1[1]==dPoint2[1] && 
					dPoint1[2]==dPoint2[2] && dPoint1[3]==dPoint2[3])
				{
					this->StitchLines(npts1, pts1, npts2, pts2, pOutLines);
					pIsInOutput[i] = pIsInOutput[j] = true;
					dCellDataTermReason[0] = (double) pInTermReason->GetTuple1(i);
					dCellDataTermReason[1] = (double) pInTermReason->GetTuple1(j);
					dCellDataTermReason[2] = ChooseCurrentTermReason(dCellDataTermReason[0], dCellDataTermReason[1]);
					//pOutTermReason->InsertNextTuple(&dCellDataTermReason[2]);
					pOutTermReason->InsertTuple(iCurrentCellID, &dCellDataTermReason[2]);
					++iCurrentCellID;
					break;
				}

				//line1 begins with last pt of line2
				pInput->GetPoint (pts1[0], dPoint1);
				pInput->GetPoint (pts2[npts2-1], dPoint2);
				dPoint1[3] = pSimTime->GetTuple1(pts1[0]);
				dPoint2[3] = pSimTime->GetTuple1(pts2[npts2-1]);
				//if(pts1[0] == pts2[npts2-1])
				if (dPoint1[0]==dPoint2[0] && dPoint1[1]==dPoint2[1] && 
					dPoint1[2]==dPoint2[2] && dPoint1[3]==dPoint2[3])
				{
					this->StitchLines(npts2, pts2, npts1, pts1, pOutLines);
					pIsInOutput[i] = pIsInOutput[j] = true;
					dCellDataTermReason[0] = (double) pInTermReason->GetTuple1(i);
					dCellDataTermReason[1] = (double) pInTermReason->GetTuple1(j);
					dCellDataTermReason[2] = ChooseCurrentTermReason(dCellDataTermReason[0], dCellDataTermReason[1]);
					//pOutTermReason->InsertNextTuple(&dCellDataTermReason[2]);
					pOutTermReason->InsertTuple(iCurrentCellID, &dCellDataTermReason[2]);
					++iCurrentCellID;
					break;
				}

				//line1 and line 2 originate at the same point 
				//(e.g. forward-backward tracing)
				pInput->GetPoint (pts1[0], dPoint1);
				pInput->GetPoint (pts2[0], dPoint2);
				dPoint1[3] = pSimTime->GetTuple1(pts1[0]);
				dPoint2[3] = pSimTime->GetTuple1(pts2[0]);
				if (dPoint1[0]==dPoint2[0] && dPoint1[1]==dPoint2[1] && 
					dPoint1[2]==dPoint2[2] && dPoint1[3]==dPoint2[3])
				{
					//we need at least one more entry to determine "earlier" line
					if(npts1 <= 1 || npts2 <= 1)
						continue; //continue the loop otherwise
					double dNextT1 = pSimTime->GetTuple1(pts1[1]);
					double dNextT2 = pSimTime->GetTuple1(pts2[1]);
					if(dNextT1 < dNextT2)
					{
						//line 1 is "earlier" and needs to be reversed
						this->ReverseLine(npts1, pts1);
						this->StitchLines(npts1, pts1, npts2, pts2, pOutLines);
					}
					else
					{
						//line 2 is "earlier" and needs to be reversed
						this->ReverseLine(npts2, pts2);
						this->StitchLines(npts2, pts2, npts1, pts1, pOutLines);
					}
					pIsInOutput[i] = pIsInOutput[j] = true;
					dCellDataTermReason[0] = (double) pInTermReason->GetTuple1(i);
					dCellDataTermReason[1] = (double) pInTermReason->GetTuple1(j);
					dCellDataTermReason[2] = ChooseCurrentTermReason(dCellDataTermReason[0], dCellDataTermReason[1]);
					//pOutTermReason->InsertNextTuple(&dCellDataTermReason[2]);
					pOutTermReason->InsertTuple(iCurrentCellID, &dCellDataTermReason[2]);
					++iCurrentCellID;
					break;
				}
			}
			//line i could not be stitched to any other line -> just insert it anyway
			if(!pIsInOutput[i])
			{
				pOutLines->InsertNextCell(npts1, pts1);
				pIsInOutput[i] = true;
				dCellDataTermReason[2] = (double) pInput->GetCellData()->GetScalars("TERM_REASON")->GetTuple1(i);
				//pOutCD->InsertNextTuple(&iCellDataTermReason[2]);
				//pOutTermReason->InsertNextTuple(&dCellDataTermReason[2]);
				pOutTermReason->InsertTuple(iCurrentCellID, &dCellDataTermReason[2]);
				++iCurrentCellID;
			}
		}
		loc1 += npts1 + 1;
	}
	delete[] pIsInOutput;
	//make sure that the output contains at most as many cells as the input
	assert(pOutLines->GetNumberOfCells() <= pInLines->GetNumberOfCells() && "Cannot invent new cells!");

	pOutput->SetLines(pOutLines);
	pOutput->SetPoints(pInput->GetPoints());

	vtkPointData *pOutPD = pOutput->GetPointData();
	vtkPointData *pInPD = pInput->GetPointData();
	pOutPD->CopyAllocate(pInPD,pInput->GetNumberOfPoints());
	pOutPD->CopyAllOn();
	for(int i=0; i<pInput->GetNumberOfPoints(); ++i)
		pOutPD->CopyData(pInPD,i,i);
	pOutput->GetCellData()->SetScalars(pOutTermReason);

	pOutput->Squeeze();



	pOutLines->Delete();
	return 1;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   StitchLines                                                 */
/*                                                                            */
/*============================================================================*/
void vtkStitchLines::StitchLines(	vtkIdType npts1, vtkIdType *pts1, 
									vtkIdType npts2, vtkIdType *pts2,
									vtkCellArray *pOutLines)
{
	//assert that lines are matching and in correct order!
	//assert(pts1[npts1-1]==pts2[0] && "Can only stitch lines with identical ends!");
	vtkIdType noutpts = npts1+npts2-1;
	vtkIdType *outpts = new vtkIdType[noutpts];
	for(int i=0; i<npts1; ++i)
		outpts[i] = pts1[i];
	//leave out the identical point here!
	for(int i=1; i<npts2; ++i)
		outpts[i+npts1-1] = pts2[i];
	pOutLines->InsertNextCell(noutpts, outpts);
	delete[] outpts;			
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ReverseLine                                                 */
/*                                                                            */
/*============================================================================*/
void vtkStitchLines::ReverseLine(vtkIdType npts, vtkIdType *pts)
{
	vtkIdType tmp;
	for(int i=0; i<npts/2; ++i)
	{
		tmp = pts[npts-i-1];
		pts[npts-i-1] = pts[i];
		pts[i] = tmp;
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   PrintSelf                                                   */
/*                                                                            */
/*============================================================================*/
void vtkStitchLines::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ChooseCurrentTermReason                                     */
/*                                                                            */
/*============================================================================*/
double vtkStitchLines::ChooseCurrentTermReason(double dTermReason1, double dTermReason2)
{
	double dCurrentTermReason = 0.0;
	if( dTermReason1 == dTermReason2 )
	{
		dCurrentTermReason = double(VvePathlineTracer::TERM_TIME_FRAME_LEFT); 														
	}
	else 
	{	//expected changes: TERM_TIME_FRAME_LEFT->TERM_OUT_OF_DOMAIN and TERM_TIME_FRAME_LEFT->TERM_LOW_SPEED
		dCurrentTermReason = max<double>(dTermReason1,dTermReason2);
	}
	return dCurrentTermReason;
}
/*============================================================================*/
/*  END OF FILE "vtkStitchLines.cpp"                                          */
/*============================================================================*/
