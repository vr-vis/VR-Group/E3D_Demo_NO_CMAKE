/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VTKCLEANUNSTRUCTUREDGRID_H
#define _VTKCLEANUNSTRUCTUREDGRID_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkSystemIncludes.h>
#include <vtkUnstructuredGridAlgorithm.h>
#include <vtkObjectFactory.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* VtkCleanUnstructuredGrid removes unused and duplicate points from an unstructured grid.
* This can be used, e.g., to post-process an unstructured grid which resulted from
* appending several structured blocks (fully matching multi-block grid). 
*
* NOTE: This filter is able to handle multiple data fields attached to a point/cell
*		
* NOTE: Unlike vtkCleanPolyData the filter does not yet support the removal of degenerate cells
*		which might result from the joining of several nearly coinicident points, i.e., pais of 
*		points whose distance is within m_fEpsilon. 
*
* @TODO: Implement degenerate cell removal!
*
*
* @date		August 2006
* @author	Bernd Hentschel
* 
*/
class VISTAVISEXTAPI VtkCleanUnstructuredGrid : public vtkUnstructuredGridAlgorithm
{
public:
	static VtkCleanUnstructuredGrid *New();
	vtkTypeRevisionMacro(VtkCleanUnstructuredGrid,vtkUnstructuredGridAlgorithm);
	
	//void SetJoinEpsilon(vtkFloatingPointType f){
	void SetJoinEpsilon(float f){
		m_fEpsilon = f;
	}
	//vtkFloatingPointType GetJoinEpsilon() const{
	float GetJoinEpsilon() const{
		return m_fEpsilon;
	}
protected:
	//void ExecuteData(vtkDataObject *pUnused);
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
	/**
	* initialize eps=0.0f i.e. per default only exactly identical points
	* will be merged.
	*/
	VtkCleanUnstructuredGrid() : m_fEpsilon(0.0f) {}
	virtual ~VtkCleanUnstructuredGrid(){}
private:
	//vtkFloatingPointType m_fEpsilon;
	float m_fEpsilon;
};
/*============================================================================*/
/*============================================================================*/
#endif /* ifndef _VTKCLEANUNSTRUCTUREDGRID_H */

/*============================================================================*/
/*  END OF FILE "VtkCleanUnstructuredGrid.h"                                  */
/*============================================================================*/

