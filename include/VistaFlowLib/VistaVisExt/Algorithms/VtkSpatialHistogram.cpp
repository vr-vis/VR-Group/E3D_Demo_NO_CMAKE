/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VtkSpatialHistogram.h"

#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkIntArray.h>
#include <vtkPointData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkImageData.h>
#include <vtkStreamingDemandDrivenPipeline.h>

#include <VistaBase/VistaStreamUtils.h>
#include <algorithm>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
vtkCxxRevisionMacro(vtkSpatialHistogram, "$Id$");
vtkStandardNewMacro(vtkSpatialHistogram);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
vtkSpatialHistogram::vtkSpatialHistogram()
	:	m_strOutputFieldName("count"),
		m_strArrayToPlot(""),
		m_iAxisToPlot(0),
		m_bOverrideTargetAxis(false),
		m_bOverrideTargetScalar(false)
{
	m_fBounds[0] = m_fBounds[2] = m_fBounds[4] = 0.0f;
	m_fBounds[1] = m_fBounds[3] = m_fBounds[5] = 1.0f;
	m_iResolution[0] = m_iResolution[1]= 32;
	m_iResolution[2] = 0;
	m_fTargetAxisRange[0] = m_fTargetAxisRange[1] = 0.0f;
	m_fTargetScalarRange[0] = m_fTargetScalarRange[1] = 0.0f;
	this->RecomputeTargetDims(true);
}
//------------------------------------------------------------
//------------------------------------------------------------
vtkSpatialHistogram::~vtkSpatialHistogram()
{
}
/*============================================================================*/
/*  IMPLEMENTATION															  */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::PrintSelf(ostream& os, vtkIndent indent)
{
	vtkImageAlgorithm::PrintSelf(os, indent);
	os << indent << "Output field name : " << m_strOutputFieldName << endl;
	os << indent << "Bounds : " << m_fBounds[0] << ", " << m_fBounds[1] << ", " << m_fBounds[2] << ", " << m_fBounds[3] << ", " << m_fBounds[3] << ", " << m_fBounds[5] << endl;
	os << indent << "Spacing    : " << m_fSpacing[0] << ", " << m_fSpacing[1] << ", " << m_fSpacing[2] << endl;
	os << indent << "Resolution : " << m_iResolution[0] << ", " << m_iResolution[1] << ", " << m_iResolution[2] << endl;
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::SetTargetBounds(float fBounds[6])
{
	memcpy(m_fBounds, fBounds, 6*sizeof(float));
	this->RecomputeTargetDims(true);
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::GetTargetBounds(float fBounds[6]) const
{
	memcpy(fBounds, m_fBounds, 6*sizeof(float));
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::SetResolution(int iRes[2])
{
	memcpy(m_iResolution, iRes, 2*sizeof(int));
	this->RecomputeTargetDims(true);
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::GetResolution(int iRes[2]) const
{
	memcpy(iRes, m_iResolution, 2*sizeof(int));
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::SetSpacing(float fSpacing[2])
{
	memcpy(m_fSpacing, fSpacing, 2*sizeof(float));
	this->RecomputeTargetDims(false);
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::GetSpacing(float fSpacing[2]) const
{
	memcpy(fSpacing, m_fSpacing, 2*sizeof(float));
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::SetOutputFieldName(const std::string& strOutputFieldName)
{
	m_strOutputFieldName = strOutputFieldName;
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
std::string vtkSpatialHistogram::GetOutputFieldName() const
{
	return m_strOutputFieldName;
}

//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::SetArrayToPlot(const std::string& strFieldName)
{
	m_strArrayToPlot = strFieldName;
}
//------------------------------------------------------------
//------------------------------------------------------------
std::string vtkSpatialHistogram::GetArrayToPlot() const
{
	return m_strArrayToPlot;
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::SetAxisToPlot(int iAxis)
{
	m_iAxisToPlot = iAxis;
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtkSpatialHistogram::GetAxisToPlot() const
{
	return m_iAxisToPlot;
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::OverrideTargetAxisRange(float fRange[2])
{
	m_bOverrideTargetAxis = true;
	m_fTargetAxisRange[0] = fRange[0];
	m_fTargetAxisRange[1] = fRange[1];
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::OverrideTargetAttributeRange(float fRange[2])
{
	m_bOverrideTargetScalar = true;
	m_fTargetScalarRange[0] = fRange[0];
	m_fTargetScalarRange[1] = fRange[1];
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::CancelOverrideTargetAxisRange()
{
	m_bOverrideTargetAxis = false;
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::CancelOverrideTargetScalarRange()
{
	m_bOverrideTargetScalar = false;
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtkSpatialHistogram::RequestData(vtkInformation *,
									 vtkInformationVector **inputVector,
									 vtkInformationVector *outputVector)
{
	// get the info objects
	//our input is the first input on the first port of this filter
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkDataSet	 *pInput = vtkDataSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkImageData *pOutput = vtkImageData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pOutput)
	{
		vstr::errp() << "[vtkSpatialHistogram::RequestData] no output!" << endl;
		return -1;
	}
	if(!pInput)
	{
		vstr::errp() << "[vtkSpatialHistogram::RequestData] no input!" << endl;
		return -1;
	}
	vtkDataArray *pAttToPlot = pInput->GetPointData()->GetArray(m_strArrayToPlot.c_str());
	if(!pAttToPlot)
	{
		vstr::errp() << "[vtkSpatialHistogram::RequestData] att to plot not found!" << endl;
		return -1;
	}
	if(pAttToPlot->GetNumberOfComponents() > 1)
	{
		vstr::errp() << "[vtkSpatialHistogram::RequestData] only working on scalar data!" << endl;
		return -1;
	}

	//setup the output
	pOutput->Initialize();
	pOutput->SetOrigin(m_fBounds[0], m_fBounds[2], 0.0);
	pOutput->SetSpacing(m_fSpacing[0], m_fSpacing[1], 1.0);
	pOutput->SetDimensions(m_iResolution[0], m_iResolution[1], 1);

#if VTK_MAJOR_VERSION > 5
	pOutput->AllocateScalars(VTK_FLOAT, 1);
#else
	pOutput->SetNumberOfScalarComponents(1);
	pOutput->SetScalarTypeToFloat();
	pOutput->AllocateScalars();
#endif

	pOutput->GetPointData()->GetScalars()->SetName(m_strOutputFieldName.c_str());
	float *pScalars = static_cast<float*>(pOutput->GetScalarPointer());
	memset(pScalars, 0, pOutput->GetNumberOfPoints()*sizeof(float));

	double dPlotCoords[3] = {0.0, 0.0, 0.0};
	double dCurrentPt[3];
	double dMaxCount = 0;
	int iCurrentPt;
	double dRange[2];
	double dBounds[6];


	double dAxisMin;
	double dRescale[2];

	if(!m_bOverrideTargetAxis)
	{
		pInput->GetBounds(dBounds);
		//rescale axis to according to data bounds
		dRescale[0]= (m_fBounds[1] - m_fBounds[0]) / (dBounds[2*m_iAxisToPlot+1] - dBounds[2*m_iAxisToPlot]);
		dAxisMin=dBounds[2*m_iAxisToPlot];
	}
	else
	{
		//rescale axis to according to bounds set by user
		dRescale[0]= (m_fBounds[1] - m_fBounds[0]) / (m_fTargetAxisRange[1] - m_fTargetAxisRange[0]);
		dAxisMin=m_fTargetAxisRange[0];
	}
	if(!m_bOverrideTargetScalar)
	{
		//rescale scalar data according to data range
		pAttToPlot->GetRange(dRange);
	}
	else
	{
		//rescale scalar data according to user-set range
		dRange[0] = m_fTargetScalarRange[0];
		dRange[1] = m_fTargetScalarRange[1];
	}
	//rescale
	dRescale[1] = (m_fBounds[3] - m_fBounds[2]) / (dRange[1] - dRange[0]);

	//for each point in the input -> determine cell and increment count
	for(int i=0; i<pInput->GetNumberOfPoints(); ++i)
	{
		//fetch input point's coordinate and rescale to target x axis
		pInput->GetPoint(i, dCurrentPt);
		dPlotCoords[0] = m_fBounds[0] + (dCurrentPt[m_iAxisToPlot] - dAxisMin)* dRescale[0];
		//fetch input point's attribute value and rescale to target y axis
		pAttToPlot->GetTuple(i, &dPlotCoords[1]);
		dPlotCoords[1] = m_fBounds[2] + (dPlotCoords[1] - dRange[0]) * dRescale[1];
		//find the closest point in the grid and increment bin count
		iCurrentPt = pOutput->FindPoint(dPlotCoords);
		if(iCurrentPt != -1)
		{
			pScalars[iCurrentPt] += 1.0f;
			//determine max count for normalization of target attribute
			dMaxCount = std::max<double>(pScalars[iCurrentPt], dMaxCount);
		}
	}

	if(dMaxCount > 1)
	{
		//normalize output counts
		double dNorm = (m_fBounds[5] - m_fBounds[4]) / dMaxCount;
		const unsigned int iNumPts = pOutput->GetNumberOfPoints();
		for(register unsigned int i=0; i<iNumPts; ++i)
		{
			pScalars[i] = m_fBounds[4] + pScalars[i] * dNorm;
		}
	}
	else
	{
		vstr::warnp() << "[vtkSpatialHistogram::RequestData] Generated MaxCount <= 1" << endl;
	}

	int iExtent[6];
	pOutput->GetExtent(iExtent);

	return 1;
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtkSpatialHistogram::FillInputPortInformation(int port, vtkInformation *info)
{
	info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
	return 1;
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtkSpatialHistogram::RequestInformation (vtkInformation* vtkNotUsed(request),
											 vtkInformationVector** inputVector,
											 vtkInformationVector* outputVector)
{
  // get the info objects
  vtkInformation* outInfo = outputVector->GetInformationObject(0);
  int wholeExtent[6] ={0, m_iResolution[0] - 1, 0, m_iResolution[1] - 1, 0, 1};
  double outAR[3] = {m_fSpacing[0], m_fSpacing[1], 1};
  double outOrigin[3] = {m_fBounds[0], m_fBounds[2], m_fBounds[4]};
  outInfo->Set(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(), wholeExtent ,6);
  outInfo->Set(vtkDataObject::SPACING(), outAR, 3);
  outInfo->Set(vtkDataObject::ORIGIN(), outOrigin, 3);

  return 1;
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtkSpatialHistogram::RecomputeTargetDims(bool bResChanged)
{
	if(bResChanged)
	{
		//recompute spacing
		m_fSpacing[0] = (m_fBounds[1] - m_fBounds[0]) / ((float) m_iResolution[0]-1);
		m_fSpacing[1] = (m_fBounds[3] - m_fBounds[2]) / ((float) m_iResolution[1]-1);
	}
	else
	{
		//recompute resolution
		m_iResolution[0] = (int) ((m_fBounds[1] - m_fBounds[0]) / m_fSpacing[0]);
		m_iResolution[1] = (int) ((m_fBounds[3] - m_fBounds[2]) / m_fSpacing[1]);

	}
	//ensure that nothing happens in the z axis (we are only 2D!)
	m_iResolution[2] = 1;
	m_fSpacing[2] = 1.0f;

	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------


