/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VveInstatInterpolator.h"
#include "../Data/VveDiscreteDataTyped.h"
#include "../Tools/VveTimeMapper.h"

#include <vtkDataSet.h>
#include <vtkStructuredGrid.h>
#include <vtkGenericCell.h>
#include <vtkDataArray.h>
#include <vtkPointData.h>
#include <vtkInterpolatedVelocityField.h>

#include <VistaBase/VistaStreamUtils.h>

#include <iostream>
#include <cassert>

using namespace std;

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveInstatInterpolator::VveInstatInterpolator()
:	m_pData(NULL),
	m_strDataFieldName(""),
	m_pMapper(NULL),
    m_bTimeChanged(true),
	m_bInterpolateVectors(false)
{
	for(int i=0; i<2; ++i)
	{
		m_pActiveDataSets[i] = 0;
		m_pActiveArrays[i] = 0;
		m_pActiveCells[i] = vtkGenericCell::New();
		m_iLastCell[i] = -1;
		m_dTimeIndexSimTimes[i] = -1.0;

		for(int j=0; j<9; ++j){
			m_arrInterpolResults[i][j] = 0.0;
		}
	}
}


VveInstatInterpolator::~VveInstatInterpolator()
{
	m_pActiveCells[0]->Delete();
	m_pActiveCells[1]->Delete();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetData                                                     */
/*                                                                            */
/*============================================================================*/
void VveInstatInterpolator::SetData(VveDiscreteDataTyped<vtkDataSet>* pData)
{
	m_pData = pData;
	m_pMapper = m_pData->GetTimeMapper();
	if(m_pData->GetNumberOfLevels() == 0)
	{
		vstr::errp() << "[VveInstatInterpolator::SetData] Can't operate on empty data object..." << endl;
		m_pData = NULL;
		m_pMapper = NULL;
		return;
	}
	//seek first non null entry and get app. data field
	for(int i=0; i<m_pData->GetNumberOfLevels(); ++i)
	{
		vtkDataSet *pFirst = m_pData->GetTypedLevelDataByLevelIndex(i)->GetData();
		if(pFirst)
		{
			if(!m_strDataFieldName.empty() && pFirst->GetPointData()->GetArray(m_strDataFieldName.c_str()))
				m_pActiveArrays[0] = pFirst->GetPointData()->GetArray(m_strDataFieldName.c_str());
			else if(m_bInterpolateVectors) //default to vectors if bit is set app.
				m_pActiveArrays[0] = pFirst->GetPointData()->GetVectors();
			else //default to scalars
				m_pActiveArrays[0] = pFirst->GetPointData()->GetScalars();
			break;
		}
	}
	//force new data set setup!
	m_bTimeChanged = true;
	m_dTimeIndexSimTimes[0] = -1.0;
	m_dTimeIndexSimTimes[1] = -1.0;

	m_iLastCell[0] = m_iLastCell[1] = -1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   FunctionValues                                              */
/*                                                                            */
/*============================================================================*/
int VveInstatInterpolator::FunctionValues(double* x, double* f)
{

	if(!SetupDataSets(x))
	{

		vstr::debugi() << "[InstatInterpol] SetupDataSets failed" << endl;
		vstr::debugi() << " -> Probe may be out of time range " << endl;
		vstr::debugi() << "\tProbe position: [" << x[0] << "," << x[1] << ",";
		vstr::debug() << x[2] << "," << x[3] << "]" << endl;

		return 0;
	}
	if(!InterpolateProbes(x))
	{

		vstr::debugi() << "[InstatInterpol] Interpolation failed" << endl;
		vstr::debugi() << " -> Probe may be out of space domain " << endl;
		vstr::debugi() << "\tProbe position: [" << x[0] << "," << x[1] << ",";
		vstr::debug() << x[2] << "," << x[3] << "]" << endl;

		return 0;
	}
	
	//do the actual interpolation in time!
	double dTimeInterpolCoeff =	(x[3] - m_dTimeIndexSimTimes[0]) / 
							(m_dTimeIndexSimTimes[1] - m_dTimeIndexSimTimes[0]);
	double dOMA = 1.0f - dTimeInterpolCoeff;
	for(register int i=0; i<m_pActiveArrays[0]->GetNumberOfComponents(); ++i)
	{
		f[i] = dOMA * m_arrInterpolResults[0][i] + dTimeInterpolCoeff * m_arrInterpolResults[1][i];
	}
		
	m_bTimeChanged = false;
	return 1;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDefaultInterpolationMode                                 */
/*                                                                            */
/*============================================================================*/
void VveInstatInterpolator::SetDefaultInterpolationMode(int iMode)
{
	m_bInterpolateVectors = (iMode == INT_VECTORS);
	m_bInterpolateTensors = (iMode == INT_TENSORS);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDefaultInterpolationMode                                 */
/*                                                                            */
/*============================================================================*/
int VveInstatInterpolator::GetDefaultInterpolationMode() const
{
	if(m_bInterpolateVectors) 
		return INT_VECTORS;
	else if(m_bInterpolateTensors) 
		return INT_TENSORS;
	else                            
		return INT_SCALARS;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDataFieldName                                            */
/*                                                                            */
/*============================================================================*/
void VveInstatInterpolator::SetDataFieldName(const std::string& strFieldName)
{
	m_strDataFieldName = strFieldName;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDataFieldName                                            */
/*                                                                            */
/*============================================================================*/
std::string VveInstatInterpolator::GetDataFieldName() const
{
	return m_strDataFieldName;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumberOfFunctions                                        */
/*                                                                            */
/*============================================================================*/
int VveInstatInterpolator::GetNumberOfFunctions()
{
	if(m_pActiveArrays[0])
		return m_pActiveArrays[0]->GetNumberOfComponents();
	return 0;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumInterpolCells                                         */
/*                                                                            */
/*============================================================================*/
int VveInstatInterpolator::GetNumInterpolCells() const
{
	return 2;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLastCells                                                */
/*                                                                            */
/*============================================================================*/
void VveInstatInterpolator::GetLastCells(	vtkIdType *pCells, 
											vtkIdType *pTimeIdx) const
{
	pCells[0] = m_iLastCell[0];
	pCells[1] = m_iLastCell[1];
	pTimeIdx[0] = m_arrTimeIndices[0];
	pTimeIdx[1] = m_arrTimeIndices[1];
}
	
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetupDataSets                                               */
/*                                                                            */
/*============================================================================*/
bool VveInstatInterpolator::SetupDataSets(double x[4])
{
	//if time frame is still valid? -> return immediately
	if(x[3]>m_dTimeIndexSimTimes[0] && x[3]<=m_dTimeIndexSimTimes[1])
		return true;

	//remember that the time has changed -> this invalidates cached cells
	m_bTimeChanged = true;

	float fSimTime = x[3];
	float fVisTime = m_pMapper->GetVisualizationTime(fSimTime);

	if(fVisTime < 0 || fVisTime >= 1)
	{
		return false;
	}

	//one time index is always the one we are currently "living" in
	m_arrTimeIndices[0] = m_pMapper->GetTimeIndex(fVisTime);
	//the other one is either m_arrTimeIndices[0]+1 or m_arrTimeIndices[0]-1 
	//depending on the sim time
	float fSimTimeTI1 = m_pMapper->GetSimulationTime(m_arrTimeIndices[0]);
	if(x[3] < fSimTimeTI1)
	{
		//m_arrTimeIndices[0] is the "upper" time index 
		//-> assign it to m_arrTimeIndices[1] and decrease m_arrTimeIndices[0]
		m_arrTimeIndices[1] = m_arrTimeIndices[0]; 
		//take care of first time step (i.e., underrun)
		m_arrTimeIndices[0] = std::max<int>(0, m_arrTimeIndices[0]-1);
	}
	else
	{
		//m_arrTimeIndices[0] is the "lower" time index
		//take care of last time step (i.e., overrun)
		m_arrTimeIndices[1] = std::min<int>(
			m_pMapper->GetNumberOfTimeIndices()-1, m_arrTimeIndices[0]+1);
	}

	//NOTE: Take care of conversion between TIME INDEX and LEVEL INDEX! 
	//vecDataSets is accessed by LEVEL INDICES!
	
	if(m_pData->GetTypedLevelDataByLevelIndex(
		m_pMapper->GetLevelIndex(m_arrTimeIndices[0])) == NULL)
	{
		vstr::out() << endl << endl;
		vstr::errp() << "[VveInstatInterpolator::SetupDataSets] Data level not available " << endl;
		vstr::erri() << "\tTI: " << m_arrTimeIndices[0] << " ";
		vstr::err() << "TLI: " << m_pMapper->GetLevelIndex(m_arrTimeIndices[0]) << endl << endl;
		return false;
	}
	m_pActiveDataSets[0] = m_pData->GetTypedLevelDataByLevelIndex(
		m_pMapper->GetLevelIndex(m_arrTimeIndices[0]))->GetData();
	
	if(m_pData->GetTypedLevelDataByLevelIndex(
		m_pMapper->GetLevelIndex(m_arrTimeIndices[1])) == NULL)
	{
		vstr::out() << endl << endl;
		vstr::errp() << "[VveInstatInterpolator::SetupDataSets] Data level not available " << endl;
		vstr::erri() << "\tTI: " << m_arrTimeIndices[1] << " ";
		vstr::err() << "TLI: " << m_pMapper->GetLevelIndex(m_arrTimeIndices[1]) << endl << endl;
		return false;
	}
	m_pActiveDataSets[1] = m_pData->GetTypedLevelDataByLevelIndex(
		m_pMapper->GetLevelIndex(m_arrTimeIndices[1]))->GetData();

	//retrieve data arrays for interpolation
	if(!m_strDataFieldName.empty())
	{
		m_pActiveArrays[0] = m_pActiveDataSets[0]->GetPointData()->GetArray(
			m_strDataFieldName.c_str());
		m_pActiveArrays[1] = m_pActiveDataSets[1]->GetPointData()->GetArray(
			m_strDataFieldName.c_str());
	}
	else if(m_bInterpolateVectors)
	{
		m_pActiveArrays[0] = 
			m_pActiveDataSets[0]->GetPointData()->GetVectors();
		m_pActiveArrays[1] = 
			m_pActiveDataSets[1]->GetPointData()->GetVectors();
	}
	else if(m_bInterpolateTensors)
	{
		m_pActiveArrays[0] =
			m_pActiveDataSets[0]->GetPointData()->GetTensors();
		m_pActiveArrays[1] =
			m_pActiveDataSets[1]->GetPointData()->GetTensors();
	}
	else
	{
		m_pActiveArrays[0] = 
			m_pActiveDataSets[0]->GetPointData()->GetScalars();
		m_pActiveArrays[1] = 
			m_pActiveDataSets[1]->GetPointData()->GetScalars();
	}
	
	if(!m_pActiveArrays[0] || !m_pActiveArrays[1] || 
		m_pActiveArrays[0]->GetNumberOfComponents() != 
		m_pActiveArrays[1]->GetNumberOfComponents())
	{
#ifdef DEBUG
		vstr::out() << endl << endl;
		vstr::warnp() << "[VveUnsteadyDataInterpolator::SetupDataSets] ";
		vstr::warn() << "NO OR NON-MATCHING ARRAY DATA!" << endl << endl;
#endif
		return false;
	}

	m_dTimeIndexSimTimes[0] = m_pMapper->GetSimulationTime(m_arrTimeIndices[0]);
	m_dTimeIndexSimTimes[1] = m_pMapper->GetSimulationTime(m_arrTimeIndices[1]);

	float fIndexSimTime = m_dTimeIndexSimTimes[1];
	
	//assert the correct order of time steps!
	assert(m_dTimeIndexSimTimes[0] <= m_dTimeIndexSimTimes[1] 
			&& "Corrupted simulation time information during setup!");
	assert(fSimTime <= fIndexSimTime 
			&& "Corrupted simulation time information during setup!");

	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   InterpolateProbes                                           */
/*                                                                            */
/*============================================================================*/
bool VveInstatInterpolator::InterpolateProbes(double x[4])
{
	double pCoords[8], weights[8], data[9];
	int subId;
	
	//for each of the two time steps
	for(int iTimeIdx=0;iTimeIdx<2; ++iTimeIdx)
	{
		if(m_bTimeChanged)
		{
			m_iLastCell[iTimeIdx] = 
				m_pActiveDataSets[iTimeIdx]->FindCell(x,NULL,0,0,subId,pCoords,weights);
			//check if we are outside the data set!
			if(m_iLastCell[iTimeIdx] < 0)
				return false;
			m_pActiveDataSets[iTimeIdx]->GetCell(m_iLastCell[iTimeIdx],
												 m_pActiveCells[iTimeIdx]);
		}
		else
		{
			//if so -> check the surrounding cells
			m_iLastCell[iTimeIdx] = 
				m_pActiveDataSets[iTimeIdx]->FindCell(x,m_pActiveCells[iTimeIdx], 
														m_iLastCell[iTimeIdx], 
														0, subId, pCoords, weights);
			//check if we are outside the data set!
			if(m_iLastCell[iTimeIdx] < 0)
				return false;
			m_pActiveDataSets[iTimeIdx]->GetCell(m_iLastCell[iTimeIdx],
												 m_pActiveCells[iTimeIdx]);
		}
		//init data field
		for(register int i=0; i<m_pActiveArrays[iTimeIdx]->GetNumberOfComponents(); ++i)
			m_arrInterpolResults[iTimeIdx][i] = 0.0f;
		/*
		* data should be valid by now -> Interpolate
		*/
		for(register int i=0; i<m_pActiveCells[iTimeIdx]->GetNumberOfPoints(); ++i)
		{
			m_pActiveArrays[iTimeIdx]->GetTuple(m_pActiveCells[iTimeIdx]->GetPointId(i),data);
			for(register int j=0; j<m_pActiveArrays[iTimeIdx]->GetNumberOfComponents(); ++j)
				m_arrInterpolResults[iTimeIdx][j] += data[j] * weights[i];
		}
	}
	return true;
}
/*============================================================================*/
/*  END OF FILE "VveInstatInterpolator.cpp"                                   */
/*============================================================================*/
