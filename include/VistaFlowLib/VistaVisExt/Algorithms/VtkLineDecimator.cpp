/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VtkLineDecimator.h"

#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkMath.h>
#include <vtkBitArray.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkInformationVector.h>
#include <vtkInformation.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
vtkCxxRevisionMacro(vtkLineDecimator, "$Revision: 1.4 $");
vtkStandardNewMacro(vtkLineDecimator);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
vtkLineDecimator::vtkLineDecimator()
: m_fMaximumRelativeError(1.0f)
{
}

vtkLineDecimator::~vtkLineDecimator()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetMaximumRelativeError                                     */
/*                                                                            */
/*============================================================================*/
void vtkLineDecimator::SetMaximumRelativeError(const float fEpsilon)
{
	m_fMaximumRelativeError = 1.0f + fEpsilon;
	this->Modified();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetMaximumRelativeError                                     */
/*                                                                            */
/*============================================================================*/
float vtkLineDecimator::GetMaximumRelativeError() const
{
	return m_fMaximumRelativeError - 1.0f;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   RequestData                                                 */
/*                                                                            */
/*============================================================================*/
int vtkLineDecimator::RequestData(	vtkInformation *, 
									vtkInformationVector **pInVec, 
									vtkInformationVector *pOutVec)
{
	vtkInformation *pInInfo = pInVec[0]->GetInformationObject(0);
	vtkInformation *pOutInfo = pOutVec->GetInformationObject(0);

	// get the input and ouptut
	vtkPolyData *pInput = vtkPolyData::SafeDownCast(pInInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkPolyData *pOutput = vtkPolyData::SafeDownCast(pOutInfo->Get(vtkDataObject::DATA_OBJECT()));
	
	if(!pInput || !pOutput)
		return 0;
	
	vtkCellArray *pInLines = pInput->GetLines();

	if(!pInLines || pInLines->GetNumberOfCells() == 0)
		return 0;

	vtkPoints *pInPts = pInput->GetPoints();
	vtkPoints *pOutPts = vtkPoints::New();
	pOutPts->Allocate(pInPts->GetNumberOfPoints());

	vtkPointData *pInPD = pInput->GetPointData();
	vtkCellData *pInCD = pInput->GetCellData();

	vtkPointData *pOutPD = pOutput->GetPointData();
	vtkCellData *pOutCD = pOutput->GetCellData();
	
	pOutPD->CopyAllocate(pInPD);
	//since we generate one line for each input line -> copy cell data directly!
	pOutCD->DeepCopy(pInCD);
	
	//allocate memory for new cell array (we will need at most as many entries as in the current one)
	vtkCellArray *pOutLines = vtkCellArray::New();
	pOutLines->Allocate(pInLines->GetSize());

	//decimate lines
	vtkIdType *pCurLinePts, iCurLineNumPts;
	vtkIdType *pNewLinePts, iNewLineNumPts;
	pInLines->InitTraversal();
	while(pInLines->GetNextCell(iCurLineNumPts, pCurLinePts))
	{
		this->DecimateLine(pInPts, iCurLineNumPts, pCurLinePts, iNewLineNumPts, pNewLinePts);
		this->CopyLineToOutput(pInPts, pInPD, iNewLineNumPts, pNewLinePts, pOutPts, pOutPD);
		pOutLines->InsertNextCell(iNewLineNumPts, pNewLinePts);
		//DON'T YOU DARE LOSE MEMORY HERE! (allocated in DecimateLine)
		delete [] pNewLinePts;
	}
	//assemble output
	pOutput->SetPoints(pOutPts);
	pOutPts->Delete();
	pOutput->SetLines(pOutLines);
	pOutLines->Delete();
	pOutput->
Squeeze();
	return 1;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   DecimateLine                                                */
/*                                                                            */
/*============================================================================*/
void vtkLineDecimator::DecimateLine(vtkPoints *pPts, 
									vtkIdType iNumInPts, vtkIdType *pInPts, 
									vtkIdType& iNumOutPts, vtkIdType *&pOutPts)
{
	//small lines won't be decimated anyway
	if(iNumInPts <= 3)
	{
		iNumOutPts = iNumInPts;
		pOutPts = new vtkIdType[iNumOutPts];
		memcpy(pOutPts, pInPts, iNumOutPts * sizeof(vtkIdType));
		return;
	}

	//we need at most as many points as there are in the input
	double fCurrentLen = 0.0f;
	double fOriginalLen = 0.0f;
	double pStartPt[3], pCurPt[3], pPrevPt[3];
	
	iNumOutPts = 1;
	pOutPts = new vtkIdType[iNumInPts];
	pOutPts[0] = pInPts[0];
	vtkIdType iCurrentStartPtIdx = 0;
	pPts->GetPoint(pInPts[0], pStartPt);
	vtkIdType iCurrentPtIdx = 1;
	vtkIdType iNextInsertIdx = 1;
	
	while(iCurrentPtIdx < iNumInPts) //while there are points to process
	{
		//advance along line as long as error is o.k.
		while(m_fMaximumRelativeError * fCurrentLen >= fOriginalLen && iCurrentPtIdx < iNumInPts)
		{
			pPts->GetPoint(pInPts[iCurrentPtIdx], pCurPt);
			pPts->GetPoint(pInPts[iCurrentPtIdx-1], pPrevPt);
			//compute length of current decimated line segment
			fCurrentLen = sqrt(vtkMath::Distance2BetweenPoints(pCurPt, pStartPt));
			//accumulate length for current original segments
			fOriginalLen += sqrt(vtkMath::Distance2BetweenPoints(pCurPt, pPrevPt));
			++iCurrentPtIdx;
		}
		//add a line segment from [iCurrentStartPtIdx] to [iCurrentPtIdx-1]
		pOutPts[iNumOutPts] = pInPts[iCurrentPtIdx-1];
		++iNumOutPts;
		//reset for next iteration
		iCurrentStartPtIdx = iCurrentPtIdx-1;
		pPts->GetPoint(pInPts[iCurrentStartPtIdx], pStartPt);
		fCurrentLen = 0.0f;
		fOriginalLen = 0.0f;
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CopyLineToOutput                                            */
/*                                                                            */
/*============================================================================*/
void vtkLineDecimator::CopyLineToOutput(vtkPoints* pSrcPts, 
										vtkPointData* pSrcPD, 
										vtkIdType iNumPts, 
										vtkIdType *pPtIds, 
										vtkPoints *pTgtPts, 
										vtkPointData *pTgtPD)
{
	/*
	* NOTE: point ids in the new vtkPoints object are different from those in the original one
	* pPtIds refers to pts in pSrcPts. To get a valid output line we have to translate
	* pPtIds to the new enumeration! We do this here on the fly!
	*/
	int iCurrentPtId = 0;
	for(register int i=0; i<iNumPts; ++i)
	{
		//copy point location to pTgtPts
		iCurrentPtId = pTgtPts->InsertNextPoint(pSrcPts->GetPoint(pPtIds[i]));
		//copy point data
		pTgtPD->CopyData(pSrcPD,pPtIds[i], iCurrentPtId);
		//assign new point's index to current cell enumeration slot
		pPtIds[i] = iCurrentPtId;
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   PrintSelf                                                   */
/*                                                                            */
/*============================================================================*/
void vtkLineDecimator::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
/*============================================================================*/
/*  END OF FILE "vtkLineDecimator.cpp"                                        */
/*============================================================================*/
