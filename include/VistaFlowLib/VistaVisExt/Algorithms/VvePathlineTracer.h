/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/

#ifndef _VVEPATHLINETRACER_H
#define _VVEPATHLINETRACER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <vector>
#include <list>

#include <VistaVisExt/VistaVisExtConfig.h>
#include "../Data/VveParticlePopulation.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTimeMapper;
template <class T> class VveDiscreteDataTyped;
class vtkPoints;
class vtkPointSet;
class vtkPolyData;
class vtkDataSet;
class vtkDataArray;
class vtkIdList;
class VistaVector3D;
class VveInstatInterpolator;
class vtkCellArray;
class vtkInitialValueProblemSolver;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VvePathlineTracer is a simple pathline tracer based on VTK and
 * VVE data strucutres but not tightly integrated with VTK itself 
 * i.e. it is not (yet) a full blown VTK filter.
 *
 * @todo make it a "real filter"
 * @todo add support for native VVE particle data output
 * @todo ...so many things....
 *
 * @author	Bernd Hentschel
 * @date	September 2006
 *
 */
class VISTAVISEXTAPI VvePathlineTracer
{
public:
	VvePathlineTracer();
	virtual ~VvePathlineTracer();
	/**
	 * This will not only set the unsteady data, but also
	 * update the TimeFrame's to the data's ranges.
	 *
	 */
	void SetData(VveDiscreteDataTyped<vtkDataSet>* pData);
	VveDiscreteDataTyped<vtkDataSet>* GetData() const; 
	/**
	 * Provide seed point set via VTK point set data structure.
	 * Additionally an array of sim time values can be provided
	 * which then uniquely defines starting positions in 4D space-time.
	 * If this array is left out, the sim time start is assumed.
	 *
	 * NOTE: internally the points are copied to another data structure
	 * for convenience. Any updates to the vtkPointSet that occur
	 * after a call to SetSeed will therefore be lost! So always make
	 * sure, that a) your point set is current BEFORE you call SetSeed
	 * and b) that you call SetSeed after EVERY change to your point set!
	 */
	void SetSeed(vtkPointSet *pSource, double *pStartTimes);
	void GetSeed(vtkPointSet *pSeed, double *pStartTimes) const;
	/**
	 * Give seed points in raw double arrays. The format assumes
	 * that 4 doubles are defined per point so as to define the 
	 * seeds in 4D space-time! DO NOT GIVE ONLY 3 COORDINATES PER POINT
	 * HERE BECAUSE THIS WILL MAKE THE METHOD CRASH!
	 *
	 * NOTE: Points are copied internally so make sure you take care
	 *       of the input fPts pointer's content afterwards...
	 */
	void SetSeed(double *fPts, int iNumSeeds);
	void GetSeed(double *&fPts, int& iNumSeed) const;
	/**
	 * Give seeds by means of VistaVector3D
	 * Note that the homogeneous coordinate is interpreted as sim time
	 * this defining a 4D space-time point. So make sure you ALWAYS
	 * set the value appropriately, since it will ALLWAYS be interpreted 
	 * as time value.
	 */
	void SetSeed(const std::vector<VistaVector3D>& vecSeeds);
	void GetSeed(std::vector<VistaVector3D>& vecSeed) const;
	/**
	 *
	 */
	int GetNumSeedPts() const;
	/**
	 *
	 */
	void SetIntegrationStep(double dStep);
	double GetIntegrationStep() const;
	/**
	 *
	 */
	void SetTerminalSpeed(double dSpeed);
	double GetTerminalSpeed() const;

	/**
	 * Should be called after SetData
	 */
	void SetTimeFrame(double dSimStart, double dSimEnd);
	void GetTimeFrame(double& dSimStart, double& dSimEnd) const;

	/**
	 * Get/Set the point attributes which will be interpolated along
	 * the pathline.
	 * Per default ALL point fields available in the input data will 
	 * be interpolated.
	 */
	void SetFieldsToInterpolate(const std::list<std::string>& liFieldNames);
	void GetFieldsToInterpolate(std::list<std::string>& liFieldNames) const;

	/**
	 *
	 */
	enum{
		TRACE_FORWARD,
		TRACE_BACKWARD,
		TRACE_BOTH
	};

	/**
	 * Set/Get whether to trace forward in time, backward in time
	 * or in both directions (using the above enum).
	 */
	void SetTraceMode(int i);
	int GetTraceMode() const;
	/**
	 *
	 */
	void SetRecordCells(bool b);
	bool GetRecordCells() const;

	/**
	 * helper struct carrying cell ids and corrsponding time
	 * indices for visited cell inquiry
	 */
	struct SCellID{
		SCellID(int iID, int iTime)
			:	m_iID(iID), m_iTimeIdx(iTime) {}
		~SCellID(){}

		int m_iID, m_iTimeIdx;
	};
	
	/**
	 * inquire the cells that have been visited during the last update
	 */
	void GetVisitedCells(std::list<SCellID>& liCells) const;

	enum TERM_REASON{
		TERM_TIME_FRAME_LEFT,
		TERM_OUT_OF_DOMAIN,
		TERM_LOW_SPEED,
		TERM_UNKNOWN
	};

	/**
	 * actually compute particle traces....
	 */
	void Execute();

	/**
	 * output conversion routines for convenience
	 * @todo think about the interface...
	 */
	vtkPolyData *GetOutput();

	/**
	* Get particle population, pPop must != NULL
	*
	*/
	void GetOutput( VveParticlePopulation *pPop);


protected:

	/**
	 * convenience method to allocate new data arrays and
	 * corresponding interpolators given a template array 
	 * from the input data.
	 */
	void AllocateDataArray(	vtkDataArray *pTemplate,
							unsigned int iAllocSize,
							vtkDataArray *&pNewArray,
							VveInstatInterpolator *&pInterpolator) const;
	/**
	 * internal helper: Trace a single pathline given an initial configuration 
	 * with the first pt already being in the output lits pCurrentLine.
	 *
	 * @param[in] dStartPt				the seed position from which on out the 
	 *								trace proceeds
	 * @param[in] dIntegrationStep		the integration time step given in 
									simulation time
	 * @param[in] pInterpolator			the main interpolator for the underlying 
	 *								vector field
	 * @param[in] pIntegrator			the integration scheme performing
	 *								numerical integration
	 * @param[out] pVelocity			the output velocity data field
	 *								(one entry for each particle pos)
	 * @param[out] pTimeStamps			the output (sim) time stamp field
	 *								(one entry for each particle pos)
	 * @param[in] vecFieldInterpolators the interpolators for all other data 
	 *								attribs
	 * @param[in] vecDataFields			the data arrays for all other data 
	 *								attributes
	 * @param[out] pCurrentLine			the cell array holding the point ids of the 
	 *								line generated by this method
	 * @param[out] pOutPts				the collection of output points computed by 
	 *								this method
	 * @param[out] pLines				cell array containing the cells (i.e. lines)
	 *								generated by this method (1 entry will be 
	 *								added for each successfuly call)
	 * @param[out] pTermination			array holding the reason for termination for
	 *								each line (e.g. if the trace left its time
	 *								frame or if it left the domain or ...)
	 *
	 * NOTE: This method is NOT THREAD SAFE because it changes the data arrays as
	 *       well as the vtkPoints object passed to it!
	 */
	void TracePathline(	const double dStartPt[4],
						double dIntegrationStep,
						VveInstatInterpolator *pInterpolator,
						vtkInitialValueProblemSolver *pIntegrator,
						vtkDataArray *pVelocity,
						vtkDataArray *pTimeStamps,
						std::vector<VveInstatInterpolator*>& vecFieldInterpolators,
						std::vector<vtkDataArray*>& vecDataFields,
						vtkIdList *pCurrentLine,
						vtkPoints *pOutPts,
						vtkCellArray *pLines,
						vtkDataArray *pTermination);


						
private:
	/**
	*
	*/
	VveDiscreteDataTyped<vtkDataSet>* m_pData;
	/**
	*
	*/
	VveTimeMapper *m_pTimeMapper;
	/**
	*
	*/
	vtkPolyData *m_pOutput;
	/**
	*
	*/
	double *m_pSeedPoints;
	int m_iSeedSize;
	/**
	 *
	 */
	std::list<std::string> m_liInterpolFields;
	/**
	*
	*/
	double m_dIntegrationStep;
	/**
	 *
	 */
	int m_iTraceMode;

	/**
	*
	*/
	double m_dTerminalSpeed;
	/**
	*
	*/
	double m_dTimeFrame[2];
	/**
	 *
	 */
	bool m_bRecordCells;
	/**
	 *
	 */
	std::list<SCellID> m_liVisitedCells;
};

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/


#endif //_VVEPATHLINETRACER_H

