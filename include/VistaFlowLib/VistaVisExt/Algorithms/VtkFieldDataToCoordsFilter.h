#ifndef _VTKFIELDDATATOCOORDSFILTER_H
#define _VTKFIELDDATATOCOORDSFILTER_H

#include <vtkPolyDataAlgorithm.h>

#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>

/**
 * Put up to three different scalar data fields to the output's coordinate
 * values in order to create a simple scatterplot
 */
class VISTAVISEXTAPI vtkFieldDataToCoordsFilter : public vtkPolyDataAlgorithm
{
	vtkTypeRevisionMacro(vtkFieldDataToCoordsFilter,vtkPolyDataAlgorithm);

	static vtkFieldDataToCoordsFilter *New();

	void PrintSelf(ostream& os, vtkIndent indent);

	void SetXField(const std::string& strFName, int iComp=0);
	std::string GetXFieldName() const;
	int GetXFieldDim() const;

	void SetYField(const std::string& strFName, int iComp=0);
	std::string GetYFieldName() const;
	int GetYFieldDim() const;

	void SetZField(const std::string& strFName, int iComp=0);
	std::string GetZFieldName() const;
	int GetZFieldDim() const;

	void SetDefaultVals(double d[3]);
	void GetDefaultVals(double d[3]);

	void SetXDefaultVal(double d);
	double GetXDefaultVal() const;

	void SetYDefaultVal(double d);
	double GetXYDefaultVal() const;

	void SetZDefaultVal(double d);
	double GetZDefaultVal() const;

	void SetGenerateVertices(bool b);
	bool GetGenerateVertices() const;

	void SetTargetBounds(double d[6]);
	void GetTargetBounds(double d[6]) const;

protected:
	vtkFieldDataToCoordsFilter();
	virtual ~vtkFieldDataToCoordsFilter();
	/**
	*
	*/
	virtual int RequestData(vtkInformation* request,
							vtkInformationVector** inputVector,
							vtkInformationVector* outputVector);
	/**
	*
	*/
	virtual int FillInputPortInformation(int port, vtkInformation *info);
private:
	// safe data field names for three coordinate axes
	std::string m_strXFieldName;
	std::string m_strYFieldName;
	std::string m_strZFieldName;

	// remember the component (dimension) to be extracted from the data field
	int m_iFieldComponent[3];

	// safe default values for non-mapped axes
	double m_dDefaultVals[3];

	bool m_bGenerateVertices;

	double m_dTargetBounds[6];
};

#endif
