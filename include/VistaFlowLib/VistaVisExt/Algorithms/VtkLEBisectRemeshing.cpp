#include "VtkLEBisectRemeshing.h"
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkPolyDataWriter.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkMath.h>
#include <vtkBitArray.h>
#include <vtkCellArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>


#include <queue>
#include <stack>
#include <string>
#include <cassert>

#include <VistaVisExt/Tools/VtkPolyDataTools.h>
#include <VistaVisExt/IO/VveDataSetName.h>

#include <VistaBase/VistaStreamUtils.h>


vtkCxxRevisionMacro(vtkLEBisectRemeshing, "$Revision: 1.4 $");
vtkStandardNewMacro(vtkLEBisectRemeshing);

vtkLEBisectRemeshing::vtkLEBisectRemeshing()
{
    this->MaximumEdgeLength = 1.0f;
    this->MaximumNumberOfSplits = 10000;
    m_pNeighCells = vtkIdList::New();
	AllowBoundaryBisect = false;

    //pWriter = vtkPolyDataWriter::New();
    
}

vtkLEBisectRemeshing::~vtkLEBisectRemeshing()
{
    m_pNeighCells->Delete();
    //pWriter->Delete();
}


int vtkLEBisectRemeshing::RequestData(vtkInformation* request,
											vtkInformationVector** inputVector,
											vtkInformationVector* outputVector)
{

	// retrieve data objects from information vectors
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkPolyData *input = vtkPolyData::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkPolyData *output = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

    if(input->GetNumberOfCells() != input->GetPolys()->GetNumberOfCells())
    {
       vstr::errp()<<"[VtkLEBisectRemeshing::Execute] Unable to operate on mixed polydata objects!"<<endl;
        this->ReasonForTermination = TERMINATE_ERROR_NON_TRIANGULAR;
        return -1;
    }

    //first copy input to output. all changes will be made on output only
    output->ShallowCopy(input);
    output->BuildLinks();
    
    VtkPolyDataTools oTools(output);

    float fMaxEdgeLength = this->GetMaximumEdgeLength() + 1.0f;
    float fCurrentLen;
    int iCurrentCellCount, iNumSplitsPerformed = 0;
    vtkIdType iCurrentTriangle, iLongestV1, iLongestV2, iFlipC, iFlipD;
    vtkIdType iNeighTriangle, iNumCells, iNumPts, *pPts, iA, iB;
    std::stack<SEdgeInfo> stFlipEdges;
    SEdgeInfo oFlipEdge;
    
    
    /*
    VveDataSetName oOutName;
    oOutName.Set(std::string("anim/tmpMesh_****.vtk"));
    int iMeshCount = 0;
    vtkPolyData *pTmpMesh = vtkPolyData::New();
    */
    output->GetCellPoints(0,iNumPts, pPts);

    int iMaxSplits = this->GetMaximumNumberOfSplits();
    while((fMaxEdgeLength > this->GetMaximumEdgeLength()) && (iNumSplitsPerformed < iMaxSplits))
    {
		
        fMaxEdgeLength = 0.0f;
        /*
        * find all triangles in output whose longest edge is longer than the given
        * threshold
        */ 
        iCurrentTriangle = -1; //init so that fisr increment will take it to 0
        iCurrentCellCount = output->GetNumberOfCells()-1;
        while(iCurrentTriangle < iCurrentCellCount && iNumSplitsPerformed < iMaxSplits)
        {
			
            //increase counter in any case!!!
            ++iCurrentTriangle;
            
            output->GetCellPoints(iCurrentTriangle, iNumPts, pPts);
            if(iNumPts != 3 || !output->IsTriangle(pPts[0], pPts[1], pPts[2]))
            {
                vstr::errp()<<"[vtkLEBisectRemeshing::Execute] Unable to operate on non-triangle mesh! ABORTING EXECUTE!!!"<<endl;
                this->ReasonForTermination = TERMINATE_ERROR_NON_TRIANGULAR;
                return -1;
            }
            //check edge length against maximum
            fCurrentLen = oTools.ComputeLongestEdge(iCurrentTriangle, iLongestV1, iLongestV2);
            if(fCurrentLen <= this->GetMaximumEdgeLength())
                continue;
            iNumCells = oTools.GetEdgeIncidentCells(iLongestV1, iLongestV2, m_pNeighCells);
            if(iNumCells>2)
            {
                vstr::errp()<<"[vtkLEBisectRemeshing::Execute] Unable to operate on non-manifold mesh! ABORTING EXECUTE!!!"<<endl
					<<"\tNon-manifold found in triangle "<<iCurrentTriangle<<" = ["<<iLongestV1<<", "<<iLongestV2<<", "<<oTools.GetApexVertex(iCurrentTriangle, iLongestV1, iLongestV2)<<"]"<<endl
					<<"\tQueried edge ("<<iLongestV1<<", "<<iLongestV2<<")"<<endl;
                for(int k=0; k<iNumCells; ++k)
                {
                    vtkIdType n, *p;
                    output->GetCellPoints(m_pNeighCells->GetId(k),n,p);
                    vstr::erri()<<"\tt"<<m_pNeighCells->GetId(k)<<" = ["<<p[0]<<", "<<p[1]<<", "<<p[2]<<"]"<<endl;
                }
                this->ReasonForTermination = TERMINATE_ERROR_NON_MANIFOLD;
                return -1;
            }
            //we are only interested in inner edges here!
            if(iNumCells < 2) 
                continue;
            //printf("Inserting new triangle for split : [%d | (%d,%d) ]\n", iCurrentTriangle, iLongestV1, iLongestV2);
            //update current max len. NOTE: Do this only if we split the edge hereafter!
            //If we did this before this might result in a non-terminating loop since boundary edges are never split
            //if(fCurrentLen > fMaxEdgeLength)
			fMaxEdgeLength = std::max<float>(fCurrentLen, fMaxEdgeLength);
            
            /*
            ** Split too long edges
            */
            //determine split neighborhood
			iNeighTriangle = (m_pNeighCells->GetId(0) == iCurrentTriangle ? 
										m_pNeighCells->GetId(1) : 
										m_pNeighCells->GetId(0));
			//oTools.GetCellEdgeNeighbor(iCurrentTriangle, iLongestV1, iLongestV2);
            vtkIdType iSplitRing[4];
            iSplitRing[0] = iLongestV1;
            iSplitRing[2] = iLongestV2;
            iSplitRing[1] = oTools.GetApexVertex(iNeighTriangle,iSplitRing[2],iSplitRing[0]);
            iSplitRing[3] = oTools.GetApexVertex(iCurrentTriangle,iSplitRing[0],iSplitRing[2]);

            //split current edge
            vtkIdType iSplitVertex = oTools.SplitTriangle(iCurrentTriangle, iSplitRing[0], iSplitRing[2]);
            ++iNumSplitsPerformed;
            
            /*
            if(iMeshCount++ % 10 == 0)
            {
                pTmpMesh->ShallowCopy(output);
                pWriter->SetInput(pTmpMesh);
                pWriter->SetFileName(oOutName.GetFileName(iMeshCount).c_str());
                pWriter->Write();
            }
            */
            //printf("Inserted SplitVertex %d\n", iSplitVertex);
            
            //mark all the triangles after split as "MODIFIED"
            unsigned short iNumCells;
            vtkIdType *pNewTriangles;
            output->GetPointCells(iSplitVertex, iNumCells, pNewTriangles); 
            //error checking : if split vertex does not have 4 incident triangles right after splitting, something went terribly wrong
            if(iNumCells != 4) 
            {
                vstr::errp()<<"[vtkLEBisectRemeshing::Execute] Split has failed! I have != 4 incident cells after split here!"<<endl;;
                vstr::erri()<<"\tProbably you gave me a non-manifold input!"<<endl<<endl;
                this->ReasonForTermination = TERMINATE_ERROR_SPLIT_FAILED;
                return -1;
            }
            /*  
            ** Recursively check to flip edges. Start with the split vertex's 1-ring neighborhood.
            ** Since the vertex has just been inserted, the neighs are given by the split ring vertices 
            */
            for(int i=0; i<4; ++i) //for all edges around the 1-ring
            {
                iA = iSplitRing[i];
                iB = iSplitRing[(i+1)%4];
                if(oTools.GetEdgeIncidentCells(iA,iB,m_pNeighCells) == 2) //we may flip it only if it is an inner edge
                {
                    //compare first incident cell id to all newly created triangles
                    //if (we get a hit) -> we have the triangle with which to do the flip
                    //else -> the other one must be it
                    if( m_pNeighCells->GetId(0) == pNewTriangles[0] ||
                        m_pNeighCells->GetId(0) == pNewTriangles[1] ||
                        m_pNeighCells->GetId(0) == pNewTriangles[2] ||
                        m_pNeighCells->GetId(0) == pNewTriangles[3])
                    {
                        stFlipEdges.push(SEdgeInfo(m_pNeighCells->GetId(0),iA,iB));
                        //printf("Pushing new edge to flip stack : [%d | (%d,%d)]\n", m_pNeighCells->GetId(0), iA, iB);
                    }
                    else
                    {
                        stFlipEdges.push(SEdgeInfo(m_pNeighCells->GetId(1),iA,iB));
                        //printf("Pushing new edge to flip stack : [%d | (%d,%d)]\n", m_pNeighCells->GetId(1), iA, iB);
                    }
                }
            }
            
            while(!stFlipEdges.empty())
            {
                oFlipEdge = stFlipEdges.top();
                stFlipEdges.pop();
                if( oTools.ComputeFlipIds(oFlipEdge.iTriangleID,oFlipEdge.iV1, oFlipEdge.iV2, iNeighTriangle, iFlipC, iFlipD) &&
                    oTools.ShouldEdgeBeFlipped(oFlipEdge.iV1, oFlipEdge.iV2, iFlipC, iFlipD) )
                {
                    //flip edge
                    oTools.FlipEdge(oFlipEdge.iTriangleID, iNeighTriangle, oFlipEdge.iV1, oFlipEdge.iV2, iFlipC, iFlipD);
                    //printf("Flipped edge (%d,%d) of triangle %d \n", oFlipEdge.iV1, oFlipEdge.iV2, oFlipEdge.iTriangleID);
                    //two new edges have just become conspiciuous
                    stFlipEdges.push(SEdgeInfo(oFlipEdge.iTriangleID, oFlipEdge.iV1, iFlipD));
                    //printf("Pushing new edge to flip stack * : [%d | (%d,%d)]\n", oFlipEdge.iTriangleID, oFlipEdge.iV1, iFlipD);
                    stFlipEdges.push(SEdgeInfo(iNeighTriangle, iFlipD, oFlipEdge.iV2));
                    //printf("Pushing new edge to flip stack * : [%d | (%d,%d)]\n", iNeighTriangle, iFlipD, oFlipEdge.iV2);
                    
                    /*
                    if(iMeshCount++ % 10 == 0)
                    {
                        pTmpMesh->ShallowCopy(output);
                        pWriter->SetInput(pTmpMesh);
                        pWriter->SetFileName(oOutName.GetFileName(iMeshCount).c_str());
                        pWriter->Write();
                    }
                    */
                    
                }
            }
        }
    }
    // determine reason for termination (other than ERROR)
    if(this->GetMaximumEdgeLength() > fMaxEdgeLength)
        this->ReasonForTermination = TERMINATE_LENGTH_MET;
    else
        this->ReasonForTermination = TERMINATE_SPLITS_MET;

    output->DeleteLinks();
    output->Squeeze();

    //pTmpMesh->Delete();
	return 1;
}

void vtkLEBisectRemeshing::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

