/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/

#include "VveVtkVortexUtils.h"

#include <VistaVisExt/Algorithms/VtkInterpolatedScalarField.h>
#include <VistaVisExt/Algorithms/VtkComputeLambda2.h>

#include <VistaBase/VistaStreamUtils.h>

#include <VistaAspects/VistaAspectsUtils.h>

#include <vtkInterpolatedVelocityField.h>
#include <vtkDataSetReader.h>
#include <vtkCellDerivatives.h>
#include <vtkCellDataToPointData.h>
#include <vtkMergeFilter.h>
#include <vtkDataSetWriter.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkPolyData.h>
#include <vtkCleanPolyData.h>
#include <vtkGenericCell.h>

#include <sstream>
#include <limits>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

// needed to get numeric_limits to work in MS Windows
#undef max
#undef min

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

std::string		VveVtkVortexUtils::sFilePath = "";

vtkGenericCell	*VveVtkVortexUtils::m_pInterpolationGenericCell 
							= vtkGenericCell::New();
vtkCell			*VveVtkVortexUtils::m_pInterpolationCell = 0;
vtkIdType		VveVtkVortexUtils::m_iInterpolationCellId = 0;
vtkDataSet		*VveVtkVortexUtils::m_pInterpolationLastDataSet = 0;
vtkDataArray	*VveVtkVortexUtils::m_pInterpolationLastDataArray = 0;

void VveVtkVortexUtils::Save(
	vtkDataSet * pDataSet,
	const std::string & sPrefix,
	const int & iNum,
	bool bBinaryFormat)
{
	vtkDataSetWriter * pDSWriter(vtkDataSetWriter::New());
	std::stringstream sstr;
	sstr << sPrefix << iNum << ".vtk";
	const std::string sFileName = sstr.str();

	vstr::outi() << "Saving " << sFileName << " ..." << std::endl;

#if VTK_MAJOR_VERSION > 5
	pDSWriter->SetInputData(pDataSet);
#else
	pDSWriter->SetInput(pDataSet);
#endif

	pDSWriter->SetFileName(sFileName.c_str());

	if(bBinaryFormat)
	{
		pDSWriter->SetFileTypeToBinary();
	}
	else
	{
		pDSWriter->SetFileTypeToASCII();
	}

	pDSWriter->Update();
	pDSWriter->Delete();
}

void VveVtkVortexUtils::Save( vtkDataSet * pDataSet,
	const std::string & sFileName )
{
	vtkDataSetWriter * pDSWriter(vtkDataSetWriter::New());

	vstr::outi() << "Saving " << sFileName << "...";

#if VTK_MAJOR_VERSION > 5
	pDSWriter->SetInputData(pDataSet);
#else
	pDSWriter->SetInput(pDataSet);
#endif

	pDSWriter->SetFileName(sFileName.c_str());

	pDSWriter->SetFileTypeToBinary();
	pDSWriter->SetFileTypeToASCII();

	pDSWriter->Update();
	pDSWriter->Delete();

	vstr::out() << " done!" << std::endl;
}

void VveVtkVortexUtils::AppendStreamLines(
	vtkPolyData * pLineOut,
	vtkPolyData * pForwardLine,
	vtkPolyData * pBackwardLine )
{
	const int iNumOfBackwardPoints (pBackwardLine->GetNumberOfPoints());
	const int iNumOfForwardPoints (pForwardLine->GetNumberOfPoints());

	vtkPoints * pPoints(vtkPoints::New());
	vtkCellArray * pCell(vtkCellArray::New());

	pCell->InsertNextCell(iNumOfBackwardPoints + iNumOfForwardPoints - 1);

	// for all backward points
	for(int i=0; i<iNumOfBackwardPoints; ++i)
	{
		pPoints->InsertNextPoint
			(pBackwardLine->GetPoint(iNumOfBackwardPoints-1-i));
		pCell->InsertCellPoint(i);
	}

	// for all forward points
	for(int i=1; i<iNumOfForwardPoints; ++i)
	{
		pPoints->InsertNextPoint(pForwardLine->GetPoint(i));
		pCell->InsertCellPoint(iNumOfBackwardPoints+i-1);
	}

	pLineOut->SetPoints(pPoints);
	pLineOut->SetLines(pCell);

	pPoints->Delete();
	pCell->Delete();

	return;
}

void VveVtkVortexUtils::DeleteNthPoint( vtkPointSet * PS, int Index )
{
	vtkPoints * pPoints(PS->GetPoints());
	const int iNumOfPoints(pPoints->GetNumberOfPoints());

	// check Index
	if(Index < 0 || Index > iNumOfPoints-1)
	{
		vstr::errp() << "[VveVtkVortexUtils::DeleteNthPoint]: Index is invalid! " << Index << std::endl;
		return;
	}

	vtkPoints * pNewPoints(vtkPoints::New());
	pNewPoints->SetNumberOfPoints(iNumOfPoints-1);

	int j(0);
	for(int i=0; i<iNumOfPoints; i++)
	{
		if(i != Index)
		{
			pNewPoints->InsertPoint(j, pPoints->GetPoint(i));
			++j;
		}

	}		

	PS->SetPoints(pNewPoints);
	pNewPoints->Delete();
}

bool VveVtkVortexUtils::Interpolate(
	double pos[3],
	vtkDataSet *pData,
	vtkDataArray *pDataArray,
	double *result )
{
	double d24Weights[24];

	int iSubId = 0;
	double d3PCoords[3] = { .0, .0, .0 };

	// only reuse cells for starting findcell, if search is performed on the
	// same data set as last time
	if (pData != m_pInterpolationLastDataSet
		|| pDataArray != m_pInterpolationLastDataArray)
	{
		m_iInterpolationCellId = 0;
		m_pInterpolationCell = 0;
	}

	vtkIdType iCellId = 
		pData->FindCell(
		pos, 
		m_pInterpolationCell, 
		m_pInterpolationGenericCell, 
		m_iInterpolationCellId,
		std::numeric_limits<double>::min(), 
		iSubId, 
		d3PCoords, 
		d24Weights);

	vtkCell *pCurrentCell = 0;

	if (iCellId > 0)
	{
		pCurrentCell = pData->GetCell(iCellId);
	}
	else
	{
		return false;
	}

	if (pCurrentCell)
	{
		// safe current cell as starting point for next interpolations
		m_iInterpolationCellId = iCellId;
		m_pInterpolationCell = pCurrentCell;

		m_pInterpolationLastDataSet = pData;
		m_pInterpolationLastDataArray = pDataArray;

		int iNumPoints = pCurrentCell->GetNumberOfPoints();
		int iNumComponents = pDataArray->GetNumberOfComponents();

		for (int i = 0; i < iNumComponents; ++i)
		{
			result[i] = 0;
		}

		vtkPoints *pPoints = pCurrentCell->GetPoints();

		double d9PointData[9];

		for (int i=0; i < iNumPoints; ++i)
		{
			pDataArray->GetTuple(pCurrentCell->GetPointId(i), d9PointData);

			for (int j=0; j < iNumComponents; j++)
			{
				result[j] += d9PointData[j] * d24Weights[i];
			}
		}

		return true;
	}
	else
	{
		return false;
	}
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveVtkVortexUtils.cpp"                                       */
/*============================================================================*/
