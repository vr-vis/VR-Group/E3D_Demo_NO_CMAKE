/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#ifndef __VVEVTKEXTRACTLOCALEXTREMA_H__
#define __VVEVTKEXTRACTLOCALEXTREMA_H__

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaVisExt/VistaVisExtConfig.h>

#include <vtkDataSetToPolyDataFilter.h> 

#include "VveVtkVortexUtils.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkDataArray;
class vtkIdList;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* VTK Filter that extracts local extrema in a vtk dataset. The outoput is a
* vtkDataSet containing the points where local extrema are located.
*/
class VISTAVISEXTAPI VveVtkExtractLocalExtrema : public vtkDataSetToPolyDataFilter
{
public:

	static VveVtkExtractLocalExtrema *New();
	vtkTypeRevisionMacro(VveVtkExtractLocalExtrema,vtkDataSetToPolyDataFilter);

	bool SetExtractMinima();
	bool SetExtractMaxima();
	bool SetExtractBoth();

	bool GetExtractMinima() const;
	bool GetExtractMaxima() const;
	bool GetExtractBoth() const;

	void PrintSelf(ostream& os, vtkIndent indent);

	void SetFieldName(const std::string &sFieldName);

	const std::string GetFieldName() const;

protected:
	VveVtkExtractLocalExtrema();
	~VveVtkExtractLocalExtrema() {};

	virtual void  ExecuteData (vtkDataObject *vtkNotUsed(output));

	bool IsMin(vtkIdType PointID, vtkDataArray * PointScalars);
	bool IsMax(vtkIdType PointID, vtkDataArray * PointScalars);
	bool IsCriterion(vtkIdType PointID, vtkDataArray * PointScalars,
					 VveVtkVortexUtils::binary_function_base<double> * 
					 BinaryFunction);

	void GetNeighbors(vtkIdType PointID, vtkIdList * NeighborIDs);

private:
	VveVtkExtractLocalExtrema(const VveVtkExtractLocalExtrema & );
	// Not implemented.
	void operator=(const VveVtkExtractLocalExtrema & );
	// Not implemented.

	bool m_bMin;
	bool m_bMax;

	vtkDataSet * m_pInput;

	std::string m_sFieldName;
	
};
/*============================================================================*/
/*============================================================================*/

#endif /* ifndef __VVEVTKEXTRACTLOCALEXTREMA_H__ */

/*============================================================================*/
/*  END OF FILE "VveVtkExtractLocalExtrema.h"                                 */
/*============================================================================*/
