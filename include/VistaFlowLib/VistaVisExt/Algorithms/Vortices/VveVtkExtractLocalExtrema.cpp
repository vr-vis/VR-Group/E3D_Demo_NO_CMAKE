/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/

#include "VveVtkExtractLocalExtrema.h"

#include <vtkBitArray.h>
#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkDataSet.h>
#include <vtkPolyData.h>
#include <vtkIdTypeArray.h>
#include <vtkIdList.h>

#include <VistaBase/VistaStreamUtils.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

vtkCxxRevisionMacro(VveVtkExtractLocalExtrema, "$Revision: 1.23 $");
vtkStandardNewMacro(VveVtkExtractLocalExtrema);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

//------------------------------------------------------------------------------
// Input: dataset
// Output: filtered point set with attributes "IsMinMax" and "OldPointIndices"
//------------------------------------------------------------------------------
VveVtkExtractLocalExtrema::VveVtkExtractLocalExtrema() :
m_bMin (false), m_bMax (false), m_pInput(0)
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

bool VveVtkExtractLocalExtrema::SetExtractMinima()
{
	m_bMin = true;
	m_bMax = false;
	return true;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
bool VveVtkExtractLocalExtrema::SetExtractMaxima()
{
	m_bMax = true;
	m_bMin = false;
	return true;
}
//------------------------------------------------------------------------------
// extract local minima and maxima
// vtkBitArray "IsMinMax" holds IsMin and IsMax information
//------------------------------------------------------------------------------
bool VveVtkExtractLocalExtrema::SetExtractBoth()
{
	m_bMax = true;
	m_bMin = true;
	return true;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
bool VveVtkExtractLocalExtrema::GetExtractMinima() const
{
	return m_bMin;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
bool VveVtkExtractLocalExtrema::GetExtractMaxima() const
{
	return m_bMax;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
bool VveVtkExtractLocalExtrema::GetExtractBoth() const
{
	return m_bMin && m_bMax;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void VveVtkExtractLocalExtrema::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os,indent);

	os << "VtkExtractLocalExtrema: ...";
}
//------------------------------------------------------------------------------
// Main filter function
//------------------------------------------------------------------------------
void VveVtkExtractLocalExtrema::ExecuteData(vtkDataObject *vtkNotUsed(output))
{
	m_pInput = this->GetInput();

	if (! m_pInput)
    {
		vstr::errp() << "[VveVtkExtractLocalExtrema::ExecuteData]: No input!" << std::endl;
		return;
    }

	if(!m_bMin && !m_bMax)
	{
		vstr::errp() << "[VtkExtractLocalExtrema::ExecuteData]: Neither ExtractMinima nor ExtractMaxima is set!" << std::endl;
		return;
	}

	vtkPolyData * pOutput = this->GetOutput();

	vtkPointData * pPointData = m_pInput->GetPointData();
	vtkPointData * pOutPointData = pOutput->GetPointData();

	int iIndex = -1;
	vtkDataArray * pPointScalars = 
		pPointData->GetArray(m_sFieldName.c_str(), iIndex);
	
	pOutPointData->CopyAllocate(pPointData);

	if ( ! pPointScalars  )
	{
		vstr::errp() << "[VtkExtractLocalExtrema::ExecuteData]: no scalar data!" << std::endl;		
		return;
    }

	const int NumOfPts(m_pInput->GetNumberOfPoints());
	vtkPoints * NewPoints(vtkPoints::New());
	NewPoints->Allocate(NumOfPts);

	double dPoint[3];

	vtkIdType NewID(-1);

	// store old point indices in vtkIdTypeArray
	vtkIdTypeArray * pOldPointIndices(vtkIdTypeArray::New());
	pOldPointIndices->SetNumberOfComponents(1);
	pOldPointIndices->SetNumberOfTuples(NumOfPts);
	pOldPointIndices->SetName("OldPointIndices");
	double dOldPointIndex(-1);

	// store in vtkBitArray: 1 if point is local min; 2 if point is local max
	vtkBitArray * pbIsMinMax(vtkBitArray::New());
	pbIsMinMax->SetNumberOfComponents(1);
	pbIsMinMax->SetNumberOfTuples(NumOfPts);
	pbIsMinMax->SetName("IsMinMax");
	double dMinMax(0);

	// for all Points
	bool bIsCriterion(false);

	for (int i=0; i < NumOfPts; ++i)
	{
		bIsCriterion = false;
		// if point matches criterion
		if( m_bMin && IsMin(i, pPointScalars) )
		{
			dMinMax = 0;
			bIsCriterion = true;
		}

		else if( m_bMax && IsMax(i, pPointScalars) )
		{
			dMinMax = 1;
			bIsCriterion = true;
		}

		// criterion is true
		if(bIsCriterion)
		{
			m_pInput->GetPoint(i, dPoint);
			NewID = NewPoints->InsertNextPoint(dPoint);
			pOutPointData->CopyData(pPointData,i,NewID);
			dOldPointIndex = static_cast<double>(i);
			pOldPointIndices->SetTuple1(NewID, dOldPointIndex);			
			pbIsMinMax->SetTuple1(NewID, dMinMax);
		}


	}

	// set output
	pOutput->SetPoints(NewPoints);
	pOutput->GetPointData()->AddArray(pOldPointIndices);
	
	// only set array if needed
	if(m_bMin && m_bMax)
	{
		pOutput->GetPointData()->AddArray(pbIsMinMax);
	}
	
	// Delete
	NewPoints->Delete();
	pbIsMinMax->Delete();
	pOldPointIndices->Delete();

	pOutput->Squeeze();
}
//------------------------------------------------------------------------------
// criteron for local minima
//------------------------------------------------------------------------------
bool VveVtkExtractLocalExtrema::IsMin
(vtkIdType PointID, vtkDataArray * PointScalars)
{
	VveVtkVortexUtils::less_or_equal<double> compare;
	return IsCriterion(PointID, PointScalars, & compare);
}
//------------------------------------------------------------------------------
// criterion for local maxima
//------------------------------------------------------------------------------
bool VveVtkExtractLocalExtrema::IsMax
(vtkIdType PointID, vtkDataArray * PointScalars)
{
	VveVtkVortexUtils::greater<double> compare;
	return IsCriterion(PointID, PointScalars, & compare);	
}
//------------------------------------------------------------------------------
// generic criterion with BinaryFunction
//------------------------------------------------------------------------------
bool VveVtkExtractLocalExtrema::IsCriterion
(vtkIdType PointID, vtkDataArray * PointScalars, 
 VveVtkVortexUtils::binary_function_base<double> * BinaryFunction)
{
	vtkIdList * pNeighborIDs(vtkIdList::New());
	GetNeighbors(PointID, pNeighborIDs);

	double CurrPointScalarValue(PointScalars->GetComponent(PointID, 0));
	int iNumOfEquals(0);
	
	const vtkIdType NumOfIDs(pNeighborIDs->GetNumberOfIds());

	vtkIdType NeighborID(-1);
	double NeighborScalarValue(0);

	// for all neighbors
	for(int i=0; i<NumOfIDs; ++i)
	{
		NeighborID = pNeighborIDs->GetId(i);
		NeighborScalarValue = PointScalars->GetComponent(NeighborID, 0);

		if((*BinaryFunction)(NeighborScalarValue, CurrPointScalarValue))
		{
			pNeighborIDs->Delete();
			return false;
		}

		if(CurrPointScalarValue == NeighborScalarValue)
		{
			++iNumOfEquals;
		}
	}

	pNeighborIDs->Delete();

	if(iNumOfEquals == NumOfIDs)
	{
		return false;
	}

	return true;	
}
//------------------------------------------------------------------------------
// for given PointID get IDs of neighbors
//------------------------------------------------------------------------------
void VveVtkExtractLocalExtrema::GetNeighbors
(vtkIdType PointID, vtkIdList * NeighborIDs)
{
	// idea: for PointID get all cells containing corresponding point
	// for all cells for all points: if not PointID then insert unique point
	if(! m_pInput)
	{
		vstr::errp() << "[VtkExtractLocalExtrema::GetNeighbors]: Input is not set!" << std::endl;
		return;
	}

	vtkIdList * pCellIDs(vtkIdList::New());
	m_pInput->GetPointCells(PointID, pCellIDs);

	vtkIdType CurrCellID(-1);
	vtkIdList * pPointIDs(vtkIdList::New());

	vtkIdType NumOfPoints(0);
	vtkIdType NeighborPointID(-1);

	// for all point cells
	const vtkIdType NumOfCells(pCellIDs->GetNumberOfIds());
	for(int i=0; i<NumOfCells; ++i)
	{		
		CurrCellID = pCellIDs->GetId(i);
		m_pInput->GetCellPoints(CurrCellID, pPointIDs);
		
		// for all cell points
		NumOfPoints = pPointIDs->GetNumberOfIds();
		for(int j=0; j<NumOfPoints; j++)
		{
			NeighborPointID = pPointIDs->GetId(j);
			// do not get point, only neighbors
			if(NeighborPointID != PointID)
			{
				NeighborIDs->InsertUniqueId( NeighborPointID );
			}
		}
	}

	// Delete
	pPointIDs->Delete();
	pCellIDs->Delete();
}

void VveVtkExtractLocalExtrema::SetFieldName( const std::string &sFieldName )
{
	m_sFieldName = sFieldName;
}

const std::string VveVtkExtractLocalExtrema::GetFieldName() const
{
	return m_sFieldName;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveVtkExtractLocalExtrema.cpp"                               */
/*============================================================================*/

