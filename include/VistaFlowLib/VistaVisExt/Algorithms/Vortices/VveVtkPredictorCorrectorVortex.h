/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#ifndef __VVEVTKPREDICTORCORRECTORVORTEX_H__
#define __VVEVTKPREDICTORCORRECTORVORTEX_H__

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaVisExt/VistaVisExtConfig.h>

#include <vtkDataSetToPolyDataFilter.h>

#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkStreamTracer;
class VveVtkSFCorrector;
class vtkThreshold;
class vtkPoints;
class VveVtkVortexHullCalculator;
class VveVtkVortexData;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* This is a vtk Filter that extracts vortices from a VTK Data Set.
* Vortices are extracted with the aid of the predictor corrector approach
* that was proposed by Banks and Singer and later on modified and improved by
* Stegmair et. al.
* This filter accepts a vtkDataSet as input that must contain the following
* data arrays: a lambda2 array, a gradient of lambda2 array and a vorticity
* array (their names must be passed to the filter before its execution).
* It has five outputs: vortex hulls, vortex core lines, used seed points,
* vortex skeletons and small vortex skeletons
*/

class VISTAVISEXTAPI VveVtkPredictorCorrectorVortex : public vtkDataSetToPolyDataFilter
{
public:

	static VveVtkPredictorCorrectorVortex * New();

	vtkTypeRevisionMacro(VveVtkPredictorCorrectorVortex, 
		vtkDataSetToPolyDataFilter);

	/**
	 * Number of radial segments of the vortices.
	 */
	void SetNumOfSamplesPerSegment(const unsigned int iNumOfSamplesPerSegment);
	unsigned int GetNumberOfSamplesPerRingSegment() const;

	/**
	 * Lambda2 threshold for the meshing of the vortices. Radii are sampled
	 * until this value is reached.
	 */
	void SetLambda2Threshold(float fInitialLambda2Threshold);
	float GetLambda2Threshold();

	/** 
	 * Minimum core line length. This "length" is the number of points on the
	 * line. Core lines with less points are omitted
	 */
	void SetMinCoreLineLength(int MinLength);
	int GetMinCoreLineLength();

	/**
	 * Set the name of the scalar field that contains the lambda2 values
	 */
	void SetLambda2FieldName(const std::string &sFieldName);
	const std::string GetLambda2FieldName() const;

	/**
	 * Set the name of the vector field that contains the vorticity vectors
	 */
	void SetVorticityFieldName(const std::string &sFieldName);
	const std::string GetVorticityFieldName() const;
	
	/**
	 * Set the name of the vector field that contains the gradients of the
	 * lambda2 values
	 */
	void SetLambda2GradientFieldName(const std::string &sFieldName);
	const std::string GetLambda2GradientFieldName() const;

	/**
	 * Set the minimum vortex radius. If the lambda2 value exceeds the threshold
	 * before this value is reached while sampling, the vortex will get assigned
	 * the minimum radius in that point.
	 */
	void SetMinVortexRadius(const double dRadius);
	double GetMinVortexRadius() const;

	/**
     * Step size for correcting the trace of vortex corelines to the direction
	 * of lambda2 minima. About half of the average cell diameter is a good 
	 * value.
	 */
	void SetCorelineCorrectionSamplingStepSize(const double dStep);
	double GetCorelineCorrectionSamplingStepSize() const;

	/**
	 * Step size for sampling the radii the hull segments. The avarage cell 
	 * diameter is a good starting value.
	 */
	void SetHullSamplingStepSize(const double dHullSamplingStepSize);
	double GetHullSamplingStepSize() const;

	/**
	 * Minimum volume of vortices. Vortices with a smaller volume are omitted.
	 */
	void SetMinVortexVolume(const double dMinVortexVolume);
	double GetMinVortexVolume() const;

	/**
	 * Sets a vtkDataSet that contains the seed points of the previous animation
	 * frame. Those are used to try to reduce flickering in animations
	 */
	void SetPreviousFrameSeeds(vtkDataSet *pSeeds);
	vtkDataSet *GetPreviousFrameSeeds() const;

	/**
	 * Scales the smaller skeletons by a certain factor (can be used to 
	 * generate particle seed points inside of vortices).
	 */
	void SetSmallSkeletonRadiusMultiplier(float fMultiplier);
	float GetSmallSkeletonRadiusMultiplier();

	/**
	 * Returns the number of extracted vortices
	 */
	size_t GetNumberOfVortices() const;

	/**
	 * Returns a certain vortex
	 */
	VveVtkVortexData *GetVortex(int iId);

	/**
	 * Before tracing a seed, it is first traced for some steps forward and then 
	 * backward. In theory, if the seed is on a real vortex coreline, this 
	 * forward/backward test should return to the seed from where it startet,
	 * if it does not the seed does not lie on a vortex core line and is
	 * ommited. The average cell diameter is a good initial value for this
	 * parameter.
	 */
	void SetMaxForwardBackwardTracingDistance(double val);
	double GetMaxForwardBackwardTracingDistance() const;

	/**
	 * This is a speedup parameter for the hull sampling. Sampling is stopped if 
	 * sampled to a value HullSamplingL2Epsilon or less far away from the 
	 * lambda2 threshold
	 */
	double GetHullSamplingL2Epsilon() const;
	void SetHullSamplingL2Epsilon(double val);

	/**
	 * Enables/disables verbose output
	 */
	bool GetVerbose() const;
	void SetVerbose(bool val);

	/**
	 * Remove self intersections of the vortices. Gives better results but
	 * consumes lots of calculation time especially if really long vortices 
	 * exist
	 */
	bool GetRemoveSelfIntersections() const;
	void SetRemoveSelfIntersections(bool val);

	/**
	 * Enable/disable smoothing the surface of the vortex surfaces
	 */
	bool GetSmoothVortices() const;
	void SetSmoothVortices(bool val);

protected:

	VveVtkPredictorCorrectorVortex();
	virtual ~VveVtkPredictorCorrectorVortex();	

	virtual void Execute();

	/**
	 * Computes the seed points for vortex core growing
	 */
	void ComputeSeeds(vtkDataSet * pData, double dLambda2Threshold);
	
	/**
	 * Traces a single seed point and creates an ordered point set that
	 * defines a core line
	 */
	void TraceSeed(vtkDataSet * pData, double * SeedPoint, 
		vtkPolyData * pCoreLine);

	/**
	 * Tests if all radii of a vortex are equal to the minimum vortex radius
	 */
	bool AreAllRadiiMinimum( VveVtkVortexHullCalculator * pComputeVH);

	/**
	 * Removes all seed points that are enclosed by a certain vortex hull
	 */
	size_t EliminateSeedPointsInVortex(
		vtkPolyData *pSeedPoints, vtkPolyData *pVortexHull);

	/**
	 * As the name says: calculates the volume of a vortex
	 */
	double CalculateVortexVolume(vtkPolyData *pData);

	/**
	 * Performs a forward-backward tracing test and returns the distance of 
	 * that test. That means it traces a streamline for a given number of steps
	 * and at the endpoint, traces a streamline again in the backwards 
	 * direction. The intention of that is to find out if a seed point is 
	 * located inside of a vortex, because then, this distance will be very low
	 */
	double GetForwardBackwardTracingDistance(
		vtkDataSet *pData,
		vtkStreamTracer *pStreamTracer,
		double pStartPoint[3],
		vtkIdType &iNumIntegrations,
		bool bStartWithForwardIntegration = true);

	/**
	 * Performs the forward-backward-test for a single seed point and return if
	 * that seed point passed the test
	 */
	bool ForwardBackwardTestSingleSeedPoint(
		vtkDataSet *pData, double d3Point[3]);

	/**
	 * Resorts the seed points based on those given by a previous frame. Seeds
	 * that are very close to previous ones will be put at the beginning to have
	 * them calculated early. That helps to reduce flickering in animations of
	 * the core lines
	 */
	void ResortSeedsBasedOnPreviousFrame();

private:

	VveVtkPredictorCorrectorVortex
		(const VveVtkPredictorCorrectorVortex &);  // Not implemented.
	void operator=
		(const VveVtkPredictorCorrectorVortex &);  // Not implemented.
	
	void PrintProgress(double dPercentage);

	void PrintVerbose(std::string str);

	vtkStreamTracer * m_pStreamTracer;
	VveVtkSFCorrector * m_pSFCorrector;

	vtkPolyData * m_pSeedPoints;
	vtkPoints * m_pUsedSeedPoints;
	vtkDataSet *m_pPreviousFrameSeeds;

	vtkThreshold * m_pLambda2ThresholdFilter;
	
	unsigned int m_uiNumOfSamplesPerRingSegment;
	double m_dLambda2Threshold;

	int m_iMinCoreLineLength;

	int m_iNumOfSteps;

	std::string m_sLambda2FieldName;
	std::string m_sGradientLambda2FieldName;
	std::string m_sVorticityFieldName;

	double m_dMinVortexRadius;

	double m_dCorelineCorrectionSamplingStepSize;

	double m_dHullSamplingStepSize;

	double m_dMinVortexVolume;

	double m_dHullSamplingL2Epsilon;

	double m_dMaxForwardBackwardTracingDistance;
	float m_fSmallSkeletonRadiusMultiplier;

	bool m_bVerbose;
	std::vector<VveVtkVortexHullCalculator*> m_vVortexHullCalculators;

	bool m_bRemoveSelfIntersections;
	bool m_bSmoothVortices;
};
/*============================================================================*/
/*============================================================================*/
#endif /* ifndef __VVEVTKPREDICTORCORRECTORVORTEX_H__ */

/*============================================================================*/
/*  END OF FILE "VveVtkPredictorCorrectorVortex.h"                            */
/*============================================================================*/
