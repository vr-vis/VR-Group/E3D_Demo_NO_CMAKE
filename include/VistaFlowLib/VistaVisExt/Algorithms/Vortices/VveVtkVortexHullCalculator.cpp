/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/

#include "VveVtkVortexHullCalculator.h"

#include "VveVtkVortexUtils.h"
#include "VveVtkVortexData.h"

#include <VistaBase/VistaTimer.h>
#include <VistaBase/VistaDefaultTimerImp.h>
#include <VistaBase/VistaQuaternion.h>
#include <VistaBase/VistaStreamUtils.h>

#include <vtkPolyDataNormals.h>
#include <vtkTriangleStrip.h>
#include <vtkPolygon.h>
#include <vtkTriangleFilter.h>
#include <vtkMassProperties.h>
#include <vtkAppendPolyData.h>
#include <vtkObjectFactory.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

vtkCxxRevisionMacro(VveVtkVortexHullCalculator, "$Revision: 1.20 $");

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

vtkStandardNewMacro(VveVtkVortexHullCalculator);

//------------------------------------------------------------------------------
// Input 0: core line
// Input 1: dataset
// Output 0: VortexHull
// Output 1: reduced core line
// Output 2: Hull Skeleton (just line segments)
//------------------------------------------------------------------------------
VveVtkVortexHullCalculator::VveVtkVortexHullCalculator() : 
	m_iNumOfSamplesPerSegment(8),
	m_dLambda2Threshold(0.),
	m_dMinRadius(0.),
	m_dHullSamplingStepSize(.01),
	m_pVortexData(0),
	m_fSmallSkeletonMultiplier(.5f),
	m_dHullSamplingEpsilon(0.)
{
	SetNumberOfInputs(2);
	SetNumberOfOutputs(4);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
VveVtkVortexHullCalculator::~VveVtkVortexHullCalculator()
{
	delete m_pVortexData;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

//------------------------------------------------------------------------------
// vtk virtual members
//------------------------------------------------------------------------------
void VveVtkVortexHullCalculator::AddInput(vtkPolyData * ds)
{
	this->vtkProcessObject::AddInput(ds);
}

void VveVtkVortexHullCalculator::AddInput( vtkDataObject * )
{
	vtkErrorMacro( << "AddInput() must be called with a vtkPolyData " 
		<< "not a vtkDataObject.");
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void VveVtkVortexHullCalculator::RemoveInput(vtkPolyData * ds)
{
	this->vtkProcessObject::RemoveInput(ds);
	this->vtkProcessObject::SqueezeInputArray();
}

void VveVtkVortexHullCalculator::RemoveInput( vtkDataObject *input )
{
	this->vtkProcessObject::RemoveInput(input);
	this->vtkProcessObject::SqueezeInputArray();
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
vtkPolyData * VveVtkVortexHullCalculator::GetInput(int idx)
{
	if (idx >= this->NumberOfInputs || idx < 0)
	{
		return NULL;
	}

	return (vtkPolyData *)(this->Inputs[idx]);

}

vtkPolyData * VveVtkVortexHullCalculator::GetInput()
{
	return this->GetInput(0);
}

//------------------------------------------------------------------------------
// NumOfSamplesPerSegment: for vortex hull
//------------------------------------------------------------------------------
int VveVtkVortexHullCalculator::GetNumOfSamplesPerSegment() const
{
	return m_iNumOfSamplesPerSegment;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
bool VveVtkVortexHullCalculator::SetNumOfSamplesPerSegment(const int i)
{
	m_iNumOfSamplesPerSegment = i;
	return true;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void VveVtkVortexHullCalculator::SetData(vtkDataSet * pDataSet)
{
	this->vtkProcessObject::SetNthInput(1, pDataSet);
}

//------------------------------------------------------------------------------
// minimum lambda2 value of each hull point
//------------------------------------------------------------------------------
void VveVtkVortexHullCalculator::SetLambda2Threshold(
	const double & dL2Threshold)
{
	m_dLambda2Threshold = dL2Threshold;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
double VveVtkVortexHullCalculator::GetLambda2Threshold() const
{
	return m_dLambda2Threshold;
}

// returns the distance in the direction of d3Direction
double VveVtkVortexHullCalculator::SampleOverLambda2(
	const double d3StartPoint[3],
	const double d3Direction[3],
	double dInitialStepSize,
	double dL2Threshold,
	double dL2Epsilon = 0.)
{
	vtkDataSet *pDataSet = this->GetInput(1);
	vtkDataArray *pLambda2Array = 
		pDataSet->GetPointData()->GetScalars(m_sLambda2FieldName.c_str());

	vtkDataArray *pVorticityArray = 
		pDataSet->GetPointData()->GetVectors(m_sVorticityFieldName.c_str());

	double dLambda2Value = -std::numeric_limits<double>::max();
	double dOldLambda2Value = -std::numeric_limits<double>::max();

	double d3CurrentSamplingPoint[3], 
		d3LastSamplingPoint[3], 
		d3IntegrationStep[3];
	
	for (int i = 0; i < 3; i++)
	{
		d3LastSamplingPoint[i] = d3StartPoint[i];
		d3IntegrationStep[i] = d3Direction[i];
	}

	if (vtkMath::Norm(d3IntegrationStep) == 0.
		|| !VveVtkVortexUtils::Interpolate(
				d3LastSamplingPoint,
				pDataSet,
				pLambda2Array,
				&dLambda2Value))
	{
		// if start point is outside of the input dataset
		// or sampling direction has no length return 0
		return 0.;
	}

	double d3VorticityAtCoreline[3], d3VorticityAtSamplingPoint[3];

	VveVtkVortexUtils::Interpolate(
		d3LastSamplingPoint,
		pDataSet,
		pVorticityArray,
		d3VorticityAtCoreline);

	vtkMath::Normalize(d3IntegrationStep);
	vtkMath::MultiplyScalar(d3IntegrationStep, dInitialStepSize);	

	int iRefinementCount = 0;
	int iMaxRefinements = 15;

	int iSteps = 0;

	int iCountInterpolationFaults = 0;
	int iMaxInterpolationFaults = 5;

	int iNumberOfLambda2Descents = 0;
	int iMaxNumberOfLambda2Descents = 6;

	bool bLambda2StartedRising = false;

	VistaDefaultTimerImp *pTimerImp = new VistaDefaultTimerImp();
	VistaTimer *pTimer = new VistaTimer(pTimerImp);

	double dRadius = 0.;

	// test if there is a self intersection with the vortex hull, if there is 
	// one stop sampling at the intersection point
	double d3LineEndPoint[3];
	for (int i = 0; i < 3; ++i)
	{
		d3LineEndPoint[i] = d3IntegrationStep[i];
	}
	vtkMath::MultiplyScalar(d3LineEndPoint, 1000000.);
	vtkMath::Add(d3StartPoint, d3LineEndPoint, d3LineEndPoint);
	double dMaxRadius = std::numeric_limits<double>::max();

	while (true)
	{
		++iSteps;

		vtkMath::Add(
			d3LastSamplingPoint, 
			d3IntegrationStep, 
			d3CurrentSamplingPoint);

		dOldLambda2Value = dLambda2Value;

		// set a timeout for interpolation
		double time = pTimer->GetMicroTime();

		// get Lambda2 value and check if current sampling point is still within
		// the volume dataset
		bool bInterpolationValid = VveVtkVortexUtils::Interpolate(
			d3CurrentSamplingPoint,
			pDataSet,
			pLambda2Array,
			&dLambda2Value);

		bool bBreakDueToVorticity = false;
		if (bInterpolationValid)
		{
			VveVtkVortexUtils::Interpolate(
				d3CurrentSamplingPoint,
				pDataSet,
				pVorticityArray,
				d3VorticityAtSamplingPoint);

			// get angle between central vorticity and sampled vorticity
			double dDot = vtkMath::Dot(
				d3VorticityAtCoreline,
				d3VorticityAtSamplingPoint);

			double dAngle = acos(dDot / (
				vtkMath::Norm(d3VorticityAtCoreline)
				* vtkMath::Norm(d3VorticityAtSamplingPoint)))
				* 180. / vtkMath::Pi();

			// if the angle is greater than 90�, stop sampling because we run
			// into another vortex
			if (dAngle > 90.) 
			{
				bBreakDueToVorticity = true;
			}
		}

		// TODO set timeout per parameter
		// if the time for interpolation exceeds a certain value, break
		if (pTimer->GetMicroTime() - time > .5)
		{
			break;
		}

		// limit the number of interpolation faults, because they can consume 
		// lots of calculation time
		if (!bInterpolationValid)
		{
			if (++iCountInterpolationFaults >= iMaxInterpolationFaults)
			{
				break;
			}
		}

		if (!bInterpolationValid
			|| dLambda2Value > dL2Threshold
			|| bBreakDueToVorticity)
			//|| iNumberOfLambda2Descents > iMaxNumberOfLambda2Descents)
		{
			// if a break condition is reached, refine sampling and try again
			if (abs(static_cast<int>(dLambda2Value - dL2Threshold)) > dL2Epsilon
				&& iRefinementCount < iMaxRefinements)
			{
				vtkMath::MultiplyScalar(d3IntegrationStep, .5);
				++iRefinementCount;
				continue;
			}
			else
			{
				break;
			}
		}
		else if (iRefinementCount > 0 && iRefinementCount < iMaxRefinements)
		{
			// refinement of sampling distance started because of coming 
			// close to the border, so further refine sampling upon every step
			vtkMath::MultiplyScalar(d3IntegrationStep, .5);
			++iRefinementCount;
		}


		for (int i = 0; i < 3; i++)
		{
			d3LastSamplingPoint[i] = d3CurrentSamplingPoint[i];
		}

		double d3StartToEnd[3];

		vtkMath::Subtract(
			d3LastSamplingPoint,
			d3StartPoint,
			d3StartToEnd);

		dRadius = vtkMath::Norm(d3StartToEnd);

		// test if a self-intersection occurs, if it is the case, stop sampling
		if (dRadius >= dMaxRadius)
		{
			dRadius = dMaxRadius;
			break;
		}
	}

	delete pTimer;

	return dRadius;	
}

//------------------------------------------------------------------------------
// Main filter function
//------------------------------------------------------------------------------
void VveVtkVortexHullCalculator::Execute()
{
	delete m_pVortexData;

	m_pVortexData = new VveVtkVortexData();
	m_pVortexData->SetNumberOfSegments(m_iNumOfSamplesPerSegment);

	vtkDataSet *pCoreLine = this->GetInput(0);
	vtkDataSet *pDataSet = this->GetInput(1);

	if(! pCoreLine )
	{
		vstr::errp() << "[VveVtkComputeVortexHull::ComputeVortexSurface]: no core line set!" << std::endl;
		return;
	}

	if(! pDataSet)
	{
		vstr::errp() << "[VveVtkComputeVortexHull::ComputeVortexSurface]: no data set!" << std::endl;
		return;
	}

	double d3LastCorelinePoint[3], 
		d3ActualCorelinePoint[3], 
		d3NextCoreLinePoint[3];

	double d3VectorToNextCorelinePoint[3],
		d3VectorFromLastCorelinePoint[3];

	double d3LastFrontVector[3],
		d3ActualFrontVector[3];

	double d3LastUpVector[3] = { .0, 1.0, .0 }, d3ActualUpVector[3];

	double d3LastRightVector[3] = { 1.0, .0, .0 }, d3ActualRightVector[3];

	int iNumPoints = static_cast<int>(pCoreLine->GetNumberOfPoints());

	pCoreLine->GetPoint(0, d3LastCorelinePoint);
	m_pVortexData->SetStartPoint(d3LastCorelinePoint);

	// called once to initialize dataset for parallel processing
	double d3Dir[] = { 1., 0., 0. };
	double d3Point[] = {0., 0., 0. };
	SampleOverLambda2(
		d3Point, 
		d3Dir, 
		m_dHullSamplingStepSize, 
		m_dLambda2Threshold, 
		10000000.);

	// iterate over all core line segments and compute hull
	for (int i = 1; i < iNumPoints - 1; ++i)
	{
		pCoreLine->GetPoint(i, d3ActualCorelinePoint);
		pCoreLine->GetPoint(i + 1, d3NextCoreLinePoint);

		// get direction vectors

		vtkMath::Subtract(
			d3ActualCorelinePoint, 
			d3LastCorelinePoint, 
			d3VectorFromLastCorelinePoint);

		vtkMath::Subtract(
			d3NextCoreLinePoint, 
			d3ActualCorelinePoint, 
			d3VectorToNextCorelinePoint);


		// !!!This test is deactivated, because it consumes an incredible amount
		// of Calculation time!!! -> more performing test needed
		// test if the actual coreline segment runs into the vortex again
		// if it does, terminate
// 		bool bTerminateMeshing = false;
// 		int iNumRings = m_pVortexData->GetNumberOfRings();
// 		double d3Dummy[3];
// 		for (int i = 0; i < iNumRings - 1; ++i)
// 		{
// 			// test against all rings except the last one
// 			bTerminateMeshing |= m_pVortexData->GetVortexRingData(i)->
// 				IntersectWithLine(
// 				d3ActualCorelinePoint, d3NextCoreLinePoint, d3Dummy);
// 			if (bTerminateMeshing)
// 			{
// 				break;
// 			}
// 		}
// 	
// 		if (bTerminateMeshing)
// 		{
// 			// terminate the meshing
// 			break;
// 		}

		for (int k = 0; k < 3; ++k)
		{
			// the hull segment axis is between the two directions
			d3ActualFrontVector[k] = 
				.5 * 
				(d3VectorFromLastCorelinePoint[k]
			+ d3VectorToNextCorelinePoint[k]);
		}

		vtkMath::Cross(d3LastUpVector, 
			d3LastFrontVector, 
			d3ActualRightVector);

		if (vtkMath::Dot(d3ActualFrontVector, d3ActualRightVector) != 0)
		{
			vtkMath::Cross(d3ActualFrontVector,
				d3ActualRightVector,
				d3ActualUpVector);
		}
		else
		{
			// if last right vector is collinear to current front vector use
			// the last front vector instead (this will than be 
			// perpendicular to current front vector)
			vtkMath::Cross(d3ActualFrontVector,
				d3LastFrontVector,
				d3ActualUpVector);
		}

		vtkMath::Normalize(d3ActualUpVector);
		vtkMath::Normalize(d3ActualRightVector);
		vtkMath::Normalize(d3ActualFrontVector);

		float fAnglePerStep = 360.0f / m_iNumOfSamplesPerSegment;

		double *pRingRadii = new double[m_iNumOfSamplesPerSegment];

		// sample over the points of a single segment

		for (int r = 0; r < m_iNumOfSamplesPerSegment; ++r)
		{
			VistaQuaternion qRotation = VistaQuaternion(
				VistaAxisAndAngle(
					VistaVector3D(d3ActualFrontVector),
					Vista::DegToRad(r * fAnglePerStep)
				));
			
			VistaVector3D v3dSamplingDirection = 
				qRotation.Rotate(VistaVector3D(d3ActualUpVector));

			double d3SamplingDirection[3];
			v3dSamplingDirection.GetValues(d3SamplingDirection);

			double dRadius = SampleOverLambda2(
				d3ActualCorelinePoint, 
				d3SamplingDirection, 
				m_dHullSamplingStepSize, 
				m_dLambda2Threshold, 
				m_dHullSamplingEpsilon);

			if (dRadius < m_dMinRadius) dRadius = m_dMinRadius;

			pRingRadii[r] = dRadius;
		}

		m_pVortexData->AddRing(
			d3ActualCorelinePoint,
			d3ActualFrontVector,
			d3ActualUpVector,
			pRingRadii);

		// make the actual vectors the last vectors for the next iteration
		for (int j = 0; j < 3; j++)
		{
			d3LastCorelinePoint[j] = d3ActualCorelinePoint[j];
			d3LastRightVector[j] = d3ActualRightVector[j];
			d3LastUpVector[j] = d3ActualUpVector[j];
			d3LastFrontVector[j] = d3ActualFrontVector[j];
		}

	}

	m_pVortexData->SetEndPoint(d3NextCoreLinePoint);

	if (m_bSmoothVortex)
	{
		m_pVortexData->SmoothCoreLine();
		m_pVortexData->SmoothCoreLine();

		m_pVortexData->SmoothLongitudinal();
		m_pVortexData->SmoothRadial();
	}

	if (m_bRemoveSelfIntersections)
	{
		m_pVortexData->RemoveSelfIntersections(m_dMinRadius * .5);
	}

	// set hull output
	vtkPolyData *pHull = m_pVortexData->CreateHull();
	GetOutput(0)->DeepCopy(pHull);
	pHull->Delete();

	// set core line output
	vtkPolyData *pReducedCoreLine = m_pVortexData->CreateCoreLine();
	GetOutput(1)->DeepCopy(pReducedCoreLine);
	pReducedCoreLine->Delete();

	// set skeleton output
	vtkPolyData *pSkeleton = m_pVortexData->CreateSkeleton();
	GetOutput(2)->DeepCopy(pSkeleton);
	pSkeleton->Delete();

	// set small skeleton output
	vtkPolyData *pSmallSkeleton = m_pVortexData->CreateSkeleton(
		m_fSmallSkeletonMultiplier);
	GetOutput(3)->DeepCopy(pSmallSkeleton);
	pSmallSkeleton->Delete();
}

// returns true if the line defined by d3LineStart and d3LineEnd intersects
// the vortex hull
// if d3IntersectionPoint != 0 the closest intersection to d3LineStart is 
// returned in that array
bool VveVtkVortexHullCalculator::IntersectVortexHullWithLine(
	const double d3LineStartIn[3], 
	const double d3LineEndIn[3],
	vtkCellArray *pHullPolys,
	vtkPoints *pHullPoints,
	double d3IntersectionPoint[3])
{
	// needed to be able to use const inputs
	double d3LineStart[3], d3LineEnd[3];
	for (int i = 0; i < 3; ++i)
	{
		d3LineStart[i] = d3LineStartIn[i];
		d3LineEnd[i] = d3LineEndIn[i];
	}
	vtkPolygon *pPoly = vtkPolygon::New();
	pHullPolys->InitTraversal();
	vtkIdType iNPts, *pPts;
	double d3X[3], d3PCoords[3];
	double iT;
	int iSubId;
	bool bHullIntersected = false;
	double dClosestDistance = std::numeric_limits<double>::max();
	while(pHullPolys->GetNextCell(iNPts, pPts))
	{
		pPoly->Initialize(iNPts, pPts, pHullPoints);
		if (bHullIntersected = pPoly->IntersectWithLine(
			d3LineStart, 
			d3LineEnd, 
			0., 
			iT, 
			d3X, 
			d3PCoords,
			iSubId) != 0)
		{
			if (!d3IntersectionPoint) 
			{
				// no return value is wanted, so we can break now
				break;
			}
			else
			{
				double dTmp[3];
				vtkMath::Subtract(d3X, d3LineStart, dTmp);				
				double dDistance = vtkMath::Norm(dTmp);

				if (dDistance < dClosestDistance)
				{
					dClosestDistance = dDistance;
					for (int i = 0; i < 3; i++)
					{
						d3IntersectionPoint[i] = d3X[i];
					}
				}
			}
		}
	}
	pPoly->Delete();

	return bHullIntersected;
}

bool VveVtkVortexHullCalculator::GetRemoveSelfIntersections() const
{
	return m_bRemoveSelfIntersections;
}

void VveVtkVortexHullCalculator::SetRemoveSelfIntersections( bool val )
{
	m_bRemoveSelfIntersections = val;
}

bool VveVtkVortexHullCalculator::GetSmoothVortex() const
{
	return m_bSmoothVortex;
}

void VveVtkVortexHullCalculator::SetSmoothVortex( bool val )
{
	m_bSmoothVortex = val;
}

double VveVtkVortexHullCalculator::GetHullSamplingEpsilon() const
{
	return m_dHullSamplingEpsilon;
}

void VveVtkVortexHullCalculator::SetHullSamplingEpsilon( double val )
{
	m_dHullSamplingEpsilon = val;
}

float VveVtkVortexHullCalculator::GetSmallSkeletonRadiusMultiplier() const
{
	return m_fSmallSkeletonMultiplier;
}

void VveVtkVortexHullCalculator::
	SetSmallSkeletonRadiusMultiplier( float fMultiplier )
{
	m_fSmallSkeletonMultiplier = fMultiplier;
}

VveVtkVortexData * VveVtkVortexHullCalculator::GetVortexData() const
{
	return m_pVortexData;
}

const std::string VveVtkVortexHullCalculator::GetVorticityFieldName() const
{
	return m_sVorticityFieldName;
}

void VveVtkVortexHullCalculator::
	SetVorticityFieldName( const std::string &sFieldName )
{
	m_sVorticityFieldName = sFieldName;
}

const std::string VveVtkVortexHullCalculator::GetLambda2FieldName() const
{
	return m_sLambda2FieldName;
}

void VveVtkVortexHullCalculator::
	SetLambda2FieldName( const std::string &sFieldName )
{
	m_sLambda2FieldName = sFieldName;
}

double VveVtkVortexHullCalculator::GetHullSamplingStepSize() const
{
	return m_dHullSamplingStepSize;
}

void VveVtkVortexHullCalculator::
	SetHullSamplingStepSize( const double dHullSamplingStepSize )
{
	m_dHullSamplingStepSize = dHullSamplingStepSize;
}

double VveVtkVortexHullCalculator::GetMinRadius() const
{
	return m_dMinRadius;
}

void VveVtkVortexHullCalculator::SetMinRadius( const double dMinRadius )
{
	m_dMinRadius = dMinRadius;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveVtkComputeVortexHull.cpp"                                 */
/*============================================================================*/

