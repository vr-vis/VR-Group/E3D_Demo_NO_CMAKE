/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#ifndef _VVEVTKVORTEXDATA_H
#define _VVEVTKVORTEXDATA_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaVisExt/VistaVisExtConfig.h>

#include <vector>
#include <string>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkPolyData;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveVtkVortexData
{
public:
	class CVortexRingData
	{
	public:
		CVortexRingData(
			double d3Center[3],
			double d3Axis[3],
			double d3UpVector[3],
			unsigned int uiNumberOfSegments,
			double *pRadii);

		~CVortexRingData();

		double m_d3Center[3];
		double m_d3Axis[3];
		double m_d3UpVector[3];
		double *m_pRadii;
		bool m_bModified;

		void Update();

		vtkPolyData *GetDisc();

		void GetRingPoint(
			int iIndex,
			double d3ReturnPoint[3],
			double dRadiusScaleFactor = 1.);

		void PrintToConsole();

		bool IntersectWithLine( 
			const double d3LineStartIn[3], 
			const double d3LineEndIn[3], 
			double d3IntersectionPoint[3] );

	protected:
		vtkPolyData *CreateDisc();

	protected:
		unsigned int GetValidRingIndex(int iIndex);

		unsigned int m_uiNumberOfSegments;
		vtkPolyData *m_pRingDisc;

	};

public:
	VveVtkVortexData();

	virtual ~VveVtkVortexData();

	/**
	 * Add a new ring to the vortex.
	 * pRadii _must_ be a double array with NumberOfSegments ring segments
	 */
	void AddRing(
		double d3Center[3],
		double d3Axis[3],
		double d3UpVector[3],
		double *pRadii);

	void SetNumberOfSegments(unsigned int uiNum);
	unsigned int GetNumberOfSegments() const;

	unsigned int GetNumberOfRings() const;

	CVortexRingData *GetVortexRingData(unsigned int i);

	void SmoothRadial();
	void SmoothLongitudinal();
	void SmoothCoreLine();

	/**
	 * Sets the start point of the vortex. Must be set for a proper behavior
	 */
	void SetStartPoint(double d3Start[3]);

	/**
	 * Sets the end point of the vortex. Must be set for a proper behavior
	 */	
	void SetEndPoint(double d3End[3]);

	/**
	 * Creates a vtkPolyData object containing the core line. The caller is
	 * responsible for its deletion
	 */
	vtkPolyData *CreateCoreLine();

	/**
	 * Creates a vtkPolyData object containing the vortex hull. The caller is
	 * responsible for its deletion
	 */
	vtkPolyData *CreateHull();

	/**
	 * Creates a vtkPolyData object containing the vortex skeleton. The caller 
	 * is responsible for its deletion
	 */
	vtkPolyData *CreateSkeleton(double dRadiusScaleFactor = 1.);

	/**
	 * Creates a vtkPolyData object containing the vortex ring discs
	 * (=filled skeleton rings). The caller is responsible for its deletion
	 */
	vtkPolyData *CreateRingDiscs();

	/**
	 * Removes self intersections of the vortex hull by reducing all ring 
	 * segments against each other. Can be very time consuming, especially for
	 * long vortices!
	 */
	void RemoveSelfIntersections(double dSelfIntersectionDistance = 0.);

	void PrintToConsole();

protected:

	unsigned int m_uiNumberOfSegments;

	double m_d3StartPoint[3], m_d3EndPoint[3];

	std::vector<CVortexRingData*> m_vRings;

	/**
	 * Takes two ring segments and reduces them against each other, what means
	 * that all radii of both segments are shortened in regions where 
	 * intersections occur.
	 */
	bool ReduceOneRingAgainstAnother(
		CVortexRingData *pReducedRing,
		CVortexRingData *pCollisionRing,
		double *pNewRadii,
		double dSelfIntersectionDistance = 0.);

	unsigned int GetValidRingIndex(int iIndex);

private:

	std::string PointToString(double d3[3]);

};

#endif // Include guard.

/*============================================================================*/
/*  END OF FILE "VveVtkVortexData.h"                                          */
/*============================================================================*/
