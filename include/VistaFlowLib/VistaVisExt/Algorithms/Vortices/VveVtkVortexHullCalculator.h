/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#ifndef __VVEVTKCOMPUTEVORTEXHULL_H__
#define __VVEVTKCOMPUTEVORTEXHULL_H__

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

#pragma warning( disable : 4996 )

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaVisExt/VistaVisExtConfig.h>

#include <vtkPolyDataToPolyDataFilter.h> 

#include <vector>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkFloatArray;
class vtkPoints;
class vtkTransform;
class vtkDataSet;
class vtkCellArray;
class VtkInterpolatedScalarField;
class VveVtkVortexData;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
* This class calculates a vortex hull for a given vortex core line
*/
class VISTAVISEXTAPI VveVtkVortexHullCalculator : public vtkPolyDataToPolyDataFilter
{
public:
	
	static VveVtkVortexHullCalculator * New();

	vtkTypeRevisionMacro(VveVtkVortexHullCalculator, 
		vtkPolyDataToPolyDataFilter);

	// Line + DataSet
	
	void AddInput(vtkPolyData * ds);
	void RemoveInput(vtkPolyData * ds);
	
	vtkPolyData * GetInput(int idx);	
	vtkPolyData * GetInput();

	/**
	 * Set the number of segment on each vortex ring segment
	 */
	bool SetNumOfSamplesPerSegment(const int i);
	int GetNumOfSamplesPerSegment() const;

	/**
	 * Sets the dataset that is used for the hull creation. The dataset must
	 * contain a lambda2 and a vorticity field.
	 */
	void SetData(vtkDataSet * pDataSet);

	void SetLambda2Threshold(const double & MinLambda2);
	double GetLambda2Threshold() const;

	/**
	 * Sets the minimum vortex radius.
	 */
	void SetMinRadius(const double dMinRadius);
	double GetMinRadius() const;

	/**
	 * Step size for sampling the radii the hull segments. The avarage cell 
	 * diameter is a good starting value.
	 */
	void SetHullSamplingStepSize(const double dHullSamplingStepSize);
	double GetHullSamplingStepSize() const;

	/**
	 * Set the name of the scalar field that contains the lambda2 values
	 */
	void SetLambda2FieldName(const std::string &sFieldName);
	const std::string GetLambda2FieldName() const;

	/**
	 * Set the name of the vector field that contains the vorticity vectors
	 */
	void SetVorticityFieldName(const std::string &sFieldName);
	const std::string GetVorticityFieldName() const;

	/**
	 * Scales the smaller skeletons by a certain factor (can be used to 
	 * generate particle seed points inside of vortices).
	 */
	void SetSmallSkeletonRadiusMultiplier(float fMultiplier);
	float GetSmallSkeletonRadiusMultiplier() const;

	/**
	 * This is a speedup parameter for the hull sampling. Sampling is stopped if 
	 * sampled to a value HullSamplingL2Epsilon or less far away from the 
	 * lambda2 threshold
	 */
	void SetHullSamplingEpsilon(double val);
	double GetHullSamplingEpsilon() const;

	/**
	 * Remove self intersections of the vortices. Gives better results but
	 * consumes lots of calculation time especially if really long vortices 
	 * exist
	 */
	bool GetRemoveSelfIntersections() const;
	void SetRemoveSelfIntersections(bool val);

	/**
	 * Enable/disable smoothing the surface of the vortex surface
	 */
	bool GetSmoothVortex() const;
	void SetSmoothVortex(bool val);

	VveVtkVortexData *GetVortexData() const;

protected:

	VveVtkVortexHullCalculator();
	virtual ~VveVtkVortexHullCalculator();	

	void Execute();

	// hide the superclass' AddInput() from the user and the compiler
	void AddInput(vtkDataObject *);

	void RemoveInput(vtkDataObject *input);

	bool IntersectVortexHullWithLine(
		const double d3LineStart[3], 
		const double d3LineEnd[3],
		vtkCellArray *pHullStrips,
		vtkPoints *pHullPoints,
		double d3IntersectionPoint[3] = 0);

private:

	VveVtkVortexHullCalculator(const VveVtkVortexHullCalculator &);
	// Not implemented.
	void operator=(const VveVtkVortexHullCalculator &);
	// Not implemented.

	double SampleOverLambda2(
		const double d3StartPoint[3],
		const double d3Direction[3],
		double dStepWidth,
		double dL2Threshold,
		double dL2Epsilon);

	int m_iNumOfSamplesPerSegment;	

	double m_dLambda2Threshold;

	double m_dMinRadius;

	double m_dHullSamplingStepSize;

	double m_dHullSamplingEpsilon;

	float m_fSmallSkeletonMultiplier;

	std::string m_sLambda2FieldName;
	std::string m_sVorticityFieldName;

	VveVtkVortexData *m_pVortexData;

	bool m_bRemoveSelfIntersections;
	bool m_bSmoothVortex;
};

/*============================================================================*/
/*============================================================================*/

#endif /* ifndef __VVEVTKCOMPUTEVORTEXHULL_H */

/*============================================================================*/
/*  END OF FILE "VveVtkComputeVortexHull.h"                                   */
/*============================================================================*/
