

set( RelativeDir "./Algorithms/Vortices" )
set( RelativeSourceGroup "Source Files\\Algorithms\\Vortices" )

set( DirFiles
	_SourceFiles.cmake
#	VveVtkExtractLocalExtrema.cpp
#	VveVtkExtractLocalExtrema.h
#	VveVtkPredictorCorrectorVortex.cpp
#	VveVtkPredictorCorrectorVortex.h
	VveVtkSFCorrector.cpp
	VveVtkSFCorrector.h
	VveVtkVortexData.cpp
	VveVtkVortexData.h
#	VveVtkVortexHullCalculator.cpp
#	VveVtkVortexHullCalculator.h
	VveVtkVortexUtils.cpp
	VveVtkVortexUtils.h
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )
