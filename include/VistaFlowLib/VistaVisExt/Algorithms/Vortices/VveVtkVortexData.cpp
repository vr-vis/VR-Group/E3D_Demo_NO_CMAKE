#include "VveVtkVortexData.h"

#include "VveVtkVortexUtils.h"

#include <VistaBase/VistaQuaternion.h>
#include <VistaBase/VistaTimer.h>
#include <VistaBase/VistaDefaultTimerImp.h>
#include <VistaBase/VistaStreamUtils.h>

#include <vtkMath.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPolyDataNormals.h>
#include <vtkPolygon.h>
#include <vtkTriangleFilter.h>
#include <vtkAppendPolyData.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>

#include <vector>
#include <sstream>

vtkPolyData * VveVtkVortexData::CreateRingDiscs()
{
	vtkAppendPolyData *pRingDiscs = vtkAppendPolyData::New();

	for (unsigned int i = 0; i < m_vRings.size(); ++i)
	{
#if VTK_MAJOR_VERSION > 5
		pRingDiscs->AddInputData(m_vRings[i]->GetDisc());
#else
		pRingDiscs->AddInput(m_vRings[i]->GetDisc());
#endif
	}

	pRingDiscs->Update();

	vtkPolyData *pReturnData = vtkPolyData::New();
	pReturnData->DeepCopy(pRingDiscs->GetOutput());

	pRingDiscs->Delete();

	return pReturnData;
}

void VveVtkVortexData::
	RemoveSelfIntersections(double dSelfIntersectionDistance)
{
	VistaDefaultTimerImp *pTimerImp = new VistaDefaultTimerImp();
	VistaTimer *pTimer = new VistaTimer(pTimerImp);

	double dStartTime = pTimer->GetMicroTime();

	size_t nNumRings = m_vRings.size();

	// reduce all rings against each other, from near to far
	for (size_t k=1; k<nNumRings; ++k)
	{
		for (size_t i=0; i<nNumRings-k; i++)
		{
			CVortexRingData *pRing1 = m_vRings[i];
			CVortexRingData *pRing2 = m_vRings[i + k];

			double *pNewRing1Radii = new double[m_uiNumberOfSegments];
			double *pNewRing2Radii = new double[m_uiNumberOfSegments];

			bool bRing1Reduced = ReduceOneRingAgainstAnother(
				pRing1, pRing2, pNewRing1Radii, dSelfIntersectionDistance);
			bool bRing2Reduced = ReduceOneRingAgainstAnother(
				pRing2, pRing1, pNewRing2Radii, dSelfIntersectionDistance);

			if (bRing1Reduced)
			{
				pRing1->m_bModified = true;
			}

			if (bRing2Reduced)
			{
				pRing2->m_bModified = true;
			}

			for (unsigned int j = 0; j < m_uiNumberOfSegments; ++j)
			{
				pRing1->m_pRadii[j] = pNewRing1Radii[j];
				pRing2->m_pRadii[j] = pNewRing2Radii[j];
			}

			delete [] pNewRing1Radii;
			delete [] pNewRing2Radii;
		}
	}

	delete pTimer;
}

vtkPolyData *VveVtkVortexData::
	CreateSkeleton(double dRadiusScaleFactor)
{
	size_t nNumRings = m_vRings.size();

	vtkPolyData *pSkeleton = vtkPolyData::New();

	///////////////////////////////////
	// create the hull skeleton consisting of rings around the core line
	vtkPoints *pSkeletonPoints = vtkPoints::New();
	pSkeleton->SetPoints(pSkeletonPoints);

	vtkCellArray *pSkeletonSegments = vtkCellArray::New();
	pSkeleton->SetLines(pSkeletonSegments);

	vtkDataArray *pNormals = vtkDoubleArray::New();
	pNormals->SetNumberOfComponents(3);
	pNormals->SetNumberOfTuples(nNumRings * m_uiNumberOfSegments);
	pSkeleton->GetPointData()->SetNormals(pNormals);

	// insert all points for the vortex skeleton
	double d3NewHullPoint[3];
	double d3Normal[3];
	for (size_t i = 0; i < nNumRings; ++i)
	{
		CVortexRingData *pRing = m_vRings[i];
		for (unsigned int j = 0; j < m_uiNumberOfSegments; ++j)
		{
			pRing->GetRingPoint(j, d3NewHullPoint, dRadiusScaleFactor);
			pSkeletonPoints->InsertNextPoint(d3NewHullPoint);

			// calculate normal
			vtkMath::Subtract(d3NewHullPoint, pRing->m_d3Center, d3Normal);
			pNormals->SetTuple(i * m_uiNumberOfSegments + j, d3Normal);
		}
	}

	for (size_t i = 0; i < nNumRings; ++i)
	{
		int iFirstPointInSegment = static_cast<int>(i * m_uiNumberOfSegments);
		int iLastPointInSegment = iFirstPointInSegment +
			static_cast<int>(m_uiNumberOfSegments) - 1;

		vtkIdList *pSkeletonPointIds = vtkIdList::New();

		for (int j = iFirstPointInSegment; j < iLastPointInSegment; ++j)
		{
			pSkeletonPointIds->InsertNextId(j);			
		}
		pSkeletonPointIds->InsertNextId(iFirstPointInSegment);

		pSkeletonSegments->InsertNextCell(pSkeletonPointIds);

		pSkeletonPointIds->Delete();
	}

	pSkeletonPoints->Delete();
	pSkeletonSegments->Delete();

	return pSkeleton;
}

vtkPolyData * VveVtkVortexData::CreateHull()
{
	vtkPolyData *pVortexHull = vtkPolyData::New();

	vtkPoints *pHullPoints = vtkPoints::New();
	pVortexHull->SetPoints(pHullPoints);

	vtkCellArray *pHullPolys = vtkCellArray::New();
	pVortexHull->SetPolys(pHullPolys);

	size_t iNumRings = m_vRings.size();
	// insert all points for the vortex hull
	double d3NewHullPoint[3];
	for (size_t i = 0; i < iNumRings; ++i)
	{
		CVortexRingData *pRing = m_vRings[i];
		for (unsigned int j = 0; j < m_uiNumberOfSegments; ++j)
		{
			pRing->GetRingPoint(j, d3NewHullPoint);
			pHullPoints->InsertNextPoint(d3NewHullPoint);
		}
	}

	// mesh the vortex hull
	for (size_t i = 1; i < iNumRings; ++i)
	{
		vtkIdType iFirstPointInLastRing = 
			(i - 1) * m_uiNumberOfSegments;

		vtkIdType iFirstPointInCurrentRing = 
			i * m_uiNumberOfSegments;

		if (iFirstPointInLastRing >= 0)
		{
			// mesh the surface between the actual and the last 
			// ring segment
			for (
				unsigned int iPointId = 0; 
				iPointId < m_uiNumberOfSegments; 
			++iPointId)
			{
				vtkIdList *pPointIds = vtkIdList::New();

				pPointIds->InsertNextId(
					iFirstPointInLastRing + iPointId);

				pPointIds->InsertNextId(
					iFirstPointInLastRing 
					+ GetValidRingIndex(iPointId + 1));

				pPointIds->InsertNextId(
					iFirstPointInCurrentRing
					+ GetValidRingIndex(iPointId + 1));

				pPointIds->	InsertNextId(
					iFirstPointInCurrentRing + iPointId);

				pHullPolys->InsertNextCell(pPointIds);

				pPointIds->Delete();
			}
		}
	}

	///////////////////////////////////
	// create beginning and end caps

	// insert start point
	vtkIdType iStartPointID = pHullPoints->InsertNextPoint(m_d3StartPoint);	

	// insert end point
	vtkIdType iEndPointID = pHullPoints->InsertNextPoint(m_d3EndPoint);

	// mesh the caps at the beginning and the end of the vortex
	for (unsigned int i = 0; i < m_uiNumberOfSegments; ++i)
	{	

		pHullPolys->InsertNextCell(3);
		pHullPolys->InsertCellPoint(iStartPointID);
		pHullPolys->InsertCellPoint(i);
		pHullPolys->InsertCellPoint(GetValidRingIndex(i + 1));

		pHullPolys->InsertNextCell(3);
		pHullPolys->InsertCellPoint(iEndPointID);
		pHullPolys->InsertCellPoint(
			iStartPointID - 1 - i);
		pHullPolys->InsertCellPoint(
			iStartPointID - 1 - GetValidRingIndex(i + 1));			
	}


	// create smooth normals for the vortex hull
	vtkPolyDataNormals *pNormals = vtkPolyDataNormals::New();
	pNormals->SetFeatureAngle(360.);
	pNormals->SplittingOff();
	pNormals->AutoOrientNormalsOff();
	pNormals->ComputeCellNormalsOn();
	pNormals->ComputePointNormalsOn();
	pNormals->FlipNormalsOff();

#if VTK_MAJOR_VERSION > 5
	pNormals->SetInputData(pVortexHull);
#else
	pNormals->SetInput(pVortexHull);
#endif

	pNormals->Update();

	vtkPolyData *pReturnData = vtkPolyData::New();
	pReturnData->DeepCopy(pNormals->GetOutput());

	pNormals->Delete();
	pVortexHull->Delete();

	return pReturnData;
}

vtkPolyData * VveVtkVortexData::CreateCoreLine()
{
	vtkPolyData *pCoreLine = vtkPolyData::New();

	vtkPoints *pCoreLinePoints = vtkPoints::New();
	pCoreLine->SetPoints(pCoreLinePoints);

	vtkCellArray *pCoreLineLines = vtkCellArray::New();
	pCoreLine->SetLines(pCoreLineLines);

	// insert the line points
	pCoreLinePoints->InsertNextPoint(m_d3StartPoint);
	unsigned int uiNumRings = static_cast<unsigned int>(m_vRings.size());
	for (unsigned int i = 0; i < uiNumRings; ++i)
	{
		pCoreLinePoints->InsertNextPoint(m_vRings[i]->m_d3Center);
	}
	pCoreLinePoints->InsertNextPoint(m_d3EndPoint);

	// connect the points
	vtkIdList *pLineIds = vtkIdList::New();
	vtkIdType iNumLinePoints = pCoreLinePoints->GetNumberOfPoints();
	for (int i = 0; i < iNumLinePoints; ++i)
	{
		pLineIds->InsertNextId(i);
	}
	pCoreLineLines->InsertNextCell(pLineIds);

	// clean up
	pCoreLinePoints->Delete();
	pCoreLineLines->Delete();

	// return result
	return pCoreLine;
}

void VveVtkVortexData::SetEndPoint( double d3End[3] )
{
	for (int i = 0; i < 3; ++i)
	{
		m_d3EndPoint[i] = d3End[i];
	}
}

void VveVtkVortexData::SetStartPoint( double d3Start[3] )
{
	for (int i = 0; i < 3; ++i)
	{
		m_d3StartPoint[i] = d3Start[i];
	}
}

void VveVtkVortexData::SmoothCoreLine()
{
	size_t uiNumRings = m_vRings.size();
	double *pNewCoreLine = new double[uiNumRings * 3];

	for (unsigned int i = 0; i < uiNumRings; ++i)
	{
		double *pNewPoint = pNewCoreLine + 3*i;

		for (unsigned int j = 0; j < 3; j++)
		{
			pNewPoint[j] = 0;

			if (i == 0)
			{
				pNewPoint[j] += m_d3StartPoint[j];
			}
			else
			{
				pNewPoint[j] += m_vRings[i - 1]->m_d3Center[j];
			}

			pNewPoint[j] += m_vRings[i]->m_d3Center[j];

			if (i == uiNumRings - 1)
			{
				pNewPoint[j] += m_d3EndPoint[j];
			}
			else
			{
				pNewPoint[j] += m_vRings[i + 1]->m_d3Center[j];
			}

			pNewPoint[j] /= 3.;
		}
	}

	for (unsigned int i = 0; i < uiNumRings; ++i)
	{
		double *pNewPoint = pNewCoreLine + 3*i;
		for (unsigned int j = 0; j < 3; j++)
		{
			m_vRings[i]->m_d3Center[j] = pNewPoint[j];
		}
		m_vRings[i]->m_bModified = true;
	}

	delete [] pNewCoreLine;

}

void VveVtkVortexData::SmoothLongitudinal()
{
	size_t uiNumRings = m_vRings.size();

	for (unsigned int i = 0; i < m_uiNumberOfSegments; ++i)
	{
		double *pNewRadii = new double[uiNumRings];

		for (unsigned int j = 0; j < uiNumRings; ++j)
		{
			pNewRadii[j] = 0.;
			int iCnt = 0;

			if (j > 0) 
			{
				pNewRadii[j] += m_vRings[j-1]->m_pRadii[i];
				++iCnt;
			}

			pNewRadii[j] += m_vRings[j]->m_pRadii[i];
			++iCnt;

			if (j < uiNumRings - 1)
			{
				pNewRadii[j] += m_vRings[j+1]->m_pRadii[i];
				++iCnt;
			}

			pNewRadii[j] /= iCnt;
		}			

		for (unsigned int j = 0; j < uiNumRings; ++j)
		{
			m_vRings[j]->m_pRadii[i] = pNewRadii[j];
			m_vRings[j]->m_bModified = true;
		}			

		delete [] pNewRadii;
	}
}

void VveVtkVortexData::SmoothRadial()
{
	size_t uiNumRings = m_vRings.size();
	for (unsigned int i = 0; i < uiNumRings; ++i)
	{
		double *pRadii = m_vRings[i]->m_pRadii;
		double *pNewRadii = new double[m_uiNumberOfSegments];

		for (unsigned int j = 0; j < m_uiNumberOfSegments; ++j)
		{
			pNewRadii[j] = pRadii[j];
			pNewRadii[j] += pRadii[GetValidRingIndex(j - 1)];
			pNewRadii[j] += pRadii[GetValidRingIndex(j + 1)];
			pNewRadii[j] /= 3.;
		}			

		for (unsigned int j = 0; j < m_uiNumberOfSegments; ++j)
		{
			pRadii[j] = pNewRadii[j];
		}			

		delete [] pNewRadii;
		m_vRings[i]->m_bModified = true;
	}
}

VveVtkVortexData::CVortexRingData 
	*VveVtkVortexData::GetVortexRingData(unsigned int i)
{
	return m_vRings[i];
}

unsigned int VveVtkVortexData::GetNumberOfSegments() const
{
	return m_uiNumberOfSegments;
}

void VveVtkVortexData::SetNumberOfSegments( unsigned int uiNum )
{
	m_uiNumberOfSegments = uiNum;
}

void VveVtkVortexData::AddRing(
	double d3Center[3],
	double d3Axis[3],
	double d3UpVector[3],
	double *pRadii )
{
	CVortexRingData *pData = new CVortexRingData(
		d3Center,
		d3Axis,
		d3UpVector,
		m_uiNumberOfSegments,
		pRadii);

	m_vRings.push_back(pData);
}

VveVtkVortexData::~VveVtkVortexData()
{
	for (unsigned int i = 0; i < m_vRings.size(); ++i)
	{
		delete m_vRings[i];
	}
}

VveVtkVortexData::VveVtkVortexData()
{

}

std::string VveVtkVortexData::PointToString( double d3[3] )
{
	std::stringstream sStr;
	sStr << "(" << d3[0] << ";" << d3[1] << ";" << d3[2]<< ")";
	return sStr.str();
}

unsigned int VveVtkVortexData::GetValidRingIndex( int iIndex )
{
	if (iIndex < 0) 
	{
		int iTmpIndex = -iIndex;
		iTmpIndex %= m_uiNumberOfSegments;
		return m_uiNumberOfSegments - iTmpIndex;
	}
	if (iIndex >= static_cast<int>(m_uiNumberOfSegments)) 
	{
		return iIndex % m_uiNumberOfSegments;
	}
	return iIndex;
}

bool VveVtkVortexData::ReduceOneRingAgainstAnother( 
	CVortexRingData *pReducedRing, 
	CVortexRingData *pCollisionRing, 
	double *pNewRadii, 
	double dSelfIntersectionDistance /*= 0.*/ )
{
	double d3StartPoint[3], 
		d3EndPoint[3], 
		d3IntersectionPoint[3],
		d3CenterToIntersection[3];

	for (int i = 0; i < 3; ++i)
	{
		d3StartPoint[i] = pReducedRing->m_d3Center[i];
	}

	bool bReductionOcccured = false;

	for (unsigned int j = 0; j < m_uiNumberOfSegments; ++j)
	{
		pReducedRing->GetRingPoint(
			j,
			d3EndPoint);

		if (pCollisionRing->IntersectWithLine(
			d3StartPoint,
			d3EndPoint,
			d3IntersectionPoint))
		{
			vtkMath::Subtract(
				d3IntersectionPoint, 
				pReducedRing->m_d3Center, 
				d3CenterToIntersection);

			pNewRadii[j] = vtkMath::Norm(d3CenterToIntersection)
				- dSelfIntersectionDistance;

			if (pNewRadii[j] < 0.) pNewRadii[j] = 0.;

			bReductionOcccured = true;
		}
		else
		{
			pNewRadii[j] = pReducedRing->m_pRadii[j];
		}
	}

	return bReductionOcccured;
}

bool VveVtkVortexData::CVortexRingData::IntersectWithLine( 
	const double d3LineStartIn[3], 
	const double d3LineEndIn[3], 
	double d3IntersectionPoint[3] )
{
	// needed to be able to use const inputs
	double d3LineStart[3], d3LineEnd[3];
	for (int i = 0; i < 3; ++i)
	{
		d3LineStart[i] = d3LineStartIn[i];
		d3LineEnd[i] = d3LineEndIn[i];
	}

	vtkPolygon *pPoly = vtkPolygon::New();

	vtkIdType iNPts, *pPts;

	vtkPolyData *pRingDisc = GetDisc();

	vtkCellArray *pRingDiscPolys = pRingDisc->GetPolys();
	vtkPoints *pRingDiscPoints = pRingDisc->GetPoints();

	pRingDiscPolys->InitTraversal();

	double d3X[3], d3PCoords[3];
	double iT;
	int iSubId;
	bool bHullIntersected = false;

	static int icount = 0;

	while (pRingDiscPolys->GetNextCell(iNPts, pPts))
	{
		pPoly->Initialize(iNPts, pPts, pRingDiscPoints);

		bHullIntersected = 0 != pPoly->IntersectWithLine(
			d3LineStart,
			d3LineEnd,
			0.0001,
			iT,
			d3X,
			d3PCoords,
			iSubId);

		if (bHullIntersected)
		{
			for (int i = 0; i < 3; i++)
			{
				d3IntersectionPoint[i] = d3X[i];
			}
			break;
		}
	}

	pPoly->Delete();

	return bHullIntersected;
}

void VveVtkVortexData::PrintToConsole()
{
	vstr::outi() << "Number of Rings: " << m_vRings.size() << std::endl;
	for(size_t i = 0; i < m_vRings.size(); ++i)
	{
		m_vRings[i]->PrintToConsole();
	}
}

unsigned int VveVtkVortexData::GetNumberOfRings() const
{
	return static_cast<unsigned int>(m_vRings.size());
}

VveVtkVortexData::CVortexRingData::CVortexRingData( 
	double d3Center[3], 
	double d3Axis[3], 
	double d3UpVector[3], 
	unsigned int uiNumberOfSegments, 
	double *pRadii ) :
m_bModified(true),
m_uiNumberOfSegments(uiNumberOfSegments),
m_pRingDisc(0)
{
	for (int i = 0; i < 3; ++i)
	{
		m_d3Center[i] = d3Center[i];
		m_d3Axis[i] = d3Axis[i];
		m_d3UpVector[i] = d3UpVector[i];
	}
	m_pRadii = pRadii;

	vtkMath::Normalize(m_d3Axis);
	vtkMath::Normalize(m_d3UpVector);
}

VveVtkVortexData::CVortexRingData::~CVortexRingData()
{
	delete [] m_pRadii;
	if (m_pRingDisc) m_pRingDisc->Delete();
}

void VveVtkVortexData::CVortexRingData::Update()
{
	if (m_bModified)
	{
		if (m_pRingDisc) m_pRingDisc->Delete();
		m_pRingDisc = CreateDisc();

		m_bModified = false;
	}
}

vtkPolyData * VveVtkVortexData::CVortexRingData::GetDisc()
{
	Update();
	return m_pRingDisc;
}

void VveVtkVortexData::CVortexRingData::GetRingPoint( 
	int iIndex, double d3ReturnPoint[3], double dRadiusScaleFactor)
{
	double dAngle = 2. * vtkMath::Pi() * iIndex / m_uiNumberOfSegments;

	VistaQuaternion qRotation = VistaQuaternion(
		VistaAxisAndAngle(
		VistaVector3D(m_d3Axis),
		dAngle
		));

	VistaVector3D v3dDirectionVector = 
		qRotation.Rotate(VistaVector3D(m_d3UpVector));

	double d3DirectionVector[3];
	v3dDirectionVector.GetValues(d3DirectionVector);

	vtkMath::MultiplyScalar(
		d3DirectionVector, 
		m_pRadii[iIndex] * dRadiusScaleFactor);

	vtkMath::Add(m_d3Center, d3DirectionVector, d3ReturnPoint);
}

vtkPolyData * VveVtkVortexData::CVortexRingData::CreateDisc()
{
	vtkPolyData *pRingDiscData = vtkPolyData::New();
	vtkPoints *pRingDiscPoints = vtkPoints::New();
	vtkCellArray *pRingDiscPolys = vtkCellArray::New();

	pRingDiscData->SetPoints(pRingDiscPoints);
	pRingDiscData->SetPolys(pRingDiscPolys);

	double d3Tmp[3];

	// insert points
	for (unsigned int i = 0; i < m_uiNumberOfSegments; ++i)
	{
		GetRingPoint(i, d3Tmp);
		pRingDiscPoints->InsertNextPoint(d3Tmp);
	}

	vtkIdType iCenter = pRingDiscPoints->InsertNextPoint(m_d3Center);

	// create polys
	vtkIdType i3TrianglePoints[3];
	for (unsigned int i = 0; i < m_uiNumberOfSegments; ++i)
	{
		i3TrianglePoints[0] = iCenter;
		i3TrianglePoints[1] = i;
		i3TrianglePoints[2] = GetValidRingIndex(i + 1);

		pRingDiscPolys->InsertNextCell(3, i3TrianglePoints);
	}

	pRingDiscPoints->Delete();
	pRingDiscPolys->Delete();

	return pRingDiscData;
}

unsigned int VveVtkVortexData::CVortexRingData::GetValidRingIndex( int iIndex )
{
	if (iIndex < 0) 
	{
		int iTmpIndex = -iIndex;
		iTmpIndex %= m_uiNumberOfSegments;
		return m_uiNumberOfSegments - iTmpIndex;
	}
	if (iIndex >= static_cast<int>(m_uiNumberOfSegments)) 
	{
		return iIndex % m_uiNumberOfSegments;
	}
	return iIndex;
}

void VveVtkVortexData::CVortexRingData::PrintToConsole()
{
	vstr::outi() << "[";
	for (unsigned int i = 0; i < m_uiNumberOfSegments; ++i)
	{
		if (i > 0) vstr::out() << ";";
		vstr::out() << m_pRadii[i];
	}
	vstr::out() << "]" << std::endl;
}
