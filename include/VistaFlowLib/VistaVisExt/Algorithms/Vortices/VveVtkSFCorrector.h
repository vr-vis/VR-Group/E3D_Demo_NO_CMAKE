/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#ifndef __VVEVTKSFCORRECTOR_H__
#define __VVEVTKSFCORRECTOR_H__

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaVisExt/VistaVisExtConfig.h>

#include <vtkInitialValueProblemSolver.h> 

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VtkPolyDataTools;

class vtkFunctionSet;
class vtkDataSet;
class vtkPolyData;
class vtkPlane;
class vtkCutter;
class vtkMatrix4x4;
class vtkClipPolyData;
class vtkTransform;
class vtkCone;
class vtkSelectEnclosedPoints;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* This is the implementation of the predictor-corrector vortex core line 
* extraction approach. Be careful: this class is ABSOLUTELY NOT THREAD SAFE!!
* Due to the implementation of the vtk stream tracer it is not possible to 
* configure and pass a single instance of a vtkInitialProblemSolver, because
* the tracer just creates a new instance of it. Therefor all configuration has
* to be made in a static context before starting the stream tracer.
*/
class VISTAVISEXTAPI VveVtkSFCorrector : public vtkInitialValueProblemSolver
{
public:

	static VveVtkSFCorrector * New();
	vtkTypeRevisionMacro(VveVtkSFCorrector,vtkInitialValueProblemSolver);

	/**
	 * Implementation of the ComputeNextStep methods of the 
	 * vtkInitialValueProblemSolver
	 */
	virtual int ComputeNextStep(double* xprev, double* xnext, double t,
		double& delT, double maxError, double& error);

	/**
	 * Implementation of the ComputeNextStep methods of the 
	 * vtkInitialValueProblemSolver
	 */
	virtual int ComputeNextStep(double* xprev, double* dxprev, double* xnext, 
		double t, double& delT, 
		double maxError, double& error);

	/**
	 * Implementation of the ComputeNextStep methods of the 
	 * vtkInitialValueProblemSolver
	 */
	virtual int ComputeNextStep(double* xprev, double* xnext, 
		double t, double& delT, double& delTActual,
		double minStep, double maxStep,
		double maxError, double& error);

	/**
	 * Implementation of the ComputeNextStep methods of the 
	 * vtkInitialValueProblemSolver
	 */
	virtual int ComputeNextStep(double* xprev, double* dxprev, double* xnext, 
		double t, double& delT, double& delTActual,
		double minStep, double maxStep, 
		double maxError, double& error);

	/**
	 * Sets the function set for the given vtkInitialProblemSolver that does
	 * the normal integration
	 */
	virtual void  SetFunctionSet (vtkFunctionSet *functionset);

	/**
	 * Sets the integrator that is responsible for the normal integration
	 */
	void SetIntegrator(vtkInitialValueProblemSolver * pSolver);

	/**
	 * Does a correction following the lambda2 gradient to a minimum in the 
	 * plane perpendicular to the current integration vector
	 */
	bool Correct(double * CurrPos, double * NewPos, double * NewPosCorrected);

	/**
	 * Sets the input data set
	 */
	virtual void SetInput(vtkDataSet * pInput);
	vtkDataSet * GetInput();

	/**
	 * Set the lambda2 threshold for the termination of the integration
	 */
	static void SetLambda2Threshold(double dL2Threshold);
	static double GetLambda2Threshold();

	/**
	 * Set the sampling step size
	 */
	static void SetSamplingStepSize(double dSamplingStepSize);
	static double GetSamplingStepSize();

	/**
	 * Sets the name of the lambda2 field of the input data set
	 */
	static void SetLambda2FieldName(const std::string &sFieldName);
	static const std::string GetLambda2FieldName();	

	/**
	 * Sets the name of the lambda2 gradient fiel of the input data set
	 */
	static void SetGradientLambda2FieldName(const std::string &sFieldName);
	static const std::string GetGradientLambda2FieldName();

	/**
	 * Sets a poly data that is used to test if the current core line runs into
	 * another vortex. If that is the case, the integration stops
	 */
	static void SetVortexHulls(vtkPolyData *pVortexHulls);
	static const vtkPolyData* GetVortexHulls();

protected:

	VveVtkSFCorrector();
	virtual ~VveVtkSFCorrector();

	vtkDataSet * m_pInput;

private:
	
	VveVtkSFCorrector(const VveVtkSFCorrector & );  // Not implemented.
	void operator=(const VveVtkSFCorrector & );      // Not implemented.

	vtkInitialValueProblemSolver * m_pIntegrator;

	vtkSelectEnclosedPoints *m_pSelectEnclosedPoints;

	// making these ones static is really dirty but due to the fact, that
	// vtk's stream tracer creates a new instance of the integrator before
	// using it, this is the only solution to set a lambda2 value as termination
	// criterion
	static double m_dLambda2Threshold;

	static std::string m_sLambda2FieldName;
	static std::string m_sGradientLambda2FieldName;

	static double m_dSamplingStepSize;
	static double m_dMaxCorrectionAngle;

	static vtkPolyData *m_pVortexHulls;	
};

#endif  /* ifndef __VVEVTKSFCORRECTOR_H__ */

/*============================================================================*/
/*  END OF FILE "VveVtkSFCorrector.h"                                         */
/*============================================================================*/
