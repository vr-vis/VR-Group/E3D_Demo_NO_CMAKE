/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


#ifndef __VVEVTKVORTEXUTILS_H__
#define __VVEVTKVORTEXUTILS_H__

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

#pragma warning( disable : 4996 )

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <VistaVisExt/VistaVisExtConfig.h>

#include <vtkType.h>

#include <string>
#include <set>
#include <functional>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class vtkDataSet;
class vtkPolyData;
class vtkPointSet;
class vtkGenericCell;
class vtkCell;
class vtkDataArray;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveVtkVortexUtils
{
public:

	typedef enum{ Average, Min, Max } algo_type;
	static std::string sFilePath;
	//--------------------------------------------------------------------------
	// class to sort std::pair<int,double> elements in a stl container
	//--------------------------------------------------------------------------	
	template <class T1, class T2>
	class less_pair_second : public 
		std::binary_function<std::pair<T1, T2>, std::pair<T1, T2>, bool>
	{
	public:
        typedef std::binary_function<std::pair<T1, T2>, std::pair<T1, T2>, bool>
			PairType;
		bool operator()(
			typename PairType::first_argument_type a,
			typename PairType::second_argument_type b)
		{
			return (a.second < b.second);
		}
	};
	//--------------------------------------------------------------------------
	// class to sort std::pair<int,double> elements in a stl container
	//--------------------------------------------------------------------------
	template <class T1, class T2>
	class greater_pair_second : public 
		std::binary_function<std::pair<T1, T2>, std::pair<T1, T2>, bool>
	{
	public:
		typedef std::binary_function<std::pair<T1, T2>, std::pair<T1, T2>, bool> 
			PairType;
		typename PairType::result_type operator() ( 
			typename PairType::first_argument_type a,
			typename PairType::second_argument_type b )
		{
			return ( a.second > b.second );
		}
	};
	//--------------------------------------------------------------------------
	// pure virtual base class for binary functions
	//--------------------------------------------------------------------------	
	template <class T>
	class binary_function_base : public std::binary_function<T, T, bool>
	{
	public:
		virtual bool operator() ( T a, T b ) = 0;		
	};
	//--------------------------------------------------------------------------
	// less_or_equal: own implementation; std::less has no baseclass
	//--------------------------------------------------------------------------
	template <class T>
	class less_or_equal : public binary_function_base<T>
	{
	public:
        typedef binary_function_base<T> BaseTypes;
		virtual bool operator() ( T a, T b )
		{
			return ( a <= b );
		}
	};
	//--------------------------------------------------------------------------
	// greater: own implementation; std::greater has no baseclass
	//--------------------------------------------------------------------------	
	template <class T>
	class greater : public binary_function_base<T>
	{
	public:
                typedef binary_function_base<T> BaseTypes;
				virtual typename BaseTypes::result_type operator() (
					T a, T b )
		{
			return ( a > b );
		}
	};

	//--------------------------------------------------------------------------
	// Save a vtkDataSet as vtk file using sPrefix and iNum as file name
	// e. g. coreline00 coreline01 coreline02 ...
	// bBinaryFormat: switch on/off binary file format; useful for debugging
	//--------------------------------------------------------------------------
	static void Save
		(vtkDataSet * pDataSet, const std::string & sPrefix, const int & iNum, 
		bool bBinaryFormat = true);

	static void Save
		(vtkDataSet * pDataSet, const std::string & sFileName);

	//--------------------------------------------------------------------------
	// Append two stream lines: a forward line and a backward line
	//--------------------------------------------------------------------------	
	static void AppendStreamLines
		(vtkPolyData * pLineOut, vtkPolyData * pForwardLine, 
		vtkPolyData * pBackwardLine);


	//--------------------------------------------------------------------------
	// Delete nth point of a vtkPointSet
	//--------------------------------------------------------------------------
	static void DeleteNthPoint(vtkPointSet * PS, int Index);

	// these variables are for storing cell information to give FindCell a 
	// starting point for its search in the Interpolate method
	static vtkGenericCell	*m_pInterpolationGenericCell;
	static vtkCell			*m_pInterpolationCell;
	static vtkIdType		m_iInterpolationCellId;
	static vtkDataSet		*m_pInterpolationLastDataSet;
	static vtkDataArray		*m_pInterpolationLastDataArray;

	static bool Interpolate(
		double pos[3],
		vtkDataSet *pData,
		vtkDataArray *pDataArray,
		double *result);

protected:
	virtual ~VveVtkVortexUtils();

private:
	VveVtkVortexUtils();
	VveVtkVortexUtils(const VveVtkVortexUtils & );
	void operator=(const VveVtkVortexUtils & );
};

#endif

/*============================================================================*/
/*  END OF FILE "VveVtkVortexUtils.h"                                         */
/*============================================================================*/
