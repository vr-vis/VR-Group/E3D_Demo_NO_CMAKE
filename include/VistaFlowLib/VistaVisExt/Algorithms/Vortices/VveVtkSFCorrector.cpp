/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/

#include "VveVtkVortexUtils.h"

#include "VveVtkSFCorrector.h"

#include <VistaVisExt/Tools/VtkPolyDataTools.h>

#include <vtkPlane.h>
#include <vtkCutter.h>
#include <vtkClipPolyData.h>
#include <vtkCone.h>
#include <vtkRungeKutta4.h>
#include <vtkRungeKutta45.h>
#include <vtkInterpolatedVelocityField.h>
#include <vtkPointData.h>
#include <vtkObjectFactory.h>
#include <vtkSelectEnclosedPoints.h>
#include <vtkCell.h>
#include <vtkMath.h>

#include <VistaBase/VistaStreamUtils.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

vtkCxxRevisionMacro(VveVtkSFCorrector, "$Revision: 0.9 $");
vtkStandardNewMacro(VveVtkSFCorrector);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

VveVtkSFCorrector::VveVtkSFCorrector() : 
	m_pInput(0), 
	m_pIntegrator(vtkRungeKutta4::New())
{
	m_pSelectEnclosedPoints = vtkSelectEnclosedPoints::New();

	//	m_pSelectEnclosedPoints->SetSurface(m_pVortexHulls);
	
	if (m_pVortexHulls && m_pVortexHulls->GetNumberOfCells() > 0)
	{
		m_pSelectEnclosedPoints->Initialize(m_pVortexHulls);
	}
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
VveVtkSFCorrector::~VveVtkSFCorrector()
{
	this->SetIntegrator(0);
	m_pSelectEnclosedPoints->Delete();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

double VveVtkSFCorrector::m_dLambda2Threshold = 0;
std::string VveVtkSFCorrector::m_sLambda2FieldName = "lambda2";
std::string VveVtkSFCorrector::m_sGradientLambda2FieldName = "gradientlambda2";
double VveVtkSFCorrector::m_dSamplingStepSize = .00001;
double VveVtkSFCorrector::m_dMaxCorrectionAngle = 20. * 3.14 / 180.;
vtkPolyData *VveVtkSFCorrector::m_pVortexHulls = 0;

void VveVtkSFCorrector::SetIntegrator 
(vtkInitialValueProblemSolver* _arg) 
{
	if (this->m_pIntegrator != _arg) 
	{ 
		vtkInitialValueProblemSolver* tempSGMacroVar = this->m_pIntegrator; 
		this->m_pIntegrator = _arg; 
		if (this->m_pIntegrator) 
		{
			this->m_pIntegrator->Register(this); 
		} 
		if (tempSGMacroVar) 
		{ 
			tempSGMacroVar->UnRegister(this); 
		} 
		this->Modified(); 
	}
} 

//------------------------------------------------------------------------------
// reimplemented virtual function
//------------------------------------------------------------------------------
int VveVtkSFCorrector::ComputeNextStep(double* xprev, double* dxprev, 
										double* xnext, double t, double& delT, 
										double& delTActual, double minStep, 
										double maxStep, double maxError, 
										double& error)
{
	int iErrorCode = m_pIntegrator->ComputeNextStep(xprev, dxprev, xnext, 
		t, delT, delTActual,
		minStep, maxStep, 
		maxError, error);

	double pNewPosCorrected[3];
	if(!iErrorCode)
	{
		// correct data from integrator
		if (Correct(xprev, xnext, pNewPosCorrected))
		{

			vtkInterpolatedVelocityField * pFunctionSet = 
				static_cast<vtkInterpolatedVelocityField*>(
					m_pIntegrator->GetFunctionSet());

			vtkDataSet *pData = pFunctionSet->GetLastDataSet();

			vtkDataArray *pL2Array =
				pData->GetPointData()->GetScalars(m_sLambda2FieldName.c_str());

			double dL2 = 0.;
			bool bValidPoint = VveVtkVortexUtils::Interpolate(
				pNewPosCorrected, 
				pData, 
				pL2Array, 
				&dL2);

			// check if the coreline runs into an already existing vortex
			if (m_pVortexHulls && m_pVortexHulls->GetNumberOfCells() > 0)
			{
				if (m_pSelectEnclosedPoints->IsInsideSurface(pNewPosCorrected))
				{
					// the coreline ran into an existing vortex,
					// so stop growing it
					iErrorCode = vtkInitialValueProblemSolver::OUT_OF_DOMAIN;
				}
			}

			if (bValidPoint)
			{
				// if valid then copy value
				memcpy(xnext, pNewPosCorrected, sizeof(double) * 3);
			}
			else
			{
				// trigger termination of core line creation
				iErrorCode = vtkInitialValueProblemSolver::OUT_OF_DOMAIN;
			}

		}
		else
		{
			iErrorCode = vtkInitialValueProblemSolver::OUT_OF_DOMAIN;
		}
	}

	return iErrorCode;
}

int VveVtkSFCorrector::ComputeNextStep(
	double* xprev,
	double* xnext,
	double t,
	double& delT,
	double maxError,
	double& error)
{
	double minStep = delT;
	double maxStep = delT;
	double delTActual;
	return this->ComputeNextStep(xprev, 0, xnext, t, delT, delTActual,
		minStep, maxStep, maxError, error);
}

int VveVtkSFCorrector::ComputeNextStep(
	double* xprev,
	double* dxprev,
	double* xnext,
	double t,
	double& delT,
	double maxError,
	double& error)
{
	double minStep = delT;
	double maxStep = delT;
	double delTActual;
	return this->ComputeNextStep(xprev, dxprev, xnext, t, delT, delTActual,
		minStep, maxStep, maxError, error);
}

int VveVtkSFCorrector::ComputeNextStep(
	double* xprev,
	double* xnext,
	double t,
	double& delT,
	double& delTActual,
	double minStep,
	double maxStep,
	double maxError,
	double& error )
{
	return this->ComputeNextStep(xprev, 0, xnext, t, delT, delTActual,
		minStep, maxStep, maxError, error);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void VveVtkSFCorrector::SetFunctionSet(vtkFunctionSet *functionset)
{
	if(!m_pIntegrator)
	{
		vstr::errp() << "[VveVtkSFCorrector::SetFunctionSet]: no integrator is set!" << std::endl;
		return;
	}
	m_pIntegrator->SetFunctionSet(functionset);
}

//------------------------------------------------------------------------------
// correct data from vtk integrator
// CurrPos: current position
// NewPos: new position
// NewPosCorrected: corrected new position
//------------------------------------------------------------------------------
bool VveVtkSFCorrector::Correct(
	double *pLastPosition, 
	double *pNewPosition, 
	double *pCorrectedNewPosition)
{
	// get function set
	vtkInterpolatedVelocityField * pFunctionSet
		(static_cast<vtkInterpolatedVelocityField*>(
			m_pIntegrator->GetFunctionSet()));

	if(!pFunctionSet)
	{
		vstr::errp() << "[VveVtkSFCorrector::Correct]: cast into vtkInterpolatedVelocityField failed!" << std::endl;
		return false;
	}

	double d3CorrectedPosition[3];
	d3CorrectedPosition[0] = pNewPosition[0];
	d3CorrectedPosition[1] = pNewPosition[1];
	d3CorrectedPosition[2] = pNewPosition[2];

	pCorrectedNewPosition[0] = d3CorrectedPosition[0];
	pCorrectedNewPosition[1] = d3CorrectedPosition[1];
	pCorrectedNewPosition[2] = d3CorrectedPosition[2];

	double d3CurrentVorticityVector[3];
	pFunctionSet->FunctionValues(pLastPosition, d3CurrentVorticityVector);
	vtkMath::Normalize(d3CurrentVorticityVector);

	vtkDataSet *pData = pFunctionSet->GetLastDataSet();

	vtkDataArray *pL2GradientArray =
		pData->GetPointData()->GetVectors(m_sGradientLambda2FieldName.c_str());

	vtkDataArray *pL2Array =
		pData->GetPointData()->GetScalars(m_sLambda2FieldName.c_str());

	double d3L2Gradient[3];
	double dNormalDotL2Gradient;
	double d3L2GradientVorticityComponent[3];
	double d3L2GradientProjection[3];

	double dL2GradientProjectionLength2 = 0.0;

	int iIterationCount = 0;

	double dL2 = 0., dOldL2 = 0.;

	int iSteps = 0;

	do
	{
		++iSteps;

		// Get Lambda2 gradient at *new* position
		if (VveVtkVortexUtils::Interpolate(
			d3CorrectedPosition, pData, pL2GradientArray, d3L2Gradient))
		{
			// Project the L2-gradient into the plane perpendicular to the 
			// vorticity vector
			// normalize the projection
			vtkMath::Normalize(d3L2Gradient);

			dNormalDotL2Gradient = vtkMath::Dot(
				d3CurrentVorticityVector, d3L2Gradient);

			memcpy(d3L2GradientVorticityComponent, d3CurrentVorticityVector, 
				3*sizeof(double));
			vtkMath::MultiplyScalar(d3L2GradientVorticityComponent, 
				dNormalDotL2Gradient);

			vtkMath::Subtract(d3L2Gradient, 
				d3L2GradientVorticityComponent, d3L2GradientProjection);

			vtkMath::MultiplyScalar(
				d3L2GradientProjection, m_dSamplingStepSize);

			// Subtract the projection of the Lambda2 gradient from the current 
			// position
			vtkMath::Subtract(
				d3CorrectedPosition, 
				d3L2GradientProjection, 
				d3CorrectedPosition);

			if (VveVtkVortexUtils::Interpolate(
				d3CorrectedPosition, 
				pData, 
				pL2Array, 
				&dL2))
			{
				pCorrectedNewPosition[0] = d3CorrectedPosition[0];
				pCorrectedNewPosition[1] = d3CorrectedPosition[1];
				pCorrectedNewPosition[2] = d3CorrectedPosition[2];
			}
			else
			{
				// we ran out of the bounds of the volume
				break;
			}

			// calculate angle of correction and break if it exceeds a given
			// maximum correction angle
			double d3OriginalDirection[3], d3CorrectedDirection[3];
			vtkMath::Subtract(
				pNewPosition, pLastPosition, d3OriginalDirection);
			vtkMath::Subtract(
				pCorrectedNewPosition, pLastPosition, d3CorrectedDirection);
			double dAngle = acos(
				vtkMath::Dot(d3OriginalDirection, d3CorrectedDirection)
				/ 
				(vtkMath::Norm(d3OriginalDirection) 
				* vtkMath::Norm(d3CorrectedDirection)));
			if (dAngle >= m_dMaxCorrectionAngle) break;

			// Only follow the gradient until Lambda2 is increasing again
			if (dOldL2 - dL2 < 0) break;

			dOldL2 = dL2;

			dL2GradientProjectionLength2 = 
				vtkMath::Norm(d3L2GradientProjection);
			dL2GradientProjectionLength2 *= dL2GradientProjectionLength2;
		}
		else
		{
			break;
		}
		// TODO: maximale iterationszahl sinnvoll w�hlen
	} while (++iIterationCount < 5000 
		&& dL2GradientProjectionLength2 > 0.0); 

	return true;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void VveVtkSFCorrector::SetInput(vtkDataSet * pInput)
{
	m_pInput = pInput;
	// Since Vtk 6.* Update() should be called on the generating algorithm.
	// Therefore, the calling function has to take care of that
	//m_pInput->Update();	
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
vtkDataSet * VveVtkSFCorrector::GetInput()
{
	return m_pInput;
}

void VveVtkSFCorrector::SetLambda2Threshold( double dL2Threshold )
{
	m_dLambda2Threshold = dL2Threshold;
}

double VveVtkSFCorrector::GetLambda2Threshold()
{
	return m_dLambda2Threshold;
}

void VveVtkSFCorrector::SetSamplingStepSize( double dSamplingStepSize )
{
	m_dSamplingStepSize = dSamplingStepSize;
}

double VveVtkSFCorrector::GetSamplingStepSize()
{
	return m_dSamplingStepSize;
}

void VveVtkSFCorrector::SetLambda2FieldName( const std::string &sFieldName )
{
	m_sLambda2FieldName = sFieldName;
}

const std::string VveVtkSFCorrector::GetLambda2FieldName()
{
	return m_sLambda2FieldName;
}

void VveVtkSFCorrector::SetGradientLambda2FieldName(
	const std::string &sFieldName)
{
	m_sGradientLambda2FieldName = sFieldName;
}

const std::string VveVtkSFCorrector::GetGradientLambda2FieldName()
{
	return m_sGradientLambda2FieldName;
}

void VveVtkSFCorrector::SetVortexHulls( vtkPolyData *pVortexHulls )
{
	m_pVortexHulls = pVortexHulls;
}

const vtkPolyData* VveVtkSFCorrector::GetVortexHulls()
{
	return m_pVortexHulls;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveVtkSFCorrector.cpp"                                       */
/*============================================================================*/

