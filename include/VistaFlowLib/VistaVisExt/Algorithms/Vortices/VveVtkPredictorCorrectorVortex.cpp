/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: 


/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/

#include "VveVtkPredictorCorrectorVortex.h"

#include "VveVtkExtractLocalExtrema.h"
#include "VveVtkSFCorrector.h"
#include "VveVtkVortexData.h"
#include "VveVtkVortexHullCalculator.h"

#include <VistaBase/VistaTimer.h>
#include <VistaBase/VistaDefaultTimerImp.h>
#include <VistaBase/VistaStreamUtils.h>

#include <vtkStreamTracer.h>
#include <vtkThreshold.h>
#include <vtkUnstructuredGrid.h>
#include <vtkAppendPolyData.h>
#include <vtkSelectEnclosedPoints.h>
#include <vtkPolyDataNormals.h>
#include <vtkTriangleFilter.h>
#include <vtkMassProperties.h>
#include <vtkObjectFactory.h>
#include <vtkMath.h>
#include <vtkPointData.h>

#include <algorithm>
#include <cassert>
#include <sstream>
#include <limits>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

#pragma warning( disable : 4996 )

vtkCxxRevisionMacro(VveVtkPredictorCorrectorVortex, "$Revision: 1.20 $");

#undef Min
#undef Max

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

vtkStandardNewMacro(VveVtkPredictorCorrectorVortex);

VveVtkPredictorCorrectorVortex::VveVtkPredictorCorrectorVortex() : 
	m_pStreamTracer(vtkStreamTracer::New()), 
	m_pSFCorrector(VveVtkSFCorrector::New()), 
	m_pSeedPoints(vtkPolyData::New()),
	m_pUsedSeedPoints(vtkPoints::New()),
	m_pLambda2ThresholdFilter(vtkThreshold::New()),
	m_uiNumOfSamplesPerRingSegment(8), 
	m_dLambda2Threshold(0.), 
	m_iMinCoreLineLength(3),
	m_iNumOfSteps(0),
	m_dMinVortexRadius(0.),
	m_dMinVortexVolume(0.),
	m_pPreviousFrameSeeds(NULL),
	m_dMaxForwardBackwardTracingDistance(std::numeric_limits<double>::max()),
	m_bVerbose(false)
{
	SetNumberOfInputs(1);
	SetNumberOfOutputs(5);

	// init threshold filter for lambda2
	m_pLambda2ThresholdFilter->AllScalarsOff();	
	m_pLambda2ThresholdFilter->ThresholdByLower(0);
	m_pLambda2ThresholdFilter->SetComponentModeToUseSelected();
	m_pLambda2ThresholdFilter->SetSelectedComponent(0);

	// init stream tracer using integrator m_pSFCorrector
	m_pStreamTracer->SetMaximumNumberOfSteps(2500);
	m_pStreamTracer->SetTerminalSpeed(0.0);
	m_pStreamTracer->SetMaximumPropagation(15.0);
	m_pStreamTracer->SetInitialIntegrationStep(0.33);
	m_pStreamTracer->SetComputeVorticity(false);
	m_pStreamTracer->SetIntegrator(m_pSFCorrector);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
VveVtkPredictorCorrectorVortex::~VveVtkPredictorCorrectorVortex()
{
	m_pStreamTracer->Delete();
	m_pSFCorrector->Delete();
	m_pSeedPoints->Delete();
	m_pUsedSeedPoints->Delete();
	m_pLambda2ThresholdFilter->Delete();
	for (size_t i = 0; i < m_vVortexHullCalculators.size(); ++i)
	{
		m_vVortexHullCalculators[i]->Delete();
	}
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

//------------------------------------------------------------------------------
// sets number of samples per segment
//------------------------------------------------------------------------------
void VveVtkPredictorCorrectorVortex::SetNumOfSamplesPerSegment
(const unsigned int iNumOfSamplesPerSegment)
{
	m_uiNumOfSamplesPerRingSegment = iNumOfSamplesPerSegment;
}

//------------------------------------------------------------------------------
// sets lambda2 threshold for seed extraction
//------------------------------------------------------------------------------
void VveVtkPredictorCorrectorVortex::SetLambda2Threshold
(float fInitialLambda2Threshold)
{
	m_dLambda2Threshold = fInitialLambda2Threshold;
}

//------------------------------------------------------------------------------
// sets minimum core line length to filter too short core lines
// (core line length is given in number of points the line contains)
//------------------------------------------------------------------------------
void VveVtkPredictorCorrectorVortex::SetMinCoreLineLength(int MinLength)
{
	m_iMinCoreLineLength = MinLength;
	if(m_iMinCoreLineLength < 2)
	{
		m_iMinCoreLineLength = 2;
	}
}

//------------------------------------------------------------------------------
// compute lambda2 sorted seeds using VveVtkExtractLocalExtrema
//------------------------------------------------------------------------------
void VveVtkPredictorCorrectorVortex::ComputeSeeds
(vtkDataSet * pData, double dLambda2Threshold)
{
	vstr::outi() << "Extracting seed points";

	vtkThreshold *pL2Threshold = vtkThreshold::New();

	pL2Threshold->SetInput(pData);
	pL2Threshold->AllScalarsOff();	
	pL2Threshold->ThresholdByLower(dLambda2Threshold);
	pL2Threshold->SetComponentModeToUseSelected();
	pL2Threshold->Update();

	// extract local extrema from lambd2 scalar field
	VveVtkExtractLocalExtrema * pExtrLocalExtrema
		(VveVtkExtractLocalExtrema::New());

	pExtrLocalExtrema->SetInput(pL2Threshold->GetOutput());
	pExtrLocalExtrema->SetFieldName(m_sLambda2FieldName);
	pExtrLocalExtrema->SetExtractMinima();
	pExtrLocalExtrema->Update();

	vtkPolyData * pGrid = pExtrLocalExtrema->GetOutput();

	// sort seeds
	std::pair<int,double> oIdAndValue;
 	std::vector< std::pair<int,double> > vSeeds;

	const vtkIdType NumOfPoints = pGrid->GetPoints()->GetNumberOfPoints();

	for(int iIdx=0; iIdx<NumOfPoints; iIdx++)
	{
		oIdAndValue = std::pair<int,double>
			(iIdx, 
			*pGrid->GetPointData()->GetScalars(m_sLambda2FieldName.c_str())
				->GetTuple(iIdx));
		vSeeds.push_back(oIdAndValue);		
	}

	std::sort(vSeeds.begin(), vSeeds.end(), 
		VveVtkVortexUtils::less_pair_second<int, double>());

	vtkPoints * pPoints(vtkPoints::New());
	pPoints->SetNumberOfPoints(NumOfPoints);

	int iIdx = 0;

	for(std::vector< std::pair<int,double> >::const_iterator 
		cit = vSeeds.begin(); cit != vSeeds.end(); ++cit)
	{
		oIdAndValue = *cit;

		int iID = oIdAndValue.first;
		double dPoint[3];
		pGrid->GetPoints()->GetPoint(iID, dPoint);

		pPoints->SetPoint(iIdx, dPoint);		

		++iIdx;
	}

	m_pSeedPoints->SetPoints(pPoints);

	vstr::out() << ": " << NumOfPoints << " seeds extracted" << std::endl;

	pExtrLocalExtrema->Delete();
	pL2Threshold->Delete();
	pPoints->Delete();
}

//------------------------------------------------------------------------------
// trace next core line from a seed point
//------------------------------------------------------------------------------
void VveVtkPredictorCorrectorVortex::TraceSeed
(vtkDataSet * pData, double * pSeedPoint, vtkPolyData * pCoreLine)
{
	// this is done by using two stream tracers, one for forward, one for 
	// backward (instead of using ->SetIntegrationDirectionToBoth) because this
	// way it can be made sure that the points on the line are ordered, what is
	// needed for the creation of the vortex hull
	m_pStreamTracer->SetInput(pData);
	m_pStreamTracer->SetStartPosition(pSeedPoint);
	m_pStreamTracer->SetIntegrationDirectionToBackward();

	// forward tracing
	vtkPolyData * pCoreLineBackward(vtkPolyData::New());
	m_pStreamTracer->Update();
	pCoreLineBackward->DeepCopy(m_pStreamTracer->GetOutput());

	// backward tracing
	vtkPolyData * pCoreLineForward(vtkPolyData::New());
	m_pStreamTracer->SetIntegrationDirectionToForward();
	m_pStreamTracer->Update();	
	pCoreLineForward->ShallowCopy(m_pStreamTracer->GetOutput());

	// append forward and backward CoreLines
	VveVtkVortexUtils::AppendStreamLines
		(pCoreLine, pCoreLineForward, pCoreLineBackward);

	pCoreLineForward->Delete();
	pCoreLineBackward->Delete();
}

//------------------------------------------------------------------------------
// Main filter function
//------------------------------------------------------------------------------
void VveVtkPredictorCorrectorVortex::Execute()
{	
	vtkPolyData * pOutput = this->GetOutput();
	vtkDataSet * pData = this->GetInput();

	for (size_t i = 0; i < m_vVortexHullCalculators.size(); ++i)
	{
		m_vVortexHullCalculators[i]->Delete();
	}
	m_vVortexHullCalculators.clear();

	vtkAppendPolyData *pCompleteHullData = vtkAppendPolyData::New();
	vtkAppendPolyData *pCompleteLineData = vtkAppendPolyData::New();
	vtkAppendPolyData *pCompleteSkeletonData = vtkAppendPolyData::New();
	vtkAppendPolyData *pCompleteSmallSkeletonData = vtkAppendPolyData::New();

	VistaDefaultTimerImp *pTimerImp = new VistaDefaultTimerImp();
	VistaTimer *pTimer = new VistaTimer(pTimerImp);

	double dStartTime = pTimer->GetMicroTime();

	m_pLambda2ThresholdFilter->ThresholdByLower(0.);
	m_pLambda2ThresholdFilter->SetInput(pData);
	m_pLambda2ThresholdFilter->Update();

	// Set up the corrector initial problem solver
	VveVtkSFCorrector::SetLambda2Threshold(m_dLambda2Threshold);
	VveVtkSFCorrector::SetLambda2FieldName(m_sLambda2FieldName);
	VveVtkSFCorrector::SetGradientLambda2FieldName(
		m_sGradientLambda2FieldName);
	VveVtkSFCorrector::SetSamplingStepSize(
		m_dCorelineCorrectionSamplingStepSize);
	VveVtkSFCorrector::SetVortexHulls(pCompleteHullData->GetOutput());

	ComputeSeeds(m_pLambda2ThresholdFilter->GetOutput(), m_dLambda2Threshold);

	int iNumOfInitialSeeds = m_pSeedPoints->GetNumberOfPoints();

	if (m_pPreviousFrameSeeds)
	{
		// if a file with seed points from a previous frame is given,
		// those seeds, located near to previously used seeds are traced
		// early, to have a good chance to extract large vortices early and
		// by that speed up the whole calculation process
		ResortSeedsBasedOnPreviousFrame();
	}

	double pSeedPoint[3];

	m_iNumOfSteps = 0;
	size_t uiKilledSeeds = 0;
	double dMinVortexVolume = std::numeric_limits<double>::max(), 
		dMaxVortexVolume = 0.0;

	int iCounter = 0;

	vstr::outi() << "Generating Vortex Hulls" << std::endl;

	// loop while seeds left and requested number of steps not finished
	while(m_pSeedPoints->GetNumberOfPoints() > 0)
	{		

		PrintProgress(
			(iNumOfInitialSeeds - 
			m_pSeedPoints->GetPoints()->GetNumberOfPoints()) /
			static_cast<double>(iNumOfInitialSeeds) * 100.);

		vtkPolyData *pCoreLine = vtkPolyData::New();
		m_pSeedPoints->GetPoints()->GetPoint(0, pSeedPoint);

		bool bSeedPointOK = 
			ForwardBackwardTestSingleSeedPoint(
				m_pLambda2ThresholdFilter->GetOutput(), pSeedPoint);

		if (bSeedPointOK)
		{
			PrintVerbose("Found good seed, tracing it.");

			// Tracing, Compute CoreLine
			TraceSeed(m_pLambda2ThresholdFilter->GetOutput(), 
				pSeedPoint, pCoreLine);

			// Check for CoreLineLength
			if (pCoreLine->GetNumberOfPoints() > m_iMinCoreLineLength)
			{
				std::ostringstream s;
				s << "Seed traced, length: " <<
					pCoreLine->GetNumberOfPoints() <<
					". Meshing it";
				PrintVerbose(s.str());

				// compute vortex hull
				VveVtkVortexHullCalculator * pComputeVH = 
					VveVtkVortexHullCalculator::New();
				pComputeVH->SetInput(pCoreLine);
				pComputeVH->SetData(m_pLambda2ThresholdFilter->GetOutput());
				pComputeVH->SetMinRadius(m_dMinVortexRadius);
				pComputeVH->SetHullSamplingStepSize(m_dHullSamplingStepSize);
				pComputeVH->SetNumOfSamplesPerSegment(
					m_uiNumOfSamplesPerRingSegment);
				pComputeVH->SetLambda2Threshold(m_dLambda2Threshold);
				pComputeVH->SetLambda2FieldName(m_sLambda2FieldName);
				pComputeVH->SetVorticityFieldName(m_sVorticityFieldName);
				pComputeVH->SetHullSamplingEpsilon(m_dHullSamplingL2Epsilon);
				pComputeVH->SetSmoothVortex(m_bSmoothVortices);
				pComputeVH->SetRemoveSelfIntersections(
					m_bRemoveSelfIntersections);

				pComputeVH->Update();


				// test if the reduced core line still has the minimum length
				bool bCorelineHasMinimumLength =
					pComputeVH->GetOutput(1)->GetNumberOfPoints() 
					>= m_iMinCoreLineLength;

				if (bCorelineHasMinimumLength)
				{
					// don't consider vortices that only have the minimum 
					// radius in all ring segments
					bool bAllRadiiMinimum = AreAllRadiiMinimum(pComputeVH);

					if (!bAllRadiiMinimum)
					{
						// test if the vortex has at least the minimum volume
						double dVortexVolume = CalculateVortexVolume(
							pComputeVH->GetOutput());

 						if (dVortexVolume >= m_dMinVortexVolume)
						{
							++iCounter;

							pCompleteHullData->AddInput(
								pComputeVH->GetOutput(0));
							pCompleteLineData->AddInput(
								pComputeVH->GetOutput(1));
							pCompleteSkeletonData->
								AddInput(pComputeVH->GetOutput(2));
							pCompleteSmallSkeletonData->
								AddInput(pComputeVH->GetOutput(3));

							// updating hull data is important for seed point 
							// elimination and core line termination!!
							pCompleteHullData->Update();
							pCompleteLineData->Update();
							pCompleteSkeletonData->Update();
							pCompleteSmallSkeletonData->Update();
							
	// 						static int iFileCounter = 0;
	// 
	// 						VveVtkVortexUtils::Save(
	// 							pComputeVH->GetOutput(0),
	// 							"c:/tmp/Hulls_",
	// 							iFileCounter);
	// 						
	// 						VveVtkVortexUtils::Save(
	// 							pComputeVH->GetOutput(1),
	// 							"c:/tmp/Lines_",
	// 							iFileCounter);				
	// 
	// 						VveVtkVortexUtils::Save(
	// 							pComputeVH->GetOutput(2),
	// 							"c:/tmp/Skeletons_",
	// 							iFileCounter);
	// 
	// 						VveVtkVortexUtils::Save(
	// 							pComputeVH->GetOutput(3),
	// 							"c:/tmp/SmallSkeletons_",
	// 							iFileCounter);
	// 
	// 						++iFileCounter;

							m_pUsedSeedPoints->InsertNextPoint(pSeedPoint);

							size_t uiKilledSeedsNow = 
								EliminateSeedPointsInVortex(
									m_pSeedPoints, pComputeVH->GetOutput());
							uiKilledSeeds += uiKilledSeedsNow;
						}
					}
				}
				m_vVortexHullCalculators.push_back(pComputeVH);
			}
		}

		VveVtkVortexUtils::DeleteNthPoint(m_pSeedPoints, 0);

		pCoreLine->Delete();
	}

	PrintProgress(
		(iNumOfInitialSeeds - 
		m_pSeedPoints->GetPoints()->GetNumberOfPoints()) /
		static_cast<double>(iNumOfInitialSeeds) * 100.);

	GetOutput(0)->DeepCopy(pCompleteHullData->GetOutput());
	GetOutput(1)->DeepCopy(pCompleteLineData->GetOutput());
	GetOutput(2)->DeepCopy(pCompleteSkeletonData->GetOutput());
	GetOutput(3)->SetPoints(m_pUsedSeedPoints);
	GetOutput(4)->DeepCopy(pCompleteSmallSkeletonData->GetOutput());

	vstr::outi() << std::endl << "Number of vortices: " << iCounter << std::endl;
	vstr::outi() << "Vortex volumina:" << std::endl;
	vstr::outi() << "Min: " << dMinVortexVolume 
		<< ", Max: " << dMaxVortexVolume << std::endl;

	vstr::outi() << "Calculation time: " << 
		(pTimer->GetMicroTime() - dStartTime) << "s" << std::endl;

	pCompleteHullData->Delete();
	pCompleteLineData->Delete();
	pCompleteSkeletonData->Delete();
	pCompleteSmallSkeletonData->Delete();
}

// returns the number of eliminated seed points
size_t VveVtkPredictorCorrectorVortex::EliminateSeedPointsInVortex(
	vtkPolyData *pSeedPoints, vtkPolyData *pVortexHull)
{
	vtkSelectEnclosedPoints *pSelectEnclosedPoints = 
		vtkSelectEnclosedPoints::New();

	pSelectEnclosedPoints->SetInput(pSeedPoints);
	pSelectEnclosedPoints->SetSurface(pVortexHull);
	pSelectEnclosedPoints->CheckSurfaceOn();
	pSelectEnclosedPoints->Update();

	vtkIdType iIndex = 0;

	vtkPolyData *pNewSeedPoints = vtkPolyData::New();
	vtkPoints *pNewPointsSet = vtkPoints::New();
	pNewSeedPoints->SetPoints(pNewPointsSet);
	pNewPointsSet->Delete();

	double d3SeedPoint[3];

	vtkIdType iNewSeedsIndex = 0;

	size_t uiEliminatedSeeds = 0;

	for (vtkIdType iIndex = 0; 
		iIndex < pSeedPoints->GetNumberOfPoints();
		++iIndex)
	{
		if (!pSelectEnclosedPoints->IsInside(iIndex))
		{
			pSeedPoints->GetPoint(iIndex, d3SeedPoint);
			pNewSeedPoints->GetPoints()->InsertPoint(
				iNewSeedsIndex, d3SeedPoint);
			++iNewSeedsIndex;
		}
		else
		{
			++uiEliminatedSeeds;
		}
	}
	
	pSeedPoints->DeepCopy(pNewSeedPoints);

	pNewSeedPoints->Delete();

	return uiEliminatedSeeds;
}

double VveVtkPredictorCorrectorVortex::CalculateVortexVolume(
	vtkPolyData *pData)
{
	vtkTriangleFilter *pTriangles = vtkTriangleFilter::New();

	pTriangles->SetInput(pData);

	vtkMassProperties *pMassProperties = vtkMassProperties::New();
	pMassProperties->SetInput(pTriangles->GetOutput());

	pMassProperties->Update();

	double dVolume = pMassProperties->GetVolume();

	pMassProperties->Delete();
	pTriangles->Delete();

	return dVolume;
}

double VveVtkPredictorCorrectorVortex::GetForwardBackwardTracingDistance(
	vtkDataSet *pData,
	vtkStreamTracer *pStreamTracer,
	double pStartPoint[3],
	vtkIdType &iNumIntegrations,
	bool bDoForwardIntegration)
{
	double d3EndPoint[3], d3BacktracedPoint[3], d3Distance[3];


	vtkIdType iMaxNumSteps = 
		pStreamTracer->GetMaximumNumberOfSteps();

	if (bDoForwardIntegration)
	{
		pStreamTracer->SetIntegrationDirectionToForward();
	}
	else
	{
		pStreamTracer->SetIntegrationDirectionToBackward();
	}
	pStreamTracer->SetStartPosition(pStartPoint);
	pStreamTracer->Update();
	vtkIdType iNumPoints = 
		pStreamTracer->GetOutput()->GetPoints()->GetNumberOfPoints();
	if (iNumPoints > 1)
	{
		pStreamTracer->GetOutput()->GetPoints()->GetPoint(
			iNumPoints - 1,
			d3EndPoint);
	}
	else
	{
		iNumIntegrations = 0;
		return 0.;
	}

	iNumIntegrations = iNumPoints - 1;

	if (bDoForwardIntegration)
	{
		pStreamTracer->SetIntegrationDirectionToBackward();
	}
	else
	{
		pStreamTracer->SetIntegrationDirectionToForward();		
	}
	pStreamTracer->SetMaximumNumberOfSteps(iNumIntegrations);
	pStreamTracer->SetStartPosition(d3EndPoint);
	pStreamTracer->Update();	
	iNumPoints = pStreamTracer->GetOutput()->GetPoints()->GetNumberOfPoints();
	if (iNumPoints > 1)
	{
		pStreamTracer->GetOutput()->GetPoints()->GetPoint(
			iNumPoints - 1,
			d3BacktracedPoint);
	}
	else
	{
		pStreamTracer->SetMaximumNumberOfSteps(iMaxNumSteps);
		return std::numeric_limits<double>::max();
	}

	pStreamTracer->SetMaximumNumberOfSteps(iMaxNumSteps);

	vtkMath::Subtract(d3BacktracedPoint, pStartPoint, d3Distance);

	return vtkMath::Norm(d3Distance);
}

bool VveVtkPredictorCorrectorVortex::
	ForwardBackwardTestSingleSeedPoint(
		vtkDataSet *pData,
		double d3SeedPoint[3])
{
	vtkPoints *pSeedPoints = m_pSeedPoints->GetPoints();

	vtkStreamTracer *pStreamTracer = vtkStreamTracer::New();

	vtkIdType iMaxSteps = 10;

	pStreamTracer->SetMaximumNumberOfSteps(iMaxSteps);
	pStreamTracer->SetTerminalSpeed(0.0);
	pStreamTracer->SetMaximumPropagation(15.0);
	pStreamTracer->SetInitialIntegrationStep(0.33);
	pStreamTracer->SetComputeVorticity(false);
	pStreamTracer->SetIntegrator(m_pSFCorrector);
	pStreamTracer->SetInput(pData);

	vtkIdType iTotalIntegrations = 0;
	vtkIdType iNumIntgerations = 0;

	double dDistanceForward = GetForwardBackwardTracingDistance(
		pData,
		pStreamTracer,
		d3SeedPoint,
		iNumIntgerations,
		true);

	iTotalIntegrations += iNumIntgerations;

	double dDistanceBackward = GetForwardBackwardTracingDistance(
		pData,
		pStreamTracer,
		d3SeedPoint,
		iNumIntgerations,
		false);

	iTotalIntegrations += iNumIntgerations;

	double dDistanceMean = (dDistanceForward + dDistanceBackward) * .5;

	bool bReturnValue;

	if (iTotalIntegrations > 0 
		&& dDistanceMean < m_dMaxForwardBackwardTracingDistance
		&& iTotalIntegrations >= 
		std::min(
			static_cast<int>(2 * iMaxSteps),
			static_cast<int>(m_iMinCoreLineLength - 1) ))
	{
		bReturnValue = true;
	}
	else
	{
		bReturnValue = false;
	}

	pStreamTracer->Delete();

	return bReturnValue;
}

// takes all seeds of the previous frame and places seeds located near to them
// at the beginning of the seed list to grow their vortices first
// (hopefully this reduces jitter in the animation of the core lines)
void VveVtkPredictorCorrectorVortex::
ResortSeedsBasedOnPreviousFrame()
{
	vstr::outi() << "Resorting Seeds based on previous frame seed points";

	vtkDataSet *pPreviousSeeds = m_pPreviousFrameSeeds;
	vtkPoints *pSeeds = m_pSeedPoints->GetPoints();
	vtkPoints *pResortedSeeds = vtkPoints::New();

	vtkIdType iNumPreviousFrameSeeds = pPreviousSeeds->GetNumberOfPoints();
	vtkIdType iNumSeeds = pSeeds->GetNumberOfPoints();

	double d3PreviousFrameSeed[3], d3Seed[3], d3Tmp[3];

	bool *pPointAvailable = new bool[iNumSeeds];
	for (int i = 0; i < iNumSeeds; i++)
	{
		pPointAvailable[i] = true;
	}

	for (vtkIdType i = 0; i < iNumPreviousFrameSeeds; ++i)
	{
		pPreviousSeeds->GetPoint(i, d3PreviousFrameSeed);

		vtkIdType iClosestPoint = 0;
		double dSmallestDistance = std::numeric_limits<double>::max();

		for (vtkIdType j = 0; j < iNumSeeds; ++j)
		{
			if (!pPointAvailable[j]) continue;

			pSeeds->GetPoint(j, d3Seed);

			vtkMath::Subtract(d3Seed, d3PreviousFrameSeed, d3Tmp);

			double dDistance = vtkMath::Norm(d3Tmp);

			if (dDistance < dSmallestDistance)
			{
				dSmallestDistance = dDistance;
				iClosestPoint = j;
			}
		}

		pSeeds->GetPoint(iClosestPoint, d3Tmp);
		pResortedSeeds->InsertNextPoint(d3Tmp);
		pPointAvailable[iClosestPoint] = false;
	}

	for (vtkIdType i = 0; i < iNumSeeds; ++i)
	{
		if (!pPointAvailable[i]) continue;
		pSeeds->GetPoint(i, d3Tmp);
		pResortedSeeds->InsertNextPoint(d3Tmp);
	}

	pSeeds->DeepCopy(pResortedSeeds);

	pResortedSeeds->Delete();
	delete [] pPointAvailable;

	vstr::out() << "... done" << std::endl;
}

VveVtkVortexData * VveVtkPredictorCorrectorVortex::GetVortex( int iId )
{
	return m_vVortexHullCalculators[iId]->GetVortexData();
}

size_t VveVtkPredictorCorrectorVortex::GetNumberOfVortices() const
{
	return m_vVortexHullCalculators.size();
}

float VveVtkPredictorCorrectorVortex::GetSmallSkeletonRadiusMultiplier()
{
	return m_fSmallSkeletonRadiusMultiplier;
}

void VveVtkPredictorCorrectorVortex::SetLambda2FieldName(
	const std::string &sFieldName )
{
	m_sLambda2FieldName = sFieldName;
	Modified();
}

const std::string VveVtkPredictorCorrectorVortex::GetLambda2FieldName() const
{
	return m_sLambda2FieldName;
}

void VveVtkPredictorCorrectorVortex::SetVorticityFieldName(
	const std::string &sFieldName)
{
	m_sVorticityFieldName = sFieldName;
	Modified();
}

const std::string VveVtkPredictorCorrectorVortex::GetVorticityFieldName() const
{
	return m_sVorticityFieldName;
}

void VveVtkPredictorCorrectorVortex::SetLambda2GradientFieldName(
	const std::string &sFieldName)
{
	m_sGradientLambda2FieldName = sFieldName;
	Modified();
}

const std::string VveVtkPredictorCorrectorVortex::
	GetLambda2GradientFieldName() const
{
	return m_sGradientLambda2FieldName;
}

void VveVtkPredictorCorrectorVortex::SetMinVortexRadius( const double dRadius )
{
	m_dMinVortexRadius = dRadius;
	Modified();
}

double VveVtkPredictorCorrectorVortex::GetMinVortexRadius() const
{
	return m_dMinVortexRadius;
}

void VveVtkPredictorCorrectorVortex::SetCorelineCorrectionSamplingStepSize(
	const double dStep)
{
	m_dCorelineCorrectionSamplingStepSize = dStep;
	Modified();
}

double VveVtkPredictorCorrectorVortex::
	GetCorelineCorrectionSamplingStepSize() const
{
	return m_dCorelineCorrectionSamplingStepSize;
}

void VveVtkPredictorCorrectorVortex::SetHullSamplingStepSize(
	const double dHullSamplingStepSize)
{
	m_dHullSamplingStepSize = dHullSamplingStepSize;
	Modified();
}

double VveVtkPredictorCorrectorVortex::GetHullSamplingStepSize() const
{
	return m_dHullSamplingStepSize;
}

void VveVtkPredictorCorrectorVortex::SetMinVortexVolume(
	const double dMinVortexVolume)
{
	m_dMinVortexVolume = dMinVortexVolume;
	Modified();
}

double VveVtkPredictorCorrectorVortex::GetMinVortexVolume() const
{
	return m_dMinVortexVolume;
}

void VveVtkPredictorCorrectorVortex::SetPreviousFrameSeeds(vtkDataSet *pSeeds)
{
	m_pPreviousFrameSeeds = pSeeds;
	Modified();
}

vtkDataSet * VveVtkPredictorCorrectorVortex::GetPreviousFrameSeeds() const
{
	return m_pPreviousFrameSeeds;
}

void VveVtkPredictorCorrectorVortex::SetSmallSkeletonRadiusMultiplier( 
	float fMultiplier )
{
	m_fSmallSkeletonRadiusMultiplier = fMultiplier;
	Modified();
}

void VveVtkPredictorCorrectorVortex::PrintProgress( double dPercentage )
{
	static unsigned int iLastPrintLenght = 0;
	for (unsigned int i = 0; i < iLastPrintLenght; ++i)
	{
		vstr::out() << "\b";
	}
	for (unsigned int i = 0; i < iLastPrintLenght; ++i)
	{
		vstr::out() << " ";
	}
	for (unsigned int i = 0; i < iLastPrintLenght; ++i)
	{
		vstr::out() << "\b";
	}
	std::stringstream sStream;
	sStream << dPercentage << "% done. ";
	sStream << m_vVortexHullCalculators.size() << " hulls extracted. ";
	sStream << m_pSeedPoints->GetNumberOfPoints() << " seeds left.";
	std::string strOutput = sStream.str();
	vstr::out() << strOutput;
	iLastPrintLenght = static_cast<unsigned int>(strOutput.length());
}

bool VveVtkPredictorCorrectorVortex::AreAllRadiiMinimum(
	VveVtkVortexHullCalculator * pComputeVH )
{
	bool bAllRadiiMinimum = false;
	VveVtkVortexData *pVortexData = pComputeVH->GetVortexData();
	unsigned int iNumRings = pVortexData->GetNumberOfRings();
	for (unsigned int i = 0; i < iNumRings; ++i)
	{
		VveVtkVortexData::CVortexRingData *pRing = 
			pVortexData->GetVortexRingData(i);
		unsigned int iNumSegments = 
			pVortexData->GetNumberOfSegments();
		for (unsigned int j = 0; j < iNumSegments; ++j)
		{
			if (pRing->m_pRadii[j] > m_dMinVortexRadius * 1.0001)
			{
				bAllRadiiMinimum = false;
				break;
			}
		}
		if (!bAllRadiiMinimum) break;
	}
	return bAllRadiiMinimum;
}

unsigned int VveVtkPredictorCorrectorVortex::
	GetNumberOfSamplesPerRingSegment() const
{
	return m_uiNumOfSamplesPerRingSegment;
}

void VveVtkPredictorCorrectorVortex::SetMaxForwardBackwardTracingDistance(
	double val)
{
	Modified();
	m_dMaxForwardBackwardTracingDistance = val;
}

double VveVtkPredictorCorrectorVortex::
	GetMaxForwardBackwardTracingDistance() const
{
	return m_dMaxForwardBackwardTracingDistance;
}

double VveVtkPredictorCorrectorVortex::GetHullSamplingL2Epsilon() const
{
	return m_dHullSamplingL2Epsilon;
}

void VveVtkPredictorCorrectorVortex::SetHullSamplingL2Epsilon( double val )
{
	m_dHullSamplingL2Epsilon = val;
}

bool VveVtkPredictorCorrectorVortex::GetVerbose() const
{
	return m_bVerbose;
}

void VveVtkPredictorCorrectorVortex::SetVerbose( bool val )
{
	m_bVerbose = val;
}

void VveVtkPredictorCorrectorVortex::PrintVerbose( std::string str )
{
	if (m_bVerbose)
	{
		vstr::outi() << "\n" << str << "\n" << std::endl;
	}
}

bool VveVtkPredictorCorrectorVortex::GetRemoveSelfIntersections() const
{
	return m_bRemoveSelfIntersections;
}

void VveVtkPredictorCorrectorVortex::SetRemoveSelfIntersections( bool val )
{
	m_bRemoveSelfIntersections = val;
}

bool VveVtkPredictorCorrectorVortex::GetSmoothVortices() const
{
	return m_bSmoothVortices;
}

void VveVtkPredictorCorrectorVortex::SetSmoothVortices( bool val )
{
	m_bSmoothVortices = val;
}

float VveVtkPredictorCorrectorVortex::GetLambda2Threshold()
{
	return m_dLambda2Threshold;
}

int VveVtkPredictorCorrectorVortex::GetMinCoreLineLength()
{
	return m_iMinCoreLineLength;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveVtkPredictorCorrectorVortex.cpp"                          */
/*============================================================================*/

