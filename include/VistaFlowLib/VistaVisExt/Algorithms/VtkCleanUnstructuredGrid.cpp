/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VtkCleanUnstructuredGrid.h"

#include <vtkUnstructuredGrid.h>

#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkFloatArray.h>
#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkCell.h>

#include <vtkObjectFactory.h>
#include <vtkPointLocator.h>
#include <vtkIdTypeArray.h>

#include <vtkInformation.h>
#include <vtkInformationVector.h>

#include <VistaBase/VistaStreamUtils.h>


#include <string>
#include <vector>
#include <algorithm>

using namespace std;
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
vtkCxxRevisionMacro(VtkCleanUnstructuredGrid, "$Id$");
vtkStandardNewMacro(VtkCleanUnstructuredGrid);
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RequestData                                                 */
/*                                                                            */
/*============================================================================*/
int VtkCleanUnstructuredGrid::RequestData(vtkInformation *, 
										   vtkInformationVector **pInputInfo, 
										   vtkInformationVector *pOutputInfo)
{
	//get the info objects
	//our input is the first input on the first port of this filter
	vtkInformation *inInfo = pInputInfo[0]->GetInformationObject(0);
	vtkInformation *outInfo = pOutputInfo->GetInformationObject(0);

	vtkUnstructuredGrid *pInput = vtkUnstructuredGrid::SafeDownCast(
									inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkUnstructuredGrid *pOutput = vtkUnstructuredGrid::SafeDownCast(
									outInfo->Get(vtkDataObject::DATA_OBJECT()));

	if(!pInput || !pOutput || pInput->GetNumberOfPoints() == 0)
	{
		vstr::out() << endl << endl;
		vstr::errp() << "[VtkCleanUnstructuredGrid::ExecuteData]: No valid input provided - ABORTING!" << endl << endl;
		return 0;
	}

	/*
	* 1) filter points and remember new ids
	*/
	vtkPoints *pOutPts = vtkPoints::New();
	pOutPts->Allocate(pInput->GetNumberOfPoints());
	
	vtkPointData *pOutPD = pOutput->GetPointData();
	pOutPD->CopyAllocate(pInput->GetPointData());
	
	vtkIdList *pIdList = vtkIdList::New();

	//simply use a point locator with the given epsilon tolerance to remove duplicate points!
	vtkPointLocator *pLocator = vtkPointLocator::New();
	pLocator->SetTolerance(m_fEpsilon);
	pLocator->InitPointInsertion(pOutPts,pInput->GetBounds());
	
	vtkIdType iCurrentPt = 0;
	//so this will keep the mapping M: {input pt ids} |-> {output pt ids} <= { input pt ids}
	int *iPointIdMapping = new int[pInput->GetNumberOfPoints()];

	for(vtkIdType i=0; i<pInput->GetNumberOfPoints(); ++i)
	{
		pInput->GetPointCells(i,pIdList);
		//if there's no cells -> leave point out
		if(pIdList->GetNumberOfIds() == 0)
		{
			iPointIdMapping[i] = -1;
			continue;
		}
		//eliminate duplicate points 
		pLocator->InsertUniquePoint(pInput->GetPoint(i),iCurrentPt);
		pOutPD->CopyData(pInput->GetPointData(),i,iCurrentPt);
		//remember this to be able to replace the point id correctly in the cell array
		iPointIdMapping[i] = iCurrentPt;
	}
	pOutput->SetPoints(pOutPts);
	pOutPts->Delete();
	pLocator->Delete();
	/*
	* 2) copy cell array with permuted point ids
	* 
	* @todo integrate degenerate cell removal here!
	*/
	vtkCellArray *pOutCells = vtkCellArray::New();
	vtkCellData  *pOutCD = pOutput->GetCellData();
	pOutCD->CopyAllocate(pInput->GetCellData());

	pOutCells->Allocate(pInput->GetNumberOfCells());
	int *iCellTypes = new int[pInput->GetNumberOfCells()];
	for(vtkIdType i=0; i<pInput->GetNumberOfCells(); ++i)
	{
		pInput->GetCellPoints(i,pIdList);
		for(vtkIdType j=0; j<pIdList->GetNumberOfIds(); ++j)
			pIdList->SetId(j,iPointIdMapping[pIdList->GetId(j)]);
		pOutCells->InsertNextCell(pIdList);
		iCellTypes[i] = pInput->GetCell(i)->GetCellType();
		//this is an identity mapping currently -> change if implementing degenerate cell removal!
		pOutCD->CopyData(pInput->GetCellData(), i, i);
	}
	pOutput->SetCells(iCellTypes,pOutCells);
	delete [] iCellTypes;
	delete [] iPointIdMapping;
	pOutCells->Delete();
	pIdList->Delete();
	
	pOutput->Squeeze();
	return 1;
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VtkPolyDataTools.cpp"                                        */
/*============================================================================*/
