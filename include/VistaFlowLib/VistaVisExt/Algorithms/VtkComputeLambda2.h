/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkComputeLambda2.h          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  ViSTA VtkExt                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - vtkComputeLambda2    :                                            */
/*                                                                            */
/*============================================================================*/
#ifndef _VTKCOMPUTELAMBDA2_H
#define _VTKCOMPUTELAMBDA2_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkDataSetAlgorithm.h>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* This is a generic vtkDataSetAlgorithm which computes the lambda2 value of a given 
* input vector field. In theory the isosurface L2=0 defines vortex hulls. For more 
* details see JEONG, HUSSAIN: "On the identification of a vortex", Journal of Fluid 
* Mechanics, vol. 285, pp. 69-94, 1995.
*
* NOTE: If enabled by the compiler, the filter internally uses OpenMP loop parallelization 
* in order to execute faster on multi-core/multi-processor smp systems.
*
* @author   Bernd Hentschel
* @date     June, 2006
*
*/
class VISTAVISEXTAPI vtkComputeLambda2 : public vtkDataSetAlgorithm
{
public:
	vtkTypeRevisionMacro(vtkComputeLambda2,vtkDataSetAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

	static vtkComputeLambda2 *New();

	/**
	* Choose whether to compute point data or cell data.
	* Normally, point data will be the standard data attribute type handled by
	* a downstream pipeline. However, using cell data as output will circumvent one
	* additional interpolation step and thus yield more precise results in less time.
	* Default is ComputePointDataOn
	*/
	void ComputePointDataOn();
	void ComputePointDataOff();
	bool GetComputePointData() const;

	/**
	* Get/Set the name of the output data field (either point or cell data)
	* The default name is lambda2. If this filter's input already has a data
	* field named like m_strOutputFieldName, execution will be terminated right away.
	*/
	void SetOutputFieldName(const std::string& strFieldName);
	std::string GetOutputFieldName() const;
protected:
	vtkComputeLambda2();
	virtual ~vtkComputeLambda2();

	/**
	* this is where the real work is done.
	*/
	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

private:
	std::string m_strOutputFieldName;
	bool m_bComputePointData;
};


#endif //#ifdef _VTKCOMPUTELAMBDA2_H

