/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VvePathlineTracer.h"

#include "VveInstatInterpolator.h"

#include "../Data/VveDiscreteDataTyped.h"
#include "../Tools/VveTimeMapper.h"

#include <VistaBase/VistaVectorMath.h>
#include <VistaBase/VistaStreamUtils.h>

#include <vtkPointSet.h>
#include <vtkPoints.h>
#include <vtkDataSet.h>
#include <vtkPolyData.h>
#include <vtkDoubleArray.h>
#include <vtkIntArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkRungeKutta4.h>
#include <vtkFloatArray.h>
#include <vtkCharArray.h>
#include <vtkMath.h>

#include <string>
#include <cassert>
#include "VistaVisExt/Data/VveParticleTrajectory.h"
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VvePathlineTracer::VvePathlineTracer()
	:	m_pData(NULL), 
		m_pTimeMapper(NULL), 
		m_pOutput(vtkPolyData::New()),
		m_pSeedPoints(NULL),
		m_iSeedSize(0),
		m_dIntegrationStep(1.0), 
		m_iTraceMode(TRACE_FORWARD),
		m_dTerminalSpeed(0.0),
		m_bRecordCells(false)
{
	m_dTimeFrame[0] = m_dTimeFrame[1] = 0.0;
}

VvePathlineTracer::~VvePathlineTracer()
{
	delete[] m_pSeedPoints;
	m_pOutput->Delete();
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetData                                                     */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::SetData(VveDiscreteDataTyped<vtkDataSet> *pData)
{
	m_pData = pData;
	m_pTimeMapper = pData->GetTimeMapper();
	m_dTimeFrame[0] = m_pTimeMapper->GetSimulationTime(0);
	m_dTimeFrame[1] = m_pTimeMapper->GetSimulationTime(m_pTimeMapper->GetNumberOfTimeIndices() - 1);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData                                                     */
/*                                                                            */
/*============================================================================*/
VveDiscreteDataTyped<vtkDataSet> *VvePathlineTracer::GetData() const
{
	return m_pData;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetSeed                                                     */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::SetSeed(vtkPointSet *pPts, double *pStartTimes)
{
	// Since Vtk 6.* Update() should be called on the generating algorithm.
	// Therefore, the calling function has to take care of that.
	// pPts->Update();
	if(m_pSeedPoints != NULL)
		delete [] m_pSeedPoints;
	m_iSeedSize = pPts->GetNumberOfPoints();
	m_pSeedPoints = new double[4*m_iSeedSize];
	for(int i=0; i<m_iSeedSize; ++i)
	{
		pPts->GetPoint(i,&(m_pSeedPoints[4*i]));
		m_pSeedPoints[4*i+3] = pStartTimes[i];
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :    GetSeed                                                    */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::GetSeed(vtkPointSet* pPtSet, double *pStartTimes) const
{
	pPtSet->Initialize();
	vtkPoints *pPts = vtkPoints::New();
	pPts->Allocate(m_iSeedSize);
	for(int i=0; i<m_iSeedSize; ++i)
	{
		pPts->InsertNextPoint(&(m_pSeedPoints[4*i]));
		pStartTimes[i] = m_pSeedPoints[4*i+3];
	}
	pPtSet->SetPoints(pPts);
	pPts->Delete();
	pPtSet->Squeeze();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetSeed                                                     */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::SetSeed(double *fPts, int iNumSeeds)
{
	if(m_pSeedPoints != NULL)
		delete[] m_pSeedPoints;
	m_iSeedSize = iNumSeeds;
	m_pSeedPoints = new double[4*m_iSeedSize];
	memcpy(m_pSeedPoints, fPts, 4*iNumSeeds*sizeof(double));
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSeed                                                     */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::GetSeed(double*& fPts, int& iNumSeeds) const
{
	if(fPts != NULL)
		delete[] fPts;
	iNumSeeds = m_iSeedSize;
	fPts = new double[4*m_iSeedSize];
	memcpy(fPts, m_pSeedPoints, 4*m_iSeedSize*sizeof(double));
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetSeed                                                     */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::SetSeed(const std::vector<VistaVector3D>& vecSeeds)
{
	if(m_pSeedPoints != NULL)
		delete [] m_pSeedPoints;
	// @todo: Get rid of this cast. Pronto.
	m_iSeedSize = static_cast<int>(vecSeeds.size());
	m_pSeedPoints = new double[4*m_iSeedSize];
	for(register int i=0; i<m_iSeedSize; ++i)
	{
		m_pSeedPoints[4*i]   = vecSeeds[i][0];
		m_pSeedPoints[4*i+1] = vecSeeds[i][1];
		m_pSeedPoints[4*i+2] = vecSeeds[i][2];
		m_pSeedPoints[4*i+3] = vecSeeds[i][3];
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSeed                                                     */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::GetSeed(std::vector<VistaVector3D>& vecSeeds) const
{
	vecSeeds.clear();
	vecSeeds.reserve(m_iSeedSize);
	for(int i=0; i<m_iSeedSize; ++i)
	{
		vecSeeds.push_back(VistaVector3D(	m_pSeedPoints[4*i], 
			m_pSeedPoints[4*i+1], 
			m_pSeedPoints[4*i+2], 
			m_pSeedPoints[4*i+3]	));
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetIntegrationStep                                          */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::SetIntegrationStep(double dStep)
{
	m_dIntegrationStep = dStep;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIntegrationStep                                          */
/*                                                                            */
/*============================================================================*/
double VvePathlineTracer::GetIntegrationStep() const
{
	return m_dIntegrationStep;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTerminalSpeed                                            */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::SetTerminalSpeed(double dSpeed)
{
	m_dTerminalSpeed = dSpeed;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTerminalSpeed                                            */
/*                                                                            */
/*============================================================================*/
double VvePathlineTracer::GetTerminalSpeed() const
{
	return m_dTerminalSpeed;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTimeFrame                                                */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::SetTimeFrame(double dSimStart, double dSimEnd)
{
	m_dTimeFrame[0] = dSimStart;
	m_dTimeFrame[1] = dSimEnd;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeFrame                                                */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::GetTimeFrame(double& dSimStart, double& dSimEnd) const
{
	dSimStart = m_dTimeFrame[0];
	dSimEnd = m_dTimeFrame[1];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetFieldsToInterpolate                                      */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::SetFieldsToInterpolate(
							const std::list<std::string> &liFieldNames)
{
	m_liInterpolFields = liFieldNames;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFieldsToInterpolate                                      */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::GetFieldsToInterpolate(
							std::list<std::string>& liFieldNames) const
{
	liFieldNames = m_liInterpolFields;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTraceMode			                                      */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::SetTraceMode(int i)
{
	m_iTraceMode = i;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTraceMode			                                      */
/*                                                                            */
/*============================================================================*/
int VvePathlineTracer::GetTraceMode() const
{
	return m_iTraceMode;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRecordCells                                              */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::SetRecordCells(bool b)
{
	m_bRecordCells = b;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetRecordCells                                              */
/*                                                                            */
/*============================================================================*/
bool VvePathlineTracer::GetRecordCells() const
{
	return m_bRecordCells;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisitedCells                                             */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::GetVisitedCells(std::list<SCellID>& liCells) const
{
	liCells = m_liVisitedCells;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :										                          */
/*                                                                            */
/*============================================================================*/
void VvePathlineTracer::Execute()
{
	if(	m_pData == NULL || 
		m_pTimeMapper == NULL || 
		m_pData->GetNumberOfLevels() == 0 || 
		m_pData->GetTypedLevelDataByLevelIndex(0) == NULL)
	{
		vstr::errp() << "[VvePathlineTracer::Execute]: Invalid input! Bailing out!" << endl;
		return;
	}

	//retrieve time mapper from input and determine start index
	VveTimeMapper *pTM = m_pData->GetTimeMapper();
	float fStartVisTime = pTM->GetVisualizationTime((float) m_dTimeFrame[0]);
	int iFirstLevelIndex = pTM->GetLevelIndex(pTM->GetTimeIndex(fStartVisTime));
	
	//retrieve input data
	assert (m_pData->GetTypedLevelDataByLevelIndex(iFirstLevelIndex) != NULL);
	vtkDataSet *pFirstInput = m_pData->GetTypedLevelDataByLevelIndex(iFirstLevelIndex)->GetData();
	if(pFirstInput == NULL)
	{
		vstr::errp() << "[VvePathlineTracer::Execute]: Unable to retrieve first input data set for level index <";
		vstr::err() << iFirstLevelIndex << ">" << endl;
		return;
	}
	vtkPointData *pInPD = pFirstInput->GetPointData();
	//we need an active vector field!
	if(pInPD->GetVectors() == NULL)
	{
		vstr::errp() << "[VvePathlineTracer::Execute]: Unable to retrieve vector field!" << endl;
		return;
	}
	
	//configure output
	vtkPolyData *pOutput = this->GetOutput();
	vtkPoints *pOutPts = vtkPoints::New();
	vtkPointData *pOutPD = pOutput->GetPointData();

	//allocate data structures for saving the lines
	vtkCellArray *pLines = vtkCellArray::New();
	vtkIdList *pCurrentLine = vtkIdList::New();

	//create integrator and interpolator
	vtkInitialValueProblemSolver *pIntegrator = vtkRungeKutta4::New();
	VveInstatInterpolator *pInterpolator = new VveInstatInterpolator;
	pInterpolator->SetDefaultInterpolationMode(VveInstatInterpolator::INT_VECTORS);
	pInterpolator->SetData(m_pData);
	pIntegrator->SetFunctionSet(pInterpolator);
	
	m_liVisitedCells.clear();
	
	//Determine which fields need to be interpolated
	std::list<std::string> liAttsToInterpolate;
	if(m_liInterpolFields.empty())
	{
		int iNumAtts = pInPD->GetNumberOfArrays();
		std::string strCurrentName;
		for(int a=0; a<iNumAtts; ++a)
		{
			strCurrentName = pInPD->GetArray(a)->GetName();
			//do not need to interpolate vectors since this is done anyway!
			if(strCurrentName == string(pInPD->GetVectors()->GetName()))
				continue;
			liAttsToInterpolate.push_back(strCurrentName);
		}
	}
	else
	{
		std::list<std::string>::iterator itCurr = m_liInterpolFields.begin();
		std::list<std::string>::iterator itEnd  = m_liInterpolFields.end();
		for(; itCurr!=itEnd; ++itCurr)
		{
			//do not need to interpolate vectors since this is done anyway!
			if(*itCurr == string(pInPD->GetVectors()->GetName()))
				continue;
			liAttsToInterpolate.push_back(*itCurr);
		}
	}

	//initialize data field interpolation
	std::vector<vtkDataArray*> vecDataFields;
	vecDataFields.reserve(liAttsToInterpolate.size());
	std::vector<VveInstatInterpolator*> vecFieldInterpolators;
	vecFieldInterpolators.reserve(liAttsToInterpolate.size());
	
	std::list<std::string>::iterator itCurr = liAttsToInterpolate.begin();
	std::list<std::string>::iterator itEnd  = liAttsToInterpolate.end();
	vtkDataArray *pNewArray;
	VveInstatInterpolator *pInterpol;
	for(;itCurr != itEnd; ++itCurr)
	{
		//allocate the array itself and the interpolator
		this->AllocateDataArray(pInPD->GetArray(itCurr->c_str()), 5000, 
												pNewArray, pInterpol);
		vecDataFields.push_back(pNewArray);
		vecFieldInterpolators.push_back(pInterpol);
	}

	//get ourselves the velocity at the traced points as well...
	vtkDataArray *pVelocity = vtkFloatArray::New();
	pVelocity->SetNumberOfComponents(3);
	pVelocity->Allocate(500,1000);
	pVelocity->SetName(pInPD->GetVectors()->GetName());

	//remeber the time for which each point is valid
	vtkDataArray *pTimeStamps = vtkFloatArray::New();
	pTimeStamps->SetNumberOfComponents(1);
	pTimeStamps->Allocate(500,1000);
	pTimeStamps->SetName("sim_time");

	//remember the reason, why a line terminated
	//NOTE: This relies on the fact that there are no more than 8 reasons for termination!
	vtkDataArray *pTermination = vtkCharArray::New();
	pTermination->SetNumberOfComponents(1);
	if(m_iTraceMode == TRACE_BOTH)
		pTermination->SetNumberOfTuples(2*m_iSeedSize);
	else
		pTermination->SetNumberOfTuples(m_iSeedSize);
	pTermination->FillComponent(0, TERM_UNKNOWN);
	pTermination->SetName("TERM_REASON");
	// to catch the value returned by pIntegrator->ComputeNextStep(...)
	int iStatusIntegrator = 0;

	double dStartPt[4];
	double dVelVec[3];
	
	//float fCurT = m_pTimeMapper->GetSimulationTime(0.0f);
	int iCurPtId = -1, iCurrentStartPtId = -1;
	vtkIdType pLastCells[2] = {-1,-1};
	vtkIdType pLastTimeIdx[2] = {-1,-1};
	double dCurrentVal[9];
	double iReasonForTerm;
	SCellID sCurrentCell(-1,-1);

	int iNumCellsPerInterpol = pInterpolator->GetNumInterpolCells();
	vtkIdType *pCellIDs = new vtkIdType[iNumCellsPerInterpol];
	vtkIdType *pCellTimeIdx = new vtkIdType[iNumCellsPerInterpol];

	//integrate for every seed
	for(int iCurPt = 0; iCurPt<m_iSeedSize; ++iCurPt)
	{
		//initialize new line with first point
		//pSeeds->GetPoint(iCurPt,fCurPt);
		dStartPt[0] = m_pSeedPoints[4*iCurPt];
		dStartPt[1] = m_pSeedPoints[4*iCurPt+1];
		dStartPt[2] = m_pSeedPoints[4*iCurPt+2];
		//init time! (ensure valid time frame here
		dStartPt[3] = m_pSeedPoints[4*iCurPt+3];
		if(dStartPt[3] < m_dTimeFrame[0] || dStartPt[3] > m_dTimeFrame[1])
		{
			vstr::warnp() << "[VvePathlineTracer::Execute] Seed out of time frame!" << endl;
			vstr::warni() << "\tSeed position: [" << dStartPt[0] << "," << dStartPt[1] << ",";
			vstr::warn() << dStartPt[2] << "," << dStartPt[3] << "]" << endl;
			iReasonForTerm = double(TERM_TIME_FRAME_LEFT);  // notes: declare this case as TERM_TIME_FRAME_LEFT
			pTermination->SetTuple(iCurPt, &iReasonForTerm);
			continue;
		}
		iCurPtId = pOutPts->InsertNextPoint(dStartPt);
		//add time and velocity for start point
		pTimeStamps->InsertNextTuple(&(dStartPt[3]));
		pInterpolator->FunctionValues(dStartPt, dVelVec);
		pVelocity->InsertNextTuple(dVelVec);
		
		//retrieve the data for the last cells
		if(m_bRecordCells)
		{
			pInterpolator->GetLastCells(pLastCells, pLastTimeIdx);
			sCurrentCell.m_iID = pLastCells[0];
			sCurrentCell.m_iTimeIdx = pLastTimeIdx[0];
			m_liVisitedCells.push_back(sCurrentCell);
			sCurrentCell.m_iID = pLastCells[1];
			sCurrentCell.m_iTimeIdx = pLastTimeIdx[1];
			m_liVisitedCells.push_back(sCurrentCell);
		}

		//compute field values for first pt
		for(size_t att=0; att<vecDataFields.size(); ++att)
		{
			vecFieldInterpolators[att]->FunctionValues(dStartPt, dCurrentVal);
			vecDataFields[att]->InsertNextTuple(dCurrentVal);
		}

		if(m_iTraceMode == TRACE_FORWARD || m_iTraceMode == TRACE_BOTH)
		{
			//add start point to line
			pCurrentLine->InsertNextId(iCurPtId);
			//trace the line forward in time
			this->TracePathline(dStartPt, m_dIntegrationStep, pInterpolator, pIntegrator,
					pVelocity, pTimeStamps, vecFieldInterpolators, vecDataFields,
					pCurrentLine, pOutPts, pLines, pTermination);

			//reset line after use!
			pCurrentLine->Reset();
		}
		if(m_iTraceMode == TRACE_BACKWARD || m_iTraceMode == TRACE_BOTH)
		{
			//add start point to line
			pCurrentLine->InsertNextId(iCurPtId);
			//trace the line backward in time
			this->TracePathline(dStartPt, -m_dIntegrationStep, pInterpolator, pIntegrator,
					pVelocity, pTimeStamps, vecFieldInterpolators, vecDataFields,
					pCurrentLine, pOutPts, pLines, pTermination);
			//reset line after use!
			pCurrentLine->Reset();
		}
	}
	//assemble output
	pOutput->SetPoints(pOutPts);
	pOutPts->Delete();
	pOutput->SetLines(pLines);
	pLines->Delete();
	
	pOutput->GetCellData()->SetScalars(pTermination);
	pTermination->Delete();
	
	pOutPD->AddArray(pTimeStamps);
	pTimeStamps->Delete();
	pOutPD->SetVectors(pVelocity);
	pVelocity->Delete();
	
	for(size_t att=0; att<vecDataFields.size(); ++att)
	{
		pOutPD->AddArray(vecDataFields[att]);
		vecDataFields[att]->Delete();
		vecFieldInterpolators[att]->Delete();
	}
	
	pOutput->Squeeze();

	//cleanup
	pCurrentLine->Delete();
	pIntegrator->Delete();
	pInterpolator->Delete();
	
	delete[] pCellIDs;
	delete[] pCellTimeIdx;
}

vtkPolyData *VvePathlineTracer::GetOutput()
{
	return m_pOutput;
}


void VvePathlineTracer::GetOutput( VveParticlePopulation *pParticles)
{
	if(!pParticles)
	{
		vstr::errp() << "[VvePathlineTracer::GetOutput] Can't work on NULL output!" << endl << endl;
		return;
	}

	vtkPoints *pPts = m_pOutput->GetPoints();
	vtkDataArray *pTime = m_pOutput->GetPointData()->GetArray("sim_time");
	vtkDataArray *pVelocity = m_pOutput->GetPointData()->GetVectors();
	vtkDataArray *pScalars = m_pOutput->GetPointData()->GetScalars();
	vtkCellArray *pLines = m_pOutput->GetLines();

	if(!pPts || !pTime || !pVelocity || !pLines)
	{
		vstr::errp() << "[VvePathlineTracer::GetOutput] Can't convert incomplete data!" << endl << endl;
		return ;
	}

	int iNumLines = pLines->GetNumberOfCells();
	pParticles->Clear();

	vtkIdType *pts, npts;
	int iPos = 0;
	double dCurrentV[3];
	for(int i=0; i<iNumLines; ++i)
	{
		pLines->GetCell(iPos, npts, pts);
		iPos += (npts+1);

		// create particle data arrays and add to a new particle trajectory
		VveParticleTrajectory* pTraj = new VveParticleTrajectory();
		for(int i=0; i<m_pOutput->GetPointData()->GetNumberOfArrays(); i++){
			VveParticleDataArray<float> *tmp = new VveParticleDataArray<float>(m_pOutput->GetPointData()->GetArray(i)->GetNumberOfComponents(), m_pOutput->GetPointData()->GetArray(i)->GetName());
			pTraj->AddArray(tmp);
		}

		VveParticleDataArray<float> *aPos = new VveParticleDataArray<float>(3, VveParticlePopulation::sPositionArrayDefault);
        pTraj->AddArray(aPos);

        // Resize arrays
        pTraj->Resize(npts);

		//Fill Data Array with Data
		for(int j=0; j<npts; ++j)
		{
		      pPts->GetPoint(pts[j], dCurrentV);
			  aPos->SetElement(j, dCurrentV);

			  for(int i=0; i<m_pOutput->GetPointData()->GetNumberOfArrays(); i++){
				  pTraj->GetArray(i)->SetElement(j, m_pOutput->GetPointData()->GetArray(i)->GetTuple(pts[j]));
			  }
		}
        pParticles->AddTrajectory(pTraj);
	}
}

void VvePathlineTracer::AllocateDataArray(vtkDataArray *pTemplate, 
										   unsigned int iAllocSize, 
										   vtkDataArray *&pNewArray, 
										   VveInstatInterpolator *&pInterpolator) const
{
	pNewArray = pTemplate->NewInstance();
	pNewArray->SetName(pTemplate->GetName());
	pNewArray->SetNumberOfComponents(pTemplate->GetNumberOfComponents());
	pNewArray->Allocate(iAllocSize, 2*iAllocSize);


	pInterpolator = new VveInstatInterpolator;
	pInterpolator->SetDataFieldName(pTemplate->GetName());
	pInterpolator->SetData(m_pData);
	switch (pTemplate->GetNumberOfComponents()){
		case 1: pInterpolator->SetDefaultInterpolationMode(2); break;
		case 3: pInterpolator->SetDefaultInterpolationMode(0); break;
		case 9: pInterpolator->SetDefaultInterpolationMode(1); break;
		default: vstr::errp() << "[VvePathlineTracer::AllocateDataArray] pTemplate->GetNumberOfComponents not 1,3, or 9 (Scalar, Vector or Tensor)! Default is used!" << endl << endl;
	}
	
}


void VvePathlineTracer::TracePathline(	const double dStartPt[4],
										double dIntegrationStep,
										VveInstatInterpolator *pInterpolator,
										vtkInitialValueProblemSolver *pIntegrator,
										vtkDataArray *pVelocity,
										vtkDataArray *pTimeStamps,
										std::vector<VveInstatInterpolator*>& vecFieldInterpolators,
										std::vector<vtkDataArray*>& vecDataFields,
										vtkIdList *pCurrentLine,
										vtkPoints *pOutPts,
										vtkCellArray *pLines,
										vtkDataArray *pTermination)
{
	int iCurPtId = -1;
	int iReasonForTerm = TERM_UNKNOWN;
	int iStatusIntegrator;
	SCellID sCurrentCell(-1,-1);
	double dVel;
	double dVelVec[3] = {0.0, 0.0, 0.0};
	double dNextPt[4] = {0.0, 0.0, 0.0, 0.0};
	double dErr = 0.0, dMaxErr = 0.0;
	vtkIdType pLastCells[2] = {-1,-1};
	vtkIdType pLastTimeIdx[2] = {-1,-1};
	double dCurrentVal[9] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	double dCurPt[4] = {dStartPt[0], dStartPt[1], dStartPt[2], dStartPt[3]};
	
	//Integrate while in the given time frame and ComputeNextStep yields a valid result.
	bool bFinished = false;
	if(dIntegrationStep > 0.0f)
	{
		//FORWARD TRACING:
		//NOTE: To ensure a valid result for a complete integration step 
		//      fCurPt[3] + m_fIntegrationStep must be within the limits!
		bFinished = !	(dCurPt[3] >= m_dTimeFrame[0]) && 
						(dCurPt[3] +  dIntegrationStep <= m_dTimeFrame[1]);
	}
	else
	{
		//BACKWARD TRACING ANALOGUOUS
		bFinished = !	(dCurPt[3] +  dIntegrationStep >= m_dTimeFrame[0]) && 
						(dCurPt[3] <= m_dTimeFrame[1]);
	}

	while(!bFinished)
	{
		iStatusIntegrator = pIntegrator->ComputeNextStep(
			dCurPt, dNextPt, dCurPt[3], dIntegrationStep, dMaxErr, dErr);
		//catch the TERM_OUT_OF_DOMAIN case, based on retun value of pIntegrator
		if(iStatusIntegrator != 0) 
		{
			iReasonForTerm = TERM_OUT_OF_DOMAIN;
			break;
		}

		dCurPt[0] = dNextPt[0];
		dCurPt[1] = dNextPt[1];
		dCurPt[2] = dNextPt[2];
		dCurPt[3] += dIntegrationStep;
		if(dIntegrationStep > 0.0f)
		{
			//FORWARD TRACING
			//leaving time frame at the upper end
			bFinished = dCurPt[3]+dIntegrationStep > m_dTimeFrame[1];
		}
		else
		{
			//BACKWARD TRACING 
			//leaving time frame at the lower end
			bFinished = dCurPt[3]+dIntegrationStep < m_dTimeFrame[0];
		}

		//insert point into line
		iCurPtId = pOutPts->InsertNextPoint(dCurPt);
		pCurrentLine->InsertNextId(iCurPtId);
		//remember time 
		pTimeStamps->InsertNextTuple(&(dCurPt[3]));	
		
		//compute current velocity
		//vstr::errp() << "\nCalling FunctionValues with dCurPt and dVelVec[3]!!!!"<< std::endl;
		pInterpolator->FunctionValues(dCurPt, dVelVec);
		pVelocity->InsertNextTuple(dVelVec);
		dVel = vtkMath::Norm(dVelVec);
		
		//retrieve the data for the last cells
		if(m_bRecordCells)
		{
			pInterpolator->GetLastCells(pLastCells, pLastTimeIdx);
			sCurrentCell.m_iID = pLastCells[0];
			sCurrentCell.m_iTimeIdx = pLastTimeIdx[0];
			m_liVisitedCells.push_back(sCurrentCell);
			sCurrentCell.m_iID = pLastCells[1];
			sCurrentCell.m_iTimeIdx = pLastTimeIdx[1];
			m_liVisitedCells.push_back(sCurrentCell);
		}

		//interpolate attributes
		for(size_t att=0; att<vecDataFields.size(); ++att)
		{
		//	vstr::errp() << "\nCalling FunctionValues with dCurPt and dCurrentVal[9]!"<< std::endl;
			vecFieldInterpolators[att]->FunctionValues(dCurPt, dCurrentVal);
			vecDataFields[att]->InsertNextTuple(dCurrentVal);
		}
	
		//finally, check for terminal speed
		if(dVel < m_dTerminalSpeed)
		{
			iReasonForTerm = TERM_LOW_SPEED;
			break;
		}
	}
	if(dIntegrationStep > 0)
	{
		if(dCurPt[3]<m_dTimeFrame[0] || dCurPt[3] + dIntegrationStep > m_dTimeFrame[1])
			iReasonForTerm = TERM_TIME_FRAME_LEFT;
	}
	else
	{
		if(dCurPt[3] + dIntegrationStep < m_dTimeFrame[0] || dCurPt[3] > m_dTimeFrame[1])
			iReasonForTerm = TERM_TIME_FRAME_LEFT;
	}

	//insert line anyway even if it has only one pt 
	//-> this maintains the mapping from seeds to traces
	int iNewLine = pLines->InsertNextCell(pCurrentLine);
	double dRT = (double) iReasonForTerm;
	pTermination->SetTuple(iNewLine, &dRT);
}
/*============================================================================*/
/*  END OF FILE "VvePathlineTracer.cpp"                                       */
/*============================================================================*/




