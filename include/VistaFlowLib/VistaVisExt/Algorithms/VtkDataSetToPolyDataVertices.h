#ifndef _VTKDATASETTOPOLYDATAVERTICES_H
#define _VTKDATASETTOPOLYDATAVERTICES_H

#include <vtkPolyDataAlgorithm.h>

#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>

/**
 * Put up to three different scalar data fields to the output's coordinate
 * values in order to create a simple scatterplot
 */
class VISTAVISEXTAPI vtkDataSetToPolyDataVertices : public vtkPolyDataAlgorithm
{
	vtkTypeRevisionMacro(vtkDataSetToPolyDataVertices,vtkPolyDataAlgorithm);

	static vtkDataSetToPolyDataVertices *New();

	void PrintSelf(ostream& os, vtkIndent indent);

protected:
	vtkDataSetToPolyDataVertices();
	virtual ~vtkDataSetToPolyDataVertices();
	/**
	*
	*/
	virtual int RequestData(vtkInformation* request,
							vtkInformationVector** inputVector,
							vtkInformationVector* outputVector);
	/**
	*
	*/
	virtual int FillInputPortInformation(int port, vtkInformation *info);
private:
};

#endif
