/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "Vtk2DHistogramToImageData.h"

#include "../Tools/Vve2DHistogram.h"

#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkIntArray.h>
#include <vtkPointData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkImageData.h>
#include <vtkStreamingDemandDrivenPipeline.h>

#include <VistaBase/VistaStreamUtils.h>

#include <iostream>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
vtkCxxRevisionMacro(vtk2DHistogramToImageData, "$Id$");
vtkStandardNewMacro(vtk2DHistogramToImageData);

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
vtk2DHistogramToImageData::vtk2DHistogramToImageData() 
	:	m_strOutputFieldName("count"),
		m_pHistogram(NULL)
{
	m_fBounds[0] = m_fBounds[2] = m_fBounds[4] = 0.0f;
	m_fBounds[1] = m_fBounds[3] = m_fBounds[5] = 1.0f;

	this->SetNumberOfInputPorts(0);
	this->SetNumberOfOutputPorts(1);
}
//------------------------------------------------------------
//------------------------------------------------------------
vtk2DHistogramToImageData::~vtk2DHistogramToImageData()
{
}
/*============================================================================*/
/*  IMPLEMENTATION															  */
/*============================================================================*/
//------------------------------------------------------------
//------------------------------------------------------------
void vtk2DHistogramToImageData::PrintSelf(std::ostream& os, vtkIndent indent)
{
	using std::endl;
	vtkImageAlgorithm::PrintSelf(os, indent);
	os << indent << "Output field name : " << m_strOutputFieldName << endl;
	os << indent << "Bounds : " << m_fBounds[0] << ", " << m_fBounds[1];
	os << ", " << m_fBounds[2] << ", " << m_fBounds[3] << ", ";
	os << m_fBounds[3] << ", " << m_fBounds[5] << endl;
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtk2DHistogramToImageData::SetTargetBounds(float fBounds[6])
{
	memcpy(m_fBounds, fBounds, 6*sizeof(float));
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtk2DHistogramToImageData::GetTargetBounds(float fBounds[6]) const
{
	memcpy(fBounds, m_fBounds, 6*sizeof(float));
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtk2DHistogramToImageData::SetOutputFieldName(const std::string& strOutputFieldName)
{
	m_strOutputFieldName = strOutputFieldName;
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
std::string vtk2DHistogramToImageData::GetOutputFieldName() const
{
	return m_strOutputFieldName;
}
//------------------------------------------------------------
//------------------------------------------------------------
void vtk2DHistogramToImageData::SetHistogram(Vve2DHistogram *pHistogram)
{
	m_pHistogram = pHistogram;
	this->Modified();
}
//------------------------------------------------------------
//------------------------------------------------------------
Vve2DHistogram *vtk2DHistogramToImageData::GetHistogram() const
{
	return m_pHistogram;
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtk2DHistogramToImageData::RequestData(vtkInformation *, 
									 vtkInformationVector **inputVector, 
									 vtkInformationVector *outputVector)
{
	if(m_pHistogram == NULL)
	{
		vstr::errp() << "[vtk2DHistogramToImageData::RequestData] no input!" << endl;
		return -1;
	}
	// get the info objects
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkImageData *pOutput = vtkImageData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
	if(!pOutput)
	{
		vstr::errp() << "[vtk2DHistogramToImageData::RequestData] no output!" << endl;
		return -1;
	}
	float fRange[3] = {
		m_fBounds[1] - m_fBounds[0],
		m_fBounds[3] - m_fBounds[2],
		m_fBounds[5] - m_fBounds[4]
	};
	int iRes[3] = {0,0,1};
	m_pHistogram->GetResolution(iRes[0], iRes[1]);

	float fSpacing[3] = {
		fRange[0] / (float) iRes[0],
		fRange[1] / (float) iRes[1],
		fRange[2] / (float) iRes[2]
	};

	//setup the output
	pOutput->Initialize();
	pOutput->SetOrigin(m_fBounds[0], m_fBounds[2], 0.0);
	pOutput->SetSpacing(fSpacing[0], fSpacing[1], 1.0);
	pOutput->SetDimensions(iRes[0], iRes[1], 1);

#if VTK_MAJOR_VERSION > 5
	pOutput->AllocateScalars(VTK_FLOAT, 1);
#else
	pOutput->SetScalarTypeToFloat();
	pOutput->SetNumberOfScalarComponents(1);
	pOutput->AllocateScalars();
#endif

	//retrieve and null output scalars 
	pOutput->GetPointData()->GetScalars()->SetName(m_strOutputFieldName.c_str());
	float *pScalars = static_cast<float*>(pOutput->GetScalarPointer());
	memset(pScalars, 0, pOutput->GetNumberOfPoints()*sizeof(float));
	
	//normalize values to z=[fBounds[4]...fBounds[5]];
	float fScaleZ = fRange[2] / (float) m_pHistogram->GetMaxBinCount();
	//for each point in the input -> determine cell and increment count
	for(register int y=0; y<iRes[1]; ++y)
	{
		for(register int x=0; x<iRes[0]; ++x)
		{
			pScalars[y*iRes[0]+x] = m_fBounds[4] + 
				fScaleZ * (float) m_pHistogram->GetBinCount(x,y);
		}
	}
	return 1;
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtk2DHistogramToImageData::FillInputPortInformation(int port, vtkInformation *info)
{
	return 1;
}
//------------------------------------------------------------
//------------------------------------------------------------
int vtk2DHistogramToImageData::RequestInformation (vtkInformation* vtkNotUsed(request),
												   vtkInformationVector** inputVector,
												   vtkInformationVector* outputVector)
{
	// get the info objects
	vtkInformation* outInfo = outputVector->GetInformationObject(0);
	int iRes[3] = {0, 0, 1};
	if(m_pHistogram)
	{
		m_pHistogram->GetResolution(iRes);
	}
	int wholeExtent[6] ={0, iRes[0]-1, 0, iRes[1]-1, 0, 1};
	outInfo->Set(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(), wholeExtent ,6);
	
	float fRange[3] = {
		m_fBounds[1] - m_fBounds[0],
		m_fBounds[3] - m_fBounds[2],
		m_fBounds[5] - m_fBounds[4]
	};
	double dSpacing[3] = {
		fRange[0] / (float) iRes[0],
		fRange[1] / (float) iRes[1],
		1
	};
	outInfo->Set(vtkDataObject::SPACING(), dSpacing, 3);
	
	double dOutOrigin[3] = {m_fBounds[0], m_fBounds[2], m_fBounds[4]};
	outInfo->Set(vtkDataObject::ORIGIN(), dOutOrigin, 3);

	return 1;
}
//------------------------------------------------------------
//------------------------------------------------------------


