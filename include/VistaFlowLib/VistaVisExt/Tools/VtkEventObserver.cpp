/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VtkEventObserver.CPP              RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VtkTopo                      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  VtkTopo                        Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/

#include "VtkEventObserver.h"

#include <vtkObject.h>
#include <algorithm>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
class VtkEventObserver::VtkObserver : public vtkCommand
{
public:
	VtkObserver(VtkEventObserver *pObs) : m_pObs(pObs), m_pTgt(0) {}
	virtual ~VtkObserver(){
		UnObserve();
	}

	virtual void Execute(vtkObject *pObj, unsigned long l, void *pData){
		//just forward the call
		m_pObs->Notify(l);
	}

	void SetTarget(vtkObject *pTarget){
		UnObserve();
		m_pTgt = pTarget;
	}

	vtkObject *GetTarget() const{
		return m_pTgt;
	}

	void ObserveEvent(vtkCommand::EventIds id){
		if(m_pTgt)
			m_pTgt->AddObserver(id, this);
	}
	
	void UnObserve(){
		if(m_pTgt)
			m_pTgt->RemoveObserver(this);
	}

private:
	VtkEventObserver *m_pObs;
	vtkObject *m_pTgt;
};
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VtkEventObserver::VtkEventObserver()
{
	m_pObs = new VtkObserver(this);
}

VtkEventObserver::~VtkEventObserver()
{
	delete m_pObs;
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :       SetTarget                                               */
/*                                                                            */
/*============================================================================*/
void VtkEventObserver::SetTarget(vtkObject *pTgt)
{
	m_pObs->SetTarget(pTgt);
	this->ReRegister();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetTarget                                               */
/*                                                                            */
/*============================================================================*/
vtkObject* VtkEventObserver::GetTarget() const
{
	return m_pObs->GetTarget();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :      ObserveEvent                                             */
/*                                                                            */
/*============================================================================*/
void VtkEventObserver::ObserveEvent(vtkCommand::EventIds id)
{
	m_pObs->ObserveEvent(id);
	m_liActiveEvents.push_back(id);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     UnObserveEvent                                            */
/*                                                                            */
/*============================================================================*/
void VtkEventObserver::UnObserveEvent(vtkCommand::EventIds id)
{
	list<vtkCommand::EventIds>::iterator it = find(m_liActiveEvents.begin(), m_liActiveEvents.end(), id);
	//no such event?
	if(it == m_liActiveEvents.end())
		return;
	m_liActiveEvents.erase(it);
	m_pObs->UnObserve();
	this->ReRegister();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :       GetObservedEvents                                       */
/*                                                                            */
/*============================================================================*/
void VtkEventObserver::GetObservedEvents(std::list<vtkCommand::EventIds>& liEvents) const
{
	liEvents = m_liActiveEvents;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :      ClearObservedEvents                                      */
/*                                                                            */
/*============================================================================*/
void VtkEventObserver::ClearObservedEvents()
{
	m_pObs->UnObserve();
	m_liActiveEvents.clear();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     ReRegister                                                */
/*                                                                            */
/*============================================================================*/
void VtkEventObserver::ReRegister()
{
	std::list<vtkCommand::EventIds>::iterator it = m_liActiveEvents.begin();
	//register for all the events we were registered before
	for(; it!=m_liActiveEvents.end(); ++it)
		m_pObs->ObserveEvent(*it);
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VtkEventObserver.cpp"                                             */
/*============================================================================*/
