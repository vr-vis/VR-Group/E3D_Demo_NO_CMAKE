/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEDATACHUNKINFO_H
#define _VVEDATACHUNKINFO_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaSerializable.h>
#include <vector>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveDataFieldInfo;
class IVistaSerializer;
class IVistaDeSerializer;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Hold meta data (e.g. min, max...) for a data field (e.g. scalar field) over
* one data chunk, i.e. either a data set, a time step, a block or some other sub-division of time-varying data.
*/
class VISTAVISEXTAPI VveDataChunkInfo : public IVistaSerializable
{
public:
	VveDataChunkInfo();
	virtual ~VveDataChunkInfo();

	void SetNumPoints(int);
	int GetNumPoints() const;
	
	void SetNumCells(int);
	int GetNumCells() const;

	// add/get/getnum of data fields in this chunk
	void AddFieldInfo(VveDataFieldInfo*);
	VveDataFieldInfo* GetFieldInfo(int) const;
	VveDataFieldInfo* GetFieldInfoByName(const std::string & sFieldName) const;
	size_t GetNumFieldInfo() const;//size of vector

	/*
	* This is the semantics this chunk of data, if available,
	* for instance dataset, time step or block.
	*/
	void SetSemanticName (const std::string & sSemantics);
	bool GetSemanticName (std::string & sSemantics) const;

	/*
	* This is the ID for this chunk of data, its interpretation depends on the
	* underlying semantics. E.g., for a "timestep" this could be time level,
	* for a "block" this could be block ID.
	*/
	void SetID (int iID);
	int GetID () const;

	// add/get/getnum of childs for hierarchical chunk organization
	void AddChildChunkInfo(VveDataChunkInfo*);
	VveDataChunkInfo* GetChildChunkInfo(int) const;
	size_t GetNumChildChunkInfo() const;//size of vector

	/*
	* Set/get spatial bounds of chunk
	*
	*/
	void SetBounds(double fBounds[6]);
	void GetBounds(double fBounds[6]) const;

	void SetIthBound(int i, double fIthBound);
	double GetIthBound(int i) const;

	/**
     * Think of this as "SAVE"
     */
    virtual int Serialize(IVistaSerializer &) const;
    /**
     * Think of this as "LOAD"
     */
    virtual int DeSerialize(IVistaDeSerializer &);

	/*
	* Should the child chunk nodes be serialized, too?
	* Default is true.
	*/
	void SetDoSerializeChilds (bool bDoSerializeChilds);
	bool GetDoSerializeChilds () const;

    virtual std::string GetSignature() const;
private:
	int m_iNumPoints;
	int m_iNumCells;

	double m_fBounds[6];

	bool m_bSerializeChilds;

	std::string m_sSemanticName;
	int			m_iID;

	// stored fields in this chunk
	std::vector<VveDataFieldInfo*> m_vecFieldInfo;
	// child chunks
	std::vector<VveDataChunkInfo*> m_vecChildDataChunks;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VVEDATACHUNKINFO_H

