/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveInfoReader.h"
#include "VveHistogram.h"
#include "VveDataFieldInfo.h"
#include "VveDataChunkInfo.h"

#include <VistaInterProcComm/Connections/VistaByteBufferDeSerializer.h>
//#include <VistaInterProcComm/Connections/VistaCSVDeserializer.h>
#include <VistaBase/VistaStreamUtils.h>

#include <iostream>
#include <string>

using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/


/*============================================================================*/
/*																			  */
/*  VveASCIIInfoReader                                                       */
/*																			  */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveASCIIInfoReader::VveASCIIInfoReader(){}
VveASCIIInfoReader::~VveASCIIInfoReader(){}

//set file name
void VveASCIIInfoReader::SetFileName(const string &strFileName)
{
	m_myDataFile.open(strFileName.c_str());
}

//read information about each field
bool VveASCIIInfoReader::ReadFieldInfo(VveDataFieldInfo *pFieldInfo)
{
	string strInfo, strSomeString;
	int someInt;
	double dSomeDouble;
	
	m_myDataFile >> strInfo;
	strSomeString = strInfo;
	m_myDataFile >> strInfo;
	strSomeString += strInfo;
	
	bool bIsField = (strSomeString == "BEGINFIELD");

	//name of one field
	if(strSomeString == "BEGINFIELD")
	{
		m_myDataFile >> strInfo;
		pFieldInfo->SetFieldName(strInfo);
	}
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFieldInfo] Invalid token <" << strSomeString << ">" << endl;
		return false;
	}
	//read type
	m_myDataFile >> strInfo >> someInt;
	if(strInfo == "TYPE")
	{
		pFieldInfo->SetType(someInt);
	}
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFieldInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}
	//read number of components
	m_myDataFile >> strInfo >> someInt;
	if(strInfo == "NUMCOMPS")
	{
		pFieldInfo->SetNumberOfComponents(someInt);
	}
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFieldInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}
	//read mininum
	m_myDataFile >> strInfo;;
	if(strInfo == "MIN")
	{
		for(int c=0; c<someInt; ++c)
		{
			m_myDataFile >> dSomeDouble;
			pFieldInfo->SetMin(c, dSomeDouble);
		}
	}
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFieldInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}
	//read maximum
	m_myDataFile >> strInfo;;
	if(strInfo == "MAX")
	{
		for(int c=0; c<someInt; ++c)
		{
			m_myDataFile >> dSomeDouble;
			pFieldInfo->SetMax(c, dSomeDouble);
		}
	}
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFieldInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}

	//read average
	m_myDataFile >> strInfo;;
	if(strInfo == "AVG")
	{
		for(int c=0; c<someInt; ++c)
		{
			m_myDataFile >> dSomeDouble;
			pFieldInfo->SetAverage(c, dSomeDouble);
		}
	}
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFieldInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}

	//read standard deviation
	m_myDataFile >> strInfo;;
	if(strInfo == "STD")
	{
		for(int c=0; c<someInt; ++c)
		{
			m_myDataFile >> dSomeDouble;
			pFieldInfo->SetStandardDeviation(c, dSomeDouble);
		}

		// re-read info for histogram
		m_myDataFile >> strInfo;

	}

	// read histogram data
	strSomeString = strInfo;

	if(strInfo == "HIST")
	{
		//set min and max of histogram
		pFieldInfo->GetHistogram()->SetMin(pFieldInfo->GetMin(0));
		pFieldInfo->GetHistogram()->SetMax(pFieldInfo->GetMax(0));


		int iSomeInt, iBinCount;
		int iNumTotalPts = 0;

		m_myDataFile >> iBinCount;
		pFieldInfo->GetHistogram()->SetNumBins(iBinCount);
				
		for(int i = 0; i < iBinCount; ++i)
		{
			m_myDataFile >> iSomeInt;
			if(iSomeInt < 0)
			{
				vstr::errp() << "[VveASCIIInfoReader::ReadFieldInfo] "
					 << "Invalid histogram bin count (<0)!" << endl;
				iSomeInt = 0;
			}
			pFieldInfo->GetHistogram()->SetBinCount(i, iSomeInt);
			iNumTotalPts += iSomeInt;
		}
		pFieldInfo->GetHistogram()->SetNumTotalPoints(iNumTotalPts);


		//read the max bin count
		m_myDataFile >> strInfo;
		m_myDataFile >> iSomeInt;
		if(strInfo == "MAX")
			pFieldInfo->GetHistogram()->SetMaxBinCount(iSomeInt);
		else
		{
			vstr::errp() << "[VveASCIIInfoReader::ReadFieldInfo] Invalid token <" << strInfo << ">" << endl;
			return false;
		}


		m_myDataFile >> strInfo;
		strSomeString = strInfo;
		m_myDataFile >> strInfo;
		strSomeString += strInfo;
	}
	else if(strInfo == "END")
	{
		m_myDataFile >> strInfo;
		strSomeString += strInfo;
	}
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFieldInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}

	
	if(bIsField && strSomeString == "ENDFIELD" )
		return true;
	else 
		return false;
}

//read information about each time step
bool VveASCIIInfoReader::ReadTimeStepInfo(VveDataChunkInfo* pTimeStepInfo)
{
	string strInfo, strSomeString;
	int iSomeInt;

	m_myDataFile >> strInfo;
	strSomeString = strInfo;
	m_myDataFile >> strInfo;
	strSomeString += strInfo;
		
	bool bIsTimeStep = (strSomeString == "BEGINTIMESTEP");

	if(strSomeString == "BEGINTIMESTEP")
		m_myDataFile >> iSomeInt;
	else 
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadTimeStepInfo] Invalid token <" << strSomeString << ">" << endl;
		return false;
	}

	//points
	m_myDataFile >> strInfo >> iSomeInt;
	if(strInfo == "POINTS")
		pTimeStepInfo->SetNumPoints(iSomeInt);
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadTimeStepInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}

	//cells
	m_myDataFile >> strInfo >> iSomeInt;
	if(strInfo == "CELLS")
		pTimeStepInfo->SetNumCells(iSomeInt);
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadTimeStepInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}

	//bounds
	m_myDataFile >> strInfo;
	if(strInfo == "BOUNDS")
	{
		double dSomeDoulbe;
		for(int i = 0; i < 6; ++i)
		{
			m_myDataFile >> dSomeDoulbe;
			pTimeStepInfo->SetIthBound(i, dSomeDoulbe);	
		}
	}
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadTimeStepInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}
	

	//field info
	m_myDataFile >> strInfo >> iSomeInt;
	if(strInfo == "POINT_FIELDS")
	{
		for(int j = 0; j < iSomeInt; ++j)
		{
			VveDataFieldInfo *pFieldInfo = new VveDataFieldInfo(); 

			if(ReadFieldInfo(pFieldInfo))
				pTimeStepInfo->AddFieldInfo(pFieldInfo);
			else
			{
				vstr::errp() << "[VveASCIIInfoReader::ReadTimeStepInfo] Error when reading the field info!" << endl;
				return false;
			}
		}
	}
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadTimeStepInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}

	m_myDataFile >> strInfo;
	strSomeString = strInfo;
	m_myDataFile >> strInfo;
	strSomeString += strInfo;//strInfo == "ENDTIMESTEP" 

	if(bIsTimeStep && strSomeString == "ENDTIMESTEP" )
		return true;
	else 
		return false;
}


//read information from file
bool VveASCIIInfoReader::ReadFileInfo(VveDataSetInfo *pDataSetInfo)
{
	//check if the file is already open
	if(!m_myDataFile.is_open())
	{
		vstr::warnp() << "[VveASCIIInfoReader::ReadFileInfo] Unable to open file"; 
		return false;
	}

	string strInfo, strSomeString;
	int iMinT, iMaxT, iMinB, iMaxB, iSomeInt;
	//read the 1st row
	m_myDataFile >> strInfo >> iMinT >> iMaxT >> iMinB >> iMaxB;

	pDataSetInfo->SetDataSetName(strInfo);
	pDataSetInfo->SetMinTimeStep(iMinT);
	pDataSetInfo->SetMaxTimeStep(iMaxT);

	//read 2nd row
	int iNumTimeSteps;
	m_myDataFile >> strInfo >> iNumTimeSteps;

	//begin with the "TIMESTEP I"
	for(int i = iMinT; i <= iMaxT; ++i)
	{
		VveDataChunkInfo *pTimeStepInfo = new VveDataChunkInfo;

		if(ReadTimeStepInfo(pTimeStepInfo))
		{
			pTimeStepInfo->SetSemanticName ("timestep");
			pTimeStepInfo->SetID(i);
			pDataSetInfo->AddChildChunkInfo(pTimeStepInfo);
		}
		else
		{
			vstr::errp() << "[VveASCIIInfoReader::ReadFileInfo] Error when reading the file!" << endl;
			return false;
		}
	}

	m_myDataFile >> strInfo;
	strSomeString = strInfo;
	m_myDataFile >> strInfo;
	strSomeString += strInfo;//"BEGINTOTAL"

	bool bIsTotal = (strSomeString == "BEGINTOTAL");

	if(strSomeString != "BEGINTOTAL")
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFileInfo] Invalid token <" << strSomeString << ">" << endl;
		return false;
	}

	//points
	m_myDataFile >> strInfo >> iSomeInt;
	if(strInfo == "POINTS")
		pDataSetInfo->SetNumPoints(iSomeInt);
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFileInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}

	//cells
	m_myDataFile >> strInfo >> iSomeInt;
	if(strInfo == "CELLS")
		pDataSetInfo->SetNumCells(iSomeInt);
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFileInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}

	//bounds
	m_myDataFile >> strInfo;
	if(strInfo == "BOUNDS")
	{
		double dSomeDoulbe;
		for(int i = 0; i < 6; ++i)
		{
			m_myDataFile >> dSomeDoulbe;
			pDataSetInfo->SetIthBound(i, dSomeDoulbe);	
		}
	}
	else
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFileInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}
	

	m_myDataFile >> strInfo >> iSomeInt;
	if(strInfo == "POINT_FIELDS")
	{
		for(int j = 0; j < iSomeInt; ++j)
		{
			VveDataFieldInfo *pFieldInfo = new VveDataFieldInfo; 

			if(ReadFieldInfo(pFieldInfo))
				pDataSetInfo->AddFieldInfo(pFieldInfo);
			else
			{
				vstr::errp() << "[VveASCIIInfoReader::ReadFileInfo] Error when reading the field info!" << endl;
				return false;
			}
		}
	}
	else 
	{
		vstr::errp() << "[VveASCIIInfoReader::ReadFileInfo] Invalid token <" << strInfo << ">" << endl;
		return false;
	}

	m_myDataFile >> strInfo;
	strSomeString = strInfo;
	m_myDataFile >> strInfo;
	strSomeString += strInfo;//strInfo == "ENDTOTAL" 

	m_myDataFile.close();

	if(bIsTotal && strSomeString == "ENDTOTAL" )
		return true;
	else 
		return false;
}


/*============================================================================*/
/*																			  */
/*  VveInfoReader (binary)                                                   */
/*																			  */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveInfoReader::VveInfoReader()
{
}
VveInfoReader::~VveInfoReader()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
//set file name
void VveInfoReader::SetFileName(const string &strFileName)
{
	m_myDataFile.clear();
	m_myDataFile.open(strFileName.c_str(), std::ios_base::binary);
}


//read information from file
bool VveInfoReader::ReadFileInfo(VveDataSetInfo *pDataSetInfo)
{
	//check if the file is already open
	if(!m_myDataFile.is_open())
	{
		vstr::warnp() << "[VveInfoReader::ReadFileInfo] Unable to open file"; 
		return false;
	}

	int iSize;
	m_myDataFile >> iSize;

	VistaType::byte* pBuffer = new VistaType::byte[iSize];

	// read buffer
	m_myDataFile.read ( (char*)pBuffer, iSize);

	// deserialize buffer into data set information object
	VistaByteBufferDeSerializer oDeSerializer;
	//oDeSerializer.FillBuffer (pBuffer, iSize);
	oDeSerializer.SetBuffer (pBuffer, iSize, true);
	int iRet = pDataSetInfo->DeSerialize (oDeSerializer);
	oDeSerializer.ClearBuffer();

	//delete [] pBuffer;

	m_myDataFile.close();
	return true;
}

//read information from file
bool VveInfoReader::ReadSingleChunkInfo(VveDataChunkInfo *pDataChunkInfo)
{
	//check if the file is already open
	if(!m_myDataFile.is_open())
	{
		vstr::warnp() << "[VveInfoReader::ReadSingleChunkInfo] Unable to open file"; 
		return false;
	}

	int iSize;
	m_myDataFile >> iSize;

	VistaType::byte* pBuffer = new VistaType::byte [iSize];

	// read buffer
	m_myDataFile.read ( (char*)pBuffer, iSize);

	// deserialize buffer into data set information object
	VistaByteBufferDeSerializer oDeSerializer;
	//oDeSerializer.FillBuffer (pBuffer, iSize);
	oDeSerializer.SetBuffer (pBuffer, iSize, true);
	int iRet = pDataChunkInfo->DeSerialize (oDeSerializer);

	oDeSerializer.ClearBuffer();
	//delete [] pBuffer;

	m_myDataFile.close();
	return true;
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveInfoReader.cpp"                                           */
/*============================================================================*/

