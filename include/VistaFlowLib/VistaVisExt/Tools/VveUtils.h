/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEUTILS_H
#define _VVEUTILS_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include <VistaBase/VistaVectorMath.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 */
class VISTAVISEXTAPI VveConversionStuff
{
public:
	static VistaVector3D ConvertToVistaVector3D(const std::string &sValue);
	static VistaVector3D ConvertToVistaVector3D(const std::string &sValue, char cSep);

	static VistaQuaternion ConvertToVistaQuaternion(const std::string &sValue);
	static VistaQuaternion ConvertToVistaQuaternion(const std::string &sValue, char cSep);

	static std::string ConvertToString(const VistaVector3D &v3Value);
	static std::string ConvertToString(const VistaQuaternion &qValue);
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* INLINE FUNCTIONS                                                           */
/*============================================================================*/

#endif // !defined(_VVEUTILS_H)
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
