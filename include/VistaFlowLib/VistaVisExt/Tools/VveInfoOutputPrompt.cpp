/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveInfoOutputPrompt.h"
#include "VveHistogram.h"
#include "VveDataChunkInfo.h"
#include "VveDataFieldInfo.h"

#include <VistaBase/VistaStreamUtils.h>

#include <iostream>

using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveInfoOutputPrompt::VveInfoOutputPrompt(){}
VveInfoOutputPrompt::~VveInfoOutputPrompt(){}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

void VveInfoOutputPrompt::WriteChunkInfo (VveDataChunkInfo* pInfo, const std::string & sPrefix)
{
	vstr::outi() <<sPrefix << "There are " << pInfo->GetNumPoints() << " points at this chunk." << endl;
	vstr::outi() <<sPrefix << "There are " << pInfo->GetNumCells() << " cells at this chunk.\n" << endl;
	vstr::outi() <<sPrefix << "Bounds:	 ";
	for(int iBound = 0; iBound < 6; ++iBound)
		vstr::outi() <<sPrefix << pInfo->GetIthBound(iBound) << " ";
	vstr::outi() <<sPrefix << endl << endl;

	const int nNumFieldInfo = static_cast<int>(pInfo->GetNumFieldInfo());
	for(int k = 0; k < nNumFieldInfo; ++k)
	{
		VveDataFieldInfo *pFieldInfo = pInfo->GetFieldInfo(k);
		vstr::outi() <<sPrefix << pFieldInfo->GetFieldName() << ":" << endl;
		vstr::outi() <<sPrefix << "Type     : " << pFieldInfo->GetType() << endl;
		vstr::outi() <<sPrefix << "NumComps : " << pFieldInfo->GetNumberOfComponents() << endl;
		vstr::outi() <<sPrefix << "Minima   : ";
		for(int c=0; c<pFieldInfo->GetNumberOfComponents(); ++c)
		{
			vstr::outi() <<sPrefix << pFieldInfo->GetMin(c);
			vstr::outi() <<sPrefix << (c==pFieldInfo->GetNumberOfComponents()-1 ? "\n" : ", ");
		}
		vstr::outi() <<sPrefix << "Maxima   : ";
		for(int c=0; c<pFieldInfo->GetNumberOfComponents(); ++c)
		{
			vstr::outi() <<sPrefix << pFieldInfo->GetMax(c);
			vstr::outi() <<sPrefix << (c==pFieldInfo->GetNumberOfComponents()-1 ? "\n" : ", ");
		}
		vstr::outi() <<sPrefix << "Averages : ";
		for(int c=0; c<pFieldInfo->GetNumberOfComponents(); ++c)
		{
			vstr::outi() <<sPrefix << pFieldInfo->GetAverage(c);
			vstr::outi() <<sPrefix << (c==pFieldInfo->GetNumberOfComponents()-1 ? "\n" : ", ");
		}

		//histogram info
		if(pInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints() != 0)
		{
			int iBins = pInfo->GetFieldInfo(k)->GetHistogram()->GetNumBins();

			vstr::outi() <<sPrefix << "Histogram:	" << iBins << " bins, ";
			for(int j = 0; j < iBins; ++j)
				vstr::outi() <<sPrefix << pInfo->GetFieldInfo(k)->GetHistogram()->GetBinCount(j) << " ";
			vstr::outi() <<sPrefix << "Max Bin Count: " << pInfo->GetFieldInfo(k)->GetHistogram()->GetMaxBinCount() << endl;
		}
		vstr::outi() << endl;
	}
}

//write the information on the screen
void VveInfoOutputPrompt::WriteInfo(VveDataSetInfo *pDataSetInfo)
{
	int iMinTimeLevel = pDataSetInfo->GetMinTimeStep();
	int iMaxTimeLevel = pDataSetInfo->GetMaxTimeStep();

	int iNumTimeLevels = iMaxTimeLevel - iMinTimeLevel + 1;
	vstr::outi() << pDataSetInfo->GetDataSetName() << " has " << iNumTimeLevels <<" \n\n" << endl;


	//information about each single step
	int iTimeIndex = 0;
	for(int i = iMinTimeLevel; i <= iMaxTimeLevel; ++i)
	{
		vstr::outi() << "Information at time step " << i << ":\n" << endl;

		iTimeIndex = i - iMinTimeLevel;

		this->WriteChunkInfo (pDataSetInfo->GetChildChunkInfo(iTimeIndex), "\t");

		const int nNumChildChunkInfo = static_cast<int>(
			pDataSetInfo->GetChildChunkInfo(iTimeIndex)->GetNumChildChunkInfo());
		for (int j=0; j < nNumChildChunkInfo; j++)
		{
			vstr::outi() << "\t Information at block " << j << ":\n" << endl;
			this->WriteChunkInfo (pDataSetInfo->GetChildChunkInfo(iTimeIndex)->GetChildChunkInfo(j), "\t\t");
		}

		vstr::outi() << endl;
	}

	//information about the whole data set
	vstr::outi() << "Information about the whole data set:\n" << endl;
	this->WriteChunkInfo (pDataSetInfo, "");
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveInfoOutputPrompt.cpp"                                     */
/*============================================================================*/

