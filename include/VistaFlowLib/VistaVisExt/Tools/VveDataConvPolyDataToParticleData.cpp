/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
// ViSTA VisExt
#include "VveDataConvPolyDataToParticleData.h"
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>

// vtk
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkCell.h>

// ViSTA
#include <VistaAspects/VistaAspectsUtils.h>

#include <string>
#include <VistaBase/VistaVector3D.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
enum DATA_SOURCES
{
    VELOCITY,
    VELOCITY_MAGNITUDE,
    SCALAR,
    FIXED_VALUE
};


/*============================================================================*/
/*                                                                            */
/*  CONSTRUCTOR / DESTRUCTOR                                                  */
/*                                                                            */
/*============================================================================*/
VveDataConvPolyDataToParticleData::VveDataConvPolyDataToParticleData(
	VveUnsteadyVtkPolyData *pSource, VveParticleDataCont *pDest)
:	m_pSource(pSource)
,	m_pDest(pDest)
,	m_bParticleExt(false)
{ }

VveDataConvPolyDataToParticleData::~VveDataConvPolyDataToParticleData()
{ }

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetParticleExtention                                        */
/*                                                                            */
/*============================================================================*/
void VveDataConvPolyDataToParticleData::SetParticleExtention(
	const std::list<std::string> &liExtFieldnames)
{
	m_liExtFieldnames = liExtFieldnames;
	m_bParticleExt = true;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Update                                                      */
/*                                                                            */
/*============================================================================*/
void VveDataConvPolyDataToParticleData::Update()
{
#ifdef DEBUG
	vstr::debugi() << " [VveDaCoPolyToPart] - updating destination data..." << endl;
#endif

	// first of all, lock destination data...
	m_pDest->GetData()->LockData();

	VveParticlePopulation *pParts = m_pDest->GetData()->GetData();

	// delete the old population
	pParts->Clear();
    int iTrajectoryCount = 0;

    // find out about some time information...
	VveTimeMapper *pTimeMapper = m_pSource->GetTimeMapper();

    float aFixedVelocity[3] = { 0, 0, 0 };
    float fFixedScalar = 0.0f;
    float fFixedRadius = 1.0f;
    int iVelocitySource = VELOCITY;
    int iScalarSource = SCALAR;
    int iRadiusSource = SCALAR;

	double dDeltaTime = m_oProps.GetValueOrDefault<double>( "PATHLINE_TIME_DELTA", 0.0 );
	double dOffset = m_oProps.GetValueOrDefault<double>( "PATHLINE_TIME_OFFSET", 0.0 );

	std::string strTemp;
	if( m_oProps.GetValue( "PARTICLE_VELOCITY", strTemp ) )
	{
		strTemp = VistaConversion::StringToUpper(strTemp);

		if (strTemp == "VELOCITY")
		{
			iVelocitySource = VELOCITY;
		}
		else if (strTemp == "FIXED")
		{
			iVelocitySource = FIXED_VALUE;
		}
	}

	if (m_oProps.GetValue("PARTICLE_SCALAR", strTemp))
	{
		strTemp = VistaConversion::StringToUpper(strTemp);

		if (strTemp == "SCALAR")
		{
			iScalarSource = SCALAR;
		}
		else if (strTemp == "VELOCITY_MAGNITUDE")
		{
			iScalarSource = VELOCITY_MAGNITUDE;
		}
		else if (strTemp == "FIXED")
		{
			iScalarSource = FIXED_VALUE;
		}
	}

	if (m_oProps.GetValue("PARTICLE_RADIUS", strTemp))
	{
		strTemp = VistaAspectsConversionStuff::ConvertToUpper(strTemp);

		if (strTemp == "SCALAR")
		{
			iRadiusSource = SCALAR;
		}
		else if (strTemp == "VELOCITY_MAGNITUDE")
		{
			iRadiusSource = VELOCITY_MAGNITUDE;
		}
		else if (strTemp == "FIXED")
		{
			iRadiusSource = FIXED_VALUE;
		}
	}

	// we can ignore returntype here - they have been default-initialized before
	// note. we could also use GetValueOrDefault for some @DRCLUSTER@
	m_oProps.GetValueAsArray<3>( "PARTICLE_FIXED_VELOCITY", aFixedVelocity );

	m_oProps.GetValue( "PARTICLE_FIXED_SCALAR", fFixedScalar );

	m_oProps.GetValue( "PARTICLE_FIXED_RADIUS", fFixedRadius );

	for (int i=0; i<pTimeMapper->GetNumberOfTimeLevels(); ++i)
	{
        int iCurrVelSrc = iVelocitySource;
        int iCurrScalarSrc = iScalarSource;
        int iCurrRadSrc = iRadiusSource;

		// hm, this is difficult: a time level might be used for several
		// time indices, so we just have to retrieve the first one...
		int iTimeIndex = -1;
		int iTimeLevel = pTimeMapper->GetTimeLevelForLevelIndex(i);

		for (int j=0; j<pTimeMapper->GetNumberOfTimeIndices(); ++j)
		{
			if (pTimeMapper->GetTimeLevel(j) == iTimeLevel)
			{
				iTimeIndex = j;
				break;
			}
		}
		if (iTimeIndex < 0)
			continue;

		float fStartTime = pTimeMapper->GetSimulationTime(iTimeIndex);

		VveVtkPolyDataItem *pLevelData = m_pSource->GetTypedLevelDataByLevelIndex(i);
		pLevelData->LockData();

		vtkPolyData *pPolys = pLevelData->GetData();
		if (pPolys==NULL)
		{
			pLevelData->UnlockData();
			continue;
		}

		vtkPointData *pData = pPolys->GetPointData();
		vtkDataArray *pVectors = NULL;
		vtkDataArray *pScalars = NULL;
		vtkDataArray **pExtensionData = NULL;
		
		if (pData)
		{
			pVectors = pData->GetVectors();
			pScalars = pData->GetScalars();

			if(m_bParticleExt)
			{
					// extention_fields: value0, value1, value2, vector0, vector1, vector2
					pExtensionData = reinterpret_cast<vtkDataArray**>(
						malloc(6*sizeof(vtkDataArray*)));
					int i=0;
					for(std::list<std::string>::iterator it = m_liExtFieldnames.begin();
						it != m_liExtFieldnames.end(); ++it)
					{
						pExtensionData[i] = pData->GetArray((*it).c_str());
						if(pExtensionData[i] == NULL)
						{
							vstr::errp() << "[VveDataConvPolyDataToParticleData] given tensor field not found" << endl;
							m_bParticleExt = false;
							break;
						}
						++i;
					}
				}
			}

        if (!pVectors)
        {
            if (iCurrVelSrc == VELOCITY)
            {
                iCurrVelSrc = FIXED_VALUE;
            }

            if (iCurrScalarSrc == VELOCITY_MAGNITUDE)
            {
                iCurrScalarSrc = FIXED_VALUE;
            }

            if (iCurrRadSrc == VELOCITY_MAGNITUDE)
            {
                iCurrRadSrc = FIXED_VALUE;
            }
        }

        if (!pScalars)
        {
            if (iCurrScalarSrc == SCALAR)
            {
                iCurrScalarSrc = FIXED_VALUE;
            }

            if (iCurrRadSrc == SCALAR)
            {
                iCurrRadSrc = FIXED_VALUE;
            }
        }

        float fVelMag = 0.0f;
        double aTemp[3];

		if (dDeltaTime >= 0 )
		{
			// if we have a valid delta time and valid cells, 
			// traverse the cell list and add a pathline for every cell
			int iCellCount = pPolys->GetNumberOfCells();

			for (int j=0; j<iCellCount; j++, iTrajectoryCount++)
			{
					vtkCell *pCell = pPolys->GetCell(j);
					int iPointCount = pCell->GetNumberOfPoints();
					float fTime = fStartTime;

                // create particle data arrays and add to a new particle trajectory
                VveParticleTrajectory* pTraj = new VveParticleTrajectory();

                VveParticleDataArray<float> *aPos = new VveParticleDataArray<float>(3, VveParticlePopulation::sPositionArrayDefault);
                pTraj->AddArray(aPos);
                VveParticleDataArray<float> *aTime = new VveParticleDataArray<float>(1, VveParticlePopulation::sTimeArrayDefault);
                pTraj->AddArray(aTime);
                VveParticleDataArray<float> *aVel = new VveParticleDataArray<float>(3, VveParticlePopulation::sVelocityArrayDefault);
                pTraj->AddArray(aVel);
                VveParticleDataArray<float> *aScalar = new VveParticleDataArray<float>(1, VveParticlePopulation::sScalarArrayDefault);
                pTraj->AddArray(aScalar);
                VveParticleDataArray<float> *aRadius = new VveParticleDataArray<float>(1, VveParticlePopulation::sRadiusArrayDefault);
                pTraj->AddArray(aRadius);

				VveParticleDataArray<float> *aEVal = NULL, *aEVec = NULL;
				if(m_bParticleExt)
				{
					aEVal = new VveParticleDataArray<float>(3, VveParticlePopulation::sEigenValueArrayDefault);
					pTraj->AddArray(aEVal);
					aEVec = new VveParticleDataArray<float>(9, VveParticlePopulation::sEigenVectorArrayDefault);
					pTraj->AddArray(aEVec);
				}

                // Resize arrays
                pTraj->Resize(iPointCount);
                
					for (int k=0; k<iPointCount; ++k, fTime += dDeltaTime)
					{
						int iPointId = pCell->GetPointId(k);
					pPolys->GetPoint(iPointId, aTemp);
                    aPos->SetElement(k, aTemp);

                        if (iCurrVelSrc == FIXED_VALUE)
                        {
                        aVel->SetElement(k, aFixedVelocity);
                        }
                        else
                        {
						pVectors->GetTuple(iPointId, aTemp);
                        aVel->SetElement(k, aTemp);
                        }

                        if (iCurrScalarSrc == VELOCITY_MAGNITUDE
                            || iCurrRadSrc == VELOCITY_MAGNITUDE) 
                        {
						pVectors->GetTuple(iPointId, aTemp);
                        fVelMag = sqrt(aTemp[0]*aTemp[0] + aTemp[1]*aTemp[1] + aTemp[2]*aTemp[2]);
                        }

                        if (iCurrScalarSrc == VELOCITY_MAGNITUDE)
                        {
                        aScalar->SetElementValue(k, fVelMag);
                        }
                        else if (iCurrScalarSrc == FIXED_VALUE)
                        {
                        aScalar->SetElementValue(k, fFixedScalar);
                        }
                        else
                        {
                        aScalar->SetElementValue(k, pScalars->GetComponent(iPointId, 0));
                        }

                        if (iCurrRadSrc == VELOCITY_MAGNITUDE)
                        {
                        aRadius->SetElementValue(k, fVelMag);
                        }
                        else if (iCurrRadSrc == FIXED_VALUE)
                        {
                        aRadius->SetElementValue(k, fFixedRadius);
                        }
                        else
                        {
                        aRadius->SetElementValue(k, pScalars->GetComponent(iPointId, 0));
                        }
						
                    aTime->SetElementValue(k, fTime + dOffset);

					if(m_bParticleExt)
					{
						// load particle extension: Eigenvalues
						aTemp[0] = static_cast<float>(pExtensionData[0]->GetComponent(iPointId,0));
						aTemp[1] = static_cast<float>(pExtensionData[1]->GetComponent(iPointId,0));
						aTemp[2] = static_cast<float>(pExtensionData[2]->GetComponent(iPointId,0));
						aEVal->SetElement(k, aTemp);

						// load particle extension: Eigenvectors
						float aEV[9];
						for (int n=0; n<3; n++)
						{
							pExtensionData[3+n]->GetTuple(iPointId, aTemp);
							aEV[3*n+0] = static_cast<float>( aTemp[0] );
							aEV[3*n+1] = static_cast<float>( aTemp[1] );
							aEV[3*n+2] = static_cast<float>( aTemp[2] );
                    }
						aEVec->SetElement(k, aEV);
                }
            }
                pParts->AddTrajectory(pTraj);
        }
        }
        else
        {
            // otherwise add a particle for every point
            int iPointCount = pPolys->GetNumberOfPoints();
            
            for (int j=0; j<iPointCount; ++j, ++iTrajectoryCount)
            {
                // create particle data arrays and add to a new particle trajectory
                VveParticleTrajectory* pTraj = new VveParticleTrajectory();

                VveParticleDataArray<float> *aPos = new VveParticleDataArray<float>(3, VveParticlePopulation::sPositionArrayDefault);
                pTraj->AddArray(aPos);
                VveParticleDataArray<float> *aTime = new VveParticleDataArray<float>(1, VveParticlePopulation::sTimeArrayDefault);
                pTraj->AddArray(aTime);
                VveParticleDataArray<float> *aVel = new VveParticleDataArray<float>(3, VveParticlePopulation::sVelocityArrayDefault);
                pTraj->AddArray(aVel);
                VveParticleDataArray<float> *aScalar = new VveParticleDataArray<float>(1, VveParticlePopulation::sScalarArrayDefault);
                pTraj->AddArray(aScalar);
                VveParticleDataArray<float> *aRadius = new VveParticleDataArray<float>(1, VveParticlePopulation::sRadiusArrayDefault);
                pTraj->AddArray(aRadius);

				VveParticleDataArray<float> *aEVal = NULL, *aEVec = NULL;
				if(m_bParticleExt)
				{
					aEVal = new VveParticleDataArray<float>(3, VveParticlePopulation::sEigenValueArrayDefault);
					pTraj->AddArray(aEVal);
					aEVec = new VveParticleDataArray<float>(9, VveParticlePopulation::sEigenVectorArrayDefault);
					pTraj->AddArray(aEVec);
				}

			    // Resize arrays to hold a single position
                pTraj->Resize(1);

                // position
                pPolys->GetPoint(j, aTemp);
                aPos->SetElement(0, aTemp);

                // velocity
                if (iCurrVelSrc == FIXED_VALUE)
                {
                    aVel->SetElement(0, aFixedVelocity);
                }
                else
                {
                    pVectors->GetTuple(j, aTemp);
                    aVel->SetElement(0, aTemp);
                }

                // velocity magnitude
                if (iCurrScalarSrc == VELOCITY_MAGNITUDE
                    || iCurrRadSrc == VELOCITY_MAGNITUDE) 
                {
                    pVectors->GetTuple(j, aTemp);
                    fVelMag = sqrt(aTemp[0]*aTemp[0] + aTemp[1]*aTemp[1] + aTemp[2]*aTemp[2]);
                }

                // scalar
                if (iCurrScalarSrc == VELOCITY_MAGNITUDE)
                {
                    aScalar->SetElementValue(0, fVelMag);
                }
                else if (iCurrScalarSrc == FIXED_VALUE)
                {
                    aScalar->SetElementValue(0, fFixedScalar);
                }
                else
                {
                    aScalar->SetElementValue(0, pScalars->GetComponent(j, 0));
                }
                
                // radius
                if (iCurrRadSrc == VELOCITY_MAGNITUDE)
                {
                    aRadius->SetElementValue(0, fVelMag);
                }
                else if (iCurrRadSrc == FIXED_VALUE)
                {
                    aRadius->SetElementValue(0, fFixedRadius);
                }
                else
                {
                    aRadius->SetElementValue(0, pScalars->GetComponent(j, 0));
                }

                // time
                aTime->SetElementValue(0, fStartTime + dOffset);

				if(m_bParticleExt)
				{
					// load particle extension: Eigenvalues
					aTemp[0] = static_cast<float>(pExtensionData[0]->GetComponent(j, 0));
					aTemp[1] = static_cast<float>(pExtensionData[1]->GetComponent(j, 0));
					aTemp[2] = static_cast<float>(pExtensionData[2]->GetComponent(j, 0));
					aEVal->SetElement(0, aTemp);

					// load particle extension: Eigenvectors
					float aEV[9];
					for (int k=0; k<3; k++)
					{
						pExtensionData[3+k]->GetTuple(j, aTemp);
						aEV[3*k+0] = static_cast<float>( aTemp[0] );
						aEV[3*k+1] = static_cast<float>( aTemp[1] );
						aEV[3*k+2] = static_cast<float>( aTemp[2] );
			}
					aEVec->SetElement(0, aEV);
		}

                // Add trajectory to particle population
                pParts->AddTrajectory(pTraj); // this is trajectory #iTrajectoryCount
			}
		}
		pLevelData->UnlockData();

		free(pExtensionData);
	} // for: pTimeMapper->GetNumberOfTimeLevels()

#ifdef DEBUG
        // Get Bounds
        float fScalarMin, fScalarMax, fRadiusMin, fRadiusMax, fTimeMin, fTimeMax;
        VistaVector3D vecVelMin, vecVelMax;
        pParts->GetBounds(VveParticlePopulation::sTimeArrayDefault,   &fTimeMin,   &fTimeMax,   1);
        pParts->GetBounds(VveParticlePopulation::sScalarArrayDefault, &fScalarMin, &fScalarMax, 1);
        pParts->GetBounds(VveParticlePopulation::sRadiusArrayDefault, &fRadiusMin, &fRadiusMax, 1);
        pParts->GetBounds(VveParticlePopulation::sVelocityArrayDefault, &vecVelMin[0], &vecVelMax[0], 3);

		vstr::outi() << " [VflDaCoPolyToPart] - particle data loaded successfully..." << endl
			 << "                       scalar range: " << fScalarMin << " - " << fScalarMax << endl
			 << "                       radius range: " << fRadiusMin << " - " << fRadiusMax << endl
			 << "                       velocity range: " << vecVelMin.GetLength() << " - " << vecVelMax.GetLength() << endl
			 << "                       time range: " << fTimeMin << " - " << fTimeMax << endl;
#endif

//	m_pDest->GetData()->Notify();
//	m_pDest->Notify();

	m_pDest->GetData()->UnlockData();
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSource                                                   */
/*                                                                            */
/*============================================================================*/
VveUnsteadyVtkPolyData *VveDataConvPolyDataToParticleData::GetSource() const
{
	return m_pSource;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDestination                                              */
/*                                                                            */
/*============================================================================*/
VveParticleDataCont *VveDataConvPolyDataToParticleData::GetDestination() const
{
	return m_pDest;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetProperties                                               */
/*                                                                            */
/*============================================================================*/

void VveDataConvPolyDataToParticleData::SetProperties (const VistaPropertyList & props)
{
	m_oProps = props;
}

/*============================================================================*/
/*  END OF FILE "VveDataConvPolyDataToParticleData.cpp"                       */
/*============================================================================*/
