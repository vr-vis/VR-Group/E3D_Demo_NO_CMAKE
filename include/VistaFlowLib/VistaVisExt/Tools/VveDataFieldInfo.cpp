/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "VveDataFieldInfo.h"
#include "VveHistogram.h"
#include "Vve2DHistogram.h"

#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaBase/VistaStreamUtils.h>

#include <vtkType.h>

#include <limits>
#include <iostream>
using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveDataFieldInfo::VveDataFieldInfo() 
	:	m_strName(""), 
		m_iNumberOfComponents(1),
		m_iType(VTK_FLOAT), 
		m_pHist(new VveHistogram ())
{
	for(int i=0; i<9; ++i)
	{
		m_fAverage[i] = 0;
		m_fStandardDeviation[i] = 0;
		m_fRange[i][0] = numeric_limits<double>::max();
		m_fRange[i][1] = -numeric_limits<double>::max();
	}
	m_vecJointHistograms.clear();

}
VveDataFieldInfo::~VveDataFieldInfo()
{
	delete m_pHist;
	for (unsigned int i=0; i < m_vecJointHistograms.size(); ++i)
	{
		delete m_vecJointHistograms[i];
		m_vecJointHistograms[i] = NULL;
	}
	m_vecJointHistograms.clear();

}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFieldName					                          */
/*                                                                            */
/*============================================================================*/
void VveDataFieldInfo::SetFieldName(const string &strFieldName)
{
	m_strName = strFieldName;
}
string VveDataFieldInfo::GetFieldName() const
{
	return m_strName;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNumberOfComponents			                          */
/*                                                                            */
/*============================================================================*/
void VveDataFieldInfo::SetNumberOfComponents(int i)
{
	m_iNumberOfComponents = i;
}
int VveDataFieldInfo::GetNumberOfComponents() const
{
	return m_iNumberOfComponents;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetType						                          */
/*                                                                            */
/*============================================================================*/
void VveDataFieldInfo::SetType(int iType)
{
	m_iType = iType;
}
int VveDataFieldInfo::GetType() const
{
	return m_iType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMax						                          */
/*                                                                            */
/*============================================================================*/
void VveDataFieldInfo::SetMax(int iComp, double fMax)
{
	m_fRange[iComp][1] = fMax;
}
double VveDataFieldInfo::GetMax(int iComp) const
{
	return m_fRange[iComp][1];
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMin						                          */
/*                                                                            */
/*============================================================================*/
void VveDataFieldInfo::SetMin(int iComp, double fMin)
{
	m_fRange[iComp][0] = fMin;
}
double VveDataFieldInfo::GetMin(int iComp) const
{
	return m_fRange[iComp][0];
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRange						                          */
/*                                                                            */
/*============================================================================*/
void VveDataFieldInfo::SetRange(int iComp, double fRange[2])
{
	m_fRange[iComp][0] = fRange[0];
	m_fRange[iComp][1] = fRange[1];
}
void VveDataFieldInfo::GetRange(int iComp, double fRange[2]) const
{
	fRange[0] = m_fRange[iComp][0];
	fRange[1] = m_fRange[iComp][1];
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetAverage					                          */
/*                                                                            */
/*============================================================================*/
void VveDataFieldInfo::SetAverage(int iComp, double fAverage)
{
	m_fAverage[iComp] = fAverage;
}
double VveDataFieldInfo::GetAverage(int iComp) const
{
	return m_fAverage[iComp];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetStandardDeviation			                          */
/*                                                                            */
/*============================================================================*/
void VveDataFieldInfo::SetStandardDeviation(int iComp, double fSD)
{
	m_fStandardDeviation[iComp] = fSD;
}
double VveDataFieldInfo::GetStandardDeviation(int iComp) const
{
	return m_fStandardDeviation[iComp];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetHistogram						                          */
/*                                                                            */
/*============================================================================*/
VveHistogram* VveDataFieldInfo::GetHistogram() const
{
	return m_pHist;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNumberOfJointHistograms		                      */
/*                                                                            */
/*============================================================================*/
void VveDataFieldInfo::SetNumberOfJointHistograms(size_t nSize)
{
	if (!m_vecJointHistograms.empty())
		return;

	m_vecJointHistograms.resize(nSize);
	for(size_t i=0; i<nSize; ++i)
		m_vecJointHistograms[i] = new Vve2DHistogram;
}

size_t VveDataFieldInfo::GetNumberOfJointHistograms() const
{
	return m_vecJointHistograms.size();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetJointHistogram						                      */
/*                                                                            */
/*============================================================================*/
Vve2DHistogram* VveDataFieldInfo::GetJointHistogram(int i) const
{
	if ((i<0)||(i >=static_cast<int>(m_vecJointHistograms.size())))
		return NULL;

	return m_vecJointHistograms[i];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Serialize								                      */
/*                                                                            */
/*============================================================================*/
int VveDataFieldInfo::Serialize(IVistaSerializer & oSerializer) const
{
	int iNumBytes = 0;

#ifdef DEBUG
	//vstr::debugi() << "\t Serialize field info "<<m_strName<<"...\n";
#endif

	int iRet = oSerializer.WriteDelimitedString(this->GetSignature());
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = oSerializer.WriteInt32((unsigned int) m_strName.size());
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = oSerializer.WriteString(m_strName);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = oSerializer.WriteInt32(m_iType);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = oSerializer.WriteInt32(m_iNumberOfComponents);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	for (int k=0; k < m_iNumberOfComponents; ++k)
	{
		iRet = oSerializer.WriteDouble(m_fRange[k][0]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;
		iRet = oSerializer.WriteDouble(m_fRange[k][1]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;
		iRet = oSerializer.WriteDouble(m_fAverage[k]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;
		iRet = oSerializer.WriteDouble(m_fStandardDeviation[k]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;
	}

	iRet = oSerializer.WriteSerializable (*m_pHist);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = oSerializer.WriteInt32((unsigned int) m_vecJointHistograms.size());
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	for (unsigned int i=0; i < m_vecJointHistograms.size(); ++i)
	{
		iRet = oSerializer.WriteSerializable (*m_vecJointHistograms[i]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;

	}

#ifdef DEBUG
	//vstr::debugi() << "\t... field finished ("<<iNumBytes<<" bytes)\n";
#endif

	return iNumBytes;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeSerialize								                  */
/*                                                                            */
/*============================================================================*/
int VveDataFieldInfo::DeSerialize(IVistaDeSerializer & oDeSerializer)
{
	int iNumBytes = 0;

#ifdef DEBUG
	//vstr::debugi() << "\t Deserialize field info...\n";
#endif

	string str("");
	int iRet = oDeSerializer.ReadDelimitedString (str);
	if(iRet == -1 || str != this->GetSignature())
		return -1;

	iNumBytes += iRet+1;

	int iStrSize = 0;
	iRet = oDeSerializer.ReadInt32(iStrSize);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = oDeSerializer.ReadString(m_strName, iStrSize);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = oDeSerializer.ReadInt32(m_iType);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = oDeSerializer.ReadInt32(m_iNumberOfComponents);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	for (int k=0; k < m_iNumberOfComponents; ++k)
	{
		iRet = oDeSerializer.ReadDouble(m_fRange[k][0]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;
		iRet = oDeSerializer.ReadDouble(m_fRange[k][1]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;
		iRet = oDeSerializer.ReadDouble(m_fAverage[k]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;
		iRet = oDeSerializer.ReadDouble(m_fStandardDeviation[k]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;
	}

	// histogram must exist
	assert (m_pHist!=NULL);
		
	iRet = oDeSerializer.ReadSerializable (*m_pHist);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	// read number of saved joint histograms
	unsigned int iNumJointHistograms = 0;
	iRet = oDeSerializer.ReadInt32(iNumJointHistograms);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	// joint histograms must be empty
	assert (m_vecJointHistograms.size() == 0);

	// create vector of joint 2D histograms
	this->SetNumberOfJointHistograms(iNumJointHistograms);

	for (unsigned int i=0; i < m_vecJointHistograms.size(); ++i)
	{
		iRet = oDeSerializer.ReadSerializable (*m_vecJointHistograms[i]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;

	}

#ifdef DEBUG
	//vstr::debugi() << "\t ... field finished ("<<iNumBytes<<" bytes)\n";
#endif

	return iNumBytes;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSignature							                      */
/*                                                                            */
/*============================================================================*/
std::string VveDataFieldInfo::GetSignature() const
{
	return "__VveDataFieldInfo__";
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveDataFieldInfo.cpp"                                        */
/*============================================================================*/

