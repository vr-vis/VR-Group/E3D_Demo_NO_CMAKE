/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VTKPOLYDATATOOLS_H
#define _VTKPOLYDATATOOLS_H
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <set>
#include <vector>
#include <map>

#include <vtkSystemIncludes.h>
#include <vtkPolyData.h>
#include <vtkIdList.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkPolyData;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* A little helper class gathering some tools often needed, when dealing with vtkPolyData
*
* NOTE: All of the algorithms integrated so far rely on the following ASSUMPTIONS:
*    1) Links and cells have already been built using vtkPolyData::BuildCells and vtkPolyData::BuildLinks
*    2) Mesh contains only triangles i.e. this stuff WILL NOT work correctly on arbitrary 
*       polygonal meshes containing e.g. quadrilaterals or lines or even drawable vertices...
*    3) Mesh is non-manifold i.e. contains no complex edges or vertices
*
* NOTE: Some members are held in order to prevent reallocating local variables over and over again.
*       This is done solely for performance purposes, when using this in high frequency calls. 
*       In order to prevent threading problems the routines are thus NOT provides as static methods.
*/
class VISTAVISEXTAPI VtkPolyDataTools
{
public:
    VtkPolyDataTools(vtkPolyData* pMesh);
    virtual ~VtkPolyDataTools();
    /**
    * Compute the so called one-ring-neighborhood of a given vertex v in a mesh pMesh.
    * Returns a set of vertices which are connected to v via an edge in pMesh.
    *
    * @PARAM    iVertexId      ID of the vertex for which to compute the neighborhood
    * @param[out]   setNeighbors   set of neighboring vertices IDs excluding the vertex itself
    *
    * @return   bool      Output correct?
    */
    void ComputeVertexNeighbors(vtkIdType iVertexId, std::set<vtkIdType>& setNeighbors);
    /**
    * Compute the so called one-ring-neighborhood of a given vertex v in a mesh pMesh.
    * Returns a set of vertices which are connected to v via an edge in pMesh.
    *
    * @PARAM    iVertexId      ID of the vertex for which to compute the neighborhood
    * @param[out]    pNeighbors     list of neighboring vertices IDs excluding the vertex itself
    *
    * @return   bool      Output correct?
    */
    void ComputeVertexNeighbors(vtkIdType iVertexId, vtkIdList* pNeighbors);

    /**
    * Compute the set of cells incident to the given edge
    * 
    * @PARAM    iVertex1    edge's first defining vertex
    * @PARAM    iVertex2    edge's second defining vertex
    * @param[out]   pCells      IDList containing the IDs of incident cells
    * @return   int         number of incident cells for the given edge
    * NOTE: For a two manifold mesh this should always return values <= 2
    */
    int GetEdgeIncidentCells(vtkIdType iVertex1, vtkIdType iVertex2, vtkIdList* pCells);

    /**
    * Compute the ONE (unique) neighbor along the given edge
    *
    * @param    iCellId     Id of input cell
    * @PARAM    iVertex1    edge's first defining vertex
    * @PARAM    iVertex2    edge's second defining vertex
    *
    * @return -1 if there is no or more than one such cell along the given edge
    *         cell id of neighboring cell iff this could be determined uniquely  
    */
    vtkIdType GetCellEdgeNeighbor(vtkIdType iTriId, vtkIdType iVertex1, vtkIdType iVertex2);
    
    /**
    * check if the edge specified by v1,v2 is a boundary edge i.e. if there is exactly
    * one triangle connected to it
    *
    * @PARAM    iVertex1   edge's first defining vertex
    * @PARAM    iVertex2   edge's second defining vertex
    * @return   bool      <true> IFF edge is boundary (ELSE <false> particularly if (iV1,iV2) 
    *                   does not define an edge at all
    */
    bool IsBoundaryEdge(vtkIdType iVertex1, vtkIdType iVertex2);

    /**
    * Check if the given cell is a boundary cell i.e. has at least one boundary edge.
    *
    * @PARAM    iCell       ID of the cell to check
    * @return   bool        true IFF cell is boundary
    */
    bool IsBoundaryCell(vtkIdType iCell);

    /**
    * Determine the boundary edges of a given cell. First determines all edges and then 
    * sorts out the ones not beeing "boundary".
    *
    * @PARAM    iCell       ID of the cell to check
    * @param[out]   vector<...> vector of point id pairs designating the cell's B-edges
    * @return   size_t         number of boundary edges of the given cell
    */
    size_t GetCellBoundaryEdges(vtkIdType iCell, std::vector< std::pair<vtkIdType,vtkIdType> >& vecEdges);

    /**
    * Determine the edges of a given cell. Runs in O(n^2) by checking all n*(n-1)/2 possible
    * point to point connection.
    *
    * @PARAM    iCell       ID of the cell to check
    * @param[out]   vector<...> vector of point id pairs designating the cell's edges
    */
    size_t GetCellEdges(vtkIdType iCell, std::vector< std::pair<vtkIdType,vtkIdType> >& vecEdges);

    /**
    * Compute the least squares fitting plane through the given set of points
    *
    * @PARAM setPoints      set of input points for which to compute the plane
    * @param[out] fPlaneCenter  the point set COG
    * @param[out] fPlaneNormal  the normal of the least squares plane through the point set
    */
    void ComputeLeastSquaresPlane(const std::set<vtkIdType>& setPoints, double fPlaneCenter[3], double fPlaneNormal[3]);
    /**
    * Compute the least squares fitting plane through the given set of points
    *
    * @PARAM    iSetSize        number of point ids passed along in pSet
    * @PARAM    pSet            array of point IDs
    * @param[out]   fPlaneCenter    the point set COG
    * @param[out]   fPlaneNormal    the normal of the least squares plane through the point set
    */
    void ComputeLeastSquaresPlane(const vtkIdType iSetSize, vtkIdType* pSet, double fPlaneCenter[3], double fPlaneNormal[3]);
    
    /**
    * Compute the ordered sequence of points which define a boundary of the underlying
    * mesh. It is assumed that the set of all boundary vertices is given in
    * setBoundaryPoints. The output is provided in pBoundaryLoop. It will
    * contain an array of n+1 indices, i.e. a closed loop for which pBL[0]==pBL[n].
    *
    * NOTE: There is currently no error checking for the loop condition. It is assumed
    *       that the input is a two-manifold mesh with boundary, where each boundary
    *       vertex has EXACTLY TWO incident boundary edges! If this is not the 
    *       case calls to this routine might lead to an endless loop!
    *
    *   @TODO: Add some sophisicated (i.e. FAST) error detection and recovery strategy!
    *
    * @PARAM    setBoundaryPoints       set of points on the boundary
    * @param[out]   pBoundaryLoop           ordered sequence of boundary points forming the B-Loop
    *                                   The resulting array consists of setBoundaryPoints.size()
    *                                   many entries and memory is allocated appropriately
    *                                   NOTE: Caller HAS TO CALL delete[] on pBoundaryLoop
    *                                   to avoid mem leaks!
	* @return	true	if correct boundary loop could be assembled
	*			false	if algorithm was unable to resolve correct ordering OR if input did
	*					not fulfill the loop condition.
    */
    bool ComputeBoundaryPolygon(const std::set<vtkIdType>& setBoundaryPoints, vtkIdType*& pBoundaryLoop);

    /**
    * Project the given points of the underlying mesh into the plane spec. by fCenter and fNormal.
    * An additional relaxation factor can be given to controll the degree of translation applied to
    * each point. A relaxation value of 1.0f yields total projection into the plane whereas a factor
    * of 0.0 yields no translation at all.
    *
    * @PARAM    setPoints       set of point ids designating input points to be projected
    * @PARAM    fCenter         base point for projection plane
    * @PARAM    fNormal         projection plane's normal
    * @PARAM    fRelaxation     relaxation factor for projection
    */
    void ProjectPointsToPlane(  std::set<vtkIdType> setPoints, 
                                double fCenter[3], double fNormal[3], 
                                double fRelaxation = 1.0f);

    /**
    * Project the given points of the underlying mesh into the plane spec. by fCenter and fNormal.
    * An additional relaxation factor can be given to controll the degree of translation applied to
    * each point. A relaxation value of 1.0f yields total projection into the plane whereas a factor
    * of 0.0 yields no translation at all.
    *
    * @PARAM    iSetSize        size of point set to be projected
    * @PARAM    pSet            array of point ids designating input points to be projected (length == iSetSize)
    * @PARAM    fCenter         base point for projection plane
    * @PARAM    fNormal         projection plane's normal
    * @PARAM    fRelaxation     relaxation factor for projection
    */
    void ProjectPointsToPlane(  const vtkIdType iSetSize, vtkIdType *pSet,
                                double fCenter[3], double fNormal[3], 
                                double fRelaxation);
    
    /**
    * Given the triangle iTriId and its edge (iA, iB) this checks if an edge flip is 
    * useful and possible.
    * It is <b>useful</b> iff the minimal inner angle of the triangles is smaller than for the 
    * unflipped configuration.
    * It is <b>possible</b> iff neither the two inner angles currently adjacent to iA nor those adjacent to iB 
    * sum up to more than 180� 
    */
    bool ShouldEdgeBeFlipped(vtkIdType iTriId, vtkIdType iA, vtkIdType iB);
    
    /**
    * Given two triangles (iA,iB,iC) and (iA,iD,iB), this method checks if flipping
    * of edge (iA,iB) is useful and possible. 
    * It is <b>useful</b> iff the minimal inner angle of the triangles is smaller than for the 
    * unflipped configuration.
    * It is <b>possible</b> iff neither the two inner angles currently adjacent to iA nor those adjacent to iB 
    * sum up to more than 180� 
    * NOTE: This ASSUMES that (iA,iB,iC) and (iA,iD,iB) are valid triangles in the current mesh
    *       It doesn't check on this, so be careful!
    */
    bool ShouldEdgeBeFlipped(vtkIdType iA, vtkIdType iB, vtkIdType iC, vtkIdType iD);
    
    /**
    * check if a given edge flip is legal i.e. if
    * the sum of angles iA and iB is < 180� each.
    */
    bool IsEdgeFlippable(vtkIdType iA, vtkIdType iB, vtkIdType iC, vtkIdType iD);

    /**
    * Swap two triangles along the given edge
    * First compute the apex vertex C over edge (iA,iB) in triangle iTriId and then compute the
    * neighbor triangle along (iA,iB) and neighboring apex D. Finally, flip the edge (iA,iB) to
    * (C,D). 
    * This ASSUMES that the edge can be flipped legally, so make sure you check beforehand!
    *
    * @return vtkIdType neighbor triangle's index iff flip ocurred else -1
    */
    vtkIdType FlipEdge(vtkIdType iTriId, vtkIdType iA, vtkIdType iB);

    /**
    * Given two triangles iTriID1(iA,iB,iC) and iTriID2(iA,iD,iB), this method flips the edge (iA, iB) to
    * (iC,iD) resulting in the two triangles (iA,iD,iC) and (iB,iC,iD).
    * This ASSUMES that the edge can be flipped legally, so make sure you check beforehand!
    */
    void FlipEdge(vtkIdType iTriID1, vtkIdType iTriID2, vtkIdType iA, vtkIdType iB, vtkIdType iC, vtkIdType iD);
    
    /**
    *
    */
    bool ComputeFlipIds(vtkIdType iTriId, vtkIdType iFlipA, vtkIdType iFlipB, vtkIdType &iNeighTri, vtkIdType &iMyApex, vtkIdType &iNeighApex);

    /**
    * Split the given triangle along a given edge's midpoint, thereby creating two new triangles. 
    * If there is an adjacent triangle along the given edge (i.e. the edge is not 
    * boundary edge), this triangle will also be split into half, resulting in a total of 
    * four triangles (two for the split original one and another two for its neighbor).
    * The newly inserted point's id is returned and can be used e.g. to modify the 
    * point's coordinates.
    *
    * NOTE: Assumes that the given mesh is two manifold!
    *
    * @return vtkIdType PointID of the newly inserted vertex (-1 if failed!)
    */
    vtkIdType SplitTriangle(vtkIdType iTriID, vtkIdType iV1, vtkIdType iV2);
    
    /**
    * Compute the longest edge of the given triangle
    */
    float ComputeLongestEdge(vtkIdType iTriID, vtkIdType& iVertex1, vtkIdType& iVertex2);

    /**
    * Compute the midpoint of the given edge
    */
    void ComputeEdgeMidPoint(vtkIdType iPt1ID, vtkIdType iPt2ID, double x[3]);

    /**
    * Compute the inner angles (in radians) in the triangle given by cellID 
    * fAngles[i] will contain the angle @ point i hereafter
    */
    void ComputeInnerAngles(int iCellID, double fAngles[3]);
    void ComputeInnerAngles(vtkIdType iPtIDs[3], double fAngles[3]);

    /**
    *
    */
    vtkIdType GetApexVertex(vtkIdType iTriangle, vtkIdType iV1, vtkIdType iV2);
    /**
    * @TODO
    * Check the two-manifoldness of the underlying input
    */
    bool CeckTwoManifoldness(){
        return true;
    }

    /**
    * @TODO
    * Enforce the two-manifoldness of the underlying input
    * This will be a rather crude implementation. Any triangle belonging to a complex vertex 
    * or edge will be deleted. Note that this heavily depends on the ordering in which
    * the triangle structure is traversed, e.g. the first two triangles incident to an
    * edge will be considered valid whereas the third one will be considered as a complex 
    * triangle and thus be deleted.
    */
    void EnforceTwoManifoldness(){}
    /**
    * Compute the area of the given triangle. Currently this routing assumes that the 
    * given input mesh contains ONLY TRIANGLES, i.e. it will not work properly on any
    * other cell type. Use vtkTriangleFilter to create a proper input!
    * This routine ASSERTs that the given vtkCellArray contains triangles only i.e.
    * your program will crash if you pass the wrong input...
    */
    float ComputeTriangleArea(vtkIdType iTriID);
private:
    /**
    * update triangle connectivity in order to split triangle iTriID along the edge (iV1,iV2) and 
    * replace it by two other ones: (iV1,iNewID, iV3) and (iV2, iV3, iNewID), where iV3
    * designated the triangle's third vertex. The first of these two triangles takes the 
    * id of the original one. For the latter one a new triangle id is created and returned 
    * by this function.
    * Assumes that edge (iV1, iV2) is given in CCW order!
    */
    vtkIdType SplitTriangle(vtkIdType iTriID, vtkIdType iV1, vtkIdType iV2, vtkIdType iNewID);
    
    vtkPolyData* m_pMesh;
    /*
    * Some common variables -> no need to always reallocate 'em when dealing with high 
    * frequency calls
    */
    vtkIdType *m_pV1Cells;
    unsigned short m_iV1NumCells;
    
    vtkIdType *m_pV2Cells;
    unsigned short m_iV2NumCells;

    vtkIdType m_iNumPts;
    vtkIdType *m_pCellPts;

    vtkIdList *m_pTmpIds;

    std::vector< std::pair<vtkIdType,vtkIdType> > m_vecTmpEdges;
};
/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsBoundaryEdge                                              */
/*                                                                            */
/*============================================================================*/
inline bool VtkPolyDataTools::IsBoundaryEdge(vtkIdType iVertex1, vtkIdType iVertex2)
{
    return this->GetEdgeIncidentCells(iVertex1,iVertex2,m_pTmpIds) == 1;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   IsBoundaryCell                                              */
/*                                                                            */
/*============================================================================*/
inline bool VtkPolyDataTools::IsBoundaryCell(vtkIdType iCell)
{
    return this->GetCellBoundaryEdges(iCell, m_vecTmpEdges) > 0;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCellEdgeNeighbor                                         */
/*                                                                            */
/*============================================================================*/
inline vtkIdType VtkPolyDataTools::GetCellEdgeNeighbor(vtkIdType iTriId, vtkIdType iVertex1, vtkIdType iVertex2)
{
    int iNumNeighs = this->GetEdgeIncidentCells(iVertex1,iVertex2,m_pTmpIds);
    if(iNumNeighs != 2)
        return -1;
    return (m_pTmpIds->GetId(0) == iTriId ? m_pTmpIds->GetId(1) : m_pTmpIds->GetId(0));
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :      ShouldEdgeBeFlipped                                      */
/*                                                                            */
/*============================================================================*/
inline bool VtkPolyDataTools::ShouldEdgeBeFlipped(vtkIdType iTriId, vtkIdType iA, vtkIdType iB)
{
    vtkIdType iApex , iNeighId, iNeighApex;
    if(!this->ComputeFlipIds(iTriId, iA, iB, iNeighId, iApex, iNeighApex))
        return false;
    return this->ShouldEdgeBeFlipped(iA, iB, iApex, iNeighApex);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :       FlipEdge                                                */
/*                                                                            */
/*============================================================================*/
inline vtkIdType VtkPolyDataTools::FlipEdge(vtkIdType iTriId, vtkIdType iA, vtkIdType iB)
{
    vtkIdType iApex , iNeighId, iNeighApex;
    if(!this->ComputeFlipIds(iTriId, iA, iB, iNeighId, iApex, iNeighApex))
        return -1;
    this->FlipEdge(iTriId, iNeighId, iA, iB, iApex, iNeighApex);
    return iNeighId;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :         ComputeEdgeMidPoint                                   */
/*                                                                            */
/*============================================================================*/
inline void VtkPolyDataTools::ComputeEdgeMidPoint(vtkIdType iPt1ID, vtkIdType iPt2ID, double x[3])
{
    double *fV1 = m_pMesh->GetPoint(iPt1ID);
    double *fV2 = m_pMesh->GetPoint(iPt2ID);
    for(int i=0; i<3; ++i)
        x[i] = (fV2[i] + fV1[i]) * 0.5f;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :     GetApexVertex                                             */
/*                                                                            */
/*============================================================================*/
inline vtkIdType VtkPolyDataTools::GetApexVertex(vtkIdType iTriangle, vtkIdType iV1, vtkIdType iV2)
{
    m_pMesh->GetCellPoints(iTriangle,m_iNumPts,m_pCellPts);
    int i;
    for(i=0; i<2; ++i)
        if(m_pCellPts[i] != iV1 && m_pCellPts[i] != iV2)
            break;
    return m_pCellPts[i];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeFlipIds                                              */
/*                                                                            */
/*============================================================================*/
inline bool VtkPolyDataTools::ComputeFlipIds(vtkIdType iTriId, vtkIdType iFlipA, vtkIdType iFlipB, vtkIdType &iNeighTri, vtkIdType &iMyApex, vtkIdType &iNeighApex)
{
    iNeighTri = this->GetCellEdgeNeighbor(iTriId, iFlipA, iFlipB);
    if(iNeighTri < 0)
        return false;
    iMyApex = this->GetApexVertex(iTriId,iFlipA,iFlipB);
    iNeighApex = this->GetApexVertex(iNeighTri, iFlipA, iFlipB);
    return true;
}
/*============================================================================*/
/*============================================================================*/
#endif /* ifndef _VTKPOLYDATATOOLS_H */

/*============================================================================*/
/*  END OF FILE "VtkPolyDataTools.h"                                          */
/*============================================================================*/

