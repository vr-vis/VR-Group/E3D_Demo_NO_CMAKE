/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkEventObserver.H           RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  CommExtApp                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VtkEventObserver          :   ...                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VTKEVENTOBSERVER_H
#define _VTKEVENTOBSERVER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaObserveable.h>
#include <vtkCommand.h>
#include <list>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkObject;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * VtkEventObserver is a simple adapter class which can be used to observe
 * VTK specific events with the IVistaObserveable interface. Any occuring events
 * will be forwarded to a ViSTA Observer by triggering a corresponding Notify call.
 *
 * @date May, 2006
 * @author Bernd Hentschel
 */
class VISTAVISEXTAPI VtkEventObserver : public IVistaObserveable
{
public:
    VtkEventObserver();
    virtual ~VtkEventObserver();
	/**
	* 
	*/
	void SetTarget(vtkObject *pTgt);
	/**
	*
	*/
	vtkObject *GetTarget() const;
	/**
	* observe the given vtk event on the set target
	* whenever the event is executed, all ViSTA observers will be given this
	* event id.
	*/
	void ObserveEvent(vtkCommand::EventIds id);
	/**
	*
	*/
	void UnObserveEvent(vtkCommand::EventIds id);
	/**
	*
	*/
	void GetObservedEvents(std::list<vtkCommand::EventIds>& liEvents) const;
	/**
	*
	*/
	void ClearObservedEvents();
	
private:
	void ReRegister();

	class VtkObserver;

	VtkObserver *m_pObs;
	
	std::list<vtkCommand::EventIds> m_liActiveEvents;
};


#endif // _VTKEVENTOBSERVER_H

/*============================================================================*/
/*  END OF FILE "VtkEventObserver.h"                                          */
/*============================================================================*/
