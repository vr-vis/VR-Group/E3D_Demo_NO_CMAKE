/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "Vve2DHistogram.h"
#include "VveHistogram.h"
#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaBase/VistaStreamUtils.h>

#include <iostream>
#include <cstring>
#include <cmath>
#include <algorithm>

using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
Vve2DHistogram::Vve2DHistogram()
	:	m_bCompressed(false),
		m_iMaxBinCount(0),
		m_nNumTotalPoints(0), 
		m_nNumValidPoints(0)
{
	m_iResolution[0] = m_iResolution[1] = 64;
	m_dRanges[0] = m_dRanges[2] = -numeric_limits<double>::max();
	m_dRanges[1] = m_dRanges[3] =  numeric_limits<double>::max();
}

Vve2DHistogram::Vve2DHistogram(const int iRes[2], const double fRanges[2])
{
	this->SetResolution(iRes);
	this->SetRanges(fRanges);
}

Vve2DHistogram::Vve2DHistogram(const Vve2DHistogram& oOther)
{
	this->operator =(oOther);
}

Vve2DHistogram::~Vve2DHistogram()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   operator=     				                              */
/*                                                                            */
/*============================================================================*/
Vve2DHistogram& Vve2DHistogram::operator=(const Vve2DHistogram& oOther)
{
	memcpy(m_dRanges, oOther.m_dRanges, 4*sizeof(m_dRanges[0]));
	m_bCompressed = oOther.m_bCompressed;
	m_iResolution[0] = oOther.m_iResolution[0];
	m_iResolution[1] = oOther.m_iResolution[1];
	m_iMaxBinCount = oOther.m_iMaxBinCount;
	m_nNumTotalPoints = oOther.m_nNumTotalPoints;
	m_nNumValidPoints = oOther.m_nNumValidPoints;
	//finally copy data
	m_vecBins.resize(m_iResolution[1]);
	for(unsigned int i=0; i<m_vecBins.size(); ++i)
	{
		m_vecBins[i] = oOther.m_vecBins[i];
	}
	return *this;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   operator+     				                              */
/*                                                                            */
/*============================================================================*/
Vve2DHistogram& Vve2DHistogram::operator+(const Vve2DHistogram& oOther)
{
	int iOtherRes[2];
	//need same resolution!
	oOther.GetResolution(iOtherRes);
	if(	iOtherRes[0] != m_iResolution[0] ||
		iOtherRes[1] != m_iResolution[1])
	{
		return *this;
	}
	//cannot handle compressed histograms!
	if(this->GetIsCompressed() || oOther.GetIsCompressed())
		return *this;

	for(register unsigned int y=0; y<m_vecBins.size(); ++y)
	{
		for(register unsigned int x=0; x<m_vecBins[y].size(); ++x)
		{
			m_vecBins[y][x] += oOther.m_vecBins[y][x];
			assert(m_vecBins[y][x] >= 0);
		}
	}
	m_nNumValidPoints += oOther.m_nNumValidPoints;
	m_nNumTotalPoints += oOther.m_nNumTotalPoints;

	return *this;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   operator+=    				                              */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::operator+=(const Vve2DHistogram& oOther)
{
	this->operator +(oOther);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetResolution 			                              */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::SetResolution(int iX, int iY)
{
	//we need at least one bin for outliers on either side of [min,max]!
	m_iResolution[0] = std::max(iX,2);
	m_iResolution[1] = std::max(iY,2);
	//we save the histogram row major i.e. the x-axis histograms
	//are stored in the single vectors!
	m_vecBins.resize(m_iResolution[1]);
	for (int i=0; i < m_iResolution[1]; ++i)
	{
		m_vecBins[i].resize(m_iResolution[0],0);
	}
}

void Vve2DHistogram::SetResolution(const int iR[2])
{
	this->SetResolution(iR[0], iR[1]);
}

void Vve2DHistogram::GetResolution(int &iX, int &iY) const
{
	iX = m_iResolution[0];
	iY = m_iResolution[1];
}

void Vve2DHistogram::GetResolution(int iR[2]) const
{
	this->GetResolution(iR[0], iR[1]);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRanges  				                              */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::SetRanges(const double dR[4])
{
	this->SetRanges(dR[0], dR[1], dR[2], dR[3]);
}

void Vve2DHistogram::SetRanges(double dXMin, double dXMax, double dYMin, double dYMax)
{
	m_dRanges[0] = dXMin;
	m_dRanges[1] = dXMax;
	m_dRanges[2] = dYMin;
	m_dRanges[3] = dYMax;
}

void Vve2DHistogram::GetRanges(double dR[4]) const
{
	this->GetRanges(dR[0], dR[1], dR[2], dR[3]);
}

void Vve2DHistogram::GetRanges(	double &dXMin, double &dXMax, 
								double &dYMin, double &dYMax) const
{
	dXMin = m_dRanges[0];
	dXMax = m_dRanges[1];
	dYMin = m_dRanges[2];
	dYMax = m_dRanges[3];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetBinCount				                              */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::SetBinCount(int iBinX, int iBinY, int iBinCount)
{
	assert(iBinCount>=0);
	m_vecBins[iBinY][iBinX] = iBinCount;
	m_iMaxBinCount = std::max(iBinCount, m_iMaxBinCount);
}

int Vve2DHistogram::GetBinCount(int iBinX, int iBinY) const
{
	assert(m_vecBins[iBinY][iBinX] >= 0);
	return m_vecBins[iBinY][iBinX];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNumValidPoints			                              */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::SetNumValidPoints(size_t nNumValidPoints)
{
	m_nNumValidPoints = nNumValidPoints;
}

size_t Vve2DHistogram::GetNumValidPoints() const
{
	return m_nNumValidPoints;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNumTotalPoints			                              */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::SetNumTotalPoints(size_t nNumTotalPoints)
{
	m_nNumTotalPoints = nNumTotalPoints;
}

size_t Vve2DHistogram::GetNumTotalPoints() const
{
	return m_nNumTotalPoints;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMaxBinCount				                          */
/*                                                                            */
/*============================================================================*/
int Vve2DHistogram::GetMaxBinCount() const
{
	return m_iMaxBinCount;
}

void Vve2DHistogram::SetMaxBinCount(int i) 
{
	m_iMaxBinCount = i;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLower/UpperBinLimit			                          */
/*                                                                            */
/*============================================================================*/
double Vve2DHistogram::GetLowerBinLimit(int iAxis, int iBin) const
{
	//extra cases if we mind a bin on either end of the spectrum
	//->that would be the outlier bin
	if(iBin == 0)
		return -numeric_limits<double>::max();
	if(iBin == m_iResolution[iAxis]-1)
		return m_dRanges[2*iAxis+1];
	//mind subtracting the two extra bins from resolution;
	double fInterval =	(m_dRanges[2*iAxis+1]-m_dRanges[2*iAxis])/
						(double)(m_iResolution[iAxis]-2);
	return (m_dRanges[2*iAxis] + fInterval*(double)(iBin-1));
}

double Vve2DHistogram::GetUpperBinLimit(int iAxis, int iBin) const
{
	//extra cases if we mind a bin on either end of the spectrum
	//->that would be the outlier bin
	if(iBin == 0)
		return m_dRanges[2*iAxis];
	if(iBin == m_iResolution[iAxis]-1)
		return numeric_limits<double>::max();
	//mind subtracting the two extra bins from resolution;
	double fInterval =	(m_dRanges[2*iAxis+1]-m_dRanges[2*iAxis])/
						(double)(m_iResolution[iAxis]-2);
	return (m_dRanges[2*iAxis] + fInterval*(double)iBin);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProjectedHistogram 			                          */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::GetProjectedHistogram(int iAxis, 
											VveHistogram& oHist) const
{
	//works only on uncompressed data
	if(m_bCompressed)
		return;

	oHist.CleanHistogram();
	int iSum = 0;
	int iNumValidPoints = 0;
	int iNumTotalPoints = 0;

	if(iAxis == 0)
	{
		//we want to project to the x axis i.e. sum up over all columns
		oHist.SetNumBins(m_iResolution[0]);
		oHist.SetRange(m_dRanges[0], m_dRanges[1]);
		for(int i=0; i<m_iResolution[0]; ++i)
		{
			iSum = 0;
			for(register int j=0; j<m_iResolution[1]; ++j)
				iSum += m_vecBins[j][i];
			oHist.SetBinCount(i, iSum);
			iNumTotalPoints += iSum;
			if(i!=0 || i!=m_iResolution[0])
				iNumValidPoints += iSum;
		}
	}
	else if(iAxis == 1)
	{
		//we want to project to the y axis, i.e. sum up over all rows
		oHist.SetNumBins(m_iResolution[1]);
		oHist.SetRange(m_dRanges[2], m_dRanges[3]);
		for(int i=0; i<m_iResolution[1]; ++i)
		{
			iSum = 0;
			for(register int j=0; j<m_iResolution[0]; ++j)
			{
				iSum += m_vecBins[i][j];
			}
			oHist.SetBinCount(i, iSum);
			iNumTotalPoints += iSum;
			if(i!=0 || i!=m_iResolution[0])
				iNumValidPoints += iSum;
		}
	}
	
	oHist.SetNumTotalPoints(iNumTotalPoints);
	oHist.SetNumValidPoints(iNumValidPoints);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CleanHistogram					                          */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::GetColumn(int iColumn, VveHistogram& oHist)
{
	oHist.SetNumBins(m_iResolution[1]);
	int iMax = 0;
	int iSum = 0;
	int iBC = 0;
	for(register int j=0; j<m_iResolution[1]; ++j)
	{
		iBC = m_vecBins[j][iColumn];
		iMax = std::max(iMax, iBC);
		iSum += iBC;
		oHist.SetBinCount(j, iBC);
	}
	oHist.SetRange(m_dRanges[2], m_dRanges[3]);
	oHist.SetMaxBinCount(iMax);
	oHist.SetNumTotalPoints(iSum);
	oHist.SetNumValidPoints(iSum - 
							m_vecBins[0][iColumn] - 
							m_vecBins[m_iResolution[1]-1][iColumn]);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeJointDistribution   		                          */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::ComputeJointDistribution(
	std::vector< std::vector<double> > &vecProb)
{
	//save the histogram row major i.e. the x-axis histograms
	//are stored in the single vectors!
	vecProb.resize(m_iResolution[1]);
	for (int i=0; i < m_iResolution[1]; ++i)
	{
		vecProb[i].resize(m_iResolution[0],0);
	}

	int iControlCounts = 0;
	double dControlDist = 0.0;

	for(int j=0; j<m_iResolution[1]; ++j)
	{
		for(int i=0; i<m_iResolution[0]; ++i)
		{
			vecProb[i][j] = double(m_vecBins[i][j]) / double(m_nNumValidPoints);
			iControlCounts += m_vecBins[i][j];
			dControlDist += vecProb[i][j];
		}
	}

#ifdef DEBUG
	if(iControlCounts != m_nNumValidPoints)
		vstr::warnp() << "[CVVe2DHistogram] invalid num of valid points" << std::endl;

	if(fabs(dControlDist - 1.0) > std::numeric_limits<double>::epsilon())
		vstr::warnp() << "[CVVe2DHistogram] invalid num of valid points" << std::endl;
#endif //DEBUG

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeJointEntropy       		                          */
/*                                                                            */
/*============================================================================*/
double Vve2DHistogram::ComputeJointEntropy()
{
	// get the distribution
	std::vector< std::vector<double> > vecProb;
	ComputeJointDistribution(vecProb);

	double dEntropy = 0.0;
	double dLog = 0.0;

	// compute H(X,Y)
	for(int j=0; j<m_iResolution[1]; ++j)
	{
		for(int i=0; i<m_iResolution[0]; ++i)
		{
			// compute in "bit"-fashion, i.e. use log2(vecProb[i])
			if(fabs(vecProb[i][j])< std::numeric_limits<double>::epsilon())
				dLog = 0.0;                         // per definition: log(0) = 0
			else
				dLog = log(vecProb[i][j]) / log(2.0);
			
			dEntropy -= vecProb[i][j] * dLog;
		}
	}

	return dEntropy;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   CleanHistogram					                          */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::GetRow(int iRow, VveHistogram& oHist)
{
	oHist.SetNumBins(m_iResolution[0]);
	int iMax = 0;
	int iSum = 0;
	int iBC = 0;
	for(register int i=0; i<m_iResolution[0]; ++i)
	{
		iBC = m_vecBins[iRow][i];
		iMax = std::max(iMax, iBC);
		iSum += iBC;
		oHist.SetBinCount(i, iBC);
	}
	oHist.SetRange(m_dRanges[0], m_dRanges[1]);
	oHist.SetMaxBinCount(iMax);
	oHist.SetNumTotalPoints(iSum);
	oHist.SetNumValidPoints(iSum - 
							m_vecBins[iRow][0] - 
							m_vecBins[iRow][m_iResolution[0]-1]);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CleanHistogram					                          */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::CleanHistogram ()
{
	m_iMaxBinCount = 0;
	m_nNumValidPoints = 0;
	m_nNumTotalPoints = 0;
	
	m_vecBins.clear();
	m_vecBins.resize(m_iResolution[1]);
	for (int i=0; i < m_iResolution[1]; ++i)
	{
		m_vecBins[i].resize(m_iResolution[0], 0);
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCompress						                          */
/*                                                                            */
/*============================================================================*/
void Vve2DHistogram::SetCompress(bool bCompress)
{
	if (m_bCompressed == bCompress)
		return;

	if (bCompress)
		Compress();
	else
		Decompress();
}

void Vve2DHistogram::Compress ()
{	
	if(m_bCompressed)
		return;

	for(unsigned int i = 0; i < m_vecBins.size(); ++i)
	{
		std::vector<int> vecCompressedVector;
		int iNumZeros = 0;
		for(unsigned int j = 0; j < m_vecBins[i].size(); ++j)
		{
			if (m_vecBins[i][j]==0)
			{
				iNumZeros++;
					// write first zero of a row
				if (iNumZeros==1)
					vecCompressedVector.push_back(0);
			}
			else
			{
				if (iNumZeros>0)
				{
					// write number of zeros in a row
					vecCompressedVector.push_back(iNumZeros);

					// reset counter
					iNumZeros = 0;
				}
				// write regular value
				vecCompressedVector.push_back(m_vecBins[i][j]);
			}
		}
		// if there are only zeros at the end, write them encoded, too
		if (iNumZeros>0)
		{
			// write number of zeros in a row
			vecCompressedVector.push_back(iNumZeros);

			// reset counter
			iNumZeros = 0;
		}

		// replace vector
		m_vecBins[i] = vecCompressedVector;
	}

	m_bCompressed = true;
}

void Vve2DHistogram::Decompress ()
{
	if(!m_bCompressed)
		return;

	for(unsigned int i = 0; i < m_vecBins.size(); ++i)
	{
		std::vector<int> vecUncompressedVector;
		bool bZeroMode = false;
		for(unsigned int j = 0; j < m_vecBins[i].size(); ++j)
		{
			if (m_vecBins[i][j]==0)
			{
				bZeroMode = true;
				vecUncompressedVector.push_back(0);
			}
			else
			{
				if (bZeroMode)
				{
					// write m_vecBins[i][j]-1 zeros into buffer,
					// as first zero was already written
					for (int k=0; k < m_vecBins[i][j]-1; ++k)
						vecUncompressedVector.push_back(0);
					bZeroMode = false;
				}
				else
				{
					vecUncompressedVector.push_back(m_vecBins[i][j]);
				}
			}
		}

		// replace vector
		m_vecBins[i] = vecUncompressedVector;
	}

	m_bCompressed = false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIsCompressed					                          */
/*                                                                            */
/*============================================================================*/
bool Vve2DHistogram::GetIsCompressed() const
{
	return m_bCompressed;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Serialize							                          */
/*                                                                            */
/*============================================================================*/
int Vve2DHistogram::Serialize(IVistaSerializer &Serializer) const
{
#ifdef DEBUG
	//vstr::debugi() << "\t\tSerialize histogram...\n";
#endif
	int iNumBytes = 0;

	int iRet = Serializer.WriteInt32((unsigned int) this->GetSignature().length());
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = Serializer.WriteString(this->GetSignature());
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = Serializer.WriteInt32(m_iResolution[0]);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = Serializer.WriteInt32(m_iResolution[1]);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = Serializer.WriteDouble(m_dRanges[0]);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	iRet = Serializer.WriteDouble(m_dRanges[1]);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	iRet = Serializer.WriteDouble(m_dRanges[2]);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	iRet = Serializer.WriteDouble(m_dRanges[3]);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	iRet = Serializer.WriteBool(m_bCompressed);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	for(unsigned int i = 0; i < m_vecBins.size(); ++i)
	{
		//NOTE: write length for each row here because it might differ
		//      from the resolution because of COMPRESSION!
		iRet = Serializer.WriteInt32((unsigned int) m_vecBins[i].size());
		if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;
		
		// @todo: Get rid of this cast.
		iRet = Serializer.WriteRawBuffer(&m_vecBins[i][0],
			static_cast<int>(sizeof(int)*m_vecBins[i].size()));
		if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	}

	iRet = Serializer.WriteInt32(m_iMaxBinCount);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;
	
	// @todo: Get rid of this cast.
	iRet = Serializer.WriteInt32(static_cast<VistaType::uint32>(
		m_nNumTotalPoints));
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	// @todo: Get rid of this cast.
	iRet = Serializer.WriteInt32(static_cast<VistaType::uint32>(
		m_nNumValidPoints));
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;
		
#ifdef DEBUG
	//vstr::debugi() << "\t\t... histogram finished ("<<iNumBytes<<" bytes)\n";
#endif

	return iNumBytes;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeSerialize						                          */
/*                                                                            */
/*============================================================================*/
int Vve2DHistogram::DeSerialize(IVistaDeSerializer &DeSerializer)
{
	int iNumBytes = 0;

#ifdef DEBUG
	//vstr::debugi() << "\t\tDeserialize histogram...\n";
#endif

	string str("");
	//read signature first
	int iLen = 0;
	int iRet = DeSerializer.ReadInt32(iLen);
	if(iRet == -1 || iLen != this->GetSignature().length())
		return -1;
	iNumBytes += iRet;
	iRet = DeSerializer.ReadString(str, iLen);
	if(iRet == -1 || str != this->GetSignature())
		return -1;
	iNumBytes += iRet;


	iRet = DeSerializer.ReadInt32(m_iResolution[0]);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;
	
	iRet = DeSerializer.ReadInt32(m_iResolution[1]);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = DeSerializer.ReadDouble(m_dRanges[0]);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;
	
	iRet = DeSerializer.ReadDouble(m_dRanges[1]);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;
	
	iRet = DeSerializer.ReadDouble(m_dRanges[2]);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;
	
	iRet = DeSerializer.ReadDouble(m_dRanges[3]);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;
	
	iRet = DeSerializer.ReadBool(m_bCompressed);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	m_vecBins.resize(m_iResolution[1]);
	for(unsigned int i = 0; i < m_vecBins.size(); ++i)
	{
		int iBinSize = 0;
		iRet = DeSerializer.ReadInt32(iBinSize);
		if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;
		m_vecBins[i].resize(iBinSize);
		// @todo: Get rid of this cast.
		iRet = DeSerializer.ReadRawBuffer(&m_vecBins[i][0], 
			static_cast<int>(sizeof(int)*m_vecBins[i].size()));
		if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;
	}

	iRet = DeSerializer.ReadInt32(m_iMaxBinCount);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	// @todo: Get rid of this cast.
	VistaType::uint32 uiBuffer = 0;
	iRet = DeSerializer.ReadInt32(uiBuffer);
	m_nNumTotalPoints = static_cast<size_t>(uiBuffer);

	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	// @todo: Get rid of this cast.
	iRet = DeSerializer.ReadInt32(uiBuffer);
	m_nNumValidPoints = static_cast<size_t>(uiBuffer);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

#ifdef DEBUG
	//vstr::debugi() << "\t\t... histogram finished ("<<iNumBytes<<" bytes)\n";
#endif

	return iNumBytes;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSignature						                          */
/*                                                                            */
/*============================================================================*/
string Vve2DHistogram::GetSignature() const
{
	return "__Vve2DHistogram__";
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveHistogram.cpp"                                            */
/*============================================================================*/



