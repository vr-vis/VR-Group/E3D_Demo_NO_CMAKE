/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifdef WIN32
	#pragma warning (disable: 4786)
#endif

// VistaFlowLib includes
#include "VveExecutable.h"

/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
IVveExecutable::IVveExecutable()
: m_iExecutableState(VFL_ES_UNDEFINED), m_strName("")
{
}


/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetState                                                    */
/*                                                                            */
/*============================================================================*/
void IVveExecutable::SetState(int iState)
{
	if(m_iExecutableState != iState)
	{
		m_iExecutableState = iState;
		//notify observers whenever the state changes.
		this->Notify(MSG_STATE_CHANGED);
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetState                                                    */
/*                                                                            */
/*============================================================================*/
int IVveExecutable::GetState() const
{
    return m_iExecutableState;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetStateString                                              */
/*                                                                            */
/*============================================================================*/
std::string IVveExecutable::GetStateString() const
{
    switch (m_iExecutableState)
    {
	case VFL_ES_UNDEFINED:
        return "UNDEFINED";
    case VFL_ES_IDLE:
        return "IDLE";
	case VFL_ES_ERROR:
		return "ERROR";
	default:
        return "UNKNOWN";
    }
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
