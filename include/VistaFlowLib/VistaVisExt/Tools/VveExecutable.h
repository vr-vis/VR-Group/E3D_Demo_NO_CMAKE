/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEEXECUTABLE_H
#define _VVEEXECUTABLE_H


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>

//ViSTA includes


// ViSTA FlowLib includes
#include <VistaVisExt/VistaVisExtConfig.h>
#include <VistaAspects/VistaReflectionable.h>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaPropertyList;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
*/
class VISTAVISEXTAPI IVveExecutable : public IVistaReflectionable
{
public:
	enum // messages
	{
		MSG_STATE_CHANGED = IVistaObserveable::MSG_LAST,
		MSG_LAST
	};

    virtual ~IVveExecutable() {};

    /**
     * 'Execute' this object, i.e. make it do whatever it's supposed to do...
     */
    virtual void Execute() = 0;
	/**
	* set the parameters which determine the outcome of execute
	*/
	virtual void SetParametersByPropList(const VistaPropertyList& rParams){}
	
	virtual bool GetParametersByPropList( VistaPropertyList &oOut ) const { return false; }

	/**
	 * Set this object's state.
	 */
	virtual void SetState(int iState);

    /**
     * Retrieve information about this object's state.
     */
    virtual int GetState() const;

    /** 
     * Retrieve information about this object's state in a human readable form.
     * This method provides information about all strings known at this point,
     * i.e. it returns "UNDEFINED", iff the state is UNDEFINED,
     * it returns "IDLE", iff the state is IDLE,
     * and it returns "UNKNOWN", otherwise. 
     * In the latter case, it is obviously a pretty good idea to provide the 
     * corresponding information in the respective child class.
     */
    virtual std::string GetStateString() const; 

    enum EExecutableState
    {
        VFL_ES_UNDEFINED = -2,
		VFL_ES_ERROR = -1,
        VFL_ES_IDLE = 0,
        VFL_ES_LAST //entry point for first specific state here
    };

	REFL_INLINEIMP(IVflExecutable,IVistaReflectionable);
protected:
    IVveExecutable();

private:
    int m_iExecutableState;
	std::string m_strName;
};

/*============================================================================*/
/*  INLINE MEMBERS                                                            */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VVEEXECUTABLE_H

