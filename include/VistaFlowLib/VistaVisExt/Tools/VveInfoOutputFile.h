/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEINFOOUTPUTFILE_H
#define _VVEINFOOUTPUTFILE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include<string>
#include<iostream>
#include<fstream>

#include "VveDataSetInfo.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Write the data set information into file data.dat.
*/
class VISTAVISEXTAPI VveLegacyInfoOutputFile
{
public:
	VveLegacyInfoOutputFile();
	virtual ~VveLegacyInfoOutputFile();

	void SetFileName(const std::string &);

	void WriteInfo(VveDataSetInfo *);

private:
	std::ofstream m_myDataFile;
};

class VISTAVISEXTAPI VveInfoOutputFile
{
public:
	VveInfoOutputFile();
	virtual ~VveInfoOutputFile();

	void SetFileName(const std::string &);

	void SetTimeStepPrefix(const std::string &);

	void WriteCombinedInfo(VveDataSetInfo *);

	void WriteDiscreteTimeInfo(VveDataSetInfo *);

private:
	std::ofstream m_myDataFile;
	std::string m_strTimeStepPrefix;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VVEINFOOUTPUTFILE_H

