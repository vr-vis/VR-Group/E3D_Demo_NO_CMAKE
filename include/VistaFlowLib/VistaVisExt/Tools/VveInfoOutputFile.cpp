/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveInfoOutputFile.h"
#include "VveHistogram.h"
#include "VveDataChunkInfo.h"
#include "VveDataFieldInfo.h"

#include <VistaInterProcComm/Connections/VistaByteBufferSerializer.h>
#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaBase/VistaStreamUtils.h>
#include <iostream>
#include <limits>

using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveLegacyInfoOutputFile::VveLegacyInfoOutputFile(){}
VveLegacyInfoOutputFile::~VveLegacyInfoOutputFile(){}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
//set file name
void VveLegacyInfoOutputFile::SetFileName(const string &strFileName)
{
	m_myDataFile.open(strFileName.c_str());
}


//write the information in the file
void VveLegacyInfoOutputFile::WriteInfo(VveDataSetInfo *pDataSetInfo)
{
	if(!m_myDataFile.is_open())
	{
		vstr::warnp() << "[VveInfoOutputFile::WriteInfo] Unable to open file" << endl; 
		//return -1;
	}
	
	int iMinTimeStep = pDataSetInfo->GetMinTimeStep();
	int iMaxTimeStep = pDataSetInfo->GetMaxTimeStep();

	//1st row of data.dat
	m_myDataFile << pDataSetInfo->GetDataSetName() << " " << iMinTimeStep << " " << iMaxTimeStep  << endl;
	//2nd row of data.dat
	int iNumTimeSteps = iMaxTimeStep - iMinTimeStep + 1; 
	m_myDataFile << "TIMESTEPS " << iNumTimeSteps << endl;

	//output of each time step
	int iCurrInArray  = 0;
	for(int i = iMinTimeStep; i <= iMaxTimeStep; ++i)
	{
		m_myDataFile << "BEGIN TIMESTEP " << i << endl;
	
		iCurrInArray = i - iMinTimeStep;

		m_myDataFile << "POINTS		" << pDataSetInfo->GetChildChunkInfo(iCurrInArray)->GetNumPoints() << endl;
		m_myDataFile << "CELLS		" << pDataSetInfo->GetChildChunkInfo(iCurrInArray)->GetNumCells() << endl;
		m_myDataFile << "BOUNDS		";
		for(int iBound = 0; iBound < 6; ++iBound)
			m_myDataFile << pDataSetInfo->GetChildChunkInfo(iCurrInArray)->GetIthBound(iBound) << " ";
		m_myDataFile << endl;
		m_myDataFile << "POINT_FIELDS	" << pDataSetInfo->GetChildChunkInfo(iCurrInArray)->GetNumFieldInfo() << endl;

		const int nNumChildFieldInfo = static_cast<int>(
			pDataSetInfo->GetChildChunkInfo(iCurrInArray)->GetNumFieldInfo());
		for(int k = 0; k < nNumChildFieldInfo; ++k)
		{
			VveDataFieldInfo *pCurrentInfo = pDataSetInfo->GetChildChunkInfo(iCurrInArray)->GetFieldInfo(k);
			m_myDataFile << "BEGIN FIELD	" << pCurrentInfo->GetFieldName() << endl;
			m_myDataFile << "TYPE       " << pCurrentInfo->GetType() << endl;
			m_myDataFile << "NUMCOMPS " << pCurrentInfo->GetNumberOfComponents() << endl;
			//write min max and avg values for each component in a single row
			m_myDataFile << "MIN  ";
			for(int c=0; c<pCurrentInfo->GetNumberOfComponents(); ++c)
			{
				m_myDataFile << pCurrentInfo->GetMin(c);
				m_myDataFile << (c==pCurrentInfo->GetNumberOfComponents()-1 ? "\n" : " ");
			}
			m_myDataFile << "MAX  ";
			for(int c=0; c<pCurrentInfo->GetNumberOfComponents(); ++c)
			{
				m_myDataFile << pCurrentInfo->GetMax(c) ;
				m_myDataFile << (c==pCurrentInfo->GetNumberOfComponents()-1 ? "\n" : " ");
			}
			m_myDataFile << "AVG  ";
			for(int c=0; c<pCurrentInfo->GetNumberOfComponents(); ++c)
			{
				m_myDataFile << pCurrentInfo->GetAverage(c) ;
				m_myDataFile << (c==pCurrentInfo->GetNumberOfComponents()-1 ? "\n" : " ");
			}
			
			if(pCurrentInfo->GetHistogram()->GetNumTotalPoints() != 0)
			{
				int iBins = pCurrentInfo->GetHistogram()->GetNumBins();

				m_myDataFile << "HIST		" << iBins << " ";
				for(int j = 0; j < iBins; ++j)
					m_myDataFile <<pCurrentInfo->GetHistogram()->GetBinCount(j) << " ";

				m_myDataFile << "MAX " << pCurrentInfo->GetHistogram()->GetMaxBinCount() << endl;
			}
			
			m_myDataFile << "END FIELD" << endl << endl;
		}

		m_myDataFile << "END TIMESTEP" << endl << endl;
	}

	//output of total information
	m_myDataFile << "BEGIN TOTAL" << endl;

	m_myDataFile << "POINTS		" << pDataSetInfo->GetNumPoints() << endl;
	m_myDataFile << "CELLS		" << pDataSetInfo->GetNumCells() << endl;
	m_myDataFile << "BOUNDS		";
	for(int iBound = 0; iBound < 6; ++iBound)
		m_myDataFile << pDataSetInfo->GetIthBound(iBound) << " ";
	m_myDataFile << endl;
	m_myDataFile << "POINT_FIELDS	" << pDataSetInfo->GetNumFieldInfo() << endl;

	const int nNumFieldInfo = static_cast<int>(pDataSetInfo->GetNumFieldInfo());
	for(int k = 0; k < nNumFieldInfo; ++k)
	{
		VveDataFieldInfo *pCurrentInfo = pDataSetInfo->GetFieldInfo(k);
		m_myDataFile << "BEGIN FIELD	" << pCurrentInfo->GetFieldName() << endl;
		m_myDataFile << "TYPE " << pCurrentInfo->GetType() << endl;
		m_myDataFile << "NUMCOMPS " << pCurrentInfo->GetNumberOfComponents() << endl;
		//write min max and avg values for each component in a single row
		m_myDataFile << "MIN  ";
		for(int c=0; c<pCurrentInfo->GetNumberOfComponents(); ++c)
		{
			m_myDataFile << pCurrentInfo->GetMin(c);
			m_myDataFile << (c==pCurrentInfo->GetNumberOfComponents()-1 ? "\n" : " ");
		}
		m_myDataFile << "MAX  ";
		for(int c=0; c<pCurrentInfo->GetNumberOfComponents(); ++c)
		{
			m_myDataFile << pCurrentInfo->GetMax(c) ;
			m_myDataFile << (c==pCurrentInfo->GetNumberOfComponents()-1 ? "\n" : " ");
		}
		m_myDataFile << "AVG  ";
		for(int c=0; c<pCurrentInfo->GetNumberOfComponents(); ++c)
		{
			m_myDataFile << pCurrentInfo->GetAverage(c) ;
			m_myDataFile << (c==pCurrentInfo->GetNumberOfComponents()-1 ? "\n" : " ");
		}
		m_myDataFile << "STD  ";
		for(int c=0; c<pCurrentInfo->GetNumberOfComponents(); ++c)
		{
			m_myDataFile << pCurrentInfo->GetStandardDeviation(c) ;
			m_myDataFile << (c==pCurrentInfo->GetNumberOfComponents()-1 ? "\n" : " ");
		}		
		
		
		if(pCurrentInfo->GetHistogram()->GetNumTotalPoints() != 0)
		{
			int iBins = pCurrentInfo->GetHistogram()->GetNumBins();

			m_myDataFile << "HIST		" << iBins << " ";
			for(int j = 0; j < iBins; ++j)
				m_myDataFile << pCurrentInfo->GetHistogram()->GetBinCount(j) << " ";

			m_myDataFile << "MAX " << pCurrentInfo->GetHistogram()->GetMaxBinCount() << endl;
		}

		m_myDataFile << "END FIELD" << endl << endl;
	}

	m_myDataFile << "END TOTAL" << endl;

	m_myDataFile.close();
}

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveInfoOutputFile::VveInfoOutputFile()
:m_strTimeStepPrefix("")
{
}
VveInfoOutputFile::~VveInfoOutputFile()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
//set file name
void VveInfoOutputFile::SetFileName(const string &strFileName)
{
	m_myDataFile.open(strFileName.c_str(), std::ios_base::binary);
}

void VveInfoOutputFile::SetTimeStepPrefix(const std::string &strPrefix)
{
	m_strTimeStepPrefix = strPrefix;
}


//write the information in one file
void VveInfoOutputFile::WriteCombinedInfo(VveDataSetInfo *pDataSetInfo)
{
	if(!m_myDataFile.is_open())
	{
		vstr::warnp() << "[VveInfoOutputFile::WriteCombinedInfo] Unable to open file" << endl; 
		//return -1;
	}
	
	VistaByteBufferSerializer oBBSerializer;
	int iRet = pDataSetInfo->Serialize (oBBSerializer);

	m_myDataFile << oBBSerializer.GetBufferSize();

	std::vector<unsigned char> uBuff;
	oBBSerializer.GetBuffer (uBuff);

	m_myDataFile.write ((char*) (&uBuff[0]), oBBSerializer.GetBufferSize());
#ifdef DEBUG
	//oBBSerializer.DumpCurrentBuffer();
#endif

	m_myDataFile.close();
}


//write the information separated into dataset and timestep files
void VveInfoOutputFile::WriteDiscreteTimeInfo(VveDataSetInfo *pDataSetInfo)
{
	// 1. data set
	if(!m_myDataFile.is_open())
	{
		vstr::warnp() << "[VveInfoOutputFile::WriteDiscreteTimeInfo] Unable to open data set file "<< endl; 
		//return -1;
	}
	
	VistaByteBufferSerializer oBBSerializer;
	pDataSetInfo->SetDoSerializeChilds(false);
	int iRet = pDataSetInfo->Serialize (oBBSerializer);

	m_myDataFile << oBBSerializer.GetBufferSize();

	std::vector<unsigned char> uBuff;
	oBBSerializer.GetBuffer (uBuff);

	m_myDataFile.write ((char*) (&uBuff[0]), oBBSerializer.GetBufferSize());

	oBBSerializer.ClearBuffer();
	m_myDataFile.close();

	const int nNumChildChunkInfo = static_cast<int>(pDataSetInfo->GetNumChildChunkInfo());
	for (int iTimeStep=0; iTimeStep < nNumChildChunkInfo; iTimeStep++)
	{
		std::string strFilename (m_strTimeStepPrefix+VistaAspectsConversionStuff::ConvertToString(iTimeStep)+".dat");
		m_myDataFile.open(strFilename.c_str(), std::ios_base::binary);
		if(!m_myDataFile.is_open())
		{
			vstr::warnp() << "[VveInfoOutputFile::WriteDiscreteTimeInfo] Unable to open file "<<strFilename << endl; 
			m_myDataFile.close();
			continue;
		}

		int iRet = pDataSetInfo->GetChildChunkInfo(iTimeStep)->Serialize (oBBSerializer);

		m_myDataFile << oBBSerializer.GetBufferSize();

		std::vector<unsigned char> uBuff;
		oBBSerializer.GetBuffer (uBuff);

		m_myDataFile.write ((char*) (&uBuff[0]), oBBSerializer.GetBufferSize());

		oBBSerializer.ClearBuffer();
		m_myDataFile.close();
	}
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveInfoOutputFile.cpp"                                       */
/*============================================================================*/

