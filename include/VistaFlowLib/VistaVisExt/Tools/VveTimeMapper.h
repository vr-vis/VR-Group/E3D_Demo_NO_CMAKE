/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VVETIMEMAPPER_H
#define _VVETIMEMAPPER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaNameable.h>

#include "../VistaVisExtConfig.h"

#include <vector>
#include <string>


/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VveTimeMapper is responsible for the conversion of normalized
 * visualization time to simulation time and simulation time level. The following
 * terms and intervalls are used for a simulation running from time0 to time1,
 * which is discretized in n time indices from 0 to n-1,
 * and which contains m time levels from i[0] to i[m-1], which can be arbitrarily
 * distributed along the n indices.
 * visualization time: continuous, [0, 1]
 * simulation time: continuous, [start_time, end_time]
 * time level: discrete, [i[0], i[m-1]]
 * time index: discrete, [0, n-1]
 *
 * Note though, that we don't necessarily have explicit time level information
 * available, but only a visualization and/or simulation time frame and a number
 * of time indices and time levels. In this case, time levels are given as l[i] with
 *     l[i] = first_level + i*level_increment for 0<=i<m. For time indices j
 * greater than or equal to m, j is divided modulo m, i.e.
 *     l[j] = l[j mod m]
 *
 * Another note: Mapping time levels / indices to continuous time is *not* done
 * on a level-to-interval basis (as might be more intuitive at first sight),
 * but on a level-to-"instant in time" basis. This means, that e.g. the first
 * time level does *not* correspond to the interval from time[0] to time[1],
 * but to time[0] and is therefore valid from time[0] to (time[0]+time[1])/2.
 * Consequently, the second time level corresponds to time[1] and is therefore
 * valid from (time[0]+time[1])/2 to (time[1]+time[2]/2). 
 * A desperate try to make it clearer: (behold the power of ascii art...)
 * 
 * vis time:             0 [----------------------------------] 1
 * sim time:    start_time [----------------------------------] end_time
 * time index:             0    1    2    3    4    5    6    7
 * time level:             i    j    k    l    m    i    k    j
 * level index:            0    1    2    3    4    0    2    1
 * corresponding interval: [-][---][---][---][---][---][---][-]
 *
 * example vis time:                  *            *
 *                                    t1           t2
 *
 * vis time:                      t1                   |                t2                  
 * sim time:       start_time+t1*(end_time-start_time) | start_time+t2*(end_time-start_time)
 * time index:                    2                    |                5                   
 * time level:                    k                    |                n                   
 * level index:                   2                    |                0
 * 
 */    
class VISTAVISEXTAPI VveTimeMapper : public IVistaNameable
{
public:
    /**
	 * Helper class that is needed for implicit timing creations.
	 * In an nutshell:
	 * - levels: indicate the number of physical resources (e.g., data items)
	             that may be multiplexed (e.g., in a repeated animation)
				 over time.
	 * - indices: the total number of animation slots, regardless of your
	              physical data items (e.g., 18000 particle positions 
				  over 60 vtk context geometries -> indices == 18000, levels == 60)
     * <b>Always set levels and indices correctly, other missing values will be adjusted
	 * to default values</b>.
	 */
	class VISTAVISEXTAPI Timings
	{
	public:
		Timings();
		virtual ~Timings();

		int   m_iNumberOfIndices;	//<** number of time indices */
		int   m_iNumberOfLevels;	//<** number of time levels */
		int   m_iFirstLevel;		//<** number of the first time level */
		int	  m_iLevelStride;		//<** stride of the time levels */
		double m_dVisStart;			//<** start time (in visualization time frame, i.e. [0,1]) */
		double m_dVisEnd;			//<** end time (in visualization time frame, i.e. [0,1]) */
		double m_dSimStart;			//<** start time (in simulation time frame) */
		double m_dSimEnd;			//<**  end time (in simulation time frame) */
	};

	/**
	 * Helper class that is needed for explicit timings during construction.
	 */
	class VISTAVISEXTAPI IndexInfo		// every time index corresponds to one of these informational nodes
	{
	public:
		IndexInfo();
		virtual ~IndexInfo();

		int		m_iLevel;		//<** this is the number of this time level (i.e. the time level itself) */
		double	m_dSimTime;	//<** the simulation time for this time level */
	};

	VveTimeMapper();
	VveTimeMapper(const Timings &oImplicitTimings);
	VveTimeMapper(const std::vector<IndexInfo> &oExplicitTimings);	
	virtual ~VveTimeMapper();

    /**
     * Returns the simulation time for the given visualization time.
     */    
    double GetSimulationTime(double dVisTime);

    /**
     * Returns the simulation time for the given time index.
     */    
    double GetSimulationTime(int iTimeIndex);

    /**
     * Returns the time index for the given visualization time.
	 *
	 * This methods is optimized for sequential scans, that is
	 * successive request like GetTimeIndex(0.1), GetTimeIndex(0.2), ....
	 * e.g., as done in the rendering pass.
	 *
	 * This method is approx. 10 times faster than GetTimeIndexRandomAccess
	 * for sequential access patterns (rule of thumb from measurements, 13.04.2010).
	 *
     */    
	int GetTimeIndex(double dVisTime);

	/**
     * Returns the time index for the given visualization time.
	 * This method is optimized for random access of time indices and
	 * therefore uses a binary search. Moreover, it does not affect the time
	 * mapper's internal state and should therefore be more convenient for
	 * multi-threaded acces.
	 *
	 * This method is approx. 100 times faster than GetTimeIndex
	 * for random access patterns (rule of thumb from measurements, 13.04.2010).
	 *
     */   
	int GetTimeIndexRandomAccess(double dVisTime) const;

    /**
     * Returns the time level for the given visualization time.
     */    
	int GetTimeLevel(double dVisTime);

    /**
     * Returns the time level for the given time index.
     */    
	int GetTimeLevel(int iIndex);

	/**
	 * Returns the time level for the given level index.
	 */
	int GetTimeLevelForLevelIndex(int iLevelIndex);

	/**
	 * Returns the time level index for the given visualization time.
	 */
	int GetLevelIndex(double dVisTime) const;

	/**
	 * Returns the time level index for the given time index.
	 */
	int GetLevelIndex(int iTimeIndex) const;

	/**
	 * Returns the time level index for the given time level.
	 */
	int GetLevelIndexForTimeLevel(int iTimeLevel);

    /**
     * Returns the visualization time for the given time index.
     */    
	double GetVisualizationTime(int iTimeIndex);

    /**
     * Returns the visualization time for the given simulation time.
     */    
	double GetVisualizationTime(double dSimTime);

    /**
     * Returns the visualization time interval start time for the given time index.
     */    
	double GetVisualizationTimeIntervalStart(int iTimeIndex);
	
	
	/**
	 * Gets interpolation weight and time indices for a given visualization time.
	 * This method returns interpolation weight alpha with
	 *     fVisTime = (1-alpha) * VisTime(iIndex1) + alpha * VisTime(iIndex2);
	 *
	 * NOTE: This method DOES NOT alter the TM's internal state so it can be
	 *		 considereed THREAD SAFE.
	 */
	double GetWeightedTimeIndices(double dVisTime, int &iIndex1, int &iIndex2) const;

	/**
	 * Gets interpolation weight and level indices for a given visualization time.
	 * This method returns interpolation weight alpha with
	 *     fVisTime = (1-alpha) * VisTime(iIndex1) + alpha * VisTime(iIndex2);
	 *
	 * NOTE: This method DOES NOT alter the TM's internal state so it can be
	 *		 considereed THREAD SAFE.
	 */
	double GetWeightedLevelIndices(double dVisTime, int &iIndex1, int &iIndex2) const;

	/**
	 * Returns the number of time indices.
	 */
	int GetNumberOfTimeIndices();

    /**
     * Returns the number of time levels.
     */    
	int GetNumberOfTimeLevels();

	void Dump(bool bDetailed) const;

	/**
	 * Set timing by explicit structure (list of <simtime, level>). 
     * This will rebuild the timelevels every time.
	 */
	bool SetByExplicitTimeMapping(const std::vector<IndexInfo> &vecExplicitTimings);

	/**
	 * Update timing by explicit structure (list of <simtime, level>).
     * This will keep the existing time levels and just change the time indices (including vistime).
	 */
	bool UpdateExplicitTimeMapping(const std::vector<IndexInfo> &vecExplicitTimings);

	/**
	 * Set timing by implicit structure. This will rebuild the timelevels every time.
	 */
	bool SetByImplicitTimeMapping(const Timings &oImplicitTimings);

	/**
	 * Get the implicit or explicit timing information
	 */
	bool GetExplicitTimeMapping(std::vector<IndexInfo> &vecExplicitTimings) const;
	bool GetImplicitTimeMapping(Timings &oImplicitTimings) const;

	/**
	 * Set implicit visrange
	 */
	void SetImplicitVisRange (double dLowerVisRange, double dHigherVisRange);

	size_t GetSize() const;

	//#################################################
	// NAMEABLE-INTERFACE
	//#################################################

	virtual std::string GetNameForNameable() const;
	virtual void SetNameForNameable(const std::string &strName);

private:
	/**
	 * Helper class that is needed for internal storage of timing information.
	 */
	class VISTAVISEXTAPI InternalIndexInfo		// every time index corresponds to one of these informational nodes
	{
	public:
		InternalIndexInfo();
		virtual ~InternalIndexInfo();

		double	m_dVisTime;	//<** the visualization time for this time level */
		double	m_dVisTimeIntervalStart; //<** the start of the time interval, in which this time level is valid */
		double	m_dSimTime;	//<** the simulation time for this time level */
		int		m_iLevelIndex;	//<** the number of the level index of this level */
	};
	static bool LessThanVisTimeIntervalStart (const InternalIndexInfo & oInfoA, const InternalIndexInfo & oInfoB);

	bool BuildExplicitDefinitionFromImplicitDefinition();
	bool BuildTimeIndicesFromExplicitDefinition(bool bSetSimTimings);
    bool BuildTimeLevels ();
    bool BuildLevelIndices ();

	std::vector<InternalIndexInfo>	m_vecTimeIndices; //<**  data for all time indices */

	std::vector<int> m_vecTimeLevels;	//<** vector for all available time levels */


	// member for implicit timings
	Timings m_oTimings;
    // member for explitit timings
	std::vector<IndexInfo>	m_vecExplicitTimings; 

	int m_iCurrentTimeIndex;
	double m_dVisDuration; //<** duration of visualization time (inside [0,1]) */
	double m_dSimDuration; //<** duration of simulation time */

	std::string m_strName;
};

/*============================================================================*/
/* INLINE METHODS                                                             */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeLevel                                                 */
/*                                                                            */
/*============================================================================*/
inline int VveTimeMapper::GetTimeLevel(int iIndex)
{
	if (iIndex < 0 || iIndex >= m_oTimings.m_iNumberOfIndices)
		return -1;

	return m_vecTimeLevels[m_vecTimeIndices[iIndex].m_iLevelIndex];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeLevel                                                */
/*                                                                            */
/*============================================================================*/
inline int VveTimeMapper::GetTimeLevel(double dVisTime)
{
	return GetTimeLevel(GetTimeIndex(dVisTime));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLevelIndex                                               */
/*                                                                            */
/*============================================================================*/
inline int VveTimeMapper::GetLevelIndex(double dVisTime) const
{
	//return GetLevelIndexForTimeLevel(GetTimeLevel(fVisTime));
	return GetLevelIndex(GetTimeIndexRandomAccess(dVisTime));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLevelIndex                                               */
/*                                                                            */
/*============================================================================*/
inline int VveTimeMapper::GetLevelIndex(int iTimeIndex) const
{
	if (iTimeIndex < 0 || iTimeIndex >= m_oTimings.m_iNumberOfIndices)
	{
		return -1;
	}

    return m_vecTimeIndices[iTimeIndex].m_iLevelIndex;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSimulationTime                                           */
/*                                                                            */
/*============================================================================*/
inline double VveTimeMapper::GetSimulationTime(int iTimeIndex)
{
	return GetSimulationTime(GetVisualizationTime(iTimeIndex));
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisualizationTime                                        */
/*                                                                            */
/*============================================================================*/
inline double VveTimeMapper::GetVisualizationTime(int iTimeIndex)
{
	if (iTimeIndex < 0 || iTimeIndex >= m_oTimings.m_iNumberOfIndices)
	{
		return -1;
	}

	return m_vecTimeIndices[iTimeIndex].m_dVisTime;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisualizationTimeIntervalStart                           */
/*                                                                            */
/*============================================================================*/
inline double VveTimeMapper::GetVisualizationTimeIntervalStart(int iTimeIndex)
{
	if (iTimeIndex < 0 || iTimeIndex >= m_oTimings.m_iNumberOfIndices)
	{
		return -1;
	}

	return m_vecTimeIndices[iTimeIndex].m_dVisTimeIntervalStart;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumberOfTimeIndices                                      */
/*                                                                            */
/*============================================================================*/
inline int VveTimeMapper::GetNumberOfTimeIndices()
{
	return m_oTimings.m_iNumberOfIndices;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumberOfTimeLevels                                       */
/*                                                                            */
/*============================================================================*/
inline int VveTimeMapper::GetNumberOfTimeLevels()
{
	return m_oTimings.m_iNumberOfLevels;
}

#endif // !defined(_VVETIMEMAPPER_H)
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
