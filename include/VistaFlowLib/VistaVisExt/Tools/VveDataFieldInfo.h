/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEDATAFIELDINFO_H
#define _VVEDATAFIELDINFO_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaSerializable.h>
#include <string>
#include <vector>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveHistogram;
class Vve2DHistogram;
class IVistaSerializer;
class IVistaDeSerializer;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Hold meta data (e.g. min, max...) for a data field (e.g. scalar field).
*/
class VISTAVISEXTAPI VveDataFieldInfo : public IVistaSerializable
{
public:
	VveDataFieldInfo();
	virtual ~VveDataFieldInfo();

	void SetFieldName(const std::string&);
	std::string GetFieldName() const;

	void SetNumberOfComponents(int i);
	int GetNumberOfComponents() const;
	
	//save the underlying data type using VTK enums (e.g. VTK_CHAR ...)
	void SetType(int iType);
	int GetType() const;

	/*
	* Set/get min/max values. Range is also [min, max]
	*
	*/
	void SetMax(int iComp, double dMax);
	double GetMax(int iComp) const;

	void SetMin(int iComp, double dMax);
	double GetMin(int iComp ) const;

	void SetRange(int iComp, double fRange[2]);
	void GetRange(int iComp, double fRange[2]) const;
	
	/*
	* Set/get average value of component
	*
	*/
	void SetAverage(int iComp, double dAvg);
	double GetAverage(int iComp) const;

	/*
	* Set/get standard deviation of component. 
	* CAVE: it depends on the implementation of the data collector which
	* sets the standard deviation of which semantics standard deviation has.
	* For instance, it could be SD of the field in the whole data set w.r.t. time steps 
	* or the single data values.
	*
	*/
	void SetStandardDeviation(int iComp, double dSD);
	double GetStandardDeviation(int iComp) const;

	/*
	* Get histogram data for this field
	*
	*/
	VveHistogram* GetHistogram() const;

	/*
	* Define joint histograms.
	*
	*/
	void SetNumberOfJointHistograms(size_t nSize);
	size_t GetNumberOfJointHistograms() const;
	Vve2DHistogram* GetJointHistogram(int i) const;

	/**
     * Think of this as "SAVE"
     */
    virtual int Serialize(IVistaSerializer &) const;
    /**
     * Think of this as "LOAD"
     */
    virtual int DeSerialize(IVistaDeSerializer &);

    virtual std::string GetSignature() const;
private:
	std::string m_strName;
	//remember the entire shape of the attributes (i.e. type and num comps)
	int m_iNumberOfComponents;
	int m_iType;
	//allocate space for at most 9 components of an array
	double m_fRange[9][2];
	double m_fAverage[9];
	double m_fStandardDeviation[9];
	VveHistogram *m_pHist;

	std::vector<Vve2DHistogram*> m_vecJointHistograms;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VVEDATAFIELDINFO_H

