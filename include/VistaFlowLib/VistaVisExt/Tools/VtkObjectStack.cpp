/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VtkObjectStack.CPP           RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/

#include "VtkObjectStack.h"
#include <iostream>
#include <vtkObject.h>

#include <VistaBase/VistaStreamUtils.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VtkObjectStack::VtkObjectStack()
{
#ifdef DEBUG
	vstr::debugi() << " [VtkObjectStack] constructor..." << endl;
#endif
}

VtkObjectStack::~VtkObjectStack()
{
#ifdef DEBUG
	vstr::debugi() << " [VtkObjectStack] destructor..." << endl;
#endif
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   PushObject                                                  */
/*                                                                            */
/*============================================================================*/
void VtkObjectStack::PushObject(vtkObject *pObject)
{
	if (pObject)
		m_stackObjects.push(pObject);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeleteObjects                                               */
/*                                                                            */
/*============================================================================*/
void VtkObjectStack::DeleteObjects()
{
#ifdef DEBUG
	vstr::debugi() << " [VtkObjectStack] deleting vtk objects" << endl;
#endif

	while (!m_stackObjects.empty())
	{
		m_stackObjects.top()->Delete();
		m_stackObjects.pop();
	}
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VtkObjectStack.cpp"                                          */
/*============================================================================*/
