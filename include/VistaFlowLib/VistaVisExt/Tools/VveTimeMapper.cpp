/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VveTimeMapper.h"
#include <cassert>
#include <iostream>
#include <set>
#include <map>
#include <algorithm>

#include <VistaBase/VistaStreamUtils.h>

using namespace std;


VveTimeMapper::Timings::Timings()
:	m_iNumberOfIndices(1)
,	m_iNumberOfLevels(1)
,	m_iFirstLevel(0)
,	m_iLevelStride(1)
,	m_dVisStart(0.0)
,	m_dVisEnd(1.0)
,	m_dSimStart(0.0)
,	m_dSimEnd(1.0)
{ }

VveTimeMapper::Timings::~Timings()
{ }


VveTimeMapper::IndexInfo::IndexInfo()
:	m_dSimTime(0.0)
,   m_iLevel(0)
{ }

VveTimeMapper::IndexInfo::~IndexInfo()
{ }


VveTimeMapper::InternalIndexInfo::InternalIndexInfo()
:	m_dVisTime(0.0)
,	m_dVisTimeIntervalStart(0.0)
,	m_dSimTime(0.0)
,	m_iLevelIndex(0)
{ }

VveTimeMapper::InternalIndexInfo::~InternalIndexInfo()
{ }


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveTimeMapper::VveTimeMapper()
:	m_iCurrentTimeIndex(0)
,	m_dVisDuration(1.0)
,	m_dSimDuration(1.0)
{
    // we use the standard constructor for implicit timings
    BuildExplicitDefinitionFromImplicitDefinition();
    BuildTimeIndicesFromExplicitDefinition(false);
    BuildTimeLevels();
    BuildLevelIndices();
}

VveTimeMapper::VveTimeMapper(const Timings &oTimings)
:	m_oTimings(oTimings)
,	m_iCurrentTimeIndex(0)
,	m_dVisDuration(1.0)
,   m_dSimDuration(1.0)  
{
    BuildExplicitDefinitionFromImplicitDefinition();
    BuildTimeIndicesFromExplicitDefinition(false);
    BuildTimeLevels();
    BuildLevelIndices();
}

VveTimeMapper::VveTimeMapper(const std::vector<IndexInfo> &oExplicitTimings)
:	m_vecExplicitTimings(oExplicitTimings)
,	m_iCurrentTimeIndex(0)
,	m_dVisDuration(1.0)
,	m_dSimDuration(1.0)  
{
	BuildTimeIndicesFromExplicitDefinition(true);
    BuildTimeLevels();
    BuildLevelIndices();
}

VveTimeMapper::~VveTimeMapper()
{ }



/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
bool VveTimeMapper::SetByExplicitTimeMapping(
	const std::vector<IndexInfo> &vecExplicitTimings)
{
    // save explicit timings
    // this costs memory, but saves runtime when returning explicit timings
	m_vecExplicitTimings = vecExplicitTimings;

    // build time indices from the explicit definition
	BuildTimeIndicesFromExplicitDefinition(true);

    // build the set of references time levels
    BuildTimeLevels();

    // map time indices to time levels using a level index
    BuildLevelIndices();
    return true;
}

bool VveTimeMapper::UpdateExplicitTimeMapping(
	const std::vector<IndexInfo> &vecExplicitTimings)
{
    // save explicit timings
    // this costs memory, but saves runtime when returning explicit timings
	m_vecExplicitTimings = vecExplicitTimings;

    // just update time indices and level indices,
    // do not adjust the time levels
	BuildTimeIndicesFromExplicitDefinition(true);
    BuildLevelIndices();
    
	return true;
}

bool VveTimeMapper::SetByImplicitTimeMapping(const Timings &oImplicitTimings)
{
    // reset timings
	m_vecTimeIndices.clear();
	m_oTimings = oImplicitTimings;

    // first build explicit timings from the CTimings object
    BuildExplicitDefinitionFromImplicitDefinition();

    // then do it the normal way
    BuildTimeIndicesFromExplicitDefinition(false);
    BuildTimeLevels();
    BuildLevelIndices();
    
	return true;
}

bool VveTimeMapper::GetExplicitTimeMapping(
	std::vector<IndexInfo> &vecExplicitTimings) const
{
	vecExplicitTimings = m_vecExplicitTimings;
	return (!m_vecExplicitTimings.empty());
}

bool VveTimeMapper::GetImplicitTimeMapping(Timings &oImplicitTimings) const
{
	oImplicitTimings = m_oTimings;
	return true;
}

void VveTimeMapper::SetImplicitVisRange(double dLowerVisRange,
	double dHigherVisRange)
{
	m_oTimings.m_dVisStart	= dLowerVisRange;
	m_oTimings.m_dVisEnd	= dHigherVisRange;

	m_dVisDuration			= m_oTimings.m_dVisEnd - m_oTimings.m_dVisStart;

	BuildTimeIndicesFromExplicitDefinition(false);
    BuildTimeLevels();
    BuildLevelIndices();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSimulationTime                                           */
/*                                                                            */
/*============================================================================*/
double VveTimeMapper::GetSimulationTime(double dVisTime)
{
	// make sure, we're receiving valid time data
	assert(dVisTime >= 0.0 && "ERROR - received vis time less than zero!");
	assert(dVisTime <= 1.0 && "ERROR - received vis time greater than one!");

	if (!m_oTimings.m_iNumberOfIndices)
		return -1.0;

	if (dVisTime < m_oTimings.m_dVisStart || dVisTime > m_oTimings.m_dVisEnd)
		// i.e. we're outside of our desired time frame
		return -1.0;

	double dVisTimeCoeff = (dVisTime - m_oTimings.m_dVisStart) / m_dVisDuration;

	return m_oTimings.m_dSimStart + dVisTimeCoeff * m_dSimDuration;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeIndex                                                */
/*                                                                            */
/*============================================================================*/
bool VveTimeMapper::LessThanVisTimeIntervalStart(
	const VveTimeMapper::InternalIndexInfo &oInfoA,
	const VveTimeMapper::InternalIndexInfo &oInfoB)
{
	return oInfoA.m_dVisTimeIntervalStart < oInfoB.m_dVisTimeIntervalStart;
}

int VveTimeMapper::GetTimeIndexRandomAccess(double dVisTime) const
{
	if (!m_oTimings.m_iNumberOfIndices)
		return -1;
	
	if (dVisTime < m_oTimings.m_dVisStart || dVisTime > m_oTimings.m_dVisEnd)	
	// i.e. we're outside of our desired time frame
		return -1;

	if (m_oTimings.m_iNumberOfIndices==1)
		return 0;

	// put fVisTime into CInternalIndexInfo structure for comparison
	InternalIndexInfo oTmpInfo;
	oTmpInfo.m_dVisTimeIntervalStart = dVisTime;

	// check lower bound, i.e., the first entry which is not less than oTmpInfo
	std::vector<InternalIndexInfo>::const_iterator itBound =
		lower_bound (m_vecTimeIndices.begin(), m_vecTimeIndices.end(),
			oTmpInfo, VveTimeMapper::LessThanVisTimeIntervalStart);

	// if no lower_bound is found is has to be the last one (range check
	// was executed before)
	if (itBound == m_vecTimeIndices.end())
		return static_cast<int>(m_vecTimeIndices.size()) - 1;

	// else, take the one before the lower_bound, i.e. the last entry which
	// includes fVisTime
	int iIndex = static_cast<int>(itBound-m_vecTimeIndices.begin());
	if(iIndex > 0)
		--iIndex;

	return iIndex;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeIndex                                                */
/*                                                                            */
/*============================================================================*/
int VveTimeMapper::GetTimeIndex(double dVisTime)
{
	if (!m_oTimings.m_iNumberOfIndices)
		return -1;

	if (dVisTime < m_oTimings.m_dVisStart || dVisTime > m_oTimings.m_dVisEnd)	
		// i.e. we're outside of our desired time frame
		return -1;

	if (m_oTimings.m_iNumberOfIndices==1)
		return 0;

	// start searching at the cached time index
	int iIndex = m_iCurrentTimeIndex;

	// if our vis time is smaller than the start of the last known 
	// time level, start searching at the beginning.
	if (m_vecTimeIndices[iIndex].m_dVisTimeIntervalStart > dVisTime)
		iIndex = 0;

	// find the currently valid time index
	do
	{
		++iIndex;
	} while (iIndex < m_oTimings.m_iNumberOfIndices
		&& m_vecTimeIndices[iIndex].m_dVisTimeIntervalStart < dVisTime);
	--iIndex;

	// cache the current time index
	m_iCurrentTimeIndex = iIndex;

	return iIndex;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTimeLevelForLevelIndex                                   */
/*                                                                            */
/*============================================================================*/
int VveTimeMapper::GetTimeLevelForLevelIndex(int iLevelIndex)
{
	if (0 <= iLevelIndex && iLevelIndex < m_oTimings.m_iNumberOfLevels)
	{
		return m_vecTimeLevels[iLevelIndex];
	}

	return -1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetLevelIndexForTimeLevel                                   */
/*                                                                            */
/*============================================================================*/
int VveTimeMapper::GetLevelIndexForTimeLevel(int iTimeLevel)
{
	for (int i=0; i<m_oTimings.m_iNumberOfLevels; ++i)
	{
		if (m_vecTimeLevels[i] == iTimeLevel)
		{
			return i;
		}
	}

	return -1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetVisualizationTime                                        */
/*                                                                            */
/*============================================================================*/
double VveTimeMapper::GetVisualizationTime(double dSimTime)
{
	if (!m_oTimings.m_iNumberOfIndices)
	{
		return -1;
	}

	// i.e. we're outside of our desired time frame
	if (dSimTime < m_oTimings.m_dSimStart || dSimTime > m_oTimings.m_dSimEnd)
	{
		return -1;
	}

	double fSimTimeCoeff = (dSimTime - m_oTimings.m_dSimStart) / m_dSimDuration;

	return m_oTimings.m_dVisStart + fSimTimeCoeff * m_dVisDuration;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetWeightedTimeIndices                                      */
/*                                                                            */
/*============================================================================*/
double VveTimeMapper::GetWeightedTimeIndices(double dVisTime, 
											 int &iIndex1, 
											 int &iIndex2) const
{
	//no indices --> no fun
	if (!m_oTimings.m_iNumberOfIndices)
	{
		return -1.0f;
	}
	// check if we're outside of our desired time frame
	if (dVisTime < m_oTimings.m_dVisStart || dVisTime > m_oTimings.m_dVisEnd)	
	{
		return -1.0f;
	}

	//static case --> trivial
	if (m_oTimings.m_iNumberOfIndices==1)
	{
		iIndex1 = iIndex2 = 0;
		return 0;
	}

	//use random access method here in order to be efficient AND const
	//i.e. do not use internal caching!
	iIndex1 = this->GetTimeIndexRandomAccess(dVisTime);
	
	// find out about the second valid time step
	if (dVisTime < m_vecTimeIndices[iIndex1].m_dVisTime)
	{
		iIndex2 = iIndex1;
		--iIndex1;
	}
	else if (dVisTime > m_vecTimeIndices[iIndex1].m_dVisTime)
	{
		iIndex2 = iIndex1+1;
	}
	else
	{
		iIndex2 = iIndex1;
		return 0;
	}

	double fAlpha = (dVisTime-m_vecTimeIndices[iIndex1].m_dVisTime)
		/ (m_vecTimeIndices[iIndex2].m_dVisTime - m_vecTimeIndices[iIndex1].m_dVisTime);
 
	return fAlpha;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetWeightedLevelIndices                                     */
/*                                                                            */
/*============================================================================*/
double VveTimeMapper::GetWeightedLevelIndices(double dVisTime, 
											  int &iIndex1, 
											  int &iIndex2) const
{
	const double fAlpha = GetWeightedTimeIndices(dVisTime, iIndex1, iIndex2);
	if (fAlpha < 0.0)
		return fAlpha;

	iIndex1 = GetLevelIndex(iIndex1);
	iIndex2 = GetLevelIndex(iIndex2);

	return fAlpha;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Dump                                                        */
/*                                                                            */
/*============================================================================*/
void VveTimeMapper::Dump(bool bDetailed) const
{
	vstr::outi() << " [VveTimeMapper] - vis time: " << m_oTimings.m_dVisStart << " - " 
		<< m_oTimings.m_dVisEnd << " [" << m_dVisDuration << "]" << endl;
	vstr::outi() << "                   sim time: " << m_oTimings.m_dSimStart << " - "
		<< m_oTimings.m_dSimEnd << " [" << m_dSimDuration << "]" << endl;
	vstr::outi() << "                   index count: " << m_oTimings.m_iNumberOfIndices << endl;
	vstr::outi() << "                   level count: " << m_oTimings.m_iNumberOfLevels << endl;

	if (bDetailed)
	{
		vstr::outi() << "        m_vecTimeIndices:" << endl;
		for (std::vector<IndexInfo>::size_type i=0; i<m_vecTimeIndices.size(); ++i)
		{
			const InternalIndexInfo &refInfo = m_vecTimeIndices[i];
			vstr::outi() << "                   idx: " << i << " - lvl idx: " 
				<< refInfo.m_iLevelIndex << " - sim time: " 
				<< refInfo.m_dSimTime << endl;
		}

		vstr::outi() << "       m_vecTimeLevels:" << endl;
		for (std::vector<int>::size_type n=0; n<m_vecTimeLevels.size(); ++n)
		{
			vstr::outi() << "                   idx: " << n << " - lvl: "
				<< m_vecTimeLevels[n] << endl;
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   BuildTimeLevels                                             */
/*                                                                            */
/*============================================================================*/
bool VveTimeMapper::BuildTimeLevels ()
{
    // by now, we've got a list of indices along with level and timing information,
    // but we still need information about the levels only
    set<int> setLevels;
    for (unsigned int i=0; i<m_vecExplicitTimings.size(); ++i)
    {
        setLevels.insert(m_vecExplicitTimings[i].m_iLevel);
    }

    m_vecTimeLevels.resize(setLevels.size());
    set<int>::iterator it = setLevels.begin();
    for(int i=0; it!=setLevels.end(); ++i, ++it)
    {
        m_vecTimeLevels[i] = (*it);
    }
  //  m_vecTimeLevels.assign (setLevels.begin(), setLevels.end());

    m_oTimings.m_iNumberOfLevels = static_cast<int>( m_vecTimeLevels.size() );

    return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   BuildLevelIndices                                           */
/*                                                                            */
/*============================================================================*/

namespace
{
struct _hlp
{
	_hlp()
	:	m_nIdx(0)
	,	m_nLvIdx(0)
	{}

	_hlp(unsigned int nIdx, unsigned int nLvIdx)
	:	m_nIdx(nIdx)
	,   m_nLvIdx(nLvIdx)
	{
	}
	_hlp( const _hlp &other )
		: m_nIdx(other.m_nIdx),
		  m_nLvIdx(other.m_nLvIdx)
	{
	}
	
	unsigned int	m_nIdx;
	unsigned int	m_nLvIdx;

	bool operator<(const _hlp &oOther) const
	{
		return m_nLvIdx < oOther.m_nLvIdx;
	}

	bool operator==(const _hlp &oOther) const
	{
		return oOther.m_nLvIdx == m_nLvIdx;
	}
};

} // namespace


bool VveTimeMapper::BuildLevelIndices ()
{
#if !defined (USE_NAIVE_LEVELINDEX_BUILD)
    //finally -> replace all time levels in m_vecTimeIndices by the 
    //corresponding level indices
	std::vector<_hlp> vecHlp(m_vecTimeLevels.size());

	const unsigned int nNumTimeLevels = static_cast<unsigned int>(m_vecTimeLevels.size());

	for(unsigned int n=0; n<nNumTimeLevels; ++n)
	{
		vecHlp[n].m_nIdx = n;
		vecHlp[n].m_nLvIdx = m_vecTimeLevels[n];
	}

	// The below std::lower_bound requires this sort in order to run as
	// expected.
	std::sort( vecHlp.begin(), vecHlp.end() );

    for(size_t i=0; i<m_vecExplicitTimings.size(); ++i)
    {
		std::vector<_hlp>::const_iterator cit 
			= std::lower_bound( vecHlp.begin(), vecHlp.end(),
				_hlp( 0, m_vecExplicitTimings[i].m_iLevel ) );

		if(cit != vecHlp.end())
		{
			m_vecTimeIndices[i].m_iLevelIndex = (*cit).m_nIdx;
		}
		else
		{
			continue; // @todo: error?
		}
    }

#else

    for(unsigned int i=0; i<m_vecExplicitTimings.size(); ++i)
    {
        for(unsigned int j=0; j<m_vecTimeLevels.size(); ++j)
        {
            if(m_vecExplicitTimings[i].m_iLevel == m_vecTimeLevels[j])
            {
                m_vecTimeIndices[i].m_iLevelIndex = j;
                break;
            }
        }
    }

#endif // endif

    return true;
}

bool VveTimeMapper::BuildExplicitDefinitionFromImplicitDefinition()
{
    // no, we are using implicit definitions
    m_dSimDuration = m_oTimings.m_dSimEnd - m_oTimings.m_dSimStart;
    m_dVisDuration = m_oTimings.m_dVisEnd - m_oTimings.m_dVisStart;

    if (m_dSimDuration < 0)
    {
        m_oTimings.m_iNumberOfLevels = 0;
        m_oTimings.m_iNumberOfIndices = 0;
        return true; // we failed, but then again... 
    }
	// no, we didn't, so fill in the data according to the implicit definitions

	if (m_oTimings.m_iNumberOfLevels < 1 || m_oTimings.m_iNumberOfIndices < 1)
	{
		m_oTimings.m_iNumberOfLevels = 0;
		m_oTimings.m_iNumberOfIndices = 0;
		
		return false;
	}

    // resize old explicit timings..you don't need clearing as all entries will be overwritten
    m_vecExplicitTimings.resize (m_oTimings.m_iNumberOfIndices);
   

    m_vecExplicitTimings [0].m_dSimTime = m_oTimings.m_dSimStart;
    m_vecExplicitTimings [0].m_iLevel = m_oTimings.m_iFirstLevel;

    // create explicit timings from implicit description
    // just set simtime and level, the rest will be constructed by BuildTimeIndicesFromExplicitDefinition
	IndexInfo oInfo;
	for (int i=1; i<m_oTimings.m_iNumberOfIndices; ++i)
	{
		m_vecExplicitTimings [i].m_dSimTime = m_oTimings.m_dSimStart + m_dSimDuration*(double(i)/double(m_oTimings.m_iNumberOfIndices-1));
		m_vecExplicitTimings [i].m_iLevel = m_oTimings.m_iFirstLevel + ((i%m_oTimings.m_iNumberOfLevels) * m_oTimings.m_iLevelStride);
	}

	return true;
}

bool VveTimeMapper::BuildTimeIndicesFromExplicitDefinition(bool bSetSimTimings)
{
	assert (!m_vecExplicitTimings.empty());
	// we've read something, so compute the starts of the intervals,
	// where the respective time levels become valid... (poor English - I know...)
	m_oTimings.m_iNumberOfIndices = static_cast<int>(
		m_vecExplicitTimings.size() );

	if(bSetSimTimings)
	{
		m_oTimings.m_dSimStart = m_vecExplicitTimings[0].m_dSimTime;
		m_oTimings.m_dSimEnd = m_vecExplicitTimings[m_oTimings.m_iNumberOfIndices-1].m_dSimTime;
		m_dSimDuration = m_oTimings.m_dSimEnd - m_oTimings.m_dSimStart;
	}
	
    // resize old time indices..you don't need clearing as all entries will be overwritten
    m_vecTimeIndices.resize(m_oTimings.m_iNumberOfIndices);


	// sanity checks:
	if(m_oTimings.m_iNumberOfIndices > 1)
	{
		if(m_dSimDuration < 0.0)
		{
			// okay, quite superficial, but we do this, nevertheless...
			// obviously this data is not legal -> don't show anything...
			m_oTimings.m_iNumberOfLevels = 0;
			m_oTimings.m_iNumberOfIndices = 0;

			return true;
		}

		// calculate visualization times for the single time levels
		for(int i=0; i<m_oTimings.m_iNumberOfIndices; ++i)
		{
            // copy simtime from explicit timings
            m_vecTimeIndices[i].m_dSimTime = m_vecExplicitTimings[i].m_dSimTime;

			// first: scale to [0,1]
			m_vecTimeIndices[i].m_dVisTime = m_dSimDuration ? ((m_vecTimeIndices[i].m_dSimTime - m_oTimings.m_dSimStart) / m_dSimDuration) : 0;
			// then: scale to [m_fVisStart, m_fVisEnd]
			m_vecTimeIndices[i].m_dVisTime = (m_vecTimeIndices[i].m_dVisTime * m_dVisDuration) + m_oTimings.m_dVisStart;

			// compute the start of the validity interval for this time level
			if(i > 0)
			{
				m_vecTimeIndices[i].m_dVisTimeIntervalStart 
					= (m_vecTimeIndices[i].m_dVisTime + m_vecTimeIndices[i-1].m_dVisTime) / 2.0f;
			}
			else
				m_vecTimeIndices[i].m_dVisTimeIntervalStart = m_oTimings.m_dVisStart;
		}

	}
	else
	{
		m_vecTimeIndices[0].m_dVisTimeIntervalStart = m_oTimings.m_dVisStart;
	}

    // reset current time index
    m_iCurrentTimeIndex= 0;

	return true;
}

size_t VveTimeMapper::GetSize() const
{
	return sizeof(VveTimeMapper)
		+ m_vecTimeIndices.size() * sizeof(InternalIndexInfo)
		+ m_vecTimeLevels.size() * sizeof(int)
		+ m_vecExplicitTimings.size() * sizeof(IndexInfo);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// NAMEABLE INTERFACE
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
std::string VveTimeMapper::GetNameForNameable() const
{
	return m_strName;
}

void VveTimeMapper::SetNameForNameable(const std::string &strName)
{
	m_strName = strName;
}

/*============================================================================*/
/*  END OF FILE "VveTimeMapper.cpp"                                           */
/*============================================================================*/
