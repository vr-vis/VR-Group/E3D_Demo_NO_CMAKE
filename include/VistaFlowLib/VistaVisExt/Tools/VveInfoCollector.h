/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEINFOCOLLECTOR_H
#define _VVEINFOCOLLECTOR_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include <vector>
#include <VistaVisExt/IO/VveDataSetName.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkDataArray;
class vtkDataSet;
class vtkExtractGeometry;
class vtkBox;
class vtkDataSetReader;
class VveDataSetInfo;
class VveDataChunkInfo;
class VveOctree;
class VveOctreeNode;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Collect information (e.g. min, max...) for all data fields
* and also information for histogram of each data field.
*
* This algorithm will use the following hierarchical structure:
*
*	VveDatasetInfo -->* VveDataChunkInfo (time steps) -->* VveDataChunkInfo (blocks)
*
* Blocks can be used in two ways, first for multi-block data 
* (specify min/max block and set SetGenerateBlocks to zero), or for generated blocks 
* which are octree leaf nodes (costs some time!, SetGenerateBlocks > 0 ).
* 
*/
class VISTAVISEXTAPI VveInfoCollector
{
public:
	enum{
		MSG_READFILE,
		MSG_LAST
	};

	VveInfoCollector();
	virtual ~VveInfoCollector();

	/*
	* Set data set name, which is given as a pattern.
	*
	*/
	void SetDataSetName(const std::string&);
	std::string GetDataSetName() const;

	/*
	* Min/max time step numbers (time levels).
	*
	*/
	void SetMinTimeStep(int);
	int GetMinTimeStep() const;
	
	void SetMaxTimeStep(int);
	int GetMaxTimeStep() const;

	/*
	* Number of bins for histograms.
	*
	*/
	void SetNumBins(int);
	int GetNumBins() const;
	
	/*
	* This call combines CollectInformation, BuildHistogram
	*/
	bool CollectAllInformation(VveDataSetInfo *, int iJointWindow = 0);

	/*
	* Collect information for whole data set. This will hierarchically collect information
	* for all time steps and all blocks within time step.
	*/
	bool CollectInformation(VveDataSetInfo *);

	/*
	* Build hierarchical histogram information for data set info
	*/
	//void BuildHistogram(VveDataSetInfo *);

	/*
	* Build joint 2D histogram information within a time window of 
	* iTimeWindow time steps.
	*/
	void BuildJointHistogram(VveDataSetInfo *, int iTimeWindow);

	/*
	* If this is no multi-block data set, and iPointTreshold > 0, compute blocks
	* based on an octree structure and try to put iPointTreshold points into each block.
	*
	*/
	void SetGenerateBlocks (int iPointTreshold);
	int GetGenerateBlocks () const;

protected:
	/*
	* Collect info for a single time level
	*/
	VveDataChunkInfo* CollectInfoSingleTimeLevel(int iTimeLevel);

	/*
	* Collect info for a single block
	*/
	VveDataChunkInfo* CollectInfoSingleChunk(int iChunkID, const std::string & strSemanticName, vtkDataSet*);

	/*
	* Compute average in data field
	*/
	double ComputeAverage(vtkDataArray*, int);

	/*
	* Compute standard deviation in data field with given average
	*/
	double ComputeStandardDeviation(vtkDataArray*, int, double dAverage);

	/*
	* Get explicit file name from stored file pattern with time step
	*/
	std::string GetFileName(int) const;
	
	/*
	* Load a single file.
	*/
	vtkDataSet* LoadFile (vtkDataSetReader *pReader, int iTimeStep);

	
	/*
	* Block management: as the InfoCollector supports two modi, (A) multi-block and (B)
	* generation of octree-based blocks, to retrieve a block different methods are needed.
	* 
	* First call InitBlockManagement for each time step, and this will return the min/max block number. 
	* In (A), these are the multi-block information provided by the user, in (B) this is 0,n-1, where n is
	* the number of leafs in the generated octree.
	*
	* After that, GetBlock returns the i-th block (starting from zero) for the current time step. In (A),
	* this will load block iMinBlock+i from the current time step, in (B) this will extract the data
	* within the i-th octree leaf node from the current time step.
	*/

	/*
	* Initialize block management for iTimeStep. This will, depending on multi-block or generate blocks
	* mode, generate blocks from an octree and determine min/max block numbers.
	*/
	bool InitTimeLevel (int iTimeLevel, size_t &nMinBlock, size_t &nMaxBlock);

	/*
	* Get block nBlock with initialized block management, i.e. for current time step.
	*/
	vtkDataSet* GetBlock(size_t nBlock);

	/*
	* Return current time step with initalized block management, if available. For multi-block data,
	* this will return NULL, as we would need to append and clean the joined blocks.
	*/
	vtkDataSet* GetCurrentTimeLevel () const;

	/*
	* for joint histograms: a time window
	*/
	struct sTimeWindowSlot {
		int	iTimeLevel;
		vtkDataSet* pTimeLevelData;
		std::vector<vtkDataSet*> vecBlockData;
	};

	bool FillTimeWindow (int iCurrentTimeLevel, int iForwardSize, std::vector<sTimeWindowSlot> & vecTimeWindow);
	bool ReleaseTimeWindowSlot (int iReleaseTimeLevel, std::vector<sTimeWindowSlot> & vecTimeWindow);

private:
	// data set information
	VveDataSetName m_oDSName;
	int m_iMinTimeLevel;
	int m_iMaxTimeLevel;

	// number of bins for histograms
	int m_iNumBins;

	// for octree blocks
	int	m_iGenerateBlocks;
	bool m_bGenerateBlocks;

	vtkDataSet*			m_pTimeStepData;
	vtkExtractGeometry*	m_pExtractGeometry;
	vtkBox*				m_pBoundingBox;
	vtkDataSetReader *	m_pBlockReader;

	VveOctree*			m_pOctree;
	std::vector<VveOctreeNode*> m_vecLeafs;
	int					m_iCurrentTimeLevel;

};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VVEINFOCOLLECTOR_H

