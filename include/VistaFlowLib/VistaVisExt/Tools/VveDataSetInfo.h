/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEDATASETINFO_H
#define _VVEDATASETINFO_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveDataChunkInfo.h"
#include <string>
#include <vector>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveDataFieldInfo;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Hold meta data (e.g. min, max...) for a data field (e.g. scalar field)
* over the whole data set (as this is a VveDataChunkInfo).
*/
class VISTAVISEXTAPI VveDataSetInfo : public VveDataChunkInfo
{
public:
	VveDataSetInfo();
	virtual ~VveDataSetInfo();

	void SetMinTimeStep(int);
	int GetMinTimeStep() const;
	
	void SetMaxTimeStep(int);
	int GetMaxTimeStep() const;

	void SetDataSetName(const std::string&);
	std::string GetDataSetName() const;

private:
	int m_iMinTimeStep;
	int m_iMaxTimeStep;
	std::string m_strDataSetName;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VVEDATASETINFO_H

