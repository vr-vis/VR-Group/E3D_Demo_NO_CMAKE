/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/*  INCLUDES                                                                  */
/*============================================================================*/
#include "VtkPolyDataTools.h"

#include <vtkCell.h>
#include <vtkMath.h>
#include <vtkCellArray.h>

#include <VistaBase/VistaVectorMath.h>
#include <VistaMath/VistaSquareMatrix.h>
#include <VistaMath/VistaLeastSquaresPlane.h>

#include <cassert>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VtkPolyDataTools::VtkPolyDataTools(vtkPolyData* pMesh)
    :   m_pMesh(pMesh),
        m_pTmpIds(vtkIdList::New())
{
    assert(pMesh != NULL && "[VtkPolyDataTools::CONSTRUCTOR] Cannot operate on NULL input!");
	//register with the input mesh in order to prevent premature deletion
	m_pMesh->Register(NULL);
}

VtkPolyDataTools::~VtkPolyDataTools()
{
	//unregister with the current input mesh in order to allow proper deletion
	m_pMesh->UnRegister(NULL);
    m_pTmpIds->Delete();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeVertexNeighbors                                      */
/*                                                                            */
/*============================================================================*/
void VtkPolyDataTools::ComputeVertexNeighbors(vtkIdType iVertexId, set<vtkIdType>& setNeighbors)
{
    assert(iVertexId>=0 && iVertexId < m_pMesh->GetNumberOfPoints() && "[VtkPolyDataTools::ComputeVertexNeighbors] Illegal vertex id");

    vtkIdType* pIncidentCells;
    unsigned short iNumCells;
    //fetch all cells incident to point iVertexId
    m_pMesh->GetPointCells(iVertexId, iNumCells, pIncidentCells);
    
    int i(0);
    int j(0);
    vtkIdType* pIncidentPoints;
    vtkIdType iNumPoints;

    //for each incident cell
    for(; i<iNumCells; ++i)
    {
        //fetch cell's points
        m_pMesh->GetCellPoints(pIncidentCells[i], iNumPoints, pIncidentPoints);
        for(j=0; j<iNumPoints; ++j)
            /* insert all neighbor points != iVertexId into set
            *   NOTE:   This works ONLY for triangle meshes!!! When dealing with arbitrary
                        polygonal data (e.g. quadrilaterals) one has to insert an IsEdge-Test 
                        here!
            */
            if(pIncidentPoints[j]!=iVertexId)
                setNeighbors.insert(pIncidentPoints[j]);
    }
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeVertexNeighbors                                      */
/*                                                                            */
/*============================================================================*/
void VtkPolyDataTools::ComputeVertexNeighbors(vtkIdType iVertexId, vtkIdList* pNeighbors)
{
    //compute neighbors
    set<vtkIdType> setNeighbors;
    ComputeVertexNeighbors(iVertexId, setNeighbors);
        
    //embed into vtk data structure
    pNeighbors->SetNumberOfIds(setNeighbors.size());
    vtkIdType i(0);
    set<vtkIdType>::iterator itNeighbor;
    for(itNeighbor = setNeighbors.begin();
        itNeighbor != setNeighbors.end();
        ++itNeighbor, ++i)
    {
        pNeighbors->InsertId(i,*itNeighbor);
    }
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetEdgeIncidentCells                                        */
/*                                                                            */
/*============================================================================*/
int VtkPolyDataTools::GetEdgeIncidentCells(vtkIdType iVertex1, vtkIdType iVertex2, vtkIdList* pCells)
{
    //sanity check
    if(!m_pMesh->IsEdge(iVertex1,iVertex2))
        return -1;
    /*
    * Get the cells incident to vertex 1 and vertex 2
    */
    m_pMesh->GetPointCells(iVertex1, m_iV1NumCells, m_pV1Cells);
    m_pMesh->GetPointCells(iVertex2, m_iV2NumCells, m_pV2Cells);
    /**
    * compute cut of cell sets
    */
    pCells->Reset();
    register int j, i;
    for(i=0; i<m_iV1NumCells; ++i)
        for(j=0; j<m_iV2NumCells; ++j)
            if(m_pV1Cells[i] == m_pV2Cells[j])
                pCells->InsertNextId(m_pV1Cells[i]);

#ifdef DEBUG
    if(pCells->GetNumberOfIds() > 2)
    {
        vstr::warnp() << "[VtkPolyDataTools::GetEdgeIncidentCells] "
			 << "Found COMPLEX EDGE! (#Triangles: " <<  pCells->GetNumberOfIds()
			 << ") Mesh is not two-manifold!" << endl;
    }
#endif
    /**
    * the given edge is BOUNDARY iff there is exactly ONE incident triangle
    */
    return pCells->GetNumberOfIds();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCellBoundaryEdges                                        */
/*                                                                            */
/*============================================================================*/
size_t VtkPolyDataTools::GetCellBoundaryEdges(vtkIdType iCell, vector< pair<vtkIdType,vtkIdType> >& vecEdges)
{
    this->GetCellEdges(iCell, vecEdges);
    vector< pair<vtkIdType,vtkIdType> >::iterator itCurrent(vecEdges.begin());
    vector< pair<vtkIdType,vtkIdType> >::iterator itEnd(vecEdges.end());
    while(itCurrent != itEnd)
    {
        if(!this->IsBoundaryEdge(itCurrent->first, itCurrent->second))
            itCurrent = vecEdges.erase(itCurrent);
        else
            ++itCurrent;
    }
    return vecEdges.size();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetCellEdges                                                */
/*                                                                            */
/*============================================================================*/
size_t VtkPolyDataTools::GetCellEdges(vtkIdType iCell, vector< pair<vtkIdType,vtkIdType> >& vecEdges)
{
    vtkIdType iNumPts;
    vtkIdType *pPoints;
    vecEdges.clear();
    m_pMesh->GetCellPoints(iCell, iNumPts, pPoints);
    vecEdges.reserve((iNumPts * iNumPts)/2 + 1);
    pair<vtkIdType,vtkIdType> paEdge;
    int j, i=0;
    //check all possible edges
    for(; i<iNumPts; ++i){
        for(j=i+1; j<iNumPts; ++j){
            if(m_pMesh->IsEdge(i,j)){
                paEdge.first = i;
                paEdge.second = j;
                vecEdges.push_back(paEdge);
            }
        }
    }
    return vecEdges.size();    
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeLeastSquaresPlane                                    */
/*                                                                            */
/*============================================================================*/
void VtkPolyDataTools::ComputeLeastSquaresPlane(const std::set<vtkIdType>& setPoints, 
                                                 double fPlaneCenter[3], 
                                                 double fPlaneNormal[3])
{
    vector<double*> vecPoints;
    vecPoints.resize(setPoints.size(), NULL);
    set<vtkIdType>::const_iterator itId(setPoints.begin());
    set<vtkIdType>::const_iterator itEnd(setPoints.end());
    int i=0;
    for(;itId != itEnd; ++itId, ++i)
    {
        vecPoints[i] = m_pMesh->GetPoint(*itId);
    }
    VistaLeastSquaresPlane::ComputeLeastSquaresPlane(vecPoints,fPlaneCenter,fPlaneNormal);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeLeastSquaresPlane                                    */
/*                                                                            */
/*============================================================================*/
void VtkPolyDataTools::ComputeLeastSquaresPlane(const vtkIdType iSetSize, vtkIdType* pSet,
                                                 double fPlaneCenter[3], 
                                                 double fPlaneNormal[3])
{
    vector<double*> vecPoints;
    vecPoints.resize(iSetSize, NULL);
    int i=0;
    for(;i<iSetSize;++i)
        vecPoints[i] = m_pMesh->GetPoint(pSet[i]);
    VistaLeastSquaresPlane::ComputeLeastSquaresPlane(vecPoints,fPlaneCenter,fPlaneNormal);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeBoundaryPolygon                                      */
/*                                                                            */
/*============================================================================*/
bool VtkPolyDataTools::ComputeBoundaryPolygon(const set<vtkIdType>& setBoundaryPoints, 
                                               vtkIdType*& pBoundaryLoop)
{
    //allocate mem for boundary loop representation
    pBoundaryLoop = new vtkIdType[setBoundaryPoints.size()+1];
    //set of current veritex's incident vertices
    set<vtkIdType> setNeighbors;
    //iterator for the "current neighbor id" into the current neighborhood set
    set<vtkIdType>::iterator itCurrentNeighbor;
    //end of the current neighborhood set
    set<vtkIdType>::iterator itLastNeighbor;
    //iterator for the "current neighbor id" into the boundary point set
    set<vtkIdType>::iterator itNeighborPointer;
    //indicate if next id has been found
    bool bFoundNeigh(false);
    //get and erase first element
    vtkIdType iCurrentId(*(setBoundaryPoints.begin()));
    pBoundaryLoop[0] = iCurrentId;
    //remember the node we are coming from
    vtkIdType iLastId(-1);
    unsigned int iCurrentLoopPosition(1);
	unsigned int iOldLoopPosition(1);
    
    while(iCurrentLoopPosition <= setBoundaryPoints.size())
    {
        setNeighbors.clear();      
        //compute 1-ring neighborhood
        this->ComputeVertexNeighbors(iCurrentId, setNeighbors);
        //start neighborhood traversal
        itCurrentNeighbor = setNeighbors.begin();
        itLastNeighbor = setNeighbors.end();
        //for all neighboring vertices
        for(; itCurrentNeighbor != itLastNeighbor; ++itCurrentNeighbor)
        {
			/**
			* We assume that there're exactly two boundary vertices adjacent to the current vertex.
			* The one we are coming from can be ruled out. So it must be the other one.
			* In every iteration of the surrounding while loop the inner for loop should increase
			* the iCurrentLoopPosition counter by exactly one. If iCurrentLoopPosition wasn't increased,
			* the algorithm would be trapped in an endless loop. Therefore we test for this condition.
			*/
            //check point for validity
            if( *itCurrentNeighbor != iLastId && //is this the vertex we are coming from?
                this->IsBoundaryEdge(iCurrentId, *itCurrentNeighbor)) //check if edge is boundary
            {
                //This is the next loop vertex -> save it
                iLastId = iCurrentId;
                iCurrentId = *itCurrentNeighbor;
                //insert it into the loop
                pBoundaryLoop[iCurrentLoopPosition] = iCurrentId;
                ++iCurrentLoopPosition;         
                break;
            }
        }
		//check if loop position has been updated once during the last neighborhood traversal
		if(iOldLoopPosition == iCurrentLoopPosition)
		{
			//if this was not the case we have an endless loop here!
			#ifdef DEBUG
				vstr::errp() << "[VtkPolyDataTools::ComputeBoundaryPolygon] Endless loop detection engaged! BREAKING" << endl;
			#endif
			return false;
		}
		else
			iOldLoopPosition = iCurrentLoopPosition;
    }
	//ASSERT that we have constructed a loop polygon
	if(pBoundaryLoop[0]==pBoundaryLoop[setBoundaryPoints.size()])
		return true;
	//still here? -> loop condition is not fullfilled.
	#ifdef DEBUG
		vstr::errp() << "[VtkPolyDataTools::ComputeBoundaryPolygon] FAILED TO CONSTRUCT A POLYGON LOOP!" << endl;
	#endif
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ProjectPointsToPlane                                        */
/*                                                                            */
/*============================================================================*/
void VtkPolyDataTools::ProjectPointsToPlane(set<vtkIdType> setPoints, 
                                             double fCenter[3], 
                                             double fNormal[3], 
                                             double fRelaxation)
{
	//create an array rep.
	vtkIdType iSetSize(setPoints.size());
	vtkIdType *pSet(new vtkIdType[iSetSize]);
	//convert set to array
	int i(0);
	set<vtkIdType>::iterator itId(setPoints.begin());
    set<vtkIdType>::iterator itEnd(setPoints.end());
    for(;itId != itEnd; ++itId,++i)
    	pSet[i] = *itId;
	//call native routine
	this->ProjectPointsToPlane(iSetSize,pSet,fCenter,fNormal,fRelaxation);
	//cleanup
	delete[] pSet;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ProjectPointsToPlane                                        */
/*                                                                            */
/*============================================================================*/
void VtkPolyDataTools::ProjectPointsToPlane(  const vtkIdType iSetSize, vtkIdType *pSet,
                                double fCenter[3], double fNormal[3], 
                                double fRelaxation)
{
	vtkPoints *pPoints = m_pMesh->GetPoints();
    double fX[3];
    double fScale(0.0);
    int k=0,i=0;
    
    for(;k<iSetSize; ++k)
    {
        //fetch point
        pPoints->GetPoint(pSet[k], fX);
        //compute projection to normal
        fScale = 0.0;
        for(i=0; i<3; ++i)
            fScale += (fX[i]-fCenter[i]) * fNormal[i];
        //NOTE: THESE NEED TO BE TWO SEPARATE FOR LOOPS!
        //add relaxation*(scale*normal)  
        for(i=0; i<3; ++i)
            fX[i] -= fRelaxation * fScale * fNormal[i];
        //set point back
        pPoints->SetPoint(pSet[k], fX);
    }
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :      ShouldEdgeBeFlipped                                      */
/*                                                                            */
/*============================================================================*/
bool VtkPolyDataTools::ShouldEdgeBeFlipped(vtkIdType iA, vtkIdType iB, vtkIdType iC, vtkIdType iD)
{
    vtkIdType iABC[3] = {iA, iB, iC};
    vtkIdType iADB[3] = {iA, iD, iB};
    vtkIdType iADC[3] = {iA, iD, iC};
    vtkIdType iBCD[3] = {iB, iC, iD};
    double fAngles1[3], fAngles2[3];
    //determine minimum interior angle of first configuration
    this->ComputeInnerAngles(iABC,fAngles1);
    this->ComputeInnerAngles(iADB,fAngles2);
    
    /*  check legality constraint => if either the total angle @ A or that @ B exceed 180� the flip
        will result in an illegal triangle configuration! */
    if( (fAngles1[0] + fAngles2[0] >= vtkMath::Pi()) || //check angles @ A
        (fAngles1[1] + fAngles2[2] >= vtkMath::Pi()) )  //check angles @ B
        return false;
    
    float fMin1(numeric_limits<float>::max());
    for(int i=0; i<3; ++i)
    {
		fMin1 = std::min<float>(fMin1,fAngles1[i]);
		fMin1 = std::min<float>(fMin1,fAngles2[i]);
    }
    //determine minimum interior angle of first configuration
    this->ComputeInnerAngles(iADC,fAngles1);
    this->ComputeInnerAngles(iBCD,fAngles2);
    float fMin2(numeric_limits<float>::max());
    for(int i=0; i<3; ++i)
    {
    	fMin2 = std::min<float>(fMin2,fAngles1[i]);
		fMin2 = std::min<float>(fMin2,fAngles2[i]);
    }
    //the flip is useful <=> the minimal angle is increased!
    return fMin2 > fMin1;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :       IsEdgeFlippable                                         */
/*                                                                            */
/*============================================================================*/
bool VtkPolyDataTools::IsEdgeFlippable(vtkIdType iA, vtkIdType iB, vtkIdType iC, vtkIdType iD)
{
    vtkIdType iABC[3] = {iA, iB, iC};
    vtkIdType iADB[3] = {iA, iD, iB};
    double fAngles1[3], fAngles2[3];
    
    this->ComputeInnerAngles(iABC,fAngles1);
    this->ComputeInnerAngles(iADB,fAngles2);
    
    return  (fAngles1[0] + fAngles2[0] < vtkMath::Pi()) &&   //check angles @ A
            (fAngles1[1] + fAngles2[2] < vtkMath::Pi());     //check angles @ B
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :       FlipEdge                                                */
/*                                                                            */
/*============================================================================*/
void VtkPolyDataTools::FlipEdge(vtkIdType iTriID1, vtkIdType iTriID2, vtkIdType iA, vtkIdType iB, vtkIdType iC, vtkIdType iD)
{
    /* Do something like this:
     *
     *                   A                             A                          
     *                  /|\                           / \                          
     *                 / | \                         /   \                        
     *                /  |  \                       /  1  \                       
     *               D 2 | 1 C       ===>          D-------C                       
     *                \  |  /                       \  2  /                        
     *                 \ | /                         \   /                         
     *                  \|/                           \ /                          
     *                   B                             B                           
     *                                                                             
     */                                                                                   
    //remove old cell references A->iTriID2 and B->iTriID1
    m_pMesh->RemoveReferenceToCell(iA, iTriID2);
    m_pMesh->RemoveReferenceToCell(iB, iTriID1);
    //replace triangles
    vtkIdType swapTri[3];
    swapTri[0] = iA; swapTri[1] = iD, swapTri[2] = iC;
    m_pMesh->ReplaceCell(iTriID1, 3, swapTri);
    swapTri[0] = iB; swapTri[1] = iC, swapTri[2] = iD;
    m_pMesh->ReplaceCell(iTriID2, 3, swapTri);
    //update links
    m_pMesh->ResizeCellList(iC,1);
    m_pMesh->AddReferenceToCell(iC,iTriID2);
    m_pMesh->ResizeCellList(iD,1);
    m_pMesh->AddReferenceToCell(iD,iTriID1);

}
/*============================================================================*/
/*                                                                            */
/*  NAME      :      SplitEdge                                                */
/*                                                                            */
/*============================================================================*/
vtkIdType VtkPolyDataTools::SplitTriangle(vtkIdType iTriID, vtkIdType iV1, vtkIdType iV2)
{
    /* Do something like this:
     *
     *                   V1                            V1                          
     *                  /|\                           /|\                          
     *                 / | \                         / | \                        
     *                /  |  \                       /  |  \                       
     *               D 1 | 2 C       ===>          D---+---C                       
     *                \  |  /                       \  |  /                        
     *                 \ | /                         \ | /                         
     *                  \|/                           \|/                          
     *                   V2                            V2                           
     *                                                                             
     */                                                                                   
    vtkIdType iNumPts, *pPts;
    m_pMesh->GetCellPoints(iTriID,iNumPts,pPts);
    if(iNumPts != 3)
    {
		vstr::errp()<<"[VtkPolyDataTools::SplitLongestEdge] Unable to operate on non-triangle mesh!"<<endl;
        return -1;
    }
    //get the neighboring cell
    int iNumNeighbors(this->GetEdgeIncidentCells(iV1,iV2,m_pTmpIds));
    if(iNumNeighbors > 2 || iNumNeighbors < 0)
    {
		vstr::errp()<<"[VtkPolyDataTools::SplitLongestEdge] Unable to operate on non-manifold mesh!"<<endl
			<<"\tFound edge with "<<iNumNeighbors<<" incident cells...\n\n\n";
        return -1;
    }
    double fMid[3];
    this->ComputeEdgeMidPoint(iV1,iV2,fMid);
    //insert the new point with 4 links (one for each possible triangle)
    vtkIdType iNewID = m_pMesh->InsertNextLinkedPoint(fMid,4);
    this->SplitTriangle(iTriID, iV1, iV2, iNewID);
    
    //take care of the neighbor if any
    if(iNumNeighbors == 2)
    {
        vtkIdType iNeighborTri = (m_pTmpIds->GetId(0) == iTriID ? m_pTmpIds->GetId(1) : m_pTmpIds->GetId(0));
        this->SplitTriangle(iNeighborTri, iV2, iV1, iNewID);
    }
    return iNewID;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     ComputeLongestEdge                                        */
/*                                                                            */
/*============================================================================*/
float VtkPolyDataTools::ComputeLongestEdge(vtkIdType iTriID, vtkIdType& iVertex1, vtkIdType& iVertex2)
{
    int i;
    //vtkIdList *pCellPoints = m_pMesh->GetCell(iTriID)->GetPointIds();
    m_pMesh->GetCellPoints(iTriID, m_iNumPts, m_pCellPts);
    
    double **fV = new double*[m_iNumPts];
    
    for(i=0; i<m_iNumPts; ++i)
	{
        fV[i] = new double[3];
		m_pMesh->GetPoint(m_pCellPts[i], fV[i]);
	}
    
    float fMax(-numeric_limits<float>::max());
    float fDist(0.0f);
    for(i=0; i<m_iNumPts; ++i) // for all vertices
    {
        fDist = vtkMath::Distance2BetweenPoints(fV[i],fV[(i+1) % m_iNumPts]);
		//we need no square root here since we only want to have the longest edge!
        //check if we got a new maximum and adapt values accordingly
        if(fDist > fMax)
        {
            iVertex1 = m_pCellPts[i];
            iVertex2 = m_pCellPts[(i+1) % m_iNumPts];
            fMax = fDist;
        }
    }
    
	for(int i=0; i<m_iNumPts; ++i)
		delete[] fV[i];
	delete[] fV;

    return sqrt(fMax);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     ComputeInnerAngles                                        */
/*                                                                            */
/*============================================================================*/
void VtkPolyDataTools::ComputeInnerAngles(int iCellID, double fAngles[3])
{
    m_pMesh->GetCellPoints(iCellID, m_iNumPts, m_pCellPts);
    if(m_iNumPts != 3)
    {
		vstr::errp()<<"[VtkPolyDataTools::ComputeInnerAngles] Will only operate on triangles!"<<endl;
        return;
    }
    this->ComputeInnerAngles(m_pCellPts, fAngles);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     ComputeInnerAngles                                        */
/*                                                                            */
/*============================================================================*/
void VtkPolyDataTools::ComputeInnerAngles(vtkIdType iPtIDs[3], double fAngles[3])
{
    double *fV[3]; //vertices
    int i,j;
    for(i=0; i<3; ++i)
        fV[i] = m_pMesh->GetPoint(iPtIDs[i]);
    //compute triangle sides and normalize length
    double sides[3][3];
    for(i=0; i<3; ++i) //for all vertices
        for(j=0; j<3; ++j) //for x,y,z
            sides[i][j] = fV[(i+1)%3][j] - fV[i][j];
    for(i=0; i<3; ++i)
        vtkMath::Normalize(sides[i]);
    //finally compute angles
    for(i=0; i<3; ++i)
        fAngles[i] = vtkMath::Pi() - acos(vtkMath::Dot(sides[(i+2)%3],sides[i]));
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     ComputeTriangleArea                                       */
/*                                                                            */
/*============================================================================*/
float VtkPolyDataTools::ComputeTriangleArea(vtkIdType iTriID)
{
    vtkCellArray *pPolys = m_pMesh->GetPolys();
    
    //This will only work with triangle meshes!!
    assert(pPolys->GetMaxCellSize() == 3 && "Unable to compute area of generic polygon!");

    pPolys->GetCell(3*iTriID,m_iNumPts, m_pCellPts);

    double *fA(m_pMesh->GetPoint(m_pCellPts[0]));
    double *fB(m_pMesh->GetPoint(m_pCellPts[1]));
    double *fC(m_pMesh->GetPoint(m_pCellPts[2]));
    
    double fAB[3] = {fB[0] - fA[0], fB[1] - fA[1], fB[2] - fA[2]};
    double fAC[3] = {fC[0] - fA[0], fC[1] - fA[1], fC[2] - fA[2]};
    double fCross[3];

    vtkMath::Cross(fAB,fAC,fCross);

    return vtkMath::Norm(fCross) * 0.5f;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     SplitTriangle                                             */
/*                                                                            */
/*============================================================================*/
vtkIdType VtkPolyDataTools::SplitTriangle(vtkIdType iTriID, vtkIdType iV1, vtkIdType iV2, vtkIdType iNewID)
{
    /**
	*            iApex                 iApex
	*             / \                   /|\ 
	*            /   \                 / | \
	*           /     \               /  |  \
	*          /       \             /   |   \
	*         / iTriId  \           /    |    \
	*        /           \         /     |iNew \
	*       /             \       /iTriId|TriId \
	*     V1---------------V2   V1----iNewID----V2
	*/
	vtkIdType iApex(this->GetApexVertex(iTriID, iV1, iV2));
    //apex's valence will increase by 1 => allocate one more link!
    m_pMesh->ResizeCellList(iApex, 1); 
    //replace old triangle with first one
    m_pMesh->RemoveReferenceToCell(iV2,iTriID); //v2 is not adjacent to new triangle
    m_pMesh->AddReferenceToCell(iNewID,iTriID); //new vertex "replaces" v2 in its former role
    vtkIdType iNewTri[3];
    iNewTri[0] = iV1; iNewTri[1] = iNewID; iNewTri[2] = iApex;
    m_pMesh->ReplaceCell(iTriID,3,iNewTri);
    //insert a new triangle
    iNewTri[0] = iNewID; iNewTri[1] = iV2; iNewTri[2] = iApex;
    vtkIdType iNewTriangleID = m_pMesh->InsertNextCell(VTK_TRIANGLE,3,iNewTri);
    m_pMesh->AddReferenceToCell(iNewID, iNewTriangleID); 
    m_pMesh->AddReferenceToCell(iV2, iNewTriangleID); 
    m_pMesh->AddReferenceToCell(iApex, iNewTriangleID);

    return iNewTriangleID;
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VtkPolyDataTools.cpp"                                        */
/*============================================================================*/
