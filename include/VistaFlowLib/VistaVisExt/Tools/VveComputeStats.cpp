/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveComputeStats.h"

#include "VveHistogram.h"
#include "Vve2DHistogram.h"

#include "../Data/VveDiscreteDataTyped.h"
#include "../Data/VveTimeHistogram.h"
#include "../Data/VveTimeSeries.h"
#include "../Data/VveStatsSet.h"
#include "../Tools/VveTimeMapper.h"

#include <vtkPointSet.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>

#include <limits>

using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveComputeStats::VveComputeStats() 
	:	m_pData(NULL) 
{
}

VveComputeStats::~VveComputeStats()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :					                                              */
/*                                                                            */
/*============================================================================*/
void VveComputeStats::SetReferenceDataSet(VveDiscreteDataTyped<vtkDataSet> *pData)
{
	m_pData = pData;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :					                                              */
/*                                                                            */
/*============================================================================*/
void VveComputeStats::ComputeStats(const std::string& strFieldName,
									VveStatsSet *pStats,
									int iMinLevelIdx/*=-1*/,
									int iMaxLevelIdx/*=-1*/)
{
	if(m_pData == NULL)
	{
		vstr::errp() << "[VveComputeStats::ComputeStats] "
			 << "No reference data set!" << endl;
		return;
	}
	//get the individual stats
	VveTimeSeries *pMinSeries = pStats->GetMinSeries();
	VveTimeSeries *pMaxSeries = pStats->GetMaxSeries();
	VveTimeSeries *pAvgSeries = pStats->GetAvgSeries();
	VveTimeHistogram *pHistogram = pStats->GetHistogram();

	const int T=m_pData->GetNumberOfLevels();
	int iStartTime = (iMinLevelIdx == -1 ? 0 : iMinLevelIdx);
	int iEndTime = (iMaxLevelIdx == -1 ? T-1 : iMaxLevelIdx);
	VveTimeMapper *pTM = m_pData->GetTimeMapper();

	//create time series for each field and per avg, min, max and the histogram
	vtkDataSet *pData = m_pData->GetTypedLevelDataByLevelIndex(iStartTime)->GetData();
	if(pData == NULL)
	{
		vstr::errp() << "[VveComputeStats::ComputeStats] "
			 << "No data available for li=" << iStartTime << endl;
		return;
	}
	vtkPointData *pPD = pData->GetPointData();
	vtkDataArray *pArray = pPD->GetArray(strFieldName.c_str());
	if(pArray == NULL)
	{
		vstr::errp() << "[VveComputeStats::ComputeStats] "
			 << "Data array " << strFieldName << "> not available" << endl;
		return;
	}
	if(pHistogram != NULL)
	{
		pHistogram->GetHistogram()->CleanHistogram();
	}
	double dTuple[9];
	//for all time steps
	for(int t=iStartTime; t<=iEndTime; ++t)
	{
		pData = m_pData->GetTypedLevelDataByLevelIndex(t)->GetData();
		const int iNumPts = (pData == NULL ? 0 : pData->GetNumberOfPoints());
		//init series entry for that level idx
		if(iNumPts == 0)
		{
			(*pMinSeries)[t] = 0;
			(*pMaxSeries)[t] = 0;
			(*pAvgSeries)[t] = 0;
			continue;
		}
		else
		{
			(*pMinSeries)[t] =  std::numeric_limits<float>::max();
			(*pMaxSeries)[t] = -std::numeric_limits<float>::max();
			(*pAvgSeries)[t] =  0;
		}

		float *pHistogramCacheX, *pHistogramCacheY;
		if(pHistogram != NULL)
		{
			pHistogramCacheX = new float[iNumPts];
			pHistogramCacheY = new float[iNumPts];
		}

		//get the respective field
		pPD = pData->GetPointData();
		pArray = pPD->GetArray(strFieldName.c_str());
		//we should have checked in the first place!
		assert(pArray != NULL);
		//for all pts -> update time series value
		for(int p=0; p<iNumPts; ++p)
		{
			pArray->GetTuple(p, dTuple);
			(*pMinSeries)[t]  = std::min<float>(dTuple[0], (*pMinSeries)[t]);
			(*pAvgSeries)[t]  = (*pAvgSeries)[t] + dTuple[0];
			(*pMaxSeries)[t]  = std::max<float>(dTuple[0], (*pMaxSeries)[t]);
			if(pHistogram == NULL)
				continue;
			//remember points for histogram
			pHistogramCacheX[p] = t;
			pHistogramCacheY[p] = dTuple[0];
		}
		//update average
		if(iNumPts > 0)
			(*pAvgSeries)[t] = (*pAvgSeries)[t]/(float)iNumPts;
		else
			(*pAvgSeries)[t] = 0;
		//make sure we have valid entries here!
		assert((*pMinSeries)[t] == (*pMinSeries)[t]);
		assert((*pAvgSeries)[t] == (*pAvgSeries)[t]);
		assert((*pMaxSeries)[t] == (*pMaxSeries)[t]);
		assert(	(*pMinSeries)[t] <= (*pAvgSeries)[t] &&
				(*pAvgSeries)[t] <= (*pMaxSeries)[t]);
		//append data to histogram -> fill entire time level at once
		if(pHistogram != NULL)
		{
			pHistogram->GetHistogram()->AppendData(
				iNumPts, pHistogramCacheX, pHistogramCacheY);
			//remember to free up memory again...
			delete[] pHistogramCacheX;
			delete[] pHistogramCacheY;
		}
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :					                                              */
/*                                                                            */
/*============================================================================*/
void VveComputeStats::ComputeStats(const std::vector< std::set<int> > &vecPointIdsPerLevel, 
									const std::string& strFieldName,
									VveStatsSet *pStats,
									int iMinLevelIdx/*=-1*/,
									int iMaxLevelIdx/*=-1*/)
{
	if(m_pData == NULL)
	{
		vstr::errp() << "[VveComputeStats::ComputeStats] "
			 << "No reference data set!" << endl;
		return;
	}

	VveTimeSeries *pMinSeries = pStats->GetMinSeries();
	VveTimeSeries *pMaxSeries = pStats->GetMaxSeries();
	VveTimeSeries *pAvgSeries = pStats->GetAvgSeries();
	VveTimeHistogram *pHistogram = pStats->GetHistogram();

	const int T=m_pData->GetNumberOfLevels();
	int iStartTime = (iMinLevelIdx == -1 ? 0 : iMinLevelIdx);
	int iEndTime = (iMaxLevelIdx == -1 ? T-1 : iMaxLevelIdx);
	VveTimeMapper *pTM = m_pData->GetTimeMapper();

	//create time series for each field and per avg, min, max and the histogram
	vtkDataSet *pData = m_pData->GetTypedLevelDataByLevelIndex(iStartTime)->GetData();
	if(pData == NULL)
	{
		vstr::errp() << "[VveComputeStats::ComputeStats] "
			 << "No data available for li=" << iStartTime << endl;
		return;
	}
	vtkPointData *pPD = pData->GetPointData();
	vtkDataArray *pArray = pPD->GetArray(strFieldName.c_str());
	if(pArray == NULL)
	{
		vstr::errp() << "[VveComputeStats::ComputeStats] "
			 << "Data array " << strFieldName << "> not available" << endl;
		return;
	}

	pStats->SetFieldName(strFieldName);
	
	if(pHistogram != NULL)
	{
		pHistogram->GetHistogram()->CleanHistogram();
	}
		

	double dTuple[9];
	//for all time steps
	for(int t=iStartTime; t<=iEndTime; ++t)
	{
		const size_t nNumPts = vecPointIdsPerLevel[t].size();
		//init series entry for that level idx
		pData = m_pData->GetTypedLevelDataByLevelIndex(t)->GetData();
		if(pData==NULL || nNumPts == 0)
		{
			(*pMinSeries)[t] = 0;
			(*pMaxSeries)[t] = 0;
			(*pAvgSeries)[t] = 0;
			continue;
		}
		else
		{
			(*pMinSeries)[t] =  std::numeric_limits<float>::max();
			(*pMaxSeries)[t] = -std::numeric_limits<float>::max();
			(*pAvgSeries)[t] =  0;
		}

		float *pHistogramCacheX, *pHistogramCacheY;
		if(pHistogram != NULL)
		{
			pHistogramCacheX = new float[nNumPts];
			pHistogramCacheY = new float[nNumPts];
		}

		//get the respective field
		pPD = pData->GetPointData();
		pArray = pPD->GetArray(strFieldName.c_str());
		//we should have checked in the first place!
		assert(pArray != NULL);
		//for all pts -> update time series value
		std::set<int>::const_iterator itPt = vecPointIdsPerLevel[t].begin();
		std::set<int>::const_iterator itEnd = vecPointIdsPerLevel[t].end();
		for(int i=0; itPt != itEnd; ++itPt, ++i)
		{
			pArray->GetTuple(*itPt, dTuple);
			(*pMinSeries)[t]  = std::min<float>(dTuple[0], (*pMinSeries)[t]);
			(*pAvgSeries)[t]  = (*pAvgSeries)[t] + dTuple[0];
			(*pMaxSeries)[t]  = std::max<float>(dTuple[0], (*pMaxSeries)[t]);
			if(pHistogram == NULL)
				continue;
			//remember points for histogram
			pHistogramCacheX[i] = t;
			pHistogramCacheY[i] = dTuple[0];
		}
		//update average
		if(nNumPts > 0)
			(*pAvgSeries)[t] = (*pAvgSeries)[t] / static_cast<float>(nNumPts);
		else
			(*pAvgSeries)[t] = 0;
		//make sure we have valid entries here!
		assert((*pMinSeries)[t] == (*pMinSeries)[t]);
		assert((*pAvgSeries)[t] == (*pAvgSeries)[t]);
		assert((*pMaxSeries)[t] == (*pMaxSeries)[t]);
		assert(	(*pMinSeries)[t] <= (*pAvgSeries)[t] &&
				(*pAvgSeries)[t] <= (*pMaxSeries)[t]);
		//append data to histogram -> fill entire time level at once
		if(pHistogram != NULL)
		{
			pHistogram->GetHistogram()->AppendData(
				nNumPts, pHistogramCacheX, pHistogramCacheY);
			//remember to free up memory again...
			delete[] pHistogramCacheX;
			delete[] pHistogramCacheY;
		}
	}
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveComputeStats.cpp"                                        */
/*============================================================================*/

