/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VveUtils.h"
#include <VistaAspects/VistaAspectsUtils.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  IMPLEMENTATION FOR VflConversionStuff                                    */
/*============================================================================*/
VistaVector3D VveConversionStuff::ConvertToVistaVector3D(const std::string &sValue)
{
	list<float> liTemp;
	int iSize = VistaAspectsConversionStuff::ConvertToList(sValue, liTemp);
	if (iSize > 4)
		iSize = 4;	// allow setting of the homogeneous coordinate

	VistaVector3D v3Temp;
	list<float>::iterator it=liTemp.begin();
	for (int i=0; i<iSize; ++i, ++it)
		v3Temp[i] = *it;

	return v3Temp;
}

VistaVector3D VveConversionStuff::ConvertToVistaVector3D(const std::string &sValue,
														   char cSep)
{
	list<float> liTemp;
	int iSize = VistaAspectsConversionStuff::ConvertToList(sValue, liTemp, cSep);
	if (iSize > 4)
		iSize = 4;	// allow setting of the homogeneous coordinate

	VistaVector3D v3Temp;
	list<float>::iterator it=liTemp.begin();
	for (int i=0; i<iSize; ++i, ++it)
		v3Temp[i] = *it;

	return v3Temp;
}

VistaQuaternion VveConversionStuff::ConvertToVistaQuaternion(const std::string &sValue)
{
	list<float> liTemp;
	int iSize = VistaAspectsConversionStuff::ConvertToList(sValue, liTemp);
	if (iSize > 4)
		iSize = 4;

	VistaQuaternion qTemp;
	list<float>::iterator it=liTemp.begin();
	for (int i=0; i<iSize; ++i, ++it)
		qTemp[i] = *it;

	return qTemp;
}

VistaQuaternion VveConversionStuff::ConvertToVistaQuaternion(const std::string &sValue,
															   char cSep)
{
	list<float> liTemp;
	int iSize = VistaAspectsConversionStuff::ConvertToList(sValue, liTemp, cSep);
	if (iSize > 4)
		iSize = 4;	

	VistaQuaternion qTemp;
	list<float>::iterator it=liTemp.begin();
	for (int i=0; i<iSize; ++i, ++it)
		qTemp[i] = *it;

	return qTemp;
}

std::string VveConversionStuff::ConvertToString(const VistaVector3D &v3Value)
{
	list<float> liTemp;
	for (int i=0; i<4; ++i)
		liTemp.push_back(v3Value[i]);

	return VistaAspectsConversionStuff::ConvertToString(liTemp);
}

std::string VveConversionStuff::ConvertToString(const VistaQuaternion &qValue)
{
	list<float> liTemp;
	for (int i=0; i<4; ++i)
		liTemp.push_back(qValue[i]);

	return VistaAspectsConversionStuff::ConvertToString(liTemp);
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VflUtils.cpp"                                                */
/*============================================================================*/
