/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveHistogram.h"
#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaBase/VistaStreamUtils.h>

#include <iostream>
#include <limits>
#include <cmath>
#include <algorithm>

using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveHistogram::VveHistogram(double fMin/*=0*/, double fMax/*=1*/, int iBins/*=10*/)
	:	m_fMin(fMin), m_fMax(fMax), 
		m_iMaxBinCount(0),
		m_iNumTotalPoints(0), 
		m_iNumValidPoints(0),
		m_bCompressed(false)
{
	SetNumBins(iBins);
					   //m_vecBins[1] contains the number of elements falling the 1st bin.
                      //m_vecBins[0] and m_vecBins[m_iBins+1] contain the number of the unregular ones. 
}
VveHistogram::~VveHistogram(){}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
VveHistogram& VveHistogram::operator=(const VveHistogram& other)
{
	m_vecBins = other.m_vecBins;
	m_fMin = other.m_fMin;
	m_fMax = other.m_fMax;
	m_bCompressed = other.m_bCompressed;

	m_iBins = other.m_iBins;
	m_iMaxBinCount = other.m_iMaxBinCount;
	m_iNumTotalPoints = other.m_iNumTotalPoints;
	m_iNumValidPoints = other.m_iNumValidPoints;

	return (*this);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBinForValue				                              */
/*                                                                            */
/*============================================================================*/
int VveHistogram::GetBinForValue (double dValue) const
{
	double fInterval = (m_fMax-m_fMin)/(m_iBins-2);
	int iBin = -1;
	if(dValue < m_fMin)			// smaller than fMin in bins number 0
	{
		iBin = 0;
	}
	else if(dValue > m_fMax)
	{
		iBin = m_iBins-1;
	}
	else
	{
		iBin = (int) ((dValue-m_fMin)/fInterval)+1;
	}

	return iBin;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNumBins				                              */
/*                                                                            */
/*============================================================================*/

void VveHistogram::SetNumBins(int iBins)
{
	//we need at least one bin for outliers on either side of [min,max]!
	m_iBins = std::max(iBins,2);
	m_vecBins.resize(m_iBins);
}

int VveHistogram::GetNumBins() const
{
	return m_iBins;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetBinCount				                              */
/*                                                                            */
/*============================================================================*/

void VveHistogram::SetBinCount(int iBin, int iBinCount)
{
	assert(iBinCount>=0);

	m_vecBins[iBin] = iBinCount;
	m_iMaxBinCount = std::max(iBinCount, m_iMaxBinCount);
}

int VveHistogram::GetBinCount(int iBin) const
{
	assert(m_vecBins[iBin] >= 0);
	return m_vecBins[iBin];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNumValidPoints			                              */
/*                                                                            */
/*============================================================================*/

void VveHistogram::SetNumValidPoints(int iNumValidPoints)
{
	m_iNumValidPoints =  iNumValidPoints;
}

int VveHistogram::GetNumValidPoints() const
{
	return m_iNumValidPoints;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNumTotalPoints			                              */
/*                                                                            */
/*============================================================================*/

void VveHistogram::SetNumTotalPoints(int iNumTotalPoints)
{
	m_iNumTotalPoints = iNumTotalPoints;
}

int VveHistogram::GetNumTotalPoints() const
{
	return m_iNumTotalPoints;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetRange					                              */
/*                                                                            */
/*============================================================================*/

void VveHistogram::SetRange(double fMin, double fMax)
{
	m_fMin = fMin;
	m_fMax = fMax;
}

void VveHistogram::GetRange(double &fMin, double &fMax) const
{
	fMin = m_fMin;
	fMax = m_fMax;
}

void VveHistogram::SetRange(double fRange[2])
{
	if(fRange[0] > fRange[1])
	{
		vstr::errp() << "[VveHistogram::SetRange] "
			 << "Invalid range [" << fRange[0] << ","
			 << fRange[1] << "]" << endl;
		return;
	}
	m_fMin = fRange[0];
	m_fMax = fRange[1];
}

void VveHistogram::GetRange(double fRange[2]) const
{
	fRange[0] = m_fMin;
	fRange[1] = m_fMax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMin/Max				                              */
/*                                                                            */
/*============================================================================*/

void VveHistogram::SetMin(double fMin)
{
	if(fMin > m_fMax)
	{
		vstr::errp() << "[VveHistogram::SetMin] " 
			 << "Invalid range [" << fMin << ","
			 << m_fMax << "]" << endl;
		return;
	}
	m_fMin = fMin;
}

double VveHistogram::GetMin() const
{
	return m_fMin;
}

void VveHistogram::SetMax(double fMax)
{
	if(fMax < m_fMin)
	{
		vstr::errp() << "[VveHistogram::SetMax] "
			 << "Invalid range [" << m_fMin << ","
			 << fMax << "]" << endl;
		return;
	}
	m_fMax = fMax;
}

double VveHistogram::GetMax() const
{
	return m_fMax;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMaxBinCount				                          */
/*                                                                            */
/*============================================================================*/

int VveHistogram::GetMaxBinCount() const
{
	return m_iMaxBinCount;
}

void VveHistogram::SetMaxBinCount(int i) 
{
	m_iMaxBinCount = i;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetBinMin/Max						                          */
/*                                                                            */
/*============================================================================*/
double VveHistogram::GetLowerBinLimit(int iBin) const
{
	double fInterval = (m_fMax-m_fMin)/(m_iBins-2);
	return (m_fMin + fInterval*(iBin-1));
}
double VveHistogram::GetUpperBinLimit(int iBin) const
{
	double fInterval = (m_fMax-m_fMin)/(m_iBins-2);
	return (m_fMin + fInterval*iBin);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeProbabilityDist			                          */
/*                                                                            */
/*============================================================================*/
void VveHistogram::ComputeDistribution(std::vector<double> &vecProb)
{
	vecProb.clear();
	vecProb.resize(m_iBins, 0);

	int iControlCounts = 0;
	double dControlDist = 0.0;

	for(int i=0; i<m_iBins; ++i)
	{
		vecProb[i] = double(m_vecBins[i]) / double(m_iNumValidPoints);

		iControlCounts += m_vecBins[i];
		dControlDist += vecProb[i];
	}

#ifdef DEBUG
	if(iControlCounts != m_iNumValidPoints)
		vstr::warnp() << "[CVVeHistogram] invalid num of valid points" << std::endl;
	if(fabs(dControlDist - 1.0) > std::numeric_limits<double>::epsilon())
		vstr::warnp() << "[CVVeHistogram] invalid num of valid points" << std::endl;
#endif //DEBUG

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeEntropy        			                          */
/*                                                                            */
/*============================================================================*/
double VveHistogram::ComputeEntropy()
{
	// get the distribution
	std::vector<double> vecProb;
	ComputeDistribution(vecProb);

	double dEntropy = 0.0;
	double dLog = 0.0;
	size_t nNum = vecProb.size();
	for(size_t i=0; i<nNum; ++i)
	{
		// compute in "bit"-fashion, i.e. use log2(vecProb[i])
		if(fabs(vecProb[i]) < std::numeric_limits<double>::epsilon())
			dLog = 0.0;
		else
			dLog = log(vecProb[i]) / log(2.0);   

		dEntropy -= vecProb[i] * dLog; 
	}

	return dEntropy;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CleanHistogram					                          */
/*                                                                            */
/*============================================================================*/

void VveHistogram::CleanHistogram ()
{
	m_iMaxBinCount = 0;
	m_iNumValidPoints = 0;
	m_iNumTotalPoints = 0;
	
	m_vecBins.clear();
	m_vecBins.resize(m_iBins, 0);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetCompress						                          */
/*                                                                            */
/*============================================================================*/
void VveHistogram::SetCompress(bool bCompress)
{
	if (m_bCompressed == bCompress)
		return;

	if (bCompress)
		Compress();
	else
		Decompress();
}

void VveHistogram::Compress ()
{	
	if(m_bCompressed)
		return;

	std::vector<int> vecCompressedVector;
	int iNumZeros = 0;
	for(unsigned int j = 0; j < m_vecBins.size(); ++j)
	{
		if (m_vecBins[j]==0)
		{
			iNumZeros++;
			// write first zero of a row
			if (iNumZeros==1)
				vecCompressedVector.push_back(0);
		}
		else
		{
			if (iNumZeros>0)
			{
				// write number of zeros in a row
				vecCompressedVector.push_back(iNumZeros);

				// reset counter
				iNumZeros = 0;
			}
			// write regular value
			vecCompressedVector.push_back(m_vecBins[j]);
		}
	}
	// if there are only zeros at the end, write them encoded, too
	if (iNumZeros>0)
	{
		// write number of zeros in a row
		vecCompressedVector.push_back(iNumZeros);

		// reset counter
		iNumZeros = 0;
	}

	// replace vector
	m_vecBins = vecCompressedVector;

	m_bCompressed = true;
}

void VveHistogram::Decompress ()
{
	if(!m_bCompressed)
		return;

	std::vector<int> vecUncompressedVector;
	bool bZeroMode = false;
	for(unsigned int j = 0; j < m_vecBins.size(); ++j)
	{
		if (m_vecBins[j]==0)
		{
			bZeroMode = true;
			vecUncompressedVector.push_back(0);
		}
		else
		{
			if (bZeroMode)
			{
				// write m_vecBins[j]-1 zeros into buffer,
				// as first zero was already written
				for (int k=0; k < m_vecBins[j]-1; ++k)
					vecUncompressedVector.push_back(0);
				bZeroMode = false;
			}
			else
			{
				vecUncompressedVector.push_back(m_vecBins[j]);
			}
		}
	}

	// replace vector
	m_vecBins = vecUncompressedVector;

	m_bCompressed = false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIsCompressed					                          */
/*                                                                            */
/*============================================================================*/
bool VveHistogram::GetIsCompressed() const
{
	return m_bCompressed;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Serialize							                          */
/*                                                                            */
/*============================================================================*/
int VveHistogram::Serialize(IVistaSerializer &Serializer) const
{
#ifdef DEBUG
	//vstr::debugi() << "\t\tSerialize histogram...\n";
#endif
	int iNumBytes = 0;

	int iRet = Serializer.WriteDelimitedString(this->GetSignature());
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = Serializer.WriteInt32(m_iBins);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = Serializer.WriteInt32((unsigned int) m_vecBins.size());
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = Serializer.WriteDouble(m_fMin);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	iRet = Serializer.WriteDouble(m_fMax);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	iRet = Serializer.WriteBool(m_bCompressed);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	//int iMaxBinCount = 0;
	//int iNumTotalPoints = 0;
	//int iNumValidPoints = 0;
	//for(unsigned int i = 0; i < m_vecBins.size(); ++i)
	//{
	//	iRet = Serializer.WriteInt32(m_vecBins[i]);
	//	if(iRet != -1)
	//		iNumBytes += iRet;
	//	else 
	//		return -1;

	//	//iMaxBinCount = std::max(iMaxBinCount, m_vecBins[i]);
	//	//iNumTotalPoints += m_vecBins[i];
	//	//if(i != 0 && i != (m_vecBins.size()-1))
	//	//	iNumValidPoints += m_vecBins[i];
	//}
	// @todo: Get rid of this cast.
	iRet = Serializer.WriteRawBuffer(&m_vecBins[0],
		static_cast<int>(sizeof(int)*m_vecBins.size()));
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = Serializer.WriteInt32(m_iMaxBinCount);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;
	
	iRet = Serializer.WriteInt32(m_iNumTotalPoints);
	if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	iRet = Serializer.WriteInt32(m_iNumValidPoints);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;
		
#ifdef DEBUG
	//vstr::debugi() << "\t\t... histogram finished ("<<iNumBytes<<" bytes)\n";
#endif

	return iNumBytes;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeSerialize						                          */
/*                                                                            */
/*============================================================================*/
int VveHistogram::DeSerialize(IVistaDeSerializer &DeSerializer)
{
	int iNumBytes = 0;

#ifdef DEBUG
	//vstr::debugi() << "\t\tDeserialize histogram...\n";
#endif

	string str("");
	int iRet = DeSerializer.ReadDelimitedString(str);
	if(iRet == -1 || str != this->GetSignature())
		return -1;

	iNumBytes += iRet+1;


	iRet = DeSerializer.ReadInt32(m_iBins);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	int iBinSize = 0;
	iRet = DeSerializer.ReadInt32(iBinSize);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = DeSerializer.ReadDouble(m_fMin);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;
	
	iRet = DeSerializer.ReadDouble(m_fMax);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = DeSerializer.ReadBool(m_bCompressed);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	m_vecBins.resize(iBinSize);
	//for(unsigned int i = 0; i < m_vecBins.size(); ++i)
	//{
	//	iRet = DeSerializer.ReadInt32(m_vecBins[i]);
	//	if(iRet != -1)
	//		iNumBytes += iRet;
	//	else 
	//		return -1;
	//}

	// @todo: Get rid of this cast.
	iRet = DeSerializer.ReadRawBuffer(&m_vecBins[0],
		static_cast<int>(sizeof(int)*m_vecBins.size()));
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = DeSerializer.ReadInt32(m_iMaxBinCount);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = DeSerializer.ReadInt32(m_iNumTotalPoints);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = DeSerializer.ReadInt32(m_iNumValidPoints);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

#ifdef DEBUG
	//vstr::debugi() << "\t\t... histogram finished ("<<iNumBytes<<" bytes)\n";
#endif

	return iNumBytes;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSignature						                          */
/*                                                                            */
/*============================================================================*/

string VveHistogram::GetSignature() const
{
	return "__VveHistogram__";
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveHistogram.cpp"                                            */
/*============================================================================*/



