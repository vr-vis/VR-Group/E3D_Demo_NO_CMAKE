/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEHISTOGRAM_H
#define _VVEHISTOGRAM_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaSerializable.h>
#include <vector>
#include <string>
#include <cassert>
#include <limits>
#include <cmath>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVistaSerializer;
class IVistaDeSerializer;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Histogram describes the distribution of the data. 
* This class collects information of histogram for one data field (e.g. scalar field).
*/
class VISTAVISEXTAPI VveHistogram : public IVistaSerializable
{
public:
	VveHistogram(double fMin=0, double fMax=1, int iBins=10);
	           
	virtual ~VveHistogram();

	VveHistogram& operator=(const VveHistogram& other);

	// set data (for first fill of histogram
	template<class T> bool	SetData(int iSize, T fData[]);//template function

	// append data to histogram
	template<class T> bool	AppendData(int iSize, T fData[]);//template function

	/*
	* Bins are equally distributed within [min,max] range.
	* The first and last bin (bin 0 and bin n-1 for n bins) are used
	* for outliers outside [min, max] (bin 0 for < min and bin n-1 for > max).
	*/
	int GetBinForValue (double dValue) const;

	void	SetNumBins(int iBins);
	int		GetNumBins() const;
	
	void	SetBinCount(int iBin, int iBinCount);
	int		GetBinCount(int iBin) const;

	void	SetNumValidPoints(int iNumValidPoints);
	int		GetNumValidPoints() const;

	//returns number of ALL points in the histogramm (including outliers)
	void	SetNumTotalPoints(int iNumTotalPoints);
	int		GetNumTotalPoints() const;

	/*
	* Set/Get min/max values, which are the range [min,max].
	*
	*/
	void	SetRange(double fMin, double fMax);
	void	GetRange(double &fMin, double &fMax) const;

	void	SetRange(double fRange[2]);
	void	GetRange(double fRange[2]) const;

	double	GetMin() const;
	void	SetMin(double);
	
	double	GetMax() const;
	void	SetMax(double);

	int		GetMaxBinCount() const;
	void	SetMaxBinCount(int i);
	
	// get min/max value within bin
	double GetLowerBinLimit(int) const;
	double GetUpperBinLimit(int) const;

	// clean up information, remove bins
	void CleanHistogram ();

	// compute the probability distribution, 
	// i.e. the "normalized" bin counts [0,1]
	// for each bin counts n and for total num of valid points m: n/m
	// only valid points will be taken into account
	void ComputeDistribution(std::vector<double> &vecProb); 

	// compute entropy of the underlying data
	double ComputeEntropy();

	// Compress histogram using RLE for zeros
	// you cannot access bins using GetBinCount if the histogram is compressed, yet
	void	SetCompress(bool bCompress);
	bool	GetIsCompressed() const;

	 /**
     * Think of this as "SAVE"
     */
    virtual int Serialize(IVistaSerializer &) const;
    /**
     * Think of this as "LOAD"
     */
    virtual int DeSerialize(IVistaDeSerializer &);

	virtual std::string GetSignature() const;

private:
	void Compress ();
	void Decompress ();

	std::vector<int> m_vecBins;
	double m_fMin;
	double m_fMax;
	bool m_bCompressed;

	int m_iBins;//number of bins
	int m_iMaxBinCount;
	int m_iNumTotalPoints;//number of ALL points in the histogramm (including outliers)
	int m_iNumValidPoints;//number of points whose values are within the given [min, max] interval.	                      //no need to store fData[]
};
/*============================================================================*/
/* TEMPLATE IMPLEMENTATION                                                    */
/*============================================================================*/
template<class T>
bool VveHistogram::AppendData(int iSize, T f[])
{
	int i;
	double fInterval = (m_fMax-m_fMin)/(m_iBins-2);
	m_iNumTotalPoints += iSize;
	// build histomap
	for(i = 0; i < iSize; ++i)
	{
		if(f[i] < m_fMin)			// smaller than fMin in bins number 0
		{
			++(m_vecBins[0]);
			assert(m_vecBins[0] > 0);
		}
		else if(f[i] > m_fMax)		// larger than fMax in bins m_iBins-1
		{
			++(m_vecBins[m_iBins-1]);
			assert(m_vecBins[m_iBins-1] > 0);
		}
		else
		{
			++m_iNumValidPoints;
			// to catch the case where the item is exactly on the upper border
			if(fabs(f[i]-m_fMax) < std::numeric_limits<double>::epsilon())
				++ m_vecBins[m_iBins-2];
			else
				++(m_vecBins[(int)((f[i]-m_fMin)/fInterval)+1]); // regular in bins 1..m_iBins-2
			assert(m_vecBins[(int)((f[i]-m_fMin)/fInterval)+1] > 0);
		}
	}

	// Set max Bins	
	for(i = 0; i < m_iBins; i++)
		m_iMaxBinCount = std::max<int>(m_iMaxBinCount,m_vecBins[i]);

	return true;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "VveHistogram.h"                                               */
/*============================================================================*/
#endif //_VVEHISTOGRAM_H

