/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "VveTimeIntervalMapping.h"
#include "VveTimeMapper.h"

#include <algorithm>
#include <limits>
#include <cmath>


/*============================================================================*/
/* IMPLEMENTATION															  */
/*============================================================================*/
VveTimeIntervalMapping::VveTimeIntervalMapping()
:	m_pTimeMapper( new VveTimeMapper )
,	m_nLevelIndexForEmptyTimeIntervals( -1 )
,	m_bUseExplicitGlobalVisTimeDomain( false )
,	m_dGlobalVisTimeStart( 0.0 )
,	m_dGlobalVisTimeEnd( 1.0 )
{ }

VveTimeIntervalMapping::VveTimeIntervalMapping( const VveTimeIntervalMapping& other )
:	m_pTimeMapper( new VveTimeMapper )
{
	*this = other;
}

VveTimeIntervalMapping::~VveTimeIntervalMapping()
{
	delete m_pTimeMapper;
}


VveTimeIntervalMapping& VveTimeIntervalMapping::operator=( const VveTimeIntervalMapping& other )
{
	m_vecExplicitTimeIntervals = other.m_vecExplicitTimeIntervals;
	m_nLevelIndexForEmptyTimeIntervals = other.m_nLevelIndexForEmptyTimeIntervals;
	m_bUseExplicitGlobalVisTimeDomain = other.m_bUseExplicitGlobalVisTimeDomain;
	m_dGlobalVisTimeStart = other.m_dGlobalVisTimeStart;
	m_dGlobalVisTimeEnd = other.m_dGlobalVisTimeEnd;

	// As the other mapping can be assumed to be valid, simply rebuilding our own
	// TimeMapper without further checks suffices.
	UpdateTimeMapper();
	return *this;
}


bool VveTimeIntervalMapping::SetExplicitGlobalVisTimeDomain( const double dStart, const double dEnd )
{
	if( dStart >= dEnd )
		return false;

	m_dGlobalVisTimeStart = dStart;
	m_dGlobalVisTimeEnd = dEnd;

	if( m_bUseExplicitGlobalVisTimeDomain && m_vecExplicitTimeIntervals.size() != 0 )
	{
		ClampExistingIntervals();
		return UpdateTimeMapper();
	}
	
	return true;
}

bool VveTimeIntervalMapping::SetExplicitGlobalVisTimeDomain( const double dDomain[2] )
{
	return SetExplicitGlobalVisTimeDomain( dDomain[0], dDomain[1] );
}


void VveTimeIntervalMapping::GetExplicitGlobalVisTimeDomain( double& dStart, double& dEnd ) const
{
	dStart = m_dGlobalVisTimeStart;
	dEnd = m_dGlobalVisTimeEnd;
}

void VveTimeIntervalMapping::GetExplicitGlobalVisTimeDomain( double dDomain[2] )
{
	GetExplicitGlobalVisTimeDomain( dDomain[0], dDomain[1] );
}


void VveTimeIntervalMapping::SetUseExplicitGlobalVisTimeDomain( bool bUseExplicitGlobalVisTimeDomain )
{
	m_bUseExplicitGlobalVisTimeDomain = bUseExplicitGlobalVisTimeDomain;
	if( m_bUseExplicitGlobalVisTimeDomain && m_vecExplicitTimeIntervals.size() != 0 )
	{
		ClampExistingIntervals();
		UpdateTimeMapper();
	}
}

bool VveTimeIntervalMapping::GetUseExplicitGlobalVisTimeDomain() const
{
	return m_bUseExplicitGlobalVisTimeDomain;
}


void VveTimeIntervalMapping::SetTimeLevelForEmptyTimeIntervals( int nLevelIndex )
{
	m_nLevelIndexForEmptyTimeIntervals = nLevelIndex;
}

int VveTimeIntervalMapping::GetTimeLevelForEmptyTimeIntervals() const
{
	return m_nLevelIndexForEmptyTimeIntervals;
}


bool VveTimeIntervalMapping::BuildFromExplicitIntervalInfo( const std::vector<IntervalInfo>& vecExplicitIntervalInfo )
{
	if( !ValidateIntervals( vecExplicitIntervalInfo ) )
		return false;

	m_vecExplicitTimeIntervals = vecExplicitIntervalInfo;
	SortIntervals();
	return UpdateTimeMapper();
}


void VveTimeIntervalMapping::GetCopyOfExplicitIntervalInfo( std::vector<IntervalInfo>& vecTarget ) const
{
	vecTarget = m_vecExplicitTimeIntervals;
}


size_t VveTimeIntervalMapping::GetNumberOfTimeIntervals() const
{
	return m_vecExplicitTimeIntervals.size();
}


VveTimeMapper* VveTimeIntervalMapping::GetTimeMapper() const
{
	return m_pTimeMapper;
}


void VveTimeIntervalMapping::ClampExistingIntervals()
{
	const size_t nNumberIntervals = m_vecExplicitTimeIntervals.size();

	// The new vector will hold all the remaining IntervalInfo objects.
	std::vector<IntervalInfo> vecClampedIntervals( nNumberIntervals );

	// Clamping.
	for( size_t i=0; i<nNumberIntervals; ++i )
	{
		if( !ViolatesExplicitGlobalVisTimeDomain( m_vecExplicitTimeIntervals[i] ) )
			vecClampedIntervals.push_back( m_vecExplicitTimeIntervals[i] );
	}

	// Update structures.
	m_vecExplicitTimeIntervals = vecClampedIntervals;
	UpdateTimeMapper();
}


bool VveTimeIntervalMapping::ValidateIntervals( const std::vector<IntervalInfo>& vecExplicitIntervalInfo )
{
	const size_t nNumIntervals = vecExplicitIntervalInfo.size();
	for( size_t i=0; i<nNumIntervals; ++i )
	{
		for( size_t j=i+1; j<nNumIntervals; ++j )
		{
			if( DoTimeIntervalsOverlap( vecExplicitIntervalInfo[i], vecExplicitIntervalInfo[j] ) )
			{
				return false;
			}			
		}

		if( m_bUseExplicitGlobalVisTimeDomain && ViolatesExplicitGlobalVisTimeDomain( vecExplicitIntervalInfo[i] ) )		
		{
			return false;
		}
	}
	return true;
}


bool IsSmaller( const VveTimeIntervalMapping::IntervalInfo& isThisSmaller, const VveTimeIntervalMapping::IntervalInfo& thanThat )
{
	return ( isThisSmaller.m_dEndVisTime <= thanThat.m_dStartVisTime );
}

void VveTimeIntervalMapping::SortIntervals()
{
	std::sort( m_vecExplicitTimeIntervals.begin(), m_vecExplicitTimeIntervals.end(), IsSmaller );	
}

bool VveTimeIntervalMapping::UpdateTimeMapper()
{
	const size_t nNumberOfIntervals = m_vecExplicitTimeIntervals.size();

	std::vector<VveTimeMapper::IndexInfo> vecIndexInfo;
	vecIndexInfo.reserve( 2 * nNumberOfIntervals );

	if( nNumberOfIntervals > 0 )
	{
		// We'll simply set the start and end vis time to 
		if( !m_bUseExplicitGlobalVisTimeDomain )
		{
			m_dGlobalVisTimeStart = m_vecExplicitTimeIntervals[0].m_dStartVisTime;
			m_dGlobalVisTimeEnd = m_vecExplicitTimeIntervals[nNumberOfIntervals-1].m_dEndVisTime;
		}

		for( size_t i=0; i<nNumberOfIntervals; ++i )
		{
			const double dCurHalfLength = 0.5 * ( m_vecExplicitTimeIntervals[i].m_dEndVisTime - m_vecExplicitTimeIntervals[i].m_dStartVisTime );
						
			// Previous.
			double dPrevHalfDist = 0.0;
			double dPrevHalfLength = 0.0;

			if( 0 == i )
			{
 				dPrevHalfDist = 0.5 * ( m_vecExplicitTimeIntervals[i].m_dStartVisTime - m_dGlobalVisTimeStart );
				dPrevHalfLength = dCurHalfLength;
			}
			else
			{
				dPrevHalfDist = 0.5 * ( m_vecExplicitTimeIntervals[i].m_dStartVisTime - m_vecExplicitTimeIntervals[i-1].m_dEndVisTime ); 
				dPrevHalfLength = 0.5 * ( m_vecExplicitTimeIntervals[i-1].m_dEndVisTime - m_vecExplicitTimeIntervals[i-1].m_dStartVisTime );
			}

			double dSelectedPrevHelfLength = 0.0;
			VveTimeMapper::IndexInfo prevIndexInfo;

			const bool bTouchesPrevTimeInterval = std::numeric_limits<double>::epsilon() >= dPrevHalfDist;
			if( bTouchesPrevTimeInterval )
				dSelectedPrevHelfLength = std::min( dCurHalfLength, dPrevHalfLength );
			else
				dSelectedPrevHelfLength = std::min( dCurHalfLength, dPrevHalfDist );

			// In case there is empty region between this and the previous time interval, another index info needs to be
			// inserted for that region as well.
			if( !bTouchesPrevTimeInterval )
			{
				// If necessary, mark the beginning of the global vis time domain.
				if( 0 == i && m_bUseExplicitGlobalVisTimeDomain )
				{
					prevIndexInfo.m_dSimTime = m_dGlobalVisTimeStart;
					prevIndexInfo.m_iLevel = m_nLevelIndexForEmptyTimeIntervals;
					vecIndexInfo.push_back( prevIndexInfo );
				}

				prevIndexInfo.m_dSimTime = m_vecExplicitTimeIntervals[i].m_dStartVisTime - dSelectedPrevHelfLength;
				prevIndexInfo.m_iLevel = m_nLevelIndexForEmptyTimeIntervals;
				vecIndexInfo.push_back( prevIndexInfo );
			}
			// Special case: First time interval "touches" beginning of time global time interval.
			else if( 0 == i /* && bTouchesPrevTimeInterval */ )
			{
				prevIndexInfo.m_dSimTime = m_vecExplicitTimeIntervals[i].m_dStartVisTime;
				prevIndexInfo.m_iLevel = m_vecExplicitTimeIntervals[i].m_nTimeLevel;
				vecIndexInfo.push_back( prevIndexInfo );
			}

			// This index info is always needed to mark the beginning of the time interval.
			prevIndexInfo.m_dSimTime = m_vecExplicitTimeIntervals[i].m_dStartVisTime + dSelectedPrevHelfLength;
			prevIndexInfo.m_iLevel = m_vecExplicitTimeIntervals[i].m_nTimeLevel;
			vecIndexInfo.push_back( prevIndexInfo );
	
			///////////////////////////////////////////////////////////////////////////////////////////////////////////

			// Next.
			double dNextHalfDist = 0.0;
			double dNextHalfLength = 0.0;

			if( nNumberOfIntervals-1 == i )
			{
				dNextHalfDist = 0.5 * ( m_dGlobalVisTimeEnd - m_vecExplicitTimeIntervals[i].m_dEndVisTime );
				dNextHalfLength = dCurHalfLength;
			}
			else
			{
				dNextHalfDist = 0.5 * ( m_vecExplicitTimeIntervals[i+1].m_dStartVisTime - m_vecExplicitTimeIntervals[i].m_dEndVisTime ); 
				dNextHalfLength = 0.5 * ( m_vecExplicitTimeIntervals[i+1].m_dEndVisTime - m_vecExplicitTimeIntervals[i+1].m_dStartVisTime );
			}

			double dSelectedNextHelfLength = 0.0;
			VveTimeMapper::IndexInfo nextIndexInfo;

			const bool bTouchesNextTimeInterval = std::numeric_limits<double>::epsilon() >= dNextHalfDist;
			if( bTouchesNextTimeInterval )
				dSelectedNextHelfLength = std::min( dCurHalfLength, dNextHalfLength );
			else
				dSelectedNextHelfLength = std::min( dCurHalfLength, dNextHalfDist );
			
			// This index info is always needed to mark the ending of the time interval.
			nextIndexInfo.m_dSimTime = m_vecExplicitTimeIntervals[i].m_dEndVisTime - dSelectedNextHelfLength;
			nextIndexInfo.m_iLevel = m_vecExplicitTimeIntervals[i].m_nTimeLevel;
			vecIndexInfo.push_back( nextIndexInfo );

			// In case there is empty region between this and the next time interval, another index info needs to be
			// inserted for that region as well.
			if( !bTouchesNextTimeInterval )
			{
				nextIndexInfo.m_dSimTime = m_vecExplicitTimeIntervals[i].m_dEndVisTime + dSelectedNextHelfLength;
				nextIndexInfo.m_iLevel = m_nLevelIndexForEmptyTimeIntervals;
				vecIndexInfo.push_back( nextIndexInfo );

				// If necessary, mark the ending of the global vis time domain.
				if( nNumberOfIntervals-1 == i && m_bUseExplicitGlobalVisTimeDomain )
				{
					prevIndexInfo.m_dSimTime = m_dGlobalVisTimeEnd;
					prevIndexInfo.m_iLevel = m_nLevelIndexForEmptyTimeIntervals;
					vecIndexInfo.push_back( prevIndexInfo );
				}
			}
			// Special case: First time interval "touches" ending of time global time interval.
			else if( nNumberOfIntervals-1 == i /* && bTouchesNextTimeInterval */ )
			{
				nextIndexInfo.m_dSimTime = m_vecExplicitTimeIntervals[i].m_dEndVisTime;
				nextIndexInfo.m_iLevel = m_vecExplicitTimeIntervals[i].m_nTimeLevel;
				vecIndexInfo.push_back( nextIndexInfo );
			}		
		}
	}

	// Kill double states.
	const size_t nNumberOfIndexInfos = vecIndexInfo.size();
	std::vector<VveTimeMapper::IndexInfo> vecCleanedIndexInfo;
	vecCleanedIndexInfo.reserve( nNumberOfIndexInfos );
	
	for( size_t i=0; i<nNumberOfIndexInfos; ++i )
	{
		if( 0 != i )
		{
			// The others are pushed if they differ from the previous one.
			// The sim time difference is always positive!
			if( vecIndexInfo[i].m_iLevel != vecCleanedIndexInfo.back().m_iLevel
				|| vecIndexInfo[i].m_dSimTime - vecCleanedIndexInfo.back().m_dSimTime > std::numeric_limits<double>::epsilon() )
			{
				vecCleanedIndexInfo.push_back( vecIndexInfo[i] );
			}
		}
		else
		{
			// Push the first any time.
			vecCleanedIndexInfo.push_back( vecIndexInfo[i] );
		}	
	}

	// Create time mapper.
	return m_pTimeMapper->SetByExplicitTimeMapping( vecCleanedIndexInfo );	
}

bool VveTimeIntervalMapping::DoTimeIntervalsOverlap( const IntervalInfo& me, const IntervalInfo& her ) const
{
	return ( me.m_dStartVisTime >= her.m_dStartVisTime && me.m_dStartVisTime < me.m_dEndVisTime )
		|| ( me.m_dEndVisTime > her.m_dStartVisTime && me.m_dEndVisTime <= her.m_dEndVisTime );
}

bool VveTimeIntervalMapping::ViolatesExplicitGlobalVisTimeDomain( const IntervalInfo& me ) const
{
	return ( me.m_dStartVisTime < m_dGlobalVisTimeStart || me.m_dStartVisTime > m_dGlobalVisTimeEnd
		|| me.m_dEndVisTime < m_dGlobalVisTimeStart || me.m_dEndVisTime > m_dGlobalVisTimeEnd );
}


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/


