

set( RelativeDir "./Tools" )
set( RelativeSourceGroup "Source Files\\Tools" )

set( DirFiles
	VtkEventObserver.cpp
	VtkEventObserver.h
	VtkObjectStack.cpp
	VtkObjectStack.h
	VtkPolyDataTools.cpp
	VtkPolyDataTools.h
	Vve2DHistogram.cpp
	Vve2DHistogram.h
	VveComputeStats.cpp
	VveComputeStats.h
	VveDataChunkInfo.cpp
	VveDataChunkInfo.h
	VveDataConvPolyDataToParticleData.cpp
	VveDataConvPolyDataToParticleData.h
	VveDataFieldInfo.cpp
	VveDataFieldInfo.h
	VveDataSetInfo.cpp
	VveDataSetInfo.h
	VveExecutable.cpp
	VveExecutable.h
	VveExtractStats.cpp
	VveExtractStats.h
	VveHistogram.cpp
	VveHistogram.h
	VveInfoCollector.cpp
	VveInfoCollector.h
	VveInfoOutputFile.cpp
	VveInfoOutputFile.h
	VveInfoOutputPrompt.cpp
	VveInfoOutputPrompt.h
	VveInfoReader.cpp
	VveInfoReader.h
	VveTimeIntervalMapping.cpp
	VveTimeIntervalMapping.h
	VveTimeMapper.cpp
	VveTimeMapper.h
	VveUtils.cpp
	VveUtils.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

