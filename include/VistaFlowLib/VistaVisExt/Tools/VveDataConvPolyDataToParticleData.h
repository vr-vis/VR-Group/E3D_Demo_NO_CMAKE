/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEDATACONVPOLYDATATOPARTICLEDATA_H
#define _VVEDATACONVPOLYDATATOPARTICLEDATA_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaVisExt/Data/VveVtkData.h>
#include <VistaAspects/VistaPropertyAwareable.h>
#include <VistaVisExt/VistaVisExtConfig.h>
#include <list>
#include <string>
#include <VistaVisExt/Data/VveParticlePopulation.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkPolyData;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * VveDataConvPolyDataToParticleData converts unsteady vtk poly data to
 * particle data. Depending on whether a time delta (see below) is given, 
 * points or cells are interpreted as particle instants or pathlines.
 * In addition, by providing a time offset, the whole particle population
 * can be moved in time.
 * So, if a non-negative (i.e. >= 0) time delta is given, every cell is 
 * interpreted as a pathline starting at the simulation time of its (the cell's) 
 * time step with particle positions for time 
 * t = simulation_time + index * (delta_time) + offset.
 * Otherwise, all points are interpreted as particle positions at the current
 * simulation time modified by the offset. In the latter case, no pathlines 
 * are created, because every point generates a completely new particle.
 *
 * To determine further parameters of the conversion, this converter analyzes 
 * the meta-data of the source data object. The following keys are recognized:
 *
 * PATHLINE_TIME_DELTA                  -   optional [default: -1]
 * PATHLINE_TIME_OFFSET                 -   optional [default: 0]
 * PARTICLE_VELOCITY [VELOCITY|FIXED]   -   optional [default: VELOCITY]
 * PARTICLE_SCALAR [SCALAR|VELOCITY_MAGNITUDE|FIXED]
 *                                      -   optional [default: SCALAR]
 * PARTICLE_RADIUS [SCALAR|VELOCITY_MAGNITUDE|FIXED]
 *                                      -   optional [default: SCALAR]
 * PARTICLE_FIXED_VELOCITY              -   optional [default: 0,0,0]
 * PARTICLE_FIXED_SCALAR                -   optional [default: 0.0f]
 * PARTICLE_FIXED_RADIUS                -   optional [default: 1.0f]
 *
 * As particle data can contain a velocity, a scalar, and a radius for every
 * particle instant/position, the conversion process has to provide these
 * values as well. By providing appropriate meta-data the mapping from VTK
 * data to particle data can be defined.
 *
 * The key PARTICLE_VELOCITY defines, which value is used as a particle's 
 * velocity. Valid values are VELOCITY (taking VTK's vector data) and
 * FIXED (using the value given with PARTICLE_FIXED_VELOCITY).
 *
 * The keys PARTICLE_SCALAR and PARTICLE_RADIUS define, which values are used
 * for a particle's scalar value and radius, respectively. Valid values are
 * SCALAR (taking VTK's scalar value), VELOCITY_MAGNITUDE (taking the magnitude
 * of VTK's vector data), and FIXED (using the value given with PARTICLE_FIXED_SCALAR
 * and PARTICLE_FIXED_RADIUS, respectively).
 *
 * If required values are not given (e.g. the VTK data doesn't contain
 * a scalar field) the fixed values are used.
 *
 */    
class VISTAVISEXTAPI VveDataConvPolyDataToParticleData
{
public:
	VveDataConvPolyDataToParticleData(VveUnsteadyVtkPolyData *pSource, VveParticleDataCont *pDest);
	virtual ~VveDataConvPolyDataToParticleData();


	virtual void Update();

	// retrieve information
	VveUnsteadyVtkPolyData *GetSource() const;
	VveParticleDataCont *GetDestination() const;

	void SetProperties (const VistaPropertyList & props);

	/**
	 * Set the required information to load an extended particle data.
	 * VveParticleExt is the superclass for all kind of particle extensions, the VistaPropertyList holds the
	 * information about the extention needed in the loading process.
	 *
	 * Current existing extension:
	 * VveParticleExtTensor 
	 *      - std::list<std::string> PARTICLEEXT_FIELDNAMES : eigenvalue 0, eigenvalue 1, eigenvalue 2,
	 *												eigenvector 0, eigenvector 1, eigenvector 2
 	 */
	void SetParticleExtention( const std::list<std::string> &liExtFieldnames);

private:
	VveUnsteadyVtkPolyData	        *m_pSource;
	VveParticleDataCont			    *m_pDest;
		
	VistaPropertyList					    m_oProps;

	// for particle extention
	bool                            m_bParticleExt;
	std::list<std::string>					m_liExtFieldnames;
};

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // _VVEDATACONVPOLYDATATOPARTICLEDATA_H
