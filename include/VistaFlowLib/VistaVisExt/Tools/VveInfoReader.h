/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEINFOREADER_H
#define _VVEINFOREADER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include<string>
#include<iostream>
#include<fstream>

#include "VveDataSetInfo.h"

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveDataChunkInfo;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Read information from file "data.dat" and save it in a VveDataSetInfo object
*/

/*
* This is the reader for "old" ASCII based data set information files.
*
*/
class VISTAVISEXTAPI VveASCIIInfoReader
{
public:
	VveASCIIInfoReader();
	virtual ~VveASCIIInfoReader();

	void SetFileName(const std::string &);

	bool ReadFileInfo(VveDataSetInfo *);

protected:
	bool ReadTimeStepInfo(VveDataChunkInfo *);

	bool ReadFieldInfo(VveDataFieldInfo *);

private:
	std::ifstream m_myDataFile;
};

/*
* This is the "new" reader for binary data set information files.
*
*/

class VISTAVISEXTAPI VveInfoReader
{
public:
	VveInfoReader();
	virtual ~VveInfoReader();

	void SetFileName(const std::string &);

	bool ReadFileInfo(VveDataSetInfo *);

	bool ReadSingleChunkInfo(VveDataChunkInfo *);

private:
	std::ifstream m_myDataFile;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VVEINFOREADER_H

