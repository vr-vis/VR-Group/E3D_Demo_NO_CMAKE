/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVE2DHISTOGRAM_H
#define _VVE2DHISTOGRAM_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaSerializable.h>
#include <vector>
#include <cassert>
#include <string>
#include <limits>
#include <cmath>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class IVistaSerializer;
class IVistaDeSerializer;
class VveHistogram;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* Histogram describes the distribution of the data. 
* This class collects information of histogram for one data field (e.g. scalar field).
*/
class VISTAVISEXTAPI Vve2DHistogram : public IVistaSerializable
{
public:
	Vve2DHistogram();
	Vve2DHistogram(const int iRes[2], const double fRanges[4]);
	Vve2DHistogram(const Vve2DHistogram& oOther);
	           
	virtual ~Vve2DHistogram();

	/**
	 * Write acces to the histogram
	 * Keep these templated in order to be able to bin arbitrary attribute
	 * types.
	 */
	template<class T> bool	SetData(size_t nSize, T fDataX[], T fDataY[]);
	template<class T> bool	AppendData(size_t nSize, T fDataX[], T fDataY[]);

	/**
	 *
	 */
	Vve2DHistogram& operator=(const Vve2DHistogram& oOther);
	/**
	 * Add two histograms and return the result
	 * NOTE: This implicitly assumes that the ranges are matching. Not test
	 *       is performed, so take care!
	 * NOTE: Dimensions are tested however. If dimensions do not match, the
	 *		 operation will return the unchanged first operand.
	 * NOTE: Neither operand may be compressed!
	 */
	Vve2DHistogram& operator+(const Vve2DHistogram& oOther);
	/**
	 * self-addition
	 * NOTE: Same restriction as above apply!
	 */
	void operator+=(const Vve2DHistogram& oOther);

	/*
	void	SetNumBins(int iBins);
	int		GetNumBins() const;
	*/
	/**
	 * Set the resolution of the histogram in both dimensions.
	 * NOTE: This will INVALIDATE the histogram!
	 */
	void SetResolution(int iX, int iY);
	void SetResolution(const int iR[2]);
	void GetResolution(int &iX, int &iY) const;
	void GetResolution(int iR[2]) const;
	
	/**
	 * Get/Set the ranges vor the two axis. Array arguments are meant
	 * to be stored interleaved i.e. dR[0]=iMinX, dR[1]=iMaxX
	 * dR[2]=iMiny, and dR[3]=iMaxY.
	 *
	 * NOTE: Changing the ranges will INVALIDATE the histogram!
	 */
	void SetRanges(const double dR[4]);
	void SetRanges(double dXMin, double dXMax, double dYMin, double dYMax);
	
	void GetRanges(double dR[4]) const;
	void GetRanges(	double &dXMin, double &dXMax, 
					double &dYMin, double &dYMax) const;

	void	SetBinCount(int iBinX, int iBinY, int iBinCount);
	int		GetBinCount(int iBinX, int iBinY) const;

	void	SetNumValidPoints(size_t nNumValidPoints);
	size_t	GetNumValidPoints() const;

	//returns number of ALL points in the histogramm (including outliers)
	void	SetNumTotalPoints(size_t nNumTotalPoints);
	size_t	GetNumTotalPoints() const;

	int		GetMaxBinCount() const;
	void	SetMaxBinCount(int i);
	
	/**
	 * get the minimum/maximum value for a given bin
	 */
	double GetLowerBinLimit(int iAxis, int iBin) const;
	double GetUpperBinLimit(int iAxis, int iBin) const;

	/**
	 * Project histogram to the given axis (X=0, Y=1).
	 * This results in a 1D histogram which contains the sum
	 * of bin values for all bins along the given axis.
	 */
	void GetProjectedHistogram(int iAxis, VveHistogram& oHist) const;

	/**
	 * retrieve a single column of the histogram
	 */
	void GetColumn(int iColumn, VveHistogram& oHist);

	/**
	 * retrieve a single row of the histogram
	 */
	void GetRow(int iRow, VveHistogram& oHist);

	// clean up information, remove bins
	void CleanHistogram ();


	// compute the probability distribution, 
	// i.e. the "normalized" bin counts [0,1]
	// for each bin counts n and for total num of valid points m: n/m
	// only valid points will be taken into account
	void ComputeJointDistribution(std::vector< std::vector<double> > &vecProb); 

	// compute entropy of the underlying data
	double ComputeJointEntropy();

	// Compress histogram using RLE for zeros
	// you cannot access bins using GetBinCount if the histogram is compressed, yet
	void	SetCompress(bool bCompress);
	bool	GetIsCompressed() const;

	 /**
     * Think of this as "SAVE"
     */
    virtual int Serialize(IVistaSerializer &) const;
    /**
     * Think of this as "LOAD"
     */
    virtual int DeSerialize(IVistaDeSerializer &);

	virtual std::string GetSignature() const;

	void Compress ();
	void Decompress ();

private:
	std::vector< std::vector<int> > m_vecBins;
	//save different ranges for both axes
	double m_dRanges[4];
	bool m_bCompressed;
	//number of bins
	int m_iResolution[2];
	//count in the "biggest" bin
	int m_iMaxBinCount;
	//number of ALL points in the histogramm (including outliers)
	size_t m_nNumTotalPoints;
	//number of points whose values are within the given [min, max] interval.
	size_t m_nNumValidPoints;
};

/*============================================================================*/
/* TEMPLATE IMPLEMENTATION                                                    */
/*============================================================================*/
template<class T> 
bool Vve2DHistogram::SetData(size_t nSize, T fDataX[], T fDataY[])
{ 
	this->CleanHistogram();
	return this->AppendData(nSize, fDataX, fDataY);
}

template<class T>
bool Vve2DHistogram::AppendData(size_t nSize, T fX[], T fY[])
{
	double fInterval[2] = {
		(m_dRanges[1]-m_dRanges[0])/(double)(m_iResolution[0]-2),
		(m_dRanges[3]-m_dRanges[2])/(double)(m_iResolution[1]-2)
	};
	m_nNumTotalPoints += nSize;
	
	int iXBin = -1, iYBin = -1;
	// build histomap
	for(size_t i = 0; i < nSize; ++i)
	{
		// smaller than fMin in bins number 0
		if(fX[i] < m_dRanges[0])			
			iXBin = 0;
		// larger than fMax in bins m_iBins-1
		else if(fX[i] > m_dRanges[1])		
			iXBin = m_iResolution[0]-1;
		// regular in bins 1..m_iBins-2
		else
		{
			// to catch the case where the item is exactly on the upper border
			if(fabs(fX[i]-m_dRanges[1]) < std::numeric_limits<double>::epsilon())
				iXBin = m_iResolution[0]-2;
			else
				iXBin = (int) floor( (fX[i] - m_dRanges[0]) / fInterval[0] ) + 1;
		}
		// smaller than fMin in bins number 0
		if(fY[i] < m_dRanges[2])			
			iYBin = 0;
		// larger than fMax in bins m_iBins-1
		else if(fY[i] > m_dRanges[3])		
			iYBin = m_iResolution[1]-1;
		// regular in bins 1..m_iBins-2
		else
		{
			// to catch the case where the item is exactly on the upper border
			if(fabs(fY[i]-m_dRanges[3]) < std::numeric_limits<double>::epsilon())
				iYBin = m_iResolution[1]-2;
			else
				iYBin = (int) floor( (fY[i] - m_dRanges[2]) / fInterval[1] ) + 1; 
		}

		++m_vecBins[iYBin][iXBin];
		m_iMaxBinCount = std::max<int>(m_iMaxBinCount,m_vecBins[iYBin][iXBin]);
		if(	iXBin > 0 && iXBin < m_iResolution[0]-1 &&
			iYBin > 0 && iYBin < m_iResolution[1]-1)
		{
			++m_nNumValidPoints;
		}
		assert(m_vecBins[iYBin][iXBin] > 0);
	}
	
	return true;
}
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE "Vve2DHistogram.h"                                             */
/*============================================================================*/
#endif //_VVE2DHISTOGRAM_H

