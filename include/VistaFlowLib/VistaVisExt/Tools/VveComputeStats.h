/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVECOMPUTESTATS_H
#define _VVECOMPUTESTATS_H 

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>
#include <list>
#include <set>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkDataSet;
template<class C> class VveDiscreteDataTyped;
class VveStatsSet;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Compute statistics for a either the entire set or a subset of points 
 * in a given reference data set
 */
class VISTAVISEXTAPI VveComputeStats 
{
public:
	VveComputeStats();
	virtual ~VveComputeStats();

	/**
	 *
	 */
	void SetReferenceDataSet(VveDiscreteDataTyped<vtkDataSet> *pData);

	/**
	 * compute full stats for the entire data set
	 * This call can be restricted to a certen level index range.
	 * Per default (iMinLevelIdx=-1, iMaxLevelIdx=-1) all data for
	 * all time levels will be generated.
	 * It is assumed that there is a valid (possibly empty) entry 
	 * in the input vector for each level index for which data is computed.
	 * NOTE: It is assumed that the target time histogram contained in the 
	 *       stats set is preconfigured with the desired resolution and 
	 *       ranges by the calling client.
	 * NOTE: The routine will bail out with no data generated if the given 
	 *       field is not present in the data.
	 */
	void ComputeStats(	const std::string& strFieldName,
						VveStatsSet *pStats,
						int iMinLevelIdx=-1, int iMaxLevelIdx=-1);
	/**
	 * Compute the stats for the given point ids and data field per time level.
	 * This call can be restricted to a certen level index range.
	 * Per default (iMinLevelIdx=-1, iMaxLevelIdx=-1) all data for
	 * all time levels will be generated.
	 * It is assumed that there is a valid (possibly empty) entry 
	 * in the input vector for each level index for which data is computed.
	 * NOTE: It is assumed that the target time histogram contained in the 
	 *       stats set is preconfigured with the desired resolution and 
	 *       ranges by the calling client.
	 * NOTE: The routine will bail out with no data generated if the given 
	 *       field is not present in the data.
	 */
	void ComputeStats(	const std::vector< std::set<int> > &vecPointIdsPerLevel, 
						const std::string& strFieldName,
						VveStatsSet *pStats,
						int iMinLevelIdx=-1, int iMaxLevelIdx=-1);

protected:

private:
	VveDiscreteDataTyped<vtkDataSet> *m_pData;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VVECOMPUTESTATS_H

