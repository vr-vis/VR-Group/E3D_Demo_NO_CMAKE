/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkObjectStack.H             RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VtkObjectStack :   ...                                           */
/*                                                                            */
/*============================================================================*/

#ifndef _VTKOBJECTSTACK_H
#define _VTKOBJECTSTACK_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <stack>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkObject;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * VtkObjectStack is responsible for collecting vtk objects to allow for an
 * easy deletion without having to keep track of every single object ever created.
 * 
 * @author  Marc Schirski
 * @date    May 2003
 */    
class VISTAVISEXTAPI VtkObjectStack
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VtkObjectStack();
    virtual ~VtkObjectStack();

    // IMPLEMENTATION
    /**
     * Pushes an object on the stack to schedule it for deletion.
     * 
     * @author  Marc Schirski
     * @date    May 2003
     *
     * @param[in]   vtkObject *pObject	a pointer to the vtk object to be registered
     * @param[out]  --
     * @GLOBALS --
     * @return  --
     *
     */    
    void PushObject(vtkObject *pObject);
    
    /**
     * Deletes all objects on the stack.
     * 
     * @author  Marc Schirski
     * @date    May 2003
     *
     * @param[in]   --
     * @param[out]  --
     * @GLOBALS --
     * @return  --
     *
     */    
     void DeleteObjects();

// MEMBERS
protected:
	std::stack<vtkObject*> m_stackObjects;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VTKOBJECTSTACK_H)
