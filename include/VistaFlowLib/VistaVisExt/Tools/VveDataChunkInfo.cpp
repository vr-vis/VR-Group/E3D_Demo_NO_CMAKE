/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveDataChunkInfo.h"
#include "VveDataFieldInfo.h"

#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaBase/VistaStreamUtils.h>

#include <limits>
#include <iostream>
using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveDataChunkInfo::VveDataChunkInfo() 
:m_iNumPoints(0), 
m_iNumCells(0), 
m_sSemanticName(""), 
m_iID(-1),
m_bSerializeChilds(true)
{
	for(int i = 0; i < 3; ++i)
	{
		m_fBounds[2*i] = numeric_limits<double>::max();
		m_fBounds[2*i+1] = -numeric_limits<double>::max();
	}
}
VveDataChunkInfo::~VveDataChunkInfo()
{
	for(unsigned int i = 0; i<m_vecChildDataChunks.size(); ++i)
		delete m_vecChildDataChunks[i];

	for(unsigned int i = 0; i < m_vecFieldInfo.size(); ++i)
		delete m_vecFieldInfo[i];
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNumPoints                                            */
/*                                                                            */
/*============================================================================*/

void VveDataChunkInfo::SetNumPoints(int iNumPoints)
{
	m_iNumPoints = iNumPoints;
}
int VveDataChunkInfo::GetNumPoints() const
{
	return m_iNumPoints;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNumCells                                             */
/*                                                                            */
/*============================================================================*/
void VveDataChunkInfo::SetNumCells(int iNumCells)
{
	m_iNumCells = iNumCells;
}
int VveDataChunkInfo::GetNumCells() const
{
	return m_iNumCells;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetBounds		                                          */
/*                                                                            */
/*============================================================================*/
void VveDataChunkInfo::SetBounds(double fBounds[6])
{
	for(int i = 0; i < 6; ++i)
		m_fBounds[i] = fBounds[i];
}
void VveDataChunkInfo::GetBounds(double fBounds[6]) const
{
	for(int i = 0; i < 6; ++i)
		fBounds[i] = m_fBounds[i];
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GeIthBound                                              */
/*                                                                            */
/*============================================================================*/
void VveDataChunkInfo::SetIthBound(int i, double fIthBound)
{
	m_fBounds[i] = fIthBound;
}
double VveDataChunkInfo::GetIthBound(int i) const
{
	return m_fBounds[i];
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddFieldInfo                                                */
/*                                                                            */
/*============================================================================*/
void VveDataChunkInfo::AddFieldInfo(VveDataFieldInfo* pFieldInfo)
{
	m_vecFieldInfo.push_back(pFieldInfo);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFieldInfo	                                              */
/*                                                                            */
/*============================================================================*/
VveDataFieldInfo* VveDataChunkInfo::GetFieldInfo(int i) const
{
	return m_vecFieldInfo[i];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFieldInfo	                                              */
/*                                                                            */
/*============================================================================*/
VveDataFieldInfo* VveDataChunkInfo::GetFieldInfoByName(
	const std::string & sFieldName) const
{
	const size_t nVecSize = m_vecFieldInfo.size();
	for(size_t i=0; i<nVecSize; ++i)
	{
		if (m_vecFieldInfo[i]->GetFieldName() == sFieldName)
		{
			return m_vecFieldInfo[i];
		}
	}
	return NULL;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumFieldInfo                                             */
/*                                                                            */
/*============================================================================*/
size_t VveDataChunkInfo::GetNumFieldInfo() const
{
	return m_vecFieldInfo.size();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetSemanticName                                         */
/*                                                                            */
/*============================================================================*/
void VveDataChunkInfo::SetSemanticName (const std::string & sChildSemantics)
{
	m_sSemanticName = sChildSemantics;
}

bool VveDataChunkInfo::GetSemanticName (std::string & sChildSemantics) const
{
	sChildSemantics = m_sSemanticName;
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetID			                                          */
/*                                                                            */
/*============================================================================*/
void VveDataChunkInfo::SetID(int iID)
{
	m_iID = iID;
}

int VveDataChunkInfo::GetID() const
{
	return m_iID;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddChildChunkInfo                                           */
/*                                                                            */
/*============================================================================*/
void VveDataChunkInfo::AddChildChunkInfo(VveDataChunkInfo* pChildChunkInfo)
{
	m_vecChildDataChunks.push_back(pChildChunkInfo);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetChildChunkInfo                                           */
/*                                                                            */
/*============================================================================*/
VveDataChunkInfo* VveDataChunkInfo::GetChildChunkInfo(int i) const
{
	return m_vecChildDataChunks[i];
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumChildChunkInfo                                        */
/*                                                                            */
/*============================================================================*/
size_t VveDataChunkInfo::GetNumChildChunkInfo() const
{
	return m_vecChildDataChunks.size();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDoSerializeChilds                                    */
/*                                                                            */
/*============================================================================*/
void VveDataChunkInfo::SetDoSerializeChilds (bool bDoSerializeChilds)
{
	m_bSerializeChilds = bDoSerializeChilds;
}

bool VveDataChunkInfo::GetDoSerializeChilds () const
{
	return m_bSerializeChilds;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Serialize					                                  */
/*                                                                            */
/*============================================================================*/
int VveDataChunkInfo::Serialize(IVistaSerializer & oSerializer) const
{
	int iNumBytes = 0;

#ifdef DEBUG
	//vstr::debugi() << "Serialize data chunk info above "<<m_sSemanticName<<"...\n";
#endif

	int iRet = oSerializer.WriteDelimitedString(this->GetSignature());
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = oSerializer.WriteInt32(m_iNumPoints);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	iRet = oSerializer.WriteInt32(m_iNumCells);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	for (int k=0; k < 6; ++k)
	{
		iRet = oSerializer.WriteDouble(m_fBounds[k]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;

	}

	iRet = oSerializer.WriteInt32((unsigned int) m_vecFieldInfo.size());
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	for (unsigned int k=0; k < m_vecFieldInfo.size(); ++k)
	{
		iRet = oSerializer.WriteSerializable (*m_vecFieldInfo[k]);
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;
	}

	iRet = oSerializer.WriteInt32(m_iID);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = oSerializer.WriteInt32((unsigned int) m_sSemanticName.size());
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = oSerializer.WriteString(m_sSemanticName);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;


	iRet = oSerializer.WriteBool(m_bSerializeChilds);
	if(iRet!=-1)
		iNumBytes += iRet;
	else
		return -1;

	if (m_bSerializeChilds)
	{
		iRet = oSerializer.WriteInt32((unsigned int) m_vecChildDataChunks.size());
		if(iRet!=-1)
			iNumBytes += iRet;
		else
			return -1;

		for (unsigned int k=0; k < m_vecChildDataChunks.size(); ++k)
		{
			iRet = oSerializer.WriteSerializable (*m_vecChildDataChunks[k]);
			if(iRet!=-1)
				iNumBytes += iRet;
			else
				return -1;
		}
	}
#ifdef DEBUG
	//vstr::debugi() << "... chunk finished ("<<iNumBytes<<" bytes)\n";
#endif

	return iNumBytes;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeSerialize					                              */
/*                                                                            */
/*============================================================================*/
int VveDataChunkInfo::DeSerialize(IVistaDeSerializer & oDeSerializer)
{
	int iNumBytes = 0;

#ifdef DEBUG
	//vstr::debugi() << "Deserialize data chunk info...\n";
#endif

	string str("");
	int iRet = oDeSerializer.ReadDelimitedString(str);
	if(iRet == -1 || str != this->GetSignature())
		return -1;

	iNumBytes += iRet+1;


	iRet = oDeSerializer.ReadInt32(m_iNumPoints);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = oDeSerializer.ReadInt32(m_iNumCells);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	for (int k=0; k < 6; ++k)
	{
		iRet = oDeSerializer.ReadDouble(m_fBounds[k]);
		if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

	}

	int iNumFields;
	iRet = oDeSerializer.ReadInt32(iNumFields);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	m_vecFieldInfo.resize(iNumFields);
	for (int k=0; k < iNumFields; ++k)
	{
		m_vecFieldInfo[k] = new VveDataFieldInfo();
		
		iRet = oDeSerializer.ReadSerializable (*m_vecFieldInfo[k]);
		if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;
	}

	iRet = oDeSerializer.ReadInt32(m_iID);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	int iStrSize = 0;
	iRet = oDeSerializer.ReadInt32(iStrSize);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = oDeSerializer.ReadString(m_sSemanticName, iStrSize);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	iRet = oDeSerializer.ReadBool(m_bSerializeChilds);
	if(iRet != -1)
		iNumBytes += iRet;
	else 
		return -1;

	if (m_bSerializeChilds)
	{
		int iNumChilds;
		iRet = oDeSerializer.ReadInt32(iNumChilds);
		if(iRet != -1)
			iNumBytes += iRet;
		else 
			return -1;

		m_vecChildDataChunks.resize(iNumChilds);
		for (int k=0; k < iNumChilds; ++k)
		{
			m_vecChildDataChunks[k] = new VveDataChunkInfo();
			iRet = oDeSerializer.ReadSerializable (*m_vecChildDataChunks[k]);
			if(iRet != -1)
				iNumBytes += iRet;
			else 
				return -1;

		}
	}
#ifdef DEBUG
	//vstr::debugi() << "... chunk finished ("<<iNumBytes<<" bytes)\n";
#endif

	return iNumBytes;

}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSignature				                                  */
/*                                                                            */
/*============================================================================*/
std::string VveDataChunkInfo::GetSignature() const
{
	return "__VveDataChunkInfo__";
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveDataChunkInfo.cpp"                                        */
/*============================================================================*/

