/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveDataSetInfo.h"
#include "VveDataFieldInfo.h"
#include "VveDataChunkInfo.h"

#include <limits>

using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveDataSetInfo::VveDataSetInfo() 
:VveDataChunkInfo (),
m_iMinTimeStep(0), 
m_iMaxTimeStep(0), 
m_strDataSetName("")
{
	
}
VveDataSetInfo::~VveDataSetInfo()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetDataSetName					                      */
/*                                                                            */
/*============================================================================*/
void VveDataSetInfo::SetDataSetName(const string &strDataSetName)
{
	m_strDataSetName = strDataSetName;
}
string VveDataSetInfo::GetDataSetName() const
{
	return m_strDataSetName;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMinTimeStep					                      */
/*                                                                            */
/*============================================================================*/
void VveDataSetInfo::SetMinTimeStep(int i)
{
	m_iMinTimeStep = i;
}
int VveDataSetInfo::GetMinTimeStep() const
{
	return m_iMinTimeStep;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetMaxTimeStep					                      */
/*                                                                            */
/*============================================================================*/
void VveDataSetInfo::SetMaxTimeStep(int i)
{
	m_iMaxTimeStep = i;
}
int VveDataSetInfo::GetMaxTimeStep() const
{
	return m_iMaxTimeStep;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveDataSetInfo.cpp"                                          */
/*============================================================================*/

