/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEEXTRACTSTATS_H
#define _VVEEXTRACTSTATS_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>
#include <list>
#include <set>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTimeMapper;
class VveStatsSet;
class VveDataSetInfo;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Extract stats from a given data set information and create time series
 * and a time histogram.
 */
class VISTAVISEXTAPI VveExtractStats
{
public:
	VveExtractStats();
	virtual ~VveExtractStats();

	/**
	 *
	 */
	void SetReferenceInfo(VveDataSetInfo *pInfo);
	VveDataSetInfo *GetReferenceInfo() const;

	void SetReferenceTimeMapper(VveTimeMapper *pTM);
	VveTimeMapper *GetReferenceTimeMapper() const;

	/**
	 * Extract stats based on the set reference information set and time
	 * mapper.
	 */
	void ExtractStats(const std::string& strFieldName, VveStatsSet *pSet);

protected:

private:
	VveDataSetInfo *m_pInfo;
	VveTimeMapper *m_pTM;
};
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VveExtractStats_H

