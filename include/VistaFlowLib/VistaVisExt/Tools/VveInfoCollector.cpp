/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveInfoCollector.h"
#include "VveDataChunkInfo.h"
#include "VveHistogram.h"
#include "VveDataSetInfo.h"
#include "VveDataFieldInfo.h"
#include "Vve2DHistogram.h"

#include "../Data/VveOctree.h"
#include "../Data/VvePointSetOctreeBuilder.h"

#include <VistaTools/VistaProgressBar.h>
#include <VistaBase/VistaStreamUtils.h>

#include <vtkDataSet.h>
#include <vtkDataSetReader.h>
#include <vtkDataArray.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>

#include <vtkExtractCells.h>
#include <vtkExtractGeometry.h>
#include <vtkBox.h>
#include <vtkIdList.h>

#include <cassert>
#include <algorithm>

using namespace std;

// a const value for a epsilon, needed to extract sub-datasets described by
// octree nodes from the overall time step data
static const double dEps = 0.0000001;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveInfoCollector::VveInfoCollector()
: m_iMinTimeLevel(0),
m_iMaxTimeLevel(0),
m_pExtractGeometry(vtkExtractGeometry::New()),
m_pBoundingBox(vtkBox::New()),
m_pBlockReader(vtkDataSetReader::New()),
m_pTimeStepData(NULL),
m_iGenerateBlocks(0),
m_bGenerateBlocks(false),
m_pOctree(NULL)
{
}
VveInfoCollector::~VveInfoCollector()
{
	m_pExtractGeometry->Delete ();
	m_pBoundingBox->Delete ();
	m_pBlockReader->Delete();

	if (m_pOctree)
			delete m_pOctree;
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*				                                                              */
/*  NAME      :   Set/GetDataSetName                                          */
/*			                                                                  */
/*============================================================================*/

void VveInfoCollector::SetDataSetName(const string &strDataSetName)
{
	m_oDSName.Set(strDataSetName);
}
string VveInfoCollector::GetDataSetName() const
{
	return m_oDSName.GetPattern();
}


/*============================================================================*/
/*				                                                              */
/*  NAME      :  Set/GetMin/MaxTimeStep				                          */
/*			                                                                  */
/*============================================================================*/
void VveInfoCollector::SetMinTimeStep(int i)
{
	m_iMinTimeLevel = i;
}
int VveInfoCollector::GetMinTimeStep() const
{
	return m_iMinTimeLevel;
}
void VveInfoCollector::SetMaxTimeStep(int i)
{
	m_iMaxTimeLevel = i;
}
int VveInfoCollector::GetMaxTimeStep() const
{
	return m_iMaxTimeLevel;
}


/*============================================================================*/
/*				                                                              */
/*  NAME      :  GetFileName						                          */
/*			                                                                  */
/*============================================================================*/
string VveInfoCollector::GetFileName(int iTimeStep) const
{
	return m_oDSName.GetFileName(iTimeStep);
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  Set/GetNumBins						                          */
/*			                                                                  */
/*============================================================================*/
void VveInfoCollector::SetNumBins(int i)
{
	m_iNumBins = i+2;
}
int VveInfoCollector::GetNumBins() const
{
	return m_iNumBins-2;
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  Set/GetGenerateBlocks				                          */
/*			                                                                  */
/*============================================================================*/
void VveInfoCollector::SetGenerateBlocks (int iNumBlocks)
{
	m_iGenerateBlocks = iNumBlocks;
	m_bGenerateBlocks = (m_iGenerateBlocks>0);
}

int VveInfoCollector::GetGenerateBlocks () const
{
	return m_iGenerateBlocks;
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  ComputeAverage						                          */
/*			                                                                  */
/*============================================================================*/
double VveInfoCollector::ComputeAverage(vtkDataArray *pArray, int j)
{
	double fAverage = 0.0f;
	double dRange[2];
	double dVal;
	pArray->GetRange(dRange,j);
	for(int i = 0; i < pArray->GetNumberOfTuples(); ++i)
	{
		dVal = pArray->GetComponent(i, j);
		fAverage += dVal;
		if(dVal < dRange[0] || dVal > dRange[1])
		{
			vstr::errp() << "*** ERROR *** Value out of bounds : range["
				 << dRange[0] << ", " << dRange[1] << "] value: " << dVal << endl;
		}
	}
	return fAverage /= (double) pArray->GetNumberOfTuples();
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  ComputeStandardDeviation			                          */
/*			                                                                  */
/*============================================================================*/
double VveInfoCollector::ComputeStandardDeviation(vtkDataArray *pArray, int j, double dAverage)
{
	double fStandardDeviation = 0.0f;
	double dRange[2];
	double dVal;
	pArray->GetRange(dRange,j);
	for(int i = 0; i < pArray->GetNumberOfTuples(); ++i)
	{
		dVal = pArray->GetComponent(i, j);
		fStandardDeviation += ((dVal-dAverage)*(dVal-dAverage));
	}
	return sqrt (fStandardDeviation * 1.0/( (double) pArray->GetNumberOfTuples()));
}


/*============================================================================*/
/*				                                                              */
/*  NAME      :  CollectInfoSingleBlock				                          */
/*			                                                                  */
/*============================================================================*/
VveDataChunkInfo* VveInfoCollector::CollectInfoSingleChunk(int iBlockID, const std::string & strSemanticName, vtkDataSet *pDataSet)
{
	VveDataChunkInfo *pBlockInfo = new VveDataChunkInfo;
	pBlockInfo->SetSemanticName(strSemanticName);
	pBlockInfo->SetID(iBlockID);

	//get the number of points and cells in the file
  	pBlockInfo->SetNumPoints(pDataSet->GetNumberOfPoints());
	pBlockInfo->SetNumCells(pDataSet->GetNumberOfCells());

	//get bounds in the file
	double fBounds[6] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	pDataSet->ComputeBounds();
	pDataSet->GetBounds(fBounds);
	pBlockInfo->SetBounds(fBounds);

	vtkPointData *pPD = pDataSet->GetPointData();

	// for all fields
	for(int i = 0; i < pPD->GetNumberOfArrays(); ++i)
	{
		VveDataFieldInfo *pFieldInfo = new VveDataFieldInfo;

		vtkDataArray *pArray = pPD->GetArray(i);
		const int iNumCom = pArray->GetNumberOfComponents();
		pFieldInfo->SetFieldName(pArray->GetName());
		pFieldInfo->SetNumberOfComponents(pArray->GetNumberOfComponents());
		pFieldInfo->SetType(pArray->GetDataType());
		if (pArray->GetNumberOfTuples()>0)
			for(int j=0; j<iNumCom; ++j)
			{

				//range of each component
				double fRange[2] = {0.0, 0.0};
				pArray->GetRange(fRange,j);
				pFieldInfo->SetRange(j, fRange);
				//average of each component
				double fAvg = ComputeAverage(pArray, j);
				pFieldInfo->SetAverage(j, fAvg);
				assert(	pFieldInfo->GetMin(j) <= pFieldInfo->GetAverage(j) &&
					pFieldInfo->GetMax(j) >= pFieldInfo->GetAverage(j));
				double fStdDev = ComputeStandardDeviation(pArray, j, fAvg);
				pFieldInfo->SetStandardDeviation(j, fStdDev);
			}

		pBlockInfo->AddFieldInfo(pFieldInfo);
	}

	return pBlockInfo;
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  LoadFile							                          */
/*			                                                                  */
/*============================================================================*/
vtkDataSet* VveInfoCollector::LoadFile (vtkDataSetReader *pReader, int iTimeStep)
{
	std::string strFilename = this->GetFileName(iTimeStep);
	ifstream oFileInput(strFilename.c_str());
	if(!oFileInput.is_open())
	{
		vstr::outi() << endl;
		vstr::errp() << "[VveInfoCollector::"
			 << "CollectInfoSingleTimeStep]" << endl
			 << "\tUnable to open file "
			 << strFilename << endl << endl;
		return NULL;
	}
	oFileInput.close();

	pReader->SetFileName(strFilename.c_str());

	//make sure we read everything there is
	pReader->ReadAllColorScalarsOn();
	pReader->ReadAllScalarsOn();
	pReader->ReadAllVectorsOn();
	pReader->ReadAllNormalsOn();
	pReader->ReadAllTensorsOn();
	pReader->ReadAllFieldsOn();
	pReader->Update();

	return pReader->GetOutput();
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  InitBlockManagement				                          */
/*			                                                                  */
/*============================================================================*/
bool VveInfoCollector::InitTimeLevel (int iTimeStep, size_t &nMinBlock,
	size_t &nMaxBlock)
{
	m_iCurrentTimeLevel = iTimeStep;

	// load file
	m_pTimeStepData= LoadFile (m_pBlockReader, m_iCurrentTimeLevel);

	if (m_pTimeStepData==NULL)
	{
		// no data, no fun
		return false;
	}
	m_pTimeStepData->ComputeBounds();

	if (m_bGenerateBlocks)
	{
		if (m_pOctree)
			delete m_pOctree;

		m_pOctree = VvePointSetOctreeBuilder::BuildOctreeForPoints (m_pTimeStepData, m_iGenerateBlocks);
		//m_pOctree = VvePointSetOctreeBuilder::BuildOctreeForCells (m_pData, m_iGenerateBlocks);

		m_pOctree->GetLeafNodes (m_vecLeafs);
		nMinBlock = 0;
		nMaxBlock = m_vecLeafs.size() - 1;

		// check, if accumulated number of points in all leafs is equal to
		// the number of points in the time step
		size_t nNumPoints = 0;
		for (size_t i=nMinBlock; i<=nMaxBlock; ++i)
		{
			nNumPoints += m_vecLeafs[i]->GetNumPoints();
		}
		assert(m_pTimeStepData->GetNumberOfPoints()
			== static_cast<int>(nNumPoints));

		//m_pExtractCells->RemoveAllInputs();
		m_pExtractGeometry->RemoveAllInputs();

#if VTK_MAJOR_VERSION > 5
		m_pExtractGeometry->SetInputData(m_pTimeStepData);
#else
		m_pExtractGeometry->SetInput(m_pTimeStepData);
#endif
		m_pExtractGeometry->SetImplicitFunction(m_pBoundingBox);
		m_pExtractGeometry->ExtractInsideOn();
		m_pExtractGeometry->ExtractBoundaryCellsOff ();

		vstr::outi() << "Generate blocks (threshold "<<m_iGenerateBlocks<<") will divide into "<<m_vecLeafs.size()<<" blocks\n";
	}
	else
	{
		nMinBlock = 0;
		nMaxBlock = 0;
	}
	return true;
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  GetBlock							                          */
/*			                                                                  */
/*============================================================================*/
vtkDataSet* VveInfoCollector::GetBlock(size_t nBlock)
{
	vtkDataSet* pBlockData = NULL;

	// if we do not generate blocks or we generate, but this is the first one, load the data
	if (!m_bGenerateBlocks)
		return NULL;


	VveOctreeNode* pOctreeNode = m_vecLeafs[nBlock];
	assert (pOctreeNode->IsLeaf());

	// compute pData from octree node

	double dBounds[6];
	// make sure this leaf has valid bounds
	pOctreeNode->ComputeBounds();
	pOctreeNode->GetBounds(dBounds);
	for (int i=0; i < 3; ++i)
	{
		dBounds[i*2] -= dEps;
		dBounds[i*2+1] += dEps;
	}
	// feed bounds into implicit function
	m_pBoundingBox->SetBounds (dBounds);
	// cut data with this implicit function
	m_pExtractGeometry->Update();
	pBlockData = (vtkDataSet*) m_pExtractGeometry->GetOutput();

	// the number of points in the extracted block must match
	// the number of points in the octree leaf
	assert (pBlockData->GetNumberOfPoints()==pOctreeNode->GetNumPoints());

	return pBlockData;
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  GetCurrentTimeStep					                          */
/*			                                                                  */
/*============================================================================*/

vtkDataSet* VveInfoCollector::GetCurrentTimeLevel () const
{
	return m_pTimeStepData;
}


/*============================================================================*/
/*				                                                              */
/*  NAME      :  CollectInfoSingleTimeStep			                          */
/*			                                                                  */
/*============================================================================*/
VveDataChunkInfo* VveInfoCollector::CollectInfoSingleTimeLevel(int iTimeLevel)
{
	VveDataChunkInfo *pTimeStepInfo = NULL;

	int	iNumPoints = 0;
	int iNumCells = 0;

	size_t nMinBlock = 0;
	size_t nMaxBlock = 0;

	// load current time step and (if m_bGenerateBlocks, generate octree on time step data
	this->InitTimeLevel (iTimeLevel, nMinBlock, nMaxBlock);

	// if we have subdivisions on the data, go through them
	if (m_bGenerateBlocks)
	{
		pTimeStepInfo = new VveDataChunkInfo;
		pTimeStepInfo->SetSemanticName ("Timelevel");
		pTimeStepInfo->SetID (iTimeLevel);

		for(size_t nBlock = nMinBlock; nBlock <= nMaxBlock; ++nBlock)
		{
			// extract block from current time step
			vtkDataSet* pBlockData = this->GetBlock(nBlock);

			// collect information on block data
			// @todo: Get rid of this cast!
			VveDataChunkInfo *pBlockInfo = this->CollectInfoSingleChunk(
				static_cast<int>(nBlock), "Block", pBlockData);

			iNumPoints += pBlockInfo->GetNumPoints();

			// this bracket is only for readability: in this source code block,
			// we compute ranges and averages from the block data to avoid computing it again
			// from the whole time level data
			{
				//compare bounds
				for(int i = 0; i < 3; ++i)
				{
					pTimeStepInfo->SetIthBound(2*i, min(pTimeStepInfo->GetIthBound(2*i), pBlockInfo->GetIthBound(2*i)));//min
					pTimeStepInfo->SetIthBound(2*i+1, max(pTimeStepInfo->GetIthBound(2*i+1), pBlockInfo->GetIthBound(2*i+1)));//max
				}

				VveDataFieldInfo *pTimeStepFieldInfo;

				//for every data field in the block info object
				const int nNumFieldInfo = static_cast<int>(pBlockInfo->GetNumFieldInfo());
				for(int k = 0; k < nNumFieldInfo; ++k)
				{
					//if we are evaluating the first block -> init new field info for time step
					if(nBlock == nMinBlock)
					{
						pTimeStepFieldInfo = new VveDataFieldInfo;
						//this stuff should be the same for all blocks anyway -> do only once!
						pTimeStepFieldInfo->SetFieldName(pBlockInfo->GetFieldInfo(k)->GetFieldName());
						pTimeStepFieldInfo->SetType(pBlockInfo->GetFieldInfo(k)->GetType());
						pTimeStepFieldInfo->SetNumberOfComponents(pBlockInfo->GetFieldInfo(k)->GetNumberOfComponents());

						pTimeStepInfo->AddFieldInfo(pTimeStepFieldInfo);
					}
					else
					{
						pTimeStepFieldInfo = pTimeStepInfo->GetFieldInfo(k);
					}

					for(int c=0; c<pBlockInfo->GetFieldInfo(k)->GetNumberOfComponents(); ++c)
					{
						//compare range with the one of the updated pBlockInfo
						pTimeStepFieldInfo->SetMin(c,
							std::min<double>(pTimeStepFieldInfo->GetMin(c),
							pBlockInfo->GetFieldInfo(k)->GetMin(c)));
						pTimeStepFieldInfo->SetMax(c,
							std::max<double>(pTimeStepFieldInfo->GetMax(c),
							pBlockInfo->GetFieldInfo(k)->GetMax(c)));
						//add up the sum of all scalars of some field data
						pTimeStepFieldInfo->SetAverage(c,
							pTimeStepFieldInfo->GetAverage(c) + (pBlockInfo->GetFieldInfo(k)->GetAverage(c))*(pBlockInfo->GetNumPoints()));
					}
				}

				// if there are several blocks, add them as childs to the timestep info
				// add block information to time step info
				pTimeStepInfo->AddChildChunkInfo (pBlockInfo);

			}
		}

		pTimeStepInfo->SetNumPoints(GetCurrentTimeLevel()->GetNumberOfPoints());
		pTimeStepInfo->SetNumCells(GetCurrentTimeLevel()->GetNumberOfCells());

		const int nNumFieldInfo = static_cast<int>(pTimeStepInfo->GetNumFieldInfo());
		for(int k = 0; k < nNumFieldInfo; ++k)
		{
			for(int c=0; c<pTimeStepInfo->GetFieldInfo(k)->GetNumberOfComponents(); ++c)
			{
				VveDataFieldInfo *pFieldInfo = pTimeStepInfo->GetFieldInfo(k);
				pFieldInfo->SetAverage(c, pFieldInfo->GetAverage(c)/(double)iNumPoints);
				assert(	pFieldInfo->GetMin(c) <= pFieldInfo->GetAverage(c) &&
					pFieldInfo->GetMax(c) >= pFieldInfo->GetAverage(c));
			}
		}

		// as we can't sum up the standard deviation as we can do with averages (you need covariances
		// to do that), we compute the time levels SD "manually" here
		vtkPointData *pPD = GetCurrentTimeLevel()->GetPointData();

		// for all fields
		for(int k = 0; k < nNumFieldInfo; ++k)
		{
			// compute standard deviation..we can't just sum this up as the average, as
			// we therefore need the covariance of the single blocks, which we don't have
			vtkDataArray *pArray = pPD->GetArray(k);
			for(int c=0; c<pTimeStepInfo->GetFieldInfo(k)->GetNumberOfComponents(); ++c)
			{
				VveDataFieldInfo *pFieldInfo = pTimeStepInfo->GetFieldInfo(k);
				double dSD = this->ComputeStandardDeviation (pArray, c, pFieldInfo->GetAverage(c));
				pFieldInfo->SetStandardDeviation (c, dSD);
			}

		}

	}
	else
	{
		// no blocks generated, collect information for timestep
		pTimeStepInfo = this->CollectInfoSingleChunk (iTimeLevel, "Timelevel", GetCurrentTimeLevel());
	}



	return pTimeStepInfo;
}


/*============================================================================*/
/*				                                                              */
/*  NAME      :  CollectInformation					                          */
/*			                                                                  */
/*============================================================================*/
bool VveInfoCollector::CollectInformation(VveDataSetInfo *pDataSetInfo)
{
	if(pDataSetInfo == NULL)
	{
		vstr::err() << endl;
		vstr::errp() << "[VveInfoCollector::CollectInformation] "
			 << "Unable to operate on NULL dataset info" << endl << endl;
		return false;
	}
	int	iTotalNumPoints = 0;
	int iTotalNumCells = 0;


	//instead of doing it in main function
	pDataSetInfo->SetDataSetName(m_oDSName.GetPattern());
	pDataSetInfo->SetMinTimeStep(m_iMinTimeLevel);
	pDataSetInfo->SetMaxTimeStep(m_iMaxTimeLevel);
	pDataSetInfo->SetSemanticName("Dataset");

	vstr::outi() << "[VveInfoCollector::CollectInformation()] Analyze time steps ... " << endl;

	// setup progress bar for user feedback
	VistaProgressBar oProgressBar (m_iMaxTimeLevel-m_iMinTimeLevel+1, 1.0f);
	oProgressBar.SetPrefixString ("Time steps ");
	oProgressBar.SetSilent (false);
	oProgressBar.SetDisplayBar (true);
	oProgressBar.SetBarTicks (10);

	oProgressBar.Start();
	// for all time steps, merge collected data into pDataSetInfo
	for(int iTimeLevel = m_iMinTimeLevel; iTimeLevel <= m_iMaxTimeLevel; ++iTimeLevel)
	{
		// collect data for single time step
		VveDataChunkInfo *pTimeStepInfo = CollectInfoSingleTimeLevel(iTimeLevel);

		if(pTimeStepInfo == NULL)
		{
			vstr::err() << endl;
			vstr::errp() << "[VveInfoCollector::CollectInformation] "
				<< "Failed to collect data for time level <" << iTimeLevel
				<< ">" << endl << endl;
			return false;
		}

		//compare bounds
		for(int i = 0; i < 3; ++i)
		{
			pDataSetInfo->SetIthBound(2*i, min(pTimeStepInfo->GetIthBound(2*i), pDataSetInfo->GetIthBound(2*i)));//min
			pDataSetInfo->SetIthBound(2*i+1, max(pTimeStepInfo->GetIthBound(2*i+1), pDataSetInfo->GetIthBound(2*i+1)));//max
		}

		pDataSetInfo->AddChildChunkInfo(pTimeStepInfo);

		iTotalNumPoints += pTimeStepInfo->GetNumPoints();
		iTotalNumCells += pTimeStepInfo->GetNumCells();

		//merge time step info into data set field info
		const int nNumFieldInfo = static_cast<int>(pTimeStepInfo->GetNumFieldInfo());
		for(int k = 0; k < nNumFieldInfo; ++k)
		{
			// if this field is not present in pDataSetInfo, add it
			if(static_cast<int>(pDataSetInfo->GetNumFieldInfo()) <= k)
			{
				pDataSetInfo->AddFieldInfo(new VveDataFieldInfo);// initialize it with empty SFieldInfo
				pDataSetInfo->GetFieldInfo(k)->SetFieldName(pTimeStepInfo->GetFieldInfo(k)->GetFieldName());
				pDataSetInfo->GetFieldInfo(k)->SetType(pTimeStepInfo->GetFieldInfo(k)->GetType());
				pDataSetInfo->GetFieldInfo(k)->SetNumberOfComponents(pTimeStepInfo->GetFieldInfo(k)->GetNumberOfComponents());
			}
			// retrieve field k from pDataSetInfo
			VveDataFieldInfo *pInfo = pDataSetInfo->GetFieldInfo(k);
			for(int c=0; c<pInfo->GetNumberOfComponents(); ++c)
			{
				//compare range with the one of the updated ResultOneTimeStep
				pInfo->SetMin(c, std::min<double>(pInfo->GetMin(c), pTimeStepInfo->GetFieldInfo(k)->GetMin(c)));
				pInfo->SetMax(c, std::max<double>(pInfo->GetMax(c), pTimeStepInfo->GetFieldInfo(k)->GetMax(c)));
				//add up the sum of all scalars of some field data, will be divided by total num points later
				pInfo->SetAverage(c, pInfo->GetAverage(c) + (pTimeStepInfo->GetFieldInfo(k)->GetAverage(c))*((double)pTimeStepInfo->GetNumPoints()));
			}
		}

		oProgressBar.Increment ();
	}
	oProgressBar.Finish(true);

	vstr::outi() << "[VveInfoCollector::CollectInformation()] Merge time step analysis data. " << endl;

	// recompute averages
	const int nNumFieldInfo = static_cast<int>(pDataSetInfo->GetNumFieldInfo());
	for(int k = 0; k < nNumFieldInfo; ++k)
	{
		for(int c=0; c<pDataSetInfo->GetFieldInfo(k)->GetNumberOfComponents(); ++c)
		{
			VveDataFieldInfo *pInfo = pDataSetInfo->GetFieldInfo(k);
			pInfo->SetAverage(c, pInfo->GetAverage(c)/(float)iTotalNumPoints);
			assert(	pInfo->GetMin(c) <= pInfo->GetAverage(c) &&
					pInfo->GetMax(c) >= pInfo->GetAverage(c));

			// now that we have the average for field k component c, compute its standard deviation
			// w.r.t. the time step data
			double dSD = 0;
			for (int t=0; t <= m_iMaxTimeLevel-m_iMinTimeLevel; t++)
			{
				dSD += pow (pDataSetInfo->GetChildChunkInfo(t)->GetFieldInfo(k)->GetAverage(c)-pInfo->GetAverage(c),2);
			}
			dSD = sqrt (dSD * 1/(m_iMaxTimeLevel-m_iMinTimeLevel+1));
			pInfo->SetStandardDeviation (c, dSD);
		}
	}
	pDataSetInfo->SetNumPoints(iTotalNumPoints);
	pDataSetInfo->SetNumCells(iTotalNumCells);

	return true;
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  BuildHistogram						                          */
/*			                                                                  */
/*============================================================================*/
/*
void VveInfoCollector::BuildHistogram(VveDataSetInfo *pDataSetInfo)
{
	vstr::outi() << "Compute histograms. " << endl;

	//set min, max and number of bins
	for(int k = 0; k < pDataSetInfo->GetNumFieldInfo(); ++k)
	{
		//for total data set (histogram if any only for the first component)
		pDataSetInfo->GetFieldInfo(k)->GetHistogram()->SetRange(pDataSetInfo->GetFieldInfo(k)->GetMin(0), pDataSetInfo->GetFieldInfo(k)->GetMax(0));
		pDataSetInfo->GetFieldInfo(k)->GetHistogram()->SetNumBins(m_iNumBins);
		pDataSetInfo->GetFieldInfo(k)->GetHistogram()->CleanHistogram();
	}
	vstr::outi() << "Compute histograms for time steps " << endl;

	// setup progress bar for user feedback
	VistaProgressBar oProgressBar (m_iMaxTimeLevel-m_iMinTimeLevel+1, 1.0f);
	oProgressBar.SetPrefixString ("Time steps ");
	oProgressBar.SetSilent (false);
	oProgressBar.SetDisplayBar (true);
	oProgressBar.SetBarTicks (10);

	oProgressBar.Start();
	//build histogram
	//although all fieldinfo elements are initialized, the ones containing vector will not be processed later
	for(int iTimeStep = m_iMinTimeLevel; iTimeStep <= m_iMaxTimeLevel; ++iTimeStep)
	{
		// use this as time index [0,...,n]
		int	iCurrTimeStep = iTimeStep - m_iMinTimeLevel;

		int iMinBlock = -1;
		int iMaxBlock = -1;
		this->InitTimeLevel (iTimeStep, iMinBlock, iMaxBlock);

		// get current time step info
		VveDataChunkInfo* pTimeInfo = pDataSetInfo->GetChildChunkInfo(iCurrTimeStep);

		//set min, max and number of bins
		for(int k = 0; k < pDataSetInfo->GetNumFieldInfo(); ++k)
		{
			//for total data set (histogram if any only for the first component)
			pTimeInfo->GetFieldInfo(k)->GetHistogram()->SetRange(pDataSetInfo->GetFieldInfo(k)->GetMin(0), pDataSetInfo->GetFieldInfo(k)->GetMax(0));
			pTimeInfo->GetFieldInfo(k)->GetHistogram()->SetNumBins(m_iNumBins);
			pTimeInfo->GetFieldInfo(k)->GetHistogram()->CleanHistogram();
		}

		for(int iBlock = iMinBlock; iBlock <= iMaxBlock; ++iBlock)
		{
			// use this as block index [0,...,m]
			int iCurrBlock = iBlock-iMinBlock;

			// get current block info
			vtkDataSet* pBlockData = this->GetBlock (iBlock);
			VveDataChunkInfo* pBlockInfo = pTimeInfo->GetChildChunkInfo(iCurrBlock);

			vtkPointData *pPD = pBlockData->GetPointData();

			for(int k = 0; k < pPD->GetNumberOfArrays(); ++k)
			{
				vtkDataArray *pArray = pPD->GetArray(k);

				//set min, max and number of bins
				pBlockInfo->GetFieldInfo(k)->GetHistogram()->SetRange (pDataSetInfo->GetFieldInfo(k)->GetMin(0), pDataSetInfo->GetFieldInfo(k)->GetMax(0));
				pBlockInfo->GetFieldInfo(k)->GetHistogram()->SetNumBins(m_iNumBins);
				pBlockInfo->GetFieldInfo(k)->GetHistogram()->CleanHistogram();

				//for now build histogramm only for scalar data
				if(	pArray->GetNumberOfComponents() > 1 ||
					pArray->GetDataType() != VTK_FLOAT)
					continue;

				//what about non float data here ?
				pBlockInfo->GetFieldInfo(k)->GetHistogram()->AppendData(
					pArray->GetSize(), ((vtkFloatArray *)pArray)->GetPointer(0));

				// do not compute histograms from single block infos, as in multi-block mode,
				// there may be overlapping points in separate blocks, which leads
				// to erroneous results
				if (!this->GetCurrentTimeLevel())
				{
					//pTimeInfo->GetFieldInfo(k)->GetHistogram()->AppendData(
					//	pArray->GetSize(), ((vtkFloatArray *)pArray)->GetPointer(0));
					//assert (pTimeInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints()<=pTimeInfo->GetNumPoints());
					//pDataSetInfo->GetFieldInfo(k)->GetHistogram()->AppendData(
					//	pArray->GetSize(), ((vtkFloatArray *)pArray)->GetPointer(0));
					//assert (pDataSetInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints()<=pDataSetInfo->GetNumPoints());
				}
			}
		}

		if (this->GetCurrentTimeLevel())
		{
			vtkPointData *pPD = this->GetCurrentTimeLevel()->GetPointData();
			for(int k = 0; k < pPD->GetNumberOfArrays(); ++k)
			{
				vtkDataArray *pArray = pPD->GetArray(k);

				if(	pArray->GetNumberOfComponents() > 1 ||
					pArray->GetDataType() != VTK_FLOAT)
					continue;

				pTimeInfo->GetFieldInfo(k)->GetHistogram()->AppendData(
					pArray->GetSize(), ((vtkFloatArray *)pArray)->GetPointer(0));
				pDataSetInfo->GetFieldInfo(k)->GetHistogram()->AppendData(
					pArray->GetSize(), ((vtkFloatArray *)pArray)->GetPointer(0));
			}
		}
		// some assertions: check if number of data in histograms match number of points in field
		for(int k = 0; k < pDataSetInfo->GetNumFieldInfo(); ++k)
			if ((pTimeInfo->GetFieldInfo(k)->GetNumberOfComponents()==1)&&(pTimeInfo->GetFieldInfo(k)->GetType()== VTK_FLOAT))
			{
				//vstr::outi() << "Check field "<<k<<" comp "<<pTimeInfo->GetFieldInfo(k)->GetNumberOfComponents()<<" has "<<pTimeInfo->GetNumPoints()<<" points, hist points "<<pTimeInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints()<<"\n";
				assert (pTimeInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints()==pTimeInfo->GetNumPoints());
			}

		oProgressBar.Increment ();

	}
	oProgressBar.Finish(true);
	// some assertions: check if number of data in histograms match number of points in field
	for(int k = 0; k < pDataSetInfo->GetNumFieldInfo(); ++k)
		if ((pDataSetInfo->GetFieldInfo(k)->GetNumberOfComponents()==1)&&(pDataSetInfo->GetFieldInfo(k)->GetType()== VTK_FLOAT))
		{
			//vstr::outi() << "Check field "<<k<<" comp "<<pDataSetInfo->GetFieldInfo(k)->GetNumberOfComponents()<<" has "<<pDataSetInfo->GetNumPoints()<<" points, hist points "<<pDataSetInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints()<<"\n";
			assert (pDataSetInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints()==pDataSetInfo->GetNumPoints());
		}

}
*/

/*============================================================================*/
/*				                                                              */
/*  NAME      :  FillTimeWindow						                          */
/*			                                                                  */
/*============================================================================*/

bool VveInfoCollector::FillTimeWindow (int iCurrentTimeStep, int iForwardSize, std::vector<sTimeWindowSlot> & vecTimeWindow)
{
	// load all missing time steps in window and build octree blocks for these
	for (int j=iCurrentTimeStep; j <= iCurrentTimeStep+iForwardSize; ++j)
	{
		// search existing window
		int l=0;
		while ((l < (int) vecTimeWindow.size())&&(vecTimeWindow[l].iTimeLevel != j))
			l++;

		// if time step j was not found, load and create it now
		if (l==vecTimeWindow.size())
		{
			sTimeWindowSlot oNewSlot;
			oNewSlot.iTimeLevel = j;

			size_t nMinBlock = 0;
			size_t nMaxBlock = 0;
			this->InitTimeLevel (j, nMinBlock, nMaxBlock);

			// copy the initialized time step data
			oNewSlot.pTimeLevelData = this->GetCurrentTimeLevel()->NewInstance();
			oNewSlot.pTimeLevelData->DeepCopy(this->GetCurrentTimeLevel());

			// save single blocks
			if (m_bGenerateBlocks)
			{
				oNewSlot.vecBlockData.resize(nMaxBlock-nMinBlock+1);
				for (size_t k=nMinBlock; k<=nMaxBlock; k++)
				{
					vtkDataSet* pGeneratedBlock = this->GetBlock(k-nMinBlock);
					oNewSlot.vecBlockData[k-nMinBlock] = pGeneratedBlock->NewInstance();
					oNewSlot.vecBlockData[k-nMinBlock]->DeepCopy (pGeneratedBlock);
				}
			}

			vecTimeWindow.push_back (oNewSlot);
		}
	}
	return true;
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  ReleaseTimeWindowSlot				                          */
/*			                                                                  */
/*============================================================================*/
bool VveInfoCollector::ReleaseTimeWindowSlot (int iLastTimeLevel, std::vector<sTimeWindowSlot> & vecTimeWindow)
{
	// release slot for iLastTimeStep

	// search existing window
	std::vector<sTimeWindowSlot>::iterator itSlot = vecTimeWindow.begin();
	while ((itSlot != vecTimeWindow.end())&&(itSlot->iTimeLevel != iLastTimeLevel))
		itSlot++;

	// not found => no need to release
	if (itSlot == vecTimeWindow.end())
		return false;

	// delete time step and block data
	for (unsigned int i=0; i < itSlot->vecBlockData.size(); ++i)
		itSlot->vecBlockData[i]->Delete();
	itSlot->pTimeLevelData->Delete();

	vecTimeWindow.erase (itSlot);

	return true;
}

/*============================================================================*/
/*				                                                              */
/*  NAME      :  BuildHistogram						                          */
/*			                                                                  */
/*============================================================================*/
void VveInfoCollector::BuildJointHistogram(VveDataSetInfo *pDataSetInfo, int iTimeWindow)
{
	vstr::outi() << "[VveInfoCollector::BuildJointHistogram()] Compute histograms. " << endl;

	//set min, max and number of bins
	const int nNumFieldInfo = static_cast<int>(pDataSetInfo->GetNumFieldInfo());
	for(int k = 0; k < nNumFieldInfo; ++k)
	{
		//for total data set (histogram if any only for the first component)
		pDataSetInfo->GetFieldInfo(k)->GetHistogram()->SetRange(
			pDataSetInfo->GetFieldInfo(k)->GetMin(0), pDataSetInfo->GetFieldInfo(k)->GetMax(0));
		pDataSetInfo->GetFieldInfo(k)->GetHistogram()->SetNumBins(m_iNumBins);
		pDataSetInfo->GetFieldInfo(k)->GetHistogram()->CleanHistogram();
		pDataSetInfo->GetFieldInfo(k)->SetNumberOfJointHistograms(0);
	}

	/*
	* Ok, we use a time window as follows: the time window is centered at the current time step (C).
	* The larger half is forward (F), the smaller half backwards (B).
	*
	*	Example:
	*			... 4  5  6  7  8  9 ....
	*				B  B  C  F  F  F
	*
	* To compute the mutual information I(X;Y) with Y in time window, it suffices to save the joint histograms
	* for all forward time steps, as I(X;Y)=I(Y;X) is symmetric.
	*
	* To conclude, iNum2DHistograms is the larger half of the time window without the current time step.
	*/

	int iNum2DHistograms = (int) ceil ((float) (iTimeWindow-1)/2);

	if (iNum2DHistograms<0)
	{
		vstr::warnp() << "[VveInfoCollector::BuildJointHistogram()] Time window size is smaller than zero, no histograms are computed " << endl;
		return;
	}
	if (iNum2DHistograms==0)
		vstr::outi() << "[VveInfoCollector::BuildJointHistogram()] Compute 1D histograms for time levels " << endl;
	else
		vstr::outi() << "[VveInfoCollector::BuildJointHistogram()] Compute 1D and 2D histograms for time levels " << endl;

	// setup progress bar for user feedback
	VistaProgressBar oProgressBar (m_iMaxTimeLevel-m_iMinTimeLevel+1, 1.0f);
	oProgressBar.SetPrefixString ("Time levels ");
	oProgressBar.SetSilent (false);
	oProgressBar.SetDisplayBar (true);
	oProgressBar.SetBarTicks (10);

	oProgressBar.Start();

	std::vector<sTimeWindowSlot> vecCurrentTimeWindow;

	// go through all time step within [min, max). The last one does not need to computed,
	// as the last time step cannot have a forward time window
	for(int iTimeLevel = m_iMinTimeLevel; iTimeLevel <= m_iMaxTimeLevel; ++iTimeLevel)
	{
		// use this as time index [0,...,n]
		int	iTimeIndex = iTimeLevel - m_iMinTimeLevel;

		// resize iNum2DHistograms, if the window exceeds the maximum time step
		if (iTimeLevel+iNum2DHistograms>m_iMaxTimeLevel)
			iNum2DHistograms = m_iMaxTimeLevel-iTimeLevel+1;

		// no joint histograms at last level, as we do not have any successor time levels
		if (iTimeLevel == m_iMaxTimeLevel)
			iNum2DHistograms = 0;

		// get valid time window [iTimeLevel, ..., iTimeLevel+iNum2DHistograms], saved in vecCurrentTimeWindow
		this->FillTimeWindow (iTimeLevel, iNum2DHistograms, vecCurrentTimeWindow);

		// get current time step info
		VveDataChunkInfo* pTimeInfo = pDataSetInfo->GetChildChunkInfo(iTimeIndex);

		// check, if the first entry of vecCurrentTimeWindow is really the desired time level
		std::vector<sTimeWindowSlot>::iterator itSlot = vecCurrentTimeWindow.begin();
		while ((itSlot != vecCurrentTimeWindow.end())&&(itSlot->iTimeLevel != iTimeLevel))
			itSlot++;

		// we should have a valid slot for the current time step here
		assert (itSlot == vecCurrentTimeWindow.begin());

		//set min, max and number of bins
		for(int k = 0; k < nNumFieldInfo; ++k)
		{
			//for total data set (histogram if any only for the first component)
			pTimeInfo->GetFieldInfo(k)->GetHistogram()->SetRange(pDataSetInfo->GetFieldInfo(k)->GetMin(0), pDataSetInfo->GetFieldInfo(k)->GetMax(0));
			pTimeInfo->GetFieldInfo(k)->GetHistogram()->SetNumBins(m_iNumBins);
			pTimeInfo->GetFieldInfo(k)->GetHistogram()->CleanHistogram();

			//for total data set (histogram if any only for the first component)
			//pTimeInfo->GetFieldInfo(k)->SetNumberOfJointHistograms (iNum2DHistograms);
			pTimeInfo->GetFieldInfo(k)->SetNumberOfJointHistograms (iNum2DHistograms);
			double dRange[4];
			for (int j=0; j < iNum2DHistograms; ++j)
			{
				//here we assume intendical ranges for each direction
				dRange[0] = dRange[2] = pDataSetInfo->GetFieldInfo(k)->GetMin(0);
				dRange[1] = dRange[3] = pDataSetInfo->GetFieldInfo(k)->GetMax(0);
				pTimeInfo->GetFieldInfo(k)->GetJointHistogram(j)->SetRanges(dRange);
				pTimeInfo->GetFieldInfo(k)->GetJointHistogram(j)->SetResolution(m_iNumBins, m_iNumBins);
				pTimeInfo->GetFieldInfo(k)->GetJointHistogram(j)->CleanHistogram();
			}
		}

		// for each block
		const int nNumBlockData = static_cast<int>(vecCurrentTimeWindow[0].vecBlockData.size());
		for(int iBlock = 0; iBlock < nNumBlockData; ++iBlock)
		{
			// get current block info
			vtkDataSet* pBlockData = vecCurrentTimeWindow[0].vecBlockData[iBlock];

			VveDataChunkInfo* pBlockInfo = pTimeInfo->GetChildChunkInfo(iBlock);

			vtkPointData *pPD = pBlockData->GetPointData();

			// for each field
			for(int k = 0; k < pPD->GetNumberOfArrays(); ++k)
			{
				vtkDataArray *pArray = pPD->GetArray(k);

				//for now build histogramm only for scalar data, so skip here
				if(	pArray->GetNumberOfComponents() > 1 ||
					pArray->GetDataType() != VTK_FLOAT)
				{
					pBlockInfo->GetFieldInfo(k)->SetNumberOfJointHistograms (0);
					continue;
				}

				//set min, max and number of bins
				pBlockInfo->GetFieldInfo(k)->GetHistogram()->SetRange(
					pDataSetInfo->GetFieldInfo(k)->GetMin(0), pDataSetInfo->GetFieldInfo(k)->GetMax(0));
				pBlockInfo->GetFieldInfo(k)->GetHistogram()->SetNumBins(m_iNumBins);
				pBlockInfo->GetFieldInfo(k)->GetHistogram()->CleanHistogram();

				// compute 1D histogram
				pBlockInfo->GetFieldInfo(k)->GetHistogram()->AppendData(
					pArray->GetSize(), ((vtkFloatArray *)pArray)->GetPointer(0));

				pBlockInfo->GetFieldInfo(k)->GetHistogram()->SetCompress(true);

				// init and compute 2D histograms
				pBlockInfo->GetFieldInfo(k)->SetNumberOfJointHistograms(iNum2DHistograms);

				// for each forward slot
				double dRange[4];
				for (int j=0; j < iNum2DHistograms; ++j)
				{
					//NOTE: Here we assume identical range and resolution for both axes
					dRange[0] = dRange[2] = pDataSetInfo->GetFieldInfo(k)->GetMin(0);
					dRange[1] = dRange[3] = pDataSetInfo->GetFieldInfo(k)->GetMax(0);
					pBlockInfo->GetFieldInfo(k)->GetJointHistogram(j)->SetRanges(dRange);
					pBlockInfo->GetFieldInfo(k)->GetJointHistogram(j)->SetResolution(m_iNumBins, m_iNumBins);
					pBlockInfo->GetFieldInfo(k)->GetJointHistogram(j)->CleanHistogram();

					// append data for each of the corresponding arrays (block, field) in the time window
					vtkDataArray *pSecondArray = vecCurrentTimeWindow[j+1].vecBlockData[iBlock]->GetPointData()->GetArray(k);

					pBlockInfo->GetFieldInfo(k)->GetJointHistogram(j)->AppendData(
						pArray->GetSize(), ((vtkFloatArray *)pArray)->GetPointer(0), ((vtkFloatArray *)pSecondArray)->GetPointer(0));

					pBlockInfo->GetFieldInfo(k)->GetJointHistogram(j)->SetCompress(true);
				}


			}

		}

		// compute 1D histogram for time level and data set
		vtkPointData *pPD = vecCurrentTimeWindow[0].pTimeLevelData->GetPointData();
		for(int k = 0; k < pPD->GetNumberOfArrays(); ++k)
		{
			vtkDataArray *pArray = pPD->GetArray(k);

			if(	pArray->GetNumberOfComponents() > 1 ||
				pArray->GetDataType() != VTK_FLOAT)
				continue;

			pTimeInfo->GetFieldInfo(k)->GetHistogram()->AppendData(
				pArray->GetSize(), ((vtkFloatArray *)pArray)->GetPointer(0));
			pDataSetInfo->GetFieldInfo(k)->GetHistogram()->AppendData(
				pArray->GetSize(), ((vtkFloatArray *)pArray)->GetPointer(0));

			for (int j=0; j < iNum2DHistograms; ++j)
			{
				// append data for each of the corresponding arrays (block, field) in the time window
				vtkDataArray *pSecondArray = vecCurrentTimeWindow[j+1].pTimeLevelData->GetPointData()->GetArray(k);

				pTimeInfo->GetFieldInfo(k)->GetJointHistogram(j)->AppendData(
					pArray->GetSize(), ((vtkFloatArray *)pArray)->GetPointer(0), ((vtkFloatArray *)pSecondArray)->GetPointer(0));

				pTimeInfo->GetFieldInfo(k)->GetJointHistogram(j)->SetCompress(true);
			}
		}

		// some assertions: check if number of data in histograms match number of points in field
		for(int k = 0; k < nNumFieldInfo; ++k)
		{
			pTimeInfo->GetFieldInfo(k)->GetHistogram()->SetCompress(true);
			if ((pTimeInfo->GetFieldInfo(k)->GetNumberOfComponents()==1)&&(pTimeInfo->GetFieldInfo(k)->GetType()== VTK_FLOAT))
			{
				//vstr::debugi() << "Check field "<<k<<" comp "<<pTimeInfo->GetFieldInfo(k)->GetNumberOfComponents()<<" has "<<pTimeInfo->GetNumPoints()<<" points, hist points "<<pTimeInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints()<<"\n";
				assert (pTimeInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints()==pTimeInfo->GetNumPoints());
			}
		}

		oProgressBar.Increment ();

		// release current time step
		this->ReleaseTimeWindowSlot (iTimeLevel, vecCurrentTimeWindow);
	}

	// release all
	while (!vecCurrentTimeWindow.empty())
	{
		this->ReleaseTimeWindowSlot (vecCurrentTimeWindow.front().iTimeLevel, vecCurrentTimeWindow);
	}
	oProgressBar.Finish(true);
	// some assertions: check if number of data in histograms match number of points in field
	for(int k = 0; k < nNumFieldInfo; ++k)
	{
		pDataSetInfo->GetFieldInfo(k)->GetHistogram()->SetCompress(true);
		if ((pDataSetInfo->GetFieldInfo(k)->GetNumberOfComponents()==1)
			&& (pDataSetInfo->GetFieldInfo(k)->GetType()== VTK_FLOAT))
		{
			//vstr::debugi() << "Check field "<<k<<" comp "<<pDataSetInfo->GetFieldInfo(k)->GetNumberOfComponents()<<" has "<<pDataSetInfo->GetNumPoints()<<" points, hist points "<<pDataSetInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints()<<"\n";
			assert(pDataSetInfo->GetFieldInfo(k)->GetHistogram()->GetNumTotalPoints() == pDataSetInfo->GetNumPoints());
		}
	}
}


bool VveInfoCollector::CollectAllInformation(VveDataSetInfo * pInfo, int iJointWindow)
{
	bool bRet = false;

	bRet = CollectInformation (pInfo);

	/*
	* Now, after ranges and averages are determined, we can compute histograms
	* within the global ranges
	*/

	BuildJointHistogram (pInfo, iJointWindow);

	return bRet;
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveInfoCollector.cpp"                                        */
/*============================================================================*/

