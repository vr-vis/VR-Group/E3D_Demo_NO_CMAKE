/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef VVETIMEINTERVALMAPPING_H
#define VVETIMEINTERVALMAPPING_H

/*============================================================================*/
/* FORWARD DECLARATIONS														  */
/*============================================================================*/
class VveTimeMapper;


/*============================================================================*/
/* INCLUDES																	  */
/*============================================================================*/
#include "../VistaVisExtConfig.h"
#include <vector>


/*============================================================================*/
/* CLASS DEFINITION															  */
/*============================================================================*/
/**
 * This class allows to create a VveTimeMapper from a series of non-overlapping
 * time intervals that do not (necessarily) have to partition the complete
 * (simulation) time domain but can also imply empty, undefined intervals. For
 * those intervals, a special identifier (that can be specified by the user) is
 * returned, when querrying the generated VveTimeMapper for a point in time for
 * which no time interval was specified.
 *
 * @author Sebastian Pick (sp841227)
 * @date 2011/09/29
 */
class VISTAVISEXTAPI VveTimeIntervalMapping
{
public:
	struct IntervalInfo
	{
		double		m_dStartVisTime;
		double		m_dEndVisTime;
		int			m_nTimeLevel;
	};
	
	VveTimeIntervalMapping();
	VveTimeIntervalMapping( const VveTimeIntervalMapping& other );
	virtual ~VveTimeIntervalMapping();

	VveTimeIntervalMapping& operator=( const VveTimeIntervalMapping& other );

	bool SetExplicitGlobalVisTimeDomain( const double dStart, const double dEnd );
	void GetExplicitGlobalVisTimeDomain( double& dStart, double& dEnd ) const;

	bool SetExplicitGlobalVisTimeDomain( const double dDomain[2] );
	void GetExplicitGlobalVisTimeDomain( double dDomain[2] );

	void SetUseExplicitGlobalVisTimeDomain( bool bUseExplicitGlobalVisTimeDomain );
	bool GetUseExplicitGlobalVisTimeDomain() const;

	void SetTimeLevelForEmptyTimeIntervals( int nLevelIndex );
	int GetTimeLevelForEmptyTimeIntervals() const;

	bool BuildFromExplicitIntervalInfo( const std::vector<IntervalInfo>& vecExplicitIntervalInfo );
	/**
	 * Will retrieve a sorted vector of non-overlapping intervals that make up the current interval mapping.
	 */
	void GetCopyOfExplicitIntervalInfo( std::vector<IntervalInfo>& vecTarget ) const;
	std::size_t GetNumberOfTimeIntervals() const;

	VveTimeMapper* GetTimeMapper() const;	

protected:
	void ClampExistingIntervals();
	
	bool ValidateIntervals( const std::vector<IntervalInfo>& vecExplicitIntervalInfo );
	//! Assumes that no overlapping time intervals are contained in m_vecExplicitTimeIntervals.
	void SortIntervals();
	bool UpdateTimeMapper();
		
	bool DoTimeIntervalsOverlap( const IntervalInfo& me, const IntervalInfo& it ) const;
	bool ViolatesExplicitGlobalVisTimeDomain( const IntervalInfo& me ) const;

private:
	VveTimeMapper*				m_pTimeMapper;
	std::vector<IntervalInfo>	m_vecExplicitTimeIntervals;

	int							m_nLevelIndexForEmptyTimeIntervals;
	
	bool						m_bUseExplicitGlobalVisTimeDomain;
	double						m_dGlobalVisTimeStart;
	double						m_dGlobalVisTimeEnd;
};


#endif // Include guard.


/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/

