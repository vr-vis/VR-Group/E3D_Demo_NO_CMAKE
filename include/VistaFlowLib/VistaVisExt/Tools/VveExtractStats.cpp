/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveExtractStats.h"

#include "VveHistogram.h"
#include "Vve2DHistogram.h"

#include "../Data/VveDiscreteDataTyped.h"
#include "../Data/VveTimeHistogram.h"
#include "../Data/VveTimeSeries.h"
#include "../Data/VveStatsSet.h"
#include "../Tools/VveTimeMapper.h"
#include "../Tools/VveDataSetInfo.h"
#include "../Tools/VveDataFieldInfo.h"

#include <vtkPointSet.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>

#include <limits>

using namespace std;
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveExtractStats::VveExtractStats() 
	:	m_pInfo(NULL), m_pTM(NULL)
{
}

VveExtractStats::~VveExtractStats()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :					                                              */
/*                                                                            */
/*============================================================================*/
void VveExtractStats::SetReferenceInfo(VveDataSetInfo *pInfo)
{
	m_pInfo = pInfo;
}

VveDataSetInfo *VveExtractStats::GetReferenceInfo() const
{
	return m_pInfo;
}

void VveExtractStats::SetReferenceTimeMapper(VveTimeMapper *pTM)
{
	m_pTM = pTM;
}

VveTimeMapper *VveExtractStats::GetReferenceTimeMapper() const
{
	return m_pTM;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :					                                              */
/*                                                                            */
/*============================================================================*/
void VveExtractStats::ExtractStats(const std::string& strFieldName,
									VveStatsSet *pStats)
{
	if(m_pInfo == NULL || m_pTM == NULL)
	{
		vstr::errp() << "[VveExtractStats::ExtractStats] "
			 << "No reference data set and/or time mapper!" << endl;
		return;
	}
	VveTimeSeries *pMinSeries = pStats->GetMinSeries();
	VveTimeSeries *pMaxSeries = pStats->GetMaxSeries();
	VveTimeSeries *pAvgSeries = pStats->GetAvgSeries();
	VveTimeHistogram *pHistogram = pStats->GetHistogram();

	const int T=m_pTM->GetNumberOfTimeLevels();
	//here we assume that there are exactly as many time chunks in the
	//info object as in the given time mapper!
	if(T != m_pInfo->GetNumChildChunkInfo())
	{
		vstr::errp() << "[VveExtractStats::ExtractStats] "
			 << "Time mapper does not fit data set info!" << endl;
		return;
	}

	if( T != pMinSeries->GetTimeMapper()->GetNumberOfTimeLevels() ||
		T != pMaxSeries->GetTimeMapper()->GetNumberOfTimeLevels() ||
		T != pAvgSeries->GetTimeMapper()->GetNumberOfTimeLevels())
	{
		vstr::errp() << "[VveExtractStats::ExtractStats] "
			 << "Time mapper does not fit time series!" << endl;
		return;
	}

	if( pHistogram != NULL && 
		pHistogram->GetTimeMapper()->GetNumberOfTimeLevels() != T)
	{
		vstr::errp() << "[VveExtractStats::ExtractStats] "
			 << "Time mapper does not fit histogram!" << endl;
		return;
	}

	//sanity check for the field info
	VveDataFieldInfo *pFieldInfo = m_pInfo->GetFieldInfoByName(strFieldName);
	if(pFieldInfo == NULL)
	{
		vstr::errp() << "[VveExtractStats::ExtractStats] "
			 << "Field <" << strFieldName << "> does not exist in the data!" << endl;
		return;
	}
	//make sure the field name is set correctly
	pStats->SetFieldName(strFieldName);

	//configure histogram
	Vve2DHistogram *p2DHist = NULL;
	int iYRes = 0;
	if(pHistogram != NULL)
	{
		double dR[2] = {0.0, 0.0};
		//get the range for the overall histogram
		pFieldInfo->GetHistogram()->GetRange(dR);
		iYRes = pFieldInfo->GetHistogram()->GetNumBins();
		
		double dRanges[4] = {0, T-1, dR[0], dR[1]};
		p2DHist = pHistogram->GetHistogram();
		p2DHist->SetResolution(T+2, iYRes);
		p2DHist->SetRanges(dRanges);
		p2DHist->CleanHistogram();
	}
		
	VveHistogram *pH;
	bool bWasCompressed = false;
	//for all time steps
	for(int t=0; t<T; ++t)
	{
		//retrieve field info for this time step
		pFieldInfo = m_pInfo->GetChildChunkInfo(t)->GetFieldInfoByName(strFieldName);
		assert(pFieldInfo != NULL);

		//update the series entries for that level idx
		(*pMinSeries)[t] = pFieldInfo->GetMin(0);
		(*pMaxSeries)[t] = pFieldInfo->GetMax(0);
		(*pAvgSeries)[t] = pFieldInfo->GetAverage(0);
		
		if(p2DHist == NULL)
			continue;

		//copy histogram information
		pH = pFieldInfo->GetHistogram();
		if(pH == NULL)
		{
#ifdef DEBUG
			vstr::warnp() << " [VveExtractStats::ExtractStats] "
				 << "Should extract time histogram but info contains no histogram!" << endl;
#endif
			for(int b=0; b<iYRes; ++b)
				p2DHist->SetBinCount(t, b, 0);
			continue;
		}
		
		if(pH->GetIsCompressed())
		{
			pH->SetCompress(false);
			bWasCompressed = true;
		}
		//NOTE: mind the offset by one here!
		for(int b=0; b<iYRes; ++b)
			p2DHist->SetBinCount(t+1, b, pH->GetBinCount(b));
		
		pH->SetCompress(bWasCompressed);
		bWasCompressed = false;
	}
	//we changed a whole lot -> make this known
	pMinSeries->Notify();
	pMaxSeries->Notify();
	pAvgSeries->Notify();
	if(pHistogram)
		pHistogram->Notify();
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveExtractStats.cpp"                                        */
/*============================================================================*/

