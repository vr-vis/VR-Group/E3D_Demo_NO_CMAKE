/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VvePathlineWriterVtk.h"

#include <cassert>
#include <iostream>
#include <cmath>
#include <list>

#include <vtkPolyDataWriter.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>
#include <vtkCellArray.h>
#include <vtkLine.h>
#include <vtkPoints.h>
#include <vtkType.h>


#include <VistaTools/VistaFileSystemFile.h>
#include "VistaVisExt/Data/VveParticleTrajectory.h"


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VvePathlineWriterVtk::VvePathlineWriterVtk( VveParticlePopulation *pPopulation)
: m_pPopulation(pPopulation), 
  m_nCellCount(0),
  m_nCurrentCell(0),
  m_fProgress(0.0f)
{
}

VvePathlineWriterVtk::~VvePathlineWriterVtk()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Load                                                        */
/*                                                                            */
/*============================================================================*/
bool VvePathlineWriterVtk::Save(const std::string &strFilename,
								 const std::string &strScalarField, /*=VveParticlePopulation::sScalarArrayDefault*/ 
								 const std::string &strRadiusField, /*=VveParticlePopulation::sRadiusArrayDefault*/
								 const std::string &strTimeField, /*=VveParticlePopulation::sTimeArrayDefault*/
								 const std::string &strVelocityField, /*=VveParticlePopulation::sVelocityArrayDefault*/
								 const std::string &strTensorField /* = VveParticlePopulation::sEigenVectorArrayDefault*/)
{
    if (!m_pPopulation)
    {
        vstr::errp() << " [VvePathlineWriterVtk] no particle "
			 << "population to be filled..." << endl;
        return false;
    }
	
    if (strFilename.empty())
    {
        vstr::errp() << " [VvePathlineWriterVtk] no file name given..." << endl;
        return false;
    }


	// make a block to deconstruct file automatically
	VistaFileSystemFile file(strFilename);
	if(file.Exists())
	{
		vstr::warnp() << " [VvePathlineWriterVtk] Target file '" 
			<< strFilename << "' exists." << endl;
	}

	// init writer
	SetProgress(0, "Init Stage");
    vtkPolyDataWriter *pWriter = vtkPolyDataWriter::New();
    pWriter->SetFileName(strFilename.c_str());

	// use binary format by default
	pWriter->SetFileTypeToBinary();
    

	SetProgress(1.0f/3.0f, "Particle data convert");

	// create vtkPolyData object
	vtkPolyData *pData = vtkPolyData::New();
    if (!pData)
    {
        vstr::errp() << " [VvePathlineWriterVtk] unable to create vtk polydata..." << endl;
        return false;
    }

	const size_t nCellCount = static_cast<size_t>(m_pPopulation->GetNumTrajectories());

	int iPointCount = 0;
	for (size_t i=0; i<nCellCount; ++i)
	{
		// @todo: Get rid of cast.
		iPointCount += static_cast<int>( m_pPopulation->GetTrajectory(i)->GetSize());
	}
	// create data fields: vector, scalar, particle radius and time
	vtkFloatArray *pTimeStamps = NULL;
	if(!strTimeField.empty())
	{
		pTimeStamps = vtkFloatArray::New ();
		pTimeStamps->SetNumberOfComponents(1);
		pTimeStamps->SetNumberOfTuples(iPointCount);
		pTimeStamps->SetName (strTimeField.c_str());
	}

	vtkFloatArray *pScalars = NULL;
	if(!strScalarField.empty())
	{
		pScalars = vtkFloatArray::New ();
		pScalars->SetNumberOfComponents(1);
		pScalars->SetNumberOfTuples(iPointCount);
		pScalars->SetName (strScalarField.c_str());
	}
	
	vtkFloatArray *pVectors = NULL;
	if(!strVelocityField.empty())
	{
		pVectors = vtkFloatArray::New ();
		pVectors->SetNumberOfComponents(3);
		pVectors->SetNumberOfTuples(iPointCount);
		pVectors->SetName (strVelocityField.c_str());
	}
	
	vtkFloatArray *pTensors = NULL;
	//check if we got tensor data
	if(!strTensorField.empty())
	{
		pTensors = vtkFloatArray::New();
		pTensors->SetNumberOfComponents(9);
		pTensors->SetNumberOfTuples(iPointCount);
		pTensors->SetName(strTensorField.c_str());
	}
    
	vtkFloatArray *pRadius = NULL;
	if(!strRadiusField.empty())
	{
		pRadius = vtkFloatArray::New ();
		pRadius->SetNumberOfComponents(1);
		pRadius->SetNumberOfTuples(iPointCount);
		pRadius->SetName (strRadiusField.c_str());
	}

	// create cell array
	vtkCellArray* pLines = vtkCellArray::New();
	pLines->Allocate(nCellCount);

	// create point array
	vtkPoints* pPoints = vtkPoints::New ();
	pPoints->SetDataTypeToFloat();
	pPoints->SetNumberOfPoints (iPointCount);

#ifdef DEBUG
    vstr::debugi() << " [VvePathlineWriterVtk] - number of pathlines: " << nCellCount << endl;
    vstr::debugi() << " [VvePathlineWriterVtk] - number of trajectory points: " << iPointCount << endl;
#endif

    if (nCellCount)
    {
        // We do have pathlines, so interpret them as cells. Each cell is a trajectory consisting of points.
		// The whole population consists of several cells(==trajectories).
        SetCellCount(nCellCount);

		// global id counter
		vtkIdType iCurrentPoint = 0;

		// current line id list, is reseted for each cell
		vtkIdList *pCurrentLine = vtkIdList::New();

        for (size_t i=0; i<nCellCount; ++i)
        {
			SetCurrentCell(i);

            VveParticleTrajectory* pTraj = m_pPopulation->GetTrajectory(i);

			//skip empty ones!
			if(pTraj->GetSize() == 0)
				continue;

            VveParticleDataArrayBase *aPos = NULL, *aTime = NULL, *aVel = NULL, 
                                     *aScalar = NULL, *aRadius = NULL, *aEV = NULL;

			//retrieve arrays
			pTraj->GetArrayByName(VveParticlePopulation::sPositionArrayDefault, aPos);
			pTraj->GetArrayByName(strTimeField, aTime);

			if(aPos == NULL || aTime == NULL)
			{
				//major error --> no pos --> no fun
				vstr::errp()<<"[VvePathlineWriterVtk::Save] Unable to find point position and/or time array!\n";
				//cleanup the mess!
				pWriter->Delete();
				pData->Delete();
				pVectors->Delete();
				pScalars->Delete();
				pRadius->Delete();
				pTimeStamps->Delete();
				if(pTensors)
					pTensors->Delete();
				pLines->Delete();
				pPoints->Delete();
				//return w/o writing
				return false;
			}
			
			//all usual data arrays are strictly optional
			if(!strVelocityField.empty())
				pTraj->GetArrayByName(strVelocityField, aVel);
			if(!strScalarField.empty())
				pTraj->GetArrayByName(strScalarField, aScalar);
			if(!strRadiusField.empty())
				pTraj->GetArrayByName(strRadiusField, aRadius);
			if(!strTensorField.empty())
				pTraj->GetArrayByName(strTensorField, aEV);

            const size_t nTrajectorySize = pTraj->GetSize();

            float aTemp[3];
			// for each trajectory, insert points and data
			for (size_t j=0; j<nTrajectorySize; ++j)
			{
				//position
				aPos->GetElementCopy(j, aTemp, 3);
                pPoints->SetPoint (iCurrentPoint, aTemp);
                //time
				pTimeStamps->SetTuple1 (iCurrentPoint, aTime->GetElementValueAsDouble(j));
				//vector (optional
				if(aVel)
				{
					aVel->GetElementCopy(j, aTemp, 3);
					pVectors->SetTupleValue (iCurrentPoint, aTemp);
				}
				//scalar (optional)
				if(aScalar)
                {
                    pScalars->SetTuple1 (iCurrentPoint, aScalar->GetElementValueAsDouble(j));
                }
				//radius (optional)
                if (aRadius)
                {
                    pRadius->SetTuple1 (iCurrentPoint, aRadius->GetElementValueAsDouble(j));
                }
				//tensors (optional)
				if (pTensors && aEV)
                {
					double dVal[9];
                    aEV->GetElementCopy(j, dVal, 9);
					pTensors->SetTuple(iCurrentPoint, dVal);
				}
				//fill pt into line
				pCurrentLine->InsertNextId(iCurrentPoint);
				++iCurrentPoint;
			}
			// insert cell in line cell array
			pLines->InsertNextCell (pCurrentLine);
			pCurrentLine->Reset();
        }
		Notify(MSG_FINISH);
		pCurrentLine->Delete();
    }
    
	SetProgress(2.0f/3.0f, "Writing target file");

	//assemble vtkPolyData output
	pData->SetPoints(pPoints);
	pPoints->Delete();
	pData->SetLines(pLines);
	pLines->Delete();

	vtkPointData *pPD = pData->GetPointData();
	if(pVectors != NULL)
	{
		pPD->SetVectors (pVectors);
		pVectors->Delete();
	}
	if(pScalars != NULL)
	{
		pPD->SetScalars (pScalars);
		pScalars->Delete();
	}
	if(pTensors != NULL)
	{
		pPD->SetTensors(pTensors);
		pTensors->Delete();
	}
	if(pTimeStamps != NULL)
	{
		pPD->AddArray(pTimeStamps);
		pTimeStamps->Delete();
	}
	if(pRadius != NULL)
	{
		pPD->AddArray(pRadius);
		pRadius->Delete();
	}
	
	// finally, write it!
#if VTK_MAJOR_VERSION > 5
	pWriter->SetInputData(pData);
#else
	pWriter->SetInput(pData);
#endif
	pWriter->Update();

	pWriter->Delete();
	pData->Delete();
	
	// done
	SetProgress(1.0f, "Finish");

    return true;
}


size_t VvePathlineWriterVtk::GetCellCount() const
{
	return m_nCellCount;
}

size_t VvePathlineWriterVtk::GetCurrentCell() const
{
	return m_nCurrentCell;
}


void VvePathlineWriterVtk::SetCellCount(size_t nCellCount)
{
	if(compAndAssignFunc(nCellCount, m_nCellCount))
		Notify(MSG_CELLCOUNT_CHANGE);
}

void VvePathlineWriterVtk::SetCurrentCell(size_t nCurrentCell)
{
	if(compAndAssignFunc(nCurrentCell, m_nCurrentCell))
		Notify(MSG_CELLPROGRESS);
}


void VvePathlineWriterVtk::SetProgress(float fProgress,
	const std::string &strLabel)
{
	if(compAndAssignFunc(fProgress, m_fProgress))
	{
		m_strProgressLabel = strLabel;
		Notify(MSG_PROGRESS);
	}
}

std::string VvePathlineWriterVtk::GetProgressLabel() const
{
	return m_strProgressLabel;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
