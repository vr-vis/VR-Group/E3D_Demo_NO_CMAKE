/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VveMultiBlockIO.CPP          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VtkTopo                      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  VtkTopo                        Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/

#include "VveSerializableVtkData.h"

#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>

#include <vtkDataSet.h>
#include <vtkCharArray.h>
#include <vtkDataSetReader.h>
#include <vtkDataSetWriter.h>

#include <string>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;

const string VveSerializableVtkData::m_strSignature("VveSerializableVtkData");
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveSerializableVtkData::VveSerializableVtkData(){}
VveSerializableVtkData::~VveSerializableVtkData()
{
	/* @todo CHECK memory management here
    this->Clear();
    */
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Serialize                                                   */
/*                                                                            */
/*============================================================================*/
int VveSerializableVtkData::Serialize(IVistaSerializer & rSerializer) const
{
	int iNumBytesWritten = 0;
	iNumBytesWritten += rSerializer.WriteString(m_strSignature);
	iNumBytesWritten += rSerializer.WriteInt32(int(m_vecObjectsToBeSerialized.size()));
	for(unsigned int i=0; i<m_vecObjectsToBeSerialized.size(); ++i)
	{
		iNumBytesWritten += this->WriteVtkData(rSerializer, m_vecObjectsToBeSerialized[i]);
	}
	return iNumBytesWritten;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeSerialize                                                 */
/*                                                                            */
/*============================================================================*/
int VveSerializableVtkData::DeSerialize(IVistaDeSerializer & rDeSerializer)
{
	string strSig;
	int iNumBytesRead = 0;
	//read signature
	// @todo: Get rid of this cast.
	iNumBytesRead += rDeSerializer.ReadString(strSig,
		static_cast<int>(m_strSignature.length()));
	//check signature
	if(strSig == m_strSignature)
	{
		int iNumElems = 0;
		int iLen = 0;
		//read number of elements
		iNumBytesRead += rDeSerializer.ReadInt32(iNumElems);
		m_vecObjectsToBeSerialized.reserve(iNumElems+1);
		vtkDataSet* pData;
		//read single elements
		for(int i=0; i<iNumElems; ++i)
		{
			iLen = this->ReadVtkData(rDeSerializer, pData);
			//check for error
			if(iLen > -1)
			{
				m_vecObjectsToBeSerialized.push_back(pData);
				iNumBytesRead+=iLen;
			}
			else
			{
				return -1;
			}
		}
	}
	return iNumBytesRead;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetSignature                                                */
/*                                                                            */
/*============================================================================*/
string VveSerializableVtkData::GetSignature() const
{
	return string(m_strSignature);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddDataObject                                               */
/*                                                                            */
/*============================================================================*/
void VveSerializableVtkData::AddDataObject(vtkDataSet* pDataSet)
{
	/* @todo CHECK memory management here
    pDataSet->Register(NULL);
    */
	m_vecObjectsToBeSerialized.push_back(pDataSet);
}	
/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemoveDataObject                                            */
/*                                                                            */
/*============================================================================*/
bool VveSerializableVtkData::RemoveDataObject(vtkDataSet* pDataSet)
{
	vector<vtkDataSet*>::iterator vIter = m_vecObjectsToBeSerialized.begin();
	vector<vtkDataSet*>::iterator vEnd  = m_vecObjectsToBeSerialized.end();
	while(vIter!=vEnd)
	{
		if(pDataSet == *vIter)
		{
			/* @todo CHECK memory management here
            pDataSet->UnRegister(NULL);
            */
			m_vecObjectsToBeSerialized.erase(vIter);
            return true;
		}
		++vIter;
	}
	return false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemoveDataObject                                            */
/*                                                                            */
/*============================================================================*/
bool VveSerializableVtkData::RemoveDataObject(int i)
{
	vtkDataSet* pDataSet = m_vecObjectsToBeSerialized[i];
	return this->RemoveDataObject(pDataSet);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDataObject                                               */
/*                                                                            */
/*============================================================================*/
vtkDataSet* VveSerializableVtkData::GetDataObject(int i) const
{
	if(i>=0 && i<int(m_vecObjectsToBeSerialized.size()))
        return m_vecObjectsToBeSerialized[i];
    return NULL;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetNumDataObjects                                           */
/*                                                                            */
/*============================================================================*/
size_t VveSerializableVtkData::GetNumDataObjects() const
{
	return m_vecObjectsToBeSerialized.size();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Clear				                                          */
/*                                                                            */
/*============================================================================*/
void VveSerializableVtkData::Clear()
{
	//free vtk resources...
	/* @todo CHECK memory management here
    for(int i=0; i<m_vecObjectsToBeSerialized.size(); ++i)
		m_vecObjectsToBeSerialized[i]->UnRegister(NULL);
    */
	m_vecObjectsToBeSerialized.clear();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ReadVtkData	                                              */
/*                                                                            */
/*============================================================================*/
int VveSerializableVtkData::ReadVtkData(IVistaDeSerializer& rDeSerializer, vtkDataSet*& pData)
{
	int iLen = -1;
	int iNumBytesRead = 0;
	iNumBytesRead = rDeSerializer.ReadInt32(iLen);
	if( iLen>0 )
	{
		vtkCharArray* pCharArray = vtkCharArray::New();
		pCharArray->SetNumberOfComponents(1);
		pCharArray->SetNumberOfTuples(iLen);
		//read char stream from input
		//char* pCodeString = new char[iLen];
		iNumBytesRead += rDeSerializer.ReadRawBuffer(pCharArray->GetVoidPointer(0), iLen);
		//put raw data into vtk structure
		//NOTE: the indirection via vtkDataArray circumvents any further copy operations
		//pCharArray->SetArray(pCodeString,iLen,1);
		//perform vtk read
		vtkDataSetReader* pReader = vtkDataSetReader::New();
		pReader->ReadFromInputStringOn();
		pReader->SetInputArray(pCharArray);
		pReader->Update();
		//configure output object
		pData = pReader->GetOutput()->NewInstance();
		pData->ShallowCopy(pReader->GetOutput());
		//cleanup 
		pReader->Delete();
		pCharArray->Delete();
		pData->Squeeze();
		return iNumBytesRead;
	}
	return -1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   WriteVtkData                                                */
/*                                                                            */
/*============================================================================*/
int VveSerializableVtkData::WriteVtkData(IVistaSerializer& rSerializer, vtkDataSet* pData) const
{
	//write poly data to byte stream
	vtkDataSetWriter* pWriter = vtkDataSetWriter::New();
	pWriter->SetFileTypeToBinary();
	pWriter->WriteToOutputStringOn();

#if VTK_MAJOR_VERSION > 5
	pWriter->SetInputData(pData);
#else
	pWriter->SetInput(pData);
#endif
	pWriter->Update();
	//copy byte stream to underlying serializer
	int iNumBytes = rSerializer.WriteInt32(pWriter->GetOutputStringLength()); //write size
	iNumBytes +=	rSerializer.WriteRawBuffer(pWriter->GetOutputString(), pWriter->GetOutputStringLength()); //write buffer
	//cleanup
	pWriter->Delete();
	return iNumBytes;
}


/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveSerializableVtkData.cpp"                                     */
/*============================================================================*/
