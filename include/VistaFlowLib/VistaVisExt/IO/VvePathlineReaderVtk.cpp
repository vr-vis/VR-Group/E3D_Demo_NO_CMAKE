/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VvePathlineReaderVtk.h"
#include <cassert>
#include <iostream>
#include <cmath>
#include <vtkByteSwap.h>

#include <vtkPolyDataReader.h>
#include <vtkPolyData.h>
#include <vtkCell.h>
#include <vtkPointData.h>

#include <VistaTools/VistaProgressBar.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

const char* const SPINNER = "-\\|/";
#ifdef WIN32
#include <float.h>
#define isnan(x) _isnan(x)
#endif

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VvePathlineReaderVtk::VvePathlineReaderVtk(const std::string & strFilename, float fStartTime, float fDeltaTime)
: m_strFilename(strFilename), m_fStartTime(fStartTime), m_fDeltaTime(fDeltaTime), m_bUseExtensions(false),
  m_sPositionArray(VveParticlePopulation::sPositionArrayDefault),
  m_sVelocityArray(VveParticlePopulation::sVelocityArrayDefault),
  m_sScalarArray(VveParticlePopulation::sScalarArrayDefault),
  m_sRadiusArray(VveParticlePopulation::sRadiusArrayDefault),
  m_sTimeArray(VveParticlePopulation::sTimeArrayDefault),
  m_sEigenValueArray(VveParticlePopulation::sEigenValueArrayDefault),
  m_sEigenVectorArray(VveParticlePopulation::sEigenVectorArrayDefault)
{
}

VvePathlineReaderVtk::VvePathlineReaderVtk(const std::string & strFilename, std::string strTimeFieldName)
: m_strFilename(strFilename), m_strTimeFieldName(strTimeFieldName), m_bUseExtensions(false),
  m_fStartTime(0.0f), m_fDeltaTime(0.0f),
  m_sPositionArray(VveParticlePopulation::sPositionArrayDefault),
  m_sVelocityArray(VveParticlePopulation::sVelocityArrayDefault),
  m_sScalarArray(VveParticlePopulation::sScalarArrayDefault),
  m_sRadiusArray(VveParticlePopulation::sRadiusArrayDefault),
  m_sTimeArray(VveParticlePopulation::sTimeArrayDefault),
  m_sEigenValueArray(VveParticlePopulation::sEigenValueArrayDefault),
  m_sEigenVectorArray(VveParticlePopulation::sEigenVectorArrayDefault)
{
}

VvePathlineReaderVtk::~VvePathlineReaderVtk()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetParticleExtention                                        */
/*                                                                            */
/*============================================================================*/
void VvePathlineReaderVtk::SetParticleExtention( const VistaPropertyList &refPropsParticleExt)
{
	m_oPropsParticleExt = refPropsParticleExt;
    m_bUseExtensions = true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Load                                                        */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderVtk::Load( VveParticlePopulation *pPopulation, 
                          const std::string &strScalar, 
                          const std::string &strRadius,
                          const std::string &strTsField)
{
    if (!pPopulation)
    {
        vstr::errp() << " [VvePathlineReaderVtk] no particle "
			 << "population to be filled..." << endl;
        return false;
    }

    if (m_strFilename.empty())
    {
        vstr::errp() << " [VvePathlineReaderVtk] no file name given..." << endl;
        return false;
    }

    // check, whether file is available...
    ifstream is(m_strFilename.c_str());
    if (!is.is_open())
    {
        vstr::errp() << " [VflPathlineReaderVtk] unable to find particle file '" << m_strFilename << "'..." << endl;
        return false;
    }
    is.close();

    vtkPolyDataReader *pReader = vtkPolyDataReader::New();
    pReader->SetFileName(m_strFilename.c_str());

    //if (!strScalar.empty())                            // don't filter it here, but after we read the whole vtk data
    //{													 // so we still have a chance to get other important scalar field, i.e. time
    //    pReader->SetScalarsName(strScalar.c_str());
    //}

#ifndef DEBUG
    pReader->DebugOff();
#endif
    pReader->Update();

    vtkPolyData *pData = pReader->GetOutput();
    if (!pData)
    {
        vstr::errp() << " [VvePathlineReaderVtk] unable to read particle data..." << endl;
        return false;
    }

    int iCellCount = pData->GetNumberOfCells();

    vtkDataArray *pVectors = pData->GetPointData()->GetVectors();
        
    vtkDataArray *pScalars = NULL;
	if(!strScalar.empty())
    {
		pScalars = pData->GetPointData()->GetScalars(strScalar.c_str());
        // use given scalar array name for the particle data array
        SetScalarArray(strScalar);
    }
	else
    {
		pScalars = pData->GetPointData()->GetScalars();
    }

    
    vtkDataArray *pTimeStamps = NULL;
	if(!m_strTimeFieldName.empty())
	{
		pTimeStamps = pData->GetPointData()->GetScalars(m_strTimeFieldName.c_str());
        // use given time array name for the particle data array
        SetTimeArray(m_strTimeFieldName);
    }

    // local time parameter overrides member variable
    if(!strTsField.empty())
    {
        pTimeStamps = pData->GetPointData()->GetScalars( strTsField.c_str() );
        SetTimeArray(strTsField);
    }

		if(!pTimeStamps)
		{
			vstr::errp() << " [VflPathlineReaderVtk] unable to read time scalar field..." << endl;
			return false;
		}

	// prepare data of particle extentions
	vtkDataArray **pParticleExtData = NULL;
    if( m_bUseExtensions )
	{
        std::list<std::string> liExtFieldNames;
		m_oPropsParticleExt.GetValue( "PARTICLEEXT_FIELDNAMES", liExtFieldNames );
		pParticleExtData = new vtkDataArray*[liExtFieldNames.size()];

		int iExtId = 0;
        for(std::list<std::string>::iterator it=liExtFieldNames.begin(); it!=liExtFieldNames.end(); ++it )
	        pParticleExtData[iExtId++] = pData->GetPointData()->GetScalars((*it).c_str());
	}


#ifdef DEBUG
    vstr::debugi() << " [VvePathlineReaderVtk] - number of pathlines: " << iCellCount << endl;
#endif

    if (iCellCount)
    {
        // we do have cells, so interpret them as pathlines...

        // re-allocate trajectory data
    
        //pPopulation->vecPopulation.resize(iCellCount);

        VistaProgressBar oBar(iCellCount);
        oBar.SetPrefixString("                   ");
        oBar.SetBarTicks(30);
        oBar.Start();

        for (int i=0; i<iCellCount; ++i)
        {
            //SVveParticleTrajectory &refT = pPopulation->vecPopulation[i];
            vtkCell *pCell = pData->GetCell(i);
            int iPointCount = pCell->GetNumberOfPoints();

            // create particle data arrays and add to a new particle trajectory
            VveParticleTrajectory* pTraj = new VveParticleTrajectory();

            VveParticleDataArray<float> *aPos = new VveParticleDataArray<float>(3, m_sPositionArray);
            pTraj->AddArray(aPos);
            VveParticleDataArray<float> *aVel = new VveParticleDataArray<float>(3, m_sVelocityArray);
            pTraj->AddArray(aVel);
            VveParticleDataArray<float> *aTime = new VveParticleDataArray<float>(1, m_sTimeArray);
            pTraj->AddArray(aTime);
            VveParticleDataArray<float> *aScalar = new VveParticleDataArray<float>(1, m_sScalarArray);
            pTraj->AddArray(aScalar);
            VveParticleDataArray<float> *aRadius = new VveParticleDataArray<float>(1, m_sRadiusArray);
            pTraj->AddArray(aRadius);

			VveParticleDataArray<float> *aEVal = 0;
			VveParticleDataArray<float> *aEVec = 0;
			if (m_bUseExtensions)
			{
				aEVal = new VveParticleDataArray<float>(3, m_sEigenValueArray);
				pTraj->AddArray(aEVal);
				aEVec = new VveParticleDataArray<float>(9, m_sEigenVectorArray);
				pTraj->AddArray(aEVec);
			}

            // Resize arrays
            pTraj->Resize(iPointCount);

			float fTime = -m_fDeltaTime;			
            double aTemp[3];

			for (int j=0; j<iPointCount; ++j)
			{
				int iPointId = pCell->GetPointId(j);

				// get timing information
				if(pTimeStamps)
					fTime = (float) pTimeStamps->GetTuple1(iPointId);
				else
					fTime += m_fDeltaTime;

                aTime->SetElementValue(j, fTime);

				pData->GetPoint(iPointId, aTemp);
                aPos->SetElement(j, aTemp);

				if (pVectors)
				{
					pVectors->GetTuple(iPointId, aTemp);
                    aVel->SetElement(j, aTemp);
				}
				else
                {
                    aVel->SetElementValue(j, 0.0f);
                }

				if (pScalars)
                {
                    aRadius->SetElementValue(j, pScalars->GetComponent(iPointId, 0));
                    aScalar->SetElementValue(j, pScalars->GetComponent(iPointId, 0));
                }
				else
				{
                    aRadius->SetElementValue(j, 1.0f);
                    aScalar->SetElementValue(j, 0.0f);
				}

				if (m_bUseExtensions)
				{
					// load particle extension: Eigenvalues
					aTemp[0] = static_cast<float>(pParticleExtData[0]->GetComponent(iPointId,0));
					aTemp[1] = static_cast<float>(pParticleExtData[1]->GetComponent(iPointId,0));
					aTemp[2] = static_cast<float>(pParticleExtData[2]->GetComponent(iPointId,0));
					aEVal->SetElement(j, aTemp);

					// load particle extension: Eigenvectors
					float aEV[9];
					for (int k=0; k<3; k++)
					{
						pParticleExtData[3+k]->GetTuple(iPointId, aTemp);
						aEV[3*k+0] = static_cast<float>( aTemp[0] );
						aEV[3*k+1] = static_cast<float>( aTemp[1] );
						aEV[3*k+2] = static_cast<float>( aTemp[2] );
					}
					aEVec->SetElement(j, aEV);
				}

    		}

            // add initialized trajectory to the population
            pPopulation->AddTrajectory(pTraj);
			oBar.Increment();
        }
        oBar.Finish();
    }
    else
    {
        // we don't have cells, so interpret the points as particle instants...
        iCellCount = pData->GetNumberOfPoints();

#ifdef DEBUG
        vstr::debugi() << " [VvePathlineReaderVtk] - interpreting point data as particle positions..." << endl;
        vstr::debugi() << "                   number of cells / particles: " << iCellCount << endl;
#endif

        VistaProgressBar oBar(iCellCount);
        oBar.SetPrefixString("                   ");
        oBar.SetBarTicks(30);
        oBar.Start();
        double aTemp[3];
        for (int i=0; i<iCellCount; ++i)
        {
            // create particle data arrays and add to a new particle trajectory
            VveParticleTrajectory* pTraj = new VveParticleTrajectory();

            VveParticleDataArray<float> *aPos = new VveParticleDataArray<float>(3, m_sPositionArray);
            pTraj->AddArray(aPos);
            VveParticleDataArray<float> *aVel = new VveParticleDataArray<float>(3, m_sVelocityArray);
            pTraj->AddArray(aVel);
            VveParticleDataArray<float> *aTime = new VveParticleDataArray<float>(1, m_sTimeArray);
            pTraj->AddArray(aTime);
            VveParticleDataArray<float> *aScalar = new VveParticleDataArray<float>(1, m_sScalarArray);
            pTraj->AddArray(aScalar);
            VveParticleDataArray<float> *aRadius = new VveParticleDataArray<float>(1, m_sRadiusArray);
            pTraj->AddArray(aRadius);

			VveParticleDataArray<float> *aEVal = NULL;
			VveParticleDataArray<float> *aEVec = NULL;
			if( m_bUseExtensions )
			{
				aEVal = new VveParticleDataArray<float>(3, m_sEigenValueArray);
				pTraj->AddArray(aEVal);
				aEVec = new VveParticleDataArray<float>(9, m_sEigenVectorArray);
				pTraj->AddArray(aEVec);
			}

			// Resize arrays to hold a single position
			pTraj->Resize(1);

            pData->GetPoint(i, aTemp);
            aPos->SetElement(0, aTemp);

            if (pVectors)
            {
                pVectors->GetTuple(i, aTemp);
                aVel->SetElement(0, aTemp);
            }
            else
            {
                aVel->SetElementValue(0,0.0f);
            }

            if (pScalars)
            {
                aRadius->SetElementValue(0, pScalars->GetComponent(i, 0));
                aScalar->SetElementValue(0, pScalars->GetComponent(i, 0));
            }
            else
            {
                aRadius->SetElementValue(0, 1.0f);
                aScalar->SetElementValue(0, 0.0f);
            }

            aTime->SetElementValue(0, m_fStartTime);


			if( m_bUseExtensions )
			{
				// load particle extension: Eigenvalues
				aTemp[0] = static_cast<float>(pParticleExtData[0]->GetComponent(i,0));
				aTemp[1] = static_cast<float>(pParticleExtData[1]->GetComponent(i,0));
				aTemp[2] = static_cast<float>(pParticleExtData[2]->GetComponent(i,0));
				aEVal->SetElement(0, aTemp);

				// load particle extension: Eigenvectors
				float aEV[9];
				for (int k=0; k<3; k++)
				{
					pParticleExtData[3+k]->GetTuple(i, aTemp);
					aEV[3*k+0] = static_cast<float>( aTemp[0] );
					aEV[3*k+1] = static_cast<float>( aTemp[1] );
					aEV[3*k+2] = static_cast<float>( aTemp[2] );
				}
				aEVec->SetElement(0, aEV);
			}

            // add initialized trajectory to the population
            pPopulation->AddTrajectory(pTraj);

            oBar.Increment();
        }

        oBar.Finish();
    }

    pReader->Delete();

    return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPositionArray                                            */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderVtk::SetPositionArray( std::string sPositionArray )
{
    if(compAndAssignFunc<std::string>(sPositionArray, m_sPositionArray))
    {
    return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetVelocityArray                                            */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderVtk::SetVelocityArray( std::string sVelocityArray )
{
    if(compAndAssignFunc<std::string>(sVelocityArray, m_sVelocityArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRadiusArray                                              */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderVtk::SetRadiusArray( std::string sRadiusArray )
{
    if(compAndAssignFunc<std::string>(sRadiusArray, m_sRadiusArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetScalarArray                                              */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderVtk::SetScalarArray( std::string sScalarArray )
{
    if(compAndAssignFunc<std::string>(sScalarArray, m_sScalarArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTimeArray                                                */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderVtk::SetTimeArray( std::string sTimeArray )
{
    if(compAndAssignFunc<std::string>(sTimeArray, m_sTimeArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetEigenValueArray                                          */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderVtk::SetEigenValueArray( std::string sEigenValueArray )
{
    if(compAndAssignFunc<std::string>(sEigenValueArray, m_sEigenValueArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetEigenVectorArray                                         */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderVtk::SetEigenVectorArray( std::string sEigenVectorArray )
{
    if(compAndAssignFunc<std::string>(sEigenVectorArray, m_sEigenVectorArray))
    {
        return true;
    }
    return false;}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
