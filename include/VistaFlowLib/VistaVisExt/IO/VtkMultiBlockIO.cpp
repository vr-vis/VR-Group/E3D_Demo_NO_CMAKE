/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName : VtkVveMultiBlockIO.CPP        RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaVisExt                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : Vista                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/

#include <VistaVisExt/IO/VtkMultiBlockIO.h>

#include <vtkDataSet.h>
#include <vtkUnstructuredGrid.h>
#include <vtkStructuredGrid.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkDataSetReader.h>
#include <vtkDataSetWriter.h>

#include <VistaBase/VistaStreamUtils.h>

#include <string>
#include <iostream>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VtkMultiBlockIO::VtkMultiBlockIO()
	: m_bUseBinaryMode(true)
{
}

VtkMultiBlockIO::~VtkMultiBlockIO()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadSingleBlock                                             */
/*                                                                            */
/*============================================================================*/
bool VtkMultiBlockIO::LoadSingleBlock (int iTimeLevel, int iBlockNum,
                                        vtkDataSet*& pDataSet, float& fSimTime)
{
	//init reader
	vtkDataSetReader* pReader = vtkDataSetReader::New();
	pReader->SetFileName(
        this->GetDataSetName()->GetFileName(iTimeLevel,iBlockNum).c_str());

	//try to open VTK-file
	if(!pReader->OpenVTKFile())
	{
		vstr::errp() <<"[VtkVveMultiBlockIO::ReadSingleBlock] Error opening file > "<<pReader->GetFileName()<<endl;
		pReader->CloseVTKFile();
		pReader->Delete();
		return false;
	}
	pReader->CloseVTKFile();
	string strScalarName = this->GetScalarFieldName();
	//set scalar field to read
	if(!strScalarName.empty())
		pReader->SetScalarsName(strScalarName.c_str());
	//update reader -> actually execute loading
    pReader->Update();
    //set dataset pointer accordingly 
    pDataSet = pReader->GetOutput();
	if(!pDataSet)
	{
		pReader->Delete();
		vstr::errp()<<"[VtkVveMultiBlockIO::LoadSingleBlock] Invalid reader output! "<<endl;
		return false;
	}
	//use vtk's reference counting to circumvent copying
	pDataSet->Register(NULL); 
	//@TODO Is simply removing the next line ok to resolve vtk6.x issues?
	//		Nothing found about this in the migration guide.
	//pDataSet->SetSource(NULL);
	//delete reader (this will dec ref counter of reader output by 1)
	//NOTE: ref counting of reader's output should be == 1 now, i.e. output will not be deleted an can be processed further
	pReader->Delete(); 
	//hard set the ref count to 1 to ensure proper destruction
	pDataSet->SetReferenceCount(1);
	pDataSet->Squeeze();
	
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   WriteSingleBlock                                            */
/*                                                                            */
/*============================================================================*/
bool VtkMultiBlockIO::WriteSingleBlock(int iTimeLevel, int iBlockNum, vtkDataSet* pDataSet, float fSimTime)
{
	/*
	*	NOTE:	There might well be a problem here: Since we don't give any concrete writer
	*			it may well be that the data gets written into some generically formatted 
	*			file and that any type information is lost after another reload.
	*			I HAVE TO CHECK ON THIS AS SOON AS WRITING TO DISK GETS SERIOUS! (av006he)
	*/
	//init writer
	vtkDataSetWriter* pWriter = vtkDataSetWriter::New();

#if VTK_MAJOR_VERSION > 5
	pWriter->SetInputData(pDataSet);
#else
	pWriter->SetInput(pDataSet);
#endif
	pWriter->SetFileName(this->GetDataSetName()->GetFileName().c_str());
	if(m_bUseBinaryMode)
		pWriter->SetFileTypeToBinary();
	else
		pWriter->SetFileTypeToASCII();
	//set scalar field to read
	string strScalarName = this->GetScalarFieldName();
	if(strScalarName != "")
		pWriter->SetScalarsName(strScalarName.c_str());
	//update writer -> actually execute writing
	pWriter->Update();
	//cleanup
	pWriter->Delete(); 
	return true;
}
/*============================================================================*/
/*  END OF FILE "VtkVveMultiBlockIO.cpp"                               */
/*============================================================================*/
