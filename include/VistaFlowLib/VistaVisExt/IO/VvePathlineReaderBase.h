/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEPATHLINEREADER_BASE_H
#define _VVEPATHLINEREADER_BASE_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <string>
#include <list>

#include <VistaVisExt/VistaVisExtConfig.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VvePathlineReaderBase: load the contents of a different file formats 
 * (in ascii or binary format) into a given particle population object.
 */    
class VISTAVISEXTAPI VvePathlineReaderBase
{
public:
    enum SCALAR
    {
        VELOCITY,
        COUNT,
        RADIUS,
        MASS,
        TEMPERATURE
    };

    enum TYPE
    {
        UNKNOWN,
        PARTICLES,
        DROPLETS
    };

public:

    /**
     * Load the particle data into the given population. The parameters
     * eScalar and eRadius determine, which scalar value is to be loaded
     * into the scalar and radius entries of the particles.
     * Once loaded, the data is cached until this loader is destroyed or
     * EmptyCache() is called.
     */
    bool Load( VveParticlePopulation *pPopulation, VvePathlineReaderBase::SCALAR eScalar, VvePathlineReaderBase::SCALAR eRadius);

    /**
     * Fill the data cache, if it's empty.
     * In conjunction with CopyDataToPopulation(), it allows for a two-phase loading process,
     * thus minimizing blocking time for the graphical output.
     */
    virtual bool LoadDataFromFile() = 0;

    /** 
     * Copy the contents of the data cache into the given population. The parameters
     * eScalar and eRadius determine, which scalar value is to be loaded 
     * into the scalar and radius entries of the particles.
     * In conjunction with LoadDataFromFile(), it allows for a two-phase loading process,
     * thus minimizing blocking time for the graphical output.
     */
    bool CopyCacheToPopulation( VveParticlePopulation *pPopulation, SCALAR eScalar, SCALAR eRadius) const;

    /**
     * Empty the data cache, i.e. deallocate all resources.
     */
    void EmptyCache();

    /**
     * Find out about data type, i.e. whether we're talking about
     * particle or droplet data.
     */
    TYPE GetType();


     /** 
     * Setters for the names of the particle data arrays
     */    
    bool SetPositionArray(std::string sPositionArray);
    bool SetVelocityArray(std::string sVelocityArray);
    bool SetRadiusArray(std::string sRadiusArray);
    bool SetScalarArray(std::string sScalarArray);
    bool SetTimeArray(std::string sTimeArray);

protected:

    // CONSTRUCTORS / DESTRUCTOR
    VvePathlineReaderBase(std::string strFilename);
    virtual ~VvePathlineReaderBase();

    // temporary particle data structure
    struct STemporaryParticleData
    {
        int        iIndex;
        float    aPos[3];
        float    aVel[3];
        int        iType;
        float   fTime;
        int        iCell;
        float    fTemperature;
        float    fDiameter;
        float    fMass;
        float    fCount;
        int        padding[2];
    };

    std::string m_strFilename;
    bool m_bBinary;
    TYPE m_eType;
    std::list<STemporaryParticleData> m_liData;

    std::vector<int> m_vecTimeLevelCounts;

	float      m_fScale;

    // Particle data array names ( + default in c'tor)
    std::string m_sPositionArray; /* = "positions" */
    std::string m_sVelocityArray; /* = "velocities" */
    std::string m_sScalarArray;   /* = "scalars" */
    std::string m_sRadiusArray;   /* = "radius" */
    std::string m_sTimeArray;     /* = "sim_time" */
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLPATHLINEREADERTRK_H)
