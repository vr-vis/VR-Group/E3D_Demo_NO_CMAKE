/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VvePathlineReaderTrk.h"
#include <cassert>
#include <iostream>
#include <cmath>
#include <vtkByteSwap.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>

#include <VistaBase/VistaStreamUtils.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

static const char* SPINNER = "-\\|/";

#ifdef WIN32
#include <float.h>
#define isnan(x) _isnan(x)
#endif

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VvePathlineReaderTrk::VvePathlineReaderTrk(std::string strFilename, bool bBinary, float fScale)
: VvePathlineReaderBase(strFilename),
  m_iBinaryDelimiter(0), m_iBinaryDataSize(0)
{
    m_bBinary = bBinary;
    m_fScale = fScale;

    if (m_eType==UNKNOWN)
    {
        ifstream is;

        if (m_bBinary)
        {
            is.open(m_strFilename.c_str(), ios::in | ios::binary);
        }
        else
        {
            is.open(m_strFilename.c_str());
        }

        if (is.bad() || !is.is_open())
        {
           vstr::errp() << " [VflPathlineReaderTrk] unable to open particle data file..." << endl;
            vstr::erri() << "                 file name: " << m_strFilename << endl;
        }

        char buf[256];
        int iTemp;
        string strTemp;

        if (m_bBinary)
        {
            // binary version

            // skip whitespace
            is.read(buf, sizeof(int));

            is.read(buf, 4*sizeof(char));
            if (strncmp(buf, "TRPT", 4) && strncmp(buf, "SSPT", 4))
            {
                vstr::errp() << " [VflPathlineReaderTrk] wrong file format..." << endl;
                vstr::erri() << "                 file name: " << m_strFilename << endl;
                is.close();
            }

            is.read((char *)&iTemp, sizeof(int));
            vtkByteSwap::Swap4BE((char *) &iTemp);
            vstr::outi() << " [VflPathlineReaderTrk] - found version " << iTemp << " particle file..." << endl;
            vstr::outi() << "                   format: binary" << endl;

            // read in 4 bytes determining the file type (droplet or particle)
            is.read(buf, 4);
            if (!strncmp(buf, "DROP", 4))
            {
                vstr::outi() << "                   file type: droplet file" << endl;
                m_eType = DROPLETS;

                // skip some floats and padding
                is.read(buf, 52);
            }
            else
            {
                vstr::outi() << "                   file type: particle file" << endl;
                m_eType = PARTICLES;
            }
        }
        else
        {
            // ascii version

            // read in file header
            is >> strTemp;
            if (strTemp != "TRPT" && strTemp != "SSPT")
            {
                vstr::errp() << " [VflPathlineReaderTrk] wrong file format..." << endl;
                vstr::erri() << "                 file name: " << m_strFilename << endl;
                is.close();
            }

            is >> strTemp;
            vstr::outi() << " [VflPathlineReaderTrk] - found version " << strTemp << " particle file..." << endl;
            vstr::outi() << "                   format: ascii" << endl;

            // determine the file type (droplet or particle)
            is >> strTemp;
            if (strTemp == "DROP")
            {
                vstr::outi() << "                   file type: droplet file" << endl;
                m_eType = DROPLETS;
            }
            else
            {
                vstr::outi() << "                   file type: particle file" << endl;
                m_eType = PARTICLES;
            }
        }
        is.close();
    }
}

VvePathlineReaderTrk::~VvePathlineReaderTrk()
{
    EmptyCache();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadDataFromFile                                            */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderTrk::LoadDataFromFile()
{
    if (!m_liData.empty())
    {
		vstr::warnp() << " [VflPathlineReaderTrk] skipping file load due to full cache..." << endl;
        return false;
    }

    ifstream is;

    if (m_bBinary)
    {
        is.open(m_strFilename.c_str(), ios::in | ios::binary);
    }
    else
    {
        is.open(m_strFilename.c_str(), ios::in);
    }

    if (is.bad() || !is.is_open())
    {
        vstr::errp() << " [VflPathlineReaderTrk] unable to open particle data file..." << endl;
        vstr::erri() << "                 file name: " << m_strFilename << endl;
        return false;
    }

    char buf[256];
    int iTemp=0, i;
    float fTemp;
    string strTemp;

    if (m_bBinary)
    {
        // binary version

        // skip whitespace
        is.read(buf, sizeof(int));

        is.read(buf, 4*sizeof(char));
        if (strncmp(buf, "TRPT", 4) && strncmp(buf, "SSPT", 4))
        {
            vstr::errp() << " [VflPathlineReaderTrk] wrong file format..." << endl;
            vstr::erri() << "                 file name: " << m_strFilename << endl;
            is.close();
            return false;
        }

        is.read((char *)&iTemp, sizeof(int));
        vtkByteSwap::Swap4BE((char *) &iTemp);
        vstr::outi() << " [VflPathlineReaderTrk] - found version " << iTemp << " particle file..." << endl;
        vstr::outi() << "                   format: binary" << endl;

        // read in 4 bytes determining the file type (droplet or particle)
        is.read(buf, 4);
        if (!strncmp(buf, "DROP", 4))
        {
            vstr::outi() << "                   file type: droplet file" << endl;
            m_eType = DROPLETS;

            // skip some floats and padding
            is.read(buf, 52);
        }
        else
        {
            vstr::outi() << "                   file type: particle file" << endl;
            m_eType = PARTICLES;

            // skip some floats and padding
            is.read(buf, 64);

            // find out about padding quadruple
            is.read((char *)&m_iBinaryDelimiter, sizeof(int));

#ifdef DEBUG
            // analyze delimiter...
            iTemp = m_iBinaryDelimiter;
            vtkByteSwap::Swap4BE((char *)&iTemp);
            vstr::debugi() << "                 delimiter: " << m_iBinaryDelimiter 
                << " (swapped: " << iTemp << ")" << endl;
#endif

            // find double delimiter
            bool bDelimiterFound = false;
            while (!is.eof())
            {
                is.read((char *)&iTemp, sizeof(int));
                ++m_iBinaryDataSize;

                if (iTemp == m_iBinaryDelimiter)
                {
                    if (bDelimiterFound)
                    {
                        break;
                    }
                    else
                    {
                        bDelimiterFound = true;
                    }
                }
                else
                    bDelimiterFound = false;
            }

            // now find first character, which doesn't equal the delimiter...
            while (!is.eof())
            {
                is.read((char *)&iTemp, sizeof(int));

                if (iTemp != m_iBinaryDelimiter)
                {
                    break;
                }
                ++m_iBinaryDataSize;
            }

            if (is.eof())
            {
                vstr::errp() << " [VflPathlineReaderTrk] unable to find data delimiter..." << endl;
                vstr::erri() << "                 file name: " << m_strFilename << endl;
                is.close();
                return false;
            }

            // reset input stream and go to beginning of particle data
            is.close();
            is.open(m_strFilename.c_str(), ios::in | ios::binary);
            is.read(buf, 84);
        }
    }
    else
    {
        // ascii version

        // read in file header
        is >> strTemp;
        if (strTemp != "TRPT" && strTemp != "SSPT")
        {
            vstr::errp() << " [VflPathlineReaderTrk] wrong file format..." << endl;
			vstr::erri() << "                 file name: " << m_strFilename << endl;
            is.close();
            return false;
        }

        is >> strTemp;
		vstr::outi() << " [VflPathlineReaderTrk] - found version " << strTemp << " particle file..." << endl;
        vstr::outi() << "                   format: ascii" << endl;

        // skip a whitespace
        is.read(buf, 1);

        // read in 4 bytes determining the file type (droplet or particle)
        is.read(buf, 4);
        if (!strncmp(buf, "DROP", 4))
        {
            vstr::outi() << "                   file type: droplet file" << endl;
            m_eType = DROPLETS;
        }
        else
        {
            vstr::outi() << "                   file type: particle file" << endl;
            m_eType = PARTICLES;
        }

        // skip 11 floats (including the scale factor...)
        for (i=0; i<11; ++i)
            is >> fTemp;
    }

    vstr::outi() << " [VflPathlineReaderTrk] - starting data import...";
    vstr::outi().flush();

    STemporaryParticleData sTPD;
    sTPD.iIndex = 0;    // avoid processing of the first, empty structure...

    int iDataCount = 0;
    int iSpinnerCount = 0;
    int iNumberOfIndices=0;

    float fLastTime = -1;

    iTemp = 0;

	float fScale = m_fScale;

    if (m_bBinary)
    {
        while (!is.eof() && is.good())
        {
            if ((++iDataCount)%10000 == 0)
            {
                vstr::outi() << SPINNER[(++iSpinnerCount)%4] << "\b";
                vstr::outi().flush();
            }

            if (iNumberOfIndices < sTPD.iIndex)
                iNumberOfIndices = sTPD.iIndex;

            // is the particle alive? (and skip the first (illegal) one...)
            if (sTPD.iIndex > 0)
            {
                // particle indices start with 1 in the data file
                --sTPD.iIndex;
                m_liData.push_back(sTPD);
            }

            // read next particle
            if (m_eType == DROPLETS)
            {
                is.read((char *)&sTPD, sizeof(STemporaryParticleData));
                vtkByteSwap::Swap4BERange((char *)&sTPD, sizeof(STemporaryParticleData)/4);
            }
            else
            {
                is.read((char*)&sTPD, m_iBinaryDataSize*4);
                vtkByteSwap::Swap4BERange((char *)&sTPD, m_iBinaryDataSize);
                sTPD.fTemperature = sTPD.fDiameter = sTPD.fMass = sTPD.fCount = 0;
            }

            // check for valid position and velocity
            if (isnan(sTPD.aPos[0]) || isnan(sTPD.aVel[0]))
                sTPD.iIndex = -sTPD.iIndex;
			else
			{
				sTPD.aPos[0] *= fScale;
				sTPD.aPos[1] *= fScale;
				sTPD.aPos[2] *= fScale;
			}
        }
    }
    else
    {
        while (!is.eof() && is.good())
        {
            if ((++iDataCount)%10000 == 0)
            {
                vstr::outi() << SPINNER[(++iSpinnerCount)%4] << "\b";
                vstr::outi().flush();
            }

            if (iNumberOfIndices < sTPD.iIndex)
                iNumberOfIndices = sTPD.iIndex;

            // is the particle alive? (and skip the first (illegal) one...)
            if (sTPD.iIndex > 0)
            {
                // particle indices start with 1 in the data file
                --sTPD.iIndex;
                m_liData.push_back(sTPD);
            }

            // read next particle
            is >> sTPD.iIndex;

            // work around non-numerical representation of "nan"
            if (sTPD.iIndex < 0)
            {
                // skip the rest of the data
                if (m_eType == DROPLETS)
                {
                    for (i=0; i<13; ++i)
                        is >> strTemp;
                }
                else
                {
                    for (i=0; i<9; ++i)
                        is >> strTemp;
                }
                continue;
            }

			is >> sTPD.aPos[0] >> sTPD.aPos[1] >> sTPD.aPos[2];
			sTPD.aPos[0] *= fScale;
			sTPD.aPos[1] *= fScale;
			sTPD.aPos[2] *= fScale;
			is >> sTPD.aVel[0] >> sTPD.aVel[1] >> sTPD.aVel[2];

            // skip cell type
            is >> strTemp;
            sTPD.iType = 0;

            is >> sTPD.fTime >> sTPD.iCell;

            if (m_eType == DROPLETS)
            {
                is >> sTPD.fTemperature >> sTPD.fDiameter >> sTPD.fMass >> sTPD.fCount;
            }
            else
            {
                sTPD.fTemperature = sTPD.fDiameter = sTPD.fMass = sTPD.fCount = 0;
            }
        }
    }
    vstr::outi() << " " << endl;

    if (!is.eof())
    {
        vstr::warnp() << " [VflPathlineReaderTrk] problems with data file..." << endl;
    }
    is.close();

    vstr::outi() << " [VflPathlineReaderTrk] - " << iDataCount << " particle positions read..." << endl;
    vstr::outi() << "                   number of particle indices: " << iNumberOfIndices << endl;

    // now, we try to find out, which particle indices are not used and collapse
    // the indices accordingly
	vstr::outi() << " [VflPathlineReaderTrk] - collapsing particle indices..." << endl;

    vector<int> vecIndexUsage;
    vecIndexUsage.resize(iNumberOfIndices);
    for (i=0; i<iNumberOfIndices; ++i)
        vecIndexUsage[i] = 0;

    // check, which indices are in use
    list<STemporaryParticleData>::iterator itTPD = m_liData.begin();
    while (itTPD != m_liData.end())
    {
        ++vecIndexUsage[itTPD->iIndex];
        ++itTPD;
    }

    // now, build a table, which associates a particles index before the collapse
    // with the new index after the collapse
    int iIndexCountAfterCollapse = 0;
    for (i=0; i<iNumberOfIndices; ++i)
    {
        if (vecIndexUsage[i]>0)
        {
            // this index is in use, so store its new compressed index
            vecIndexUsage[i] = iIndexCountAfterCollapse++;
        }
        else
        {
            // this is just for some error checking later on...
            vecIndexUsage[i] = -1;
        }
    }

    // collapse the particle indices by the help of the table built above
    int iNewIndex;
    itTPD = m_liData.begin();
    while (itTPD != m_liData.end())
    {
        iNewIndex = vecIndexUsage[itTPD->iIndex];

        if (iNewIndex < 0)
        {
            vstr::errp() << " [VflPathlineReaderTrk] something went terribly wrong during" << endl;
            vstr::erri() << "                 particle index collapse..." << endl;
            vstr::erri() << "                 This is pretty serious shouldn't happen at all - ever!" << endl;

            m_liData.clear();
            m_eType = UNKNOWN;
            return false;
        }
        itTPD->iIndex = iNewIndex;

        ++itTPD;
    }
    vstr::outi() << " [VflPathlineReaderTrk] - number of particle indices after collapse: " << iIndexCountAfterCollapse << endl;

    // finally, find out, how many time levels every particle has...
    // I know, we could have done that before, but it might be a little
    // easier to understand this way... ;-)
    vstr::outi() << " [VflPathlineReaderTrk] - counting particle time levels..." << endl;
    m_vecTimeLevelCounts.resize(iIndexCountAfterCollapse);
    for (i=0; i<iIndexCountAfterCollapse; ++i)
    {
        m_vecTimeLevelCounts[i] = 0;
    }

    itTPD = m_liData.begin();
    while (itTPD != m_liData.end())
    {
        ++m_vecTimeLevelCounts[itTPD->iIndex];
        ++itTPD;
    }

    return true;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
