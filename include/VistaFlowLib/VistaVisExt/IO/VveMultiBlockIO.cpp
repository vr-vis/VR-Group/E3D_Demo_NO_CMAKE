/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VveMultiBlockIO.CPP             RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VtkTopo                      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  VtkTopo                        Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/

#include "VveMultiBlockIO.h"

#include <vtkDataSet.h>

class VveMultiBlockTopoDef;
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveMultiBlockIO::VveMultiBlockIO()
	:	m_iMinTime(0),
		m_iMinBlock(0),
		m_iTimeStride(0),
		m_iBlockStride(0)
{
}

VveMultiBlockIO::~VveMultiBlockIO()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDataSetName                                              */
/*                                                                            */
/*============================================================================*/
void VveMultiBlockIO::SetDataSetName(VveDataSetName* pDSName)
{
	m_pDataSetName = pDSName;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetDataSetName                                              */
/*                                                                            */
/*============================================================================*/
VveDataSetName* VveMultiBlockIO::GetDataSetName()
{
	return m_pDataSetName;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTopologyFileName                                         */
/*                                                                            */
/*============================================================================*/
void VveMultiBlockIO::SetTopologyFileName(VveDataSetName* pTopoName)
{
	m_pTopologyName = pTopoName;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTopologyFileName                                         */
/*                                                                            */
/*============================================================================*/
VveDataSetName* VveMultiBlockIO::GetTopologyFileName()
{
	return m_pDataSetName;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDataExtents (1)                                          */
/*                                                                            */
/*============================================================================*/
void VveMultiBlockIO::SetDataExtents(int iMinTime,int iNumTimeLevels,int iMinBlock, int iNumBlocks,int iTimeStride /*=1*/,int iBlockStride /*=1*/)
{
	//copy data 
	m_iMinTime		= iMinTime;
	m_iMinBlock		= iMinBlock;
	
	//init std::vector for blocks and time levels
	register int i=0;
	m_vecTimeLevels.clear();
	m_vecTimeLevels.reserve(iNumTimeLevels);
	for(i=0; i<iNumTimeLevels; ++i)
		m_vecTimeLevels.push_back(iMinTime+i*iTimeStride);
	m_vecBlockIds.clear();
	m_vecBlockIds.reserve(iNumBlocks);
	for(i=0; i<iNumBlocks; ++i)
		m_vecBlockIds.push_back(iMinBlock+i*iBlockStride);	
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetDataExtents (2)                                          */
/*                                                                            */
/*============================================================================*/
void VveMultiBlockIO::SetDataExtents(std::vector<int> vecTimeLevels, std::vector<int> vecBlockIds)
{
	//copy std::vectors
	m_vecTimeLevels = vecTimeLevels;
	m_vecBlockIds = vecBlockIds;
	//init min time / num time levels
	m_iMinTime = vecTimeLevels[0];
	//init min block / num blocks
	m_iMinBlock = vecBlockIds[0];
	//set strides to 'save' values
	m_iTimeStride  = 1;
	m_iBlockStride = 1;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ReadAccess to data extents                                  */
/*                                                                            */
/*============================================================================*/
int VveMultiBlockIO::GetMinTime() const
{ 
	return m_iMinTime; 
}
int VveMultiBlockIO::GetMinBlock() const
{ 
	return m_iMinBlock; 
}
size_t VveMultiBlockIO::GetNumTimeLevels() const
{ 
	return m_vecTimeLevels.size(); 
}
size_t VveMultiBlockIO::GetNumBlocks() const
{ 
	return m_vecBlockIds.size(); 
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Access to ScalarFieldIdentifiers                            */
/*                                                                            */
/*============================================================================*/
void VveMultiBlockIO::SetScalarFieldName(std::string strScalarFieldName)
{ 
	m_strScalarFieldName = strScalarFieldName; 
}

std::string VveMultiBlockIO::GetScalarFieldName() const
{
	return m_strScalarFieldName; 
}

void VveMultiBlockIO::SetScalarFieldId(int iScalarFieldIdx)
{ 
	m_iScalarFieldId = iScalarFieldIdx; 
}

int VveMultiBlockIO::GetScalarFieldId() const
{ 
	return m_iScalarFieldId; 
}	
/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadFullDataSet                                             */
/*                                                                            */
/*============================================================================*/
bool VveMultiBlockIO::LoadFullDataSet(std::vector< std::vector<vtkDataSet*> >& vecUnsteadyData, std::vector<float>& vecSimTimes)
{
	//cleanup first
	vecUnsteadyData.clear();
	
	//reserve memory
	vecUnsteadyData.reserve(this->GetNumTimeLevels());
	vecSimTimes.reserve(this->GetNumTimeLevels());

	//local data
	std::vector<vtkDataSet*> vecCurrentTimeLevel;
	float fCurrentSimTime;
	bool bSuccess = true;
	
	//read time steps
	for(size_t i=0; i<this->GetNumTimeLevels(); ++i)
	{
		//NOTE: Use indirect access via m_vecTimeLevels to identify indices for time levels
		bSuccess = LoadTimeLevel(m_vecTimeLevels[i],vecCurrentTimeLevel, fCurrentSimTime);
		
		vecUnsteadyData.push_back(vecCurrentTimeLevel);
		vecSimTimes.push_back(fCurrentSimTime);
	}
	
	//if something went wrong -> free memory
	if(!bSuccess)
	{
		for(size_t i=0; i<vecUnsteadyData.size(); ++i)
			this->CleanUpData(vecUnsteadyData[i]);
		vecUnsteadyData.clear();
	}
	return bSuccess;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadTimeStep                                                */
/*                                                                            */
/*============================================================================*/
bool VveMultiBlockIO::LoadTimeLevel(int iTimeLevel, std::vector<vtkDataSet*>& vecDataSets, float& fSimTime)
{
	//cleanup first
	vecDataSets.clear();

	//reserve memory
	vecDataSets.reserve(this->GetNumBlocks());

	//local data
	vtkDataSet* pCurrentDataSet;
	bool bSuccess = true;
	
	//read single blocks
	for(size_t i=0; i<this->GetNumBlocks(); ++i)
	{
		//NOTE: Use indirect access to block numbers via m_vecBlockIds
		bSuccess = LoadSingleBlock(iTimeLevel,m_vecBlockIds[i],pCurrentDataSet,fSimTime);
		if(bSuccess)
			vecDataSets.push_back(pCurrentDataSet);
	}

	//if something went wrong -> free memory
	if(!bSuccess)
		this->CleanUpData(vecDataSets);

	return bSuccess;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadFullTopology                                            */
/*                                                                            */
/*============================================================================*/
bool VveMultiBlockIO::LoadFullTopology(std::vector< std::vector<VveMultiBlockTopoDef*> >& vecUnsteadyTopoData)
{
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadTimeStepTopology                                        */
/*                                                                            */
/*============================================================================*/
bool VveMultiBlockIO::LoadTimeLevelTopology(int iTimeLevel, std::vector<VveMultiBlockTopoDef*>& vecTopoDataSets)
{
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   WriteFullDataSet                                            */
/*                                                                            */
/*============================================================================*/
bool VveMultiBlockIO::WriteFullDataSet(std::vector< std::vector<vtkDataSet*> >& vecUnsteadyData, std::vector<float>& vecSimTimes)
{
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   WriteTimeStep                                               */
/*                                                                            */
/*============================================================================*/
bool VveMultiBlockIO::WriteTimeLevel(int iTimeLevel, std::vector<vtkDataSet*>& vecDataSets, float fSimTime)
{
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   WriteFullTopology                                           */
/*                                                                            */
/*============================================================================*/
bool VveMultiBlockIO::WriteFullTopology(std::vector< std::vector<VveMultiBlockTopoDef*> >& vecUnsteadyTopoData)
{
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   WriteTimeStepTopology                                       */
/*                                                                            */
/*============================================================================*/
bool VveMultiBlockIO::WriteTimeLevelTopology(int iTimeLevel, std::vector<VveMultiBlockTopoDef*>& vecTopoDataSets)
{
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   CleanUpData                                                 */
/*                                                                            */
/*============================================================================*/
void VveMultiBlockIO::CleanUpData(std::vector<vtkDataSet*>& vecDataSets)
{
	for(int i=0; i<int(vecDataSets.size()); ++i)
		vecDataSets[i]->Delete();
	vecDataSets.clear();
}
/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveDataSetName.cpp"                                             */
/*============================================================================*/
