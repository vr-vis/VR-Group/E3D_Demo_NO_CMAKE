/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEUNSTEADYDATALOADER_H
#define _VVEUNSTEADYDATALOADER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaVisExt/Tools/VveExecutable.h>

#include <VistaInterProcComm/Concurrency/VistaMutex.h>

#include <VistaAspects/VistaPropertyAwareable.h>
#include <VistaAspects/VistaObserveable.h>

#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveUnsteadyData;
class VistaThreadPool;
class IVistaThreadPoolWorkInstance;
class VistaThread;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflUnsteadyDataLoader loads data from disk, either threaded or blocking.
 */    
class VISTAVISEXTAPI VveUnsteadyDataLoader : public IVveExecutable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VveUnsteadyDataLoader(VveUnsteadyData *pTarget);
    virtual ~VveUnsteadyDataLoader();

    virtual bool Init(const VistaPropertyList &refProps) = 0;
    virtual bool Update (const VistaPropertyList &refProps)
    {return false;};

    /**
     * Kick off data loading - either blocking or threaded...
     */
    virtual void Execute();

    enum ELoaderState
    {
        READY = IVveExecutable::VFL_ES_LAST,
		WAITING,
        LOADING,
        FINISHED_SUCCESS,
        FINISHED_FAILURE,
		FINISHED_WARNING,
		ABORTING,
        LAST_STATE
    };


    virtual std::string GetStateString() const; 
    
	/**
	 * Register a thread pool which to enqueue this work in.
	 * (OPTIONAL!)
	 */
	void SetThreadPool(VistaThreadPool *pPool);
	VistaThreadPool *GetThreadPool() const;

    /**
     * Do the actual loading...
     * This method should set IVflExecutable::m_iExecutableState to 
     * LOADING upon start and to
	 * FINISHED_SUCCESS or FINISHED_FAILURE upon exit.
	 *
	 * NOTE: Don't call this method directly - call Execute() instead!!!
     */
    virtual void LoadDataThreadSafe() = 0;
    
	/**
	 * Set the executable's state and notify observers...
	 */
	virtual void SetState(int iState);

	/**
	 * Manage observer list, protected with a mutex.
	 */
	virtual int AttachObserver(IVistaObserver *pObs, int eTicket = TICKET_NONE);
	virtual int DetachObserver(IVistaObserver *pObs);
	virtual void Notify(int eMsg = MSG_NONE);

	bool GetLoadBlocking() const;
	void SetLoadBlocking(const bool b);

	/**
	 * Retrieve target data.
	 */
	virtual VveUnsteadyData *GetTarget() const;
    
protected:
	/**
	 * Allow subclasses to query whether to early out...
	 */
	bool GetAbort();
	void SetAbort(bool bAbort);

	void SetExecutableState(int iState);

    VveUnsteadyData *m_pTarget;
    bool m_bBlocking;

private:
    // don't let any subclass use this mutex!!!
    VistaMutex						m_oMutex;
	VistaMutex						m_oStateMutex;
	VistaMutex						m_oObserverMutex;

	// threading stuff...
	VistaThreadPool				*m_pPool;
	IVistaThreadPoolWorkInstance	*m_pWorkInstance;
	VistaThread					*m_pThread;
	bool							m_bAbort;
    bool                            m_bNeedJoin;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VVEUNSTEADYDATALOADER_H)
