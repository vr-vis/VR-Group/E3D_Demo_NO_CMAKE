/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VflExtRawIdxVtkPolyData.cpp  RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
#ifdef WIN32
    #pragma warning (disable : 4786)
#endif
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#include "VveAnnotatedVtkData.h"
#include <VistaVisExt/IO/VveSerializableVtkData.h>

#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>

const std::string VveAnnotatedVtkData::m_strSignature = "VveAnnotatedVtkData";
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveAnnotatedVtkData::VveAnnotatedVtkData()
    :    m_pData(new VveSerializableVtkData()),
         m_iRawIndex(-1),
		 m_strMetaTag("")
{
}

VveAnnotatedVtkData::~VveAnnotatedVtkData()
{
    delete m_pData;
}
/*============================================================================*/
/*                   IMPLEMENTATION                                           */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Serialize                                                   */
/*                                                                            */
/*============================================================================*/
int VveAnnotatedVtkData::Serialize(IVistaSerializer& rSerializer) const
{
	int iNumBytesWritten = 0;
	iNumBytesWritten += rSerializer.WriteString(m_strSignature);
    iNumBytesWritten += rSerializer.WriteInt32(m_iRawIndex);
	iNumBytesWritten += rSerializer.WriteBool(!m_propMetaData.empty());
	if(!m_propMetaData.empty())
	{
		iNumBytesWritten += VistaPropertyList::SerializePropertyList(
											rSerializer, 
											m_propMetaData, 
											m_strMetaTag);
	}
    iNumBytesWritten += m_pData->Serialize(rSerializer);
	return iNumBytesWritten;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeSerialize                                                 */
/*                                                                            */
/*============================================================================*/
int VveAnnotatedVtkData::DeSerialize(IVistaDeSerializer& rDeSerializer)
{
    int iNumBytesRead = 0;
	m_pData->Clear();
	std::string strSig;
	// @todo: Get rid of this cast.
	iNumBytesRead += rDeSerializer.ReadString(strSig,
		static_cast<int>(m_strSignature.length()));
	
	if(strSig != m_strSignature)
		return -1;

    iNumBytesRead += rDeSerializer.ReadInt32(m_iRawIndex);
	bool bHasMetaData = false;
	iNumBytesRead += rDeSerializer.ReadBool(bHasMetaData);
	if(bHasMetaData)
	{
		m_propMetaData.clear();
		m_strMetaTag.clear();
		iNumBytesRead += VistaPropertyList::DeSerializePropertyList(
											rDeSerializer, 
											m_propMetaData, 
											m_strMetaTag);
	}
    iNumBytesRead += m_pData->DeSerialize(rDeSerializer);
	return iNumBytesRead;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetData                                                     */
/*                                                                            */
/*============================================================================*/
vtkDataSet* VveAnnotatedVtkData::GetData() const
{
    return m_pData->GetDataObject(0);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetData                                                      */
/*                                                                            */
/*============================================================================*/
void VveAnnotatedVtkData::SetData(vtkDataSet* pData)
{
    m_pData->Clear();
    m_pData->AddDataObject(pData);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetIndex                                                    */
/*                                                                            */
/*============================================================================*/
int VveAnnotatedVtkData::GetRawIndex() const
{
    return m_iRawIndex;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetIndex                                                    */
/*                                                                            */
/*============================================================================*/
void VveAnnotatedVtkData::SetRawIndex(int i)
{
    m_iRawIndex = i;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetMetaData                                                 */
/*                                                                            */
/*============================================================================*/
void VveAnnotatedVtkData::SetMetaData(const VistaPropertyList& rProps, 
									   const std::string& strMetaTag)
{
	m_propMetaData = rProps;
	m_strMetaTag = strMetaTag;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ClearMetaData                                               */
/*                                                                            */
/*============================================================================*/
void VveAnnotatedVtkData::ClearMetaData()
{
	m_propMetaData.clear();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetHasMetaData                                              */
/*                                                                            */
/*============================================================================*/
bool VveAnnotatedVtkData::GetHasMetaData() const
{
	return !m_propMetaData.empty();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetMetaData                                                 */
/*                                                                            */
/*============================================================================*/
const VistaPropertyList & VveAnnotatedVtkData::GetMetaData() const
{
	return m_propMetaData;
}

