/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkVveMultiBlockIO.h	 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VtkExt		                 RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VtkVveMultiBlockIO      :   ...                                     */
/*                                                                            */
/*============================================================================*/


#ifndef _VTKVVEMULTIBLOCKIO_H
#define _VTKVVEMULTIBLOCKIO_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>
#include <VistaVisExt/IO/VveDataSetName.h>
#include <VistaVisExt/IO/VveMultiBlockIO.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveMultiBlockTopoDef;
class vtkDataSet;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 *	This class implements I/O-operations for simple multi-block grids made up off simple 
 *	vtkDataSets. It uses a vtkDataSetReader/Writer internally. Use VTK operations for
 *	safe downcasting into concrete subtype (e.g. vtkStructuredGrid) if needed.
 *
 *
 *	NOTE:	This implementation is still under construction. There is no support for multiple
 *			scalar fields in one data set right now as well as  no time reading/writing.
 *
 *	NOTE:	There may be something needed in here like the external provision of an 
 *			vtkReader/vtkWriter
 *
 *	@author Bernd Hentschel
 *	@version 1.0
 *	@since 2004 
 */
class VISTAVISEXTAPI VtkMultiBlockIO : public VveMultiBlockIO
{
public:
	VtkMultiBlockIO(); 
	virtual ~VtkMultiBlockIO();

	/**
	*
	*/
	bool GetBinaryMode() const;
	/**
	*
	*/
	void SetBinaryMode(bool bBMode);

	/**
	* Load a single block of a complete instationary multi-block data set containing time as well as data grid information
	*
	* @param[in]	iTimeLevel		index of the requested time level (this is the index incorporated in the file name)
	* @param[in]	iBlockNum		index of the requested block
	* @param[out]	pDataSet		pointer to the dataset loaded (NULL if unsuccessful)
	* @return	bool			<TRUE> iff load operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool LoadSingleBlock(int iTimeLevel, int iBlockNum, vtkDataSet*& pDataSet, float& fSimTime);
	/**
	* Save a single block's data including time information and grid data
	*
	* @param[in]	iTimeLevel		time level index of data to be saved (this is the index incorporated in the file name)
	* @param[in]	iBlockNum		block index of data to be saved
	* @param[in]	pDataSet		dataset to be saved
	* @param[in]	fSimTime		simulation time of time level to be saved 
	* @return	bool				<TRUE> iff load operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool WriteSingleBlock(int iTimeLevel, int iBlockNum, vtkDataSet* pDataSet, float fSimTime);
protected:

private:
	bool m_bUseBinaryMode;
};

#endif // _VTKVVEMULTIBLOCKIO_H

/*============================================================================*/
/*  END OF FILE "VtkVveMultiBlockIO.h"                                 */
/*============================================================================*/


