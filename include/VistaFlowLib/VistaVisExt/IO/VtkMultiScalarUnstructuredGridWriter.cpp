/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  MultiScalarUSGridWriter.CPP  RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VtkTopo                      RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  VtkTopo                        Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/

#include "VtkMultiScalarUnstructuredGridWriter.h"
#include <vtkObjectFactory.h>

#include <vtkPointData.h>
#include <vtkDataSet.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataArray.h>
#include <vtkCellArray.h>
#include <vtkByteSwap.h>
//#include "SwapWrite4BERange.h"

#include <utility>   // pair

using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VtkMultiScalarUnstructuredGridWriter::VtkMultiScalarUnstructuredGridWriter()
{
	//bMultiScalars = false;
}

VtkMultiScalarUnstructuredGridWriter::~VtkMultiScalarUnstructuredGridWriter()
{
    // what is the problem with this?
    // illegal heap mem: "delete[] this->ScalarsName"
    (this->ScalarsName)=NULL;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   New                                                         */
/*                                                                            */
/*============================================================================*/
#if (VTK_MAJOR_VERSION == 4) && (VTK_MINOR_VERSION < 2)
VtkMultiScalarUnstructuredGridWriter * VtkMultiScalarUnstructuredGridWriter::New()
{
    // First try to create the object from the vtkObjectFactory
    vtkObject* ret = vtkObjectFactory::CreateInstance("VtkMultiScalarUnstructuredGridWriter");
    if(ret)
    {
        return (VtkMultiScalarUnstructuredGridWriter*)ret;
    }
    // If the factory was unable to create the object, then create it here.
	return new VtkMultiScalarUnstructuredGridWriter;
}
#else
    vtkCxxRevisionMacro(VtkMultiScalarUnstructuredGridWriter, "$Revision: 1.1 $");
    vtkStandardNewMacro(VtkMultiScalarUnstructuredGridWriter);
#endif


/*============================================================================*/
/*                                                                            */
/*  NAME      :   New                                                         */
/*                                                                            */
/*============================================================================*/
bool VtkMultiScalarUnstructuredGridWriter::SetMultiScalars(
                   vtkDataArray *pScalars, std::string strName )
{
	pair <string, vtkDataArray *> newPair = make_pair (strName, pScalars);
    map  <string, vtkDataArray *>::iterator mapIter =
          m_mapScalarsAndName.end();
   
    m_mapScalarsAndName.insert( newPair );

    return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   WritePointData                                              */
/*                                                                            */
/*============================================================================*/
// original one: vtkDataWriter
int VtkMultiScalarUnstructuredGridWriter::WritePointData(ostream *fp, vtkDataSet *ds)
{
  int numPts;
  vtkDataArray *scalars;
  vtkDataArray *vectors;
  vtkDataArray *normals;
  vtkDataArray *tcoords;
  vtkDataArray *tensors;
  vtkFieldData *field;
  vtkPointData *pd=ds->GetPointData();

  vtkDebugMacro(<<"Writing point data...");

  numPts = ds->GetNumberOfPoints();
  scalars = pd->GetScalars();
  vectors = pd->GetVectors();
  normals = pd->GetNormals();
  tcoords = pd->GetTCoords();
  tensors = pd->GetTensors();
  field = pd;

  if ( numPts <= 0 || !(scalars || vectors || normals || tcoords || 
                        tensors || field))
    {
    vtkDebugMacro(<<"No point data to write!");
    return 1;
    }

  *fp << "POINT_DATA " << numPts << "\n";
  //
  // Write scalar data
  //
  //########## MULTI SCALAR MODIFICATION...
  if( m_mapScalarsAndName.size() > 0 )
  {
      map  <string, vtkDataArray *>::iterator mapIter =
          m_mapScalarsAndName.begin();
      while (mapIter != m_mapScalarsAndName.end())
      {
          this->ScalarsName = (char*)mapIter->first.c_str();
          if (!this->WriteScalarData (fp, mapIter->second, numPts ))
          {
              return 0;
          }
          mapIter++;
      }

#if 0
      for( int i = 0; i < m_mapScalarsAndName.size(); ++i )
	  {
		this->ScalarsName = (char *)m_mapScalarsAndName[i].strName.c_str();
		if(  ! this->WriteScalarData( fp, m_mapScalarsAndName[i].pScalars, numPts )  )
		{
			return 0;
		}
	  }
#endif
  }
  //########## MULTI SCALAR MODIFICATION #### !!
  else
  {
      if ( scalars && scalars->GetNumberOfTuples() > 0 )
        {
        if ( ! this->WriteScalarData(fp, scalars, numPts) )
          {
          return 0;
          }
        }
  }
  //
  // Write vector data
  //
  if ( vectors && vectors->GetNumberOfTuples() > 0 )
    {
    if ( ! this->WriteVectorData(fp, vectors, numPts) )
      {
      return 0;
      }
    }
  //
  // Write normals
  //
  if ( normals && normals->GetNumberOfTuples() > 0 )
    {
    if ( ! this->WriteNormalData(fp, normals, numPts) )
      {
      return 0;
      }
    }
  //
  // Write texture coords
  //
  if ( tcoords && tcoords->GetNumberOfTuples() > 0 )
    {
    if ( ! this->WriteTCoordData(fp, tcoords, numPts) )
      {
      return 0;
      }
    }
  //
  // Write tensors
  //
  if ( tensors && tensors->GetNumberOfTuples() > 0 )
    {
    if ( ! this->WriteTensorData(fp, tensors, numPts) )
      {
      return 0;
      }
    }
  //
  // Write tensors
  //
  if ( tensors && tensors->GetNumberOfTuples() > 0 )
    {
    if ( ! this->WriteTensorData(fp, tensors, numPts) )
      {
      return 0;
      }
    }
  //
  // Write field
  //
  if ( field && field->GetNumberOfTuples() > 0 )
    {
    if ( ! this->WriteFieldData(fp, field) )
      {
      return 0;
      }
    }

  return 1;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   WriteData                                                   */
/*                                                                            */
/*============================================================================*/
// original one: vtkUnstructuredGridWriter
// however: NO MODIFICATIONS!??
// this is needed because WritePointData is NOT VIRTUAL!
void VtkMultiScalarUnstructuredGridWriter::WriteData()
{
  ostream *fp=NULL;
  vtkUnstructuredGrid *input=this->GetInput();
  int *types, ncells, cellId;

  vtkDebugMacro(<<"Writing vtk unstructured grid data...");

  if ( !(fp=this->OpenVTKFile()) || !this->WriteHeader(fp) )
    {
    return;
    }
  //
  // Write unstructured grid specific stuff
  //
  *fp << "DATASET UNSTRUCTURED_GRID\n";

  // Write data owned by the dataset
  this->WriteDataSetData(fp, input);

  this->WritePoints(fp, input->GetPoints());
  this->WriteCells(fp, input->GetCells(),"CELLS");
  //
  // Cell types are a little more work
  //
  ncells = input->GetCells()->GetNumberOfCells();
  types = new int[ncells];
  for (cellId=0; cellId < ncells; cellId++)
    {
    types[cellId] = input->GetCellType(cellId);
    }

  *fp << "CELL_TYPES " << ncells << "\n";
  if ( this->FileType == VTK_ASCII )
    {
    for (cellId=0; cellId<ncells; cellId++)
      {
      *fp << types[cellId] << "\n";
      }
    }
  else
    {
    // swap the bytes if necc
    vtkByteSwap::SwapWrite4BERange(types,ncells,fp);
    }
  *fp << "\n";
  delete [] types;

  this->WriteCellData(fp, input);
  this->WritePointData(fp, input);

  this->CloseVTKFile(fp);  
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   RemoveInput                                                 */
/*                                                                            */
/*============================================================================*/
void VtkMultiScalarUnstructuredGridWriter::RemoveInput  (vtkDataObject * pInput) 
{
    //m_mapScalarsAndName;
    //pInput
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   PrintSelf                                                   */
/*                                                                            */
/*============================================================================*/
void VtkMultiScalarUnstructuredGridWriter::PrintSelf(ostream& os, vtkIndent indent)
{
    vtkUnstructuredGridWriter::PrintSelf(os,indent);
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "CMultiScalarUnstructuredGridWriter.cpp"                      */
/*============================================================================*/
