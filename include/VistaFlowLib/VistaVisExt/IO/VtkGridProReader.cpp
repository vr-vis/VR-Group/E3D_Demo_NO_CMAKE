/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName : VtkGridProReader.CP           RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaVisExt                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : Vista                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
#include "VtkGridProReader.h"

#include <VistaBase/VistaStreamUtils.h>

#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VtkGridProReader::VtkGridProReader()
: m_bTriangulateData(false)
{
}
VtkGridProReader::~VtkGridProReader()
{
}
/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   TriangulateDataOn                                           */
/*                                                                            */
/*============================================================================*/
void VtkGridProReader::TriangulateDataOn()
{
	m_bTriangulateData = true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   TriangulateDataOff                                          */
/*                                                                            */
/*============================================================================*/
void VtkGridProReader::TriangulateDataOff()
{
	m_bTriangulateData = false;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTriangulateData                                          */
/*                                                                            */
/*============================================================================*/
bool VtkGridProReader::GetTriangulateData() const
{
	return m_bTriangulateData ;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   ExecuteData                                                 */
/*                                                                            */
/*============================================================================*/
void VtkGridProReader::ExecuteData(vtkDataObject *output)
{
	vtkPolyData *pMyPolyData = (vtkPolyData*) output;
	vtkPoints *pPoints = vtkPoints::New();
	vtkCellArray *pCellArray = vtkCellArray::New();
	FILE *pFile;
	pFile = fopen(this->GetFileName(), "rt");
	
	if(pFile)
	{
		// reading point data
		int numberOfPoints;
		float myPointData[3];
		fscanf(pFile,"%d\n",&numberOfPoints);
		for (int i=0;i<numberOfPoints;++i) {
			fscanf(pFile, "%f %f %f \n", &myPointData[0], &myPointData[1], &myPointData[2]);
			pPoints->InsertPoint(i,myPointData);
		}

		// reading cell data
		vtkIdType numberOfCells;
		vtkIdType myCellData[4];
		vtkIdType myPartialCellData[3];
		fscanf(pFile,"%d\n",&numberOfCells);
		for (vtkIdType i=0;i<numberOfCells;++i) {
			fscanf(pFile, "%d %d %d %d 1\n", &myCellData[0], &myCellData[1], &myCellData[2], &myCellData[3]);
			--myCellData[0];
			--myCellData[1];
			--myCellData[2];
			--myCellData[3];
			if (m_bTriangulateData) {
				myPartialCellData[0] = myCellData[0];
				myPartialCellData[1] = myCellData[1];
				myPartialCellData[2] = myCellData[2];
				pCellArray->InsertNextCell(3,myPartialCellData);
				myPartialCellData[1] = myCellData[3];
				pCellArray->InsertNextCell(3,myPartialCellData);
			}
			else {//*/
				pCellArray->InsertNextCell(4,myCellData);
			}
		}

		fclose (pFile);
		pMyPolyData->SetPoints(pPoints);
		pMyPolyData->SetPolys(pCellArray);
		pPoints->Delete();
		pCellArray->Delete();
	}
	else
	{
		vstr::errp() << "[VtkGridProReader::Update] UNABLE TO OPEN FILE!" << endl;
	}
}
