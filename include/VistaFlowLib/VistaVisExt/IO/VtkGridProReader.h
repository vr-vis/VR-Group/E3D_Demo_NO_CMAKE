/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkGridProReader.h			 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VtkExt                       RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VtkGridProReader      :   ...                                    */
/*                                                                            */
/*============================================================================*/
#ifndef _VTKGRIDPROREADER_H
#define _VTKGRIDPROREADER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkPolyDataReader.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkDataObject;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * Read a mesh in gridpro format from disk and store it in a vtkPolyData object. 
 * Reading is performed by stdlib functionality i.e. no additional dependencies exist.
 *
 *	@author		Michael Holtman / Bernd Hentschel
 *	@date		June 2005
*/
class VISTAVISEXTAPI VtkGridProReader : public vtkPolyDataReader
{
public:
	static VtkGridProReader *New(){
		return new VtkGridProReader;
	};
	/**
	* Triangulate the data upon read (default is false)
	*/
	void TriangulateDataOn();
	/**
	* Don't triangulate the data upon read
	*/
	void TriangulateDataOff();
	/**
	* Inquire the data triangulation flag
	*/
	bool GetTriangulateData() const;

protected:
	bool m_bTriangulateData;
	VtkGridProReader();
	virtual ~VtkGridProReader();
	virtual void ExecuteData(vtkDataObject *output);
};


#endif // _VTKGRIDPROREADER_H
/*============================================================================*/
/*  END OF FILE "VtkGridProReader.h"                                          */
/*============================================================================*/
