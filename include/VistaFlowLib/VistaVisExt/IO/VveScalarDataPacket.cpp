/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VveScalarDataPacket.cpp		 RRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
#include "VveScalarDataPacket.h"
#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>

#include <VistaBase/VistaStreamUtils.h>

#include <cassert>

const string VveScalarDataPacket::m_strSignature("VveScalarDataPacket");
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
/*============================================================================*/
/*                   IMPLEMENTATION                                           */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :     Serialize                                                 */
/*                                                                            */
/*============================================================================*/
int VveScalarDataPacket::Serialize(IVistaSerializer& rSerializer) const
{
	/*
	*	Encode data
	*/
	rSerializer.WriteInt32(int(m_strSignature.length()));
	rSerializer.WriteString(m_strSignature);
	rSerializer.WriteInt32(m_iPacketID);
	rSerializer.WriteInt32(m_iTimeLevel);
	rSerializer.WriteInt32(m_iPointOffset);
	rSerializer.WriteInt32(int(m_vecPointIds.size()));	
	for(unsigned int i=0; i<m_vecPointIds.size(); ++i)
	{
		rSerializer.WriteInt32(m_vecPointIds[i]);
		rSerializer.WriteFloat32(m_vecScalars[i]);
	}
	return 1;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     DeSerialize                                               */
/*                                                                            */
/*============================================================================*/
int VveScalarDataPacket::DeSerialize(IVistaDeSerializer& rDeserializer)
{
	int iNumBytesRead = 0;
	int iNumScalars;
	int iSigLen;
	string strSignature;
	m_vecPointIds.clear();
	m_vecScalars.clear();
	/*
	*	Decode data
	*/
	iNumBytesRead = rDeserializer.ReadInt32(iSigLen);
	iNumBytesRead += rDeserializer.ReadString(strSignature,iSigLen);
	if(strSignature == m_strSignature)
	{
		iNumBytesRead += rDeserializer.ReadInt32(m_iPacketID);
		iNumBytesRead += rDeserializer.ReadInt32(m_iTimeLevel);
		iNumBytesRead += rDeserializer.ReadInt32(m_iPointOffset);
		iNumBytesRead += rDeserializer.ReadInt32(iNumScalars);	
		m_vecPointIds.resize(iNumScalars);
		m_vecScalars.resize(iNumScalars);
		for(int i=0; i<iNumScalars; ++i)
		{
			iNumBytesRead += rDeserializer.ReadInt32(m_vecPointIds[i]);
			iNumBytesRead += rDeserializer.ReadFloat32(m_vecScalars[i]);
		}
	}
	else
	{
		vstr::errp() << "[VveScalarDataPacket::DeSerialize] Received worng signature (" << strSignature << ")" << endl;
	}
	return iNumBytesRead;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :     GetSignature                                              */
/*                                                                            */
/*============================================================================*/
string VveScalarDataPacket::GetSignature() const
{
	return string("VveScalarDataPacket");
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     GetPacketId                                               */
/*                                                                            */
/*============================================================================*/
int VveScalarDataPacket::GetPacketID() const
{
	return m_iPacketID;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     GetTimeLevel                                              */
/*                                                                            */
/*============================================================================*/
int VveScalarDataPacket::GetTimeLevel() const
{
	return m_iTimeLevel;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     GetPointOffset                                            */
/*                                                                            */
/*============================================================================*/
int VveScalarDataPacket::GetPointOffset() const
{
	return m_iPointOffset;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     GetNumberOfScalars                                        */
/*                                                                            */
/*============================================================================*/
size_t VveScalarDataPacket::GetNumberOfScalars() const
{
	return m_vecScalars.size();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     operator[]                                                */
/*                                                                            */
/*============================================================================*/
float VveScalarDataPacket::operator[](size_t index) const
{
	assert(index < m_vecScalars.size());
	return m_vecScalars[index];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     GetScalar                                                 */
/*                                                                            */
/*============================================================================*/
float VveScalarDataPacket::GetScalar(size_t index) const
{
	assert(index < m_vecScalars.size());
	return m_vecScalars[index];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     GetPointId                                                */
/*                                                                            */
/*============================================================================*/
int VveScalarDataPacket::GetPointId(size_t index) const
{
	assert(index < m_vecScalars.size());
	return m_vecPointIds[index];
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     SetPacketId                                               */
/*                                                                            */
/*============================================================================*/
void VveScalarDataPacket::SetPacketID(int iId)
{
	m_iPacketID = iId;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     SetPacketSize                                             */
/*                                                                            */
/*============================================================================*/
void VveScalarDataPacket::SetPacketSize(int iSize)
{
	if(iSize>static_cast<int>(m_vecScalars.capacity()))
	{
		m_vecScalars.reserve(iSize);
		m_vecPointIds.reserve(iSize);
	}
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     SetTimeLevel                                              */
/*                                                                            */
/*============================================================================*/
void VveScalarDataPacket::SetTimeLevel(int iTime)
{
	m_iTimeLevel = iTime;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     SetPointOffset                                            */
/*                                                                            */
/*============================================================================*/
void VveScalarDataPacket::SetPointOffset(int iOfs)
{
	m_iPointOffset = iOfs;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     SetPointId                                                */
/*                                                                            */
/*============================================================================*/
void VveScalarDataPacket::SetPointId(size_t index, int iId)
{
	assert(index < m_vecPointIds.size());
	m_vecPointIds[index] = iId;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     SetScalar                                              */
/*                                                                            */
/*============================================================================*/
void VveScalarDataPacket::SetScalar(size_t index, float fScalar)
{
	assert(index < m_vecScalars.size());
	m_vecScalars[index] = fScalar;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     AddValue	                                              */
/*                                                                            */
/*============================================================================*/
void VveScalarDataPacket::AddValue(int i, float f)
{
	m_vecPointIds.push_back(i);
	m_vecScalars.push_back(f);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     Clear		                                              */
/*                                                                            */
/*============================================================================*/
void VveScalarDataPacket::Clear()
{
	m_vecPointIds.clear();
	m_vecScalars.clear();
	m_iTimeLevel = -1;
	m_iPointOffset = -1;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :     EstimateByteSize                                          */
/*                                                                            */
/*============================================================================*/
size_t VveScalarDataPacket::EstimateByteSize() const
{
	//  signature |  id,step,ofs,#values |                    ptids               |            scalar values
	return   16   +  (4 * sizeof(int) + m_vecPointIds.size()) * sizeof(int) + m_vecScalars.size() * sizeof(float);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :     operator<<	                                              */
/*                                                                            */
/*============================================================================*/
ostream& operator<<(ostream& out, const VveScalarDataPacket& packet)
{
	out << endl;
	out << "PACKET INFORMATION for packet No. <" << packet.GetPacketID() << ">" << endl;
	out << "\tTimeLevel = " << packet.GetTimeLevel() << endl;
	out << "\tOffset   = " << packet.GetPointOffset() << endl;
	out << "\t#Scalars = " << packet.GetNumberOfScalars() << endl;
	for(size_t i=0; i<packet.GetNumberOfScalars(); ++i)
		out << "\tScalar[" << i << "] for point [" << packet.GetPointId(i) << "] = " << packet[i] << endl;
	out << endl;
	out.flush();
	return out;
}
