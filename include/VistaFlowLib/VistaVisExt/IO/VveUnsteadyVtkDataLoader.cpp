/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VveUnsteadyVtkDataLoader.h"

#include "VveDataSetName.h"
#include "VtkGridProReader.h"

#include <VistaVisExt/Tools/VveTimeMapper.h>

#include <VistaVisExt/Data/VveUnsteadyData.h>
#include <VistaVisExt/Data/VveVtkData.h>

#include <vtkDataSetReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkAlgorithm.h>

#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkStructuredGrid.h>
#include <vtkStructuredPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataSet.h>

#include <VistaTools/VistaProgressBar.h>
#include <VistaTools/VistaFileSystemFile.h>

#include <VistaAspects/VistaAspectsUtils.h>
#include <assert.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;
//avoid to write the code for actually setting the data for each data type!
template<class TData>
bool VflUnsteadyVtkDataLoader_SetDataToVis(IVveDataItem *pContainer, 
											TData* pData, 
											const std::string& strActiveScalars, 
											const std::string& strActiveVectors);
/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveUnsteadyVtkDataLoader::VveUnsteadyVtkDataLoader(VveUnsteadyData *pTarget)
	: 	VveUnsteadyDataLoader(pTarget),
		m_pMyProps(new CProperties())
{
}

VveUnsteadyVtkDataLoader::~VveUnsteadyVtkDataLoader()
{
	delete m_pMyProps;
	m_pMyProps = 0;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::Init(const VistaPropertyList &refProps)
{
	// check whether we've got a valid data object...
	if (!m_pTarget)
	{
		vstr::errp()  << "[VflUnsteadyVtkDataLoader] invalid target data!" << endl << endl;
		return false;
	}
	//paramter checks
	if( !refProps.HasProperty("FILE_NAME") )
	{
		vstr::errp() << "[VflUnsteadyVtkDataLoader] no file name specified!" << endl << endl;
		return false;
	}
	if( refProps.HasProperty("ACTIVE_SCALARS") && refProps.HasProperty("SCALARS_NAME"))
	{
		vstr::warnp() << "[VflUnsteadyVtkDataLoader] setting ACTIVE_SCALARS and SCALARS_NAME!" << endl;
	}
	if( refProps.HasProperty("ACTIVE_VECTORS") && refProps.HasProperty("VECTORS_NAME"))
	{
		vstr::warnp() << "[VflUnsteadyVtkDataLoader] setting ACTIVE_VECTORS and VECTORS_NAME!" << endl;
	}
	if( !refProps.HasProperty("FORMAT") )
	{
		vstr::errp() << "[VflUnsteadyVtkDataLoader] No format given!" << endl << endl;
		return false;
	}
	if(refProps.HasProperty("BLOCKING"))
		this->SetLoadBlocking(refProps.GetValue<bool>("BLOCKING"));
	
	//init my props thread safe 
	m_pMyProps->LockProps();
	m_pMyProps->SetPropertiesByList(refProps);
	m_pMyProps->UnlockProps();
	
	if(m_pMyProps->GetEFileFormat() == DF_INVALID)
	{
		vstr::errp() << "[VflUnsteadyVtkDataLoader] Unknown data format <" << m_pMyProps->GetFileFormat() << ">" << endl << endl;
		return false;
	}
	if(m_pMyProps->GetEFileFormat() != DF_GRIDPRO_POLYDATA && m_pMyProps->GetTriangulateInput())
	{
		vstr::warnp() << "[VflUnsteadyVtkDataLoader] automatic triangulation of input data only implemented for GridPro data!" << endl;
	}
	

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadDataThreadSafe                                          */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyVtkDataLoader::LoadDataThreadSafe()
{
	SetState(LOADING);
	
	//copy props (locked for thread safety)
	m_pMyProps->LockProps();
	string strFilePattern = m_pMyProps->GetFilePattern();
	EDataFormat eFormat = m_pMyProps->GetEFileFormat();
	//deprecated: time file should no longer be used!
	//string strTimeFile = m_pMyProps->GetTimeFile();  
    
	string strScalarsName = m_pMyProps->GetScalarsName();
    string strVectorsName = m_pMyProps->GetVectorsName();
	string strActiveScalars = m_pMyProps->GetActiveScalars();
	string strActiveVectors = m_pMyProps->GetActiveVectors();
	
	bool bProgressBar = m_pMyProps->GetShowProgressBar();
	bool bTriangulate = m_pMyProps->GetTriangulateInput();
	m_pMyProps->UnlockProps();
	//check if output data format matches data target type -> do this once since dyn cast is expensive!
	//@todo: add DF_VTP_POLYDATA case
	if(	(eFormat == DF_VTK_POLYDATA         && !dynamic_cast<VveDiscreteDataTyped<vtkPolyData>*>(m_pTarget)) ||
		(eFormat == DF_GRIDPRO_POLYDATA     && !dynamic_cast<VveDiscreteDataTyped<vtkPolyData>*>(m_pTarget)) ||
		(eFormat == DF_VTK_STRUCTUREDPOINTS && !dynamic_cast<VveDiscreteDataTyped<vtkStructuredPoints>*>(m_pTarget)) ||
		(eFormat == DF_VTK_STRUCTUREDGRID   && !dynamic_cast<VveDiscreteDataTyped<vtkStructuredGrid>*>(m_pTarget)) ||
		(eFormat == DF_VTK_UNSTRUCTUREDGRID && !dynamic_cast<VveDiscreteDataTyped<vtkUnstructuredGrid>*>(m_pTarget)) ||
		(eFormat == DF_VTK_DATASET          && !dynamic_cast<VveDiscreteDataTyped<vtkDataSet>*>(m_pTarget)) )
	{
		vstr::errp()<<"[VflUnsteadyVtkDataLoader] invalid target data type!\n"<<
			"\tCast to output type "<<m_pMyProps->GetFileFormat().c_str()<<"failed!\n\n";
		SetState(FINISHED_FAILURE);
		return;
	}
	vstr::outi()<<"[VflUnsteadyVtkDataLoader] - starting data load..."<<endl;


	// determine time mapper(s)
	VveTimeMapper *pLoadMapper = m_pTarget->GetTimeMapper();
	assert(pLoadMapper);
//     if (strTimeFile.empty())
//     {
//         pLoadMapper = new VflTimeMapper(pTargetMapper);
// #ifdef DEBUG
//         printf("                                  using time mapper of data target...\n");
// #endif
//     }
//     else
//     {
//         printf("                                  using dedicated time file...\n");
//         printf("                                  file name: %s\n", strTimeFile.c_str());
//         VflResourceKey<VflTimeMapper> oKey(strTimeFile);
//         pLoadMapper = VflResourceManager::GetResourceManager()->GetResource(oKey, NULL);
//         if (!pLoadMapper)
//         {
//             printf("*** ERROR *** [VflUnsteadyVtkDataLoader] unable to retrieve time mapper...\n");
//             printf("\tusing time mapper of data target instead...\n");
//             pLoadMapper = new VflTimeMapper(pTargetMapper);
//         }
//     }

    if (strFilePattern.empty())
    {
		vstr::errp()<<"[VflUnsteadyVtkDataLoader] no file name given..."<<endl;
        SetState(FINISHED_FAILURE);
        return;
    }

    
#ifdef DEBUG
	vstr::debugi() << "    file name       : " << strFilePattern << endl
		<< "    scalars name    :   " << (strScalarsName.empty()?"*default*":strScalarsName) << endl
		<< "    vectors name    :   " << (strVectorsName.empty()?"*default*":strVectorsName) << endl
		<< "    active scalars  :   " << (strActiveScalars.empty()?"*default*":strActiveScalars) << endl
		<< "    active vectors  :   " << (strActiveVectors.empty()?"*default*":strActiveVectors) << endl
		<< "    blocking        :  " << (m_bBlocking?"true":"false") << endl;
#endif

	VveDataSetName oFilename;
	oFilename.Set(strFilePattern);

	int i, iLevel=0, iIndex;
    int iLevelCount = pLoadMapper->GetNumberOfTimeLevels();
	
	VistaProgressBar oBar(iLevelCount);
	if (bProgressBar)
	{
		oBar.SetPrefixString("    ");
		oBar.SetBarTicks(30);
		oBar.Start();
	}
	else
	{
		oBar.SetPrefixString("    finished loading - ");
		oBar.SetDisplayBar(false);
		oBar.SetSilent(true);
		oBar.Start();
	}
	string strTemp;
	int iWarnings = 0;

	vtkDataReader *pReader = NULL;

	//@todo: we use vtkXMLPolyDataReader only for one specific case 
	//(loading PV vtp files (eFormat == DF_VTP_POLYDATA)) -> code properly
	vtkXMLPolyDataReader *pXMLReader = NULL;
	
	VveDiscreteData *pData = dynamic_cast<VveDiscreteData*>(m_pTarget);
	IVveDataItem *pContainer;

	for (i=0; i<iLevelCount; ++i)
	{
		if (GetAbort())
		{
			SetState(FINISHED_WARNING);
			break;
		}
		// find time level to be loaded
		iLevel = pLoadMapper->GetTimeLevelForLevelIndex(i);
		strTemp = oFilename.GetFileName(iLevel);
		// check for existence of data file
		VistaFileSystemFile oFile(strTemp);
		if (!oFile.Exists())
		{
			vstr::warnp()<<"[VflUnsteadyVtkDataLoader] unable to access file!"<<endl<<
				"                                           file name: <"<<strTemp.c_str()<<">"<<endl;
			++iWarnings;
			oBar.Increment();
			continue;
		}
		//create suitable reader
		switch(eFormat)
		{
			case DF_VTK_POLYDATA:
			case DF_VTK_STRUCTUREDGRID:
			case DF_VTK_STRUCTUREDPOINTS:
			case DF_VTK_UNSTRUCTUREDGRID:
			case DF_VTK_DATASET:
				pReader = vtkDataSetReader::New();
			break;
			case DF_GRIDPRO_POLYDATA:
				pReader = VtkGridProReader::New();
				if(bTriangulate)
					((VtkGridProReader*)pReader)->TriangulateDataOn();
			break;
			case DF_VTP_POLYDATA:
				pXMLReader = vtkXMLPolyDataReader::New();
			break;
			default:
				vstr::errp() << "[VflUnstVtkDataL::LoadData] No format specified!" << endl;
				return;
		};
		
		if(eFormat==DF_VTP_POLYDATA)
		{
			pXMLReader->SetFileName(strTemp.c_str());
			/*@todo: vtkXMLPolyDataReader has no SetScalarsName/SetVectorsName methods! how to set them properly? 

			if (!strScalarsName.empty())
				pXMLReader->SetScalarsName(strScalarsName.c_str());
			if (!strVectorsName.empty())
				pXMLReader->SetVectorsName(strVectorsName.c_str());
			*/
			pXMLReader->Update();
		}
		else
		{
			pReader->SetFileName(strTemp.c_str());
			if (!strScalarsName.empty())
				pReader->SetScalarsName(strScalarsName.c_str());
			if (!strVectorsName.empty())
				pReader->SetVectorsName(strVectorsName.c_str());
			pReader->Update();
		}

		iIndex = pLoadMapper->GetLevelIndexForTimeLevel(iLevel);
		if (iIndex < 0)
		{
			vstr::errp()<<"[VflUnsteadyVtkDataLoader] invalid time index ["<<iIndex<<"]"<<endl<<endl;
			++iWarnings;
		}

		//actually SET the data
		pContainer = pData->GetLevelDataByLevelIndex(iIndex);
		pContainer->LockData();
		switch(eFormat)
		{
			case DF_GRIDPRO_POLYDATA:
				{
					if(VflUnsteadyVtkDataLoader_SetDataToVis(pContainer, ((VtkGridProReader*)pReader)->GetOutput(), strActiveScalars, strActiveVectors))
						pContainer->Notify();
					else
						++iWarnings;
					break;
				}
			case DF_VTK_POLYDATA:
				{
					if(VflUnsteadyVtkDataLoader_SetDataToVis(pContainer, ((vtkDataSetReader*)pReader)->GetPolyDataOutput(), strActiveScalars, strActiveVectors))
						pContainer->Notify();
					else
						++iWarnings;
					break;
				}
			case DF_VTK_STRUCTUREDGRID:
				{
					if(VflUnsteadyVtkDataLoader_SetDataToVis(pContainer, ((vtkDataSetReader*)pReader)->GetStructuredGridOutput(), strActiveScalars, strActiveVectors))
						pContainer->Notify();
					else
						++iWarnings;
					break;
				}
			case DF_VTK_STRUCTUREDPOINTS:
				{
					if(VflUnsteadyVtkDataLoader_SetDataToVis(pContainer, ((vtkDataSetReader*)pReader)->GetStructuredPointsOutput(), strActiveScalars, strActiveVectors))
						pContainer->Notify();
					else
						++iWarnings;
					break;
				}
			case DF_VTK_UNSTRUCTUREDGRID:
				{
					if(VflUnsteadyVtkDataLoader_SetDataToVis(pContainer, ((vtkDataSetReader*)pReader)->GetUnstructuredGridOutput(), strActiveScalars, strActiveVectors))
						pContainer->Notify();
					else
						++iWarnings;
					break;
				}
			case DF_VTK_DATASET:
				{
					if(VflUnsteadyVtkDataLoader_SetDataToVis(pContainer, ((vtkDataSetReader*)pReader)->GetOutput(), strActiveScalars, strActiveVectors))
						pContainer->Notify();
					else
						++iWarnings;
					break;
				}
			case DF_VTP_POLYDATA:
				{
					if(VflUnsteadyVtkDataLoader_SetDataToVis(pContainer, pXMLReader->GetOutput(), strActiveScalars, strActiveVectors))
						pContainer->Notify();
					else
						++iWarnings;
					break;
				}
		};
		pContainer->UnlockData();
		//NOTE: DURING SETTING WE DO A SHALLOW COPY ONLY -> This relies on the fact that we create a seperate loader for each time level!
		oBar.Increment();
	}
	oBar.SetSilent(false);
	oBar.Finish();

	if (iWarnings)
		SetState(FINISHED_WARNING);
	else 
		SetState(FINISHED_SUCCESS);

	if(pReader)
		pReader->Delete();
	if(pXMLReader)
		pXMLReader->Delete();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
VveUnsteadyVtkDataLoader::CProperties *VveUnsteadyVtkDataLoader::GetProperties() const
{
	return m_pMyProps;
}


//*****************************************************************************
//*****************************************************************************//
//                IMPLEMENTATION OF PROPERTY OBJECT
//*****************************************************************************
//*****************************************************************************

static const string STR_REFLECTIONABLE_TYPENAME("VflUnsteadyVtkDataLoader::CProperties");
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
VveUnsteadyVtkDataLoader::CProperties::CProperties()
	:	m_strFilePattern(""),
		m_eDataFormat(DF_INVALID),
		m_strTimeFile(""),
		m_strActiveScalars(""),
		m_strActiveVectors(""),
		m_strScalarsName(""),
		m_strVectorsName(""),
		m_bShowProgressBar(true),
		m_bTriangulate(false),
		m_pPropLock(new VistaMutex())
{}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
VveUnsteadyVtkDataLoader::CProperties::CProperties(const VveUnsteadyVtkDataLoader::CProperties& source)
	:	m_pPropLock(new VistaMutex())
{
	this->operator=(source);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
VveUnsteadyVtkDataLoader::CProperties::~CProperties()
{
	delete m_pPropLock;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
VveUnsteadyVtkDataLoader::CProperties& VveUnsteadyVtkDataLoader::CProperties::operator =(const VveUnsteadyVtkDataLoader::CProperties& source)
{
	this->SetFilePattern(source.GetFilePattern());
	this->SetFileFormat(source.GetFileFormat());
	this->SetTimeFile(source.GetTimeFile());
	this->SetActiveScalars(source.GetActiveScalars());
	this->SetActiveVectors(source.GetActiveVectors());
	this->SetScalarsName(source.GetScalarsName());
	this->SetVectorsName(source.GetVectorsName());
	//this->SetLoadBlocking(source.GetLoadBlocking());
	this->SetShowProgressBar(source.GetShowProgressBar());
	this->SetTriangulateInput(source.GetTriangulateInput());
	return *this;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::SetFilePattern(const std::string& str)
{
	if(str == m_strFilePattern)
		return false;
	m_strFilePattern = str;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyVtkDataLoader::CProperties::GetFilePattern() const
{
	return m_strFilePattern;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::SetFileFormat(const std::string& str)
{
	VveUnsteadyVtkDataLoader::EDataFormat eNewFormat = this->String2Format(str);
	if(eNewFormat == VveUnsteadyVtkDataLoader::DF_INVALID || eNewFormat == m_eDataFormat)
		return false;
	m_eDataFormat = eNewFormat;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::SetFileFormat(const VveUnsteadyVtkDataLoader::EDataFormat e)
{
	if(e == m_eDataFormat)
		return false;
	m_eDataFormat = e;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyVtkDataLoader::CProperties::GetFileFormat() const
{
	return this->Format2String(m_eDataFormat);
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
VveUnsteadyVtkDataLoader::EDataFormat VveUnsteadyVtkDataLoader::CProperties::GetEFileFormat() const
{
	return m_eDataFormat;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::SetTimeFile(const std::string& str)
{
	if(str == m_strTimeFile)
		return false;
	m_strTimeFile = str;
	vstr::warnp() << "[VflUnsteadyVtkDataLoader] SetTimeFile is deprecated!" 
		 << endl;

	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyVtkDataLoader::CProperties::GetTimeFile() const
{
	return m_strTimeFile;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::SetActiveScalars(const std::string& str)
{
	if(m_strActiveScalars == str)
		return false;
	m_strActiveScalars = str;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyVtkDataLoader::CProperties::GetActiveScalars() const
{
	return m_strActiveScalars;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::SetActiveVectors(const std::string& str)
{
	if(m_strActiveVectors == str)
		return false;
	m_strActiveVectors = str;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyVtkDataLoader::CProperties::GetActiveVectors() const
{
	return m_strActiveVectors;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::SetScalarsName(const std::string& str)
{
	if(m_strScalarsName == str)
		return false;
	m_strScalarsName = str;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyVtkDataLoader::CProperties::GetScalarsName() const
{
	return m_strScalarsName;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::SetVectorsName(const std::string& str)
{
	if(m_strVectorsName == str)
		return false;
	m_strVectorsName = str;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyVtkDataLoader::CProperties::GetVectorsName() const
{
	return m_strVectorsName;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::SetShowProgressBar(const bool b)
{
	if(b == m_bShowProgressBar)
		return false;
	m_bShowProgressBar = b;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::GetShowProgressBar() const
{
	return m_bShowProgressBar;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::SetTriangulateInput(const bool b)
{
	if(b == m_bTriangulate)
		return false;
	m_bTriangulate = b;
	return true;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyVtkDataLoader::CProperties::GetTriangulateInput() const
{
	return m_bTriangulate;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyVtkDataLoader::CProperties::LockProps()
{
	m_pPropLock->Lock();
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyVtkDataLoader::CProperties::UnlockProps()
{
	m_pPropLock->Unlock();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyVtkDataLoader::CProperties::GetReflectionableType() const
{
	return STR_REFLECTIONABLE_TYPENAME;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
int VveUnsteadyVtkDataLoader::CProperties::AddToBaseTypeList(std::list<std::string>& rBTList) const
{
	int iSize = IVistaReflectionable::AddToBaseTypeList(rBTList);
	rBTList.push_back(STR_REFLECTIONABLE_TYPENAME);
	return iSize+1;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
VveUnsteadyVtkDataLoader::EDataFormat VveUnsteadyVtkDataLoader::CProperties::String2Format(const std::string& str) const
{
	if(str == "VTK_POLYDATA")
		return DF_VTK_POLYDATA;
	if(str == "GRIDPRO_POLYDATA")
		return DF_GRIDPRO_POLYDATA;
	if(str == "VTK_STRUCTUREDPOINTS")
		return DF_VTK_STRUCTUREDPOINTS;
	if(str == "VTK_STRUCTUREDGRID")
		return DF_VTK_STRUCTUREDGRID;
	if(str == "VTK_UNSTRUCTUREDGRID")
		return DF_VTK_UNSTRUCTUREDGRID;
	if(str == "VTK_DATASET")
		return DF_VTK_DATASET;
	if (str == "VTP_POLYDATA")
		return DF_VTP_POLYDATA;
	return DF_INVALID;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :                                                               */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyVtkDataLoader::CProperties::Format2String(const VveUnsteadyVtkDataLoader::EDataFormat e) const
{
	switch(e)
	{
	case DF_VTK_POLYDATA:
		return "VTK_POLYDATA";
	case DF_GRIDPRO_POLYDATA:
		return "GRIDPRO_POLYDATA";
	case DF_VTK_STRUCTUREDPOINTS:
		return "VTK_STRUCTUREDPOINTS";
	case DF_VTK_STRUCTUREDGRID:
		return "VTK_STRUCTUREDGRID";
	case DF_VTK_UNSTRUCTUREDGRID:
		return "VTK_UNSTRUCTUREDGRID";
	case DF_VTK_DATASET:
		return "VTK_DATASET";
	case DF_VTP_POLYDATA:
		return "VTP_POLYDATA";
	default:
		return "";
	}
}
/*============================================================================*/
/*                                                                            */
/* Register reflectionable access routines                                    */
/*                                                                            */
/*============================================================================*/
static IVistaPropertySetFunctor *setFunctors[] = {
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyVtkDataLoader::CProperties>("FILE_NAME", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::SetFilePattern),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyVtkDataLoader::CProperties>("FORMAT", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::SetFileFormat),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyVtkDataLoader::CProperties>("TIME_FILE", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::SetTimeFile),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyVtkDataLoader::CProperties>("SCALARS_NAME", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::SetScalarsName),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyVtkDataLoader::CProperties>("VECTORS_NAME", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::SetVectorsName),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyVtkDataLoader::CProperties>("ACTIVE_SCALARS", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::SetActiveScalars),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyVtkDataLoader::CProperties>("ACTIVE_VECTORS", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::SetActiveVectors),
	new TVistaPropertySet<const bool, bool, VveUnsteadyVtkDataLoader::CProperties>("PROGRESS_BAR", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::SetShowProgressBar),
	new TVistaPropertySet<const bool, bool, VveUnsteadyVtkDataLoader::CProperties>("TRIANGULATE", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::SetShowProgressBar),
	NULL
};

static IVistaPropertyGetFunctor *getFunctors[] = {
	new TVistaPropertyGet<std::string, VveUnsteadyVtkDataLoader::CProperties>("FILE_NAME", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::GetFilePattern),
	new TVistaPropertyGet<std::string, VveUnsteadyVtkDataLoader::CProperties>("FORMAT", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::GetFileFormat),
	new TVistaPropertyGet<std::string, VveUnsteadyVtkDataLoader::CProperties>("TIME_FILE", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::GetTimeFile),
	new TVistaPropertyGet<std::string, VveUnsteadyVtkDataLoader::CProperties>("SCALARS_NAME", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::GetScalarsName),
	new TVistaPropertyGet<std::string, VveUnsteadyVtkDataLoader::CProperties>("VECTORS_NAME", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::GetVectorsName),
	new TVistaPropertyGet<std::string, VveUnsteadyVtkDataLoader::CProperties>("ACTIVE_SCALARS", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::GetActiveScalars),
	new TVistaPropertyGet<std::string, VveUnsteadyVtkDataLoader::CProperties>("ACTIVE_VECTORS", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::GetActiveVectors),
	new TVistaPropertyGet<bool, VveUnsteadyVtkDataLoader::CProperties>("PROGRESS_BAR", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::GetShowProgressBar),
	new TVistaPropertyGet<bool, VveUnsteadyVtkDataLoader::CProperties>("TRIANGULATE", STR_REFLECTIONABLE_TYPENAME, &VveUnsteadyVtkDataLoader::CProperties::GetTriangulateInput),
	NULL
};


/*============================================================================*/
/*                                                                            */
/*         VflUnsteadyVtkDataLoader_SetDataToVis                             */
/*                                                                            */
/*============================================================================*/
template<class TData>
bool VflUnsteadyVtkDataLoader_SetDataToVis(IVveDataItem *pContainer, TData* pData, const std::string& strActiveScalars, const std::string& strActiveVectors)
{
	VveDataItem<TData>* pDataEntry = dynamic_cast<VveDataItem<TData>*>(pContainer);
	if(!pDataEntry)
	{
		vstr::errp()<<"[VflUnsteadyVtkDataLoader] unable to access vtk container and/or content!\n\n";
		return false;
	}
	//NOTE: THIS SHALLOW COPY RELIES ON THE FACT THAT WE CREATE A NEW LOADER FOR EACH TIME LEVEL!
	if(!pDataEntry->GetData())
	{
		pDataEntry->UnlockData();
		pDataEntry->SetData(pData->NewInstance());
		pDataEntry->LockData();
	}

	pDataEntry->GetData()->ShallowCopy(pData);
	if (!strActiveScalars.empty())
		pDataEntry->GetData()->GetPointData()->SetActiveScalars(strActiveScalars.c_str());
	if (!strActiveVectors.empty())
		pDataEntry->GetData()->GetPointData()->SetActiveVectors(strActiveVectors.c_str());
	return true;	
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/


