/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VveUnsteadyDataLoader.h"

#include <VistaInterProcComm/Concurrency/VistaThread.h>
#include <VistaInterProcComm/Concurrency/VistaThreadPool.h>

#include <iostream>
#include <VistaBase/VistaTimeUtils.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  LOCAL HELPER CLASSES                                                      */
/*============================================================================*/
class CLoaderWorkInstance : public IVistaThreadPoolWorkInstance
{
public:
	CLoaderWorkInstance(VveUnsteadyDataLoader *pLoader)
		: m_pLoader(pLoader)
	{
	}
	virtual ~CLoaderWorkInstance()
	{
	}
protected:
	virtual void DefinedThreadWork()
	{
		m_pLoader->LoadDataThreadSafe();
	};
	VveUnsteadyDataLoader *m_pLoader;
};

class CLoaderThread : public VistaThread
{
public:
	CLoaderThread(VveUnsteadyDataLoader *pLoader)
		: m_pLoader(pLoader)
	{
		SetThreadName("[VflUnsteadyDataLoader - LoaderThread]");
	};

protected:
	virtual void ThreadBody()
	{
		m_pLoader->LoadDataThreadSafe();
	};

	VveUnsteadyDataLoader *m_pLoader;
};

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveUnsteadyDataLoader::VveUnsteadyDataLoader(VveUnsteadyData *pTarget)
: IVveExecutable(),
  m_pTarget(pTarget),
  m_pPool(NULL),
  m_pWorkInstance(NULL),
  m_pThread(NULL),
  m_bAbort(false),
  m_bNeedJoin(false)
{
    if (!m_pTarget)
    {
		vstr::warnp() << " [VflUnsteadyDataLoader] initialization problem:" << endl;
        vstr::warni() << "                                     no data target pointer given..." << endl;
    }
    else
    {
        SetState(READY);
    }

	// create a thread work instance in any case - we will decide, whether to run it later on...
	m_pWorkInstance = new CLoaderWorkInstance(this);

	// create a thread in any case - we will decide, whether to run it later on...
	m_pThread = new CLoaderThread(this);
}

VveUnsteadyDataLoader::~VveUnsteadyDataLoader()
{
	vstr::outi() << " [VflUnsteadyDataLoader] - shutting down loader thread..." << endl;
	{
		VistaMutexLock oLock(m_oMutex);
		SetState(ABORTING);
		m_bAbort = true;
	}
	m_pThread->Join();
	delete m_pThread;
	m_pThread = NULL;


	if (m_pPool)
	{
		// if we can remove the job, it has not already started and we can safely delete it
		if (!m_pPool->RemoveWork(m_pWorkInstance))
		{
			// if we can remove the job from the done list, it has already finished and can be safely deleted
			if (!m_pPool->RemoveDoneJob(m_pWorkInstance->GetJobId()))
			{
				// otherwise it is still running an we must wait for it to finish
				vstr::outi() << " [VflUnsteadyDataLoader] - loader thread is still in pool - waiting for thread to finish..." << endl;

				SetAbort(true);

				// Wait for the loader thread to abort/finish
				while (!m_pWorkInstance->GetIsDone())
				{
					VistaTimeUtils::Sleep(10);
				}

				while (!m_pPool->RemoveDoneJob(m_pWorkInstance->GetJobId()))
				{
					VistaTimeUtils::Sleep(10);
				}
			}
		}
	}

// 	if (m_pPool && (m_pPool->GetJobById(m_pWorkInstance->GetJobId()) || m_pWorkInstance->GetIsProcessed()))
// 	{
// 		m_pPool->RemoveWork(m_pWorkInstance);
// 
// 		vstr::outi() << " [VflUnsteadyDataLoader] - loader thread is still in pool - waiting for thread to finish..." << endl;
// 		SetAbort(true);
// 		
// 		unsigned int nCount = 0;
// 
// 		while(m_pWorkInstance->GetIsProcessed())
// 		{
// // 			if( ++nCount == 1000 )
// // 				break;
// 			VistaTimeUtils::Sleep(100);
// 		}
// 
// 		m_pPool->RemoveDoneJob(m_pWorkInstance);
// 
// 		vstr::outi() << " [VflUnsteadyDataLoader] - loader thread finished..." << endl;
// 	}

	delete m_pWorkInstance;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Execute                                                     */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyDataLoader::Execute()
{
    if (!m_pTarget)
    {
        vstr::errp() << " [VflUnsteadyDataLoader] no data to be filled given..." << endl;
        return;
    }

    if (m_bBlocking)
    {
        LoadDataThreadSafe();
    }
    else
    {
		if (m_bNeedJoin)
			m_pThread->Join();

		VistaMutexLock oLock(m_oMutex);

        if (m_pThread->IsRunning() 
			|| GetState() == LOADING 
			|| GetState() == WAITING
			|| (m_pPool && m_pPool->GetJobById(m_pWorkInstance->GetJobId())))
        {
            vstr::outi() << " [VflUnsteadyDataLoader] - unable to start multi-threaded data load..." << endl;
            vstr::outi() << "                           thread already running..." << endl;

            return;
        }
		if (m_pPool)
		{
#ifdef DEBUG
			vstr::debugi() << " [VflUnsteadyDataLoader] - enqueueing loader into thread pool..." << endl;
#endif
			SetState(WAITING);
			m_pPool->AddWork(m_pWorkInstance);
		}
		else
		{
#ifdef DEBUG
			vstr::debugi() << " [VflUnsteadyDataLoader] - starting threaded data load..." << endl;
#endif
			m_bNeedJoin = true;
			m_pThread->Run();
		}
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ThreadBody                                                  */
/*                                                                            */
/*============================================================================*
void VflUnsteadyDataLoader::ThreadBody()
{
#ifdef DEBUG
    vstr::debugi() << " [VflUnsteadyDataLoader] - starting multi-threaded data load..." << endl;
#endif

    LoadDataThreadSafe();

    VistaMutexLock oLock(m_oMutex);
    m_bLoading = false;
}
*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetStateString                                              */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyDataLoader::GetStateString() const
{
    switch (GetState())
    {
    case READY:
        return "READY";
	case WAITING:
		return "WAITING";
    case LOADING:
        return "LOADING";
    case FINISHED_SUCCESS:
        return "FINISHED_SUCCESS";
    case FINISHED_FAILURE:
        return "FINISHED_FAILURE";
	case FINISHED_WARNING:
		return "FINISHED_WARNING";
	case ABORTING:
		return "ABORTING";
    default:
        return IVveExecutable::GetStateString();
    }
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLoadBlocking                                         */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyDataLoader::GetLoadBlocking() const{
	return m_bBlocking;
}

void VveUnsteadyDataLoader::SetLoadBlocking(const bool b){
	m_bBlocking = b;
}
/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetTarget                                                   */
/*                                                                            */
/*============================================================================*/
VveUnsteadyData *VveUnsteadyDataLoader::GetTarget() const
{
	return m_pTarget;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetThreadPool                                           */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyDataLoader::SetThreadPool(VistaThreadPool *pPool)
{
	m_pPool = pPool;
}

VistaThreadPool *VveUnsteadyDataLoader::GetThreadPool() const
{
	return m_pPool;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetState                                                    */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyDataLoader::SetState(int iState)
{
	VistaMutexLock oLock(m_oStateMutex);
	IVveExecutable::SetState(iState);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Attach/DetachObserver                                       */
/*                                                                            */
/*============================================================================*/
int VveUnsteadyDataLoader::AttachObserver(IVistaObserver *pObs, int eTicket)
{
	VistaMutexLock oLock(m_oObserverMutex);
	return IVistaObserveable::AttachObserver(pObs, eTicket);
}

int VveUnsteadyDataLoader::DetachObserver(IVistaObserver *pObs)
{
	VistaMutexLock oLock(m_oObserverMutex);
	return IVistaObserveable::DetachObserver(pObs);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Notify                                                      */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyDataLoader::Notify(int eMsg)
{
	VistaMutexLock oLock(m_oObserverMutex);
	IVistaObserveable::Notify(eMsg);
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetAbort                                                */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyDataLoader::SetAbort(bool bAbort)
{
	VistaMutexLock oLock(m_oMutex);
	m_bAbort = true;
}

bool VveUnsteadyDataLoader::GetAbort()
{
	VistaMutexLock oLock(m_oMutex);
	return m_bAbort;
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
