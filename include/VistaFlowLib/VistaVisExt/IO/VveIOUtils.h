/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEIOUTILS_H
#define _VVEIOUTILS_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <string>
#include <iostream>
#include "../Data/VveCartesianGrid.h"

#include <VistaVisExt/VistaVisExtConfig.h>

using namespace std;

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveTetGrid;
class VistaSerializer;
class VistaDeSerializer;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class VISTAVISEXTAPI VveIOUtils
{
public:
	static bool LoadCartesianGridVtkFile(VveCartesianGrid *pTarget,
		const std::string &strFileName,
		bool bPadData=false,
		int iTargetType=VveCartesianGrid::DT_FLOAT,
		const std::string &strActiveScalars="",
		const std::string &strActiveVectors="",
		const std::string &strScalarsName="",
		const std::string &strVectorsName="",
		float fNormRangeMin = 0.0f,
		float fNormRangeMax = 0.0f);

	static bool LoadTetGridVtkFile(VveTetGrid *pTarget, 
		const std::string &strFileName,
		const std::string &strOutputFileBase="", 
		bool bPadData=false,
		const std::string &strActiveScalars="", 
		const std::string &strActiveVectors="",
		const std::string &strScalarsName="", 
		const std::string &strVectorsName="",
		bool bSkipTopology=false);

	static bool LoadCartesianGridVtkImageData(VveCartesianGrid *pTarget,
		vtkImageData *pImageData,
		bool bPadData=false,
		int iTargetType=VveCartesianGrid::DT_FLOAT,
		const std::string &strActiveScalars="",
		const std::string &strActiveVectors="",
		const std::string &strScalarsName="",
		const std::string &strVectorsName="");


	/** 
	 * Read a file containing seed points for particle tracing
	 * legacy format:
	 * #pts in file
	 * pt0_x pt0_y pt0_z pt0_t
	 * pt1_x pt1_y pt1_z pt1_t
	 * ...
	 * ptn-1_x ptn-1_y ptn-1_z ptn-1_t
	 * <put an extra, empty line here!>
	 */
	static bool ReadLegacySeedFile(	const std::string& strInFile,
									double *&dSeeds,
									int &iNumSeeds);

	/** 
	 * Write a file containings seed points for particle tracing
	 * Points have to be given with 4 components each (3space+1simtime)
	 */
	static bool WriteLegacySeedFile(double dSeeds[], 
									int iNumSeeds, 
									const std::string& strOutFile);
	/**
	 * Configure time mapper pTargetTM with data read from XML time mapper
	 * specification strTimeFile. Refer to VistaVisExt/IO/timemapper_example.xml
	 */
	static bool LoadXMLTimeFile(const std::string &strTimeFile, VveTimeMapper *pTargetTM);

	/**
	 * Write the time mapper's information into an XML file that can be read
	 * by LoadXMLTimeFile().
	 * The method tries to infer an implicit time mapping (by checking whether
	 * the mapping is regular); if one is found, the information will be
	 * stored in the implicit format (else in the explicit one).
	 * If bForceImplicitTimings is true, the time mapper's implicit timings
	 * will always be used for writing. Careful: if the time mapper was defined
	 * using explicit time mappings, this will result in an invalid output.
	 */
	static bool WriteXMLTimeFile(const std::string &strTimeFile, 
		VveTimeMapper *pSourceTM, bool bForceImplicitTimings = false);

	/**
	 * serialize a time mapper to binary format
	 * NOTE: Currently, this assumes an implicit time definition
	 *       i.e. it resorts to time levels, level indices, sim times,
	 *       vis times and a fixed stride.
	 *	@todo Think about writing of explicit time info
	 */
	static int SerializeImplicitTimeMapper(	VveTimeMapper *pTM,
											IVistaSerializer &oSer);
	/**
	 * deserialize an implicit time mapper description
	 */
	static int DeSerializeImplicitTimeMapper(IVistaDeSerializer &oDeSer,
											 VveTimeMapper *pTM);

 	/**
 	 * serialize an explicit time mapper to binary format
 	 */
 	static int SerializeExplicitTimeMapper(VveTimeMapper *pTM,
 	                                        IVistaSerializer &oSer);
 
 	/**
 	 * deserialize an explicit time mapper description
 	 */
 	static int DeSerializeExplicitTimeMapper(IVistaDeSerializer &oDeSer,
 											 VveTimeMapper *pTM);


};

#endif // _VVEIOUTILS_H

/*============================================================================*/
/*  END OF FILE "VveIOUtils.h"                                                */
/*============================================================================*/
