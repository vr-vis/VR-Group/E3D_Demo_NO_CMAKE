

set( RelativeDir "./IO" )
set( RelativeSourceGroup "Source Files\\IO" )

set( DirFiles
	VtkGridProReader.cpp
	VtkGridProReader.h
	VtkGridProWriter.cpp
	VtkGridProWriter.h
	VtkMultiBlockIO.cpp
	VtkMultiBlockIO.h
	VtkMultiScalarUnstructuredGridWriter.cpp
	VtkMultiScalarUnstructuredGridWriter.h
	VveAnnotatedVtkData.cpp
	VveAnnotatedVtkData.h
	VveDataSetName.cpp
	VveDataSetName.h
	VveIOUtils.cpp
	VveIOUtils.h
	VveMultiBlockIO.cpp
	VveMultiBlockIO.h
	VvePathlineReaderBase.cpp
	VvePathlineReaderBase.h
	VvePathlineReaderPtl.cpp
	VvePathlineReaderPtl.h
	VvePathlineReaderTrk.cpp
	VvePathlineReaderTrk.h
	VvePathlineReaderVtk.cpp
	VvePathlineReaderVtk.h
	VvePathlineWriterVtk.cpp
	VvePathlineWriterVtk.h
	VveScalarDataPacket.cpp
	VveScalarDataPacket.h
	VveSerializableVtkData.cpp
	VveSerializableVtkData.h
	VveUnsteadyCartesianGridLoader.cpp
	VveUnsteadyCartesianGridLoader.h
	VveUnsteadyDataLoader.cpp
	VveUnsteadyDataLoader.h
	VveUnsteadyTetGridLoader.cpp
	VveUnsteadyTetGridLoader.h
	VveUnsteadyVtkDataLoader.cpp
	VveUnsteadyVtkDataLoader.h
	VveDatasetDescriptionXMLLoader.h
	VveDatasetDescriptionXMLLoader.cpp
	VveDatasetDescriptionXMLWriter.h
	VveDatasetDescriptionXMLWriter.cpp
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

