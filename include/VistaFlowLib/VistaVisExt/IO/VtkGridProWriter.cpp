/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName : VtkGridProWriter.CPP          RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaVisExt                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : Vista                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
#include "VtkGridProWriter.h"

#include <VistaBase/VistaStreamUtils.h>

#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/
/*============================================================================*/
/*                                                                            */
/*  NAME      :   Write                                                       */
/*                                                                            */
/*============================================================================*/
int VtkGridProWriter::Write()
{
	vtkPolyData *pMyPolyData = this->GetInput();
	vtkPoints *pPoints;
	vtkCellArray *pCellArray;
	FILE *pFile;
	pFile = fopen(this->GetFileName(), "wt");

	if(pFile)
	{
		int numberOfPoints = pMyPolyData->GetNumberOfPoints();
		fprintf(pFile, "%d\n", numberOfPoints);
		pPoints = pMyPolyData->GetPoints();
        if(!pPoints)
        {
            vstr::errp()<<"[VtkGridProWriter::Write] Unable to retrieve points!"<<endl;
            fclose(pFile);
            return 0;
        }
		double myPointData[3];
		for (int i=0;i<numberOfPoints;i++) {
			pPoints->GetPoint(i, myPointData);
			fprintf(pFile, "%f %f %f \n", myPointData[0], myPointData[1], myPointData[2]);
		}
		
		int numberOfCells = pMyPolyData->GetNumberOfCells();
		int myCellData[4];
		fprintf(pFile, "%d\n", numberOfCells);
		int nextCell;
		vtkIdType numberOfIDs;
		vtkIdType *IDs;
		pCellArray = pMyPolyData->GetPolys();
        if(!pCellArray)
        {
            vstr::errp()<<"[VtkGridProWriter::Write] Unable to retrieve cells!"<<endl;
            fclose(pFile);
            return 0;
        }
		pCellArray->InitTraversal();
		do {
			nextCell = pCellArray->GetNextCell(numberOfIDs,IDs);
			if (nextCell) {
				for (int i=0;i<numberOfIDs;i++) {
					myCellData[i] = ++(IDs[i]);
					fprintf(pFile, "%d ", myCellData[i]);
				}
				fprintf(pFile, "1\n");
			}
		} while (nextCell);

		fclose(pFile);
	}
	else
	{
		vstr::errp() << "[VtkGridProWriter::Write] UNABLE TO OPEN FILE!" << endl;
		return 0;
	}
	return 1;
}
