/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   : VveAnnotatedVtkData.h         RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   : VistaVisExt                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  : ViSTA                           Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VveAnnotatedVtkData                                              */
/*                                                                            */
/*============================================================================*/
#ifndef _VVEANNOTATEDVTKDATA_H
#define _VVEANNOTATEDVTKDATA_H
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaSerializable.h>
#include <VistaAspects/VistaPropertyAwareable.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* DEFINES                                                                   */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveSerializableVtkData;
class IVistaSerializer;
class IVistaDeSerializer;
class vtkDataSet;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
* VveAnnotatedVtkData is used for the transmission (read: serialization/
* deserialization) of annotated data of type vtkDataset. The annotation
* comprises two parts: First a raw integer index which is usually used
* to encode time step information and second a VistaPropertyList which can be 
* used to carry any additional payload in VistaPropertyList format.
*
*/
class VISTAVISEXTAPI VveAnnotatedVtkData : public IVistaSerializable
{
public:
    VveAnnotatedVtkData();
    virtual ~VveAnnotatedVtkData();
    /**
    *    Serialization/DeSerialization
    */
    virtual int Serialize(IVistaSerializer& rSerializer)const;
    virtual int DeSerialize(IVistaDeSerializer& rDeSerializer);
    virtual std::string GetSignature() const{
        return m_strSignature;
    }
    /**
    * data object access
    */
    vtkDataSet* GetData() const;
    void SetData(vtkDataSet* pData);
    
	/*
	 * MetaData access
	 */
	int GetRawIndex() const;
    void SetRawIndex(int i);
	
	void SetMetaData(const VistaPropertyList& rProps, const std::string& strMetaTag);
	void ClearMetaData();
	bool GetHasMetaData() const;
	const VistaPropertyList& GetMetaData() const;

protected:
    VveSerializableVtkData* m_pData;
    int m_iRawIndex;

	std::string m_strMetaTag;
	VistaPropertyList m_propMetaData;
	
    static const std::string m_strSignature;
};
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif //_VFLSTREAMEDPOLYHANDLER_H

