/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEPATHLINEWRITERVTK_H
#define _VVEPATHLINEWRITERVTK_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
#ifdef WIN32
    #pragma warning(disable:4786)
#endif

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaObserveable.h>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
#include "../Data/VveParticlePopulation.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VvePathlineWriterVtk saves a particle population in a vtk file (in binary
 * format).
 */    
class VISTAVISEXTAPI VvePathlineWriterVtk : public IVistaObserveable
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    /*
     * Specify the particle population to write.
     */
    VvePathlineWriterVtk( VveParticlePopulation *pPopulation);
    virtual ~VvePathlineWriterVtk();

    /**
     * Save the particle data into the file strFilename. 
     * The field names specify how the data arrays for scalar, radius, time and velocity are named.
	 * NOTE: The defined standard name for the sim time field is "sim_time"
	 *       The field with this specific semantics is assumed in several conversion steps
	 *		 between VveParticleData and VtkPolyData.
	 * NOTE: If a non-empty tensor name is passed, it is assumed that the underlying 
	 *       particle population has a tensor extension attached to every single particle
	 *		 instant!
     */
    bool Save(const std::string &strFilename,
		      const std::string &strScalarField = VveParticlePopulation::sScalarArrayDefault, 
			  const std::string &strRadiusField = VveParticlePopulation::sRadiusArrayDefault,
			  const std::string &strTimestampField = VveParticlePopulation::sTimeArrayDefault,
              const std::string &strVelocityField = VveParticlePopulation::sVelocityArrayDefault,
			  const std::string &strTensorField = VveParticlePopulation::sEigenVectorArrayDefault);

	/**
	* Observing methods
	**/
	enum
	{
		MSG_CELLPROGRESS = IVistaObserveable::MSG_LAST,
		MSG_CELLCOUNT_CHANGE,
		MSG_FINISH,
		MSG_PROGRESS,
		MSG_LAST
	};


	size_t GetCellCount() const;
	size_t GetCurrentCell() const;
	float        GetProgress() const;
	std::string  GetProgressLabel() const;

private:
	void SetCellCount(size_t nCellCount);
	void SetCurrentCell(size_t nCurrentCell);
	void SetProgress(float fProgress,
		             const std::string &sProgressLabel);

	size_t m_nCellCount;
	size_t m_nCurrentCell;

    float m_fProgress;

	VveParticlePopulation *m_pPopulation;

    std::string   m_strProgressLabel;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLPATHLINEREADERVTK_H)
