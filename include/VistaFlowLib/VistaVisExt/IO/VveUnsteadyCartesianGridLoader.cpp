/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VveUnsteadyCartesianGridLoader.h"

#include "VveDataSetName.h"
#include "VveIOUtils.h"

#include <VistaBase/VistaStreamUtils.h>
#include <VistaBase/VistaStreamManager.h>

#include <VistaVisExt/Data/VveCartesianGrid.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/Tools/VveUtils.h>

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaTools/VistaProgressBar.h>
#include <VistaTools/VistaFileSystemFile.h>

#include <vtkStructuredPointsReader.h>
#include <vtkStructuredPoints.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>

#include <cassert>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

static void gen_tornado( int xs, int ys, int zs, int time, float *tornado);


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveUnsteadyCartesianGridLoader::VveUnsteadyCartesianGridLoader(VveUnsteadyData *pTarget)
: VveUnsteadyDataLoader(pTarget),
  m_pProps(new VveUnsteadyCartesianGridLoader::CProperties())
{
}

VveUnsteadyCartesianGridLoader::~VveUnsteadyCartesianGridLoader()
{
	delete m_pProps;
	m_pProps = NULL;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::Init(const VistaPropertyList &refProps)
{
	// check whether we've got the correct kind of data...
	VveUnsteadyCartesianGrid *pData = dynamic_cast<VveUnsteadyCartesianGrid *>(m_pTarget);
	if (!pData)
	{
		vstr::errp() << "[VveUnsteadyCartesianGridLoader] invalid target data..." << endl;
		return false;
	}

	// skip sanity checks altogether - they could be modified via the reflectionable 
	// inteface anyway...

	// still retrieve information about blocking load...
	bool bBlock;
	if( refProps.GetValue("BLOCKING", bBlock) )
		SetLoadBlocking( bBlock );

	m_pProps->Lock();
	m_pProps->SetPropertiesByList(refProps);
	m_pProps->Unlock();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadDataThreadSafe                                          */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyCartesianGridLoader::LoadDataThreadSafe()
{
	SetState(LOADING);

	// check whether we've got the correct kind of data...
	VveUnsteadyCartesianGrid *pData = dynamic_cast<VveUnsteadyCartesianGrid*>(m_pTarget);

	if (!pData)
	{
		vstr::errp() << "[VveUnsteadyCartesianGridLoader] invalid target data..." << endl;
        SetState(FINISHED_FAILURE);
		return;
	}

	// retrieve data loading properties
	m_pProps->Lock();
	string strFilePattern(m_pProps->GetFilePattern());
	string strFileFormat(m_pProps->GetFileFormat());
	bool bGenerateTornado = m_pProps->GetGenerateTornado();
	bool bLoadSilently = m_pProps->GetLoadSilently();
	m_pProps->Unlock();

	if(!bLoadSilently)
	vstr::debugi() << "[VveUnsteadyCartesianGridLoader] starting data load..." << endl;


	if (bGenerateTornado)
	{
		SetState(GenerateTornado());
	}
	else
	{

		if (strFilePattern.empty())
		{
			vstr::errp() << "[VveUnsteadyCartesianGridLoader] no file name given..." << endl;
			SetState(FINISHED_FAILURE);
			return;
		}

		if (strFileFormat.empty())
		{
			// determine file format by file name extension
			string strExtension;
			size_t iExtensionStart = strFilePattern.find_last_of('.');
			if (iExtensionStart == string::npos 
				|| iExtensionStart == strFilePattern.size()-1)
			{
				vstr::errp() << "[VveUnsteadyCartesianGridLoader] unable to determine data file extension..." << endl;
				vstr::erri() << vstr::singleindent << "file name: " << strFilePattern << endl;
				SetState(FINISHED_FAILURE);
				return;
			}
			else
			{
				++iExtensionStart;
				strExtension = strFilePattern.substr(iExtensionStart);
				strExtension = VistaAspectsConversionStuff::ConvertToUpper(strExtension);
			}

			if (strExtension == "VTK")
				strFileFormat = "VTK_STRUCTUREDPOINTS";
			// to be extended...
		}
		else
		{
			strFileFormat = VistaAspectsConversionStuff::ConvertToUpper(strFileFormat);
		}

		if (strFileFormat == "VTK_STRUCTUREDPOINTS")
		{
			SetState(LoadVtkFiles(strFilePattern));
		}
		else
		{
			vstr::errp() << "[VveUnsteadyCartesianGridLoader] unknown file format '"
				<< strFileFormat << "'..." << endl;
			SetState(FINISHED_FAILURE);
		}
	}

	if (GetState() == FINISHED_SUCCESS)
	{
		if(!bLoadSilently)
		{
			vstr::outi() << "[VveUnsteadyCartesianGridLoader] grid data loaded successfully..." << endl;
		}
		// signal successful data load
		m_pTarget->Notify();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadVtkFiles                                                */
/*                                                                            */
/*============================================================================*/
int VveUnsteadyCartesianGridLoader::LoadVtkFiles(const std::string &strFilePattern)
{
	VveUnsteadyCartesianGrid *pData = dynamic_cast<VveUnsteadyCartesianGrid*>(m_pTarget);

	m_pProps->Lock();
	//deprecated: time file should no longer be used!
	//string strTimeFile(m_pProps->GetTimeFile());
	string strActiveScalars(m_pProps->GetActiveScalars());
	string strActiveVectors(m_pProps->GetActiveVectors());
	string strScalarsName(m_pProps->GetScalarsName());
	string strVectorsName(m_pProps->GetVectorsName());
	bool bProgressBar(m_pProps->GetProgressBar());
	bool bLoadSilently(m_pProps->GetLoadSilently());
	bool bPadding(m_pProps->GetPadding());

	int iType(VveCartesianGrid::DT_INVALID);
	switch (m_pProps->GetTargetType())
	{
	case CProperties::TT_FLOAT:
		iType = VveCartesianGrid::DT_FLOAT;
		break;
	case CProperties::TT_HALF:
		iType = VveCartesianGrid::DT_HALF;
		break;
	case CProperties::TT_UNSIGNED_CHAR:
		iType = VveCartesianGrid::DT_UNSIGNED_CHAR;
		break;
	}

	float aNormalizationRange[2];
	m_pProps->GetNormalizationRange(aNormalizationRange[0], aNormalizationRange[1]);
	m_pProps->Unlock();

	if(!bLoadSilently)
		vstr::debugi() << "[VflUnsteadyCartesianGridLoader] loading VTK data..." << endl;


	// determine time mapper(s)
	VveTimeMapper *pLoadMapper = pData->GetTimeMapper();


// 	if (strTimeFile.empty())
// 	{
// 		pLoadMapper = pTargetMapper;
// #ifdef DEBUG
// 		vstr::debugi() << "                                    using time mapper of data target..." << endl;
// #endif
// 	}
// 	else
// 	{
// 		vstr::outi() << "                                    using dedicated time file..." << endl;
// 		vstr::outi() << "                                    file name: " << strTimeFile << endl;
// 
// 		VflResourceKey<VflTimeMapper> oKey(strTimeFile);
// 		pLoadMapper = VflResourceManager::GetResourceManager()->GetResource(oKey, NULL)->GetRealMapper();
// 
// 		if (!pLoadMapper)
// 		{
// 			vstr::errp() << " [VflUnsteadyCartesianGridLoader] - ERROR - unable to retrieve time mapper..." << endl;
// 			return FINISHED_FAILURE;
// 		}
// 	}

	VveDataSetName oFileName;
	oFileName.Set(strFilePattern);

	int iLevel=0, iIndex;
	int iLevelCount = pLoadMapper->GetNumberOfTimeLevels();

	VistaProgressBar oBar(iLevelCount);

	if (!bLoadSilently)
	{
		if (bProgressBar)
		{
			oBar.SetOutstream(&(vstr::out()));
			oBar.SetPrefixString(vstr::GetStreamManager()->GetIndentation());
			oBar.SetBarTicks(30);
			oBar.Start();
		}
		else
		{
			oBar.SetOutstream(&(vstr::out()));
			oBar.SetPrefixString(vstr::GetStreamManager()->GetIndentation() + "finished loading - ");
			oBar.SetDisplayBar(false);
			oBar.SetSilent(true);
			oBar.Start();
		}
	}
	else
	{
		oBar.SetSilent(true);
	}


	string strFileName;
	int iWarnings = 0;

	VveCartesianGrid *pTargetData = NULL;

	for (int i=0; i<iLevelCount; ++i)
	{
		if (GetAbort())
		{
			iWarnings = 1;
			break;
		}

		// find time level to be loaded
		iLevel = pLoadMapper->GetTimeLevelForLevelIndex(i);
		strFileName = oFileName.GetFileName(iLevel);

		// find time level to be written to
		iIndex = pLoadMapper->GetLevelIndexForTimeLevel(iLevel);
		if (iIndex < 0)
		{
			vstr::warnp() << "[VveUnsteadyCartesianGridLoader] unable to find target data..." << endl;
			vstr::warni() << vstr::singleindent << "time level: " << iLevel << endl;
			++iWarnings;
			continue;
		}

		if(!VveIOUtils::LoadCartesianGridVtkFile(pData->GetTypedLevelDataByLevelIndex(iIndex)->GetData(), strFileName,
			bPadding, iType, strActiveScalars, strActiveVectors, strScalarsName, 
			strVectorsName, aNormalizationRange[0], aNormalizationRange[1]))
		{
			++iWarnings;
			continue;
		}

		// retrieve data target
		pData->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
		pTargetData = pData->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

		if (!pTargetData)
		{
			vstr::warnp() << "[VveUnsteadyCartesianGridLoader] unable to find data target..." << endl;
			++iWarnings;
			pData->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
			continue;
		}
		pData->GetTypedLevelDataByLevelIndex(iIndex)->Notify();
		pData->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

		if (!bLoadSilently)
			oBar.Increment();
	}
	if (!bLoadSilently)
	{
		oBar.SetSilent(false);
		oBar.Finish();
	}

	if (iWarnings > 0)
		return FINISHED_WARNING;

	return FINISHED_SUCCESS;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GenerateTornado                                             */
/*                                                                            */
/*============================================================================*/
int VveUnsteadyCartesianGridLoader::GenerateTornado() 
{

	VveUnsteadyCartesianGrid *pData = dynamic_cast<VveUnsteadyCartesianGrid*>(m_pTarget);

	m_pProps->Lock();
	VistaVector3D v3Dims(m_pProps->GetTornadoDimensions());
	string strTimeFile(m_pProps->GetTimeFile());
	bool bProgressBar = m_pProps->GetProgressBar();
	bool bLoadSilently  = m_pProps->GetLoadSilently();
	m_pProps->Unlock();

	if(!bLoadSilently)
		vstr::debugi() << "[VveUnsteadyCartesianGridLoader] generating tornado..." << endl;

	// determine time mapper(s)
	VveTimeMapper *pLoadMapper = pData->GetTimeMapper();
	assert(pLoadMapper);

// 	if (strTimeFile.empty())
// 	{
// 		pLoadMapper = new VflTimeMapper(pTargetMapper);
// #ifdef DEBUG
// 		vstr::debugi() << "using time mapper of data target..." << endl;
// #endif
// 	}
// 	else
// 	{
// 		vstr::debugi() << "using dedicated time file..." << endl;
// 		vstr::debugi() << "file name: " << strTimeFile << endl;
// 
// 		VflResourceKey<VflTimeMapper> oKey(strTimeFile);
// 		pLoadMapper = VflResourceManager::GetResourceManager()->GetResource(oKey, NULL);
// 
// 		if (!pLoadMapper)
// 		{
// 			vstr::errp() << "[VflUnsteadyCartesianGridLoader] unable to retrieve time mapper..." << endl;
// 			return FINISHED_FAILURE;
// 		}
// 	}

	int aDimensions[3];
	int iPointCount;

	aDimensions[0] = (int)floor(v3Dims[0]);
	aDimensions[1] = (int)floor(v3Dims[1]);
	aDimensions[2] = (int)floor(v3Dims[2]);

	if (aDimensions[0] <= 0 || aDimensions[1] <= 0 || aDimensions[2] <= 0)
	{
		vstr::errp() << "[VveUnsteadyCartesianGridLoader] invalid tornado dimensions..." << endl;
		vstr::IndentObject oIndent;
		vstr::erri() << "dimensions: " << aDimensions[0] << "x"
					 << aDimensions[1] << "x" << aDimensions[2] << endl;
		return FINISHED_FAILURE;
	}
	iPointCount = aDimensions[0]*aDimensions[1]*aDimensions[2];

	int iLevel=0, iIndex;
	int iLevelCount = pLoadMapper->GetNumberOfTimeLevels();

	VistaProgressBar oBar(iLevelCount);
	if (!bLoadSilently)
	{
		if (bProgressBar)
		{
			oBar.SetPrefixString(vstr::GetStreamManager()->GetIndentation());
			oBar.SetBarTicks(30);
			oBar.Start();
		}
		else
		{
			oBar.SetPrefixString(vstr::GetStreamManager()->GetIndentation() + "finished loading - ");
			oBar.SetDisplayBar(false);
			oBar.SetSilent(true);
			oBar.Start();
		}
	}
	else
	{
		oBar.SetSilent(true);
	}

	int iWarnings = 0;
	VveCartesianGrid *pTargetData = NULL;

	for (int i=0; i<iLevelCount; ++i)
	{
		if (GetAbort())
		{
			iWarnings = 1;
			break;
		}

		// find time level to be loaded
		iLevel = pLoadMapper->GetTimeLevelForLevelIndex(i);

		// find time level to be written to
		iIndex = pLoadMapper->GetLevelIndexForTimeLevel(iLevel);
		if (iIndex < 0)
		{
			vstr::warnp() << "[VveUnsteadyCartesianGridLoader] unable to find target data..." << endl;
			vstr::warni() << vstr::singleindent << " time level: " << iLevel << endl;
			++iWarnings;
			continue;
		}

		int iComponents = 4;

		// retrieve data target
		pData->GetLevelDataByLevelIndex(iIndex)->LockData();
		pTargetData = pData->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

		if (!pTargetData)
		{
			vstr::warnp() << "[VveUnsteadyCartesianGridLoader] unable to find data target..." << endl;
			++iWarnings; 
			pData->GetLevelDataByLevelIndex(iIndex)->UnlockData();
			continue;
		}

		// re-allocate memory, if necessary
		int x, y, z, iTemp;
		pTargetData->GetDimensions(x, y, z);
		iTemp = pTargetData->GetComponents();

		if (x!=aDimensions[0] || y!=aDimensions[1] || z!=aDimensions[2] || iTemp!=iComponents)
		{
			pTargetData->Reset(aDimensions[0], aDimensions[1], aDimensions[2], iComponents, VveCartesianGrid::DT_FLOAT);
		}

		// write data to into cartesian grid
		gen_tornado(aDimensions[0], aDimensions[1], aDimensions[2], 
			iIndex, pTargetData->GetDataAsFloat());

		float *pPos = pTargetData->GetDataAsFloat();
		float aRange[2];
		for (int j=0; j<iPointCount; ++j)
		{
			pPos[3] = sqrt(pPos[0]*pPos[0]+pPos[1]*pPos[1]+pPos[2]*pPos[2]);

			if (j==0)
			{
				aRange[0] = aRange[1] = pPos[3];
			}
			else
			{
				if (pPos[3] < aRange[0])
					aRange[0] = pPos[3];
				else if (pPos[3] > aRange[1])
					aRange[1] = pPos[3];
			}
			pPos += 4;
		}

		pTargetData->SetBounds(VistaVector3D(0, 0, 0), VistaVector3D(1, 1, 1));
		pTargetData->SetValid(true);
		pTargetData->SetScalarRange(aRange);
		pData->GetLevelDataByLevelIndex(iIndex)->Notify();
		pData->GetLevelDataByLevelIndex(iIndex)->UnlockData();
		
		if (!bLoadSilently) oBar.Increment();
	}
	if (!bLoadSilently)
	{
		oBar.SetSilent(false);
		oBar.Finish();
	}


	if (iWarnings > 0)
		return FINISHED_WARNING;

	return FINISHED_SUCCESS;}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VveUnsteadyCartesianGridLoader::CProperties *VveUnsteadyCartesianGridLoader::GetProperties() const
{
	return m_pProps;
}

/*============================================================================*/
/*  IMPLEMENTATION OF VflUnsteadyCartesianGridLoader::CProperties            */
/*============================================================================*/

/*============================================================================*/
/*  STATICS AND FUNCTORS                                                      */
/*============================================================================*/
static std::string s_strCUnsteadyCartGridLdrPropsReflType("VveUnsteadyCartesianGridLoader::CProperties");

static IVistaPropertyGetFunctor *s_aUnsteadyCartGridLdrPropsGetFunctors[] =
{
	new TVistaPropertyGet<std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("FILE_NAME", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetFilePattern),
	new TVistaPropertyGet<std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("FORMAT", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetFileFormat),
	new TVistaPropertyGet<std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("TIME_FILE", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetTimeFile),
	new TVistaPropertyGet<bool, VveUnsteadyCartesianGridLoader::CProperties>
		("PROGRESS_BAR", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetProgressBar),
	new TVistaPropertyGet<bool, VveUnsteadyCartesianGridLoader::CProperties>
		("PADDING", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetPadding),
	new TVistaPropertyGet<std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("TARGET_TYPE", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetTargetTypeAsString),
	new TVistaPropertyGet<std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("SCALARS_NAME", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetScalarsName),
	new TVistaPropertyGet<std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("VECTORS_NAME", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetVectorsName),
	new TVistaPropertyGet<std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("ACTIVE_SCALARS", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetActiveScalars),
	new TVistaPropertyGet<std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("ACTIVE_VECTORS", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetActiveVectors),
	new TVistaProperty2RefGet<float, VveUnsteadyCartesianGridLoader::CProperties>
		("NORMALIZATION_RANGE", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetNormalizationRange),
	new TVistaPropertyGet<VistaVector3D, VveUnsteadyCartesianGridLoader::CProperties>
		("TORNADO_DIMENSIONS", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetTornadoDimensions ),
	new TVistaPropertyGet<bool, VveUnsteadyCartesianGridLoader::CProperties>
		("GENERATE_TORNADO", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::GetGenerateTornado),
	NULL
};

static IVistaPropertySetFunctor *s_aUnsteadyCartGridLdrPropsSetFunctors[] =
{
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("FILE_NAME", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetFilePattern),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("FORMAT", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetFileFormat),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("TIME_FILE", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetTimeFile),
	new TVistaPropertySet<bool, bool, VveUnsteadyCartesianGridLoader::CProperties>
		("PROGRESS_BAR", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetProgressBar),
	new TVistaPropertySet<bool, bool, VveUnsteadyCartesianGridLoader::CProperties>
		("PADDING", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetPadding),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("TARGET_TYPE", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetTargetType),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("SCALARS_NAME", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetScalarsName),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("VECTORS_NAME", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetVectorsName),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("ACTIVE_SCALARS", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetActiveScalars),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyCartesianGridLoader::CProperties>
		("ACTIVE_VECTORS", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetActiveVectors),
	new TVistaProperty2ValSet<float, VveUnsteadyCartesianGridLoader::CProperties>
		("NORMALIZATION_RANGE", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetNormalizationRange),
	new TVistaPropertySet<const VistaVector3D&, VistaVector3D, VveUnsteadyCartesianGridLoader::CProperties>
		("TORNADO_DIMENSIONS", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetTornadoDimensions ),
	new TVistaPropertySet<bool, bool, VveUnsteadyCartesianGridLoader::CProperties>
		("GENERATE_TORNADO", s_strCUnsteadyCartGridLdrPropsReflType,
		&VveUnsteadyCartesianGridLoader::CProperties::SetGenerateTornado),
	NULL
};


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveUnsteadyCartesianGridLoader::CProperties::CProperties()
:	m_bProgressBar(true)
,	m_bLoadSilently(false)
,	m_bPadding(true)
,	m_iTargetType(TT_FLOAT)
,	m_bGenerateTornado(false)
{
	m_aNormalizationRange[0] = m_aNormalizationRange[1] = 0.0f;
}

VveUnsteadyCartesianGridLoader::CProperties::~CProperties()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFilePattern                                          */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetFilePattern(const std::string &str)
{
	if (m_strFilePattern == str)
		return false;
	m_strFilePattern = str;
	Notify();
	return true;
}

std::string VveUnsteadyCartesianGridLoader::CProperties::GetFilePattern() const
{
	return m_strFilePattern;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFileFormat                                           */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetFileFormat(const std::string &str)
{
	if (m_strFileFormat == str)
		return false;
	m_strFileFormat = str;
	Notify();
	return true;
}

std::string VveUnsteadyCartesianGridLoader::CProperties::GetFileFormat() const
{
	return m_strFileFormat;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTimeFile                                             */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetTimeFile(const std::string &str)
{
	if (m_strTimeFile == str)
		return false;
	m_strTimeFile = str;
	vstr::warnp() << "[VveUnsteadyCartesianGridLoader] SetTimeFile is deprecated!" << endl;
	//Notify();
	return true;
}

std::string VveUnsteadyCartesianGridLoader::CProperties::GetTimeFile() const
{
	return m_strTimeFile;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetProgressBar                                          */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetProgressBar(bool b)
{
	if (m_bProgressBar == b)
		return false;
	m_bProgressBar = b;
	Notify();
	return true;
}

bool VveUnsteadyCartesianGridLoader::CProperties::GetProgressBar() const
{
	return m_bProgressBar;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetLoadSilently                                           */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetLoadSilently(bool b)
{
	if (m_bLoadSilently == b)
		return false;
	m_bLoadSilently = b;
	Notify();
	return true;
}

bool VveUnsteadyCartesianGridLoader::CProperties::GetLoadSilently() const
{
	return m_bLoadSilently;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetPadding                                              */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetPadding(bool b)
{
	if (m_bPadding == b)
		return false;
	m_bPadding = b;
	Notify();
	return true;
}

bool VveUnsteadyCartesianGridLoader::CProperties::GetPadding() const
{
	return m_bPadding;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTargetType                                           */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetTargetType(int iFormat)
{
	if (iFormat == m_iTargetType)
		return false;

	if (iFormat<=TT_INVALID || iFormat>=TT_LAST)
		iFormat = TT_INVALID;

	m_iTargetType = iFormat;
	Notify();
	return true;
}

bool VveUnsteadyCartesianGridLoader::CProperties::SetTargetType(const std::string &strFormat)
{
	string strTemp = VistaAspectsConversionStuff::ConvertToLower(strFormat);
	int iFormat = TT_INVALID;
	if (strTemp == "float")
		iFormat = TT_FLOAT;
	else if (strTemp == "half")
		iFormat = TT_HALF;
	else if (strTemp == "unsigned_char")
		iFormat = TT_UNSIGNED_CHAR;

	return SetTargetType(iFormat);
}

int VveUnsteadyCartesianGridLoader::CProperties::GetTargetType() const
{
	return m_iTargetType;
}

std::string VveUnsteadyCartesianGridLoader::CProperties::GetTargetTypeAsString() const
{
	switch (m_iTargetType)
	{
	case TT_INVALID:
		return "INVALID";
	case TT_FLOAT:
		return "FLOAT";
	case TT_HALF:
		return "HALF";
	case TT_UNSIGNED_CHAR:
		return "UNSIGNED_CHAR";
	}

	return "UNKNOWN";
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetActiveVectors                                        */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetActiveVectors(const std::string &str)
{
	if (m_strActiveVectors == str)
		return false;
	m_strActiveVectors = str;
	Notify();
	return true;
}

std::string VveUnsteadyCartesianGridLoader::CProperties::GetActiveVectors() const
{
	return m_strActiveVectors;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetActiveScalars                                        */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetActiveScalars(const std::string &str)
{
	if (m_strActiveScalars == str)
		return false;
	m_strActiveScalars = str;
	Notify();
	return true;
}

std::string VveUnsteadyCartesianGridLoader::CProperties::GetActiveScalars() const
{
	return m_strActiveScalars;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetScalarsName                                          */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetScalarsName(const std::string &str)
{
	if (m_strScalarsName == str)
		return false;
	m_strScalarsName = str;
	Notify();
	return true;
}

std::string VveUnsteadyCartesianGridLoader::CProperties::GetScalarsName() const
{
	return m_strScalarsName;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetVectorsName                                          */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetVectorsName(const std::string &str)
{
	if (m_strVectorsName == str)
		return false;
	m_strVectorsName = str;
	Notify();
	return true;
}

std::string VveUnsteadyCartesianGridLoader::CProperties::GetVectorsName() const
{
	return m_strVectorsName;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetNormalizationRange                                   */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetNormalizationRange(float fMin, 
																		 float fMax)
{
	bool bChange = (compAndAssignFunc<float>(fMin, m_aNormalizationRange[0])==1) ? true : false;
	bChange |= (compAndAssignFunc<float>(fMax, m_aNormalizationRange[1])==1) ? true : false;

	if (bChange)
	{
		Notify();
		return true;
	}

	return false;
}

bool VveUnsteadyCartesianGridLoader::CProperties::GetNormalizationRange(float &fMin, 
																		 float &fMax) const
{
	fMin = m_aNormalizationRange[0];
	fMax = m_aNormalizationRange[1];
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTornadoDimensions                                    */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetTornadoDimensions(const VistaVector3D &v3Dims)
{
	if (v3Dims == m_v3TornadoDimensions)
		return false;
	m_v3TornadoDimensions = v3Dims;
	Notify();
	return true;
}

VistaVector3D VveUnsteadyCartesianGridLoader::CProperties::GetTornadoDimensions() const
{
	return m_v3TornadoDimensions;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetGenerateTornado                                      */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyCartesianGridLoader::CProperties::SetGenerateTornado(bool bGenerate)
{
	if (bGenerate == m_bGenerateTornado)
		return false;
	m_bGenerateTornado = bGenerate;
	Notify();
	return true;
}

bool VveUnsteadyCartesianGridLoader::CProperties::GetGenerateTornado() const
{
	return m_bGenerateTornado;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Lock                                                        */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyCartesianGridLoader::CProperties::Lock()
{
	m_oMutex.Lock();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Unlock                                                      */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyCartesianGridLoader::CProperties::Unlock()
{
	m_oMutex.Unlock();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyCartesianGridLoader::CProperties::GetReflectionableType() const
{
	return s_strCUnsteadyCartGridLdrPropsReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VveUnsteadyCartesianGridLoader::CProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCUnsteadyCartGridLdrPropsReflType);
	return i+1;
}


/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/
static void gen_tornado( int xs, int ys, int zs, int time, float *tornado )
/*
 *  Gen_Tornado creates a vector field of dimension [xs,ys,zs,3] from
 *  a proceedural function. By passing in different time arguements,
 *  a slightly different and rotating field is created.
 *
 *  The magnitude of the vector field is highest at some funnel shape
 *  and values range from 0.0 to around 0.4 (I think).
 *
 *  I just wrote these comments, 8 years after I wrote the function.
 *  
 * Developed by Roger A. Crawfis, The Ohio State University
 *
 */
{
  float x, y, z;
  int ix, iy, iz;
  float r, xc, yc, scale, temp, z0;
  float r2 = 8;
  float SMALL = 0.00000000001;
  float xdelta = 1.0 / (xs-1.0);
  float ydelta = 1.0 / (ys-1.0);
  float zdelta = 1.0 / (zs-1.0);

  for( iz = 0; iz < zs; iz++ )
  {
     z = iz * zdelta;                        // map z to 0->1
     xc = 0.5 + 0.1*sin(0.04*time+10.0*z);   // For each z-slice, determine the spiral circle.
     yc = 0.5 + 0.1*cos(0.03*time+3.0*z);    //    (xc,yc) determine the center of the circle.
     r = 0.1 + 0.4 * z*z + 0.1 * z * sin(8.0*z); //  The radius also changes at each z-slice.
     r2 = 0.2 + 0.1*z;                           //    r is the center radius, r2 is for damping
     for( iy = 0; iy < ys; iy++ )
     {
		y = iy * ydelta;
		for( ix = 0; ix < xs; ix++ )
		{
			x = ix * xdelta;
			temp = sqrt( (y-yc)*(y-yc) + (x-xc)*(x-xc) );
			scale = fabs( r - temp );
/*
 *  I do not like this next line. It produces a discontinuity 
 *  in the magnitude. Fix it later.
 *
 */
           if ( scale > r2 )
              scale = 0.8 - scale;
           else
              scale = 1.0;
			z0 = 0.1 * (0.1 - temp*z );
		   if ( z0 < 0.0 )  z0 = 0.0;
		   temp = sqrt( temp*temp + z0*z0 );
			scale = (r + r2 - temp) * scale / (temp + SMALL);
			scale = scale / (1+z);
           *tornado++ = scale * (y-yc) + 0.1*(x-xc);
           *tornado++ = scale * -(x-xc) + 0.1*(y-yc);
           *tornado++ = scale * z0;

		   // edit [ms]: go for 4 components
		   ++tornado;
		}
     }
  }
}


/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
