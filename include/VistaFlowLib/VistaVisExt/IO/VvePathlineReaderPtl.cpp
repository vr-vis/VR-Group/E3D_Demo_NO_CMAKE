/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VvePathlineReaderPtl.h"
#include <cassert>
#include <iostream>
#include <cmath>
#include <vtkByteSwap.h>
#include <VistaTools/VistaProfiler.h>
#include <VistaVisExt/Data/VveParticleTrajectory.h>

#include <VistaBase/VistaStreamUtils.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

static const char* SPINNER = "-\\|/";

#ifdef WIN32
#include <float.h>
#define isnan(x) _isnan(x)
#endif

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VvePathlineReaderPtl::VvePathlineReaderPtl(std::string strFilename)
: VvePathlineReaderBase(strFilename )
{
    // check file for validity
    VistaProfiler oProf;

    if (!oProf.GetTheProfileSection("PARTICLE_DATA_FILE", m_strFilename)
        || !oProf.GetTheProfileSection("DATA", m_strFilename))
    {
        vstr::errp() << " [VflPathlineReaderPtl] unable to retrieve data from file..." << endl;
        vstr::erri() << "                   file name: " << m_strFilename << endl;
        vstr::erri() << "                   Make sure that the tags [PARTICLE_DATA_FILE] and [DATA] are present..." << endl;
        return;
    }
    vstr::outi() << " [VflPathlineReaderPtl] - loading particle data file..." << endl;
    vstr::outi() << "                   file name: " << m_strFilename << endl;


    std::string strTemp;
    oProf.GetTheProfileString("PARTICLE_DATA_FILE", "VERSION", "", strTemp, m_strFilename);
    if (strTemp != "1.0")
    {
        vstr::errp() << " [VflPathlineReaderPtl] unknown particle file version..." << endl;
        vstr::erri() << "                   file name: " << m_strFilename << endl;
        vstr::erri() << "                   version: " << strTemp << endl;
        return;
    }
    vstr::outi() << "                   version: " << strTemp << endl;

    oProf.GetTheProfileString("PARTICLE_DATA_FILE", "FORMAT", "", strTemp, m_strFilename);
    strTemp = oProf.ToUpper(strTemp);
    if (strTemp == "BINARY")
    {
        m_bBinary = true;
    }
    else if (strTemp == "ASCII")
    {
        m_bBinary = false;
    }
    else
    {
        vstr::errp() << " [VflPathlineReaderPtl] unable to determine data file format..." << endl;
        vstr::erri() << "                   file name: " << m_strFilename << endl;
        return;
    }

    oProf.GetTheProfileString("PARTICLE_DATA_FILE", "TYPE", "", strTemp, m_strFilename);
    strTemp = oProf.ToUpper(strTemp);
    if (strTemp == "DROPLETS" || strTemp == "DROPLET_DATA")
        m_eType = DROPLETS;
    else if (strTemp == "PARTICLES" || strTemp == "PARTICLE_DATA")
        m_eType = PARTICLES;
    else
    {
        vstr::errp() << " [VflPathlineReaderPtl] unable to determine data type..." << endl;
        vstr::erri() << "                   file name: " << m_strFilename << endl;
        vstr::erri() << "                   type: " << strTemp << endl;
        return;
    }

    m_fScale = oProf.GetTheProfileFloat("PARTICLE_DATA_FILE", "SCALE_FACTOR", 1.0f, m_strFilename);

    vstr::outi() << "                   type: " << (m_eType==PARTICLES?"PARTICLE_DATA":"DROPLET_DATA") << endl;
    vstr::outi() << "                   format: " << (m_bBinary?"BINARY":"ASCII") << endl;
    vstr::outi() << "                   scale factor: " << m_fScale << endl;
}

VvePathlineReaderPtl::~VvePathlineReaderPtl()
{
    EmptyCache();
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadDataFromFile                                            */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderPtl::LoadDataFromFile()
{
    if (!m_liData.empty())
    {
        vstr::warnp() << " [VflPathlineReaderPtl] skipping "
			  << " file load due to full cache..." << endl;
        return false;
    }

    ifstream is;

    if (m_bBinary)
    {
        is.open(m_strFilename.c_str(), ios::in | ios::binary);
    }
    else
    {
        is.open(m_strFilename.c_str(), ios::in);
    }

    if (is.bad() || !is.is_open())
    {
        vstr::errp() << " [VflPathlineReaderPtl] unable to open particle data file..." << endl;
        vstr::erri() << "                   file name: " << m_strFilename << endl;
        return false;
    }

    char buf[256];

    // skip file header
    do
    {
        is.getline(buf, 256);
    }
    while (strncmp(buf, "[DATA]", 6) && !is.eof());

    if (is.eof())
    {
        vstr::errp() << " [VflPathlineReaderPtl] unable to find [DATA] section..." << endl;
        vstr::erri() << "                   file name: " << m_strFilename << endl;
        return false;
    }

    // now, read in all the particle data, i.e. build a HUGE list ;-)
    vstr::outi() << " [VflPathlineReaderPtl] - starting data import...";
	vstr::outi().flush();

    STemporaryParticleData sTPD;
    sTPD.iIndex = 0;    // avoid processing of the first, empty structure...

    int iDataCount = 0;
    int iSpinnerCount = 0;
    int iNumberOfIndices=0;

    float fLastTime = -1;
    float fTemp;

    if (m_bBinary)
    {
        while (!is.eof() && is.good())
        {
            if ((++iDataCount)%10000 == 0)
            {
                vstr::outi() << SPINNER[(++iSpinnerCount)%4] << "\b";
				vstr::outi().flush();
            }

            if (iNumberOfIndices < sTPD.iIndex)
                iNumberOfIndices = sTPD.iIndex;

            // is the particle alive? (and skip the first (illegal) one...)
            if (sTPD.iIndex > 0)
            {
                // particle indices start with 1 in the data file
                --sTPD.iIndex;
                m_liData.push_back(sTPD);
            }

            // read ptl data
            // find first integer number (for the particle index)
            do
            {
                is.read((char *)&fTemp, sizeof(float));
                vtkByteSwap::Swap4BE((char*)&fTemp);
            }
            while (((float)(int)fTemp) != fTemp && !is.eof());

            if (is.eof())
            {
                break;
            }

            sTPD.iIndex = (int)fTemp;

            // read the rest of the data
            is.read((char*)sTPD.aPos, 6*sizeof(float));    // postition and velocity
            is.read((char*)&sTPD.fTime, sizeof(float));    // time

            if (m_eType == DROPLETS)
            {
                is.read((char*)&sTPD.fTemperature, 4*sizeof(float));    // temperature, diameter, mass, and count
            }
            vtkByteSwap::Swap4BERange((char*) &sTPD.aPos, sizeof(STemporaryParticleData)/4-1);

            // scale positions
            sTPD.aPos[0] *= m_fScale;
            sTPD.aPos[1] *= m_fScale;
            sTPD.aPos[2] *= m_fScale;

            sTPD.iType = sTPD.iCell = 0;
            if (m_eType != DROPLETS)
            {
                sTPD.fTemperature = sTPD.fDiameter = sTPD.fMass = sTPD.fCount = 0;
            }
        }
    }
    else
    {
        while (!is.eof() && is.good())
        {
            if ((++iDataCount)%10000 == 0)
            {
                vstr::outi() << SPINNER[(++iSpinnerCount)%4] << "\b";
				vstr::outi().flush();
            }

            if (iNumberOfIndices < sTPD.iIndex)
                iNumberOfIndices = sTPD.iIndex;

            // is the particle alive? (and skip the first (illegal) one...)
            if (sTPD.iIndex > 0)
            {
                // particle indices start with 1 in the data file
                --sTPD.iIndex;
                m_liData.push_back(sTPD);
            }

            is >> fTemp;

            if (is.eof())
            {
                break;
            }

            sTPD.iIndex = (int) fTemp;

            is >> sTPD.aPos[0] >> sTPD.aPos[1] >> sTPD.aPos[2];
            is >> sTPD.aVel[0] >> sTPD.aVel[1] >> sTPD.aVel[2];
            is >> sTPD.fTime;

            if (m_eType == DROPLETS)
            {
                is >> sTPD.fTemperature >> sTPD.fDiameter >> sTPD.fMass >> sTPD.fCount;
            }
            else
            {
                sTPD.fTemperature = sTPD.fDiameter = sTPD.fMass = sTPD.fCount = 0;
            }
            // scale positions
            sTPD.aPos[0] *= m_fScale;
            sTPD.aPos[1] *= m_fScale;
            sTPD.aPos[2] *= m_fScale;
        }
    }
    vstr::outi() << " " << endl;

    if (!is.eof())
    {
        vstr::warnp() << " [VflPathlineReaderPtl] problems with data file..." << endl;
    }
    is.close();

    vstr::outi() << " [VflPathlineReaderPtl] - " << iDataCount << " particle positions read..." << endl;
    vstr::outi() << "                   number of particle indices: " << iNumberOfIndices << endl;

    // now, we try to find out, which particle indices are not used and collapse
    // the indices accordingly
    vstr::outi() << " [VflPathlineReaderPtl] - collapsing particle indices..." << endl;

    std::vector<int> vecIndexUsage;
    vecIndexUsage.resize(iNumberOfIndices);
    int i;
    for (i=0; i<iNumberOfIndices; ++i)
        vecIndexUsage[i] = 0;

    // check, which indices are in use
    std::list<STemporaryParticleData>::iterator itTPD = m_liData.begin();
    while (itTPD != m_liData.end())
    {
        ++vecIndexUsage[itTPD->iIndex];
        ++itTPD;
    }

    // now, build a table, which associates a particles index before the collapse
    // with the new index after the collapse
    int iIndexCountAfterCollapse = 0;
    for (i=0; i<iNumberOfIndices; ++i)
    {
        if (vecIndexUsage[i]>0)
        {
            // this index is in use, so store its new compressed index
            vecIndexUsage[i] = iIndexCountAfterCollapse++;
        }
        else
        {
            // this is just for some error checking later on...
            vecIndexUsage[i] = -1;
        }
    }

    // collapse the particle indices by the help of the table built above
    int iNewIndex;
    itTPD = m_liData.begin();
    while (itTPD != m_liData.end())
    {
        iNewIndex = vecIndexUsage[itTPD->iIndex];

        if (iNewIndex < 0)
        {
            vstr::errp() << " [VflPathlineReaderPtl] something went terribly wrong during" << endl;
            vstr::erri() << "                 particle index collapse..." << endl;
            vstr::erri() << "                 This is pretty serious shouldn't happen at all - ever!" << endl;

            m_liData.clear();
            m_eType = UNKNOWN;
            return false;
        }
        itTPD->iIndex = iNewIndex;

        ++itTPD;
    }
    vstr::outi() << " [VflPathlineReaderPtl] - number of particle indices after collapse: " << iIndexCountAfterCollapse << endl;

    // finally, find out, how many time levels every particle has...
    // I know, we could have done that before, but it might be a little
    // easier to understand this way... ;-)
    vstr::outi() << " [VflPathlineReaderPtl] - counting particle time levels..." << endl;
    m_vecTimeLevelCounts.resize(iIndexCountAfterCollapse);
    for (i=0; i<iIndexCountAfterCollapse; ++i)
    {
        m_vecTimeLevelCounts[i] = 0;
    }

    itTPD = m_liData.begin();
    while (itTPD != m_liData.end())
    {
        ++m_vecTimeLevelCounts[itTPD->iIndex];
        ++itTPD;
    }

    return true;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
