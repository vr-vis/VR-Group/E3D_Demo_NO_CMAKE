/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#include "VveDataSetName.h"

//#include <sstream>
#include <VistaAspects/VistaAspectsUtils.h>
#include <iostream>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
// always put this line below your constant definitions
// to avoid problems with HP's compiler
using namespace std;


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveDataSetName::VveDataSetName()
	:	m_strRootDirectory("YOU_NEED_TO"),
		m_strFileNamePrefix("SPECIFY_A"),
		m_strFileNamePostfix("CORRECT_TEMPLATE"),
		m_strFileNameExtension(".vtk"),
		m_strBlockPrefix("_"),
		m_strTimePrefix("_"),
		m_bUseBlock(true),
		m_bUseTime(true),
		m_nBlockDigits(0),
		m_nTimeDigits(0),
		m_bTimeFirst(true),
		m_bFillTimeNumZeros(true),
		m_bFillBlockNumZeros(true),
		m_bOmitTimeZeroTerms(false),
		m_bOmitBlockZeroTerms(false)
{
}

VveDataSetName::VveDataSetName(const std::string& strPattern)
	//just make sure everything is initialized correctly!
	:	m_strRootDirectory(""),
		m_strFileNamePrefix(""),
		m_strFileNamePostfix(""),
		m_strFileNameExtension(""),
		m_strBlockPrefix("_"),
		m_strTimePrefix("_"),
		m_bUseBlock(true),
		m_bUseTime(true),
		m_nBlockDigits(0),
		m_nTimeDigits(0),
		m_bTimeFirst(true),
		m_bFillTimeNumZeros(true),
		m_bFillBlockNumZeros(true),
		m_bOmitTimeZeroTerms(false),
		m_bOmitBlockZeroTerms(false)
{
	//overwrite the predefined stuff
	this->Set(strPattern);
}

VveDataSetName::~VveDataSetName()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFileName                                                 */
/*                                                                            */
/*============================================================================*/
std::string VveDataSetName::GetFileName (int nTimeStep, int nBlockNum) const
{
    /*
	std::ostringstream strBlock;
    std::ostringstream strTime;
    std::ostringstream strBlockPur;
    std::ostringstream strTimePur;
	*/
	std::string strBlock("");
	std::string strTime("");
	std::string strBlockPur("");
	std::string strTimePur("");

    // determine string length
	strBlockPur = VistaAspectsConversionStuff::ConvertToString(nBlockNum);
    strTimePur  = VistaAspectsConversionStuff::ConvertToString(nTimeStep);

    if (m_bUseTime)
    {
		//check if time data can be omitted
        if(!m_bOmitTimeZeroTerms || nTimeStep>0)
		{
			//insert time prefix
			strTime += m_strTimePrefix;
			if(m_bFillTimeNumZeros)
			{
				// fill with 0s
                if( m_nTimeDigits > strTimePur.length() )
				{
					const size_t numDigitsToFill =
						m_nTimeDigits - strTimePur.length();
					
					for(size_t curDigi=0; curDigi<numDigitsToFill; ++curDigi)
					{
						strTime += string("0");
					}
				}
			}
			strTime += VistaAspectsConversionStuff::ConvertToString(nTimeStep);
		}
    }

    if (m_bUseBlock)
    {
		//check if block data can be omitted
		if(!m_bOmitBlockZeroTerms || nBlockNum>0)
		{
			//insert block prefix
			strBlock += m_strBlockPrefix;
			if(m_bFillBlockNumZeros)
			{
				//fill with 0
				if( m_nBlockDigits > strBlockPur.length() )
				{
					const size_t numDigitsToFill =
						m_nBlockDigits - strBlockPur.length();
					
					for (size_t curDigi=0; curDigi<numDigitsToFill; ++curDigi)
					{
						strBlock += string("0");
					}
				}
			}
			strBlock += VistaAspectsConversionStuff::ConvertToString(nBlockNum);
		}
    }

    // concatenate filename
//    std::ostringstream strFileName;
	std::string strFileName(m_strRootDirectory);
    strFileName += m_strFileNamePrefix;
    if (m_bTimeFirst)
    {
        strFileName += strTime;
		strFileName += strBlock; 
    }
    else
    {
        strFileName += strBlock;
		strFileName += strTime;
    }
    strFileName += m_strFileNamePostfix;
	strFileName += m_strFileNameExtension;

    // return result string
    return strFileName;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFile                                                     */
/*                                                                            */
/*============================================================================*/
std::string VveDataSetName::GetFile (int nTimeStep, int nBlockNum) const
{
	std::string filename(GetFileName(nTimeStep,nBlockNum));
	filename.erase(0, filename.find_last_of("/")+1);
	return filename;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetFilePattern                                              */
/*                                                                            */
/*============================================================================*/
std::string VveDataSetName::GetFilePattern () const
{
	std::string filename(GetPattern());
	filename.erase(0, filename.find_last_of("/")+1);
	return filename;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set                                                         */
/*                                                                            */
/*============================================================================*/
void VveDataSetName::Set(const std::string& strPattern)
{
	// initialize data:
    m_strRootDirectory     = "";
    m_strFileNamePrefix    = "";
    m_strFileNamePostfix   = "";
    m_strFileNameExtension = "";
    m_strBlockPrefix       = "";
    m_strTimePrefix        = "";
    m_bUseBlock            = false;
    m_bUseTime             = false;
    m_nTimeDigits          = 0;
    m_nBlockDigits         = 0;
    m_bTimeFirst           = false;
	m_bFillTimeNumZeros    = true;
	m_bFillBlockNumZeros   = true;

	// replace "\" by "/"
	std::string strTmpPattern = strPattern;
	size_t iPos, iLength = strTmpPattern.size();
	for (iPos=0; iPos<iLength; ++iPos)
	{
		if (strTmpPattern[iPos] == '\\')
			strTmpPattern[iPos] = '/';
	}

	// find root
	iPos = strTmpPattern.find_last_of('/');
	if (iPos != string::npos)
	{
		++iPos;
		m_strRootDirectory = strTmpPattern.substr(0, iPos);
	}
	else
		iPos = 0;

	// find time step number or block number
	size_t iTimePos, iBlockPos, iFirstNumeric=string::npos;
	iTimePos = strTmpPattern.find('*', iPos);
	iBlockPos = strTmpPattern.find('?', iPos);

	if (iTimePos != string::npos)
	{
		m_bUseTime = true;
		iFirstNumeric = iTimePos;
	}

	if (iBlockPos != string::npos)
	{
		m_bUseBlock = true;
		if (iFirstNumeric == string::npos || iBlockPos<iFirstNumeric)
			iFirstNumeric = iBlockPos;
	}

	if (m_bUseTime && m_bUseBlock && iTimePos < iBlockPos)
	{
		m_bTimeFirst = true;
	}

	if (iFirstNumeric == string::npos)
	{
		iFirstNumeric = strTmpPattern.find_last_of('.');

		if (iFirstNumeric == string::npos || iFirstNumeric < iPos)
		{
			m_strFileNamePrefix = strTmpPattern.substr(iPos);
			return;
		}
	}

	m_strFileNamePrefix = strTmpPattern.substr(iPos, iFirstNumeric-iPos);
	iPos = iFirstNumeric;

	if (m_bTimeFirst)
	{
		iPos = iTimePos;
		while (iPos < iLength && strTmpPattern[iPos] == '*')
		{
			++m_nTimeDigits;
			++iPos;
		}
	}
	
	if (m_bUseBlock)
	{
		m_strBlockPrefix = strTmpPattern.substr(iPos, iBlockPos-iPos);
		iPos = iBlockPos;
		while (iPos < iLength && strTmpPattern[iPos] == '?')
		{
			++m_nBlockDigits;
			++iPos;
		}
	}

	if (!m_bTimeFirst && m_bUseTime)
	{
		m_strTimePrefix = strTmpPattern.substr(iPos, iTimePos-iPos);
		iPos = iTimePos;
		while (iPos < iLength && strTmpPattern[iPos] == '*')
		{
			++m_nTimeDigits;
			++iPos;
		}
	}

	if (iPos < iLength)
	{
		size_t iExtPos = strTmpPattern.find_last_of('.');
		if (iExtPos == string::npos)
		{
			m_strFileNamePostfix = strTmpPattern.substr(iPos);
		}
		else
		{
			if (iPos < iExtPos)
			{
				m_strFileNamePostfix = strTmpPattern.substr(iPos, iExtPos-iPos);
				iPos = iExtPos;
			}
			m_strFileNameExtension = strTmpPattern.substr(iPos);
		}
	}

	return;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetPattern                                                  */
/*                                                                            */
/*============================================================================*/
std::string VveDataSetName::GetPattern () const
{
    /*
	std::ostringstream strBlock;
    std::ostringstream strTime;
    std::ostringstream strBlockPur;
    std::ostringstream strTimePur;
	*/
	std::string strBlock("");
	std::string strTime("");
	std::string strBlockPur("");
	std::string strTimePur("");

    size_t curDigi;
    if (m_bUseTime)
    {
        strTime += m_strTimePrefix;
        for (curDigi = 0; curDigi < m_nTimeDigits; ++curDigi)
        {
            strTime += string("*");
        }
    }

    if (m_bUseBlock)
    {
        strBlock += m_strBlockPrefix;
        for (curDigi = 0; curDigi < m_nBlockDigits; ++curDigi)
        {
            strBlock += string("?");
        }
    }

    // concatenate pattern
    //std::ostringstream strPattern;
	std::string strPattern(m_strRootDirectory);
    strPattern += m_strFileNamePrefix;
    if (m_bTimeFirst)
    {
        strPattern += strTime;
		strPattern += strBlock;
    }
    else
    {
        strPattern += strBlock;
		strPattern += strTime;
    }
    strPattern += m_strFileNamePostfix;
	strPattern += m_strFileNameExtension;

    // return result string
    return strPattern;


	
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   operator<<                                                  */
/*                                                                            */
/*============================================================================*/
std::ostream & operator<<(std::ostream & oOutput, const VveDataSetName & p)
{
	oOutput << p.GetRootDir() << p.GetFileNamePrefix();

	if (p.IsTimeFirst() && p.IsTimeEnabled())
	{
		oOutput << p.GetTimePrefix();
		size_t i, iLength = p.GetTimeDigits();
		for (i=0; i<iLength; ++i)
			oOutput << "*";
	}

	if (p.IsBlockEnabled())
	{
		oOutput << p.GetBlockPrefix();
		size_t i, iLength = p.GetBlockDigits();
		for (i=0; i<iLength; ++i)
			oOutput << "?";
	}

	if (p.IsTimeEnabled() && !p.IsTimeFirst())
	{
		oOutput << p.GetTimePrefix();
		size_t i, iLength = p.GetTimeDigits();
		for (i=0; i<iLength; ++i)
			oOutput << "*";
	}

	oOutput << p.GetFileNamePostfix() << p.GetFileNameExtension();

	return oOutput;
}



/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*  END OF FILE "VveDataSetName.cpp"                                             */
/*============================================================================*/
