/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VvePathlineReaderTrk.h"
#include <cassert>
#include <iostream>
#include <cmath>
#include <vtkByteSwap.h>
#include "VistaVisExt/Data/VveParticleTrajectory.h"

#include <VistaBase/VistaStreamUtils.h>


/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VvePathlineReaderBase::VvePathlineReaderBase(std::string strFilename)
: m_strFilename(strFilename), 
m_eType(UNKNOWN), m_bBinary(false), m_fScale(1.0f),
m_sPositionArray(VveParticlePopulation::sPositionArrayDefault),
m_sVelocityArray(VveParticlePopulation::sVelocityArrayDefault),
m_sScalarArray(VveParticlePopulation::sScalarArrayDefault),
m_sRadiusArray(VveParticlePopulation::sRadiusArrayDefault),
m_sTimeArray(VveParticlePopulation::sTimeArrayDefault)
{
}

VvePathlineReaderBase::~VvePathlineReaderBase()
{
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Load                                                        */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderBase::Load(VveParticlePopulation *pPopulation, 
                          VvePathlineReaderBase::SCALAR eScalar, 
                          VvePathlineReaderBase::SCALAR eRadius)
{
    if (!pPopulation)
    {
        vstr::errp() << " [VflPathlineReader] no particle population to be filled..." << endl;
        return false;
    }

    if (m_liData.empty())
    {
        vstr::outi() << " [VflPathlineReader] data cache empty - loading data from file..." << endl;
        if (!LoadDataFromFile())
            return false;
    }

    if (m_liData.empty())
    {
        vstr::errp() << " [VflPathlineReader] unable to load data from file..." << endl;
        vstr::erri() << "                 file name: " << m_strFilename << endl;
        vstr::erri() << "                 format: " << (m_bBinary?"binary":"ascii") << endl;

        return false;
    }

    vstr::outi() << " [VflPathlineReader] destroying old particle data..." << endl;
    pPopulation->Clear();

    vstr::outi() << " [VflPathlineReader] copying cache contents to particle population..." << endl;
    CopyCacheToPopulation(pPopulation, eScalar, eRadius);

    return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   EmptyCache                                                  */
/*                                                                            */
/*============================================================================*/
void VvePathlineReaderBase::EmptyCache()
{
#ifdef DEBUG
    vstr::debugi() << " [VflPathlineReader] - cleaning up data cache..." << endl;
#endif
    m_liData.clear();
    m_vecTimeLevelCounts.clear();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetType                                                     */
/*                                                                            */
/*============================================================================*/
VvePathlineReaderBase::TYPE VvePathlineReaderBase::GetType()
{
    return m_eType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   CopyCacheToPopulation                                       */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderBase::CopyCacheToPopulation( VveParticlePopulation *pPopulation,
                                           VvePathlineReaderBase::SCALAR eScalar,
                                           VvePathlineReaderBase::SCALAR eRadius) const
{
    if (!pPopulation)
    {
        vstr::warnp() << " [VflPathlineReaderTrk] no particle population to be filled given..." << endl;
        return false;
    }

    // allocate memory for particle data
    const int iPopulationSize = static_cast<int>(m_vecTimeLevelCounts.size());

    std::vector<int> vecCurrentLevelIndex;
    vecCurrentLevelIndex.resize(iPopulationSize);

    for (int i=0; i<iPopulationSize; ++i)
    {
        vecCurrentLevelIndex[i] = 0;

        // create particle trajectories
        VveParticleTrajectory* pTraj = new VveParticleTrajectory();

        pTraj->AddArray(new VveParticleDataArray<float>(3, m_sPositionArray));
        pTraj->AddArray(new VveParticleDataArray<float>(3, m_sVelocityArray));
        pTraj->AddArray(new VveParticleDataArray<float>(1, m_sTimeArray));
        pTraj->AddArray(new VveParticleDataArray<float>(1, m_sScalarArray));
        pTraj->AddArray(new VveParticleDataArray<float>(1, m_sRadiusArray));
        pTraj->Resize(m_vecTimeLevelCounts[i]);

        pPopulation->AddTrajectory(pTraj);
    }

    // These pointers are used to address the particle data arrays
    const STemporaryParticleData *pTPD;
    VveParticleTrajectory* pTraj = NULL;
    VveParticleDataArray<float> *aPos = NULL;
    VveParticleDataArray<float> *aVel = NULL;
    VveParticleDataArray<float> *aTime = NULL;
    VveParticleDataArray<float> *aScalar = NULL;
    VveParticleDataArray<float> *aRadius = NULL;

    std::list<STemporaryParticleData>::const_iterator itParticle = m_liData.begin();
    while (itParticle != m_liData.end())
    {
        pTPD = &(*itParticle);

        // Get the trajectory of the current particles
        pTraj = pPopulation->GetTrajectory(pTPD->iIndex);

        // References to the particle data arrays within the trajectory
        pTraj->GetTypedArrayByName(m_sPositionArray, aPos);
        pTraj->GetTypedArrayByName(m_sVelocityArray, aVel);
        pTraj->GetTypedArrayByName(m_sTimeArray,     aTime);
        pTraj->GetTypedArrayByName(m_sScalarArray,   aScalar);
        pTraj->GetTypedArrayByName(m_sRadiusArray,   aRadius);

        // Get the index of the current particle within the trajectory
        int iCurrentParticleIndex = vecCurrentLevelIndex[pTPD->iIndex];

        // Skip current particle, if time is constant
        if (iCurrentParticleIndex > 0 && 
            pTPD->fTime == aTime->GetElementValueAsFloat(iCurrentParticleIndex-1))
        {    
            ++itParticle;
            continue;
        }

        // copy temporary particle data to arrays
        aPos->SetElement(iCurrentParticleIndex, pTPD->aPos);
        aVel->SetElement(iCurrentParticleIndex, pTPD->aVel);
        aTime->SetElementValue(iCurrentParticleIndex, pTPD->fTime);

        float fScalar = 0.0f;
        switch (eScalar)
        {
        case VELOCITY:
            fScalar = sqrt(pTPD->aVel[0]*pTPD->aVel[0]
                         + pTPD->aVel[1]*pTPD->aVel[1]
                         + pTPD->aVel[2]*pTPD->aVel[2]);
            break;
        case TEMPERATURE:
            fScalar = pTPD->fTemperature;
            break;
        case MASS:
            fScalar = pTPD->fMass;
            break;
        case RADIUS:
            fScalar = pTPD->fDiameter*0.5f;
            break;
        case COUNT:
            fScalar = pTPD->fCount;
            break;
        }
        aScalar->SetElementValue(iCurrentParticleIndex, fScalar);

        float fRadius = 0.0f;
        switch (eRadius)
        {
        case VELOCITY:
            fRadius = sqrt(pTPD->aVel[0]*pTPD->aVel[0]
                         + pTPD->aVel[1]*pTPD->aVel[1]
                         + pTPD->aVel[2]*pTPD->aVel[2]);
            break;
        case TEMPERATURE:
            fRadius = pTPD->fTemperature;
            break;
        case MASS:
            fRadius = pTPD->fMass;
            break;
        case RADIUS:
            fRadius = pTPD->fDiameter*0.5f;
            break;
        case COUNT:
            fRadius = pTPD->fCount;
            break;
        }
        aRadius->SetElementValue(iCurrentParticleIndex, fRadius);

        ++(vecCurrentLevelIndex[pTPD->iIndex]);
        ++itParticle;
    }

    // resize the trajectory vectors
    for (int i=0; i<iPopulationSize; ++i)
    {
        pPopulation->GetTrajectory(i)->Resize(vecCurrentLevelIndex[i]);
    }

    return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetPositionArray                                            */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderBase::SetPositionArray( std::string sPositionArray )
{
    if(compAndAssignFunc<std::string>(sPositionArray, m_sPositionArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetVelocityArray                                            */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderBase::SetVelocityArray( std::string sVelocityArray )
{
    if(compAndAssignFunc<std::string>(sVelocityArray, m_sVelocityArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetRadiusArray                                              */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderBase::SetRadiusArray( std::string sRadiusArray )
{
    if(compAndAssignFunc<std::string>(sRadiusArray, m_sRadiusArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetScalarArray                                              */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderBase::SetScalarArray( std::string sScalarArray )
{
    if(compAndAssignFunc<std::string>(sScalarArray, m_sScalarArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   SetTimeArray                                                */
/*                                                                            */
/*============================================================================*/
bool VvePathlineReaderBase::SetTimeArray( std::string sTimeArray )
{
    if(compAndAssignFunc<std::string>(sTimeArray, m_sTimeArray))
    {
        return true;
    }
    return false;
}

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
