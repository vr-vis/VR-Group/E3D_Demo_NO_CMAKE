/**
*	Implement deserialization of streamed scalar data which results form 3DCutObj computations
*/
#ifndef _VVESCALARDATAPACKET_H
#define _VVESCALARDATAPACKET_H

#include <VistaAspects/VistaSerializable.h>

#include <vector>
#include <iostream>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>

using namespace std;

class IVistaSerializer;
class IVistaDeSerializer;

class VISTAVISEXTAPI VveScalarDataPacket : public IVistaSerializable
{
public:
    VveScalarDataPacket(){}
    virtual ~VveScalarDataPacket(){}

	/**
	*	(De)Serialization
	*/
    virtual int Serialize(IVistaSerializer &) const;
    virtual int DeSerialize(IVistaDeSerializer &);
    
	/**
	*	DATA READ ACCESS
	*/
	virtual std::string GetSignature() const;
	int GetPacketID() const;
	int GetTimeLevel() const;
	int GetPointOffset() const;
	size_t GetNumberOfScalars() const;
	int GetPointId(size_t index) const;
	float operator[](size_t index) const;
	float GetScalar(size_t index) const;
	
	/**
	*	DATA WRITE ACCESS
	*/
	void SetPacketID(int iId);
	void SetPacketSize(int iSize);
	void SetTimeLevel(int iTime);
	void SetPointOffset(int iOfs);
	void SetPointId(size_t index, int iId);
	void SetScalar(size_t index, float fScalar);
	
	void AddValue(int i, float f);
	void Clear();

	size_t EstimateByteSize() const ;

protected:
	static const std::string m_strSignature;

	int m_iPacketID;
	int m_iTimeLevel;
	int m_iPointOffset;
	vector<int>		m_vecPointIds;
	vector<float>	m_vecScalars;
};

ostream& operator<<(ostream& out, const VveScalarDataPacket& packet);

#endif //_VVESCALARDATAPACKET_H

