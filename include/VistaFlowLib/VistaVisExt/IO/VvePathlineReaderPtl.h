/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEPATHLINEREADERPTL_H
#define _VVEPATHLINEREADERPTL_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>
#include <list>

#include <VistaVisExt/VistaVisExtConfig.h>
#include "VvePathlineReaderBase.h"

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VvePathlineReaderPtl loads the contents of a given trk file (in ascii or binary
 * format) into a given particle population object.
 */    
class VISTAVISEXTAPI VvePathlineReaderPtl : public VvePathlineReaderBase
{

public:
    // CONSTRUCTORS / DESTRUCTOR
    VvePathlineReaderPtl(std::string strFilename);
    virtual ~VvePathlineReaderPtl();

    /**
     * Fill the data cache, if it's empty.
     * In conjunction with CopyDataToPopulation(), it allows for a two-phase loading process,
     * thus minimizing blocking time for the graphical output.
     */
    bool LoadDataFromFile();

protected:

};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLPATHLINEREADERPTL_H)
