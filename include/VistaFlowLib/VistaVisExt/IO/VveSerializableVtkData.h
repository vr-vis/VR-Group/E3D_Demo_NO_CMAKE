/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VveSerializableVtkData.h	 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VveSerializableVtkData :   ...                                      */
/*                                                                            */
/*============================================================================*/


#ifndef _VVESERIALIZABLEVTKDATA_H
#define _VVESERIALIZABLEVTKDATA_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <VistaAspects/VistaSerializable.h>
#include <vector>
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkDataSet;
class IVistaSerializer;
class IVistaDeSerializer;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 *	This class can be used to (de-)serialize a set of 1...n vtkDataSets using the Vista(De)Serializer interface,
 *	as defined in VistaInterProcComm.
 *
 *	@author		Bernd Hentschel
 *	@date		April 2004 
 */
class VISTAVISEXTAPI VveSerializableVtkData : public IVistaSerializable
{
public:
	VveSerializableVtkData();
	virtual ~VveSerializableVtkData();
	/**
	*	Serialize the collection of vtkDataSets contained by this object 
	*/
	virtual int Serialize(IVistaSerializer &) const;
    /**
	*	DeSerialize an incoming byte stream which contains 1...n vtkDataSets.
	*	NOTE:	The set of vtkDataSets contained by this object is not cleared implicitly
	*			by this function, i.e. if you want to ensure that this->GetDataObject(0)
	*			yields the first object from the incoming DeSerializer you need to call
	*			this->Clear() before deserialization explicitly.
	*/
	virtual int DeSerialize(IVistaDeSerializer &);
    /**
	*	
	*/
	virtual std::string GetSignature() const;
	/**
	*	Add a vtkDataSet to be serialized
	*/
	void AddDataObject(vtkDataSet* pDataSet);
	/**
	*	Remove a vtkDataObject
	*/
	bool RemoveDataObject(vtkDataSet* pDataSet);
	/**
	*	...
	*/
	bool RemoveDataObject(int i);
	/**
	*	Get the i-th data object contained by this object
	*/
	vtkDataSet* GetDataObject(int i) const;
	/**
	*	Get the total number of vtkDataSets contained by this object
	*/
	size_t GetNumDataObjects() const;
	/**
	*	Clear, i.e. remove all the vtkDataSets which are currently contained by this object
	*	NOTE:	This affects solely the references to the data sets. There are no calls
	*			to vtkDataSet::Delete(). This has to be done by the user explicitly!
	*/
	void Clear();
protected:
	int WriteVtkData(IVistaSerializer& rSerializer, vtkDataSet* pData) const;
	int ReadVtkData(IVistaDeSerializer& rDeSerializer, vtkDataSet*& pData);

private:
	std::vector<vtkDataSet*> m_vecObjectsToBeSerialized;
	static const std::string m_strSignature;
};

#endif // _VVESERIALIZABLEVTKDATA_H

/*============================================================================*/
/*  END OF FILE "VveSerializableVtkData.h"		                                  */
/*============================================================================*/


