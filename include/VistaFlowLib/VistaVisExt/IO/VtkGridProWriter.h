/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkGridProWriter.h			 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VtkExt                       RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VtkGridProWriter      :   ...                                    */
/*                                                                            */
/*============================================================================*/
#ifndef _VTKGRIDPROWRITER_H
#define _VTKGRIDPROWRITER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkPolyDataWriter.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkDataObject;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * Write a vtkPolyData mesh to a file in grid pro data format. 
 * Writing is performed by stdlib functionality i.e. no additional dependencies exist.
 *
 *	@author		Michael Holtman / Bernd Hentschel
 *	@date		June 2005
*/
class VISTAVISEXTAPI VtkGridProWriter : public vtkPolyDataWriter
{
public:
	static VtkGridProWriter *New(){
		return new VtkGridProWriter;
	};

protected:
	VtkGridProWriter(){};
	virtual ~VtkGridProWriter(){};
	virtual int Write();
};


#endif // _VTKGRIDPROWRITER_H
/*============================================================================*/
/*  END OF FILE "VtkGridProWriter.h"                                          */
/*============================================================================*/
