/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEUNSTEADYCARTESIANGRIDLOADER_H
#define _VVEUNSTEADYCARTESIANGRIDLOADER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveUnsteadyDataLoader.h"
#include <VistaAspects/VistaReflectionable.h>
#include <VistaBase/VistaVectorMath.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflUnsteadyCartesianGridLoader loads precalculated pathlines from disk by 
 * using different file format readers.
 */    
class VISTAVISEXTAPI VveUnsteadyCartesianGridLoader : public VveUnsteadyDataLoader
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    VveUnsteadyCartesianGridLoader(VveUnsteadyData *pTarget);
    virtual ~VveUnsteadyCartesianGridLoader();

    /**
     * Initialize the loader.
     * Basically, this method just does a sanity check of the 
     * given parameters. The property list gets finally parsed
     * and used when data loading is being invoked.
     */
    virtual bool Init(const VistaPropertyList &refProps);

    virtual void LoadDataThreadSafe();

	/**
	 * Save loader properties separately and provide a standard interface. Due to 
	 * IVistaReflectionable all members can be accessed via the property keys 
	 * listed below:
	 *
	 * FILE_NAME	     	File name pattern of files to be read.
	 *
	 * FORMAT	            File format. This optional(!) parameters overrides
	 *                      file format determination by file extension.
	 *						The user still has to make sure, that the vtk-file
	 *						contains structuredPoints-Data.
	 *                      Options: VTK_STRUCTUREDPOINTS
	 *
	 * TIME_FILE			Optional time file name to use for loading.
	 *
	 * PROGRESS_BAR			Show progress bar during load operation.
	 *
	 * PADDING				Pad data dimensions to powers of two.
	 *
	 * GENERATE_TORNADO     Generate an artificial tornado instead of loading data.
	 *
	 * TORNADO_DIMENSIONS   Give the dimensions for the artificial tornado.
	 *
	 * If a VTK file is to be loaded, the following parameters are supported:
	 *
	 * SCALARS_NAME	     	scalar field to be loaded upon reader execution
	 *						works ONLY WITH multi-scalar data using the
	 *						"VtkMultiScalarUnstructuredGridWriter-method" i.e.
	 *						several scalar fields with type flag "SCALARS" in one vtk data file
	 *
	 * VECTORS_NAME	     	same as above for vector field data
	 *
	 * ACTIVE_SCALARS      	scalar field to be selected AFTER reader execution
	 *						works with vtk datasets with multiple associated field
	 *						data arrays (generated via vtkPointData::AddArray(...))
	 * 
	 * ACTIVE_VECTORS      	same as above for vectors
	 *
	 * TARGET_TYPE        Convert and store data as FLOAT (default), HALF or UNSIGNED_CHAR.
	 *
	 * NORMALIZATION_RANGE  Normalize data from given range to [0, 1]. 
	 *                      If this range is set to [0, 0] (default), no normalization is performed.
	 *
	 */
	class VISTAVISEXTAPI CProperties : public IVistaReflectionable
	{
	public:
		CProperties();
//		CProperties(const CProperties& source);
		virtual ~CProperties();

//		CProperties& operator=(const CProperties& source);

		/**
		* NOTE : each setter will return true iff the value has changed!
		*/
		bool SetFilePattern(const std::string &str);
		std::string GetFilePattern() const;

		bool SetFileFormat(const std::string &str);
		std::string GetFileFormat() const;
		
		bool SetTimeFile(const std::string &str);
		std::string GetTimeFile() const;
		
		bool SetProgressBar(bool b);
		bool GetProgressBar() const;

		bool SetLoadSilently(bool b);
		bool GetLoadSilently() const;

		bool SetPadding(bool b);
		bool GetPadding() const;

		enum TARGET_TYPE
		{
			TT_INVALID = -1,
			TT_FLOAT = 0,
			TT_HALF,
			TT_UNSIGNED_CHAR,
			TT_LAST
		};

		bool SetTargetType(int iType);
		bool SetTargetType(const std::string &strType);
		int GetTargetType() const;
		std::string GetTargetTypeAsString() const;

		bool SetActiveScalars(const std::string &str);
		std::string GetActiveScalars() const;
		
		bool SetActiveVectors(const std::string &str);
		std::string GetActiveVectors() const;
		
		bool SetScalarsName(const std::string &str);
		std::string GetScalarsName() const;
		
		bool SetVectorsName(const std::string &str);
		std::string GetVectorsName() const;

		bool SetNormalizationRange(float fMin, float fMax);
		bool GetNormalizationRange(float &fMin, float &fMax) const;

		bool SetTornadoDimensions(const VistaVector3D &v3Dims);
		VistaVector3D GetTornadoDimensions() const;
		
		bool SetGenerateTornado(bool bGenerate);
		bool GetGenerateTornado() const;
		
		void Lock();
		void Unlock();

		virtual std::string GetReflectionableType() const;
	protected:
		int AddToBaseTypeList(std::list<std::string>& rBTList) const;

	private:
		std::string m_strFilePattern;
		std::string m_strFileFormat;
		std::string m_strTimeFile;
		std::string m_strActiveScalars;
		std::string m_strActiveVectors;
		std::string m_strScalarsName;
		std::string m_strVectorsName;
		VistaVector3D	m_v3TornadoDimensions;
		bool m_bProgressBar;
		bool m_bLoadSilently;
		bool m_bPadding;
		int m_iTargetType;
		bool m_bGenerateTornado;
		float m_aNormalizationRange[2];

		VistaMutex m_oMutex;
	};

	CProperties *GetProperties() const;
protected:
    // loading different data files
    int LoadVtkFiles(const std::string &strFilePattern);

	// generate an artificial tornado
	int GenerateTornado();

	
private:
    CProperties *m_pProps;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VVEUNSTEADYCARTESIANGRIDLOADER_H)
