/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveDatasetDescriptionXMLLoader.h"
#include <VistaVisExt/Data/DatasetDescription/VveDatasetDescription.h>
#include <VistaVisExt/Data/DatasetDescription/VveComponentDescription.h>
#include <VistaVisExt/Data/DatasetDescription/VveMetaInfoDescription.h>
#include <VistaVisExt/Data/DatasetDescription/VveTimestepDescription.h>
#include <VistaVisExt/Data/DatasetDescription/VveBlockDescription.h>
#include <VistaVisExt/Data/DatasetDescription/VveEnsembleDescription.h>


#include <VistaTools/tinyXML/tinyxml.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/IO/VveIOUtils.h>
#include <VistaAspects/VistaPropertyList.h>
#include <VistaVisExt/IO/VveDataSetName.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveDatasetDescriptionXMLLoader::VveDatasetDescriptionXMLLoader(std::string strFilename)
	:	m_strFilename(strFilename)
{
	// convert '\' by '/'
	for(int i = 0; i < m_strFilename.length(); ++i)
	{
		if(m_strFilename[i] == '\\')
			m_strFilename[i] = '/';
	}

	// find directory of the file (to evaluate relative paths)
	std::string::size_type stPos = m_strFilename.find_last_of('/');
	if(stPos == std::string::npos)
	{
		// no path given - directory is this one
		m_strDirectory = "";
	}
	else
	{
		// cut string for directory (final '/' is not included)
		m_strDirectory = m_strFilename.substr(0,stPos);
	}
}

VveDatasetDescriptionXMLLoader::~VveDatasetDescriptionXMLLoader()
{
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/


VveDatasetDescription* VveDatasetDescriptionXMLLoader::LoadDataset(bool bMustHaveName /* =false */)
{
	VistaXML::TiXmlDocument oDatasetFile;
	if(!oDatasetFile.LoadFile(m_strFilename))
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader] dataset XML file could not be loaded!" 
			<< std::endl;
		return NULL;
	}

	// top element is called "Dataset"
	VistaXML::TiXmlElement *pDatasetEl = oDatasetFile.FirstChildElement("Dataset");

	// not found?
	if(!pDatasetEl)
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader] the given XML file does not "
			<< "contain a dataset (missing <Dataset> top-level element)!"
			<< std::endl;
		return NULL;
	}

	return LoadDataset(pDatasetEl,bMustHaveName);
}

VveDatasetDescription*	VveDatasetDescriptionXMLLoader::LoadDataset(VistaXML::TiXmlElement *pDatasetEl,
		bool bMustHaveName)
{
	assert(pDatasetEl);


	std::string strDirectory = m_strDirectory;

	// check whether it is defined externally
	const char *pSource = pDatasetEl->Attribute("source");
	VistaXML::TiXmlDocument *pDatasetFile = NULL;
	if(pSource)
	{
		// load this file
		std::string strSourceFile = strDirectory + '/' + std::string(pSource);
		pDatasetFile = new VistaXML::TiXmlDocument();
		if(!pDatasetFile->LoadFile(strSourceFile))
		{
			vstr::errp() << " [VveDatasetDescriptionXMLLoader] external dataset XML file "
				<< strSourceFile << " for dataset could not be loaded!" 
				<< std::endl;
			delete pDatasetFile;
			return NULL;
		}

		// must have a top element <Dataset>
		VistaXML::TiXmlElement *pDatasetExt = 
			pDatasetFile->FirstChildElement("Dataset");
		if(!pDatasetExt)
		{
			vstr::errp() << " [VveDatasetDescriptionXMLLoader] external dataset XML file "
				<< strSourceFile << " misses <Dataset> top-level tag!" 
				<< std::endl;
			return NULL;
		}

		// okay, take this one.
		pDatasetEl = pDatasetExt;

		// path should be relative to the new file!
		std::string::size_type stPos = strSourceFile.find_last_of('/');
		if(stPos == std::string::npos)
		{
			// no path given - directory is top one
			strDirectory = "";
		}
		else
		{
			// cut string for directory (final '/' is not included)
			strDirectory = strSourceFile.substr(0,stPos);
		}
	}



	// create dataset
	VveDatasetDescription *pDataset = new VveDatasetDescription();

	// set name, if attribute given
	const char* pAttr = pDatasetEl->Attribute("name");
	if(pAttr)
	{
		pDataset->SetNameForNameable(pAttr);
	}
	else
	{
		if(bMustHaveName)
		{
			vstr::errp() << " [VveDatasetDescriptionXMLLoader] the given dataset "
				<< "does not specify a name, but must have one! "
				<< "Dataset could not be loaded."
				<< std::endl;
			delete pDataset;
			if(pDatasetFile) delete pDatasetFile;
			return NULL;
		}
	}

	// does the dataset have a top-level time mapper?
	VveTimeMapper *pTimeMapper = NULL;
	VistaXML::TiXmlElement *pTimefileEl = pDatasetEl->FirstChildElement("Timefile");
	if(pTimefileEl)
	{
		std::string strTimefileName = pTimefileEl->GetText();
		pTimeMapper = new VveTimeMapper();
		if(!VveIOUtils::LoadXMLTimeFile(strDirectory + '/' + strTimefileName, pTimeMapper))
		{
			vstr::errp() << " [VveDatasetDescriptionXMLLoader] a time file was given "
				<< "for the dataset (" << strDirectory + '/' + strTimefileName 
				<< "), but could not be loaded!"	<< std::endl;
			delete pTimeMapper;
			pTimeMapper = NULL;
		}
	}

	if(pTimeMapper)
		pDataset->SetTimeMapper(pTimeMapper);

	// load description (if given)
	VistaXML::TiXmlElement *pDescriptionEl = 
		pDatasetEl->FirstChildElement("Description");
	if(pDescriptionEl)
		pDataset->SetDescription(pDescriptionEl->GetText());

	// load meta info (if given)
	VistaXML::TiXmlElement *pMetaInfoEl = pDatasetEl->FirstChildElement("MetaInfo");
	if(pMetaInfoEl)
	{
		VveMetaInfoDescription *pMetaInfo = LoadMetaInfo(pMetaInfoEl);
		if(pMetaInfo)
		{
			pDataset->SetMetaInfo(pMetaInfo);
		}
	}


	// now load all components
	VistaXML::TiXmlElement *pComponentEl = pDatasetEl->FirstChildElement("Component");
	if(pComponentEl == NULL)
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader] dataset does not contain "
			<< "any components!" << std::endl;
		if(pDatasetFile) delete pDatasetFile;
		delete pDataset;
		return NULL;
	}
	while(pComponentEl != NULL)
	{
		// read component
		VveComponentDescription *pComponent = LoadComponent(pComponentEl, pTimeMapper);
		// add component
		if(pComponent != NULL)
			pDataset->AddComponent(pComponent);

		// next one
		pComponentEl = pComponentEl->NextSiblingElement("Component");
	}

	if(pDatasetFile) delete pDatasetFile;
	return pDataset;
}

VveComponentDescription* VveDatasetDescriptionXMLLoader::LoadComponent( VistaXML::TiXmlElement *pComponentEl, 
	VveTimeMapper *pDatasetTimeMapper )
{
	assert(pComponentEl);

	std::string strDirectory = m_strDirectory;

	// check whether it is defined externally
	const char *pSource = pComponentEl->Attribute("source");
	VistaXML::TiXmlDocument *pComponentFile = NULL;
	if(pSource)
	{
		// load this file
		std::string strSourceFile = strDirectory + '/' + std::string(pSource);
		pComponentFile = new VistaXML::TiXmlDocument();
		if(!pComponentFile->LoadFile(strSourceFile))
		{
			vstr::errp() << " [VveDatasetDescriptionXMLLoader] external dataset XML file "
				<< strSourceFile << " for component could not be loaded!" 
				<< std::endl;
			delete pComponentFile;
			return NULL;
		}

		// must have a top element <Component>
		VistaXML::TiXmlElement *pComponentExt = 
			pComponentFile->FirstChildElement("Component");
		if(!pComponentExt)
		{
			vstr::errp() << " [VveDatasetDescriptionXMLLoader] external dataset XML file "
				<< strSourceFile << " misses <Component> top-level tag!" 
				<< std::endl;
			delete pComponentFile;
			return NULL;
		}

		// okay, take this one.
		pComponentEl = pComponentExt;

		// path should be relative to the new file!
		std::string::size_type stPos = strSourceFile.find_last_of('/');
		if(stPos == std::string::npos)
		{
			// no path given - directory is top one
			strDirectory = "";
		}
		else
		{
			// cut string for directory (final '/' is not included)
			strDirectory = strSourceFile.substr(0,stPos);
		}
	}

	// read name
	const char* pAttr = pComponentEl->Attribute("name");
	if(!pAttr)
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader] found component definition "
			<< "without a name! Component will not be loaded." << std::endl;
		if(pComponentFile) delete pComponentFile;
		return NULL;
	}
	std::string strComponentName = pAttr;

	// read component type
	VistaXML::TiXmlElement *pTypeEl = pComponentEl->FirstChildElement("Type");
	if(!pTypeEl)
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader]   no type given "
			<< "for the component " << strComponentName << "! "
			<< "Component could not be loaded." << std::endl;
		if(pComponentFile) delete pComponentFile;
		return NULL;
	}
	std::string strTypeText = pTypeEl->GetText();
	VveComponentDescription::DataComponentType eType;
	if(strTypeText == "STRUCTURED_POINTS")
		eType = VveComponentDescription::DCT_STRUCTURED_POINTS;
	else if(strTypeText == "STRUCTURED_GRID")
		eType = VveComponentDescription::DCT_STRUCTURED_GRID;
	else if(strTypeText == "UNSTRUCTURED_GRID")
		eType = VveComponentDescription::DCT_UNSTRUCTURED_GRID;
	else if(strTypeText == "POLYDATA")
		eType = VveComponentDescription::DCT_POLYDATA;
	else if(strTypeText == "RECTILINEAR_GRID")
		eType = VveComponentDescription::DCT_RECTILINEAR_GRID;
	else
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader]   illegal type ("
			<< strTypeText << ") given "
			<< "for the component " << strComponentName << "! "
			<< "Component could not be loaded." << std::endl;
		if(pComponentFile) delete pComponentFile;
		return NULL;
	}

	// read time file, if given
	VistaXML::TiXmlElement *pTimefileEl = pComponentEl->FirstChildElement("Timefile");
	if(pTimefileEl)
	{
		std::string strTimefileName = pTimefileEl->GetText();
		VveTimeMapper *pTimeMapperTemp = new VveTimeMapper();
		if(!VveIOUtils::LoadXMLTimeFile(strDirectory + '/' + strTimefileName, pTimeMapperTemp))
		{
			vstr::errp() << " [VveDatasetDescriptionXMLLoader]   a time file was given "
				<< "for the component " << strComponentName
				<< ", but could not be loaded!"	<< std::endl;
			delete pTimeMapperTemp;
		}
		else
		{
			pDatasetTimeMapper = pTimeMapperTemp;
		}
	}

	// time mapper given?
	if(!pDatasetTimeMapper)
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader]   no time file specified "
			<< "for the component " << strComponentName << "! Using default "
			<< "time mapper." << std::endl;
		// create default time mapper
		pDatasetTimeMapper = new VveTimeMapper();
	}

	// create component
	VveComponentDescription *pComponent = new VveComponentDescription(strComponentName,
		eType, pDatasetTimeMapper);


	// load description (if given)
	VistaXML::TiXmlElement *pDescriptionEl = 
		pComponentEl->FirstChildElement("Description");
	if(pDescriptionEl)
		pComponent->SetDescription(pDescriptionEl->GetText());

	// load scalars / vectors (if given)
	VistaXML::TiXmlElement *pElement = pComponentEl->FirstChildElement("Scalars");
	if(pElement)
		pComponent->SetScalars(pElement->GetText());
	
	pElement = pComponentEl->FirstChildElement("Vectors");
	if(pElement)
		pComponent->SetVectors(pElement->GetText());

	// load meta info (if given)
	VistaXML::TiXmlElement *pMetaInfoEl = pComponentEl->FirstChildElement("MetaInfo");
	if(pMetaInfoEl)
	{
		VveMetaInfoDescription *pMetaInfo = LoadMetaInfo(pMetaInfoEl);
		if(pMetaInfo)
		{
			pComponent->SetMetaInfo(pMetaInfo);
		}
	}

	// load time steps and blocks
	VistaXML::TiXmlElement *pFilesEl = pComponentEl->FirstChildElement("Files");
	if(!pFilesEl)
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader]   no file definition for "
			<< "component " << strComponentName << "! Component could not be "
			<< "loaded." << std::endl;
		delete pComponent;
		if(pComponentFile) delete pComponentFile;
		return NULL;
	}

	// implicit definition via <FilePattern> or explicitly via <File> ?
	VistaXML::TiXmlElement *pFilePatternEl = pFilesEl->FirstChildElement("FilePattern");
	if(pFilePatternEl)
	{
		// implicitly. 
		// one or several blocks per timestep?
		int iFirstBlock = 0;
		int iLastBlock = 0;
		pFilePatternEl->QueryIntAttribute("firstblock", &iFirstBlock);
		pFilePatternEl->QueryIntAttribute("lastblock", &iLastBlock);
		if(iLastBlock < iFirstBlock)
			iLastBlock = iFirstBlock;

		// meta info given?
		const char* pMetaInfoString = pFilePatternEl->Attribute("metainfo");

		VveDataSetName oName;
		oName.Set(pFilePatternEl->GetText());

		// create time steps
		std::vector<VveTimeMapper::IndexInfo> vecExplicitMapping;
		pDatasetTimeMapper->GetExplicitTimeMapping(vecExplicitMapping);
		for(int i = 0; i < pDatasetTimeMapper->GetNumberOfTimeLevels(); ++i)
		{
			VveTimestepDescription *pTimestep = new VveTimestepDescription(eType);
			// create blocks
			for(int j = iFirstBlock; j <= iLastBlock; ++j)
			{
				// filename via pattern
				std::string strFilename = strDirectory + '/' + 
					oName.GetFileName(vecExplicitMapping[i].m_iLevel, j);
				// create block
				VveBlockDescription *pBlock = new VveBlockDescription(strFilename,eType);
				// add block to timestep
				pTimestep->AddBlock(pBlock);


				// meta info?
				if(pMetaInfoString)
				{
					VveDataSetName oMetaInfoFilename;
					oMetaInfoFilename.Set(strDirectory + '/' + std::string(pMetaInfoString));
					VveMetaInfoDescription *pMetaInfo = 
						LoadMetaInfo(oMetaInfoFilename.GetFileName(i,j));
					
					// do we only have one block? Then, the meta info is for 
					// block AND timestep. And we shouldn't use the reference
					// twice, as both will delete their meta infos on
					// destruction. So, load twice.
					// (this may not be the most elegant solution, but meta
					// info files are in almost all cases small anyway)
					if(iFirstBlock == iLastBlock)
					{
						VveMetaInfoDescription *pMetaInfo2 =
							LoadMetaInfo(oMetaInfoFilename.GetFileName(i,j));
						pTimestep->SetMetaInfo(pMetaInfo2);
						pBlock->SetMetaInfo(pMetaInfo);
					}
					else
					{
						// check whether our file pattern included blocks
						// if so: meta info is for blocks
						// otherwise: for timestep only
						if(oMetaInfoFilename.IsBlockEnabled())
						{
							pBlock->SetMetaInfo(pMetaInfo);
						}
						else
						{
							pTimestep->SetMetaInfo(pMetaInfo);
						}
					}
					
					
					
				}


			}
			// add timestep to component
			pComponent->SetTimelevel(vecExplicitMapping[i].m_iLevel, pTimestep);
		} 
	}
	else
	{
		// explicitly using <File>
		// find all <File> info
		VistaXML::TiXmlElement *pFileEl = pFilesEl->FirstChildElement("File");
		if(!pFileEl)
		{
			// no <FilePattern> and no <File> --> no information, stop!!
			vstr::errp() << " [VveDatasetDescriptionXMLLoader]   no file definition for "
				<< "component " << strComponentName << "! Component could not be "
				<< "loaded." << std::endl;
			delete pComponent;
			if(pComponentFile) delete pComponentFile;
			return NULL;
		}

		while(pFileEl != NULL)
		{
			// read timestep and block information
			int iFirstBlock = 0;
			int iLastBlock = 0;
			int iTimestep = 0;
			pFileEl->QueryIntAttribute("timestep",&iTimestep);
			pFileEl->QueryIntAttribute("firstblock", &iFirstBlock);
			pFileEl->QueryIntAttribute("lastblock", &iLastBlock);
			if(iLastBlock < iFirstBlock)
				iLastBlock = iFirstBlock;

			VveDataSetName oName;
			oName.Set(pFileEl->GetText());

			// meta info given?
			const char* pMetaInfoString = pFileEl->Attribute("metainfo");

			// create timestep
			VveTimestepDescription *pTimestep = new VveTimestepDescription(eType);
			// create blocks
			for(int j = iFirstBlock; j <= iLastBlock; ++j)
			{
				// filename via pattern
				std::string strFilename = strDirectory + '/' + 
					oName.GetFileName(iTimestep, j);
				// create block
				VveBlockDescription *pBlock = new VveBlockDescription(strFilename,eType);
				// add block to timestep
				pTimestep->AddBlock(pBlock);


				// meta info?
				if(pMetaInfoString)
				{
					VveDataSetName oMetaInfoFilename;
					oMetaInfoFilename.Set(strDirectory + '/' + std::string(pMetaInfoString));
					VveMetaInfoDescription *pMetaInfo = 
						LoadMetaInfo(oMetaInfoFilename.GetFileName(0,j));

					// do we only have one block? Then, the meta info is for 
					// block AND timestep. And we shouldn't use the reference
					// twice, as both will delete their meta infos on
					// destruction. So, load twice.
					// (this may not be the most elegant solution, but meta
					// info files are in almost all cases small anyway)
					if(iFirstBlock == iLastBlock)
					{
						VveMetaInfoDescription *pMetaInfo2 =
							LoadMetaInfo(oMetaInfoFilename.GetFileName(0,j));
						pTimestep->SetMetaInfo(pMetaInfo2);
						pBlock->SetMetaInfo(pMetaInfo);
					}
					else
					{
						// check whether our file pattern included blocks
						// if so: meta info is for blocks
						// otherwise: for timestep only
						if(oMetaInfoFilename.IsBlockEnabled())
						{
							pBlock->SetMetaInfo(pMetaInfo);
						}
						else
						{
							pTimestep->SetMetaInfo(pMetaInfo);
						}
					}

					pMetaInfo->GetPropertyList()->GetValueOrDefault<float>("HelpMe",5);


				}
			}
			// add timestep to component
			pComponent->SetTimelevel(iTimestep, pTimestep);


			// next one
			pFileEl = pFileEl->NextSiblingElement("File");
		}

	}

	if(pComponentFile) delete pComponentFile;
	return pComponent;
}

VveMetaInfoDescription* VveDatasetDescriptionXMLLoader::LoadMetaInfo( VistaXML::TiXmlElement *pMetaInfoEl )
{
	assert(pMetaInfoEl);

	VistaPropertyList *pPropList = new VistaPropertyList();


	VistaXML::TiXmlElement *pElement = pMetaInfoEl->FirstChildElement();
	while(pElement != NULL)
	{
		// read text
		std::string strValue = pElement->GetText();
		std::string strName = pElement->ValueStr();

		VistaProperty oProperty(strName, strValue);
		pPropList->SetProperty(oProperty);

		pElement = pElement->NextSiblingElement();
	}

	VveMetaInfoDescription *pIndex = new VveMetaInfoDescription(pPropList);

	return pIndex;
}

VveMetaInfoDescription* VveDatasetDescriptionXMLLoader::LoadMetaInfo( std::string strIndexFile )
{
	VistaXML::TiXmlDocument oIndexFile;
	if(!oIndexFile.LoadFile(strIndexFile))
	{
		return NULL;
	}

	// top element is called "Index"
	VistaXML::TiXmlElement *pIndexEl = oIndexFile.FirstChildElement("MetaInfo");

	// not found?
	if(!pIndexEl)
		return NULL;

	// load index
	return LoadMetaInfo(pIndexEl);
}




VveEnsembleDescription* VveDatasetDescriptionXMLLoader::LoadEnsemble()
{
	VistaXML::TiXmlDocument oEnsembleFile;
	if(!oEnsembleFile.LoadFile(m_strFilename))
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader]   ensemble XML file "
			<< "could not be loaded!" 
			<< std::endl;
		return NULL;
	}

	// top element is called "Ensemble"
	VistaXML::TiXmlElement *pEnsembleEl = 
		oEnsembleFile.FirstChildElement("Ensemble");

	// not found?
	if(!pEnsembleEl)
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader]   the given XML file does not "
			<< "contain an ensemble (missing <Ensemble> top-level element)!"
			<< std::endl;
		return NULL;
	}

	// create ensemble
	VveEnsembleDescription *pEnsemble = new VveEnsembleDescription();

	// read name, if given
	const char* pAttr = pEnsembleEl->Attribute("name");
	if(pAttr)
	{
		pEnsemble->SetNameForNameable(pAttr);
	}

	// load description (if given)
	VistaXML::TiXmlElement *pDescriptionEl = 
		pEnsembleEl->FirstChildElement("Description");
	if(pDescriptionEl)
		pEnsemble->SetDescription(pDescriptionEl->GetText());

	// load meta info (if given)
	VistaXML::TiXmlElement *pMetaInfoEl = pEnsembleEl->FirstChildElement("MetaInfo");
	if(pMetaInfoEl)
	{
		VveMetaInfoDescription *pMetaInfo = LoadMetaInfo(pMetaInfoEl);
		if(pMetaInfo)
		{
			pEnsemble->SetMetaInfo(pMetaInfo);
		}
	}


	// now load all datasets
	VistaXML::TiXmlElement *pDatasetEl = pEnsembleEl->FirstChildElement("Dataset");
	if(pDatasetEl == NULL)
	{
		vstr::errp() << " [VveDatasetDescriptionXMLLoader]   ensemble does not contain "
			<< "any datasets!" << std::endl;
		delete pEnsemble;
		return NULL;
	}
	while(pDatasetEl != NULL)
	{
		// read dataset
		VveDatasetDescription *pDataset = LoadDataset(pDatasetEl, true);
		// add dataset
		if(pDataset != NULL)
		{
			if(!pEnsemble->AddDataset(pDataset))
			{
				vstr::errp() << " [VveDatasetDescriptionXMLLoader]   could not add "
					<< "dataset to ensemble." << std::endl;
			}
		}

		// next one
		pDatasetEl = pDatasetEl->NextSiblingElement("Dataset");
	}

	return pEnsemble;
}












/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

