/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEDATASETDESCRIPTIONXMLLOADER_H
#define _VVEDATASETDESCRIPTIONXMLLOADER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <string>
#include <VistaTools/tinyXML/tinyxml.h>
#include <VistaVisExt/VistaVisExtConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveDatasetDescription;
class VveEnsembleDescription;
class VveComponentDescription;
class VveMetaInfoDescription;
class VveTimeMapper;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/* Loads a VveDatasetDescription or VveEnsembleDescription construct from an
 * xml file. Does not load any actual heavy data,
 * only creates the internal representation from an xml file. */
class VISTAVISEXTAPI VveDatasetDescriptionXMLLoader 
{
public:
	
	VveDatasetDescriptionXMLLoader(std::string strFilename);
	virtual ~VveDatasetDescriptionXMLLoader();

	/* Load a dataset from the file. Returns NULL if an error occurred. */
	VveDatasetDescription*		LoadDataset(bool bMustHaveName = false);

	/* Load an ensemble from the file (also loads any contained datasets).
	 * Returns NULL if the ensemble could not be loaded. */
	VveEnsembleDescription*	LoadEnsemble();


protected:

	VveComponentDescription*	LoadComponent(VistaXML::TiXmlElement *pComponentEl,
		VveTimeMapper *pTimeMapper);

	VveDatasetDescription*		LoadDataset(VistaXML::TiXmlElement *pDatasetEl,
		bool bMustHaveName = false);

	VveMetaInfoDescription*		LoadMetaInfo(VistaXML::TiXmlElement *pMetaInfoEl);
	VveMetaInfoDescription*		LoadMetaInfo(std::string strIndexFile);

	std::string			m_strFilename;
	std::string			m_strDirectory;

};

#endif 


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
