/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      FileName :  VveIOUtils.cpp               RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/

#include "VveIOUtils.h"
#include "../Data/VveCartesianGrid.h"
#include "../Data/VveTetGrid.h"
#include "../Algorithms/VveTetGridPointLocator.h"

#include <VistaBase/VistaTimer.h>
#include <VistaBase/Half/VistaHalf.h>
#include <VistaBase/VistaStreamUtils.h>

#include <VistaBase/VistaSerializingToolset.h>
#include <VistaAspects/VistaSerializer.h>
#include <VistaAspects/VistaDeSerializer.h>

#include <VistaTools/VistaFileSystemFile.h>
#include <VistaTools/tinyXML/tinyxml.h>

#include <vtkStructuredPointsReader.h>
#include <vtkPointData.h>
#include <vtkStructuredPoints.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnsignedCharArray.h>
#include <vtkCell.h>
#include <vtkImageData.h>


using namespace VistaType;

/*============================================================================*/
/*  FORWARD DECLARATIONS                                                      */
/*============================================================================*/

static inline int ComputeTextureDimensions(int iCount, int &iWidth, int &iHeight);

static bool ReadElementAttribute(const VistaXML::TiXmlElement *pParentElement,
								 const std::string &strChildElementName, 
								 const std::string &strAttName,
								 std::string &strOutAttVal);

static const std::string STR_TM_SIG = "__VveTimeMapper__";

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadCartesianGridVtkFile                                    */
/*                                                                            */
/*============================================================================*/
bool VveIOUtils::LoadCartesianGridVtkFile(VveCartesianGrid *pTarget,
										   const std::string &strFileName,
										   bool bPadData,
										   int iTargetType,
										   const std::string &strActiveScalars,
										   const std::string &strActiveVectors,
										   const std::string &strScalarsName,
										   const std::string &strVectorsName,
										   float fNormRangeMin,
										   float fNormRangeMax)
{
	if (!pTarget)
	{
		vstr::errp() << " [VVE:LoadCartesianGridVtkFile] no data target given..." << endl;
		return false;
	}

	// check for existence of data file
	{
		VistaFileSystemFile oFile(strFileName);
		if (!oFile.Exists())
		{
			vstr::warnp() << " [VVE:LoadCartesianGridVtkFile] unable to access file..." << endl;
			vstr::warni() << "                                         file name: " << strFileName << endl;
			return false;
		}
	}

	bool bNormalize = (fNormRangeMin!=fNormRangeMax);
	float fNormScale = 1.0f;
	float fNormBias = 0.0f;
	if (bNormalize)
	{
		fNormBias = -fNormRangeMin;
		fNormScale = 1.0f/(fNormRangeMax-fNormRangeMin);
	}

	vtkStructuredPointsReader *pReader = vtkStructuredPointsReader::New();
	pReader->SetFileName(strFileName.c_str());
	if (!strScalarsName.empty())
		pReader->SetScalarsName(strScalarsName.c_str());
	else
		pReader->ReadAllScalarsOn();
	if (!strVectorsName.empty())
		pReader->SetVectorsName(strVectorsName.c_str());
	else
		pReader->ReadAllVectorsOn();
	pReader->Update();

	// read and parse data
	vtkStructuredPoints *pPoints = pReader->GetOutput();
	if (!pPoints)
	{
		vstr::warnp() << " [VVE:LoadCartesianGridVtkFile] unable to load data set..." << endl;
		pReader->Delete();
		return false;
	}

	vtkPointData *pPointData = pPoints->GetPointData();
	if (!pPointData)
	{
		vstr::warnp() << " [VVE:LoadCartesianGridVtkFile] unable to retrieve point data..." << endl;
		pReader->Delete();
		return false;
	}
	if (!strActiveScalars.empty())
		pPointData->SetActiveScalars(strActiveScalars.c_str());
	if (!strActiveVectors.empty())
		pPointData->SetActiveVectors(strActiveVectors.c_str());

	int aDimensions[3];
	int aDataDim[3];
	float aBounds[6];
	float aRange[2];
	int iPointCount;

	pPoints->GetDimensions(aDataDim);
#if (VTK_MAJOR_VERSION >= 5)
	double dblBounds[6];
	double dblRange[2];
	pPoints->GetBounds(dblBounds);
	aBounds[0] = dblBounds[0];
	aBounds[1] = dblBounds[1];
	aBounds[2] = dblBounds[2];
	aBounds[3] = dblBounds[3];
	aBounds[4] = dblBounds[4];
	aBounds[5] = dblBounds[5];
	pPoints->GetScalarRange(dblRange);
	aRange[0] = dblRange[0];
	aRange[1] = dblRange[1];
#else
	pPoints->GetBounds(aBounds);
	pPoints->GetScalarRange(aRange);
#endif
	iPointCount = pPoints->GetNumberOfPoints();
	if (bNormalize)
	{
		aRange[0] = 0.0f;
		aRange[1] = 1.0f;
	}

	if (iPointCount != aDataDim[0] * aDataDim[1] * aDataDim[2])
	{
		vstr::warnp() << " [VVE:LoadCartesianGridVtkFile] point count mismatch..." << endl;
		pReader->Delete();
		return false;
	}

	// find out about data dimensions and pad data accordingly
	if (bPadData)
	{
		for (int i=0; i<3; ++i)
		{
			aDimensions[i] = 1;
			while (aDimensions[i] < aDataDim[i])
				aDimensions[i] *= 2;
		}
	}
	else
	{
		memcpy(aDimensions, aDataDim, 3*sizeof(int));
	}

	// find out about data dimensionality
	if (!strActiveScalars.empty())
		pPointData->SetActiveScalars(strActiveScalars.c_str());
	if (!strActiveVectors.empty())
		pPointData->SetActiveVectors(strActiveVectors.c_str());

	int iComponents = 0;
	vtkDataArray *pVectors = pPointData->GetVectors();
	vtkDataArray *pScalars = pPointData->GetScalars();

	if (pVectors)
		iComponents += 3;
	if (pScalars)
		++iComponents;

	if (!iComponents)
	{
		vstr::warnp() << " [VVE:LoadCartesianGridVtkFile] unable to retrieve any data..." << endl;
		pReader->Delete();
		return false;
	}

	// re-allocate memory, if necessary
	int x, y, z, iTemp;
	pTarget->GetDimensions(x, y, z);
	iTemp = pTarget->GetComponents();

	if (x!=aDimensions[0] || y!=aDimensions[1] || z!=aDimensions[2] || iTemp!=iComponents
		|| iTargetType != pTarget->GetDataType())
	{
		if (!pTarget->Reset(aDimensions[0], aDimensions[1], aDimensions[2], iComponents, iTargetType))
		{
			pReader->Delete();
			return false;
		}
	}

	// write data into cartesian grid
	float aZero[4] = { 0, 0, 0, 0 };
	VistaHalf aZeroHalf[4] = { 0, 0, 0, 0};
	unsigned char aZeroUChar[4] = { 0, 0, 0, 0};
	float aVec[4] = { 0, 0, 0, 0 };
	VistaHalf aVecHalf[4] = { 0, 0, 0, 0};
	unsigned char aVecUChar[4] = { 0, 0, 0, 0};
	int iDataPos = 0;
	for (int z=0; z<aDimensions[2]; ++z)
	{
		for (int y=0; y<aDimensions[1]; ++y)
		{
			for (int x=0; x<aDimensions[0]; ++x)
			{
				if (z>=aDataDim[2] || y>=aDataDim[1] || x>=aDataDim[0])
				{
					switch (iTargetType)
					{
					case VveCartesianGrid::DT_FLOAT:
						pTarget->SetData(x, y, z, aZero);
						break;
					case VveCartesianGrid::DT_HALF:
						pTarget->SetData(x, y, z, aZeroHalf);
						break;
					case VveCartesianGrid::DT_UNSIGNED_CHAR:
						pTarget->SetData(x, y, z, aZeroUChar);
						break;
					}
				}
				else
				{
					if (pVectors)
					{
#if (VTK_MAJOR_VERSION >= 5)
						double dblVector[3];
						pVectors->GetTuple(iDataPos, dblVector);
						aVec[0] = dblVector[0];
						aVec[1] = dblVector[1];
						aVec[2] = dblVector[2];
#else
						pVectors->GetTuple(iDataPos, aVec);
#endif
						if (bNormalize)
						{
							for (int i=0; i<3; ++i)
							{
								aVec[i] = (aVec[i]+fNormBias)*fNormScale;
								if (aVec[i]<0.0f)
									aVec[i] = 0.0f;
								else if (aVec[i]>1.0f)
									aVec[i] = 1.0f;
							}
						}
					}
					if (pScalars)
					{
#if (VTK_MAJOR_VERSION >= 5)
						double nScalar;
						pScalars->GetTuple(iDataPos, &nScalar);
						aVec[iComponents-1] = nScalar;
#else
						pScalars->GetTuple(iDataPos, &aVec[iComponents-1]);
#endif
						if (bNormalize)
						{
							aVec[iComponents-1] = (aVec[iComponents-1]+fNormBias)*fNormScale;
								if (aVec[iComponents-1]<0.0f)
									aVec[iComponents-1] = 0.0f;
								else if (aVec[iComponents-1]>1.0f)
									aVec[iComponents-1] = 1.0f;
						}
					}
					switch (iTargetType)
					{
					case VveCartesianGrid::DT_FLOAT:
						pTarget->SetData(x, y, z, aVec);
						break;
					case VveCartesianGrid::DT_HALF:
						for (int i=0; i<iComponents; ++i)
							aVecHalf[i] = VistaHalf(aVec[i]);
						pTarget->SetData(x, y, z, aVecHalf);
						break;
					case VveCartesianGrid::DT_UNSIGNED_CHAR:
						for (int i=0; i<iComponents; ++i)
							aVecUChar[i] = (unsigned char)(aVec[i]*255);
						pTarget->SetData(x, y, z, aVecUChar);
						break;
					}
					++iDataPos;
				}
			}
		}
	}
	pTarget->SetDataDimensions(aDataDim[0], aDataDim[1], aDataDim[2]);
	pTarget->SetBounds(VistaVector3D(aBounds[0], aBounds[2], aBounds[4]),
		VistaVector3D(aBounds[1], aBounds[3], aBounds[5]));
	pTarget->SetScalarRange(aRange);
	pTarget->SetValid(true);

	pReader->Delete();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadTetGridVtkFile                                          */
/*                                                                            */
/*============================================================================*/
bool VveIOUtils::LoadTetGridVtkFile(VveTetGrid *pTarget,
									 const std::string &strFileName,
									 const std::string &strOutputFileBase,
									 bool bPadData,
									 const std::string &strActiveScalars,
									 const std::string &strActiveVectors,
									 const std::string &strScalarsName,
									 const std::string &strVectorsName,
									 bool bSkipTopology)
{
	if (!pTarget)
	{
		vstr::errp() << " [VVE:LoadTetGridVtkFile] no data target given..." << endl;
		return false;
	}

	// check for existence of data file
	{
		VistaFileSystemFile oFile(strFileName);
		if (!oFile.Exists())
		{
			vstr::warnp() << " [VVE:LoadTetGridVtkFile] unable to access file..." << endl;
			vstr::warni() << "                                      file name: " << strFileName << endl;
			return false;
		}
	}

	vtkUnstructuredGridReader *pReader = vtkUnstructuredGridReader::New();
	pReader->SetFileName(strFileName.c_str());
	if (!strScalarsName.empty())
		pReader->SetScalarsName(strScalarsName.c_str());
	if (!strVectorsName.empty())
		pReader->SetVectorsName(strVectorsName.c_str());

	pReader->Update();

	// read and parse data
	vtkUnstructuredGrid *pGrid = pReader->GetOutput();
	if (!pGrid)
	{
		vstr::warnp() << " [VVE:LoadTetGridVtkFile] unable to load data set..." << endl;
		pReader->Delete();
		return false;
	}

	vtkPointData *pPointData = pGrid->GetPointData();
	if (!pPointData)
	{
		vstr::warnp() << " [VVE:LoadTetGridVtkFile] unable to retrieve point data..." << endl;
		pReader->Delete();
		return false;
	}

	int iPointCount = pGrid->GetNumberOfPoints();

	// find out about data dimensionality
	if (!strActiveScalars.empty())
		pPointData->SetActiveScalars(strActiveScalars.c_str());
	if (!strActiveVectors.empty())
		pPointData->SetActiveVectors(strActiveVectors.c_str());

	int iComponents = 0;
	vtkDataArray *pVectors = pPointData->GetVectors();
	if (!pVectors)
	{
		vstr::warnp() << " [VVE:LoadTetGridVtkFile] no vectors found..." << endl;
	}

	vtkDataArray *pScalars = pPointData->GetScalars();
	if (!pScalars)
	{
		vstr::warnp() << " [VVE:LoadTetGridVtkFile] no scalars found..." << endl;
	}

	if (pVectors)
		iComponents += 3;
	if (pScalars)
		++iComponents;

	if (!iComponents)
	{
		vstr::warnp() << " [VVE:LoadTetGridVtkFile] unable to retrieve any data..." << endl;
		pReader->Delete();
		return false;
	}

	float *pDataArray = VveTetGrid::NewFloatArray(iComponents*iPointCount);
	float *pDataPos = pDataArray;
	double aVector[3];

	if (pVectors)
	{
		for (int j=0; j<iPointCount; ++j)
		{
			pVectors->GetTuple(j, aVector);
			pDataPos[0] = float (aVector[0]);
			pDataPos[1] = float (aVector[1]);
			pDataPos[2] = float (aVector[2]);
			pDataPos += iComponents;

			//pVectors->GetTuple(j, pDataPos);
			//pDataPos += 4;
		}
	}

	double dScalar[1];
	pDataPos = &pDataArray[3];
	if (pScalars)
	{
		for (int j=0; j<iPointCount; ++j)
		{
			pScalars->GetTuple(j, dScalar);
			pDataPos[0] = float (dScalar[0]);
			pDataPos+=iComponents;

			//pScalars->GetTuple(j, pDataPos);
			//pDataPos+=4;
		}
	}

	int iCellCount = 0;
	if (!bSkipTopology)
	{
		float *pVertices = VveTetGrid::NewFloatArray(3*iPointCount);
		float *pPos = pVertices;
		double aDoublePos[3];

		for (int j=0; j<iPointCount; ++j)
		{
			pGrid->GetPoint(j, aDoublePos);
			(*pPos) = (float) aDoublePos[0];
			pPos++;
			(*pPos) = (float) aDoublePos[1];
			pPos++;
			(*pPos) = (float) aDoublePos[2];
			pPos++;
			// old (float) code
			//pGrid->GetPoint(j, pPos);
			//pPos += 3;
		}

		int iNonTetraCells = 0;
		int iTetraCells = 0;
		iCellCount = pGrid->GetNumberOfCells();

#if 1
		vtkUnsignedCharArray *pCellTypeArray = pGrid->GetCellTypesArray();
		unsigned char *pCellTypes = pCellTypeArray->GetPointer(0);
		unsigned char *pCurrentCellType = pCellTypes;
		for (int j=0; j<iCellCount; ++j, ++pCurrentCellType)
		{
			if (*pCurrentCellType == VTK_TETRA)
				++iTetraCells;
			else
				++iNonTetraCells;
		}

		int *pCells = VveTetGrid::NewIntArray(4*iTetraCells);
		int *pCellPos = pCells;
		vtkIdType *pTemp;
		vtkIdType iCount;
		if (iNonTetraCells)
		{
			vstr::warnp() << " [VVE:LoadTetGridVtkFile] skipping "
				<< iNonTetraCells << " non-tetrahedral cells..." << endl;

			pCurrentCellType = pCellTypes;
			for (vtkIdType j=0; j<iCellCount; ++j, ++pCurrentCellType)
			{
				if (*pCurrentCellType == VTK_TETRA)
				{
					pGrid->GetCellPoints(j, iCount, pTemp);
					*(pCellPos++) = *(pTemp++);
					*(pCellPos++) = *(pTemp++);
					*(pCellPos++) = *(pTemp++);
					*(pCellPos++) = *(pTemp++);
				}
			}
		}
		else
		{
			for (int j=0; j<iCellCount; ++j)
			{
				pGrid->GetCellPoints(j, iCount, pTemp);
				*(pCellPos++) = *(pTemp++);
				*(pCellPos++) = *(pTemp++);
				*(pCellPos++) = *(pTemp++);
				*(pCellPos++) = *(pTemp++);
			}
		}
#else
		for (int j=0; j<iCellCount; ++j)
		{
			if (pGrid->GetCellType(j) == VTK_TETRA)
				++iTetraCells;
			else
				++iNonTetraCells;
		}
		if (iNonTetraCells)
		{
			vstr::warnp() << " [VVE:LoadTetGridVtkFile] skipping "
				<< iNonTetraCells << " non-tetrahedral cells..." << endl;
		}

		int *pCells = VveTetGrid::NewIntArray(4*iTetraCells);
		int *pCellPos = pCells;
		int *pTemp;
		int iCount;
		for (int j=0; j<iCellCount; ++j)
		{
			if (pGrid->GetCellType(j) == VTK_TETRA)
			{
				pGrid->GetCellPoints(j, iCount, pTemp);
				*(pCellPos++) = *(pTemp++);
				*(pCellPos++) = *(pTemp++);
				*(pCellPos++) = *(pTemp++);
				*(pCellPos++) = *(pTemp++);
			}
		}
#endif
		iCellCount = iTetraCells;

		pTarget->SetVertices(pVertices);
		pTarget->SetCellCount(iCellCount);
		pTarget->SetPaddedCellCount(iCellCount);
		pTarget->SetCells(pCells);
	}
	pTarget->SetVertexCount(iPointCount);
	pTarget->SetPaddedVertexCount(iPointCount);
	pTarget->SetPointData(pDataArray);
	pTarget->SetComponents(iComponents);
	pTarget->GatherInformation();

	if (!bSkipTopology)
	{
		// read or create neighboring information
		string strNeighborFile = strFileName + ".neigh";
		ifstream is;
		is.open(strNeighborFile.c_str(), ios::in | ios::binary);
		if (!is.is_open())
		{
			vstr::warnp() << " [VVE:LoadTetGridVtkFile] unable to load neighboring information..." << endl;
			vstr::outi() << "                                        starting computation..." << endl;
			VistaTimer oTimer;

			pTarget->ComputeCellNeighbors();

			vstr::outi() << "                                        computation finished..." << endl;
			vstr::outi() << "                                        [time needed: " << oTimer.GetLifeTime() << "]" << endl;

			if (!strOutputFileBase.empty())
			{
				vstr::outi() << " [VVE:LoadTetGridVtkFile] - writing neighboring information to file..." << endl;
				string strOutputFile = strOutputFileBase + ".neigh";

				ofstream os(strOutputFile.c_str(), ios::out | ios::binary);
				if (os.is_open())
				{
					int *pNeighbors = pTarget->GetCellNeighbors();

					// swap if we're on a big-endian platform
					if (VistaSerializingToolset::GetPlatformEndianess() == VistaSerializingToolset::VST_BIGENDIAN)
						VistaSerializingToolset::Swap(pNeighbors, sizeof(int), 4*iCellCount);

					int *pPos = pNeighbors;
					for (int j=0; j<iCellCount; ++j, pPos+=4)
						os.write((const char *)pPos, 4*sizeof(int));

					// well, swap it back...
					if (VistaSerializingToolset::GetPlatformEndianess() == VistaSerializingToolset::VST_BIGENDIAN)
						VistaSerializingToolset::Swap(pNeighbors, sizeof(int), 4*iCellCount);
				}
				else
				{
					vstr::warnp() << " [VVE:LoadTetGridVtkFile] unable to open file for output..." << endl;
				}
			}
		}
		else
		{
			int *pNeighbors = VveTetGrid::NewIntArray(4*iCellCount);
			int *pPos = pNeighbors;
#if 0
			for (int j=0; j<iCellCount; ++j, pPos+=4)
				is.read((char *)pPos, 4*sizeof(int));
#else
			is.read((char *)pPos, 4*iCellCount*sizeof(int));
#endif

			if (VistaSerializingToolset::GetPlatformEndianess() == VistaSerializingToolset::VST_BIGENDIAN)
				VistaSerializingToolset::Swap(pNeighbors, sizeof(int), 4*iCellCount);

			pTarget->SetCellNeighbors(pNeighbors);
		}
		is.close();

		// read or create data for point locator
		VveTetGridPointLocator *pLocator = pTarget->GetPointLocator();
		if (pLocator)
		{
			string strKdTreeFile = strFileName + ".kdtree";
			is.open(strKdTreeFile.c_str(), ios::in | ios::binary);
			if (!is.is_open())
			{
				vstr::warnp() << " [VVE:LoadTetGridVtkFile] unable to load kd-tree information..." << endl;
				vstr::outi() << "                                        starting computation..." << endl;
				VistaTimer oTimer;

				pLocator->ComputeInformation();

				vstr::outi() << "                                        computation finished..." << endl;
				vstr::outi() << "                                        [time needed: " << oTimer.GetLifeTime() << "]" << endl;

				if (!strOutputFileBase.empty())
				{
					vstr::outi() << " [VVE:LoadTetGridVtkFile] - writing kd-tree information to file..." << endl;
					string strOutputFile = strOutputFileBase + ".kdtree";

					ofstream os(strOutputFile.c_str(), ios::out | ios::binary);
					if (os.is_open())
					{
						int iTreeRoot = pLocator->GetTreeRoot();
						int *pTree = pLocator->GetKdTree();
						int *pIncident = pLocator->GetIncidentCells();

						// swap if we're on a big-endian platform
						if (VistaSerializingToolset::GetPlatformEndianess() == VistaSerializingToolset::VST_BIGENDIAN)
						{
							VistaSerializingToolset::Swap(&iTreeRoot, sizeof(int), 1);
							VistaSerializingToolset::Swap(pTree, sizeof(int), 2*iPointCount);
							VistaSerializingToolset::Swap(pIncident, sizeof(int), iPointCount);
						}

						os.write((const char *)&iTreeRoot, sizeof(int));
						os.write((const char *)pTree, 2*iPointCount*sizeof(int));
						os.write((const char *)pIncident, iPointCount*sizeof(int));

						// reverse swapping
						if (VistaSerializingToolset::GetPlatformEndianess() == VistaSerializingToolset::VST_BIGENDIAN)
						{
							VistaSerializingToolset::Swap(pTree, sizeof(int), 2*iPointCount);
							VistaSerializingToolset::Swap(pIncident, sizeof(int), iPointCount);
						}
					}
					else
					{
						vstr::warnp() << " [VVE:LoadTetGridVtkFile] unable to open file for output..." << endl;
					}
				}
			}
			else
			{
				int iTreeRoot;
				int *pTree = VveTetGrid::NewIntArray(2*iPointCount);
				int *pIncident = VveTetGrid::NewIntArray(iPointCount);

				is.read((char *)&iTreeRoot, sizeof(int));
				is.read((char *)pTree, 2*iPointCount*sizeof(int));
				is.read((char *)pIncident, iPointCount*sizeof(int));

				// swap if we're on a big-endian platform
				if (VistaSerializingToolset::GetPlatformEndianess() == VistaSerializingToolset::VST_BIGENDIAN)
				{
					VistaSerializingToolset::Swap(&iTreeRoot, sizeof(int), 1);
					VistaSerializingToolset::Swap(pTree, sizeof(int), 2*iPointCount);
					VistaSerializingToolset::Swap(pIncident, sizeof(int), iPointCount);
				}

				pLocator->SetTreeRoot(iTreeRoot);
				pLocator->SetKdTree(pTree);
				pLocator->SetIncidentCells(pIncident);
			}
		}
	}

	// pad data, if necessary
	if (bPadData)
	{
		int iWidth, iHeight;
		ComputeTextureDimensions(pTarget->GetVertexCount(),
			iWidth, iHeight);
		int iLocalVertexCount = iWidth*iHeight;

		ComputeTextureDimensions(pTarget->GetCellCount(),
			iWidth, iHeight);
		int iLocalCellCount = iWidth*iHeight;

		pTarget->PadData(iLocalVertexCount, iLocalCellCount);
	}

	pTarget->SetValid(true);

	pReader->Delete();

	return true;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadCartesianGridVtkImageData                               */
/*                                                                            */
/*============================================================================*/
bool VveIOUtils::LoadCartesianGridVtkImageData(VveCartesianGrid *pTarget,
										   vtkImageData *pImageData,
										   bool bPadData,
										   int iTargetType,
										   const std::string &strActiveScalars,
										   const std::string &strActiveVectors,
										   const std::string &strScalarsName,
										   const std::string &strVectorsName)
{
	if (!pTarget)
	{
		vstr::errp() << " [VVE:LoadCartesianGridVtkImageData] no data target given..." << endl;
		return false;
	}

	// read and parse data
	//vtkStructuredPoints *pPoints = pReader->GetOutput();   --> pImageData

	vtkPointData *pPointData = pImageData->GetPointData(); //pPoints->GetPointData();
	if (!pPointData)
	{
		vstr::warnp() << " [VVE:LoadCartesianGridVtkImageData] unable to retrieve point data..." << endl;
		return false;
	}
	if (!strActiveScalars.empty())
		pPointData->SetActiveScalars(strActiveScalars.c_str());
	if (!strActiveVectors.empty())
		pPointData->SetActiveVectors(strActiveVectors.c_str());

	int aDimensions[3];
	int aDataDim[3];
	float aBounds[6];
	float aRange[2];
	int iPointCount;

	pImageData->GetDimensions(aDataDim);
	double dblBounds[6];
	double dblRange[2];
	pImageData->GetBounds(dblBounds);
	aBounds[0] = dblBounds[0];
	aBounds[1] = dblBounds[1];
	aBounds[2] = dblBounds[2];
	aBounds[3] = dblBounds[3];
	aBounds[4] = dblBounds[4];
	aBounds[5] = dblBounds[5];
	pImageData->GetScalarRange(dblRange);
	aRange[0] = dblRange[0];
	aRange[1] = dblRange[1];

	iPointCount = pImageData->GetNumberOfPoints();

	if (iPointCount != aDataDim[0] * aDataDim[1] * aDataDim[2])
	{
		vstr::warnp() << " [VVE:LoadCartesianGridVtkImageData] point count mismatch..." << endl;
		return false;
	}

	// find out about data dimensions and pad data accordingly
	if (bPadData)
	{
		for (int i=0; i<3; ++i)
		{
			aDimensions[i] = 1;
			while (aDimensions[i] < aDataDim[i])
				aDimensions[i] *= 2;
		}
	}
	else
	{
		memcpy(aDimensions, aDataDim, 3*sizeof(int));
	}

	// find out about data dimensionality
	if (!strActiveScalars.empty())
		pPointData->SetActiveScalars(strActiveScalars.c_str());
	if (!strActiveVectors.empty())
		pPointData->SetActiveVectors(strActiveVectors.c_str());

	int iComponents = 0;
	vtkDataArray *pVectors = pPointData->GetVectors();
	vtkDataArray *pScalars = pPointData->GetScalars();

	if (pVectors)
		iComponents += 3;
	if (pScalars)
		++iComponents;

	if (!iComponents)
	{
		vstr::warnp() << " [VVE:LoadCartesianGridVtkImageData] unable to retrieve any data..." << endl;
		return false;
	}

	// re-allocate memory, if necessary
	int x, y, z, iTemp;
	pTarget->GetDimensions(x, y, z);
	iTemp = pTarget->GetComponents();

	if (x!=aDimensions[0] || y!=aDimensions[1] || z!=aDimensions[2] || iTemp!=iComponents
		|| iTargetType != pTarget->GetDataType())
	{
		if (!pTarget->Reset(aDimensions[0], aDimensions[1], aDimensions[2], iComponents, iTargetType))
		{
			return false;
		}
	}

	// write data into cartesian grid
	float aZero[4] = { 0, 0, 0, 0 };
	VistaHalf aZeroHalf[4] = { 0, 0, 0, 0};
	unsigned char aZeroUChar[4] = { 0, 0, 0, 0};
	float aVec[4] = { 0, 0, 0, 0 };
	VistaHalf aVecHalf[4] = { 0, 0, 0, 0};
	unsigned char aVecUChar[4] = { 0, 0, 0, 0};
	int iDataPos = 0;
	for (int z=0; z<aDimensions[2]; ++z)
	{
		for (int y=0; y<aDimensions[1]; ++y)
		{
			for (int x=0; x<aDimensions[0]; ++x)
			{
				if (z>=aDataDim[2] || y>=aDataDim[1] || x>=aDataDim[0])
				{
					switch (iTargetType)
					{
					case VveCartesianGrid::DT_FLOAT:
						pTarget->SetData(x, y, z, aZero);
						break;
					case VveCartesianGrid::DT_HALF:
						pTarget->SetData(x, y, z, aZeroHalf);
						break;
					case VveCartesianGrid::DT_UNSIGNED_CHAR:
						pTarget->SetData(x, y, z, aZeroUChar);
						break;
					}
				}
				else
				{
					if (pVectors)
					{
						double dblVector[3];
						pVectors->GetTuple(iDataPos, dblVector);
						aVec[0] = dblVector[0];
						aVec[1] = dblVector[1];
						aVec[2] = dblVector[2];

					}
					if (pScalars)
					{
						double nScalar;
						pScalars->GetTuple(iDataPos, &nScalar);
						aVec[iComponents-1] = nScalar;
					}
					switch (iTargetType)
					{
					case VveCartesianGrid::DT_FLOAT:
						pTarget->SetData(x, y, z, aVec);
						break;
					case VveCartesianGrid::DT_HALF:
						for (int i=0; i<iComponents; ++i)
							aVecHalf[i] = VistaHalf(aVec[i]);
						pTarget->SetData(x, y, z, aVecHalf);
						break;
					case VveCartesianGrid::DT_UNSIGNED_CHAR:
						for (int i=0; i<iComponents; ++i)
							aVecUChar[i] = (unsigned char)(aVec[i]*255);
						pTarget->SetData(x, y, z, aVecUChar);
						break;
					}
					++iDataPos;
				}
			}
		}
	}
	pTarget->SetDataDimensions(aDataDim[0], aDataDim[1], aDataDim[2]);
	pTarget->SetBounds(VistaVector3D(aBounds[0], aBounds[2], aBounds[4]),
		VistaVector3D(aBounds[1], aBounds[3], aBounds[5]));
	pTarget->SetScalarRange(aRange);
	pTarget->SetValid(true);

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ReadLegacySeedFile                                          */
/*                                                                            */
/*============================================================================*/
bool VveIOUtils::ReadLegacySeedFile(const std::string& strInFile,
	double *&dSeeds,
	int &iNumSeeds)
{
	ifstream inFile(strInFile.c_str());
	if(!inFile.is_open())
	{
		vstr::errp() << "[VveIOUtils::ReadLegacySeedFile] Unable to open file <"
		 << strInFile << "> for writing!" << endl;
		return false;
	}
	inFile >> iNumSeeds;
	dSeeds = new double[4*iNumSeeds];
	int i=0;
	for(; i<iNumSeeds; ++i)
	{
		inFile >> dSeeds[4*i];
		if(!inFile.good())
			break;
		inFile >> dSeeds[4*i+1];
		if(!inFile.good())
			break;
		inFile >> dSeeds[4*i+2];
		if(!inFile.good())
			break;
		inFile >> dSeeds[4*i+3];
		if(!inFile.good())
			break;
	}
	inFile.close();
	//if we failed => cleanup
	if(iNumSeeds != i)
	{
		delete[] dSeeds;
		dSeeds = NULL;
		iNumSeeds = 0;
	}
	return i==iNumSeeds;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   WriteLegacySeedFile		                                  */
/*                                                                            */
/*============================================================================*/
bool VveIOUtils::WriteLegacySeedFile(double dSeeds[],
	int iNumSeeds,
	const std::string& strOutFile)
{
#ifdef DEBUG
	vstr::debugi() << "[VveIOUtils::WriteLegacySeedFile] Writing seed with ";
	vstr::debugi() << iNumSeeds << " points to " << endl << "\t" << strOutFile << endl;
#endif
	ofstream outFile(strOutFile.c_str());
	if(!outFile.is_open())
	{
		vstr::errp() << "[VveIOUtils::WriteLegacySeedFile] Unable to open file <"
			<< strOutFile << "> for writing!" << endl;
		return false;
	}

	outFile << iNumSeeds << endl;
	for(int pt=0; pt<iNumSeeds; ++pt)
	{
		outFile << dSeeds[4*pt]   << " " << dSeeds[4*pt+1] << " ";
		outFile << dSeeds[4*pt+2] << " " << dSeeds[4*pt+3] << endl;
	}

	outFile.close();
	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadXMLTimeFile				                              */
/*                                                                            */
/*============================================================================*/
bool VveIOUtils::LoadXMLTimeFile( const std::string &strTimeFile, VveTimeMapper *pTargetTM )
{
	//try and open the xml document for the time mapper
	VistaXML::TiXmlDocument oTimeFile;
	if(!oTimeFile.LoadFile(strTimeFile))
	{
		vstr::errp() << "[VveIOUtils::LoadXMLTimeFile] Failed to parse time file <"<<strTimeFile.c_str()<<">"<<endl;
		return false;
	}
	//check for time mapper type here
	VistaXML::TiXmlElement *pTimeConfig = oTimeFile.FirstChildElement("TimeMapper");

	bool bImplicitMapping = false;
	const char* pTypeAttr = pTimeConfig->Attribute("Type");
	if(pTypeAttr == NULL) 
		pTypeAttr = pTimeConfig->Attribute("type");
	if(std::string(pTypeAttr) == "implicit")
		bImplicitMapping = true;

	//exit if explicit timing is given
	if(bImplicitMapping)
	{
		//parse implicit time mapper config
		VveTimeMapper::Timings oTimings;

		std::string strValue="";
		if(!ReadElementAttribute(pTimeConfig, "NumTimeSteps", "value", strValue))
			return false;
		oTimings.m_iNumberOfLevels = VistaAspectsConversionStuff::ConvertToInt(strValue);

		if(!ReadElementAttribute(pTimeConfig, "NumTimeIndices", "value", strValue))
			return false;
		oTimings.m_iNumberOfIndices = VistaAspectsConversionStuff::ConvertToInt(strValue);

		if(!ReadElementAttribute(pTimeConfig, "FirstTimestep", "value", strValue))
			return false;
		oTimings.m_iFirstLevel = VistaAspectsConversionStuff::ConvertToInt(strValue);

		if(!ReadElementAttribute(pTimeConfig, "Stride", "value", strValue))
			return false;
		oTimings.m_iLevelStride = VistaAspectsConversionStuff::ConvertToInt(strValue);

		if(!ReadElementAttribute(pTimeConfig, "SimStart", "value", strValue))
			return false;
		oTimings.m_dSimStart = VistaAspectsConversionStuff::ConvertToDouble(strValue);

		if(!ReadElementAttribute(pTimeConfig, "SimEnd", "value", strValue))
			return false;
		oTimings.m_dSimEnd = VistaAspectsConversionStuff::ConvertToDouble(strValue);

		if(!ReadElementAttribute(pTimeConfig, "VisStart", "value", strValue))
			return false;
		oTimings.m_dVisStart = VistaAspectsConversionStuff::ConvertToDouble(strValue);

		if(!ReadElementAttribute(pTimeConfig, "VisEnd", "value", strValue))
			return false;
		oTimings.m_dVisEnd = VistaAspectsConversionStuff::ConvertToDouble(strValue);

		pTargetTM->SetByImplicitTimeMapping(oTimings);
	}	
	else
	{
		std::vector<VveTimeMapper::IndexInfo> vecIndexInfo;

		std::string strValue="";
		/*if(!ReadElementAttribute(pTimeConfig, "NumTimeIndices", "value", strValue))
			return false;
		int iNumberOfIndices = VistaAspectsConversionStuff::ConvertToInt(strValue);*/

		if(!ReadElementAttribute(pTimeConfig, "VisStart", "value", strValue))
			return false;
		double dVisStart = VistaAspectsConversionStuff::ConvertToDouble(strValue);

		if(!ReadElementAttribute(pTimeConfig, "VisEnd", "value", strValue))
			return false;
		double dVisEnd = VistaAspectsConversionStuff::ConvertToDouble(strValue);


		// sub-element <TimeSteps> for single explicit time steps
		const VistaXML::TiXmlElement *pTimeSteps = pTimeConfig->FirstChildElement("TimeSteps");
		if(pTimeSteps == NULL)
		{
			vstr::errp() << "[VveIOUtils::LoadXMLTimeFile] Missing <TimeSteps> element specification!"<<endl;
			return false;
		}
		// iterate all "ts" children
		VistaXML::TiXmlElement *pChild = NULL;
		pChild = const_cast< VistaXML::TiXmlElement*> (pTimeSteps->FirstChildElement("ts"));
		if(pChild == NULL)
		{
			vstr::errp() << "[VveIOUtils::LoadXMLTimeFile] Missing time steps!"<<endl;
			return false;
		}
		while(pChild != NULL)
		{
			// read child info
			VveTimeMapper::IndexInfo oInfo;
			const char *pstrT = pChild->Attribute("t");
			const char *pstrStep = pChild->Attribute("step");
			if(pstrT != NULL && pstrStep != NULL)
			{
				oInfo.m_dSimTime = VistaAspectsConversionStuff::ConvertToDouble(pstrT);
				oInfo.m_iLevel = VistaAspectsConversionStuff::ConvertToInt(pstrStep);

				vecIndexInfo.push_back(oInfo);
			}
			pChild = pChild->NextSiblingElement("ts");
		}

		pTargetTM->SetImplicitVisRange(dVisStart,dVisEnd);
		pTargetTM->SetByExplicitTimeMapping(vecIndexInfo);
	}

	return true;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadXMLTimeFile				                              */
/*                                                                            */
/*============================================================================*/
bool VveIOUtils::WriteXMLTimeFile( const std::string &strTimeFile, 
	VveTimeMapper *pSourceTM, bool bForceImplicitTimings )
{
	// create an xml document
	VistaXML::TiXmlDocument oTimeFile;

	// create a top-level element <TimeMapper>
	VistaXML::TiXmlElement oTimeMapperEl("TimeMapper");
	
	// get implicit timings
	VveTimeMapper::Timings oTimings;
	pSourceTM->GetImplicitTimeMapping(oTimings);

	// find out whether we have an implicit timing
	// (as the time mapper is not clear about that, as time mappings are
	// always stored explicitly)
	if(!bForceImplicitTimings)
	{
		// get explicit timings
		std::vector<VveTimeMapper::IndexInfo> vecMappings;
		if(pSourceTM->GetExplicitTimeMapping(vecMappings))
		{
			// we must have an unbroken series of time levels, and equal
			// distance between
			bool bIsImplicit = true;
			int iLevelStride;
			if((int)vecMappings.size() > 1)
			{
				double dFirstDiff = 
					vecMappings[1].m_dSimTime - vecMappings[0].m_dSimTime;
				iLevelStride = vecMappings[1].m_iLevel - vecMappings[0].m_iLevel;
				for(int i = 0; i < pSourceTM->GetNumberOfTimeLevels(); ++i)
				{
					if(vecMappings[i].m_iLevel != vecMappings[0].m_iLevel + i*iLevelStride)
					{
						bIsImplicit = false;
						break;
					}
					if(i > 0)
					{
						double dDiff = vecMappings[i].m_dSimTime - vecMappings[i-1].m_dSimTime;
						if(fabs(dDiff - dFirstDiff) > Vista::Epsilon)
						{
							bIsImplicit = false;
							break;
						}
					}
				}
			}

			if(bIsImplicit)
			{
				oTimings.m_iFirstLevel = vecMappings[0].m_iLevel;
				oTimings.m_iNumberOfLevels = pSourceTM->GetNumberOfTimeLevels();
				oTimings.m_iNumberOfIndices = pSourceTM->GetNumberOfTimeIndices();
				oTimings.m_iLevelStride = iLevelStride;
				oTimings.m_dSimStart = vecMappings[0].m_dSimTime;
				oTimings.m_dSimEnd = vecMappings[pSourceTM->GetNumberOfTimeIndices()-1].m_dSimTime;
				bForceImplicitTimings = true;
			}
		}
	}

	if(bForceImplicitTimings)
	{
		// type is "implicit"
		oTimeMapperEl.SetAttribute("type","implicit");


		// write all parameters
		VistaXML::TiXmlElement oElement("NumTimeSteps");
		oElement.SetAttribute("value",oTimings.m_iNumberOfLevels);
		oTimeMapperEl.InsertEndChild(oElement);

		oElement = VistaXML::TiXmlElement("NumTimeIndices");
		oElement.SetAttribute("value",oTimings.m_iNumberOfIndices);
		oTimeMapperEl.InsertEndChild(oElement);

		oElement = VistaXML::TiXmlElement("Stride");
		oElement.SetAttribute("value",oTimings.m_iLevelStride);
		oTimeMapperEl.InsertEndChild(oElement);

		oElement = VistaXML::TiXmlElement("FirstTimestep");
		oElement.SetAttribute("value",oTimings.m_iFirstLevel);
		oTimeMapperEl.InsertEndChild(oElement);

		oElement = VistaXML::TiXmlElement("SimStart");
		oElement.SetAttribute("value",
			VistaAspectsConversionStuff::ConvertToString(oTimings.m_dSimStart));
		oTimeMapperEl.InsertEndChild(oElement);

		oElement = VistaXML::TiXmlElement("SimEnd");
		oElement.SetAttribute("value",
			VistaAspectsConversionStuff::ConvertToString(oTimings.m_dSimEnd));
		oTimeMapperEl.InsertEndChild(oElement);

		oElement = VistaXML::TiXmlElement("VisStart");
		oElement.SetAttribute("value",
			VistaAspectsConversionStuff::ConvertToString(oTimings.m_dVisStart));
		oTimeMapperEl.InsertEndChild(oElement);

		oElement = VistaXML::TiXmlElement("VisEnd");
		oElement.SetAttribute("value",
			VistaAspectsConversionStuff::ConvertToString(oTimings.m_dVisEnd));
		oTimeMapperEl.InsertEndChild(oElement);
	}
	else
	{
		// type is "explicit"
		oTimeMapperEl.SetAttribute("type","explicit");
		// get explicit timings
		std::vector<VveTimeMapper::IndexInfo> vecMappings;
		if(!pSourceTM->GetExplicitTimeMapping(vecMappings))
		{
			vstr::warnp() << "[VveIOUtils::WriteXMLTimeFile] could not extract "
				<< "explicit time mappings while trying to write XML time file!"
				<< std::endl;
			return false;
		}

		// write VisStart and VisEnd parameters
		// they are stored in the implicit timing parameters... so get them
		// from there
		VistaXML::TiXmlElement oElement("VisStart");
		oElement.SetAttribute("value",
			VistaAspectsConversionStuff::ConvertToString(oTimings.m_dVisStart));
		oTimeMapperEl.InsertEndChild(oElement);

		oElement = VistaXML::TiXmlElement("VisEnd");
		oElement.SetAttribute("value",
			VistaAspectsConversionStuff::ConvertToString(oTimings.m_dVisEnd));
		oTimeMapperEl.InsertEndChild(oElement);

		// write timesteps
		VistaXML::TiXmlElement oTimeStepsEl("TimeSteps");

		for(int i = 0; i < (int)vecMappings.size(); ++i)
		{
			// create time step element
			oElement = VistaXML::TiXmlElement("ts");
			oElement.SetAttribute("t",
				VistaAspectsConversionStuff::ConvertToString(vecMappings[i].m_dSimTime));
			oElement.SetAttribute("step", vecMappings[i].m_iLevel);
			oTimeStepsEl.InsertEndChild(oElement);
		}

		oTimeMapperEl.InsertEndChild(oTimeStepsEl);

	}


	// write file
	oTimeFile.InsertEndChild(oTimeMapperEl);

	return oTimeFile.SaveFile(strTimeFile);
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   SerializeImplicitTimeMapper	                              */
/*                                                                            */
/*============================================================================*/
int VveIOUtils::SerializeImplicitTimeMapper(VveTimeMapper *pTM,
	IVistaSerializer &oSer)
{
	VveTimeMapper::Timings oTimings;
	pTM->GetImplicitTimeMapping(oTimings);

	int iRet=0;
	int iNumBytes=0;

	//write signature
	iRet = oSer.WriteInt32((VistaType::uint32)STR_TM_SIG.length());
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	iRet = oSer.WriteString(STR_TM_SIG);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write time level range
	iRet = oSer.WriteInt32(oTimings.m_iFirstLevel);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;
	iRet = oSer.WriteInt32(oTimings.m_iNumberOfLevels);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write time index range
	iRet = oSer.WriteInt32(oTimings.m_iNumberOfIndices);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write stride
	iRet = oSer.WriteInt32(oTimings.m_iLevelStride);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write vis time range
	iRet = oSer.WriteDouble(oTimings.m_dVisStart);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;
	iRet = oSer.WriteDouble(oTimings.m_dVisEnd);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write sim time range
	iRet = oSer.WriteDouble(oTimings.m_dSimStart);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;
	iRet = oSer.WriteDouble(oTimings.m_dSimEnd);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;
	return iNumBytes;
}


/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeSerializeImplicitTimeMapper                               */
/*                                                                            */
/*============================================================================*/
int VveIOUtils::DeSerializeImplicitTimeMapper(IVistaDeSerializer &oDeSer,
	VveTimeMapper *pTM)
{
	VveTimeMapper::Timings oTimings;

	int iRet=0;
	int iNumBytes = 0;
	int iInput = 0;
	float fInput = 0;
	std::string strInSig;

	//write signature
	iRet = oDeSer.ReadInt32(iInput);
	if(iRet < 0 || iInput != STR_TM_SIG.length())
		return -1;
	iNumBytes += iRet;

	iRet = oDeSer.ReadString(strInSig, iInput);
	if(iRet < 0 || strInSig != STR_TM_SIG)
		return -1;
	iNumBytes += iRet;

	//write time level range
	iRet = oDeSer.ReadInt32(oTimings.m_iFirstLevel);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;
	iRet = oDeSer.ReadInt32(oTimings.m_iNumberOfLevels);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write time index range
	iRet = oDeSer.ReadInt32(oTimings.m_iNumberOfIndices);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write stride
	iRet = oDeSer.ReadInt32(oTimings.m_iLevelStride);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write vis time range
	iRet = oDeSer.ReadDouble(oTimings.m_dVisStart);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;
	iRet = oDeSer.ReadDouble(oTimings.m_dVisEnd);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	//write sim time range
	iRet = oDeSer.ReadDouble(oTimings.m_dSimStart);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;
	iRet = oDeSer.ReadDouble(oTimings.m_dSimEnd);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	pTM->SetByImplicitTimeMapping(oTimings);
	return iNumBytes;
}



/*============================================================================*/
/*                                                                            */
/*  NAME      :   SerializeExplicitTimeMapper	                              */
/*                                                                            */
/*============================================================================*/
int VveIOUtils::SerializeExplicitTimeMapper(VveTimeMapper *pTM,
											IVistaSerializer &oSer)
{
	std::vector<VveTimeMapper::IndexInfo> vecTimings;
	if(!pTM->GetExplicitTimeMapping(vecTimings) || vecTimings.empty())
		return -1;

	int iNumBytes=0;
	int iRet = 0;

	//write signature
	iRet = oSer.WriteInt32(static_cast<VistaType::sint32>(STR_TM_SIG.length()));
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	iRet = oSer.WriteString(STR_TM_SIG);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	// write "explicit" tag
	const std::string expTag("_explicit_");
	iRet = oSer.WriteInt32(static_cast<VistaType::sint32>(expTag.length()));
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	iRet = oSer.WriteString(expTag);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	// write number of entries
	iRet = oSer.WriteInt32(static_cast<VistaType::sint32>(vecTimings.size()));
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	// write entries
	for(size_t i=0; i<vecTimings.size(); ++i)
	{
		const VveTimeMapper::IndexInfo &ii = vecTimings[i];
		iRet = oSer.WriteInt32(ii.m_iLevel);
		if(iRet < 0)
			return -1;
		iNumBytes += iRet;
		iRet = oSer.WriteDouble(ii.m_dSimTime);
		if(iRet < 0)
			return -1;
		iNumBytes += iRet;
	}

	// that's it!

	return iNumBytes;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   DeSerializeExplicitTimeMapper	                              */
/*                                                                            */
/*============================================================================*/
int VveIOUtils::DeSerializeExplicitTimeMapper( IVistaDeSerializer &oDeSer, VveTimeMapper *pTM )
{

	std::vector<VveTimeMapper::IndexInfo> vecTimings;
	//if(!pTM->GetExplicitTimeMapping(vecTimings) || vecTimings.empty())
	//	return -1;

	int iNumBytes=0;
	int iRet = 0;
	VistaType::sint32 sz = 0;
	std::string str;

	// check signature
	iRet = oDeSer.ReadInt32(sz);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	iRet = oDeSer.ReadString(str, sz);
	if(iRet < 0 || str != STR_TM_SIG)
		return -1;
	iNumBytes += iRet;

	// check "explicit" tag
	iRet = oDeSer.ReadInt32(sz);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	iRet = oDeSer.ReadString(str, sz);
	if(iRet < 0 || str != std::string("_explicit_"))
		return -1;
	iNumBytes += iRet;


	// get number of entries
	iRet = oDeSer.ReadInt32(sz);
	if(iRet < 0)
		return -1;
	iNumBytes += iRet;

	vecTimings.resize(sz);
	for(int i = 0; i < sz; ++i)
	{
		VveTimeMapper::IndexInfo &ii = vecTimings[i];
		iRet = oDeSer.ReadInt32(ii.m_iLevel);
		if(iRet < 0)
			return -1;
		iNumBytes += iRet;
		iRet = oDeSer.ReadDouble(ii.m_dSimTime);
		if(iRet < 0)
			return -1;
		iNumBytes += iRet;
	}

	// that's it!
	pTM->SetByExplicitTimeMapping(vecTimings);
	return iNumBytes;
}

/*============================================================================*/
/*  LOKAL VARS / FUNCTIONS                                                    */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   ComputeTextureDimensions                                    */
/*                                                                            */
/*============================================================================*/
static inline int ComputeTextureDimensions(int iCount, int &iWidth, int &iHeight)
{
	if (iCount < 0)
		return 0;

	iWidth = int(ceil(sqrt(float(iCount))));
	iHeight = int(floor(sqrt(float(iCount))));
	while (iWidth * iHeight < iCount)
	{
		++iHeight;
		if (iWidth * iHeight < iCount)
			++iWidth;
	}
	return iWidth * iHeight;
}




static inline  bool ReadElementAttribute(const VistaXML::TiXmlElement *pParentElement,
										 const std::string &strChildElementName, 
										 const std::string &strAttName,
										 std::string &strOutAttVal)
{
	const VistaXML::TiXmlElement *pParam = pParentElement->FirstChildElement(strChildElementName);
	if(pParam == NULL)
	{
		vstr::errp()<<"[VveIOUtils::ReadElementAttribute] Missing <"<<strChildElementName.c_str()
			<<"> element specification!"<<endl;
		return false;
	}
	const std::string *pAttVal = pParam->Attribute(strAttName);
	if(pAttVal == NULL)
	{
		vstr::errp()<<"[VveIOUtils::ReadElementAttribute] Attribute <"<<strAttName.c_str()
			<<"> not found in element <"<<strChildElementName.c_str()<<">!"<<endl;
		return false;
	}
	strOutAttVal = *pAttVal;
	return true;
}

/*============================================================================*/
/*  END OF FILE "VveIOUtils.cpp"                                              */
/*============================================================================*/
