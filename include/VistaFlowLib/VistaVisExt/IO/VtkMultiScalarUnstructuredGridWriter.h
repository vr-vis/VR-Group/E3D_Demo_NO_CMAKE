/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                             .                              */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VtkMultiScalarUSGridWriterH  RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  VistaVisExt                  RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                             .                              */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - CMultiScalarUnstructuredGridWriter  :   ...                       */
/*                                                                            */
/*============================================================================*/

#ifndef  _VTKMULTISCALARUNSTRUCTGRIDWRITER_H
#define  _VTKMULTISCALARUNSTRUCTGRIDWRITER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vtkUnstructuredGridWriter.h>

#include <string>
#include <map>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class vtkDataArray;
class vtkDataSet;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VtkMultiScalarUnstructuredGridWriter
 * 
 * @author  Andreas Gerndt
 * @date    29.09.2002
 */    

class VISTAVISEXTAPI VtkMultiScalarUnstructuredGridWriter : public vtkUnstructuredGridWriter  
{
protected:
	VtkMultiScalarUnstructuredGridWriter();
	virtual ~VtkMultiScalarUnstructuredGridWriter();

public:
#if (VTK_MAJOR_VERSION == 4) && (VTK_MINOR_VERSION < 2)
    vtkTypeMacro(VtkMultiScalarUnstructuredGridWriter,vtkUnstructuredGridWriter);
#else
    vtkTypeRevisionMacro(VtkMultiScalarUnstructuredGridWriter,vtkUnstructuredGridWriter);
#endif
	static VtkMultiScalarUnstructuredGridWriter * New();
    void PrintSelf(ostream& os, vtkIndent indent);

    void WriteData();
	bool SetMultiScalars( vtkDataArray * pScalars, std::string strName );

protected:
    int WritePointData(ostream *fp, vtkDataSet *ds);

	//typedef std::map  <std::string, vtkDataArray *> ListOfScalarsAndName;
    //typedef std::pair <std::string, vtkDataArray *> NameScalarsPair;

    virtual void RemoveInput  (  vtkDataObject *    input  );//  [protected, virtual] 


private:
    VtkMultiScalarUnstructuredGridWriter(
        const VtkMultiScalarUnstructuredGridWriter&);  // Not implemented.
    void operator=(const VtkMultiScalarUnstructuredGridWriter&);  // Not implemented.

protected:
	std::map  <std::string, vtkDataArray *>  m_mapScalarsAndName;
};

#endif // !defined(_VTKMULTISCALARUNSTRUCTGRIDWRITER_H)

