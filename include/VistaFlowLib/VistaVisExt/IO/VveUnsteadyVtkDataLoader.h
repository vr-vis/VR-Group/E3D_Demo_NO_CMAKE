/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEUNSTEADYVTKDATALOADER_H
#define _VVEUNSTEADYVTKDATALOADER_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveUnsteadyDataLoader.h"
#include <VistaAspects/VistaReflectionable.h>

#include <VistaVisExt/VistaVisExtConfig.h>

#include <string>
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VflUnsteadyVtkDataLoader loads precalculated vtk data from disk.
 * The data sets have to be given 
 */    
class VISTAVISEXTAPI VveUnsteadyVtkDataLoader : public VveUnsteadyDataLoader
{
public:
	//define shortcuts for the output data format
	enum EDataFormat{
		DF_INVALID = -1,
		DF_VTK_DATASET = 0,
		DF_VTK_STRUCTUREDGRID,
		DF_VTK_STRUCTUREDPOINTS,
		DF_VTK_UNSTRUCTUREDGRID,
		DF_VTK_POLYDATA,
		DF_GRIDPRO_POLYDATA,
		DF_VTP_POLYDATA
	};
	
	// CONSTRUCTORS / DESTRUCTOR
    VveUnsteadyVtkDataLoader(VveUnsteadyData *pTarget);
    virtual ~VveUnsteadyVtkDataLoader();

    /**
     * Initialize the loader.
     * Basically, this method just does a sanity check of the 
     * given parameters. The property list gets finally parsed
     * and used when data loading is being invoked.
	 */
    virtual bool Init(const VistaPropertyList &refProps);

	

    virtual void LoadDataThreadSafe();
	/*
	 * Save loader properties separately and provide a standard interface. Due to 
	 * IVistaReflectionable all members can be accessed via the property keys 
	 * listed below:
	 *
	 * FILE_NAME	     	file name pattern (Vve-style) of file(s) to read
	 *
	 * FORMAT		     	format of the file to read (defines output format as well) 
	 *                      [ VTK_POLYDATA, VTK_STRUCTUREDPOINTS, VTK_STRUCTUREDGRID, 
	 *                        VTK_UNSTRUCTUREDGRID, VTK_DATASET, GRIDPRO_POLYDATA ]
	 *
	 * TIME_FILE			time file name to use for loading
	 *
	 * SCALARS_NAME	     	scalar field to be loaded upon reader execution
	 *						works ONLY WITH multi-scalar data using the
	 *						"VtkMultiScalarUnstructuredGridWriter-method" i.e.
	 *						several scalar fields with type flag "SCALARS" in one vtk data file
	 *
	 * VECTORS_NAME	     	same as above for vector field data
	 *
	 * ACTIVE_SCALARS      	scalar field to be selected AFTER reader execution
	 *						works with vtk datasets with multiple associated field
	 *						data arrays (generated via vtkPointData::AddArray(...))
	 * 
	 * ACTIVE_VECTORS      	same as above for vectors
     *
	 * PROGRESS_BAR			show progress bar during load operaion
	 *
	 * TRIANGULATE	     	triangulate data upon read (in case of GRIDPRO geometry input only)
	 */
	class VISTAVISEXTAPI CProperties : public IVistaReflectionable
	{
	public:
		CProperties();
		CProperties(const CProperties& source);
		virtual ~CProperties();

		CProperties& operator=(const CProperties& source);
		
		/**
		* NOTE : each setter will return true iff the value has changed!
		*/
		bool SetFilePattern(const std::string& str);
		std::string GetFilePattern() const;
		
		bool SetFileFormat(const std::string& str);
		bool SetFileFormat(const EDataFormat iF);
		std::string GetFileFormat() const;
		EDataFormat GetEFileFormat() const;
		
		bool SetTimeFile(const std::string& str);
		std::string GetTimeFile() const;
		
		bool SetActiveScalars(const std::string& str);
		std::string GetActiveScalars() const;
		
		bool SetActiveVectors(const std::string& str);
		std::string GetActiveVectors() const;
		
		bool SetScalarsName(const std::string& str);
		std::string GetScalarsName() const;
		
		bool SetVectorsName(const std::string& str);
		std::string GetVectorsName() const;
		
		bool SetShowProgressBar(const bool b);
		bool GetShowProgressBar() const;

		bool SetTriangulateInput(const bool b);
		bool GetTriangulateInput() const;

		void LockProps();
		void UnlockProps();

		virtual std::string GetReflectionableType() const;
	protected:
		int AddToBaseTypeList(std::list<std::string>& rBTList) const;

		EDataFormat String2Format(const std::string& str) const;
		std::string Format2String(const EDataFormat e) const;
	private:
		std::string m_strFilePattern;
		EDataFormat m_eDataFormat;
		std::string m_strTimeFile;
		std::string m_strActiveScalars;
		std::string m_strActiveVectors;
		std::string m_strScalarsName;
		std::string m_strVectorsName;
		bool m_bShowProgressBar;
		bool m_bTriangulate;

		VistaMutex* m_pPropLock;
	};
	CProperties *GetProperties() const;
protected:
	
private:
    CProperties *m_pMyProps;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VVEUNSTEADYVTKDATALOADER_H)
