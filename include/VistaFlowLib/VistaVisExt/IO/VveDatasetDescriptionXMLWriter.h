/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEDATASETDESCRIPTIONXMLWRITER_H
#define _VVEDATASETDESCRIPTIONXMLWRITER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <string>
#include <vector>
#include <limits>
#include <VistaTools/tinyXML/tinyxml.h>
#include <VistaVisExt/VistaVisExtConfig.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

class VveDatasetDescription;
class VveEnsembleDescription;
class VveComponentDescription;
class VveMetaInfoDescription;
class VveTimeMapper;

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/* Writes a VveDatasetDescription / VveEnsembleDescription / 
 * VveComponentDescription to an XML file, to be loaded again by the
 * VveDatasetDescriptionXMLLoader.
 */
class VISTAVISEXTAPI VveDatasetDescriptionXMLWriter 
{
public:
	
	VveDatasetDescriptionXMLWriter();
	virtual ~VveDatasetDescriptionXMLWriter();

	/* Writes the given dataset description into the given XML file.
	 * All components are embedded within the dataset XML.
	 * All file names are given explicitly (<FilePattern> is not used)
	 * The time file will always be saved as "time.xml" in the same
	 * directory; time files for components (where a component has an own
	 * time mapper) are automatically created within
	 * the component directory (as long as all files of the component share
	 * the same directory, which should definitely be the case); meta info
	 * files are automatically created within the component directory.
	 * At the moment, multi-block timesteps are not supported.
	 */
	virtual bool WriteDataset(VveDatasetDescription *pDataset,
		std::string strFilename);

	/* Writes the given component description into the given XML file.
	 * Expects that the file is placed in the same directory as the component
	 * files! All file names are given explicitly (<FilePattern> is not used).
	 * If pComponent has a time mapper, a time file "time.xml" will be 
	 * automatically generated in the same directory as strFilename.
	 * Meta info files are automatically created in the same directory.
	 */
	virtual bool WriteComponent(VveComponentDescription *pComponent,
		std::string strFilename);


protected:

	//virtual VistaXML::TiXmlElement CreateXML(VveEnsembleDescription *pEnsemble);
	virtual VistaXML::TiXmlElement CreateXML(VveDatasetDescription *pDataset,
		std::string strTopDirectory = "");

	virtual VistaXML::TiXmlElement CreateXML(VveComponentDescription *pComponent,
		std::string strTopDirectory = "",
		bool bWriteTimeFile = false);
	virtual VistaXML::TiXmlElement CreateXML(VveMetaInfoDescription *pMetaInfo);
	
	virtual std::string GetCommonPrefix(std::vector<std::string> vecStrings);
	virtual int GetCommonPrefixLength(std::string str1, std::string str2,
		int iHintMax = std::numeric_limits<int>::max());

	virtual std::string GetDirectoryFromFilename(std::string strFilename);

	virtual void ReplaceBackslashes(std::string &strPath);
};

#endif 


/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/
