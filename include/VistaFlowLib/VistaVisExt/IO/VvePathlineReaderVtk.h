/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#ifndef _VVEPATHLINEREADERVTK_H
#define _VVEPATHLINEREADERVTK_H

/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <string>
#include <list>
#include <VistaVisExt/VistaVisExtConfig.h>
#include <VistaVisExt/Data/VveParticlePopulation.h>

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
/**
 * VvePathlineReaderVtk loads the contents of a given trk file (in ascii or binary
 * format) into a given particle population object.
 */    
class VISTAVISEXTAPI VvePathlineReaderVtk
{
public:
    // CONSTRUCTORS / DESTRUCTOR
    /*
     * As there is no timing information stored in the vtk data file (yet),
     * the pathlines are assumed to start at time fStartTime with 
     * particle positions given at a timely distance of fDeltaTime.
     */
    VvePathlineReaderVtk(const std::string &strFilename, 
		                  float fStartTime, 
						  float fDeltaTime);

	/*
	 * Timing information is stored in the vtk data file as a scalar field named
	 * strTimeFieldName.
	 */
	VvePathlineReaderVtk(const std::string & strFilename, std::string strTimeFieldName);

    virtual ~VvePathlineReaderVtk();

    /**
     * Load the particle data into the given population. 
     * The parameters strScalar and strRadius determine, which scalar value 
     * is to be loaded into the scalar and radius entries of the particles.
	 *
	 * This methods first checks whether timing information is stored in the vtk data
	 * (strTimeFieldName must be given and a scalar field with that name must exist).
	 * If strTimeFieldName is given but no appropriate scalar field can be found, there will be
	 * an error message.
	 * If no strTimeFieldName is given, than the method will compute the timing information from
	 * given m_fStartTime and m_fDeltaTime (and assumes that we have constant time distances between
	 * the successive trace points).
     */
    bool Load(VveParticlePopulation *pPopulation, 
		      const std::string &strScalar, 
              const std::string &strRadius,
              const std::string &strTsField = "");

	/**
	 * Set the required information to load an extended particle data.
	 * VveParticleExt is the superclass for all kind of particle extensions, the VistaPropertyList holds the
	 * information about the extention needed in the loading process.
	 *
	 * Current existing extension:
	 * VveParticleExtTensor 
	 *      - list<string> PARTICLEEXT_FIELDNAMES : eigenvalue 0, eigenvalue 1, eigenvalue 2,
	 *												eigenvector 0, eigenvector 1, eigenvector 2
 	 */
	void SetParticleExtention( const VistaPropertyList &refPropsParticleExt );

         /** 
     * Setters for the names of the particle data arrays
     */    
    bool SetPositionArray(std::string sPositionArray);
    bool SetVelocityArray(std::string sVelocityArray);
    bool SetRadiusArray(std::string sRadiusArray);
    bool SetScalarArray(std::string sScalarArray);
    bool SetTimeArray(std::string sTimeArray);
    bool SetEigenValueArray(std::string sEigenValueArray);
    bool SetEigenVectorArray(std::string sEigenVectorArray);

protected:

    // temporary particle data structure
    struct STemporaryParticleData
    {
        int        iIndex;
        float    aPos[3];
        float    aVel[3];
        float    fScalar;
        float    fRadius;
        float   fTime;
    };

    float m_fStartTime, m_fDeltaTime;

    std::string m_strFilename;
	std::string m_strTimeFieldName;


	// remember the kind of particle extention and its properties
    bool m_bUseExtensions;
	VistaPropertyList         m_oPropsParticleExt;

    // Particle data array names ( + default in c'tor)
    std::string m_sPositionArray; /* = "positions" */
    std::string m_sVelocityArray; /* = "velocities" */
    std::string m_sScalarArray;   /* = "scalars" */
    std::string m_sRadiusArray;   /* = "radius" */
    std::string m_sTimeArray;     /* = "sim_time" */
    std::string m_sEigenValueArray;  /* = "eigenvalues" */
    std::string m_sEigenVectorArray; /* = "eigenvectors" */

};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
#endif // !defined(_VFLPATHLINEREADERVTK_H)
