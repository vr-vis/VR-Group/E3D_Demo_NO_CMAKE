/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/


/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "VveDatasetDescriptionXMLWriter.h"
#include <VistaVisExt/Data/DatasetDescription/VveDatasetDescription.h>
#include <VistaVisExt/Data/DatasetDescription/VveComponentDescription.h>
#include <VistaVisExt/Data/DatasetDescription/VveMetaInfoDescription.h>
#include <VistaVisExt/Data/DatasetDescription/VveTimestepDescription.h>
#include <VistaVisExt/Data/DatasetDescription/VveBlockDescription.h>
#include <VistaVisExt/Data/DatasetDescription/VveEnsembleDescription.h>


#include <VistaTools/tinyXML/tinyxml.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>
#include <VistaVisExt/IO/VveIOUtils.h>
#include <VistaAspects/VistaPropertyList.h>
#include <VistaVisExt/IO/VveDataSetName.h>


/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
VveDatasetDescriptionXMLWriter::VveDatasetDescriptionXMLWriter()
{
}

VveDatasetDescriptionXMLWriter::~VveDatasetDescriptionXMLWriter()
{
}


/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/


VistaXML::TiXmlElement VveDatasetDescriptionXMLWriter::CreateXML( 
	VveComponentDescription *pComponent, 
	std::string strTopDirectory,
	bool bWriteTimeFile)
{
	VistaXML::TiXmlElement oComponentEl("Component");

	// set name
	oComponentEl.SetAttribute("name",pComponent->GetNameForNameable());

	// set type
	VveComponentDescription::DataComponentType eType = pComponent->GetType();
	std::string strType = "";
	switch(eType)
	{
		case VveComponentDescription::DCT_POLYDATA:
			strType = "POLYDATA";
			break;
		case VveComponentDescription::DCT_RECTILINEAR_GRID:
			strType = "RECTILINEAR_GRID";
			break;
		case VveComponentDescription::DCT_STRUCTURED_GRID:
			strType = "STRUCTURED_GRID";
			break;
		case VveComponentDescription::DCT_STRUCTURED_POINTS:
			strType = "STRUCTURED_POINTS";
			break;
		case VveComponentDescription::DCT_UNSTRUCTURED_GRID:
			strType = "UNSTRUCTURED_GRID";
			break;
	}
	VistaXML::TiXmlElement oTypeEl("Type");
	VistaXML::TiXmlText oText(strType);
	oTypeEl.InsertEndChild(oText);
	oComponentEl.InsertEndChild(oTypeEl);

	// set description
	std::string strDescription = pComponent->GetDescription();
	if(strDescription != "")
	{
		VistaXML::TiXmlElement oDescEl("Description");
		oText = VistaXML::TiXmlText(strDescription);
		oDescEl.InsertEndChild(oText);
		oComponentEl.InsertEndChild(oDescEl);
	}

	

	// set scalars and vectors
	std::string strScalars = pComponent->GetScalars();
	if(strScalars != "")
	{
		VistaXML::TiXmlElement oScalarsEl("Scalars");
		oText = VistaXML::TiXmlText(strScalars);
		oScalarsEl.InsertEndChild(oText);
		oComponentEl.InsertEndChild(oScalarsEl);
	}
	std::string strVectors = pComponent->GetVectors();
	if(strVectors != "")
	{
		VistaXML::TiXmlElement oVectorsEl("Vectors");
		oText = VistaXML::TiXmlText(strVectors);
		oVectorsEl.InsertEndChild(oText);
		oComponentEl.InsertEndChild(oVectorsEl);
	}

	// set metainfo
	if(pComponent->GetMetaInfo())
	{
		oComponentEl.InsertEndChild(CreateXML(pComponent->GetMetaInfo()));
	}


	// now: files.
	VistaXML::TiXmlElement oFilesEl("Files");
	// files are always stored in explicit format to avoid having to detect
	// patterns in the filenames.
	std::list<int> lisTimelevels = pComponent->GetListOfTimelevels();
	std::list<int>::iterator it;

	// first: scan all files and note all paths that we see to determine
	// the component directory (if there is such)
	std::string strAbsoluteComponentDirectory;
	std::vector<std::string> vecFilenames;
	for(it = lisTimelevels.begin(); it != lisTimelevels.end(); it++)
	{
		VveTimestepDescription *pTimestep = pComponent->GetTimelevel(*it);
		for(int i = 0; i < pTimestep->GetNumberOfBlocks(); ++i)
		{
			vecFilenames.push_back(pTimestep->GetBlock(i)->GetFilename());
		}
	}
	// get component directory
	strAbsoluteComponentDirectory = GetDirectoryFromFilename(
		GetCommonPrefix(vecFilenames));
	std::string strRelativeComponentDirectory;
	if(strAbsoluteComponentDirectory.substr(0,strTopDirectory.size()) ==
		strTopDirectory)
	{
		strRelativeComponentDirectory = 
			strAbsoluteComponentDirectory.substr(strTopDirectory.size());
	}
	else 
		strRelativeComponentDirectory = strAbsoluteComponentDirectory;

	if(strRelativeComponentDirectory[0] == '/')
		strRelativeComponentDirectory = strRelativeComponentDirectory.substr(1);

	for(it = lisTimelevels.begin(); it != lisTimelevels.end(); it++)
	{
		VistaXML::TiXmlElement oFileEl("File");
		oFileEl.SetAttribute("timestep", *it);
		VveTimestepDescription *pTimestep = pComponent->GetTimelevel(*it);

		// has blocks?
		if(pTimestep->GetNumberOfBlocks() == 1)
		{
			// no blocks, just the one file
			std::string strFilename = pTimestep->GetBlock(0)->GetFilename();
			// strip top directory
			if(strFilename.substr(0,strTopDirectory.size()) == strTopDirectory)
			{
				strFilename = strFilename.substr(strTopDirectory.size());
				if(strFilename[0] == '/') 
					strFilename = strFilename.substr(1);
			}

			VistaXML::TiXmlText oText(strFilename);
			oFileEl.InsertEndChild(oText);

			// metainfo? with one block, the meta info should be the same
			// for timestep and block, so it doesn't matter which one we take
			VveMetaInfoDescription *pMetaInfo = pTimestep->GetMetaInfo();
			if(pMetaInfo == NULL) pMetaInfo = pTimestep->GetBlock(0)->GetMetaInfo();
			if(pMetaInfo)
			{
				// write meta info to file
				VistaXML::TiXmlElement oMetaInfoEl = CreateXML(pMetaInfo);
				VistaXML::TiXmlDocument oDoc;
				oDoc.InsertEndChild(oMetaInfoEl);
				std::string strMetaInfoFilename = strRelativeComponentDirectory 
					+ '/' + "metainfo" + VistaAspectsConversionStuff::ConvertToString(*it) 
					+ ".xml";
				oDoc.SaveFile(strTopDirectory + '/' + strMetaInfoFilename);
				// reference meta info file
				oFileEl.SetAttribute("metainfo",strMetaInfoFilename);
			}


			oFilesEl.InsertEndChild(oFileEl);
		}
		else if(pTimestep->GetNumberOfBlocks() > 1)
		{
			// multiple blocks not supported at the moment
			vstr::errp() << " [VveDatasetDescriptionXMLWriter] Error while writing "
				<< "XML file: Timestep " << (*it) << " of component " 
				<< pComponent->GetNameForNameable() << " seems to consist of "
				<< "multiple blocks. Multiple block writing is not supported "
				<< "at the moment!" << std::endl;
		}
		else
		{
			// no blocks? error.
			vstr::errp() << " [VveDatasetDescriptionXMLWriter] Error while writing "
				<< "XML file: Timestep " << (*it) << " of component " 
				<< pComponent->GetNameForNameable() << " seems to have no "
				<< "blocks assigned." << std::endl;
		}
		
	}

	// insert files tag
	oComponentEl.InsertEndChild(oFilesEl);


	// time file?
	if(bWriteTimeFile)
	{
		if(pComponent->GetTimeMapper())
		{
			std::string strTimeFile = strTopDirectory + '/' + 
				strRelativeComponentDirectory + '/' + "time.xml";
			if(!VveIOUtils::WriteXMLTimeFile(strTimeFile, pComponent->GetTimeMapper()))
			{
				vstr::errp() << " [VveDatasetDescriptionXMLWriter] Could not "
					<< "write time file " << strTimeFile << " when trying "
					<< "to write component " << pComponent->GetNameForNameable()
					<< "!" << std::endl;
			}
			else
			{
				VistaXML::TiXmlElement oTimefileEl("Timefile");
				VistaXML::TiXmlText oText(strRelativeComponentDirectory + '/' + "time.xml");
				oTimefileEl.InsertEndChild(oText);
				oComponentEl.InsertEndChild(oTimefileEl);
			}
		}

	}


	return oComponentEl;
}

VistaXML::TiXmlElement VveDatasetDescriptionXMLWriter::CreateXML( 
	VveMetaInfoDescription *pMetaInfo )
{
	VistaXML::TiXmlElement oMetaInfoEl("MetaInfo");

	VistaPropertyList *pPropertyList = pMetaInfo->GetPropertyList();
	VistaPropertyList::iterator it;
	for(it = pPropertyList->begin(); it != pPropertyList->end(); it++)
	{
		VistaXML::TiXmlElement oElement(it->first);
		VistaXML::TiXmlText oText(it->second.GetValue());
		oElement.InsertEndChild(oText);
		oMetaInfoEl.InsertEndChild(oElement);
	}

	return oMetaInfoEl;
}


VistaXML::TiXmlElement VveDatasetDescriptionXMLWriter::CreateXML(
	VveDatasetDescription *pDataset,
	std::string strTopDirectory)
{
	VistaXML::TiXmlElement oDatasetEl("Dataset");

	// time mapper?
	if(pDataset->GetTimeMapper())
	{
		std::string strTimeFile = strTopDirectory + '/' + "time.xml";
		if(!VveIOUtils::WriteXMLTimeFile(strTimeFile, pDataset->GetTimeMapper()))
		{
			vstr::errp() << " [VveDatasetDescriptionXMLWriter] Could not "
				<< "write time file " << strTimeFile << " when trying "
				<< "to write dataset! " << std::endl;
		}
		else
		{
			VistaXML::TiXmlElement oTimefileEl("Timefile");
			VistaXML::TiXmlText oText("time.xml");
			oTimefileEl.InsertEndChild(oText);
			oDatasetEl.InsertEndChild(oTimefileEl);
		}
	}

	// set description
	std::string strDescription = pDataset->GetDescription();
	if(strDescription != "")
	{
		VistaXML::TiXmlElement oDescEl("Description");
		VistaXML::TiXmlText oText(strDescription);
		oDescEl.InsertEndChild(oText);
		oDatasetEl.InsertEndChild(oDescEl);
	}

	// set metainfo
	if(pDataset->GetMetaInfo())
	{
		oDatasetEl.InsertEndChild(CreateXML(pDataset->GetMetaInfo()));
	}

	// list all components
	std::list<std::string> lisComponents = pDataset->GetListOfComponentNames();
	std::list<std::string>::iterator it;
	for(it = lisComponents.begin(); it != lisComponents.end(); it++)
	{
		VveComponentDescription *pComponent = pDataset->GetComponent(*it);
		bool bWriteTimeFile = (pDataset->GetTimeMapper() != pComponent->GetTimeMapper());
		VistaXML::TiXmlElement oComponentEl = CreateXML(pComponent, 
			strTopDirectory, bWriteTimeFile);

		oDatasetEl.InsertEndChild(oComponentEl);
	}

	return oDatasetEl;
}

std::string VveDatasetDescriptionXMLWriter::GetCommonPrefix( 
	std::vector<std::string> vecStrings )
{
	if(vecStrings.empty()) return "";

	int iCommonPrefixLength = std::numeric_limits<int>::max();
	for(int i = 0; i < (int)vecStrings.size(); ++i)
	{
		for(int j = i+1; j < (int)vecStrings.size(); ++j)
		{
			// find out common prefix length
			int iCommonLength = GetCommonPrefixLength(vecStrings[i],
				vecStrings[j], iCommonPrefixLength);
			if(iCommonLength < iCommonPrefixLength)
				iCommonPrefixLength = iCommonLength;

			// 0? nothing can help us now
			if(iCommonPrefixLength == 0)
				break;
		}
		// 0? nothing can help us now
		if(iCommonPrefixLength == 0)
			break;
	}

	return vecStrings[0].substr(0,iCommonPrefixLength);
}

int VveDatasetDescriptionXMLWriter::GetCommonPrefixLength( 
	std::string str1, std::string str2, int iHintMax )
{
	int iLength = (int)str1.size();
	if(str2.size() < str1.size()) iLength = (int)str2.size();
	if(iLength > iHintMax) iLength = iHintMax;

	int iCommonLength = 0;
	for(; iCommonLength < iLength; ++iCommonLength)
	{
		if(str1[iCommonLength] != str2[iCommonLength])
			break;
	}
	return iCommonLength;
}

std::string VveDatasetDescriptionXMLWriter::GetDirectoryFromFilename( 
	std::string strFilename )
{
	// find directory of the file (to evaluate relative paths)
	std::string::size_type stPos = strFilename.find_last_of('/');
	if(stPos == std::string::npos)
	{
		// no path given - directory is this one
		return "";
	}
	else
	{
		// cut string for directory (final '/' is not included)
		return strFilename.substr(0,stPos);
	}
}

bool VveDatasetDescriptionXMLWriter::WriteDataset( 
	VveDatasetDescription *pDataset, 
	std::string strFilename )
{
	VistaXML::TiXmlDocument oDoc;
	ReplaceBackslashes(strFilename);

	VistaXML::TiXmlElement oDatasetEl = CreateXML(pDataset, 
		GetDirectoryFromFilename(strFilename));
	oDoc.InsertEndChild(oDatasetEl);

	return oDoc.SaveFile(strFilename);
}

void VveDatasetDescriptionXMLWriter::ReplaceBackslashes( std::string &strPath )
{
	for(int i = 0; i < (int)strPath.size(); ++i)
	{
		if(strPath[i] == '\\')
			strPath[i] = '/';
	}
}

bool VveDatasetDescriptionXMLWriter::WriteComponent( 
	VveComponentDescription *pComponent, std::string strFilename )
{
	VistaXML::TiXmlDocument oDoc;
	ReplaceBackslashes(strFilename);

	VistaXML::TiXmlElement oComponentEl = CreateXML(pComponent, 
		GetDirectoryFromFilename(strFilename), 
		(pComponent->GetTimeMapper() != NULL));
	oDoc.InsertEndChild(oComponentEl);

	return oDoc.SaveFile(strFilename);
}

/*============================================================================*/
/* END OF FILE		                                                          */
/*============================================================================*/

