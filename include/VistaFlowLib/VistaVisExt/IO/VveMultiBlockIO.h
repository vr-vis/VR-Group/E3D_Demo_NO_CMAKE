/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VveMultiBlockIO.h				 RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  CommExtApp                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VveMultiBlockIO      :   ...                                        */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEMULTIBLOCKIO_H
#define _VVEMULTIBLOCKIO_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>
#include <string>
#include <VistaVisExt/IO/VveDataSetName.h>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VveMultiBlockTopoDef;
class vtkDataSet;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 *	This class defines the basic interface for I/O operations on instationary 
 *	multi-block data. Since we use vtk for all basic visualization functionality, this 
 *	interface yields vtkDataSets as return type. Time information is passed as simple 
 *	float for further (more sophisticated) processing.
 *	Some notes are to be considered:
 *	1)	Data ALWAYS consists of the data grid itself AND a simulation time index.
 *		As this connection is strict, time and data information are always loaded 
 *		in conjunction..
 *	2)	Data can SOMETIMES be connected to some type of meta information (e.g.topology). Since  
 *		this meta information is not generally available it has to be loaded via a separate
 *		call.
 *	3)	a)	Datasets consisting only of a single data file (i.e. single-block data
 *			can be stored as trivial multi-block data.
 *		b)	Datasets consisting only of a single time level (i.e. stationary data)
 *			will be stored as trivial instationary data with only one timestep.
 *		Since we are mainly interested in large scale unsteady data the overhead for
 *		trivial exeptions shouldn't be to high...
 *	4)	This class should be used to interface to any kind of vtkDataSet which also 
 *		includes vtkPolyData.
 *	5)	This class (and its deriavtives) try(s) to do as little bookkeeping as possible. 
 *		Thus it doesn't keep any loader obejects that might be used internally. 
 *		Theerefore the user is responsible for the data objects created during loading 
 *		operations (particularly for deleting them properly).
 *	6)	INDEXING:
 *		a)	All load an write operations are caried out with an offset of m_iMinTime which is the 
 *			minumum time level for the data at hand. Thus if one of the load routines yields a
 *			data vector, pos. [0] contains the data for time level m_iMinTime.
 *		b)	Data is processed in the interval [iMinIdx...iMaxIdx] that is iMaxIdx is INCLUDED.
 *			Th�s holds for block IDs as well as time levels.
 *
 *	USAGE:	1)	First create a concrete implementation of this subclass suited for the data (i.e. disc) format you want to access.
 *			2)	Specify which part of the data you want to access. 
 *			3)	Call any of the access functions
 *
 *	@author		Bernd Hentschel
 *	@date		February 2004 
 */
class VISTAVISEXTAPI VveMultiBlockIO
{
public:
	virtual ~VveMultiBlockIO();

	/**
	*
	*/
	void SetDataSetName(VveDataSetName* pDSName);
	/**
	*
	*/
	VveDataSetName* GetDataSetName();

	/**
	*
	*/
	void SetTopologyFileName(VveDataSetName* pTopoName);
	/**
	*
	*/
	VveDataSetName* GetTopologyFileName();
	/**
	* Set the extents of the data files implicitly (i.e. the number of blocks as well as the number of different time levels)
	* NOTE: The indices given here will be directly incorporated into file names
	*
	* @param[in]	iMinTime		minimum time level of the data set to be loaded
	* @param[in]	iNumTimeLevels	maximum time level of the data set to be loaded
	* @param[in]	iMinBlock		minimum block number of the data set to be loaded
	* @param[in]	iNumBlocks		maximum block number of the data set to be loaded
	* @param[in]	iTimeStride		Offset inbetween two time indices (e.g. if you wanted to load every tenth step only 
	*							you would indsert 10 here)
	* @param[in]	iBlockStride	Offset inbetween two block indices 
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
	void SetDataExtents(int iMinTime, int iNumTimeLevels, int iMinBlock, int iNumBlocks, int iTimeStride=1, int iBlockStride=1);
	/**
	* Set the extents of the data files explicitly (i.e. the number of blocks as well as the number of different time levels)
	* By explicitly setting the extents you have the possibility to flexibly select any particular subset of the
	* dataset at hand
	*
	* @param[in]	vecTimeLevels		vector containing the indices of the time steps under consideration
	* @param[in]	vecBlockIds			vector containing the indices of the blocks under consideration
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
	void SetDataExtents(std::vector<int> vecTimeLevels, std::vector<int> vecBlockIds);
	/**
	*	Read access to dataset extent
	*/
	int GetMinTime() const;
	int GetMinBlock() const;
	size_t GetNumTimeLevels() const;
	size_t GetNumBlocks() const;
	
	/**
	* Since vtk can handle only one scalar field at any given time we have to specify which one should be loaded
	* in case a file contains more than one of them
	*
	* @param[in]	strScalarFieldName	name of the scalar field to be loaded.
	* 
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
	void SetScalarFieldName(std::string strScalarFieldName);
	/**
	*
	*/
	std::string GetScalarFieldName() const;
	/**
	* Since vtk can handle only one scalar field at any given time we have to specify which one should be loaded
	* in case a file contains more than one of them
	*
	* @param[in]	iScalarFieldIdx		index of the scalar field to be loaded (taken from [0..n-1])
	* 
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
	void SetScalarFieldId(int iScalarFieldIdx);
	/**
	*
	*/
	int GetScalarFieldId() const;
	/**
	* Load a complete instationary multi-block data set containing time as well as data grid information.
	*
	* @param[out]	vecUnsteadyData		after a successful load operation, this will contain pointers to the various data sets
	*								the indexing order is [TIMELEVEL][BLOCKID].
	* @param[out]	vecSimTimes			vector containing the sim times for each single timelevel
	* @return	bool				<TRUE> iff load operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool LoadFullDataSet(std::vector< std::vector<vtkDataSet*> >& vecUnsteadyData, std::vector<float>& vecSimTimes);
	/**
	* Load one time step of an instationary multi-block data set containing time as well as data grid information.
	*
	* @param[in]	iTimeLevel		index of the requested time level 
	* @param[out]	vecDataSets		after a successful load operation, this will contain pointers to the various data sets (empty otherwise)
	* @param[out]	fSimeTime		simulation time of the loaded time level
	* @return	bool			<TRUE> iff load operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool LoadTimeLevel(int iTimeLevel, std::vector<vtkDataSet*>& vecDataSets, float& fSimTime);
	/**
	* Load a single block of a complete instationary multi-block data set containing time as well as data grid information
	*
	* @param[in]	iTimeLevel		index of the requested time level
	* @param[in]	iBlockNum		index of the requested block
	* @param[out]	pDataSet		pointer to the dataset loaded (NULL if unsuccessful)
	* @return	bool			<TRUE> iff load operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool LoadSingleBlock(int iTimeLevel, int iBlockNum, vtkDataSet*& pDataSet, float& fSimTime) = 0;

	/**
	* Load topology information for a complete instationary multi-block data set 
	*
	* @param[out]	vecUnsteadyTopoData	after a successful load operation, this will contain pointers to the topology data sets
	*								the indexing order is [TIMELEVEL][BLOCKID].
	* @return	bool				<TRUE> iff load operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool LoadFullTopology(std::vector< std::vector<VveMultiBlockTopoDef*> >& vecUnsteadyTopoData);
	/**
	* Load topology information for a single time step
	*
	* @param[in]	iTimeLevel		requested time level
	* @param[out]	vecTopoDataSets	after a successful load operation this will contain pointers to the topo data sets 
	* @return	bool				<TRUE> iff load operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool LoadTimeLevelTopology(int iTimeLevel, std::vector<VveMultiBlockTopoDef*>& vecTopoDataSets);
	/**
	*	NOTE: Loading topology for a single block file is not reasonable
	*/
	
	/**
	* Save a complete instationary multi-block data set containing time and data grid information to disk.
	*
	* @param[in]	vecUnsteadyData		data sets to be saved
	* @param[in]	vecSimTimes			time information to be saved
	* @return	bool				<TRUE> iff write operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool WriteFullDataSet(std::vector< std::vector<vtkDataSet*> >& vecUnsteadyData, std::vector<float>& vecSimTimes);
	/**
	* Save one timestep of a complete instationary multi-block data set containing time and data grid information to disk
	*
	* @param[in]	vecUnsteadyData		data sets to be saved
	* @param[in]	fSimTime			simulation time for time level
	* @return	bool				<TRUE> iff load operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool WriteTimeLevel(int iTimeLevel, std::vector<vtkDataSet*>& vecDataSets, float fSimTime);
	/**
	* Save a single block's data including time information and grid data
	*
	* @param[in]	iTimeLevel		time level index of data to be saved
	* @param[in]	iBlockNum		block index of data to be saved
	* @param[in]	pDataSet		dataset to be saved
	* @param[in]	fSimTime		simulation time of time level to be saved 
	* @return	bool				<TRUE> iff load operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool WriteSingleBlock(int iTimeLevel, int iBlockNum, vtkDataSet* pDataSet, float fSimTime) = 0;
	
	/**
	* Write topology information for a complete instationary multi-block data set 
	*
	* @param[in]	vecUnsteadyTopoData	data to write to disc
	*								the indexing order is [TIMELEVEL][BLOCKID].
	* @return	bool				<TRUE> iff write operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool WriteFullTopology(std::vector< std::vector<VveMultiBlockTopoDef*> >& vecUnsteadyTopoData);
	/**
	* Write topology information for a single time step
	*
	* @param[in]	iTimeLevel			time level to be written
	* @param[out]	vecTopoDataSets		topology data to be written
	* @return	bool				<TRUE> iff load operation has been successful.
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
    virtual bool WriteTimeLevelTopology(int iTimeLevel, std::vector<VveMultiBlockTopoDef*>& vecTopoDataSets);
	
protected:
	VveMultiBlockIO(); 
	/**
	* Helper method:	Deletes all vtkDataSets contained in a vector. Used to free memory in case
	*					something goes wrong during load.
	*
	* @param[in]	vecDataSets		vector containing data sets to be deleted
	*
	* @author	Bernd Hentschel
	* @date		February 2004
	*/
	void CleanUpData(std::vector<vtkDataSet*>& vecDataSets);

	/**
	* Get-Member-Method for m_pTimer.
	*
	* @param[in]	---
	*
	* @author	Marc Wolter
	* @date		February 2004
	*/
	//CCommTimer* GetTimer () { return m_pTimer; }

private:
	/*
	*	NOTE:	I've to rethink the pointers here. They are quite convenient if one wants to change parts of the filename 
	*			later on, but that's not quite what information hiding is all about :-(
	*/
	VveDataSetName*	m_pDataSetName;
	VveDataSetName*	m_pTopologyName;
	
	std::string		m_strScalarFieldName;
	int m_iScalarFieldId;

	int m_iMinTime,
		m_iMinBlock,
		m_iTimeStride,
		m_iBlockStride;
	/**
	*	vector containing the indices of all time levels to be loaded
	*/
	std::vector<int> m_vecTimeLevels;
	/**
	*	vector containing the indices of all blocks to be loaded
	*/
	std::vector<int>	 m_vecBlockIds;
};

#endif // _VVEMULTIBLOCKIO_H

/*============================================================================*/
/*  END OF FILE "MultiBlockLoaderInterface.h"                                 */
/*============================================================================*/


