/*============================================================================*/
/*       1         2         3         4         5         6         7        */
/*3456789012345678901234567890123456789012345678901234567890123456789012345678*/
/*============================================================================*/
/*                                                                            */
/*                                               RRRR WW  WW   WTTTTTTHH  HH  */
/*                                               RR RR WW WWW  W  TT  HH  HH  */
/*      Header   :  VveDataSetName.H                RRRR   WWWWWWWW  TT  HHHHHH  */
/*                                               RR RR   WWW WWW  TT  HH  HH  */
/*      Module   :  CommExtApp                   RR  R    WW  WW  TT  HH  HH  */
/*                                                                            */
/*      Project  :  ViSTA                          Rheinisch-Westfaelische    */
/*                                               Technische Hochschule Aachen */
/*      Purpose  :  ...                                                       */
/*                                                                            */
/*                                                 Copyright (c)  1998-2014   */
/*                                                 by  RWTH-Aachen, Germany   */
/*                                                 All rights reserved.       */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*    THIS SOFTWARE IS PROVIDED 'AS IS'. ANY WARRANTIES ARE DISCLAIMED. IN    */
/*    NO CASE SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DAMAGES.    */
/*    REDISTRIBUTION AND USE OF THE NON MODIFIED TOOLKIT IS PERMITTED. OWN    */
/*    DEVELOPMENTS BASED ON THIS TOOLKIT MUST BE CLEARLY DECLARED AS SUCH.    */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*      CLASS DEFINITIONS:                                                    */
/*                                                                            */
/*        - VveDataSetName          :   ...                                  */
/*                                                                            */
/*============================================================================*/


#ifndef _VVEDATASETNAME_H
#define _VVEDATASETNAME_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <string>

#include <VistaVisExt/VistaVisExtConfig.h>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * This class manages file names for unsteady multi-block
 * datasets. The main input data are time step and block number.
 * The resulting can be controlled by different parameters.
 * "${path}/${namepre}${blockpre}blocknum${timepre}timenum${namepost}.${nameext}
 * @author Andreas Gerndt
 * @version 1.0
 * @since 2004 
 */
class VISTAVISEXTAPI VveDataSetName
{
public:
    VveDataSetName();
	VveDataSetName(const std::string& strPattern);
    virtual ~VveDataSetName();

    void SetRootDir (const std::string & newDir)
        {m_strRootDirectory = newDir;}
    std::string GetRootDir () const
        {return m_strRootDirectory;}
    void SetFileNamePrefix (const std::string & newPre)
        {m_strFileNamePrefix = newPre;}
    std::string GetFileNamePrefix () const
        {return m_strFileNamePrefix;}
    void SetFileNamePostfix (const std::string & newPost)
        {m_strFileNamePostfix = newPost;}
    std::string GetFileNamePostfix () const
        {return m_strFileNamePostfix;}
    void SetFileNameExtension (const std::string & newExt)
        {m_strFileNameExtension = newExt;}
    std::string GetFileNameExtension () const
        {return m_strFileNameExtension;}
    void SetBlockPrefix (const std::string & newPre)
        {m_strBlockPrefix = newPre;}
    std::string GetBlockPrefix () const
        {return m_strBlockPrefix;}
    void SetTimePrefix (const std::string & newPre)
        {m_strTimePrefix = newPre;}
    std::string GetTimePrefix () const
        {return m_strTimePrefix;}
    void EnableBlock (bool useBlock)
        {m_bUseBlock = useBlock;}
    bool IsBlockEnabled () const
        {return m_bUseBlock;}
    void EnableTime (bool useTime)
        {m_bUseTime = useTime;}
    bool IsTimeEnabled () const
        {return m_bUseTime;}
    void SetBlockDigits (int digi)
        {m_nBlockDigits = digi;}
    size_t GetBlockDigits () const
        {return m_nBlockDigits;}
    void SetTimeDigits (int digi)
        {m_nTimeDigits = digi;}
    size_t GetTimeDigits () const
        {return m_nTimeDigits;}
	void SetTimeFirst(bool timeFirst)
		{m_bTimeFirst=timeFirst;}
	bool IsTimeFirst() const
		{ return m_bTimeFirst; }
	void SetFillTimeNumZeros(bool bFlag)
		{ m_bFillTimeNumZeros = bFlag; }
	bool GetFillTimeNumZeros()
		{return m_bFillTimeNumZeros;}
	void SetFillBlockNumZeros(bool bFlag)
		{ m_bFillBlockNumZeros = bFlag; }
	bool GetFillBlockNumZeros()
		{return m_bFillBlockNumZeros;}
	void SetOmitTimeZeroTerms(bool bFlag)
		{ m_bOmitTimeZeroTerms = bFlag; }
	bool GetOmitTimeZeroTerms()
		{return m_bOmitTimeZeroTerms;}
	void SetOmitBlockZeroTerms(bool bFlag)
		{ m_bOmitBlockZeroTerms = bFlag; }
	bool GetOmitBlockZeroTerms()
		{return m_bOmitBlockZeroTerms;}
	
	// set the filename according to a pattern.
	// '*' is used for time step digits,
	// '?' is used for block number digits.
	// Example: oName.Set("a/test/path/data*****_???.vtk")
	void Set (const std::string& strPattern);
	//get string in pattern format as described above
	std::string GetPattern () const;
	std::string GetFilePattern () const;

    std::string GetFile (int nTimeStep = 0, int nBlockNum = 0) const;
    std::string GetFileName (int nTimeStep = 0, int nBlockNum = 0) const;

protected:
    std::string     m_strRootDirectory;
    std::string     m_strFileNamePrefix;
    std::string     m_strFileNamePostfix;
    std::string     m_strFileNameExtension;
    std::string     m_strBlockPrefix;
    std::string     m_strTimePrefix;
    bool            m_bUseBlock;
    bool            m_bUseTime;
    size_t          m_nBlockDigits;
    size_t          m_nTimeDigits;
    bool            m_bTimeFirst;
	bool			m_bFillTimeNumZeros;
	bool			m_bFillBlockNumZeros;
	bool			m_bOmitTimeZeroTerms;
	bool			m_bOmitBlockZeroTerms;
};

std::ostream & operator<< (std::ostream & out, const VveDataSetName & p);


#endif // _VVEDATASETNAME_H

/*============================================================================*/
/*  END OF FILE "VveDataSetName.h"                                               */
/*============================================================================*/
