/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2014 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/



#include "VveUnsteadyTetGridLoader.h"

#include <VistaVisExt/Data/VveTetGrid.h>
#include <VistaVisExt/Data/VveUnsteadyTetGrid.h>
#include <VistaVisExt/Algorithms/VveTetGridPointLocator.h>
#include <VistaVisExt/Tools/VveTimeMapper.h>

#include <VistaAspects/VistaAspectsUtils.h>
#include <VistaTools/VistaProgressBar.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <VistaBase/VistaTimer.h>
#include <VistaBase/VistaSerializingToolset.h>

#include <VistaVisExt/IO/VveDataSetName.h>
#include <VistaVisExt/IO/VveIOUtils.h>

#include <vtkUnstructuredGridReader.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkCell.h>
#include <vtkGenericCell.h>
#include <vtkUnsignedCharArray.h>
#include <VistaBase/VistaExceptionBase.h>

/*============================================================================*/
/*  MAKROS AND DEFINES                                                        */
/*============================================================================*/
using namespace std;

/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveUnsteadyTetGridLoader::VveUnsteadyTetGridLoader(VveUnsteadyData *pTarget)
: VveUnsteadyDataLoader(pTarget),
  m_pProps(new VveUnsteadyTetGridLoader::CProperties())
{
}

VveUnsteadyTetGridLoader::~VveUnsteadyTetGridLoader()
{
	delete m_pProps;
	m_pProps = NULL;
}

/*============================================================================*/
/*  IMPLEMENTATION                                                            */
/*============================================================================*/

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Init                                                        */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::Init(const VistaPropertyList &refProps)
{
	// check whether we've got the correct kind of data...
	VveUnsteadyTetGrid *pData = dynamic_cast<VveUnsteadyTetGrid *>(m_pTarget);
	if (!pData)
	{
		vstr::errp() << " [VflUnsteadyTetGridLoader] invalid target data..." << endl;
		return false;
	}

	// skip sanity checks altogether - they could be modified via the reflectionable 
	// inteface anyway...

	// still retrieve information about blocking load...
	if (refProps.HasProperty("BLOCKING"))
		SetLoadBlocking(refProps.GetValue<bool>("BLOCKING"));

	m_pProps->Lock();
	m_pProps->SetPropertiesByList(refProps);
	m_pProps->Unlock();

	return true;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadDataThreadSafe                                          */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyTetGridLoader::LoadDataThreadSafe()
{
	SetState(LOADING);

	// check whether we've got the correct kind of data...
	VveUnsteadyTetGrid *pData = dynamic_cast<VveUnsteadyTetGrid*>(m_pTarget);

	if (!pData)
	{
		vstr::errp() << " [VflUnsteadyTetGridLoader] invalid target data..." << endl;
        SetState(FINISHED_FAILURE);
		return;
	}

	vstr::outi() << " [VflUnsteadyTetGridLoader] - starting data load..." << endl;

	// retrieve data loading properties
	m_pProps->Lock();
	string strFilePattern(m_pProps->GetFilePattern());
	string strFileFormat(m_pProps->GetFileFormat());
	m_pProps->Unlock();

	if (strFilePattern.empty())
	{
		vstr::errp() << " [VflUnsteadyTetGridLoader] no file name given..." << endl;
		SetState(FINISHED_FAILURE);
		return;
	}

	if (strFileFormat.empty())
	{
		// determine file format by file name extension
		string strExtension;
		size_t iExtensionStart = strFilePattern.find_last_of('.');
		if (iExtensionStart == string::npos 
			|| iExtensionStart == strFilePattern.size()-1)
		{
			vstr::errp() << " [VflUnsteadyTetGridLoader] unable to determine data file extension..." << endl;
			vstr::erri() << "                                            file name: " << strFilePattern << endl;
			SetState(FINISHED_FAILURE);
			return;
		}
		else
		{
			++iExtensionStart;
			strExtension = strFilePattern.substr(iExtensionStart);
			strExtension = VistaAspectsConversionStuff::ConvertToUpper(strExtension);
		}

		if (strExtension == "VTK")
			strFileFormat = "VTK_UNSTRUCTUREDGRID";
		// to be extended...
	}
	else
	{
		strFileFormat = VistaAspectsConversionStuff::ConvertToUpper(strFileFormat);
	}

	// @todo This does not get called for old data, as their format ist set
	//		 to "VTK". Check how to fix this!!!
	if (strFileFormat == "VTK_UNSTRUCTUREDGRID")
	{
		SetState(LoadVtkFiles(strFilePattern));
	}
	else
	{
		vstr::errp() << " [VflUnsteadyTetGridLoader] unknown file format '"
			<< strFileFormat << "'..." << endl;
		SetState(FINISHED_FAILURE);
	}

	if (GetState() == FINISHED_SUCCESS)
	{
		vstr::outi() << " [VflUnsteadyTetGridLoader] - grid data loaded successfully..." << endl;

		// signal successful data load
		m_pTarget->Notify();
	}
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   LoadVtkFiles                                                */
/*                                                                            */
/*============================================================================*/
int VveUnsteadyTetGridLoader::LoadVtkFiles(const std::string &strFilePattern)
{
	vstr::outi() << " [VflUnsteadyTetGridLoader] - loading VTK data..." << endl;

	VveUnsteadyTetGrid *pData = dynamic_cast<VveUnsteadyTetGrid*>(m_pTarget);

	m_pProps->Lock();
	string strTimeFile(m_pProps->GetTimeFile());
	string strActiveScalars(m_pProps->GetActiveScalars());
	string strActiveVectors(m_pProps->GetActiveVectors());
	string strScalarsName(m_pProps->GetScalarsName());
	string strVectorsName(m_pProps->GetVectorsName());
	bool bProgressBar(m_pProps->GetProgressBar());
	bool bPadding(m_pProps->GetPadding());
	bool bShareTopology(m_pProps->GetShareTopology());
	string strOutputDir(m_pProps->GetOutputDir());
	m_pProps->Unlock();

	// determine time mapper(s)
	VveTimeMapper *pLoadMapper = pData->GetTimeMapper();
	if (pLoadMapper==NULL)
	{
		VISTA_THROW("No TimeMapper given",-56)
	}

	VveDataSetName oFileName;
	oFileName.Set(strFilePattern);

	int iLevel=0, iIndex;
	int iLevelCount = pLoadMapper->GetNumberOfTimeLevels();

	VistaProgressBar oBar(iLevelCount);
	if (bProgressBar)
	{
		oBar.SetPrefixString("    ");
		oBar.SetBarTicks(30);
		oBar.Start();
	}
	else
	{
		oBar.SetPrefixString("    finished loading - ");
		oBar.SetDisplayBar(false);
		oBar.SetSilent(true);
		oBar.Start();
	}
	string strFileName;
	int iWarnings = 0;

	VveTetGrid *pTargetData = NULL;
	VveTetGrid *pTempData = new VveTetGrid;
	VveTetGridPointLocator *pLocator = new VveTetGridPointLocator;
	pTempData->SetPointLocator(pLocator);
	bool bSkipTopology = false;

	for (int i=0; i<iLevelCount; ++i)
	{
		if (GetAbort())
		{
			iWarnings = 1;
			break;
		}

		// find time level to be loaded
		iLevel = pLoadMapper->GetTimeLevelForLevelIndex(i);
		strFileName = oFileName.GetFileName(iLevel);

		// find time level to be written to
		iIndex = pLoadMapper->GetLevelIndexForTimeLevel(iLevel);
		if (iIndex < 0)
		{
			vstr::warnp() << " [VflUnsteadyTetGridLoader] unable to find target data..." << endl;
			vstr::warni() << "                                        time level: " << iLevel << endl;
			++iWarnings;
			continue;
		}

		if (!VveIOUtils::LoadTetGridVtkFile(pTempData, strFileName, 
			strOutputDir.empty() ? "" : strOutputDir + oFileName.GetFile(iLevel), 
			bPadding, strActiveScalars, strActiveVectors, strScalarsName, 
			strVectorsName, bSkipTopology))
		{
			++iWarnings;
			continue;
		}

		// retrieve data target
		pData->GetTypedLevelDataByLevelIndex(iIndex)->LockData();
		pTargetData = pData->GetTypedLevelDataByLevelIndex(iIndex)->GetData();

		if (!pTargetData)
		{
			vstr::warnp() << " [VflUnsteadyTetGridLoader] unable to find data target..." << endl;
			++iWarnings;

			pData->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();
			delete pTempData;
			pTempData = new VveTetGrid;
			continue;
		}

		if (bShareTopology)
		{
			VveTetGrid *pTopologyGrid = pData->GetTopologyGrid()->GetData();

			// get rid of old data
			pTargetData->Reset();
			pTargetData->SetTopologyGrid(pTopologyGrid);

			if (i==0)	// first step, i.e. we now have our topology
			{
				// reset topology grid, as well...
				pData->GetTopologyGrid()->LockData();
				pTopologyGrid->Reset();

				// move information from the temporary data into the data target
				// as a side effect, the corresponding values are set for
				// the shared topology grid where applicable
				pTargetData->SetVertexCount(pTempData->GetVertexCount());
				pTargetData->SetPaddedVertexCount(pTempData->GetPaddedVertexCount());
				pTargetData->SetVertices(pTempData->GetVertices());
				pTargetData->SetPointData(pTempData->GetPointData());
				pTargetData->SetCellCount(pTempData->GetCellCount());
				pTargetData->SetPaddedCellCount(pTempData->GetPaddedCellCount());
				pTargetData->SetCells(pTempData->GetCells());
				pTargetData->SetCellNeighbors(pTempData->GetCellNeighbors());
				pTargetData->SetComponents(pTempData->GetComponents());
				pTargetData->SetValid(pTempData->GetValid());
				pTopologyGrid->SetValid(pTempData->GetValid());

				VistaVector3D v3Min, v3Max;
				float aRange[2];
				pTempData->GetBounds(v3Min, v3Max);
				pTargetData->SetBounds(v3Min, v3Max);
				pTempData->GetScalarRange(aRange);
				pTargetData->SetScalarRange(aRange);

				VveTetGridPointLocator *pTargetLocator = pTargetData->GetPointLocator();
				if (pTargetLocator)
				{
					pTargetLocator->SetTreeRoot(pLocator->GetTreeRoot());
					pTargetLocator->SetKdTree(pLocator->GetKdTree());
					pTargetLocator->SetIncidentCells(pLocator->GetIncidentCells());
				}
				pTopologyGrid->ComputeByteCount();
				pData->GetTopologyGrid()->UnlockData();
				bSkipTopology = true;
			}
			else
			{
				// move information from the temporary data into the data target,
				// if not already contained within the topology grid;
				// destroy the rest...
				pTargetData->SetPointData(pTempData->GetPointData());

				VveTetGrid::DestroyArray(pTempData->GetVertices());
				VveTetGrid::DestroyArray(pTempData->GetCells());
				VveTetGrid::DestroyArray(pTempData->GetCellNeighbors());

				VveTetGrid::DestroyArray(pLocator->GetKdTree());
				VveTetGrid::DestroyArray(pLocator->GetIncidentCells());

				pTargetData->SetComponents(pTempData->GetComponents());
				pTargetData->SetValid(pTempData->GetValid());

				float aRange[2];
				pTempData->GetScalarRange(aRange);
				pTargetData->SetScalarRange(aRange);
			}
		}
		else
		{
			pTargetData->Reset();

			// move information from the temporary data into the data target
			pTargetData->SetVertexCount(pTempData->GetVertexCount());
			pTargetData->SetPaddedVertexCount(pTempData->GetPaddedVertexCount());
			pTargetData->SetVertices(pTempData->GetVertices());
			pTargetData->SetPointData(pTempData->GetPointData());
			pTargetData->SetCellCount(pTempData->GetCellCount());
			pTargetData->SetPaddedCellCount(pTempData->GetPaddedCellCount());
			pTargetData->SetCells(pTempData->GetCells());
			pTargetData->SetCellNeighbors(pTempData->GetCellNeighbors());
			pTargetData->SetComponents(pTempData->GetComponents());
			pTargetData->SetValid(pTempData->GetValid());

			VistaVector3D v3Min, v3Max;
			float aRange[2];
			pTempData->GetBounds(v3Min, v3Max);
			pTargetData->SetBounds(v3Min, v3Max);
			pTempData->GetScalarRange(aRange);
			pTargetData->SetScalarRange(aRange);

//			VveTetGridPointLocator *pTargetLocator = pTargetData->GetPointLocator();
			VveTetGridPointLocator *pTargetLocator = new VveTetGridPointLocator();
			pTargetData->SetPointLocator(pTargetLocator);
			if (pTargetLocator)
			{
				pTargetLocator->SetTreeRoot(pLocator->GetTreeRoot());
				pTargetLocator->SetKdTree(pLocator->GetKdTree());
				pTargetLocator->SetIncidentCells(pLocator->GetIncidentCells());
			}
		}
		pTargetData->ComputeByteCount();

		pData->GetTypedLevelDataByLevelIndex(iIndex)->Notify();
		pData->GetTypedLevelDataByLevelIndex(iIndex)->UnlockData();

		pData->ComputeBounds();

		pTempData->SetVertices(NULL);
		pTempData->SetPointData(NULL);
		pTempData->SetCells(NULL);
		pTempData->SetCellNeighbors(NULL);
		pTempData->SetValid(false);
		pLocator->SetKdTree(NULL);
		pLocator->SetIncidentCells(NULL);

		oBar.Increment();
	}
	oBar.SetSilent(false);
	oBar.Finish();

	delete pTempData;
	pTempData = NULL;
	delete pLocator;
	pLocator = NULL;

	if (iWarnings > 0)
		return FINISHED_WARNING;

	return FINISHED_SUCCESS;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetProperties                                               */
/*                                                                            */
/*============================================================================*/
VveUnsteadyTetGridLoader::CProperties *VveUnsteadyTetGridLoader::GetProperties() const
{
	return m_pProps;
}

/*============================================================================*/
/*  IMPLEMENTATION OF VflUnsteadyTetGridLoader::CProperties                  */
/*============================================================================*/

/*============================================================================*/
/*  STATICS AND FUNCTORS                                                      */
/*============================================================================*/
static std::string s_strCUnsteadyTetGridLdrPropsReflType("VflUnsteadyTetGridLoader::CProperties");

static IVistaPropertyGetFunctor *s_aUnsteadyTetGridLdrPropsGetFunctors[] =
{
	new TVistaPropertyGet<std::string, VveUnsteadyTetGridLoader::CProperties>
		("FILE_NAME", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetFilePattern),
	new TVistaPropertyGet<std::string, VveUnsteadyTetGridLoader::CProperties>
		("FORMAT", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetFileFormat),
	new TVistaPropertyGet<std::string, VveUnsteadyTetGridLoader::CProperties>
		("TIME_FILE", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetTimeFile),
	new TVistaPropertyGet<bool, VveUnsteadyTetGridLoader::CProperties>
		("PROGRESS_BAR", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetProgressBar),
	new TVistaPropertyGet<bool, VveUnsteadyTetGridLoader::CProperties>
		("PADDING", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetPadding),
	new TVistaPropertyGet<std::string, VveUnsteadyTetGridLoader::CProperties>
		("SCALARS_NAME", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetScalarsName),
	new TVistaPropertyGet<std::string, VveUnsteadyTetGridLoader::CProperties>
		("VECTORS_NAME", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetVectorsName),
	new TVistaPropertyGet<std::string, VveUnsteadyTetGridLoader::CProperties>
		("ACTIVE_SCALARS", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetActiveScalars),
	new TVistaPropertyGet<std::string, VveUnsteadyTetGridLoader::CProperties>
		("ACTIVE_VECTORS", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetActiveVectors),
	new TVistaPropertyGet<bool, VveUnsteadyTetGridLoader::CProperties>
		("SHARE_TOPOLOGY", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetShareTopology),
	new TVistaPropertyGet<std::string, VveUnsteadyTetGridLoader::CProperties>
		("OUTPUT_DIR", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::GetOutputDir),
	NULL
};

static IVistaPropertySetFunctor *s_aUnsteadyTetGridLdrPropsSetFunctors[] =
{
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyTetGridLoader::CProperties>
		("FILE_NAME", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetFilePattern),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyTetGridLoader::CProperties>
		("FORMAT", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetFileFormat),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyTetGridLoader::CProperties>
		("TIME_FILE", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetTimeFile),
	new TVistaPropertySet<bool, bool, VveUnsteadyTetGridLoader::CProperties>
		("PROGRESS_BAR", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetProgressBar),
	new TVistaPropertySet<bool, bool, VveUnsteadyTetGridLoader::CProperties>
		("PADDING", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetPadding),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyTetGridLoader::CProperties>
		("SCALARS_NAME", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetScalarsName),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyTetGridLoader::CProperties>
		("VECTORS_NAME", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetVectorsName),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyTetGridLoader::CProperties>
		("ACTIVE_SCALARS", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetActiveScalars),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyTetGridLoader::CProperties>
		("ACTIVE_VECTORS", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetActiveVectors),
	new TVistaPropertySet<bool, bool, VveUnsteadyTetGridLoader::CProperties>
		("SHARE_TOPOLOGY", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetShareTopology),
	new TVistaPropertySet<const std::string&, std::string, VveUnsteadyTetGridLoader::CProperties>
		("OUTPUT_DIR", s_strCUnsteadyTetGridLdrPropsReflType,
		&VveUnsteadyTetGridLoader::CProperties::SetOutputDir),
	NULL
};


/*============================================================================*/
/*  CONSTRUCTORS / DESTRUCTOR                                                 */
/*============================================================================*/
VveUnsteadyTetGridLoader::CProperties::CProperties()
: m_bProgressBar(true),
  m_bPadding(true),
  m_bShareTopology(false)
{
}

VveUnsteadyTetGridLoader::CProperties::~CProperties()
{
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFilePattern                                          */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetFilePattern(const std::string &str)
{
	if (m_strFilePattern == str)
		return false;
	m_strFilePattern = str;
	Notify();
	return true;
}

std::string VveUnsteadyTetGridLoader::CProperties::GetFilePattern() const
{
	return m_strFilePattern;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetFileFormat                                           */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetFileFormat(const std::string &str)
{
	if (m_strFileFormat == str)
		return false;
	m_strFileFormat = str;
	Notify();
	return true;
}

std::string VveUnsteadyTetGridLoader::CProperties::GetFileFormat() const
{
	return m_strFileFormat;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetTimeFile                                             */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetTimeFile(const std::string &str)
{
	if (m_strTimeFile == str)
		return false;
	m_strTimeFile = str;
	Notify();
	return true;
}

std::string VveUnsteadyTetGridLoader::CProperties::GetTimeFile() const
{
	return m_strTimeFile;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetProgressBar                                          */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetProgressBar(bool b)
{
	if (m_bProgressBar == b)
		return false;
	m_bProgressBar = b;
	Notify();
	return true;
}

bool VveUnsteadyTetGridLoader::CProperties::GetProgressBar() const
{
	return m_bProgressBar;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetPadding                                              */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetPadding(bool b)
{
	if (m_bPadding == b)
		return false;
	m_bPadding = b;
	Notify();
	return true;
}

bool VveUnsteadyTetGridLoader::CProperties::GetPadding() const
{
	return m_bPadding;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetActiveVectors                                        */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetActiveVectors(const std::string &str)
{
	if (m_strActiveVectors == str)
		return false;
	m_strActiveVectors = str;
	Notify();
	return true;
}

std::string VveUnsteadyTetGridLoader::CProperties::GetActiveVectors() const
{
	return m_strActiveVectors;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetActiveScalars                                        */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetActiveScalars(const std::string &str)
{
	if (m_strActiveScalars == str)
		return false;
	m_strActiveScalars = str;
	Notify();
	return true;
}

std::string VveUnsteadyTetGridLoader::CProperties::GetActiveScalars() const
{
	return m_strActiveScalars;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetScalarsName                                          */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetScalarsName(const std::string &str)
{
	if (m_strScalarsName == str)
		return false;
	m_strScalarsName = str;
	Notify();
	return true;
}

std::string VveUnsteadyTetGridLoader::CProperties::GetScalarsName() const
{
	return m_strScalarsName;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetVectorsName                                          */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetVectorsName(const std::string &str)
{
	if (m_strVectorsName == str)
		return false;
	m_strVectorsName = str;
	Notify();
	return true;
}

std::string VveUnsteadyTetGridLoader::CProperties::GetVectorsName() const
{
	return m_strVectorsName;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetShareTopology                                        */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetShareTopology(bool bShare)
{
	if (bShare == m_bShareTopology)
		return false;
	m_bShareTopology = bShare;
	Notify();
	return true;
}

bool VveUnsteadyTetGridLoader::CProperties::GetShareTopology() const
{
	return m_bShareTopology;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Set/GetOutputDir                                            */
/*                                                                            */
/*============================================================================*/
bool VveUnsteadyTetGridLoader::CProperties::SetOutputDir(const std::string &str)
{
	if (m_strOutputDir == str)
		return false;
	m_strOutputDir = str;
	Notify();
	return true;
}

std::string VveUnsteadyTetGridLoader::CProperties::GetOutputDir() const
{
	return m_strOutputDir;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Lock                                                        */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyTetGridLoader::CProperties::Lock()
{
	m_oMutex.Lock();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   Unlock                                                      */
/*                                                                            */
/*============================================================================*/
void VveUnsteadyTetGridLoader::CProperties::Unlock()
{
	m_oMutex.Unlock();
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   GetReflectionableType                                       */
/*                                                                            */
/*============================================================================*/
std::string VveUnsteadyTetGridLoader::CProperties::GetReflectionableType() const
{
	return s_strCUnsteadyTetGridLdrPropsReflType;
}

/*============================================================================*/
/*                                                                            */
/*  NAME      :   AddToBaseTypeList                                           */
/*                                                                            */
/*============================================================================*/
int VveUnsteadyTetGridLoader::CProperties::AddToBaseTypeList(std::list<std::string> &rBtList) const
{
	int i = IVistaReflectionable::AddToBaseTypeList(rBtList);
	rBtList.push_back(s_strCUnsteadyTetGridLdrPropsReflType);
	return i+1;
}


/*============================================================================*/
/* LOCAL METHODS                                                              */
/*============================================================================*/

/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/
