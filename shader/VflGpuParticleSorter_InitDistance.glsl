uniform sampler2D texUnitParticles;
uniform vec4 v4ViewPosition;	// viewer position (xyz), active particle count (w)
uniform vec2 v2WidthHeight;		// width, height

void main(void)
{
	vec2 v2XY = floor(gl_TexCoord[0].xy * v2WidthHeight);
	float fIndex = v2XY.x + v2XY.y * v2WidthHeight.x;

	// retrieve particle position
	vec3 v3Pos = texture2D(texUnitParticles, gl_TexCoord[0].xy).xyz;

	// compute squared distance
	v3Pos -= v4ViewPosition.xyz;
	float fDist = fIndex < v4ViewPosition.w ? dot(v3Pos, v3Pos) : -1.0;

	gl_FragColor = vec4(gl_TexCoord[0].xy, fDist, 1.0);
}