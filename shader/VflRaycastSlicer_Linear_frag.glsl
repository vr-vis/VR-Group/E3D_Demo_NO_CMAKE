uniform sampler3D tex_vol;  //< 3D volume 
uniform sampler1D tex_tf;  //< transfer function 
uniform vec2 tf_aRange;	//< 1/(max-min) ; -min/(max-min) for transfer function lookups

void main(void)
{
	float density = texture3D(tex_vol, gl_TexCoord[0].xyz).a;
	gl_FragColor = texture1D(tex_tf, density * tf_aRange.x + tf_aRange.y);
}