uniform sampler2D texUnitPosition;
uniform sampler2D texUnitAttrib;
uniform sampler2DRect texUnitVertices;
uniform sampler2DRect texUnitPointData;
uniform sampler2DRect texUnitCells;
uniform sampler2DRect texUnitCellNeighbors;

uniform vec2 v2TexWidth;	// texture widths for vertex and cell textures
uniform float fDeltaT;

vec3 x1, x2, x3, x4;

#define SHOW_ITERATIONS
#ifdef SHOW_ITERATIONS
int iSteps;
#endif

#if 1
vec2 ComputePointTCs(float fIndex)
{
	vec2 result;
	int iTemp = int(fIndex) / int(v2TexWidth.x);
	result.y = float(iTemp) + 0.5;
	result.x = fIndex - v2TexWidth.x * float(iTemp) + 0.5;
	return result;
}

vec2 ComputeCellTCs(float fIndex)
{
	vec2 result;
	int iTemp = int(fIndex) / int(v2TexWidth.y);
	result.y = float(iTemp) + 0.5;
	result.x = fIndex - v2TexWidth.y * float(iTemp) + 0.5;
	return result;
}
#else
vec2 ComputePointTCs(float fIndex)
{
	vec2 result;
	result.x = mod(fIndex, v2TexWidth.x) + 0.5;
	result.y = floor(fIndex / v2TexWidth.x) + 0.5;
	return result;
}

vec2 ComputeCellTCs(float fIndex)
{
	vec2 result;
	result.x = mod(fIndex, v2TexWidth.y) + 0.5;
	result.y = floor(fIndex / v2TexWidth.y) + 0.5;
	return result;
}
#endif

vec4 ComputeBCs(vec3 pos, vec4 v4VertexIndices)
{
	x1 = texture2DRect(texUnitVertices, ComputePointTCs(v4VertexIndices.x)).xyz;
	x2 = texture2DRect(texUnitVertices, ComputePointTCs(v4VertexIndices.y)).xyz;
	x3 = texture2DRect(texUnitVertices, ComputePointTCs(v4VertexIndices.z)).xyz;
	x4 = texture2DRect(texUnitVertices, ComputePointTCs(v4VertexIndices.w)).xyz;

	vec3 v41 = x4-x1;
	vec3 v34 = x3-x4;
	vec3 v12 = x1-x2;
	vec3 v23 = x2-x3;
	vec3 v21 = x2-x1;
	vec3 v31 = x3-x1;
	
	float fDet = dot(v21, cross(v31, v41));
	vec3 vp1ByDet = (pos-x1) / fDet;
	
	vec4 weights;
	weights.y = dot(cross(v34, v41), vp1ByDet);
	weights.z = dot(cross(v12, v41), vp1ByDet);
	weights.w = dot(cross(v12, v23), vp1ByDet);
	weights.x = 1.0 - dot(vec3(1.0), weights.yzw);

	return weights;
}

float TetWalk(float fStartCell, vec3 v3Pos, out vec4 v4BCs, out vec4 v4VertexIndices)
{
	int iCount = 20;
	while (fStartCell > 0)
	{
		vec2 v2CellTCs = ComputeCellTCs(fStartCell);
		v4VertexIndices = texture2DRect(texUnitCells, v2CellTCs);
		v4BCs = ComputeBCs(v3Pos, v4VertexIndices);

		if (any(lessThan(v4BCs, vec4(-0.001))))
		{
			vec4 v4Neighbors = texture2DRect(texUnitCellNeighbors, v2CellTCs);
			float fMinBC = 0.0;
			for (int i=0; i<4; ++i)
			{
				if (v4BCs[i] < fMinBC)
				{
					fMinBC = v4BCs[i];
					fStartCell = v4Neighbors[i];
				}
			}
		}
		else
			break;

		if (iCount < 0)
		{
			fStartCell = -1;
			break;
		}
		--iCount;
	}

	return fStartCell;
}

vec2 ComputeRadii()
{
	// compute the insphere and circumsphere radii and return them
	// as values for radii.x and radii.y, respectively
	// x1 to x4 still contain the vertex coordinates from 
	// current cell as determined by the tetrahedral walk
	
	vec2 radii;
	
	vec3 v21 = x2-x1;
	vec3 v31 = x3-x1;
	vec3 v41 = x4-x1;
	
	vec3 v32 = x3-x2;
	vec3 v42 = x4-x2;
	
	vec3 v31Xv41 = cross(v31, v41);
	
	vec3 v3Temp = dot(v41,v41) * cross(v21, v31)
		+ dot(v31, v31) * cross(v41, v21)
		+ dot(v21, v21) * v31Xv41;
	radii.y = abs(0.5 * length(v3Temp) / dot(v21, v31Xv41));
	
	radii.x = abs(dot(x2, cross(x3, x4)) - dot(x1, cross(x3, x4))
		+ dot(x1, cross(x2, x4)) - dot(x1, cross(x2, x3)))
		/ (length(cross(v21, v31)) + length(cross(v41, v21))
		+ length(cross(v31, v41)) + length(cross(v42, v32)));
	
	return radii;
}

void main(void)
{
	vec4 current = texture2D(texUnitPosition, gl_TexCoord[0].xy);
	float fCell = texture2D(texUnitAttrib, gl_TexCoord[0].xy).x;
	float fDeltaTLocal = fDeltaT;

	vec4 v4BCs;
	vec4 v4VertexIndices;

	fCell = TetWalk(fCell, current.xyz, v4BCs, v4VertexIndices);


	if (fCell < 0)
	{
		gl_FragData[0] = current;
		gl_FragData[1].x = fCell;
	}
	else
	{
		vec2 v2Radii = ComputeRadii();
		int iIterations = 1;

		vec3 velocity = v4BCs.x * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.x)).xyz
			+ v4BCs.y * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.y)).xyz
			+ v4BCs.z * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.z)).xyz
			+ v4BCs.w * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.w)).xyz;

		if (v2Radii.x / v2Radii.y > 1e-5)
		{
			float fVelMag = length(velocity*fDeltaT);
			iIterations = int(max(1.0, ceil(fVelMag/v2Radii.x)));
			fDeltaTLocal /= float(iIterations);
		}

		velocity *= fDeltaTLocal;

#ifdef SHOW_ITERATIONS
		iSteps = iIterations;
#endif

		while (true)
		{
			current.xyz += velocity;

			if (--iIterations<=0)
				break;

			fCell = TetWalk(fCell, current.xyz, v4BCs, v4VertexIndices);
			velocity = v4BCs.x * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.x)).xyz
				+ v4BCs.y * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.y)).xyz
				+ v4BCs.z * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.z)).xyz
				+ v4BCs.w * texture2DRect(texUnitPointData, ComputePointTCs(v4VertexIndices.w)).xyz;
			velocity *= fDeltaTLocal;
		}

		gl_FragData[0].xyz = current.xyz;
		gl_FragData[1].x = fCell;

#ifndef SHOW_ITERATIONS
		gl_FragData[0].w = current.w;
#else
		gl_FragData[0].w = iSteps;
#endif
	}
}
