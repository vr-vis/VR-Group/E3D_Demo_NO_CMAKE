uniform vec3 v3TCScale;	// scale for converting vertex positions to texture coordinates
uniform vec3 v3TCBias;	// bias for converting vertex positions to texture coordinates

uniform float slice_distance;

// inputs:
// gl_Vertex			: vertex position

// outputs:
// gl_Position			: transformed vertex position
// gl_ClipVertex    : transformed clip vertex position, required if GL_CLIP_PLANE is used
// gl_TexCoord[0].xyz	: texture coordinates for 3D texture

void main(void)
{
	gl_Position = ftransform();
  gl_ClipVertex = gl_ModelViewMatrix*gl_Vertex;
	gl_TexCoord[0].xyz = gl_Vertex.xyz * v3TCScale + v3TCBias;

  vec4 pos = vec4(0.0,0.0,0.0,1.0);
  pos = gl_ModelViewMatrixInverse * pos;
  vec4 dir = vec4(0.0,0.0,-1.0,1.0);
  dir = normalize(gl_ModelViewMatrixInverse * dir);
  vec4 eye_to_vert = normalize(gl_Vertex - pos);
  vec4 s_b = gl_Vertex - eye_to_vert * (slice_distance / dot(dir,eye_to_vert));
	gl_TexCoord[1].xyz = s_b.xyz * v3TCScale + v3TCBias;

}
