uniform sampler3D texUnitVolume;
uniform sampler1D texUnitLookupTable;
uniform sampler1D texUnitOpacityCorrection;

uniform sampler2D texUnitPreIntegration;


uniform vec2 v2RangeParams;	// 1/(max-min) ; -min/(max-min)
uniform vec2 v2BlendingCorrection; // blending factor; sampling rate

// input:
// gl_TexCoord[0].xyz	: texture coordinate for lookup into volume data

// output:
// gl_FragColor			: final fragment color

void main(void)
{
  vec4 val;
  val.x = texture3D(texUnitVolume,gl_TexCoord[0].xyz).w;
  val.y = texture3D(texUnitVolume,gl_TexCoord[1].xyz).w;
  val = val * v2RangeParams.x + vec4(v2RangeParams.y);
    
  vec4 col = texture2D(texUnitPreIntegration,val.xy );	
	
  col.w *= v2BlendingCorrection.x;

  gl_FragColor = col;	


}
