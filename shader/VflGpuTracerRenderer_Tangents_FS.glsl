uniform sampler2D texUnitPosition;

// infinity ;)
const float g_fInf = 9999999.0;
// The point where particles are moved, with are no longer moving.
const vec3 g_v3Inf = vec3( g_fInf, g_fInf, g_fInf ); 
	
// input:
// gl_TexCoord[0].xyzw	: texture coordinates 
//						: ( u, v, offset v [prev], offset v [next] )
// gl_TexCoord[1].xy	: interval for valid texture coordinates, one by tracer length
// gl_TexCoord[1].z 	: 1.0f/m_iTracerLength

// output:
// gl_FragColor.xyz		: tangent vector
// gl_FragColor.w		: luminance information

void main(void)
{
	vec4 v4ClampedPos = gl_TexCoord[0].xyzw;
	v4ClampedPos.zw = clamp(v4ClampedPos.zw, gl_TexCoord[1].x, gl_TexCoord[1].y);

	vec3 v3Prev = texture2D(texUnitPosition, v4ClampedPos.xz).xyz;
	vec3 v3Next = texture2D(texUnitPosition, v4ClampedPos.xw).xyz;

	gl_FragColor.xyz = normalize(v3Next - v3Prev);
	gl_FragColor.w = (gl_FragCoord.y+0.5) * gl_TexCoord[1].z - 1.0;

	// The ParticleTracer setes the position of praticles, with stoped movnig, to 
	// vec3( g_fInf, g_fInf, g_fInf ).
	// Theas leasd to traces, wich sudenliy twist and go to infinety.
	// To avoid sutch traces, we apply the zero-vertor as tengent vector,
	// if the prev or the next position of the particle is locate 
	// at vec3( g_fInf, g_fInf, g_fInf ).
	if(distance(v3Next, g_v3Inf) < 0.0001) // if no movement 
		gl_FragColor.xyz = vec3(0);

	if(distance(v3Prev, g_v3Inf) < 0.0001) // if no movement 
		gl_FragColor.xyz = vec3(0);
}
