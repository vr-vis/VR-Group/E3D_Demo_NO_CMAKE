uniform sampler3D texUnitVolume;
uniform sampler1D texUnitLookupTable;
uniform sampler1D texUnitOpacityCorrection;

uniform vec2 v2RangeParams;	// 1/(max-min) ; -min/(max-min)
uniform vec2 v2BlendingCorrection; // blending factor; sampling rate

// input:
// gl_TexCoord[0].xyz	: texture coordinate for lookup into volume data

// output:
// gl_FragColor			: final fragment color

void main(void)
{
	vec4 v4Data = texture3D(texUnitVolume, gl_TexCoord[0].xyz);
	vec4 v4Color = texture1D(texUnitLookupTable, v4Data.w * v2RangeParams.x + v2RangeParams.y);

	v4Color.w = texture1D(texUnitOpacityCorrection, v4Color.w).w;
	v4Color.w *= v2BlendingCorrection.x;

	gl_FragColor = vec4(v4Color.xyz, v4Color.w);
}
