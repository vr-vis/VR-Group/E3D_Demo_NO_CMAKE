uniform sampler3D Data;
uniform sampler1D transfer_function;

void main()
{
  float sample = texture3D(Data, gl_TexCoord[0].stp).x;
  gl_FragColor = texture1D(transfer_function, sample);
}