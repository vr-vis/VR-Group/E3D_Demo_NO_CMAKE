/*============================================================================*/
/* SHADER FUNKTION															  */
/*============================================================================*/

vec4 ShadeFragment(vec3 v3Normal, vec3 v3Position, vec4 v4MatColor)
{
	vec3 light_dir  = normalize(gl_LightSource[0].position.xyz);
	return v4MatColor * dot(light_dir, v3Normal);
}

/*============================================================================*/
/* END OF FILE																  */
/*============================================================================*/