uniform vec2 v2Offsets;		// texture coordinate offsets for neighboring texels

// inputs:
// gl_Vertex			: vertex position
// gl_MultiTexCoord0	: texture coordinate for looking up particle information
// gl_MultiTexCoord1	: interval for valid texture coordinates

// outputs:
// gl_Position			: transformed vertex position
// gl_TexCoord[0].xyzw	: texture coordinates 
//						  ( u, v, offset v [prev], offset v [next] )
// gl_TexCoord[1]		: inteval for valid texture coordinates, tracer length

void main(void)
{
	gl_Position = ftransform();
	gl_TexCoord[0].xy = gl_MultiTexCoord0.xy;
	gl_TexCoord[0].zw = gl_MultiTexCoord0.yy + v2Offsets;
	gl_TexCoord[1] = gl_MultiTexCoord1;
}
