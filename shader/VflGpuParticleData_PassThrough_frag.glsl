#version 120 

uniform sampler2D samp_texture;

void main()
{
	gl_FragColor = texture2D( samp_texture, gl_TexCoord[0].st );
}