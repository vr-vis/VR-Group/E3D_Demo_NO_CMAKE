/*============================================================================*/
/*                                 VistaFlowLib                               */
/*               Copyright (c) 1998-2011 RWTH Aachen University               */
/*============================================================================*/
/*                                  License                                   */
/*                                                                            */
/*  This program is free software: you can redistribute it and/or modify      */
/*  it under the terms of the GNU Lesser General Public License as published  */
/*  by the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with this program.  If not, see <http://www.gnu.org/licenses/>.     */
/*============================================================================*/
/*                                Contributors                                */
/*                                                                            */
/*============================================================================*/
// $Id: VistaGLSLShader.h 24042 2011-11-04 12:16:11Z jh864363 $

/*============================================================================*/
/* UNIFORM VARIABLES														  */
/*============================================================================*/
uniform sampler2DRect g_texEntry;		//< entry points
uniform sampler2DRect g_texExit;		//< exit points
uniform sampler3D	  g_texVol;			//< 3D volume 
uniform sampler1D	  g_texTF;			//< transfer function 
uniform vec2		  g_v2TFRange;		//< 1/(max-min) ; -min/(max-min) for transfer function lookups
uniform vec2		  g_v2RenderParams;	//< global factor for (transparency, brightness)
uniform float		  g_fStepSize;		//< stepsize
uniform int			  g_iNumSteps;		//< number of steps

#ifdef _Multi
uniform sampler1D g_texTFRed;   //< transfer function for channel red
uniform sampler1D g_texTFGreen; //< transfer function for channel green
uniform sampler1D g_texTFBlue;  //< transfer function for channel blue

uniform vec3	  g_AlphaFade;  //< in case of segmented data in alpha channel (x = scale, y = threshold, z = alpha_weight)
#endif//_Multi

#ifdef _MagicLens
uniform sampler1D g_texMagicLens;		//< transfer function for magiclens
uniform vec4	  g_v4MagicLensParams;	//< center of magiclens lens (x,y,z), w=radius
uniform vec3	  g_v3ProxyScaling;		//< nonuniform proxy scaling

#	ifdef _Multi
uniform sampler1D g_texTFRedLens;   //< transfer function for channel red
uniform sampler1D g_texTFGreenLens; //< transfer function for channel green
uniform sampler1D g_texTFBlueLens;  //< transfer function for channel blue
#	endif//_Multi
#endif//_MagicLens

/*============================================================================*/
/* LOCAL FUNKTIONS															  */
/*============================================================================*/
// final color will be a composition of each channels color , alpha value
// will be maximum of all alpha values 
vec4 Composite(const vec4 col0, const vec4 col1, const vec4 col2, const vec4 col3)
{
	// get largest alpha value
	float max_alpha = max( max( col0.a, col1.a ),  max( col2.a, col3.a ) );
	float norm = 0.0;
	if(max_alpha > 0.0)
		norm = 1.0/max_alpha;
	// add all terms, weighted with their alpha value and normalize with maximum alpha
	vec4 comp = vec4(norm *	(	col0.xyz * col0.a + 
								col1.xyz * col1.a + 
								col2.xyz * col2.a + 
								col3.xyz * col3.a ), max_alpha);
	return comp;
}

/*============================================================================*/
/* SHADER MAIN																  */
/*============================================================================*/
void main(void)
{
	vec4 v3Entry = texture2DRect( g_texEntry, gl_TexCoord[0].xy );
	vec4 v3Exit  = texture2DRect( g_texExit,  gl_TexCoord[0].xy );

	if(v3Entry.a < 0.0001 || v3Exit.a < 0.0001) 
		discard;//< numerical issues

	vec3 v3Dir = (v3Exit - v3Entry);
	float fTotalLength = length(v3Dir);
	v3Dir = v3Dir / fTotalLength; //< normalize

	vec3 v3Pos = v3Entry.xyz;
	vec4 v3Dst = vec4(0.0,0.0,0.0,0.0); // black
	float fAlpha = 0.0;
	float fLenAcum = 0.0;
	for(int i=0; i < g_iNumSteps; ++i)
	{
#ifndef _Multi
		// specifiy which channel contains data!
		float fDensity = texture3D(g_texVol, v3Pos).a;
		float fNormalizedDensity = fDensity*g_v2TFRange.x + g_v2TFRange.y;
		vec4 v4Src	   = texture1D(g_texTF, fNormalizedDensity);
#else
		vec4 v4Density = texture3D(g_texVol, v3Pos);
		vec4 v4NormalizedDensity = v4Density*g_v2TFRange.x + vec4(g_v2TFRange.y);

		vec4 v4SrcA = texture1D( g_texTF,      v4NormalizedDensity.a );
        vec4 v4SrcR = texture1D( g_texTFRed,   v4NormalizedDensity.r );
        vec4 v4SrcG = texture1D( g_texTFGreen, v4NormalizedDensity.g );
        vec4 v4SrcB = texture1D( g_texTFBlue,  v4NormalizedDensity.b );

        if(frac(v4Density.a*g_AlphaFade.x) > g_AlphaFade.y)
			v4SrcA.a *= g_AlphaFade.z;

        vec4 v4Src = Composite(v4SrcR, v4SrcG, v4SrcB, v4SrcA);
#endif //_Multi

#ifdef _MagicLens
#	ifndef _Multi
        vec4 v4SrcAlt  = texture1D(g_texMagicLens, fNormalizedDensity);
#	else
        vec4 v4ASrcA = texture1D(g_texMagicLens,   v4NormalizedDensity.a );
        vec4 v4ASrcR = texture1D(g_texTFRedLens,   v4NormalizedDensity.r );
        vec4 v4ASrcG = texture1D(g_texTFGreenLens, v4NormalizedDensity.g );
        vec4 v4ASrcB = texture1D(g_texTFBlueLens,  v4NormalizedDensity.b );

        if(frac(v4Density.a*g_AlphaFade.x) > g_AlphaFade.y)
			v4ASrcA.a *= g_AlphaFade.z;

        vec4 v4SrcAlt = Composite(v4ASrcR, v4ASrcG, v4ASrcB, v4ASrcA);
#	endif //_Multi

		vec3 v3ScalePos = v3Pos.xyz*g_v3ProxyScaling.xyz;
        if(distance(g_v4MagicLensParams.xyz, v3ScalePos) < g_v4MagicLensParams.w)
			v4Src = v4SrcAlt;
#endif//_MagicLens
			
		v4Src.w *= g_v2RenderParams.x; //< overall transparency;
		v3Dst.xyz += (1.0 - fAlpha) * v4Src * v4Src.w; 
		v3Dst.w   += (1.0 - fAlpha) * v4Src.w; 
		fAlpha = v3Dst.w; 
		if(fAlpha > 0.99)
			break; //< no more contributions 
		v3Pos += v3Dir * g_fStepSize;
		fLenAcum += g_fStepSize;
		if(fLenAcum > fTotalLength)
			break;
		if(v3Pos.x < 0.0 || v3Pos.y < 0.0 || v3Pos.z < 0.0)
			break;
		if(v3Pos.x > 1.0 || v3Pos.y > 1.0 || v3Pos.z > 1.0)
			break;
	}
	gl_FragColor = v3Dst * g_v2RenderParams.y; //< adjust overall brightness
}