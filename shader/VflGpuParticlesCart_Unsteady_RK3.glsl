uniform sampler2D texUnitPosition;
uniform sampler3D texUnitVolume[6];
uniform vec3 v3Min, v3Max;
uniform vec3 v3Scale, v3Bias;
uniform float fDeltaT;

uniform float aWeights[6];

#ifdef TRILINEAR_INTERPOLATION
uniform vec3 v3Dims;
uniform vec3 v3OneByDims;
#endif

vec4 RetrieveData(sampler3D texUnit, vec3 v3TC)
{
#ifdef TRILINEAR_INTERPOLATION
	vec3 v3Index = v3TC * v3Dims - vec3(0.5);
	vec3 v3Floor = floor(v3Index);
	vec3 v3Alpha = v3Index - v3Floor;
	v3Floor = (v3Floor + vec3(0.5)) * v3OneByDims;

	vec4 v4Temp1 = mix(texture3D(texUnit, v3Floor),
		texture3D(texUnit, v3Floor + vec3(1.0, 0.0, 0.0)*v3OneByDims),
		v3Alpha.x);

	vec4 v4Temp2 = mix(texture3D(texUnit, v3Floor + vec3(0.0, 1.0, 0.0)*v3OneByDims),
		texture3D(texUnit, v3Floor + vec3(1.0, 1.0, 0.0)*v3OneByDims),
		v3Alpha.x);

	v4Temp1 = mix(v4Temp1, v4Temp2, v3Alpha.y);

	v4Temp2 = mix(texture3D(texUnit, v3Floor + vec3(0.0, 0.0, 1.0)*v3OneByDims),
		texture3D(texUnit, v3Floor + vec3(1.0, 0.0, 1.0)*v3OneByDims),
		v3Alpha.x);

	vec4 v4Temp3 = mix(texture3D(texUnit, v3Floor + vec3(0.0, 1.0, 1.0)*v3OneByDims),
		texture3D(texUnit, v3Floor + vec3(1.0, 1.0, 1.0)*v3OneByDims),
		v3Alpha.x);

	v4Temp2 = mix(v4Temp2, v4Temp3, v3Alpha.y);

	return mix(v4Temp1, v4Temp2, v3Alpha.z);
#else
	return texture3D(texUnit, v3TC);
#endif
}

void main(void)
{
	vec4 pos = texture2D(texUnitPosition, gl_TexCoord[0].xy);
	vec3 dxt = pos.xyz * v3Scale + v3Bias;
	vec4 dx1 = aWeights[0] * RetrieveData(texUnitVolume[0], dxt)
		+ aWeights[1] * RetrieveData(texUnitVolume[1], dxt);
	dx1.xyz *= fDeltaT;

	dxt = 0.5 * dx1.xyz + pos.xyz;
	dxt = dxt * v3Scale + v3Bias;
	vec4 dx2 = aWeights[2] * RetrieveData(texUnitVolume[2], dxt)
		+ aWeights[3] * RetrieveData(texUnitVolume[3], dxt);
	dx2.xyz *= fDeltaT;

	dxt = pos.xyz - dx1.xyz + 2*dx2.xyz;
	dxt = dxt * v3Scale + v3Bias;
	vec4 dx3 = aWeights[4] * RetrieveData(texUnitVolume[4], dxt)
		+ aWeights[5] * RetrieveData(texUnitVolume[5], dxt);
	dx3.xyz *= fDeltaT;

	vec3 result = pos.xyz + (dx1.xyz + 2*dx2.xyz + dx3.xyz) / 6.0;
	result = clamp(result, v3Min, v3Max);

  // remove particles that are standing still
  float inf = 9999999.0; // infinity ;)
  if(distance(pos,result) < 0.0001) // if no movement 
    result = vec3(inf,inf,inf); // cull away by shifting point to infinity
	
		
	gl_FragColor.xyz = result;
	gl_FragColor.w = pos.w;
}
